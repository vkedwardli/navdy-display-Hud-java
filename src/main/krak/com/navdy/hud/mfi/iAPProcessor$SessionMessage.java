package com.navdy.hud.mfi;

class iAPProcessor$SessionMessage {
    com.navdy.hud.mfi.ByteArrayBuilder builder;
    int msgId;
    int msgLen;
    final com.navdy.hud.mfi.iAPProcessor this$0;
    
    iAPProcessor$SessionMessage(com.navdy.hud.mfi.iAPProcessor a, int i, int i0) {
        super();
        this.this$0 = a;
        this.msgLen = i;
        this.msgId = i0;
        this.builder = new com.navdy.hud.mfi.ByteArrayBuilder(i);
    }
    
    void append(byte[] a) {
        try {
            this.builder.addBlob(a);
        } catch(java.io.IOException ignoredException) {
            android.util.Log.e(com.navdy.hud.mfi.iAPProcessor.access$400(), "Error appending messages ");
        }
    }
}
