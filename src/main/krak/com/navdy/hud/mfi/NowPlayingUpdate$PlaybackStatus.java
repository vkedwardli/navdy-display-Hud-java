package com.navdy.hud.mfi;


public enum NowPlayingUpdate$PlaybackStatus {
    Stopped(0),
    Playing(1),
    Paused(2),
    SeekForward(3),
    SeekBackward(4);

    private int value;
    NowPlayingUpdate$PlaybackStatus(int value) {
        this.value = value;
    }
    public int getValue() {
        return value;
    }
}

//final public class NowPlayingUpdate$PlaybackStatus extends Enum {
//    final private static com.navdy.hud.mfi.NowPlayingUpdate$PlaybackStatus[] $VALUES;
//    final public static com.navdy.hud.mfi.NowPlayingUpdate$PlaybackStatus Paused;
//    final public static com.navdy.hud.mfi.NowPlayingUpdate$PlaybackStatus Playing;
//    final public static com.navdy.hud.mfi.NowPlayingUpdate$PlaybackStatus SeekBackward;
//    final public static com.navdy.hud.mfi.NowPlayingUpdate$PlaybackStatus SeekForward;
//    final public static com.navdy.hud.mfi.NowPlayingUpdate$PlaybackStatus Stopped;
//    
//    static {
//        Stopped = new com.navdy.hud.mfi.NowPlayingUpdate$PlaybackStatus("Stopped", 0);
//        Playing = new com.navdy.hud.mfi.NowPlayingUpdate$PlaybackStatus("Playing", 1);
//        Paused = new com.navdy.hud.mfi.NowPlayingUpdate$PlaybackStatus("Paused", 2);
//        SeekForward = new com.navdy.hud.mfi.NowPlayingUpdate$PlaybackStatus("SeekForward", 3);
//        SeekBackward = new com.navdy.hud.mfi.NowPlayingUpdate$PlaybackStatus("SeekBackward", 4);
//        com.navdy.hud.mfi.NowPlayingUpdate$PlaybackStatus[] a = new com.navdy.hud.mfi.NowPlayingUpdate$PlaybackStatus[5];
//        a[0] = Stopped;
//        a[1] = Playing;
//        a[2] = Paused;
//        a[3] = SeekForward;
//        a[4] = SeekBackward;
//        $VALUES = a;
//    }
//    
//    private NowPlayingUpdate$PlaybackStatus(String s, int i) {
//        super(s, i);
//    }
//    
//    public static com.navdy.hud.mfi.NowPlayingUpdate$PlaybackStatus valueOf(String s) {
//        return (com.navdy.hud.mfi.NowPlayingUpdate$PlaybackStatus)Enum.valueOf(com.navdy.hud.mfi.NowPlayingUpdate$PlaybackStatus.class, s);
//    }
//    
//    public static com.navdy.hud.mfi.NowPlayingUpdate$PlaybackStatus[] values() {
//        return $VALUES.clone();
//    }
//}
//