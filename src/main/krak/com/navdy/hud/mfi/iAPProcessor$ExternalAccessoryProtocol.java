package com.navdy.hud.mfi;


    public enum iAPProcessor$ExternalAccessoryProtocol {
        ExternalAccessoryProtocolIdentifier(0),
    ExternalAccessoryProtocolName(1),
    ExternalAccessoryProtocolMatchAction(2),
    NativeTransportComponentIdentifier(3);

        private int value;
        iAPProcessor$ExternalAccessoryProtocol(int value) {
            this.value = value;
        }
        public int getValue() {
            return value;
        }
    }
    
//final class iAPProcessor$ExternalAccessoryProtocol extends Enum {
//    final private static com.navdy.hud.mfi.iAPProcessor$ExternalAccessoryProtocol[] $VALUES;
//    final public static com.navdy.hud.mfi.iAPProcessor$ExternalAccessoryProtocol ExternalAccessoryProtocolIdentifier;
//    final public static com.navdy.hud.mfi.iAPProcessor$ExternalAccessoryProtocol ExternalAccessoryProtocolMatchAction;
//    final public static com.navdy.hud.mfi.iAPProcessor$ExternalAccessoryProtocol ExternalAccessoryProtocolName;
//    final public static com.navdy.hud.mfi.iAPProcessor$ExternalAccessoryProtocol NativeTransportComponentIdentifier;
//    
//    static {
//        ExternalAccessoryProtocolIdentifier = new com.navdy.hud.mfi.iAPProcessor$ExternalAccessoryProtocol("ExternalAccessoryProtocolIdentifier", 0);
//        ExternalAccessoryProtocolName = new com.navdy.hud.mfi.iAPProcessor$ExternalAccessoryProtocol("ExternalAccessoryProtocolName", 1);
//        ExternalAccessoryProtocolMatchAction = new com.navdy.hud.mfi.iAPProcessor$ExternalAccessoryProtocol("ExternalAccessoryProtocolMatchAction", 2);
//        NativeTransportComponentIdentifier = new com.navdy.hud.mfi.iAPProcessor$ExternalAccessoryProtocol("NativeTransportComponentIdentifier", 3);
//        com.navdy.hud.mfi.iAPProcessor$ExternalAccessoryProtocol[] a = new com.navdy.hud.mfi.iAPProcessor$ExternalAccessoryProtocol[4];
//        a[0] = ExternalAccessoryProtocolIdentifier;
//        a[1] = ExternalAccessoryProtocolName;
//        a[2] = ExternalAccessoryProtocolMatchAction;
//        a[3] = NativeTransportComponentIdentifier;
//        $VALUES = a;
//    }
//    
//    private iAPProcessor$ExternalAccessoryProtocol(String s, int i) {
//        super(s, i);
//    }
//    
//    public static com.navdy.hud.mfi.iAPProcessor$ExternalAccessoryProtocol valueOf(String s) {
//        return (com.navdy.hud.mfi.iAPProcessor$ExternalAccessoryProtocol)Enum.valueOf(com.navdy.hud.mfi.iAPProcessor$ExternalAccessoryProtocol.class, s);
//    }
//    
//    public static com.navdy.hud.mfi.iAPProcessor$ExternalAccessoryProtocol[] values() {
//        return $VALUES.clone();
//    }
//}
//