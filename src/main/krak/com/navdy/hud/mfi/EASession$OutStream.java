package com.navdy.hud.mfi;

class EASession$OutStream extends java.io.OutputStream {
    private int FLUSH_DELAY;
    private Runnable autoFlush;
    private java.util.List frames;
    private android.os.Handler handler;
    final com.navdy.hud.mfi.EASession this$0;
    
    EASession$OutStream(com.navdy.hud.mfi.EASession a) {
        super();
        this.this$0 = a;
        this.handler = new android.os.Handler(android.os.Looper.getMainLooper());
        this.FLUSH_DELAY = 250;
        this.frames = (java.util.List)new java.util.ArrayList();
        this.autoFlush = (Runnable)new com.navdy.hud.mfi.EASession$OutStream$1(this);
    }
    
    private byte[] assembleFrames() {
        int i = 0;
        int i0 = 0;
        while(i0 < this.frames.size()) {
            i = i + ((byte[])this.frames.get(i0)).length;
            i0 = i0 + 1;
        }
        byte[] a = null;
        if (i > 0) {
            if (this.frames.size() != 1) {
                a = new byte[i];
                int i1 = 0;
                int i2 = 0;
                while(i2 < this.frames.size()) {
                    byte[] a0 = (byte[])this.frames.get(i2);
                    System.arraycopy(a0, 0, a, i1, a0.length);
                    i1 = i1 + a0.length;
                    i2 = i2 + 1;
                }
            } else {
                a = (byte[])this.frames.get(0);
            }
            this.frames.clear();
        }
        return a;
    }
    
    public void close() {
        this.flush();
    }
    
    public void flush() {
        synchronized(this) {
            this.handler.removeCallbacks(this.autoFlush);
            byte[] a = this.assembleFrames();
            if (!com.navdy.hud.mfi.EASession.access$100(this.this$0) && a != null) {
                com.navdy.hud.mfi.EASession.access$200(this.this$0).queue(new com.navdy.hud.mfi.EASessionPacket(this.this$0.getSessionIdentifier(), a));
            }
        }
        /*monexit(this)*/;
    }
    
    public void write(int i) {
        byte[] a = new byte[1];
        int i0 = (byte)i;
        a[0] = (byte)i0;
        this.write(a);
    }
    
    public void write(byte[] a) {
        synchronized(this) {
            if (android.util.Log.isLoggable(com.navdy.hud.mfi.EASession.access$300(), 3)) {
                String s = com.navdy.hud.mfi.EASession.access$300();
                Object[] a0 = new Object[4];
                a0[0] = com.navdy.hud.mfi.EASession.access$400(this.this$0);
                a0[1] = Integer.valueOf(a.length);
                a0[2] = com.navdy.hud.mfi.Utils.bytesToHex(a, true);
                a0[3] = com.navdy.hud.mfi.Utils.toASCII(a);
                android.util.Log.d(s, String.format("%s: OutStream.write: %d bytes: %s, '%s'", a0));
            }
            this.frames.add(a);
            this.handler.removeCallbacks(this.autoFlush);
            this.handler.postDelayed(this.autoFlush, (long)this.FLUSH_DELAY);
        }
        /*monexit(this)*/;
    }
    
    public void write(byte[] a, int i, int i0) {
        this.write(java.util.Arrays.copyOfRange(a, i, i0));
    }
}
