package com.navdy.hud.mfi;

abstract public interface IAPFileTransferListener {
    abstract public void onFileReceived(int arg, byte[] arg0);
    
    
    abstract public void onFileTransferCancel(int arg);
    
    
    abstract public void onFileTransferSetup(int arg, long arg0);
}
