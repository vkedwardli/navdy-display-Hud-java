package com.navdy.hud.mfi;


public enum IAPCommunicationsManager$Service {
    Unknown(0),
    Telephony(1),
    FaceTimeAudio(2),
    FaceTimeVideo(3);

    private int value;
    IAPCommunicationsManager$Service(int value) {
        this.value = value;
    }
    public int getValue() {
        return value;
    }
}

//final public class IAPCommunicationsManager$Service extends Enum {
//    final private static com.navdy.hud.mfi.IAPCommunicationsManager$Service[] $VALUES;
//    final public static com.navdy.hud.mfi.IAPCommunicationsManager$Service FaceTimeAudio;
//    final public static com.navdy.hud.mfi.IAPCommunicationsManager$Service FaceTimeVideo;
//    final public static com.navdy.hud.mfi.IAPCommunicationsManager$Service Telephony;
//    final public static com.navdy.hud.mfi.IAPCommunicationsManager$Service Unknown;
//    
//    static {
//        Unknown = new com.navdy.hud.mfi.IAPCommunicationsManager$Service("Unknown", 0);
//        Telephony = new com.navdy.hud.mfi.IAPCommunicationsManager$Service("Telephony", 1);
//        FaceTimeAudio = new com.navdy.hud.mfi.IAPCommunicationsManager$Service("FaceTimeAudio", 2);
//        FaceTimeVideo = new com.navdy.hud.mfi.IAPCommunicationsManager$Service("FaceTimeVideo", 3);
//        com.navdy.hud.mfi.IAPCommunicationsManager$Service[] a = new com.navdy.hud.mfi.IAPCommunicationsManager$Service[4];
//        a[0] = Unknown;
//        a[1] = Telephony;
//        a[2] = FaceTimeAudio;
//        a[3] = FaceTimeVideo;
//        $VALUES = a;
//    }
//    
//    private IAPCommunicationsManager$Service(String s, int i) {
//        super(s, i);
//    }
//    
//    public static com.navdy.hud.mfi.IAPCommunicationsManager$Service valueOf(String s) {
//        return (com.navdy.hud.mfi.IAPCommunicationsManager$Service)Enum.valueOf(com.navdy.hud.mfi.IAPCommunicationsManager$Service.class, s);
//    }
//    
//    public static com.navdy.hud.mfi.IAPCommunicationsManager$Service[] values() {
//        return $VALUES.clone();
//    }
//}
//