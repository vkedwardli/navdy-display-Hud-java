package com.navdy.hud.mfi;

public class BluetoothIAP2Server implements com.navdy.hud.mfi.LinkLayer$PhysicalLayer {
    final public static java.util.UUID ACCESSORY_IAP2;
    final public static java.util.UUID DEVICE_IAP2;
    final private static String SDP_NAME = "Wireless iAP";
    final public static int STATE_CONNECTED = 3;
    final public static int STATE_CONNECTING = 2;
    final public static int STATE_LISTEN = 1;
    final public static int STATE_NONE = 0;
    final private static String TAG = "BluetoothIAP2Server";
    private com.navdy.hud.mfi.LinkLayer linkLayer;
    private com.navdy.hud.mfi.BluetoothIAP2Server$AcceptThread mAcceptThread;
    final private android.bluetooth.BluetoothAdapter mAdapter;
    private com.navdy.hud.mfi.BluetoothIAP2Server$ConnectThread mConnectThread;
    private com.navdy.hud.mfi.BluetoothIAP2Server$ConnectedThread mConnectedThread;
    final private android.os.Handler mHandler;
    private int mState;
    
    static {
        DEVICE_IAP2 = java.util.UUID.fromString("00000000-deca-fade-deca-deafdecacafe");
        ACCESSORY_IAP2 = java.util.UUID.fromString("00000000-deca-fade-deca-deafdecacaff");
    }
    
    public BluetoothIAP2Server(android.os.Handler a) {
        this.mAdapter = android.bluetooth.BluetoothAdapter.getDefaultAdapter();
        this.mState = 0;
        this.mHandler = a;
    }
    
    static android.bluetooth.BluetoothAdapter access$000(com.navdy.hud.mfi.BluetoothIAP2Server a) {
        return a.mAdapter;
    }
    
    static int access$100(com.navdy.hud.mfi.BluetoothIAP2Server a) {
        return a.mState;
    }
    
    static void access$200(com.navdy.hud.mfi.BluetoothIAP2Server a) {
        a.connectionFailed();
    }
    
    static com.navdy.hud.mfi.BluetoothIAP2Server$ConnectThread access$302(com.navdy.hud.mfi.BluetoothIAP2Server a, com.navdy.hud.mfi.BluetoothIAP2Server$ConnectThread a0) {
        a.mConnectThread = a0;
        return a0;
    }
    
    static com.navdy.hud.mfi.LinkLayer access$400(com.navdy.hud.mfi.BluetoothIAP2Server a) {
        return a.linkLayer;
    }
    
    static void access$500(com.navdy.hud.mfi.BluetoothIAP2Server a) {
        a.connectionLost();
    }
    
    private void connectionFailed() {
        android.os.Message a = this.mHandler.obtainMessage(5);
        android.os.Bundle a0 = new android.os.Bundle();
        a0.putString("toast", "Unable to connect device");
        a.setData(a0);
        this.mHandler.sendMessage(a);
        this.start();
    }
    
    private void connectionLost() {
        this.linkLayer.connectionEnded();
        android.os.Message a = this.mHandler.obtainMessage(5);
        android.os.Bundle a0 = new android.os.Bundle();
        a0.putString("toast", "Device connection was lost");
        a.setData(a0);
        this.mHandler.sendMessage(a);
        this.mHandler.postDelayed((Runnable)new com.navdy.hud.mfi.BluetoothIAP2Server$1(this), 500L);
    }
    
    private void setState(int i) {
        synchronized(this) {
            android.util.Log.d("BluetoothIAP2Server", new StringBuilder().append("setState() ").append(this.mState).append(" -> ").append(i).toString());
            this.mState = i;
            this.mHandler.obtainMessage(1, i, -1).sendToTarget();
        }
        /*monexit(this)*/;
    }
    
    public void connect(android.bluetooth.BluetoothDevice a) {
        synchronized(this) {
            android.util.Log.d("BluetoothIAP2Server", new StringBuilder().append("connect to: ").append(a).toString());
            if (this.mConnectThread != null) {
                this.mConnectThread.cancel();
                this.mConnectThread = null;
            }
            if (this.mConnectedThread != null) {
                this.mConnectedThread.cancel();
                this.mConnectedThread = null;
            }
            this.mConnectThread = new com.navdy.hud.mfi.BluetoothIAP2Server$ConnectThread(this, a);
            this.mConnectThread.start();
            this.setState(2);
        }
        /*monexit(this)*/;
    }
    
    public void connect(com.navdy.hud.mfi.LinkLayer a) {
        this.linkLayer = a;
    }
    
    public void connected(android.bluetooth.BluetoothSocket a, android.bluetooth.BluetoothDevice a0) {
        synchronized(this) {
            android.util.Log.d("BluetoothIAP2Server", "connected");
            if (this.mConnectThread != null) {
                this.mConnectThread.cancel();
                this.mConnectThread = null;
            }
            if (this.mConnectedThread != null) {
                this.mConnectedThread.cancel();
                this.mConnectedThread = null;
            }
            if (this.mAcceptThread != null) {
                this.mAcceptThread.cancel();
                this.mAcceptThread = null;
            }
            this.mConnectedThread = new com.navdy.hud.mfi.BluetoothIAP2Server$ConnectedThread(this, a);
            this.mConnectedThread.start();
            android.os.Message a1 = this.mHandler.obtainMessage(4);
            android.os.Bundle a2 = new android.os.Bundle();
            a2.putString("device_name", a0.getName());
            a1.setData(a2);
            this.mHandler.sendMessage(a1);
            this.setState(3);
        }
        /*monexit(this)*/;
    }
    
    public byte[] getLocalAddress() {
        return com.navdy.hud.mfi.Utils.parseMACAddress(this.mAdapter.getAddress());
    }
    
    public String getName() {
        return this.mAdapter.getName();
    }
    
    public void queue(com.navdy.hud.mfi.LinkPacket a) {
        this.write(a.data);
    }
    
    public void reconfirmDiscoverableState(android.app.Activity a) {
        synchronized(this) {
            if (a == null) {
                break label0;
            }
            try {
                Exception a1 = null;
                try {
                    android.content.Intent a2 = new android.content.Intent("android.bluetooth.adapter.action.REQUEST_DISCOVERABLE");
                    a2.putExtra("android.bluetooth.adapter.extra.DISCOVERABLE_DURATION", 300);
                    a.startActivity(a2);
                    android.util.Log.d("BluetoothIAP2Server", "Now Discoverable");
                    break label0;
                } catch(Exception a3) {
                    a1 = a3;
                }
                android.util.Log.e("BluetoothIAP2Server", "Failed to make discoverable", (Throwable)a1);
                break label0;
            } catch(Throwable a4) {
                a0 = a4;
            }
            /*monexit(this)*/;
            throw a0;
        }
        /*monexit(this)*/;
    }
    
    public void start() {
        synchronized(this) {
            android.util.Log.d("BluetoothIAP2Server", "start");
            this.setState(1);
            if (this.mAcceptThread == null) {
                this.mAcceptThread = new com.navdy.hud.mfi.BluetoothIAP2Server$AcceptThread(this);
                this.mAcceptThread.start();
            }
        }
        /*monexit(this)*/;
    }
    
    public void stop() {
        synchronized(this) {
            android.util.Log.d("BluetoothIAP2Server", "stop");
            if (this.mConnectThread != null) {
                this.mConnectThread.cancel();
                this.mConnectThread = null;
            }
            if (this.mConnectedThread != null) {
                this.mConnectedThread.cancel();
                this.mConnectedThread = null;
            }
            if (this.mAcceptThread != null) {
                this.mAcceptThread.cancel();
                this.mAcceptThread = null;
            }
            this.setState(0);
        }
        /*monexit(this)*/;
    }
    
    public void write(byte[] a) {
        label2: synchronized(this) {
            com.navdy.hud.mfi.BluetoothIAP2Server$ConnectedThread a0 = null;
            label1: try {
                int i = this.mState;
                label0: {
                    if (i != 3) {
                        break label0;
                    }
                    a0 = this.mConnectedThread;
                    /*monexit(this)*/;
                    break label1;
                }
                /*monexit(this)*/;
                break label2;
            } catch(IllegalMonitorStateException | NullPointerException a1) {
                RuntimeException a2 = a1;
                while(true) {
                    Throwable a3 = null;
                    try {
                        /*monexit(this)*/;
                        a3 = a2;
                    } catch(IllegalMonitorStateException | NullPointerException a4) {
                        a2 = a4;
                        continue;
                    }
                    throw a3;
                }
            }
            a0.write(a);
        }
    }
}
