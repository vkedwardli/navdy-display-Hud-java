package com.navdy.hud.mfi;

abstract public interface IAPListener {
    abstract public void onCoprocessorStatusCheckFailed(String arg, String arg0, String arg1);
    
    
    abstract public void onDeviceAuthenticationSuccess(int arg);
}
