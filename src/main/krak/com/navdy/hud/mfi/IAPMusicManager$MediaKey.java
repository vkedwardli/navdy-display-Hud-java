package com.navdy.hud.mfi;


public enum IAPMusicManager$MediaKey {
    Siri(0),
    PlayPause(1),
    Next(2),
    Previous(3);

    private int value;
    IAPMusicManager$MediaKey(int value) {
        this.value = value;
    }
    public int getValue() {
        return value;
    }
}

//final public class IAPMusicManager$MediaKey extends Enum {
//    final private static com.navdy.hud.mfi.IAPMusicManager$MediaKey[] $VALUES;
//    final public static com.navdy.hud.mfi.IAPMusicManager$MediaKey Next;
//    final public static com.navdy.hud.mfi.IAPMusicManager$MediaKey PlayPause;
//    final public static com.navdy.hud.mfi.IAPMusicManager$MediaKey Previous;
//    final public static com.navdy.hud.mfi.IAPMusicManager$MediaKey Siri;
//    
//    static {
//        Siri = new com.navdy.hud.mfi.IAPMusicManager$MediaKey("Siri", 0);
//        PlayPause = new com.navdy.hud.mfi.IAPMusicManager$MediaKey("PlayPause", 1);
//        Next = new com.navdy.hud.mfi.IAPMusicManager$MediaKey("Next", 2);
//        Previous = new com.navdy.hud.mfi.IAPMusicManager$MediaKey("Previous", 3);
//        com.navdy.hud.mfi.IAPMusicManager$MediaKey[] a = new com.navdy.hud.mfi.IAPMusicManager$MediaKey[4];
//        a[0] = Siri;
//        a[1] = PlayPause;
//        a[2] = Next;
//        a[3] = Previous;
//        $VALUES = a;
//    }
//    
//    private IAPMusicManager$MediaKey(String s, int i) {
//        super(s, i);
//    }
//    
//    public static com.navdy.hud.mfi.IAPMusicManager$MediaKey valueOf(String s) {
//        return (com.navdy.hud.mfi.IAPMusicManager$MediaKey)Enum.valueOf(com.navdy.hud.mfi.IAPMusicManager$MediaKey.class, s);
//    }
//    
//    public static com.navdy.hud.mfi.IAPMusicManager$MediaKey[] values() {
//        return $VALUES.clone();
//    }
//}
//