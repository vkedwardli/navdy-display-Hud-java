package com.navdy.hud.mfi;

abstract public interface IAPCommunicationsUpdateListener {
    abstract public void onCallStateUpdate(com.navdy.hud.mfi.CallStateUpdate arg);
    
    
    abstract public void onCommunicationUpdate(com.navdy.hud.mfi.CommunicationUpdate arg);
}
