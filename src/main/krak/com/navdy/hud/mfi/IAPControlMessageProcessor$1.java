package com.navdy.hud.mfi;

class IAPControlMessageProcessor$1 implements Runnable {
    final com.navdy.hud.mfi.IAPControlMessageProcessor this$0;
    final byte[] val$data;
    final com.navdy.hud.mfi.iAPProcessor$iAPMessage val$message;
    final int val$session;
    
    IAPControlMessageProcessor$1(com.navdy.hud.mfi.IAPControlMessageProcessor a, com.navdy.hud.mfi.iAPProcessor$iAPMessage a0, int i, byte[] a1) {
        super();
        this.this$0 = a;
        this.val$message = a0;
        this.val$session = i;
        this.val$data = a1;
    }
    
    public void run() {
        this.this$0.bProcessControlMessage(this.val$message, this.val$session, this.val$data);
    }
}
