package com.navdy.hud.mfi;

public class IAPCommunicationsManager extends com.navdy.hud.mfi.IAPControlMessageProcessor {
    private static String TAG;
    public static com.navdy.hud.mfi.iAPProcessor$iAPMessage[] mMessages;
    private com.navdy.hud.mfi.CallStateUpdate$Status callStatus;
    private String callUUid;
    private com.navdy.hud.mfi.IAPCommunicationsUpdateListener mUpdatesListener;
    
    static {
        TAG = com.navdy.hud.mfi.IAPCommunicationsManager.class.getSimpleName();
        com.navdy.hud.mfi.iAPProcessor$iAPMessage[] a = new com.navdy.hud.mfi.iAPProcessor$iAPMessage[2];
        a[0] = com.navdy.hud.mfi.iAPProcessor$iAPMessage.CallStateUpdate;
        a[1] = com.navdy.hud.mfi.iAPProcessor$iAPMessage.CommunicationUpdate;
        mMessages = a;
    }
    
    public IAPCommunicationsManager(com.navdy.hud.mfi.iAPProcessor a) {
        super(mMessages, a);
    }
    
    public static com.navdy.hud.mfi.CallStateUpdate parseCallStateUpdate(com.navdy.hud.mfi.iAPProcessor$IAP2Params a) {
        com.navdy.hud.mfi.CallStateUpdate a0 = new com.navdy.hud.mfi.CallStateUpdate();
        if (a.hasParam((Enum)com.navdy.hud.mfi.IAPCommunicationsManager$StartCallStateUpdates.RemoteID)) {
            a0.remoteID = a.getUTF8((Enum)com.navdy.hud.mfi.IAPCommunicationsManager$StartCallStateUpdates.RemoteID);
        }
        if (a.hasParam((Enum)com.navdy.hud.mfi.IAPCommunicationsManager$StartCallStateUpdates.DisplayName)) {
            a0.displayName = a.getUTF8((Enum)com.navdy.hud.mfi.IAPCommunicationsManager$StartCallStateUpdates.DisplayName);
        }
        if (a.hasParam((Enum)com.navdy.hud.mfi.IAPCommunicationsManager$StartCallStateUpdates.Status)) {
            a0.status = (com.navdy.hud.mfi.CallStateUpdate$Status)a.getEnum(com.navdy.hud.mfi.CallStateUpdate$Status.class, (Enum)com.navdy.hud.mfi.IAPCommunicationsManager$StartCallStateUpdates.Status);
        }
        if (a.hasParam((Enum)com.navdy.hud.mfi.IAPCommunicationsManager$StartCallStateUpdates.Direction)) {
            a0.direction = (com.navdy.hud.mfi.CallStateUpdate$Direction)a.getEnum(com.navdy.hud.mfi.CallStateUpdate$Direction.class, (Enum)com.navdy.hud.mfi.IAPCommunicationsManager$StartCallStateUpdates.Direction);
        }
        if (a.hasParam((Enum)com.navdy.hud.mfi.IAPCommunicationsManager$StartCallStateUpdates.CallUUID)) {
            a0.callUUID = a.getUTF8((Enum)com.navdy.hud.mfi.IAPCommunicationsManager$StartCallStateUpdates.CallUUID);
        }
        if (a.hasParam((Enum)com.navdy.hud.mfi.IAPCommunicationsManager$StartCallStateUpdates.AddressBookID)) {
            a0.addressBookID = a.getUTF8((Enum)com.navdy.hud.mfi.IAPCommunicationsManager$StartCallStateUpdates.AddressBookID);
        }
        if (a.hasParam((Enum)com.navdy.hud.mfi.IAPCommunicationsManager$StartCallStateUpdates.Label)) {
            a0.label = a.getUTF8((Enum)com.navdy.hud.mfi.IAPCommunicationsManager$StartCallStateUpdates.Label);
        }
        if (a.hasParam((Enum)com.navdy.hud.mfi.IAPCommunicationsManager$StartCallStateUpdates.Service)) {
            a0.service = (com.navdy.hud.mfi.IAPCommunicationsManager$Service)a.getEnum(com.navdy.hud.mfi.IAPCommunicationsManager$Service.class, (Enum)com.navdy.hud.mfi.IAPCommunicationsManager$StartCallStateUpdates.Service);
        }
        if (a.hasParam((Enum)com.navdy.hud.mfi.IAPCommunicationsManager$StartCallStateUpdates.IsConferenced)) {
            a0.isConferenced = a.getBoolean((Enum)com.navdy.hud.mfi.IAPCommunicationsManager$StartCallStateUpdates.IsConferenced);
        }
        if (a.hasParam((Enum)com.navdy.hud.mfi.IAPCommunicationsManager$StartCallStateUpdates.ConferenceGroup)) {
            a0.conferenceGroup = a.getUInt8((Enum)com.navdy.hud.mfi.IAPCommunicationsManager$StartCallStateUpdates.ConferenceGroup);
        }
        if (a.hasParam((Enum)com.navdy.hud.mfi.IAPCommunicationsManager$StartCallStateUpdates.DisconnectReason)) {
            a0.disconnectReason = (com.navdy.hud.mfi.CallStateUpdate$DisconnectReason)a.getEnum(com.navdy.hud.mfi.CallStateUpdate$DisconnectReason.class, (Enum)com.navdy.hud.mfi.IAPCommunicationsManager$StartCallStateUpdates.DisconnectReason);
        }
        return a0;
    }
    
    public static com.navdy.hud.mfi.CommunicationUpdate parseCommunicationUpdate(com.navdy.hud.mfi.iAPProcessor$IAP2Params a) {
        com.navdy.hud.mfi.CommunicationUpdate a0 = new com.navdy.hud.mfi.CommunicationUpdate();
        if (a.hasParam(com.navdy.hud.mfi.IAPCommunicationsManager$StartCommunicationUpdates.MuteStatus.id)) {
            a0.muteStatus = a.getBoolean(com.navdy.hud.mfi.IAPCommunicationsManager$StartCommunicationUpdates.MuteStatus.id);
        }
        return a0;
    }
    
    public void acceptCall() {
        if (!android.text.TextUtils.isEmpty((CharSequence)this.callUUid)) {
            com.navdy.hud.mfi.iAPProcessor$IAP2SessionMessage a = new com.navdy.hud.mfi.iAPProcessor$IAP2SessionMessage(com.navdy.hud.mfi.iAPProcessor$iAPMessage.AcceptCall);
            a.addEnum((Enum)com.navdy.hud.mfi.IAPCommunicationsManager$AcceptCall.AcceptAction, (Enum)com.navdy.hud.mfi.IAPCommunicationsManager$AcceptAction.HoldAndAccept);
            a.addString((Enum)com.navdy.hud.mfi.IAPCommunicationsManager$AcceptCall.CallUUID, this.callUUid);
            this.miAPProcessor.sendControlMessage((com.navdy.hud.mfi.iAPProcessor$IAP2ParamsCreator)a);
        }
    }
    
    public void acceptCall(String s) {
        if (!android.text.TextUtils.isEmpty((CharSequence)s)) {
            com.navdy.hud.mfi.iAPProcessor$IAP2SessionMessage a = new com.navdy.hud.mfi.iAPProcessor$IAP2SessionMessage(com.navdy.hud.mfi.iAPProcessor$iAPMessage.AcceptCall);
            a.addEnum((Enum)com.navdy.hud.mfi.IAPCommunicationsManager$AcceptCall.AcceptAction, (Enum)com.navdy.hud.mfi.IAPCommunicationsManager$AcceptAction.HoldAndAccept);
            a.addString((Enum)com.navdy.hud.mfi.IAPCommunicationsManager$AcceptCall.CallUUID, s);
            this.miAPProcessor.sendControlMessage((com.navdy.hud.mfi.iAPProcessor$IAP2ParamsCreator)a);
        }
    }
    
    public void bProcessControlMessage(com.navdy.hud.mfi.iAPProcessor$iAPMessage a, int i, byte[] a0) {
        com.navdy.hud.mfi.iAPProcessor$IAP2Params a1 = com.navdy.hud.mfi.iAPProcessor.parse(a0);
        int i0 = a.id;
        int i1 = com.navdy.hud.mfi.iAPProcessor$iAPMessage.CallStateUpdate.id;
        label1: {
            Throwable a2 = null;
            label0: {
                Throwable a3 = null;
                label2: if (i0 != i1) {
                    if (a.id != com.navdy.hud.mfi.iAPProcessor$iAPMessage.CommunicationUpdate.id) {
                        break label1;
                    }
                    com.navdy.hud.mfi.CommunicationUpdate a4 = com.navdy.hud.mfi.IAPCommunicationsManager.parseCommunicationUpdate(a1);
                    {
                        try {
                            this.mUpdatesListener.onCommunicationUpdate(a4);
                        } catch(Throwable a5) {
                            a3 = a5;
                            break label2;
                        }
                        break label1;
                    }
                } else {
                    com.navdy.hud.mfi.CallStateUpdate a6 = com.navdy.hud.mfi.IAPCommunicationsManager.parseCallStateUpdate(a1);
                    this.callStatus = a6.status;
                    this.callUUid = a6.callUUID;
                    try {
                        this.mUpdatesListener.onCallStateUpdate(a6);
                        break label1;
                    } catch(Throwable a7) {
                        a2 = a7;
                        break label0;
                    }
                }
                android.util.Log.d(TAG, new StringBuilder().append("Bad call state update listener ").append(a3).toString());
                break label1;
            }
            android.util.Log.d(TAG, new StringBuilder().append("Bad call state update listener ").append(a2).toString());
        }
    }
    
    public void callVoiceMail() {
        com.navdy.hud.mfi.iAPProcessor$IAP2SessionMessage a = new com.navdy.hud.mfi.iAPProcessor$IAP2SessionMessage(com.navdy.hud.mfi.iAPProcessor$iAPMessage.InitiateCall);
        a.addEnum((Enum)com.navdy.hud.mfi.IAPCommunicationsManager$InitiateCall.Type, (Enum)com.navdy.hud.mfi.IAPCommunicationsManager$InitiateCallType.VoiceMail);
        this.miAPProcessor.sendControlMessage((com.navdy.hud.mfi.iAPProcessor$IAP2ParamsCreator)a);
    }
    
    public void endCall() {
        if (!android.text.TextUtils.isEmpty((CharSequence)this.callUUid)) {
            com.navdy.hud.mfi.iAPProcessor$IAP2SessionMessage a = new com.navdy.hud.mfi.iAPProcessor$IAP2SessionMessage(com.navdy.hud.mfi.iAPProcessor$iAPMessage.EndCall);
            a.addEnum((Enum)com.navdy.hud.mfi.IAPCommunicationsManager$EndCall.EndAction, (Enum)com.navdy.hud.mfi.IAPCommunicationsManager$EndCallAction.EndDecline);
            a.addString((Enum)com.navdy.hud.mfi.IAPCommunicationsManager$EndCall.CallUUID, this.callUUid);
            this.miAPProcessor.sendControlMessage((com.navdy.hud.mfi.iAPProcessor$IAP2ParamsCreator)a);
        }
    }
    
    public void endCall(String s) {
        if (!android.text.TextUtils.isEmpty((CharSequence)s)) {
            com.navdy.hud.mfi.iAPProcessor$IAP2SessionMessage a = new com.navdy.hud.mfi.iAPProcessor$IAP2SessionMessage(com.navdy.hud.mfi.iAPProcessor$iAPMessage.EndCall);
            a.addEnum((Enum)com.navdy.hud.mfi.IAPCommunicationsManager$EndCall.EndAction, (Enum)com.navdy.hud.mfi.IAPCommunicationsManager$EndCallAction.EndDecline);
            a.addString((Enum)com.navdy.hud.mfi.IAPCommunicationsManager$EndCall.CallUUID, s);
            this.miAPProcessor.sendControlMessage((com.navdy.hud.mfi.iAPProcessor$IAP2ParamsCreator)a);
        }
    }
    
    public void initiateDestinationCall(String s) {
        com.navdy.hud.mfi.iAPProcessor$IAP2SessionMessage a = new com.navdy.hud.mfi.iAPProcessor$IAP2SessionMessage(com.navdy.hud.mfi.iAPProcessor$iAPMessage.InitiateCall);
        a.addEnum((Enum)com.navdy.hud.mfi.IAPCommunicationsManager$InitiateCall.Type, (Enum)com.navdy.hud.mfi.IAPCommunicationsManager$InitiateCallType.Destination);
        a.addString((Enum)com.navdy.hud.mfi.IAPCommunicationsManager$InitiateCall.DestinationID, s);
        a.addEnum((Enum)com.navdy.hud.mfi.IAPCommunicationsManager$InitiateCall.Service, (Enum)com.navdy.hud.mfi.IAPCommunicationsManager$Service.Telephony);
        this.miAPProcessor.sendControlMessage((com.navdy.hud.mfi.iAPProcessor$IAP2ParamsCreator)a);
    }
    
    public void mute() {
        com.navdy.hud.mfi.iAPProcessor$IAP2SessionMessage a = new com.navdy.hud.mfi.iAPProcessor$IAP2SessionMessage(com.navdy.hud.mfi.iAPProcessor$iAPMessage.MuteStatusUpdate);
        a.addBoolean((Enum)com.navdy.hud.mfi.IAPCommunicationsManager$MuteStatusUpdate.MuteStatus, true);
        this.miAPProcessor.sendControlMessage((com.navdy.hud.mfi.iAPProcessor$IAP2ParamsCreator)a);
    }
    
    public void redial() {
        com.navdy.hud.mfi.iAPProcessor$IAP2SessionMessage a = new com.navdy.hud.mfi.iAPProcessor$IAP2SessionMessage(com.navdy.hud.mfi.iAPProcessor$iAPMessage.InitiateCall);
        a.addEnum((Enum)com.navdy.hud.mfi.IAPCommunicationsManager$InitiateCall.Type, (Enum)com.navdy.hud.mfi.IAPCommunicationsManager$InitiateCallType.Redial);
        this.miAPProcessor.sendControlMessage((com.navdy.hud.mfi.iAPProcessor$IAP2ParamsCreator)a);
    }
    
    public void setUpdatesListener(com.navdy.hud.mfi.IAPCommunicationsUpdateListener a) {
        this.mUpdatesListener = a;
    }
    
    public void startCommunicationUpdates() {
    }
    
    public void startUpdates() {
        com.navdy.hud.mfi.iAPProcessor$IAP2SessionMessage a = new com.navdy.hud.mfi.iAPProcessor$IAP2SessionMessage(com.navdy.hud.mfi.iAPProcessor$iAPMessage.StartCallStateUpdates);
        a.addNone((Enum)com.navdy.hud.mfi.IAPCommunicationsManager$StartCallStateUpdates.RemoteID).addNone((Enum)com.navdy.hud.mfi.IAPCommunicationsManager$StartCallStateUpdates.DisplayName).addNone((Enum)com.navdy.hud.mfi.IAPCommunicationsManager$StartCallStateUpdates.Status).addNone((Enum)com.navdy.hud.mfi.IAPCommunicationsManager$StartCallStateUpdates.Direction).addNone((Enum)com.navdy.hud.mfi.IAPCommunicationsManager$StartCallStateUpdates.CallUUID).addNone((Enum)com.navdy.hud.mfi.IAPCommunicationsManager$StartCallStateUpdates.AddressBookID).addNone((Enum)com.navdy.hud.mfi.IAPCommunicationsManager$StartCallStateUpdates.Label).addNone((Enum)com.navdy.hud.mfi.IAPCommunicationsManager$StartCallStateUpdates.Service).addNone((Enum)com.navdy.hud.mfi.IAPCommunicationsManager$StartCallStateUpdates.IsConferenced).addNone((Enum)com.navdy.hud.mfi.IAPCommunicationsManager$StartCallStateUpdates.ConferenceGroup).addNone((Enum)com.navdy.hud.mfi.IAPCommunicationsManager$StartCallStateUpdates.DisconnectReason);
        this.miAPProcessor.sendControlMessage((com.navdy.hud.mfi.iAPProcessor$IAP2ParamsCreator)a);
    }
    
    public void stopCommunicationUpdates() {
    }
    
    public void stopUpdates() {
        com.navdy.hud.mfi.iAPProcessor$IAP2SessionMessage a = new com.navdy.hud.mfi.iAPProcessor$IAP2SessionMessage(com.navdy.hud.mfi.iAPProcessor$iAPMessage.StopCallStateUpdates);
        this.miAPProcessor.sendControlMessage((com.navdy.hud.mfi.iAPProcessor$IAP2ParamsCreator)a);
    }
    
    public void unMute() {
        com.navdy.hud.mfi.iAPProcessor$IAP2SessionMessage a = new com.navdy.hud.mfi.iAPProcessor$IAP2SessionMessage(com.navdy.hud.mfi.iAPProcessor$iAPMessage.MuteStatusUpdate);
        a.addBoolean((Enum)com.navdy.hud.mfi.IAPCommunicationsManager$MuteStatusUpdate.MuteStatus, false);
        this.miAPProcessor.sendControlMessage((com.navdy.hud.mfi.iAPProcessor$IAP2ParamsCreator)a);
    }
}
