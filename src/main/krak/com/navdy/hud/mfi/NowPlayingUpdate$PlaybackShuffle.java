package com.navdy.hud.mfi;


public enum NowPlayingUpdate$PlaybackShuffle {
    Off(0),
    Songs(1),
    Albums(2);

    private int value;
    NowPlayingUpdate$PlaybackShuffle(int value) {
        this.value = value;
    }
    public int getValue() {
        return value;
    }
}

//final public class NowPlayingUpdate$PlaybackShuffle extends Enum {
//    final private static com.navdy.hud.mfi.NowPlayingUpdate$PlaybackShuffle[] $VALUES;
//    final public static com.navdy.hud.mfi.NowPlayingUpdate$PlaybackShuffle Albums;
//    final public static com.navdy.hud.mfi.NowPlayingUpdate$PlaybackShuffle Off;
//    final public static com.navdy.hud.mfi.NowPlayingUpdate$PlaybackShuffle Songs;
//    
//    static {
//        Off = new com.navdy.hud.mfi.NowPlayingUpdate$PlaybackShuffle("Off", 0);
//        Songs = new com.navdy.hud.mfi.NowPlayingUpdate$PlaybackShuffle("Songs", 1);
//        Albums = new com.navdy.hud.mfi.NowPlayingUpdate$PlaybackShuffle("Albums", 2);
//        com.navdy.hud.mfi.NowPlayingUpdate$PlaybackShuffle[] a = new com.navdy.hud.mfi.NowPlayingUpdate$PlaybackShuffle[3];
//        a[0] = Off;
//        a[1] = Songs;
//        a[2] = Albums;
//        $VALUES = a;
//    }
//    
//    private NowPlayingUpdate$PlaybackShuffle(String s, int i) {
//        super(s, i);
//    }
//    
//    public static com.navdy.hud.mfi.NowPlayingUpdate$PlaybackShuffle valueOf(String s) {
//        return (com.navdy.hud.mfi.NowPlayingUpdate$PlaybackShuffle)Enum.valueOf(com.navdy.hud.mfi.NowPlayingUpdate$PlaybackShuffle.class, s);
//    }
//    
//    public static com.navdy.hud.mfi.NowPlayingUpdate$PlaybackShuffle[] values() {
//        return $VALUES.clone();
//    }
//}
//