package com.navdy.hud.mfi;

abstract public interface IIAPFileTransferManager {
    final public static int FILE_TRANSFER_SESSION_ID = 2;
    
    abstract public void cancel();
    
    
    abstract public void clear();
    
    
    abstract public long getFileTransferLimit();
    
    
    abstract public void onCanceled(int arg);
    
    
    abstract public void onError(int arg, Throwable arg0);
    
    
    abstract public void onFileTransferSetupRequest(int arg, long arg0);
    
    
    abstract public void onFileTransferSetupResponse(int arg, boolean arg0);
    
    
    abstract public void onPaused(int arg);
    
    
    abstract public void onSuccess(int arg, byte[] arg0);
    
    
    abstract public void queue(byte[] arg);
    
    
    abstract public void sendMessage(int arg, byte[] arg0);
    
    
    abstract public void setFileTransferLimit(int arg);
}
