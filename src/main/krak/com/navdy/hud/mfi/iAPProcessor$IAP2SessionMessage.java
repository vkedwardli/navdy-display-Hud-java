package com.navdy.hud.mfi;

class iAPProcessor$IAP2SessionMessage extends com.navdy.hud.mfi.iAPProcessor$IAP2ParamsCreator {
    int msgId;
    
    iAPProcessor$IAP2SessionMessage(int i) {
        this.msgId = i;
        try {
            this.dos.write(com.navdy.hud.mfi.iAPProcessor.access$500());
            this.dos.writeShort(0);
            this.dos.writeShort(i);
        } catch(java.io.IOException a) {
            android.util.Log.e(com.navdy.hud.mfi.iAPProcessor.access$400(), "Exception while creating message ", (Throwable)a);
        }
    }
    
    iAPProcessor$IAP2SessionMessage(com.navdy.hud.mfi.iAPProcessor$iAPMessage a) {
        this(a.id);
    }
    
    byte[] toBytes() {
        byte[] a = ((com.navdy.hud.mfi.iAPProcessor$IAP2ParamsCreator)this).toBytes();
        com.navdy.hud.mfi.iAPProcessor.copy(a, 2, com.navdy.hud.mfi.Utils.packUInt16(a.length));
        return a;
    }
}
