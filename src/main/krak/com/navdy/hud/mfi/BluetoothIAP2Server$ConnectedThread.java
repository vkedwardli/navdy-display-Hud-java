package com.navdy.hud.mfi;

class BluetoothIAP2Server$ConnectedThread extends Thread {
    final private java.io.InputStream mmInStream;
    final private java.io.OutputStream mmOutStream;
    final private android.bluetooth.BluetoothSocket mmSocket;
    final com.navdy.hud.mfi.BluetoothIAP2Server this$0;
    
    public BluetoothIAP2Server$ConnectedThread(com.navdy.hud.mfi.BluetoothIAP2Server a, android.bluetooth.BluetoothSocket a0) {
        super();
        java.io.InputStream a1 = null;
        java.io.OutputStream a2 = null;
        this.this$0 = a;
        android.util.Log.d("BluetoothIAP2Server", "create ConnectedThread");
        this.mmSocket = a0;
        try {
            a1 = null;
            a1 = a0.getInputStream();
            a2 = a0.getOutputStream();
        } catch(java.io.IOException a3) {
            android.util.Log.e("BluetoothIAP2Server", "temp sockets not created", (Throwable)a3);
            a2 = null;
        }
        this.mmInStream = a1;
        this.mmOutStream = a2;
    }
    
    public void cancel() {
        try {
            this.mmSocket.close();
        } catch(java.io.IOException a) {
            android.util.Log.e("BluetoothIAP2Server", "close() of connect socket failed", (Throwable)a);
        }
    }
    
    public void run() {
        android.bluetooth.BluetoothDevice a = this.mmSocket.getRemoteDevice();
        android.util.Log.i("BluetoothIAP2Server", new StringBuilder().append("BEGIN mConnectedThread - ").append(a.getAddress()).append(" name:").append(a.getName()).toString());
        com.navdy.hud.mfi.BluetoothIAP2Server.access$400(this.this$0).connectionStarted(a.getAddress(), a.getName());
        byte[] a0 = new byte[1024];
        try {
            while(true) {
                byte[] a1 = java.util.Arrays.copyOf(a0, this.mmInStream.read(a0));
                com.navdy.hud.mfi.BluetoothIAP2Server.access$400(this.this$0).queue(new com.navdy.hud.mfi.LinkPacket(a1));
            }
        } catch(java.io.IOException a2) {
            android.util.Log.e("BluetoothIAP2Server", "disconnected", (Throwable)a2);
            com.navdy.hud.mfi.BluetoothIAP2Server.access$500(this.this$0);
            return;
        }
    }
    
    public void write(byte[] a) {
        try {
            this.mmOutStream.write(a);
        } catch(java.io.IOException a0) {
            android.util.Log.e("BluetoothIAP2Server", "Exception during write", (Throwable)a0);
        }
    }
}
