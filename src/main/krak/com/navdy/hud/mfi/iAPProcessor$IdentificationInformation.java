package com.navdy.hud.mfi;


    public enum iAPProcessor$IdentificationInformation {
        Name(0),
    ModelIdentifier(1),
    Manufacturer(2),
    SerialNumber(3),
    FirmwareVersion(4),
    HardwareVersion(5),
    MessagesSentByAccessory(6),
    MessagesReceivedFromDevice(7),
    PowerProvidingCapability(8),
    MaximumCurrentDrawnFromDevice(9),
    SupportedExternalAccessoryProtocol(10),
    AppMatchTeamID(11),
    CurrentLanguage(12),
    SupportedLanguage(13),
    SerialTransportComponent(14),
    USBDeviceTransportComponent(15),
    USBHostTransportComponent(16),
    BluetoothTransportComponent(17),
    iAP2HIDComponent(18),
    VehicleInformationComponent(19),
    VehicleStatusComponent(20),
    LocationInformationComponent(21),
    USBHostHIDComponent(22);

        private int value;
        iAPProcessor$IdentificationInformation(int value) {
            this.value = value;
        }
        public int getValue() {
            return value;
        }
    }
    
//final class iAPProcessor$IdentificationInformation extends Enum {
//    final private static com.navdy.hud.mfi.iAPProcessor$IdentificationInformation[] $VALUES;
//    final public static com.navdy.hud.mfi.iAPProcessor$IdentificationInformation AppMatchTeamID;
//    final public static com.navdy.hud.mfi.iAPProcessor$IdentificationInformation BluetoothTransportComponent;
//    final public static com.navdy.hud.mfi.iAPProcessor$IdentificationInformation CurrentLanguage;
//    final public static com.navdy.hud.mfi.iAPProcessor$IdentificationInformation FirmwareVersion;
//    final public static com.navdy.hud.mfi.iAPProcessor$IdentificationInformation HardwareVersion;
//    final public static com.navdy.hud.mfi.iAPProcessor$IdentificationInformation LocationInformationComponent;
//    final public static com.navdy.hud.mfi.iAPProcessor$IdentificationInformation Manufacturer;
//    final public static com.navdy.hud.mfi.iAPProcessor$IdentificationInformation MaximumCurrentDrawnFromDevice;
//    final public static com.navdy.hud.mfi.iAPProcessor$IdentificationInformation MessagesReceivedFromDevice;
//    final public static com.navdy.hud.mfi.iAPProcessor$IdentificationInformation MessagesSentByAccessory;
//    final public static com.navdy.hud.mfi.iAPProcessor$IdentificationInformation ModelIdentifier;
//    final public static com.navdy.hud.mfi.iAPProcessor$IdentificationInformation Name;
//    final public static com.navdy.hud.mfi.iAPProcessor$IdentificationInformation PowerProvidingCapability;
//    final public static com.navdy.hud.mfi.iAPProcessor$IdentificationInformation SerialNumber;
//    final public static com.navdy.hud.mfi.iAPProcessor$IdentificationInformation SerialTransportComponent;
//    final public static com.navdy.hud.mfi.iAPProcessor$IdentificationInformation SupportedExternalAccessoryProtocol;
//    final public static com.navdy.hud.mfi.iAPProcessor$IdentificationInformation SupportedLanguage;
//    final public static com.navdy.hud.mfi.iAPProcessor$IdentificationInformation USBDeviceTransportComponent;
//    final public static com.navdy.hud.mfi.iAPProcessor$IdentificationInformation USBHostHIDComponent;
//    final public static com.navdy.hud.mfi.iAPProcessor$IdentificationInformation USBHostTransportComponent;
//    final public static com.navdy.hud.mfi.iAPProcessor$IdentificationInformation VehicleInformationComponent;
//    final public static com.navdy.hud.mfi.iAPProcessor$IdentificationInformation VehicleStatusComponent;
//    final public static com.navdy.hud.mfi.iAPProcessor$IdentificationInformation iAP2HIDComponent;
//    
//    static {
//        Name = new com.navdy.hud.mfi.iAPProcessor$IdentificationInformation("Name", 0);
//        ModelIdentifier = new com.navdy.hud.mfi.iAPProcessor$IdentificationInformation("ModelIdentifier", 1);
//        Manufacturer = new com.navdy.hud.mfi.iAPProcessor$IdentificationInformation("Manufacturer", 2);
//        SerialNumber = new com.navdy.hud.mfi.iAPProcessor$IdentificationInformation("SerialNumber", 3);
//        FirmwareVersion = new com.navdy.hud.mfi.iAPProcessor$IdentificationInformation("FirmwareVersion", 4);
//        HardwareVersion = new com.navdy.hud.mfi.iAPProcessor$IdentificationInformation("HardwareVersion", 5);
//        MessagesSentByAccessory = new com.navdy.hud.mfi.iAPProcessor$IdentificationInformation("MessagesSentByAccessory", 6);
//        MessagesReceivedFromDevice = new com.navdy.hud.mfi.iAPProcessor$IdentificationInformation("MessagesReceivedFromDevice", 7);
//        PowerProvidingCapability = new com.navdy.hud.mfi.iAPProcessor$IdentificationInformation("PowerProvidingCapability", 8);
//        MaximumCurrentDrawnFromDevice = new com.navdy.hud.mfi.iAPProcessor$IdentificationInformation("MaximumCurrentDrawnFromDevice", 9);
//        SupportedExternalAccessoryProtocol = new com.navdy.hud.mfi.iAPProcessor$IdentificationInformation("SupportedExternalAccessoryProtocol", 10);
//        AppMatchTeamID = new com.navdy.hud.mfi.iAPProcessor$IdentificationInformation("AppMatchTeamID", 11);
//        CurrentLanguage = new com.navdy.hud.mfi.iAPProcessor$IdentificationInformation("CurrentLanguage", 12);
//        SupportedLanguage = new com.navdy.hud.mfi.iAPProcessor$IdentificationInformation("SupportedLanguage", 13);
//        SerialTransportComponent = new com.navdy.hud.mfi.iAPProcessor$IdentificationInformation("SerialTransportComponent", 14);
//        USBDeviceTransportComponent = new com.navdy.hud.mfi.iAPProcessor$IdentificationInformation("USBDeviceTransportComponent", 15);
//        USBHostTransportComponent = new com.navdy.hud.mfi.iAPProcessor$IdentificationInformation("USBHostTransportComponent", 16);
//        BluetoothTransportComponent = new com.navdy.hud.mfi.iAPProcessor$IdentificationInformation("BluetoothTransportComponent", 17);
//        iAP2HIDComponent = new com.navdy.hud.mfi.iAPProcessor$IdentificationInformation("iAP2HIDComponent", 18);
//        VehicleInformationComponent = new com.navdy.hud.mfi.iAPProcessor$IdentificationInformation("VehicleInformationComponent", 19);
//        VehicleStatusComponent = new com.navdy.hud.mfi.iAPProcessor$IdentificationInformation("VehicleStatusComponent", 20);
//        LocationInformationComponent = new com.navdy.hud.mfi.iAPProcessor$IdentificationInformation("LocationInformationComponent", 21);
//        USBHostHIDComponent = new com.navdy.hud.mfi.iAPProcessor$IdentificationInformation("USBHostHIDComponent", 22);
//        com.navdy.hud.mfi.iAPProcessor$IdentificationInformation[] a = new com.navdy.hud.mfi.iAPProcessor$IdentificationInformation[23];
//        a[0] = Name;
//        a[1] = ModelIdentifier;
//        a[2] = Manufacturer;
//        a[3] = SerialNumber;
//        a[4] = FirmwareVersion;
//        a[5] = HardwareVersion;
//        a[6] = MessagesSentByAccessory;
//        a[7] = MessagesReceivedFromDevice;
//        a[8] = PowerProvidingCapability;
//        a[9] = MaximumCurrentDrawnFromDevice;
//        a[10] = SupportedExternalAccessoryProtocol;
//        a[11] = AppMatchTeamID;
//        a[12] = CurrentLanguage;
//        a[13] = SupportedLanguage;
//        a[14] = SerialTransportComponent;
//        a[15] = USBDeviceTransportComponent;
//        a[16] = USBHostTransportComponent;
//        a[17] = BluetoothTransportComponent;
//        a[18] = iAP2HIDComponent;
//        a[19] = VehicleInformationComponent;
//        a[20] = VehicleStatusComponent;
//        a[21] = LocationInformationComponent;
//        a[22] = USBHostHIDComponent;
//        $VALUES = a;
//    }
//    
//    private iAPProcessor$IdentificationInformation(String s, int i) {
//        super(s, i);
//    }
//    
//    public static com.navdy.hud.mfi.iAPProcessor$IdentificationInformation valueOf(String s) {
//        return (com.navdy.hud.mfi.iAPProcessor$IdentificationInformation)Enum.valueOf(com.navdy.hud.mfi.iAPProcessor$IdentificationInformation.class, s);
//    }
//    
//    public static com.navdy.hud.mfi.iAPProcessor$IdentificationInformation[] values() {
//        return $VALUES.clone();
//    }
//}
//