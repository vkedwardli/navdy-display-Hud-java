package com.navdy.hud.mfi;


    public enum IAPCommunicationsManager$EndCall {
        EndAction(0),
    CallUUID(1);

        private int value;
        IAPCommunicationsManager$EndCall(int value) {
            this.value = value;
        }
        public int getValue() {
            return value;
        }
    }
    
//final class IAPCommunicationsManager$EndCall extends Enum {
//    final private static com.navdy.hud.mfi.IAPCommunicationsManager$EndCall[] $VALUES;
//    final public static com.navdy.hud.mfi.IAPCommunicationsManager$EndCall CallUUID;
//    final public static com.navdy.hud.mfi.IAPCommunicationsManager$EndCall EndAction;
//    
//    static {
//        EndAction = new com.navdy.hud.mfi.IAPCommunicationsManager$EndCall("EndAction", 0);
//        CallUUID = new com.navdy.hud.mfi.IAPCommunicationsManager$EndCall("CallUUID", 1);
//        com.navdy.hud.mfi.IAPCommunicationsManager$EndCall[] a = new com.navdy.hud.mfi.IAPCommunicationsManager$EndCall[2];
//        a[0] = EndAction;
//        a[1] = CallUUID;
//        $VALUES = a;
//    }
//    
//    private IAPCommunicationsManager$EndCall(String s, int i) {
//        super(s, i);
//    }
//    
//    public static com.navdy.hud.mfi.IAPCommunicationsManager$EndCall valueOf(String s) {
//        return (com.navdy.hud.mfi.IAPCommunicationsManager$EndCall)Enum.valueOf(com.navdy.hud.mfi.IAPCommunicationsManager$EndCall.class, s);
//    }
//    
//    public static com.navdy.hud.mfi.IAPCommunicationsManager$EndCall[] values() {
//        return $VALUES.clone();
//    }
//}
//