package com.navdy.hud.mfi;

public class LinkLayer implements com.navdy.hud.mfi.SessionPacketReceiver, com.navdy.hud.mfi.LinkPacketReceiver {
    final public static int CONTROL_BYTE_ACK = 64;
    final public static int CONTROL_BYTE_EAK = 32;
    final public static int CONTROL_BYTE_RST = 16;
    final public static int CONTROL_BYTE_SLP = 8;
    final public static int CONTROL_BYTE_SYN = 128;
    final public static int DELAY_MILLIS = 20;
    final public static int MESSAGE_CONNECTION_ENDED = 4;
    final public static int MESSAGE_CONNECTION_STARTED = 3;
    final public static int MESSAGE_LINK_PACKET = 1;
    final public static int MESSAGE_PERIODIC = 0;
    final public static int MESSAGE_SESSION_PACKET = 2;
    final public static String TAG = "LinkLayer";
    final private static boolean sLogLinkPacket = true;
    private java.util.concurrent.ConcurrentLinkedQueue controlMessages;
    private com.navdy.hud.mfi.iAPProcessor iAPProcessor;
    private android.os.Handler myHandler;
    private java.util.concurrent.ConcurrentLinkedQueue nonControlMessages;
    private com.navdy.hud.mfi.LinkLayer$PhysicalLayer physicalLayer;
    private String remoteAddress;
    private String remoteName;
    
    static {
        System.loadLibrary("iap2");
    }
    
    public LinkLayer() {
        this.start();
    }
    
    static java.util.concurrent.ConcurrentLinkedQueue access$000(com.navdy.hud.mfi.LinkLayer a) {
        return a.controlMessages;
    }
    
    static java.util.concurrent.ConcurrentLinkedQueue access$100(com.navdy.hud.mfi.LinkLayer a) {
        return a.nonControlMessages;
    }
    
    static com.navdy.hud.mfi.iAPProcessor access$200(com.navdy.hud.mfi.LinkLayer a) {
        return a.iAPProcessor;
    }
    
    static int access$300(com.navdy.hud.mfi.LinkLayer a, int i, byte[] a0, int i0) {
        return a.native_runloop(i, a0, i0);
    }
    
    static int access$400(com.navdy.hud.mfi.LinkLayer a) {
        return a.native_get_message_session();
    }
    
    static byte[] access$500(com.navdy.hud.mfi.LinkLayer a, int i) {
        return a.native_pop_message_data(i);
    }
    
    static void access$600(com.navdy.hud.mfi.LinkLayer a, byte[] a0) {
        a.logOutGoingLinkPacket(a0);
    }
    
    static com.navdy.hud.mfi.LinkLayer$PhysicalLayer access$700(com.navdy.hud.mfi.LinkLayer a) {
        return a.physicalLayer;
    }
    
    static void access$800(com.navdy.hud.mfi.LinkLayer a, String s, com.navdy.hud.mfi.Packet a0) {
        a.log(s, a0);
    }
    
    private void log(String s, com.navdy.hud.mfi.Packet a) {
        Object[] a0 = new Object[2];
        a0[0] = s;
        a0[1] = a.data;
        com.navdy.hud.mfi.Utils.logTransfer("LinkLayer", "%s: %s", a0);
    }
    
    private void logOutGoingLinkPacket(byte[] a) {
        if (a != null && a.length > 7) {
            int i = a[5];
            int i0 = a[6];
            int i1 = a[4];
            int i2 = i1 & 255;
            StringBuilder a0 = new StringBuilder();
            if ((i2 & 64) != 0) {
                a0.append(" ACK ");
            }
            if ((i2 & 128) != 0) {
                a0.append(" SYN ");
            }
            if ((i2 & 32) != 0) {
                a0.append(" EAK ");
            }
            if ((i2 & 16) != 0) {
                a0.append(" RST ");
            }
            if ((i2 & 8) != 0) {
                a0.append(" SLP ");
            }
            android.util.Log.d("MFi", new StringBuilder().append("Accessory(L)   :").append(i & 255).append(" ").append(i0 & 255).append(" Size:").append(a.length).append(a0.toString()).toString());
        }
    }
    
    native private int native_get_message_session();
    
    
    native private byte[] native_pop_message_data(int arg);
    
    
    native private int native_runloop(int arg, byte[] arg0, int arg1);
    
    
    private void queuePacket(int i, Object a) {
        android.os.Message a0 = this.myHandler.obtainMessage(i, a);
        this.myHandler.sendMessage(a0);
    }
    
    public void connect(com.navdy.hud.mfi.LinkLayer$PhysicalLayer a) {
        this.physicalLayer = a;
    }
    
    public void connect(com.navdy.hud.mfi.iAPProcessor a) {
        this.iAPProcessor = a;
    }
    
    public void connectionEnded() {
        if (this.myHandler != null) {
            this.myHandler.removeMessages(0);
            this.myHandler.removeMessages(1);
            this.myHandler.removeMessages(2);
        }
        this.controlMessages.clear();
        this.nonControlMessages.clear();
        this.queuePacket(4, null);
    }
    
    public void connectionStarted(String s, String s0) {
        this.remoteAddress = s;
        this.remoteName = s0;
        this.queuePacket(3, null);
    }
    
    public byte[] getLocalAddress() {
        return this.physicalLayer.getLocalAddress();
    }
    
    public String getRemoteAddress() {
        return this.remoteAddress;
    }
    
    public String getRemoteName() {
        return this.remoteName;
    }
    
    native public int native_get_maximum_payload_size();
    
    
    public void queue(com.navdy.hud.mfi.LinkPacket a) {
        this.queuePacket(1, a);
    }
    
    public void queue(com.navdy.hud.mfi.SessionPacket a) {
        this.log(new StringBuilder().append("iAP->LL[").append(a.session).append("]").toString(), (com.navdy.hud.mfi.Packet)a);
        if (a.session != this.iAPProcessor.getControlSessionId()) {
            this.nonControlMessages.add(a);
        } else {
            this.controlMessages.add(a);
        }
        this.myHandler.sendEmptyMessage(2);
    }
    
    public void start() {
        this.controlMessages = new java.util.concurrent.ConcurrentLinkedQueue();
        this.nonControlMessages = new java.util.concurrent.ConcurrentLinkedQueue();
        this.myHandler = new com.navdy.hud.mfi.LinkLayer$1(this, android.os.Looper.getMainLooper());
        this.myHandler.sendEmptyMessageDelayed(0, 20L);
    }
    
    public void stop() {
    }
}
