package com.navdy.hud.mfi;

abstract public interface SessionPacketReceiver {
    abstract public void queue(com.navdy.hud.mfi.SessionPacket arg);
}
