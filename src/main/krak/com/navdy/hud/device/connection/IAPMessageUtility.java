package com.navdy.hud.device.connection;

public class IAPMessageUtility {
    final private static java.util.Map PLAYBACK_STATES;
    final private static java.util.Map REPEAT_MODES;
    final private static java.util.Map SHUFFLE_MODES;
    
    static {
        PLAYBACK_STATES = (java.util.Map)new com.navdy.hud.device.connection.IAPMessageUtility$1();
        SHUFFLE_MODES = (java.util.Map)new com.navdy.hud.device.connection.IAPMessageUtility$2();
        REPEAT_MODES = (java.util.Map)new com.navdy.hud.device.connection.IAPMessageUtility$3();
    }
    
    public IAPMessageUtility() {
    }
    
    public static com.navdy.service.library.events.audio.MusicTrackInfo getMusicTrackInfoForNowPlayingUpdate(com.navdy.hud.mfi.NowPlayingUpdate a) {
        com.navdy.service.library.events.audio.MusicTrackInfo a0 = null;
        if (a != null) {
            com.navdy.service.library.events.audio.MusicTrackInfo$Builder a1 = new com.navdy.service.library.events.audio.MusicTrackInfo$Builder().playbackState((com.navdy.service.library.events.audio.MusicPlaybackState)PLAYBACK_STATES.get(a.mPlaybackStatus)).name(a.mediaItemTitle).album(a.mediaItemAlbumTitle).author(a.mediaItemArtist).duration(Integer.valueOf((int)a.mediaItemPlaybackDurationInMilliseconds)).currentPosition(Integer.valueOf((int)a.mPlaybackElapsedTimeMilliseconds)).shuffleMode((com.navdy.service.library.events.audio.MusicShuffleMode)SHUFFLE_MODES.get(a.playbackShuffle)).repeatMode((com.navdy.service.library.events.audio.MusicRepeatMode)REPEAT_MODES.get(a.playbackRepeat));
            if (a.mediaItemPersistentIdentifier != null) {
                a1.trackId(a.mediaItemPersistentIdentifier.toString());
            }
            a1.isPreviousAllowed(Boolean.valueOf(true)).isNextAllowed(Boolean.valueOf(true));
            a0 = a1.build();
        } else {
            a0 = null;
        }
        return a0;
    }
    
    public static com.navdy.service.library.events.callcontrol.PhoneEvent getPhoneEventForCallStateUpdate(com.navdy.hud.mfi.CallStateUpdate a, com.navdy.service.library.events.callcontrol.CallAction a0) {
        com.navdy.service.library.events.callcontrol.PhoneEvent a1 = null;
        label0: if (a != null) {
            com.navdy.service.library.events.callcontrol.PhoneEvent$Builder a2 = new com.navdy.service.library.events.callcontrol.PhoneEvent$Builder();
            int i = com.navdy.hud.device.connection.IAPMessageUtility$4.$SwitchMap$com$navdy$hud$mfi$CallStateUpdate$Status[a.status.ordinal()];
            a1 = null;
            switch(i) {
                case 6: {
                    a2.status(com.navdy.service.library.events.callcontrol.PhoneStatus.PHONE_HELD);
                    break;
                }
                case 5: {
                    a2.status(com.navdy.service.library.events.callcontrol.PhoneStatus.PHONE_OFFHOOK);
                    break;
                }
                case 4: {
                    a2.status(com.navdy.service.library.events.callcontrol.PhoneStatus.PHONE_OFFHOOK);
                    break;
                }
                case 3: {
                    a2.status(com.navdy.service.library.events.callcontrol.PhoneStatus.PHONE_RINGING);
                    break;
                }
                case 2: {
                    a2.status(com.navdy.service.library.events.callcontrol.PhoneStatus.PHONE_DIALING);
                    break;
                }
                case 1: {
                    label1: {
                        label2: {
                            if (a0 == null) {
                                break label2;
                            }
                            if (a0 == com.navdy.service.library.events.callcontrol.CallAction.CALL_DIAL) {
                                break label1;
                            }
                        }
                        a2.status(com.navdy.service.library.events.callcontrol.PhoneStatus.PHONE_IDLE);
                        break;
                    }
                    a2.status(com.navdy.service.library.events.callcontrol.PhoneStatus.PHONE_DISCONNECTING);
                    break;
                }
                default: {
                    break;
                }
                case 7: {
                    break label0;
                }
            }
            if (!android.text.TextUtils.isEmpty((CharSequence)a.displayName)) {
                a2.contact_name(a.displayName);
            }
            if (!android.text.TextUtils.isEmpty((CharSequence)a.remoteID)) {
                a2.number(a.remoteID);
            }
            if (!android.text.TextUtils.isEmpty((CharSequence)a.label)) {
                a2.label(a.label);
            }
            if (!android.text.TextUtils.isEmpty((CharSequence)a.callUUID)) {
                a2.callUUID(a.callUUID);
            }
            a1 = a2.build();
        }
        return a1;
    }
    
    public static boolean performActionForTelephonyRequest(com.navdy.service.library.events.callcontrol.TelephonyRequest a, com.navdy.hud.mfi.IAPCommunicationsManager a0) {
        boolean b = false;
        label0: if (a == null) {
            b = false;
        } else {
            switch(com.navdy.hud.device.connection.IAPMessageUtility$4.$SwitchMap$com$navdy$service$library$events$callcontrol$CallAction[a.action.ordinal()]) {
                case 6: {
                    a0.unMute();
                    break;
                }
                case 5: {
                    a0.mute();
                    break;
                }
                case 4: {
                    if (android.text.TextUtils.isEmpty((CharSequence)a.number)) {
                        break;
                    }
                    a0.initiateDestinationCall(a.number);
                    break;
                }
                case 2: case 3: {
                    if (android.text.TextUtils.isEmpty((CharSequence)a.callUUID)) {
                        a0.endCall();
                        break;
                    } else {
                        a0.endCall(a.callUUID);
                        break;
                    }
                }
                case 1: {
                    if (android.text.TextUtils.isEmpty((CharSequence)a.callUUID)) {
                        a0.acceptCall();
                        break;
                    } else {
                        a0.acceptCall(a.callUUID);
                        break;
                    }
                }
                default: {
                    b = false;
                    break label0;
                }
            }
            b = true;
        }
        return b;
    }
}
