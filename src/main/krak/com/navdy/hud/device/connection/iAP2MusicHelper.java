package com.navdy.hud.device.connection;

class iAP2MusicHelper implements com.navdy.hud.mfi.IAPNowPlayingUpdateListener, com.navdy.hud.mfi.IAPFileTransferListener {
    final private static int LIMITED_BANDWIDTH_FILE_TRANSFER_LIMIT = 10000;
    final private static com.navdy.service.library.log.Logger sLogger;
    private boolean fileTransferCanceled;
    final private Object helperStateLock;
    private com.navdy.hud.mfi.IAPMusicManager iAPMusicManager;
    private com.navdy.hud.mfi.IAPFileTransferManager iapFileTransferManager;
    private long keyDownTime;
    private com.navdy.service.library.events.audio.MusicArtworkResponse lastMusicArtworkResponse;
    private com.navdy.hud.device.connection.iAP2Link link;
    private int nowPlayingFileTransferIdentifier;
    private android.util.LruCache transferIdTrackInfoMap;
    private com.navdy.service.library.events.audio.MusicArtworkRequest waitingArtworkRequest;
    private com.navdy.hud.device.connection.iAP2MusicHelper$FileHolder waitingFile;
    private int waitingFileTransferIdentifier;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.device.connection.iAP2MusicHelper.class);
    }
    
    iAP2MusicHelper(com.navdy.hud.device.connection.iAP2Link a, com.navdy.hud.mfi.iAPProcessor a0) {
        this.nowPlayingFileTransferIdentifier = -1;
        this.waitingFileTransferIdentifier = -1;
        this.fileTransferCanceled = false;
        this.waitingArtworkRequest = null;
        this.keyDownTime = 0L;
        this.transferIdTrackInfoMap = new android.util.LruCache(5);
        this.helperStateLock = new Object();
        this.link = a;
        this.iapFileTransferManager = new com.navdy.hud.mfi.IAPFileTransferManager(a0);
        a0.connect((com.navdy.hud.mfi.IIAPFileTransferManager)this.iapFileTransferManager);
        this.iAPMusicManager = new com.navdy.hud.mfi.IAPMusicManager(a0);
        this.iAPMusicManager.setNowPlayingUpdateListener((com.navdy.hud.mfi.IAPNowPlayingUpdateListener)this);
        this.addEventProcessors();
    }
    
    static com.navdy.service.library.log.Logger access$000() {
        return sLogger;
    }
    
    static com.navdy.service.library.events.audio.MusicArtworkResponse access$100(com.navdy.hud.device.connection.iAP2MusicHelper a) {
        return a.lastMusicArtworkResponse;
    }
    
    static com.navdy.service.library.events.audio.MusicArtworkResponse access$102(com.navdy.hud.device.connection.iAP2MusicHelper a, com.navdy.service.library.events.audio.MusicArtworkResponse a0) {
        a.lastMusicArtworkResponse = a0;
        return a0;
    }
    
    static com.navdy.hud.device.connection.iAP2Link access$200(com.navdy.hud.device.connection.iAP2MusicHelper a) {
        return a.link;
    }
    
    static void access$300(com.navdy.hud.device.connection.iAP2MusicHelper a, com.navdy.service.library.events.input.MediaRemoteKeyEvent a0) {
        a.onMediaRemoteKeyEvent(a0);
    }
    
    static void access$400(com.navdy.hud.device.connection.iAP2MusicHelper a, com.navdy.service.library.events.photo.PhotoUpdatesRequest a0) {
        a.onPhotoUpdatesRequest(a0);
    }
    
    static boolean access$500(com.navdy.hud.device.connection.iAP2MusicHelper a, com.navdy.service.library.events.audio.MusicArtworkRequest a0) {
        return a.onMusicArtworkRequest(a0);
    }
    
    private void addEventProcessors() {
        this.link.addEventProcessor(com.navdy.service.library.events.NavdyEvent$MessageType.MediaRemoteKeyEvent, (com.navdy.hud.device.connection.iAP2Link$NavdyEventProcessor)new com.navdy.hud.device.connection.iAP2MusicHelper$2(this));
        this.link.addEventProcessor(com.navdy.service.library.events.NavdyEvent$MessageType.NowPlayingUpdateRequest, (com.navdy.hud.device.connection.iAP2Link$NavdyEventProcessor)new com.navdy.hud.device.connection.iAP2MusicHelper$3(this));
        this.link.addEventProcessor(com.navdy.service.library.events.NavdyEvent$MessageType.PhotoUpdatesRequest, (com.navdy.hud.device.connection.iAP2Link$NavdyEventProcessor)new com.navdy.hud.device.connection.iAP2MusicHelper$4(this));
        this.link.addEventProcessor(com.navdy.service.library.events.NavdyEvent$MessageType.MusicArtworkRequest, (com.navdy.hud.device.connection.iAP2Link$NavdyEventProcessor)new com.navdy.hud.device.connection.iAP2MusicHelper$5(this));
    }
    
    private com.navdy.service.library.events.audio.MusicTrackInfo getLastMusicTrackInfo() {
        com.navdy.service.library.events.audio.MusicTrackInfo a = null;
        synchronized(this.helperStateLock) {
            a = (com.navdy.service.library.events.audio.MusicTrackInfo)this.transferIdTrackInfoMap.get(Integer.valueOf(this.nowPlayingFileTransferIdentifier));
            /*monexit(a0)*/;
        }
        return a;
    }
    
    private void onMediaRemoteKeyEvent(com.navdy.service.library.events.input.MediaRemoteKeyEvent a) {
        sLogger.d(new StringBuilder().append("(MFi) Media Key : ").append(a.key).append(" , ").append(a.action).toString());
        switch(com.navdy.hud.device.connection.iAP2MusicHelper$6.$SwitchMap$com$navdy$service$library$events$input$KeyEvent[a.action.ordinal()]) {
            case 2: {
                if (android.os.SystemClock.elapsedRealtime() - this.keyDownTime <= 500L) {
                    this.iapFileTransferManager.cancel();
                }
                this.keyDownTime = 0L;
                this.iAPMusicManager.onKeyUp(a.key.ordinal());
                break;
            }
            case 1: {
                this.keyDownTime = android.os.SystemClock.elapsedRealtime();
                this.iAPMusicManager.onKeyDown(a.key.ordinal());
                break;
            }
        }
    }
    
    private boolean onMusicArtworkRequest(com.navdy.service.library.events.audio.MusicArtworkRequest a) {
        boolean b = false;
        label5: synchronized(this) {
            com.navdy.service.library.events.audio.MusicTrackInfo a0 = this.getLastMusicTrackInfo();
            sLogger.d(new StringBuilder().append("onMusicArtworkRequest: ").append(a).append(", musicTrackInfo: ").append(a0).toString());
            label3: {
                label4: {
                    if (a == null) {
                        break label4;
                    }
                    if (!android.text.TextUtils.isEmpty((CharSequence)a.name)) {
                        break label3;
                    }
                    if (!android.text.TextUtils.isEmpty((CharSequence)a.album)) {
                        break label3;
                    }
                    if (!android.text.TextUtils.isEmpty((CharSequence)a.author)) {
                        break label3;
                    }
                }
                b = false;
                break label5;
            }
            synchronized(this.helperStateLock) {
                label2: {
                    label0: {
                        label1: {
                            if (a0 == null) {
                                break label1;
                            }
                            if (!this.sameTrack(a, a0)) {
                                break label1;
                            }
                            if (this.nowPlayingFileTransferIdentifier == this.waitingFileTransferIdentifier) {
                                break label0;
                            }
                        }
                        this.waitingArtworkRequest = a;
                        sLogger.d(new StringBuilder().append("onMusicArtworkRequest waiting for file transfer, waitingArtworkRequest: ").append(this.waitingArtworkRequest).toString());
                        break label2;
                    }
                    com.navdy.service.library.log.Logger a2 = sLogger;
                    StringBuilder a3 = new StringBuilder().append("onMusicArtworkRequest, waiting file: ");
                    Object a4 = (this.waitingFile == null) ? "null" : Integer.valueOf(this.waitingFile.size());
                    a2.d(a3.append(a4).toString());
                    if (this.waitingFile == null) {
                        if (this.fileTransferCanceled) {
                            sLogger.d("onMusicArtworkRequest, The file transfer has been canceled, send an empty artwork response");
                            this.sendArtworkResponse(this.waitingFileTransferIdentifier, (byte[])null, a0);
                        } else {
                            sLogger.d("onMusicArtworkRequest continuing file transfer");
                            this.iapFileTransferManager.onFileTransferSetupResponse(this.nowPlayingFileTransferIdentifier, true);
                        }
                    } else {
                        sLogger.d(new StringBuilder().append("onMusicArtworkRequest using waiting file ").append(this.waitingFileTransferIdentifier).toString());
                        this.sendArtworkResponse(this.waitingFileTransferIdentifier, this.waitingFile.data, a0);
                    }
                }
                /*monexit(a1)*/;
                b = true;
            }
        }
        /*monexit(this)*/;
        return b;
    }
    
    private void onPhotoUpdatesRequest(com.navdy.service.library.events.photo.PhotoUpdatesRequest a) {
        boolean b = false;
        com.navdy.service.library.events.photo.PhotoType a0 = a.photoType;
        com.navdy.service.library.events.photo.PhotoType a1 = com.navdy.service.library.events.photo.PhotoType.PHOTO_ALBUM_ART;
        label2: {
            label0: {
                label1: {
                    if (a0 != a1) {
                        break label1;
                    }
                    if (a.start != null) {
                        break label0;
                    }
                }
                b = false;
                break label2;
            }
            b = a.start.booleanValue();
        }
        sLogger.d(new StringBuilder().append("onPhotoUpdatesRequest: ").append(b).append(", ").append(a).toString());
        if (b) {
            this.link.sendMessageAsNavdyEvent((com.squareup.wire.Message)this.lastMusicArtworkResponse);
        }
    }
    
    private boolean sameTrack(com.navdy.service.library.events.audio.MusicArtworkRequest a, com.navdy.service.library.events.audio.MusicTrackInfo a0) {
        boolean b = false;
        label2: {
            label0: {
                label1: {
                    if (a == null) {
                        break label1;
                    }
                    if (a0 == null) {
                        break label1;
                    }
                    if (!android.text.TextUtils.equals((CharSequence)a.name, (CharSequence)a0.name)) {
                        break label1;
                    }
                    if (!android.text.TextUtils.equals((CharSequence)a.album, (CharSequence)a0.album)) {
                        break label1;
                    }
                    if (android.text.TextUtils.equals((CharSequence)a.author, (CharSequence)a0.author)) {
                        break label0;
                    }
                }
                b = false;
                break label2;
            }
            b = true;
        }
        return b;
    }
    
    private void sendArtworkResponse(int i, byte[] a, com.navdy.service.library.events.audio.MusicTrackInfo a0) {
        com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.device.connection.iAP2MusicHelper$1(this, i, a, a0), 1);
    }
    
    void close() {
        this.keyDownTime = 0L;
    }
    
    public void onFileReceived(int i, byte[] a) {
        label1: synchronized(this) {
            com.navdy.service.library.events.audio.MusicTrackInfo a0 = null;
            synchronized(this.helperStateLock) {
                com.navdy.service.library.log.Logger a2 = sLogger;
                a2.d(new StringBuilder().append("onFileReceived: ").append(i).append(" (").append((a == null) ? 0 : a.length).append(")").toString());
                Object a3 = this.transferIdTrackInfoMap.get(Integer.valueOf(i));
                label0: {
                    if (a3 != null) {
                        break label0;
                    }
                    sLogger.e("onFileReceived: no music info for this file (yet?)");
                    this.waitingFile = new com.navdy.hud.device.connection.iAP2MusicHelper$FileHolder(this, a);
                    /*monexit(a1)*/;
                    break label1;
                }
                a0 = (com.navdy.service.library.events.audio.MusicTrackInfo)this.transferIdTrackInfoMap.remove(Integer.valueOf(i));
                /*monexit(a1)*/;
            }
            this.sendArtworkResponse(i, a, a0);
        }
        /*monexit(this)*/;
    }
    
    public void onFileTransferCancel(int i) {
        synchronized(this) {
            synchronized(this.helperStateLock) {
                com.navdy.service.library.log.Logger a0 = sLogger;
                a0.d(new StringBuilder().append("onFileTransferCancel: ").append(i).toString());
                com.navdy.service.library.events.audio.MusicTrackInfo a1 = (com.navdy.service.library.events.audio.MusicTrackInfo)this.transferIdTrackInfoMap.remove(Integer.valueOf(i));
                com.navdy.service.library.events.audio.MusicArtworkRequest a2 = this.waitingArtworkRequest;
                label1: {
                    label0: {
                        if (a2 == null) {
                            break label0;
                        }
                        if (!this.sameTrack(this.waitingArtworkRequest, a1)) {
                            break label0;
                        }
                        if (this.nowPlayingFileTransferIdentifier != i) {
                            break label0;
                        }
                        sLogger.d("Sending response with null data");
                        this.sendArtworkResponse(i, (byte[])null, a1);
                        break label1;
                    }
                    sLogger.d("File transferred canceled, waiting for the meta data to send empty response");
                    this.fileTransferCanceled = true;
                    this.waitingFileTransferIdentifier = i;
                    this.waitingFile = null;
                }
                /*monexit(a)*/;
            }
        }
        /*monexit(this)*/;
    }
    
    public void onFileTransferSetup(int i, long j) {
        synchronized(this.helperStateLock) {
            com.navdy.service.library.log.Logger a0 = sLogger;
            a0.d(new StringBuilder().append("onFileTransferSetup, fileTransferIdentifier: ").append(i).append(", size: ").append(j).append(", waitingArtworkRequest: ").append(this.waitingArtworkRequest).append(", nowPlayingFileTransferIdentifier: ").append(this.nowPlayingFileTransferIdentifier).toString());
            com.navdy.service.library.events.audio.MusicTrackInfo a1 = (com.navdy.service.library.events.audio.MusicTrackInfo)this.transferIdTrackInfoMap.get(Integer.valueOf(i));
            this.fileTransferCanceled = false;
            com.navdy.service.library.events.audio.MusicArtworkRequest a2 = this.waitingArtworkRequest;
            label1: {
                label0: {
                    if (a2 == null) {
                        break label0;
                    }
                    if (!this.sameTrack(this.waitingArtworkRequest, a1)) {
                        break label0;
                    }
                    if (this.nowPlayingFileTransferIdentifier != i) {
                        break label0;
                    }
                    sLogger.d("onFileTransferSetup continuing file transfer");
                    this.iapFileTransferManager.onFileTransferSetupResponse(this.nowPlayingFileTransferIdentifier, true);
                    break label1;
                }
                sLogger.d(new StringBuilder().append("onFileTransferSetup waiting for artwork request, waitingFileTransferIdentifier: ").append(i).toString());
                this.waitingFileTransferIdentifier = i;
                this.waitingFile = null;
            }
            /*monexit(a)*/;
        }
    }
    
    public void onNowPlayingUpdate(com.navdy.hud.mfi.NowPlayingUpdate a) {
        synchronized(this) {
            com.navdy.service.library.events.audio.MusicTrackInfo a0 = com.navdy.hud.device.connection.IAPMessageUtility.getMusicTrackInfoForNowPlayingUpdate(a);
            synchronized(this.helperStateLock) {
                this.nowPlayingFileTransferIdentifier = a.mediaItemArtworkFileTransferIdentifier;
                sLogger.d(new StringBuilder().append("onNowPlayingUpdate nowPlayingFileTransferIdentifier: ").append(this.nowPlayingFileTransferIdentifier).append(", musicTrackInfo: ").append(a0).toString());
                label0: if (a0 != null) {
                    boolean b = android.text.TextUtils.isEmpty((CharSequence)a0.name);
                    label1: {
                        if (!b) {
                            break label1;
                        }
                        if (!android.text.TextUtils.isEmpty((CharSequence)a0.album)) {
                            break label1;
                        }
                        if (android.text.TextUtils.isEmpty((CharSequence)a0.author)) {
                            break label0;
                        }
                    }
                    this.transferIdTrackInfoMap.put(Integer.valueOf(this.nowPlayingFileTransferIdentifier), a0);
                }
                /*monexit(a1)*/;
            }
            this.link.sendMessageAsNavdyEvent((com.squareup.wire.Message)a0);
        }
        /*monexit(this)*/;
    }
    
    void onReady() {
        this.iapFileTransferManager.setFileTransferListener((com.navdy.hud.mfi.IAPFileTransferListener)this);
        this.iAPMusicManager.startNowPlayingUpdates();
    }
    
    void setBandwidthLevel(int i) {
        sLogger.d(new StringBuilder().append("Bandwidth level changing : ").append((i > 0) ? "NORMAL" : "LOW").toString());
        if (i > 0) {
            this.iapFileTransferManager.setFileTransferLimit(1048576);
        } else {
            this.iapFileTransferManager.setFileTransferLimit(10000);
        }
    }
}
