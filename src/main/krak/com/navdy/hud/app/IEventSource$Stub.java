package com.navdy.hud.app;

abstract public class IEventSource$Stub extends android.os.Binder implements com.navdy.hud.app.IEventSource {
    final private static String DESCRIPTOR = "com.navdy.hud.app.IEventSource";
    final static int TRANSACTION_addEventListener = 1;
    final static int TRANSACTION_postEvent = 3;
    final static int TRANSACTION_postRemoteEvent = 4;
    final static int TRANSACTION_removeEventListener = 2;
    
    public IEventSource$Stub() {
        this.attachInterface((android.os.IInterface)this, "com.navdy.hud.app.IEventSource");
    }
    
    public static com.navdy.hud.app.IEventSource asInterface(android.os.IBinder a) {
        Object a0 = null;
        label1: if (a != null) {
            android.os.IInterface a1 = a.queryLocalInterface("com.navdy.hud.app.IEventSource");
            label0: {
                if (a1 == null) {
                    break label0;
                }
                if (!(a1 instanceof com.navdy.hud.app.IEventSource)) {
                    break label0;
                }
                a0 = a1;
                break label1;
            }
            a0 = new com.navdy.hud.app.IEventSource$Stub$Proxy(a);
        } else {
            a0 = null;
        }
        return (com.navdy.hud.app.IEventSource)a0;
    }
    
    public android.os.IBinder asBinder() {
        return (android.os.IBinder)this;
    }
    
    public boolean onTransact(int i, android.os.Parcel a, android.os.Parcel a0, int i0) {
        boolean b = false;
        switch(i) {
            case 1598968902: {
                a0.writeString("com.navdy.hud.app.IEventSource");
                b = true;
                break;
            }
            case 4: {
                a.enforceInterface("com.navdy.hud.app.IEventSource");
                this.postRemoteEvent(a.readString(), a.createByteArray());
                b = true;
                break;
            }
            case 3: {
                a.enforceInterface("com.navdy.hud.app.IEventSource");
                this.postEvent(a.createByteArray());
                b = true;
                break;
            }
            case 2: {
                a.enforceInterface("com.navdy.hud.app.IEventSource");
                this.removeEventListener(com.navdy.hud.app.IEventListener$Stub.asInterface(a.readStrongBinder()));
                a0.writeNoException();
                b = true;
                break;
            }
            case 1: {
                a.enforceInterface("com.navdy.hud.app.IEventSource");
                this.addEventListener(com.navdy.hud.app.IEventListener$Stub.asInterface(a.readStrongBinder()));
                a0.writeNoException();
                b = true;
                break;
            }
            default: {
                b = super.onTransact(i, a, a0, i0);
            }
        }
        return b;
    }
}
