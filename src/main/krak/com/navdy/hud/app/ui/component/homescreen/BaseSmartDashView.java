package com.navdy.hud.app.ui.component.homescreen;

public class BaseSmartDashView extends android.widget.FrameLayout implements com.navdy.hud.app.manager.InputManager$IInputHandler, com.navdy.hud.app.gesture.GestureDetector$GestureListener, com.navdy.hud.app.ui.component.homescreen.IHomeScreenLifecycle {
    protected com.squareup.otto.Bus bus;
    private android.os.Handler handler;
    protected com.navdy.hud.app.ui.component.homescreen.HomeScreenView homeScreenView;
    private int lastObdSpeed;
    private com.navdy.service.library.log.Logger logger;
    private com.navdy.hud.app.manager.SpeedManager speedManager;
    protected com.navdy.hud.app.ui.framework.UIStateManager uiStateManager;
    
    public BaseSmartDashView(android.content.Context a) {
        this(a, (android.util.AttributeSet)null);
    }
    
    public BaseSmartDashView(android.content.Context a, android.util.AttributeSet a0) {
        this(a, a0, 0);
    }
    
    public BaseSmartDashView(android.content.Context a, android.util.AttributeSet a0, int i) {
        super(a, a0, i);
        this.lastObdSpeed = -1;
        this.handler = new android.os.Handler();
    }
    
    static void access$000(com.navdy.hud.app.ui.component.homescreen.BaseSmartDashView a, float f) {
        a.updateSpeedLimitInternal(f);
    }
    
    private void initViews() {
        if (this.uiStateManager != null) {
            if (this.uiStateManager.getCurrentDashboardType() == null) {
                this.logger.v(new StringBuilder().append("setting initial dashtype = ").append(com.navdy.hud.app.ui.component.homescreen.SmartDashViewConstants$Type.Smart).toString());
                this.uiStateManager.setCurrentDashboardType(com.navdy.hud.app.ui.component.homescreen.SmartDashViewConstants$Type.Smart);
            }
            this.setDashboard(this.uiStateManager.getCurrentDashboardType());
        }
        if (com.navdy.hud.app.framework.voice.TTSUtils.isDebugTTSEnabled()) {
            this.handler.postDelayed((Runnable)new com.navdy.hud.app.ui.component.homescreen.BaseSmartDashView$1(this), 60000L);
        }
    }
    
    private void updateSpeedLimitInternal(float f) {
        com.navdy.hud.app.manager.SpeedManager$SpeedUnit a = com.navdy.hud.app.manager.SpeedManager.getInstance().getSpeedUnit();
        this.updateSpeedLimit(Math.round((float)com.navdy.hud.app.manager.SpeedManager.convert((double)f, com.navdy.hud.app.manager.SpeedManager$SpeedUnit.METERS_PER_SECOND, a)));
    }
    
    public void ObdPidChangeEvent(com.navdy.hud.app.obd.ObdManager$ObdPidChangeEvent a) {
        if (a.pid.getId() == 13) {
            this.updateSpeed(false);
        }
    }
    
    public void ObdStateChangeEvent(com.navdy.hud.app.obd.ObdManager$ObdConnectionStatusEvent a) {
        this.updateSpeed(true);
    }
    
    protected void adjustDashHeight(int i) {
        ((android.view.ViewGroup$MarginLayoutParams)this.getLayoutParams()).height = i;
        this.invalidate();
        this.requestLayout();
        if (i == com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.openMapHeight && this.homeScreenView.getDisplayMode() == com.navdy.hud.app.ui.component.homescreen.HomeScreen$DisplayMode.SMART_DASH) {
            this.animateToFullMode();
        }
    }
    
    protected void animateToFullMode() {
    }
    
    protected void animateToFullModeInternal(android.animation.AnimatorSet$Builder a) {
    }
    
    public void getCustomAnimator(com.navdy.hud.app.view.MainView$CustomAnimationMode a, android.animation.AnimatorSet$Builder a0) {
    }
    
    public com.navdy.hud.app.ui.component.homescreen.SmartDashViewConstants$Type getDashBoardType() {
        return this.uiStateManager.getCurrentDashboardType();
    }
    
    public void getTopAnimator(android.animation.AnimatorSet$Builder a, boolean b) {
    }
    
    public void init(com.navdy.hud.app.ui.component.homescreen.HomeScreenView a) {
        this.homeScreenView = a;
        this.initializeView();
        this.bus.register(new com.navdy.hud.app.ui.component.homescreen.BaseSmartDashView$2(this));
    }
    
    protected void initializeView() {
        this.updateSpeedLimitInternal(com.navdy.hud.app.maps.here.HereMapUtil.getCurrentSpeedLimit());
        this.updateSpeed(true);
    }
    
    public com.navdy.hud.app.manager.InputManager$IInputHandler nextHandler() {
        return null;
    }
    
    public void onClick() {
    }
    
    protected void onFinishInflate() {
        this.logger = com.navdy.hud.app.ui.component.homescreen.HomeScreenView.sLogger;
        super.onFinishInflate();
        if (!this.isInEditMode()) {
            butterknife.ButterKnife.inject((android.view.View)this);
            this.speedManager = com.navdy.hud.app.manager.SpeedManager.getInstance();
            com.navdy.hud.app.manager.RemoteDeviceManager a = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance();
            this.bus = a.getBus();
            this.uiStateManager = a.getUiStateManager();
            this.initViews();
        }
    }
    
    public boolean onGesture(com.navdy.service.library.events.input.GestureEvent a) {
        return false;
    }
    
    public boolean onKey(com.navdy.hud.app.manager.InputManager$CustomKeyEvent a) {
        return false;
    }
    
    public void onPause() {
    }
    
    public void onResume() {
    }
    
    public void onTrackHand(float f) {
    }
    
    public void resetTopViewsAnimator() {
        com.navdy.hud.app.view.MainView$CustomAnimationMode a = this.uiStateManager.getCustomAnimateMode();
        this.logger.v(new StringBuilder().append("mode-view:").append(a).toString());
        this.setView(a);
    }
    
    public void setDashboard(com.navdy.hud.app.ui.component.homescreen.SmartDashViewConstants$Type a) {
        switch(com.navdy.hud.app.ui.component.homescreen.BaseSmartDashView$3.$SwitchMap$com$navdy$hud$app$ui$component$homescreen$SmartDashViewConstants$Type[a.ordinal()]) {
            case 2: {
                this.uiStateManager.setCurrentDashboardType(a);
                break;
            }
            case 1: {
                this.uiStateManager.setCurrentDashboardType(a);
                break;
            }
        }
    }
    
    public void setMiddleGaugeView(com.navdy.hud.app.view.MainView$CustomAnimationMode a) {
    }
    
    public void setView(com.navdy.hud.app.view.MainView$CustomAnimationMode a) {
        this.setMiddleGaugeView(a);
    }
    
    public boolean shouldShowTopViews() {
        return true;
    }
    
    public void updateLayoutForMode(com.navdy.hud.app.maps.NavigationMode a, boolean b) {
        boolean b0 = this.homeScreenView.isNavigationActive();
        if (b) {
            this.adjustDashHeight(b0 ? com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.activeMapHeight : com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.openMapHeight);
        }
    }
    
    protected void updateSpeed(int i) {
    }
    
    protected void updateSpeed(boolean b) {
        boolean b0 = false;
        int i = this.speedManager.getCurrentSpeed();
        if (i == -1) {
            i = 0;
        }
        label0: {
            label1: {
                if (b) {
                    break label1;
                }
                if (i == this.lastObdSpeed) {
                    b0 = false;
                    break label0;
                }
            }
            this.lastObdSpeed = i;
            this.updateSpeed(i);
            b0 = true;
        }
        if (b0 && this.logger.isLoggable(2)) {
            this.logger.v(new StringBuilder().append("SPEED updated :").append(i).toString());
        }
    }
    
    protected void updateSpeedLimit(int i) {
    }
}
