package com.navdy.hud.app.ui.component.destination;

class DestinationPickerScreen$Presenter$5 implements Runnable {
    final com.navdy.hud.app.ui.component.destination.DestinationPickerScreen$Presenter this$0;
    final int val$id;
    final int val$pos;
    
    DestinationPickerScreen$Presenter$5(com.navdy.hud.app.ui.component.destination.DestinationPickerScreen$Presenter a, int i, int i0) {
        super();
        this.this$0 = a;
        this.val$pos = i;
        this.val$id = i0;
    }
    
    public void run() {
        com.navdy.hud.app.ui.component.destination.DestinationPickerView a = (com.navdy.hud.app.ui.component.destination.DestinationPickerView)com.navdy.hud.app.ui.component.destination.DestinationPickerScreen$Presenter.access$900(this.this$0);
        label7: if (a != null) {
            boolean b = false;
            java.util.List a0 = com.navdy.hud.app.ui.component.destination.DestinationPickerScreen$Presenter.access$1000(this.this$0);
            label5: {
                label6: {
                    if (a0 == null) {
                        break label6;
                    }
                    if (this.val$pos < com.navdy.hud.app.ui.component.destination.DestinationPickerScreen$Presenter.access$1000(this.this$0).size()) {
                        break label5;
                    }
                }
                com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.access$100().w("invalid list,no selection");
                com.navdy.hud.app.ui.component.destination.DestinationPickerScreen$Presenter.access$1102(this.this$0, false);
                break label7;
            }
            if (com.navdy.hud.app.ui.component.destination.DestinationPickerScreen$Presenter.access$400(this.this$0) == null) {
                b = false;
            } else {
                int i = 0;
                com.navdy.hud.app.ui.component.destination.DestinationPickerScreen$DestinationPickerState a1 = new com.navdy.hud.app.ui.component.destination.DestinationPickerScreen$DestinationPickerState();
                com.navdy.hud.app.ui.component.destination.DestinationParcelable a2 = (com.navdy.hud.app.ui.component.destination.DestinationParcelable)((com.navdy.hud.app.ui.component.vlist.VerticalList$Model)com.navdy.hud.app.ui.component.destination.DestinationPickerScreen$Presenter.access$1000(this.this$0).get(this.val$pos)).state;
                label4: {
                    label3: {
                        if (a2 == null) {
                            break label3;
                        }
                        if (a2.id == 0) {
                            break label3;
                        }
                        i = a2.id;
                        break label4;
                    }
                    i = this.val$id;
                }
                b = com.navdy.hud.app.ui.component.destination.DestinationPickerScreen$Presenter.access$400(this.this$0).onItemClicked(i, this.val$pos - 1, a1);
                if (com.navdy.hud.app.ui.component.destination.DestinationPickerScreen$Presenter.access$1200(this.this$0)) {
                    com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.access$100().v(new StringBuilder().append("map-route- doNotAddOriginalRoute=").append(a1.doNotAddOriginalRoute).toString());
                    this.this$0.doNotAddOriginalRoute = a1.doNotAddOriginalRoute;
                }
            }
            label2: {
                label0: {
                    label1: {
                        if (b) {
                            break label1;
                        }
                        if (!com.navdy.hud.app.ui.component.destination.DestinationPickerScreen$Presenter.access$1300(this.this$0, this.val$pos)) {
                            break label0;
                        }
                    }
                    com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.access$100().v("overridden");
                    this.this$0.close();
                    break label2;
                }
                com.navdy.hud.app.ui.component.vlist.VerticalList$Model a3 = (com.navdy.hud.app.ui.component.vlist.VerticalList$Model)com.navdy.hud.app.ui.component.destination.DestinationPickerScreen$Presenter.access$1000(this.this$0).get(this.val$pos);
                com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.access$100().v(new StringBuilder().append("not overriden, launching pos:").append(this.val$pos).append(" title=").append(a3.title).toString());
                com.navdy.hud.app.ui.component.destination.DestinationParcelable a4 = (com.navdy.hud.app.ui.component.destination.DestinationParcelable)a3.state;
                com.navdy.hud.app.ui.component.destination.DestinationPickerScreen$Presenter.access$1400(this.this$0, a, a4);
            }
            com.navdy.hud.app.ui.component.destination.DestinationPickerScreen$Presenter.access$1102(this.this$0, true);
        }
    }
}
