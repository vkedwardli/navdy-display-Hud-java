package com.navdy.hud.app.ui.component.mainmenu;

class MusicMenu2$2 implements com.navdy.hud.app.util.MusicArtworkCache$Callback {
    final com.navdy.hud.app.ui.component.mainmenu.MusicMenu2 this$0;
    
    MusicMenu2$2(com.navdy.hud.app.ui.component.mainmenu.MusicMenu2 a) {
        super();
        this.this$0 = a;
    }
    
    public void onHit(byte[] a) {
        com.navdy.hud.app.ui.component.mainmenu.MusicMenu2.access$800(this.this$0, a, (Integer)null, com.navdy.hud.app.ui.component.mainmenu.MusicMenu2.access$700(this.this$0, com.navdy.hud.app.ui.component.mainmenu.MusicMenu2.access$600(this.this$0).musicCollectionInfo), false);
    }
    
    public void onMiss() {
        com.navdy.service.library.events.audio.MusicArtworkRequest a = new com.navdy.service.library.events.audio.MusicArtworkRequest$Builder().collectionSource(com.navdy.hud.app.ui.component.mainmenu.MusicMenu2.access$600(this.this$0).musicCollectionInfo.collectionSource).collectionType(com.navdy.hud.app.ui.component.mainmenu.MusicMenu2.access$600(this.this$0).musicCollectionInfo.collectionType).collectionId(com.navdy.hud.app.ui.component.mainmenu.MusicMenu2.access$600(this.this$0).musicCollectionInfo.collectionId).size(Integer.valueOf(200)).build();
        com.navdy.hud.app.ui.component.mainmenu.MusicMenu2.access$900(this.this$0, a, com.navdy.hud.app.ui.component.mainmenu.MusicMenu2.access$600(this.this$0).musicCollectionInfo);
    }
}
