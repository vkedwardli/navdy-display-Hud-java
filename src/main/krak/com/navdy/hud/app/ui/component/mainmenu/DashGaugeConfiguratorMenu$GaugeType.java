package com.navdy.hud.app.ui.component.mainmenu;


    public enum DashGaugeConfiguratorMenu$GaugeType {
        CENTER(0),
        SIDE(1);

        private int value;
        DashGaugeConfiguratorMenu$GaugeType(int value) {
            this.value = value;
        }
        public int getValue() {
            return value;
        }
    }
    
//final class DashGaugeConfiguratorMenu$GaugeType extends Enum {
//    final private static com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu$GaugeType[] $VALUES;
//    final public static com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu$GaugeType CENTER;
//    final public static com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu$GaugeType SIDE;
//    
//    static {
//        com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu$GaugeType a = new com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu$GaugeType("CENTER", 0);
//        CENTER = a;
//        com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu$GaugeType a0 = new com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu$GaugeType("SIDE", 1);
//        SIDE = a0;
//        com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu$GaugeType[] a1 = new com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu$GaugeType[2];
//        a1[0] = a;
//        a1[1] = a0;
//        $VALUES = a1;
//    }
//    
//    protected DashGaugeConfiguratorMenu$GaugeType(String s, int i) {
//        super(s, i);
//    }
//    
//    public static com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu$GaugeType valueOf(String s) {
//        return (com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu$GaugeType)Enum.valueOf(com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu$GaugeType.class, s);
//    }
//    
//    public static com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu$GaugeType[] values() {
//        return $VALUES.clone();
//    }
//}
//