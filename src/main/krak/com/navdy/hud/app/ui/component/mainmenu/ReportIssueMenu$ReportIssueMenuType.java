package com.navdy.hud.app.ui.component.mainmenu;


    public enum ReportIssueMenu$ReportIssueMenuType {
        NAVIGATION_ISSUES(0),
        SNAP_SHOT(1);

        private int value;
        ReportIssueMenu$ReportIssueMenuType(int value) {
            this.value = value;
        }
        public int getValue() {
            return value;
        }
    }
    
//final class ReportIssueMenu$ReportIssueMenuType extends Enum {
//    final private static com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu$ReportIssueMenuType[] $VALUES;
//    final public static com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu$ReportIssueMenuType NAVIGATION_ISSUES;
//    final public static com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu$ReportIssueMenuType SNAP_SHOT;
//    
//    static {
//        com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu$ReportIssueMenuType a = new com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu$ReportIssueMenuType("NAVIGATION_ISSUES", 0);
//        NAVIGATION_ISSUES = a;
//        com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu$ReportIssueMenuType a0 = new com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu$ReportIssueMenuType("SNAP_SHOT", 1);
//        SNAP_SHOT = a0;
//        com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu$ReportIssueMenuType[] a1 = new com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu$ReportIssueMenuType[2];
//        a1[0] = a;
//        a1[1] = a0;
//        $VALUES = a1;
//    }
//    
//    protected ReportIssueMenu$ReportIssueMenuType(String s, int i) {
//        super(s, i);
//    }
//    
//    public static com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu$ReportIssueMenuType valueOf(String s) {
//        return (com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu$ReportIssueMenuType)Enum.valueOf(com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu$ReportIssueMenuType.class, s);
//    }
//    
//    public static com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu$ReportIssueMenuType[] values() {
//        return $VALUES.clone();
//    }
//}
//