package com.navdy.hud.app.ui.interpolator;

public class FastOutSlowInInterpolator extends com.navdy.hud.app.ui.interpolator.LookupTableInterpolator {
    final private static float[] VALUES;
    
    static {
        float[] a = new float[201];
        a[0] = 0.0f;
        a[1] = 0.0001f;
        a[2] = 0.0002f;
        a[3] = 0.0005f;
        a[4] = 0.0009f;
        a[5] = 0.0014f;
        a[6] = 0.002f;
        a[7] = 0.0027f;
        a[8] = 0.0036f;
        a[9] = 0.0046f;
        a[10] = 0.0058f;
        a[11] = 0.0071f;
        a[12] = 0.0085f;
        a[13] = 0.0101f;
        a[14] = 0.0118f;
        a[15] = 0.0137f;
        a[16] = 0.0158f;
        a[17] = 0.018f;
        a[18] = 0.0205f;
        a[19] = 0.0231f;
        a[20] = 0.0259f;
        a[21] = 0.0289f;
        a[22] = 0.0321f;
        a[23] = 0.0355f;
        a[24] = 0.0391f;
        a[25] = 0.043f;
        a[26] = 0.0471f;
        a[27] = 0.0514f;
        a[28] = 0.056f;
        a[29] = 0.0608f;
        a[30] = 0.066f;
        a[31] = 0.0714f;
        a[32] = 0.0771f;
        a[33] = 0.083f;
        a[34] = 0.0893f;
        a[35] = 0.0959f;
        a[36] = 0.1029f;
        a[37] = 0.1101f;
        a[38] = 0.1177f;
        a[39] = 0.1257f;
        a[40] = 0.1339f;
        a[41] = 0.1426f;
        a[42] = 0.1516f;
        a[43] = 0.161f;
        a[44] = 0.1707f;
        a[45] = 0.1808f;
        a[46] = 0.1913f;
        a[47] = 0.2021f;
        a[48] = 0.2133f;
        a[49] = 0.2248f;
        a[50] = 0.2366f;
        a[51] = 0.2487f;
        a[52] = 0.2611f;
        a[53] = 0.2738f;
        a[54] = 0.2867f;
        a[55] = 0.2998f;
        a[56] = 0.3131f;
        a[57] = 0.3265f;
        a[58] = 0.34f;
        a[59] = 0.3536f;
        a[60] = 0.3673f;
        a[61] = 0.381f;
        a[62] = 0.3946f;
        a[63] = 0.4082f;
        a[64] = 0.4217f;
        a[65] = 0.4352f;
        a[66] = 0.4485f;
        a[67] = 0.4616f;
        a[68] = 0.4746f;
        a[69] = 0.4874f;
        a[70] = 0.5f;
        a[71] = 0.5124f;
        a[72] = 0.5246f;
        a[73] = 0.5365f;
        a[74] = 0.5482f;
        a[75] = 0.5597f;
        a[76] = 0.571f;
        a[77] = 0.582f;
        a[78] = 0.5928f;
        a[79] = 0.6033f;
        a[80] = 0.6136f;
        a[81] = 0.6237f;
        a[82] = 0.6335f;
        a[83] = 0.6431f;
        a[84] = 0.6525f;
        a[85] = 0.6616f;
        a[86] = 0.6706f;
        a[87] = 0.6793f;
        a[88] = 0.6878f;
        a[89] = 0.6961f;
        a[90] = 0.7043f;
        a[91] = 0.7122f;
        a[92] = 0.7199f;
        a[93] = 0.7275f;
        a[94] = 0.7349f;
        a[95] = 0.7421f;
        a[96] = 0.7491f;
        a[97] = 0.7559f;
        a[98] = 0.7626f;
        a[99] = 0.7692f;
        a[100] = 0.7756f;
        a[101] = 0.7818f;
        a[102] = 0.7879f;
        a[103] = 0.7938f;
        a[104] = 0.7996f;
        a[105] = 0.8053f;
        a[106] = 0.8108f;
        a[107] = 0.8162f;
        a[108] = 0.8215f;
        a[109] = 0.8266f;
        a[110] = 0.8317f;
        a[111] = 0.8366f;
        a[112] = 0.8414f;
        a[113] = 0.8461f;
        a[114] = 0.8507f;
        a[115] = 0.8551f;
        a[116] = 0.8595f;
        a[117] = 0.8638f;
        a[118] = 0.8679f;
        a[119] = 0.872f;
        a[120] = 0.876f;
        a[121] = 0.8798f;
        a[122] = 0.8836f;
        a[123] = 0.8873f;
        a[124] = 0.8909f;
        a[125] = 0.8945f;
        a[126] = 0.8979f;
        a[127] = 0.9013f;
        a[128] = 0.9046f;
        a[129] = 0.9078f;
        a[130] = 0.9109f;
        a[131] = 0.9139f;
        a[132] = 0.9169f;
        a[133] = 0.9198f;
        a[134] = 0.9227f;
        a[135] = 0.9254f;
        a[136] = 0.9281f;
        a[137] = 0.9307f;
        a[138] = 0.9333f;
        a[139] = 0.9358f;
        a[140] = 0.9382f;
        a[141] = 0.9406f;
        a[142] = 0.9429f;
        a[143] = 0.9452f;
        a[144] = 0.9474f;
        a[145] = 0.9495f;
        a[146] = 0.9516f;
        a[147] = 0.9536f;
        a[148] = 0.9556f;
        a[149] = 0.9575f;
        a[150] = 0.9594f;
        a[151] = 0.9612f;
        a[152] = 0.9629f;
        a[153] = 0.9646f;
        a[154] = 0.9663f;
        a[155] = 0.9679f;
        a[156] = 0.9695f;
        a[157] = 0.971f;
        a[158] = 0.9725f;
        a[159] = 0.9739f;
        a[160] = 0.9753f;
        a[161] = 0.9766f;
        a[162] = 0.9779f;
        a[163] = 0.9791f;
        a[164] = 0.9803f;
        a[165] = 0.9815f;
        a[166] = 0.9826f;
        a[167] = 0.9837f;
        a[168] = 0.9848f;
        a[169] = 0.9858f;
        a[170] = 0.9867f;
        a[171] = 0.9877f;
        a[172] = 0.9885f;
        a[173] = 0.9894f;
        a[174] = 0.9902f;
        a[175] = 0.991f;
        a[176] = 0.9917f;
        a[177] = 0.9924f;
        a[178] = 0.9931f;
        a[179] = 0.9937f;
        a[180] = 0.9944f;
        a[181] = 0.9949f;
        a[182] = 0.9955f;
        a[183] = 0.996f;
        a[184] = 0.9964f;
        a[185] = 0.9969f;
        a[186] = 0.9973f;
        a[187] = 0.9977f;
        a[188] = 0.998f;
        a[189] = 0.9984f;
        a[190] = 0.9986f;
        a[191] = 0.9989f;
        a[192] = 0.9991f;
        a[193] = 0.9993f;
        a[194] = 0.9995f;
        a[195] = 0.9997f;
        a[196] = 0.9998f;
        a[197] = 0.9999f;
        a[198] = 0.9999f;
        a[199] = 1f;
        a[200] = 1f;
        VALUES = a;
    }
    
    public FastOutSlowInInterpolator() {
        super(VALUES);
    }
    
    public float getInterpolation(float f) {
        return super.getInterpolation(f);
    }
}
