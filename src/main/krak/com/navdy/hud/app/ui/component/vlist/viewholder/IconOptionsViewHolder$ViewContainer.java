package com.navdy.hud.app.ui.component.vlist.viewholder;

class IconOptionsViewHolder$ViewContainer {
    com.navdy.hud.app.ui.component.image.IconColorImageView big;
    com.navdy.hud.app.ui.component.image.CrossFadeImageView crossFadeImageView;
    com.navdy.hud.app.ui.component.HaloView haloView;
    android.view.ViewGroup iconContainer;
    com.navdy.hud.app.ui.component.image.IconColorImageView small;
    
    private IconOptionsViewHolder$ViewContainer() {
    }
    
    IconOptionsViewHolder$ViewContainer(com.navdy.hud.app.ui.component.vlist.viewholder.IconOptionsViewHolder$1 a) {
        this();
    }
}
