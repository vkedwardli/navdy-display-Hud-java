package com.navdy.hud.app.ui.component;

public class ChoiceLayout2$Choice implements android.os.Parcelable {
    final public static android.os.Parcelable$Creator CREATOR;
    final private int fluctuatorColor;
    final private int id;
    final private String label;
    final private int resIdSelected;
    final private int resIdUnselected;
    final private int selectedColor;
    final private int unselectedColor;
    
    static {
        CREATOR = (android.os.Parcelable$Creator)new com.navdy.hud.app.ui.component.ChoiceLayout2$Choice$1();
    }
    
    public ChoiceLayout2$Choice(int i, int i0, int i1, int i2, int i3, String s, int i4) {
        this.id = i;
        this.resIdSelected = i0;
        this.selectedColor = i1;
        this.resIdUnselected = i2;
        this.unselectedColor = i3;
        this.label = s;
        this.fluctuatorColor = i4;
    }
    
    public ChoiceLayout2$Choice(android.os.Parcel a) {
        this.id = a.readInt();
        this.resIdSelected = a.readInt();
        this.selectedColor = a.readInt();
        this.resIdUnselected = a.readInt();
        this.unselectedColor = a.readInt();
        String s = a.readString();
        if (android.text.TextUtils.isEmpty((CharSequence)s)) {
            this.label = null;
        } else {
            this.label = s;
        }
        this.fluctuatorColor = a.readInt();
    }
    
    static int access$100(com.navdy.hud.app.ui.component.ChoiceLayout2$Choice a) {
        return a.resIdSelected;
    }
    
    static int access$200(com.navdy.hud.app.ui.component.ChoiceLayout2$Choice a) {
        return a.selectedColor;
    }
    
    static int access$300(com.navdy.hud.app.ui.component.ChoiceLayout2$Choice a) {
        return a.resIdUnselected;
    }
    
    static int access$400(com.navdy.hud.app.ui.component.ChoiceLayout2$Choice a) {
        return a.unselectedColor;
    }
    
    static int access$500(com.navdy.hud.app.ui.component.ChoiceLayout2$Choice a) {
        return a.fluctuatorColor;
    }
    
    static int access$800(com.navdy.hud.app.ui.component.ChoiceLayout2$Choice a) {
        return a.id;
    }
    
    static String access$900(com.navdy.hud.app.ui.component.ChoiceLayout2$Choice a) {
        return a.label;
    }
    
    public int describeContents() {
        return 0;
    }
    
    public int getId() {
        return this.id;
    }
    
    public void writeToParcel(android.os.Parcel a, int i) {
        a.writeInt(this.id);
        a.writeInt(this.resIdSelected);
        a.writeInt(this.selectedColor);
        a.writeInt(this.resIdUnselected);
        a.writeInt(this.unselectedColor);
        a.writeString((this.label == null) ? "" : this.label);
        a.writeInt(this.fluctuatorColor);
    }
}
