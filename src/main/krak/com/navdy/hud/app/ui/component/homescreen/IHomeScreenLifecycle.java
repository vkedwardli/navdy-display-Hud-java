package com.navdy.hud.app.ui.component.homescreen;

abstract public interface IHomeScreenLifecycle {
    abstract public void onPause();
    
    
    abstract public void onResume();
}
