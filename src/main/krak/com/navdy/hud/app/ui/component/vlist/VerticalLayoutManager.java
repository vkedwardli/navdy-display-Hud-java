package com.navdy.hud.app.ui.component.vlist;

public class VerticalLayoutManager extends android.support.v7.widget.LinearLayoutManager {
    final private static com.navdy.service.library.log.Logger sLogger;
    private com.navdy.hud.app.ui.component.vlist.VerticalList$Callback callback;
    private android.content.Context context;
    private boolean listLoaded;
    private com.navdy.hud.app.ui.component.vlist.VerticalRecyclerView recyclerView;
    private com.navdy.hud.app.ui.component.vlist.VerticalList vlist;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.ui.component.vlist.VerticalLayoutManager.class);
    }
    
    public VerticalLayoutManager(android.content.Context a, com.navdy.hud.app.ui.component.vlist.VerticalList a0, com.navdy.hud.app.ui.component.vlist.VerticalList$Callback a1, com.navdy.hud.app.ui.component.vlist.VerticalRecyclerView a2) {
        super(a);
        this.context = a;
        this.vlist = a0;
        this.callback = a1;
        this.recyclerView = a2;
    }
    
    static com.navdy.service.library.log.Logger access$000() {
        return sLogger;
    }
    
    public void clear() {
        this.listLoaded = false;
    }
    
    public void onLayoutChildren(android.support.v7.widget.RecyclerView$Recycler a, android.support.v7.widget.RecyclerView$State a0) {
        super.onLayoutChildren(a, a0);
        if (!this.listLoaded && !a0.isPreLayout()) {
            this.listLoaded = true;
            this.callback.onLoad();
        }
    }
    
    public void smoothScrollToPosition(int i, int i0) {
        com.navdy.hud.app.ui.component.vlist.VerticalLayoutManager$VerticalScroller a = new com.navdy.hud.app.ui.component.vlist.VerticalLayoutManager$VerticalScroller(this.context, this.vlist, this, this.recyclerView, i, i0);
        a.setTargetPosition(i);
        this.startSmoothScroll((android.support.v7.widget.RecyclerView$SmoothScroller)a);
    }
}
