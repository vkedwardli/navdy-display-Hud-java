package com.navdy.hud.app.ui.component;
import com.navdy.hud.app.R;

public class FullScreenNotificationLayout extends android.widget.RelativeLayout {
    final private static long ANIM_DURATION = 800L;
    private boolean autoPopup;
    private java.util.List bodyViews;
    private float directionalTranslation;
    private android.view.View footer;
    private int footerId;
    private android.view.View title;
    private int titleId;
    
    public FullScreenNotificationLayout(android.content.Context a, android.util.AttributeSet a0) {
        super(a, a0);
        this.bodyViews = (java.util.List)new java.util.ArrayList();
        float f = this.getResources().getDimension(R.dimen.text_fade_anim_translationy);
        android.content.res.TypedArray a1 = a.obtainStyledAttributes(a0, com.navdy.hud.app.R$styleable.FullScreenNotificationLayout);
        label1: {
            Throwable a2 = null;
            if (a1 == null) {
                break label1;
            }
            label0: {
                try {
                    this.titleId = a1.getResourceId(0, 0);
                    this.footerId = a1.getResourceId(1, 0);
                    if (com.navdy.hud.app.ui.component.FullScreenNotificationLayout$AnimationDirection.values()[a1.getInt(2, 0)] != com.navdy.hud.app.ui.component.FullScreenNotificationLayout$AnimationDirection.UP) {
                        f = -f;
                    }
                    this.directionalTranslation = f;
                    this.autoPopup = a1.getBoolean(3, true);
                } catch(Throwable a3) {
                    a2 = a3;
                    break label0;
                }
                a1.recycle();
                break label1;
            }
            a1.recycle();
            throw a2;
        }
    }
    
    static void access$000(com.navdy.hud.app.ui.component.FullScreenNotificationLayout a, Runnable a0) {
        a.animateInSecondStage(a0);
    }
    
    static void access$100(com.navdy.hud.app.ui.component.FullScreenNotificationLayout a, Runnable a0) {
        a.animateOutSecondStage(a0);
    }
    
    private void animateInSecondStage(Runnable a) {
        if (this.title != null) {
            if (a == null) {
                this.getInTitleFooterAnimation(this.title);
            } else {
                this.getInTitleFooterAnimation(this.title).withEndAction(a);
            }
        }
        if (this.footer != null) {
            if (this.title != null) {
                this.getInTitleFooterAnimation(this.footer);
            } else if (a == null) {
                this.getInTitleFooterAnimation(this.footer);
            } else {
                this.getInTitleFooterAnimation(this.footer).withEndAction(a);
            }
        }
    }
    
    private void animateOutSecondStage(Runnable a) {
        java.util.Iterator a0 = this.bodyViews.iterator();
        Object a1 = a;
        boolean b = false;
        Object a2 = a0;
        while(((java.util.Iterator)a2).hasNext()) {
            android.view.View a3 = (android.view.View)((java.util.Iterator)a2).next();
            if (b) {
                this.getOutBodyAnimation(a3);
            } else if (a1 == null) {
                this.getOutBodyAnimation(a3);
                b = true;
            } else {
                this.getOutBodyAnimation(a3).withEndAction((Runnable)a1);
                b = true;
            }
        }
    }
    
    private android.view.ViewPropertyAnimator getInBodyAnimation(android.view.View a) {
        return a.animate().alpha(1f).translationY(0.0f).setDuration(800L);
    }
    
    private android.view.ViewPropertyAnimator getInTitleFooterAnimation(android.view.View a) {
        return a.animate().alpha(1f).setDuration(800L);
    }
    
    private android.view.ViewPropertyAnimator getOutBodyAnimation(android.view.View a) {
        return a.animate().alpha(0.0f).translationY(this.directionalTranslation).setDuration(800L);
    }
    
    private android.view.ViewPropertyAnimator getOutTitleFooterAnimation(android.view.View a) {
        return a.animate().alpha(0.0f).setDuration(800L);
    }
    
    private void initialize() {
        this.title = this.findViewById(this.titleId);
        this.footer = this.findViewById(this.footerId);
        this.title.setAlpha(0.0f);
        this.footer.setAlpha(0.0f);
        int i = this.getChildCount();
        int i0 = 0;
        while(i0 < i) {
            android.view.View a = this.getChildAt(i0);
            if (a.getId() != this.titleId && a.getId() != this.footerId) {
                a.setAlpha(0.0f);
                a.setTranslationY(this.directionalTranslation);
                this.bodyViews.add(a);
            }
            i0 = i0 + 1;
        }
    }
    
    public void animateIn() {
        this.animateIn((Runnable)null);
    }
    
    public void animateIn(Runnable a) {
        java.util.Iterator a0 = this.bodyViews.iterator();
        Object a1 = a;
        boolean b = false;
        Object a2 = a0;
        while(((java.util.Iterator)a2).hasNext()) {
            android.view.View a3 = (android.view.View)((java.util.Iterator)a2).next();
            if (b) {
                this.getInBodyAnimation(a3);
            } else {
                if (this.title == null && this.footer == null) {
                    if (a1 == null) {
                        this.getInBodyAnimation(a3);
                        b = true;
                        continue;
                    } else {
                        this.getInBodyAnimation(a3).withEndAction((Runnable)a1);
                        b = true;
                        continue;
                    }
                }
                this.getInBodyAnimation(a3).withEndAction((Runnable)new com.navdy.hud.app.ui.component.FullScreenNotificationLayout$1(this, (Runnable)a1));
                b = true;
            }
        }
        if (!b) {
            this.animateInSecondStage((Runnable)a1);
        }
    }
    
    public void animateOut(Runnable a) {
        android.view.View a0 = this.title;
        label1: {
            label0: {
                if (a0 != null) {
                    break label0;
                }
                if (this.footer != null) {
                    break label0;
                }
                this.animateOutSecondStage(a);
                break label1;
            }
            if (this.title != null) {
                this.getOutTitleFooterAnimation(this.title).withEndAction((Runnable)new com.navdy.hud.app.ui.component.FullScreenNotificationLayout$2(this, a));
            }
            if (this.footer != null) {
                if (this.title != null) {
                    this.getOutTitleFooterAnimation(this.footer);
                } else {
                    this.getOutTitleFooterAnimation(this.footer).withEndAction((Runnable)new com.navdy.hud.app.ui.component.FullScreenNotificationLayout$3(this, a));
                }
            }
        }
    }
    
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (this.autoPopup) {
            this.animateIn();
        }
    }
    
    protected void onFinishInflate() {
        super.onFinishInflate();
        this.initialize();
    }
}
