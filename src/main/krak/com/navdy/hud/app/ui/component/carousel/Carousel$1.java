package com.navdy.hud.app.ui.component.carousel;

class Carousel$1 {
    final static int[] $SwitchMap$com$navdy$hud$app$ui$component$carousel$Carousel$ViewType;
    
    static {
        $SwitchMap$com$navdy$hud$app$ui$component$carousel$Carousel$ViewType = new int[com.navdy.hud.app.ui.component.carousel.Carousel$ViewType.values().length];
        int[] a = $SwitchMap$com$navdy$hud$app$ui$component$carousel$Carousel$ViewType;
        com.navdy.hud.app.ui.component.carousel.Carousel$ViewType a0 = com.navdy.hud.app.ui.component.carousel.Carousel$ViewType.SIDE;
        try {
            a[a0.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$ui$component$carousel$Carousel$ViewType[com.navdy.hud.app.ui.component.carousel.Carousel$ViewType.MIDDLE_LEFT.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException0) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$ui$component$carousel$Carousel$ViewType[com.navdy.hud.app.ui.component.carousel.Carousel$ViewType.MIDDLE_RIGHT.ordinal()] = 3;
        } catch(NoSuchFieldError ignoredException1) {
        }
    }
}
