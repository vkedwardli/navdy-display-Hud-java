package com.navdy.hud.app.ui.component;

public class FluctuatorAnimatorView extends android.view.View {
    private android.animation.ValueAnimator alphaAnimator;
    private int animationDelay;
    private int animationDuration;
    private com.navdy.hud.app.ui.framework.DefaultAnimationListener animationListener;
    private android.animation.AnimatorSet animatorSet;
    float currentCircle;
    private float endRadius;
    private int fillColor;
    private boolean fillEnabled;
    private android.os.Handler handler;
    private android.view.animation.LinearInterpolator interpolator;
    private android.graphics.Paint paint;
    private android.animation.ValueAnimator radiusAnimator;
    private float startRadius;
    public Runnable startRunnable;
    boolean started;
    private int strokeColor;
    private float strokeWidth;
    
    public FluctuatorAnimatorView(android.content.Context a) {
        this(a, (android.util.AttributeSet)null);
    }
    
    public FluctuatorAnimatorView(android.content.Context a, android.util.AttributeSet a0) {
        this(a, a0, 0);
    }
    
    public FluctuatorAnimatorView(android.content.Context a, android.util.AttributeSet a0, int i) {
        super(a, a0, i);
        this.interpolator = new android.view.animation.LinearInterpolator();
        this.handler = new android.os.Handler(android.os.Looper.getMainLooper());
        this.animationListener = new com.navdy.hud.app.ui.component.FluctuatorAnimatorView$1(this);
        this.startRunnable = (Runnable)new com.navdy.hud.app.ui.component.FluctuatorAnimatorView$2(this);
        this.started = false;
        android.content.res.TypedArray a1 = a.obtainStyledAttributes(a0, com.navdy.hud.app.R$styleable.FluctuatorAnimatorView, i, 0);
        if (a1 != null) {
            this.strokeColor = a1.getColor(0, -1);
            this.fillColor = a1.getColor(1, -1);
            this.startRadius = a1.getDimension(2, 0.0f);
            this.endRadius = a1.getDimension(3, 0.0f);
            this.strokeWidth = a1.getDimension(4, 0.0f);
            this.animationDuration = a1.getInteger(5, 0);
            this.animationDelay = a1.getInteger(6, 0);
            a1.recycle();
        }
        this.paint = new android.graphics.Paint();
        this.paint.setStrokeWidth(this.strokeWidth);
        this.paint.setAntiAlias(true);
        this.animatorSet = new android.animation.AnimatorSet();
        this.animatorSet.setDuration((long)this.animationDuration);
        float[] a2 = new float[2];
        a2[0] = this.startRadius;
        a2[1] = this.endRadius;
        this.radiusAnimator = android.animation.ValueAnimator.ofFloat(a2);
        float[] a3 = new float[2];
        a3[0] = 1f;
        a3[1] = 0.0f;
        this.alphaAnimator = android.animation.ValueAnimator.ofFloat(a3);
        this.animatorSet.setInterpolator((android.animation.TimeInterpolator)this.interpolator);
        this.radiusAnimator.addUpdateListener((android.animation.ValueAnimator$AnimatorUpdateListener)new com.navdy.hud.app.ui.component.FluctuatorAnimatorView$3(this));
        this.alphaAnimator.addUpdateListener((android.animation.ValueAnimator$AnimatorUpdateListener)new com.navdy.hud.app.ui.component.FluctuatorAnimatorView$4(this));
        android.animation.AnimatorSet a4 = this.animatorSet;
        android.animation.Animator[] a5 = new android.animation.Animator[2];
        a5[0] = this.radiusAnimator;
        a5[1] = this.alphaAnimator;
        a4.playTogether(a5);
    }
    
    static int access$000(com.navdy.hud.app.ui.component.FluctuatorAnimatorView a) {
        return a.animationDelay;
    }
    
    static android.os.Handler access$100(com.navdy.hud.app.ui.component.FluctuatorAnimatorView a) {
        return a.handler;
    }
    
    static android.animation.AnimatorSet access$200(com.navdy.hud.app.ui.component.FluctuatorAnimatorView a) {
        return a.animatorSet;
    }
    
    public boolean isStarted() {
        return this.started;
    }
    
    public void onDraw(android.graphics.Canvas a) {
        super.onDraw(a);
        int i = this.fillColor;
        label0: {
            label1: {
                if (i != -1) {
                    break label1;
                }
                if (!this.fillEnabled) {
                    break label0;
                }
            }
            this.paint.setStyle(android.graphics.Paint$Style.FILL);
            this.paint.setColor(this.fillColor);
            a.drawCircle((float)(this.getWidth() / 2), (float)(this.getHeight() / 2), this.currentCircle, this.paint);
        }
        this.paint.setStyle(android.graphics.Paint$Style.STROKE);
        this.paint.setColor(this.strokeColor);
        a.drawCircle((float)(this.getWidth() / 2), (float)(this.getHeight() / 2), this.currentCircle, this.paint);
    }
    
    public void setFillColor(int i) {
        this.fillColor = i;
        this.fillEnabled = true;
    }
    
    public void setStrokeColor(int i) {
        this.strokeColor = i;
    }
    
    public void start() {
        this.stop();
        this.animatorSet.addListener((android.animation.Animator$AnimatorListener)this.animationListener);
        this.animatorSet.start();
        this.started = true;
    }
    
    public void stop() {
        this.handler.removeCallbacks(this.startRunnable);
        if (this.animatorSet.isRunning()) {
            this.animatorSet.removeAllListeners();
            this.animatorSet.cancel();
        }
        this.started = false;
    }
}
