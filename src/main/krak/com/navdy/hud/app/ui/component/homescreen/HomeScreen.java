package com.navdy.hud.app.ui.component.homescreen;

@Layout(R.layout.screen_home)
public class HomeScreen extends com.navdy.hud.app.screen.BaseScreen {
    private static com.navdy.hud.app.ui.component.homescreen.HomeScreen$Presenter presenter;
    
    public HomeScreen() {
    }
    
    static com.navdy.hud.app.ui.component.homescreen.HomeScreen$Presenter access$002(com.navdy.hud.app.ui.component.homescreen.HomeScreen$Presenter a) {
        presenter = a;
        return a;
    }
    
    public int getAnimationIn(flow.Flow$Direction a) {
        return 17432576;
    }
    
    public int getAnimationOut(flow.Flow$Direction a) {
        return 17432577;
    }
    
    public Object getDaggerModule() {
        return new com.navdy.hud.app.ui.component.homescreen.HomeScreen$Module(this);
    }
    
    public com.navdy.hud.app.ui.component.homescreen.HomeScreen$DisplayMode getDisplayMode() {
        com.navdy.hud.app.ui.component.homescreen.HomeScreen$DisplayMode a = (presenter == null) ? null : presenter.getDisplayMode();
        return a;
    }
    
    public com.navdy.hud.app.ui.component.homescreen.HomeScreenView getHomeScreenView() {
        com.navdy.hud.app.ui.component.homescreen.HomeScreenView a = (presenter == null) ? null : presenter.getHomeScreenView();
        return a;
    }
    
    public String getMortarScopeName() {
        return (this).getClass().getName();
    }
    
    public com.navdy.service.library.events.ui.Screen getScreen() {
        return com.navdy.service.library.events.ui.Screen.SCREEN_HOME;
    }
    
    public void setDisplayMode(com.navdy.hud.app.ui.component.homescreen.HomeScreen$DisplayMode a) {
        if (presenter != null) {
            presenter.setDisplayMode(a);
        }
    }
}
