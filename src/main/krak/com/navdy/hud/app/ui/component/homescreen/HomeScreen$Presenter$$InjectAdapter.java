package com.navdy.hud.app.ui.component.homescreen;

final public class HomeScreen$Presenter$$InjectAdapter extends dagger.internal.Binding implements javax.inject.Provider, dagger.MembersInjector {
    private dagger.internal.Binding bus;
    private dagger.internal.Binding supertype;
    
    public HomeScreen$Presenter$$InjectAdapter() {
        super("com.navdy.hud.app.ui.component.homescreen.HomeScreen$Presenter", "members/com.navdy.hud.app.ui.component.homescreen.HomeScreen$Presenter", true, com.navdy.hud.app.ui.component.homescreen.HomeScreen$Presenter.class);
    }
    
    public void attach(dagger.internal.Linker a) {
        this.bus = a.requestBinding("com.squareup.otto.Bus", com.navdy.hud.app.ui.component.homescreen.HomeScreen$Presenter.class, (this).getClass().getClassLoader());
        this.supertype = a.requestBinding("members/com.navdy.hud.app.ui.framework.BasePresenter", com.navdy.hud.app.ui.component.homescreen.HomeScreen$Presenter.class, (this).getClass().getClassLoader(), false, true);
    }
    
    public com.navdy.hud.app.ui.component.homescreen.HomeScreen$Presenter get() {
        com.navdy.hud.app.ui.component.homescreen.HomeScreen$Presenter a = new com.navdy.hud.app.ui.component.homescreen.HomeScreen$Presenter();
        this.injectMembers(a);
        return a;
    }
    
    public Object get() {
        return this.get();
    }
    
    public void getDependencies(java.util.Set a, java.util.Set a0) {
        a0.add(this.bus);
        a0.add(this.supertype);
    }
    
    public void injectMembers(com.navdy.hud.app.ui.component.homescreen.HomeScreen$Presenter a) {
        a.bus = (com.squareup.otto.Bus)this.bus.get();
        this.supertype.injectMembers(a);
    }
    
    public void injectMembers(Object a) {
        this.injectMembers((com.navdy.hud.app.ui.component.homescreen.HomeScreen$Presenter)a);
    }
}
