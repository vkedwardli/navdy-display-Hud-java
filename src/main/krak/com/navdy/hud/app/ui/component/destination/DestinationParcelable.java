package com.navdy.hud.app.ui.component.destination;

public class DestinationParcelable implements android.os.Parcelable {
    final public static android.os.Parcelable$Creator CREATOR;
    final private static String EMPTY = "";
    java.util.List contacts;
    String destinationAddress;
    String destinationPlaceId;
    String destinationSubTitle;
    String destinationSubTitle2;
    boolean destinationSubTitle2Formatted;
    boolean destinationSubTitleFormatted;
    String destinationTitle;
    double displayLatitude;
    double displayLongitude;
    public String distanceStr;
    int icon;
    int iconDeselectedColor;
    int iconSelectedColor;
    int iconUnselected;
    int id;
    String identifier;
    double navLatitude;
    double navLongitude;
    java.util.List phoneNumbers;
    com.navdy.service.library.events.places.PlaceType placeType;
    String routeId;
    com.navdy.hud.app.ui.component.destination.DestinationParcelable$DestinationType type;
    
    static {
        CREATOR = (android.os.Parcelable$Creator)new com.navdy.hud.app.ui.component.destination.DestinationParcelable$1();
    }
    
    public DestinationParcelable(int i, String s, String s0, boolean b, String s1, boolean b0, String s2, double d, double d0, double d1, double d2, int i0, int i1, int i2, int i3, com.navdy.hud.app.ui.component.destination.DestinationParcelable$DestinationType a, com.navdy.service.library.events.places.PlaceType a0) {
        this.id = i;
        this.destinationTitle = s;
        this.destinationSubTitle = s0;
        this.destinationSubTitleFormatted = b;
        this.destinationSubTitle2 = s1;
        this.destinationSubTitle2Formatted = b0;
        this.destinationAddress = s2;
        this.navLatitude = d;
        this.navLongitude = d0;
        this.displayLatitude = d1;
        this.displayLongitude = d2;
        this.icon = i0;
        this.iconUnselected = i1;
        this.iconSelectedColor = i2;
        this.iconDeselectedColor = i3;
        this.type = a;
        this.placeType = a0;
    }
    
    protected DestinationParcelable(android.os.Parcel a) {
        this.destinationTitle = a.readString();
        this.destinationSubTitle = a.readString();
        int i = a.readByte();
        this.destinationSubTitleFormatted = i == 1;
        this.destinationSubTitle2 = a.readString();
        int i0 = a.readByte();
        this.destinationSubTitle2Formatted = i0 == 1;
        this.destinationAddress = a.readString();
        this.navLatitude = a.readDouble();
        this.navLongitude = a.readDouble();
        this.displayLatitude = a.readDouble();
        this.displayLongitude = a.readDouble();
        this.icon = a.readInt();
        this.iconUnselected = a.readInt();
        this.iconSelectedColor = a.readInt();
        this.iconDeselectedColor = a.readInt();
        this.routeId = a.readString();
        this.destinationPlaceId = a.readString();
        this.type = com.navdy.hud.app.ui.component.destination.DestinationParcelable$DestinationType.values()[a.readInt()];
        this.id = a.readInt();
        this.placeType = com.navdy.service.library.events.places.PlaceType.values()[a.readInt()];
        this.contacts = (java.util.List)a.readArrayList((ClassLoader)null);
        this.phoneNumbers = (java.util.List)a.readArrayList((ClassLoader)null);
        this.identifier = a.readString();
    }
    
    public int describeContents() {
        return 0;
    }
    
    public void setContacts(java.util.List a) {
        this.contacts = a;
    }
    
    public void setIdentifier(String s) {
        this.identifier = s;
    }
    
    public void setPhoneNumbers(java.util.List a) {
        this.phoneNumbers = a;
    }
    
    public void setPlaceId(String s) {
        this.destinationPlaceId = s;
    }
    
    public void setRouteId(String s) {
        this.routeId = s;
    }
    
    public void writeToParcel(android.os.Parcel a, int i) {
        a.writeString((this.destinationTitle != null) ? this.destinationTitle : "");
        a.writeString((this.destinationSubTitle != null) ? this.destinationSubTitle : "");
        a.writeByte((byte)this.destinationSubTitleFormatted);
        a.writeString((this.destinationSubTitle2 != null) ? this.destinationSubTitle2 : "");
        a.writeByte((byte)this.destinationSubTitle2Formatted);
        a.writeString((this.destinationAddress != null) ? this.destinationAddress : "");
        a.writeDouble(this.navLatitude);
        a.writeDouble(this.navLongitude);
        a.writeDouble(this.displayLatitude);
        a.writeDouble(this.displayLongitude);
        a.writeInt(this.icon);
        a.writeInt(this.iconUnselected);
        a.writeInt(this.iconSelectedColor);
        a.writeInt(this.iconDeselectedColor);
        a.writeString((this.routeId != null) ? this.routeId : "");
        a.writeString((this.destinationPlaceId != null) ? this.routeId : "");
        a.writeInt(this.type.ordinal());
        a.writeInt(this.id);
        a.writeInt(this.placeType.ordinal());
        a.writeList(this.contacts);
        a.writeList(this.phoneNumbers);
        a.writeString(this.identifier);
    }
}
