package com.navdy.hud.app.ui.component.carousel;


public enum Carousel$ViewType {
    SIDE(0),
    MIDDLE_LEFT(1),
    MIDDLE_RIGHT(2);

    private int value;
    Carousel$ViewType(int value) {
        this.value = value;
    }
    public int getValue() {
        return value;
    }
}

//final public class Carousel$ViewType extends Enum {
//    final private static com.navdy.hud.app.ui.component.carousel.Carousel$ViewType[] $VALUES;
//    final public static com.navdy.hud.app.ui.component.carousel.Carousel$ViewType MIDDLE_LEFT;
//    final public static com.navdy.hud.app.ui.component.carousel.Carousel$ViewType MIDDLE_RIGHT;
//    final public static com.navdy.hud.app.ui.component.carousel.Carousel$ViewType SIDE;
//    
//    static {
//        SIDE = new com.navdy.hud.app.ui.component.carousel.Carousel$ViewType("SIDE", 0);
//        MIDDLE_LEFT = new com.navdy.hud.app.ui.component.carousel.Carousel$ViewType("MIDDLE_LEFT", 1);
//        MIDDLE_RIGHT = new com.navdy.hud.app.ui.component.carousel.Carousel$ViewType("MIDDLE_RIGHT", 2);
//        com.navdy.hud.app.ui.component.carousel.Carousel$ViewType[] a = new com.navdy.hud.app.ui.component.carousel.Carousel$ViewType[3];
//        a[0] = SIDE;
//        a[1] = MIDDLE_LEFT;
//        a[2] = MIDDLE_RIGHT;
//        $VALUES = a;
//    }
//    
//    private Carousel$ViewType(String s, int i) {
//        super(s, i);
//    }
//    
//    public static com.navdy.hud.app.ui.component.carousel.Carousel$ViewType valueOf(String s) {
//        return (com.navdy.hud.app.ui.component.carousel.Carousel$ViewType)Enum.valueOf(com.navdy.hud.app.ui.component.carousel.Carousel$ViewType.class, s);
//    }
//    
//    public static com.navdy.hud.app.ui.component.carousel.Carousel$ViewType[] values() {
//        return $VALUES.clone();
//    }
//}
//