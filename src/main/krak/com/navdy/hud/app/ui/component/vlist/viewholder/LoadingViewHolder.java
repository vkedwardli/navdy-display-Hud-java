package com.navdy.hud.app.ui.component.vlist.viewholder;
import com.navdy.hud.app.R;

public class LoadingViewHolder extends com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder {
    final private static int LOADING_ANIMATION_DELAY = 33;
    final private static int LOADING_ANIMATION_DURATION = 500;
    final private static int loadingImageSize;
    final private static int loadingImageSizeUnselected;
    final private static com.navdy.service.library.log.Logger sLogger;
    private android.animation.ObjectAnimator loadingAnimator;
    private android.widget.ImageView loadingImage;
    
    static {
        sLogger = com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.sLogger;
        android.content.res.Resources a = com.navdy.hud.app.HudApplication.getAppContext().getResources();
        loadingImageSize = a.getDimensionPixelSize(R.dimen.vlist_loading_image);
        loadingImageSizeUnselected = a.getDimensionPixelSize(R.dimen.vlist_loading_image_unselected);
    }
    
    public LoadingViewHolder(android.view.ViewGroup a, com.navdy.hud.app.ui.component.vlist.VerticalList a0, android.os.Handler a1) {
        super(a, a0, a1);
        this.loadingImage = (android.widget.ImageView)a.findViewById(R.id.loading);
    }
    
    static android.animation.ObjectAnimator access$000(com.navdy.hud.app.ui.component.vlist.viewholder.LoadingViewHolder a) {
        return a.loadingAnimator;
    }
    
    static com.navdy.service.library.log.Logger access$100() {
        return sLogger;
    }
    
    public static com.navdy.hud.app.ui.component.vlist.VerticalList$Model buildModel() {
        com.navdy.hud.app.ui.component.vlist.VerticalList$Model a = new com.navdy.hud.app.ui.component.vlist.VerticalList$Model();
        a.type = com.navdy.hud.app.ui.component.vlist.VerticalList$ModelType.LOADING;
        a.id = R.id.vlist_loading;
        return a;
    }
    
    public static com.navdy.hud.app.ui.component.vlist.viewholder.LoadingViewHolder buildViewHolder(android.view.ViewGroup a, com.navdy.hud.app.ui.component.vlist.VerticalList a0, android.os.Handler a1) {
        return new com.navdy.hud.app.ui.component.vlist.viewholder.LoadingViewHolder((android.view.ViewGroup)android.view.LayoutInflater.from(a.getContext()).inflate(R.layout.vlist_loading_item, a, false), a0, a1);
    }
    
    private void startLoadingAnimation() {
        if (this.loadingAnimator == null) {
            android.widget.ImageView a = this.loadingImage;
            android.util.Property a0 = android.view.View.ROTATION;
            float[] a1 = new float[1];
            a1[0] = 360f;
            this.loadingAnimator = android.animation.ObjectAnimator.ofFloat(a, a0, a1);
            this.loadingAnimator.setDuration(500L);
            this.loadingAnimator.setInterpolator((android.animation.TimeInterpolator)new android.view.animation.AccelerateDecelerateInterpolator());
            this.loadingAnimator.addListener((android.animation.Animator$AnimatorListener)new com.navdy.hud.app.ui.component.vlist.viewholder.LoadingViewHolder$1(this));
        }
        sLogger.v("started loading animation");
        this.loadingAnimator.start();
    }
    
    private void stopLoadingAnimation() {
        if (this.loadingAnimator != null) {
            sLogger.v("cancelled loading animation");
            this.loadingAnimator.removeAllListeners();
            this.loadingAnimator.cancel();
            this.loadingImage.setRotation(0.0f);
            this.loadingAnimator = null;
        }
    }
    
    public void bind(com.navdy.hud.app.ui.component.vlist.VerticalList$Model a, com.navdy.hud.app.ui.component.vlist.VerticalList$ModelState a0) {
    }
    
    public void clearAnimation() {
        this.stopLoadingAnimation();
    }
    
    public void copyAndPosition(android.widget.ImageView a, android.widget.TextView a0, android.widget.TextView a1, android.widget.TextView a2, boolean b) {
    }
    
    public com.navdy.hud.app.ui.component.vlist.VerticalList$ModelType getModelType() {
        return com.navdy.hud.app.ui.component.vlist.VerticalList$ModelType.LOADING;
    }
    
    public void select(com.navdy.hud.app.ui.component.vlist.VerticalList$Model a, int i, int i0) {
    }
    
    public void setItemState(com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$State a, com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$AnimationType a0, int i, boolean b) {
        int i0 = (a != com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$State.SELECTED) ? loadingImageSizeUnselected : loadingImageSize;
        switch(com.navdy.hud.app.ui.component.vlist.viewholder.LoadingViewHolder$2.$SwitchMap$com$navdy$hud$app$ui$component$vlist$viewholder$VerticalViewHolder$AnimationType[a0.ordinal()]) {
            case 3: {
                this.startLoadingAnimation();
                android.animation.Animator a1 = com.navdy.hud.app.ui.component.vmenu.VerticalAnimationUtils.animateDimension((android.view.View)this.loadingImage, i0);
                a1.setDuration((long)i);
                a1.setInterpolator((android.animation.TimeInterpolator)this.interpolator);
                a1.start();
                break;
            }
            case 1: case 2: {
                this.startLoadingAnimation();
                android.view.ViewGroup$MarginLayoutParams a2 = (android.view.ViewGroup$MarginLayoutParams)this.loadingImage.getLayoutParams();
                a2.width = i0;
                a2.height = i0;
                break;
            }
        }
    }
}
