package com.navdy.hud.app.ui.component.mainmenu;
import com.navdy.hud.app.R;

class MainMenu$3$1 implements Runnable {
    final com.navdy.hud.app.ui.component.mainmenu.MainMenu$3 this$1;
    final com.navdy.service.library.events.audio.MusicTrackInfo val$trackInfo;
    
    MainMenu$3$1(com.navdy.hud.app.ui.component.mainmenu.MainMenu$3 a, com.navdy.service.library.events.audio.MusicTrackInfo a0) {
        super();
        this.this$1 = a;
        this.val$trackInfo = a0;
    }
    
    public void run() {
        com.navdy.hud.app.ui.component.mainmenu.MainMenu.access$802(this.this$1.this$0, this.val$trackInfo);
        if (com.navdy.hud.app.ui.component.mainmenu.MainMenu.access$1100(this.this$1.this$0) == R.id.main_menu_now_playing && com.navdy.hud.app.ui.component.mainmenu.MainMenu.access$400(this.this$1.this$0) >= 0) {
            com.navdy.hud.app.ui.component.mainmenu.MainMenu.access$1200().v("update music tool tip");
            String s = com.navdy.hud.app.ui.component.mainmenu.MainMenu.access$1300(this.this$1.this$0);
            if (s != null) {
                com.navdy.hud.app.ui.component.mainmenu.MainMenu.access$600(this.this$1.this$0).showToolTip(com.navdy.hud.app.ui.component.mainmenu.MainMenu.access$400(this.this$1.this$0), s);
            }
        }
    }
}
