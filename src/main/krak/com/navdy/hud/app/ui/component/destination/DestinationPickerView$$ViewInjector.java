package com.navdy.hud.app.ui.component.destination;
import com.navdy.hud.app.R;

public class DestinationPickerView$$ViewInjector {
    public DestinationPickerView$$ViewInjector() {
    }
    
    public static void inject(butterknife.ButterKnife$Finder a, com.navdy.hud.app.ui.component.destination.DestinationPickerView a0, Object a1) {
        a0.rightBackground = a.findRequiredView(a1, R.id.rightBackground, "field 'rightBackground'");
        a0.confirmationLayout = (com.navdy.hud.app.ui.component.ConfirmationLayout)a.findRequiredView(a1, R.id.confirmationLayout, "field 'confirmationLayout'");
    }
    
    public static void reset(com.navdy.hud.app.ui.component.destination.DestinationPickerView a) {
        a.rightBackground = null;
        a.confirmationLayout = null;
    }
}
