package com.navdy.hud.app.ui.component.tbt;

final public class TbtViewContainer$updateDisplay$1 extends com.navdy.hud.app.ui.framework.DefaultAnimationListener {
    final com.navdy.hud.app.ui.component.tbt.TbtViewContainer this$0;
    
    TbtViewContainer$updateDisplay$1(com.navdy.hud.app.ui.component.tbt.TbtViewContainer a) {
        super();
        this.this$0 = a;
    }
    
    public void onAnimationEnd(android.animation.Animator a) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a, "animation");
        com.navdy.hud.app.ui.component.tbt.TbtView a0 = com.navdy.hud.app.ui.component.tbt.TbtViewContainer.access$getInactiveView$p(this.this$0);
        if (a0 != null) {
            a0.setTranslationY(-com.navdy.hud.app.ui.component.tbt.TbtViewContainer.Companion.getItemHeight());
        }
    }
}
