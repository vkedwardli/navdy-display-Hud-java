package com.navdy.hud.app.ui.component.carousel;

class CarouselLayout$2 implements android.view.View$OnClickListener {
    final com.navdy.hud.app.ui.component.carousel.CarouselLayout this$0;
    
    CarouselLayout$2(com.navdy.hud.app.ui.component.carousel.CarouselLayout a) {
        super();
        this.this$0 = a;
    }
    
    public void onClick(android.view.View a) {
        android.graphics.Rect a0 = new android.graphics.Rect();
        int[] a1 = new int[2];
        int[] a2 = new int[2];
        this.this$0.getLocationInWindow(a1);
        android.view.View[] a3 = new android.view.View[3];
        a3[0] = this.this$0.leftView;
        a3[1] = this.this$0.middleLeftView;
        a3[2] = this.this$0.rightView;
        int i = 0;
        while(true) {
            if (i < a3.length) {
                android.view.View a4 = a3[i];
                ((android.view.View)a4.getParent()).getLocationInWindow(a2);
                int i0 = com.navdy.hud.app.ui.component.carousel.CarouselLayout.access$000(this.this$0);
                int i1 = a2[0];
                int i2 = a1[0];
                int i3 = com.navdy.hud.app.ui.component.carousel.CarouselLayout.access$100(this.this$0);
                int i4 = a2[1];
                int i5 = a1[1];
                a4.getHitRect(a0);
                if (!a0.contains(i0 - (i1 - i2), i3 - (i4 - i5))) {
                    i = i + 1;
                    continue;
                }
                if (i != 1) {
                    com.navdy.hud.app.ui.component.carousel.CarouselLayout.access$300(this.this$0, (android.animation.Animator$AnimatorListener)null, i > 1, true, false, com.navdy.hud.app.ui.component.carousel.CarouselLayout.access$200(this.this$0));
                } else {
                    this.this$0.selectItem();
                }
            }
            return;
        }
    }
}
