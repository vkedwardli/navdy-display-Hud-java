package com.navdy.hud.app.ui.component.mainmenu;
import com.navdy.hud.app.R;

class ContactOptionsMenu implements com.navdy.hud.app.ui.component.mainmenu.IMenu {
    final private static com.navdy.hud.app.ui.component.vlist.VerticalList$Model back;
    final private static int backColor;
    final private static int callColor;
    final private static com.navdy.service.library.log.Logger logger;
    final private static int messageColor;
    final private static android.content.res.Resources resources;
    final private static int shareLocationColor;
    final private static int shareTripLocationColor;
    private int backSelection;
    private int backSelectionId;
    final private com.squareup.otto.Bus bus;
    private java.util.List cachedList;
    final private java.util.List contacts;
    private boolean hasScrollModel;
    private com.navdy.hud.app.ui.component.mainmenu.MessagePickerMenu messagePickerMenu;
    final private String notifId;
    private com.navdy.hud.app.framework.contacts.Contact numberPicked;
    final private com.navdy.hud.app.ui.component.mainmenu.IMenu parent;
    final private com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2$Presenter presenter;
    final private com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent vscrollComponent;
    
    static {
        logger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.ui.component.mainmenu.ContactOptionsMenu.class);
        resources = com.navdy.hud.app.HudApplication.getAppContext().getResources();
        backColor = resources.getColor(R.color.mm_back);
        callColor = resources.getColor(R.color.mm_contacts);
        shareTripLocationColor = resources.getColor(R.color.mm_active_trip);
        shareLocationColor = resources.getColor(R.color.share_location);
        messageColor = resources.getColor(R.color.share_location);
        String s = resources.getString(R.string.back);
        int i = backColor;
        back = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(R.id.menu_back, R.drawable.icon_mm_back, i, com.navdy.hud.app.ui.component.mainmenu.MainMenu.bkColorUnselected, i, s, (String)null);
    }
    
    ContactOptionsMenu(java.util.List a, com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent a0, com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2$Presenter a1, com.navdy.hud.app.ui.component.mainmenu.IMenu a2, com.squareup.otto.Bus a3) {
        this(a, (String)null, a0, a1, a2, a3);
    }
    
    ContactOptionsMenu(java.util.List a, String s, com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent a0, com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2$Presenter a1, com.navdy.hud.app.ui.component.mainmenu.IMenu a2, com.squareup.otto.Bus a3) {
        this.contacts = a;
        this.notifId = s;
        this.vscrollComponent = a0;
        this.presenter = a1;
        this.parent = a2;
        this.bus = a3;
    }
    
    static void access$000(com.navdy.hud.app.ui.component.mainmenu.ContactOptionsMenu a, com.navdy.hud.app.framework.contacts.Contact a0) {
        a.makeCall(a0);
    }
    
    static com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2$Presenter access$100(com.navdy.hud.app.ui.component.mainmenu.ContactOptionsMenu a) {
        return a.presenter;
    }
    
    static void access$200(com.navdy.hud.app.ui.component.mainmenu.ContactOptionsMenu a, com.navdy.hud.app.framework.contacts.Contact a0) {
        a.launchShareTripMenu(a0);
    }
    
    static String access$300(com.navdy.hud.app.ui.component.mainmenu.ContactOptionsMenu a) {
        return a.notifId;
    }
    
    static com.squareup.otto.Bus access$400(com.navdy.hud.app.ui.component.mainmenu.ContactOptionsMenu a) {
        return a.bus;
    }
    
    private String getContactName(com.navdy.hud.app.framework.contacts.Contact a) {
        return (android.text.TextUtils.isEmpty((CharSequence)a.name)) ? (android.text.TextUtils.isEmpty((CharSequence)a.formattedNumber)) ? "" : a.formattedNumber : a.name;
    }
    
    private com.navdy.hud.app.ui.component.destination.DestinationParcelable getDestinationParcelable(String s) {
        return new com.navdy.hud.app.ui.component.destination.DestinationParcelable(0, s, (String)null, false, (String)null, false, (String)null, 0.0, 0.0, 0.0, 0.0, R.drawable.icon_message, 0, shareTripLocationColor, com.navdy.hud.app.ui.component.mainmenu.MainMenu.bkColorUnselected, com.navdy.hud.app.ui.component.destination.DestinationParcelable$DestinationType.NONE, (com.navdy.service.library.events.places.PlaceType)null);
    }
    
    private void launchShareTripMenu(com.navdy.hud.app.framework.contacts.Contact a) {
        android.os.Bundle a0 = new android.os.Bundle();
        a0.putInt("PICKER_DESTINATION_ICON", R.drawable.icon_pin_dot_destination_blue);
        a0.putInt("PICKER_LEFT_ICON", R.drawable.icon_message);
        a0.putInt("PICKER_LEFT_ICON_BKCOLOR", resources.getColor(R.color.share_location_trip_color));
        com.here.android.mpa.common.GeoCoordinate a1 = com.navdy.hud.app.maps.here.HereMapsManager.getInstance().getLocationFixManager().getLastGeoCoordinate();
        com.navdy.service.library.events.navigation.NavigationRouteRequest a2 = com.navdy.hud.app.maps.here.HereNavigationManager.getInstance().getCurrentNavigationRouteRequest();
        com.navdy.service.library.events.location.Coordinate a3 = null;
        if (a2 != null) {
            a3 = a2.destination;
        }
        a0.putBoolean("PICKER_SHOW_ROUTE_MAP", true);
        if (a1 != null) {
            a0.putDouble("PICKER_MAP_START_LAT", a1.getLatitude());
            a0.putDouble("PICKER_MAP_START_LNG", a1.getLongitude());
        }
        String s = this.getContactName(a);
        if (a3 == null) {
            android.content.res.Resources a4 = resources;
            Object[] a5 = new Object[1];
            a5[0] = s;
            a0.putString("PICKER_TITLE", a4.getString(R.string.share_your_location_with, a5));
        } else {
            a0.putDouble("PICKER_MAP_END_LAT", a3.latitude.doubleValue());
            a0.putDouble("PICKER_MAP_END_LNG", a3.longitude.doubleValue());
            android.content.res.Resources a6 = resources;
            Object[] a7 = new Object[1];
            a7[0] = s;
            a0.putString("PICKER_TITLE", a6.getString(R.string.share_your_trip_with, a7));
        }
        String[] a8 = com.navdy.hud.app.framework.glympse.GlympseManager.getInstance().getMessages();
        com.navdy.hud.app.ui.component.destination.DestinationParcelable[] a9 = new com.navdy.hud.app.ui.component.destination.DestinationParcelable[a8.length];
        String s0 = com.navdy.hud.app.maps.here.HereNavigationManager.getInstance().getCurrentRouteId();
        int i = 0;
        while(i < a8.length) {
            a9[i] = this.getDestinationParcelable(a8[i]);
            if (s0 != null) {
                a9[i].setRouteId(s0);
            }
            i = i + 1;
        }
        a0.putParcelableArray("PICKER_DESTINATIONS", (android.os.Parcelable[])a9);
        this.presenter.close((Runnable)new com.navdy.hud.app.ui.component.mainmenu.ContactOptionsMenu$6(this, a0, a));
    }
    
    private void makeCall(com.navdy.hud.app.framework.contacts.Contact a) {
        this.removeNotification();
        com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getCallManager().dial(a.number, (String)null, a.name);
    }
    
    private void removeNotification() {
        if (this.notifId != null) {
            com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().removeNotification(this.notifId);
        }
    }
    
    public com.navdy.hud.app.ui.component.mainmenu.IMenu getChildMenu(com.navdy.hud.app.ui.component.mainmenu.IMenu a, String s, String s0) {
        return null;
    }
    
    public int getInitialSelection() {
        return (this.cachedList == null) ? 0 : (this.cachedList.isEmpty()) ? 0 : (this.notifId != null) ? 0 : 1;
    }
    
    public java.util.List getItems() {
        Object a = null;
        if (this.cachedList == null) {
            boolean b = false;
            java.util.ArrayList a0 = new java.util.ArrayList();
            if (this.notifId != null) {
                ((java.util.List)a0).add(com.navdy.hud.app.ui.component.vlist.viewholder.TitleViewHolder.buildModel(resources.getString(R.string.pick_reply)));
            } else {
                ((java.util.List)a0).add(back);
            }
            String s = resources.getString(R.string.call);
            ((java.util.List)a0).add(com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(R.id.menu_call, R.drawable.icon_mm_contacts_2, callColor, com.navdy.hud.app.ui.component.mainmenu.MainMenu.bkColorUnselected, callColor, s, (String)null));
            boolean b0 = com.navdy.hud.app.framework.glympse.GlympseManager.getInstance().isSynced();
            logger.v(new StringBuilder().append("glympse synced:").append(b0).toString());
            if (com.navdy.hud.app.maps.here.HereMapsManager.getInstance().isInitialized() && b0) {
                if (com.navdy.hud.app.maps.here.HereNavigationManager.getInstance().isNavigationModeOn()) {
                    String s0 = resources.getString(R.string.share_trip);
                    ((java.util.List)a0).add(com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(R.id.menu_share_trip, R.drawable.icon_badge_active_trip, shareTripLocationColor, com.navdy.hud.app.ui.component.mainmenu.MainMenu.bkColorUnselected, shareTripLocationColor, s0, (String)null));
                } else {
                    String s1 = resources.getString(R.string.share_location);
                    ((java.util.List)a0).add(com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(R.id.menu_share_location, R.drawable.icon_share_location, shareLocationColor, com.navdy.hud.app.ui.component.mainmenu.MainMenu.bkColorUnselected, shareLocationColor, s1, (String)null));
                }
            }
            com.navdy.service.library.events.DeviceInfo a1 = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getRemoteDeviceInfo();
            if (a1 == null) {
                b = false;
            } else {
                switch(com.navdy.hud.app.ui.component.mainmenu.ContactOptionsMenu$7.$SwitchMap$com$navdy$service$library$events$DeviceInfo$Platform[a1.platform.ordinal()]) {
                    case 2: {
                        b = com.navdy.hud.app.ui.component.UISettings.supportsIosSms();
                        break;
                    }
                    case 1: {
                        b = true;
                        break;
                    }
                    default: {
                        b = false;
                    }
                }
            }
            if (b) {
                String s2 = resources.getString(R.string.message);
                ((java.util.List)a0).add(com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(R.id.menu_message, R.drawable.icon_message, messageColor, com.navdy.hud.app.ui.component.mainmenu.MainMenu.bkColorUnselected, messageColor, s2, (String)null));
            }
            this.cachedList = (java.util.List)a0;
            a = a0;
        } else {
            a = this.cachedList;
        }
        return (java.util.List)a;
    }
    
    public com.navdy.hud.app.ui.component.vlist.VerticalList$Model getModelfromPos(int i) {
        com.navdy.hud.app.ui.component.vlist.VerticalList$Model a = null;
        java.util.List a0 = this.cachedList;
        label2: {
            label0: {
                label1: {
                    if (a0 == null) {
                        break label1;
                    }
                    if (this.cachedList.size() > i) {
                        break label0;
                    }
                }
                a = null;
                break label2;
            }
            a = (com.navdy.hud.app.ui.component.vlist.VerticalList$Model)this.cachedList.get(i);
        }
        return a;
    }
    
    public com.navdy.hud.app.ui.component.vmenu.VerticalFastScrollIndex getScrollIndex() {
        return null;
    }
    
    public com.navdy.hud.app.ui.component.mainmenu.IMenu$Menu getType() {
        return com.navdy.hud.app.ui.component.mainmenu.IMenu$Menu.CONTACT_OPTIONS;
    }
    
    public boolean isBindCallsEnabled() {
        return false;
    }
    
    public boolean isFirstItemEmpty() {
        return this.notifId == null;
    }
    
    public boolean isItemClickable(int i, int i0) {
        return true;
    }
    
    public void onBindToView(com.navdy.hud.app.ui.component.vlist.VerticalList$Model a, android.view.View a0, int i, com.navdy.hud.app.ui.component.vlist.VerticalList$ModelState a1) {
    }
    
    public void onFastScrollEnd() {
    }
    
    public void onFastScrollStart() {
    }
    
    public void onItemSelected(com.navdy.hud.app.ui.component.vlist.VerticalList$ItemSelectionState a) {
    }
    
    public void onScrollIdle() {
    }
    
    public void onUnload(com.navdy.hud.app.ui.component.mainmenu.IMenu$MenuLevel a) {
    }
    
    public boolean selectItem(com.navdy.hud.app.ui.component.vlist.VerticalList$ItemSelectionState a) {
        logger.v(new StringBuilder().append("select id:").append(a.id).append(" pos:").append(a.pos).toString());
        int dummy = a.pos;
        com.navdy.hud.app.framework.contacts.Contact a0 = (com.navdy.hud.app.framework.contacts.Contact)this.contacts.get(0);
        switch(a.id) {
            case R.id.menu_share_trip: {
                logger.v("share trip");
                com.navdy.hud.app.analytics.AnalyticsSupport.recordMenuSelection("share_trip");
                if (this.contacts.size() != 1) {
                    com.navdy.hud.app.ui.component.mainmenu.NumberPickerMenu a1 = new com.navdy.hud.app.ui.component.mainmenu.NumberPickerMenu(this.vscrollComponent, this.presenter, (com.navdy.hud.app.ui.component.mainmenu.IMenu)this, com.navdy.hud.app.ui.component.mainmenu.NumberPickerMenu$Mode.SHARE_TRIP, this.contacts, (String)null, R.drawable.icon_badge_active_trip, shareTripLocationColor, (com.navdy.hud.app.ui.component.mainmenu.NumberPickerMenu$Callback)new com.navdy.hud.app.ui.component.mainmenu.ContactOptionsMenu$3(this));
                    this.presenter.loadMenu((com.navdy.hud.app.ui.component.mainmenu.IMenu)a1, com.navdy.hud.app.ui.component.mainmenu.IMenu$MenuLevel.SUB_LEVEL, a.pos, 0);
                    break;
                } else {
                    this.launchShareTripMenu(a0);
                    break;
                }
            }
            case R.id.menu_share_location: {
                logger.v("share location");
                com.navdy.hud.app.analytics.AnalyticsSupport.recordMenuSelection("share_location");
                if (this.contacts.size() != 1) {
                    com.navdy.hud.app.ui.component.mainmenu.NumberPickerMenu a2 = new com.navdy.hud.app.ui.component.mainmenu.NumberPickerMenu(this.vscrollComponent, this.presenter, (com.navdy.hud.app.ui.component.mainmenu.IMenu)this, com.navdy.hud.app.ui.component.mainmenu.NumberPickerMenu$Mode.SHARE_TRIP, this.contacts, (String)null, R.drawable.icon_share_location, shareLocationColor, (com.navdy.hud.app.ui.component.mainmenu.NumberPickerMenu$Callback)new com.navdy.hud.app.ui.component.mainmenu.ContactOptionsMenu$4(this));
                    this.presenter.loadMenu((com.navdy.hud.app.ui.component.mainmenu.IMenu)a2, com.navdy.hud.app.ui.component.mainmenu.IMenu$MenuLevel.SUB_LEVEL, a.pos, 0);
                    break;
                } else {
                    this.launchShareTripMenu(a0);
                    break;
                }
            }
            case R.id.menu_message: {
                logger.v("send message");
                com.navdy.hud.app.analytics.AnalyticsSupport.recordMenuSelection("send_message");
                if (this.contacts.size() != 1) {
                    com.navdy.hud.app.ui.component.mainmenu.NumberPickerMenu a3 = new com.navdy.hud.app.ui.component.mainmenu.NumberPickerMenu(this.vscrollComponent, this.presenter, (com.navdy.hud.app.ui.component.mainmenu.IMenu)this, com.navdy.hud.app.ui.component.mainmenu.NumberPickerMenu$Mode.MESSAGE, this.contacts, this.notifId, R.drawable.icon_message, messageColor, (com.navdy.hud.app.ui.component.mainmenu.NumberPickerMenu$Callback)new com.navdy.hud.app.ui.component.mainmenu.ContactOptionsMenu$5(this));
                    this.presenter.loadMenu((com.navdy.hud.app.ui.component.mainmenu.IMenu)a3, com.navdy.hud.app.ui.component.mainmenu.IMenu$MenuLevel.SUB_LEVEL, a.pos, 0);
                    break;
                } else {
                    if (this.messagePickerMenu == null) {
                        this.messagePickerMenu = new com.navdy.hud.app.ui.component.mainmenu.MessagePickerMenu(this.vscrollComponent, this.presenter, (com.navdy.hud.app.ui.component.mainmenu.IMenu)this, com.navdy.hud.app.framework.glance.GlanceConstants.getCannedMessages(), a0, this.notifId);
                    }
                    this.presenter.loadMenu((com.navdy.hud.app.ui.component.mainmenu.IMenu)this.messagePickerMenu, com.navdy.hud.app.ui.component.mainmenu.IMenu$MenuLevel.SUB_LEVEL, a.pos, 0);
                    break;
                }
            }
            case R.id.menu_call: {
                logger.v("call contact");
                if (this.contacts.size() != 1) {
                    com.navdy.hud.app.ui.component.mainmenu.NumberPickerMenu a4 = new com.navdy.hud.app.ui.component.mainmenu.NumberPickerMenu(this.vscrollComponent, this.presenter, (com.navdy.hud.app.ui.component.mainmenu.IMenu)this, com.navdy.hud.app.ui.component.mainmenu.NumberPickerMenu$Mode.CALL, this.contacts, (String)null, R.drawable.icon_mm_contacts_2, callColor, (com.navdy.hud.app.ui.component.mainmenu.NumberPickerMenu$Callback)new com.navdy.hud.app.ui.component.mainmenu.ContactOptionsMenu$2(this));
                    this.presenter.loadMenu((com.navdy.hud.app.ui.component.mainmenu.IMenu)a4, com.navdy.hud.app.ui.component.mainmenu.IMenu$MenuLevel.SUB_LEVEL, a.pos, 0);
                    break;
                } else {
                    com.navdy.hud.app.analytics.AnalyticsSupport.recordMenuSelection("call_contact");
                    this.presenter.performSelectionAnimation((Runnable)new com.navdy.hud.app.ui.component.mainmenu.ContactOptionsMenu$1(this, a0));
                    break;
                }
            }
            case R.id.menu_back: {
                logger.v("back");
                com.navdy.hud.app.analytics.AnalyticsSupport.recordMenuSelection("back");
                this.presenter.loadMenu(this.parent, com.navdy.hud.app.ui.component.mainmenu.IMenu$MenuLevel.BACK_TO_PARENT, this.backSelection, this.backSelectionId, this.hasScrollModel);
                this.backSelectionId = 0;
                break;
            }
        }
        return true;
    }
    
    public void setBackSelectionId(int i) {
        this.backSelectionId = i;
    }
    
    public void setBackSelectionPos(int i) {
        this.backSelection = i;
    }
    
    public void setScrollModel(boolean b) {
        this.hasScrollModel = b;
    }
    
    public void setSelectedIcon() {
        com.navdy.hud.app.framework.contacts.Contact a = (com.navdy.hud.app.framework.contacts.Contact)this.contacts.get(0);
        if (this.contacts.size() != 1) {
            com.navdy.hud.app.framework.contacts.ContactImageHelper a0 = com.navdy.hud.app.framework.contacts.ContactImageHelper.getInstance();
            int i = a0.getContactImageIndex(a.name);
            this.vscrollComponent.setSelectedIconImage(a0.getResourceId(i), a.initials, com.navdy.hud.app.ui.component.image.InitialsImageView$Style.LARGE);
        } else {
            android.graphics.Bitmap a1 = com.navdy.hud.app.util.picasso.PicassoUtil.getBitmapfromCache(com.navdy.hud.app.framework.contacts.PhoneImageDownloader.getInstance().getImagePath(a.number, com.navdy.service.library.events.photo.PhotoType.PHOTO_CONTACT));
            if (a1 == null) {
                if (android.text.TextUtils.isEmpty((CharSequence)a.name)) {
                    this.vscrollComponent.setSelectedIconImage(R.drawable.icon_user_bg_4, (String)null, com.navdy.hud.app.ui.component.image.InitialsImageView$Style.LARGE);
                } else {
                    com.navdy.hud.app.framework.contacts.ContactImageHelper a2 = com.navdy.hud.app.framework.contacts.ContactImageHelper.getInstance();
                    this.vscrollComponent.setSelectedIconImage(a2.getResourceId(a.defaultImageIndex), a.initials, com.navdy.hud.app.ui.component.image.InitialsImageView$Style.LARGE);
                }
            } else {
                this.vscrollComponent.setSelectedIconImage(a1);
            }
        }
        label2: if (android.text.TextUtils.isEmpty((CharSequence)a.name)) {
            this.vscrollComponent.selectedText.setText((CharSequence)a.formattedNumber);
        } else {
            int i0 = this.contacts.size();
            label0: {
                label1: {
                    if (i0 != 1) {
                        break label1;
                    }
                    if (!android.text.TextUtils.isEmpty((CharSequence)a.numberTypeStr)) {
                        break label0;
                    }
                }
                this.vscrollComponent.selectedText.setText((CharSequence)a.name);
                break label2;
            }
            this.vscrollComponent.selectedText.setText((CharSequence)new StringBuilder().append(a.name).append("\n").append(a.numberTypeStr).toString());
        }
    }
    
    public void showToolTip() {
    }
}
