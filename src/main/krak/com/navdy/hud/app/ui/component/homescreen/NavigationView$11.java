package com.navdy.hud.app.ui.component.homescreen;

class NavigationView$11 implements Runnable {
    final com.navdy.hud.app.ui.component.homescreen.NavigationView this$0;
    final com.here.android.mpa.common.GeoCoordinate val$center;
    
    NavigationView$11(com.navdy.hud.app.ui.component.homescreen.NavigationView a, com.here.android.mpa.common.GeoCoordinate a0) {
        super();
        this.this$0 = a;
        this.val$center = a0;
    }
    
    public void run() {
        label12: {
            Throwable a = null;
            label0: {
                label9: {
                    label7: {
                        label4: {
                            label11: try {
                                com.navdy.hud.app.ui.component.homescreen.NavigationView.access$200(this.this$0).v("check if prev route was active");
                                com.navdy.hud.app.maps.here.HereMapUtil$SavedRouteData a0 = com.navdy.hud.app.maps.here.HereMapUtil.getSavedRouteData(com.navdy.hud.app.ui.component.homescreen.NavigationView.access$200(this.this$0));
                                com.navdy.service.library.events.navigation.NavigationRouteRequest a1 = a0.navigationRouteRequest;
                                label10: {
                                    if (a1 != null) {
                                        break label10;
                                    }
                                    com.navdy.hud.app.ui.component.homescreen.NavigationView.access$200(this.this$0).v("no saved route info");
                                    com.navdy.hud.app.framework.destinations.DestinationsManager.getInstance().launchSuggestedDestination();
                                    break label11;
                                }
                                com.navdy.hud.app.ui.component.homescreen.NavigationView.access$200(this.this$0).v("saved route found");
                                com.navdy.hud.app.maps.here.HereRouteManager.printRouteRequest(a1);
                                boolean b = com.navdy.hud.app.ui.component.homescreen.NavigationView.access$1100(this.this$0).isNavigationActive();
                                label8: {
                                    if (!b) {
                                        break label8;
                                    }
                                    com.navdy.hud.app.ui.component.homescreen.NavigationView.access$200(this.this$0).v("navigation is already active or route being calculated,ignore the saved route");
                                    com.navdy.hud.app.framework.destinations.DestinationsManager.getInstance().clearSuggestedDestination();
                                    break label9;
                                }
                                com.here.android.mpa.common.GeoCoordinate a2 = new com.here.android.mpa.common.GeoCoordinate(a1.destination.latitude.doubleValue(), a1.destination.longitude.doubleValue(), 0.0);
                                double d = this.val$center.distanceTo(a2);
                                com.navdy.hud.app.ui.component.homescreen.NavigationView.access$200(this.this$0).v(new StringBuilder().append("distance remaining to destination is [").append(d).append("] current[").append(this.val$center).append("] target [").append(a2).append("]").toString());
                                boolean b0 = a1.routeAttributes.contains(com.navdy.service.library.events.navigation.NavigationRouteRequest$RouteAttribute.ROUTE_ATTRIBUTE_GAS);
                                label6: {
                                    if (!b0) {
                                        break label6;
                                    }
                                    if (!(d >= 0.0)) {
                                        break label6;
                                    }
                                    if (!(d <= 500.0)) {
                                        break label6;
                                    }
                                    com.navdy.hud.app.ui.component.homescreen.NavigationView.access$200(this.this$0).v("gas route and distance to destination is <= 500.0, don't navigate");
                                    com.navdy.hud.app.ui.component.homescreen.NavigationView.access$200(this.this$0).v("remove gas route and look for non gas route");
                                    com.navdy.hud.app.maps.here.HereMapUtil.removeRouteInfo(com.navdy.hud.app.ui.component.homescreen.NavigationView.access$200(this.this$0), false);
                                    a0 = com.navdy.hud.app.maps.here.HereMapUtil.getSavedRouteData(com.navdy.hud.app.ui.component.homescreen.NavigationView.access$200(this.this$0));
                                    a1 = a0.navigationRouteRequest;
                                    label5: {
                                        if (a1 == null) {
                                            break label5;
                                        }
                                        com.here.android.mpa.common.GeoCoordinate a3 = new com.here.android.mpa.common.GeoCoordinate(a1.destination.latitude.doubleValue(), a1.destination.longitude.doubleValue(), 0.0);
                                        d = this.val$center.distanceTo(a3);
                                        b0 = false;
                                        break label6;
                                    }
                                    com.navdy.hud.app.ui.component.homescreen.NavigationView.access$200(this.this$0).v("no non gas route");
                                    com.navdy.hud.app.framework.destinations.DestinationsManager.getInstance().launchSuggestedDestination();
                                    break label7;
                                }
                                label3: {
                                    if (b0) {
                                        break label3;
                                    }
                                    if (!(d >= 0.0)) {
                                        break label3;
                                    }
                                    if (!(d <= 500.0)) {
                                        break label3;
                                    }
                                    com.navdy.hud.app.ui.component.homescreen.NavigationView.access$200(this.this$0).v("distance to destination is <= 500, don't navigate");
                                    com.navdy.hud.app.framework.destinations.DestinationsManager.getInstance().launchSuggestedDestination();
                                    break label4;
                                }
                                com.navdy.hud.app.framework.destinations.DestinationsManager.getInstance().clearSuggestedDestination();
                                com.navdy.service.library.events.navigation.NavigationRouteRequest a4 = new com.navdy.service.library.events.navigation.NavigationRouteRequest$Builder(a1).originDisplay(Boolean.valueOf(true)).cancelCurrent(Boolean.valueOf(false)).build();
                                com.navdy.hud.app.ui.component.homescreen.NavigationView.access$200(this.this$0).v("sleep before starting navigation");
                                com.navdy.hud.app.util.GenericUtil.sleep(2000);
                                com.navdy.hud.app.ui.component.homescreen.NavigationView.access$200(this.this$0).v("slept");
                                boolean b1 = a0.isGasRoute;
                                label2: {
                                    label1: {
                                        if (!b1) {
                                            break label1;
                                        }
                                        if (!com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getFeatureUtil().isFeatureEnabled(com.navdy.hud.app.util.FeatureUtil$Feature.FUEL_ROUTING)) {
                                            break label1;
                                        }
                                        com.navdy.hud.app.ui.component.homescreen.NavigationView.access$200(this.this$0).v("start saveroute navigation (gas route)");
                                        break label2;
                                    }
                                    com.navdy.hud.app.ui.component.homescreen.NavigationView.access$200(this.this$0).v("start saveroute navigation");
                                }
                                com.navdy.hud.app.ui.component.homescreen.NavigationView.access$1200(this.this$0).post(a4);
                                com.navdy.hud.app.ui.component.homescreen.NavigationView.access$1200(this.this$0).post(new com.navdy.hud.app.event.RemoteEvent((com.squareup.wire.Message)a4));
                            } catch(Throwable a5) {
                                a = a5;
                                break label0;
                            }
                            com.navdy.hud.app.maps.here.HereMapUtil.removeRouteInfo(com.navdy.hud.app.ui.component.homescreen.NavigationView.access$200(this.this$0), false);
                            break label12;
                        }
                        com.navdy.hud.app.maps.here.HereMapUtil.removeRouteInfo(com.navdy.hud.app.ui.component.homescreen.NavigationView.access$200(this.this$0), false);
                        break label12;
                    }
                    com.navdy.hud.app.maps.here.HereMapUtil.removeRouteInfo(com.navdy.hud.app.ui.component.homescreen.NavigationView.access$200(this.this$0), false);
                    break label12;
                }
                com.navdy.hud.app.maps.here.HereMapUtil.removeRouteInfo(com.navdy.hud.app.ui.component.homescreen.NavigationView.access$200(this.this$0), false);
                break label12;
            }
            try {
                com.navdy.hud.app.ui.component.homescreen.NavigationView.access$200(this.this$0).e(a);
            } catch(Throwable a6) {
                com.navdy.hud.app.maps.here.HereMapUtil.removeRouteInfo(com.navdy.hud.app.ui.component.homescreen.NavigationView.access$200(this.this$0), false);
                throw a6;
            }
            com.navdy.hud.app.maps.here.HereMapUtil.removeRouteInfo(com.navdy.hud.app.ui.component.homescreen.NavigationView.access$200(this.this$0), false);
        }
    }
}
