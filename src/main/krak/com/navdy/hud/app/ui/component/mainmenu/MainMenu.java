package com.navdy.hud.app.ui.component.mainmenu;
import com.navdy.hud.app.R;

public class MainMenu implements com.navdy.hud.app.ui.component.mainmenu.IMenu {
    final private static int ACTIVITY_TRAY_UPDATE;
    final private static int ETA_UPDATE_INTERVAL;
    final private static String activeTripTitle;
    final private static java.util.ArrayList activityTrayIcon;
    final private static java.util.ArrayList activityTrayIconId;
    final private static java.util.ArrayList activityTraySelectedColor;
    final private static java.util.ArrayList activityTrayUnSelectedColor;
    final public static int bkColorUnselected;
    final private static com.navdy.hud.app.framework.phonecall.CallManager callManager;
    final static java.util.HashMap childMenus;
    final private static com.navdy.hud.app.ui.component.vlist.VerticalList$Model connectPhone;
    final private static com.navdy.hud.app.ui.component.vlist.VerticalList$Model contacts;
    final private static String dashTitle;
    final public static com.navdy.hud.app.ui.component.vlist.VerticalList$FontInfo defaultFontInfo;
    final private static int glancesColor;
    final private static android.os.Handler handler;
    final private static String mapTitle;
    final private static com.navdy.hud.app.ui.component.vlist.VerticalList$Model maps;
    final private static com.navdy.hud.app.ui.component.vlist.VerticalList$Model musicControl;
    private static com.navdy.hud.app.manager.MusicManager musicManager;
    final private static int nowPlayingColor;
    final private static com.navdy.hud.app.ui.component.vlist.VerticalList$Model places;
    final private static com.navdy.hud.app.manager.RemoteDeviceManager remoteDeviceManager;
    final private static android.content.res.Resources resources;
    final private static com.navdy.service.library.log.Logger sLogger;
    final private static com.navdy.hud.app.ui.component.vlist.VerticalList$Model settings;
    final private static com.navdy.hud.app.ui.component.vlist.VerticalList$Model smartDash;
    final private static com.navdy.hud.app.ui.framework.UIStateManager uiStateManager;
    final private static com.navdy.hud.app.ui.component.vlist.VerticalList$Model voiceAssistanceGoogle;
    final private static com.navdy.hud.app.ui.component.vlist.VerticalList$Model voiceAssistanceSiri;
    final private static com.navdy.hud.app.ui.component.vlist.VerticalList$Model voiceSearch;
    private com.navdy.hud.app.ui.component.mainmenu.ActiveTripMenu activeTripMenu;
    private Runnable activityTrayRunnable;
    private boolean activityTrayRunning;
    private int activityTraySelection;
    private int activityTraySelectionId;
    private com.navdy.service.library.events.audio.MusicTrackInfo addedTrackInfo;
    private com.squareup.otto.Bus bus;
    private boolean busRegistered;
    private java.util.List cachedList;
    private com.navdy.hud.app.ui.component.mainmenu.ContactsMenu contactsMenu;
    private int curToolTipId;
    private int curToolTipPos;
    private String currentEta;
    private Runnable etaRunnable;
    private com.navdy.hud.app.ui.component.mainmenu.MainOptionsMenu mainOptionsMenu;
    private com.navdy.hud.app.ui.component.mainmenu.IMenu musicMenu;
    private com.navdy.hud.app.manager.MusicManager$MusicUpdateListener musicUpdateListener;
    private com.navdy.hud.app.ui.component.mainmenu.PlacesMenu placesMenu;
    private com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2$Presenter presenter;
    private boolean registeredMusicCallback;
    private int selectedIndex;
    private com.navdy.hud.app.ui.component.mainmenu.SettingsMenu settingsMenu;
    private String swVersion;
    private com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent vscrollComponent;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.ui.component.mainmenu.MainMenu.class);
        ETA_UPDATE_INTERVAL = (int)java.util.concurrent.TimeUnit.SECONDS.toMillis(30L);
        ACTIVITY_TRAY_UPDATE = (int)java.util.concurrent.TimeUnit.SECONDS.toMillis(1L);
        childMenus = new java.util.HashMap();
        handler = new android.os.Handler(android.os.Looper.getMainLooper());
        activityTrayIcon = new java.util.ArrayList();
        activityTrayIconId = new java.util.ArrayList();
        activityTraySelectedColor = new java.util.ArrayList();
        activityTrayUnSelectedColor = new java.util.ArrayList();
        childMenus.put(com.navdy.hud.app.ui.component.mainmenu.IMenu$Menu.SETTINGS.name(), com.navdy.hud.app.ui.component.mainmenu.IMenu$Menu.SETTINGS);
        childMenus.put(com.navdy.hud.app.ui.component.mainmenu.IMenu$Menu.PLACES.name(), com.navdy.hud.app.ui.component.mainmenu.IMenu$Menu.PLACES);
        childMenus.put(com.navdy.hud.app.ui.component.mainmenu.IMenu$Menu.CONTACTS.name(), com.navdy.hud.app.ui.component.mainmenu.IMenu$Menu.CONTACTS);
        childMenus.put(com.navdy.hud.app.ui.component.mainmenu.IMenu$Menu.MUSIC.name(), com.navdy.hud.app.ui.component.mainmenu.IMenu$Menu.MUSIC);
        remoteDeviceManager = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance();
        resources = com.navdy.hud.app.HudApplication.getAppContext().getResources();
        callManager = remoteDeviceManager.getCallManager();
        uiStateManager = remoteDeviceManager.getUiStateManager();
        bkColorUnselected = resources.getColor(R.color.icon_bk_color_unselected);
        activeTripTitle = resources.getString(R.string.mm_active_trip);
        defaultFontInfo = com.navdy.hud.app.ui.component.vlist.VerticalList.getFontInfo(com.navdy.hud.app.ui.component.vlist.VerticalList$FontSize.FONT_SIZE_26);
        nowPlayingColor = resources.getColor(R.color.music_now_playing);
        glancesColor = resources.getColor(R.color.mm_glances);
        String s = resources.getString(R.string.mm_voice_search);
        String s0 = resources.getString(R.string.mm_voice_search_description);
        int i = resources.getColor(R.color.mm_voice_search);
        voiceSearch = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(R.id.main_menu_voice_search, R.drawable.icon_mm_voice_search_2, i, bkColorUnselected, i, s, s0);
        String s1 = resources.getString(R.string.carousel_menu_smartdash_title);
        int i0 = resources.getColor(R.color.mm_dash);
        smartDash = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(R.id.main_menu_smart_dash, R.drawable.icon_mm_dash_2, i0, bkColorUnselected, i0, s1, (String)null);
        String s2 = resources.getString(R.string.carousel_menu_map_title);
        int i1 = resources.getColor(R.color.mm_map);
        maps = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(R.id.main_menu_maps, R.drawable.icon_mm_map_2, i1, bkColorUnselected, i1, s2, (String)null);
        String s3 = resources.getString(R.string.carousel_menu_map_places);
        int i2 = resources.getColor(R.color.mm_places);
        places = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(R.id.main_menu_places, R.drawable.icon_mm_places_2, i2, bkColorUnselected, i2, s3, (String)null);
        String s4 = resources.getString(R.string.carousel_menu_music_control);
        int i3 = resources.getColor(R.color.mm_music);
        musicControl = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(R.id.main_menu_music, R.drawable.icon_mm_music_2, i3, bkColorUnselected, i3, s4, (String)null);
        String s5 = resources.getString(R.string.carousel_menu_voice_control_siri);
        int i4 = resources.getColor(R.color.mm_voice_assist_siri);
        voiceAssistanceSiri = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(R.id.main_menu_voice, R.drawable.icon_mm_siri_2, i4, bkColorUnselected, i4, s5, (String)null);
        String s6 = resources.getString(R.string.carousel_menu_voice_control_google);
        int i5 = resources.getColor(R.color.mm_voice_assist_gnow);
        voiceAssistanceGoogle = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(R.id.main_menu_voice, R.drawable.icon_mm_googlenow_2, i5, bkColorUnselected, i5, s6, (String)null);
        String s7 = resources.getString(R.string.carousel_menu_contacts);
        int i6 = resources.getColor(R.color.mm_contacts);
        contacts = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(R.id.main_contacts, R.drawable.icon_mm_contacts_2, i6, bkColorUnselected, i6, s7, (String)null);
        String s8 = resources.getString(R.string.carousel_menu_settings_title);
        int i7 = resources.getColor(R.color.mm_settings);
        settings = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(R.id.main_menu_settings, R.drawable.icon_mm_settings_2, i7, bkColorUnselected, i7, s8, (String)null);
        String s9 = resources.getString(R.string.carousel_settings_connect_phone_title);
        int i8 = resources.getColor(R.color.mm_connnect_phone);
        connectPhone = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(R.id.main_menu_settings_connect_phone, R.drawable.icon_settings_connect_phone_2, i8, bkColorUnselected, i8, s9, (String)null);
        dashTitle = resources.getString(R.string.carousel_menu_smartdash_options);
        mapTitle = resources.getString(R.string.carousel_menu_map_options);
        musicManager = remoteDeviceManager.getMusicManager();
    }
    
    public MainMenu(com.squareup.otto.Bus a, com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent a0, com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2$Presenter a1) {
        this.activityTraySelection = -1;
        this.activityTraySelectionId = -1;
        this.etaRunnable = (Runnable)new com.navdy.hud.app.ui.component.mainmenu.MainMenu$1(this);
        this.curToolTipPos = -1;
        this.curToolTipId = -1;
        this.activityTrayRunnable = (Runnable)new com.navdy.hud.app.ui.component.mainmenu.MainMenu$2(this);
        this.musicUpdateListener = (com.navdy.hud.app.manager.MusicManager$MusicUpdateListener)new com.navdy.hud.app.ui.component.mainmenu.MainMenu$3(this);
        this.bus = a;
        this.vscrollComponent = a0;
        this.presenter = a1;
    }
    
    static void access$000(com.navdy.hud.app.ui.component.mainmenu.MainMenu a) {
        a.fillEta();
    }
    
    static int access$100() {
        return ETA_UPDATE_INTERVAL;
    }
    
    static boolean access$1000(com.navdy.hud.app.ui.component.mainmenu.MainMenu a, com.navdy.service.library.events.audio.MusicTrackInfo a0) {
        return a.canShowTrackInfo(a0);
    }
    
    static int access$1100(com.navdy.hud.app.ui.component.mainmenu.MainMenu a) {
        return a.curToolTipId;
    }
    
    static com.navdy.service.library.log.Logger access$1200() {
        return sLogger;
    }
    
    static String access$1300(com.navdy.hud.app.ui.component.mainmenu.MainMenu a) {
        return a.getMusicTrackInfo();
    }
    
    static com.squareup.otto.Bus access$1400(com.navdy.hud.app.ui.component.mainmenu.MainMenu a) {
        return a.bus;
    }
    
    static com.navdy.hud.app.manager.MusicManager access$1500() {
        return musicManager;
    }
    
    static android.os.Handler access$200() {
        return handler;
    }
    
    static boolean access$300(com.navdy.hud.app.ui.component.mainmenu.MainMenu a) {
        return a.activityTrayRunning;
    }
    
    static boolean access$302(com.navdy.hud.app.ui.component.mainmenu.MainMenu a, boolean b) {
        a.activityTrayRunning = b;
        return b;
    }
    
    static int access$400(com.navdy.hud.app.ui.component.mainmenu.MainMenu a) {
        return a.curToolTipPos;
    }
    
    static String access$500(com.navdy.hud.app.ui.component.mainmenu.MainMenu a, int i) {
        return a.getToolTipString(i);
    }
    
    static com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2$Presenter access$600(com.navdy.hud.app.ui.component.mainmenu.MainMenu a) {
        return a.presenter;
    }
    
    static int access$700() {
        return ACTIVITY_TRAY_UPDATE;
    }
    
    static com.navdy.service.library.events.audio.MusicTrackInfo access$800(com.navdy.hud.app.ui.component.mainmenu.MainMenu a) {
        return a.addedTrackInfo;
    }
    
    static com.navdy.service.library.events.audio.MusicTrackInfo access$802(com.navdy.hud.app.ui.component.mainmenu.MainMenu a, com.navdy.service.library.events.audio.MusicTrackInfo a0) {
        a.addedTrackInfo = a0;
        return a0;
    }
    
    static boolean access$900(com.navdy.hud.app.ui.component.mainmenu.MainMenu a, com.navdy.service.library.events.audio.MusicTrackInfo a0, com.navdy.service.library.events.audio.MusicTrackInfo a1) {
        return a.isSameTrack(a0, a1);
    }
    
    private void addLongPressAction(com.navdy.service.library.events.DeviceInfo a, java.util.List a0, boolean b, boolean b0, boolean b1) {
        if (b1) {
            if (this.isLongPressActionPlaceSearch()) {
                if (this.selectedIndex == -1) {
                    this.selectedIndex = a0.size();
                }
                this.addVoiceAssistance(a, a0);
            } else if (b && b0) {
                if (this.selectedIndex == -1) {
                    this.selectedIndex = a0.size();
                }
                a0.add(voiceSearch);
            }
        } else if (this.isLongPressActionPlaceSearch()) {
            if (b && b0) {
                a0.add(voiceSearch);
            }
        } else {
            this.addVoiceAssistance(a, a0);
        }
    }
    
    private void addVoiceAssistance(com.navdy.service.library.events.DeviceInfo a, java.util.List a0) {
        if (a.platform != com.navdy.service.library.events.DeviceInfo$Platform.PLATFORM_iOS) {
            a0.add(voiceAssistanceGoogle);
        } else {
            a0.add(voiceAssistanceSiri);
        }
    }
    
    private boolean canShowTrackInfo(com.navdy.service.library.events.audio.MusicTrackInfo a) {
        boolean b = false;
        label3: {
            label0: {
                label1: {
                    if (a == null) {
                        break label1;
                    }
                    boolean b0 = com.navdy.hud.app.manager.MusicManager.tryingToPlay(a.playbackState);
                    label2: {
                        if (b0) {
                            break label2;
                        }
                        if (a.playbackState != com.navdy.service.library.events.audio.MusicPlaybackState.PLAYBACK_PAUSED) {
                            break label1;
                        }
                    }
                    if (!android.text.TextUtils.isEmpty((CharSequence)a.name)) {
                        break label0;
                    }
                }
                b = false;
                break label3;
            }
            b = true;
        }
        return b;
    }
    
    private void clearNowPlayingState() {
        this.addedTrackInfo = null;
    }
    
    private com.navdy.hud.app.ui.component.mainmenu.IMenu createChildMenu(com.navdy.hud.app.ui.component.mainmenu.IMenu$Menu a, String s) {
        Object a0 = null;
        sLogger.v(new StringBuilder().append("createChildMenu:").append(s).toString());
        String s0 = null;
        if (s != null) {
            if (s.indexOf("/") != 0) {
                s0 = null;
                s = null;
            } else {
                s0 = s.substring(1);
                int i = s0.indexOf("/");
                if (i < 0) {
                    s = null;
                } else {
                    String s1 = s.substring(i + 1);
                    s0 = s0.substring(0, i);
                    s = s1;
                }
            }
        }
        switch(com.navdy.hud.app.ui.component.mainmenu.MainMenu$6.$SwitchMap$com$navdy$hud$app$ui$component$mainmenu$IMenu$Menu[a.ordinal()]) {
            case 4: {
                if (this.musicMenu == null) {
                    this.musicMenu = (com.navdy.hud.app.ui.component.mainmenu.IMenu)new com.navdy.hud.app.ui.component.mainmenu.MusicMenu2(this.bus, this.vscrollComponent, this.presenter, (com.navdy.hud.app.ui.component.mainmenu.IMenu)this, (com.navdy.hud.app.ui.component.mainmenu.MusicMenu2$MusicMenuData)null);
                }
                this.musicMenu.setBackSelectionId(R.id.main_menu_music);
                boolean b = android.text.TextUtils.isEmpty((CharSequence)s0);
                label3: {
                    if (b) {
                        break label3;
                    }
                    com.navdy.hud.app.ui.component.mainmenu.IMenu a1 = this.musicMenu.getChildMenu((com.navdy.hud.app.ui.component.mainmenu.IMenu)this, s0, s);
                    if (a1 != null) {
                        a0 = a1;
                        break;
                    }
                }
                a0 = this.musicMenu;
                break;
            }
            case 3: {
                if (this.settingsMenu == null) {
                    this.settingsMenu = new com.navdy.hud.app.ui.component.mainmenu.SettingsMenu(this.bus, this.vscrollComponent, this.presenter, (com.navdy.hud.app.ui.component.mainmenu.IMenu)this);
                }
                boolean b0 = android.text.TextUtils.isEmpty((CharSequence)s0);
                label2: {
                    if (b0) {
                        break label2;
                    }
                    com.navdy.hud.app.ui.component.mainmenu.IMenu a2 = this.settingsMenu.getChildMenu((com.navdy.hud.app.ui.component.mainmenu.IMenu)this, s0, s);
                    if (a2 != null) {
                        a0 = a2;
                        break;
                    }
                }
                this.settingsMenu.setBackSelectionId(R.id.main_menu_settings);
                a0 = this.settingsMenu;
                break;
            }
            case 2: {
                if (this.placesMenu == null) {
                    this.placesMenu = new com.navdy.hud.app.ui.component.mainmenu.PlacesMenu(this.bus, this.vscrollComponent, this.presenter, (com.navdy.hud.app.ui.component.mainmenu.IMenu)this);
                }
                boolean b1 = android.text.TextUtils.isEmpty((CharSequence)s0);
                label1: {
                    if (b1) {
                        break label1;
                    }
                    com.navdy.hud.app.ui.component.mainmenu.IMenu a3 = this.placesMenu.getChildMenu((com.navdy.hud.app.ui.component.mainmenu.IMenu)this, s0, s);
                    if (a3 != null) {
                        a0 = a3;
                        break;
                    }
                }
                this.placesMenu.setBackSelectionId(R.id.main_menu_places);
                a0 = this.placesMenu;
                break;
            }
            case 1: {
                if (this.contactsMenu == null) {
                    this.contactsMenu = new com.navdy.hud.app.ui.component.mainmenu.ContactsMenu(this.bus, this.vscrollComponent, this.presenter, (com.navdy.hud.app.ui.component.mainmenu.IMenu)this);
                }
                boolean b2 = android.text.TextUtils.isEmpty((CharSequence)s0);
                label0: {
                    if (b2) {
                        break label0;
                    }
                    com.navdy.hud.app.ui.component.mainmenu.IMenu a4 = this.contactsMenu.getChildMenu((com.navdy.hud.app.ui.component.mainmenu.IMenu)this, s0, s);
                    if (a4 != null) {
                        a0 = a4;
                        break;
                    }
                }
                this.contactsMenu.setBackSelectionId(R.id.main_contacts);
                a0 = this.contactsMenu;
                break;
            }
            default: {
                a0 = null;
            }
        }
        return (com.navdy.hud.app.ui.component.mainmenu.IMenu)a0;
    }
    
    private void fillEta() {
        try {
            String s = com.navdy.hud.app.maps.here.HereMapUtil.getCurrentEtaStringWithDestination();
            if (!android.text.TextUtils.isEmpty((CharSequence)s)) {
                this.currentEta = s;
            }
        } catch(Throwable a) {
            sLogger.e(a);
        }
    }
    
    private com.navdy.hud.app.ui.component.vlist.VerticalList$Model getActivityTrayModel(boolean b) {
        boolean b0 = false;
        boolean b1 = false;
        boolean b2 = false;
        boolean b3 = false;
        activityTrayIcon.clear();
        activityTrayIconId.clear();
        activityTraySelectedColor.clear();
        activityTrayUnSelectedColor.clear();
        if (b) {
            if (callManager.isCallInProgress()) {
                activityTrayIcon.add(Integer.valueOf(R.drawable.icon_mm_contacts_2));
                activityTrayIconId.add(Integer.valueOf(R.id.main_menu_call));
                activityTraySelectedColor.add(Integer.valueOf(resources.getColor(R.color.mm_connnect_phone)));
                activityTrayUnSelectedColor.add(Integer.valueOf(bkColorUnselected));
                b0 = true;
            } else {
                b0 = false;
            }
        } else {
            b0 = false;
        }
        handler.removeCallbacks(this.etaRunnable);
        if (com.navdy.hud.app.maps.here.HereMapsManager.getInstance().isInitialized()) {
            if (com.navdy.hud.app.maps.here.HereNavigationManager.getInstance().isNavigationModeOn()) {
                sLogger.v("nav on adding trip menu");
                activityTrayIcon.add(Integer.valueOf(R.drawable.icon_badge_active_trip));
                activityTrayIconId.add(Integer.valueOf(R.id.main_menu_active_trip));
                activityTraySelectedColor.add(Integer.valueOf(resources.getColor(R.color.mm_active_trip)));
                activityTrayUnSelectedColor.add(Integer.valueOf(bkColorUnselected));
                this.fillEta();
                handler.postDelayed(this.etaRunnable, (long)ETA_UPDATE_INTERVAL);
                b1 = true;
            } else {
                b1 = false;
            }
        } else {
            b1 = false;
        }
        if (com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().getNotificationCount() > 0) {
            activityTrayIcon.add(Integer.valueOf(R.drawable.icon_mm_glances_2));
            activityTrayIconId.add(Integer.valueOf(R.id.main_menu_glances));
            activityTraySelectedColor.add(Integer.valueOf(glancesColor));
            activityTrayUnSelectedColor.add(Integer.valueOf(bkColorUnselected));
        }
        if (b && !b0) {
            com.navdy.service.library.events.audio.MusicTrackInfo a = musicManager.getCurrentTrack();
            if (this.canShowTrackInfo(a)) {
                activityTrayIcon.add(Integer.valueOf(R.drawable.icon_mm_music_2));
                activityTrayIconId.add(Integer.valueOf(R.id.main_menu_now_playing));
                activityTraySelectedColor.add(Integer.valueOf(nowPlayingColor));
                activityTrayUnSelectedColor.add(Integer.valueOf(bkColorUnselected));
                this.addedTrackInfo = a;
                sLogger.v("added now playing");
            } else {
                sLogger.v("not added now playing");
            }
        }
        if (!b1 && com.navdy.hud.app.util.OTAUpdateService.isUpdateAvailable()) {
            android.content.res.Resources a0 = resources;
            Object[] a1 = new Object[1];
            a1[0] = com.navdy.hud.app.util.OTAUpdateService.getSWUpdateVersion();
            this.swVersion = a0.getString(R.string.mm_navdy_update, a1);
            sLogger.v("add navdy s/w");
            activityTrayIcon.add(Integer.valueOf(R.drawable.icon_software_update_2));
            activityTrayIconId.add(Integer.valueOf(R.id.settings_menu_software_update));
            activityTraySelectedColor.add(Integer.valueOf(resources.getColor(R.color.mm_settings_update_display)));
            activityTrayUnSelectedColor.add(Integer.valueOf(bkColorUnselected));
            b1 = true;
        }
        if (!b1) {
            com.navdy.hud.app.device.dial.DialManager a2 = com.navdy.hud.app.device.dial.DialManager.getInstance();
            if (a2.isDialConnected() && a2.getDialFirmwareUpdater().getVersions().isUpdateAvailable()) {
                sLogger.v("add dial s/w");
                activityTrayIcon.add(Integer.valueOf(R.drawable.icon_dial_update_2));
                activityTrayIconId.add(Integer.valueOf(R.id.settings_menu_dial_update));
                activityTraySelectedColor.add(Integer.valueOf(resources.getColor(R.color.mm_settings_update_dial)));
                activityTrayUnSelectedColor.add(Integer.valueOf(bkColorUnselected));
            }
        }
        com.navdy.service.library.events.ui.Screen a3 = uiStateManager.getDefaultMainActiveScreen();
        if (a3 == null) {
            b2 = false;
            b3 = false;
        } else if (a3 != com.navdy.service.library.events.ui.Screen.SCREEN_HOME) {
            b2 = false;
            b3 = false;
        } else {
            com.navdy.hud.app.ui.component.homescreen.HomeScreenView a4 = uiStateManager.getHomescreenView();
            if (a4 == null) {
                b2 = false;
                b3 = false;
            } else {
                switch(com.navdy.hud.app.ui.component.mainmenu.MainMenu$6.$SwitchMap$com$navdy$hud$app$ui$component$homescreen$HomeScreen$DisplayMode[a4.getDisplayMode().ordinal()]) {
                    case 2: {
                        b2 = false;
                        b3 = true;
                        break;
                    }
                    case 1: {
                        b2 = true;
                        b3 = false;
                        break;
                    }
                    default: {
                        b2 = false;
                        b3 = false;
                    }
                }
            }
        }
        if (b2) {
            activityTrayIcon.add(Integer.valueOf(R.drawable.icon_main_menu_dash_options));
            activityTrayIconId.add(Integer.valueOf(R.id.main_menu_smart_dash_options));
            activityTraySelectedColor.add(Integer.valueOf(resources.getColor(R.color.mm_dash_options)));
            activityTrayUnSelectedColor.add(Integer.valueOf(bkColorUnselected));
        } else if (b3) {
            activityTrayIcon.add(Integer.valueOf(R.drawable.icon_main_menu_map_options));
            activityTrayIconId.add(Integer.valueOf(R.id.main_menu_maps_options));
            activityTraySelectedColor.add(Integer.valueOf(resources.getColor(R.color.mm_map_options)));
            activityTrayUnSelectedColor.add(Integer.valueOf(bkColorUnselected));
        }
        int i = activityTrayIcon.size();
        int[] a5 = new int[i];
        int[] a6 = new int[i];
        int[] a7 = new int[i];
        int[] a8 = new int[i];
        int i0 = i - 1;
        int i1 = 0;
        while(i0 >= 0) {
            a5[i1] = ((Integer)activityTrayIcon.get(i0)).intValue();
            a6[i1] = ((Integer)activityTrayIconId.get(i0)).intValue();
            a7[i1] = ((Integer)activityTraySelectedColor.get(i0)).intValue();
            a8[i1] = ((Integer)activityTrayUnSelectedColor.get(i0)).intValue();
            i1 = i1 + 1;
            i0 = i0 + -1;
        }
        int i2 = this.activityTraySelection;
        if (i2 == -1) {
            i2 = i - 1;
        }
        return com.navdy.hud.app.ui.component.vlist.viewholder.IconOptionsViewHolder.buildModel(R.id.main_menu_activity_tray, a5, a6, a7, a8, a7, i2, true);
    }
    
    private String getDurationStr(long j) {
        String s = null;
        if (j >= 3600L) {
            int i = (int)j / 3600;
            long j0 = (long)((int)j - i * 60 * 60);
            int i0 = (int)j0 / 60;
            Object[] a = new Object[3];
            a[0] = Integer.valueOf(i);
            a[1] = Integer.valueOf(i0);
            a[2] = Integer.valueOf((int)(j0 - (long)(i0 * 60)));
            s = String.format("%02d:%02d:%02d", a);
        } else {
            int i1 = (int)j / 60;
            Object[] a0 = new Object[2];
            a0[0] = Integer.valueOf(i1);
            a0[1] = Integer.valueOf((int)(j - (long)(i1 * 60)));
            s = String.format("%02d:%02d", a0);
        }
        return s;
    }
    
    private String getMusicTrackInfo() {
        String s = null;
        label1: if (this.addedTrackInfo != null) {
            String s0 = this.addedTrackInfo.name;
            s = this.addedTrackInfo.author;
            boolean b = !android.text.TextUtils.isEmpty((CharSequence)s);
            boolean b0 = !android.text.TextUtils.isEmpty((CharSequence)s0);
            label0: {
                if (!b) {
                    break label0;
                }
                if (!b0) {
                    break label0;
                }
                s = new StringBuilder().append(s).append(" - ").append(s0).toString();
                break label1;
            }
            if (b0) {
                s = s0;
            }
        } else {
            s = null;
        }
        return s;
    }
    
    private String getToolTipString(int i) {
        String s = null;
        switch(i) {
            case R.id.settings_menu_software_update: {
                s = this.swVersion;
                break;
            }
            case R.id.settings_menu_dial_update: {
                com.navdy.hud.app.device.dial.DialFirmwareUpdater$Versions a = com.navdy.hud.app.device.dial.DialManager.getInstance().getDialFirmwareUpdater().getVersions();
                String s0 = new StringBuilder().append("1.0.").append(a.local.incrementalVersion).toString();
                android.content.res.Resources a0 = resources;
                Object[] a1 = new Object[1];
                a1[0] = s0;
                s = a0.getString(R.string.mm_dial_update, a1);
                break;
            }
            case R.id.main_menu_smart_dash_options: {
                s = dashTitle;
                break;
            }
            case R.id.main_menu_now_playing: {
                s = this.getMusicTrackInfo();
                break;
            }
            case R.id.main_menu_maps_options: {
                s = mapTitle;
                break;
            }
            case R.id.main_menu_glances: {
                int i0 = com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().getNotificationCount();
                if (i0 != 1) {
                    android.content.res.Resources a2 = resources;
                    Object[] a3 = new Object[1];
                    a3[0] = Integer.valueOf(i0);
                    s = a2.getString(R.string.mm_glances_subtitle, a3);
                    break;
                } else {
                    s = resources.getString(R.string.mm_glances_single);
                    break;
                }
            }
            case R.id.main_menu_call: {
                com.navdy.hud.app.framework.phonecall.CallNotification a4 = (com.navdy.hud.app.framework.phonecall.CallNotification)com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().getNotification("navdy#phone#call#notif");
                s = null;
                if (a4 == null) {
                    break;
                }
                boolean b = callManager.isCallInProgress();
                s = null;
                if (!b) {
                    break;
                }
                String s1 = a4.getCaller();
                s = null;
                if (s1 == null) {
                    break;
                }
                int i1 = callManager.getCurrentCallDuration();
                s = null;
                if (i1 < 0) {
                    break;
                }
                String s2 = this.getDurationStr((long)i1);
                android.content.res.Resources a5 = resources;
                Object[] a6 = new Object[2];
                a6[0] = s1;
                a6[1] = s2;
                s = a5.getString(R.string.mm_call_info, a6);
                break;
            }
            case R.id.main_menu_active_trip: {
                s = (this.currentEta != null) ? this.currentEta : activeTripTitle;
                break;
            }
            default: {
                s = null;
            }
        }
        return s;
    }
    
    private void handleGlances() {
        sLogger.v("glances");
        com.navdy.hud.app.analytics.AnalyticsSupport.recordMenuSelection("glances");
        android.content.res.Resources a = com.navdy.hud.app.HudApplication.getAppContext().getResources();
        com.navdy.service.library.events.preferences.NotificationPreferences a0 = com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getCurrentProfile().getNotificationPreferences();
        if (a0.enabled != null && a0.enabled.booleanValue()) {
            com.navdy.hud.app.framework.notifications.NotificationManager a1 = com.navdy.hud.app.framework.notifications.NotificationManager.getInstance();
            if (a1.makeNotificationCurrent(true)) {
                sLogger.d("glances available");
                com.navdy.hud.app.analytics.AnalyticsSupport.setGlanceNavigationSource("dial");
                a1.showNotification();
            } else {
                sLogger.d("no glances found");
                android.os.Bundle a2 = new android.os.Bundle();
                a2.putInt("13", 1000);
                a2.putInt("8", R.drawable.icon_qm_glances_grey);
                a2.putString("4", a.getString(R.string.glances_none));
                a2.putInt("5", R.style.Glances_1);
                com.navdy.hud.app.framework.toast.ToastManager.getInstance().addToast(new com.navdy.hud.app.framework.toast.ToastManager$ToastParams("glance-none", a2, (com.navdy.hud.app.framework.toast.IToastCallback)null, true, false));
                this.presenter.performSelectionAnimation((Runnable)new com.navdy.hud.app.ui.component.vmenu.VerticalAnimationUtils$ActionRunnable(this.bus, com.navdy.service.library.events.ui.Screen.SCREEN_BACK));
            }
        } else {
            sLogger.v("glances are not enabled");
            android.os.Bundle a3 = new android.os.Bundle();
            a3.putInt("15", (int)a.getDimension(R.dimen.no_glances_choice_padding));
            a3.putBoolean("12", true);
            a3.putInt("13", 5000);
            a3.putInt("8", R.drawable.icon_glances_disabled_modal);
            a3.putInt("11", R.drawable.icon_disabled);
            a3.putString("4", a.getString(R.string.glances_disabled));
            a3.putInt("5", R.style.Glances_Disabled_1);
            a3.putString("6", a.getString(R.string.glances_disabled_msg));
            a3.putInt("7", R.style.Glances_Disabled_2);
            a3.putInt("16", (int)a.getDimension(R.dimen.no_glances_width));
            a3.putString("17", com.navdy.hud.app.framework.voice.TTSUtils.TTS_GLANCES_DISABLED);
            com.navdy.hud.app.framework.toast.ToastManager.getInstance().addToast(new com.navdy.hud.app.framework.toast.ToastManager$ToastParams("glance-disabled", a3, (com.navdy.hud.app.framework.toast.IToastCallback)null, true, false));
            this.presenter.performSelectionAnimation((Runnable)new com.navdy.hud.app.ui.component.vmenu.VerticalAnimationUtils$ActionRunnable(this.bus, com.navdy.service.library.events.ui.Screen.SCREEN_BACK));
        }
    }
    
    private boolean isLongPressActionPlaceSearch() {
        return com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getCurrentProfile().getLongPressAction() == com.navdy.service.library.events.preferences.DriverProfilePreferences$DialLongPressAction.DIAL_LONG_PRESS_PLACE_SEARCH;
    }
    
    private boolean isSameTrack(com.navdy.service.library.events.audio.MusicTrackInfo a, com.navdy.service.library.events.audio.MusicTrackInfo a0) {
        boolean b = false;
        label2: {
            label0: {
                label1: {
                    if (a == null) {
                        break label1;
                    }
                    if (a0 == null) {
                        break label1;
                    }
                    if (!android.text.TextUtils.equals((CharSequence)a.name, (CharSequence)a0.name)) {
                        break label1;
                    }
                    if (android.text.TextUtils.equals((CharSequence)a.author, (CharSequence)a0.author)) {
                        break label0;
                    }
                }
                b = false;
                break label2;
            }
            b = true;
        }
        return b;
    }
    
    private void startActivityTrayRunnable(int i, int i0) {
        this.curToolTipId = i;
        this.curToolTipPos = i0;
        this.activityTrayRunning = true;
        handler.postDelayed(this.activityTrayRunnable, (long)ACTIVITY_TRAY_UPDATE);
    }
    
    private void stopActivityTrayRunnable() {
        this.curToolTipId = -1;
        this.curToolTipPos = -1;
        this.activityTrayRunning = false;
        handler.removeCallbacks(this.activityTrayRunnable);
    }
    
    public void arrivalEvent(com.navdy.hud.app.maps.MapEvents$ArrivalEvent a) {
        sLogger.v("arrival event");
        handler.removeCallbacks(this.etaRunnable);
        this.fillEta();
    }
    
    public void clearState() {
        this.settingsMenu = null;
        this.placesMenu = null;
        this.contactsMenu = null;
        this.mainOptionsMenu = null;
        this.activeTripMenu = null;
        this.musicMenu = null;
    }
    
    public int getActivityTraySelection() {
        return this.activityTraySelection;
    }
    
    public com.navdy.hud.app.ui.component.mainmenu.IMenu getChildMenu(com.navdy.hud.app.ui.component.mainmenu.IMenu a, String s, String s0) {
        com.navdy.hud.app.ui.component.mainmenu.IMenu a0 = null;
        sLogger.v(new StringBuilder().append("getChildMenu:").append(s).append(",").append(s0).toString());
        com.navdy.hud.app.ui.component.mainmenu.IMenu$Menu a1 = (com.navdy.hud.app.ui.component.mainmenu.IMenu$Menu)childMenus.get(s);
        if (a1 != null) {
            a0 = this.createChildMenu(a1, s0);
        } else {
            sLogger.v("getChildMenu: not found");
            a0 = null;
        }
        return a0;
    }
    
    public int getInitialSelection() {
        return this.selectedIndex;
    }
    
    public java.util.List getItems() {
        this.clearNowPlayingState();
        this.stopActivityTrayRunnable();
        this.curToolTipId = -1;
        this.curToolTipPos = -1;
        com.navdy.service.library.events.DeviceInfo a = remoteDeviceManager.getRemoteDeviceInfo();
        java.util.ArrayList a0 = new java.util.ArrayList();
        boolean b = !com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getCurrentProfile().isDefaultProfile();
        if (b && a == null) {
            b = false;
        }
        sLogger.v(new StringBuilder().append("isConnected:").append(b).toString());
        this.selectedIndex = -1;
        com.navdy.hud.app.maps.here.HereMapsManager a1 = com.navdy.hud.app.maps.here.HereMapsManager.getInstance();
        com.navdy.hud.app.ui.component.vlist.VerticalList$Model a2 = this.getActivityTrayModel(b);
        if (a1.isInitialized() && a1.isNavigationModeOn()) {
            this.selectedIndex = ((java.util.List)a0).size();
            if (a2.iconIds != null) {
                int i = 0;
                while(i < a2.iconIds.length) {
                    {
                        if (a2.iconIds[i] != R.id.main_menu_active_trip) {
                            i = i + 1;
                            continue;
                        }
                        a2.currentIconSelection = i;
                        break;
                    }
                }
            }
        }
        ((java.util.List)a0).add(a2);
        boolean b0 = com.navdy.hud.app.maps.here.HereMapsManager.getInstance().isNavigationPossible();
        boolean b1 = com.navdy.hud.app.util.RemoteCapabilitiesUtil.supportsVoiceSearch();
        if (b) {
            if (!b0) {
                sLogger.v("nav not possible");
            }
            this.addLongPressAction(a, (java.util.List)a0, b1, b0, true);
        } else {
            this.selectedIndex = ((java.util.List)a0).size();
            ((java.util.List)a0).add(connectPhone);
        }
        if (this.selectedIndex == -1) {
            this.selectedIndex = ((java.util.List)a0).size();
        }
        ((java.util.List)a0).add(maps);
        ((java.util.List)a0).add(smartDash);
        if (b) {
            if (b0) {
                ((java.util.List)a0).add(places);
            }
            ((java.util.List)a0).add(contacts);
        }
        String s = resources.getString(R.string.mm_glances);
        com.navdy.hud.app.ui.component.vlist.VerticalList$Model a3 = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(R.id.main_menu_glances, R.drawable.icon_mm_glances_2, glancesColor, bkColorUnselected, glancesColor, s, (String)null);
        com.navdy.service.library.events.preferences.NotificationPreferences a4 = com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getCurrentProfile().getNotificationPreferences();
        if (a4.enabled != null && a4.enabled.booleanValue()) {
            int i0 = com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().getNotificationCount();
            a3.title = resources.getString(R.string.mm_glances);
            android.content.res.Resources a5 = resources;
            Object[] a6 = new Object[1];
            a6[0] = Integer.valueOf(i0);
            a3.subTitle = a5.getString(R.string.mm_glances_subtitle, a6);
        } else {
            a3.title = resources.getString(R.string.mm_glances_disabled);
            a3.subTitle = null;
        }
        ((java.util.List)a0).add(a3);
        if (b) {
            ((java.util.List)a0).add(musicControl);
            this.addLongPressAction(a, (java.util.List)a0, b1, b0, false);
        }
        ((java.util.List)a0).add(settings);
        this.cachedList = (java.util.List)a0;
        if (!this.registeredMusicCallback) {
            musicManager.addMusicUpdateListener(this.musicUpdateListener);
            this.registeredMusicCallback = true;
            sLogger.v("register music");
        }
        if (!this.busRegistered) {
            this.bus.register(this);
            this.busRegistered = true;
            sLogger.v("registered bus");
        }
        return (java.util.List)a0;
    }
    
    public com.navdy.hud.app.ui.component.vlist.VerticalList$Model getModelfromPos(int i) {
        com.navdy.hud.app.ui.component.vlist.VerticalList$Model a = null;
        java.util.List a0 = this.cachedList;
        label2: {
            label0: {
                label1: {
                    if (a0 == null) {
                        break label1;
                    }
                    if (this.cachedList.size() > i) {
                        break label0;
                    }
                }
                a = null;
                break label2;
            }
            a = (com.navdy.hud.app.ui.component.vlist.VerticalList$Model)this.cachedList.get(i);
        }
        return a;
    }
    
    public com.navdy.hud.app.ui.component.vmenu.VerticalFastScrollIndex getScrollIndex() {
        return null;
    }
    
    public com.navdy.hud.app.ui.component.mainmenu.IMenu$Menu getType() {
        return com.navdy.hud.app.ui.component.mainmenu.IMenu$Menu.MAIN;
    }
    
    public boolean isBindCallsEnabled() {
        return false;
    }
    
    public boolean isFirstItemEmpty() {
        return true;
    }
    
    public boolean isItemClickable(int i, int i0) {
        boolean b = false;
        switch(i) {
            case R.id.main_menu_no_fav_contacts: case R.id.main_menu_no_fav_places: case R.id.main_menu_no_recent_contacts: case R.id.main_menu_no_suggested_places: {
                b = false;
                break;
            }
            default: {
                b = true;
            }
        }
        return b;
    }
    
    public void onBindToView(com.navdy.hud.app.ui.component.vlist.VerticalList$Model a, android.view.View a0, int i, com.navdy.hud.app.ui.component.vlist.VerticalList$ModelState a1) {
    }
    
    public void onFastScrollEnd() {
    }
    
    public void onFastScrollStart() {
    }
    
    public void onItemSelected(com.navdy.hud.app.ui.component.vlist.VerticalList$ItemSelectionState a) {
        if (a.id == R.id.main_menu_activity_tray) {
            String s = this.getToolTipString(a.subId);
            if (s == null) {
                this.stopActivityTrayRunnable();
                this.presenter.hideToolTip();
            } else {
                this.stopActivityTrayRunnable();
                switch(a.subId) {
                    case R.id.main_menu_now_playing: {
                        this.curToolTipId = a.subId;
                        this.curToolTipPos = a.subPosition;
                        break;
                    }
                    case R.id.main_menu_call: {
                        this.startActivityTrayRunnable(a.subId, a.subPosition);
                        break;
                    }
                    default: {
                        this.curToolTipId = -1;
                        this.curToolTipPos = -1;
                    }
                }
                this.presenter.showToolTip(a.subPosition, s);
            }
        } else {
            this.stopActivityTrayRunnable();
            this.presenter.hideToolTip();
        }
    }
    
    public void onScrollIdle() {
    }
    
    public void onUnload(com.navdy.hud.app.ui.component.mainmenu.IMenu$MenuLevel a) {
        handler.removeCallbacks(this.etaRunnable);
        if (this.registeredMusicCallback) {
            this.registeredMusicCallback = false;
            musicManager.removeMusicUpdateListener(this.musicUpdateListener);
            sLogger.v("unregister");
        }
        if (this.busRegistered) {
            this.bus.unregister(this);
            this.busRegistered = false;
            sLogger.v("bus unregistered");
        }
        this.clearNowPlayingState();
        if (a == com.navdy.hud.app.ui.component.mainmenu.IMenu$MenuLevel.CLOSE) {
            if (this.placesMenu != null) {
                this.placesMenu.onUnload(com.navdy.hud.app.ui.component.mainmenu.IMenu$MenuLevel.CLOSE);
            }
            if (this.contactsMenu != null) {
                this.contactsMenu.onUnload(com.navdy.hud.app.ui.component.mainmenu.IMenu$MenuLevel.CLOSE);
            }
            if (this.musicMenu != null) {
                this.musicMenu.onUnload(com.navdy.hud.app.ui.component.mainmenu.IMenu$MenuLevel.CLOSE);
            }
        }
    }
    
    public boolean selectItem(com.navdy.hud.app.ui.component.vlist.VerticalList$ItemSelectionState a) {
        sLogger.v(new StringBuilder().append("select id:").append(a.id).append(" pos:").append(a.pos).toString());
        label0: switch(a.id) {
            case R.id.main_menu_voice_search: {
                sLogger.v("voice_search");
                com.navdy.hud.app.analytics.AnalyticsSupport.recordMenuSelection("voice_search");
                this.presenter.performSelectionAnimation((Runnable)new com.navdy.hud.app.ui.component.mainmenu.MainMenu$4(this));
                break;
            }
            case R.id.main_menu_voice: {
                sLogger.v("voice-assist");
                com.navdy.hud.app.analytics.AnalyticsSupport.recordMenuSelection("voice_assist");
                this.presenter.performSelectionAnimation((Runnable)new com.navdy.hud.app.ui.component.vmenu.VerticalAnimationUtils$ActionRunnable(this.bus, com.navdy.service.library.events.ui.Screen.SCREEN_VOICE_CONTROL));
                break;
            }
            case R.id.main_menu_smart_dash: {
                sLogger.v("dash");
                com.navdy.hud.app.analytics.AnalyticsSupport.recordMenuSelection("dash");
                this.presenter.performSelectionAnimation((Runnable)new com.navdy.hud.app.ui.component.vmenu.VerticalAnimationUtils$ActionRunnable(this.bus, com.navdy.service.library.events.ui.Screen.SCREEN_DASHBOARD));
                break;
            }
            case R.id.main_menu_settings_connect_phone: {
                sLogger.v("connect phone");
                com.navdy.hud.app.analytics.AnalyticsSupport.recordMenuSelection("connect_phone");
                android.os.Bundle a0 = new android.os.Bundle();
                a0.putString(com.navdy.hud.app.screen.WelcomeScreen.ARG_ACTION, com.navdy.hud.app.screen.WelcomeScreen.ACTION_SWITCH_PHONE);
                this.presenter.performSelectionAnimation((Runnable)new com.navdy.hud.app.ui.component.vmenu.VerticalAnimationUtils$ActionRunnable(this.bus, com.navdy.service.library.events.ui.Screen.SCREEN_WELCOME, a0, false));
                break;
            }
            case R.id.main_menu_settings: {
                sLogger.v("settings");
                com.navdy.hud.app.analytics.AnalyticsSupport.recordMenuSelection("settings");
                this.createChildMenu(com.navdy.hud.app.ui.component.mainmenu.IMenu$Menu.SETTINGS, (String)null);
                this.presenter.loadMenu((com.navdy.hud.app.ui.component.mainmenu.IMenu)this.settingsMenu, com.navdy.hud.app.ui.component.mainmenu.IMenu$MenuLevel.SUB_LEVEL, a.pos, 0);
                break;
            }
            case R.id.main_menu_places: {
                sLogger.v("places");
                com.navdy.hud.app.analytics.AnalyticsSupport.recordMenuSelection("places");
                this.createChildMenu(com.navdy.hud.app.ui.component.mainmenu.IMenu$Menu.PLACES, (String)null);
                this.presenter.loadMenu((com.navdy.hud.app.ui.component.mainmenu.IMenu)this.placesMenu, com.navdy.hud.app.ui.component.mainmenu.IMenu$MenuLevel.SUB_LEVEL, a.pos, 0);
                break;
            }
            case R.id.main_menu_music: {
                com.navdy.hud.app.manager.MusicManager a1 = remoteDeviceManager.getMusicManager();
                boolean b = com.navdy.hud.app.ui.component.UISettings.isMusicBrowsingEnabled();
                label3: {
                    label4: {
                        if (!b) {
                            break label4;
                        }
                        if (a1.hasMusicCapabilities()) {
                            break label3;
                        }
                    }
                    sLogger.v("music");
                    com.navdy.hud.app.analytics.AnalyticsSupport.recordMenuSelection("music");
                    this.presenter.performSelectionAnimation((Runnable)new com.navdy.hud.app.ui.component.vmenu.VerticalAnimationUtils$ActionRunnable(this.bus, com.navdy.service.library.events.ui.Screen.SCREEN_MUSIC));
                    break;
                }
                sLogger.v("music browse");
                com.navdy.hud.app.analytics.AnalyticsSupport.recordMenuSelection("music");
                this.createChildMenu(com.navdy.hud.app.ui.component.mainmenu.IMenu$Menu.MUSIC, (String)null);
                this.presenter.loadMenu(this.musicMenu, com.navdy.hud.app.ui.component.mainmenu.IMenu$MenuLevel.SUB_LEVEL, a.pos, 0);
                break;
            }
            case R.id.main_menu_maps: {
                sLogger.v("map");
                com.navdy.hud.app.analytics.AnalyticsSupport.recordMenuSelection("hybrid_map");
                this.presenter.performSelectionAnimation((Runnable)new com.navdy.hud.app.ui.component.vmenu.VerticalAnimationUtils$ActionRunnable(this.bus, com.navdy.service.library.events.ui.Screen.SCREEN_HYBRID_MAP));
                break;
            }
            case R.id.main_menu_glances: {
                this.handleGlances();
                break;
            }
            case R.id.main_menu_activity_tray: {
                switch(a.subId) {
                    case R.id.settings_menu_software_update: {
                        sLogger.v("software update");
                        com.navdy.hud.app.analytics.AnalyticsSupport.recordMenuSelection("software_update_mm");
                        android.os.Bundle a2 = new android.os.Bundle();
                        a2.putBoolean("UPDATE_REMINDER", false);
                        this.presenter.performSelectionAnimation((Runnable)new com.navdy.hud.app.ui.component.vmenu.VerticalAnimationUtils$ActionRunnable(this.bus, com.navdy.service.library.events.ui.Screen.SCREEN_OTA_CONFIRMATION, a2, false));
                        break label0;
                    }
                    case R.id.settings_menu_dial_update: {
                        sLogger.v("dial update");
                        com.navdy.hud.app.analytics.AnalyticsSupport.recordMenuSelection("dial_update_mm");
                        android.os.Bundle a3 = new android.os.Bundle();
                        a3.putBoolean("UPDATE_REMINDER", false);
                        this.presenter.performSelectionAnimation((Runnable)new com.navdy.hud.app.ui.component.vmenu.VerticalAnimationUtils$ActionRunnable(this.bus, com.navdy.service.library.events.ui.Screen.SCREEN_DIAL_UPDATE_CONFIRMATION, a3, false));
                        break label0;
                    }
                    case R.id.main_menu_smart_dash_options: {
                        sLogger.v("dash-options");
                        com.navdy.hud.app.analytics.AnalyticsSupport.recordMenuSelection("dash-options");
                        if (this.mainOptionsMenu == null) {
                            this.mainOptionsMenu = new com.navdy.hud.app.ui.component.mainmenu.MainOptionsMenu(this.bus, this.vscrollComponent, this.presenter, (com.navdy.hud.app.ui.component.mainmenu.IMenu)this);
                        }
                        this.mainOptionsMenu.setMode(com.navdy.hud.app.ui.component.mainmenu.MainOptionsMenu$Mode.DASH);
                        this.activityTraySelection = a.subPosition;
                        this.activityTraySelectionId = a.subId;
                        this.presenter.loadMenu((com.navdy.hud.app.ui.component.mainmenu.IMenu)this.mainOptionsMenu, com.navdy.hud.app.ui.component.mainmenu.IMenu$MenuLevel.SUB_LEVEL, a.pos, 0);
                        break label0;
                    }
                    case R.id.main_menu_now_playing: {
                        sLogger.v("now_playing");
                        com.navdy.hud.app.analytics.AnalyticsSupport.recordMenuSelection("now_playing");
                        this.presenter.performSelectionAnimation((Runnable)new com.navdy.hud.app.ui.component.mainmenu.MainMenu$5(this));
                        break label0;
                    }
                    case R.id.main_menu_maps_options: {
                        sLogger.v("map options");
                        com.navdy.hud.app.analytics.AnalyticsSupport.recordMenuSelection("hybrid_map-options");
                        if (this.mainOptionsMenu == null) {
                            this.mainOptionsMenu = new com.navdy.hud.app.ui.component.mainmenu.MainOptionsMenu(this.bus, this.vscrollComponent, this.presenter, (com.navdy.hud.app.ui.component.mainmenu.IMenu)this);
                        }
                        this.mainOptionsMenu.setMode(com.navdy.hud.app.ui.component.mainmenu.MainOptionsMenu$Mode.MAP);
                        this.activityTraySelection = a.subPosition;
                        this.activityTraySelectionId = a.subId;
                        this.presenter.loadMenu((com.navdy.hud.app.ui.component.mainmenu.IMenu)this.mainOptionsMenu, com.navdy.hud.app.ui.component.mainmenu.IMenu$MenuLevel.SUB_LEVEL, a.pos, 0);
                        break label0;
                    }
                    case R.id.main_menu_glances: {
                        this.handleGlances();
                        break label0;
                    }
                    case R.id.main_menu_call: {
                        sLogger.v("phone call");
                        com.navdy.hud.app.analytics.AnalyticsSupport.recordMenuSelection("phone_call_mm");
                        com.navdy.hud.app.framework.notifications.NotificationManager a4 = com.navdy.hud.app.framework.notifications.NotificationManager.getInstance();
                        com.navdy.hud.app.framework.notifications.INotification a5 = a4.getNotification("navdy#phone#call#notif");
                        label1: {
                            label2: {
                                if (a5 == null) {
                                    break label2;
                                }
                                if (a4.makeNotificationCurrent(true)) {
                                    break label1;
                                }
                            }
                            this.presenter.performSelectionAnimation((Runnable)new com.navdy.hud.app.ui.component.vmenu.VerticalAnimationUtils$ActionRunnable(this.bus, com.navdy.service.library.events.ui.Screen.SCREEN_BACK));
                            break label0;
                        }
                        sLogger.d("phone call");
                        a4.showNotification();
                        break label0;
                    }
                    case R.id.main_menu_active_trip: {
                        sLogger.v("trip options");
                        com.navdy.hud.app.analytics.AnalyticsSupport.recordMenuSelection("trip-options");
                        if (this.activeTripMenu == null) {
                            this.activeTripMenu = new com.navdy.hud.app.ui.component.mainmenu.ActiveTripMenu(this.bus, this.vscrollComponent, this.presenter, (com.navdy.hud.app.ui.component.mainmenu.IMenu)this);
                        }
                        this.activityTraySelection = a.subPosition;
                        this.activityTraySelectionId = a.subId;
                        this.presenter.loadMenu((com.navdy.hud.app.ui.component.mainmenu.IMenu)this.activeTripMenu, com.navdy.hud.app.ui.component.mainmenu.IMenu$MenuLevel.SUB_LEVEL, a.pos, 1, true);
                        break label0;
                    }
                    default: {
                        break label0;
                    }
                }
            }
            case R.id.main_contacts: {
                sLogger.v("contacts");
                com.navdy.hud.app.analytics.AnalyticsSupport.recordMenuSelection("contacts");
                this.createChildMenu(com.navdy.hud.app.ui.component.mainmenu.IMenu$Menu.CONTACTS, (String)null);
                this.presenter.loadMenu((com.navdy.hud.app.ui.component.mainmenu.IMenu)this.contactsMenu, com.navdy.hud.app.ui.component.mainmenu.IMenu$MenuLevel.SUB_LEVEL, a.pos, 0);
                break;
            }
        }
        return true;
    }
    
    public void setBackSelectionId(int i) {
    }
    
    public void setBackSelectionPos(int i) {
    }
    
    public void setSelectedIcon() {
        if (this.activityTraySelection != -1 && this.presenter.getCurrentSelection() == 0) {
            String s = this.getToolTipString(this.activityTraySelectionId);
            if (s != null) {
                this.presenter.showToolTip(this.activityTraySelection, s);
            }
        }
        this.activityTraySelection = -1;
        this.activityTraySelectionId = -1;
        android.content.res.Resources a = com.navdy.hud.app.HudApplication.getAppContext().getResources();
        int i = a.getDimensionPixelSize(R.dimen.vmenu_selected_image);
        int i0 = a.getColor(R.color.mm_icon_gradient_start);
        int i1 = a.getColor(R.color.mm_icon_gradient_end);
        float f = (float)i;
        float f0 = (float)i;
        android.graphics.Shader$TileMode a0 = android.graphics.Shader$TileMode.MIRROR;
        int[] a1 = new int[2];
        a1[0] = i0;
        a1[1] = i1;
        android.graphics.LinearGradient a2 = new android.graphics.LinearGradient(0.0f, 0.0f, f, f0, a1, (float[])null, a0);
        this.vscrollComponent.setSelectedIconColorImage(R.drawable.icon_mm_navdy, -1, (android.graphics.Shader)a2, 1f);
        this.vscrollComponent.selectedText.setText((CharSequence)a.getString(R.string.menu_str));
    }
    
    public void showToolTip() {
        int i = this.presenter.getCurrentSubSelectionId();
        int i0 = this.presenter.getCurrentSubSelectionPos();
        String s = this.getToolTipString(i);
        if (s == null) {
            this.stopActivityTrayRunnable();
            this.presenter.hideToolTip();
        } else {
            this.stopActivityTrayRunnable();
            if (i == R.id.main_menu_call) {
                this.startActivityTrayRunnable(i, i0);
            }
            this.presenter.showToolTip(i0, s);
        }
    }
}
