package com.navdy.hud.app.ui.component.vmenu;

final class VerticalAnimationUtils$2 implements Runnable {
    final Runnable val$endAction;
    
    VerticalAnimationUtils$2(Runnable a) {
        super();
        this.val$endAction = a;
    }
    
    public void run() {
        if (this.val$endAction != null) {
            this.val$endAction.run();
        }
    }
}
