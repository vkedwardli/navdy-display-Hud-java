package com.navdy.hud.app.ui.component.mainmenu;

class MusicMenu2$6 implements Runnable {
    final com.navdy.hud.app.ui.component.mainmenu.MusicMenu2 this$0;
    final com.navdy.service.library.events.audio.MusicArtworkResponse val$artworkResponse;
    final String val$idString;
    final Integer val$pos;
    
    MusicMenu2$6(com.navdy.hud.app.ui.component.mainmenu.MusicMenu2 a, com.navdy.service.library.events.audio.MusicArtworkResponse a0, Integer a1, String s) {
        super();
        this.this$0 = a;
        this.val$artworkResponse = a0;
        this.val$pos = a1;
        this.val$idString = s;
    }
    
    public void run() {
        byte[] a = this.val$artworkResponse.photo.toByteArray();
        label2: {
            label0: {
                label1: {
                    if (a == null) {
                        break label1;
                    }
                    if (a.length != 0) {
                        break label0;
                    }
                }
                com.navdy.hud.app.ui.component.mainmenu.MusicMenu2.access$400().w("Received photo has null or empty byte array");
                break label2;
            }
            com.navdy.hud.app.ui.component.mainmenu.MusicMenu2.access$800(this.this$0, a, this.val$pos, this.val$idString, true);
        }
    }
}
