package com.navdy.hud.app.ui.component.carousel;

public class ShrinkAnimator extends com.navdy.hud.app.ui.component.carousel.CarouselAnimator {
    public ShrinkAnimator() {
    }
    
    public android.animation.AnimatorSet createHiddenViewAnimation(com.navdy.hud.app.ui.component.carousel.CarouselLayout a, com.navdy.hud.app.ui.component.carousel.AnimationStrategy$Direction a0) {
        android.view.View a1 = null;
        android.view.View a2 = null;
        android.animation.AnimatorSet a3 = new android.animation.AnimatorSet();
        if (a0 != com.navdy.hud.app.ui.component.carousel.AnimationStrategy$Direction.RIGHT) {
            a1 = a.newRightView;
            a2 = a.rightView;
        } else {
            a1 = a.newLeftView;
            a2 = a.leftView;
        }
        float f = 20f / (float)a2.getMeasuredWidth();
        a1.setScaleX(f);
        a1.setScaleY(f);
        float f0 = a2.getX();
        float f1 = (float)(((a0 != com.navdy.hud.app.ui.component.carousel.AnimationStrategy$Direction.RIGHT) ? 1 : -1) * a.viewPadding / 2);
        a1.setX((float)((a0 != com.navdy.hud.app.ui.component.carousel.AnimationStrategy$Direction.RIGHT) ? a2.getMeasuredWidth() : 0) + (f0 + f1));
        a1.setY(a2.getY() + (float)(a2.getMeasuredHeight() / 2) - 20f / 2f);
        android.animation.Animator[] a4 = new android.animation.Animator[4];
        float[] a5 = new float[1];
        a5[0] = a2.getX();
        a4[0] = android.animation.ObjectAnimator.ofFloat(a1, "x", a5);
        float[] a6 = new float[1];
        a6[0] = a2.getY();
        a4[1] = android.animation.ObjectAnimator.ofFloat(a1, "y", a6);
        float[] a7 = new float[1];
        a7[0] = 1f;
        a4[2] = android.animation.ObjectAnimator.ofFloat(a1, "scaleX", a7);
        float[] a8 = new float[1];
        a8[0] = 1f;
        a4[3] = android.animation.ObjectAnimator.ofFloat(a1, "scaleY", a8);
        a3.playTogether(a4);
        a1.setPivotX(0.5f);
        a1.setPivotY(0.5f);
        return a3;
    }
    
    public android.animation.Animator createViewOutAnimation(com.navdy.hud.app.ui.component.carousel.CarouselLayout a, com.navdy.hud.app.ui.component.carousel.AnimationStrategy$Direction a0) {
        android.view.View a1 = null;
        float f = 0.0f;
        if (a0 != com.navdy.hud.app.ui.component.carousel.AnimationStrategy$Direction.RIGHT) {
            a1 = a.leftView;
            f = a1.getX() - (float)(a.viewPadding / 2) - 20f / 2f;
        } else {
            a1 = a.rightView;
            f = a1.getX() + (float)a1.getMeasuredWidth() + (float)(a.viewPadding / 2) - 20f / 2f;
        }
        float f0 = a1.getY();
        float f1 = (float)(a1.getMeasuredHeight() / 2);
        float f2 = 20f / 2f;
        float f3 = 20f / (float)a1.getMeasuredWidth();
        android.animation.AnimatorSet a2 = new android.animation.AnimatorSet();
        android.animation.Animator[] a3 = new android.animation.Animator[4];
        float[] a4 = new float[1];
        a4[0] = f;
        a3[0] = android.animation.ObjectAnimator.ofFloat(a1, "x", a4);
        float[] a5 = new float[1];
        a5[0] = f0 + f1 - f2;
        a3[1] = android.animation.ObjectAnimator.ofFloat(a1, "y", a5);
        float[] a6 = new float[1];
        a6[0] = f3;
        a3[2] = android.animation.ObjectAnimator.ofFloat(a1, "scaleX", a6);
        float[] a7 = new float[1];
        a7[0] = f3;
        a3[3] = android.animation.ObjectAnimator.ofFloat(a1, "scaleY", a7);
        a2.playTogether(a3);
        a1.setPivotX(0.5f);
        a1.setPivotY(0.5f);
        return a2;
    }
}
