package com.navdy.hud.app.ui.component;

public class ChoiceLayout$Icon {
    public int resIdSelected;
    public int resIdUnSelected;
    
    public ChoiceLayout$Icon(int i) {
        this.resIdSelected = i;
        this.resIdUnSelected = i;
    }
    
    public ChoiceLayout$Icon(int i, int i0) {
        this.resIdSelected = i;
        this.resIdUnSelected = i0;
    }
}
