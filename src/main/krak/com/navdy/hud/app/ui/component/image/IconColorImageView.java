package com.navdy.hud.app.ui.component.image;

public class IconColorImageView extends android.widget.ImageView {
    private int bkColor;
    private android.graphics.Shader bkColorGradient;
    private android.content.Context context;
    private boolean draw;
    private int iconResource;
    private com.navdy.hud.app.ui.component.image.IconColorImageView$IconShape iconShape;
    private android.graphics.Paint paint;
    private float scaleF;
    
    public IconColorImageView(android.content.Context a) {
        this(a, (android.util.AttributeSet)null, 0);
    }
    
    public IconColorImageView(android.content.Context a, android.util.AttributeSet a0) {
        this(a, a0, 0);
    }
    
    public IconColorImageView(android.content.Context a, android.util.AttributeSet a0, int i) {
        super(a, a0, i);
        this.iconShape = com.navdy.hud.app.ui.component.image.IconColorImageView$IconShape.CIRCLE;
        this.draw = true;
        this.context = a;
        this.init();
    }
    
    private void init() {
        this.paint = new android.graphics.Paint();
        this.paint.setStrokeWidth(0.0f);
        this.paint.setAntiAlias(true);
    }
    
    protected void onDraw(android.graphics.Canvas a) {
        int i = this.getWidth();
        int i0 = this.getHeight();
        a.drawColor(0);
        if (this.bkColorGradient == null) {
            this.paint.setShader((android.graphics.Shader)null);
            this.paint.setColor(this.bkColor);
        } else {
            this.paint.setShader(this.bkColorGradient);
        }
        com.navdy.hud.app.ui.component.image.IconColorImageView$IconShape a0 = this.iconShape;
        label2: {
            label0: {
                label1: {
                    if (a0 == null) {
                        break label1;
                    }
                    if (this.iconShape != com.navdy.hud.app.ui.component.image.IconColorImageView$IconShape.CIRCLE) {
                        break label0;
                    }
                }
                a.drawCircle((float)(i / 2), (float)(i0 / 2), (float)(i / 2), this.paint);
                break label2;
            }
            a.drawRect(0.0f, 0.0f, (float)i, (float)i0, this.paint);
        }
        if (this.draw) {
            if (this.iconResource != 0) {
                android.graphics.drawable.Drawable a1 = this.context.getDrawable(this.iconResource);
                if (a1 != null) {
                    android.graphics.Bitmap a2 = ((android.graphics.drawable.BitmapDrawable)a1).getBitmap();
                    if (a2 != null) {
                        a.scale(this.scaleF, this.scaleF);
                        a.drawBitmap(a2, 0.0f, 0.0f, (android.graphics.Paint)null);
                    }
                }
            }
        } else {
            super.onDraw(a);
        }
    }
    
    public void setDraw(boolean b) {
        this.draw = b;
    }
    
    public void setIcon(int i, int i0, android.graphics.Shader a, float f) {
        this.iconResource = i;
        this.bkColor = i0;
        this.bkColorGradient = a;
        this.scaleF = f;
        this.invalidate();
    }
    
    public void setIconShape(com.navdy.hud.app.ui.component.image.IconColorImageView$IconShape a) {
        this.iconShape = a;
    }
}
