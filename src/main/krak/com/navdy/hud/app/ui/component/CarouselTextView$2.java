package com.navdy.hud.app.ui.component;

class CarouselTextView$2 implements Runnable {
    final com.navdy.hud.app.ui.component.CarouselTextView this$0;
    final String val$text;
    
    CarouselTextView$2(com.navdy.hud.app.ui.component.CarouselTextView a, String s) {
        super();
        this.this$0 = a;
        this.val$text = s;
    }
    
    public void run() {
        this.this$0.setText((CharSequence)this.val$text);
        this.this$0.animate().translationY(0.0f).alpha(1f).setDuration(500L);
    }
}
