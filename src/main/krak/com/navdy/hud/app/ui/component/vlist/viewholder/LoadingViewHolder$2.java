package com.navdy.hud.app.ui.component.vlist.viewholder;

class LoadingViewHolder$2 {
    final static int[] $SwitchMap$com$navdy$hud$app$ui$component$vlist$viewholder$VerticalViewHolder$AnimationType;
    
    static {
        $SwitchMap$com$navdy$hud$app$ui$component$vlist$viewholder$VerticalViewHolder$AnimationType = new int[com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$AnimationType.values().length];
        int[] a = $SwitchMap$com$navdy$hud$app$ui$component$vlist$viewholder$VerticalViewHolder$AnimationType;
        com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$AnimationType a0 = com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$AnimationType.NONE;
        try {
            a[a0.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$ui$component$vlist$viewholder$VerticalViewHolder$AnimationType[com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$AnimationType.INIT.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException0) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$ui$component$vlist$viewholder$VerticalViewHolder$AnimationType[com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$AnimationType.MOVE.ordinal()] = 3;
        } catch(NoSuchFieldError ignoredException1) {
        }
    }
}
