package com.navdy.hud.app.ui.component.vlist.viewholder;

final public class SwitchViewHolder$WhenMappings {
    final public static int[] $EnumSwitchMapping$0;
    final public static int[] $EnumSwitchMapping$1;
    final public static int[] $EnumSwitchMapping$2;
    final public static int[] $EnumSwitchMapping$3;
    final public static int[] $EnumSwitchMapping$4;
    
    static {
        $EnumSwitchMapping$0 = new int[com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$State.values().length];
        $EnumSwitchMapping$0[com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$State.SELECTED.ordinal()] = 1;
        $EnumSwitchMapping$0[com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$State.UNSELECTED.ordinal()] = 2;
        $EnumSwitchMapping$1 = new int[com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$AnimationType.values().length];
        $EnumSwitchMapping$1[com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$AnimationType.INIT.ordinal()] = 1;
        $EnumSwitchMapping$1[com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$AnimationType.NONE.ordinal()] = 2;
        $EnumSwitchMapping$1[com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$AnimationType.MOVE.ordinal()] = 3;
        $EnumSwitchMapping$2 = new int[com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$AnimationType.values().length];
        $EnumSwitchMapping$2[com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$AnimationType.INIT.ordinal()] = 1;
        $EnumSwitchMapping$2[com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$AnimationType.NONE.ordinal()] = 2;
        $EnumSwitchMapping$2[com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$AnimationType.MOVE.ordinal()] = 3;
        $EnumSwitchMapping$3 = new int[com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$AnimationType.values().length];
        $EnumSwitchMapping$3[com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$AnimationType.INIT.ordinal()] = 1;
        $EnumSwitchMapping$3[com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$AnimationType.NONE.ordinal()] = 2;
        $EnumSwitchMapping$3[com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$AnimationType.MOVE.ordinal()] = 3;
        $EnumSwitchMapping$4 = new int[com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$AnimationType.values().length];
        $EnumSwitchMapping$4[com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$AnimationType.INIT.ordinal()] = 1;
        $EnumSwitchMapping$4[com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$AnimationType.NONE.ordinal()] = 2;
        $EnumSwitchMapping$4[com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$AnimationType.MOVE.ordinal()] = 3;
    }
}
