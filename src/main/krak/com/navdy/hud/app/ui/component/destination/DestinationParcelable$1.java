package com.navdy.hud.app.ui.component.destination;

final class DestinationParcelable$1 implements android.os.Parcelable$Creator {
    DestinationParcelable$1() {
    }
    
    public com.navdy.hud.app.ui.component.destination.DestinationParcelable createFromParcel(android.os.Parcel a) {
        return new com.navdy.hud.app.ui.component.destination.DestinationParcelable(a);
    }
    
    public Object createFromParcel(android.os.Parcel a) {
        return this.createFromParcel(a);
    }
    
    public com.navdy.hud.app.ui.component.destination.DestinationParcelable[] newArray(int i) {
        return new com.navdy.hud.app.ui.component.destination.DestinationParcelable[i];
    }
    
    public Object[] newArray(int i) {
        return this.newArray(i);
    }
}
