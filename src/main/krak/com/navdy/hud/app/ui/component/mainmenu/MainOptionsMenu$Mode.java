package com.navdy.hud.app.ui.component.mainmenu;


    public enum MainOptionsMenu$Mode {
        MAP(0),
        DASH(1);

        private int value;
        MainOptionsMenu$Mode(int value) {
            this.value = value;
        }
        public int getValue() {
            return value;
        }
    }
    
//final class MainOptionsMenu$Mode extends Enum {
//    final private static com.navdy.hud.app.ui.component.mainmenu.MainOptionsMenu$Mode[] $VALUES;
//    final public static com.navdy.hud.app.ui.component.mainmenu.MainOptionsMenu$Mode DASH;
//    final public static com.navdy.hud.app.ui.component.mainmenu.MainOptionsMenu$Mode MAP;
//    
//    static {
//        MAP = new com.navdy.hud.app.ui.component.mainmenu.MainOptionsMenu$Mode("MAP", 0);
//        DASH = new com.navdy.hud.app.ui.component.mainmenu.MainOptionsMenu$Mode("DASH", 1);
//        com.navdy.hud.app.ui.component.mainmenu.MainOptionsMenu$Mode[] a = new com.navdy.hud.app.ui.component.mainmenu.MainOptionsMenu$Mode[2];
//        a[0] = MAP;
//        a[1] = DASH;
//        $VALUES = a;
//    }
//    
//    private MainOptionsMenu$Mode(String s, int i) {
//        super(s, i);
//    }
//    
//    public static com.navdy.hud.app.ui.component.mainmenu.MainOptionsMenu$Mode valueOf(String s) {
//        return (com.navdy.hud.app.ui.component.mainmenu.MainOptionsMenu$Mode)Enum.valueOf(com.navdy.hud.app.ui.component.mainmenu.MainOptionsMenu$Mode.class, s);
//    }
//    
//    public static com.navdy.hud.app.ui.component.mainmenu.MainOptionsMenu$Mode[] values() {
//        return $VALUES.clone();
//    }
//}
//