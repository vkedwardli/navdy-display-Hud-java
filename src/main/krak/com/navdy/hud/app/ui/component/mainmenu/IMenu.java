package com.navdy.hud.app.ui.component.mainmenu;

abstract public interface IMenu {
    abstract public com.navdy.hud.app.ui.component.mainmenu.IMenu getChildMenu(com.navdy.hud.app.ui.component.mainmenu.IMenu arg, String arg0, String arg1);
    
    
    abstract public int getInitialSelection();
    
    
    abstract public java.util.List getItems();
    
    
    abstract public com.navdy.hud.app.ui.component.vlist.VerticalList$Model getModelfromPos(int arg);
    
    
    abstract public com.navdy.hud.app.ui.component.vmenu.VerticalFastScrollIndex getScrollIndex();
    
    
    abstract public com.navdy.hud.app.ui.component.mainmenu.IMenu$Menu getType();
    
    
    abstract public boolean isBindCallsEnabled();
    
    
    abstract public boolean isFirstItemEmpty();
    
    
    abstract public boolean isItemClickable(int arg, int arg0);
    
    
    abstract public void onBindToView(com.navdy.hud.app.ui.component.vlist.VerticalList$Model arg, android.view.View arg0, int arg1, com.navdy.hud.app.ui.component.vlist.VerticalList$ModelState arg2);
    
    
    abstract public void onFastScrollEnd();
    
    
    abstract public void onFastScrollStart();
    
    
    abstract public void onItemSelected(com.navdy.hud.app.ui.component.vlist.VerticalList$ItemSelectionState arg);
    
    
    abstract public void onScrollIdle();
    
    
    abstract public void onUnload(com.navdy.hud.app.ui.component.mainmenu.IMenu$MenuLevel arg);
    
    
    abstract public boolean selectItem(com.navdy.hud.app.ui.component.vlist.VerticalList$ItemSelectionState arg);
    
    
    abstract public void setBackSelectionId(int arg);
    
    
    abstract public void setBackSelectionPos(int arg);
    
    
    abstract public void setSelectedIcon();
    
    
    abstract public void showToolTip();
}
