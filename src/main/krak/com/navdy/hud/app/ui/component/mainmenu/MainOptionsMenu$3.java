package com.navdy.hud.app.ui.component.mainmenu;

class MainOptionsMenu$3 implements Runnable {
    final com.navdy.hud.app.ui.component.mainmenu.MainOptionsMenu this$0;
    
    MainOptionsMenu$3(com.navdy.hud.app.ui.component.mainmenu.MainOptionsMenu a) {
        super();
        this.this$0 = a;
    }
    
    public void run() {
        com.navdy.hud.app.ui.component.mainmenu.MainOptionsMenu.access$000(this.this$0).close();
        if (com.navdy.hud.app.ui.component.mainmenu.MainOptionsMenu.access$200(this.this$0).isDemoPaused()) {
            com.navdy.hud.app.ui.component.mainmenu.MainOptionsMenu.access$200(this.this$0).performDemoPlaybackAction(com.navdy.hud.app.debug.DriveRecorder$Action.RESUME, true);
        } else {
            com.navdy.hud.app.ui.component.mainmenu.MainOptionsMenu.access$200(this.this$0).performDemoPlaybackAction(com.navdy.hud.app.debug.DriveRecorder$Action.PLAY, true);
        }
    }
}
