package com.navdy.hud.app.ui.component.dashboard;

class GaugeViewPager$1 implements android.animation.ValueAnimator$AnimatorUpdateListener {
    final com.navdy.hud.app.ui.component.dashboard.GaugeViewPager this$0;
    
    GaugeViewPager$1(com.navdy.hud.app.ui.component.dashboard.GaugeViewPager a) {
        super();
        this.this$0 = a;
    }
    
    public void onAnimationUpdate(android.animation.ValueAnimator a) {
        int i = ((Integer)a.getAnimatedValue()).intValue();
        android.view.ViewGroup$MarginLayoutParams a0 = (android.view.ViewGroup$MarginLayoutParams)com.navdy.hud.app.ui.component.dashboard.GaugeViewPager.access$000(this.this$0).getLayoutParams();
        a0.topMargin = i;
        com.navdy.hud.app.ui.component.dashboard.GaugeViewPager.access$000(this.this$0).setLayoutParams((android.view.ViewGroup$LayoutParams)a0);
    }
}
