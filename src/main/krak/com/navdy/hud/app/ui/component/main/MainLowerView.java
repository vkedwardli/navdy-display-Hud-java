package com.navdy.hud.app.ui.component.main;

public class MainLowerView extends android.widget.FrameLayout {
    final private static com.navdy.service.library.log.Logger sLogger;
    private com.navdy.hud.app.ui.component.homescreen.HomeScreenView homeScreenView;
    private com.navdy.hud.app.ui.component.homescreen.NavigationView navigationView;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.ui.component.main.MainLowerView.class);
    }
    
    public MainLowerView(android.content.Context a) {
        super(a);
    }
    
    public MainLowerView(android.content.Context a, android.util.AttributeSet a0) {
        super(a, a0);
    }
    
    public MainLowerView(android.content.Context a, android.util.AttributeSet a0, int i) {
        super(a, a0, i);
    }
    
    private boolean initView() {
        boolean b = false;
        if (this.navigationView == null) {
            this.navigationView = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getUiStateManager().getNavigationView();
        }
        if (this.homeScreenView == null) {
            this.homeScreenView = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getUiStateManager().getHomescreenView();
        }
        com.navdy.hud.app.ui.component.homescreen.NavigationView a = this.navigationView;
        label2: {
            label0: {
                label1: {
                    if (a == null) {
                        break label1;
                    }
                    if (this.homeScreenView != null) {
                        break label0;
                    }
                }
                b = false;
                break label2;
            }
            b = true;
        }
        return b;
    }
    
    public void ejectView() {
        sLogger.v("ejectView");
        if (this.initView() && com.navdy.hud.app.maps.here.HereMapsManager.getInstance().isInitialized() && this.getVisibility() == 0) {
            this.animate().cancel();
            this.setVisibility(8);
            this.setAlpha(0.0f);
            this.removeAllViews();
            if (com.navdy.hud.app.maps.here.HereNavigationManager.getInstance().getNavController().getState() == com.navdy.hud.app.maps.here.HereNavController$State.NAVIGATING) {
                sLogger.v("ejectView: nav active already");
            } else {
                sLogger.v("ejectView: reset to open mode");
                this.homeScreenView.setMode(com.navdy.hud.app.maps.NavigationMode.MAP);
            }
        }
    }
    
    public void injectView(android.view.View a) {
        sLogger.v("injectView");
        if (this.initView() && com.navdy.hud.app.maps.here.HereMapsManager.getInstance().isInitialized()) {
            this.removeAllViews();
            this.addView(a);
            this.setAlpha(0.0f);
            this.setVisibility(0);
            if (this.homeScreenView.isNavigationActive()) {
                sLogger.v("injectView: nav active already");
            } else {
                sLogger.v("injectView: set to nav mode");
                this.homeScreenView.setMode(com.navdy.hud.app.maps.NavigationMode.MAP_ON_ROUTE);
            }
            this.animate().alpha(1f).start();
        }
    }
}
