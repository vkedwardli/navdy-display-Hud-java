package com.navdy.hud.app.ui.component.homescreen;

public class SmartDashWidgetManager {
    final public static String ANALOG_CLOCK_WIDGET_ID = "ANALOG_CLOCK_WIDGET";
    final public static String CALENDAR_WIDGET_ID = "CALENDAR_WIDGET";
    final public static String COMPASS_WIDGET_ID = "COMPASS_WIDGET";
    final public static String DIGITAL_CLOCK_2_WIDGET_ID = "DIGITAL_CLOCK_2_WIDGET";
    final public static String DIGITAL_CLOCK_WIDGET_ID = "DIGITAL_CLOCK_WIDGET";
    final public static String DRIVE_SCORE_GAUGE_ID = "DRIVE_SCORE_GAUGE_ID";
    final public static String EMPTY_WIDGET_ID = "EMPTY_WIDGET";
    final public static String ENGINE_TEMPERATURE_GAUGE_ID = "ENGINE_TEMPERATURE_GAUGE_ID";
    final public static String ETA_GAUGE_ID = "ETA_GAUGE_ID";
    final public static String FUEL_GAUGE_ID = "FUEL_GAUGE_ID";
    private static java.util.List GAUGES;
    private static java.util.HashSet GAUGE_NAME_LOOKUP;
    final public static String GFORCE_WIDGET_ID = "GFORCE_WIDGET";
    final public static String MPG_AVG_WIDGET_ID = "MPG_AVG_WIDGET";
    final public static String MPG_GRAPH_WIDGET_ID = "MPG_GRAPH_WIDGET";
    final public static String MUSIC_WIDGET_ID = "MUSIC_WIDGET";
    final public static String PREFERENCE_GAUGE_ENABLED = "PREF_GAUGE_ENABLED_";
    final public static String SPEED_LIMIT_SIGN_GAUGE_ID = "SPEED_LIMIT_SIGN_GAUGE_ID";
    final public static String TRAFFIC_INCIDENT_GAUGE_ID = "TRAFFIC_INCIDENT_GAUGE_ID";
    final public static String WEATHER_WIDGET_ID = "WEATHER_GRAPH_WIDGET";
    final private static com.navdy.service.library.log.Logger sLogger;
    private com.squareup.otto.Bus bus;
    private android.content.SharedPreferences currentUserPreferences;
    private com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager$IWidgetFilter filter;
    private android.content.SharedPreferences globalPreferences;
    private com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager$LifecycleEvent lastLifeCycleEvent;
    private android.content.Context mContext;
    private java.util.List mGaugeIds;
    private boolean mLoaded;
    private java.util.HashMap mWidgetIndexMap;
    private java.util.HashMap mWidgetPresentersCache;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.class);
        GAUGES = (java.util.List)new java.util.ArrayList();
        GAUGE_NAME_LOOKUP = new java.util.HashSet();
        GAUGES.add("CALENDAR_WIDGET");
        GAUGES.add("COMPASS_WIDGET");
        GAUGES.add("ANALOG_CLOCK_WIDGET");
        GAUGES.add("DIGITAL_CLOCK_WIDGET");
        GAUGES.add("DIGITAL_CLOCK_2_WIDGET");
        GAUGES.add("DRIVE_SCORE_GAUGE_ID");
        GAUGES.add("ENGINE_TEMPERATURE_GAUGE_ID");
        GAUGES.add("FUEL_GAUGE_ID");
        GAUGES.add("GFORCE_WIDGET");
        GAUGES.add("MUSIC_WIDGET");
        GAUGES.add("MPG_AVG_WIDGET");
        GAUGES.add("SPEED_LIMIT_SIGN_GAUGE_ID");
        GAUGES.add("TRAFFIC_INCIDENT_GAUGE_ID");
        GAUGES.add("ETA_GAUGE_ID");
        GAUGES.add("EMPTY_WIDGET");
        Object a = GAUGES.iterator();
        while(((java.util.Iterator)a).hasNext()) {
            String s = (String)((java.util.Iterator)a).next();
            GAUGE_NAME_LOOKUP.add(s);
        }
    }
    
    public SmartDashWidgetManager(android.content.SharedPreferences a, android.content.Context a0) {
        this.mWidgetPresentersCache = new java.util.HashMap();
        this.mLoaded = false;
        this.lastLifeCycleEvent = null;
        this.globalPreferences = a;
        this.mContext = a0;
        this.mGaugeIds = (java.util.List)new java.util.ArrayList();
        this.mWidgetIndexMap = new java.util.HashMap();
        this.bus = new com.squareup.otto.Bus();
    }
    
    public static boolean isValidGaugeId(String s) {
        boolean b = false;
        boolean b0 = android.text.TextUtils.isEmpty((CharSequence)s);
        label2: {
            label0: {
                label1: {
                    if (b0) {
                        break label1;
                    }
                    if (GAUGE_NAME_LOOKUP.contains(s)) {
                        break label0;
                    }
                }
                b = false;
                break label2;
            }
            b = true;
        }
        return b;
    }
    
    private void sendLifecycleEvent(com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager$LifecycleEvent a) {
        Object a0 = this.mWidgetPresentersCache.values().iterator();
        while(((java.util.Iterator)a0).hasNext()) {
            this.sendLifecycleEvent(a, ((com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager$SmartDashWidgetCache)((java.util.Iterator)a0).next()).getWidgetPresentersMap().values().iterator());
        }
    }
    
    private void sendLifecycleEvent(com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager$LifecycleEvent a, java.util.Iterator a0) {
        Object a1 = a0;
        while(((java.util.Iterator)a1).hasNext()) {
            switch(com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager$1.$SwitchMap$com$navdy$hud$app$ui$component$homescreen$SmartDashWidgetManager$LifecycleEvent[a.ordinal()]) {
                case 2: {
                    ((com.navdy.hud.app.view.DashboardWidgetPresenter)((java.util.Iterator)a1).next()).onResume();
                    break;
                }
                case 1: {
                    ((com.navdy.hud.app.view.DashboardWidgetPresenter)((java.util.Iterator)a1).next()).onPause();
                    break;
                }
            }
        }
    }
    
    public com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager$SmartDashWidgetCache buildSmartDashWidgetCache(int i) {
        com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager$SmartDashWidgetCache a = null;
        if (this.mWidgetPresentersCache.containsKey(Integer.valueOf(i))) {
            a = (com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager$SmartDashWidgetCache)this.mWidgetPresentersCache.get(Integer.valueOf(i));
            a.clear();
            sLogger.v(new StringBuilder().append("widget::: cache cleared for ").append(i).toString());
        } else {
            a = new com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager$SmartDashWidgetCache(this, this.mContext);
            this.mWidgetPresentersCache.put(Integer.valueOf(i), a);
            sLogger.v(new StringBuilder().append("widget::: cache created for ").append(i).toString());
        }
        Object a0 = this.mGaugeIds.iterator();
        while(((java.util.Iterator)a0).hasNext()) {
            a.add((String)((java.util.Iterator)a0).next());
        }
        if (this.lastLifeCycleEvent != null) {
            java.util.Iterator a1 = a.getWidgetPresentersMap().values().iterator();
            this.sendLifecycleEvent(this.lastLifeCycleEvent, a1);
        }
        return a;
    }
    
    public int getIndexForWidgetIdentifier(String s) {
        return (this.mWidgetIndexMap.containsKey(s)) ? ((Integer)this.mWidgetIndexMap.get(s)).intValue() : -1;
    }
    
    public String getWidgetIdentifierForIndex(int i) {
        String s = null;
        label2: {
            label0: {
                label1: {
                    if (i < 0) {
                        break label1;
                    }
                    if (i < this.mGaugeIds.size()) {
                        break label0;
                    }
                }
                s = null;
                break label2;
            }
            s = (String)this.mGaugeIds.get(i);
        }
        return s;
    }
    
    public boolean isGaugeOn(String s) {
        if (this.currentUserPreferences == null) {
            sLogger.d("isGaugeOn : Current Preferences is not set, using the global preferences");
            this.currentUserPreferences = this.globalPreferences;
        }
        return this.currentUserPreferences.getBoolean(new StringBuilder().append("PREF_GAUGE_ENABLED_").append(s).toString(), true);
    }
    
    public void onPause() {
        this.lastLifeCycleEvent = com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager$LifecycleEvent.PAUSE;
        this.sendLifecycleEvent(com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager$LifecycleEvent.PAUSE);
    }
    
    public void onResume() {
        this.lastLifeCycleEvent = com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager$LifecycleEvent.RESUME;
        this.sendLifecycleEvent(com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager$LifecycleEvent.RESUME);
    }
    
    public void reLoadAvailableWidgets(boolean b) {
        sLogger.v("reLoadAvailableWidgets");
        this.mGaugeIds.clear();
        this.mWidgetIndexMap.clear();
        boolean b0 = com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getCurrentProfile().isDefaultProfile();
        sLogger.d(new StringBuilder().append("Default profile ? ").append(b0).toString());
        this.currentUserPreferences = b0 ? this.globalPreferences : com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getDriverProfileManager().getLocalPreferencesForCurrentDriverProfile(com.navdy.hud.app.HudApplication.getAppContext());
        Object a = GAUGES.iterator();
        int i = 0;
        while(((java.util.Iterator)a).hasNext()) {
            String s = (String)((java.util.Iterator)a).next();
            {
                label0: {
                    label1: {
                        if (b) {
                            break label1;
                        }
                        if (!this.isGaugeOn(s)) {
                            break label0;
                        }
                    }
                    if (this.filter != null && !this.filter.filter(s)) {
                        this.mGaugeIds.add(s);
                        sLogger.d(new StringBuilder().append("adding Gauge : ").append(s).append(", at : ").append(i).toString());
                        this.mWidgetIndexMap.put(s, Integer.valueOf(i));
                        i = i + 1;
                        continue;
                    }
                }
                sLogger.d(new StringBuilder().append("Not adding Gauge, as its filtered out , ID : ").append(s).toString());
            }
        }
        if (this.mGaugeIds.size() == 0) {
            this.mGaugeIds.add("EMPTY_WIDGET");
            this.mWidgetIndexMap.put("EMPTY_WIDGET", Integer.valueOf(0));
        }
        if (this.mLoaded) {
            this.bus.post(com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager$Reload.RELOAD_CACHE);
            this.bus.post(com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager$Reload.RELOADED);
        } else {
            this.mLoaded = true;
        }
    }
    
    public void registerForChanges(Object a) {
        this.bus.register(a);
    }
    
    public void setFilter(com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager$IWidgetFilter a) {
        this.filter = a;
    }
    
    public void setGaugeOn(String s, boolean b) {
        if (this.currentUserPreferences == null) {
            sLogger.d("setGaugeOn : Current Preferences is not set, using the global preferences");
            this.currentUserPreferences = this.globalPreferences;
        }
        this.currentUserPreferences.edit().putBoolean(new StringBuilder().append("PREF_GAUGE_ENABLED_").append(s).toString(), b).apply();
    }
    
    public void updateWidget(String s, Object a) {
        java.util.Collection a0 = this.mWidgetPresentersCache.values();
        if (a0 != null) {
            Object a1 = a0.iterator();
            while(((java.util.Iterator)a1).hasNext()) {
                com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager$SmartDashWidgetCache a2 = (com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager$SmartDashWidgetCache)((java.util.Iterator)a1).next();
                if (a2 != null) {
                    a2.updateWidget(s, a);
                }
            }
        }
    }
}
