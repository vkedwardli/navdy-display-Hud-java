package com.navdy.hud.app.ui.component.carousel;

class CarouselLayout$OperationInfo {
    int animationDuration;
    int count;
    Runnable runnable;
    com.navdy.hud.app.ui.component.carousel.CarouselLayout$Operation type;
    
    CarouselLayout$OperationInfo(com.navdy.hud.app.ui.component.carousel.CarouselLayout$Operation a, Runnable a0) {
        this.count = 1;
        this.animationDuration = -1;
        this.type = a;
        this.runnable = a0;
    }
    
    CarouselLayout$OperationInfo(com.navdy.hud.app.ui.component.carousel.CarouselLayout$Operation a, Runnable a0, int i) {
        this.count = 1;
        this.animationDuration = -1;
        this.type = a;
        this.runnable = a0;
        this.animationDuration = i;
    }
}
