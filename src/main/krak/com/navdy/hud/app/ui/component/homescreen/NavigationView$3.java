package com.navdy.hud.app.ui.component.homescreen;

class NavigationView$3 implements Runnable {
    final com.navdy.hud.app.ui.component.homescreen.NavigationView this$0;
    final boolean val$animate;
    final Runnable val$endAction;
    final com.here.android.mpa.common.GeoCoordinate val$geoCoordinate;
    final com.here.android.mpa.routing.Route val$route;
    final double val$zoomLevel;
    
    NavigationView$3(com.navdy.hud.app.ui.component.homescreen.NavigationView a, com.here.android.mpa.routing.Route a0, Runnable a1, com.here.android.mpa.common.GeoCoordinate a2, boolean b, double d) {
        super();
        this.this$0 = a;
        this.val$route = a0;
        this.val$endAction = a1;
        this.val$geoCoordinate = a2;
        this.val$animate = b;
        this.val$zoomLevel = d;
    }
    
    public void run() {
        long j = android.os.SystemClock.elapsedRealtime();
        boolean b = com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getDriverProfileManager().isTrafficEnabled();
        if (this.val$route != null) {
            com.navdy.hud.app.ui.component.homescreen.NavigationView.access$200(this.this$0).v(new StringBuilder().append("switchToOverviewMode:route traffic=").append(b).toString());
            if (com.navdy.hud.app.ui.component.homescreen.NavigationView.access$500(this.this$0) != null) {
                com.navdy.hud.app.ui.component.homescreen.NavigationView.access$500(this.this$0).setCoordinate(this.val$geoCoordinate);
                com.navdy.hud.app.ui.component.homescreen.NavigationView.access$000(this.this$0).addMapObject((com.here.android.mpa.mapping.MapObject)com.navdy.hud.app.ui.component.homescreen.NavigationView.access$500(this.this$0));
            }
            com.navdy.hud.app.ui.component.homescreen.NavigationView.access$200(this.this$0).v("switchToOverviewMode:map:executeSynchronized");
            com.navdy.hud.app.ui.component.homescreen.NavigationView.access$000(this.this$0).setTilt(0.0f);
            if (this.val$endAction != null) {
                com.navdy.hud.app.ui.component.homescreen.NavigationView.access$000(this.this$0).addTransformListener((com.here.android.mpa.mapping.Map$OnTransformListener)new com.navdy.hud.app.ui.component.homescreen.NavigationView$3$2(this));
            }
            com.navdy.hud.app.ui.component.homescreen.NavigationView.access$300(this.this$0, com.navdy.hud.app.maps.here.HereMapController$State.OVERVIEW);
            com.navdy.hud.app.ui.component.homescreen.NavigationView.access$000(this.this$0).zoomTo(this.val$route.getBoundingBox(), com.navdy.hud.app.ui.component.homescreen.HomeScreenConstants.routeOverviewRect, (this.val$animate) ? com.here.android.mpa.mapping.Map$Animation.BOW : com.here.android.mpa.mapping.Map$Animation.NONE, 0.0f);
            com.navdy.hud.app.ui.component.homescreen.NavigationView.access$200(this.this$0).v(new StringBuilder().append("switchToOverviewMode took [").append(android.os.SystemClock.elapsedRealtime() - j).append("]").toString());
        } else {
            com.navdy.hud.app.ui.component.homescreen.NavigationView.access$200(this.this$0).v(new StringBuilder().append("switchToOverviewMode:no route traffic=").append(b).toString());
            com.navdy.hud.app.ui.component.homescreen.NavigationView.access$300(this.this$0, com.navdy.hud.app.maps.here.HereMapController$State.OVERVIEW);
            com.navdy.hud.app.ui.component.homescreen.NavigationView.access$000(this.this$0).addTransformListener((com.here.android.mpa.mapping.Map$OnTransformListener)new com.navdy.hud.app.ui.component.homescreen.NavigationView$3$1(this));
            com.navdy.hud.app.ui.component.homescreen.NavigationView.access$000(this.this$0).setCenterForState(com.navdy.hud.app.maps.here.HereMapController$State.OVERVIEW, this.val$geoCoordinate, (this.val$animate) ? com.here.android.mpa.mapping.Map$Animation.BOW : com.here.android.mpa.mapping.Map$Animation.NONE, this.val$zoomLevel, 0.0f, 0.0f);
            if (com.navdy.hud.app.ui.component.homescreen.NavigationView.access$500(this.this$0) != null) {
                com.navdy.hud.app.ui.component.homescreen.NavigationView.access$500(this.this$0).setCoordinate(this.val$geoCoordinate);
                com.navdy.hud.app.ui.component.homescreen.NavigationView.access$000(this.this$0).addMapObject((com.here.android.mpa.mapping.MapObject)com.navdy.hud.app.ui.component.homescreen.NavigationView.access$500(this.this$0));
            }
        }
    }
}
