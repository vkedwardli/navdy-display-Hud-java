package com.navdy.hud.app.ui.component.image;

class InitialsImageView$1 {
    final static int[] $SwitchMap$com$navdy$hud$app$ui$component$image$InitialsImageView$Style;
    
    static {
        $SwitchMap$com$navdy$hud$app$ui$component$image$InitialsImageView$Style = new int[com.navdy.hud.app.ui.component.image.InitialsImageView$Style.values().length];
        int[] a = $SwitchMap$com$navdy$hud$app$ui$component$image$InitialsImageView$Style;
        com.navdy.hud.app.ui.component.image.InitialsImageView$Style a0 = com.navdy.hud.app.ui.component.image.InitialsImageView$Style.SMALL;
        try {
            a[a0.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$ui$component$image$InitialsImageView$Style[com.navdy.hud.app.ui.component.image.InitialsImageView$Style.TINY.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException0) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$ui$component$image$InitialsImageView$Style[com.navdy.hud.app.ui.component.image.InitialsImageView$Style.MEDIUM.ordinal()] = 3;
        } catch(NoSuchFieldError ignoredException1) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$ui$component$image$InitialsImageView$Style[com.navdy.hud.app.ui.component.image.InitialsImageView$Style.LARGE.ordinal()] = 4;
        } catch(NoSuchFieldError ignoredException2) {
        }
    }
}
