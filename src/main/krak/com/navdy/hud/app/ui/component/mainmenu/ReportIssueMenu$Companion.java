package com.navdy.hud.app.ui.component.mainmenu;

final public class ReportIssueMenu$Companion {
    private ReportIssueMenu$Companion() {
    }
    
    public ReportIssueMenu$Companion(kotlin.jvm.internal.DefaultConstructorMarker a) {
        this();
    }
    
    final public static com.navdy.hud.app.ui.component.vlist.VerticalList$Model access$getBack$p(com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu$Companion a) {
        return a.getBack();
    }
    
    final public static com.navdy.hud.app.ui.component.vlist.VerticalList$Model access$getDriveScore$p(com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu$Companion a) {
        return a.getDriveScore();
    }
    
    final public static com.navdy.hud.app.ui.component.vlist.VerticalList$Model access$getEtaInaccurate$p(com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu$Companion a) {
        return a.getEtaInaccurate();
    }
    
    final public static com.navdy.service.library.log.Logger access$getLogger$p(com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu$Companion a) {
        return a.getLogger();
    }
    
    final public static com.navdy.hud.app.ui.component.vlist.VerticalList$Model access$getMaps$p(com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu$Companion a) {
        return a.getMaps();
    }
    
    final public static String access$getNAV_ISSUE_SENT_TOAST_ID$p(com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu$Companion a) {
        return a.getNAV_ISSUE_SENT_TOAST_ID();
    }
    
    final public static com.navdy.hud.app.ui.component.vlist.VerticalList$Model access$getNavigation$p(com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu$Companion a) {
        return a.getNavigation();
    }
    
    final public static com.navdy.hud.app.ui.component.vlist.VerticalList$Model access$getNotFastestRoute$p(com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu$Companion a) {
        return a.getNotFastestRoute();
    }
    
    final public static com.navdy.hud.app.ui.component.vlist.VerticalList$Model access$getPermanentClosure$p(com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu$Companion a) {
        return a.getPermanentClosure();
    }
    
    final public static String access$getReportIssue$p(com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu$Companion a) {
        return a.getReportIssue();
    }
    
    final public static int access$getReportIssueColor$p(com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu$Companion a) {
        return a.getReportIssueColor();
    }
    
    final public static java.util.ArrayList access$getReportIssueItems$p(com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu$Companion a) {
        return a.getReportIssueItems();
    }
    
    final public static com.navdy.hud.app.ui.component.vlist.VerticalList$Model access$getRoadBlocked$p(com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu$Companion a) {
        return a.getRoadBlocked();
    }
    
    final public static com.navdy.hud.app.ui.component.vlist.VerticalList$Model access$getSmartDash$p(com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu$Companion a) {
        return a.getSmartDash();
    }
    
    final public static int access$getTOAST_TIMEOUT$p(com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu$Companion a) {
        return a.getTOAST_TIMEOUT();
    }
    
    final public static java.util.ArrayList access$getTakeSnapShotItems$p(com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu$Companion a) {
        return a.getTakeSnapShotItems();
    }
    
    final public static String access$getTakeSnapshot$p(com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu$Companion a) {
        return a.getTakeSnapshot();
    }
    
    final public static int access$getTakeSnapshotColor$p(com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu$Companion a) {
        return a.getTakeSnapshotColor();
    }
    
    final public static String access$getToastSentSuccessfully$p(com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu$Companion a) {
        return a.getToastSentSuccessfully();
    }
    
    final public static com.navdy.hud.app.ui.component.vlist.VerticalList$Model access$getWrongDirection$p(com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu$Companion a) {
        return a.getWrongDirection();
    }
    
    final public static com.navdy.hud.app.ui.component.vlist.VerticalList$Model access$getWrongRoadName$p(com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu$Companion a) {
        return a.getWrongRoadName();
    }
    
    final private com.navdy.hud.app.ui.component.vlist.VerticalList$Model getBack() {
        return com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu.access$getBack$cp();
    }
    
    final private com.navdy.hud.app.ui.component.vlist.VerticalList$Model getDriveScore() {
        return com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu.access$getDriveScore$cp();
    }
    
    final private com.navdy.hud.app.ui.component.vlist.VerticalList$Model getEtaInaccurate() {
        return com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu.access$getEtaInaccurate$cp();
    }
    
    final private com.navdy.service.library.log.Logger getLogger() {
        return com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu.access$getLogger$cp();
    }
    
    final private com.navdy.hud.app.ui.component.vlist.VerticalList$Model getMaps() {
        return com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu.access$getMaps$cp();
    }
    
    final private String getNAV_ISSUE_SENT_TOAST_ID() {
        return com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu.access$getNAV_ISSUE_SENT_TOAST_ID$cp();
    }
    
    final private com.navdy.hud.app.ui.component.vlist.VerticalList$Model getNavigation() {
        return com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu.access$getNavigation$cp();
    }
    
    final private com.navdy.hud.app.ui.component.vlist.VerticalList$Model getNotFastestRoute() {
        return com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu.access$getNotFastestRoute$cp();
    }
    
    final private com.navdy.hud.app.ui.component.vlist.VerticalList$Model getPermanentClosure() {
        return com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu.access$getPermanentClosure$cp();
    }
    
    final private String getReportIssue() {
        return com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu.access$getReportIssue$cp();
    }
    
    final private int getReportIssueColor() {
        return com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu.access$getReportIssueColor$cp();
    }
    
    final private java.util.ArrayList getReportIssueItems() {
        return com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu.access$getReportIssueItems$cp();
    }
    
    final private com.navdy.hud.app.ui.component.vlist.VerticalList$Model getRoadBlocked() {
        return com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu.access$getRoadBlocked$cp();
    }
    
    final private com.navdy.hud.app.ui.component.vlist.VerticalList$Model getSmartDash() {
        return com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu.access$getSmartDash$cp();
    }
    
    final private int getTOAST_TIMEOUT() {
        return com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu.access$getTOAST_TIMEOUT$cp();
    }
    
    final private java.util.ArrayList getTakeSnapShotItems() {
        return com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu.access$getTakeSnapShotItems$cp();
    }
    
    final private String getTakeSnapshot() {
        return com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu.access$getTakeSnapshot$cp();
    }
    
    final private int getTakeSnapshotColor() {
        return com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu.access$getTakeSnapshotColor$cp();
    }
    
    final private String getToastSentSuccessfully() {
        return com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu.access$getToastSentSuccessfully$cp();
    }
    
    final private com.navdy.hud.app.ui.component.vlist.VerticalList$Model getWrongDirection() {
        return com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu.access$getWrongDirection$cp();
    }
    
    final private com.navdy.hud.app.ui.component.vlist.VerticalList$Model getWrongRoadName() {
        return com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu.access$getWrongRoadName$cp();
    }
    
    final public java.util.HashMap getIdToTitleMap() {
        return com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu.access$getIdToTitleMap$cp();
    }
}
