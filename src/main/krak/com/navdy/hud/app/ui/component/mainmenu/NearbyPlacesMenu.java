package com.navdy.hud.app.ui.component.mainmenu;
import com.navdy.hud.app.R;

class NearbyPlacesMenu implements com.navdy.hud.app.ui.component.mainmenu.IMenu {
    final private static com.navdy.hud.app.ui.component.vlist.VerticalList$Model atm;
    final private static com.navdy.hud.app.ui.component.vlist.VerticalList$Model back;
    final private static com.navdy.hud.app.ui.component.vlist.VerticalList$Model coffee;
    final private static com.navdy.hud.app.ui.component.vlist.VerticalList$Model food;
    final private static com.navdy.hud.app.ui.component.vlist.VerticalList$Model gas;
    final private static com.navdy.hud.app.ui.component.vlist.VerticalList$Model groceryStore;
    final private static com.navdy.service.library.log.Logger logger;
    final private static com.navdy.hud.app.ui.component.vlist.VerticalList$Model parking;
    final private static android.content.res.Resources resources;
    final private static String search;
    final private static int searchColor;
    private int backSelection;
    private int backSelectionId;
    final private com.squareup.otto.Bus bus;
    private java.util.List cachedList;
    final private com.navdy.hud.app.framework.notifications.NotificationManager notificationManager;
    final private com.navdy.hud.app.ui.component.mainmenu.IMenu parent;
    final private com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2$Presenter presenter;
    final private com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent vscrollComponent;
    
    static {
        logger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.ui.component.mainmenu.NearbyPlacesMenu.class);
        android.content.Context a = com.navdy.hud.app.HudApplication.getAppContext();
        resources = a.getResources();
        searchColor = android.support.v4.content.ContextCompat.getColor(a, R.color.mm_search);
        search = resources.getString(R.string.carousel_menu_search_title);
        String s = resources.getString(R.string.back);
        int i = android.support.v4.content.ContextCompat.getColor(a, R.color.mm_back);
        back = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(R.id.menu_back, R.drawable.icon_mm_back, i, com.navdy.hud.app.ui.component.mainmenu.MainMenu.bkColorUnselected, i, s, (String)null);
        String s0 = resources.getString(R.string.carousel_search_gas);
        int i0 = android.support.v4.content.ContextCompat.getColor(a, R.color.mm_search_gas);
        gas = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(R.id.search_menu_gas, R.drawable.icon_place_gas, i0, com.navdy.hud.app.ui.component.mainmenu.MainMenu.bkColorUnselected, i0, s0, (String)null);
        String s1 = resources.getString(R.string.carousel_search_parking);
        int i1 = android.support.v4.content.ContextCompat.getColor(a, R.color.mm_search_parking);
        parking = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(R.id.search_menu_parking, R.drawable.icon_place_parking, i1, com.navdy.hud.app.ui.component.mainmenu.MainMenu.bkColorUnselected, i1, s1, (String)null);
        String s2 = resources.getString(R.string.carousel_search_food);
        int i2 = android.support.v4.content.ContextCompat.getColor(a, R.color.mm_search_food);
        food = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(R.id.search_menu_food, R.drawable.icon_place_restaurant, i2, com.navdy.hud.app.ui.component.mainmenu.MainMenu.bkColorUnselected, i2, s2, (String)null);
        String s3 = resources.getString(R.string.carousel_search_grocery_store);
        int i3 = android.support.v4.content.ContextCompat.getColor(a, R.color.mm_search_grocery_store);
        groceryStore = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(R.id.search_menu_grocery_store, R.drawable.icon_place_store, i3, com.navdy.hud.app.ui.component.mainmenu.MainMenu.bkColorUnselected, i3, s3, (String)null);
        String s4 = resources.getString(R.string.carousel_search_coffee);
        int i4 = android.support.v4.content.ContextCompat.getColor(a, R.color.mm_search_coffee);
        coffee = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(R.id.search_menu_coffee, R.drawable.icon_place_coffee, i4, com.navdy.hud.app.ui.component.mainmenu.MainMenu.bkColorUnselected, i4, s4, (String)null);
        String s5 = resources.getString(R.string.carousel_search_atm);
        int i5 = android.support.v4.content.ContextCompat.getColor(a, R.color.mm_search_atm);
        atm = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(R.id.search_menu_atm, R.drawable.icon_place_a_t_m, i5, com.navdy.hud.app.ui.component.mainmenu.MainMenu.bkColorUnselected, i5, s5, (String)null);
    }
    
    NearbyPlacesMenu(com.squareup.otto.Bus a, com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent a0, com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2$Presenter a1, com.navdy.hud.app.ui.component.mainmenu.IMenu a2) {
        this.bus = a;
        this.vscrollComponent = a0;
        this.presenter = a1;
        this.parent = a2;
        this.notificationManager = com.navdy.hud.app.framework.notifications.NotificationManager.getInstance();
    }
    
    static com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2$Presenter access$000(com.navdy.hud.app.ui.component.mainmenu.NearbyPlacesMenu a) {
        return a.presenter;
    }
    
    static com.navdy.service.library.log.Logger access$100() {
        return logger;
    }
    
    static com.navdy.hud.app.framework.notifications.NotificationManager access$200(com.navdy.hud.app.ui.component.mainmenu.NearbyPlacesMenu a) {
        return a.notificationManager;
    }
    
    private void launchSearch(com.navdy.service.library.events.places.PlaceType a) {
        if (com.navdy.hud.app.maps.here.HereMapsManager.getInstance().isInitialized()) {
            this.presenter.performSelectionAnimation((Runnable)new com.navdy.hud.app.ui.component.mainmenu.NearbyPlacesMenu$2(this, a));
        } else {
            logger.w("Here maps engine not initialized, exit");
            this.presenter.performSelectionAnimation((Runnable)new com.navdy.hud.app.ui.component.mainmenu.NearbyPlacesMenu$1(this));
        }
    }
    
    public com.navdy.hud.app.ui.component.mainmenu.IMenu getChildMenu(com.navdy.hud.app.ui.component.mainmenu.IMenu a, String s, String s0) {
        return null;
    }
    
    public int getInitialSelection() {
        return 1;
    }
    
    public java.util.List getItems() {
        Object a = null;
        if (this.cachedList == null) {
            java.util.ArrayList a0 = new java.util.ArrayList();
            ((java.util.List)a0).add(back);
            ((java.util.List)a0).add(gas);
            ((java.util.List)a0).add(parking);
            ((java.util.List)a0).add(food);
            ((java.util.List)a0).add(groceryStore);
            ((java.util.List)a0).add(coffee);
            ((java.util.List)a0).add(atm);
            this.cachedList = (java.util.List)a0;
            a = a0;
        } else {
            a = this.cachedList;
        }
        return (java.util.List)a;
    }
    
    public com.navdy.hud.app.ui.component.vlist.VerticalList$Model getModelfromPos(int i) {
        com.navdy.hud.app.ui.component.vlist.VerticalList$Model a = null;
        java.util.List a0 = this.cachedList;
        label2: {
            label0: {
                label1: {
                    if (a0 == null) {
                        break label1;
                    }
                    if (this.cachedList.size() > i) {
                        break label0;
                    }
                }
                a = null;
                break label2;
            }
            a = (com.navdy.hud.app.ui.component.vlist.VerticalList$Model)this.cachedList.get(i);
        }
        return a;
    }
    
    public com.navdy.hud.app.ui.component.vmenu.VerticalFastScrollIndex getScrollIndex() {
        return null;
    }
    
    public com.navdy.hud.app.ui.component.mainmenu.IMenu$Menu getType() {
        return com.navdy.hud.app.ui.component.mainmenu.IMenu$Menu.SEARCH;
    }
    
    public boolean isBindCallsEnabled() {
        return false;
    }
    
    public boolean isFirstItemEmpty() {
        return true;
    }
    
    public boolean isItemClickable(int i, int i0) {
        return true;
    }
    
    public void onBindToView(com.navdy.hud.app.ui.component.vlist.VerticalList$Model a, android.view.View a0, int i, com.navdy.hud.app.ui.component.vlist.VerticalList$ModelState a1) {
    }
    
    public void onFastScrollEnd() {
    }
    
    public void onFastScrollStart() {
    }
    
    public void onItemSelected(com.navdy.hud.app.ui.component.vlist.VerticalList$ItemSelectionState a) {
    }
    
    public void onScrollIdle() {
    }
    
    public void onUnload(com.navdy.hud.app.ui.component.mainmenu.IMenu$MenuLevel a) {
    }
    
    public boolean selectItem(com.navdy.hud.app.ui.component.vlist.VerticalList$ItemSelectionState a) {
        logger.v(new StringBuilder().append("select id:").append(a.id).append(" pos:").append(a.pos).toString());
        switch(a.id) {
            case R.id.search_menu_parking: {
                logger.v("parking");
                this.launchSearch(com.navdy.service.library.events.places.PlaceType.PLACE_TYPE_PARKING);
                break;
            }
            case R.id.search_menu_hospital: {
                logger.v("hospital");
                this.launchSearch(com.navdy.service.library.events.places.PlaceType.PLACE_TYPE_HOSPITAL);
                break;
            }
            case R.id.search_menu_grocery_store: {
                logger.v("grocery store");
                this.launchSearch(com.navdy.service.library.events.places.PlaceType.PLACE_TYPE_STORE);
                break;
            }
            case R.id.search_menu_gas: {
                logger.v("gas");
                this.launchSearch(com.navdy.service.library.events.places.PlaceType.PLACE_TYPE_GAS);
                break;
            }
            case R.id.search_menu_food: {
                logger.v("food");
                this.launchSearch(com.navdy.service.library.events.places.PlaceType.PLACE_TYPE_RESTAURANT);
                break;
            }
            case R.id.search_menu_coffee: {
                logger.v("coffee");
                this.launchSearch(com.navdy.service.library.events.places.PlaceType.PLACE_TYPE_COFFEE);
                break;
            }
            case R.id.search_menu_atm: {
                logger.v("atm");
                this.launchSearch(com.navdy.service.library.events.places.PlaceType.PLACE_TYPE_ATM);
                break;
            }
            case R.id.menu_back: {
                logger.v("back");
                com.navdy.hud.app.analytics.AnalyticsSupport.recordNearbySearchReturnMainMenu();
                this.presenter.loadMenu(this.parent, com.navdy.hud.app.ui.component.mainmenu.IMenu$MenuLevel.BACK_TO_PARENT, this.backSelection, this.backSelectionId);
                break;
            }
        }
        return false;
    }
    
    public void setBackSelectionId(int i) {
        this.backSelectionId = i;
    }
    
    public void setBackSelectionPos(int i) {
        this.backSelection = i;
    }
    
    public void setSelectedIcon() {
        this.vscrollComponent.setSelectedIconColorImage(R.drawable.icon_mm_search_2, searchColor, (android.graphics.Shader)null, 1f);
        this.vscrollComponent.selectedText.setText((CharSequence)search);
    }
    
    public void showToolTip() {
    }
}
