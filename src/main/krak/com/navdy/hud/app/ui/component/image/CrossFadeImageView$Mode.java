package com.navdy.hud.app.ui.component.image;


public enum CrossFadeImageView$Mode {
    BIG(0),
    SMALL(1);

    private int value;
    CrossFadeImageView$Mode(int value) {
        this.value = value;
    }
    public int getValue() {
        return value;
    }
}

//final public class CrossFadeImageView$Mode extends Enum {
//    final private static com.navdy.hud.app.ui.component.image.CrossFadeImageView$Mode[] $VALUES;
//    final public static com.navdy.hud.app.ui.component.image.CrossFadeImageView$Mode BIG;
//    final public static com.navdy.hud.app.ui.component.image.CrossFadeImageView$Mode SMALL;
//    
//    static {
//        BIG = new com.navdy.hud.app.ui.component.image.CrossFadeImageView$Mode("BIG", 0);
//        SMALL = new com.navdy.hud.app.ui.component.image.CrossFadeImageView$Mode("SMALL", 1);
//        com.navdy.hud.app.ui.component.image.CrossFadeImageView$Mode[] a = new com.navdy.hud.app.ui.component.image.CrossFadeImageView$Mode[2];
//        a[0] = BIG;
//        a[1] = SMALL;
//        $VALUES = a;
//    }
//    
//    private CrossFadeImageView$Mode(String s, int i) {
//        super(s, i);
//    }
//    
//    public static com.navdy.hud.app.ui.component.image.CrossFadeImageView$Mode valueOf(String s) {
//        return (com.navdy.hud.app.ui.component.image.CrossFadeImageView$Mode)Enum.valueOf(com.navdy.hud.app.ui.component.image.CrossFadeImageView$Mode.class, s);
//    }
//    
//    public static com.navdy.hud.app.ui.component.image.CrossFadeImageView$Mode[] values() {
//        return $VALUES.clone();
//    }
//}
//