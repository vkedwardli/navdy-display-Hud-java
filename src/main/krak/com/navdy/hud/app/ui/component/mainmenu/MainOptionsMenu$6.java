package com.navdy.hud.app.ui.component.mainmenu;

class MainOptionsMenu$6 implements Runnable {
    final com.navdy.hud.app.ui.component.mainmenu.MainOptionsMenu this$0;
    
    MainOptionsMenu$6(com.navdy.hud.app.ui.component.mainmenu.MainOptionsMenu a) {
        super();
        this.this$0 = a;
    }
    
    public void run() {
        com.navdy.hud.app.ui.component.mainmenu.MainOptionsMenu.access$000(this.this$0).close();
        com.navdy.hud.app.ui.component.homescreen.SmartDashView a = com.navdy.hud.app.ui.component.mainmenu.MainOptionsMenu.access$300(this.this$0).getSmartDashView();
        if (a != null) {
            a.onScrollableSideOptionSelected();
        }
    }
}
