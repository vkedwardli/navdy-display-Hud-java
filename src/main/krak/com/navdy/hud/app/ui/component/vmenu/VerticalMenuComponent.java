package com.navdy.hud.app.ui.component.vmenu;
import com.navdy.hud.app.R;

public class VerticalMenuComponent {
    final public static int ANIMATION_IN_OUT_DURATION = 150;
    final public static int CLICK_ANIMATION_DURATION = 50;
    final public static int CLOSE_ANIMATION_DURATION = 150;
    final private static int FAST_SCROLL_APPEARANCE_ANIM_TIME = 50;
    final private static int FAST_SCROLL_CLOSE_MENU_WAIT_PERIOD = 150;
    final private static int FAST_SCROLL_EXIT_DELAY = 800;
    final private static int FAST_SCROLL_TRIGGER_CLICKS = 10;
    final private static int FAST_SCROLL_TRIGGER_TIME;
    final private static int INDICATOR_FADE_IDLE_DURATION = 2000;
    final private static int INDICATOR_FADE_IN_DURATION = 500;
    final private static int INDICATOR_FADE_OUT_DURATION = 500;
    final private static float MAIN_TO_SELECTED_ICON_SCALE = 1f;
    final public static int MIN_FAST_SCROLL_ITEM = 40;
    final public static int MIN_INDEX_ENTRY_COUNT = 2;
    final private static int NO_ANIMATION = -1;
    final private static float SELECTED_TO_MAIN_ICON_SCALE = 1.2f;
    final public static int animateTranslateY;
    final private static int closeContainerHeight;
    final private static int closeContainerScrollY;
    final private static int closeHaloColor;
    final private static int indicatorY;
    final private static int[] location;
    final private static com.navdy.service.library.log.Logger sLogger;
    private android.view.animation.Interpolator accelerateInterpolator;
    private android.widget.ImageView animImageView;
    private android.widget.TextView animSubTitle;
    private android.widget.TextView animSubTitle2;
    private android.widget.TextView animTitle;
    private com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent$Callback callback;
    private int clickCount;
    private long clickTime;
    @InjectView(R.id.closeContainer)
    public android.view.ViewGroup closeContainer;
    @InjectView(R.id.closeContainerScrim)
    public android.view.View closeContainerScrim;
    @InjectView(R.id.closeHalo)
    public com.navdy.hud.app.ui.component.HaloView closeHalo;
    @InjectView(R.id.closeIconContainer)
    public android.view.ViewGroup closeIconContainer;
    private Runnable closeIconEndAction;
    private boolean closeMenuAnimationActive;
    private com.navdy.hud.app.ui.framework.DefaultAnimationListener closeMenuHideListener;
    private com.navdy.hud.app.ui.framework.DefaultAnimationListener closeMenuShowListener;
    private com.navdy.hud.app.ui.component.vlist.VerticalList$ContainerCallback containerCallback;
    private android.view.animation.Interpolator decelerateInterpolator;
    private Runnable fadeOutRunnable;
    final private int fastScrollCloseWaitPeriod;
    @InjectView(R.id.fastScrollView)
    public android.view.ViewGroup fastScrollContainer;
    private int fastScrollCurrentItem;
    private com.navdy.hud.app.ui.framework.DefaultAnimationListener fastScrollIn;
    private com.navdy.hud.app.ui.component.vmenu.VerticalFastScrollIndex fastScrollIndex;
    private int fastScrollOffset;
    private com.navdy.hud.app.ui.framework.DefaultAnimationListener fastScrollOut;
    @InjectView(R.id.fastScrollText)
    public android.widget.TextView fastScrollText;
    private Runnable fastScrollTimeout;
    private android.os.Handler handler;
    @InjectView(R.id.indicator)
    public com.navdy.hud.app.ui.component.carousel.CarouselIndicator indicator;
    private long lastUpEvent;
    @InjectView(R.id.leftContainer)
    public android.view.ViewGroup leftContainer;
    private android.view.animation.Interpolator linearInterpolator;
    private boolean listLoaded;
    private java.util.HashSet overrideDefaultKeyEvents;
    private android.view.ViewGroup parentContainer;
    @InjectView(R.id.recyclerView)
    public com.navdy.hud.app.ui.component.vlist.VerticalRecyclerView recyclerView;
    @InjectView(R.id.rightContainer)
    public android.view.ViewGroup rightContainer;
    private com.navdy.hud.app.ui.component.vlist.VerticalList$Direction scrollDirection;
    @InjectView(R.id.selectedCustomView)
    public android.widget.FrameLayout selectedCustomView;
    @InjectView(R.id.selectedIconColorImage)
    public com.navdy.hud.app.ui.component.image.IconColorImageView selectedIconColorImage;
    @InjectView(R.id.selectedIconImage)
    public com.navdy.hud.app.ui.component.image.InitialsImageView selectedIconImage;
    @InjectView(R.id.selectedImage)
    public android.view.ViewGroup selectedImage;
    @InjectView(R.id.selectedText)
    public android.widget.TextView selectedText;
    private boolean stoppingFastScrolling;
    private java.util.HashMap sublistAnimationCache;
    @InjectView(R.id.tooltip)
    public com.navdy.hud.app.view.ToolTipView toolTip;
    public com.navdy.hud.app.ui.component.vlist.VerticalList verticalList;
    private com.navdy.hud.app.ui.component.vlist.VerticalList$Callback vlistCallback;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger("VerticalMenuC");
        FAST_SCROLL_TRIGGER_TIME = (int)java.util.concurrent.TimeUnit.SECONDS.toMillis(2L);
        location = new int[2];
        android.content.res.Resources a = com.navdy.hud.app.HudApplication.getAppContext().getResources();
        closeContainerHeight = a.getDimensionPixelSize(R.dimen.vmenu_close_height);
        closeContainerScrollY = a.getDimensionPixelSize(R.dimen.vmenu_close_scroll_y);
        indicatorY = a.getDimensionPixelSize(R.dimen.vmenu_indicator_y);
        animateTranslateY = a.getDimensionPixelSize(R.dimen.vmenu_anim_translate_y);
        closeHaloColor = a.getColor(R.color.close_halo);
    }
    
    public VerticalMenuComponent(android.view.ViewGroup a, com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent$Callback a0, boolean b) {
        this.linearInterpolator = (android.view.animation.Interpolator)new android.view.animation.LinearInterpolator();
        this.decelerateInterpolator = (android.view.animation.Interpolator)new android.view.animation.DecelerateInterpolator();
        this.accelerateInterpolator = (android.view.animation.Interpolator)new android.view.animation.AccelerateInterpolator();
        this.overrideDefaultKeyEvents = new java.util.HashSet();
        this.closeMenuShowListener = new com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent$1(this);
        this.fastScrollIn = new com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent$2(this);
        this.fastScrollOut = new com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent$3(this);
        this.fastScrollTimeout = (Runnable)new com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent$4(this);
        this.closeMenuHideListener = new com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent$5(this);
        this.vlistCallback = (com.navdy.hud.app.ui.component.vlist.VerticalList$Callback)new com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent$6(this);
        this.containerCallback = (com.navdy.hud.app.ui.component.vlist.VerticalList$ContainerCallback)new com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent$7(this);
        this.fadeOutRunnable = (Runnable)new com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent$8(this);
        this.closeIconEndAction = (Runnable)new com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent$9(this);
        this.handler = new android.os.Handler();
        this.sublistAnimationCache = new java.util.HashMap();
        if (a != null && a0 != null) {
            this.fastScrollCloseWaitPeriod = com.navdy.hud.app.ui.component.UISettings.isVerticalListNoCloseTimeout() ? 0 : 150;
            this.callback = a0;
            butterknife.ButterKnife.inject(this, (android.view.View)a);
            this.parentContainer = a;
            this.closeContainer.setY((float)(-closeContainerHeight));
            this.indicator.setY((float)indicatorY);
            this.fadeoutIndicator(-1);
            this.verticalList = new com.navdy.hud.app.ui.component.vlist.VerticalList(this.recyclerView, this.indicator, this.vlistCallback, this.containerCallback, b);
            this.closeHalo.setStrokeColor(closeHaloColor);
            this.toolTip.setParams(com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.mainIconSize, com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.iconMargin, 4);
            android.content.Context a1 = a.getContext();
            this.animImageView = new android.widget.ImageView(a1);
            this.animImageView.setVisibility(4);
            this.parentContainer.addView((android.view.View)this.animImageView, (android.view.ViewGroup$LayoutParams)new android.view.ViewGroup$MarginLayoutParams(com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.selectedIconSize, com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.selectedIconSize));
            this.animTitle = new android.widget.TextView(a1);
            this.animTitle.setTextAppearance(a1, R.style.vlist_title);
            this.animTitle.setVisibility(4);
            this.parentContainer.addView((android.view.View)this.animTitle, (android.view.ViewGroup$LayoutParams)new android.view.ViewGroup$MarginLayoutParams(-2, -2));
            this.animSubTitle = new android.widget.TextView(a1);
            this.animSubTitle.setTextAppearance(a1, R.style.vlist_subtitle);
            this.animSubTitle.setVisibility(4);
            this.parentContainer.addView((android.view.View)this.animSubTitle, (android.view.ViewGroup$LayoutParams)new android.view.ViewGroup$MarginLayoutParams(-2, -2));
            this.animSubTitle2 = new android.widget.TextView(a1);
            this.animSubTitle2.setTextAppearance(a1, R.style.vlist_subtitle);
            this.animSubTitle2.setVisibility(4);
            this.parentContainer.addView((android.view.View)this.animSubTitle2, (android.view.ViewGroup$LayoutParams)new android.view.ViewGroup$MarginLayoutParams(-2, -2));
            return;
        }
        throw new IllegalArgumentException();
    }
    
    static com.navdy.service.library.log.Logger access$000() {
        return sLogger;
    }
    
    static android.widget.ImageView access$1000(com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent a) {
        return a.animImageView;
    }
    
    static boolean access$102(com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent a, boolean b) {
        a.closeMenuAnimationActive = b;
        return b;
    }
    
    static android.widget.TextView access$1100(com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent a) {
        return a.animTitle;
    }
    
    static android.widget.TextView access$1200(com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent a) {
        return a.animSubTitle;
    }
    
    static android.widget.TextView access$1300(com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent a) {
        return a.animSubTitle2;
    }
    
    static Runnable access$200(com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent a) {
        return a.fastScrollTimeout;
    }
    
    static android.os.Handler access$300(com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent a) {
        return a.handler;
    }
    
    static boolean access$402(com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent a, boolean b) {
        a.stoppingFastScrolling = b;
        return b;
    }
    
    static void access$500(com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent a) {
        a.startFadeOut();
    }
    
    static void access$600(com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent a) {
        a.stopFastScrolling();
    }
    
    static com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent$Callback access$700(com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent a) {
        return a.callback;
    }
    
    static boolean access$802(com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent a, boolean b) {
        a.listLoaded = b;
        return b;
    }
    
    static boolean access$900(com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent a) {
        return a.isFastScrollVisible();
    }
    
    private void addToCache(android.widget.ImageView a, com.navdy.hud.app.ui.component.vlist.VerticalList$Model a0) {
        if (a0 != null) {
            android.graphics.drawable.BitmapDrawable a1 = (android.graphics.drawable.BitmapDrawable)a.getDrawable();
            if (a1 != null) {
                android.graphics.Bitmap a2 = a1.getBitmap();
                if (a2 != null) {
                    this.sublistAnimationCache.put(Integer.valueOf(System.identityHashCode(a0)), a2);
                }
            }
        }
    }
    
    private void changeScrollIndex(int i) {
        this.handler.removeCallbacks(this.fastScrollTimeout);
        int i0 = this.fastScrollCurrentItem + i;
        label1: {
            label0: {
                if (i0 < 0) {
                    break label0;
                }
                if (i0 >= this.fastScrollIndex.length) {
                    break label0;
                }
                String s = this.fastScrollIndex.getTitle(i0);
                this.fastScrollText.setText((CharSequence)s);
                this.fastScrollCurrentItem = i0;
                int i1 = this.fastScrollIndex.getPosition(s);
                this.indicator.setCurrentItem(i1);
                break label1;
            }
            String s0 = this.fastScrollIndex.getTitle(this.fastScrollCurrentItem);
            int i2 = this.fastScrollIndex.getPosition(s0);
            if (i2 != this.indicator.getCurrentItem()) {
                this.indicator.setCurrentItem(i2);
            }
        }
        this.handler.postDelayed(this.fastScrollTimeout, 800L);
    }
    
    private boolean checkFastScroll(com.navdy.hud.app.ui.component.vlist.VerticalList$Direction a, int i, long j) {
        boolean b = false;
        label2: if (this.stoppingFastScrolling) {
            b = true;
        } else if (this.isFastScrollVisible()) {
            this.changeScrollIndex(i);
            b = true;
        } else {
            com.navdy.hud.app.ui.component.vlist.VerticalList$Direction a0 = this.scrollDirection;
            label3: {
                label5: {
                    label6: {
                        if (a0 == a) {
                            break label6;
                        }
                        this.scrollDirection = a;
                        this.clickCount = 0;
                        this.clickTime = j;
                        break label5;
                    }
                    long j0 = j - this.clickTime;
                    long j1 = (long)FAST_SCROLL_TRIGGER_TIME;
                    int i0 = (j0 < j1) ? -1 : (j0 == j1) ? 0 : 1;
                    label4: {
                        if (i0 <= 0) {
                            break label4;
                        }
                        this.clickTime = j;
                        this.clickCount = 1;
                        break label5;
                    }
                    this.clickCount = this.clickCount + 1;
                    if (this.clickCount >= 10) {
                        break label3;
                    }
                }
                b = false;
                break label2;
            }
            int i1 = this.verticalList.getCurrentPosition();
            this.fastScrollCurrentItem = this.fastScrollIndex.getIndexForPosition(i1);
            com.navdy.hud.app.ui.component.vlist.VerticalList$Direction a1 = this.scrollDirection;
            com.navdy.hud.app.ui.component.vlist.VerticalList$Direction a2 = com.navdy.hud.app.ui.component.vlist.VerticalList$Direction.DOWN;
            label0: {
                label1: {
                    if (a1 != a2) {
                        break label1;
                    }
                    if (this.fastScrollCurrentItem == this.fastScrollIndex.getEntryCount() - 1) {
                        break label0;
                    }
                }
                this.startFastScrolling();
                this.fadeinIndicator(-1);
                this.handler.removeCallbacks(this.fadeOutRunnable);
                b = true;
                break label2;
            }
            sLogger.v("checkFastScroll: not allowed,last element");
            this.clickCount = 0;
            this.clickTime = j;
            b = false;
        }
        return b;
    }
    
    private android.graphics.Bitmap getFromCache(com.navdy.hud.app.ui.component.vlist.VerticalList$Model a) {
        return (a != null) ? (android.graphics.Bitmap)this.sublistAnimationCache.get(Integer.valueOf(System.identityHashCode(a))) : null;
    }
    
    private void hideCloseMenu() {
        if (this.isCloseMenuVisible()) {
            sLogger.v("hideCloseMenu");
            android.animation.AnimatorSet a = new android.animation.AnimatorSet();
            a.setDuration(150L);
            a.setInterpolator((android.animation.TimeInterpolator)this.linearInterpolator);
            a.addListener((android.animation.Animator$AnimatorListener)this.closeMenuHideListener);
            android.animation.Animator[] a0 = new android.animation.Animator[5];
            android.view.ViewGroup a1 = this.closeContainer;
            android.util.Property a2 = android.view.View.Y;
            float[] a3 = new float[1];
            a3[0] = (float)(-closeContainerHeight);
            a0[0] = android.animation.ObjectAnimator.ofFloat(a1, a2, a3);
            android.view.View a4 = this.closeContainerScrim;
            android.util.Property a5 = android.view.View.ALPHA;
            float[] a6 = new float[1];
            a6[0] = 0.0f;
            a0[1] = android.animation.ObjectAnimator.ofFloat(a4, a5, a6);
            android.view.ViewGroup a7 = this.leftContainer;
            android.util.Property a8 = android.view.View.Y;
            float[] a9 = new float[1];
            a9[0] = 0.0f;
            a0[2] = android.animation.ObjectAnimator.ofFloat(a7, a8, a9);
            android.view.ViewGroup a10 = this.rightContainer;
            android.util.Property a11 = android.view.View.Y;
            float[] a12 = new float[1];
            a12[0] = 0.0f;
            a0[3] = android.animation.ObjectAnimator.ofFloat(a10, a11, a12);
            com.navdy.hud.app.ui.component.carousel.CarouselIndicator a13 = this.indicator;
            android.util.Property a14 = android.view.View.Y;
            float[] a15 = new float[1];
            a15[0] = (float)indicatorY;
            a0[4] = android.animation.ObjectAnimator.ofFloat(a13, a14, a15);
            a.playTogether(a0);
            this.closeMenuAnimationActive = true;
            a.start();
            this.verticalList.animate(this.verticalList.getRawPosition(), true, 150, false, true);
        }
    }
    
    private boolean isFastScrollAvailable() {
        return this.fastScrollIndex != null;
    }
    
    private boolean isFastScrollVisible() {
        return this.fastScrollContainer.getVisibility() == 0;
    }
    
    private void selectFastScroll() {
        sLogger.v("selectFastScroll");
        this.handler.removeCallbacks(this.fastScrollTimeout);
        this.fastScrollTimeout.run();
    }
    
    private void showCloseMenu() {
        if (!this.isCloseMenuVisible()) {
            sLogger.v("showCloseMenu");
            android.animation.AnimatorSet a = new android.animation.AnimatorSet();
            a.setDuration(150L);
            a.setInterpolator((android.animation.TimeInterpolator)this.linearInterpolator);
            a.addListener((android.animation.Animator$AnimatorListener)this.closeMenuShowListener);
            android.animation.Animator[] a0 = new android.animation.Animator[5];
            android.view.ViewGroup a1 = this.closeContainer;
            android.util.Property a2 = android.view.View.Y;
            float[] a3 = new float[1];
            a3[0] = 0.0f;
            a0[0] = android.animation.ObjectAnimator.ofFloat(a1, a2, a3);
            android.view.View a4 = this.closeContainerScrim;
            android.util.Property a5 = android.view.View.ALPHA;
            float[] a6 = new float[1];
            a6[0] = 1f;
            a0[1] = android.animation.ObjectAnimator.ofFloat(a4, a5, a6);
            android.view.ViewGroup a7 = this.leftContainer;
            android.util.Property a8 = android.view.View.Y;
            float[] a9 = new float[1];
            a9[0] = (float)closeContainerScrollY;
            a0[2] = android.animation.ObjectAnimator.ofFloat(a7, a8, a9);
            android.view.ViewGroup a10 = this.rightContainer;
            android.util.Property a11 = android.view.View.Y;
            float[] a12 = new float[1];
            a12[0] = (float)closeContainerScrollY;
            a0[3] = android.animation.ObjectAnimator.ofFloat(a10, a11, a12);
            com.navdy.hud.app.ui.component.carousel.CarouselIndicator a13 = this.indicator;
            android.util.Property a14 = android.view.View.Y;
            float[] a15 = new float[1];
            a15[0] = (float)(indicatorY + closeContainerScrollY);
            a0[4] = android.animation.ObjectAnimator.ofFloat(a13, a14, a15);
            a.playTogether(a0);
            this.closeMenuAnimationActive = true;
            a.start();
            this.verticalList.animate(this.verticalList.getRawPosition(), false, 150, true, true);
            this.fadeoutIndicator(150);
        }
    }
    
    private void startFadeOut() {
        this.handler.removeCallbacks(this.fadeOutRunnable);
        this.handler.postDelayed(this.fadeOutRunnable, 2000L);
    }
    
    private void startFastScrolling() {
        if (this.isFastScrollAvailable()) {
            this.verticalList.lock();
            String s = this.fastScrollIndex.getTitle(this.fastScrollCurrentItem);
            sLogger.v(new StringBuilder().append("startFastScrolling:").append(s).toString());
            this.fastScrollContainer.setAlpha(0.0f);
            this.fastScrollContainer.setVisibility(0);
            this.fastScrollText.setText((CharSequence)s);
            this.stoppingFastScrolling = false;
            this.callback.onFastScrollStart();
            this.fastScrollContainer.animate().alpha(1f).setDuration(50L).setListener((android.animation.Animator$AnimatorListener)this.fastScrollIn).start();
        }
    }
    
    private void stopFastScrolling() {
        if (this.isFastScrollAvailable()) {
            String s = String.valueOf(this.fastScrollText.getText());
            int i = this.fastScrollIndex.getPosition(s);
            if (this.verticalList.isFirstEntryBlank()) {
                i = i + 1;
            }
            if (this.fastScrollIndex != null) {
                i = i + this.fastScrollIndex.getOffset();
            }
            sLogger.v(new StringBuilder().append("stopFastScrolling:").append(i).append(" , ").append(s).toString());
            this.callback.onFastScrollEnd();
            this.stoppingFastScrolling = true;
            this.verticalList.unlock();
            this.verticalList.scrollToPosition(i);
            this.fastScrollContainer.animate().alpha(0.0f).setDuration(50L).setListener((android.animation.Animator$AnimatorListener)this.fastScrollOut).start();
        }
    }
    
    public void animateIn(android.animation.Animator$AnimatorListener a) {
        sLogger.v("animateIn");
        android.animation.AnimatorSet a0 = new android.animation.AnimatorSet();
        a0.setDuration(150L);
        a0.setInterpolator((android.animation.TimeInterpolator)this.decelerateInterpolator);
        if (a != null) {
            a0.addListener(a);
        }
        android.util.Property a1 = android.view.View.TRANSLATION_Y;
        float[] a2 = new float[2];
        a2[0] = (float)animateTranslateY;
        a2[1] = 0.0f;
        android.animation.PropertyValuesHolder a3 = android.animation.PropertyValuesHolder.ofFloat(a1, a2);
        android.util.Property a4 = android.view.View.ALPHA;
        float[] a5 = new float[1];
        a5[0] = 1f;
        android.animation.PropertyValuesHolder a6 = android.animation.PropertyValuesHolder.ofFloat(a4, a5);
        android.util.Property a7 = android.view.View.TRANSLATION_Y;
        float[] a8 = new float[2];
        a8[0] = (float)animateTranslateY;
        a8[1] = 0.0f;
        android.animation.PropertyValuesHolder a9 = android.animation.PropertyValuesHolder.ofFloat(a7, a8);
        android.util.Property a10 = android.view.View.ALPHA;
        float[] a11 = new float[1];
        a11[0] = 1f;
        android.animation.PropertyValuesHolder a12 = android.animation.PropertyValuesHolder.ofFloat(a10, a11);
        android.animation.Animator[] a13 = new android.animation.Animator[2];
        android.view.ViewGroup a14 = this.leftContainer;
        android.animation.PropertyValuesHolder[] a15 = new android.animation.PropertyValuesHolder[2];
        a15[0] = a3;
        a15[1] = a6;
        a13[0] = android.animation.ObjectAnimator.ofPropertyValuesHolder(a14, a15);
        android.view.ViewGroup a16 = this.rightContainer;
        android.animation.PropertyValuesHolder[] a17 = new android.animation.PropertyValuesHolder[2];
        a17[0] = a9;
        a17[1] = a12;
        a13[1] = android.animation.ObjectAnimator.ofPropertyValuesHolder(a16, a17);
        a0.playTogether(a13);
        a0.start();
    }
    
    public void animateOut(android.animation.Animator$AnimatorListener a) {
        sLogger.v("animateOut");
        this.indicator.setVisibility(4);
        android.animation.AnimatorSet a0 = new android.animation.AnimatorSet();
        a0.setDuration(150L);
        a0.setInterpolator((android.animation.TimeInterpolator)this.accelerateInterpolator);
        if (a != null) {
            a0.addListener(a);
        }
        android.util.Property a1 = android.view.View.Y;
        float[] a2 = new float[1];
        a2[0] = this.leftContainer.getY() + (float)animateTranslateY;
        android.animation.PropertyValuesHolder a3 = android.animation.PropertyValuesHolder.ofFloat(a1, a2);
        android.util.Property a4 = android.view.View.ALPHA;
        float[] a5 = new float[1];
        a5[0] = 0.0f;
        android.animation.PropertyValuesHolder a6 = android.animation.PropertyValuesHolder.ofFloat(a4, a5);
        android.util.Property a7 = android.view.View.Y;
        float[] a8 = new float[1];
        a8[0] = this.rightContainer.getY() + (float)animateTranslateY;
        android.animation.PropertyValuesHolder a9 = android.animation.PropertyValuesHolder.ofFloat(a7, a8);
        android.util.Property a10 = android.view.View.ALPHA;
        float[] a11 = new float[1];
        a11[0] = 0.0f;
        android.animation.PropertyValuesHolder a12 = android.animation.PropertyValuesHolder.ofFloat(a10, a11);
        android.util.Property a13 = android.view.View.TRANSLATION_Y;
        float[] a14 = new float[1];
        a14[0] = (float)animateTranslateY;
        android.animation.PropertyValuesHolder a15 = android.animation.PropertyValuesHolder.ofFloat(a13, a14);
        android.util.Property a16 = android.view.View.ALPHA;
        float[] a17 = new float[1];
        a17[0] = 0.0f;
        android.animation.PropertyValuesHolder a18 = android.animation.PropertyValuesHolder.ofFloat(a16, a17);
        android.animation.Animator[] a19 = new android.animation.Animator[3];
        android.view.ViewGroup a20 = this.leftContainer;
        android.animation.PropertyValuesHolder[] a21 = new android.animation.PropertyValuesHolder[2];
        a21[0] = a3;
        a21[1] = a6;
        a19[0] = android.animation.ObjectAnimator.ofPropertyValuesHolder(a20, a21);
        android.view.ViewGroup a22 = this.rightContainer;
        android.animation.PropertyValuesHolder[] a23 = new android.animation.PropertyValuesHolder[2];
        a23[0] = a9;
        a23[1] = a12;
        a19[1] = android.animation.ObjectAnimator.ofPropertyValuesHolder(a22, a23);
        android.view.ViewGroup a24 = this.closeContainer;
        android.animation.PropertyValuesHolder[] a25 = new android.animation.PropertyValuesHolder[2];
        a25[0] = a15;
        a25[1] = a18;
        a19[2] = android.animation.ObjectAnimator.ofPropertyValuesHolder(a24, a25);
        a0.playTogether(a19);
        a0.start();
    }
    
    public void clear() {
        this.sublistAnimationCache.clear();
        this.verticalList.unlock(false);
        this.verticalList.clearAllAnimations();
    }
    
    public void fadeinIndicator(int i) {
        sLogger.v(new StringBuilder().append("fadeinIndicator:").append(i).toString());
        if (i != -1) {
            if (this.indicator.getAlpha() != 1f) {
                this.indicator.animate().alpha(1f).setDuration((long)i).start();
            }
        } else {
            this.indicator.setAlpha(1f);
        }
    }
    
    public void fadeoutIndicator(int i) {
        if (i != -1) {
            if (this.indicator.getAlpha() != 0.0f) {
                this.indicator.animate().alpha(0.0f).setDuration((long)i).start();
            }
        } else {
            this.indicator.setAlpha(0.0f);
        }
    }
    
    public com.navdy.hud.app.ui.component.vmenu.VerticalFastScrollIndex getFastScrollIndex() {
        return this.fastScrollIndex;
    }
    
    public android.view.ViewGroup getSelectedCustomImage() {
        return this.selectedCustomView;
    }
    
    public boolean handleGesture(com.navdy.service.library.events.input.GestureEvent a) {
        boolean b = false;
        if (this.listLoaded) {
            if (this.closeMenuAnimationActive) {
                sLogger.v("close menu animation is active, no-op");
                b = false;
            } else if (com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent$14.$SwitchMap$com$navdy$service$library$events$input$Gesture[a.gesture.ordinal()] != 0) {
                this.callback.close();
                b = true;
            } else {
                b = false;
            }
        } else {
            sLogger.v("list not loaded yet");
            b = false;
        }
        return b;
    }
    
    public boolean handleKey(com.navdy.hud.app.manager.InputManager$CustomKeyEvent a) {
        boolean b = false;
        label13: if (this.listLoaded) {
            if (this.closeMenuAnimationActive) {
                sLogger.v("close menu animation is active, no-op");
                b = false;
            } else {
                long j = android.os.SystemClock.elapsedRealtime();
                switch(com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent$14.$SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent[a.ordinal()]) {
                    case 3: {
                        boolean b0 = this.isCloseMenuVisible();
                        label11: {
                            label14: {
                                label16: {
                                    boolean b1 = false;
                                    label15: {
                                        if (!b0) {
                                            break label15;
                                        }
                                        if (this.callback.isClosed()) {
                                            break label15;
                                        }
                                        this.closeHalo.setVisibility(4);
                                        com.navdy.hud.app.ui.component.vmenu.VerticalAnimationUtils.performClick((android.view.View)this.closeIconContainer, 50, this.closeIconEndAction);
                                        break label16;
                                    }
                                    int i = this.verticalList.getCurrentPosition();
                                    com.navdy.hud.app.ui.component.vlist.VerticalList$Model a0 = this.verticalList.getCurrentModel();
                                    if (a0 == null) {
                                        break label14;
                                    }
                                    switch(a0.id) {
                                        case R.id.vlist_content_loading: case R.id.vlist_loading: {
                                            b = false;
                                            break label13;
                                        }
                                        default: {
                                            b1 = this.isFastScrollAvailable();
                                        }
                                    }
                                    label12: {
                                        if (!b1) {
                                            break label12;
                                        }
                                        if (this.isFastScrollVisible()) {
                                            break label11;
                                        }
                                    }
                                    com.navdy.hud.app.ui.component.vlist.VerticalList$ItemSelectionState a1 = this.verticalList.getItemSelectionState();
                                    a1.set(a0, a0.id, i, -1, -1);
                                    boolean b2 = this.callback.isItemClickable(a1);
                                    label10: {
                                        if (b2) {
                                            break label10;
                                        }
                                        b = false;
                                        break;
                                    }
                                    this.verticalList.select();
                                }
                                b = true;
                                break;
                            }
                            b = false;
                            break;
                        }
                        this.selectFastScroll();
                        b = true;
                        break;
                    }
                    case 2: {
                        boolean b3 = this.isCloseMenuVisible();
                        label6: {
                            label9: {
                                label8: {
                                    if (!b3) {
                                        break label8;
                                    }
                                    this.hideCloseMenu();
                                    break label9;
                                }
                                boolean b4 = this.isFastScrollAvailable();
                                label7: {
                                    if (!b4) {
                                        break label7;
                                    }
                                    if (this.checkFastScroll(com.navdy.hud.app.ui.component.vlist.VerticalList$Direction.DOWN, 1, j)) {
                                        break label6;
                                    }
                                }
                                this.hideToolTip();
                                if (this.verticalList.down().listMoved) {
                                    this.fadeinIndicator(500);
                                    this.startFadeOut();
                                }
                            }
                            b = true;
                            break;
                        }
                        b = true;
                        break;
                    }
                    case 1: {
                        int i0 = this.verticalList.getCurrentPosition();
                        label1: {
                            label0: {
                                label3: {
                                    label2: {
                                        label5: {
                                            if (i0 == 0) {
                                                break label5;
                                            }
                                            boolean b5 = this.isFastScrollAvailable();
                                            label4: {
                                                if (!b5) {
                                                    break label4;
                                                }
                                                if (this.checkFastScroll(com.navdy.hud.app.ui.component.vlist.VerticalList$Direction.UP, -1, j)) {
                                                    break label3;
                                                }
                                            }
                                            this.lastUpEvent = j;
                                            if (!this.verticalList.up().listMoved) {
                                                break label2;
                                            }
                                            this.fadeinIndicator(500);
                                            this.startFadeOut();
                                            break label2;
                                        }
                                        if (this.verticalList.up().keyHandled) {
                                            break label1;
                                        }
                                        if (android.os.SystemClock.elapsedRealtime() - this.lastUpEvent < (long)this.fastScrollCloseWaitPeriod) {
                                            break label0;
                                        }
                                        this.lastUpEvent = j;
                                        this.showCloseMenu();
                                    }
                                    b = true;
                                    break;
                                }
                                b = true;
                                break;
                            }
                            sLogger.v("user scrolled fast");
                            this.lastUpEvent = j;
                            b = true;
                            break;
                        }
                        this.lastUpEvent = j;
                        b = true;
                        break;
                    }
                    default: {
                        b = this.overrideDefaultKeyEvents.contains(a);
                    }
                }
            }
        } else {
            sLogger.v("list not loaded yet");
            b = false;
        }
        return b;
    }
    
    public void hideToolTip() {
        this.toolTip.hide();
    }
    
    public boolean isCloseMenuVisible() {
        return this.closeContainer.getVisibility() == 0;
    }
    
    public void performBackAnimation(Runnable a, Runnable a0, com.navdy.hud.app.ui.component.vlist.VerticalList$Model a1, int i) {
        boolean b = false;
        boolean b0 = false;
        sLogger.v("performBackAnimation");
        float f = (float)com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.selectedIconSize * 0.20000004768371582f / 2f;
        this.animImageView.setScaleX(1.2f);
        this.animImageView.setScaleY(1.2f);
        this.animImageView.setX(this.selectedImage.getX() + f);
        this.animImageView.setY(this.selectedImage.getY() + f);
        android.widget.ImageView a2 = (this.selectedIconColorImage.getVisibility() != 0) ? this.selectedIconImage : this.selectedIconColorImage;
        android.graphics.Bitmap a3 = this.getFromCache(a1);
        if (a3 != null) {
            this.animImageView.setImageBitmap(a3);
        } else {
            com.navdy.hud.app.ui.component.vmenu.VerticalAnimationUtils.copyImage(a2, this.animImageView);
            this.addToCache(this.animImageView, a1);
        }
        this.animImageView.setVisibility(0);
        this.selectedImage.setAlpha(0.0f);
        android.util.Property a4 = android.view.View.SCALE_X;
        float[] a5 = new float[1];
        a5[0] = 1f;
        android.animation.PropertyValuesHolder a6 = android.animation.PropertyValuesHolder.ofFloat(a4, a5);
        android.util.Property a7 = android.view.View.SCALE_Y;
        float[] a8 = new float[1];
        a8[0] = 1f;
        android.animation.PropertyValuesHolder a9 = android.animation.PropertyValuesHolder.ofFloat(a7, a8);
        android.util.Property a10 = android.view.View.X;
        float[] a11 = new float[1];
        a11[0] = (float)(com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.selectedImageX + i);
        android.animation.PropertyValuesHolder a12 = android.animation.PropertyValuesHolder.ofFloat(a10, a11);
        android.util.Property a13 = android.view.View.Y;
        float[] a14 = new float[1];
        a14[0] = (float)com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.selectedImageY;
        android.animation.PropertyValuesHolder a15 = android.animation.PropertyValuesHolder.ofFloat(a13, a14);
        android.widget.ImageView a16 = this.animImageView;
        android.animation.PropertyValuesHolder[] a17 = new android.animation.PropertyValuesHolder[4];
        a17[0] = a6;
        a17[1] = a9;
        a17[2] = a12;
        a17[3] = a15;
        android.animation.ObjectAnimator a18 = android.animation.ObjectAnimator.ofPropertyValuesHolder(a16, a17);
        a18.setDuration(150L);
        a18.addListener((android.animation.Animator$AnimatorListener)new com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent$11(this, a0));
        a18.start();
        this.animTitle.setX((float)(com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.selectedTextX - animateTranslateY));
        if (a1.fontInfo == null) {
            com.navdy.hud.app.ui.component.vlist.VerticalList.setFontSize(a1, this.verticalList.allowsTwoLineTitles());
        }
        this.animTitle.setTextSize(a1.fontInfo.titleFontSize);
        this.animTitle.setAlpha(0.0f);
        this.animTitle.setVisibility(0);
        label5: {
            label3: {
                label4: {
                    if (a1 == null) {
                        break label4;
                    }
                    if (a1.subTitle != null) {
                        break label3;
                    }
                }
                this.animSubTitle.setText((CharSequence)"");
                b = false;
                break label5;
            }
            this.animSubTitle.setX((float)(com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.selectedTextX - animateTranslateY));
            this.animSubTitle.setText((CharSequence)a1.subTitle);
            this.animSubTitle.setTextSize(a1.fontInfo.subTitleFontSize);
            this.animSubTitle.setAlpha(0.0f);
            this.animSubTitle.setVisibility(0);
            b = true;
        }
        label2: {
            label0: {
                label1: {
                    if (a1 == null) {
                        break label1;
                    }
                    if (a1.subTitle2 != null) {
                        break label0;
                    }
                }
                this.animSubTitle2.setText((CharSequence)"");
                b0 = false;
                break label2;
            }
            this.animSubTitle2.setX((float)(com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.selectedTextX - animateTranslateY));
            this.animSubTitle2.setText((CharSequence)a1.subTitle2);
            this.animSubTitle2.setTextSize(a1.fontInfo.subTitle2FontSize);
            this.animSubTitle2.setAlpha(0.0f);
            this.animSubTitle2.setVisibility(0);
            b0 = true;
        }
        android.animation.AnimatorSet a19 = new android.animation.AnimatorSet();
        android.util.Property a20 = android.view.View.ALPHA;
        float[] a21 = new float[1];
        a21[0] = 1f;
        android.animation.PropertyValuesHolder a22 = android.animation.PropertyValuesHolder.ofFloat(a20, a21);
        android.util.Property a23 = android.view.View.X;
        float[] a24 = new float[1];
        a24[0] = this.animTitle.getX() + (float)animateTranslateY;
        android.animation.PropertyValuesHolder a25 = android.animation.PropertyValuesHolder.ofFloat(a23, a24);
        android.widget.TextView a26 = this.animTitle;
        android.animation.PropertyValuesHolder[] a27 = new android.animation.PropertyValuesHolder[2];
        a27[0] = a22;
        a27[1] = a25;
        android.animation.AnimatorSet$Builder a28 = a19.play((android.animation.Animator)android.animation.ObjectAnimator.ofPropertyValuesHolder(a26, a27));
        if (b) {
            android.util.Property a29 = android.view.View.ALPHA;
            float[] a30 = new float[1];
            a30[0] = 1f;
            android.animation.PropertyValuesHolder a31 = android.animation.PropertyValuesHolder.ofFloat(a29, a30);
            android.util.Property a32 = android.view.View.X;
            float[] a33 = new float[1];
            a33[0] = this.animSubTitle.getX() + (float)animateTranslateY;
            android.animation.PropertyValuesHolder a34 = android.animation.PropertyValuesHolder.ofFloat(a32, a33);
            android.widget.TextView a35 = this.animSubTitle;
            android.animation.PropertyValuesHolder[] a36 = new android.animation.PropertyValuesHolder[2];
            a36[0] = a31;
            a36[1] = a34;
            a28.with((android.animation.Animator)android.animation.ObjectAnimator.ofPropertyValuesHolder(a35, a36));
        }
        if (b0) {
            android.util.Property a37 = android.view.View.ALPHA;
            float[] a38 = new float[1];
            a38[0] = 1f;
            android.animation.PropertyValuesHolder a39 = android.animation.PropertyValuesHolder.ofFloat(a37, a38);
            android.util.Property a40 = android.view.View.X;
            float[] a41 = new float[1];
            a41[0] = this.animSubTitle2.getX() + (float)animateTranslateY;
            android.animation.PropertyValuesHolder a42 = android.animation.PropertyValuesHolder.ofFloat(a40, a41);
            android.widget.TextView a43 = this.animSubTitle2;
            android.animation.PropertyValuesHolder[] a44 = new android.animation.PropertyValuesHolder[2];
            a44[0] = a39;
            a44[1] = a42;
            a28.with((android.animation.Animator)android.animation.ObjectAnimator.ofPropertyValuesHolder(a43, a44));
        }
        a19.setStartDelay((long)(int)(0.6666666865348816f * (float)150));
        a19.setDuration(50L);
        a19.start();
        android.widget.TextView a45 = this.selectedText;
        android.util.Property a46 = android.view.View.ALPHA;
        float[] a47 = new float[1];
        a47[0] = 0.0f;
        android.animation.ObjectAnimator a48 = android.animation.ObjectAnimator.ofFloat(a45, a46, a47);
        a48.setDuration(50L);
        a48.start();
        this.rightContainer.setPivotX(0.0f);
        this.rightContainer.setPivotY((float)(this.recyclerView.getMeasuredHeight() / 2));
        android.animation.AnimatorSet a49 = new android.animation.AnimatorSet();
        android.util.Property a50 = android.view.View.SCALE_X;
        float[] a51 = new float[1];
        a51[0] = 0.5f;
        android.animation.PropertyValuesHolder a52 = android.animation.PropertyValuesHolder.ofFloat(a50, a51);
        android.util.Property a53 = android.view.View.SCALE_Y;
        float[] a54 = new float[1];
        a54[0] = 0.5f;
        android.animation.PropertyValuesHolder a55 = android.animation.PropertyValuesHolder.ofFloat(a53, a54);
        android.util.Property a56 = android.view.View.ALPHA;
        float[] a57 = new float[1];
        a57[0] = 0.0f;
        android.animation.PropertyValuesHolder a58 = android.animation.PropertyValuesHolder.ofFloat(a56, a57);
        a49.setDuration((long)(int)(0.6666666865348816f * (float)150));
        android.view.ViewGroup a59 = this.rightContainer;
        android.animation.PropertyValuesHolder[] a60 = new android.animation.PropertyValuesHolder[3];
        a60[0] = a52;
        a60[1] = a55;
        a60[2] = a58;
        a49.play((android.animation.Animator)android.animation.ObjectAnimator.ofPropertyValuesHolder(a59, a60));
        a49.addListener((android.animation.Animator$AnimatorListener)new com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent$12(this, a));
        a49.start();
    }
    
    public void performEnterAnimation(Runnable a, Runnable a0, com.navdy.hud.app.ui.component.vlist.VerticalList$Model a1) {
        sLogger.v("performEnterAnimation");
        this.selectedImage.getLocationOnScreen(location);
        this.selectedImage.setAlpha(0.0f);
        this.selectedText.setAlpha(0.0f);
        this.indicator.setAlpha(0.0f);
        this.animImageView.setScaleX(1f);
        this.animImageView.setScaleY(1f);
        this.animImageView.setImageBitmap((android.graphics.Bitmap)null);
        com.navdy.hud.app.ui.component.vlist.VerticalList$ModelType a2 = a1.type;
        com.navdy.hud.app.ui.component.vlist.VerticalList$ModelType a3 = com.navdy.hud.app.ui.component.vlist.VerticalList$ModelType.ICON_OPTIONS;
        android.graphics.Bitmap a4 = null;
        if (a2 != a3) {
            a4 = this.getFromCache(a1);
        }
        this.verticalList.copyAndPosition(this.animImageView, this.animTitle, this.animSubTitle, this.animSubTitle2, a4 == null);
        if (a4 == null) {
            this.addToCache(this.animImageView, a1);
        } else {
            this.animImageView.setImageBitmap(a4);
        }
        this.animImageView.setVisibility(0);
        this.animTitle.setAlpha(1f);
        this.animTitle.setVisibility(0);
        if (this.animSubTitle.getText().length() > 0) {
            this.animSubTitle.setAlpha(1f);
            this.animSubTitle.setVisibility(0);
        }
        this.rightContainer.setAlpha(0.0f);
        android.util.Property a5 = android.view.View.SCALE_X;
        float[] a6 = new float[1];
        a6[0] = 1.2f;
        android.animation.PropertyValuesHolder a7 = android.animation.PropertyValuesHolder.ofFloat(a5, a6);
        android.util.Property a8 = android.view.View.SCALE_Y;
        float[] a9 = new float[1];
        a9[0] = 1.2f;
        android.animation.PropertyValuesHolder a10 = android.animation.PropertyValuesHolder.ofFloat(a8, a9);
        float f = (float)com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.selectedIconSize * -0.20000004768371582f / 2f;
        android.util.Property a11 = android.view.View.X;
        float[] a12 = new float[1];
        a12[0] = (float)(location[0] != 0) - f;
        android.animation.PropertyValuesHolder a13 = android.animation.PropertyValuesHolder.ofFloat(a11, a12);
        android.util.Property a14 = android.view.View.Y;
        float[] a15 = new float[1];
        a15[0] = (float)(location[1] - com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.rootTopOffset) - f;
        android.animation.PropertyValuesHolder a16 = android.animation.PropertyValuesHolder.ofFloat(a14, a15);
        android.widget.ImageView a17 = this.animImageView;
        android.animation.PropertyValuesHolder[] a18 = new android.animation.PropertyValuesHolder[4];
        a18[0] = a7;
        a18[1] = a10;
        a18[2] = a13;
        a18[3] = a16;
        android.animation.ObjectAnimator a19 = android.animation.ObjectAnimator.ofPropertyValuesHolder(a17, a18);
        a19.setDuration(150L);
        a19.addListener((android.animation.Animator$AnimatorListener)new com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent$13(this, a, a0));
        a19.start();
        this.animTitle.getLocationOnScreen(location);
        android.animation.AnimatorSet a20 = new android.animation.AnimatorSet();
        android.util.Property a21 = android.view.View.ALPHA;
        float[] a22 = new float[1];
        a22[0] = 0.0f;
        android.animation.PropertyValuesHolder a23 = android.animation.PropertyValuesHolder.ofFloat(a21, a22);
        android.util.Property a24 = android.view.View.X;
        float[] a25 = new float[1];
        a25[0] = this.animTitle.getX() - (float)animateTranslateY;
        android.animation.PropertyValuesHolder a26 = android.animation.PropertyValuesHolder.ofFloat(a24, a25);
        android.widget.TextView a27 = this.animTitle;
        android.animation.PropertyValuesHolder[] a28 = new android.animation.PropertyValuesHolder[2];
        a28[0] = a23;
        a28[1] = a26;
        android.animation.AnimatorSet$Builder a29 = a20.play((android.animation.Animator)android.animation.ObjectAnimator.ofPropertyValuesHolder(a27, a28));
        if (this.animSubTitle.getText().length() > 0) {
            android.util.Property a30 = android.view.View.ALPHA;
            float[] a31 = new float[1];
            a31[0] = 0.0f;
            android.animation.PropertyValuesHolder a32 = android.animation.PropertyValuesHolder.ofFloat(a30, a31);
            android.util.Property a33 = android.view.View.X;
            float[] a34 = new float[1];
            a34[0] = this.animSubTitle.getX() - (float)animateTranslateY;
            android.animation.PropertyValuesHolder a35 = android.animation.PropertyValuesHolder.ofFloat(a33, a34);
            android.widget.TextView a36 = this.animSubTitle;
            android.animation.PropertyValuesHolder[] a37 = new android.animation.PropertyValuesHolder[2];
            a37[0] = a32;
            a37[1] = a35;
            a29.with((android.animation.Animator)android.animation.ObjectAnimator.ofPropertyValuesHolder(a36, a37));
        }
        a20.setDuration(50L);
        a20.start();
        android.widget.TextView a38 = this.selectedText;
        android.util.Property a39 = android.view.View.ALPHA;
        float[] a40 = new float[1];
        a40[0] = 1f;
        android.animation.ObjectAnimator a41 = android.animation.ObjectAnimator.ofFloat(a38, a39, a40);
        a41.setDuration(50L);
        a41.setStartDelay((long)(int)(0.6666666865348816f * (float)150));
        a41.start();
        android.animation.AnimatorSet a42 = new android.animation.AnimatorSet();
        android.util.Property a43 = android.view.View.SCALE_X;
        float[] a44 = new float[1];
        a44[0] = 1f;
        android.animation.PropertyValuesHolder a45 = android.animation.PropertyValuesHolder.ofFloat(a43, a44);
        android.util.Property a46 = android.view.View.SCALE_Y;
        float[] a47 = new float[1];
        a47[0] = 1f;
        android.animation.PropertyValuesHolder a48 = android.animation.PropertyValuesHolder.ofFloat(a46, a47);
        android.util.Property a49 = android.view.View.ALPHA;
        float[] a50 = new float[1];
        a50[0] = 1f;
        android.animation.PropertyValuesHolder a51 = android.animation.PropertyValuesHolder.ofFloat(a49, a50);
        a42.setStartDelay(50L);
        a42.setDuration((long)(int)(0.6666666865348816f * (float)150));
        android.view.ViewGroup a52 = this.rightContainer;
        android.animation.PropertyValuesHolder[] a53 = new android.animation.PropertyValuesHolder[3];
        a53[0] = a45;
        a53[1] = a48;
        a53[2] = a51;
        a42.play((android.animation.Animator)android.animation.ObjectAnimator.ofPropertyValuesHolder(a52, a53));
        a42.start();
    }
    
    public void performSelectionAnimation(Runnable a) {
        this.performSelectionAnimation(a, 0);
    }
    
    public void performSelectionAnimation(Runnable a, int i) {
        sLogger.v("performSelectionAnimation");
        android.animation.AnimatorSet a0 = new android.animation.AnimatorSet();
        android.view.ViewGroup a1 = this.leftContainer;
        android.util.Property a2 = android.view.View.ALPHA;
        float[] a3 = new float[1];
        a3[0] = 0.0f;
        android.animation.AnimatorSet$Builder a4 = a0.play((android.animation.Animator)android.animation.ObjectAnimator.ofFloat(a1, a2, a3));
        com.navdy.hud.app.ui.component.carousel.CarouselIndicator a5 = this.indicator;
        android.util.Property a6 = android.view.View.ALPHA;
        float[] a7 = new float[1];
        a7[0] = 0.0f;
        a4.with((android.animation.Animator)android.animation.ObjectAnimator.ofFloat(a5, a6, a7));
        this.verticalList.addCurrentHighlightAnimation(a4);
        a0.setDuration(50L);
        a0.addListener((android.animation.Animator$AnimatorListener)new com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent$10(this, a, i));
        a0.start();
    }
    
    public void setLeftContainerWidth(int i) {
        sLogger.v(new StringBuilder().append("leftContainer width=").append(i).toString());
        ((android.view.ViewGroup$MarginLayoutParams)this.leftContainer.getLayoutParams()).width = i;
    }
    
    public void setOverrideDefaultKeyEvents(com.navdy.hud.app.manager.InputManager$CustomKeyEvent a, boolean b) {
        if (b) {
            this.overrideDefaultKeyEvents.add(a);
        } else {
            this.overrideDefaultKeyEvents.remove(a);
        }
    }
    
    public void setRightContainerWidth(int i) {
        sLogger.v(new StringBuilder().append("rightContainer width=").append(i).toString());
        ((android.view.ViewGroup$MarginLayoutParams)this.rightContainer.getLayoutParams()).width = i;
    }
    
    public void setSelectedIconColorImage(int i, int i0, android.graphics.Shader a, float f) {
        this.setSelectedIconColorImage(i, i0, a, f, com.navdy.hud.app.ui.component.image.IconColorImageView$IconShape.CIRCLE);
    }
    
    public void setSelectedIconColorImage(int i, int i0, android.graphics.Shader a, float f, com.navdy.hud.app.ui.component.image.IconColorImageView$IconShape a0) {
        this.selectedIconColorImage.setIcon(i, i0, a, f);
        this.selectedIconColorImage.setVisibility(0);
        this.selectedIconColorImage.setIconShape(a0);
        this.selectedIconImage.setVisibility(8);
        this.selectedCustomView.setVisibility(8);
    }
    
    public void setSelectedIconImage(int i, String s, com.navdy.hud.app.ui.component.image.InitialsImageView$Style a) {
        this.selectedIconImage.setImage(i, s, a);
        this.selectedIconImage.setVisibility(0);
        this.selectedIconColorImage.setVisibility(8);
        this.selectedCustomView.setVisibility(8);
    }
    
    public void setSelectedIconImage(android.graphics.Bitmap a) {
        this.selectedIconImage.setInitials((String)null, com.navdy.hud.app.ui.component.image.InitialsImageView$Style.DEFAULT);
        this.selectedIconImage.setImageBitmap(a);
        this.selectedIconImage.setVisibility(0);
        this.selectedIconColorImage.setVisibility(8);
        this.selectedCustomView.setVisibility(8);
    }
    
    public void showSelectedCustomView() {
        this.selectedCustomView.setVisibility(0);
        this.selectedIconImage.setVisibility(8);
        this.selectedIconColorImage.setVisibility(8);
    }
    
    public void showToolTip(int i, String s) {
        this.toolTip.show(i, s);
    }
    
    public void unlock() {
        this.verticalList.unlock();
    }
    
    public void unlock(boolean b) {
        this.verticalList.unlock(b);
    }
    
    public void updateView(java.util.List a, int i, boolean b) {
        this.updateView(a, i, b, (com.navdy.hud.app.ui.component.vmenu.VerticalFastScrollIndex)null);
    }
    
    public void updateView(java.util.List a, int i, boolean b, com.navdy.hud.app.ui.component.vmenu.VerticalFastScrollIndex a0) {
        this.updateView(a, i, b, false, a0);
    }
    
    public void updateView(java.util.List a, int i, boolean b, boolean b0, com.navdy.hud.app.ui.component.vmenu.VerticalFastScrollIndex a0) {
        this.listLoaded = false;
        this.fastScrollIndex = null;
        label1: if (a0 != null) {
            int i0 = a.size();
            label0: {
                if (i0 < 40) {
                    break label0;
                }
                if (a0.getEntryCount() < 2) {
                    break label0;
                }
                this.fastScrollIndex = a0;
                sLogger.i(new StringBuilder().append("fast scroll available:").append(i0).toString());
                break label1;
            }
            sLogger.i(new StringBuilder().append("fast scroll threshold not met:").append(i0).append(",").append(a0.getEntryCount()).toString());
        }
        if (b0) {
            this.verticalList.updateViewWithScrollableContent(a, i, b);
        } else {
            this.verticalList.updateView(a, i, b);
        }
    }
}
