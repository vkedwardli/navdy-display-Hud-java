package com.navdy.hud.app.ui.component.homescreen;

public class HomeScreenUtils {
    final private static float DENSITY;
    
    static {
        DENSITY = com.navdy.hud.app.HudApplication.getAppContext().getResources().getDisplayMetrics().density;
    }
    
    public HomeScreenUtils() {
    }
    
    public static android.animation.ObjectAnimator getAlphaAnimator(android.view.View a, int i) {
        float[] a0 = new float[1];
        a0[0] = (float)i;
        return android.animation.ObjectAnimator.ofFloat(a, "alpha", a0);
    }
    
    public static android.animation.AnimatorSet getFadeInAndScaleUpAnimator(android.view.View a) {
        float[] a0 = new float[2];
        a0[0] = 0.0f;
        a0[1] = 1f;
        android.animation.ObjectAnimator a1 = android.animation.ObjectAnimator.ofFloat(a, "alpha", a0);
        float[] a2 = new float[1];
        a2[0] = 1f;
        android.animation.ObjectAnimator a3 = android.animation.ObjectAnimator.ofFloat(a, "scaleX", a2);
        float[] a4 = new float[1];
        a4[0] = 1f;
        android.animation.ObjectAnimator a5 = android.animation.ObjectAnimator.ofFloat(a, "scaleY", a4);
        android.animation.AnimatorSet a6 = new android.animation.AnimatorSet();
        a6.play((android.animation.Animator)a1).with((android.animation.Animator)a3).with((android.animation.Animator)a5);
        return a6;
    }
    
    public static android.animation.AnimatorSet getFadeOutAndScaleDownAnimator(android.view.View a) {
        float[] a0 = new float[1];
        a0[0] = 0.0f;
        android.animation.ObjectAnimator a1 = android.animation.ObjectAnimator.ofFloat(a, "alpha", a0);
        float[] a2 = new float[1];
        a2[0] = 0.9f;
        android.animation.ObjectAnimator a3 = android.animation.ObjectAnimator.ofFloat(a, "scaleX", a2);
        float[] a4 = new float[1];
        a4[0] = 0.9f;
        android.animation.ObjectAnimator a5 = android.animation.ObjectAnimator.ofFloat(a, "scaleY", a4);
        android.animation.AnimatorSet a6 = new android.animation.AnimatorSet();
        a6.play((android.animation.Animator)a1).with((android.animation.Animator)a3).with((android.animation.Animator)a5);
        return a6;
    }
    
    public static android.animation.AnimatorSet getMarginAnimator(android.view.View a, int i, int i0) {
        android.view.ViewGroup$MarginLayoutParams a0 = (android.view.ViewGroup$MarginLayoutParams)a.getLayoutParams();
        int[] a1 = new int[2];
        a1[0] = a0.leftMargin;
        a1[1] = i;
        android.animation.ValueAnimator a2 = android.animation.ValueAnimator.ofInt(a1);
        a2.addUpdateListener((android.animation.ValueAnimator$AnimatorUpdateListener)new com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils$4(a));
        int[] a3 = new int[2];
        a3[0] = a0.rightMargin;
        a3[1] = i0;
        android.animation.ValueAnimator a4 = android.animation.ValueAnimator.ofInt(a3);
        a4.addUpdateListener((android.animation.ValueAnimator$AnimatorUpdateListener)new com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils$5(a));
        android.animation.AnimatorSet a5 = new android.animation.AnimatorSet();
        android.animation.Animator[] a6 = new android.animation.Animator[2];
        a6[0] = a2;
        a6[1] = a4;
        a5.playTogether(a6);
        return a5;
    }
    
    public static android.animation.ValueAnimator getProgressBarAnimator(android.widget.ProgressBar a, int i) {
        int[] a0 = new int[2];
        a0[0] = a.getProgress();
        a0[1] = i;
        android.animation.ValueAnimator a1 = android.animation.ValueAnimator.ofInt(a0);
        a1.addUpdateListener((android.animation.ValueAnimator$AnimatorUpdateListener)new com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils$2(a));
        return a1;
    }
    
    public static android.animation.ObjectAnimator getTranslationXPositionAnimator(android.view.View a, int i) {
        android.util.Property a0 = android.view.View.TRANSLATION_X;
        float[] a1 = new float[1];
        a1[0] = (float)i;
        return android.animation.ObjectAnimator.ofFloat(a, a0, a1);
    }
    
    public static android.animation.ObjectAnimator getTranslationYPositionAnimator(android.view.View a, int i) {
        android.util.Property a0 = android.view.View.TRANSLATION_Y;
        float[] a1 = new float[1];
        a1[0] = (float)i;
        return android.animation.ObjectAnimator.ofFloat(a, a0, a1);
    }
    
    public static android.animation.ValueAnimator getWidthAnimator(android.view.View a, int i) {
        int[] a0 = new int[2];
        a0[0] = ((android.view.ViewGroup$MarginLayoutParams)a.getLayoutParams()).width;
        a0[1] = i;
        android.animation.ValueAnimator a1 = android.animation.ValueAnimator.ofInt(a0);
        a1.addUpdateListener((android.animation.ValueAnimator$AnimatorUpdateListener)new com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils$1(a));
        return a1;
    }
    
    public static android.animation.ValueAnimator getWidthAnimator(android.view.View a, int i, int i0) {
        int[] a0 = new int[2];
        a0[0] = i;
        a0[1] = i0;
        android.animation.ValueAnimator a1 = android.animation.ValueAnimator.ofInt(a0);
        a1.addUpdateListener((android.animation.ValueAnimator$AnimatorUpdateListener)new com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils$3(a));
        return a1;
    }
    
    public static android.animation.ObjectAnimator getXPositionAnimator(android.view.View a, float f) {
        float[] a0 = new float[1];
        a0[0] = f;
        return android.animation.ObjectAnimator.ofFloat(a, "x", a0);
    }
    
    public static android.animation.ObjectAnimator getYPositionAnimator(android.view.View a, float f) {
        float[] a0 = new float[1];
        a0[0] = f;
        return android.animation.ObjectAnimator.ofFloat(a, "y", a0);
    }
    
    public static void setWidth(android.view.View a, int i) {
        ((android.view.ViewGroup$MarginLayoutParams)a.getLayoutParams()).width = i;
        a.invalidate();
        a.requestLayout();
    }
}
