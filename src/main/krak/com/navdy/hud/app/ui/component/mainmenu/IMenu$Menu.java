package com.navdy.hud.app.ui.component.mainmenu;


public enum IMenu$Menu {
    MAIN(0),
    SETTINGS(1),
    PLACES(2),
    RECENT_PLACES(3),
    CONTACTS(4),
    RECENT_CONTACTS(5),
    CONTACT_OPTIONS(6),
    MAIN_OPTIONS(7),
    MUSIC(8),
    SEARCH(9),
    REPORT_ISSUE(10),
    ACTIVE_TRIP(11),
    SYSTEM_INFO(12),
    TEST_FAST_SCROLL_MENU(13),
    NUMBER_PICKER(14),
    MESSAGE_PICKER(15);

    private int value;
    IMenu$Menu(int value) {
        this.value = value;
    }
    public int getValue() {
        return value;
    }
}

//final public class IMenu$Menu extends Enum {
//    final private static com.navdy.hud.app.ui.component.mainmenu.IMenu$Menu[] $VALUES;
//    final public static com.navdy.hud.app.ui.component.mainmenu.IMenu$Menu ACTIVE_TRIP;
//    final public static com.navdy.hud.app.ui.component.mainmenu.IMenu$Menu CONTACTS;
//    final public static com.navdy.hud.app.ui.component.mainmenu.IMenu$Menu CONTACT_OPTIONS;
//    final public static com.navdy.hud.app.ui.component.mainmenu.IMenu$Menu MAIN;
//    final public static com.navdy.hud.app.ui.component.mainmenu.IMenu$Menu MAIN_OPTIONS;
//    final public static com.navdy.hud.app.ui.component.mainmenu.IMenu$Menu MESSAGE_PICKER;
//    final public static com.navdy.hud.app.ui.component.mainmenu.IMenu$Menu MUSIC;
//    final public static com.navdy.hud.app.ui.component.mainmenu.IMenu$Menu NUMBER_PICKER;
//    final public static com.navdy.hud.app.ui.component.mainmenu.IMenu$Menu PLACES;
//    final public static com.navdy.hud.app.ui.component.mainmenu.IMenu$Menu RECENT_CONTACTS;
//    final public static com.navdy.hud.app.ui.component.mainmenu.IMenu$Menu RECENT_PLACES;
//    final public static com.navdy.hud.app.ui.component.mainmenu.IMenu$Menu REPORT_ISSUE;
//    final public static com.navdy.hud.app.ui.component.mainmenu.IMenu$Menu SEARCH;
//    final public static com.navdy.hud.app.ui.component.mainmenu.IMenu$Menu SETTINGS;
//    final public static com.navdy.hud.app.ui.component.mainmenu.IMenu$Menu SYSTEM_INFO;
//    final public static com.navdy.hud.app.ui.component.mainmenu.IMenu$Menu TEST_FAST_SCROLL_MENU;
//    
//    static {
//        MAIN = new com.navdy.hud.app.ui.component.mainmenu.IMenu$Menu("MAIN", 0);
//        SETTINGS = new com.navdy.hud.app.ui.component.mainmenu.IMenu$Menu("SETTINGS", 1);
//        PLACES = new com.navdy.hud.app.ui.component.mainmenu.IMenu$Menu("PLACES", 2);
//        RECENT_PLACES = new com.navdy.hud.app.ui.component.mainmenu.IMenu$Menu("RECENT_PLACES", 3);
//        CONTACTS = new com.navdy.hud.app.ui.component.mainmenu.IMenu$Menu("CONTACTS", 4);
//        RECENT_CONTACTS = new com.navdy.hud.app.ui.component.mainmenu.IMenu$Menu("RECENT_CONTACTS", 5);
//        CONTACT_OPTIONS = new com.navdy.hud.app.ui.component.mainmenu.IMenu$Menu("CONTACT_OPTIONS", 6);
//        MAIN_OPTIONS = new com.navdy.hud.app.ui.component.mainmenu.IMenu$Menu("MAIN_OPTIONS", 7);
//        MUSIC = new com.navdy.hud.app.ui.component.mainmenu.IMenu$Menu("MUSIC", 8);
//        SEARCH = new com.navdy.hud.app.ui.component.mainmenu.IMenu$Menu("SEARCH", 9);
//        REPORT_ISSUE = new com.navdy.hud.app.ui.component.mainmenu.IMenu$Menu("REPORT_ISSUE", 10);
//        ACTIVE_TRIP = new com.navdy.hud.app.ui.component.mainmenu.IMenu$Menu("ACTIVE_TRIP", 11);
//        SYSTEM_INFO = new com.navdy.hud.app.ui.component.mainmenu.IMenu$Menu("SYSTEM_INFO", 12);
//        TEST_FAST_SCROLL_MENU = new com.navdy.hud.app.ui.component.mainmenu.IMenu$Menu("TEST_FAST_SCROLL_MENU", 13);
//        NUMBER_PICKER = new com.navdy.hud.app.ui.component.mainmenu.IMenu$Menu("NUMBER_PICKER", 14);
//        MESSAGE_PICKER = new com.navdy.hud.app.ui.component.mainmenu.IMenu$Menu("MESSAGE_PICKER", 15);
//        com.navdy.hud.app.ui.component.mainmenu.IMenu$Menu[] a = new com.navdy.hud.app.ui.component.mainmenu.IMenu$Menu[16];
//        a[0] = MAIN;
//        a[1] = SETTINGS;
//        a[2] = PLACES;
//        a[3] = RECENT_PLACES;
//        a[4] = CONTACTS;
//        a[5] = RECENT_CONTACTS;
//        a[6] = CONTACT_OPTIONS;
//        a[7] = MAIN_OPTIONS;
//        a[8] = MUSIC;
//        a[9] = SEARCH;
//        a[10] = REPORT_ISSUE;
//        a[11] = ACTIVE_TRIP;
//        a[12] = SYSTEM_INFO;
//        a[13] = TEST_FAST_SCROLL_MENU;
//        a[14] = NUMBER_PICKER;
//        a[15] = MESSAGE_PICKER;
//        $VALUES = a;
//    }
//    
//    private IMenu$Menu(String s, int i) {
//        super(s, i);
//    }
//    
//    public static com.navdy.hud.app.ui.component.mainmenu.IMenu$Menu valueOf(String s) {
//        return (com.navdy.hud.app.ui.component.mainmenu.IMenu$Menu)Enum.valueOf(com.navdy.hud.app.ui.component.mainmenu.IMenu$Menu.class, s);
//    }
//    
//    public static com.navdy.hud.app.ui.component.mainmenu.IMenu$Menu[] values() {
//        return $VALUES.clone();
//    }
//}
//