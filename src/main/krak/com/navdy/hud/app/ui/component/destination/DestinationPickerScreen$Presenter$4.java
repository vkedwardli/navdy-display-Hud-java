package com.navdy.hud.app.ui.component.destination;

class DestinationPickerScreen$Presenter$4 implements com.navdy.hud.app.ui.component.ChoiceLayout$IListener {
    final com.navdy.hud.app.ui.component.destination.DestinationPickerScreen$Presenter this$0;
    final com.navdy.hud.app.ui.component.vlist.VerticalList$ItemSelectionState val$selection;
    
    DestinationPickerScreen$Presenter$4(com.navdy.hud.app.ui.component.destination.DestinationPickerScreen$Presenter a, com.navdy.hud.app.ui.component.vlist.VerticalList$ItemSelectionState a0) {
        super();
        this.this$0 = a;
        this.val$selection = a0;
    }
    
    public void executeItem(int i, int i0) {
        com.navdy.hud.app.ui.component.destination.DestinationPickerView a = (com.navdy.hud.app.ui.component.destination.DestinationPickerView)com.navdy.hud.app.ui.component.destination.DestinationPickerScreen$Presenter.access$600(this.this$0);
        if (a != null) {
            switch(i) {
                case 1: {
                    a.confirmationLayout.setVisibility(8);
                    this.this$0.clearSelection();
                    a.vmenuComponent.verticalList.unlock();
                    break;
                }
                case 0: {
                    com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.access$100().v("called presenter select");
                    com.navdy.hud.app.ui.component.destination.DestinationPickerView a0 = (com.navdy.hud.app.ui.component.destination.DestinationPickerView)com.navdy.hud.app.ui.component.destination.DestinationPickerScreen$Presenter.access$700(this.this$0);
                    if (a0 == null) {
                        break;
                    }
                    com.navdy.hud.app.ui.component.destination.DestinationPickerScreen$Presenter.access$800(this.this$0, a0, this.val$selection.id, this.val$selection.pos);
                    break;
                }
            }
        } else {
            com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.access$100().v("not alive");
        }
    }
    
    public void itemSelected(int i, int i0) {
    }
}
