package com.navdy.hud.app.ui.component.mainmenu;
import com.navdy.hud.app.R;

class ContactsMenu implements com.navdy.hud.app.ui.component.mainmenu.IMenu {
    final private static float CONTACT_PHOTO_OPACITY = 0.6f;
    final private static com.navdy.hud.app.ui.component.vlist.VerticalList$Model back;
    final private static int backColor;
    final private static int contactColor;
    final private static String contactStr;
    final private static android.os.Handler handler;
    final private static int noContactColor;
    final private static String recentContactStr;
    final private static com.navdy.hud.app.ui.component.vlist.VerticalList$Model recentContacts;
    final private static android.content.res.Resources resources;
    final private static com.squareup.picasso.Transformation roundTransformation;
    final private static com.navdy.service.library.log.Logger sLogger;
    private int backSelection;
    private int backSelectionId;
    private com.squareup.otto.Bus bus;
    private java.util.List cachedList;
    private com.navdy.hud.app.ui.component.mainmenu.IMenu parent;
    private com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2$Presenter presenter;
    private com.navdy.hud.app.ui.component.mainmenu.RecentContactsMenu recentContactsMenu;
    private java.util.List returnToCacheList;
    private com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent vscrollComponent;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.ui.component.mainmenu.ContactsMenu.class);
        handler = new android.os.Handler(android.os.Looper.getMainLooper());
        roundTransformation = new com.makeramen.RoundedTransformationBuilder().oval(true).build();
        resources = com.navdy.hud.app.HudApplication.getAppContext().getResources();
        backColor = resources.getColor(R.color.mm_back);
        contactColor = resources.getColor(R.color.mm_contacts);
        noContactColor = resources.getColor(R.color.icon_user_bg_4);
        contactStr = resources.getString(R.string.carousel_menu_contacts);
        recentContactStr = resources.getString(R.string.carousel_menu_recent_contacts_title);
        String s = resources.getString(R.string.back);
        int i = backColor;
        back = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(R.id.menu_back, R.drawable.icon_mm_back, i, com.navdy.hud.app.ui.component.mainmenu.MainMenu.bkColorUnselected, i, s, (String)null);
        String s0 = recentContactStr;
        int i0 = contactColor;
        recentContacts = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(R.id.contacts_menu_recent, R.drawable.icon_place_recent_2, i0, com.navdy.hud.app.ui.component.mainmenu.MainMenu.bkColorUnselected, i0, s0, (String)null);
    }
    
    ContactsMenu(com.squareup.otto.Bus a, com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent a0, com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2$Presenter a1, com.navdy.hud.app.ui.component.mainmenu.IMenu a2) {
        this.bus = a;
        this.vscrollComponent = a0;
        this.presenter = a1;
        this.parent = a2;
    }
    
    static void access$000(com.navdy.hud.app.ui.component.mainmenu.ContactsMenu a, java.io.File a0, int i, int i0, int i1) {
        a.setImage(a0, i, i0, i1);
    }
    
    static android.os.Handler access$100() {
        return handler;
    }
    
    static com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2$Presenter access$200(com.navdy.hud.app.ui.component.mainmenu.ContactsMenu a) {
        return a.presenter;
    }
    
    private com.navdy.hud.app.ui.component.vlist.VerticalList$Model getContactsNotAllowed() {
        android.content.res.Resources a = com.navdy.hud.app.HudApplication.getAppContext().getResources();
        String s = (com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().isRemoteDeviceIos()) ? a.getString(R.string.allow_contact_access_iphone_title) : a.getString(R.string.allow_contact_access_android_title);
        return com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(R.id.menu_back, R.drawable.icon_mm_back, contactColor, com.navdy.hud.app.ui.component.mainmenu.MainMenu.bkColorUnselected, contactColor, s, (String)null);
    }
    
    private void setImage(java.io.File a, int i, int i0, int i1) {
        com.navdy.hud.app.util.picasso.PicassoUtil.getInstance().load(a).resize(i, i0).transform(roundTransformation).into((com.squareup.picasso.Target)new com.navdy.hud.app.ui.component.mainmenu.ContactsMenu$2(this, i1));
    }
    
    com.navdy.hud.app.ui.component.vlist.VerticalList$Model buildModel(int i, String s, String s0, String s1, int i0, com.navdy.hud.app.framework.contacts.NumberType a, String s2) {
        com.navdy.hud.app.ui.component.vlist.VerticalList$Model a0 = null;
        com.navdy.hud.app.framework.contacts.ContactImageHelper a1 = com.navdy.hud.app.framework.contacts.ContactImageHelper.getInstance();
        if (android.text.TextUtils.isEmpty((CharSequence)s)) {
            a0 = com.navdy.hud.app.ui.component.vlist.viewholder.IconsTwoViewHolder.buildModel(i, R.drawable.icon_user_bg_4, R.drawable.icon_user_numberonly, noContactColor, -1, s0, (String)null);
        } else {
            int i1 = a1.getResourceId(i0);
            int i2 = a1.getResourceColor(i0);
            if (a != com.navdy.hud.app.framework.contacts.NumberType.OTHER) {
                s0 = s1;
            }
            a0 = com.navdy.hud.app.ui.component.vlist.viewholder.IconsTwoViewHolder.buildModel(i, i1, R.drawable.icon_user_grey, i2, -1, s, s0);
        }
        a0.extras = new java.util.HashMap();
        a0.extras.put("INITIAL", s2);
        return a0;
    }
    
    public com.navdy.hud.app.ui.component.mainmenu.IMenu getChildMenu(com.navdy.hud.app.ui.component.mainmenu.IMenu a, String s, String s0) {
        return null;
    }
    
    public int getInitialSelection() {
        return (this.cachedList != null) ? (this.cachedList.size() < 3) ? 1 : 2 : 0;
    }
    
    public java.util.List getItems() {
        Object a = null;
        if (this.cachedList == null) {
            java.util.ArrayList a0 = new java.util.ArrayList();
            this.returnToCacheList = (java.util.List)new java.util.ArrayList();
            com.navdy.hud.app.framework.contacts.FavoriteContactsManager a1 = com.navdy.hud.app.framework.contacts.FavoriteContactsManager.getInstance();
            if (a1.isAllowedToReceiveContacts()) {
                ((java.util.List)a0).add(back);
                ((java.util.List)a0).add(recentContacts);
                java.util.List a2 = a1.getFavoriteContacts();
                if (a2 == null) {
                    sLogger.v("no favorite contacts");
                } else {
                    sLogger.v(new StringBuilder().append("favorite contacts:").append(a2.size()).toString());
                    Object a3 = a2.iterator();
                    int i = 0;
                    while(((java.util.Iterator)a3).hasNext()) {
                        com.navdy.hud.app.framework.contacts.Contact a4 = (com.navdy.hud.app.framework.contacts.Contact)((java.util.Iterator)a3).next();
                        com.navdy.hud.app.ui.component.vlist.VerticalList$Model a5 = this.buildModel(i, a4.name, a4.formattedNumber, a4.numberTypeStr, a4.defaultImageIndex, a4.numberType, a4.initials);
                        a5.state = a4;
                        ((java.util.List)a0).add(a5);
                        this.returnToCacheList.add(a5);
                        i = i + 1;
                    }
                }
                this.cachedList = (java.util.List)a0;
                a = a0;
            } else {
                ((java.util.List)a0).add(this.getContactsNotAllowed());
                a = a0;
            }
        } else {
            a = this.cachedList;
        }
        return (java.util.List)a;
    }
    
    public com.navdy.hud.app.ui.component.vlist.VerticalList$Model getModelfromPos(int i) {
        com.navdy.hud.app.ui.component.vlist.VerticalList$Model a = null;
        java.util.List a0 = this.cachedList;
        label2: {
            label0: {
                label1: {
                    if (a0 == null) {
                        break label1;
                    }
                    if (this.cachedList.size() > i) {
                        break label0;
                    }
                }
                a = null;
                break label2;
            }
            a = (com.navdy.hud.app.ui.component.vlist.VerticalList$Model)this.cachedList.get(i);
        }
        return a;
    }
    
    public com.navdy.hud.app.ui.component.vmenu.VerticalFastScrollIndex getScrollIndex() {
        return null;
    }
    
    public com.navdy.hud.app.ui.component.mainmenu.IMenu$Menu getType() {
        return com.navdy.hud.app.ui.component.mainmenu.IMenu$Menu.CONTACTS;
    }
    
    public boolean isBindCallsEnabled() {
        return true;
    }
    
    public boolean isFirstItemEmpty() {
        return true;
    }
    
    public boolean isItemClickable(int i, int i0) {
        return true;
    }
    
    public void onBindToView(com.navdy.hud.app.ui.component.vlist.VerticalList$Model a, android.view.View a0, int i, com.navdy.hud.app.ui.component.vlist.VerticalList$ModelState a1) {
        if (a.state != null) {
            com.navdy.hud.app.ui.component.image.InitialsImageView a2 = (com.navdy.hud.app.ui.component.image.InitialsImageView)com.navdy.hud.app.ui.component.vlist.VerticalList.findImageView(a0);
            com.navdy.hud.app.ui.component.image.InitialsImageView a3 = (com.navdy.hud.app.ui.component.image.InitialsImageView)com.navdy.hud.app.ui.component.vlist.VerticalList.findSmallImageView(a0);
            com.navdy.hud.app.ui.component.image.CrossFadeImageView a4 = (com.navdy.hud.app.ui.component.image.CrossFadeImageView)com.navdy.hud.app.ui.component.vlist.VerticalList.findCrossFadeImageView(a0);
            a2.setTag(null);
            String s = (a.state instanceof com.navdy.hud.app.framework.recentcall.RecentCall) ? ((com.navdy.hud.app.framework.recentcall.RecentCall)a.state).number : ((com.navdy.hud.app.framework.contacts.Contact)a.state).number;
            java.io.File a5 = com.navdy.hud.app.framework.contacts.PhoneImageDownloader.getInstance().getImagePath(s, com.navdy.service.library.events.photo.PhotoType.PHOTO_CONTACT);
            android.graphics.Bitmap a6 = com.navdy.hud.app.util.picasso.PicassoUtil.getBitmapfromCache(a5);
            if (a6 == null) {
                a2.setTag(a.state);
                com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.ui.component.mainmenu.ContactsMenu$1(this, a5, a2, a, i), 1);
            } else {
                a2.setTag(null);
                a2.setInitials((String)null, com.navdy.hud.app.ui.component.image.InitialsImageView$Style.DEFAULT);
                a2.setImageBitmap(a6);
                a3.setInitials((String)null, com.navdy.hud.app.ui.component.image.InitialsImageView$Style.DEFAULT);
                a3.setImageBitmap(a6);
                a3.setAlpha(0.6f);
                a4.setSmallAlpha(0.6f);
                a1.updateImage = false;
                a1.updateSmallImage = false;
            }
        }
    }
    
    public void onFastScrollEnd() {
    }
    
    public void onFastScrollStart() {
    }
    
    public void onItemSelected(com.navdy.hud.app.ui.component.vlist.VerticalList$ItemSelectionState a) {
    }
    
    public void onScrollIdle() {
    }
    
    public void onUnload(com.navdy.hud.app.ui.component.mainmenu.IMenu$MenuLevel a) {
        if (com.navdy.hud.app.ui.component.mainmenu.ContactsMenu$3.$SwitchMap$com$navdy$hud$app$ui$component$mainmenu$IMenu$MenuLevel[a.ordinal()] != 0) {
            if (this.returnToCacheList != null) {
                sLogger.v("cm:unload add to cache");
                com.navdy.hud.app.ui.component.vlist.VerticalModelCache.addToCache(this.returnToCacheList);
                this.returnToCacheList = null;
            }
            if (this.recentContactsMenu != null) {
                this.recentContactsMenu.onUnload(com.navdy.hud.app.ui.component.mainmenu.IMenu$MenuLevel.CLOSE);
            }
        }
    }
    
    public boolean selectItem(com.navdy.hud.app.ui.component.vlist.VerticalList$ItemSelectionState a) {
        sLogger.v(new StringBuilder().append("select id:").append(a.id).append(" pos:").append(a.pos).toString());
        switch(a.id) {
            case R.id.menu_back: {
                sLogger.v("back");
                com.navdy.hud.app.analytics.AnalyticsSupport.recordMenuSelection("back");
                this.presenter.loadMenu(this.parent, com.navdy.hud.app.ui.component.mainmenu.IMenu$MenuLevel.BACK_TO_PARENT, this.backSelection, this.backSelectionId);
                this.backSelectionId = 0;
                break;
            }
            case R.id.contacts_menu_recent: {
                sLogger.v("recent contacts");
                com.navdy.hud.app.analytics.AnalyticsSupport.recordMenuSelection("recent_contacts");
                if (this.recentContactsMenu == null) {
                    this.recentContactsMenu = new com.navdy.hud.app.ui.component.mainmenu.RecentContactsMenu(this.bus, this.vscrollComponent, this.presenter, (com.navdy.hud.app.ui.component.mainmenu.IMenu)this);
                }
                this.presenter.loadMenu((com.navdy.hud.app.ui.component.mainmenu.IMenu)this.recentContactsMenu, com.navdy.hud.app.ui.component.mainmenu.IMenu$MenuLevel.SUB_LEVEL, a.pos, 0);
                break;
            }
            default: {
                sLogger.v("contact options");
                com.navdy.hud.app.analytics.AnalyticsSupport.recordMenuSelection("contact_options");
                com.navdy.hud.app.framework.contacts.Contact a0 = (com.navdy.hud.app.framework.contacts.Contact)((com.navdy.hud.app.ui.component.vlist.VerticalList$Model)this.cachedList.get(a.pos)).state;
                java.util.ArrayList a1 = new java.util.ArrayList(1);
                ((java.util.List)a1).add(a0);
                com.navdy.hud.app.ui.component.mainmenu.ContactOptionsMenu a2 = new com.navdy.hud.app.ui.component.mainmenu.ContactOptionsMenu((java.util.List)a1, this.vscrollComponent, this.presenter, (com.navdy.hud.app.ui.component.mainmenu.IMenu)this, this.bus);
                this.presenter.loadMenu((com.navdy.hud.app.ui.component.mainmenu.IMenu)a2, com.navdy.hud.app.ui.component.mainmenu.IMenu$MenuLevel.SUB_LEVEL, a.pos, 0);
            }
        }
        return true;
    }
    
    public void setBackSelectionId(int i) {
        this.backSelectionId = i;
    }
    
    public void setBackSelectionPos(int i) {
        this.backSelection = i;
    }
    
    public void setSelectedIcon() {
        this.vscrollComponent.setSelectedIconColorImage(R.drawable.icon_mm_contacts_2, contactColor, (android.graphics.Shader)null, 1f);
        this.vscrollComponent.selectedText.setText((CharSequence)contactStr);
    }
    
    public void showToolTip() {
    }
}
