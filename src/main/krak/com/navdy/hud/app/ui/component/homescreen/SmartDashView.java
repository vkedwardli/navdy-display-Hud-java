package com.navdy.hud.app.ui.component.homescreen;
import com.navdy.hud.app.R;

public class SmartDashView extends com.navdy.hud.app.ui.component.homescreen.BaseSmartDashView implements com.navdy.hud.app.ui.component.dashboard.IDashboardOptionsAdapter {
    final public static int HEADING_EXPIRE_INTERVAL = 3000;
    final public static long MINIMUM_INTERVAL_BETWEEN_RECORDING_USER_PREFERENCE;
    final public static float NAVIGATION_MODE_SCALE_FACTOR = 0.8f;
    final private static boolean SHOW_ETA = false;
    final private static com.navdy.service.library.log.Logger sLogger;
    private int activeModeGaugeMargin;
    private String activeRightGaugeId;
    private String activeleftGaugeId;
    @InjectView(R.id.eta_time_amPm)
    android.widget.TextView etaAmPm;
    @InjectView(R.id.eta_time)
    android.widget.TextView etaText;
    private int fullModeGaugeTopMargin;
    private android.content.SharedPreferences globalPreferences;
    private com.navdy.hud.app.util.HeadingDataUtil headingDataUtil;
    public boolean isShowingDriveScoreGauge;
    private String lastETA;
    private String lastETA_AmPm;
    private double lastHeading;
    private long lastHeadingSampleTime;
    private long lastUserPreferenceRecordedTime;
    @InjectView(R.id.eta_layout)
    android.view.ViewGroup mEtaLayout;
    com.navdy.hud.app.ui.component.homescreen.SmartDashView$GaugeViewPagerAdapter mLeftAdapter;
    @InjectView(R.id.left_gauge)
    com.navdy.hud.app.ui.component.dashboard.GaugeViewPager mLeftGaugeViewPager;
    @InjectView(R.id.middle_gauge)
    com.navdy.hud.app.view.GaugeView mMiddleGaugeView;
    com.navdy.hud.app.ui.component.homescreen.SmartDashView$GaugeViewPagerAdapter mRightAdapter;
    @InjectView(R.id.right_gauge)
    com.navdy.hud.app.ui.component.dashboard.GaugeViewPager mRightGaugeViewPager;
    private com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager mSmartDashWidgetManager;
    private int middleGauge;
    private int middleGaugeShrinkEarlyTbtOffset;
    private int middleGaugeShrinkMargin;
    private int middleGaugeShrinkModeMargin;
    private boolean paused;
    private int scrollableSide;
    com.navdy.hud.app.view.SpeedometerGaugePresenter2 speedometerGaugePresenter2;
    com.navdy.hud.app.view.TachometerGaugePresenter tachometerPresenter;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.ui.component.homescreen.SmartDashView.class);
        MINIMUM_INTERVAL_BETWEEN_RECORDING_USER_PREFERENCE = java.util.concurrent.TimeUnit.MINUTES.toMillis(15L);
    }
    
    public SmartDashView(android.content.Context a) {
        super(a);
        this.lastUserPreferenceRecordedTime = 0L;
        this.lastHeading = 0.0;
        this.lastHeadingSampleTime = 0L;
        this.middleGauge = 1;
        this.scrollableSide = 1;
        this.headingDataUtil = new com.navdy.hud.app.util.HeadingDataUtil();
    }
    
    public SmartDashView(android.content.Context a, android.util.AttributeSet a0) {
        super(a, a0);
        this.lastUserPreferenceRecordedTime = 0L;
        this.lastHeading = 0.0;
        this.lastHeadingSampleTime = 0L;
        this.middleGauge = 1;
        this.scrollableSide = 1;
        this.headingDataUtil = new com.navdy.hud.app.util.HeadingDataUtil();
    }
    
    public SmartDashView(android.content.Context a, android.util.AttributeSet a0, int i) {
        super(a, a0, i);
        this.lastUserPreferenceRecordedTime = 0L;
        this.lastHeading = 0.0;
        this.lastHeadingSampleTime = 0L;
        this.middleGauge = 1;
        this.scrollableSide = 1;
        this.headingDataUtil = new com.navdy.hud.app.util.HeadingDataUtil();
    }
    
    static com.navdy.hud.app.util.HeadingDataUtil access$000(com.navdy.hud.app.ui.component.homescreen.SmartDashView a) {
        return a.headingDataUtil;
    }
    
    static com.navdy.service.library.log.Logger access$100() {
        return sLogger;
    }
    
    static com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager access$200(com.navdy.hud.app.ui.component.homescreen.SmartDashView a) {
        return a.mSmartDashWidgetManager;
    }
    
    static String access$300(com.navdy.hud.app.ui.component.homescreen.SmartDashView a) {
        return a.lastETA;
    }
    
    static String access$302(com.navdy.hud.app.ui.component.homescreen.SmartDashView a, String s) {
        a.lastETA = s;
        return s;
    }
    
    static String access$400(com.navdy.hud.app.ui.component.homescreen.SmartDashView a) {
        return a.lastETA_AmPm;
    }
    
    static String access$402(com.navdy.hud.app.ui.component.homescreen.SmartDashView a, String s) {
        a.lastETA_AmPm = s;
        return s;
    }
    
    static void access$500(com.navdy.hud.app.ui.component.homescreen.SmartDashView a, String s, Object a0) {
        a.savePreference(s, a0);
    }
    
    static void access$600(com.navdy.hud.app.ui.component.homescreen.SmartDashView a) {
        a.showViewsAccordingToUserPreferences();
    }
    
    static String access$700(com.navdy.hud.app.ui.component.homescreen.SmartDashView a) {
        return a.activeleftGaugeId;
    }
    
    static String access$702(com.navdy.hud.app.ui.component.homescreen.SmartDashView a, String s) {
        a.activeleftGaugeId = s;
        return s;
    }
    
    static String access$800(com.navdy.hud.app.ui.component.homescreen.SmartDashView a) {
        return a.activeRightGaugeId;
    }
    
    static String access$802(com.navdy.hud.app.ui.component.homescreen.SmartDashView a, String s) {
        a.activeRightGaugeId = s;
        return s;
    }
    
    static void access$900(com.navdy.hud.app.ui.component.homescreen.SmartDashView a, boolean b) {
        a.showEta(b);
    }
    
    private void savePreference(String s, Object a) {
        boolean b = com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getCurrentProfile().isDefaultProfile();
        android.content.SharedPreferences a0 = this.homeScreenView.getDriverPreferences();
        if (!b) {
            if (a instanceof String) {
                android.content.SharedPreferences$Editor a1 = a0.edit();
                String s0 = (String)a;
                a1.putString(s, s0).apply();
                a = s0;
            } else if (a instanceof Integer) {
                android.content.SharedPreferences$Editor a2 = a0.edit();
                Integer a3 = (Integer)a;
                a2.putInt(s, a3.intValue()).apply();
                a = a3;
            }
        }
        if (a instanceof String) {
            this.globalPreferences.edit().putString(s, (String)a).apply();
        } else if (a instanceof Integer) {
            this.globalPreferences.edit().putInt(s, ((Integer)a).intValue()).apply();
        }
    }
    
    private void showEta(boolean b) {
        if (this.mEtaLayout.getVisibility() == 0) {
            this.mEtaLayout.setVisibility(8);
        }
    }
    
    private void showMiddleGaugeAccordingToUserPreferences() {
        this.middleGauge = ((com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getCurrentProfile().isDefaultProfile()) ? this.globalPreferences : this.homeScreenView.getDriverPreferences()).getInt("PREFERENCE_MIDDLE_GAUGE", 1);
        sLogger.d(new StringBuilder().append("Middle Gauge : ").append(this.middleGauge).toString());
        com.navdy.hud.app.obd.ObdManager.getInstance();
        switch(this.middleGauge) {
            case 1: {
                this.showSpeedometerGauge();
                break;
            }
            case 0: {
                this.showTachometer();
                break;
            }
        }
    }
    
    private void showSpeedometerGauge() {
        this.tachometerPresenter.onPause();
        this.tachometerPresenter.setWidgetVisibleToUser(false);
        this.tachometerPresenter.setView((com.navdy.hud.app.view.DashboardWidgetView)null);
        if (!this.paused) {
            this.speedometerGaugePresenter2.onResume();
        }
        this.speedometerGaugePresenter2.setWidgetVisibleToUser(true);
        this.speedometerGaugePresenter2.setView((com.navdy.hud.app.view.DashboardWidgetView)this.mMiddleGaugeView);
        int i = com.navdy.hud.app.manager.SpeedManager.getInstance().getCurrentSpeed();
        this.speedometerGaugePresenter2.setSpeed(i);
    }
    
    private void showTachometer() {
        this.speedometerGaugePresenter2.onPause();
        this.speedometerGaugePresenter2.setWidgetVisibleToUser(false);
        this.speedometerGaugePresenter2.setView((com.navdy.hud.app.view.DashboardWidgetView)null);
        if (!this.paused) {
            this.tachometerPresenter.onResume();
        }
        this.tachometerPresenter.setWidgetVisibleToUser(true);
        this.tachometerPresenter.setView((com.navdy.hud.app.view.DashboardWidgetView)this.mMiddleGaugeView);
        int i = com.navdy.hud.app.obd.ObdManager.getInstance().getEngineRpm();
        if (i != -1 && this.tachometerPresenter != null) {
            this.tachometerPresenter.setRPM(i);
        }
        int i0 = com.navdy.hud.app.manager.SpeedManager.getInstance().getCurrentSpeed();
        if (this.tachometerPresenter != null) {
            this.tachometerPresenter.setSpeed(i0);
        }
    }
    
    private void showViewsAccordingToUserPreferences() {
        sLogger.d("showViewsAccordingToUserPreferences");
        boolean b = com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getCurrentProfile().isDefaultProfile();
        android.content.SharedPreferences a = b ? this.globalPreferences : this.homeScreenView.getDriverPreferences();
        this.scrollableSide = a.getInt("PREFERENCE_SCROLLABLE_SIDE", 1);
        sLogger.v(new StringBuilder().append("scrollableSide:").append(this.scrollableSide).toString());
        this.showMiddleGaugeAccordingToUserPreferences();
        String s = a.getString("PREFERENCE_LEFT_GAUGE", "ANALOG_CLOCK_WIDGET");
        int i = this.mSmartDashWidgetManager.getIndexForWidgetIdentifier(s);
        sLogger.d(new StringBuilder().append("Left Gauge ID : ").append(s).append(", Index : ").append(i).toString());
        if (i < 0) {
            i = this.mSmartDashWidgetManager.getIndexForWidgetIdentifier("EMPTY_WIDGET");
        }
        this.mLeftGaugeViewPager.setSelectedPosition(i);
        String s0 = a.getString("PREFERENCE_RIGHT_GAUGE", "COMPASS_WIDGET");
        int i0 = this.mSmartDashWidgetManager.getIndexForWidgetIdentifier(s0);
        sLogger.d(new StringBuilder().append("Right Gauge ID : ").append(s0).append(", Index : ").append(i0).toString());
        if (i0 < 0) {
            i0 = this.mSmartDashWidgetManager.getIndexForWidgetIdentifier("EMPTY_WIDGET");
        }
        this.mRightGaugeViewPager.setSelectedPosition(i0);
        if (!b) {
            this.globalPreferences.edit().putInt("PREFERENCE_MIDDLE_GAUGE", this.middleGauge).putInt("PREFERENCE_SCROLLABLE_SIDE", this.scrollableSide).putString("PREFERENCE_LEFT_GAUGE", s).putString("PREFERENCE_RIGHT_GAUGE", s0).apply();
        }
        this.mLeftAdapter.setExcludedPosition(i0);
        this.mRightAdapter.setExcludedPosition(i);
        this.recordUsersWidgetPreference();
    }
    
    public void ObdPidChangeEvent(com.navdy.hud.app.obd.ObdManager$ObdPidChangeEvent a) {
        super.ObdPidChangeEvent(a);
        switch(a.pid.getId()) {
            case 256: {
                this.mSmartDashWidgetManager.updateWidget("MPG_AVG_WIDGET", Double.valueOf(a.pid.getValue()));
                break;
            }
            case 47: {
                this.mSmartDashWidgetManager.updateWidget("FUEL_GAUGE_ID", Double.valueOf(a.pid.getValue()));
                break;
            }
            case 12: {
                if (this.tachometerPresenter == null) {
                    break;
                }
                this.tachometerPresenter.setRPM((int)a.pid.getValue());
                break;
            }
            case 5: {
                this.mSmartDashWidgetManager.updateWidget("ENGINE_TEMPERATURE_GAUGE_ID", Double.valueOf(a.pid.getValue()));
                break;
            }
        }
    }
    
    public void adjustDashHeight(int i) {
        super.adjustDashHeight(i);
    }
    
    protected void animateToFullMode() {
        super.animateToFullMode();
    }
    
    protected void animateToFullModeInternal(android.animation.AnimatorSet$Builder a) {
        super.animateToFullModeInternal(a);
    }
    
    public String getActiveRightGaugeId() {
        return this.activeRightGaugeId;
    }
    
    public String getActiveleftGaugeId() {
        return this.activeleftGaugeId;
    }
    
    public int getCurrentScrollableSideOption() {
        return (this.scrollableSide != 0) ? 0 : 1;
    }
    
    public void getCustomAnimator(com.navdy.hud.app.view.MainView$CustomAnimationMode a, android.animation.AnimatorSet$Builder a0) {
        sLogger.v(new StringBuilder().append("getCustomAnimator").append(a).toString());
        super.getCustomAnimator(a, a0);
        android.animation.AnimatorSet a1 = new android.animation.AnimatorSet();
        switch(com.navdy.hud.app.ui.component.homescreen.SmartDashView$8.$SwitchMap$com$navdy$hud$app$view$MainView$CustomAnimationMode[a.ordinal()]) {
            case 2: case 3: {
                this.mLeftGaugeViewPager.setVisibility(8);
                this.mRightGaugeViewPager.setVisibility(8);
                this.showEta(false);
                android.view.ViewGroup$MarginLayoutParams a2 = (android.view.ViewGroup$MarginLayoutParams)this.mMiddleGaugeView.getLayoutParams();
                int i = a2.leftMargin;
                int i0 = (a != com.navdy.hud.app.view.MainView$CustomAnimationMode.SHRINK_LEFT) ? this.middleGaugeShrinkModeMargin : this.middleGaugeShrinkMargin;
                android.animation.ValueAnimator a3 = new android.animation.ValueAnimator();
                int[] a4 = new int[2];
                a4[0] = i;
                a4[1] = i0;
                a3.setIntValues(a4);
                a3.addUpdateListener((android.animation.ValueAnimator$AnimatorUpdateListener)new com.navdy.hud.app.ui.component.homescreen.SmartDashView$7(this, a2));
                android.animation.Animator[] a5 = new android.animation.Animator[1];
                a5[0] = a3;
                a1.playTogether(a5);
                break;
            }
            case 1: {
                android.view.ViewGroup$MarginLayoutParams a6 = (android.view.ViewGroup$MarginLayoutParams)this.mMiddleGaugeView.getLayoutParams();
                int i1 = a6.leftMargin;
                android.animation.ValueAnimator a7 = new android.animation.ValueAnimator();
                int[] a8 = new int[2];
                a8[0] = i1;
                a8[1] = 0;
                a7.setIntValues(a8);
                a7.addUpdateListener((android.animation.ValueAnimator$AnimatorUpdateListener)new com.navdy.hud.app.ui.component.homescreen.SmartDashView$5(this, a6));
                a7.addListener((android.animation.Animator$AnimatorListener)new com.navdy.hud.app.ui.component.homescreen.SmartDashView$6(this));
                android.animation.Animator[] a9 = new android.animation.Animator[1];
                a9[0] = a7;
                a1.playTogether(a9);
                break;
            }
        }
        a0.with((android.animation.Animator)a1);
    }
    
    public int getMiddleGaugeOption() {
        return (this.middleGauge != 0) ? 0 : 1;
    }
    
    public void init(com.navdy.hud.app.ui.component.homescreen.HomeScreenView a) {
        this.globalPreferences = a.globalPreferences;
        this.mSmartDashWidgetManager = new com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager(this.globalPreferences, this.getContext());
        a.getDriverPreferences();
        this.mSmartDashWidgetManager.setFilter((com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager$IWidgetFilter)new com.navdy.hud.app.ui.component.homescreen.SmartDashView$1(this, a));
        this.mSmartDashWidgetManager.registerForChanges(this);
        this.tachometerPresenter = new com.navdy.hud.app.view.TachometerGaugePresenter(this.getContext());
        this.speedometerGaugePresenter2 = new com.navdy.hud.app.view.SpeedometerGaugePresenter2(this.getContext());
        this.bus.register(new com.navdy.hud.app.ui.component.homescreen.SmartDashView$2(this, a));
        android.content.res.Resources a0 = this.getResources();
        this.middleGaugeShrinkMargin = a0.getDimensionPixelSize(R.dimen.middle_gauge_shrink_margin);
        this.middleGaugeShrinkModeMargin = a0.getDimensionPixelSize(R.dimen.middle_gauge_shrink_mode_margin);
        this.middleGaugeShrinkEarlyTbtOffset = this.getResources().getDimensionPixelSize(R.dimen.middle_gauge_shrink_early_tbt_offset);
        super.init(a);
    }
    
    protected void initializeView() {
        super.initializeView();
        this.mSmartDashWidgetManager.reLoadAvailableWidgets(false);
        if (this.mLeftAdapter == null) {
            this.mLeftAdapter = new com.navdy.hud.app.ui.component.homescreen.SmartDashView$GaugeViewPagerAdapter(this, this.mSmartDashWidgetManager, true);
            this.mLeftGaugeViewPager.setAdapter((com.navdy.hud.app.ui.component.dashboard.GaugeViewPager$Adapter)this.mLeftAdapter);
            this.mLeftGaugeViewPager.setChangeListener((com.navdy.hud.app.ui.component.dashboard.GaugeViewPager$ChangeListener)new com.navdy.hud.app.ui.component.homescreen.SmartDashView$3(this));
        }
        if (this.mRightAdapter == null) {
            this.mRightAdapter = new com.navdy.hud.app.ui.component.homescreen.SmartDashView$GaugeViewPagerAdapter(this, this.mSmartDashWidgetManager, false);
            this.mRightGaugeViewPager.setAdapter((com.navdy.hud.app.ui.component.dashboard.GaugeViewPager$Adapter)this.mRightAdapter);
            this.mRightGaugeViewPager.setChangeListener((com.navdy.hud.app.ui.component.dashboard.GaugeViewPager$ChangeListener)new com.navdy.hud.app.ui.component.homescreen.SmartDashView$4(this));
        }
        this.showViewsAccordingToUserPreferences();
    }
    
    protected void onFinishInflate() {
        super.onFinishInflate();
        this.fullModeGaugeTopMargin = this.getResources().getDimensionPixelSize(R.dimen.gauge_frame_top_margin_full_mode);
        this.activeModeGaugeMargin = this.getResources().getDimensionPixelSize(R.dimen.gauge_frame_top_margin_active_mode);
    }
    
    public boolean onKey(com.navdy.hud.app.manager.InputManager$CustomKeyEvent a) {
        com.navdy.hud.app.ui.component.dashboard.GaugeViewPager a0 = (this.scrollableSide != 1) ? this.mLeftGaugeViewPager : this.mRightGaugeViewPager;
        switch(com.navdy.hud.app.ui.component.homescreen.SmartDashView$8.$SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent[a.ordinal()]) {
            case 2: {
                a0.movePrevious();
                break;
            }
            case 1: {
                a0.moveNext();
                break;
            }
        }
        return false;
    }
    
    public void onPause() {
        if (!this.paused) {
            sLogger.v("::onPause");
            this.paused = true;
            if (this.mSmartDashWidgetManager != null) {
                sLogger.v("widgets disabled");
                this.mSmartDashWidgetManager.onPause();
                if (this.tachometerPresenter != null) {
                    this.tachometerPresenter.onPause();
                }
                if (this.speedometerGaugePresenter2 != null) {
                    this.speedometerGaugePresenter2.onPause();
                }
            }
        }
    }
    
    public void onReload(com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager$Reload a) {
        if (a == com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager$Reload.RELOADED) {
            this.showViewsAccordingToUserPreferences();
        }
    }
    
    public void onResume() {
        if (this.paused) {
            sLogger.v("::onResume");
            this.paused = false;
            if (this.mSmartDashWidgetManager != null) {
                sLogger.v("widgets enabled");
                this.mSmartDashWidgetManager.onResume();
                if (this.tachometerPresenter != null && this.tachometerPresenter.isWidgetActive()) {
                    this.tachometerPresenter.onResume();
                }
                if (this.speedometerGaugePresenter2 != null && this.speedometerGaugePresenter2.isWidgetActive()) {
                    this.speedometerGaugePresenter2.onResume();
                }
            }
            this.recordUsersWidgetPreference();
        }
    }
    
    public void onScrollableSideOptionSelected() {
        switch(this.scrollableSide) {
            case 1: {
                this.scrollableSide = 0;
                this.savePreference("PREFERENCE_SCROLLABLE_SIDE", Integer.valueOf(0));
                break;
            }
            case 0: {
                this.scrollableSide = 1;
                this.savePreference("PREFERENCE_SCROLLABLE_SIDE", Integer.valueOf(1));
                break;
            }
        }
    }
    
    public void onSpeedoMeterSelected() {
        if (this.middleGauge != 1) {
            this.middleGauge = 1;
            this.savePreference("PREFERENCE_MIDDLE_GAUGE", Integer.valueOf(1));
            this.showMiddleGaugeAccordingToUserPreferences();
        }
    }
    
    public void onTachoMeterSelected() {
        if (this.middleGauge != 0) {
            this.middleGauge = 0;
            this.savePreference("PREFERENCE_MIDDLE_GAUGE", Integer.valueOf(0));
            this.showMiddleGaugeAccordingToUserPreferences();
        }
    }
    
    public void recordUsersWidgetPreference() {
        sLogger.d("Recording the users Widget Preference");
        long j = android.os.SystemClock.elapsedRealtime();
        label0: if (!com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getCurrentProfile().isDefaultProfile()) {
            long j0 = this.lastUserPreferenceRecordedTime;
            int i = (j0 < 0L) ? -1 : (j0 == 0L) ? 0 : 1;
            label1: {
                if (i <= 0) {
                    break label1;
                }
                if (j - this.lastUserPreferenceRecordedTime < MINIMUM_INTERVAL_BETWEEN_RECORDING_USER_PREFERENCE) {
                    break label0;
                }
            }
            android.content.SharedPreferences a = this.homeScreenView.getDriverPreferences();
            String s = a.getString("PREFERENCE_LEFT_GAUGE", "ANALOG_CLOCK_WIDGET");
            String s0 = a.getString("PREFERENCE_RIGHT_GAUGE", "COMPASS_WIDGET");
            com.navdy.hud.app.analytics.AnalyticsSupport.recordDashPreference(s, true);
            com.navdy.hud.app.analytics.AnalyticsSupport.recordDashPreference(s0, false);
            this.lastUserPreferenceRecordedTime = j;
        }
    }
    
    public void setView(com.navdy.hud.app.view.MainView$CustomAnimationMode a) {
        sLogger.v(new StringBuilder().append("setview: ").append(a).toString());
        super.setView(a);
        switch(com.navdy.hud.app.ui.component.homescreen.SmartDashView$8.$SwitchMap$com$navdy$hud$app$view$MainView$CustomAnimationMode[a.ordinal()]) {
            case 2: {
                this.mLeftGaugeViewPager.setVisibility(8);
                this.mRightGaugeViewPager.setVisibility(8);
                this.showEta(false);
                ((android.view.ViewGroup$MarginLayoutParams)this.mMiddleGaugeView.getLayoutParams()).leftMargin = this.getResources().getDimensionPixelSize(R.dimen.middle_gauge_shrink_margin);
                break;
            }
            case 1: {
                this.mLeftGaugeViewPager.setVisibility(0);
                this.mRightGaugeViewPager.setVisibility(0);
                if (this.homeScreenView.isNavigationActive()) {
                    this.showEta(true);
                }
                ((android.view.ViewGroup$MarginLayoutParams)this.mMiddleGaugeView.getLayoutParams()).leftMargin = 0;
                break;
            }
        }
    }
    
    public boolean shouldShowTopViews() {
        return false;
    }
    
    public void updateLayoutForMode(com.navdy.hud.app.maps.NavigationMode a, boolean b) {
        boolean b0 = this.homeScreenView.isNavigationActive();
        sLogger.v(new StringBuilder().append("updateLayoutForMode:").append(a).append(" forced=").append(b).append(" nav:").append(b0).toString());
        if (b0) {
            sLogger.v("updateLayoutForMode: active");
            ((android.view.ViewGroup$MarginLayoutParams)this.mLeftGaugeViewPager.getLayoutParams()).topMargin = this.activeModeGaugeMargin;
            ((android.view.ViewGroup$MarginLayoutParams)this.mRightGaugeViewPager.getLayoutParams()).topMargin = this.activeModeGaugeMargin;
            ((android.view.ViewGroup$MarginLayoutParams)this.mMiddleGaugeView.getLayoutParams()).topMargin = this.getResources().getDimensionPixelSize(R.dimen.tachometer_tbt_mode_margin_top);
            this.mMiddleGaugeView.setScaleX(0.8f);
            this.mMiddleGaugeView.setScaleY(0.8f);
            this.mEtaLayout.setVisibility(0);
            this.invalidate();
            this.requestLayout();
        } else {
            sLogger.v("updateLayoutForMode: not active");
            ((android.view.ViewGroup$MarginLayoutParams)this.mLeftGaugeViewPager.getLayoutParams()).topMargin = this.fullModeGaugeTopMargin;
            ((android.view.ViewGroup$MarginLayoutParams)this.mRightGaugeViewPager.getLayoutParams()).topMargin = this.fullModeGaugeTopMargin;
            ((android.view.ViewGroup$MarginLayoutParams)this.mMiddleGaugeView.getLayoutParams()).topMargin = this.getResources().getDimensionPixelSize(R.dimen.tachometer_full_mode_margin_top);
            this.mMiddleGaugeView.setScaleX(1f);
            this.mMiddleGaugeView.setScaleY(1f);
            this.mEtaLayout.setVisibility(8);
            this.invalidate();
            this.requestLayout();
        }
        this.showEta(b0);
        this.invalidate();
        this.requestLayout();
    }
    
    protected void updateSpeed(int i) {
        super.updateSpeed(i);
        if (this.tachometerPresenter != null) {
            this.tachometerPresenter.setSpeed(i);
        }
        if (this.speedometerGaugePresenter2 != null) {
            this.speedometerGaugePresenter2.setSpeed(i);
        }
    }
    
    protected void updateSpeedLimit(int i) {
        super.updateSpeedLimit(i);
        if (this.tachometerPresenter != null) {
            this.tachometerPresenter.setSpeedLimit(i);
        }
        if (this.speedometerGaugePresenter2 != null) {
            this.speedometerGaugePresenter2.setSpeedLimit(i);
        }
        this.mSmartDashWidgetManager.updateWidget("SPEED_LIMIT_SIGN_GAUGE_ID", Integer.valueOf(i));
    }
}
