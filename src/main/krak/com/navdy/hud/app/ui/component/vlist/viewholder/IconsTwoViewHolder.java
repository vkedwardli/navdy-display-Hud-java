package com.navdy.hud.app.ui.component.vlist.viewholder;
import com.navdy.hud.app.R;

public class IconsTwoViewHolder extends com.navdy.hud.app.ui.component.vlist.viewholder.IconBaseViewHolder {
    final private static com.navdy.service.library.log.Logger sLogger;
    private int imageIconSize;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.ui.component.vlist.viewholder.IconsTwoViewHolder.class);
    }
    
    private IconsTwoViewHolder(android.view.ViewGroup a, com.navdy.hud.app.ui.component.vlist.VerticalList a0, android.os.Handler a1) {
        super(a, a0, a1);
        this.imageIconSize = -1;
    }
    
    public static com.navdy.hud.app.ui.component.vlist.VerticalList$Model buildModel(int i, int i0, int i1, int i2, int i3, String s, String s0) {
        return com.navdy.hud.app.ui.component.vlist.viewholder.IconsTwoViewHolder.buildModel(i, i0, i1, i2, i3, s, s0, (String)null);
    }
    
    public static com.navdy.hud.app.ui.component.vlist.VerticalList$Model buildModel(int i, int i0, int i1, int i2, int i3, String s, String s0, String s1) {
        com.navdy.hud.app.ui.component.vlist.VerticalList$Model a = com.navdy.hud.app.ui.component.vlist.VerticalModelCache.getFromCache(com.navdy.hud.app.ui.component.vlist.VerticalList$ModelType.TWO_ICONS);
        if (a == null) {
            a = new com.navdy.hud.app.ui.component.vlist.VerticalList$Model();
        }
        a.type = com.navdy.hud.app.ui.component.vlist.VerticalList$ModelType.TWO_ICONS;
        a.id = i;
        a.icon = i0;
        a.iconSmall = i1;
        a.iconFluctuatorColor = i2;
        a.iconDeselectedColor = i3;
        a.title = s;
        a.subTitle = s0;
        a.subTitle2 = s1;
        return a;
    }
    
    public static com.navdy.hud.app.ui.component.vlist.viewholder.IconsTwoViewHolder buildViewHolder(android.view.ViewGroup a, com.navdy.hud.app.ui.component.vlist.VerticalList a0, android.os.Handler a1) {
        return new com.navdy.hud.app.ui.component.vlist.viewholder.IconsTwoViewHolder(com.navdy.hud.app.ui.component.vlist.viewholder.IconsTwoViewHolder.getLayout(a, R.layout.vlist_item, R.layout.crossfade_image_lyt), a0, a1);
    }
    
    private void setIcon(int i, int i0, boolean b, boolean b0, int i1, int i2) {
        this.imageIconSize = i2;
        java.util.HashMap a = this.extras;
        String s = null;
        if (a != null) {
            s = (String)this.extras.get("INITIAL");
        }
        if (b) {
            com.navdy.hud.app.ui.component.image.InitialsImageView$Style a0 = (s == null) ? com.navdy.hud.app.ui.component.image.InitialsImageView$Style.DEFAULT : com.navdy.hud.app.ui.component.image.InitialsImageView$Style.MEDIUM;
            ((com.navdy.hud.app.ui.component.image.InitialsImageView)this.crossFadeImageView.getBig()).setImage(i, s, a0);
        }
        if (b0) {
            com.navdy.hud.app.ui.component.image.InitialsImageView a1 = (com.navdy.hud.app.ui.component.image.InitialsImageView)this.crossFadeImageView.getSmall();
            a1.setAlpha(1f);
            if (i0 != 0) {
                a1.setBkColor(0);
            } else {
                a1.setBkColor(i1);
            }
            a1.setImage(i0, s, com.navdy.hud.app.ui.component.image.InitialsImageView$Style.TINY);
        }
    }
    
    public void bind(com.navdy.hud.app.ui.component.vlist.VerticalList$Model a, com.navdy.hud.app.ui.component.vlist.VerticalList$ModelState a0) {
        super.bind(a, a0);
        this.setIcon(a.icon, a.iconSmall, a0.updateImage, a0.updateSmallImage, a.iconDeselectedColor, a.iconSize);
    }
    
    public com.navdy.hud.app.ui.component.vlist.VerticalList$ModelType getModelType() {
        return com.navdy.hud.app.ui.component.vlist.VerticalList$ModelType.TWO_ICONS;
    }
    
    public void setItemState(com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$State a, com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$AnimationType a0, int i, boolean b) {
        int i0 = 0;
        com.navdy.hud.app.ui.component.image.CrossFadeImageView$Mode a1 = null;
        super.setItemState(a, a0, i, b);
        switch(com.navdy.hud.app.ui.component.vlist.viewholder.IconsTwoViewHolder$1.$SwitchMap$com$navdy$hud$app$ui$component$vlist$viewholder$VerticalViewHolder$State[a.ordinal()]) {
            case 2: {
                if (this.iconScaleAnimationDisabled) {
                    i0 = selectedIconSize;
                    a1 = com.navdy.hud.app.ui.component.image.CrossFadeImageView$Mode.SMALL;
                    break;
                } else {
                    i0 = unselectedIconSize;
                    a1 = com.navdy.hud.app.ui.component.image.CrossFadeImageView$Mode.SMALL;
                    break;
                }
            }
            case 1: {
                i0 = selectedIconSize;
                a1 = com.navdy.hud.app.ui.component.image.CrossFadeImageView$Mode.BIG;
                break;
            }
            default: {
                a1 = null;
                i0 = 0;
            }
        }
        if (this.imageIconSize != -1) {
            i0 = this.imageIconSize;
        }
        switch(com.navdy.hud.app.ui.component.vlist.viewholder.IconsTwoViewHolder$1.$SwitchMap$com$navdy$hud$app$ui$component$vlist$viewholder$VerticalViewHolder$AnimationType[a0.ordinal()]) {
            case 3: {
                this.animatorSetBuilder.with(com.navdy.hud.app.ui.component.vmenu.VerticalAnimationUtils.animateDimension((android.view.View)this.iconContainer, i0));
                if (this.crossFadeImageView.getMode() == a1) {
                    break;
                }
                this.animatorSetBuilder.with((android.animation.Animator)this.crossFadeImageView.getCrossFadeAnimator());
                break;
            }
            case 1: case 2: {
                android.view.ViewGroup$MarginLayoutParams a2 = (android.view.ViewGroup$MarginLayoutParams)this.iconContainer.getLayoutParams();
                a2.width = i0;
                a2.height = i0;
                this.crossFadeImageView.setMode(a1);
                break;
            }
        }
    }
}
