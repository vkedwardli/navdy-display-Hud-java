package com.navdy.hud.app.ui.component.mainmenu;

@Layout(R.layout.screen_main_menu_2)
public class MainMenuScreen2 extends com.navdy.hud.app.screen.BaseScreen {
    final public static String ARG_CONTACTS_LIST = "CONTACTS";
    final public static String ARG_MENU_MODE = "MENU_MODE";
    final public static String ARG_MENU_PATH = "MENU_PATH";
    final public static String ARG_NOTIFICATION_ID = "NOTIF_ID";
    final static String SLASH = "/";
    final private static com.navdy.service.library.log.Logger sLogger;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.class);
    }
    
    public MainMenuScreen2() {
    }
    
    static com.navdy.service.library.log.Logger access$000() {
        return sLogger;
    }
    
    public int getAnimationIn(flow.Flow$Direction a) {
        return -1;
    }
    
    public int getAnimationOut(flow.Flow$Direction a) {
        return -1;
    }
    
    public Object getDaggerModule() {
        return new com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2$Module(this);
    }
    
    public String getMortarScopeName() {
        return (this).getClass().getName();
    }
    
    public com.navdy.service.library.events.ui.Screen getScreen() {
        return com.navdy.service.library.events.ui.Screen.SCREEN_MAIN_MENU;
    }
}
