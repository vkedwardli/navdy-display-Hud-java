package com.navdy.hud.app.ui.component.homescreen;
import com.navdy.hud.app.R;

public class EtaView extends android.widget.RelativeLayout {
    private com.squareup.otto.Bus bus;
    @InjectView(R.id.etaTextView)
    android.widget.TextView etaTextView;
    @InjectView(R.id.etaTimeAmPm)
    android.widget.TextView etaTimeAmPm;
    @InjectView(R.id.etaTimeLeft)
    android.widget.TextView etaTimeLeft;
    private com.navdy.hud.app.ui.component.homescreen.HomeScreenView homeScreenView;
    private String lastETA;
    private String lastETADate;
    private String lastETA_AmPm;
    private com.navdy.service.library.log.Logger logger;
    
    public EtaView(android.content.Context a) {
        this(a, (android.util.AttributeSet)null);
    }
    
    public EtaView(android.content.Context a, android.util.AttributeSet a0) {
        this(a, a0, 0);
    }
    
    public EtaView(android.content.Context a, android.util.AttributeSet a0, int i) {
        super(a, a0, i);
    }
    
    private String getEtaTimeLeft(int i) {
        String s = null;
        android.content.res.Resources a = this.getResources();
        if (i >= 60) {
            if (i >= 1440) {
                int i0 = i / 1440;
                int i1 = (i - i0 * 1440) / 60;
                if (i1 <= 0) {
                    Object[] a0 = new Object[1];
                    a0[0] = Integer.valueOf(i0);
                    s = a.getString(R.string.eta_time_day_no_hour, a0);
                } else {
                    Object[] a1 = new Object[2];
                    a1[0] = Integer.valueOf(i0);
                    a1[1] = Integer.valueOf(i1);
                    s = a.getString(R.string.eta_time_day, a1);
                }
            } else {
                int i2 = i / 60;
                int i3 = i - i2 * 60;
                if (i3 <= 0) {
                    Object[] a2 = new Object[1];
                    a2[0] = Integer.valueOf(i2);
                    s = a.getString(R.string.eta_time_hour_no_min, a2);
                } else {
                    Object[] a3 = new Object[2];
                    a3[0] = Integer.valueOf(i2);
                    a3[1] = Integer.valueOf(i3);
                    s = a.getString(R.string.eta_time_hour, a3);
                }
            }
        } else {
            Object[] a4 = new Object[1];
            a4[0] = Integer.valueOf(i);
            s = a.getString(R.string.eta_time_min, a4);
        }
        return s;
    }
    
    private void setEtaTextColor(int i) {
        this.etaTextView.setTextColor(i);
        this.etaTimeLeft.setTextColor(i);
        this.etaTimeAmPm.setTextColor(i);
    }
    
    public void clearState() {
        this.lastETA = null;
        this.lastETA_AmPm = null;
        this.lastETADate = null;
        this.etaTextView.setText((CharSequence)"");
        this.etaTimeLeft.setText((CharSequence)"");
        this.etaTimeAmPm.setText((CharSequence)"");
        this.setEtaTextColor(-1);
    }
    
    public String getLastEtaDate() {
        return this.lastETADate;
    }
    
    public void init(com.navdy.hud.app.ui.component.homescreen.HomeScreenView a) {
        this.homeScreenView = a;
        this.bus.register(this);
    }
    
    protected void onFinishInflate() {
        this.logger = com.navdy.hud.app.ui.component.homescreen.HomeScreenView.sLogger;
        super.onFinishInflate();
        butterknife.ButterKnife.inject((android.view.View)this);
        if (!this.isInEditMode()) {
            if (com.navdy.hud.app.ui.component.homescreen.HomeScreenView.showDriveScoreEventsOnMap()) {
                this.etaTimeLeft.setVisibility(8);
            }
            this.bus = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getBus();
        }
    }
    
    public void onTrafficDelayDismissEvent(com.navdy.hud.app.maps.MapEvents$TrafficDelayDismissEvent a) {
        this.setEtaTextColor(-1);
    }
    
    public void onTrafficDelayEvent(com.navdy.hud.app.maps.MapEvents$TrafficDelayEvent a) {
        this.setEtaTextColor(com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.badTrafficColor);
    }
    
    public void setView(com.navdy.hud.app.view.MainView$CustomAnimationMode a) {
        switch(com.navdy.hud.app.ui.component.homescreen.EtaView$1.$SwitchMap$com$navdy$hud$app$view$MainView$CustomAnimationMode[a.ordinal()]) {
            case 2: {
                this.setX((float)com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.etaShrinkLeftX);
                break;
            }
            case 1: {
                this.setX((float)com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.etaX);
                break;
            }
        }
    }
    
    public void updateETA(com.navdy.hud.app.maps.MapEvents$ManeuverDisplay a) {
        if (this.homeScreenView.isNavigationActive()) {
            if (a.etaDate != null) {
                long j = a.etaDate.getTime() - System.currentTimeMillis();
                long j0 = (j >= 0L) ? java.util.concurrent.TimeUnit.MILLISECONDS.toMinutes(j) : 0L;
                String s = com.navdy.hud.app.maps.util.RouteUtils.formatEtaMinutes(this.getResources(), (int)j0);
                this.etaTimeLeft.setText((CharSequence)s);
                if (!android.text.TextUtils.equals((CharSequence)a.eta, (CharSequence)this.lastETA)) {
                    this.lastETA = a.eta;
                    this.lastETADate = com.navdy.hud.app.maps.here.HereMapUtil.convertDateToEta(a.etaDate);
                    this.etaTextView.setText((CharSequence)this.lastETA);
                }
                if (!android.text.TextUtils.equals((CharSequence)a.etaAmPm, (CharSequence)this.lastETA_AmPm)) {
                    this.lastETA_AmPm = a.etaAmPm;
                    this.etaTimeAmPm.setText((CharSequence)this.lastETA_AmPm);
                }
            } else {
                this.logger.w("etaDate is not set");
            }
        }
    }
}
