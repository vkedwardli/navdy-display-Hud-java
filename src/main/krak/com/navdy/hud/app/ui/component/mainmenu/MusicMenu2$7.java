package com.navdy.hud.app.ui.component.mainmenu;

class MusicMenu2$7 implements Runnable {
    final com.navdy.hud.app.ui.component.mainmenu.MusicMenu2 this$0;
    final android.graphics.Bitmap val$artwork;
    final byte[] val$byteArray;
    final String val$idString;
    final Integer val$pos;
    final boolean val$saveInDiskCache;
    
    MusicMenu2$7(com.navdy.hud.app.ui.component.mainmenu.MusicMenu2 a, String s, android.graphics.Bitmap a0, Integer a1, boolean b, byte[] a2) {
        super();
        this.this$0 = a;
        this.val$idString = s;
        this.val$artwork = a0;
        this.val$pos = a1;
        this.val$saveInDiskCache = b;
        this.val$byteArray = a2;
    }
    
    public void run() {
        com.navdy.hud.app.util.picasso.PicassoUtil.setBitmapInCache(this.val$idString, this.val$artwork);
        Integer a = this.val$pos;
        label1: {
            com.navdy.service.library.events.audio.MusicCollectionInfo a0 = null;
            label5: {
                label4: {
                    if (a != null) {
                        break label4;
                    }
                    com.navdy.hud.app.ui.component.mainmenu.MusicMenu2.access$1100(this.this$0).setSelectedIconImage(this.val$artwork);
                    a0 = com.navdy.hud.app.ui.component.mainmenu.MusicMenu2.access$600(this.this$0).musicCollectionInfo;
                    break label5;
                }
                if (com.navdy.hud.app.ui.component.mainmenu.MusicMenu2.access$1000(this.this$0).getCurrentMenu() == this.this$0) {
                    com.navdy.hud.app.ui.component.mainmenu.MusicMenu2.access$400().d(new StringBuilder().append("Refreshing artwork for position: ").append(this.val$pos).toString());
                    com.navdy.hud.app.ui.component.mainmenu.MusicMenu2.access$1000(this.this$0).refreshDataforPos(this.val$pos.intValue());
                }
                int i = com.navdy.hud.app.ui.component.mainmenu.MusicMenu2.access$1400(this.this$0).size();
                int i0 = this.val$pos.intValue();
                label2: {
                    label3: {
                        if (i0 < 0) {
                            break label3;
                        }
                        if (this.val$pos.intValue() < i) {
                            break label2;
                        }
                    }
                    com.navdy.hud.app.ui.component.mainmenu.MusicMenu2.access$400().e(new StringBuilder().append("Refreshing artwork invalid index: ").append(this.val$pos).append(" size=").append(i).toString());
                    break label1;
                }
                Object a1 = ((com.navdy.hud.app.ui.component.vlist.VerticalList$Model)com.navdy.hud.app.ui.component.mainmenu.MusicMenu2.access$1400(this.this$0).get(this.val$pos.intValue())).state;
                boolean b = a1 instanceof com.navdy.service.library.events.audio.MusicCollectionInfo;
                label0: {
                    if (b) {
                        break label0;
                    }
                    com.navdy.hud.app.ui.component.mainmenu.MusicMenu2.access$400().e("Something has gone terribly wrong.");
                    break label1;
                }
                a0 = (com.navdy.service.library.events.audio.MusicCollectionInfo)a1;
            }
            if (this.val$saveInDiskCache && a0 != null && a0.collectionType != null) {
                com.navdy.hud.app.ui.component.mainmenu.MusicMenu2.access$400().d(new StringBuilder().append("Setting bitmap in cache for collection: ").append(a0).toString());
                this.this$0.musicArtworkCache.putArtwork(a0, this.val$byteArray);
            }
        }
    }
}
