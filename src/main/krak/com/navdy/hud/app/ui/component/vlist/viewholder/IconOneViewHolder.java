package com.navdy.hud.app.ui.component.vlist.viewholder;
import com.navdy.hud.app.R;

public class IconOneViewHolder extends com.navdy.hud.app.ui.component.vlist.viewholder.IconBaseViewHolder {
    final private static com.navdy.service.library.log.Logger sLogger;
    
    static {
        sLogger = com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.sLogger;
    }
    
    private IconOneViewHolder(android.view.ViewGroup a, com.navdy.hud.app.ui.component.vlist.VerticalList a0, android.os.Handler a1) {
        super(a, a0, a1);
    }
    
    public static com.navdy.hud.app.ui.component.vlist.VerticalList$Model buildModel(int i, int i0, int i1, String s, String s0) {
        return com.navdy.hud.app.ui.component.vlist.viewholder.IconOneViewHolder.buildModel(i, i0, i1, s, s0, (String)null);
    }
    
    public static com.navdy.hud.app.ui.component.vlist.VerticalList$Model buildModel(int i, int i0, int i1, String s, String s0, String s1) {
        com.navdy.hud.app.ui.component.vlist.VerticalList$Model a = new com.navdy.hud.app.ui.component.vlist.VerticalList$Model();
        a.type = com.navdy.hud.app.ui.component.vlist.VerticalList$ModelType.ICON;
        a.id = i;
        a.icon = i0;
        a.iconFluctuatorColor = i1;
        a.title = s;
        a.subTitle = s0;
        a.subTitle2 = s1;
        return a;
    }
    
    public static com.navdy.hud.app.ui.component.vlist.viewholder.IconOneViewHolder buildViewHolder(android.view.ViewGroup a, com.navdy.hud.app.ui.component.vlist.VerticalList a0, android.os.Handler a1) {
        return new com.navdy.hud.app.ui.component.vlist.viewholder.IconOneViewHolder(com.navdy.hud.app.ui.component.vlist.viewholder.IconOneViewHolder.getLayout(a, R.layout.vlist_item, R.layout.crossfade_image_lyt), a0, a1);
    }
    
    private void setIcon(int i, boolean b, boolean b0) {
        java.util.HashMap a = this.extras;
        String s = null;
        if (a != null) {
            s = (String)this.extras.get("INITIAL");
        }
        if (b) {
            com.navdy.hud.app.ui.component.image.InitialsImageView$Style a0 = (s == null) ? com.navdy.hud.app.ui.component.image.InitialsImageView$Style.DEFAULT : com.navdy.hud.app.ui.component.image.InitialsImageView$Style.MEDIUM;
            ((com.navdy.hud.app.ui.component.image.InitialsImageView)this.crossFadeImageView.getBig()).setImage(i, (String)null, a0);
        }
        if (b0) {
            com.navdy.hud.app.ui.component.image.InitialsImageView$Style a1 = (s == null) ? com.navdy.hud.app.ui.component.image.InitialsImageView$Style.DEFAULT : com.navdy.hud.app.ui.component.image.InitialsImageView$Style.TINY;
            com.navdy.hud.app.ui.component.image.InitialsImageView a2 = (com.navdy.hud.app.ui.component.image.InitialsImageView)this.crossFadeImageView.getSmall();
            a2.setAlpha(0.5f);
            a2.setImage(i, s, a1);
        }
    }
    
    public void bind(com.navdy.hud.app.ui.component.vlist.VerticalList$Model a, com.navdy.hud.app.ui.component.vlist.VerticalList$ModelState a0) {
        super.bind(a, a0);
        this.setIcon(a.icon, a0.updateImage, a0.updateSmallImage);
    }
    
    public com.navdy.hud.app.ui.component.vlist.VerticalList$ModelType getModelType() {
        return com.navdy.hud.app.ui.component.vlist.VerticalList$ModelType.ICON;
    }
    
    public void setItemState(com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$State a, com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$AnimationType a0, int i, boolean b) {
        com.navdy.hud.app.ui.component.image.CrossFadeImageView$Mode a1 = null;
        float f = 0.0f;
        float f0 = 0.0f;
        super.setItemState(a, a0, i, b);
        switch(com.navdy.hud.app.ui.component.vlist.viewholder.IconOneViewHolder$1.$SwitchMap$com$navdy$hud$app$ui$component$vlist$viewholder$VerticalViewHolder$State[a.ordinal()]) {
            case 2: {
                a1 = com.navdy.hud.app.ui.component.image.CrossFadeImageView$Mode.SMALL;
                f = 0.6f;
                f0 = 0.5f;
                break;
            }
            case 1: {
                a1 = com.navdy.hud.app.ui.component.image.CrossFadeImageView$Mode.BIG;
                f = 1f;
                f0 = 1f;
                break;
            }
            default: {
                a1 = null;
                f = 0.0f;
                f0 = 0.0f;
            }
        }
        switch(com.navdy.hud.app.ui.component.vlist.viewholder.IconOneViewHolder$1.$SwitchMap$com$navdy$hud$app$ui$component$vlist$viewholder$VerticalViewHolder$AnimationType[a0.ordinal()]) {
            case 3: {
                android.util.Property a2 = android.view.View.SCALE_X;
                float[] a3 = new float[1];
                a3[0] = f;
                android.animation.PropertyValuesHolder a4 = android.animation.PropertyValuesHolder.ofFloat(a2, a3);
                android.util.Property a5 = android.view.View.SCALE_Y;
                float[] a6 = new float[1];
                a6[0] = f;
                android.animation.PropertyValuesHolder a7 = android.animation.PropertyValuesHolder.ofFloat(a5, a6);
                android.util.Property a8 = android.view.View.ALPHA;
                float[] a9 = new float[1];
                a9[0] = f0;
                android.animation.PropertyValuesHolder a10 = android.animation.PropertyValuesHolder.ofFloat(a8, a9);
                android.animation.AnimatorSet$Builder a11 = this.animatorSetBuilder;
                android.view.ViewGroup a12 = this.imageContainer;
                android.animation.PropertyValuesHolder[] a13 = new android.animation.PropertyValuesHolder[3];
                a13[0] = a4;
                a13[1] = a7;
                a13[2] = a10;
                a11.with((android.animation.Animator)android.animation.ObjectAnimator.ofPropertyValuesHolder(a12, a13));
                if (this.crossFadeImageView.getMode() == a1) {
                    break;
                }
                this.animatorSetBuilder.with((android.animation.Animator)this.crossFadeImageView.getCrossFadeAnimator());
                break;
            }
            case 1: case 2: {
                this.imageContainer.setScaleX(f);
                this.imageContainer.setScaleY(f);
                this.imageContainer.setAlpha(f0);
                this.crossFadeImageView.setMode(a1);
                break;
            }
        }
    }
}
