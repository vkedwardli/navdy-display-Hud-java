package com.navdy.hud.app.ui.component;


public enum SystemTrayView$State {
    CONNECTED(0),
    DISCONNECTED(1),
    LOW_BATTERY(2),
    EXTREMELY_LOW_BATTERY(3),
    OK_BATTERY(4),
    NO_PHONE_NETWORK(5),
    PHONE_NETWORK_AVAILABLE(6),
    LOCATION_LOST(7),
    LOCATION_AVAILABLE(8);

    private int value;
    SystemTrayView$State(int value) {
        this.value = value;
    }
    public int getValue() {
        return value;
    }
}

//final public class SystemTrayView$State extends Enum {
//    final private static com.navdy.hud.app.ui.component.SystemTrayView$State[] $VALUES;
//    final public static com.navdy.hud.app.ui.component.SystemTrayView$State CONNECTED;
//    final public static com.navdy.hud.app.ui.component.SystemTrayView$State DISCONNECTED;
//    final public static com.navdy.hud.app.ui.component.SystemTrayView$State EXTREMELY_LOW_BATTERY;
//    final public static com.navdy.hud.app.ui.component.SystemTrayView$State LOCATION_AVAILABLE;
//    final public static com.navdy.hud.app.ui.component.SystemTrayView$State LOCATION_LOST;
//    final public static com.navdy.hud.app.ui.component.SystemTrayView$State LOW_BATTERY;
//    final public static com.navdy.hud.app.ui.component.SystemTrayView$State NO_PHONE_NETWORK;
//    final public static com.navdy.hud.app.ui.component.SystemTrayView$State OK_BATTERY;
//    final public static com.navdy.hud.app.ui.component.SystemTrayView$State PHONE_NETWORK_AVAILABLE;
//    
//    static {
//        CONNECTED = new com.navdy.hud.app.ui.component.SystemTrayView$State("CONNECTED", 0);
//        DISCONNECTED = new com.navdy.hud.app.ui.component.SystemTrayView$State("DISCONNECTED", 1);
//        LOW_BATTERY = new com.navdy.hud.app.ui.component.SystemTrayView$State("LOW_BATTERY", 2);
//        EXTREMELY_LOW_BATTERY = new com.navdy.hud.app.ui.component.SystemTrayView$State("EXTREMELY_LOW_BATTERY", 3);
//        OK_BATTERY = new com.navdy.hud.app.ui.component.SystemTrayView$State("OK_BATTERY", 4);
//        NO_PHONE_NETWORK = new com.navdy.hud.app.ui.component.SystemTrayView$State("NO_PHONE_NETWORK", 5);
//        PHONE_NETWORK_AVAILABLE = new com.navdy.hud.app.ui.component.SystemTrayView$State("PHONE_NETWORK_AVAILABLE", 6);
//        LOCATION_LOST = new com.navdy.hud.app.ui.component.SystemTrayView$State("LOCATION_LOST", 7);
//        LOCATION_AVAILABLE = new com.navdy.hud.app.ui.component.SystemTrayView$State("LOCATION_AVAILABLE", 8);
//        com.navdy.hud.app.ui.component.SystemTrayView$State[] a = new com.navdy.hud.app.ui.component.SystemTrayView$State[9];
//        a[0] = CONNECTED;
//        a[1] = DISCONNECTED;
//        a[2] = LOW_BATTERY;
//        a[3] = EXTREMELY_LOW_BATTERY;
//        a[4] = OK_BATTERY;
//        a[5] = NO_PHONE_NETWORK;
//        a[6] = PHONE_NETWORK_AVAILABLE;
//        a[7] = LOCATION_LOST;
//        a[8] = LOCATION_AVAILABLE;
//        $VALUES = a;
//    }
//    
//    private SystemTrayView$State(String s, int i) {
//        super(s, i);
//    }
//    
//    public static com.navdy.hud.app.ui.component.SystemTrayView$State valueOf(String s) {
//        return (com.navdy.hud.app.ui.component.SystemTrayView$State)Enum.valueOf(com.navdy.hud.app.ui.component.SystemTrayView$State.class, s);
//    }
//    
//    public static com.navdy.hud.app.ui.component.SystemTrayView$State[] values() {
//        return $VALUES.clone();
//    }
//}
//