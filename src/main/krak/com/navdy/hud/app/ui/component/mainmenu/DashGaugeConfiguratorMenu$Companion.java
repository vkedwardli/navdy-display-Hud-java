package com.navdy.hud.app.ui.component.mainmenu;

final public class DashGaugeConfiguratorMenu$Companion {
    private DashGaugeConfiguratorMenu$Companion() {
    }
    
    public DashGaugeConfiguratorMenu$Companion(kotlin.jvm.internal.DefaultConstructorMarker a) {
        this();
    }
    
    final public static com.navdy.hud.app.ui.component.vlist.VerticalList$Model access$getBack$p(com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu$Companion a) {
        return a.getBack();
    }
    
    final public static int access$getBackColor$p(com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu$Companion a) {
        return a.getBackColor();
    }
    
    final public static int access$getBkColorUnselected$p(com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu$Companion a) {
        return a.getBkColorUnselected();
    }
    
    final public static int access$getCenterGaugeBackgroundColor$p(com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu$Companion a) {
        return a.getCenterGaugeBackgroundColor();
    }
    
    final public static java.util.ArrayList access$getCenterGaugeOptionsList$p(com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu$Companion a) {
        return a.getCenterGaugeOptionsList();
    }
    
    final public static String access$getCenterGaugeTitle$p(com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu$Companion a) {
        return a.getCenterGaugeTitle();
    }
    
    final public static String access$getOffLabel$p(com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu$Companion a) {
        return a.getOffLabel();
    }
    
    final public static String access$getOnLabel$p(com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu$Companion a) {
        return a.getOnLabel();
    }
    
    final public static com.navdy.service.library.log.Logger access$getSLogger$p(com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu$Companion a) {
        return a.getSLogger();
    }
    
    final public static java.util.ArrayList access$getSideGaugesOptionsList$p(com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu$Companion a) {
        return a.getSideGaugesOptionsList();
    }
    
    final public static String access$getSideGaugesTitle$p(com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu$Companion a) {
        return a.getSideGaugesTitle();
    }
    
    final public static com.navdy.hud.app.ui.component.vlist.VerticalList$Model access$getSpeedoMeter$p(com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu$Companion a) {
        return a.getSpeedoMeter();
    }
    
    final public static com.navdy.hud.app.ui.component.vlist.VerticalList$Model access$getTachoMeter$p(com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu$Companion a) {
        return a.getTachoMeter();
    }
    
    final public static com.navdy.hud.app.ui.framework.UIStateManager access$getUiStateManager$p(com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu$Companion a) {
        return a.getUiStateManager();
    }
    
    final public static String access$getUnavailableLabel$p(com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu$Companion a) {
        return a.getUnavailableLabel();
    }
    
    final private com.navdy.hud.app.ui.component.vlist.VerticalList$Model getBack() {
        return com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu.access$getBack$cp();
    }
    
    final private int getBackColor() {
        return com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu.access$getBackColor$cp();
    }
    
    final private int getBkColorUnselected() {
        return com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu.access$getBkColorUnselected$cp();
    }
    
    final private int getCenterGaugeBackgroundColor() {
        return com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu.access$getCenterGaugeBackgroundColor$cp();
    }
    
    final private java.util.ArrayList getCenterGaugeOptionsList() {
        return com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu.access$getCenterGaugeOptionsList$cp();
    }
    
    final private String getCenterGaugeTitle() {
        return com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu.access$getCenterGaugeTitle$cp();
    }
    
    final private String getOffLabel() {
        return com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu.access$getOffLabel$cp();
    }
    
    final private String getOnLabel() {
        return com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu.access$getOnLabel$cp();
    }
    
    final private com.navdy.service.library.log.Logger getSLogger() {
        return com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu.access$getSLogger$cp();
    }
    
    final private java.util.ArrayList getSideGaugesOptionsList() {
        return com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu.access$getSideGaugesOptionsList$cp();
    }
    
    final private String getSideGaugesTitle() {
        return com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu.access$getSideGaugesTitle$cp();
    }
    
    final private com.navdy.hud.app.ui.component.vlist.VerticalList$Model getSpeedoMeter() {
        return com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu.access$getSpeedoMeter$cp();
    }
    
    final private com.navdy.hud.app.ui.component.vlist.VerticalList$Model getTachoMeter() {
        return com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu.access$getTachoMeter$cp();
    }
    
    final private com.navdy.hud.app.ui.framework.UIStateManager getUiStateManager() {
        return com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu.access$getUiStateManager$cp();
    }
    
    final private String getUnavailableLabel() {
        return com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu.access$getUnavailableLabel$cp();
    }
}
