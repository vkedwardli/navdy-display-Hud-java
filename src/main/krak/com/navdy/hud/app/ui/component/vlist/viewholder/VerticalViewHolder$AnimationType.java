package com.navdy.hud.app.ui.component.vlist.viewholder;


public enum VerticalViewHolder$AnimationType {
    NONE(0),
    MOVE(1),
    INIT(2);

    private int value;
    VerticalViewHolder$AnimationType(int value) {
        this.value = value;
    }
    public int getValue() {
        return value;
    }
}

//final public class VerticalViewHolder$AnimationType extends Enum {
//    final private static com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$AnimationType[] $VALUES;
//    final public static com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$AnimationType INIT;
//    final public static com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$AnimationType MOVE;
//    final public static com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$AnimationType NONE;
//    
//    static {
//        NONE = new com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$AnimationType("NONE", 0);
//        MOVE = new com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$AnimationType("MOVE", 1);
//        INIT = new com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$AnimationType("INIT", 2);
//        com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$AnimationType[] a = new com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$AnimationType[3];
//        a[0] = NONE;
//        a[1] = MOVE;
//        a[2] = INIT;
//        $VALUES = a;
//    }
//    
//    private VerticalViewHolder$AnimationType(String s, int i) {
//        super(s, i);
//    }
//    
//    public static com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$AnimationType valueOf(String s) {
//        return (com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$AnimationType)Enum.valueOf(com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$AnimationType.class, s);
//    }
//    
//    public static com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$AnimationType[] values() {
//        return $VALUES.clone();
//    }
//}
//