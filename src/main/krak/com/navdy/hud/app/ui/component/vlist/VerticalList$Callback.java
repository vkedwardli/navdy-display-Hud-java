package com.navdy.hud.app.ui.component.vlist;

abstract public interface VerticalList$Callback {
    abstract public void onBindToView(com.navdy.hud.app.ui.component.vlist.VerticalList$Model arg, android.view.View arg0, int arg1, com.navdy.hud.app.ui.component.vlist.VerticalList$ModelState arg2);
    
    
    abstract public void onItemSelected(com.navdy.hud.app.ui.component.vlist.VerticalList$ItemSelectionState arg);
    
    
    abstract public void onLoad();
    
    
    abstract public void onScrollIdle();
    
    
    abstract public void select(com.navdy.hud.app.ui.component.vlist.VerticalList$ItemSelectionState arg);
}
