package com.navdy.hud.app.ui.component.homescreen;
import com.navdy.hud.app.R;

public class RecalculatingView$$ViewInjector {
    public RecalculatingView$$ViewInjector() {
    }
    
    public static void inject(butterknife.ButterKnife$Finder a, com.navdy.hud.app.ui.component.homescreen.RecalculatingView a0, Object a1) {
        a0.recalcImageView = (com.navdy.hud.app.ui.component.image.ColorImageView)a.findRequiredView(a1, R.id.recalcImageView, "field 'recalcImageView'");
        a0.recalcAnimator = (com.navdy.hud.app.ui.component.FluctuatorAnimatorView)a.findRequiredView(a1, R.id.recalcAnimator, "field 'recalcAnimator'");
    }
    
    public static void reset(com.navdy.hud.app.ui.component.homescreen.RecalculatingView a) {
        a.recalcImageView = null;
        a.recalcAnimator = null;
    }
}
