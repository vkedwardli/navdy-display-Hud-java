package com.navdy.hud.app.ui.component;

class ChoiceLayout$4 implements android.animation.ValueAnimator$AnimatorUpdateListener {
    final com.navdy.hud.app.ui.component.ChoiceLayout this$0;
    
    ChoiceLayout$4(com.navdy.hud.app.ui.component.ChoiceLayout a) {
        super();
        this.this$0 = a;
    }
    
    public void onAnimationUpdate(android.animation.ValueAnimator a) {
        if (com.navdy.hud.app.ui.component.ChoiceLayout.access$100(this.this$0) != null) {
            int i = ((Integer)a.getAnimatedValue()).intValue();
            ((android.view.ViewGroup$MarginLayoutParams)this.this$0.highlightView.getLayoutParams()).topMargin = i;
            this.this$0.highlightView.invalidate();
            this.this$0.highlightView.requestLayout();
        }
    }
}
