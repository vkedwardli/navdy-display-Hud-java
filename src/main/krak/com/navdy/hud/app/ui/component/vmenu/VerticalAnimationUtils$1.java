package com.navdy.hud.app.ui.component.vmenu;

final class VerticalAnimationUtils$1 implements Runnable {
    final boolean val$clickup;
    final int val$duration;
    final Runnable val$endAction;
    final android.view.View val$view;
    
    VerticalAnimationUtils$1(boolean b, android.view.View a, int i, Runnable a0) {
        super();
        this.val$clickup = b;
        this.val$view = a;
        this.val$duration = i;
        this.val$endAction = a0;
    }
    
    public void run() {
        if (this.val$clickup) {
            com.navdy.hud.app.ui.component.vmenu.VerticalAnimationUtils.performClickUp(this.val$view, this.val$duration, this.val$endAction);
        } else if (this.val$endAction != null) {
            this.val$endAction.run();
        }
    }
}
