package com.navdy.hud.app.ui.component.systeminfo;
import com.navdy.hud.app.R;

public class GpsSignalView extends android.widget.LinearLayout {
    private com.navdy.service.library.log.Logger logger;
    
    public GpsSignalView(android.content.Context a) {
        super(a);
        this.logger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.ui.component.systeminfo.GpsSignalView.class);
    }
    
    public GpsSignalView(android.content.Context a, android.util.AttributeSet a0) {
        super(a, a0);
        this.logger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.ui.component.systeminfo.GpsSignalView.class);
    }
    
    public GpsSignalView(android.content.Context a, android.util.AttributeSet a0, int i) {
        super(a, a0, i);
        this.logger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.ui.component.systeminfo.GpsSignalView.class);
        this.setOrientation(0);
    }
    
    public void setSatelliteData(com.navdy.hud.app.ui.component.systeminfo.GpsSignalView$SatelliteData[] a) {
        this.removeAllViews();
        android.view.LayoutInflater a0 = android.view.LayoutInflater.from(this.getContext());
        android.content.res.Resources a1 = com.navdy.hud.app.HudApplication.getAppContext().getResources();
        int i = a1.getDimensionPixelSize(R.dimen.satellite_width_separator);
        int i0 = a1.getDimensionPixelSize(R.dimen.satellite_width);
        int i1 = 0;
        while(i1 < a.length) {
            android.view.ViewGroup a2 = (android.view.ViewGroup)a0.inflate(R.layout.screen_home_system_info_gps_satellite, (android.view.ViewGroup)null);
            a2.setLayoutParams((android.view.ViewGroup$LayoutParams)new android.widget.LinearLayout$LayoutParams(i0, -1));
            ((android.widget.TextView)a2.findViewById(R.id.satellite_cno)).setText((CharSequence)String.valueOf(a[i1].cNo));
            ((android.widget.TextView)a2.findViewById(R.id.satellite_type)).setText((CharSequence)a[i1].provider);
            ((android.widget.TextView)a2.findViewById(R.id.satellite_id)).setText((CharSequence)String.valueOf(a[i1].satelliteId));
            ((android.widget.RelativeLayout$LayoutParams)a2.findViewById(R.id.satellite_strength).getLayoutParams()).height = a[i1].cNo * 2;
            this.addView((android.view.View)a2);
            if (i1 < a.length - 1) {
                android.view.View a3 = new android.view.View(this.getContext());
                a3.setLayoutParams((android.view.ViewGroup$LayoutParams)new android.widget.LinearLayout$LayoutParams(i, i));
                this.addView(a3);
            }
            i1 = i1 + 1;
        }
        this.logger.v(new StringBuilder().append("set satellite =").append(a.length).toString());
    }
}
