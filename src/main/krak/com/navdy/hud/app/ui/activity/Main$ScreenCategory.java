package com.navdy.hud.app.ui.activity;


    public enum Main$ScreenCategory {
        NOTIFICATION(0),
        SCREEN(1);

        private int value;
        Main$ScreenCategory(int value) {
            this.value = value;
        }
        public int getValue() {
            return value;
        }
    }
    
//final class Main$ScreenCategory extends Enum {
//    final private static com.navdy.hud.app.ui.activity.Main$ScreenCategory[] $VALUES;
//    final public static com.navdy.hud.app.ui.activity.Main$ScreenCategory NOTIFICATION;
//    final public static com.navdy.hud.app.ui.activity.Main$ScreenCategory SCREEN;
//    
//    static {
//        NOTIFICATION = new com.navdy.hud.app.ui.activity.Main$ScreenCategory("NOTIFICATION", 0);
//        SCREEN = new com.navdy.hud.app.ui.activity.Main$ScreenCategory("SCREEN", 1);
//        com.navdy.hud.app.ui.activity.Main$ScreenCategory[] a = new com.navdy.hud.app.ui.activity.Main$ScreenCategory[2];
//        a[0] = NOTIFICATION;
//        a[1] = SCREEN;
//        $VALUES = a;
//    }
//    
//    private Main$ScreenCategory(String s, int i) {
//        super(s, i);
//    }
//    
//    public static com.navdy.hud.app.ui.activity.Main$ScreenCategory valueOf(String s) {
//        return (com.navdy.hud.app.ui.activity.Main$ScreenCategory)Enum.valueOf(com.navdy.hud.app.ui.activity.Main$ScreenCategory.class, s);
//    }
//    
//    public static com.navdy.hud.app.ui.activity.Main$ScreenCategory[] values() {
//        return $VALUES.clone();
//    }
//}
//