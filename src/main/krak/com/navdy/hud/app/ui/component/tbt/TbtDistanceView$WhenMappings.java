package com.navdy.hud.app.ui.component.tbt;

final public class TbtDistanceView$WhenMappings {
    final public static int[] $EnumSwitchMapping$0;
    
    static {
        $EnumSwitchMapping$0 = new int[com.navdy.hud.app.view.MainView$CustomAnimationMode.values().length];
        $EnumSwitchMapping$0[com.navdy.hud.app.view.MainView$CustomAnimationMode.EXPAND.ordinal()] = 1;
        $EnumSwitchMapping$0[com.navdy.hud.app.view.MainView$CustomAnimationMode.SHRINK_LEFT.ordinal()] = 2;
    }
}
