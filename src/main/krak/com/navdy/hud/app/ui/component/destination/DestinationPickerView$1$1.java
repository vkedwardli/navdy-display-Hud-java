package com.navdy.hud.app.ui.component.destination;

class DestinationPickerView$1$1 implements Runnable {
    final com.navdy.hud.app.ui.component.destination.DestinationPickerView$1 this$1;
    
    DestinationPickerView$1$1(com.navdy.hud.app.ui.component.destination.DestinationPickerView$1 a) {
        super();
        this.this$1 = a;
    }
    
    public void run() {
        if (this.this$1.this$0.presenter.isAlive()) {
            if (this.this$1.this$0.presenter.isShowingRouteMap()) {
                this.this$1.this$0.presenter.startMapFluctuator();
            } else if (this.this$1.this$0.presenter.isShowDestinationMap()) {
                this.this$1.this$0.presenter.itemSelected(this.this$1.this$0.presenter.initialSelection);
                this.this$1.this$0.presenter.itemSelection = this.this$1.this$0.presenter.initialSelection;
            }
        }
    }
}
