package com.navdy.hud.app.ui.component.homescreen;


public enum SmartDashViewConstants$Type {
    Smart(0),
    Eco(1);

    private int value;
    SmartDashViewConstants$Type(int value) {
        this.value = value;
    }
    public int getValue() {
        return value;
    }
}

//final public class SmartDashViewConstants$Type extends Enum {
//    final private static com.navdy.hud.app.ui.component.homescreen.SmartDashViewConstants$Type[] $VALUES;
//    final public static com.navdy.hud.app.ui.component.homescreen.SmartDashViewConstants$Type Eco;
//    final public static com.navdy.hud.app.ui.component.homescreen.SmartDashViewConstants$Type Smart;
//    
//    static {
//        Smart = new com.navdy.hud.app.ui.component.homescreen.SmartDashViewConstants$Type("Smart", 0);
//        Eco = new com.navdy.hud.app.ui.component.homescreen.SmartDashViewConstants$Type("Eco", 1);
//        com.navdy.hud.app.ui.component.homescreen.SmartDashViewConstants$Type[] a = new com.navdy.hud.app.ui.component.homescreen.SmartDashViewConstants$Type[2];
//        a[0] = Smart;
//        a[1] = Eco;
//        $VALUES = a;
//    }
//    
//    private SmartDashViewConstants$Type(String s, int i) {
//        super(s, i);
//    }
//    
//    public static com.navdy.hud.app.ui.component.homescreen.SmartDashViewConstants$Type valueOf(String s) {
//        return (com.navdy.hud.app.ui.component.homescreen.SmartDashViewConstants$Type)Enum.valueOf(com.navdy.hud.app.ui.component.homescreen.SmartDashViewConstants$Type.class, s);
//    }
//    
//    public static com.navdy.hud.app.ui.component.homescreen.SmartDashViewConstants$Type[] values() {
//        return $VALUES.clone();
//    }
//}
//