package com.navdy.hud.app.ui.component.vlist;

abstract public interface VerticalList$ContainerCallback {
    abstract public void hideToolTips();
    
    
    abstract public boolean isCloseMenuVisible();
    
    
    abstract public boolean isFastScrolling();
    
    
    abstract public void showToolTips();
}
