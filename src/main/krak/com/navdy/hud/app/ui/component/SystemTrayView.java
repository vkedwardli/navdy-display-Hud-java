package com.navdy.hud.app.ui.component;
import com.navdy.hud.app.R;

public class SystemTrayView extends android.widget.LinearLayout {
    static java.util.HashMap dialStateResMap;
    static java.util.HashMap phoneStateResMap;
    final private static com.navdy.service.library.log.Logger sLogger;
    private android.widget.TextView debugGpsMarker;
    private com.navdy.hud.app.ui.component.SystemTrayView$State dialBattery;
    private boolean dialConnected;
    @InjectView(R.id.dial)
    android.widget.ImageView dialImageView;
    @InjectView(R.id.gps)
    android.widget.ImageView locationImageView;
    private boolean noNetwork;
    private com.navdy.hud.app.ui.component.SystemTrayView$State phoneBattery;
    private boolean phoneConnected;
    @InjectView(R.id.phone)
    android.widget.ImageView phoneImageView;
    @InjectView(R.id.phoneNetwork)
    android.widget.ImageView phoneNetworkImageView;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.ui.component.SystemTrayView.class);
        phoneStateResMap = new java.util.HashMap();
        dialStateResMap = new java.util.HashMap();
        dialStateResMap.put(com.navdy.hud.app.ui.component.SystemTrayView$State.LOW_BATTERY, Integer.valueOf(R.drawable.icon_dial_batterylow_2_copy));
        dialStateResMap.put(com.navdy.hud.app.ui.component.SystemTrayView$State.EXTREMELY_LOW_BATTERY, Integer.valueOf(R.drawable.icon_dial_batterylow_copy));
        dialStateResMap.put(com.navdy.hud.app.ui.component.SystemTrayView$State.DISCONNECTED, Integer.valueOf(R.drawable.icon_dial_missing_copy));
        phoneStateResMap.put(com.navdy.hud.app.ui.component.SystemTrayView$State.LOW_BATTERY, Integer.valueOf(R.drawable.icon_device_batterylow_copy));
        phoneStateResMap.put(com.navdy.hud.app.ui.component.SystemTrayView$State.EXTREMELY_LOW_BATTERY, Integer.valueOf(R.drawable.icon_device_batterylow_2_copy));
        phoneStateResMap.put(com.navdy.hud.app.ui.component.SystemTrayView$State.DISCONNECTED, Integer.valueOf(R.drawable.icon_device_missing_copy));
    }
    
    public SystemTrayView(android.content.Context a) {
        this(a, (android.util.AttributeSet)null);
    }
    
    public SystemTrayView(android.content.Context a, android.util.AttributeSet a0) {
        this(a, a0, 0);
    }
    
    public SystemTrayView(android.content.Context a, android.util.AttributeSet a0, int i) {
        super(a, a0, i);
        this.phoneBattery = com.navdy.hud.app.ui.component.SystemTrayView$State.OK_BATTERY;
        this.dialBattery = com.navdy.hud.app.ui.component.SystemTrayView$State.OK_BATTERY;
        this.init();
    }
    
    private void handleDialState(com.navdy.hud.app.ui.component.SystemTrayView$State a) {
        if (a != null) {
            switch(com.navdy.hud.app.ui.component.SystemTrayView$1.$SwitchMap$com$navdy$hud$app$ui$component$SystemTrayView$State[a.ordinal()]) {
                case 3: case 4: case 5: {
                    if (this.dialBattery == a) {
                        break;
                    }
                    this.dialBattery = a;
                    this.updateViews();
                    break;
                }
                case 1: case 2: {
                    boolean b = a == com.navdy.hud.app.ui.component.SystemTrayView$State.CONNECTED;
                    if (this.dialConnected == b) {
                        break;
                    }
                    this.dialConnected = b;
                    this.updateViews();
                    break;
                }
            }
        } else {
            this.dialImageView.setImageResource(0);
            this.dialImageView.setVisibility(8);
        }
    }
    
    private void handleGpsState(com.navdy.hud.app.ui.component.SystemTrayView$State a) {
        switch(com.navdy.hud.app.ui.component.SystemTrayView$1.$SwitchMap$com$navdy$hud$app$ui$component$SystemTrayView$State[a.ordinal()]) {
            case 9: {
                this.locationImageView.setVisibility(8);
                break;
            }
            case 8: {
                this.locationImageView.setVisibility(0);
                this.setDebugGpsMarker((String)null);
                break;
            }
        }
    }
    
    private void handlePhoneState(com.navdy.hud.app.ui.component.SystemTrayView$State a) {
        if (a != null) {
            switch(com.navdy.hud.app.ui.component.SystemTrayView$1.$SwitchMap$com$navdy$hud$app$ui$component$SystemTrayView$State[a.ordinal()]) {
                case 6: case 7: {
                    boolean b = a == com.navdy.hud.app.ui.component.SystemTrayView$State.NO_PHONE_NETWORK;
                    if (this.noNetwork == b) {
                        break;
                    }
                    this.noNetwork = b;
                    this.updateViews();
                    break;
                }
                case 3: case 4: case 5: {
                    if (this.phoneBattery == a) {
                        break;
                    }
                    this.phoneBattery = a;
                    this.updateViews();
                    break;
                }
                case 1: case 2: {
                    boolean b0 = a == com.navdy.hud.app.ui.component.SystemTrayView$State.CONNECTED;
                    if (this.phoneConnected == b0) {
                        break;
                    }
                    this.phoneConnected = b0;
                    this.noNetwork = false;
                    this.phoneBattery = com.navdy.hud.app.ui.component.SystemTrayView$State.OK_BATTERY;
                    this.updateViews();
                    break;
                }
            }
        } else {
            this.phoneImageView.setImageResource(0);
            this.phoneImageView.setVisibility(8);
            this.phoneNetworkImageView.setVisibility(8);
        }
    }
    
    private void init() {
        android.view.LayoutInflater.from(this.getContext()).inflate(R.layout.system_tray, (android.view.ViewGroup)this, true);
        this.setOrientation(0);
        butterknife.ButterKnife.inject((android.view.View)this);
        this.dialImageView.setVisibility(8);
        this.phoneImageView.setVisibility(8);
        this.phoneNetworkImageView.setVisibility(8);
        this.locationImageView.setVisibility(8);
    }
    
    private void updateDeviceStatus(android.widget.ImageView a, java.util.HashMap a0, com.navdy.hud.app.ui.component.SystemTrayView$State a1) {
        Integer a2 = (Integer)a0.get(a1);
        int i = (a2 == null) ? 0 : a2.intValue();
        a.setImageResource(i);
        a.setVisibility((i == 0) ? 8 : 0);
    }
    
    private void updateViews() {
        int i = 0;
        android.widget.ImageView a = this.phoneNetworkImageView;
        boolean b = this.noNetwork;
        label2: {
            label0: {
                label1: {
                    if (!b) {
                        break label1;
                    }
                    if (this.phoneConnected) {
                        break label0;
                    }
                }
                i = 8;
                break label2;
            }
            i = 0;
        }
        a.setVisibility(i);
        phoneStateResMap.put(com.navdy.hud.app.ui.component.SystemTrayView$State.OK_BATTERY, Integer.valueOf((this.noNetwork) ? R.drawable.icon_device_battery_ok : 0));
        this.updateDeviceStatus(this.phoneImageView, phoneStateResMap, (this.phoneConnected) ? this.phoneBattery : com.navdy.hud.app.ui.component.SystemTrayView$State.DISCONNECTED);
        this.updateDeviceStatus(this.dialImageView, dialStateResMap, (this.dialConnected) ? this.dialBattery : com.navdy.hud.app.ui.component.SystemTrayView$State.DISCONNECTED);
    }
    
    public void setDebugGpsMarker(String s) {
        try {
            if (s != null) {
                if (this.debugGpsMarker == null) {
                    android.content.Context a = this.getContext();
                    this.debugGpsMarker = new android.widget.TextView(a);
                    this.debugGpsMarker.setTextAppearance(a, R.style.glance_title_3);
                    this.debugGpsMarker.setTextColor(-1);
                    android.widget.LinearLayout$LayoutParams a0 = new android.widget.LinearLayout$LayoutParams(-2, -2);
                    a0.gravity = 16;
                    this.addView((android.view.View)this.debugGpsMarker, 0, (android.view.ViewGroup$LayoutParams)a0);
                }
                this.debugGpsMarker.setText((CharSequence)s);
            } else if (this.debugGpsMarker != null) {
                this.removeView((android.view.View)this.debugGpsMarker);
                this.debugGpsMarker = null;
            }
        } catch(Throwable a1) {
            sLogger.e(a1);
        }
    }
    
    public void setState(com.navdy.hud.app.ui.component.SystemTrayView$Device a, com.navdy.hud.app.ui.component.SystemTrayView$State a0) {
        switch(com.navdy.hud.app.ui.component.SystemTrayView$1.$SwitchMap$com$navdy$hud$app$ui$component$SystemTrayView$Device[a.ordinal()]) {
            case 3: {
                this.handleGpsState(a0);
                break;
            }
            case 2: {
                this.handleDialState(a0);
                break;
            }
            case 1: {
                this.handlePhoneState(a0);
                break;
            }
        }
    }
}
