package com.navdy.hud.app.ui.component.mainmenu;

class ContactOptionsMenu$6 implements Runnable {
    final com.navdy.hud.app.ui.component.mainmenu.ContactOptionsMenu this$0;
    final android.os.Bundle val$args;
    final com.navdy.hud.app.framework.contacts.Contact val$contact;
    
    ContactOptionsMenu$6(com.navdy.hud.app.ui.component.mainmenu.ContactOptionsMenu a, android.os.Bundle a0, com.navdy.hud.app.framework.contacts.Contact a1) {
        super();
        this.this$0 = a;
        this.val$args = a0;
        this.val$contact = a1;
    }
    
    public void run() {
        com.navdy.hud.app.ui.component.mainmenu.ContactOptionsMenu.access$400(this.this$0).post(new com.navdy.hud.app.event.ShowScreenWithArgs(com.navdy.service.library.events.ui.Screen.SCREEN_DESTINATION_PICKER, this.val$args, new com.navdy.hud.app.ui.component.mainmenu.ShareTripMenu(this.val$contact, com.navdy.hud.app.ui.component.mainmenu.ContactOptionsMenu.access$300(this.this$0)), false));
    }
}
