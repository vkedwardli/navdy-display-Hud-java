package com.navdy.hud.app.ui.component.vlist;

public class VerticalList$KeyHandlerState {
    public boolean keyHandled;
    public boolean listMoved;
    
    public VerticalList$KeyHandlerState() {
    }
    
    void clear() {
        this.keyHandled = false;
        this.listMoved = false;
    }
}
