package com.navdy.hud.app.ui.component.homescreen;
import com.navdy.hud.app.R;

public class ImageWidgetPresenter extends com.navdy.hud.app.view.DashboardWidgetPresenter {
    private String id;
    private android.graphics.drawable.Drawable mDrawable;
    
    public ImageWidgetPresenter(android.content.Context a, int i, String s) {
        if (i > 0) {
            this.mDrawable = a.getResources().getDrawable(i);
        }
        this.id = s;
    }
    
    public android.graphics.drawable.Drawable getDrawable() {
        return this.mDrawable;
    }
    
    public String getWidgetIdentifier() {
        return this.id;
    }
    
    public String getWidgetName() {
        return null;
    }
    
    public void setView(com.navdy.hud.app.view.DashboardWidgetView a) {
        if (a != null) {
            a.setContentView(R.layout.smart_dash_widget_layout);
        }
        super.setView(a);
    }
    
    protected void updateGauge() {
    }
}
