package com.navdy.hud.app.ui.component;
import com.navdy.hud.app.R;

public class CarouselTextView extends android.widget.TextView {
    final private static long CAROUSEL_ANIM_DURATION = 500L;
    final private static long CAROUSEL_VIEW_INTERVAL = 5000L;
    private Runnable carouselChangeRunnable;
    private int currentTextIndex;
    final private android.os.Handler handler;
    private java.util.List textList;
    final private float translationY;
    
    public CarouselTextView(android.content.Context a, android.util.AttributeSet a0) {
        super(a, a0);
        this.carouselChangeRunnable = (Runnable)new com.navdy.hud.app.ui.component.CarouselTextView$1(this);
        this.translationY = this.getResources().getDimension(R.dimen.text_fade_anim_translationy);
        this.handler = new android.os.Handler(android.os.Looper.getMainLooper());
    }
    
    static java.util.List access$000(com.navdy.hud.app.ui.component.CarouselTextView a) {
        return a.textList;
    }
    
    static int access$100(com.navdy.hud.app.ui.component.CarouselTextView a) {
        return a.currentTextIndex;
    }
    
    static int access$102(com.navdy.hud.app.ui.component.CarouselTextView a, int i) {
        a.currentTextIndex = i;
        return i;
    }
    
    static void access$200(com.navdy.hud.app.ui.component.CarouselTextView a, String s) {
        a.rotateWithAnimation(s);
    }
    
    static android.os.Handler access$300(com.navdy.hud.app.ui.component.CarouselTextView a) {
        return a.handler;
    }
    
    private void rotateWithAnimation(String s) {
        this.animate().translationY(this.translationY).alpha(0.0f).setDuration(500L).withEndAction((Runnable)new com.navdy.hud.app.ui.component.CarouselTextView$2(this, s));
    }
    
    public void clear() {
        this.animate().cancel();
        this.handler.removeCallbacks(this.carouselChangeRunnable);
        this.textList = null;
        this.currentTextIndex = -1;
    }
    
    public void setCarousel(int[] a) {
        this.handler.removeCallbacks(this.carouselChangeRunnable);
        this.textList = (java.util.List)new java.util.ArrayList();
        int i = a.length;
        int i0 = 0;
        while(i0 < i) {
            boolean b = a[i0] != 0;
            this.textList.add(this.getResources().getString(b ? 1 : 0));
            i0 = i0 + 1;
        }
        this.currentTextIndex = -1;
        this.handler.post(this.carouselChangeRunnable);
    }
}
