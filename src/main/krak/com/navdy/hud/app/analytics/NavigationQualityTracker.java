package com.navdy.hud.app.analytics;

public class NavigationQualityTracker {
    final private static int INVALID = -1;
    final private static com.navdy.hud.app.analytics.NavigationQualityTracker instance;
    final private static com.navdy.service.library.log.Logger logger;
    private boolean isRoutingWithTraffic;
    private com.navdy.hud.app.analytics.NavigationQualityTracker$Report report;
    private long tripDurationIntervalInit;
    private long tripStartUtc;
    
    static {
        logger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.analytics.NavigationQualityTracker.class);
        instance = new com.navdy.hud.app.analytics.NavigationQualityTracker();
    }
    
    private NavigationQualityTracker() {
        this.tripStartUtc = -1L;
        this.tripDurationIntervalInit = -1L;
    }
    
    public static com.navdy.hud.app.analytics.NavigationQualityTracker getInstance() {
        return instance;
    }
    
    public void cancelTrip() {
        synchronized(this) {
            this.tripStartUtc = -1L;
            this.tripDurationIntervalInit = -1L;
            this.isRoutingWithTraffic = false;
            this.report = null;
        }
        /*monexit(this)*/;
    }
    
    public int getActualDurationSoFar() {
        return (this.tripDurationIntervalInit == -1L) ? -1 : (int)((android.os.SystemClock.elapsedRealtime() - this.tripDurationIntervalInit) / 1000L);
    }
    
    public int getExpectedDistance() {
        return (this.report != null) ? (int)this.report.expectedDistance : -1;
    }
    
    public int getExpectedDuration() {
        return (this.report != null) ? (int)this.report.expectedDuration : -1;
    }
    
    public long getTripStartUtc() {
        return this.tripStartUtc;
    }
    
    public void trackCalculationWithTraffic(boolean b) {
        synchronized(this) {
            this.isRoutingWithTraffic = b;
        }
        /*monexit(this)*/;
    }
    
    public void trackTripEnded(long j) {
        synchronized(this) {
            if (this.report != null) {
                long j0 = System.currentTimeMillis();
                logger.v(new StringBuilder().append("tripEndUtc: ").append(j0).toString());
                logger.v(new StringBuilder().append("tripEndUtc - tripStartUtc, should be same as actualDuration: ").append(j0 - this.tripStartUtc).toString());
                this.report.actualDuration = (android.os.SystemClock.elapsedRealtime() - this.tripDurationIntervalInit) / 1000L;
                this.report.actualDistance = j;
                logger.i(new StringBuilder().append("route has ended, submitting navigation quality report: ").append(this.report).toString());
                if (this.report.isValid()) {
                    com.navdy.hud.app.analytics.AnalyticsSupport.submitNavigationQualityReport(this.report);
                }
                this.report = null;
            } else {
                logger.w("report is null, no-op");
            }
        }
        /*monexit(this)*/;
    }
    
    public void trackTripRecalculated() {
        synchronized(this) {
            if (this.report != null) {
                com.navdy.hud.app.analytics.NavigationQualityTracker$Report a = this.report;
                a.nRecalculations = a.nRecalculations + 1;
            } else {
                logger.w("report is null, no-op");
            }
        }
        /*monexit(this)*/;
    }
    
    public void trackTripStarted(long j, long j0, long j1) {
        synchronized(this) {
        com.navdy.service.library.log.Logger a = logger;
            a.i("trip started, init new navigation quality report");
            this.tripStartUtc = System.currentTimeMillis();
            this.tripDurationIntervalInit = android.os.SystemClock.elapsedRealtime();
            logger.v(new StringBuilder().append("tripStartUtc: ").append(this.tripStartUtc).toString());
            this.report = new com.navdy.hud.app.analytics.NavigationQualityTracker$Report(j, j0, j1, this.isRoutingWithTraffic, (com.navdy.hud.app.analytics.NavigationQualityTracker$1)null);
        }
        /*monexit(this)*/;
    }
}
