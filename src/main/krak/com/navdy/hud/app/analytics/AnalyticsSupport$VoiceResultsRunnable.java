package com.navdy.hud.app.analytics;

class AnalyticsSupport$VoiceResultsRunnable implements Runnable {
    com.navdy.hud.app.analytics.AnalyticsSupport$VoiceSearchAdditionalResultsAction additionalResultsAction;
    private boolean done;
    boolean explicitRetry;
    Integer listItemSelected;
    String microphoneUsed;
    String prefix;
    private com.navdy.service.library.events.navigation.NavigationRouteRequest request;
    private int retryCount;
    private Integer searchConfidence;
    java.util.List searchResults;
    private String searchWords;
    
    AnalyticsSupport$VoiceResultsRunnable(com.navdy.service.library.events.navigation.NavigationRouteRequest a) {
        this.listItemSelected = null;
        this.prefix = null;
        this.done = false;
        this.request = a;
        this.searchWords = com.navdy.hud.app.analytics.AnalyticsSupport.access$3500();
        this.searchConfidence = com.navdy.hud.app.analytics.AnalyticsSupport.access$3600();
        this.retryCount = com.navdy.hud.app.analytics.AnalyticsSupport.access$3700() - 1;
        this.microphoneUsed = com.navdy.hud.app.analytics.AnalyticsSupport.access$3800() ? "bluetooth" : "phone";
        this.searchResults = com.navdy.hud.app.analytics.AnalyticsSupport.access$3900();
        this.additionalResultsAction = com.navdy.hud.app.analytics.AnalyticsSupport.access$4000();
        this.explicitRetry = com.navdy.hud.app.analytics.AnalyticsSupport.access$4100();
        this.listItemSelected = com.navdy.hud.app.analytics.AnalyticsSupport.access$4200();
        this.prefix = com.navdy.hud.app.analytics.AnalyticsSupport.access$4300();
    }
    
    public void cancel(com.navdy.hud.app.analytics.AnalyticsSupport$VoiceSearchCancelReason a) {
        synchronized(this) {
            if (!this.done) {
                com.navdy.hud.app.analytics.AnalyticsSupport.access$200().removeCallbacks((Runnable)this);
                com.navdy.hud.app.analytics.AnalyticsSupport.access$4502((com.navdy.hud.app.analytics.AnalyticsSupport$VoiceResultsRunnable)null);
                this.done = true;
                com.navdy.hud.app.analytics.AnalyticsSupport.access$000().i(new StringBuilder().append("cancelling voice search reason: ").append(a).append(" data:").append(this).toString());
                com.navdy.hud.app.analytics.AnalyticsSupport.access$4400(a, this.request, this.searchWords, this.searchConfidence, this.retryCount, this.microphoneUsed, this.searchResults, this.additionalResultsAction, this.explicitRetry, this.listItemSelected, this.prefix);
            }
        }
        /*monexit(this)*/;
    }
    
    public void run() {
        synchronized(this) {
            if (!this.done) {
                com.navdy.hud.app.analytics.AnalyticsSupport.access$000().i(new StringBuilder().append("sending voice search event: ").append(this).toString());
                com.navdy.hud.app.analytics.AnalyticsSupport.access$4400((com.navdy.hud.app.analytics.AnalyticsSupport$VoiceSearchCancelReason)null, this.request, this.searchWords, this.searchConfidence, this.retryCount, this.microphoneUsed, this.searchResults, this.additionalResultsAction, this.explicitRetry, this.listItemSelected, this.prefix);
                com.navdy.hud.app.analytics.AnalyticsSupport.access$4502((com.navdy.hud.app.analytics.AnalyticsSupport$VoiceResultsRunnable)null);
                this.done = true;
            }
        }
        /*monexit(this)*/;
    }
    
    public String toString() {
        return new StringBuilder().append("VoiceResultsRunnable{done=").append(this.done).append(", request=").append(this.request).append(", searchWords='").append(this.searchWords).append((char)39).append(", searchConfidence=").append(this.searchConfidence).append(", retryCount=").append(this.retryCount).append(", microphoneUsed='").append(this.microphoneUsed).append((char)39).append(", searchResults=").append(this.searchResults).append(", additionalResultsAction=").append(this.additionalResultsAction).append(", explicitRetry=").append(this.explicitRetry).append(", listItemSelected=").append(this.listItemSelected).append(", prefix='").append(this.prefix).append((char)39).append((char)125).toString();
    }
}
