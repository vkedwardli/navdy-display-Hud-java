package com.navdy.hud.app.analytics;

class NavigationQualityTracker$Report {
    final private static double HEAVY_TRAFFIC_RATIO = 1.25;
    final private static double MODERATE_TRAFFIC_RATIO = 1.1;
    long actualDistance;
    long actualDuration;
    final long expectedDistance;
    final long expectedDuration;
    int nRecalculations;
    final com.navdy.hud.app.analytics.NavigationQualityTracker$TrafficLevel trafficLevel;
    
    private NavigationQualityTracker$Report(long j, long j0, long j1, boolean b) {
        this.expectedDuration = j;
        this.expectedDistance = j1;
        this.nRecalculations = 0;
        label2: if (b) {
            double d = (double)j / (double)j0;
            if (d < 1.1) {
                this.trafficLevel = com.navdy.hud.app.analytics.NavigationQualityTracker$TrafficLevel.LIGHT;
            } else {
                int i = (d > 1.1) ? 1 : (d == 1.1) ? 0 : -1;
                label0: {
                    label1: {
                        if (i < 0) {
                            break label1;
                        }
                        if (d < 1.25) {
                            break label0;
                        }
                    }
                    this.trafficLevel = com.navdy.hud.app.analytics.NavigationQualityTracker$TrafficLevel.HEAVY;
                    break label2;
                }
                this.trafficLevel = com.navdy.hud.app.analytics.NavigationQualityTracker$TrafficLevel.MODERATE;
            }
        } else {
            this.trafficLevel = com.navdy.hud.app.analytics.NavigationQualityTracker$TrafficLevel.NO_DATA;
        }
    }
    
    NavigationQualityTracker$Report(long j, long j0, long j1, boolean b, com.navdy.hud.app.analytics.NavigationQualityTracker$1 a) {
        this(j, j0, j1, b);
    }
    
    double getDistanceVariance() {
        return (double)(100L * (this.actualDistance - this.expectedDistance)) / (double)this.expectedDuration;
    }
    
    double getDurationVariance() {
        return (double)(100L * (this.actualDuration - this.expectedDuration)) / (double)this.expectedDuration;
    }
    
    public boolean isValid() {
        boolean b = false;
        long j = this.expectedDistance;
        int i = (j < 0L) ? -1 : (j == 0L) ? 0 : 1;
        label2: {
            label0: {
                label1: {
                    if (i <= 0) {
                        break label1;
                    }
                    if (this.expectedDuration <= 0L) {
                        break label1;
                    }
                    if (this.actualDistance <= 0L) {
                        break label1;
                    }
                    if (this.actualDuration > 0L) {
                        break label0;
                    }
                }
                b = false;
                break label2;
            }
            b = true;
        }
        return b;
    }
    
    public String toString() {
        return new StringBuilder().append("Report{trafficLevel=").append(this.trafficLevel).append(", expectedDuration=").append(this.expectedDuration).append(", expectedDistance=").append(this.expectedDistance).append(", actualDuration=").append(this.actualDuration).append(", actualDistance=").append(this.actualDistance).append(", nRecalculations=").append(this.nRecalculations).append((char)125).toString();
    }
}
