package com.navdy.hud.app.analytics;

final public class TelemetrySession {
    final private static float EXCESSIVE_SPEEDING_SPEED_OVER_SPEED_LIMIT_MS = 0.0f;
    final private static float G_LOWER_THRESHOLD_THRESHOLD = 0.3f;
    final private static double HARD_ACCELERATION_THRESHOLD_METERS_PER_SECOND = 3.53;
    final private static double HARD_BRAKING_THRESHOLD_METERS_PER_SECOND = 3.919;
    final private static long HIGH_G_MINIMUM_INTERVAL_BETWEEN_EVENTS_MILLIS = 3000L;
    final private static long HIGH_G_THRESHOLD_TIME_MILLIS = 500L;
    final public static com.navdy.hud.app.analytics.TelemetrySession INSTANCE;
    final private static float MAX_G_THRESHOLD = 0.45f;
    final private static int MINIMUM_TIME_BETWEEN_HARD_ACCELERATION_DECELERATION = 2000;
    final private static float SPEEDING_SPEED_OVER_SPEED_LIMIT_MS = 0.0f;
    final private static int SPEED_DATA_ROLLING_WINDOW_SIZE = 500;
    private static long _currentDistance;
    private static com.navdy.hud.app.analytics.RawSpeed _currentSpeed;
    private static long _excessiveSpeedingDuration;
    private static long _rollingDuration;
    private static long _speedingDuration;
    private static long confirmedHighGManeuverEndTime;
    private static boolean confirmedHighGManueverNotified;
    private static double currentMpg;
    private static int currentRpm;
    private static com.navdy.hud.app.analytics.RawSpeed currentSpeed;
    private static float currentSpeedLimit;
    private static com.navdy.hud.app.analytics.TelemetrySession$DataSource dataSource;
    private static double eventAverageMpg;
    private static int eventAverageRpm;
    private static long eventMpgSamples;
    private static int eventRpmSamples;
    private static long excessiveSpeedingDuration;
    private static long excessiveSpeedingStartTime;
    private static long highGManeuverStartTime;
    private static boolean isAboveWaterMark;
    private static float lastG;
    private static float lastGAngle;
    private static long lastGTime;
    private static long lastHardAccelerationTime;
    private static long lastHardBrakingTime;
    final private static com.navdy.service.library.log.Logger logger;
    private static float maxG;
    private static float maxGAngle;
    private static int maxRpm;
    private static long movingStartTime;
    final private static java.util.LinkedList rollingSpeedData;
    private static double sessionAverageMpg;
    private static float sessionAverageRollingSpeed;
    private static float sessionAverageSpeed;
    private static long sessionDistance;
    private static long sessionDuration;
    private static int sessionHardAccelerationCount;
    private static int sessionHardBrakingCount;
    private static int sessionHighGCount;
    private static float sessionMaxSpeed;
    private static long sessionMpgSamples;
    private static long sessionRollingDuration;
    private static long sessionStartTime;
    private static boolean sessionStarted;
    private static int sessionTroubleCodeCount;
    private static String sessionTroubleCodesReport;
    private static long speedingDuration;
    private static long speedingStartTime;
    private static long startDistance;
    
    static {
        com.navdy.hud.app.analytics.TelemetrySession a = new com.navdy.hud.app.analytics.TelemetrySession();
    }
    
    private TelemetrySession() {
        INSTANCE = this;
        logger = new com.navdy.service.library.log.Logger((INSTANCE).getClass().getSimpleName());
        SPEEDING_SPEED_OVER_SPEED_LIMIT_MS = com.navdy.hud.app.analytics.TelemetrySessionKt.MPHToMetersPerSecond(8f);
        EXCESSIVE_SPEEDING_SPEED_OVER_SPEED_LIMIT_MS = com.navdy.hud.app.analytics.TelemetrySessionKt.MPHToMetersPerSecond(16f);
        MAX_G_THRESHOLD = 0.45f;
        G_LOWER_THRESHOLD_THRESHOLD = 0.3f;
        HIGH_G_THRESHOLD_TIME_MILLIS = 500L;
        HIGH_G_MINIMUM_INTERVAL_BETWEEN_EVENTS_MILLIS = 3000L;
        sessionMpgSamples = 1L;
        sessionTroubleCodesReport = "";
        _currentSpeed = new com.navdy.hud.app.analytics.RawSpeed((float)(-1), 0L);
        SPEED_DATA_ROLLING_WINDOW_SIZE = 500;
        HARD_ACCELERATION_THRESHOLD_METERS_PER_SECOND = 3.53;
        HARD_BRAKING_THRESHOLD_METERS_PER_SECOND = 3.919;
        MINIMUM_TIME_BETWEEN_HARD_ACCELERATION_DECELERATION = 2000;
        currentSpeed = new com.navdy.hud.app.analytics.RawSpeed((float)(-1), 0L);
        rollingSpeedData = new java.util.LinkedList();
        eventMpgSamples = 1L;
        eventRpmSamples = 1;
    }
    
    final private long get_currentDistance() {
        com.navdy.hud.app.analytics.TelemetrySession$DataSource a = dataSource;
        return (a == null) ? 0L : a.totalDistanceTravelledWithMeters();
    }
    
    final private com.navdy.hud.app.analytics.RawSpeed get_currentSpeed() {
        com.navdy.hud.app.analytics.RawSpeed a = null;
        com.navdy.hud.app.analytics.TelemetrySession$DataSource a0 = dataSource;
        label0: {
            label1: {
                if (a0 == null) {
                    break label1;
                }
                a = a0.currentSpeed();
                if (a != null) {
                    break label0;
                }
            }
            a = new com.navdy.hud.app.analytics.RawSpeed((float)(-1), 0L);
        }
        return a;
    }
    
    final private void setSessionAverageRollingSpeed(float f) {
        sessionAverageRollingSpeed = f;
    }
    
    final private void setSessionAverageSpeed(float f) {
        sessionAverageSpeed = f;
    }
    
    final private void setSessionDuration(long j) {
        sessionDuration = j;
    }
    
    final private void setSessionExcessiveSpeedingPercentage(float f) {
    }
    
    final private void setSessionSpeedingPercentage(float f) {
    }
    
    final private void updateValuesBasedOnSpeed() {
        if (sessionStarted) {
            com.navdy.hud.app.analytics.RawSpeed a = currentSpeed;
            long j = this.getCurrentTime();
            if (a.getSpeed() > (float)0) {
                if (movingStartTime == 0L) {
                    movingStartTime = j;
                }
            } else if (movingStartTime != 0L) {
                _rollingDuration = _rollingDuration + (j - movingStartTime);
                movingStartTime = 0L;
            }
            sessionMaxSpeed = Math.max(a.getSpeed(), sessionMaxSpeed);
            if (currentSpeedLimit == 0.0f) {
                if (speedingStartTime != 0L) {
                    _speedingDuration = _speedingDuration + (j - speedingStartTime);
                    speedingStartTime = 0L;
                    com.navdy.hud.app.analytics.TelemetrySession$DataSource a0 = dataSource;
                    if (a0 != null) {
                        a0.interestingEventDetected(com.navdy.hud.app.analytics.TelemetrySession$InterestingEvent.SPEEDING_STOPPED);
                    }
                }
                if (excessiveSpeedingStartTime != 0L) {
                    _excessiveSpeedingDuration = _excessiveSpeedingDuration + (j - excessiveSpeedingStartTime);
                    excessiveSpeedingStartTime = 0L;
                    com.navdy.hud.app.analytics.TelemetrySession$DataSource a1 = dataSource;
                    if (a1 != null) {
                        a1.interestingEventDetected(com.navdy.hud.app.analytics.TelemetrySession$InterestingEvent.EXCESSIVE_SPEEDING_STOPPED);
                    }
                }
            } else {
                if (a.getSpeed() >= currentSpeedLimit + SPEEDING_SPEED_OVER_SPEED_LIMIT_MS) {
                    if (speedingStartTime == 0L) {
                        speedingStartTime = j;
                        com.navdy.hud.app.analytics.TelemetrySession$DataSource a2 = dataSource;
                        if (a2 != null) {
                            a2.interestingEventDetected(com.navdy.hud.app.analytics.TelemetrySession$InterestingEvent.SPEEDING_STARTED);
                        }
                    }
                } else if (speedingStartTime != 0L) {
                    _speedingDuration = _speedingDuration + (j - speedingStartTime);
                    speedingStartTime = 0L;
                    com.navdy.hud.app.analytics.TelemetrySession$DataSource a3 = dataSource;
                    if (a3 != null) {
                        a3.interestingEventDetected(com.navdy.hud.app.analytics.TelemetrySession$InterestingEvent.SPEEDING_STOPPED);
                    }
                }
                if (a.getSpeed() >= currentSpeedLimit + EXCESSIVE_SPEEDING_SPEED_OVER_SPEED_LIMIT_MS) {
                    if (excessiveSpeedingStartTime == 0L) {
                        excessiveSpeedingStartTime = j;
                        com.navdy.hud.app.analytics.TelemetrySession$DataSource a4 = dataSource;
                        if (a4 != null) {
                            a4.interestingEventDetected(com.navdy.hud.app.analytics.TelemetrySession$InterestingEvent.EXCESSIVE_SPEEDING_STARTED);
                        }
                    }
                } else if (excessiveSpeedingStartTime != 0L) {
                    _excessiveSpeedingDuration = _excessiveSpeedingDuration + (j - excessiveSpeedingStartTime);
                    excessiveSpeedingStartTime = 0L;
                    com.navdy.hud.app.analytics.TelemetrySession$DataSource a5 = dataSource;
                    if (a5 != null) {
                        a5.interestingEventDetected(com.navdy.hud.app.analytics.TelemetrySession$InterestingEvent.EXCESSIVE_SPEEDING_STOPPED);
                    }
                }
            }
        }
    }
    
    final public void eventReported() {
        eventAverageMpg = 0.0;
        eventMpgSamples = 1L;
        eventAverageRpm = 0;
        eventRpmSamples = 1;
        maxG = 0.0f;
        maxGAngle = 0.0f;
    }
    
    final public long getConfirmedHighGManeuverEndTime() {
        return confirmedHighGManeuverEndTime;
    }
    
    final public boolean getConfirmedHighGManueverNotified() {
        return confirmedHighGManueverNotified;
    }
    
    final public double getCurrentMpg() {
        return currentMpg;
    }
    
    final public int getCurrentRpm() {
        return currentRpm;
    }
    
    final public com.navdy.hud.app.analytics.RawSpeed getCurrentSpeed() {
        return currentSpeed;
    }
    
    final public float getCurrentSpeedLimit() {
        return currentSpeedLimit;
    }
    
    final public long getCurrentTime() {
        com.navdy.hud.app.analytics.TelemetrySession$DataSource a = dataSource;
        return (a == null) ? System.currentTimeMillis() : a.absoluteCurrentTime();
    }
    
    final public com.navdy.hud.app.analytics.TelemetrySession$DataSource getDataSource() {
        return dataSource;
    }
    
    final public double getEventAverageMpg() {
        return eventAverageMpg;
    }
    
    final public int getEventAverageRpm() {
        return eventAverageRpm;
    }
    
    final public long getEventMpgSamples() {
        return eventMpgSamples;
    }
    
    final public int getEventRpmSamples() {
        return eventRpmSamples;
    }
    
    final public long getExcessiveSpeedingDuration() {
        long j = _excessiveSpeedingDuration;
        return ((excessiveSpeedingStartTime == 0L) ? 0L : this.getCurrentTime() - excessiveSpeedingStartTime) + j;
    }
    
    final public long getExcessiveSpeedingStartTime() {
        return excessiveSpeedingStartTime;
    }
    
    final public double getHARD_ACCELERATION_THRESHOLD_METERS_PER_SECOND() {
        return HARD_ACCELERATION_THRESHOLD_METERS_PER_SECOND;
    }
    
    final public double getHARD_BRAKING_THRESHOLD_METERS_PER_SECOND() {
        return HARD_BRAKING_THRESHOLD_METERS_PER_SECOND;
    }
    
    final public long getHighGManeuverStartTime() {
        return highGManeuverStartTime;
    }
    
    final public float getLastG() {
        return lastG;
    }
    
    final public float getLastGAngle() {
        return lastGAngle;
    }
    
    final public long getLastGTime() {
        return lastGTime;
    }
    
    final public long getLastHardAccelerationTime() {
        return lastHardAccelerationTime;
    }
    
    final public long getLastHardBrakingTime() {
        return lastHardBrakingTime;
    }
    
    final public int getMINIMUM_TIME_BETWEEN_HARD_ACCELERATION_DECELERATION() {
        return MINIMUM_TIME_BETWEEN_HARD_ACCELERATION_DECELERATION;
    }
    
    final public float getMaxG() {
        return maxG;
    }
    
    final public float getMaxGAngle() {
        return maxGAngle;
    }
    
    final public int getMaxRpm() {
        return maxRpm;
    }
    
    final public java.util.LinkedList getRollingSpeedData() {
        return rollingSpeedData;
    }
    
    final public int getSPEED_DATA_ROLLING_WINDOW_SIZE() {
        return SPEED_DATA_ROLLING_WINDOW_SIZE;
    }
    
    final public double getSessionAverageMpg() {
        return sessionAverageMpg;
    }
    
    final public float getSessionAverageRollingSpeed() {
        return (this.getSessionRollingDuration() <= 0L) ? 0.0f : (float)this.getSessionDistance() / ((float)this.getSessionRollingDuration() / (float)1000);
    }
    
    final public float getSessionAverageSpeed() {
        return (this.getSessionDuration() <= 0L) ? 0.0f : (float)this.getSessionDistance() / ((float)this.getSessionDuration() / (float)1000);
    }
    
    final public long getSessionDistance() {
        return this.get_currentDistance() - startDistance;
    }
    
    final public long getSessionDuration() {
        return this.getCurrentTime() - sessionStartTime;
    }
    
    final public float getSessionExcessiveSpeedingPercentage() {
        return (this.getSessionRollingDuration() != 0L) ? (float)this.getExcessiveSpeedingDuration() / (float)this.getSessionRollingDuration() : 0.0f;
    }
    
    final public int getSessionHardAccelerationCount() {
        return sessionHardAccelerationCount;
    }
    
    final public int getSessionHardBrakingCount() {
        return sessionHardBrakingCount;
    }
    
    final public int getSessionHighGCount() {
        return sessionHighGCount;
    }
    
    final public float getSessionMaxSpeed() {
        return sessionMaxSpeed;
    }
    
    final public long getSessionMpgSamples() {
        return sessionMpgSamples;
    }
    
    final public long getSessionRollingDuration() {
        long j = _rollingDuration;
        return ((movingStartTime == 0L) ? 0L : this.getCurrentTime() - movingStartTime) + j;
    }
    
    final public float getSessionSpeedingPercentage() {
        return (this.getSessionRollingDuration() != 0L) ? (float)this.getSpeedingDuration() / (float)this.getSessionRollingDuration() : 0.0f;
    }
    
    final public boolean getSessionStarted() {
        return sessionStarted;
    }
    
    final public int getSessionTroubleCodeCount() {
        return sessionTroubleCodeCount;
    }
    
    final public String getSessionTroubleCodesReport() {
        return sessionTroubleCodesReport;
    }
    
    final public long getSpeedingDuration() {
        long j = _speedingDuration;
        return ((speedingStartTime == 0L) ? 0L : this.getCurrentTime() - speedingStartTime) + j;
    }
    
    final public boolean isAboveWaterMark() {
        return isAboveWaterMark;
    }
    
    final public boolean isDoingHighGManeuver() {
        return confirmedHighGManueverNotified;
    }
    
    final public boolean isExcessiveSpeeding() {
        return excessiveSpeedingStartTime > 0L;
    }
    
    final public boolean isSpeeding() {
        return speedingStartTime > 0L;
    }
    
    final public void setAboveWaterMark(boolean b) {
        isAboveWaterMark = b;
    }
    
    final public void setConfirmedHighGManeuverEndTime(long j) {
        confirmedHighGManeuverEndTime = j;
    }
    
    final public void setConfirmedHighGManueverNotified(boolean b) {
        confirmedHighGManueverNotified = b;
    }
    
    final public void setCurrentMpg(double d) {
        currentMpg = d;
        if (d > (double)0) {
            sessionAverageMpg = sessionAverageMpg + (d - sessionAverageMpg) / (double)sessionMpgSamples;
            sessionMpgSamples = sessionMpgSamples + 1L;
            eventAverageMpg = eventAverageMpg + (d - eventAverageMpg) / (double)eventMpgSamples;
            eventMpgSamples = eventMpgSamples + 1L;
        }
    }
    
    final public void setCurrentRpm(int i) {
        currentRpm = i;
        if (i > 0) {
            maxRpm = Math.max(i, maxRpm);
            eventAverageRpm = eventAverageRpm + (i - eventAverageRpm) / eventRpmSamples;
            eventRpmSamples = eventRpmSamples + 1;
        }
    }
    
    final public void setCurrentSpeed(com.navdy.hud.app.analytics.RawSpeed a) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a, "value");
        currentSpeed = a;
        this.updateValuesBasedOnSpeed();
        label0: if ((int)a.getSpeed() != -1) {
            com.navdy.hud.app.analytics.TelemetrySession$DataSource a0 = dataSource;
            if (a0 != null && a0.isHighAccuracySpeedAvailable()) {
                long j = a.getTimeStamp();
                rollingSpeedData.add(new kotlin.Pair(Long.valueOf(j), Float.valueOf(a.getSpeed())));
                if (rollingSpeedData.size() != 1) {
                    kotlin.Pair a1 = null;
                    boolean b = true;
                    while(true) {
                        kotlin.Pair a2 = (kotlin.Pair)rollingSpeedData.peek();
                        label4: {
                            label3: {
                                if (a2 == null) {
                                    break label3;
                                }
                                if (j - ((Number)a2.getFirst()).longValue() <= (long)SPEED_DATA_ROLLING_WINDOW_SIZE) {
                                    break label3;
                                }
                                a1 = (kotlin.Pair)rollingSpeedData.remove();
                                break label4;
                            }
                            if (a1 == null) {
                                b = false;
                            } else {
                                rollingSpeedData.addFirst(a1);
                                b = false;
                            }
                        }
                        if (!b) {
                            break;
                        }
                    }
                    kotlin.Pair a3 = (kotlin.Pair)rollingSpeedData.peek();
                    double d = (double)(j - ((Number)a3.getFirst()).longValue()) / (double)1000;
                    if (d >= 0.5) {
                        if (a.getSpeed() > ((Number)a3.getSecond()).floatValue()) {
                            double d0 = (double)(a.getSpeed() - ((Number)a3.getSecond()).floatValue()) / d;
                            com.navdy.hud.app.analytics.TelemetrySession$DataSource a4 = dataSource;
                            if (a4 != null && a4.isVerboseLoggingNeeded()) {
                                logger.d(new StringBuilder().append("RawSpeed ").append(a).append(", Change is speed : ").append(a.getSpeed() - ((Number)a3.getSecond()).floatValue()).append(" Acceleration : ").append(d0).append(" ").append("\n").append(" Rolling data : ").append(rollingSpeedData).toString());
                            }
                            if (d0 > HARD_ACCELERATION_THRESHOLD_METERS_PER_SECOND) {
                                long j0 = lastHardAccelerationTime;
                                int i = (j0 < 0L) ? -1 : (j0 == 0L) ? 0 : 1;
                                label2: {
                                    if (i == 0) {
                                        break label2;
                                    }
                                    if (j - lastHardAccelerationTime <= (long)MINIMUM_TIME_BETWEEN_HARD_ACCELERATION_DECELERATION) {
                                        break label0;
                                    }
                                }
                                logger.d(new StringBuilder().append("Hard Acceleration detected, Acceleration ").append(d0).append(", In time :").append(d).toString());
                                logger.d(new StringBuilder().append("Current RawSpeed : ").append(a).append(", Old RawSpeed : ").append(((Number)a3.getSecond()).floatValue()).toString());
                                logger.d(new StringBuilder().append("Rolling Window : ").append(rollingSpeedData).toString());
                                logger.d(new StringBuilder().append("Last G , g : ").append(lastG).append(" , gAngle : ").append(lastGAngle).append(" , time : ").append(lastGTime).toString());
                                lastHardAccelerationTime = j;
                                sessionHardAccelerationCount = sessionHardAccelerationCount + 1;
                                com.navdy.hud.app.analytics.TelemetrySession$DataSource a5 = dataSource;
                                if (a5 != null) {
                                    a5.interestingEventDetected(com.navdy.hud.app.analytics.TelemetrySession$InterestingEvent.HARD_ACCELERATION);
                                }
                            }
                        } else {
                            double d1 = (double)(((Number)a3.getSecond()).floatValue() - a.getSpeed()) / d;
                            com.navdy.hud.app.analytics.TelemetrySession$DataSource a6 = dataSource;
                            if (a6 != null && a6.isVerboseLoggingNeeded()) {
                                logger.d(new StringBuilder().append("RawSpeed ").append(a).append(", Change is speed : ").append(a.getSpeed() - ((Number)a3.getSecond()).floatValue()).append(" Braking : ").append(d1).append(" ").append("\n").append(" Rolling data : ").append(rollingSpeedData).toString());
                            }
                            if (d1 > HARD_BRAKING_THRESHOLD_METERS_PER_SECOND) {
                                long j1 = lastHardBrakingTime;
                                int i0 = (j1 < 0L) ? -1 : (j1 == 0L) ? 0 : 1;
                                label1: {
                                    if (i0 == 0) {
                                        break label1;
                                    }
                                    if (j - lastHardBrakingTime <= (long)MINIMUM_TIME_BETWEEN_HARD_ACCELERATION_DECELERATION) {
                                        break label0;
                                    }
                                }
                                logger.d(new StringBuilder().append("Hard Deceleration detected, Acceleration ").append(d1).append(", In time :").append(d).toString());
                                logger.d(new StringBuilder().append("Current RawSpeed : ").append(a).append(", Old RawSpeed : ").append(((Number)a3.getSecond()).floatValue()).toString());
                                logger.d(new StringBuilder().append("Rolling Window : ").append(rollingSpeedData).toString());
                                logger.d(new StringBuilder().append("Last G , g : ").append(lastG).append(" , gAngle : ").append(lastGAngle).append(" , time : ").append(lastGTime).toString());
                                lastHardBrakingTime = j;
                                sessionHardBrakingCount = sessionHardBrakingCount + 1;
                                com.navdy.hud.app.analytics.TelemetrySession$DataSource a7 = dataSource;
                                if (a7 != null) {
                                    a7.interestingEventDetected(com.navdy.hud.app.analytics.TelemetrySession$InterestingEvent.HARD_BRAKING);
                                }
                            }
                        }
                    }
                }
            }
        } else {
            rollingSpeedData.clear();
        }
    }
    
    final public void setCurrentSpeedLimit(float f) {
        if (!(f > (float)0)) {
            f = 0.0f;
        }
        currentSpeedLimit = f;
        this.updateValuesBasedOnSpeed();
    }
    
    final public void setDataSource(com.navdy.hud.app.analytics.TelemetrySession$DataSource a) {
        dataSource = a;
    }
    
    final public void setDoingHighGManeuver(boolean b) {
    }
    
    final public void setEventAverageMpg(double d) {
        eventAverageMpg = d;
    }
    
    final public void setEventAverageRpm(int i) {
        eventAverageRpm = i;
    }
    
    final public void setEventMpgSamples(long j) {
        eventMpgSamples = j;
    }
    
    final public void setEventRpmSamples(int i) {
        eventRpmSamples = i;
    }
    
    final public void setExcessiveSpeeding(boolean b) {
    }
    
    final public void setExcessiveSpeedingDuration(long j) {
        excessiveSpeedingDuration = j;
    }
    
    final public void setExcessiveSpeedingStartTime(long j) {
        excessiveSpeedingStartTime = j;
    }
    
    final public void setGForce(float f, float f0, float f1) {
        float f2 = Math.max(Math.abs(f), Math.abs(f0));
        float f3 = (float)(Math.atan((double)(-f0 / f)) * 180.0 / 3.141592653589793);
        if (f < (float)0) {
            f3 = f3 + 180f;
        }
        float f4 = ((float)360 + f3) % (float)360;
        lastG = f2;
        lastGAngle = f4;
        long j = this.getCurrentTime();
        lastGTime = j;
        if (f2 > maxG) {
            maxG = f2;
            maxGAngle = f4;
        }
        if (f2 > MAX_G_THRESHOLD) {
            boolean b = false;
            boolean b0 = false;
            logger.d(new StringBuilder().append("G ").append(f2).append(" , Angle ").append(f4).toString());
            int i = (125f < f4) ? -1 : (125f == f4) ? 0 : 1;
            label11: {
                label9: {
                    label10: {
                        if (i > 0) {
                            break label10;
                        }
                        if (f4 <= 235f) {
                            break label9;
                        }
                    }
                    b = false;
                    break label11;
                }
                b = true;
            }
            label8: {
                label0: {
                    label4: {
                        boolean b1 = false;
                        boolean b2 = false;
                        if (b) {
                            break label4;
                        }
                        int i0 = (305f < f4) ? -1 : (305f == f4) ? 0 : 1;
                        label7: {
                            label5: {
                                label6: {
                                    if (i0 > 0) {
                                        break label6;
                                    }
                                    if (f4 <= 360f) {
                                        break label5;
                                    }
                                }
                                b1 = false;
                                break label7;
                            }
                            b1 = true;
                        }
                        if (b1) {
                            break label4;
                        }
                        int i1 = (0.0f < f4) ? -1 : (0.0f == f4) ? 0 : 1;
                        label3: {
                            label1: {
                                label2: {
                                    if (i1 > 0) {
                                        break label2;
                                    }
                                    if (f4 <= 55f) {
                                        break label1;
                                    }
                                }
                                b2 = false;
                                break label3;
                            }
                            b2 = true;
                        }
                        if (!b2) {
                            break label0;
                        }
                    }
                    b0 = true;
                    break label8;
                }
                b0 = false;
            }
            if (b0) {
                if (!isAboveWaterMark && com.navdy.hud.app.analytics.TelemetrySessionKt.timeSince(j, confirmedHighGManeuverEndTime) > HIGH_G_MINIMUM_INTERVAL_BETWEEN_EVENTS_MILLIS) {
                    logger.d("High G maneuver started ");
                    highGManeuverStartTime = j;
                }
                if (isAboveWaterMark && !confirmedHighGManueverNotified && highGManeuverStartTime != 0L && com.navdy.hud.app.analytics.TelemetrySessionKt.timeSince(j, highGManeuverStartTime) > HIGH_G_THRESHOLD_TIME_MILLIS) {
                    sessionHighGCount = sessionHighGCount + 1;
                    confirmedHighGManueverNotified = true;
                    com.navdy.hud.app.analytics.TelemetrySession$DataSource a = dataSource;
                    if (a != null) {
                        a.interestingEventDetected(com.navdy.hud.app.analytics.TelemetrySession$InterestingEvent.HIGH_G_STARTED);
                    }
                    logger.d("High G maneuver confirmed and notified");
                }
                isAboveWaterMark = true;
            }
        } else if (f2 < G_LOWER_THRESHOLD_THRESHOLD) {
            if (highGManeuverStartTime != 0L) {
                highGManeuverStartTime = 0L;
                logger.d("High G maneuver ended");
                if (confirmedHighGManueverNotified) {
                    confirmedHighGManeuverEndTime = j;
                    confirmedHighGManueverNotified = false;
                    com.navdy.hud.app.analytics.TelemetrySession$DataSource a0 = dataSource;
                    if (a0 != null) {
                        a0.interestingEventDetected(com.navdy.hud.app.analytics.TelemetrySession$InterestingEvent.HIGH_G_ENDED);
                    }
                }
            }
            isAboveWaterMark = false;
        }
    }
    
    final public void setHighGManeuverStartTime(long j) {
        highGManeuverStartTime = j;
    }
    
    final public void setLastG(float f) {
        lastG = f;
    }
    
    final public void setLastGAngle(float f) {
        lastGAngle = f;
    }
    
    final public void setLastGTime(long j) {
        lastGTime = j;
    }
    
    final public void setLastHardAccelerationTime(long j) {
        lastHardAccelerationTime = j;
    }
    
    final public void setLastHardBrakingTime(long j) {
        lastHardBrakingTime = j;
    }
    
    final public void setMaxG(float f) {
        maxG = f;
    }
    
    final public void setMaxGAngle(float f) {
        maxGAngle = f;
    }
    
    final public void setMaxRpm(int i) {
        maxRpm = i;
    }
    
    final public void setSessionAverageMpg(double d) {
        sessionAverageMpg = d;
    }
    
    final public void setSessionDistance(long j) {
        sessionDistance = j;
    }
    
    final public void setSessionHardAccelerationCount(int i) {
        sessionHardAccelerationCount = i;
    }
    
    final public void setSessionHardBrakingCount(int i) {
        sessionHardBrakingCount = i;
    }
    
    final public void setSessionHighGCount(int i) {
        sessionHighGCount = i;
    }
    
    final public void setSessionMaxSpeed(float f) {
        sessionMaxSpeed = f;
    }
    
    final public void setSessionMpgSamples(long j) {
        sessionMpgSamples = j;
    }
    
    final public void setSessionRollingDuration(long j) {
        sessionRollingDuration = j;
    }
    
    final public void setSessionStarted(boolean b) {
        sessionStarted = b;
    }
    
    final public void setSessionTroubleCodeCount(int i) {
        sessionTroubleCodeCount = i;
    }
    
    final public void setSessionTroubleCodesReport(String s) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(s, "<set-?>");
        sessionTroubleCodesReport = s;
    }
    
    final public void setSpeeding(boolean b) {
    }
    
    final public void setSpeedingDuration(long j) {
        speedingDuration = j;
    }
    
    final public void speedSourceChanged() {
        if (sessionStarted) {
            rollingSpeedData.clear();
        }
    }
    
    final public void startSession() {
        sessionStartTime = this.getCurrentTime();
        sessionDuration = 0L;
        startDistance = this.get_currentDistance();
        rollingSpeedData.clear();
        this.setCurrentSpeed(this.get_currentSpeed());
        sessionRollingDuration = 0L;
        _speedingDuration = 0L;
        _rollingDuration = 0L;
        _excessiveSpeedingDuration = 0L;
        excessiveSpeedingStartTime = 0L;
        sessionMaxSpeed = 0.0f;
        movingStartTime = 0L;
        speedingStartTime = 0L;
        sessionStarted = true;
        sessionHighGCount = 0;
    }
    
    final public void stopSession() {
        sessionStarted = false;
    }
}
