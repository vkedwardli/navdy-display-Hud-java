package com.navdy.hud.app.analytics;

final public class TelemetryDataManager$Companion {
    private TelemetryDataManager$Companion() {
    }
    
    public TelemetryDataManager$Companion(kotlin.jvm.internal.DefaultConstructorMarker a) {
        this();
    }
    
    final public static long access$getANALYTICS_REPORTING_INTERVAL$p(com.navdy.hud.app.analytics.TelemetryDataManager$Companion a) {
        return a.getANALYTICS_REPORTING_INTERVAL();
    }
    
    final public static long access$getDRIVE_SCORE_PUBLISH_INTERVAL$p(com.navdy.hud.app.analytics.TelemetryDataManager$Companion a) {
        return a.getDRIVE_SCORE_PUBLISH_INTERVAL();
    }
    
    final public static boolean access$getLOG_TELEMETRY_DATA$p(com.navdy.hud.app.analytics.TelemetryDataManager$Companion a) {
        return a.getLOG_TELEMETRY_DATA();
    }
    
    final public static String access$getPREFERENCE_TROUBLE_CODES$p(com.navdy.hud.app.analytics.TelemetryDataManager$Companion a) {
        return a.getPREFERENCE_TROUBLE_CODES();
    }
    
    final public static com.navdy.service.library.log.Logger access$getSLogger$p(com.navdy.hud.app.analytics.TelemetryDataManager$Companion a) {
        return a.getSLogger();
    }
    
    final private long getANALYTICS_REPORTING_INTERVAL() {
        return com.navdy.hud.app.analytics.TelemetryDataManager.access$getANALYTICS_REPORTING_INTERVAL$cp();
    }
    
    final private long getDRIVE_SCORE_PUBLISH_INTERVAL() {
        return com.navdy.hud.app.analytics.TelemetryDataManager.access$getDRIVE_SCORE_PUBLISH_INTERVAL$cp();
    }
    
    final private boolean getLOG_TELEMETRY_DATA() {
        return com.navdy.hud.app.analytics.TelemetryDataManager.access$getLOG_TELEMETRY_DATA$cp();
    }
    
    final private float getMAX_ACCEL() {
        return com.navdy.hud.app.analytics.TelemetryDataManager.access$getMAX_ACCEL$cp();
    }
    
    final private String getPREFERENCE_TROUBLE_CODES() {
        return com.navdy.hud.app.analytics.TelemetryDataManager.access$getPREFERENCE_TROUBLE_CODES$cp();
    }
    
    final private com.navdy.service.library.log.Logger getSLogger() {
        return com.navdy.hud.app.analytics.TelemetryDataManager.access$getSLogger$cp();
    }
    
    final public kotlin.Pair serializeTroubleCodes(java.util.List a, String s) {
        Object a0 = null;
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a, "troubleCodes");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(s, "savedTroubleCodesStringFromPreviousSession");
        java.util.HashSet a1 = new java.util.HashSet();
        if (android.text.TextUtils.isEmpty((CharSequence)s)) {
            a0 = a;
        } else {
            String[] a2 = new String[1];
            a2[0] = " ";
            java.util.Iterator a3 = kotlin.text.StringsKt.split$default((CharSequence)s, a2, false, 0, 6, null).iterator();
            a0 = a;
            Object a4 = a3;
            while(((java.util.Iterator)a4).hasNext()) {
                a1.add((String)((java.util.Iterator)a4).next());
            }
        }
        StringBuilder a5 = new StringBuilder();
        StringBuilder a6 = new StringBuilder();
        if (((java.util.List)a0).size() > 0) {
            com.navdy.hud.app.analytics.TelemetrySession.INSTANCE.setSessionTroubleCodeCount(((java.util.List)a0).size());
            Object a7 = ((java.util.List)a0).iterator();
            while(((java.util.Iterator)a7).hasNext()) {
                String s0 = (String)((java.util.Iterator)a7).next();
                if (!a1.contains(s0)) {
                    a6.append(new StringBuilder().append(s0).append(" ").toString());
                }
                a5.append(new StringBuilder().append(s0).append(" ").toString());
            }
        }
        String s1 = a5.toString();
        if (s1 == null) {
            throw new kotlin.TypeCastException("null cannot be cast to non-null type kotlin.CharSequence");
        }
        String s2 = (kotlin.text.StringsKt.trim((CharSequence)s1)).toString();
        String s3 = a6.toString();
        if (s3 == null) {
            throw new kotlin.TypeCastException("null cannot be cast to non-null type kotlin.CharSequence");
        }
        return new kotlin.Pair(s2, (kotlin.text.StringsKt.trim((CharSequence)s3)).toString());
    }
}
