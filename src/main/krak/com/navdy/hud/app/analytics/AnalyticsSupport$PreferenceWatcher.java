package com.navdy.hud.app.analytics;

class AnalyticsSupport$PreferenceWatcher {
    com.navdy.hud.app.analytics.AnalyticsSupport$PreferenceAdapter adapter;
    com.navdy.hud.app.analytics.Event oldState;
    Runnable queuedUpdate;
    
    AnalyticsSupport$PreferenceWatcher(com.navdy.hud.app.profile.DriverProfile a, com.navdy.hud.app.analytics.AnalyticsSupport$PreferenceAdapter a0) {
        this.oldState = a0.calculateState(a);
        this.adapter = a0;
    }
    
    void enqueue(com.navdy.hud.app.profile.DriverProfile a) {
        if (this.queuedUpdate != null) {
            com.navdy.hud.app.analytics.AnalyticsSupport.access$200().removeCallbacks(this.queuedUpdate);
        }
        this.queuedUpdate = (Runnable)new com.navdy.hud.app.analytics.AnalyticsSupport$PreferenceWatcher$1(this, a);
        com.navdy.hud.app.analytics.AnalyticsSupport.access$200().postDelayed(this.queuedUpdate, 15000L);
    }
}
