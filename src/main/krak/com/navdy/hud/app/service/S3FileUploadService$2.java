package com.navdy.hud.app.service;

class S3FileUploadService$2 {
    final static int[] $SwitchMap$com$amazonaws$mobileconnectors$s3$transferutility$TransferState;
    
    static {
        $SwitchMap$com$amazonaws$mobileconnectors$s3$transferutility$TransferState = new int[com.amazonaws.mobileconnectors.s3.transferutility.TransferState.values().length];
        int[] a = $SwitchMap$com$amazonaws$mobileconnectors$s3$transferutility$TransferState;
        com.amazonaws.mobileconnectors.s3.transferutility.TransferState a0 = com.amazonaws.mobileconnectors.s3.transferutility.TransferState.COMPLETED;
        try {
            a[a0.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException) {
        }
        try {
            $SwitchMap$com$amazonaws$mobileconnectors$s3$transferutility$TransferState[com.amazonaws.mobileconnectors.s3.transferutility.TransferState.CANCELED.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException0) {
        }
        try {
            $SwitchMap$com$amazonaws$mobileconnectors$s3$transferutility$TransferState[com.amazonaws.mobileconnectors.s3.transferutility.TransferState.WAITING_FOR_NETWORK.ordinal()] = 3;
        } catch(NoSuchFieldError ignoredException1) {
        }
        try {
            $SwitchMap$com$amazonaws$mobileconnectors$s3$transferutility$TransferState[com.amazonaws.mobileconnectors.s3.transferutility.TransferState.IN_PROGRESS.ordinal()] = 4;
        } catch(NoSuchFieldError ignoredException2) {
        }
        try {
            $SwitchMap$com$amazonaws$mobileconnectors$s3$transferutility$TransferState[com.amazonaws.mobileconnectors.s3.transferutility.TransferState.FAILED.ordinal()] = 5;
        } catch(NoSuchFieldError ignoredException3) {
        }
    }
}
