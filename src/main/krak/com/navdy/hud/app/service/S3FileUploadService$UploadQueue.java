package com.navdy.hud.app.service;

public class S3FileUploadService$UploadQueue {
    final private java.util.PriorityQueue internalQueue;
    
    public S3FileUploadService$UploadQueue() {
        this.internalQueue = new java.util.PriorityQueue(15, (java.util.Comparator)new com.navdy.hud.app.service.S3FileUploadService$RequestTimeComparator());
    }
    
    public void add(com.navdy.hud.app.service.S3FileUploadService$Request a) {
        this.internalQueue.add(a);
    }
    
    public com.navdy.hud.app.service.S3FileUploadService$Request peek() {
        return (com.navdy.hud.app.service.S3FileUploadService$Request)this.internalQueue.peek();
    }
    
    public com.navdy.hud.app.service.S3FileUploadService$Request pop() {
        return (com.navdy.hud.app.service.S3FileUploadService$Request)this.internalQueue.poll();
    }
    
    public int size() {
        return this.internalQueue.size();
    }
}
