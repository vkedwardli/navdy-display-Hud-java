package com.navdy.hud.app.service.pandora.exceptions;

public class StringOverflowException extends Exception {
    public StringOverflowException() {
        super("Maximum byte length for String value reached");
    }
}
