package com.navdy.hud.app.service;

public class HudConnectionService$ReconnectRunnable implements Runnable {
    private String reason;
    final com.navdy.hud.app.service.HudConnectionService this$0;
    
    public HudConnectionService$ReconnectRunnable(com.navdy.hud.app.service.HudConnectionService a) {
        super();
        this.this$0 = a;
    }
    
    public void run() {
        this.this$0.reconnect(this.reason);
    }
    
    public void setReason(String s) {
        this.reason = s;
    }
}
