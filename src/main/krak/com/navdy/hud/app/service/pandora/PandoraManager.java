package com.navdy.hud.app.service.pandora;
import com.navdy.hud.app.R;

public class PandoraManager implements java.io.Closeable {
    final private static int ACK_WAITING_TIME = 750;
    final private static java.util.concurrent.atomic.AtomicInteger CONNECT_THREAD_COUNTER;
    final private static com.navdy.service.library.events.audio.MusicTrackInfo$Builder INITIAL_TRACK_INFO_BUILDER;
    final private static int MAX_SEND_MESSAGE_RETRIES = 10;
    final private static java.util.UUID PANDORA_UUID;
    final private static java.util.concurrent.atomic.AtomicInteger READ_THREAD_COUNTER;
    final private static android.os.Handler handler;
    final static com.navdy.service.library.log.Logger sLogger;
    private Runnable activeAckMessageTimeoutCounter;
    private String adTitle;
    private com.squareup.otto.Bus bus;
    private java.io.ByteArrayOutputStream currentArtwork;
    private com.navdy.service.library.events.audio.MusicTrackInfo currentTrack;
    private byte expectedArtworkSegmentIndex;
    private java.util.concurrent.atomic.AtomicBoolean isConnectingInProgress;
    private boolean isCurrentSequence0;
    private java.util.concurrent.ConcurrentLinkedQueue msgQueue;
    private String remoteDeviceId;
    private android.bluetooth.BluetoothSocket socket;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.service.pandora.PandoraManager.class);
        PANDORA_UUID = java.util.UUID.fromString("453994D5-D58B-96F9-6616-B37F586BA2EC");
        CONNECT_THREAD_COUNTER = new java.util.concurrent.atomic.AtomicInteger(1);
        READ_THREAD_COUNTER = new java.util.concurrent.atomic.AtomicInteger(1);
        handler = new android.os.Handler();
        INITIAL_TRACK_INFO_BUILDER = new com.navdy.service.library.events.audio.MusicTrackInfo$Builder().playbackState(com.navdy.service.library.events.audio.MusicPlaybackState.PLAYBACK_NONE).dataSource(com.navdy.service.library.events.audio.MusicDataSource.MUSIC_SOURCE_PANDORA_API);
    }
    
    public PandoraManager(com.squareup.otto.Bus a, android.content.res.Resources a0) {
        this.remoteDeviceId = null;
        this.socket = null;
        this.isConnectingInProgress = new java.util.concurrent.atomic.AtomicBoolean(false);
        this.isCurrentSequence0 = true;
        this.activeAckMessageTimeoutCounter = null;
        this.msgQueue = new java.util.concurrent.ConcurrentLinkedQueue();
        this.currentTrack = INITIAL_TRACK_INFO_BUILDER.build();
        this.currentArtwork = null;
        this.expectedArtworkSegmentIndex = (byte)0;
        this.adTitle = "";
        this.bus = a;
        this.adTitle = a0.getString(R.string.music_pandora_ad_title);
    }
    
    static boolean access$000(com.navdy.hud.app.service.pandora.PandoraManager a) {
        return a.isConnected();
    }
    
    static String access$100(com.navdy.hud.app.service.pandora.PandoraManager a) {
        return a.remoteDeviceId;
    }
    
    static java.util.UUID access$200() {
        return PANDORA_UUID;
    }
    
    static void access$300(com.navdy.hud.app.service.pandora.PandoraManager a, android.bluetooth.BluetoothSocket a0) {
        a.setSocket(a0);
    }
    
    static java.util.concurrent.atomic.AtomicInteger access$400() {
        return READ_THREAD_COUNTER;
    }
    
    static java.util.concurrent.atomic.AtomicBoolean access$500(com.navdy.hud.app.service.pandora.PandoraManager a) {
        return a.isConnectingInProgress;
    }
    
    static void access$600(com.navdy.hud.app.service.pandora.PandoraManager a, byte[] a0, int i) {
        a.sendDataMessage(a0, i);
    }
    
    private boolean isConnected() {
        boolean b = false;
        android.bluetooth.BluetoothSocket a = this.getSocket();
        label2: {
            label0: {
                label1: {
                    if (a == null) {
                        break label1;
                    }
                    if (a.isConnected()) {
                        break label0;
                    }
                }
                b = false;
                break label2;
            }
            b = true;
        }
        return b;
    }
    
    private boolean isFlagInField(byte a, byte a0) {
        int i = a0 & a;
        return i != 0;
    }
    
    private void onMessageReceived(com.navdy.hud.app.service.pandora.messages.BaseIncomingMessage a) {
        if (a instanceof com.navdy.hud.app.service.pandora.messages.UpdateStatus) {
            this.onUpdateStatus((com.navdy.hud.app.service.pandora.messages.UpdateStatus)a);
        } else if (a instanceof com.navdy.hud.app.service.pandora.messages.UpdateTrack) {
            this.onUpdateTrack((com.navdy.hud.app.service.pandora.messages.UpdateTrack)a);
        } else if (a instanceof com.navdy.hud.app.service.pandora.messages.UpdateTrackCompleted) {
            this.onUpdateTrackCompleted((com.navdy.hud.app.service.pandora.messages.UpdateTrackCompleted)a);
        } else if (a instanceof com.navdy.hud.app.service.pandora.messages.ReturnTrackInfoExtended) {
            this.onReturnTrackInfoExtended((com.navdy.hud.app.service.pandora.messages.ReturnTrackInfoExtended)a);
        } else if (a instanceof com.navdy.hud.app.service.pandora.messages.UpdateTrackAlbumArt) {
            this.onUpdateTrackAlbumArt((com.navdy.hud.app.service.pandora.messages.UpdateTrackAlbumArt)a);
        } else if (a instanceof com.navdy.hud.app.service.pandora.messages.ReturnTrackAlbumArtSegment) {
            this.onReturnTrackAlbumArtSegment((com.navdy.hud.app.service.pandora.messages.ReturnTrackAlbumArtSegment)a);
        } else if (a instanceof com.navdy.hud.app.service.pandora.messages.UpdateTrackElapsed) {
            this.onUpdateTrackElapsed((com.navdy.hud.app.service.pandora.messages.UpdateTrackElapsed)a);
        } else {
            sLogger.w(new StringBuilder().append("Received unsupported message from Pandora - ignoring: ").append(a.toString()).toString());
        }
    }
    
    private void onReturnTrackAlbumArtSegment(com.navdy.hud.app.service.pandora.messages.ReturnTrackAlbumArtSegment a) {
        com.navdy.service.library.events.audio.MusicTrackInfo a0 = this.currentTrack;
        label0: {
            if (a0 == null) {
                break label0;
            }
            if (this.currentTrack.index == null) {
                break label0;
            }
            long j = (long)a.trackToken;
            long j0 = this.currentTrack.index.longValue();
            int i = (j < j0) ? -1 : (j == j0) ? 0 : 1;
            label4: {
                label5: {
                    if (i != 0) {
                        break label5;
                    }
                    int i0 = a.segmentIndex;
                    int i1 = this.expectedArtworkSegmentIndex;
                    if (i0 == i1) {
                        break label4;
                    }
                }
                sLogger.e("Wrong artwork's segment sent - ignoring it");
                break label0;
            }
            java.io.ByteArrayOutputStream a1 = this.currentArtwork;
            label3: {
                if (a1 != null) {
                    break label3;
                }
                int i2 = a.segmentIndex;
                label2: {
                    if (i2 != 0) {
                        break label2;
                    }
                    this.currentArtwork = new java.io.ByteArrayOutputStream();
                    break label3;
                }
                sLogger.e("Wrong initial artwork's segment sent - ignoring it");
                break label0;
            }
            label1: {
                try {
                    this.currentArtwork.write(a.data);
                    this.currentArtwork.flush();
                } catch(java.io.IOException ignoredException) {
                    break label1;
                }
                int i3 = a.segmentIndex;
                int i4 = (byte)(i3 + 1);
                int i5 = (byte)i4;
                this.expectedArtworkSegmentIndex = (byte)i5;
                int i6 = this.expectedArtworkSegmentIndex;
                int i7 = a.totalSegments;
                if (i6 != i7) {
                    break label0;
                }
                this.bus.post(new com.navdy.service.library.events.photo.PhotoUpdate$Builder().identifier(String.valueOf(a.trackToken)).photo(okio.ByteString.of(this.currentArtwork.toByteArray())).photoType(com.navdy.service.library.events.photo.PhotoType.PHOTO_ALBUM_ART).build());
                this.resetCurrentArtwork();
                break label0;
            }
            com.navdy.service.library.log.Logger a2 = sLogger;
            StringBuilder a3 = new StringBuilder().append("Exception while processing artwork chunk: ");
            int i8 = a.segmentIndex;
            a2.e(a3.append(i8).toString());
            this.resetCurrentArtwork();
        }
    }
    
    private void onReturnTrackInfoExtended(com.navdy.hud.app.service.pandora.messages.ReturnTrackInfoExtended a) {
        String s = a.title;
        boolean b = android.text.TextUtils.isEmpty((CharSequence)s);
        label1: {
            label0: {
                if (!b) {
                    break label0;
                }
                if (!android.text.TextUtils.isEmpty((CharSequence)a.album)) {
                    break label0;
                }
                if (!android.text.TextUtils.isEmpty((CharSequence)a.artist)) {
                    break label0;
                }
                int i = a.duration;
                if (i <= 0) {
                    break label0;
                }
                s = this.adTitle;
                this.resetCurrentArtwork();
                break label1;
            }
            if (a.albumArtLength > 0) {
                this.requestArtwork();
            }
        }
        com.navdy.service.library.events.audio.MusicTrackInfo$Builder a0 = new com.navdy.service.library.events.audio.MusicTrackInfo$Builder(this.currentTrack).index(Long.valueOf((long)a.trackToken)).name(s).album(a.album).author(a.artist);
        int i0 = a.duration;
        com.navdy.service.library.events.audio.MusicTrackInfo$Builder a1 = a0.duration(Integer.valueOf(i0 * 1000));
        int i1 = a.elapsed;
        com.navdy.service.library.events.audio.MusicTrackInfo$Builder a2 = a1.currentPosition(Integer.valueOf(this.secondsToMilliseconds((short)i1))).isPreviousAllowed(Boolean.valueOf(false));
        int i2 = a.permissionFlags;
        this.setCurrentTrack(a2.isNextAllowed(Boolean.valueOf(this.isFlagInField((byte)2, (byte)i2))).isOnlineStream(Boolean.valueOf(true)).build());
    }
    
    private void onUpdateStatus(com.navdy.hud.app.service.pandora.messages.UpdateStatus a) {
        com.navdy.service.library.events.audio.MusicTrackInfo$Builder a0 = new com.navdy.service.library.events.audio.MusicTrackInfo$Builder(this.currentTrack);
        switch(com.navdy.hud.app.service.pandora.PandoraManager$2.$SwitchMap$com$navdy$hud$app$service$pandora$messages$UpdateStatus$Value[a.value.ordinal()]) {
            case 2: {
                a0.playbackState(com.navdy.service.library.events.audio.MusicPlaybackState.PLAYBACK_PAUSED);
                break;
            }
            case 1: {
                a0.playbackState(com.navdy.service.library.events.audio.MusicPlaybackState.PLAYBACK_PLAYING);
                break;
            }
            default: {
                a0.playbackState(com.navdy.service.library.events.audio.MusicPlaybackState.PLAYBACK_NONE);
            }
        }
        this.setCurrentTrack(a0.build());
        if (a0.playbackState == com.navdy.service.library.events.audio.MusicPlaybackState.PLAYBACK_NONE) {
            sLogger.w(new StringBuilder().append("Receive bad status from Pandora: ").append(a.value).toString());
            this.terminateAndClose();
        }
    }
    
    private void onUpdateTrack(com.navdy.hud.app.service.pandora.messages.UpdateTrack a) {
        int i = a.value;
        label1: {
            Exception a0 = null;
            if (i == 0) {
                break label1;
            }
            label0: {
                try {
                    this.sendOrQueueMessage((com.navdy.hud.app.service.pandora.messages.BaseOutgoingMessage)com.navdy.hud.app.service.pandora.messages.GetTrackInfoExtended.INSTANCE);
                } catch(Exception a1) {
                    a0 = a1;
                    break label0;
                }
                break label1;
            }
            sLogger.e("Cannot send request message for track's info - disconnecting", (Throwable)a0);
            this.terminateAndClose();
        }
    }
    
    private void onUpdateTrackAlbumArt(com.navdy.hud.app.service.pandora.messages.UpdateTrackAlbumArt a) {
        if (this.currentTrack != null && this.currentTrack.index != null) {
            if (this.currentTrack.index.longValue() == (long)a.trackToken) {
                this.requestArtwork();
            } else {
                sLogger.w("Received update for artwork on the wrong song - ignoring");
            }
        }
    }
    
    private void onUpdateTrackCompleted(com.navdy.hud.app.service.pandora.messages.UpdateTrackCompleted a) {
        this.resetCurrentArtwork();
    }
    
    private void onUpdateTrackElapsed(com.navdy.hud.app.service.pandora.messages.UpdateTrackElapsed a) {
        if (this.currentTrack != null && this.currentTrack.index != null) {
            if ((long)a.trackToken == this.currentTrack.index.longValue()) {
                int i = a.elapsed;
                int i0 = this.secondsToMilliseconds((short)i);
                if (i0 != this.currentTrack.currentPosition.intValue()) {
                    this.setCurrentTrack(new com.navdy.service.library.events.audio.MusicTrackInfo$Builder(this.currentTrack).currentPosition(Integer.valueOf(i0)).build());
                }
            } else {
                sLogger.e("Elapsed time received for wrong song - ignoring");
            }
        }
    }
    
    private void requestArtwork() {
        this.resetCurrentArtwork();
        com.navdy.hud.app.service.pandora.messages.GetTrackAlbumArt a = com.navdy.hud.app.service.pandora.messages.GetTrackAlbumArt.INSTANCE;
        try {
            this.sendOrQueueMessage((com.navdy.hud.app.service.pandora.messages.BaseOutgoingMessage)a);
        } catch(Exception a0) {
            sLogger.e("Exception while requesting pre-loaded artwork - ignoring", (Throwable)a0);
        }
    }
    
    private void resetCurrentArtwork() {
        com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)this.currentArtwork);
        this.currentArtwork = null;
        this.expectedArtworkSegmentIndex = (byte)0;
    }
    
    private int secondsToMilliseconds(short a) {
        int i = a * 1000;
        return i;
    }
    
    private void sendAckMessage(boolean b) {
        byte[] a = b ? com.navdy.hud.app.service.pandora.AckFrameMessage.ACK_WITH_SEQUENCE_0.buildFrame() : com.navdy.hud.app.service.pandora.AckFrameMessage.ACK_WITH_SEQUENCE_1.buildFrame();
        try {
            this.sendByteArray(a);
            if (sLogger.isLoggable(2)) {
                sLogger.i(new StringBuilder().append("Ack message sent to Pandora - is sequence 0: ").append(b).toString());
            }
        } catch(java.io.IOException a0) {
            sLogger.e("Cannot send ACK message frame through the socket - disconnecting", (Throwable)a0);
            this.close();
        }
    }
    
    private void sendByteArray(byte[] a) {
        java.io.OutputStream a0 = this.getSocket().getOutputStream();
        a0.write(a);
        a0.flush();
    }
    
    private void sendDataMessage(byte[] a, int i) {
        label0: {
            java.io.IOException a0 = null;
            label1: if (i > 0) {
                try {
                    this.sendByteArray(a);
                } catch(java.io.IOException a1) {
                    a0 = a1;
                    break label1;
                }
                this.activeAckMessageTimeoutCounter = (Runnable)new com.navdy.hud.app.service.pandora.PandoraManager$1(this, a, i - 1);
                handler.postDelayed(this.activeAckMessageTimeoutCounter, 750L);
                break label0;
            } else {
                sLogger.e("Max number of retries for the message achieved - closing connection");
                this.close();
                break label0;
            }
            sLogger.e("Cannot send message frame through the socket - disconnecting", (Throwable)a0);
            this.close();
        }
    }
    
    private void sendNextMessage() {
        try {
            com.navdy.hud.app.service.pandora.messages.BaseOutgoingMessage a = (com.navdy.hud.app.service.pandora.messages.BaseOutgoingMessage)this.msgQueue.poll();
            if (a != null) {
                sLogger.i(new StringBuilder().append("Sending message to Pandora: ").append(a.toString()).toString());
                this.sendDataMessage(new com.navdy.hud.app.service.pandora.DataFrameMessage(this.isCurrentSequence0, a.buildPayload()).buildFrame(), 10);
            } else {
                sLogger.v("Queue empty: no new message waiting to be sent");
            }
        } catch(Exception a0) {
            sLogger.e("Cannot send data message - disconnecting", (Throwable)a0);
            this.close();
        }
    }
    
    private void setCurrentTrack(com.navdy.service.library.events.audio.MusicTrackInfo a) {
        if (!this.currentTrack.equals(a)) {
            this.currentTrack = a;
            this.bus.post(this.currentTrack);
        }
    }
    
    private void setSocket(android.bluetooth.BluetoothSocket a) {
        synchronized(this) {
            if (this.socket != a) {
                if (a == null) {
                    com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)this.socket);
                }
                this.socket = a;
            }
        }
        /*monexit(this)*/;
    }
    
    private void startConnecting(boolean b) {
        if (this.isConnectingInProgress.compareAndSet(false, true)) {
            Thread a = new Thread((Runnable)new com.navdy.hud.app.service.pandora.PandoraManager$ConnectRunnable(this, b));
            a.setName(new StringBuilder().append("Pandora-Connect-").append(CONNECT_THREAD_COUNTER.getAndIncrement()).toString());
            a.start();
        } else {
            sLogger.w("Connecting thread is already running - ignoring the request");
        }
    }
    
    public void close() {
        if (this.activeAckMessageTimeoutCounter != null) {
            handler.removeCallbacks(this.activeAckMessageTimeoutCounter);
            this.activeAckMessageTimeoutCounter = null;
        }
        this.resetCurrentArtwork();
        this.setCurrentTrack(INITIAL_TRACK_INFO_BUILDER.build());
        com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)this.socket);
        this.setSocket((android.bluetooth.BluetoothSocket)null);
        this.msgQueue.clear();
    }
    
    public android.bluetooth.BluetoothSocket getSocket() {
        android.bluetooth.BluetoothSocket a = null;
        synchronized(this) {
            a = this.socket;
        }
        /*monexit(this)*/;
        return a;
    }
    
    public void onDeviceConnectionStateChange(com.navdy.service.library.events.connection.ConnectionStateChange a) {
        switch(com.navdy.hud.app.service.pandora.PandoraManager$2.$SwitchMap$com$navdy$service$library$events$connection$ConnectionStateChange$ConnectionState[a.state.ordinal()]) {
            case 2: {
                this.remoteDeviceId = null;
                this.close();
                break;
            }
            case 1: {
                this.remoteDeviceId = a.remoteDeviceId;
                break;
            }
        }
    }
    
    protected void onFrameReceived(byte[] a) {
        label2: {
            com.navdy.hud.app.service.pandora.FrameMessage a0 = null;
            boolean b = false;
            label3: {
                IllegalArgumentException a1 = null;
                try {
                    a0 = com.navdy.hud.app.service.pandora.FrameMessage.parseFrame(a);
                    b = a0 instanceof com.navdy.hud.app.service.pandora.AckFrameMessage;
                    break label3;
                } catch(IllegalArgumentException a2) {
                    a1 = a2;
                }
                sLogger.e("Cannot parse received message frame", (Throwable)a1);
                break label2;
            }
            if (b) {
                if (sLogger.isLoggable(2)) {
                    sLogger.i(new StringBuilder().append("Ack message sent to Pandora - is sequence 0: ").append(a0.isSequence0).toString());
                }
                Runnable a3 = this.activeAckMessageTimeoutCounter;
                label1: {
                    if (a3 == null) {
                        break label1;
                    }
                    if (this.isCurrentSequence0 == a0.isSequence0) {
                        break label1;
                    }
                    handler.removeCallbacks(this.activeAckMessageTimeoutCounter);
                    this.isCurrentSequence0 = a0.isSequence0;
                    this.activeAckMessageTimeoutCounter = null;
                    this.sendNextMessage();
                    break label2;
                }
                sLogger.e("Invalid ACK message received - ignoring it");
            } else if (a0 instanceof com.navdy.hud.app.service.pandora.DataFrameMessage) {
                com.navdy.hud.app.service.pandora.messages.BaseIncomingMessage a4 = null;
                this.sendAckMessage(!a0.isSequence0);
                label0: {
                    Exception a5 = null;
                    try {
                        com.navdy.hud.app.service.pandora.messages.BaseIncomingMessage a6 = null;
                        try {
                            com.navdy.hud.app.service.pandora.messages.BaseIncomingMessage a7 = null;
                            com.navdy.hud.app.service.pandora.exceptions.MessageWrongWayException a8 = null;
                            try {
                                a6 = null;
                                a4 = null;
                                a7 = null;
                                com.navdy.hud.app.service.pandora.messages.BaseIncomingMessage a9 = com.navdy.hud.app.service.pandora.messages.BaseIncomingMessage.buildFromPayload(a0.payload);
                                com.navdy.service.library.log.Logger a10 = sLogger;
                                a6 = a9;
                                a4 = a9;
                                a7 = a9;
                                if (a10.isLoggable(2)) {
                                    com.navdy.service.library.log.Logger a11 = sLogger;
                                    a6 = a9;
                                    a4 = a9;
                                    a7 = a9;
                                    a11.i(new StringBuilder().append("Received (and ACKed) message from Pandora: ").append(a9.toString()).toString());
                                    a4 = a9;
                                    break label0;
                                } else {
                                    a4 = a9;
                                    break label0;
                                }
                            } catch(com.navdy.hud.app.service.pandora.exceptions.MessageWrongWayException a12) {
                                a8 = a12;
                            }
                            a4 = a7;
                            a5 = a8;
                        } catch(com.navdy.hud.app.service.pandora.exceptions.UnsupportedMessageReceivedException a13) {
                            a4 = a6;
                            a5 = a13;
                        }
                    } catch(com.navdy.hud.app.service.pandora.exceptions.CorruptedPayloadException a14) {
                        a5 = a14;
                    }
                    sLogger.w(new StringBuilder().append(a5.getMessage()).append(" - ignoring the message (already ACKed)").toString());
                }
                if (a4 != null) {
                    this.onMessageReceived(a4);
                }
            } else {
                sLogger.e("Unsupported message type");
            }
        }
    }
    
    public void onNotificationsStatusUpdate(com.navdy.service.library.events.notification.NotificationsStatusUpdate a) {
        if (com.navdy.service.library.events.notification.ServiceType.SERVICE_PANDORA.equals(a.service)) {
            if (this.remoteDeviceId != null) {
                com.navdy.service.library.events.DeviceInfo$Platform a0 = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getRemoteDevicePlatform();
                if (com.navdy.service.library.events.DeviceInfo$Platform.PLATFORM_Android.equals(a0)) {
                    boolean b = com.navdy.service.library.events.notification.NotificationsState.NOTIFICATIONS_ENABLED.equals(a.state);
                    sLogger.d(new StringBuilder().append("Received Pandora's running status: ").append(b).toString());
                    if (b) {
                        if (this.isConnected()) {
                            sLogger.i("Pandora is already connected - restarting connection");
                            this.terminateAndClose();
                        }
                        this.startConnecting(false);
                    } else {
                        this.terminateAndClose();
                    }
                } else {
                    sLogger.e("Pandora is supported only for Android clients for now");
                }
            } else {
                sLogger.e("Received Pandora app state message while remote device ID is unknown");
            }
        }
    }
    
    public void sendOrQueueMessage(com.navdy.hud.app.service.pandora.messages.BaseOutgoingMessage a) {
        this.msgQueue.add(a);
        if (this.activeAckMessageTimeoutCounter == null) {
            this.sendNextMessage();
        }
    }
    
    public void startAndPlay() {
        if (this.isConnected()) {
            this.sendOrQueueMessage((com.navdy.hud.app.service.pandora.messages.BaseOutgoingMessage)com.navdy.hud.app.service.pandora.messages.EventTrackPlay.INSTANCE);
        } else {
            this.startConnecting(true);
        }
    }
    
    protected void terminateAndClose() {
        this.sendOrQueueMessage((com.navdy.hud.app.service.pandora.messages.BaseOutgoingMessage)com.navdy.hud.app.service.pandora.messages.SessionTerminate.INSTANCE);
        this.close();
    }
}
