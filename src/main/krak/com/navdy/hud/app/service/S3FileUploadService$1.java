package com.navdy.hud.app.service;

class S3FileUploadService$1 implements com.amazonaws.mobileconnectors.s3.transferutility.TransferListener {
    final com.navdy.hud.app.service.S3FileUploadService this$0;
    final java.io.File val$file;
    final java.util.concurrent.atomic.AtomicBoolean val$isUploading;
    final com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver val$observer;
    
    S3FileUploadService$1(com.navdy.hud.app.service.S3FileUploadService a, java.util.concurrent.atomic.AtomicBoolean a0, java.io.File a1, com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver a2) {
        super();
        this.this$0 = a;
        this.val$isUploading = a0;
        this.val$file = a1;
        this.val$observer = a2;
    }
    
    public void onError(int i, Exception a) {
        this.this$0.logger.e(new StringBuilder().append("onError , while uploading, id ").append(i).toString(), (Throwable)a);
        com.navdy.hud.app.service.S3FileUploadService$Request a0 = this.this$0.getCurrentRequest();
        synchronized(this.this$0.getUploadQueue()) {
            if (a0 != null) {
                a1.add(a0);
                this.this$0.setCurrentRequest((com.navdy.hud.app.service.S3FileUploadService$Request)null);
            }
            /*monexit(a1)*/;
        }
        this.val$isUploading.compareAndSet(true, false);
        this.this$0.reSchedule();
    }
    
    public void onProgressChanged(int i, long j, long j0) {
    }
    
    public void onStateChanged(int i, com.amazonaws.mobileconnectors.s3.transferutility.TransferState a) {
        boolean b = false;
        boolean b0 = false;
        com.navdy.hud.app.service.S3FileUploadService$UploadQueue a0 = null;
        Throwable a1 = null;
        this.this$0.logger.d(new StringBuilder().append("State changed : ").append(a).toString());
        com.navdy.hud.app.service.S3FileUploadService$Request a2 = this.this$0.getCurrentRequest();
        switch(com.navdy.hud.app.service.S3FileUploadService$2.$SwitchMap$com$amazonaws$mobileconnectors$s3$transferutility$TransferState[a.ordinal()]) {
            case 5: {
                this.this$0.logger.d("Failed");
                b = true;
                b0 = true;
                break;
            }
            case 4: {
                this.this$0.logger.d(new StringBuilder().append("In progress ").append(this.val$observer.getBytesTotal()).append(" bytes").toString());
                b = false;
                b0 = true;
                break;
            }
            case 3: {
                this.this$0.logger.d("Waiting for network");
                com.navdy.hud.app.service.S3FileUploadService.access$000(this.this$0).cancel(i);
                b = true;
                b0 = false;
                break;
            }
            case 2: {
                this.this$0.logger.d("Upload canceled");
                b = true;
                b0 = true;
                break;
            }
            case 1: {
                this.this$0.logger.d("Upload completed");
                if (a2 != null) {
                    com.navdy.service.library.util.IOUtils.deleteFile(com.navdy.hud.app.HudApplication.getAppContext(), a2.file.getAbsolutePath());
                    this.val$isUploading.set(false);
                    this.this$0.uploadFinished(true, this.val$file.getPath(), a2.userTag);
                    this.this$0.setCurrentRequest((com.navdy.hud.app.service.S3FileUploadService$Request)null);
                }
                this.this$0.sync();
                b = false;
                b0 = true;
                break;
            }
            default: {
                b = true;
                b0 = true;
            }
        }
        label0: {
            if (b) {
                synchronized(this.this$0.getUploadQueue()) {
                    if (a2 == null) {
                        this.this$0.logger.d("request object in upload callback is null, network anomaly?");
                    } else {
                        a0.add(a2);
                        this.this$0.setCurrentRequest((com.navdy.hud.app.service.S3FileUploadService$Request)null);
                    }
                    /*monexit(a0)*/;
                }
                this.val$isUploading.compareAndSet(true, false);
                if (b0) {
                    this.this$0.reSchedule();
                }
            }
            return;
        }
        while(true) {
            try {
                /*monexit(a0)*/;
            } catch(IllegalMonitorStateException | NullPointerException a4) {
                Throwable a5 = a4;
                a1 = a5;
                continue;
            }
            throw a1;
        }
    }
}
