package com.navdy.hud.app.service.pandora.messages;

abstract class BaseOutgoingEmptyMessage extends com.navdy.hud.app.service.pandora.messages.BaseOutgoingConstantMessage {
    BaseOutgoingEmptyMessage() {
    }
    
    abstract protected byte getMessageType();
    
    
    protected java.io.ByteArrayOutputStream putThis(java.io.ByteArrayOutputStream a) {
        int i = this.getMessageType();
        com.navdy.hud.app.service.pandora.messages.BaseOutgoingEmptyMessage.putByte(a, (byte)i);
        return a;
    }
}
