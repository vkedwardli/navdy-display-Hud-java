package com.navdy.hud.app.service;

public class ConnectionServiceAnalyticsSupport$IAPListenerReceiver implements com.navdy.hud.mfi.IAPListener {
    public ConnectionServiceAnalyticsSupport$IAPListenerReceiver() {
    }
    
    public void onCoprocessorStatusCheckFailed(String s, String s0, String s1) {
        String[] a = new String[6];
        a[0] = "Description";
        a[1] = s;
        a[2] = "Status";
        a[3] = s0;
        a[4] = "Error_Code";
        a[5] = s1;
        com.navdy.hud.app.service.ConnectionServiceAnalyticsSupport.access$000("Mobile_iAP_Failure", true, a);
    }
    
    public void onDeviceAuthenticationSuccess(int i) {
        if (i > 0) {
            String[] a = new String[2];
            a[0] = "Retries";
            a[1] = Integer.toString(i);
            com.navdy.hud.app.service.ConnectionServiceAnalyticsSupport.access$000("Mobile_iAP_Retry_Success", true, a);
        }
    }
}
