package com.navdy.hud.app.service.pandora.messages;

public class UpdateTrack extends com.navdy.hud.app.service.pandora.messages.BaseIncomingOneIntMessage {
    public UpdateTrack(int i) {
        super(i);
    }
    
    protected static com.navdy.hud.app.service.pandora.messages.BaseIncomingMessage innerBuildFromPayload(byte[] a) {
        return new com.navdy.hud.app.service.pandora.messages.UpdateTrack(com.navdy.hud.app.service.pandora.messages.UpdateTrack.parseIntValue(a));
    }
    
    public String toString() {
        return new StringBuilder().append("New track with token: ").append(this.value).toString();
    }
}
