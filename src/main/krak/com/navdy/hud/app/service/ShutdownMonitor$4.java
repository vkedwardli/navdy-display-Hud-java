package com.navdy.hud.app.service;

class ShutdownMonitor$4 {
    final static int[] $SwitchMap$com$navdy$hud$app$event$Shutdown$Reason;
    final static int[] $SwitchMap$com$navdy$hud$app$event$Shutdown$State;
    final static int[] $SwitchMap$com$navdy$hud$app$service$ShutdownMonitor$MonitorState;
    
    static {
        $SwitchMap$com$navdy$hud$app$service$ShutdownMonitor$MonitorState = new int[com.navdy.hud.app.service.ShutdownMonitor$MonitorState.values().length];
        int[] a = $SwitchMap$com$navdy$hud$app$service$ShutdownMonitor$MonitorState;
        com.navdy.hud.app.service.ShutdownMonitor$MonitorState a0 = com.navdy.hud.app.service.ShutdownMonitor$MonitorState.STATE_PENDING_SHUTDOWN;
        try {
            a[a0.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$service$ShutdownMonitor$MonitorState[com.navdy.hud.app.service.ShutdownMonitor$MonitorState.STATE_SHUTDOWN_PROMPT.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException0) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$service$ShutdownMonitor$MonitorState[com.navdy.hud.app.service.ShutdownMonitor$MonitorState.STATE_BOOT.ordinal()] = 3;
        } catch(NoSuchFieldError ignoredException1) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$service$ShutdownMonitor$MonitorState[com.navdy.hud.app.service.ShutdownMonitor$MonitorState.STATE_OBD_ACTIVE.ordinal()] = 4;
        } catch(NoSuchFieldError ignoredException2) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$service$ShutdownMonitor$MonitorState[com.navdy.hud.app.service.ShutdownMonitor$MonitorState.STATE_ACTIVE_USE.ordinal()] = 5;
        } catch(NoSuchFieldError ignoredException3) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$service$ShutdownMonitor$MonitorState[com.navdy.hud.app.service.ShutdownMonitor$MonitorState.STATE_QUIET_MODE.ordinal()] = 6;
        } catch(NoSuchFieldError ignoredException4) {
        }
        $SwitchMap$com$navdy$hud$app$event$Shutdown$State = new int[com.navdy.hud.app.event.Shutdown$State.values().length];
        int[] a1 = $SwitchMap$com$navdy$hud$app$event$Shutdown$State;
        com.navdy.hud.app.event.Shutdown$State a2 = com.navdy.hud.app.event.Shutdown$State.CANCELED;
        try {
            a1[a2.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException5) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$event$Shutdown$State[com.navdy.hud.app.event.Shutdown$State.CONFIRMED.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException6) {
        }
        $SwitchMap$com$navdy$hud$app$event$Shutdown$Reason = new int[com.navdy.hud.app.event.Shutdown$Reason.values().length];
        int[] a3 = $SwitchMap$com$navdy$hud$app$event$Shutdown$Reason;
        com.navdy.hud.app.event.Shutdown$Reason a4 = com.navdy.hud.app.event.Shutdown$Reason.POWER_LOSS;
        try {
            a3[a4.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException7) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$event$Shutdown$Reason[com.navdy.hud.app.event.Shutdown$Reason.CRITICAL_VOLTAGE.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException8) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$event$Shutdown$Reason[com.navdy.hud.app.event.Shutdown$Reason.LOW_VOLTAGE.ordinal()] = 3;
        } catch(NoSuchFieldError ignoredException9) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$event$Shutdown$Reason[com.navdy.hud.app.event.Shutdown$Reason.TIMEOUT.ordinal()] = 4;
        } catch(NoSuchFieldError ignoredException10) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$event$Shutdown$Reason[com.navdy.hud.app.event.Shutdown$Reason.HIGH_TEMPERATURE.ordinal()] = 5;
        } catch(NoSuchFieldError ignoredException11) {
        }
    }
}
