package com.navdy.hud.app.service.pandora;

class CRC16CCITT {
    final private static int CRC_INITIAL_VALUE = 65535;
    
    CRC16CCITT() {
    }
    
    public static java.nio.ByteBuffer calculate(java.nio.ByteBuffer a) {
        int i = 65535;
        int i0 = 0;
        while(i0 < a.limit()) {
            int i1 = (i >>> 8 | i << 8) & 65535;
            int i2 = a.get(i0);
            int i3 = i1 ^ i2 & 255;
            int i4 = i3 ^ (i3 & 255) >> 4;
            int i5 = i4 ^ i4 << 12 & 65535;
            i = i5 ^ (i5 & 255) << 5 & 65535;
            i0 = i0 + 1;
        }
        java.nio.ByteBuffer a0 = java.nio.ByteBuffer.allocate(2);
        int i6 = (short)i;
        return a0.putShort((short)i6);
    }
    
    public static boolean checkCRC(byte[] a, byte[] a0) {
        return java.util.Arrays.equals(com.navdy.hud.app.service.pandora.CRC16CCITT.calculate(java.nio.ByteBuffer.wrap(a)).array(), a0);
    }
}
