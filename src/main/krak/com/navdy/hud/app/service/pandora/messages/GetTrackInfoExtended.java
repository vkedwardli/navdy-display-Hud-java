package com.navdy.hud.app.service.pandora.messages;

public class GetTrackInfoExtended extends com.navdy.hud.app.service.pandora.messages.BaseOutgoingEmptyMessage {
    final public static com.navdy.hud.app.service.pandora.messages.GetTrackInfoExtended INSTANCE;
    
    static {
        INSTANCE = new com.navdy.hud.app.service.pandora.messages.GetTrackInfoExtended();
    }
    
    private GetTrackInfoExtended() {
    }
    
    public byte[] buildPayload() {
        return super.buildPayload();
    }
    
    protected byte getMessageType() {
        return (byte)22;
    }
    
    public String toString() {
        return "Getting extended track's info";
    }
}
