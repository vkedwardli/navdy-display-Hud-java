package com.navdy.hud.app.service.pandora.exceptions;

public class UnsupportedMessageReceivedException extends Exception {
    public UnsupportedMessageReceivedException(byte a) {
        StringBuilder a0 = new StringBuilder().append("Message received from Pandora is not supported: ");
        Object[] a1 = new Object[1];
        a1[0] = Byte.valueOf(a);
        super(a0.append(String.format("%02X", a1)).toString());
    }
}
