package com.navdy.hud.app.service;

public class RemoteDeviceProxy extends com.navdy.service.library.device.RemoteDevice {
    private volatile int bandwidthLevel;
    private boolean connected;
    protected com.navdy.hud.app.service.ConnectionServiceProxy proxy;
    
    public RemoteDeviceProxy(com.navdy.hud.app.service.ConnectionServiceProxy a, android.content.Context a0, com.navdy.service.library.device.NavdyDeviceId a1) {
        super(a0, a1, true);
        this.bandwidthLevel = 1;
        this.proxy = a;
        this.proxy.getBus().register(this);
        this.bandwidthLevel = 1;
        this.connected = true;
    }
    
    private void notSupported() {
        throw new UnsupportedOperationException("Can't call this method on remoteDeviceProxy");
    }
    
    public boolean connect() {
        this.notSupported();
        return false;
    }
    
    public boolean disconnect() {
        this.notSupported();
        return false;
    }
    
    protected void dispatchNavdyEvent(com.navdy.service.library.events.NavdyEvent a) {
        this.dispatchToListeners((com.navdy.service.library.util.Listenable$EventDispatcher)new com.navdy.hud.app.service.RemoteDeviceProxy$1(this, a));
    }
    
    public int getLinkBandwidthLevel() {
        return this.bandwidthLevel;
    }
    
    public boolean isConnected() {
        return this.connected;
    }
    
    public void onConnectionStateChange(com.navdy.service.library.events.connection.ConnectionStateChange a) {
        switch(com.navdy.hud.app.service.RemoteDeviceProxy$2.$SwitchMap$com$navdy$service$library$events$connection$ConnectionStateChange$ConnectionState[a.state.ordinal()]) {
            case 2: {
                this.connected = false;
                this.dispatchDisconnectEvent(com.navdy.service.library.device.connection.Connection$DisconnectCause.NORMAL);
                break;
            }
            case 1: {
                this.connected = true;
                this.dispatchConnectEvent();
                break;
            }
        }
    }
    
    public void onNavdyEvent(com.navdy.service.library.events.NavdyEvent a) {
        this.dispatchNavdyEvent(a);
    }
    
    public boolean postEvent(com.navdy.service.library.events.NavdyEvent a) {
        this.proxy.postRemoteEvent(this.getDeviceId(), a);
        return true;
    }
    
    public boolean postEvent(com.navdy.service.library.events.NavdyEvent a, com.navdy.service.library.device.RemoteDevice$PostEventHandler a0) {
        boolean b = false;
        if (a0 == null) {
            this.proxy.postRemoteEvent(this.getDeviceId(), a);
            b = true;
        } else {
            this.notSupported();
            b = false;
        }
        return b;
    }
    
    public void setLinkBandwidthLevel(int i) {
        this.bandwidthLevel = i;
    }
}
