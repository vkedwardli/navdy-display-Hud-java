package com.navdy.hud.app.service.pandora.messages;

public class UpdateTrackAlbumArt extends com.navdy.hud.app.service.pandora.messages.BaseIncomingMessage {
    private static int MESSAGE_LENGTH;
    public int imageLength;
    public int trackToken;
    
    static {
        MESSAGE_LENGTH = 9;
    }
    
    public UpdateTrackAlbumArt() {
    }
    
    protected static com.navdy.hud.app.service.pandora.messages.BaseIncomingMessage innerBuildFromPayload(byte[] a) {
        if (a.length != MESSAGE_LENGTH) {
            throw new com.navdy.hud.app.service.pandora.exceptions.CorruptedPayloadException();
        }
        java.nio.ByteBuffer a0 = java.nio.ByteBuffer.wrap(a);
        a0.get();
        com.navdy.hud.app.service.pandora.messages.UpdateTrackAlbumArt a1 = new com.navdy.hud.app.service.pandora.messages.UpdateTrackAlbumArt();
        a1.trackToken = a0.getInt();
        a1.imageLength = a0.getInt();
        return a1;
    }
    
    public String toString() {
        return new StringBuilder().append("Artwork image loaded on client for track: ").append(this.trackToken).toString();
    }
}
