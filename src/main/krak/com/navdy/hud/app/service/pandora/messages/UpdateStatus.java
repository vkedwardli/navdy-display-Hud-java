package com.navdy.hud.app.service.pandora.messages;

public class UpdateStatus extends com.navdy.hud.app.service.pandora.messages.BaseIncomingMessage {
    private static int MESSAGE_LENGTH;
    private static java.util.Map SINGLETONS_MAP;
    public com.navdy.hud.app.service.pandora.messages.UpdateStatus$Value value;
    
    static {
        MESSAGE_LENGTH = 2;
        SINGLETONS_MAP = (java.util.Map)new com.navdy.hud.app.service.pandora.messages.UpdateStatus$1();
    }
    
    private UpdateStatus(com.navdy.hud.app.service.pandora.messages.UpdateStatus$Value a) {
        this.value = a;
    }
    
    UpdateStatus(com.navdy.hud.app.service.pandora.messages.UpdateStatus$Value a, com.navdy.hud.app.service.pandora.messages.UpdateStatus$1 a0) {
        this(a);
    }
    
    protected static com.navdy.hud.app.service.pandora.messages.BaseIncomingMessage innerBuildFromPayload(byte[] a) {
        if (a.length != MESSAGE_LENGTH) {
            throw new com.navdy.hud.app.service.pandora.exceptions.CorruptedPayloadException();
        }
        java.util.Map a0 = SINGLETONS_MAP;
        int i = a[1];
        com.navdy.hud.app.service.pandora.messages.UpdateStatus a1 = (com.navdy.hud.app.service.pandora.messages.UpdateStatus)a0.get(Byte.valueOf((byte)i));
        if (a1 == null) {
            throw new com.navdy.hud.app.service.pandora.exceptions.CorruptedPayloadException();
        }
        return a1;
    }
    
    public String toString() {
        return new StringBuilder().append("Update Status: ").append(this.value).toString();
    }
}
