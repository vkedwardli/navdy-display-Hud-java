package com.navdy.hud.app.ancs;

class AncsServiceConnector$4 {
    final static int[] $SwitchMap$com$navdy$hud$app$event$InitEvents$Phase;
    final static int[] $SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp;
    final static int[] $SwitchMap$com$navdy$service$library$events$connection$ConnectionStateChange$ConnectionState;
    final static int[] $SwitchMap$com$navdy$service$library$events$notification$NotificationsState;
    
    static {
        $SwitchMap$com$navdy$hud$app$event$InitEvents$Phase = new int[com.navdy.hud.app.event.InitEvents$Phase.values().length];
        int[] a = $SwitchMap$com$navdy$hud$app$event$InitEvents$Phase;
        com.navdy.hud.app.event.InitEvents$Phase a0 = com.navdy.hud.app.event.InitEvents$Phase.LOCALE_UP_TO_DATE;
        try {
            a[a0.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$event$InitEvents$Phase[com.navdy.hud.app.event.InitEvents$Phase.SWITCHING_LOCALE.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException0) {
        }
        $SwitchMap$com$navdy$service$library$events$connection$ConnectionStateChange$ConnectionState = new int[com.navdy.service.library.events.connection.ConnectionStateChange$ConnectionState.values().length];
        int[] a1 = $SwitchMap$com$navdy$service$library$events$connection$ConnectionStateChange$ConnectionState;
        com.navdy.service.library.events.connection.ConnectionStateChange$ConnectionState a2 = com.navdy.service.library.events.connection.ConnectionStateChange$ConnectionState.CONNECTION_DISCONNECTED;
        try {
            a1[a2.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException1) {
        }
        $SwitchMap$com$navdy$service$library$events$notification$NotificationsState = new int[com.navdy.service.library.events.notification.NotificationsState.values().length];
        int[] a3 = $SwitchMap$com$navdy$service$library$events$notification$NotificationsState;
        com.navdy.service.library.events.notification.NotificationsState a4 = com.navdy.service.library.events.notification.NotificationsState.NOTIFICATIONS_ENABLED;
        try {
            a3[a4.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException2) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$notification$NotificationsState[com.navdy.service.library.events.notification.NotificationsState.NOTIFICATIONS_STOPPED.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException3) {
        }
        $SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp = new int[com.navdy.hud.app.framework.glance.GlanceApp.values().length];
        int[] a5 = $SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp;
        com.navdy.hud.app.framework.glance.GlanceApp a6 = com.navdy.hud.app.framework.glance.GlanceApp.GOOGLE_CALENDAR;
        try {
            a5[a6.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException4) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp[com.navdy.hud.app.framework.glance.GlanceApp.GOOGLE_MAIL.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException5) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp[com.navdy.hud.app.framework.glance.GlanceApp.GENERIC_MAIL.ordinal()] = 3;
        } catch(NoSuchFieldError ignoredException6) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp[com.navdy.hud.app.framework.glance.GlanceApp.GOOGLE_HANGOUT.ordinal()] = 4;
        } catch(NoSuchFieldError ignoredException7) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp[com.navdy.hud.app.framework.glance.GlanceApp.SLACK.ordinal()] = 5;
        } catch(NoSuchFieldError ignoredException8) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp[com.navdy.hud.app.framework.glance.GlanceApp.WHATS_APP.ordinal()] = 6;
        } catch(NoSuchFieldError ignoredException9) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp[com.navdy.hud.app.framework.glance.GlanceApp.FACEBOOK_MESSENGER.ordinal()] = 7;
        } catch(NoSuchFieldError ignoredException10) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp[com.navdy.hud.app.framework.glance.GlanceApp.FACEBOOK.ordinal()] = 8;
        } catch(NoSuchFieldError ignoredException11) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp[com.navdy.hud.app.framework.glance.GlanceApp.TWITTER.ordinal()] = 9;
        } catch(NoSuchFieldError ignoredException12) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp[com.navdy.hud.app.framework.glance.GlanceApp.IMESSAGE.ordinal()] = 10;
        } catch(NoSuchFieldError ignoredException13) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp[com.navdy.hud.app.framework.glance.GlanceApp.APPLE_MAIL.ordinal()] = 11;
        } catch(NoSuchFieldError ignoredException14) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp[com.navdy.hud.app.framework.glance.GlanceApp.APPLE_CALENDAR.ordinal()] = 12;
        } catch(NoSuchFieldError ignoredException15) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp[com.navdy.hud.app.framework.glance.GlanceApp.GENERIC.ordinal()] = 13;
        } catch(NoSuchFieldError ignoredException16) {
        }
    }
}
