package com.navdy.hud.app.ancs;

class AncsServiceConnector$1 extends com.navdy.ancs.IAncsServiceListener$Stub {
    final com.navdy.hud.app.ancs.AncsServiceConnector this$0;
    
    AncsServiceConnector$1(com.navdy.hud.app.ancs.AncsServiceConnector a) {
        super();
        this.this$0 = a;
    }
    
    public void onConnectionStateChange(int i) {
        com.navdy.hud.app.ancs.AncsServiceConnector.sLogger.d(new StringBuilder().append("ANCS connection state change - ").append(i).toString());
        com.navdy.service.library.events.notification.NotificationsError a = null;
        com.navdy.service.library.events.notification.NotificationsState a0 = null;
        switch(i) {
            case 5: {
                a = com.navdy.service.library.events.notification.NotificationsError.NOTIFICATIONS_ERROR_BOND_REMOVED;
                a0 = com.navdy.service.library.events.notification.NotificationsState.NOTIFICATIONS_PAIRING_FAILED;
                break;
            }
            case 4: {
                a = com.navdy.service.library.events.notification.NotificationsError.NOTIFICATIONS_ERROR_AUTH_FAILED;
                a0 = com.navdy.service.library.events.notification.NotificationsState.NOTIFICATIONS_PAIRING_FAILED;
                break;
            }
            case 2: {
                a0 = com.navdy.service.library.events.notification.NotificationsState.NOTIFICATIONS_ENABLED;
                a = null;
                break;
            }
            case 1: {
                a0 = com.navdy.service.library.events.notification.NotificationsState.NOTIFICATIONS_CONNECTING;
                a = null;
                break;
            }
            case 0: {
                a0 = com.navdy.service.library.events.notification.NotificationsState.NOTIFICATIONS_STOPPED;
                a = null;
                break;
            }
            default: {
                a = null;
                a0 = null;
                break;
            }
            case 3: {
            }
        }
        if (a0 != null) {
            com.navdy.hud.app.ancs.AncsServiceConnector.access$000(this.this$0, a0, a);
        }
    }
    
    public void onNotification(com.navdy.ancs.AppleNotification a) {
        String s = a.getAppId();
        com.navdy.hud.app.ancs.AncsServiceConnector.sLogger.d(new StringBuilder().append("Hud listener notified of apple notification id[").append(s).toString());
        label2: try {
            if (com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().isRemoteDeviceConnected()) {
                if (!(com.navdy.hud.app.ancs.AncsServiceConnector.access$100().get(s) != null) && a.getEventId() == 0) {
                    Boolean a0 = this.this$0.mNotificationSettings.enabled(a);
                    boolean b = Boolean.TRUE.equals(a0);
                    label3: {
                        label4: {
                            if (!b) {
                                break label4;
                            }
                            if (!a.isPreExisting()) {
                                break label3;
                            }
                        }
                        if (a0 != null) {
                            break label2;
                        }
                        Long a1 = (Long)com.navdy.hud.app.ancs.AncsServiceConnector.access$300().get(s);
                        long j = android.os.SystemClock.elapsedRealtime();
                        label0: {
                            label1: {
                                if (a1 == null) {
                                    break label1;
                                }
                                if (j - a1.longValue() < 15000L) {
                                    break label0;
                                }
                            }
                            com.navdy.service.library.events.notification.NotificationEvent a2 = com.navdy.hud.app.ancs.AncsServiceConnector.access$400(this.this$0, a);
                            com.navdy.hud.app.ancs.AncsServiceConnector.sLogger.d(new StringBuilder().append("sending unknown notification to client - ").append(a2).toString());
                            this.this$0.mBus.post(new com.navdy.hud.app.event.RemoteEvent((com.squareup.wire.Message)a2));
                            com.navdy.hud.app.ancs.AncsServiceConnector.access$300().put(s, Long.valueOf(j));
                            break label2;
                        }
                        com.navdy.hud.app.ancs.AncsServiceConnector.sLogger.d("not sending unknown notification to client, within threshold");
                        break label2;
                    }
                    com.navdy.service.library.events.glances.GlanceEvent a3 = com.navdy.hud.app.ancs.AncsServiceConnector.access$200(this.this$0, a);
                    if (a3 != null) {
                        com.navdy.hud.app.ancs.AncsServiceConnector.sLogger.d("posting notification");
                        this.this$0.mBus.post(a3);
                    } else {
                        com.navdy.hud.app.ancs.AncsServiceConnector.sLogger.e("glance not converted");
                    }
                }
            } else {
                com.navdy.hud.app.ancs.AncsServiceConnector.sLogger.d("not connected");
            }
        } catch(Throwable a4) {
            com.navdy.hud.app.ancs.AncsServiceConnector.sLogger.e("ancs-notif", a4);
        }
    }
}
