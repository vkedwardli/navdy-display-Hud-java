package com.navdy.hud.app.framework.trips;

class TripManager$3 implements Runnable {
    final com.navdy.hud.app.framework.trips.TripManager this$0;
    
    TripManager$3(com.navdy.hud.app.framework.trips.TripManager a) {
        super();
        this.this$0 = a;
    }
    
    public void run() {
        if (com.navdy.hud.app.framework.trips.TripManager.access$2000(this.this$0) != null && com.navdy.hud.app.framework.trips.TripManager.access$2000(this.this$0).chosen_destination_id != null) {
            com.navdy.hud.app.framework.trips.TripManager.access$2000(this.this$0).arrived_at_destination_id(com.navdy.hud.app.framework.trips.TripManager.access$2000(this.this$0).chosen_destination_id);
            if (com.navdy.hud.app.framework.trips.TripManager.access$000(this.this$0) != com.navdy.hud.app.framework.trips.TripManager$State.STOPPED) {
                com.navdy.hud.app.framework.trips.TripManager.access$2100(this.this$0).post(new com.navdy.hud.app.event.RemoteEvent((com.squareup.wire.Message)com.navdy.hud.app.framework.trips.TripManager.access$2000(this.this$0).build()));
            }
        }
    }
}
