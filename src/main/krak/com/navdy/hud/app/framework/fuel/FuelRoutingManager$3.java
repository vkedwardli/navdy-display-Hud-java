package com.navdy.hud.app.framework.fuel;

class FuelRoutingManager$3 implements Runnable {
    final com.navdy.hud.app.framework.fuel.FuelRoutingManager this$0;
    
    FuelRoutingManager$3(com.navdy.hud.app.framework.fuel.FuelRoutingManager a) {
        super();
        this.this$0 = a;
    }
    
    public void run() {
        int i = com.navdy.hud.app.framework.fuel.FuelRoutingManager.access$800(this.this$0).getFuelLevel();
        boolean b = com.navdy.hud.app.framework.fuel.FuelRoutingManager.access$400(this.this$0);
        label1: {
            label5: {
                if (b) {
                    break label5;
                }
                if (i != -1) {
                    break label5;
                }
                com.navdy.hud.app.framework.fuel.FuelRoutingManager.access$000().d("checkFuelLevel:fuel level is not available from OBD Manager");
                com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().removeNotification(com.navdy.hud.app.framework.fuel.FuelRoutingManager.LOW_FUEL_ID);
                break label1;
            }
            if (com.navdy.hud.app.framework.fuel.FuelRoutingManager.access$600(this.this$0)) {
                com.navdy.hud.app.framework.fuel.FuelRoutingManager.access$000().d("checkFuelLevel: already calculating fuel route");
            } else if (com.navdy.hud.app.framework.fuel.FuelRoutingManager.access$300(this.this$0).isOnGasRoute()) {
                com.navdy.hud.app.framework.fuel.FuelRoutingManager.access$000().d("checkFuelLevel: already on fuel route");
            } else {
                long j = com.navdy.hud.app.framework.fuel.FuelRoutingManager.access$900(this.this$0);
                int i0 = (j < 0L) ? -1 : (j == 0L) ? 0 : 1;
                label4: {
                    if (i0 <= 0) {
                        break label4;
                    }
                    long j0 = android.os.SystemClock.elapsedRealtime() - com.navdy.hud.app.framework.fuel.FuelRoutingManager.access$900(this.this$0);
                    long j1 = com.navdy.hud.app.framework.fuel.FuelRoutingManager.access$1000();
                    int i1 = (j0 < j1) ? -1 : (j0 == j1) ? 0 : 1;
                    label3: {
                        if (i1 < 0) {
                            break label3;
                        }
                        com.navdy.hud.app.framework.fuel.FuelRoutingManager.access$000().d(new StringBuilder().append("checkFuelLevel: fuel glance dismiss threshold expired:").append(j0).toString());
                        com.navdy.hud.app.framework.fuel.FuelRoutingManager.access$902(this.this$0, 0L);
                        break label4;
                    }
                    com.navdy.hud.app.framework.fuel.FuelRoutingManager.access$000().d(new StringBuilder().append("checkFuelLevel: fuel glance dismiss threshold still valid:").append(j0).toString());
                    break label1;
                }
                com.navdy.hud.app.framework.fuel.FuelRoutingManager.access$000().i(new StringBuilder().append("checkFuelLevel: Fuel level:").append(i).append(" threshold:").append(com.navdy.hud.app.framework.fuel.FuelRoutingManager.access$1100(this.this$0)).append(" testMode:").append(com.navdy.hud.app.framework.fuel.FuelRoutingManager.access$400(this.this$0)).toString());
                boolean b0 = com.navdy.hud.app.framework.fuel.FuelRoutingManager.access$400(this.this$0);
                label2: {
                    if (b0) {
                        break label2;
                    }
                    if (i <= com.navdy.hud.app.framework.fuel.FuelRoutingManager.access$1100(this.this$0)) {
                        break label2;
                    }
                    if (i <= 20) {
                        if (!com.navdy.hud.app.framework.fuel.FuelRoutingManager.access$000().isLoggable(2)) {
                            break label1;
                        }
                        com.navdy.hud.app.framework.fuel.FuelRoutingManager.access$000().i("checkFuelLevel: no-op");
                        break label1;
                    } else {
                        com.navdy.hud.app.framework.fuel.FuelRoutingManager.access$1200(this.this$0);
                        break label1;
                    }
                }
                boolean b1 = com.navdy.hud.app.framework.fuel.FuelRoutingManager.access$400(this.this$0);
                label0: {
                    if (b1) {
                        break label0;
                    }
                    if (com.navdy.hud.app.framework.glance.GlanceHelper.isFuelNotificationEnabled()) {
                        break label0;
                    }
                    com.navdy.hud.app.framework.fuel.FuelRoutingManager.access$000().i("checkFuelLevel:fuel notifications are not enabled");
                    break label1;
                }
                if (com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().isNotificationPresent(com.navdy.hud.app.framework.fuel.FuelRoutingManager.LOW_FUEL_ID)) {
                    com.navdy.hud.app.framework.fuel.FuelRoutingManager.access$000().i("checkFuelLevel:low fuel glance already shown to user");
                } else {
                    this.this$0.findGasStations(i, true);
                }
            }
        }
    }
}
