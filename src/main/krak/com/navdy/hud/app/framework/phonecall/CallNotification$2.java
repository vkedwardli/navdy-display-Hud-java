package com.navdy.hud.app.framework.phonecall;
import com.navdy.hud.app.R;

class CallNotification$2 implements com.navdy.hud.app.ui.component.ChoiceLayout2$IListener {
    final com.navdy.hud.app.framework.phonecall.CallNotification this$0;
    
    CallNotification$2(com.navdy.hud.app.framework.phonecall.CallNotification a) {
        super();
        this.this$0 = a;
    }
    
    public void executeItem(com.navdy.hud.app.ui.component.ChoiceLayout2$Selection a) {
        switch(a.id) {
            case 107: {
                com.navdy.hud.app.framework.phonecall.CallNotification.access$500(this.this$0);
                break;
            }
            case 106: {
                if (com.navdy.hud.app.framework.phonecall.CallNotification.access$400(this.this$0).state == com.navdy.hud.app.framework.phonecall.CallManager$CallNotificationState.IN_PROGRESS) {
                    com.navdy.hud.app.framework.phonecall.CallNotification.access$400(this.this$0).sendCallAction(com.navdy.service.library.events.callcontrol.CallAction.CALL_END, (String)null);
                    break;
                } else {
                    com.navdy.hud.app.framework.phonecall.CallNotification.access$500(this.this$0);
                    break;
                }
            }
            case 105: {
                if (com.navdy.hud.app.framework.phonecall.CallNotification.access$400(this.this$0).state != com.navdy.hud.app.framework.phonecall.CallManager$CallNotificationState.DIALING) {
                    if (com.navdy.hud.app.framework.phonecall.CallNotification.access$400(this.this$0).state == com.navdy.hud.app.framework.phonecall.CallManager$CallNotificationState.IN_PROGRESS) {
                        break;
                    }
                    com.navdy.hud.app.framework.phonecall.CallNotification.access$500(this.this$0);
                    break;
                } else {
                    com.navdy.hud.app.framework.phonecall.CallNotification.access$400(this.this$0).sendCallAction(com.navdy.service.library.events.callcontrol.CallAction.CALL_END, (String)null);
                    break;
                }
            }
            case 104: {
                if (com.navdy.hud.app.framework.phonecall.CallNotification.access$400(this.this$0).state == com.navdy.hud.app.framework.phonecall.CallManager$CallNotificationState.IN_PROGRESS) {
                    com.navdy.hud.app.framework.phonecall.CallNotification.access$400(this.this$0).sendCallAction(com.navdy.service.library.events.callcontrol.CallAction.CALL_UNMUTE, (String)null);
                    com.navdy.hud.app.framework.phonecall.CallNotification.access$700(this.this$0).setText((CharSequence)"");
                    com.navdy.hud.app.framework.phonecall.CallNotification.access$800(this.this$0).setImageResource(R.drawable.icon_call_green);
                    com.navdy.hud.app.framework.phonecall.CallNotification.access$1100(this.this$0).setChoices((java.util.List)com.navdy.hud.app.framework.phonecall.CallNotification.access$1200(this.this$0), 0, com.navdy.hud.app.framework.phonecall.CallNotification.access$1000(this.this$0), 0.5f);
                    com.navdy.hud.app.framework.phonecall.CallNotification.access$1100(this.this$0).setTag(com.navdy.hud.app.framework.phonecall.CallNotification.access$1200(this.this$0));
                    break;
                } else {
                    com.navdy.hud.app.framework.phonecall.CallNotification.access$500(this.this$0);
                    break;
                }
            }
            case 103: {
                if (com.navdy.hud.app.framework.phonecall.CallNotification.access$400(this.this$0).state == com.navdy.hud.app.framework.phonecall.CallManager$CallNotificationState.IN_PROGRESS) {
                    com.navdy.hud.app.framework.phonecall.CallNotification.access$400(this.this$0).sendCallAction(com.navdy.service.library.events.callcontrol.CallAction.CALL_MUTE, (String)null);
                    com.navdy.hud.app.framework.phonecall.CallNotification.access$700(this.this$0).setText((CharSequence)com.navdy.hud.app.framework.phonecall.CallNotification.access$600(this.this$0));
                    com.navdy.hud.app.framework.phonecall.CallNotification.access$800(this.this$0).setImageResource(R.drawable.icon_call_muted);
                    com.navdy.hud.app.framework.phonecall.CallNotification.access$1100(this.this$0).setChoices((java.util.List)com.navdy.hud.app.framework.phonecall.CallNotification.access$900(this.this$0), 0, com.navdy.hud.app.framework.phonecall.CallNotification.access$1000(this.this$0), 0.5f);
                    com.navdy.hud.app.framework.phonecall.CallNotification.access$1100(this.this$0).setTag(com.navdy.hud.app.framework.phonecall.CallNotification.access$900(this.this$0));
                    break;
                } else {
                    com.navdy.hud.app.framework.phonecall.CallNotification.access$500(this.this$0);
                    break;
                }
            }
            case 5: {
                if (com.navdy.hud.app.framework.phonecall.CallNotification.access$1300(this.this$0) == null) {
                    break;
                }
                if (!com.navdy.hud.app.framework.phonecall.CallNotification.access$1300(this.this$0).isExpandedWithStack()) {
                    break;
                }
                com.navdy.hud.app.framework.phonecall.CallNotification.access$1300(this.this$0).collapseNotification(false, false);
                break;
            }
        }
    }
    
    public void itemSelected(com.navdy.hud.app.ui.component.ChoiceLayout2$Selection a) {
    }
}
