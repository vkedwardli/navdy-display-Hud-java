package com.navdy.hud.app.framework.voice;
import com.navdy.hud.app.R;

public class VoiceSearchNotification implements com.navdy.hud.app.framework.notifications.INotification, com.navdy.hud.app.ui.component.ChoiceLayout2$IListener, com.navdy.hud.app.framework.voice.VoiceSearchHandler$VoiceSearchUserInterface {
    final public static int ANIMATION_DURATION = 300;
    final private static java.util.HashMap BADGE_ICON_MAPPING;
    final private static java.util.HashMap ERROR_TEXT_MAPPING;
    final public static int INITIALIZATION_DELAY_MILLIS;
    final public static int INPUT_DELAY_MILLIS;
    final public static int RESULT_SELECTION_TIMEOUT = 10000;
    final public static int SEARCH_DELAY_MILLIS;
    final private static java.util.HashMap STATE_NAME_MAPPING;
    final private static com.navdy.service.library.log.Logger sLogger;
    public android.animation.AnimatorSet animatorSet;
    @InjectView(R.id.choiceLayout)
    public com.navdy.hud.app.ui.component.ChoiceLayout2 choiceLayout;
    private com.navdy.hud.app.framework.notifications.INotificationController controller;
    private com.navdy.service.library.events.audio.VoiceSearchResponse$VoiceSearchError error;
    private android.os.Handler handler;
    @InjectView(R.id.image_voice_search_active)
    public android.widget.ImageView imageActive;
    @InjectView(R.id.image_voice_search_inactive)
    public android.widget.ImageView imageInactive;
    @InjectView(R.id.img_processing_badge)
    public android.widget.ImageView imageProcessing;
    @InjectView(R.id.img_status_badge)
    public android.widget.ImageView imageStatus;
    private boolean isListeningOverBluetooth;
    private android.view.ViewGroup layout;
    private android.animation.AnimatorSet listeningFeedbackAnimation;
    @InjectView(R.id.voice_search_listening_feedback)
    public android.view.View listeningFeedbackView;
    private java.util.Locale locale;
    private android.animation.AnimatorSet mainImageAnimation;
    private boolean multipleResultsMode;
    private android.animation.AnimatorSet processingImageAnimation;
    private volatile boolean responseFromClientTimedOut;
    private java.util.List results;
    @InjectView(R.id.results_count)
    public android.widget.TextView resultsCount;
    @InjectView(R.id.results_count_container)
    public android.view.ViewGroup resultsCountContainer;
    private android.animation.AnimatorSet statusImageAnimation;
    @InjectView(R.id.subStatus)
    public android.widget.TextView subStatus;
    private java.util.List voiceSearchActiveChoices;
    private java.util.List voiceSearchFailedChoices;
    private com.navdy.hud.app.framework.voice.VoiceSearchHandler voiceSearchHandler;
    private com.navdy.hud.app.framework.voice.VoiceSearchNotification$VoiceSearchStateInternal voiceSearchState;
    private java.util.List voiceSearchSuccessResultsChoices;
    private Runnable waitingTimeoutRunnable;
    private java.util.List words;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.framework.voice.VoiceSearchNotification.class);
        INITIALIZATION_DELAY_MILLIS = (int)java.util.concurrent.TimeUnit.SECONDS.toMillis(30L);
        SEARCH_DELAY_MILLIS = (int)java.util.concurrent.TimeUnit.MINUTES.toMillis(2L);
        INPUT_DELAY_MILLIS = (int)java.util.concurrent.TimeUnit.MINUTES.toMillis(2L);
        com.navdy.hud.app.HudApplication.getAppContext().getResources();
        ERROR_TEXT_MAPPING = new java.util.HashMap();
        ERROR_TEXT_MAPPING.put(com.navdy.service.library.events.audio.VoiceSearchResponse$VoiceSearchError.FAILED_TO_START, Integer.valueOf(R.string.voice_search_failed));
        ERROR_TEXT_MAPPING.put(com.navdy.service.library.events.audio.VoiceSearchResponse$VoiceSearchError.OFFLINE, Integer.valueOf(R.string.voice_search_offline));
        ERROR_TEXT_MAPPING.put(com.navdy.service.library.events.audio.VoiceSearchResponse$VoiceSearchError.NEED_PERMISSION, Integer.valueOf(R.string.voice_search_need_permission));
        ERROR_TEXT_MAPPING.put(com.navdy.service.library.events.audio.VoiceSearchResponse$VoiceSearchError.NO_WORDS_RECOGNIZED, Integer.valueOf(R.string.voice_search_failed));
        ERROR_TEXT_MAPPING.put(com.navdy.service.library.events.audio.VoiceSearchResponse$VoiceSearchError.NOT_AVAILABLE, Integer.valueOf(R.string.voice_search_failed));
        ERROR_TEXT_MAPPING.put(com.navdy.service.library.events.audio.VoiceSearchResponse$VoiceSearchError.NO_RESULTS_FOUND, Integer.valueOf(R.string.voice_search_no_results_found));
        ERROR_TEXT_MAPPING.put(com.navdy.service.library.events.audio.VoiceSearchResponse$VoiceSearchError.AMBIENT_NOISE, Integer.valueOf(R.string.voice_search_ambient_noise));
        BADGE_ICON_MAPPING = new java.util.HashMap();
        BADGE_ICON_MAPPING.put(com.navdy.service.library.events.audio.VoiceSearchResponse$VoiceSearchError.FAILED_TO_START, Integer.valueOf(R.drawable.icon_badge_alert));
        BADGE_ICON_MAPPING.put(com.navdy.service.library.events.audio.VoiceSearchResponse$VoiceSearchError.OFFLINE, Integer.valueOf(R.drawable.icon_badge_alert));
        BADGE_ICON_MAPPING.put(com.navdy.service.library.events.audio.VoiceSearchResponse$VoiceSearchError.NEED_PERMISSION, Integer.valueOf(R.drawable.icon_badge_alert));
        BADGE_ICON_MAPPING.put(com.navdy.service.library.events.audio.VoiceSearchResponse$VoiceSearchError.NO_RESULTS_FOUND, Integer.valueOf(R.drawable.icon_badge_voice_search));
        BADGE_ICON_MAPPING.put(com.navdy.service.library.events.audio.VoiceSearchResponse$VoiceSearchError.NOT_AVAILABLE, Integer.valueOf(R.drawable.icon_badge_alert));
        BADGE_ICON_MAPPING.put(com.navdy.service.library.events.audio.VoiceSearchResponse$VoiceSearchError.AMBIENT_NOISE, Integer.valueOf(R.drawable.icon_badge_alert));
        STATE_NAME_MAPPING = new java.util.HashMap();
        STATE_NAME_MAPPING.put(com.navdy.hud.app.framework.voice.VoiceSearchNotification$VoiceSearchStateInternal.STARTING, Integer.valueOf(R.string.voice_search_wait));
        STATE_NAME_MAPPING.put(com.navdy.hud.app.framework.voice.VoiceSearchNotification$VoiceSearchStateInternal.LISTENING, Integer.valueOf(R.string.voice_search_speak_now));
        STATE_NAME_MAPPING.put(com.navdy.hud.app.framework.voice.VoiceSearchNotification$VoiceSearchStateInternal.FAILED, Integer.valueOf(R.string.voice_search_failed));
        STATE_NAME_MAPPING.put(com.navdy.hud.app.framework.voice.VoiceSearchNotification$VoiceSearchStateInternal.SEARCHING, Integer.valueOf(R.string.voice_search_searching));
    }
    
    public VoiceSearchNotification() {
        this.multipleResultsMode = false;
        this.voiceSearchState = com.navdy.hud.app.framework.voice.VoiceSearchNotification$VoiceSearchStateInternal.IDLE;
        this.waitingTimeoutRunnable = (Runnable)new com.navdy.hud.app.framework.voice.VoiceSearchNotification$1(this);
    }
    
    static com.navdy.hud.app.framework.notifications.INotificationController access$000(com.navdy.hud.app.framework.voice.VoiceSearchNotification a) {
        return a.controller;
    }
    
    static boolean access$102(com.navdy.hud.app.framework.voice.VoiceSearchNotification a, boolean b) {
        a.multipleResultsMode = b;
        return b;
    }
    
    static com.navdy.service.library.events.audio.VoiceSearchResponse$VoiceSearchError access$202(com.navdy.hud.app.framework.voice.VoiceSearchNotification a, com.navdy.service.library.events.audio.VoiceSearchResponse$VoiceSearchError a0) {
        a.error = a0;
        return a0;
    }
    
    static boolean access$302(com.navdy.hud.app.framework.voice.VoiceSearchNotification a, boolean b) {
        a.responseFromClientTimedOut = b;
        return b;
    }
    
    static void access$400(com.navdy.hud.app.framework.voice.VoiceSearchNotification a, com.navdy.hud.app.framework.voice.VoiceSearchNotification$VoiceSearchStateInternal a0) {
        a.updateUI(a0);
    }
    
    static android.animation.AnimatorSet access$500(com.navdy.hud.app.framework.voice.VoiceSearchNotification a) {
        return a.processingImageAnimation;
    }
    
    private void dismissNotification() {
        com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().removeNotification("navdy#voicesearch#notif");
    }
    
    public static android.animation.AnimatorSet getBadgeAnimation(android.view.View a, boolean b) {
        a.setAlpha(1f);
        android.animation.AnimatorSet a0 = new android.animation.AnimatorSet();
        a0.addListener((android.animation.Animator$AnimatorListener)new com.navdy.hud.app.framework.voice.VoiceSearchNotification$5(a));
        if (b) {
            a.setTranslationY(-10f);
            float[] a1 = new float[2];
            a1[0] = -10f;
            a1[1] = 0.0f;
            android.animation.ObjectAnimator a2 = android.animation.ObjectAnimator.ofFloat(a, "translationY", a1);
            a0.setDuration(200L);
            a0.play((android.animation.Animator)a2);
            a0.setInterpolator((android.animation.TimeInterpolator)new android.view.animation.BounceInterpolator());
        } else {
            a.setTranslationY(0.0f);
            float[] a3 = new float[2];
            a3[0] = 1f;
            a3[1] = 0.0f;
            android.animation.ObjectAnimator a4 = android.animation.ObjectAnimator.ofFloat(a, "alpha", a3);
            a0.setDuration(200L);
            a0.play((android.animation.Animator)a4);
        }
        return a0;
    }
    
    public static android.animation.AnimatorSet getListeningFeedbackDefaultAnimation(android.view.View a) {
        android.animation.AnimatorSet a0 = (android.animation.AnimatorSet)android.animation.AnimatorInflater.loadAnimator(a.getContext(), R.animator.listening_feedback_animation);
        a0.setTarget(a);
        a0.addListener((android.animation.Animator$AnimatorListener)new com.navdy.hud.app.framework.voice.VoiceSearchNotification$6(a0));
        return a0;
    }
    
    public static android.animation.Animator getProcessingBadgeAnimation(android.view.View a) {
        android.util.Property a0 = android.view.View.ROTATION;
        float[] a1 = new float[1];
        a1[0] = 360f;
        android.animation.ObjectAnimator a2 = android.animation.ObjectAnimator.ofFloat(a, a0, a1);
        ((android.animation.Animator)a2).setDuration(300L);
        ((android.animation.Animator)a2).setInterpolator((android.animation.TimeInterpolator)new android.view.animation.AccelerateDecelerateInterpolator());
        return a2;
    }
    
    private void setChoices(com.navdy.hud.app.framework.voice.VoiceSearchNotification$VoiceSearchStateInternal a) {
        switch(com.navdy.hud.app.framework.voice.VoiceSearchNotification$7.$SwitchMap$com$navdy$hud$app$framework$voice$VoiceSearchNotification$VoiceSearchStateInternal[a.ordinal()]) {
            case 4: case 6: {
                this.choiceLayout.setVisibility(0);
                this.choiceLayout.setChoices(this.voiceSearchActiveChoices, 0, (com.navdy.hud.app.ui.component.ChoiceLayout2$IListener)this);
                break;
            }
            case 3: {
                if (this.results == null) {
                    break;
                }
                if (this.results.size() <= 1) {
                    break;
                }
                this.choiceLayout.setVisibility(0);
                this.choiceLayout.setChoices(this.voiceSearchSuccessResultsChoices, 0, (com.navdy.hud.app.ui.component.ChoiceLayout2$IListener)this);
                break;
            }
            case 1: {
                this.choiceLayout.setVisibility(0);
                this.choiceLayout.setChoices(this.voiceSearchFailedChoices, 0, (com.navdy.hud.app.ui.component.ChoiceLayout2$IListener)this);
                break;
            }
            default: {
                this.choiceLayout.setVisibility(8);
            }
        }
    }
    
    private void showHideVoiceSearchTipsIfNeeded(boolean b) {
        sLogger.d(new StringBuilder().append("showHideVoiceSearchTipsIfNeeded, show : ").append(b).toString());
        com.navdy.hud.app.ui.activity.Main a = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getUiStateManager().getRootScreen();
        if (b) {
            sLogger.d(new StringBuilder().append("Should show voice search tips :").append(this.voiceSearchHandler.shouldShowVoiceSearchTipsToUser()).toString());
            if (this.voiceSearchHandler.shouldShowVoiceSearchTipsToUser()) {
                android.view.ViewGroup a0 = (android.view.ViewGroup)android.view.LayoutInflater.from(com.navdy.hud.app.HudApplication.getAppContext()).inflate(R.layout.voice_search_tips_layout, a.getNotificationExtensionViewHolder(), false);
                com.navdy.hud.app.framework.voice.VoiceSearchTipsView a1 = (com.navdy.hud.app.framework.voice.VoiceSearchTipsView)a0.findViewById(R.id.voice_search_tips);
                a1.setListeningOverBluetooth(this.isListeningOverBluetooth);
                if (this.locale == null) {
                    a1.setVoiceSearchLocale(com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getCurrentProfile().getLocale());
                } else {
                    a1.setVoiceSearchLocale(this.locale);
                }
                a.addNotificationExtensionView((android.view.View)a0);
                this.voiceSearchHandler.showedVoiceSearchTipsToUser();
            }
        } else {
            sLogger.d("Remove notification extension");
            a.removeNotificationExtensionView();
        }
    }
    
    private void startVoiceSearch() {
        this.handler.removeCallbacks(this.waitingTimeoutRunnable);
        this.responseFromClientTimedOut = false;
        this.updateUI(com.navdy.hud.app.framework.voice.VoiceSearchNotification$VoiceSearchStateInternal.STARTING);
        this.voiceSearchHandler.startVoiceSearch();
        this.handler.postDelayed(this.waitingTimeoutRunnable, (long)INITIALIZATION_DELAY_MILLIS);
    }
    
    private void updateUI(com.navdy.hud.app.framework.voice.VoiceSearchNotification$VoiceSearchStateInternal a) {
        this.updateUI(a, this.error, this.results);
    }
    
    private void updateUI(com.navdy.hud.app.framework.voice.VoiceSearchNotification$VoiceSearchStateInternal a, com.navdy.service.library.events.audio.VoiceSearchResponse$VoiceSearchError a0, java.util.List a1) {
        sLogger.d(new StringBuilder().append("updateUI newState :").append(a).append(" , Error :").append(a0).toString());
        boolean b = this.voiceSearchState != a;
        if (b) {
            sLogger.d(new StringBuilder().append("State Changed From : ").append(this.voiceSearchState).append(" , New state : ").append(a).toString());
        }
        this.voiceSearchState = a;
        android.animation.AnimatorSet a2 = new android.animation.AnimatorSet();
        if (b) {
            this.setChoices(a);
        }
        switch(com.navdy.hud.app.framework.voice.VoiceSearchNotification$7.$SwitchMap$com$navdy$hud$app$framework$voice$VoiceSearchNotification$VoiceSearchStateInternal[a.ordinal()]) {
            case 3: {
                this.subStatus.setText((CharSequence)"");
                break;
            }
            case 2: {
                if (!b) {
                    break;
                }
                this.subStatus.setText((CharSequence)"");
                break;
            }
            case 1: {
                label2: {
                    label3: {
                        if (a0 == null) {
                            break label3;
                        }
                        if (!this.responseFromClientTimedOut) {
                            break label2;
                        }
                    }
                    this.subStatus.setText(this.voiceSearchState.getStatusTextResId());
                    break;
                }
                if (com.navdy.hud.app.framework.voice.VoiceSearchNotification$7.$SwitchMap$com$navdy$service$library$events$audio$VoiceSearchResponse$VoiceSearchError[a0.ordinal()] != 0) {
                    this.subStatus.setText((CharSequence)"");
                    break;
                } else {
                    Integer a3 = (a0 != null) ? (Integer)ERROR_TEXT_MAPPING.get(a0) : null;
                    if (a3 == null) {
                        a3 = Integer.valueOf(R.string.voice_search_failed);
                        com.navdy.service.library.log.Logger a4 = sLogger;
                        Object[] a5 = new Object[1];
                        a5[0] = a0;
                        a4.e(String.format("unhandled error response: %s", a5));
                    }
                    this.subStatus.setText(a3.intValue());
                    break;
                }
            }
            default: {
                if (b) {
                    this.subStatus.setText(((Integer)STATE_NAME_MAPPING.get(a)).intValue());
                }
            }
        }
        android.animation.AnimatorSet a6 = null;
        if (b) {
            this.resultsCountContainer.setVisibility(4);
            if (this.mainImageAnimation != null) {
                this.mainImageAnimation.removeAllListeners();
                this.mainImageAnimation.cancel();
            }
            this.mainImageAnimation = new android.animation.AnimatorSet();
            a6 = this.mainImageAnimation;
            if (com.navdy.hud.app.framework.voice.VoiceSearchNotification$7.$SwitchMap$com$navdy$hud$app$framework$voice$VoiceSearchNotification$VoiceSearchStateInternal[a.ordinal()] == 4) {
                android.widget.ImageView a7 = this.imageActive;
                float[] a8 = new float[2];
                a8[0] = 0.0f;
                a8[1] = 1f;
                android.animation.ObjectAnimator a9 = android.animation.ObjectAnimator.ofFloat(a7, "alpha", a8);
                a9.addListener((android.animation.Animator$AnimatorListener)new com.navdy.hud.app.framework.voice.VoiceSearchNotification$2(this));
                a6.play((android.animation.Animator)a9);
            } else {
                this.imageInactive.setVisibility(0);
                if (this.imageActive.getAlpha() > 0.0f) {
                    android.widget.ImageView a10 = this.imageActive;
                    float[] a11 = new float[2];
                    a11[0] = 1f;
                    a11[1] = 0.0f;
                    android.animation.ObjectAnimator a12 = android.animation.ObjectAnimator.ofFloat(a10, "alpha", a11);
                    a12.addListener((android.animation.Animator$AnimatorListener)new com.navdy.hud.app.framework.voice.VoiceSearchNotification$3(this));
                    a6.play((android.animation.Animator)a12);
                }
            }
            switch(com.navdy.hud.app.framework.voice.VoiceSearchNotification$7.$SwitchMap$com$navdy$hud$app$framework$voice$VoiceSearchNotification$VoiceSearchStateInternal[a.ordinal()]) {
                case 3: {
                    if (a1 != null && a1.size() > 0) {
                        this.resultsCount.setText((CharSequence)Integer.toString(a1.size()));
                    }
                    this.resultsCountContainer.setVisibility(0);
                    this.imageInactive.setVisibility(4);
                    break;
                }
                case 1: {
                    if (a0 != com.navdy.service.library.events.audio.VoiceSearchResponse$VoiceSearchError.NO_RESULTS_FOUND) {
                        break;
                    }
                    this.resultsCountContainer.setVisibility(0);
                    this.imageInactive.setVisibility(4);
                    break;
                }
                default: {
                    break;
                }
                case 2: {
                }
            }
        }
        android.animation.AnimatorSet a13 = null;
        if (b) {
            if (this.statusImageAnimation != null) {
                this.statusImageAnimation.removeAllListeners();
                this.statusImageAnimation.cancel();
                this.statusImageAnimation = null;
            }
            this.imageStatus.setVisibility(4);
            if (this.processingImageAnimation != null) {
                this.processingImageAnimation.cancel();
            }
            this.imageProcessing.setVisibility(4);
            this.statusImageAnimation = new android.animation.AnimatorSet();
            int i = com.navdy.hud.app.framework.voice.VoiceSearchNotification$7.$SwitchMap$com$navdy$hud$app$framework$voice$VoiceSearchNotification$VoiceSearchStateInternal[a.ordinal()];
            a13 = null;
            switch(i) {
                case 5: case 6: {
                    this.imageProcessing.setVisibility(0);
                    if (this.processingImageAnimation == null) {
                        this.processingImageAnimation = new android.animation.AnimatorSet();
                        this.processingImageAnimation.addListener((android.animation.Animator$AnimatorListener)new com.navdy.hud.app.framework.voice.VoiceSearchNotification$4(this));
                        this.processingImageAnimation.play(com.navdy.hud.app.framework.voice.VoiceSearchNotification.getProcessingBadgeAnimation((android.view.View)this.imageProcessing));
                    }
                    a13 = this.processingImageAnimation;
                    break;
                }
                case 4: {
                    if (this.isListeningOverBluetooth) {
                        this.imageStatus.setImageResource(R.drawable.icon_badge_b_t);
                    } else {
                        this.imageStatus.setImageResource(R.drawable.icon_badge_phone);
                    }
                    this.statusImageAnimation.play((android.animation.Animator)com.navdy.hud.app.framework.voice.VoiceSearchNotification.getBadgeAnimation((android.view.View)this.imageStatus, true));
                    a13 = null;
                    break;
                }
                case 3: {
                    this.imageStatus.setImageResource(R.drawable.icon_badge_voice_search);
                    this.statusImageAnimation.play((android.animation.Animator)com.navdy.hud.app.framework.voice.VoiceSearchNotification.getBadgeAnimation((android.view.View)this.imageStatus, true));
                    a13 = null;
                    break;
                }
                case 1: {
                    if (a0 == null) {
                        this.imageStatus.setImageDrawable((android.graphics.drawable.Drawable)null);
                        a13 = null;
                        break;
                    } else {
                        if (com.navdy.service.library.events.audio.VoiceSearchResponse$VoiceSearchError.NO_WORDS_RECOGNIZED != a0) {
                            Integer a14 = (a0 != null) ? (Integer)BADGE_ICON_MAPPING.get(a0) : null;
                            if (a14 == null) {
                                a14 = Integer.valueOf(R.drawable.icon_badge_alert);
                                com.navdy.service.library.log.Logger a15 = sLogger;
                                Object[] a16 = new Object[1];
                                a16[0] = a0;
                                a15.e(String.format("no icon for error: %s", a16));
                            }
                            this.imageStatus.setImageResource(a14.intValue());
                        } else if (this.isListeningOverBluetooth) {
                            this.imageStatus.setImageResource(R.drawable.icon_badge_b_t);
                        } else {
                            this.imageStatus.setImageResource(R.drawable.icon_badge_phone);
                        }
                        this.statusImageAnimation.play((android.animation.Animator)com.navdy.hud.app.framework.voice.VoiceSearchNotification.getBadgeAnimation((android.view.View)this.imageStatus, true));
                        a13 = null;
                        break;
                    }
                }
                default: {
                    a13 = null;
                    break;
                }
                case 2: {
                }
            }
        }
        android.animation.AnimatorSet$Builder a17 = null;
        if (a6 != null) {
            a17 = a2.play((android.animation.Animator)a6);
        }
        if (b && this.statusImageAnimation != null) {
            if (a17 == null) {
                a2.play((android.animation.Animator)this.statusImageAnimation);
            } else {
                a17.before((android.animation.Animator)this.statusImageAnimation);
            }
        }
        if (b && a13 != null) {
            this.processingImageAnimation.start();
        }
        a2.setDuration(300L);
        a2.start();
        if (b) {
            if (this.voiceSearchState != com.navdy.hud.app.framework.voice.VoiceSearchNotification$VoiceSearchStateInternal.LISTENING) {
                this.listeningFeedbackView.setVisibility(8);
                if (this.listeningFeedbackAnimation != null && this.listeningFeedbackAnimation.isRunning()) {
                    this.listeningFeedbackAnimation.cancel();
                }
            } else {
                this.listeningFeedbackView.setVisibility(0);
                if (this.listeningFeedbackAnimation == null) {
                    this.listeningFeedbackAnimation = com.navdy.hud.app.framework.voice.VoiceSearchNotification.getListeningFeedbackDefaultAnimation(this.listeningFeedbackView);
                }
                this.listeningFeedbackAnimation.start();
            }
        }
        label0: if (b && a == com.navdy.hud.app.framework.voice.VoiceSearchNotification$VoiceSearchStateInternal.FAILED) {
            com.navdy.service.library.events.audio.VoiceSearchResponse$VoiceSearchError a18 = com.navdy.service.library.events.audio.VoiceSearchResponse$VoiceSearchError.NO_RESULTS_FOUND;
            label1: {
                if (a0 == a18) {
                    break label1;
                }
                if (a0 != com.navdy.service.library.events.audio.VoiceSearchResponse$VoiceSearchError.NO_WORDS_RECOGNIZED) {
                    break label0;
                }
            }
            sLogger.d(new StringBuilder().append("VoiceSearch error , error :").append(a0).append(" , showing voice search").toString());
            this.showHideVoiceSearchTipsIfNeeded(true);
        }
    }
    
    public boolean canAddToStackIfCurrentExists() {
        return true;
    }
    
    public void close() {
        this.dismissNotification();
    }
    
    public void executeItem(com.navdy.hud.app.ui.component.ChoiceLayout2$Selection a) {
        switch(a.id) {
            case R.id.retry: {
                com.navdy.hud.app.analytics.AnalyticsSupport.recordVoiceSearchRetry();
                this.showHideVoiceSearchTipsIfNeeded(false);
                this.startVoiceSearch();
                break;
            }
            case R.id.list: {
                com.navdy.hud.app.analytics.AnalyticsSupport.recordVoiceSearchAdditionalResultsAction(com.navdy.hud.app.analytics.AnalyticsSupport$VoiceSearchAdditionalResultsAction.ADDITIONAL_RESULTS_LIST);
                this.voiceSearchHandler.showResults();
                break;
            }
            case R.id.go: {
                com.navdy.hud.app.analytics.AnalyticsSupport.recordVoiceSearchAdditionalResultsAction(com.navdy.hud.app.analytics.AnalyticsSupport$VoiceSearchAdditionalResultsAction.ADDITIONAL_RESULTS_GO);
                this.voiceSearchHandler.go();
                this.dismissNotification();
                break;
            }
            case R.id.dismiss: {
                com.navdy.hud.app.analytics.AnalyticsSupport.recordRouteSelectionCancelled();
                this.dismissNotification();
                break;
            }
        }
    }
    
    public boolean expandNotification() {
        return false;
    }
    
    public int getColor() {
        return 0;
    }
    
    public android.view.View getExpandedView(android.content.Context a, Object a0) {
        return null;
    }
    
    public int getExpandedViewIndicatorColor() {
        return 0;
    }
    
    public String getId() {
        return "navdy#voicesearch#notif";
    }
    
    public int getTimeout() {
        return 0;
    }
    
    public com.navdy.hud.app.framework.notifications.NotificationType getType() {
        return com.navdy.hud.app.framework.notifications.NotificationType.VOICE_SEARCH;
    }
    
    public android.view.View getView(android.content.Context a) {
        if (this.layout == null) {
            this.layout = (android.view.ViewGroup)android.view.LayoutInflater.from(a).inflate(R.layout.notification_voice_search, (android.view.ViewGroup)null);
        }
        butterknife.ButterKnife.inject(this, (android.view.View)this.layout);
        android.content.res.Resources a0 = a.getResources();
        int i = a0.getColor(R.color.glance_dismiss);
        int i0 = a0.getColor(R.color.glance_ok_blue);
        int i1 = a0.getColor(R.color.glance_ok_go);
        int i2 = a0.getColor(R.color.glance_ok_blue);
        com.navdy.hud.app.ui.component.ChoiceLayout2$Choice a1 = new com.navdy.hud.app.ui.component.ChoiceLayout2$Choice(R.id.dismiss, R.drawable.icon_glances_dismiss, i, R.drawable.icon_glances_dismiss, -16777216, a0.getString(R.string.cancel), i);
        this.voiceSearchFailedChoices = (java.util.List)new java.util.ArrayList();
        this.voiceSearchFailedChoices.add(new com.navdy.hud.app.ui.component.ChoiceLayout2$Choice(R.id.retry, R.drawable.icon_glances_retry, i0, R.drawable.icon_glances_retry, -16777216, a0.getString(R.string.retry), i0));
        this.voiceSearchFailedChoices.add(a1);
        this.voiceSearchActiveChoices = (java.util.List)new java.util.ArrayList();
        this.voiceSearchActiveChoices.add(a1);
        this.voiceSearchSuccessResultsChoices = (java.util.List)new java.util.ArrayList();
        this.voiceSearchSuccessResultsChoices.add(new com.navdy.hud.app.ui.component.ChoiceLayout2$Choice(R.id.list, R.drawable.icon_glances_read, i2, R.drawable.icon_glances_read, -16777216, a0.getString(R.string.view), i2));
        this.voiceSearchSuccessResultsChoices.add(new com.navdy.hud.app.ui.component.ChoiceLayout2$Choice(R.id.go, R.drawable.icon_go, i1, R.drawable.icon_go, -16777216, a0.getString(R.string.go), i1));
        this.choiceLayout.setChoices(this.voiceSearchActiveChoices, 0, (com.navdy.hud.app.ui.component.ChoiceLayout2$IListener)this);
        this.choiceLayout.setVisibility(0);
        return this.layout;
    }
    
    public android.animation.AnimatorSet getViewSwitchAnimation(boolean b) {
        return null;
    }
    
    public boolean isAlive() {
        return false;
    }
    
    public boolean isPurgeable() {
        return false;
    }
    
    public void itemSelected(com.navdy.hud.app.ui.component.ChoiceLayout2$Selection a) {
    }
    
    public com.navdy.hud.app.manager.InputManager$IInputHandler nextHandler() {
        return null;
    }
    
    public void onClick() {
    }
    
    public void onExpandedNotificationEvent(com.navdy.hud.app.ui.framework.UIStateManager$Mode a) {
    }
    
    public void onExpandedNotificationSwitched() {
    }
    
    public boolean onGesture(com.navdy.service.library.events.input.GestureEvent a) {
        boolean b = false;
        boolean b0 = this.multipleResultsMode;
        label0: {
            label1: {
                if (!b0) {
                    break label1;
                }
                com.navdy.service.library.events.input.Gesture a0 = a.gesture;
                switch(com.navdy.hud.app.framework.voice.VoiceSearchNotification$7.$SwitchMap$com$navdy$service$library$events$input$Gesture[a0.ordinal()]) {
                    case 2: {
                        this.voiceSearchHandler.go();
                        this.dismissNotification();
                        b = true;
                        break label0;
                    }
                    case 1: {
                        this.voiceSearchHandler.showResults();
                        b = true;
                        break label0;
                    }
                }
            }
            b = false;
        }
        return b;
    }
    
    public boolean onKey(com.navdy.hud.app.manager.InputManager$CustomKeyEvent a) {
        boolean b = false;
        switch(com.navdy.hud.app.framework.voice.VoiceSearchNotification$7.$SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent[a.ordinal()]) {
            case 3: {
                this.choiceLayout.executeSelectedItem();
                b = true;
                break;
            }
            case 2: {
                this.choiceLayout.moveSelectionRight();
                b = true;
                break;
            }
            case 1: {
                this.choiceLayout.moveSelectionLeft();
                b = true;
                break;
            }
            default: {
                b = false;
            }
        }
        return b;
    }
    
    public void onNotificationEvent(com.navdy.hud.app.ui.framework.UIStateManager$Mode a) {
    }
    
    public void onStart(com.navdy.hud.app.framework.notifications.INotificationController a) {
        this.handler = new android.os.Handler();
        if (this.layout != null) {
            this.voiceSearchHandler = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getVoiceSearchHandler();
            this.voiceSearchHandler.setVoiceSearchUserInterface((com.navdy.hud.app.framework.voice.VoiceSearchHandler$VoiceSearchUserInterface)this);
            this.controller = a;
            this.error = null;
            this.isListeningOverBluetooth = false;
            this.words = null;
            this.imageStatus.setImageDrawable((android.graphics.drawable.Drawable)null);
            com.navdy.hud.app.analytics.AnalyticsSupport.recordVoiceSearchEntered();
            this.startVoiceSearch();
        }
    }
    
    public void onStop() {
        this.handler.removeCallbacks(this.waitingTimeoutRunnable);
        this.controller = null;
        this.voiceSearchHandler.stopVoiceSearch();
        this.voiceSearchHandler.setVoiceSearchUserInterface((com.navdy.hud.app.framework.voice.VoiceSearchHandler$VoiceSearchUserInterface)null);
        if (this.layout != null) {
            if (this.processingImageAnimation != null) {
                this.processingImageAnimation.removeAllListeners();
                this.processingImageAnimation.cancel();
                this.processingImageAnimation = null;
            }
            if (this.listeningFeedbackAnimation != null) {
                this.listeningFeedbackAnimation.removeAllListeners();
                this.listeningFeedbackAnimation.cancel();
                this.listeningFeedbackAnimation = null;
            }
        }
        com.navdy.hud.app.framework.toast.ToastManager.getInstance().disableToasts(false);
        sLogger.v("startVoiceSearch: enabled toast");
    }
    
    public void onTrackHand(float f) {
    }
    
    public void onUpdate() {
    }
    
    public void onVoiceSearchResponse(com.navdy.service.library.events.audio.VoiceSearchResponse a) {
        sLogger.d(new StringBuilder().append("onVoiceSearchResponse:").append(a).toString());
        this.handler.removeCallbacks(this.waitingTimeoutRunnable);
        if (this.responseFromClientTimedOut) {
            sLogger.d("Not processing further response as the client did not respond in time");
        } else {
            com.navdy.hud.app.analytics.AnalyticsSupport.recordVoiceSearchResult(a);
            com.navdy.hud.app.manager.MusicManager a0 = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getMusicManager();
            switch(com.navdy.hud.app.framework.voice.VoiceSearchNotification$7.$SwitchMap$com$navdy$service$library$events$audio$VoiceSearchResponse$VoiceSearchState[a.state.ordinal()]) {
                case 5: {
                    this.error = a.error;
                    if (a.results == null) {
                        this.multipleResultsMode = false;
                        this.updateUI(com.navdy.hud.app.framework.voice.VoiceSearchNotification$VoiceSearchStateInternal.IDLE);
                        this.dismissNotification();
                        break;
                    } else {
                        boolean b = false;
                        sLogger.v(new StringBuilder().append("onVoiceSearchResponse: size=").append(a.results.size()).toString());
                        if (a.results.size() != 1) {
                            b = false;
                        } else {
                            com.navdy.service.library.events.destination.Destination a1 = (com.navdy.service.library.events.destination.Destination)a.results.get(0);
                            if (a1.favorite_type == null) {
                                b = false;
                            } else {
                                switch(com.navdy.hud.app.framework.voice.VoiceSearchNotification$7.$SwitchMap$com$navdy$service$library$events$destination$Destination$FavoriteType[a1.favorite_type.ordinal()]) {
                                    case 4: {
                                        if (a1.last_navigated_to == null) {
                                            b = false;
                                            break;
                                        } else {
                                            long j = a1.last_navigated_to.longValue();
                                            b = ((j < 0L) ? -1 : (j == 0L) ? 0 : 1) > 0;
                                            break;
                                        }
                                    }
                                    case 1: case 2: case 3: {
                                        b = true;
                                        break;
                                    }
                                    default: {
                                        b = false;
                                    }
                                }
                            }
                        }
                        if (b) {
                            sLogger.v("onVoiceSearchResponse: start trip");
                            this.multipleResultsMode = false;
                            this.voiceSearchHandler.go();
                            this.dismissNotification();
                            break;
                        } else {
                            sLogger.v("onVoiceSearchResponse: show picker");
                            com.navdy.hud.app.analytics.AnalyticsSupport.recordVoiceSearchAdditionalResultsAction(com.navdy.hud.app.analytics.AnalyticsSupport$VoiceSearchAdditionalResultsAction.ADDITIONAL_RESULTS_LIST);
                            this.voiceSearchHandler.showResults();
                            break;
                        }
                    }
                }
                case 4: {
                    if (a0 != null) {
                        a0.acceptResumes();
                    }
                    this.multipleResultsMode = false;
                    this.error = a.error;
                    sLogger.d(new StringBuilder().append("Error ").append(a.error).toString());
                    this.updateUI(com.navdy.hud.app.framework.voice.VoiceSearchNotification$VoiceSearchStateInternal.FAILED);
                    break;
                }
                case 3: {
                    this.multipleResultsMode = false;
                    this.isListeningOverBluetooth = ((Boolean)com.squareup.wire.Wire.get(a.listeningOverBluetooth, Boolean.valueOf(false))).booleanValue();
                    this.error = a.error;
                    this.updateUI(com.navdy.hud.app.framework.voice.VoiceSearchNotification$VoiceSearchStateInternal.LISTENING);
                    this.handler.postDelayed(this.waitingTimeoutRunnable, (long)INPUT_DELAY_MILLIS);
                    break;
                }
                case 2: {
                    if (a0 != null) {
                        a0.acceptResumes();
                    }
                    this.multipleResultsMode = false;
                    this.error = a.error;
                    this.updateUI(com.navdy.hud.app.framework.voice.VoiceSearchNotification$VoiceSearchStateInternal.SEARCHING);
                    this.handler.postDelayed(this.waitingTimeoutRunnable, (long)SEARCH_DELAY_MILLIS);
                    break;
                }
                case 1: {
                    this.multipleResultsMode = false;
                    this.error = a.error;
                    if (!android.text.TextUtils.isEmpty((CharSequence)a.locale)) {
                        this.locale = com.navdy.hud.app.profile.HudLocale.getLocaleForID(a.locale);
                    }
                    this.updateUI(com.navdy.hud.app.framework.voice.VoiceSearchNotification$VoiceSearchStateInternal.STARTING);
                    this.handler.postDelayed(this.waitingTimeoutRunnable, (long)INITIALIZATION_DELAY_MILLIS);
                    break;
                }
            }
        }
    }
    
    public boolean supportScroll() {
        return false;
    }
}
