package com.navdy.hud.app.framework.notifications;

public class NotificationAnimator {
    private static com.navdy.service.library.log.Logger sLogger;
    
    static {
        sLogger = com.navdy.hud.app.framework.notifications.NotificationManager.sLogger;
    }
    
    public NotificationAnimator() {
    }
    
    static com.navdy.service.library.log.Logger access$000() {
        return sLogger;
    }
    
    static void animateChildViewOut(android.view.View a, int i, Runnable a0) {
        a.animate().alpha(0.0f).setDuration((long)i).setStartDelay(0L).withEndAction(a0).start();
    }
    
    static void animateExpandedViewOut(com.navdy.hud.app.view.NotificationView a, int i, android.view.View a0, android.view.View a1, Runnable a2, com.navdy.hud.app.framework.notifications.NotificationManager$Info a3, boolean b, boolean b0) {
        com.navdy.hud.app.framework.notifications.NotificationAnimator$3 a4 = new com.navdy.hud.app.framework.notifications.NotificationAnimator$3(a1);
        com.navdy.hud.app.framework.notifications.NotificationAnimator$4 a5 = new com.navdy.hud.app.framework.notifications.NotificationAnimator$4(a0, a3, a, a2);
        label2: {
            label0: {
                label1: {
                    if (b) {
                        break label1;
                    }
                    if (!b0) {
                        break label0;
                    }
                }
                a.animate().x((float)i).withStartAction((Runnable)a4).withEndAction((Runnable)a5).start();
                break label2;
            }
            ((Runnable)a4).run();
            ((Runnable)a5).run();
        }
    }
    
    static void animateExpandedViews(android.view.View a, android.view.View a0, android.view.ViewGroup a1, int i, int i0, int i1, Runnable a2) {
        android.view.ViewPropertyAnimator a3 = null;
        if (a0 != null) {
            a3 = com.navdy.hud.app.framework.notifications.NotificationAnimator.animateOut(a0, a1, i, i0, 0, a2);
            a2 = null;
        }
        android.view.ViewPropertyAnimator a4 = null;
        if (a != null) {
            a4 = com.navdy.hud.app.framework.notifications.NotificationAnimator.animateIn(a, a1, i, i0, i1, a2);
        }
        if (a3 != null) {
            a3.start();
        }
        if (a4 != null) {
            a4.start();
        }
    }
    
    private static android.view.ViewPropertyAnimator animateIn(android.view.View a, android.view.ViewGroup a0, int i, int i0, int i1, Runnable a1) {
        a.setLayoutParams((android.view.ViewGroup$LayoutParams)new android.widget.FrameLayout$LayoutParams(-1, -1));
        a.setTranslationY((float)i);
        a.setAlpha(0.0f);
        a0.addView(a);
        android.view.ViewPropertyAnimator a2 = a.animate().alpha(1f).translationY(0.0f).setDuration((long)i0).setStartDelay((long)i1).withStartAction((Runnable)new com.navdy.hud.app.framework.notifications.NotificationAnimator$1(a));
        if (a1 != null) {
            a2.withEndAction(a1);
        }
        return a2;
    }
    
    static void animateNotifViews(android.view.View a, android.view.View a0, android.view.ViewGroup a1, int i, int i0, int i1, Runnable a2) {
        android.view.ViewPropertyAnimator a3 = null;
        if (a0 != null) {
            a3 = com.navdy.hud.app.framework.notifications.NotificationAnimator.animateOut(a0, a1, i, i0, 0, (Runnable)null);
        }
        android.view.ViewPropertyAnimator a4 = null;
        if (a != null) {
            a4 = com.navdy.hud.app.framework.notifications.NotificationAnimator.animateIn(a, a1, i, i0, i1, a2);
        }
        if (a3 != null) {
            a3.start();
        }
        if (a4 != null) {
            a4.start();
        }
    }
    
    private static android.view.ViewPropertyAnimator animateOut(android.view.View a, android.view.ViewGroup a0, int i, int i0, int i1, Runnable a1) {
        android.view.ViewPropertyAnimator a2 = a.animate().alpha(0.0f).translationY((float)(-i)).setDuration((long)i0).setStartDelay((long)i1).withEndAction((Runnable)new com.navdy.hud.app.framework.notifications.NotificationAnimator$2(a0, a));
        if (a1 != null) {
            a2.withEndAction(a1);
        }
        return a2;
    }
}
