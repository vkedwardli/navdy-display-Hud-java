package com.navdy.hud.app.framework.voice;

final class VoiceSearchNotification$5 extends com.navdy.hud.app.ui.framework.DefaultAnimationListener {
    final android.view.View val$view;
    
    VoiceSearchNotification$5(android.view.View a) {
        super();
        this.val$view = a;
    }
    
    public void onAnimationStart(android.animation.Animator a) {
        this.val$view.setVisibility(0);
        super.onAnimationStart(a);
    }
}
