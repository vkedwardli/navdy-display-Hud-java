package com.navdy.hud.app.framework.trips;

class TripManager$1 implements Runnable {
    final com.navdy.hud.app.framework.trips.TripManager this$0;
    final com.navdy.hud.app.framework.trips.TripManager$TrackingEvent val$event;
    
    TripManager$1(com.navdy.hud.app.framework.trips.TripManager a, com.navdy.hud.app.framework.trips.TripManager$TrackingEvent a0) {
        super();
        this.this$0 = a;
        this.val$event = a0;
    }
    
    public void run() {
        if (com.navdy.hud.app.framework.trips.TripManager.access$000(this.this$0) == com.navdy.hud.app.framework.trips.TripManager$State.STOPPED) {
            com.navdy.hud.app.framework.trips.TripManager.access$100(this.this$0);
            com.navdy.hud.app.framework.trips.TripManager.access$002(this.this$0, com.navdy.hud.app.framework.trips.TripManager$State.STARTED);
        }
        com.here.android.mpa.common.GeoCoordinate a = com.navdy.hud.app.framework.trips.TripManager$TrackingEvent.access$200(this.val$event).getCoordinate();
        double d = a.getLatitude();
        double d0 = a.getLongitude();
        double d1 = a.getAltitude();
        label2: if (com.navdy.hud.app.maps.util.MapUtils.sanitizeCoords(a) != null) {
            com.here.android.mpa.common.GeoCoordinate a0 = com.navdy.hud.app.framework.trips.TripManager.access$400(this.this$0);
            label0: {
                if (a0 == null) {
                    break label0;
                }
                long j = android.os.SystemClock.elapsedRealtime();
                long j0 = j - com.navdy.hud.app.framework.trips.TripManager.access$500(this.this$0);
                if (j0 < 1000L) {
                    break label2;
                }
                com.navdy.hud.app.framework.trips.TripManager.access$502(this.this$0, j);
                int i = (int)com.navdy.hud.app.framework.trips.TripManager$TrackingEvent.access$200(this.val$event).getCoordinate().distanceTo(com.navdy.hud.app.framework.trips.TripManager.access$400(this.this$0));
                double d2 = (double)j0 / 1000.0;
                if ((double)i / d2 <= 200.0) {
                    com.navdy.hud.app.framework.trips.TripManager.access$602(this.this$0, com.navdy.hud.app.framework.trips.TripManager.access$600(this.this$0) + i);
                    com.navdy.hud.app.framework.trips.TripManager.access$702(this.this$0, com.navdy.hud.app.framework.trips.TripManager.access$700(this.this$0) + i);
                    long j1 = com.navdy.hud.app.framework.trips.TripManager.access$800(this.this$0);
                    int i0 = (j1 < 0L) ? -1 : (j1 == 0L) ? 0 : 1;
                    label1: {
                        if (i0 == 0) {
                            break label1;
                        }
                        if (j - com.navdy.hud.app.framework.trips.TripManager.access$800(this.this$0) <= 30000L) {
                            break label0;
                        }
                    }
                    com.navdy.hud.app.framework.trips.TripManager.access$802(this.this$0, j);
                    long j2 = this.this$0.getTotalDistanceTravelledWithNavdy();
                    this.this$0.preferences.edit().putLong("trip_manager_total_distance_travelled_with_navdy", j2).apply();
                    com.navdy.hud.app.framework.trips.TripManager.access$902(this.this$0, (long)com.navdy.hud.app.framework.trips.TripManager.access$700(this.this$0));
                } else {
                    com.navdy.hud.app.framework.trips.TripManager.access$300().d(new StringBuilder().append("Unusual jump between co ordinates, Last lat :").append(com.navdy.hud.app.framework.trips.TripManager.access$400(this.this$0).getLatitude()).append(", ").append("Lon :").append(com.navdy.hud.app.framework.trips.TripManager.access$400(this.this$0).getLongitude()).append(", ").append("Current Lat:").append(com.navdy.hud.app.framework.trips.TripManager$TrackingEvent.access$200(this.val$event).getCoordinate().getLatitude()).append(", ").append("Lon :").append(com.navdy.hud.app.framework.trips.TripManager$TrackingEvent.access$200(this.val$event).getCoordinate().getLongitude()).toString());
                }
            }
            com.navdy.hud.app.framework.trips.TripManager.access$402(this.this$0, com.navdy.hud.app.framework.trips.TripManager$TrackingEvent.access$200(this.val$event).getCoordinate());
            if (com.navdy.hud.app.framework.trips.TripManager.access$1000(this.this$0)) {
                com.navdy.hud.app.framework.trips.TripManager.access$1102(this.this$0, com.navdy.hud.app.framework.trips.TripManager.access$1100(this.this$0) + 1);
                com.navdy.hud.app.analytics.TelemetrySession a1 = com.navdy.hud.app.analytics.TelemetrySession.INSTANCE;
                com.navdy.service.library.events.TripUpdate$Builder a2 = new com.navdy.service.library.events.TripUpdate$Builder().trip_number(Long.valueOf(com.navdy.hud.app.framework.trips.TripManager.access$1500(this.this$0))).sequence_number(Integer.valueOf(com.navdy.hud.app.framework.trips.TripManager.access$1100(this.this$0))).timestamp(Long.valueOf(System.currentTimeMillis())).distance_traveled(Integer.valueOf(com.navdy.hud.app.framework.trips.TripManager.access$600(this.this$0))).current_position(new com.navdy.service.library.events.location.LatLong(Double.valueOf(d), Double.valueOf(d0))).elevation(Double.valueOf(com.navdy.hud.app.framework.trips.TripManager$TrackingEvent.access$200(this.val$event).getCoordinate().getAltitude())).bearing(Float.valueOf((float)com.navdy.hud.app.framework.trips.TripManager$TrackingEvent.access$200(this.val$event).getHeading())).gps_speed(Float.valueOf((float)com.navdy.hud.app.framework.trips.TripManager$TrackingEvent.access$200(this.val$event).getSpeed())).obd_speed(Integer.valueOf(com.navdy.hud.app.framework.trips.TripManager.access$1400(this.this$0).getObdSpeed())).hard_acceleration_count(Integer.valueOf(a1.getSessionHardAccelerationCount())).hard_breaking_count(Integer.valueOf(a1.getSessionHardBrakingCount())).high_g_count(Integer.valueOf(a1.getSessionHighGCount())).speeding_ratio(Double.valueOf((double)a1.getSessionSpeedingPercentage())).excessive_speeding_ratio(Double.valueOf((double)a1.getSessionExcessiveSpeedingPercentage())).meters_traveled_since_boot(Integer.valueOf((int)a1.getSessionDistance())).horizontal_accuracy(Float.valueOf((com.navdy.hud.app.framework.trips.TripManager$TrackingEvent.access$200(this.val$event).getLatitudeAccuracy() + com.navdy.hud.app.framework.trips.TripManager$TrackingEvent.access$200(this.val$event).getLongitudeAccuracy()) / 2f)).elevation_accuracy(Float.valueOf(com.navdy.hud.app.framework.trips.TripManager$TrackingEvent.access$200(this.val$event).getAltitudeAccuracy())).last_raw_coordinate(com.navdy.hud.app.framework.trips.TripManager.access$1300(this.this$0, com.navdy.hud.app.framework.trips.TripManager.access$1200(this.this$0)));
                if (com.navdy.hud.app.framework.trips.TripManager.access$300().isLoggable(2)) {
                    com.navdy.hud.app.framework.trips.TripManager.access$1600(this.this$0).setLength(0);
                    String s = new StringBuilder().append("TripUpdate: tripNumber=").append(com.navdy.hud.app.framework.trips.TripManager.access$1500(this.this$0)).append("; sequenceNumber=").append(com.navdy.hud.app.framework.trips.TripManager.access$1100(this.this$0)).append("; timestamp=").append(System.currentTimeMillis()).append("; distanceTraveled=").append(com.navdy.hud.app.framework.trips.TripManager.access$600(this.this$0)).append("; currentPosition=").append(d).append(",").append(d0).append("; elevation=").append(com.navdy.hud.app.framework.trips.TripManager$TrackingEvent.access$200(this.val$event).getCoordinate().getAltitude()).append("; bearing=").append(com.navdy.hud.app.framework.trips.TripManager$TrackingEvent.access$200(this.val$event).getHeading()).append("; gpsSpeed=").append(com.navdy.hud.app.framework.trips.TripManager$TrackingEvent.access$200(this.val$event).getSpeed()).append("; obdSpeed=").append(com.navdy.hud.app.framework.trips.TripManager.access$1400(this.this$0).getObdSpeed()).toString();
                    com.navdy.hud.app.framework.trips.TripManager.access$1600(this.this$0).append(s);
                }
                if (this.val$event.isRouteTracking()) {
                    a2.chosen_destination_id(com.navdy.hud.app.framework.trips.TripManager$TrackingEvent.access$1900(this.val$event)).estimated_time_remaining(Integer.valueOf(com.navdy.hud.app.framework.trips.TripManager$TrackingEvent.access$1800(this.val$event))).distance_to_destination(Integer.valueOf(com.navdy.hud.app.framework.trips.TripManager$TrackingEvent.access$1700(this.val$event)));
                    if (com.navdy.hud.app.framework.trips.TripManager.access$300().isLoggable(2)) {
                        String s0 = new StringBuilder().append("; chosenDestinationId=").append(com.navdy.hud.app.framework.trips.TripManager$TrackingEvent.access$1900(this.val$event)).append("; estimatedTimeRemaining=").append(com.navdy.hud.app.framework.trips.TripManager$TrackingEvent.access$1800(this.val$event)).append("; distanceToDestination=").append(com.navdy.hud.app.framework.trips.TripManager$TrackingEvent.access$1700(this.val$event)).toString();
                        com.navdy.hud.app.framework.trips.TripManager.access$1600(this.this$0).append(s0);
                    }
                }
                if (com.navdy.hud.app.framework.trips.TripManager.access$300().isLoggable(2)) {
                    com.navdy.hud.app.framework.trips.TripManager.access$300().v(com.navdy.hud.app.framework.trips.TripManager.access$1600(this.this$0).toString());
                }
                com.navdy.hud.app.framework.trips.TripManager.access$2002(this.this$0, a2);
                if (com.navdy.hud.app.framework.trips.TripManager.access$000(this.this$0) != com.navdy.hud.app.framework.trips.TripManager$State.STOPPED) {
                    com.navdy.hud.app.framework.trips.TripManager.access$2100(this.this$0).post(new com.navdy.hud.app.event.RemoteEvent((com.squareup.wire.Message)com.navdy.hud.app.framework.trips.TripManager.access$2000(this.this$0).build()));
                }
            }
        } else {
            com.navdy.hud.app.framework.trips.TripManager.access$300().v(new StringBuilder().append("filtering out bad coords: ").append(d).append(", ").append(d0).append(", ").append(d1).toString());
        }
    }
}
