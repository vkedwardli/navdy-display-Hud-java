package com.navdy.hud.app.framework.notifications;

class NotificationManager$15 implements android.view.ViewTreeObserver$OnGlobalLayoutListener {
    final com.navdy.hud.app.framework.notifications.NotificationManager this$0;
    final int val$index;
    final com.navdy.hud.app.framework.notifications.IScrollEvent val$scroll;
    
    NotificationManager$15(com.navdy.hud.app.framework.notifications.NotificationManager a, int i, com.navdy.hud.app.framework.notifications.IScrollEvent a0) {
        super();
        this.this$0 = a;
        this.val$index = i;
        this.val$scroll = a0;
    }
    
    public void onGlobalLayout() {
        android.graphics.RectF a = com.navdy.hud.app.framework.notifications.NotificationManager.access$3900(this.this$0).getItemPos(this.val$index);
        label0: {
            label2: {
                if (a == null) {
                    break label2;
                }
                float f = a.left;
                int i = (f > 0.0f) ? 1 : (f == 0.0f) ? 0 : -1;
                label1: {
                    if (i != 0) {
                        break label1;
                    }
                    if (a.top == 0.0f) {
                        break label0;
                    }
                }
                com.navdy.hud.app.framework.notifications.NotificationManager.access$4600(this.this$0, a, this.val$scroll);
            }
            com.navdy.hud.app.framework.notifications.NotificationManager.access$3900(this.this$0).getViewTreeObserver().removeOnGlobalLayoutListener((android.view.ViewTreeObserver$OnGlobalLayoutListener)this);
        }
    }
}
