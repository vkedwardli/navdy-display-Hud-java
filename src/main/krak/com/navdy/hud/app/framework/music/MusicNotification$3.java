package com.navdy.hud.app.framework.music;

class MusicNotification$3 implements Runnable {
    final com.navdy.hud.app.framework.music.MusicNotification this$0;
    final boolean val$animate;
    final android.graphics.Bitmap val$artwork;
    
    MusicNotification$3(com.navdy.hud.app.framework.music.MusicNotification a, android.graphics.Bitmap a0, boolean b) {
        super();
        this.this$0 = a;
        this.val$artwork = a0;
        this.val$animate = b;
    }
    
    public void run() {
        com.navdy.hud.app.framework.music.MusicNotification.access$400(this.this$0).setArtworkBitmap(this.val$artwork, this.val$animate);
    }
}
