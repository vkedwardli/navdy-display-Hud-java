package com.navdy.hud.app.framework.destinations;

public class Destination$Builder {
    private java.util.List contacts;
    private int destinationIcon;
    private int destinationIconBkColor;
    private String destinationPlaceId;
    private String destinationSubtitle;
    private String destinationTitle;
    private com.navdy.hud.app.framework.destinations.Destination$DestinationType destinationType;
    private double displayPositionLatitude;
    private double displayPositionLongitude;
    private String distanceStr;
    private com.navdy.hud.app.framework.destinations.Destination$FavoriteDestinationType favoriteDestinationType;
    private String fullAddress;
    private String identifier;
    private String initials;
    private double navigationPositionLatitude;
    private double navigationPositionLongitude;
    private java.util.List phoneNumbers;
    private com.navdy.hud.app.framework.destinations.Destination$PlaceCategory placeCategory;
    private com.navdy.service.library.events.places.PlaceType placeType;
    private String recentTimeLabel;
    private int recentTimeLabelColor;
    private boolean recommendation;
    
    public Destination$Builder() {
        this.destinationIconBkColor = 0;
    }
    
    public Destination$Builder(com.navdy.hud.app.framework.destinations.Destination a) {
        this.destinationIconBkColor = 0;
        this.navigationPositionLatitude = a.navigationPositionLatitude;
        this.navigationPositionLongitude = a.navigationPositionLongitude;
        this.displayPositionLatitude = a.displayPositionLatitude;
        this.displayPositionLongitude = a.displayPositionLongitude;
        this.favoriteDestinationType = a.favoriteDestinationType;
        this.fullAddress = a.fullAddress;
        this.destinationTitle = a.destinationTitle;
        this.destinationSubtitle = a.destinationSubtitle;
        this.identifier = a.identifier;
        this.destinationType = a.destinationType;
        this.initials = a.initials;
        this.placeCategory = a.placeCategory;
        this.recentTimeLabel = a.recentTimeLabel;
        this.recentTimeLabelColor = a.recentTimeLabelColor;
        this.recommendation = a.recommendation;
        this.destinationIcon = a.destinationIcon;
        this.destinationIconBkColor = a.destinationIconBkColor;
        this.destinationPlaceId = a.destinationPlaceId;
        this.distanceStr = a.distanceStr;
        this.phoneNumbers = a.phoneNumbers;
        this.contacts = a.contacts;
    }
    
    static double access$100(com.navdy.hud.app.framework.destinations.Destination$Builder a) {
        return a.navigationPositionLatitude;
    }
    
    static com.navdy.hud.app.framework.destinations.Destination$DestinationType access$1000(com.navdy.hud.app.framework.destinations.Destination$Builder a) {
        return a.destinationType;
    }
    
    static String access$1100(com.navdy.hud.app.framework.destinations.Destination$Builder a) {
        return a.initials;
    }
    
    static com.navdy.hud.app.framework.destinations.Destination$PlaceCategory access$1200(com.navdy.hud.app.framework.destinations.Destination$Builder a) {
        return a.placeCategory;
    }
    
    static String access$1300(com.navdy.hud.app.framework.destinations.Destination$Builder a) {
        return a.recentTimeLabel;
    }
    
    static int access$1400(com.navdy.hud.app.framework.destinations.Destination$Builder a) {
        return a.recentTimeLabelColor;
    }
    
    static boolean access$1500(com.navdy.hud.app.framework.destinations.Destination$Builder a) {
        return a.recommendation;
    }
    
    static int access$1600(com.navdy.hud.app.framework.destinations.Destination$Builder a) {
        return a.destinationIcon;
    }
    
    static int access$1700(com.navdy.hud.app.framework.destinations.Destination$Builder a) {
        return a.destinationIconBkColor;
    }
    
    static String access$1800(com.navdy.hud.app.framework.destinations.Destination$Builder a) {
        return a.destinationPlaceId;
    }
    
    static com.navdy.service.library.events.places.PlaceType access$1900(com.navdy.hud.app.framework.destinations.Destination$Builder a) {
        return a.placeType;
    }
    
    static double access$200(com.navdy.hud.app.framework.destinations.Destination$Builder a) {
        return a.navigationPositionLongitude;
    }
    
    static String access$2000(com.navdy.hud.app.framework.destinations.Destination$Builder a) {
        return a.distanceStr;
    }
    
    static java.util.List access$2100(com.navdy.hud.app.framework.destinations.Destination$Builder a) {
        return a.phoneNumbers;
    }
    
    static java.util.List access$2200(com.navdy.hud.app.framework.destinations.Destination$Builder a) {
        return a.contacts;
    }
    
    static double access$300(com.navdy.hud.app.framework.destinations.Destination$Builder a) {
        return a.displayPositionLatitude;
    }
    
    static double access$400(com.navdy.hud.app.framework.destinations.Destination$Builder a) {
        return a.displayPositionLongitude;
    }
    
    static String access$500(com.navdy.hud.app.framework.destinations.Destination$Builder a) {
        return a.fullAddress;
    }
    
    static String access$600(com.navdy.hud.app.framework.destinations.Destination$Builder a) {
        return a.destinationTitle;
    }
    
    static String access$700(com.navdy.hud.app.framework.destinations.Destination$Builder a) {
        return a.destinationSubtitle;
    }
    
    static String access$800(com.navdy.hud.app.framework.destinations.Destination$Builder a) {
        return a.identifier;
    }
    
    static com.navdy.hud.app.framework.destinations.Destination$FavoriteDestinationType access$900(com.navdy.hud.app.framework.destinations.Destination$Builder a) {
        return a.favoriteDestinationType;
    }
    
    public com.navdy.hud.app.framework.destinations.Destination build() {
        return new com.navdy.hud.app.framework.destinations.Destination(this, (com.navdy.hud.app.framework.destinations.Destination$1)null);
    }
    
    public com.navdy.hud.app.framework.destinations.Destination$Builder contacts(java.util.List a) {
        this.contacts = a;
        return this;
    }
    
    public com.navdy.hud.app.framework.destinations.Destination$Builder destinationIcon(int i) {
        this.destinationIcon = i;
        return this;
    }
    
    public com.navdy.hud.app.framework.destinations.Destination$Builder destinationIconBkColor(int i) {
        this.destinationIconBkColor = i;
        return this;
    }
    
    public com.navdy.hud.app.framework.destinations.Destination$Builder destinationPlaceId(String s) {
        this.destinationPlaceId = s;
        return this;
    }
    
    public com.navdy.hud.app.framework.destinations.Destination$Builder destinationSubtitle(String s) {
        this.destinationSubtitle = s;
        return this;
    }
    
    public com.navdy.hud.app.framework.destinations.Destination$Builder destinationTitle(String s) {
        this.destinationTitle = s;
        return this;
    }
    
    public com.navdy.hud.app.framework.destinations.Destination$Builder destinationType(com.navdy.hud.app.framework.destinations.Destination$DestinationType a) {
        this.destinationType = a;
        return this;
    }
    
    public com.navdy.hud.app.framework.destinations.Destination$Builder displayPositionLatitude(double d) {
        this.displayPositionLatitude = d;
        return this;
    }
    
    public com.navdy.hud.app.framework.destinations.Destination$Builder displayPositionLongitude(double d) {
        this.displayPositionLongitude = d;
        return this;
    }
    
    public com.navdy.hud.app.framework.destinations.Destination$Builder distanceStr(String s) {
        this.distanceStr = s;
        return this;
    }
    
    public com.navdy.hud.app.framework.destinations.Destination$Builder favoriteDestinationType(com.navdy.hud.app.framework.destinations.Destination$FavoriteDestinationType a) {
        this.favoriteDestinationType = a;
        return this;
    }
    
    public com.navdy.hud.app.framework.destinations.Destination$Builder fullAddress(String s) {
        this.fullAddress = s;
        return this;
    }
    
    public com.navdy.hud.app.framework.destinations.Destination$Builder identifier(String s) {
        this.identifier = s;
        return this;
    }
    
    public com.navdy.hud.app.framework.destinations.Destination$Builder initials(String s) {
        this.initials = s;
        return this;
    }
    
    public com.navdy.hud.app.framework.destinations.Destination$Builder navigationPositionLatitude(double d) {
        this.navigationPositionLatitude = d;
        return this;
    }
    
    public com.navdy.hud.app.framework.destinations.Destination$Builder navigationPositionLongitude(double d) {
        this.navigationPositionLongitude = d;
        return this;
    }
    
    public com.navdy.hud.app.framework.destinations.Destination$Builder phoneNumbers(java.util.List a) {
        this.phoneNumbers = a;
        return this;
    }
    
    public com.navdy.hud.app.framework.destinations.Destination$Builder placeCategory(com.navdy.hud.app.framework.destinations.Destination$PlaceCategory a) {
        this.placeCategory = a;
        return this;
    }
    
    public com.navdy.hud.app.framework.destinations.Destination$Builder placeType(com.navdy.service.library.events.places.PlaceType a) {
        this.placeType = a;
        return this;
    }
    
    public com.navdy.hud.app.framework.destinations.Destination$Builder recentTimeLabel(String s) {
        this.recentTimeLabel = s;
        return this;
    }
    
    public com.navdy.hud.app.framework.destinations.Destination$Builder recentTimeLabelColor(int i) {
        this.recentTimeLabelColor = i;
        return this;
    }
    
    public com.navdy.hud.app.framework.destinations.Destination$Builder recommendation(boolean b) {
        this.recommendation = b;
        return this;
    }
}
