package com.navdy.hud.app.framework.fuel;

class FuelRoutingManager$4 implements com.navdy.hud.app.maps.here.HerePlacesManager$OnCategoriesSearchListener {
    final com.navdy.hud.app.framework.fuel.FuelRoutingManager this$0;
    final com.here.android.mpa.common.GeoCoordinate val$gasCalculationCoords;
    final com.navdy.hud.app.framework.fuel.FuelRoutingManager$OnRouteToGasStationCallback val$onRouteToGasStationCallback;
    
    FuelRoutingManager$4(com.navdy.hud.app.framework.fuel.FuelRoutingManager a, com.navdy.hud.app.framework.fuel.FuelRoutingManager$OnRouteToGasStationCallback a0, com.here.android.mpa.common.GeoCoordinate a1) {
        super();
        this.this$0 = a;
        this.val$onRouteToGasStationCallback = a0;
        this.val$gasCalculationCoords = a1;
    }
    
    public void onCompleted(java.util.List a) {
        try {
            com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
            if (com.navdy.hud.app.framework.fuel.FuelRoutingManager.access$600(this.this$0)) {
                if (a.size() > 1) {
                    java.util.Collections.sort(a, (java.util.Comparator)new com.navdy.hud.app.framework.fuel.FuelRoutingManager$4$1(this));
                }
                java.util.Iterator a0 = a.iterator();
                Object a1 = a;
                Object a2 = a0;
                while(((java.util.Iterator)a2).hasNext()) {
                    com.here.android.mpa.search.Place a3 = (com.here.android.mpa.search.Place)((java.util.Iterator)a2).next();
                    com.here.android.mpa.search.Location a4 = a3.getLocation();
                    com.here.android.mpa.common.GeoCoordinate a5 = com.navdy.hud.app.maps.here.HerePlacesManager.getPlaceEntry(a3);
                    String s = a4.getAddress().toString().replace((CharSequence)"<br/>", (CharSequence)", ").replace((CharSequence)"\n", (CharSequence)"");
                    com.here.android.mpa.common.GeoCoordinate a6 = a4.getCoordinate();
                    com.navdy.hud.app.framework.fuel.FuelRoutingManager.access$000().v(new StringBuilder().append("gas station name [").append(a3.getName()).append("]").append(" address [").append(s).append("]").append(" distance [").append((int)this.val$gasCalculationCoords.distanceTo(a5)).append("] meters").append(" displayPos [").append(a6).append("]").append(" navPos [").append(a5).append("]").toString());
                }
                java.util.LinkedList a7 = new java.util.LinkedList();
                java.util.ArrayList a8 = new java.util.ArrayList(3);
                Object a9 = ((java.util.List)a1).iterator();
                while(((java.util.Iterator)a9).hasNext()) {
                    com.here.android.mpa.search.Place a10 = (com.here.android.mpa.search.Place)((java.util.Iterator)a9).next();
                    java.util.ArrayList a11 = new java.util.ArrayList();
                    com.here.android.mpa.common.GeoCoordinate a12 = com.navdy.hud.app.maps.here.HerePlacesManager.getPlaceEntry(a10);
                    if (com.navdy.hud.app.framework.fuel.FuelRoutingManager.access$300(this.this$0).isNavigationModeOn()) {
                        ((java.util.List)a11).add(a12);
                        a12 = com.navdy.hud.app.framework.fuel.FuelRoutingManager.access$300(this.this$0).getCurrentRoute().getDestination();
                    }
                    ((java.util.Queue)a7).add(new com.navdy.hud.app.framework.fuel.FuelRoutingManager$FuelRoutingAction(this.this$0, ((java.util.List)a1).size(), a10, (java.util.Queue)a7, (java.util.List)a8, this.val$onRouteToGasStationCallback, this.val$gasCalculationCoords, (java.util.List)a11, a12));
                }
                com.navdy.hud.app.framework.fuel.FuelRoutingManager.access$000().v("posting to handler");
                com.navdy.hud.app.framework.fuel.FuelRoutingManager.access$1300(this.this$0).post((Runnable)((java.util.Queue)a7).poll());
            } else {
                this.val$onRouteToGasStationCallback.onError(com.navdy.hud.app.framework.fuel.FuelRoutingManager$OnRouteToGasStationCallback$Error.INVALID_STATE);
            }
        } catch(Throwable a13) {
            com.navdy.hud.app.framework.fuel.FuelRoutingManager.access$000().e("calculateGasStationRoutes", a13);
            this.val$onRouteToGasStationCallback.onError(com.navdy.hud.app.framework.fuel.FuelRoutingManager$OnRouteToGasStationCallback$Error.UNKNOWN_ERROR);
        }
    }
    
    public void onError(com.navdy.hud.app.maps.here.HerePlacesManager$Error a) {
        com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
        com.navdy.hud.app.framework.fuel.FuelRoutingManager.access$000().e(new StringBuilder().append("calculateGasStationRoutes error:").append(a).toString());
        this.val$onRouteToGasStationCallback.onError(com.navdy.hud.app.framework.fuel.FuelRoutingManager$OnRouteToGasStationCallback$Error.RESPONSE_ERROR);
    }
}
