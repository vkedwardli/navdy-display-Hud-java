package com.navdy.hud.app.framework.trips;

public class TripManager {
    final public static int MAX_SPEED_METERS_PER_SECOND_THRESHOLD = 200;
    final private static long MIN_TIME_THRESHOLD = 1000L;
    final public static String PREFERENCE_TOTAL_DISTANCE_TRAVELLED_WITH_NAVDY = "trip_manager_total_distance_travelled_with_navdy";
    final public static long TOTAL_DISTANCE_PERSIST_INTERVAL = 30000L;
    final private static com.navdy.service.library.log.Logger sLogger;
    final private com.squareup.otto.Bus bus;
    private int currentDistanceTraveledInMeters;
    private int currentSequenceNumber;
    private volatile com.navdy.hud.app.framework.trips.TripManager$State currentState;
    private long currentTripNumber;
    private long currentTripStartTime;
    private boolean isClientConnected;
    private com.here.android.mpa.common.GeoCoordinate lastCoords;
    private android.location.Location lastLocation;
    private long lastTotalDistancePersistTime;
    private long lastTotalDistancePersistedValue;
    private long lastTrackingTime;
    private com.navdy.service.library.events.TripUpdate$Builder lastTripUpdateBuilder;
    private StringBuilder logBuilder;
    android.content.SharedPreferences preferences;
    final private com.navdy.hud.app.manager.SpeedManager speedManager;
    private int totalDistanceTravelledInMeters;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.framework.trips.TripManager.class);
    }
    
    public TripManager(com.squareup.otto.Bus a, android.content.SharedPreferences a0) {
        this.lastTotalDistancePersistTime = 0L;
        this.lastTotalDistancePersistedValue = 0L;
        this.bus = a;
        this.speedManager = com.navdy.hud.app.manager.SpeedManager.getInstance();
        this.preferences = a0;
        this.currentState = com.navdy.hud.app.framework.trips.TripManager$State.STOPPED;
        this.logBuilder = new StringBuilder();
        this.isClientConnected = false;
        this.totalDistanceTravelledInMeters = 0;
    }
    
    static com.navdy.hud.app.framework.trips.TripManager$State access$000(com.navdy.hud.app.framework.trips.TripManager a) {
        return a.currentState;
    }
    
    static com.navdy.hud.app.framework.trips.TripManager$State access$002(com.navdy.hud.app.framework.trips.TripManager a, com.navdy.hud.app.framework.trips.TripManager$State a0) {
        a.currentState = a0;
        return a0;
    }
    
    static void access$100(com.navdy.hud.app.framework.trips.TripManager a) {
        a.createNewTrip();
    }
    
    static boolean access$1000(com.navdy.hud.app.framework.trips.TripManager a) {
        return a.isClientConnected;
    }
    
    static int access$1100(com.navdy.hud.app.framework.trips.TripManager a) {
        return a.currentSequenceNumber;
    }
    
    static int access$1102(com.navdy.hud.app.framework.trips.TripManager a, int i) {
        a.currentSequenceNumber = i;
        return i;
    }
    
    static android.location.Location access$1200(com.navdy.hud.app.framework.trips.TripManager a) {
        return a.lastLocation;
    }
    
    static com.navdy.service.library.events.location.Coordinate access$1300(com.navdy.hud.app.framework.trips.TripManager a, android.location.Location a0) {
        return a.toCoordinate(a0);
    }
    
    static com.navdy.hud.app.manager.SpeedManager access$1400(com.navdy.hud.app.framework.trips.TripManager a) {
        return a.speedManager;
    }
    
    static long access$1500(com.navdy.hud.app.framework.trips.TripManager a) {
        return a.currentTripNumber;
    }
    
    static StringBuilder access$1600(com.navdy.hud.app.framework.trips.TripManager a) {
        return a.logBuilder;
    }
    
    static com.navdy.service.library.events.TripUpdate$Builder access$2000(com.navdy.hud.app.framework.trips.TripManager a) {
        return a.lastTripUpdateBuilder;
    }
    
    static com.navdy.service.library.events.TripUpdate$Builder access$2002(com.navdy.hud.app.framework.trips.TripManager a, com.navdy.service.library.events.TripUpdate$Builder a0) {
        a.lastTripUpdateBuilder = a0;
        return a0;
    }
    
    static com.squareup.otto.Bus access$2100(com.navdy.hud.app.framework.trips.TripManager a) {
        return a.bus;
    }
    
    static com.navdy.service.library.log.Logger access$300() {
        return sLogger;
    }
    
    static com.here.android.mpa.common.GeoCoordinate access$400(com.navdy.hud.app.framework.trips.TripManager a) {
        return a.lastCoords;
    }
    
    static com.here.android.mpa.common.GeoCoordinate access$402(com.navdy.hud.app.framework.trips.TripManager a, com.here.android.mpa.common.GeoCoordinate a0) {
        a.lastCoords = a0;
        return a0;
    }
    
    static long access$500(com.navdy.hud.app.framework.trips.TripManager a) {
        return a.lastTrackingTime;
    }
    
    static long access$502(com.navdy.hud.app.framework.trips.TripManager a, long j) {
        a.lastTrackingTime = j;
        return j;
    }
    
    static int access$600(com.navdy.hud.app.framework.trips.TripManager a) {
        return a.currentDistanceTraveledInMeters;
    }
    
    static int access$602(com.navdy.hud.app.framework.trips.TripManager a, int i) {
        a.currentDistanceTraveledInMeters = i;
        return i;
    }
    
    static int access$700(com.navdy.hud.app.framework.trips.TripManager a) {
        return a.totalDistanceTravelledInMeters;
    }
    
    static int access$702(com.navdy.hud.app.framework.trips.TripManager a, int i) {
        a.totalDistanceTravelledInMeters = i;
        return i;
    }
    
    static long access$800(com.navdy.hud.app.framework.trips.TripManager a) {
        return a.lastTotalDistancePersistTime;
    }
    
    static long access$802(com.navdy.hud.app.framework.trips.TripManager a, long j) {
        a.lastTotalDistancePersistTime = j;
        return j;
    }
    
    static long access$902(com.navdy.hud.app.framework.trips.TripManager a, long j) {
        a.lastTotalDistancePersistedValue = j;
        return j;
    }
    
    private void createNewTrip() {
        com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
        this.currentTripNumber = System.currentTimeMillis();
        this.currentSequenceNumber = 0;
        this.currentDistanceTraveledInMeters = 0;
        this.currentTripStartTime = System.currentTimeMillis();
    }
    
    private com.navdy.service.library.events.location.Coordinate toCoordinate(android.location.Location a) {
        return (a != null) ? new com.navdy.service.library.events.location.Coordinate$Builder().accuracy(Float.valueOf(a.getAccuracy())).altitude(Double.valueOf(a.getAltitude())).bearing(Float.valueOf(a.getBearing())).latitude(Double.valueOf(a.getLatitude())).longitude(Double.valueOf(a.getLongitude())).provider(a.getProvider()).speed(Float.valueOf(a.getSpeed())).timestamp(Long.valueOf(a.getTime())).build() : null;
    }
    
    public int getCurrentDistanceTraveled() {
        return this.currentDistanceTraveledInMeters;
    }
    
    public long getCurrentTripStartTime() {
        return this.currentTripStartTime;
    }
    
    public int getTotalDistanceTravelled() {
        return this.totalDistanceTravelledInMeters;
    }
    
    public long getTotalDistanceTravelledWithNavdy() {
        return this.preferences.getLong("trip_manager_total_distance_travelled_with_navdy", 0L) + ((long)this.totalDistanceTravelledInMeters - this.lastTotalDistancePersistedValue);
    }
    
    public void onConnectionStateChange(com.navdy.service.library.events.connection.ConnectionStateChange a) {
        if (a.state != com.navdy.service.library.events.connection.ConnectionStateChange$ConnectionState.CONNECTION_DISCONNECTED) {
            if (a.state == com.navdy.service.library.events.connection.ConnectionStateChange$ConnectionState.CONNECTION_VERIFIED) {
                this.isClientConnected = true;
            }
        } else {
            this.isClientConnected = false;
        }
    }
    
    public void onFinishedTripRouteEvent(com.navdy.hud.app.framework.trips.TripManager$FinishedTripRouteEvent a) {
        com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.framework.trips.TripManager$3(this), 8);
    }
    
    public void onLocation(android.location.Location a) {
        this.lastLocation = a;
    }
    
    public void onNewTrip(com.navdy.hud.app.framework.trips.TripManager$NewTripEvent a) {
        com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.framework.trips.TripManager$2(this), 8);
    }
    
    public void onTrack(com.navdy.hud.app.framework.trips.TripManager$TrackingEvent a) {
        com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.framework.trips.TripManager$1(this, a), 8);
    }
}
