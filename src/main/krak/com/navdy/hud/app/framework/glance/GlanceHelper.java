package com.navdy.hud.app.framework.glance;
import com.navdy.hud.app.R;

public class GlanceHelper {
    final private static java.util.HashMap APP_ID_MAP;
    final public static String DELETE_ALL_GLANCES_TOAST_ID = "glance-deleted";
    final public static String FUEL_PACKAGE = "com.navdy.fuel";
    final private static java.util.concurrent.atomic.AtomicLong ID;
    final public static String IOS_PHONE_PACKAGE = "com.apple.mobilephone";
    final public static String MICROSOFT_OUTLOOK = "com.microsoft.Office.Outlook";
    final public static String MUSIC_PACKAGE = "com.navdy.music";
    final public static String PHONE_PACKAGE = "com.navdy.phone";
    final public static String SMS_PACKAGE = "com.navdy.sms";
    final public static String TRAFFIC_PACKAGE = "com.navdy.traffic";
    final public static String VOICE_SEARCH_PACKAGE = "com.navdy.voice.search";
    final private static StringBuilder builder;
    final private static com.navdy.hud.app.gesture.GestureServiceConnector gestureServiceConnector;
    final private static android.content.res.Resources resources;
    final private static com.navdy.service.library.log.Logger sLogger;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.framework.glance.GlanceHelper.class);
        ID = new java.util.concurrent.atomic.AtomicLong(1L);
        builder = new StringBuilder();
        APP_ID_MAP = new java.util.HashMap();
        APP_ID_MAP.put("com.navdy.fuel", com.navdy.hud.app.framework.glance.GlanceApp.FUEL);
        APP_ID_MAP.put("com.google.android.calendar", com.navdy.hud.app.framework.glance.GlanceApp.GOOGLE_CALENDAR);
        APP_ID_MAP.put("com.google.android.gm", com.navdy.hud.app.framework.glance.GlanceApp.GOOGLE_MAIL);
        APP_ID_MAP.put("com.google.android.talk", com.navdy.hud.app.framework.glance.GlanceApp.GOOGLE_HANGOUT);
        APP_ID_MAP.put("com.Slack", com.navdy.hud.app.framework.glance.GlanceApp.SLACK);
        APP_ID_MAP.put("com.whatsapp", com.navdy.hud.app.framework.glance.GlanceApp.WHATS_APP);
        APP_ID_MAP.put("com.facebook.orca", com.navdy.hud.app.framework.glance.GlanceApp.FACEBOOK_MESSENGER);
        APP_ID_MAP.put("com.facebook.katana", com.navdy.hud.app.framework.glance.GlanceApp.FACEBOOK);
        APP_ID_MAP.put("com.twitter.android", com.navdy.hud.app.framework.glance.GlanceApp.TWITTER);
        APP_ID_MAP.put("com.navdy.sms", com.navdy.hud.app.framework.glance.GlanceApp.SMS);
        APP_ID_MAP.put("com.google.android.apps.inbox", com.navdy.hud.app.framework.glance.GlanceApp.GOOGLE_INBOX);
        APP_ID_MAP.put("com.google.calendar", com.navdy.hud.app.framework.glance.GlanceApp.GOOGLE_CALENDAR);
        APP_ID_MAP.put("com.google.Gmail", com.navdy.hud.app.framework.glance.GlanceApp.GOOGLE_MAIL);
        APP_ID_MAP.put("com.google.hangouts", com.navdy.hud.app.framework.glance.GlanceApp.GOOGLE_HANGOUT);
        APP_ID_MAP.put("com.tinyspeck.chatlyio", com.navdy.hud.app.framework.glance.GlanceApp.SLACK);
        APP_ID_MAP.put("net.whatsapp.WhatsApp", com.navdy.hud.app.framework.glance.GlanceApp.WHATS_APP);
        APP_ID_MAP.put("com.facebook.Messenger", com.navdy.hud.app.framework.glance.GlanceApp.FACEBOOK_MESSENGER);
        APP_ID_MAP.put("com.facebook.Facebook", com.navdy.hud.app.framework.glance.GlanceApp.FACEBOOK);
        APP_ID_MAP.put("com.atebits.Tweetie2", com.navdy.hud.app.framework.glance.GlanceApp.TWITTER);
        APP_ID_MAP.put("com.apple.MobileSMS", com.navdy.hud.app.framework.glance.GlanceApp.IMESSAGE);
        APP_ID_MAP.put("com.apple.mobilemail", com.navdy.hud.app.framework.glance.GlanceApp.APPLE_MAIL);
        APP_ID_MAP.put("com.apple.mobilecal", com.navdy.hud.app.framework.glance.GlanceApp.APPLE_CALENDAR);
        APP_ID_MAP.put("com.microsoft.Office.Outlook", com.navdy.hud.app.framework.glance.GlanceApp.GENERIC_MAIL);
        resources = com.navdy.hud.app.HudApplication.getAppContext().getResources();
        gestureServiceConnector = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getGestureServiceConnector();
    }
    
    public GlanceHelper() {
    }
    
    public static java.util.Map buildDataMap(com.navdy.service.library.events.glances.GlanceEvent a) {
        java.util.HashMap a0 = new java.util.HashMap();
        if (a.glanceData != null) {
            Object a1 = a.glanceData.iterator();
            while(((java.util.Iterator)a1).hasNext()) {
                com.navdy.service.library.events.glances.KeyValue a2 = (com.navdy.service.library.events.glances.KeyValue)((java.util.Iterator)a1).next();
                ((java.util.Map)a0).put(a2.key, com.navdy.hud.app.framework.contacts.ContactUtil.sanitizeString(a2.value));
            }
        }
        return (java.util.Map)a0;
    }
    
    public static String getCalendarTime(long j, StringBuilder a, StringBuilder a0, com.navdy.hud.app.common.TimeHelper a1) {
        String s = null;
        long j0 = (long)Math.round((float)(j - System.currentTimeMillis()) / 60000f);
        a.setLength(0);
        a0.setLength(0);
        int i = (j0 < 60L) ? -1 : (j0 == 60L) ? 0 : 1;
        label2: {
            label0: {
                label1: {
                    if (i >= 0) {
                        break label1;
                    }
                    if (j0 >= -5L) {
                        break label0;
                    }
                }
                s = a1.formatTime12Hour(new java.util.Date(j), a0, true);
                break label2;
            }
            if (j0 > 0L) {
                s = String.valueOf(j0);
                a.append(com.navdy.hud.app.framework.glance.GlanceConstants.minute);
            } else {
                s = com.navdy.hud.app.framework.glance.GlanceConstants.nowStr;
            }
        }
        return s;
    }
    
    public static String getFrom(com.navdy.hud.app.framework.glance.GlanceApp a, java.util.Map a0) {
        String s = null;
        if (a0 != null) {
            switch(com.navdy.hud.app.framework.glance.GlanceHelper$1.$SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp[a.ordinal()]) {
                case 1: case 9: case 17: {
                    s = (String)a0.get(com.navdy.service.library.events.glances.MessageConstants.MESSAGE_FROM.name());
                    break;
                }
                default: {
                    s = null;
                }
            }
        } else {
            s = null;
        }
        return s;
    }
    
    public static com.navdy.hud.app.gesture.GestureServiceConnector getGestureService() {
        return gestureServiceConnector;
    }
    
    public static String getGlanceMessage(com.navdy.hud.app.framework.glance.GlanceApp a, java.util.Map a0) {
        String s = null;
        synchronized(com.navdy.hud.app.framework.glance.GlanceHelper.class) {
            int[] a1 = com.navdy.hud.app.framework.glance.GlanceHelper$1.$SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp;
            switch(a1[a.ordinal()]) {
                case 19: {
                    String s0 = (String)a0.get(com.navdy.service.library.events.glances.GenericConstants.GENERIC_MESSAGE.name());
                    if (s0 == null) {
                        break;
                    }
                    builder.append(s0);
                    break;
                }
                case 13: case 14: case 15: case 16: {
                    String s1 = (String)a0.get(com.navdy.service.library.events.glances.EmailConstants.EMAIL_BODY.name());
                    if (s1 == null) {
                        break;
                    }
                    builder.append(s1);
                    break;
                }
                case 6: case 7: case 8: {
                    String s2 = (String)a0.get(com.navdy.service.library.events.glances.SocialConstants.SOCIAL_MESSAGE.name());
                    if (s2 == null) {
                        break;
                    }
                    builder.append(s2);
                    break;
                }
                case 1: case 2: case 3: case 4: case 5: case 9: case 17: {
                    String s3 = (String)a0.get(com.navdy.service.library.events.glances.MessageConstants.MESSAGE_BODY.name());
                    if (s3 == null) {
                        break;
                    }
                    builder.append(s3);
                    break;
                }
            }
            s = builder.toString();
            builder.setLength(0);
        }
        /*monexit(com.navdy.hud.app.framework.glance.GlanceHelper.class)*/;
        return s;
    }
    
    public static com.navdy.hud.app.framework.glance.GlanceApp getGlancesApp(com.navdy.service.library.events.glances.GlanceEvent a) {
        com.navdy.hud.app.framework.glance.GlanceApp a0 = null;
        if (a != null) {
            a0 = (com.navdy.hud.app.framework.glance.GlanceApp)APP_ID_MAP.get(a.provider);
            if (a0 == null) {
                switch(com.navdy.hud.app.framework.glance.GlanceHelper$1.$SwitchMap$com$navdy$service$library$events$glances$GlanceEvent$GlanceType[a.glanceType.ordinal()]) {
                    case 4: {
                        a0 = com.navdy.hud.app.framework.glance.GlanceApp.GENERIC_SOCIAL;
                        break;
                    }
                    case 3: {
                        a0 = com.navdy.hud.app.framework.glance.GlanceApp.GENERIC_MESSAGE;
                        break;
                    }
                    case 2: {
                        a0 = com.navdy.hud.app.framework.glance.GlanceApp.GENERIC_CALENDAR;
                        break;
                    }
                    case 1: {
                        a0 = com.navdy.hud.app.framework.glance.GlanceApp.GENERIC_MAIL;
                        break;
                    }
                    default: {
                        a0 = null;
                    }
                }
            }
        } else {
            a0 = null;
        }
        return a0;
    }
    
    public static com.navdy.hud.app.framework.glance.GlanceApp getGlancesApp(String s) {
        return (com.navdy.hud.app.framework.glance.GlanceApp)APP_ID_MAP.get(s);
    }
    
    public static int getIcon(String s) {
        int i = 0;
        if (android.text.TextUtils.isEmpty((CharSequence)s)) {
            i = -1;
        } else {
            int i0 = 0;
            int i1 = s.hashCode();
            label0: {
                switch(i1) {
                    case -9470533: {
                        if (!s.equals("GLANCE_ICON_NAVDY_MAIN")) {
                            break;
                        }
                        i0 = 0;
                        break label0;
                    }
                    case -30680433: {
                        if (!s.equals("GLANCE_ICON_MESSAGE_SIDE_BLUE")) {
                            break;
                        }
                        i0 = 1;
                        break label0;
                    }
                }
                i0 = -1;
            }
            switch(i0) {
                case 1: {
                    i = R.drawable.icon_message_blue;
                    break;
                }
                case 0: {
                    i = R.drawable.icon_user_navdy;
                    break;
                }
                default: {
                    i = -1;
                }
            }
        }
        return i;
    }
    
    public static String getId() {
        return String.valueOf(ID.getAndIncrement());
    }
    
    public static com.navdy.hud.app.framework.glance.GlanceViewCache$ViewType getLargeViewType(com.navdy.hud.app.framework.glance.GlanceApp a) {
        com.navdy.hud.app.framework.glance.GlanceViewCache$ViewType a0 = null;
        switch(com.navdy.hud.app.framework.glance.GlanceHelper$1.$SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp[a.ordinal()]) {
            case 18: {
                a0 = com.navdy.hud.app.framework.glance.GlanceViewCache$ViewType.BIG_MULTI_TEXT;
                break;
            }
            case 10: case 11: case 12: {
                a0 = com.navdy.hud.app.framework.glance.GlanceViewCache$ViewType.BIG_CALENDAR;
                break;
            }
            default: {
                a0 = com.navdy.hud.app.framework.glance.GlanceViewCache$ViewType.BIG_GLANCE_MESSAGE_SINGLE;
            }
        }
        return a0;
    }
    
    public static String getNotificationId(com.navdy.service.library.events.glances.GlanceEvent$GlanceType a, String s) {
        String s0 = null;
        switch(com.navdy.hud.app.framework.glance.GlanceHelper$1.$SwitchMap$com$navdy$service$library$events$glances$GlanceEvent$GlanceType[a.ordinal()]) {
            case 5: {
                s0 = "glance_fuel_";
                break;
            }
            case 4: {
                s0 = "glance_soc_";
                break;
            }
            case 3: {
                s0 = "glance_msg_";
                break;
            }
            case 2: {
                s0 = "glance_cal_";
                break;
            }
            case 1: {
                s0 = "glance_email_";
                break;
            }
            default: {
                s0 = "glance_gen_";
            }
        }
        return new StringBuilder().append(s0).append(s).toString();
    }
    
    public static String getNotificationId(com.navdy.service.library.events.glances.GlanceEvent a) {
        return com.navdy.hud.app.framework.glance.GlanceHelper.getNotificationId(a.glanceType, a.id);
    }
    
    public static String getNumber(com.navdy.hud.app.framework.glance.GlanceApp a, java.util.Map a0) {
        String s = null;
        if (a0 != null) {
            switch(com.navdy.hud.app.framework.glance.GlanceHelper$1.$SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp[a.ordinal()]) {
                case 1: case 9: case 17: {
                    s = (String)a0.get(com.navdy.service.library.events.glances.MessageConstants.MESSAGE_FROM_NUMBER.name());
                    break;
                }
                default: {
                    s = null;
                }
            }
        } else {
            s = null;
        }
        return s;
    }
    
    public static com.navdy.hud.app.framework.glance.GlanceViewCache$ViewType getSmallViewType(com.navdy.hud.app.framework.glance.GlanceApp a) {
        com.navdy.hud.app.framework.glance.GlanceViewCache$ViewType a0 = null;
        switch(com.navdy.hud.app.framework.glance.GlanceHelper$1.$SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp[a.ordinal()]) {
            case 10: case 11: case 12: {
                a0 = com.navdy.hud.app.framework.glance.GlanceViewCache$ViewType.SMALL_SIGN;
                break;
            }
            default: {
                a0 = com.navdy.hud.app.framework.glance.GlanceViewCache$ViewType.SMALL_GLANCE_MESSAGE;
            }
        }
        return a0;
    }
    
    public static String getSubTitle(com.navdy.hud.app.framework.glance.GlanceApp a, java.util.Map a0) {
        String s = null;
        if (a0 != null) {
            switch(com.navdy.hud.app.framework.glance.GlanceHelper$1.$SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp[a.ordinal()]) {
                case 19: {
                    s = (String)a0.get(com.navdy.service.library.events.glances.GenericConstants.GENERIC_TITLE.name());
                    break;
                }
                case 18: {
                    s = (a0.get(com.navdy.service.library.events.glances.FuelConstants.NO_ROUTE.name()) != null) ? "" : com.navdy.hud.app.framework.glance.GlanceConstants.fuelNotificationSubtitle;
                    break;
                }
                case 13: case 14: case 15: case 16: {
                    s = (String)a0.get(com.navdy.service.library.events.glances.EmailConstants.EMAIL_SUBJECT.name());
                    break;
                }
                case 10: case 11: case 12: {
                    s = (String)a0.get(com.navdy.service.library.events.glances.CalendarConstants.CALENDAR_TIME_STR.name());
                    break;
                }
                case 2: {
                    s = (String)a0.get(com.navdy.service.library.events.glances.MessageConstants.MESSAGE_DOMAIN.name());
                    if (s == null) {
                        break;
                    }
                    android.content.res.Resources a1 = resources;
                    Object[] a2 = new Object[1];
                    a2[0] = s;
                    s = a1.getString(R.string.slack_team, a2);
                    break;
                }
                default: {
                    s = "";
                    break;
                }
                case 1: case 3: case 4: case 5: case 6: case 7: case 8: case 9: case 17: {
                    s = "";
                }
            }
            if (s == null) {
                s = "";
            }
        } else {
            s = "";
        }
        return s;
    }
    
    public static String getTimeStr(long j, java.util.Date a, com.navdy.hud.app.common.TimeHelper a0) {
        return (j - a.getTime() > 60000L) ? a0.formatTime(a, (StringBuilder)null) : com.navdy.hud.app.framework.glance.GlanceConstants.nowStr;
    }
    
    public static String getTitle(com.navdy.hud.app.framework.glance.GlanceApp a, java.util.Map a0) {
        String s = null;
        if (a0 != null) {
            switch(com.navdy.hud.app.framework.glance.GlanceHelper$1.$SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp[a.ordinal()]) {
                case 18: {
                    if ((String)a0.get(com.navdy.service.library.events.glances.FuelConstants.FUEL_LEVEL.name()) == null) {
                        s = resources.getString(R.string.gas_station);
                        break;
                    } else {
                        Object[] a1 = new Object[2];
                        a1[0] = com.navdy.hud.app.framework.glance.GlanceConstants.fuelNotificationTitle;
                        a1[1] = a0.get(com.navdy.service.library.events.glances.FuelConstants.FUEL_LEVEL.name());
                        s = String.format("%s %s%%", a1);
                        break;
                    }
                }
                case 17: {
                    s = (String)a0.get(com.navdy.service.library.events.glances.MessageConstants.MESSAGE_FROM.name());
                    if (s != null) {
                        break;
                    }
                    s = (String)a0.get(com.navdy.service.library.events.glances.MessageConstants.MESSAGE_FROM_NUMBER.name());
                    break;
                }
                case 13: case 14: case 15: case 16: {
                    s = (String)a0.get(com.navdy.service.library.events.glances.EmailConstants.EMAIL_FROM_NAME.name());
                    if (s != null) {
                        break;
                    }
                    s = (String)a0.get(com.navdy.service.library.events.glances.EmailConstants.EMAIL_FROM_EMAIL.name());
                    break;
                }
                case 10: case 11: case 12: {
                    s = (String)a0.get(com.navdy.service.library.events.glances.CalendarConstants.CALENDAR_TITLE.name());
                    break;
                }
                case 9: {
                    s = (String)a0.get(com.navdy.service.library.events.glances.MessageConstants.MESSAGE_FROM.name());
                    if (s != null) {
                        break;
                    }
                    s = (String)a0.get(com.navdy.service.library.events.glances.MessageConstants.MESSAGE_FROM_NUMBER.name());
                    break;
                }
                case 7: case 8: {
                    s = (String)a0.get(com.navdy.service.library.events.glances.SocialConstants.SOCIAL_FROM.name());
                    break;
                }
                case 6: {
                    s = (String)a0.get(com.navdy.service.library.events.glances.SocialConstants.SOCIAL_FROM.name());
                    if (!android.text.TextUtils.isEmpty((CharSequence)s)) {
                        break;
                    }
                    s = com.navdy.hud.app.framework.glance.GlanceConstants.facebook;
                    break;
                }
                case 2: case 3: case 4: case 5: {
                    s = (String)a0.get(com.navdy.service.library.events.glances.MessageConstants.MESSAGE_FROM.name());
                    break;
                }
                case 1: {
                    s = (String)a0.get(com.navdy.service.library.events.glances.MessageConstants.MESSAGE_FROM.name());
                    break;
                }
                default: {
                    s = com.navdy.hud.app.framework.glance.GlanceConstants.notification;
                }
            }
            if (s == null) {
                s = "";
            }
        } else {
            s = "";
        }
        return s;
    }
    
    public static String getTtsMessage(com.navdy.hud.app.framework.glance.GlanceApp a, java.util.Map a0, StringBuilder a1, StringBuilder a2, com.navdy.hud.app.common.TimeHelper a3) {
        String s = null;
        synchronized(com.navdy.hud.app.framework.glance.GlanceHelper.class) {
            int[] a4 = com.navdy.hud.app.framework.glance.GlanceHelper$1.$SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp;
            switch(a4[a.ordinal()]) {
                case 19: {
                    String s0 = (String)a0.get(com.navdy.service.library.events.glances.GenericConstants.GENERIC_MESSAGE.name());
                    if (s0 == null) {
                        break;
                    }
                    builder.append(s0);
                    break;
                }
                case 18: {
                    if (a0.containsKey(com.navdy.service.library.events.glances.FuelConstants.FUEL_LEVEL.name())) {
                        builder.append(resources.getString(R.string.your_fuel_level_is_low_tts));
                    }
                    StringBuilder a5 = builder;
                    android.content.res.Resources a6 = resources;
                    Object[] a7 = new Object[1];
                    a7[0] = a0.get(com.navdy.service.library.events.glances.FuelConstants.GAS_STATION_NAME.name());
                    a5.append(a6.getString(R.string.i_can_add_trip_gas_station, a7));
                    builder.append(".");
                    break;
                }
                case 13: case 14: case 15: case 16: {
                    String s1 = (String)a0.get(com.navdy.service.library.events.glances.EmailConstants.EMAIL_SUBJECT.name());
                    if (s1 != null) {
                        builder.append(s1);
                    }
                    String s2 = (String)a0.get(com.navdy.service.library.events.glances.EmailConstants.EMAIL_BODY.name());
                    if (s2 == null) {
                        break;
                    }
                    if (builder.length() > 0) {
                        builder.append(".");
                        builder.append(" ");
                    }
                    builder.append(s2);
                    break;
                }
                case 10: case 11: case 12: {
                    long j = 0L;
                    String s3 = (String)a0.get(com.navdy.service.library.events.glances.CalendarConstants.CALENDAR_LOCATION.name());
                    String s4 = (String)a0.get(com.navdy.service.library.events.glances.CalendarConstants.CALENDAR_TITLE.name());
                    String s5 = (String)a0.get(com.navdy.service.library.events.glances.CalendarConstants.CALENDAR_TIME.name());
                    label1: {
                        Throwable a8 = null;
                        if (s5 == null) {
                            j = 0L;
                            break label1;
                        } else {
                            try {
                                j = Long.parseLong(s5);
                                break label1;
                            } catch(Throwable a9) {
                                a8 = a9;
                            }
                        }
                        sLogger.e(a8);
                        j = 0L;
                    }
                    int i = (j < 0L) ? -1 : (j == 0L) ? 0 : 1;
                    String s6 = null;
                    label0: {
                        Throwable a10 = null;
                        if (i <= 0) {
                            break label0;
                        }
                        String s7 = com.navdy.hud.app.framework.glance.GlanceHelper.getCalendarTime(j, a1, a2, a3);
                        if (android.text.TextUtils.isEmpty((CharSequence)a1.toString())) {
                            String s8 = a2.toString();
                            if (android.text.TextUtils.isEmpty((CharSequence)s8)) {
                                s6 = new StringBuilder().append(s7).append(".").toString();
                                break label0;
                            } else {
                                String s9 = (android.text.TextUtils.equals((CharSequence)s8, (CharSequence)com.navdy.hud.app.framework.glance.GlanceConstants.pmMarker)) ? com.navdy.hud.app.framework.glance.GlanceConstants.pm : com.navdy.hud.app.framework.glance.GlanceConstants.am;
                                android.content.res.Resources a11 = resources;
                                Object[] a12 = new Object[2];
                                a12[0] = s7;
                                a12[1] = s9;
                                s6 = a11.getString(R.string.tts_calendar_time_at, a12);
                                break label0;
                            }
                        } else {
                            try {
                                if (Long.parseLong(s7) <= 1L) {
                                    android.content.res.Resources a13 = resources;
                                    Object[] a14 = new Object[1];
                                    a14[0] = s7;
                                    s6 = a13.getString(R.string.tts_calendar_time_min, a14);
                                    break label0;
                                } else {
                                    android.content.res.Resources a15 = resources;
                                    Object[] a16 = new Object[1];
                                    a16[0] = s7;
                                    s6 = a15.getString(R.string.tts_calendar_time_mins, a16);
                                    break label0;
                                }
                            } catch(Throwable a17) {
                                a10 = a17;
                            }
                        }
                        sLogger.e(a10);
                        s6 = null;
                    }
                    if (s4 != null) {
                        StringBuilder a18 = builder;
                        android.content.res.Resources a19 = resources;
                        Object[] a20 = new Object[1];
                        a20[0] = s4;
                        a18.append(a19.getString(R.string.tts_calendar_title, a20));
                    }
                    if (s6 != null) {
                        builder.append(" ");
                        builder.append(s6);
                    }
                    if (s3 == null) {
                        break;
                    }
                    builder.append(" ");
                    StringBuilder a21 = builder;
                    android.content.res.Resources a22 = resources;
                    Object[] a23 = new Object[1];
                    a23[0] = s3;
                    a21.append(a22.getString(R.string.tts_calendar_location, a23));
                    break;
                }
                case 6: case 7: case 8: {
                    String s10 = (String)a0.get(com.navdy.service.library.events.glances.SocialConstants.SOCIAL_MESSAGE.name());
                    if (s10 == null) {
                        break;
                    }
                    builder.append(s10);
                    break;
                }
                case 1: case 2: case 3: case 4: case 5: case 9: case 17: {
                    String s11 = (String)a0.get(com.navdy.service.library.events.glances.MessageConstants.MESSAGE_BODY.name());
                    if (s11 == null) {
                        break;
                    }
                    builder.append(s11);
                    break;
                }
            }
            s = builder.toString();
            builder.setLength(0);
        }
        /*monexit(com.navdy.hud.app.framework.glance.GlanceHelper.class)*/;
        return s;
    }
    
    public static void initMessageAttributes() {
        android.content.Context a = com.navdy.hud.app.HudApplication.getAppContext();
        android.widget.TextView a0 = new android.widget.TextView(a);
        a0.setTextAppearance(a, R.style.glance_message_title);
        android.text.StaticLayout a1 = new android.text.StaticLayout((CharSequence)"RockgOn", a0.getPaint(), com.navdy.hud.app.framework.glance.GlanceConstants.messageWidthBound, android.text.Layout$Alignment.ALIGN_NORMAL, 1f, 0.0f, false);
        int i = a1.getHeight() + (com.navdy.hud.app.framework.glance.GlanceConstants.messageTitleMarginTop + com.navdy.hud.app.framework.glance.GlanceConstants.messageTitleMarginBottom);
        sLogger.v(new StringBuilder().append("message-title-ht = ").append(i).append(" layout=").append(a1.getHeight()).toString());
        com.navdy.hud.app.framework.glance.GlanceConstants.setMessageTitleHeight(i);
    }
    
    public static boolean isCalendarApp(com.navdy.hud.app.framework.glance.GlanceApp a) {
        boolean b = false;
        switch(com.navdy.hud.app.framework.glance.GlanceHelper$1.$SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp[a.ordinal()]) {
            case 10: case 11: case 12: {
                b = true;
                break;
            }
            default: {
                b = false;
            }
        }
        return b;
    }
    
    public static boolean isDeleteAllView(android.view.View a) {
        boolean b = false;
        label2: {
            label0: {
                label1: {
                    if (a == null) {
                        break label1;
                    }
                    if (a.getTag() == com.navdy.hud.app.framework.glance.GlanceConstants.DELETE_ALL_VIEW_TAG) {
                        break label0;
                    }
                }
                b = false;
                break label2;
            }
            b = true;
        }
        return b;
    }
    
    public static boolean isFuelNotificationEnabled() {
        return com.navdy.hud.app.framework.glance.GlanceHelper.isPackageEnabled("com.navdy.fuel");
    }
    
    public static boolean isMusicNotificationEnabled() {
        return com.navdy.hud.app.framework.glance.GlanceHelper.isPackageEnabled("com.navdy.music");
    }
    
    public static boolean isPackageEnabled(String s) {
        boolean b = false;
        if (com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getCurrentProfile().getNotificationPreferences().enabled.booleanValue()) {
            com.navdy.hud.app.profile.NotificationSettings a = com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getCurrentProfile().getNotificationSettings();
            b = Boolean.TRUE.equals(a.enabled(s));
        } else {
            b = false;
        }
        return b;
    }
    
    public static boolean isPhoneNotificationEnabled() {
        boolean b = false;
        boolean b0 = com.navdy.hud.app.framework.glance.GlanceHelper.isPackageEnabled("com.navdy.phone");
        label2: {
            label0: {
                label1: {
                    if (b0) {
                        break label1;
                    }
                    if (!com.navdy.hud.app.framework.glance.GlanceHelper.isPackageEnabled("com.apple.mobilephone")) {
                        break label0;
                    }
                }
                b = true;
                break label2;
            }
            b = false;
        }
        return b;
    }
    
    public static boolean isPhotoCheckRequired(com.navdy.hud.app.framework.glance.GlanceApp a) {
        boolean b = false;
        switch(com.navdy.hud.app.framework.glance.GlanceHelper$1.$SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp[a.ordinal()]) {
            case 1: case 9: case 17: {
                b = true;
                break;
            }
            default: {
                b = false;
            }
        }
        return b;
    }
    
    public static boolean isTrafficNotificationEnabled() {
        return com.navdy.hud.app.framework.glance.GlanceHelper.isPackageEnabled("com.navdy.traffic");
    }
    
    public static boolean needsScrollLayout(String s) {
        boolean b = false;
        if (android.text.TextUtils.isEmpty((CharSequence)s)) {
            b = false;
        } else {
            android.content.Context a = com.navdy.hud.app.HudApplication.getAppContext();
            android.widget.TextView a0 = new android.widget.TextView(a);
            a0.setTextAppearance(a, R.style.glance_message_body);
            float f = (float)(new android.text.StaticLayout((CharSequence)s, a0.getPaint(), com.navdy.hud.app.framework.glance.GlanceConstants.messageWidthBound, android.text.Layout$Alignment.ALIGN_NORMAL, 1f, 0.0f, false).getHeight() / 2);
            float f0 = (float)(com.navdy.hud.app.framework.glance.GlanceConstants.messageHeightBound / 2) - f;
            int i = com.navdy.hud.app.framework.glance.GlanceConstants.getMessageTitleHeight();
            b = !(f0 > (float)i);
            sLogger.v(new StringBuilder().append("message-tittle-calc left[").append(f0).append("] titleHt [").append(i).append("]").toString());
        }
        return b;
    }
    
    public static void showGlancesDeletedToast() {
        android.os.Bundle a = new android.os.Bundle();
        a.putInt("13", 1000);
        a.putInt("8", R.drawable.icon_qm_glances_grey);
        a.putString("4", resources.getString(R.string.glances_deleted));
        a.putInt("5", R.style.Glances_1);
        com.navdy.hud.app.framework.toast.ToastManager a0 = com.navdy.hud.app.framework.toast.ToastManager.getInstance();
        a0.dismissCurrentToast();
        a0.clearAllPendingToast();
        a0.addToast(new com.navdy.hud.app.framework.toast.ToastManager$ToastParams("glance-deleted", a, (com.navdy.hud.app.framework.toast.IToastCallback)null, true, true));
    }
    
    public static boolean usesMessageLayout(com.navdy.hud.app.framework.glance.GlanceApp a) {
        boolean b = false;
        switch(com.navdy.hud.app.framework.glance.GlanceHelper$1.$SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp[a.ordinal()]) {
            case 1: case 2: case 3: case 4: case 5: case 6: case 7: case 8: case 9: case 13: case 14: case 15: case 16: case 17: case 19: {
                b = true;
                break;
            }
            default: {
                b = false;
            }
        }
        return b;
    }
}
