package com.navdy.hud.app.framework.notifications;


public enum NotificationAnimator$Operation {
    MOVE_PREV(0),
    MOVE_NEXT(1),
    SELECT(2),
    COLLAPSE_VIEW(3),
    UPDATE_INDICATOR(4),
    REMOVE_NOTIFICATION(5);

    private int value;
    NotificationAnimator$Operation(int value) {
        this.value = value;
    }
    public int getValue() {
        return value;
    }
}

//final public class NotificationAnimator$Operation extends Enum {
//    final private static com.navdy.hud.app.framework.notifications.NotificationAnimator$Operation[] $VALUES;
//    final public static com.navdy.hud.app.framework.notifications.NotificationAnimator$Operation COLLAPSE_VIEW;
//    final public static com.navdy.hud.app.framework.notifications.NotificationAnimator$Operation MOVE_NEXT;
//    final public static com.navdy.hud.app.framework.notifications.NotificationAnimator$Operation MOVE_PREV;
//    final public static com.navdy.hud.app.framework.notifications.NotificationAnimator$Operation REMOVE_NOTIFICATION;
//    final public static com.navdy.hud.app.framework.notifications.NotificationAnimator$Operation SELECT;
//    final public static com.navdy.hud.app.framework.notifications.NotificationAnimator$Operation UPDATE_INDICATOR;
//    
//    static {
//        MOVE_PREV = new com.navdy.hud.app.framework.notifications.NotificationAnimator$Operation("MOVE_PREV", 0);
//        MOVE_NEXT = new com.navdy.hud.app.framework.notifications.NotificationAnimator$Operation("MOVE_NEXT", 1);
//        SELECT = new com.navdy.hud.app.framework.notifications.NotificationAnimator$Operation("SELECT", 2);
//        COLLAPSE_VIEW = new com.navdy.hud.app.framework.notifications.NotificationAnimator$Operation("COLLAPSE_VIEW", 3);
//        UPDATE_INDICATOR = new com.navdy.hud.app.framework.notifications.NotificationAnimator$Operation("UPDATE_INDICATOR", 4);
//        REMOVE_NOTIFICATION = new com.navdy.hud.app.framework.notifications.NotificationAnimator$Operation("REMOVE_NOTIFICATION", 5);
//        com.navdy.hud.app.framework.notifications.NotificationAnimator$Operation[] a = new com.navdy.hud.app.framework.notifications.NotificationAnimator$Operation[6];
//        a[0] = MOVE_PREV;
//        a[1] = MOVE_NEXT;
//        a[2] = SELECT;
//        a[3] = COLLAPSE_VIEW;
//        a[4] = UPDATE_INDICATOR;
//        a[5] = REMOVE_NOTIFICATION;
//        $VALUES = a;
//    }
//    
//    private NotificationAnimator$Operation(String s, int i) {
//        super(s, i);
//    }
//    
//    public static com.navdy.hud.app.framework.notifications.NotificationAnimator$Operation valueOf(String s) {
//        return (com.navdy.hud.app.framework.notifications.NotificationAnimator$Operation)Enum.valueOf(com.navdy.hud.app.framework.notifications.NotificationAnimator$Operation.class, s);
//    }
//    
//    public static com.navdy.hud.app.framework.notifications.NotificationAnimator$Operation[] values() {
//        return $VALUES.clone();
//    }
//}
//