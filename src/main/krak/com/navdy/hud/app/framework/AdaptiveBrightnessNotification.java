package com.navdy.hud.app.framework;
import com.navdy.hud.app.R;

public class AdaptiveBrightnessNotification extends android.database.ContentObserver implements com.navdy.hud.app.framework.notifications.INotification, android.content.SharedPreferences$OnSharedPreferenceChangeListener {
    final private static int DEFAULT_BRIGHTNESS_INT;
    final private static long DETENT_TIMEOUT = 250L;
    final private static int INITIAL_CHANGING_STEP = 1;
    final private static int MAX_BRIGHTNESS = 255;
    final private static int MAX_STEP = 16;
    final private static int NOTIFICATION_TIMEOUT = 10000;
    final private static com.navdy.service.library.log.Logger sLogger;
    final private boolean AUTO_BRIGHTNESS_ADJ_ENABLED;
    private int changingStep;
    private com.navdy.hud.app.ui.component.ChoiceLayout2 choiceLayout;
    private android.view.ViewGroup container;
    private com.navdy.hud.app.framework.notifications.INotificationController controller;
    private int currentValue;
    private boolean isAutoBrightnessOn;
    private boolean isLastChangeIncreasing;
    private long lastChangeActionTime;
    private com.navdy.hud.app.view.Gauge progressBar;
    private android.content.SharedPreferences sharedPreferences;
    private android.content.SharedPreferences$Editor sharedPreferencesEditor;
    private android.widget.TextView textIndicator;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.framework.AdaptiveBrightnessNotification.class);
        DEFAULT_BRIGHTNESS_INT = Integer.parseInt("128");
    }
    
    public AdaptiveBrightnessNotification() {
        super(new android.os.Handler());
        this.changingStep = 1;
        this.lastChangeActionTime = -251L;
        this.isLastChangeIncreasing = false;
        this.AUTO_BRIGHTNESS_ADJ_ENABLED = true;
        this.sharedPreferences = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getSharedPreferences();
        this.sharedPreferencesEditor = this.sharedPreferences.edit();
        this.isAutoBrightnessOn = this.getCurrentIsAutoBrightnessOn();
    }
    
    static int access$000(com.navdy.hud.app.framework.AdaptiveBrightnessNotification a) {
        return a.currentValue;
    }
    
    static android.content.SharedPreferences$Editor access$100(com.navdy.hud.app.framework.AdaptiveBrightnessNotification a) {
        return a.sharedPreferencesEditor;
    }
    
    static void access$200(com.navdy.hud.app.framework.AdaptiveBrightnessNotification a) {
        a.close();
    }
    
    private void close() {
        com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().removeNotification(this.getId());
        com.navdy.hud.app.analytics.AnalyticsSupport.recordAdaptiveAutobrightnessAdjustmentChanged(this.getCurrentBrightness());
    }
    
    private void detentChangingStep(boolean b) {
        long j = android.os.SystemClock.elapsedRealtime();
        boolean b0 = this.isLastChangeIncreasing;
        label2: {
            label0: {
                label1: {
                    if (b != b0) {
                        break label1;
                    }
                    if (this.lastChangeActionTime + 250L >= j) {
                        break label0;
                    }
                }
                this.changingStep = 1;
                break label2;
            }
            this.changingStep = this.changingStep << 1;
            if (this.changingStep > 16) {
                this.changingStep = 16;
            }
        }
        this.isLastChangeIncreasing = b;
        this.lastChangeActionTime = j;
    }
    
    private int getCurrentBrightness() {
        int i = 0;
        boolean b = this.isAutoBrightnessOn;
        label1: {
            android.provider.Settings$SettingNotFoundException a = null;
            label2: {
                NumberFormatException a0 = null;
                label0: if (b) {
                    try {
                        i = android.provider.Settings$System.getInt(com.navdy.hud.app.HudApplication.getAppContext().getContentResolver(), "screen_brightness");
                        break label1;
                    } catch(android.provider.Settings$SettingNotFoundException a1) {
                        a = a1;
                        break label2;
                    }
                } else {
                    try {
                        i = Integer.parseInt(this.sharedPreferences.getString("screen.brightness", "128"));
                    } catch(NumberFormatException a2) {
                        a0 = a2;
                        break label0;
                    }
                    break label1;
                }
                sLogger.e("Cannot parse brightness from the shared preferences", (Throwable)a0);
                i = DEFAULT_BRIGHTNESS_INT;
                break label1;
            }
            sLogger.e("Settings not found exception ", (Throwable)a);
            i = DEFAULT_BRIGHTNESS_INT;
        }
        return i;
    }
    
    private boolean getCurrentIsAutoBrightnessOn() {
        return "true".equals(this.sharedPreferences.getString("screen.auto_brightness", ""));
    }
    
    private void showFixedChoices(android.view.ViewGroup a) {
        this.choiceLayout = (com.navdy.hud.app.ui.component.ChoiceLayout2)a.findViewById(R.id.choiceLayout);
        java.util.ArrayList a0 = new java.util.ArrayList(3);
        android.content.res.Resources a1 = a.getResources();
        String s = a1.getString(R.string.glances_ok);
        int i = a1.getColor(R.color.glance_ok_blue);
        ((java.util.List)a0).add(new com.navdy.hud.app.ui.component.ChoiceLayout2$Choice(2, R.drawable.icon_glances_ok, i, R.drawable.icon_glances_ok, -16777216, s, i));
        this.choiceLayout.setChoices((java.util.List)a0, 1, (com.navdy.hud.app.ui.component.ChoiceLayout2$IListener)new com.navdy.hud.app.framework.AdaptiveBrightnessNotification$1(this));
        this.choiceLayout.setVisibility(0);
    }
    
    public static void showNotification() {
        com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().addNotification((com.navdy.hud.app.framework.notifications.INotification)new com.navdy.hud.app.framework.AdaptiveBrightnessNotification());
    }
    
    private void updateBrightness(int i) {
        if (i != 0) {
            int i0 = this.currentValue + i;
            if (i0 < 0) {
                i0 = 0;
            }
            if (i0 > 255) {
                i0 = 255;
            }
            this.sharedPreferencesEditor.putString("screen.brightness", String.valueOf(i0));
            this.sharedPreferencesEditor.apply();
        }
    }
    
    public boolean canAddToStackIfCurrentExists() {
        return false;
    }
    
    public boolean expandNotification() {
        return false;
    }
    
    public int getColor() {
        return 0;
    }
    
    public android.view.View getExpandedView(android.content.Context a, Object a0) {
        return null;
    }
    
    public int getExpandedViewIndicatorColor() {
        return 0;
    }
    
    public String getId() {
        return "navdy#brightness#notif";
    }
    
    public int getTimeout() {
        return 10000;
    }
    
    public com.navdy.hud.app.framework.notifications.NotificationType getType() {
        return com.navdy.hud.app.framework.notifications.NotificationType.BRIGHTNESS;
    }
    
    public android.view.View getView(android.content.Context a) {
        if (this.container == null) {
            this.container = (android.view.ViewGroup)android.view.LayoutInflater.from(a).inflate(R.layout.notification_adaptive_auto_brightness, (android.view.ViewGroup)null);
            this.textIndicator = (android.widget.TextView)this.container.findViewById(R.id.subTitle);
            this.progressBar = (com.navdy.hud.app.view.Gauge)this.container.findViewById(R.id.circle_progress);
            this.progressBar.setAnimated(false);
            this.showFixedChoices(this.container);
        }
        return this.container;
    }
    
    public android.animation.AnimatorSet getViewSwitchAnimation(boolean b) {
        return null;
    }
    
    public boolean isAlive() {
        return false;
    }
    
    public boolean isPurgeable() {
        return false;
    }
    
    public com.navdy.hud.app.manager.InputManager$IInputHandler nextHandler() {
        return null;
    }
    
    public void onChange(boolean b, android.net.Uri a) {
    }
    
    public void onClick() {
        this.close();
    }
    
    public void onExpandedNotificationEvent(com.navdy.hud.app.ui.framework.UIStateManager$Mode a) {
    }
    
    public void onExpandedNotificationSwitched() {
    }
    
    public boolean onGesture(com.navdy.service.library.events.input.GestureEvent a) {
        return false;
    }
    
    public boolean onKey(com.navdy.hud.app.manager.InputManager$CustomKeyEvent a) {
        boolean b = false;
        if (this.controller != null) {
            if (a != com.navdy.hud.app.manager.InputManager$CustomKeyEvent.SELECT) {
                this.controller.resetTimeout();
            }
            switch(com.navdy.hud.app.framework.AdaptiveBrightnessNotification$2.$SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent[a.ordinal()]) {
                case 3: {
                    sLogger.v("Increasing brightness");
                    this.detentChangingStep(true);
                    this.updateBrightness(this.changingStep);
                    b = true;
                    break;
                }
                case 2: {
                    sLogger.v("Decreasing brightness");
                    this.detentChangingStep(false);
                    this.updateBrightness(-this.changingStep);
                    b = true;
                    break;
                }
                case 1: {
                    if (this.choiceLayout == null) {
                        this.close();
                    } else {
                        this.choiceLayout.executeSelectedItem();
                    }
                    b = true;
                    break;
                }
                default: {
                    b = false;
                }
            }
        } else {
            b = false;
        }
        return b;
    }
    
    public void onNotificationEvent(com.navdy.hud.app.ui.framework.UIStateManager$Mode a) {
    }
    
    public void onSharedPreferenceChanged(android.content.SharedPreferences a, String s) {
        if (s.equals("screen.brightness")) {
            this.updateState();
        }
    }
    
    public void onStart(com.navdy.hud.app.framework.notifications.INotificationController a) {
        this.currentValue = this.getCurrentBrightness();
        this.sharedPreferencesEditor.putString("screen.auto_brightness", "false");
        this.sharedPreferencesEditor.apply();
        this.sharedPreferencesEditor.putString("screen.brightness", String.valueOf(this.currentValue));
        this.sharedPreferencesEditor.apply();
        this.isAutoBrightnessOn = false;
        this.controller = a;
        this.sharedPreferences.registerOnSharedPreferenceChangeListener((android.content.SharedPreferences$OnSharedPreferenceChangeListener)this);
        this.updateState();
    }
    
    public void onStop() {
        this.sharedPreferencesEditor.putString("screen.auto_brightness", "true");
        this.sharedPreferencesEditor.apply();
        this.isAutoBrightnessOn = false;
        this.sharedPreferences.unregisterOnSharedPreferenceChangeListener((android.content.SharedPreferences$OnSharedPreferenceChangeListener)this);
        this.controller = null;
    }
    
    public void onTrackHand(float f) {
    }
    
    public void onUpdate() {
        if (this.controller != null) {
            this.controller.resetTimeout();
        }
        this.updateState();
    }
    
    public boolean supportScroll() {
        return false;
    }
    
    public void updateState() {
        if (this.controller != null) {
            this.currentValue = this.getCurrentBrightness();
            this.progressBar.setValue(this.currentValue);
        } else {
            sLogger.v("brightness notif offscreen");
        }
    }
}
