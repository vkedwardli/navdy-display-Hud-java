package com.navdy.hud.app.framework.glympse;


public enum GlympseNotification$Type {
    SENT(0),
    READ(1),
    OFFLINE(2);

    private int value;
    GlympseNotification$Type(int value) {
        this.value = value;
    }
    public int getValue() {
        return value;
    }
}

//final public class GlympseNotification$Type extends Enum {
//    final private static com.navdy.hud.app.framework.glympse.GlympseNotification$Type[] $VALUES;
//    final public static com.navdy.hud.app.framework.glympse.GlympseNotification$Type OFFLINE;
//    final public static com.navdy.hud.app.framework.glympse.GlympseNotification$Type READ;
//    final public static com.navdy.hud.app.framework.glympse.GlympseNotification$Type SENT;
//    
//    static {
//        SENT = new com.navdy.hud.app.framework.glympse.GlympseNotification$Type("SENT", 0);
//        READ = new com.navdy.hud.app.framework.glympse.GlympseNotification$Type("READ", 1);
//        OFFLINE = new com.navdy.hud.app.framework.glympse.GlympseNotification$Type("OFFLINE", 2);
//        com.navdy.hud.app.framework.glympse.GlympseNotification$Type[] a = new com.navdy.hud.app.framework.glympse.GlympseNotification$Type[3];
//        a[0] = SENT;
//        a[1] = READ;
//        a[2] = OFFLINE;
//        $VALUES = a;
//    }
//    
//    private GlympseNotification$Type(String s, int i) {
//        super(s, i);
//    }
//    
//    public static com.navdy.hud.app.framework.glympse.GlympseNotification$Type valueOf(String s) {
//        return (com.navdy.hud.app.framework.glympse.GlympseNotification$Type)Enum.valueOf(com.navdy.hud.app.framework.glympse.GlympseNotification$Type.class, s);
//    }
//    
//    public static com.navdy.hud.app.framework.glympse.GlympseNotification$Type[] values() {
//        return $VALUES.clone();
//    }
//}
//