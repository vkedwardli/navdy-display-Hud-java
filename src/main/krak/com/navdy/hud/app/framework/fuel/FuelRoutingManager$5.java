package com.navdy.hud.app.framework.fuel;

class FuelRoutingManager$5 implements com.navdy.hud.app.maps.here.HereRouteCalculator$RouteCalculatorListener {
    final com.navdy.hud.app.framework.fuel.FuelRoutingManager this$0;
    final com.navdy.hud.app.framework.fuel.FuelRoutingManager$RouteCacheItem val$gasRoute;
    final com.navdy.service.library.events.navigation.NavigationRouteRequest val$navigationRouteRequest;
    final com.navdy.hud.app.framework.fuel.FuelRoutingManager$OnRouteToGasStationCallback val$onRouteToGasStationCallback;
    
    FuelRoutingManager$5(com.navdy.hud.app.framework.fuel.FuelRoutingManager a, com.navdy.hud.app.framework.fuel.FuelRoutingManager$OnRouteToGasStationCallback a0, com.navdy.hud.app.framework.fuel.FuelRoutingManager$RouteCacheItem a1, com.navdy.service.library.events.navigation.NavigationRouteRequest a2) {
        super();
        this.this$0 = a;
        this.val$onRouteToGasStationCallback = a0;
        this.val$gasRoute = a1;
        this.val$navigationRouteRequest = a2;
    }
    
    public void error(com.here.android.mpa.routing.RoutingError a, Throwable a0) {
        this.val$onRouteToGasStationCallback.onError(com.navdy.hud.app.framework.fuel.FuelRoutingManager$OnRouteToGasStationCallback$Error.RESPONSE_ERROR);
    }
    
    public void postSuccess(java.util.ArrayList a) {
        this.val$onRouteToGasStationCallback.onOptimalRouteCalculationComplete(this.val$gasRoute, a, this.val$navigationRouteRequest);
    }
    
    public void preSuccess() {
    }
    
    public void progress(int i) {
        if (com.navdy.hud.app.framework.fuel.FuelRoutingManager.access$000().isLoggable(2)) {
            com.navdy.hud.app.framework.fuel.FuelRoutingManager.access$000().v(new StringBuilder().append("calculating route to optimal gas station progress %: ").append(i).toString());
        }
    }
}
