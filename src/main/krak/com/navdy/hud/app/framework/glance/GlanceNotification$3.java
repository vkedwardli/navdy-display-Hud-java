package com.navdy.hud.app.framework.glance;

class GlanceNotification$3 implements com.navdy.hud.app.view.ObservableScrollView$IScrollListener {
    final com.navdy.hud.app.framework.glance.GlanceNotification this$0;
    
    GlanceNotification$3(com.navdy.hud.app.framework.glance.GlanceNotification a) {
        super();
        this.this$0 = a;
    }
    
    public void onBottom() {
        if (com.navdy.hud.app.framework.glance.GlanceNotification.access$000(this.this$0) != null && com.navdy.hud.app.framework.glance.GlanceNotification.access$2000(this.this$0) && com.navdy.hud.app.framework.glance.GlanceNotification.access$2100(this.this$0) != null) {
            com.navdy.hud.app.framework.glance.GlanceNotification.access$2302(this.this$0, false);
            com.navdy.hud.app.framework.glance.GlanceNotification.access$2402(this.this$0, true);
            if (com.navdy.hud.app.framework.glance.GlanceNotification.access$2500(this.this$0) != null) {
                com.navdy.hud.app.framework.glance.GlanceNotification.access$2500(this.this$0).onPosChange(100);
            }
        }
    }
    
    public void onScroll(int i, int i0, int i1, int i2) {
        if (com.navdy.hud.app.framework.glance.GlanceNotification.access$000(this.this$0) != null && com.navdy.hud.app.framework.glance.GlanceNotification.access$2000(this.this$0) && com.navdy.hud.app.framework.glance.GlanceNotification.access$2100(this.this$0) != null) {
            com.navdy.hud.app.framework.glance.GlanceNotification.access$2200(this.this$0).setVisibility(0);
            com.navdy.hud.app.framework.glance.GlanceNotification.access$2302(this.this$0, false);
            com.navdy.hud.app.framework.glance.GlanceNotification.access$2402(this.this$0, false);
            if (com.navdy.hud.app.framework.glance.GlanceNotification.access$2500(this.this$0) != null) {
                int i3 = com.navdy.hud.app.framework.glance.GlanceNotification.access$2100(this.this$0).getChildAt(0).getBottom();
                int i4 = com.navdy.hud.app.framework.glance.GlanceNotification.access$2100(this.this$0).getHeight();
                double d = (double)i0 * 100.0 / (double)(i3 - i4);
                com.navdy.hud.app.framework.glance.GlanceNotification.access$2500(this.this$0).onPosChange((int)d);
            }
        }
    }
    
    public void onTop() {
        if (com.navdy.hud.app.framework.glance.GlanceNotification.access$000(this.this$0) != null && com.navdy.hud.app.framework.glance.GlanceNotification.access$2000(this.this$0) && com.navdy.hud.app.framework.glance.GlanceNotification.access$2100(this.this$0) != null) {
            com.navdy.hud.app.framework.glance.GlanceNotification.access$2200(this.this$0).setVisibility(8);
            com.navdy.hud.app.framework.glance.GlanceNotification.access$2302(this.this$0, true);
            com.navdy.hud.app.framework.glance.GlanceNotification.access$2402(this.this$0, false);
            if (com.navdy.hud.app.framework.glance.GlanceNotification.access$2500(this.this$0) != null) {
                com.navdy.hud.app.framework.glance.GlanceNotification.access$2500(this.this$0).onPosChange(1);
            }
        }
    }
}
