package com.navdy.hud.app.framework.voice;

class VoiceAssistNotification$2 {
    final static int[] $SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent;
    final static int[] $SwitchMap$com$navdy$service$library$events$DeviceInfo$Platform;
    final static int[] $SwitchMap$com$navdy$service$library$events$audio$VoiceAssistResponse$VoiceAssistState;
    
    static {
        $SwitchMap$com$navdy$service$library$events$audio$VoiceAssistResponse$VoiceAssistState = new int[com.navdy.service.library.events.audio.VoiceAssistResponse$VoiceAssistState.values().length];
        int[] a = $SwitchMap$com$navdy$service$library$events$audio$VoiceAssistResponse$VoiceAssistState;
        com.navdy.service.library.events.audio.VoiceAssistResponse$VoiceAssistState a0 = com.navdy.service.library.events.audio.VoiceAssistResponse$VoiceAssistState.VOICE_ASSIST_AVAILABLE;
        try {
            a[a0.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$audio$VoiceAssistResponse$VoiceAssistState[com.navdy.service.library.events.audio.VoiceAssistResponse$VoiceAssistState.VOICE_ASSIST_NOT_AVAILABLE.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException0) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$audio$VoiceAssistResponse$VoiceAssistState[com.navdy.service.library.events.audio.VoiceAssistResponse$VoiceAssistState.VOICE_ASSIST_STOPPED.ordinal()] = 3;
        } catch(NoSuchFieldError ignoredException1) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$audio$VoiceAssistResponse$VoiceAssistState[com.navdy.service.library.events.audio.VoiceAssistResponse$VoiceAssistState.VOICE_ASSIST_LISTENING.ordinal()] = 4;
        } catch(NoSuchFieldError ignoredException2) {
        }
        $SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent = new int[com.navdy.hud.app.manager.InputManager$CustomKeyEvent.values().length];
        int[] a1 = $SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent;
        com.navdy.hud.app.manager.InputManager$CustomKeyEvent a2 = com.navdy.hud.app.manager.InputManager$CustomKeyEvent.LONG_PRESS;
        try {
            a1[a2.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException3) {
        }
        $SwitchMap$com$navdy$service$library$events$DeviceInfo$Platform = new int[com.navdy.service.library.events.DeviceInfo$Platform.values().length];
        int[] a3 = $SwitchMap$com$navdy$service$library$events$DeviceInfo$Platform;
        com.navdy.service.library.events.DeviceInfo$Platform a4 = com.navdy.service.library.events.DeviceInfo$Platform.PLATFORM_Android;
        try {
            a3[a4.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException4) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$DeviceInfo$Platform[com.navdy.service.library.events.DeviceInfo$Platform.PLATFORM_iOS.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException5) {
        }
    }
}
