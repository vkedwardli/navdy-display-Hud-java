package com.navdy.hud.app.framework.recentcall;

class RecentCallManager$3 implements Runnable {
    final com.navdy.hud.app.framework.recentcall.RecentCallManager this$0;
    final java.util.List val$calls;
    
    RecentCallManager$3(com.navdy.hud.app.framework.recentcall.RecentCallManager a, java.util.List a0) {
        super();
        this.this$0 = a;
        this.val$calls = a0;
    }
    
    public void run() {
        com.navdy.hud.app.storage.db.helper.RecentCallPersistenceHelper.storeRecentCalls(com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getCurrentProfile().getProfileName(), this.val$calls, false);
        Object a = this.val$calls.iterator();
        while(((java.util.Iterator)a).hasNext()) {
            com.navdy.hud.app.framework.recentcall.RecentCall a0 = (com.navdy.hud.app.framework.recentcall.RecentCall)((java.util.Iterator)a).next();
            com.navdy.hud.app.framework.recentcall.RecentCallManager.access$100().v(new StringBuilder().append("added contact [").append(a0.number).append("]").toString());
        }
    }
}
