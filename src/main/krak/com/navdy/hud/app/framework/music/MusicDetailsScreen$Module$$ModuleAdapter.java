package com.navdy.hud.app.framework.music;

final public class MusicDetailsScreen$Module$$ModuleAdapter extends dagger.internal.ModuleAdapter {
    final private static Class[] INCLUDES;
    final private static String[] INJECTS;
    final private static Class[] STATIC_INJECTIONS;
    
    static {
        String[] a = new String[1];
        a[0] = "members/com.navdy.hud.app.framework.music.MusicDetailsView";
        INJECTS = a;
        STATIC_INJECTIONS = new Class[0];
        INCLUDES = new Class[0];
    }
    
    public MusicDetailsScreen$Module$$ModuleAdapter() {
        super(com.navdy.hud.app.framework.music.MusicDetailsScreen$Module.class, INJECTS, STATIC_INJECTIONS, false, INCLUDES, true, false);
    }
}
