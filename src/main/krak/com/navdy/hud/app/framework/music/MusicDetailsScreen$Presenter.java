package com.navdy.hud.app.framework.music;
import com.navdy.hud.app.R;
import com.squareup.otto.Subscribe;
import javax.inject.Singleton;
import javax.inject.Inject;

@Singleton
public class MusicDetailsScreen$Presenter extends com.navdy.hud.app.ui.framework.BasePresenter {
    final private static int MUSIC_CONTROL_TRAY_POS = 0;
    final private static int music_selected_color;
    final private static int music_unselected_color;
    final private static String[] subTitles;
    final private static com.navdy.hud.app.ui.component.vlist.VerticalList$Model thumbsDown;
    final private static com.navdy.hud.app.ui.component.vlist.VerticalList$Model thumbsUp;
    final private static String[] titles;
    private boolean animateIn;
    @Inject
    com.squareup.otto.Bus bus;
    private boolean closed;
    private int currentCounter;
    private int currentTitleSelection;
    private java.util.List data;
    private boolean handledSelection;
    private boolean keyPressed;
    private boolean registered;
    private int thumbPos;
    
    static {
        android.content.res.Resources a = com.navdy.hud.app.HudApplication.getAppContext().getResources();
        music_selected_color = a.getColor(R.color.music_details_sel);
        music_unselected_color = a.getColor(R.color.icon_bk_color_unselected);
        String[] a0 = new String[3];
        a0[0] = "The Mother We Both Share";
        a0[1] = "Tomorrow, Tomorrow, I love you";
        a0[2] = "Who Let the Dogs out";
        titles = a0;
        String[] a1 = new String[3];
        a1[0] = "Churches Your Vibe, Your Tribe Vol. 1";
        a1[1] = "Old Classics, Vol 2";
        a1[2] = "woof woof";
        subTitles = a1;
        thumbsUp = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(R.id.music_thumbsup, R.drawable.icon_thumbs_up, music_selected_color, music_unselected_color, music_selected_color, "Like", (String)null);
        thumbsDown = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(R.id.music_thumbsdown, R.drawable.icon_thumbs_dn, music_selected_color, music_unselected_color, music_selected_color, "Dislike", (String)null);
    }
    
    public MusicDetailsScreen$Presenter() {
    }
    
    void close() {
        if (this.closed) {
            com.navdy.hud.app.framework.music.MusicDetailsScreen.access$000().v("already closed");
        } else {
            this.closed = true;
            com.navdy.hud.app.framework.music.MusicDetailsScreen.access$000().v("close");
            com.navdy.hud.app.analytics.AnalyticsSupport.recordMenuSelection("exit");
            com.navdy.hud.app.framework.music.MusicDetailsView a = (com.navdy.hud.app.framework.music.MusicDetailsView)this.getView();
            if (a != null) {
                a.vmenuComponent.animateOut((android.animation.Animator$AnimatorListener)new com.navdy.hud.app.framework.music.MusicDetailsScreen$Presenter$1(this));
            }
        }
    }
    
    void init(com.navdy.hud.app.framework.music.MusicDetailsView a) {
        a.vmenuComponent.setOverrideDefaultKeyEvents(com.navdy.hud.app.manager.InputManager$CustomKeyEvent.LONG_PRESS, true);
        java.util.ArrayList a0 = new java.util.ArrayList();
        ((java.util.List)a0).add(com.navdy.hud.app.ui.component.vlist.viewholder.TitleSubtitleViewHolder.buildModel(titles[this.currentTitleSelection], subTitles[this.currentTitleSelection]));
        this.currentTitleSelection = this.currentTitleSelection + 1;
        int[] a1 = new int[3];
        a1[0] = music_selected_color;
        a1[1] = music_selected_color;
        a1[2] = music_selected_color;
        int i = music_unselected_color;
        int i0 = music_unselected_color;
        int i1 = music_unselected_color;
        int[] a2 = new int[3];
        a2[0] = R.drawable.icon_prev_music;
        a2[1] = R.drawable.icon_play_music;
        a2[2] = R.drawable.icon_next_music;
        int[] a3 = new int[3];
        a3[0] = R.id.music_rewind;
        a3[1] = R.id.music_play;
        a3[2] = R.id.music_forward;
        int[] a4 = new int[3];
        a4[0] = i;
        a4[1] = i0;
        a4[2] = i1;
        ((java.util.List)a0).add(com.navdy.hud.app.ui.component.vlist.viewholder.IconOptionsViewHolder.buildModel(R.id.music_controls, a2, a3, a1, a4, a1, 1, false));
        ((java.util.List)a0).add(com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(2, R.drawable.icon_playlist, music_selected_color, music_unselected_color, music_selected_color, "Playlist/Album name", (String)null));
        ((java.util.List)a0).add(com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(3, R.drawable.icon_heart, music_selected_color, music_unselected_color, music_selected_color, "Love", (String)null));
        this.thumbPos = ((java.util.List)a0).size();
        ((java.util.List)a0).add(thumbsUp);
        ((java.util.List)a0).add(com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(4, R.drawable.icon_shuffle, music_selected_color, music_unselected_color, music_selected_color, "Shuffle", (String)null));
        ((java.util.List)a0).add(com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(5, R.drawable.icon_stations, music_selected_color, music_unselected_color, music_selected_color, "Stations", (String)null));
        ((java.util.List)a0).add(com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(6, R.drawable.icon_bookmark, music_selected_color, music_unselected_color, music_selected_color, "Bookmark", (String)null));
        this.data = (java.util.List)a0;
        a.vmenuComponent.updateView(this.data, 0, false);
    }
    
    boolean isClosed() {
        return this.closed;
    }
    
    boolean isItemClickable(com.navdy.hud.app.ui.component.vlist.VerticalList$ItemSelectionState a) {
        return true;
    }
    
    @Subscribe
    public void onConnectionStateChange(com.navdy.service.library.events.connection.ConnectionStateChange a) {
        if (com.navdy.hud.app.framework.music.MusicDetailsScreen$1.$SwitchMap$com$navdy$service$library$events$connection$ConnectionStateChange$ConnectionState[a.state.ordinal()] != 0) {
            com.navdy.hud.app.framework.music.MusicDetailsScreen.access$000().v("disconnected");
            this.close();
        }
    }
    
    @Subscribe
    public void onKeyEvent(android.view.KeyEvent a) {
        if (com.navdy.hud.app.manager.InputManager.isCenterKey(a.getKeyCode())) {
            com.navdy.hud.app.framework.music.MusicDetailsView a0 = (com.navdy.hud.app.framework.music.MusicDetailsView)this.getView();
            if (a0 != null && !a0.vmenuComponent.isCloseMenuVisible() && a0.vmenuComponent.verticalList.getCurrentPosition() == 0) {
                com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder a1 = a0.vmenuComponent.verticalList.getCurrentViewHolder();
                if (a1 != null && a1 instanceof com.navdy.hud.app.ui.component.vlist.viewholder.IconOptionsViewHolder) {
                    com.navdy.hud.app.ui.component.vlist.viewholder.IconOptionsViewHolder a2 = (com.navdy.hud.app.ui.component.vlist.viewholder.IconOptionsViewHolder)a1;
                    int i = a2.getCurrentSelection();
                    if (i != 1) {
                        boolean b = i == 2;
                        if (a.getAction() != 0) {
                            if (a.getAction() == 1 && this.keyPressed) {
                                this.keyPressed = false;
                                a2.selectionUp();
                                this.reset(a0);
                            }
                        } else {
                            if (!this.keyPressed) {
                                this.keyPressed = true;
                                a2.selectionDown();
                            }
                            int i0 = this.currentCounter;
                            if (b) {
                                this.currentCounter = this.currentCounter + 1;
                            } else if (this.currentCounter > 1) {
                                this.currentCounter = this.currentCounter - 1;
                            }
                            a0.counter.setText((CharSequence)String.valueOf(i0));
                        }
                    }
                }
            }
        }
    }
    
    public void onLoad(android.os.Bundle a) {
        this.currentCounter = 1;
        this.currentTitleSelection = 0;
        this.animateIn = false;
        this.keyPressed = false;
        this.reset((com.navdy.hud.app.framework.music.MusicDetailsView)null);
        this.bus.register(this);
        this.registered = true;
        this.updateView();
        super.onLoad(a);
    }
    
    public void onUnload() {
        if (this.registered) {
            this.registered = false;
            this.bus.unregister(this);
        }
        this.reset((com.navdy.hud.app.framework.music.MusicDetailsView)null);
        super.onUnload();
    }
    
    void reset(com.navdy.hud.app.framework.music.MusicDetailsView a) {
        this.closed = false;
        this.handledSelection = false;
        if (a != null) {
            a.vmenuComponent.unlock();
        }
    }
    
    void resetSelectedItem() {
        com.navdy.hud.app.framework.music.MusicDetailsScreen.access$000().v("resetSelectedItem");
        this.handledSelection = false;
        if (!this.animateIn) {
            this.animateIn = true;
            com.navdy.hud.app.framework.music.MusicDetailsView a = (com.navdy.hud.app.framework.music.MusicDetailsView)this.getView();
            if (a != null) {
                a.vmenuComponent.animateIn((android.animation.Animator$AnimatorListener)null);
            }
        }
    }
    
    boolean selectItem(com.navdy.hud.app.ui.component.vlist.VerticalList$ItemSelectionState a) {
        boolean b = false;
        com.navdy.hud.app.framework.music.MusicDetailsScreen.access$000().v(new StringBuilder().append("select id:").append(a.id).append(",").append(a.pos).append(" subid=").append(a.subId).toString());
        if (this.handledSelection) {
            com.navdy.hud.app.framework.music.MusicDetailsScreen.access$000().v(new StringBuilder().append("already handled [").append(a.id).append("], ").append(a.pos).toString());
            b = true;
        } else {
            com.navdy.hud.app.framework.music.MusicDetailsView a0 = (com.navdy.hud.app.framework.music.MusicDetailsView)this.getView();
            label0: if (a0 != null) {
                switch(a.id) {
                    case R.id.music_thumbsup: {
                        com.navdy.hud.app.framework.music.MusicDetailsScreen.access$000().v("thumbsdown");
                        this.reset(a0);
                        a0.vmenuComponent.verticalList.refreshData(this.thumbPos, thumbsDown);
                        break;
                    }
                    case R.id.music_thumbsdown: {
                        com.navdy.hud.app.framework.music.MusicDetailsScreen.access$000().v("thumbsup");
                        this.reset(a0);
                        a0.vmenuComponent.verticalList.refreshData(this.thumbPos, thumbsUp);
                        break;
                    }
                    case R.id.music_controls: {
                        switch(a.subId) {
                            case R.id.music_rewind: {
                                this.reset(a0);
                                break label0;
                            }
                            case R.id.music_play: {
                                this.reset(a0);
                                if (this.currentTitleSelection == titles.length) {
                                    this.currentTitleSelection = 0;
                                }
                                com.navdy.hud.app.ui.component.vlist.VerticalList$Model a1 = com.navdy.hud.app.ui.component.vlist.viewholder.TitleSubtitleViewHolder.buildModel(titles[this.currentTitleSelection], subTitles[this.currentTitleSelection]);
                                this.currentTitleSelection = this.currentTitleSelection + 1;
                                a0.vmenuComponent.verticalList.refreshData(a.pos, a1);
                                break label0;
                            }
                            case R.id.music_pause: {
                                this.reset(a0);
                                break label0;
                            }
                            case R.id.music_forward: {
                                this.reset(a0);
                                break label0;
                            }
                            default: {
                                break label0;
                            }
                        }
                    }
                    default: {
                        this.close();
                    }
                }
            }
            b = this.handledSelection;
        }
        return b;
    }
    
    protected void updateView() {
        com.navdy.hud.app.framework.music.MusicDetailsView a = (com.navdy.hud.app.framework.music.MusicDetailsView)this.getView();
        if (a != null) {
            this.init(a);
        }
    }
}
