package com.navdy.hud.app.framework.glympse;


public enum GlympseManager$Error {
    NONE(0),
    INTERNAL_ERROR(1),
    NO_INTERNET(2);

    private int value;
    GlympseManager$Error(int value) {
        this.value = value;
    }
    public int getValue() {
        return value;
    }
}

//final public class GlympseManager$Error extends Enum {
//    final private static com.navdy.hud.app.framework.glympse.GlympseManager$Error[] $VALUES;
//    final public static com.navdy.hud.app.framework.glympse.GlympseManager$Error INTERNAL_ERROR;
//    final public static com.navdy.hud.app.framework.glympse.GlympseManager$Error NONE;
//    final public static com.navdy.hud.app.framework.glympse.GlympseManager$Error NO_INTERNET;
//    
//    static {
//        NONE = new com.navdy.hud.app.framework.glympse.GlympseManager$Error("NONE", 0);
//        INTERNAL_ERROR = new com.navdy.hud.app.framework.glympse.GlympseManager$Error("INTERNAL_ERROR", 1);
//        NO_INTERNET = new com.navdy.hud.app.framework.glympse.GlympseManager$Error("NO_INTERNET", 2);
//        com.navdy.hud.app.framework.glympse.GlympseManager$Error[] a = new com.navdy.hud.app.framework.glympse.GlympseManager$Error[3];
//        a[0] = NONE;
//        a[1] = INTERNAL_ERROR;
//        a[2] = NO_INTERNET;
//        $VALUES = a;
//    }
//    
//    private GlympseManager$Error(String s, int i) {
//        super(s, i);
//    }
//    
//    public static com.navdy.hud.app.framework.glympse.GlympseManager$Error valueOf(String s) {
//        return (com.navdy.hud.app.framework.glympse.GlympseManager$Error)Enum.valueOf(com.navdy.hud.app.framework.glympse.GlympseManager$Error.class, s);
//    }
//    
//    public static com.navdy.hud.app.framework.glympse.GlympseManager$Error[] values() {
//        return $VALUES.clone();
//    }
//}
//