package com.navdy.hud.app.framework.phonecall;

class PhoneBatteryNotification$1 {
    final static int[] $SwitchMap$com$navdy$service$library$events$callcontrol$PhoneBatteryStatus$BatteryStatus;
    
    static {
        $SwitchMap$com$navdy$service$library$events$callcontrol$PhoneBatteryStatus$BatteryStatus = new int[com.navdy.service.library.events.callcontrol.PhoneBatteryStatus$BatteryStatus.values().length];
        int[] a = $SwitchMap$com$navdy$service$library$events$callcontrol$PhoneBatteryStatus$BatteryStatus;
        com.navdy.service.library.events.callcontrol.PhoneBatteryStatus$BatteryStatus a0 = com.navdy.service.library.events.callcontrol.PhoneBatteryStatus$BatteryStatus.BATTERY_EXTREMELY_LOW;
        try {
            a[a0.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$callcontrol$PhoneBatteryStatus$BatteryStatus[com.navdy.service.library.events.callcontrol.PhoneBatteryStatus$BatteryStatus.BATTERY_LOW.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException0) {
        }
    }
}
