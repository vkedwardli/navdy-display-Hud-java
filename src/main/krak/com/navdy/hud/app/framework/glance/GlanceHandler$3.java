package com.navdy.hud.app.framework.glance;

class GlanceHandler$3 {
    final static int[] $SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp;
    
    static {
        $SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp = new int[com.navdy.hud.app.framework.glance.GlanceApp.values().length];
        int[] a = $SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp;
        com.navdy.hud.app.framework.glance.GlanceApp a0 = com.navdy.hud.app.framework.glance.GlanceApp.SMS;
        try {
            a[a0.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp[com.navdy.hud.app.framework.glance.GlanceApp.IMESSAGE.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException0) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp[com.navdy.hud.app.framework.glance.GlanceApp.GOOGLE_HANGOUT.ordinal()] = 3;
        } catch(NoSuchFieldError ignoredException1) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp[com.navdy.hud.app.framework.glance.GlanceApp.SLACK.ordinal()] = 4;
        } catch(NoSuchFieldError ignoredException2) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp[com.navdy.hud.app.framework.glance.GlanceApp.WHATS_APP.ordinal()] = 5;
        } catch(NoSuchFieldError ignoredException3) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp[com.navdy.hud.app.framework.glance.GlanceApp.FACEBOOK_MESSENGER.ordinal()] = 6;
        } catch(NoSuchFieldError ignoredException4) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp[com.navdy.hud.app.framework.glance.GlanceApp.GENERIC_MESSAGE.ordinal()] = 7;
        } catch(NoSuchFieldError ignoredException5) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp[com.navdy.hud.app.framework.glance.GlanceApp.FACEBOOK.ordinal()] = 8;
        } catch(NoSuchFieldError ignoredException6) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp[com.navdy.hud.app.framework.glance.GlanceApp.TWITTER.ordinal()] = 9;
        } catch(NoSuchFieldError ignoredException7) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp[com.navdy.hud.app.framework.glance.GlanceApp.GENERIC_SOCIAL.ordinal()] = 10;
        } catch(NoSuchFieldError ignoredException8) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp[com.navdy.hud.app.framework.glance.GlanceApp.GENERIC.ordinal()] = 11;
        } catch(NoSuchFieldError ignoredException9) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp[com.navdy.hud.app.framework.glance.GlanceApp.GOOGLE_MAIL.ordinal()] = 12;
        } catch(NoSuchFieldError ignoredException10) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp[com.navdy.hud.app.framework.glance.GlanceApp.GOOGLE_INBOX.ordinal()] = 13;
        } catch(NoSuchFieldError ignoredException11) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp[com.navdy.hud.app.framework.glance.GlanceApp.APPLE_MAIL.ordinal()] = 14;
        } catch(NoSuchFieldError ignoredException12) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp[com.navdy.hud.app.framework.glance.GlanceApp.GENERIC_MAIL.ordinal()] = 15;
        } catch(NoSuchFieldError ignoredException13) {
        }
    }
}
