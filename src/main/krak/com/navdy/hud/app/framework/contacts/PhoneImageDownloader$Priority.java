package com.navdy.hud.app.framework.contacts;


public enum PhoneImageDownloader$Priority {
    NORMAL(0),
    HIGH(1);

    private int value;
    PhoneImageDownloader$Priority(int value) {
        this.value = value;
    }
    public int getValue() {
        return value;
    }
}

//final public class PhoneImageDownloader$Priority extends Enum {
//    final private static com.navdy.hud.app.framework.contacts.PhoneImageDownloader$Priority[] $VALUES;
//    final public static com.navdy.hud.app.framework.contacts.PhoneImageDownloader$Priority HIGH;
//    final public static com.navdy.hud.app.framework.contacts.PhoneImageDownloader$Priority NORMAL;
//    final private int val;
//    
//    static {
//        NORMAL = new com.navdy.hud.app.framework.contacts.PhoneImageDownloader$Priority("NORMAL", 0, 1);
//        HIGH = new com.navdy.hud.app.framework.contacts.PhoneImageDownloader$Priority("HIGH", 1, 2);
//        com.navdy.hud.app.framework.contacts.PhoneImageDownloader$Priority[] a = new com.navdy.hud.app.framework.contacts.PhoneImageDownloader$Priority[2];
//        a[0] = NORMAL;
//        a[1] = HIGH;
//        $VALUES = a;
//    }
//    
//    private PhoneImageDownloader$Priority(String s, int i, int i0) {
//        super(s, i);
//        this.val = i0;
//    }
//    
//    public static com.navdy.hud.app.framework.contacts.PhoneImageDownloader$Priority valueOf(String s) {
//        return (com.navdy.hud.app.framework.contacts.PhoneImageDownloader$Priority)Enum.valueOf(com.navdy.hud.app.framework.contacts.PhoneImageDownloader$Priority.class, s);
//    }
//    
//    public static com.navdy.hud.app.framework.contacts.PhoneImageDownloader$Priority[] values() {
//        return $VALUES.clone();
//    }
//    
//    public int getValue() {
//        return this.val;
//    }
//}
//