package com.navdy.hud.app.framework.notifications;

abstract public interface INotificationController {
    abstract public void collapseNotification(boolean arg, boolean arg0);
    
    
    abstract public void expandNotification(boolean arg);
    
    
    abstract public android.content.Context getUIContext();
    
    
    abstract public boolean isExpanded();
    
    
    abstract public boolean isExpandedWithStack();
    
    
    abstract public boolean isShowOn();
    
    
    abstract public boolean isTtsOn();
    
    
    abstract public void moveNext(boolean arg);
    
    
    abstract public void movePrevious(boolean arg);
    
    
    abstract public void resetTimeout();
    
    
    abstract public void startTimeout(int arg);
    
    
    abstract public void stopTimeout(boolean arg);
}
