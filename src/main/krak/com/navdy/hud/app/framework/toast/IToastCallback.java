package com.navdy.hud.app.framework.toast;

abstract public interface IToastCallback {
    abstract public void executeChoiceItem(int arg, int arg0);
    
    
    abstract public boolean onKey(com.navdy.hud.app.manager.InputManager$CustomKeyEvent arg);
    
    
    abstract public void onStart(com.navdy.hud.app.view.ToastView arg);
    
    
    abstract public void onStop();
}
