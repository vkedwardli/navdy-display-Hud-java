package com.navdy.hud.app.framework.notifications;

class NotificationManager$5 implements com.navdy.hud.app.framework.notifications.IProgressUpdate {
    final com.navdy.hud.app.framework.notifications.NotificationManager this$0;
    
    NotificationManager$5(com.navdy.hud.app.framework.notifications.NotificationManager a) {
        super();
        this.this$0 = a;
    }
    
    public void onPosChange(int i) {
        if (com.navdy.hud.app.framework.notifications.NotificationManager.access$1200(this.this$0) != null && com.navdy.hud.app.framework.notifications.NotificationManager.access$1200(this.this$0).notification.supportScroll()) {
            if (i > 0) {
                if (i > 100) {
                    i = 100;
                }
            } else {
                i = 1;
            }
            if (com.navdy.hud.app.framework.notifications.NotificationManager.access$3600(this.this$0).getCurrentItem() != i) {
                label0: {
                    label1: {
                        if (i == 1) {
                            break label1;
                        }
                        if (i != 100) {
                            break label0;
                        }
                    }
                    com.navdy.hud.app.framework.notifications.NotificationManager.access$3700(this.this$0);
                }
                com.navdy.hud.app.framework.notifications.NotificationManager.access$3600(this.this$0).setCurrentItem(i);
            }
        }
    }
}
