package com.navdy.hud.app.framework.trips;

final public class TripManager$$InjectAdapter extends dagger.internal.Binding implements javax.inject.Provider {
    private dagger.internal.Binding bus;
    private dagger.internal.Binding preferences;
    
    public TripManager$$InjectAdapter() {
        super("com.navdy.hud.app.framework.trips.TripManager", "members/com.navdy.hud.app.framework.trips.TripManager", true, com.navdy.hud.app.framework.trips.TripManager.class);
    }
    
    public void attach(dagger.internal.Linker a) {
        this.bus = a.requestBinding("com.squareup.otto.Bus", com.navdy.hud.app.framework.trips.TripManager.class, (this).getClass().getClassLoader());
        this.preferences = a.requestBinding("android.content.SharedPreferences", com.navdy.hud.app.framework.trips.TripManager.class, (this).getClass().getClassLoader());
    }
    
    public com.navdy.hud.app.framework.trips.TripManager get() {
        return new com.navdy.hud.app.framework.trips.TripManager((com.squareup.otto.Bus)this.bus.get(), (android.content.SharedPreferences)this.preferences.get());
    }
    
    public Object get() {
        return this.get();
    }
    
    public void getDependencies(java.util.Set a, java.util.Set a0) {
        a.add(this.bus);
        a.add(this.preferences);
    }
}
