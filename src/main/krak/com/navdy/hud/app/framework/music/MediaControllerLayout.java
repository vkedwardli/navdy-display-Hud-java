package com.navdy.hud.app.framework.music;
import com.navdy.hud.app.R;

public class MediaControllerLayout extends com.navdy.hud.app.ui.component.ChoiceLayout {
    final public static java.util.HashMap MEDIA_CONTROL_ICON_MAPPING;
    private java.util.Set availableMediaControls;
    
    static {
        MEDIA_CONTROL_ICON_MAPPING = new java.util.HashMap();
        MEDIA_CONTROL_ICON_MAPPING.put(com.navdy.hud.app.manager.MusicManager$MediaControl.PLAY, Integer.valueOf(R.drawable.icon_play_sm_2));
        MEDIA_CONTROL_ICON_MAPPING.put(com.navdy.hud.app.manager.MusicManager$MediaControl.PAUSE, Integer.valueOf(R.drawable.icon_pause_sm_2));
        MEDIA_CONTROL_ICON_MAPPING.put(com.navdy.hud.app.manager.MusicManager$MediaControl.PREVIOUS, Integer.valueOf(R.drawable.icon_prev_sm_2));
        MEDIA_CONTROL_ICON_MAPPING.put(com.navdy.hud.app.manager.MusicManager$MediaControl.NEXT, Integer.valueOf(R.drawable.icon_next_sm_2));
        MEDIA_CONTROL_ICON_MAPPING.put(com.navdy.hud.app.manager.MusicManager$MediaControl.MUSIC_MENU, Integer.valueOf(R.drawable.icon_music_sm_2));
        MEDIA_CONTROL_ICON_MAPPING.put(com.navdy.hud.app.manager.MusicManager$MediaControl.MUSIC_MENU_DEEP, Integer.valueOf(R.drawable.icon_music_queue_sm));
    }
    
    public MediaControllerLayout(android.content.Context a) {
        super(a);
    }
    
    public MediaControllerLayout(android.content.Context a, android.util.AttributeSet a0) {
        super(a, a0);
    }
    
    public MediaControllerLayout(android.content.Context a, android.util.AttributeSet a0, int i) {
        super(a, a0, i);
    }
    
    private java.util.List getChoicesForControls() {
        java.util.ArrayList a = new java.util.ArrayList();
        if (com.navdy.hud.app.ui.component.UISettings.isMusicBrowsingEnabled()) {
            int i = (com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getMusicManager().getMusicMenuPath() == null) ? ((Integer)MEDIA_CONTROL_ICON_MAPPING.get(com.navdy.hud.app.manager.MusicManager$MediaControl.MUSIC_MENU)).intValue() : ((Integer)MEDIA_CONTROL_ICON_MAPPING.get(com.navdy.hud.app.manager.MusicManager$MediaControl.MUSIC_MENU_DEEP)).intValue();
            ((java.util.List)a).add(new com.navdy.hud.app.ui.component.ChoiceLayout$Choice(new com.navdy.hud.app.ui.component.ChoiceLayout$Icon(i, i), com.navdy.hud.app.manager.MusicManager$MediaControl.MUSIC_MENU.ordinal()));
        }
        if (this.availableMediaControls.contains(com.navdy.hud.app.manager.MusicManager$MediaControl.PREVIOUS)) {
            int i0 = ((Integer)MEDIA_CONTROL_ICON_MAPPING.get(com.navdy.hud.app.manager.MusicManager$MediaControl.PREVIOUS)).intValue();
            ((java.util.List)a).add(new com.navdy.hud.app.ui.component.ChoiceLayout$Choice(new com.navdy.hud.app.ui.component.ChoiceLayout$Icon(i0, i0), com.navdy.hud.app.manager.MusicManager$MediaControl.PREVIOUS.ordinal()));
        }
        com.navdy.hud.app.manager.MusicManager$MediaControl a0 = (this.availableMediaControls.contains(com.navdy.hud.app.manager.MusicManager$MediaControl.PLAY)) ? com.navdy.hud.app.manager.MusicManager$MediaControl.PLAY : com.navdy.hud.app.manager.MusicManager$MediaControl.PAUSE;
        int i1 = ((Integer)MEDIA_CONTROL_ICON_MAPPING.get(a0)).intValue();
        ((java.util.List)a).add(new com.navdy.hud.app.ui.component.ChoiceLayout$Choice(new com.navdy.hud.app.ui.component.ChoiceLayout$Icon(i1, i1), a0.ordinal()));
        if (this.availableMediaControls.contains(com.navdy.hud.app.manager.MusicManager$MediaControl.NEXT)) {
            int i2 = ((Integer)MEDIA_CONTROL_ICON_MAPPING.get(com.navdy.hud.app.manager.MusicManager$MediaControl.NEXT)).intValue();
            ((java.util.List)a).add(new com.navdy.hud.app.ui.component.ChoiceLayout$Choice(new com.navdy.hud.app.ui.component.ChoiceLayout$Icon(i2, i2), com.navdy.hud.app.manager.MusicManager$MediaControl.NEXT.ordinal()));
        }
        return (java.util.List)a;
    }
    
    private int getDefaultSelection(java.util.List a) {
        Object a0 = a.iterator();
        int i = 0;
        while(((java.util.Iterator)a0).hasNext()) {
            com.navdy.hud.app.ui.component.ChoiceLayout$Choice a1 = (com.navdy.hud.app.ui.component.ChoiceLayout$Choice)((java.util.Iterator)a0).next();
            int i0 = a1.id;
            int i1 = com.navdy.hud.app.manager.MusicManager$MediaControl.MUSIC_MENU.ordinal();
            {
                if (i0 != i1 && a1.id != com.navdy.hud.app.manager.MusicManager$MediaControl.PREVIOUS.ordinal()) {
                    continue;
                }
                i = i + 1;
            }
        }
        return i;
    }
    
    private void updateChoices(java.util.List a) {
        this.choices = a;
        int i = 0;
        while(i < this.choices.size()) {
            com.navdy.hud.app.ui.component.ChoiceLayout$Choice a0 = (com.navdy.hud.app.ui.component.ChoiceLayout$Choice)this.choices.get(i);
            ((android.widget.ImageView)this.choiceContainer.getChildAt(i)).setImageResource(a0.icon.resIdSelected);
            i = i + 1;
        }
    }
    
    protected boolean isHighlightPersistent() {
        return true;
    }
    
    public void updateControls(java.util.Set a) {
        label2: {
            boolean b = false;
            label3: {
                label4: {
                    if (a == null) {
                        break label4;
                    }
                    if (a.size() != 0) {
                        break label3;
                    }
                }
                this.availableMediaControls = a;
                this.setVisibility(4);
                break label2;
            }
            java.util.Set a0 = this.availableMediaControls;
            label0: {
                label1: {
                    if (a0 == null) {
                        break label1;
                    }
                    if (this.availableMediaControls.size() == a.size()) {
                        break label0;
                    }
                }
                this.availableMediaControls = a;
                java.util.List a1 = this.getChoicesForControls();
                this.setChoices(com.navdy.hud.app.ui.component.ChoiceLayout$Mode.ICON, a1, this.getDefaultSelection(a1), (com.navdy.hud.app.ui.component.ChoiceLayout$IListener)null);
                this.setHighlightVisibility(0);
                this.setVisibility(0);
                break label2;
            }
            com.navdy.hud.app.manager.MusicManager$MediaControl[] a2 = com.navdy.hud.app.manager.MusicManager.CONTROLS;
            int i = a2.length;
            Object a3 = a;
            int i0 = 0;
            while(true) {
                if (i0 >= i) {
                    b = false;
                    break;
                } else {
                    com.navdy.hud.app.manager.MusicManager$MediaControl a4 = a2[i0];
                    if (((java.util.Set)a3).contains(a4) == this.availableMediaControls.contains(a4)) {
                        i0 = i0 + 1;
                        continue;
                    }
                    this.availableMediaControls = (java.util.Set)a3;
                    b = true;
                    break;
                }
            }
            java.util.List a5 = this.getChoicesForControls();
            if (b) {
                this.updateChoices(a5);
            } else {
                int i1 = this.getSelectedItemIndex();
                if (com.navdy.hud.app.ui.component.UISettings.isMusicBrowsingEnabled() && i1 == 0) {
                    this.setSelectedItem(this.getDefaultSelection(a5));
                }
            }
        }
    }
}
