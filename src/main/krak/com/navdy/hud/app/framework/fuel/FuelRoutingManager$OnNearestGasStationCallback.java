package com.navdy.hud.app.framework.fuel;

abstract public interface FuelRoutingManager$OnNearestGasStationCallback {
    abstract public void onComplete(com.navdy.service.library.events.navigation.NavigationRouteResult arg);
    
    
    abstract public void onError(com.navdy.hud.app.framework.fuel.FuelRoutingManager$OnRouteToGasStationCallback$Error arg);
}
