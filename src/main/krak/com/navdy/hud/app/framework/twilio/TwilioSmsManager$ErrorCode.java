package com.navdy.hud.app.framework.twilio;


public enum TwilioSmsManager$ErrorCode {
    SUCCESS(0),
    INTERNAL_ERROR(1),
    NETWORK_ERROR(2),
    TWILIO_SERVER_ERROR(3),
    INVALID_PARAMETER(4);

    private int value;
    TwilioSmsManager$ErrorCode(int value) {
        this.value = value;
    }
    public int getValue() {
        return value;
    }
}

//final public class TwilioSmsManager$ErrorCode extends Enum {
//    final private static com.navdy.hud.app.framework.twilio.TwilioSmsManager$ErrorCode[] $VALUES;
//    final public static com.navdy.hud.app.framework.twilio.TwilioSmsManager$ErrorCode INTERNAL_ERROR;
//    final public static com.navdy.hud.app.framework.twilio.TwilioSmsManager$ErrorCode INVALID_PARAMETER;
//    final public static com.navdy.hud.app.framework.twilio.TwilioSmsManager$ErrorCode NETWORK_ERROR;
//    final public static com.navdy.hud.app.framework.twilio.TwilioSmsManager$ErrorCode SUCCESS;
//    final public static com.navdy.hud.app.framework.twilio.TwilioSmsManager$ErrorCode TWILIO_SERVER_ERROR;
//    
//    static {
//        SUCCESS = new com.navdy.hud.app.framework.twilio.TwilioSmsManager$ErrorCode("SUCCESS", 0);
//        INTERNAL_ERROR = new com.navdy.hud.app.framework.twilio.TwilioSmsManager$ErrorCode("INTERNAL_ERROR", 1);
//        NETWORK_ERROR = new com.navdy.hud.app.framework.twilio.TwilioSmsManager$ErrorCode("NETWORK_ERROR", 2);
//        TWILIO_SERVER_ERROR = new com.navdy.hud.app.framework.twilio.TwilioSmsManager$ErrorCode("TWILIO_SERVER_ERROR", 3);
//        INVALID_PARAMETER = new com.navdy.hud.app.framework.twilio.TwilioSmsManager$ErrorCode("INVALID_PARAMETER", 4);
//        com.navdy.hud.app.framework.twilio.TwilioSmsManager$ErrorCode[] a = new com.navdy.hud.app.framework.twilio.TwilioSmsManager$ErrorCode[5];
//        a[0] = SUCCESS;
//        a[1] = INTERNAL_ERROR;
//        a[2] = NETWORK_ERROR;
//        a[3] = TWILIO_SERVER_ERROR;
//        a[4] = INVALID_PARAMETER;
//        $VALUES = a;
//    }
//    
//    private TwilioSmsManager$ErrorCode(String s, int i) {
//        super(s, i);
//    }
//    
//    public static com.navdy.hud.app.framework.twilio.TwilioSmsManager$ErrorCode valueOf(String s) {
//        return (com.navdy.hud.app.framework.twilio.TwilioSmsManager$ErrorCode)Enum.valueOf(com.navdy.hud.app.framework.twilio.TwilioSmsManager$ErrorCode.class, s);
//    }
//    
//    public static com.navdy.hud.app.framework.twilio.TwilioSmsManager$ErrorCode[] values() {
//        return $VALUES.clone();
//    }
//}
//