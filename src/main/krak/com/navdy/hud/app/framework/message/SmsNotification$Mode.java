package com.navdy.hud.app.framework.message;


public enum SmsNotification$Mode {
    Success(0),
    Failed(1);

    private int value;
    SmsNotification$Mode(int value) {
        this.value = value;
    }
    public int getValue() {
        return value;
    }
}

//final public class SmsNotification$Mode extends Enum {
//    final private static com.navdy.hud.app.framework.message.SmsNotification$Mode[] $VALUES;
//    final public static com.navdy.hud.app.framework.message.SmsNotification$Mode Failed;
//    final public static com.navdy.hud.app.framework.message.SmsNotification$Mode Success;
//    
//    static {
//        Success = new com.navdy.hud.app.framework.message.SmsNotification$Mode("Success", 0);
//        Failed = new com.navdy.hud.app.framework.message.SmsNotification$Mode("Failed", 1);
//        com.navdy.hud.app.framework.message.SmsNotification$Mode[] a = new com.navdy.hud.app.framework.message.SmsNotification$Mode[2];
//        a[0] = Success;
//        a[1] = Failed;
//        $VALUES = a;
//    }
//    
//    private SmsNotification$Mode(String s, int i) {
//        super(s, i);
//    }
//    
//    public static com.navdy.hud.app.framework.message.SmsNotification$Mode valueOf(String s) {
//        return (com.navdy.hud.app.framework.message.SmsNotification$Mode)Enum.valueOf(com.navdy.hud.app.framework.message.SmsNotification$Mode.class, s);
//    }
//    
//    public static com.navdy.hud.app.framework.message.SmsNotification$Mode[] values() {
//        return $VALUES.clone();
//    }
//}
//