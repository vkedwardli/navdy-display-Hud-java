package com.navdy.hud.app.framework.fuel;


public enum FuelRoutingManager$OnRouteToGasStationCallback$Error {
    BAD_REQUEST(0),
    RESPONSE_ERROR(1),
    NO_USER_LOCATION(2),
    NO_ROUTES(3),
    UNKNOWN_ERROR(4),
    INVALID_STATE(5);

    private int value;
    FuelRoutingManager$OnRouteToGasStationCallback$Error(int value) {
        this.value = value;
    }
    public int getValue() {
        return value;
    }
}

//final public class FuelRoutingManager$OnRouteToGasStationCallback$Error extends Enum {
//    final private static com.navdy.hud.app.framework.fuel.FuelRoutingManager$OnRouteToGasStationCallback$Error[] $VALUES;
//    final public static com.navdy.hud.app.framework.fuel.FuelRoutingManager$OnRouteToGasStationCallback$Error BAD_REQUEST;
//    final public static com.navdy.hud.app.framework.fuel.FuelRoutingManager$OnRouteToGasStationCallback$Error INVALID_STATE;
//    final public static com.navdy.hud.app.framework.fuel.FuelRoutingManager$OnRouteToGasStationCallback$Error NO_ROUTES;
//    final public static com.navdy.hud.app.framework.fuel.FuelRoutingManager$OnRouteToGasStationCallback$Error NO_USER_LOCATION;
//    final public static com.navdy.hud.app.framework.fuel.FuelRoutingManager$OnRouteToGasStationCallback$Error RESPONSE_ERROR;
//    final public static com.navdy.hud.app.framework.fuel.FuelRoutingManager$OnRouteToGasStationCallback$Error UNKNOWN_ERROR;
//    
//    static {
//        BAD_REQUEST = new com.navdy.hud.app.framework.fuel.FuelRoutingManager$OnRouteToGasStationCallback$Error("BAD_REQUEST", 0);
//        RESPONSE_ERROR = new com.navdy.hud.app.framework.fuel.FuelRoutingManager$OnRouteToGasStationCallback$Error("RESPONSE_ERROR", 1);
//        NO_USER_LOCATION = new com.navdy.hud.app.framework.fuel.FuelRoutingManager$OnRouteToGasStationCallback$Error("NO_USER_LOCATION", 2);
//        NO_ROUTES = new com.navdy.hud.app.framework.fuel.FuelRoutingManager$OnRouteToGasStationCallback$Error("NO_ROUTES", 3);
//        UNKNOWN_ERROR = new com.navdy.hud.app.framework.fuel.FuelRoutingManager$OnRouteToGasStationCallback$Error("UNKNOWN_ERROR", 4);
//        INVALID_STATE = new com.navdy.hud.app.framework.fuel.FuelRoutingManager$OnRouteToGasStationCallback$Error("INVALID_STATE", 5);
//        com.navdy.hud.app.framework.fuel.FuelRoutingManager$OnRouteToGasStationCallback$Error[] a = new com.navdy.hud.app.framework.fuel.FuelRoutingManager$OnRouteToGasStationCallback$Error[6];
//        a[0] = BAD_REQUEST;
//        a[1] = RESPONSE_ERROR;
//        a[2] = NO_USER_LOCATION;
//        a[3] = NO_ROUTES;
//        a[4] = UNKNOWN_ERROR;
//        a[5] = INVALID_STATE;
//        $VALUES = a;
//    }
//    
//    private FuelRoutingManager$OnRouteToGasStationCallback$Error(String s, int i) {
//        super(s, i);
//    }
//    
//    public static com.navdy.hud.app.framework.fuel.FuelRoutingManager$OnRouteToGasStationCallback$Error valueOf(String s) {
//        return (com.navdy.hud.app.framework.fuel.FuelRoutingManager$OnRouteToGasStationCallback$Error)Enum.valueOf(com.navdy.hud.app.framework.fuel.FuelRoutingManager$OnRouteToGasStationCallback$Error.class, s);
//    }
//    
//    public static com.navdy.hud.app.framework.fuel.FuelRoutingManager$OnRouteToGasStationCallback$Error[] values() {
//        return $VALUES.clone();
//    }
//}
//