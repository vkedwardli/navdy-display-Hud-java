package com.navdy.hud.app.framework.fuel;

class FuelRoutingManager$4$1 implements java.util.Comparator {
    final com.navdy.hud.app.framework.fuel.FuelRoutingManager$4 this$1;
    
    FuelRoutingManager$4$1(com.navdy.hud.app.framework.fuel.FuelRoutingManager$4 a) {
        super();
        this.this$1 = a;
    }
    
    public int compare(com.here.android.mpa.search.Place a, com.here.android.mpa.search.Place a0) {
        return (int)(this.this$1.val$gasCalculationCoords.distanceTo(a.getLocation().getCoordinate()) - this.this$1.val$gasCalculationCoords.distanceTo(a0.getLocation().getCoordinate()));
    }
    
    public int compare(Object a, Object a0) {
        return this.compare((com.here.android.mpa.search.Place)a, (com.here.android.mpa.search.Place)a0);
    }
}
