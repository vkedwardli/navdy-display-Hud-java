package com.navdy.hud.app.framework.message;
import com.navdy.hud.app.R;

public class SmsNotification implements com.navdy.hud.app.framework.notifications.INotification, com.navdy.hud.app.ui.component.ChoiceLayout2$IListener, com.navdy.hud.app.framework.contacts.ContactUtil$IContactCallback {
    final private static int NOTIFICATION_TIMEOUT;
    final private static int TIMEOUT;
    final private static java.util.List choicesDismiss;
    final private static java.util.List choicesRetryAndDismiss;
    final private static android.os.Handler handler;
    final private static android.content.res.Resources resources;
    final private static com.navdy.service.library.log.Logger sLogger;
    @InjectView(R.id.choice_layout)
    com.navdy.hud.app.ui.component.ChoiceLayout2 choiceLayout;
    private com.navdy.hud.app.framework.notifications.INotificationController controller;
    final private String message;
    final private com.navdy.hud.app.framework.message.SmsNotification$Mode mode;
    final private String name;
    final private String notifId;
    @InjectView(R.id.notification_user_image)
    com.navdy.hud.app.ui.component.image.InitialsImageView notificationUserImage;
    final private String number;
    final private String rawNumber;
    private boolean retrySendingMessage;
    private com.squareup.picasso.Transformation roundTransformation;
    @InjectView(R.id.badge)
    android.widget.ImageView sideImage;
    @InjectView(R.id.subtitle)
    android.widget.TextView subtitle;
    @InjectView(R.id.title)
    android.widget.TextView title;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.framework.message.SmsNotification.class);
        TIMEOUT = (int)java.util.concurrent.TimeUnit.SECONDS.toMillis(10L);
        NOTIFICATION_TIMEOUT = (int)java.util.concurrent.TimeUnit.SECONDS.toMillis(5L);
        resources = com.navdy.hud.app.HudApplication.getAppContext().getResources();
        handler = new android.os.Handler(android.os.Looper.getMainLooper());
        String s = resources.getString(R.string.dismiss);
        int i = resources.getColor(R.color.glance_dismiss);
        int i0 = resources.getColor(R.color.glance_ok_blue);
        choicesRetryAndDismiss = (java.util.List)new java.util.ArrayList();
        choicesRetryAndDismiss.add(new com.navdy.hud.app.ui.component.ChoiceLayout2$Choice(R.id.retry, R.drawable.icon_glances_retry, i0, R.drawable.icon_glances_retry, -16777216, resources.getString(R.string.retry), i0));
        choicesRetryAndDismiss.add(new com.navdy.hud.app.ui.component.ChoiceLayout2$Choice(R.id.dismiss, R.drawable.icon_glances_dismiss, i, R.drawable.icon_glances_dismiss, -16777216, s, i));
        choicesDismiss = (java.util.List)new java.util.ArrayList();
        choicesDismiss.add(new com.navdy.hud.app.ui.component.ChoiceLayout2$Choice(R.id.dismiss, R.drawable.icon_glances_dismiss, i, R.drawable.icon_glances_dismiss, -16777216, s, i));
    }
    
    public SmsNotification(com.navdy.hud.app.framework.message.SmsNotification$Mode a, String s, String s0, String s1, String s2) {
        this.roundTransformation = new com.makeramen.RoundedTransformationBuilder().oval(true).build();
        this.mode = a;
        this.notifId = new StringBuilder().append("navdy#sms#notif#").append(s).toString();
        this.number = com.navdy.hud.app.util.PhoneUtil.formatPhoneNumber(s0);
        this.rawNumber = com.navdy.hud.app.util.PhoneUtil.convertToE164Format(s0);
        this.message = s1;
        this.name = s2;
    }
    
    static String access$000(com.navdy.hud.app.framework.message.SmsNotification a) {
        return a.rawNumber;
    }
    
    static String access$100(com.navdy.hud.app.framework.message.SmsNotification a) {
        return a.message;
    }
    
    static String access$200(com.navdy.hud.app.framework.message.SmsNotification a) {
        return a.name;
    }
    
    static com.navdy.service.library.log.Logger access$300() {
        return sLogger;
    }
    
    static String access$400(com.navdy.hud.app.framework.message.SmsNotification a) {
        return a.number;
    }
    
    static String access$500(com.navdy.hud.app.framework.message.SmsNotification a) {
        return a.notifId;
    }
    
    private String getContactName() {
        return (android.text.TextUtils.isEmpty((CharSequence)this.name)) ? (android.text.TextUtils.isEmpty((CharSequence)this.number)) ? "" : this.number : this.name;
    }
    
    private void setUI() {
        if (android.text.TextUtils.isEmpty((CharSequence)this.name)) {
            if (android.text.TextUtils.isEmpty((CharSequence)this.number)) {
                this.subtitle.setText((CharSequence)"");
            } else {
                this.subtitle.setText((CharSequence)com.navdy.hud.app.util.PhoneUtil.formatPhoneNumber(this.number));
            }
        } else {
            this.subtitle.setText((CharSequence)this.name);
        }
        com.navdy.hud.app.framework.contacts.ContactUtil.setContactPhoto(this.name, this.number, false, this.notificationUserImage, this.roundTransformation, (com.navdy.hud.app.framework.contacts.ContactUtil$IContactCallback)this);
        switch(com.navdy.hud.app.framework.message.SmsNotification$2.$SwitchMap$com$navdy$hud$app$framework$message$SmsNotification$Mode[this.mode.ordinal()]) {
            case 2: {
                this.title.setText((CharSequence)resources.getString(R.string.reply_sent));
                this.subtitle.setText((CharSequence)this.getContactName());
                this.sideImage.setImageResource(R.drawable.icon_msg_success);
                this.choiceLayout.setChoices(choicesDismiss, 0, (com.navdy.hud.app.ui.component.ChoiceLayout2$IListener)this);
                break;
            }
            case 1: {
                this.title.setText((CharSequence)resources.getString(R.string.reply_failed));
                this.subtitle.setText((CharSequence)this.getContactName());
                this.sideImage.setImageResource(R.drawable.icon_msg_failed);
                this.choiceLayout.setChoices(choicesRetryAndDismiss, 0, (com.navdy.hud.app.ui.component.ChoiceLayout2$IListener)this);
                break;
            }
        }
    }
    
    public boolean canAddToStackIfCurrentExists() {
        return true;
    }
    
    public void executeItem(com.navdy.hud.app.ui.component.ChoiceLayout2$Selection a) {
        switch(a.id) {
            case R.id.retry: {
                this.retrySendingMessage = true;
                com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().removeNotification(this.notifId);
                break;
            }
            case R.id.dismiss: {
                com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().removeNotification(this.notifId);
                break;
            }
        }
    }
    
    public boolean expandNotification() {
        return false;
    }
    
    public int getColor() {
        return 0;
    }
    
    public android.view.View getExpandedView(android.content.Context a, Object a0) {
        return null;
    }
    
    public int getExpandedViewIndicatorColor() {
        return 0;
    }
    
    public String getId() {
        return this.notifId;
    }
    
    public int getTimeout() {
        return (com.navdy.hud.app.framework.message.SmsNotification$2.$SwitchMap$com$navdy$hud$app$framework$message$SmsNotification$Mode[this.mode.ordinal()] != 0) ? TIMEOUT : NOTIFICATION_TIMEOUT;
    }
    
    public com.navdy.hud.app.framework.notifications.NotificationType getType() {
        return com.navdy.hud.app.framework.notifications.NotificationType.SMS;
    }
    
    public android.view.View getView(android.content.Context a) {
        android.view.View a0 = android.view.LayoutInflater.from(a).inflate(R.layout.notification_sms, (android.view.ViewGroup)null);
        butterknife.ButterKnife.inject(this, a0);
        return a0;
    }
    
    public android.animation.AnimatorSet getViewSwitchAnimation(boolean b) {
        return null;
    }
    
    public boolean isAlive() {
        return false;
    }
    
    public boolean isContextValid() {
        return this.controller != null;
    }
    
    public boolean isPurgeable() {
        return false;
    }
    
    public void itemSelected(com.navdy.hud.app.ui.component.ChoiceLayout2$Selection a) {
    }
    
    public com.navdy.hud.app.manager.InputManager$IInputHandler nextHandler() {
        return null;
    }
    
    public void onClick() {
    }
    
    public void onExpandedNotificationEvent(com.navdy.hud.app.ui.framework.UIStateManager$Mode a) {
    }
    
    public void onExpandedNotificationSwitched() {
    }
    
    public boolean onGesture(com.navdy.service.library.events.input.GestureEvent a) {
        return false;
    }
    
    public boolean onKey(com.navdy.hud.app.manager.InputManager$CustomKeyEvent a) {
        boolean b = false;
        if (this.controller != null) {
            switch(com.navdy.hud.app.framework.message.SmsNotification$2.$SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent[a.ordinal()]) {
                case 3: {
                    this.choiceLayout.executeSelectedItem();
                    b = true;
                    break;
                }
                case 2: {
                    this.choiceLayout.moveSelectionRight();
                    this.controller.resetTimeout();
                    b = true;
                    break;
                }
                case 1: {
                    this.choiceLayout.moveSelectionLeft();
                    this.controller.resetTimeout();
                    b = true;
                    break;
                }
                default: {
                    b = false;
                }
            }
        } else {
            b = false;
        }
        return b;
    }
    
    public void onNotificationEvent(com.navdy.hud.app.ui.framework.UIStateManager$Mode a) {
    }
    
    public void onStart(com.navdy.hud.app.framework.notifications.INotificationController a) {
        this.controller = a;
        this.setUI();
    }
    
    public void onStop() {
        this.controller = null;
        if (this.retrySendingMessage) {
            handler.post((Runnable)new com.navdy.hud.app.framework.message.SmsNotification$1(this));
        }
    }
    
    public void onTrackHand(float f) {
    }
    
    public void onUpdate() {
    }
    
    public boolean supportScroll() {
        return false;
    }
}
