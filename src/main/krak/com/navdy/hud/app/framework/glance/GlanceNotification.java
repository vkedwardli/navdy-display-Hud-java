package com.navdy.hud.app.framework.glance;
import com.navdy.hud.app.R;

public class GlanceNotification implements com.navdy.hud.app.framework.notifications.INotification, com.navdy.hud.app.framework.notifications.IScrollEvent, com.navdy.hud.app.framework.contacts.ContactUtil$IContactCallback {
    final private static float IMAGE_SCALE = 0.5f;
    private static android.os.Handler handler;
    final private static com.navdy.service.library.log.Logger sLogger;
    private boolean alive;
    private int appIcon;
    private android.widget.ImageView audioFeedback;
    private android.view.View bottomScrub;
    private com.squareup.otto.Bus bus;
    private com.navdy.service.library.events.audio.CancelSpeechRequest cancelSpeechRequest;
    private java.util.List cannedReplyMessages;
    private com.navdy.hud.app.ui.component.ChoiceLayout2 choiceLayout;
    private com.navdy.hud.app.ui.component.ChoiceLayout2$IListener choiceListener;
    final private int color;
    private com.navdy.hud.app.ui.component.image.ColorImageView colorImageView;
    private java.util.List contacts;
    private com.navdy.hud.app.framework.notifications.INotificationController controller;
    final private java.util.Map data;
    final private com.navdy.hud.app.framework.glance.GlanceApp glanceApp;
    private android.view.ViewGroup glanceContainer;
    final private com.navdy.service.library.events.glances.GlanceEvent glanceEvent;
    private android.view.ViewGroup glanceExtendedContainer;
    private boolean hasFuelLevelInfo;
    final private String id;
    private boolean initialReplyMode;
    private com.navdy.hud.app.framework.glance.GlanceViewCache$ViewType largeType;
    private com.navdy.hud.app.ui.component.image.InitialsImageView mainImage;
    private android.widget.TextView mainTitle;
    private String messageStr;
    private String number;
    private boolean onBottom;
    private boolean onTop;
    private com.navdy.hud.app.framework.glance.GlanceNotification$Mode operationMode;
    private boolean photoCheckRequired;
    final private java.util.Date postTime;
    private com.navdy.hud.app.framework.notifications.IProgressUpdate progressUpdate;
    private android.view.ViewGroup replyExitView;
    private android.view.ViewGroup replyMsgView;
    private com.squareup.picasso.Transformation roundTransformation;
    private com.navdy.hud.app.view.ObservableScrollView$IScrollListener scrollListener;
    private com.navdy.hud.app.view.ObservableScrollView scrollView;
    private android.widget.ImageView sideImage;
    private com.navdy.hud.app.framework.glance.GlanceViewCache$ViewType smallType;
    private String source;
    private StringBuilder stringBuilder1;
    private StringBuilder stringBuilder2;
    private android.widget.TextView subTitle;
    final private boolean supportsScroll;
    private android.widget.TextView text1;
    private android.widget.TextView text2;
    private android.widget.TextView text3;
    private com.navdy.hud.app.common.TimeHelper timeHelper;
    private android.view.View topScrub;
    private String ttsMessage;
    private boolean ttsSent;
    private Runnable updateTimeRunnable;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.framework.glance.GlanceNotification.class);
        handler = new android.os.Handler(android.os.Looper.getMainLooper());
    }
    
    public GlanceNotification(com.navdy.service.library.events.glances.GlanceEvent a, com.navdy.hud.app.framework.glance.GlanceApp a0, com.navdy.hud.app.framework.glance.GlanceViewCache$ViewType a1, java.util.Map a2) {
        this.stringBuilder1 = new StringBuilder();
        this.stringBuilder2 = new StringBuilder();
        this.roundTransformation = new com.makeramen.RoundedTransformationBuilder().oval(true).build();
        this.alive = true;
        this.updateTimeRunnable = (Runnable)new com.navdy.hud.app.framework.glance.GlanceNotification$1(this);
        this.choiceListener = (com.navdy.hud.app.ui.component.ChoiceLayout2$IListener)new com.navdy.hud.app.framework.glance.GlanceNotification$2(this);
        this.scrollListener = (com.navdy.hud.app.view.ObservableScrollView$IScrollListener)new com.navdy.hud.app.framework.glance.GlanceNotification$3(this);
        this.glanceEvent = a;
        this.glanceApp = a0;
        this.id = com.navdy.hud.app.framework.glance.GlanceHelper.getNotificationId(this.glanceEvent);
        this.color = this.glanceApp.getColor();
        this.appIcon = this.glanceApp.getSideIcon();
        if (a2 != null) {
            this.data = a2;
        } else {
            this.data = com.navdy.hud.app.framework.glance.GlanceHelper.buildDataMap(a);
        }
        switch(com.navdy.hud.app.framework.glance.GlanceNotification$5.$SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp[a0.ordinal()]) {
            case 19: {
                String s = (String)this.data.get(com.navdy.service.library.events.glances.GenericConstants.GENERIC_SIDE_ICON.name());
                if (android.text.TextUtils.isEmpty((CharSequence)s)) {
                    break;
                }
                int i = com.navdy.hud.app.framework.glance.GlanceHelper.getIcon(s);
                if (i == -1) {
                    break;
                }
                this.appIcon = i;
                break;
            }
            case 8: {
                this.hasFuelLevelInfo = this.data.containsKey(com.navdy.service.library.events.glances.FuelConstants.FUEL_LEVEL);
                break;
            }
        }
        this.photoCheckRequired = com.navdy.hud.app.framework.glance.GlanceHelper.isPhotoCheckRequired(this.glanceApp);
        this.postTime = new java.util.Date(a.postTime.longValue());
        this.timeHelper = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getTimeHelper();
        this.smallType = com.navdy.hud.app.framework.glance.GlanceHelper.getSmallViewType(this.glanceApp);
        if (a1 != null) {
            this.largeType = a1;
        } else {
            this.largeType = com.navdy.hud.app.framework.glance.GlanceHelper.getLargeViewType(this.glanceApp);
        }
        this.ttsMessage = com.navdy.hud.app.framework.glance.GlanceHelper.getTtsMessage(this.glanceApp, this.data, this.stringBuilder1, this.stringBuilder2, this.timeHelper);
        this.messageStr = com.navdy.hud.app.framework.glance.GlanceHelper.getGlanceMessage(this.glanceApp, this.data);
        this.cancelSpeechRequest = new com.navdy.service.library.events.audio.CancelSpeechRequest(this.id);
        this.supportsScroll = com.navdy.hud.app.framework.glance.GlanceViewCache.supportScroll(this.largeType);
        this.bus = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getBus();
    }
    
    public GlanceNotification(com.navdy.service.library.events.glances.GlanceEvent a, com.navdy.hud.app.framework.glance.GlanceApp a0, java.util.Map a1) {
        this(a, a0, (com.navdy.hud.app.framework.glance.GlanceViewCache$ViewType)null, a1);
    }
    
    static com.navdy.hud.app.framework.notifications.INotificationController access$000(com.navdy.hud.app.framework.glance.GlanceNotification a) {
        return a.controller;
    }
    
    static android.view.ViewGroup access$100(com.navdy.hud.app.framework.glance.GlanceNotification a) {
        return a.glanceExtendedContainer;
    }
    
    static com.navdy.service.library.log.Logger access$1000() {
        return sLogger;
    }
    
    static Runnable access$1100(com.navdy.hud.app.framework.glance.GlanceNotification a) {
        return a.updateTimeRunnable;
    }
    
    static android.os.Handler access$1200() {
        return handler;
    }
    
    static void access$1300(com.navdy.hud.app.framework.glance.GlanceNotification a) {
        a.switchToReplyScreen();
    }
    
    static void access$1400(com.navdy.hud.app.framework.glance.GlanceNotification a, com.navdy.hud.app.framework.glance.GlanceNotification$Mode a0, String s) {
        a.switchToMode(a0, s);
    }
    
    static void access$1500(com.navdy.hud.app.framework.glance.GlanceNotification a) {
        a.dismissNotification();
    }
    
    static void access$1600(com.navdy.hud.app.framework.glance.GlanceNotification a) {
        a.call();
    }
    
    static String access$1700(com.navdy.hud.app.framework.glance.GlanceNotification a) {
        return a.number;
    }
    
    static com.navdy.hud.app.ui.component.ChoiceLayout2 access$1800(com.navdy.hud.app.framework.glance.GlanceNotification a) {
        return a.choiceLayout;
    }
    
    static void access$1900(com.navdy.hud.app.framework.glance.GlanceNotification a) {
        a.revertChoice();
    }
    
    static com.navdy.hud.app.framework.glance.GlanceApp access$200(com.navdy.hud.app.framework.glance.GlanceNotification a) {
        return a.glanceApp;
    }
    
    static boolean access$2000(com.navdy.hud.app.framework.glance.GlanceNotification a) {
        return a.supportsScroll;
    }
    
    static com.navdy.hud.app.view.ObservableScrollView access$2100(com.navdy.hud.app.framework.glance.GlanceNotification a) {
        return a.scrollView;
    }
    
    static android.view.View access$2200(com.navdy.hud.app.framework.glance.GlanceNotification a) {
        return a.topScrub;
    }
    
    static boolean access$2302(com.navdy.hud.app.framework.glance.GlanceNotification a, boolean b) {
        a.onTop = b;
        return b;
    }
    
    static boolean access$2402(com.navdy.hud.app.framework.glance.GlanceNotification a, boolean b) {
        a.onBottom = b;
        return b;
    }
    
    static com.navdy.hud.app.framework.notifications.IProgressUpdate access$2500(com.navdy.hud.app.framework.glance.GlanceNotification a) {
        return a.progressUpdate;
    }
    
    static android.widget.ImageView access$2600(com.navdy.hud.app.framework.glance.GlanceNotification a) {
        return a.audioFeedback;
    }
    
    static void access$300(com.navdy.hud.app.framework.glance.GlanceNotification a) {
        a.setMainImage();
    }
    
    static boolean access$400(com.navdy.hud.app.framework.glance.GlanceNotification a) {
        return a.hasFuelLevelInfo;
    }
    
    static java.util.Map access$500(com.navdy.hud.app.framework.glance.GlanceNotification a) {
        return a.data;
    }
    
    static void access$600(com.navdy.hud.app.framework.glance.GlanceNotification a) {
        a.setTitle();
    }
    
    static void access$700(com.navdy.hud.app.framework.glance.GlanceNotification a, android.view.View a0) {
        a.setExpandedContent(a0);
    }
    
    static java.util.Date access$800(com.navdy.hud.app.framework.glance.GlanceNotification a) {
        return a.postTime;
    }
    
    static com.navdy.hud.app.common.TimeHelper access$900(com.navdy.hud.app.framework.glance.GlanceNotification a) {
        return a.timeHelper;
    }
    
    private void call() {
        this.dismissNotification();
        com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getCallManager().dial(this.number, (String)null, this.source);
    }
    
    private void cancelTts() {
        if (this.controller != null && this.controller.isTtsOn() && this.ttsSent) {
            sLogger.v(new StringBuilder().append("tts-cancelled [").append(this.id).append("]").toString());
            this.bus.post(new com.navdy.hud.app.event.RemoteEvent((com.squareup.wire.Message)this.cancelSpeechRequest));
            this.ttsSent = false;
            this.stopAudioAnimation(false);
        }
    }
    
    private void cleanupView(com.navdy.hud.app.framework.glance.GlanceViewCache$ViewType a, android.view.ViewGroup a0) {
        android.view.ViewGroup a1 = (android.view.ViewGroup)a0.getParent();
        if (a1 != null) {
            a1.removeView((android.view.View)a0);
        }
        switch(com.navdy.hud.app.framework.glance.GlanceNotification$5.$SwitchMap$com$navdy$hud$app$framework$glance$GlanceViewCache$ViewType[a.ordinal()]) {
            case 6: {
                this.mainTitle.setAlpha(1f);
                this.mainTitle.setVisibility(0);
                this.subTitle.setAlpha(1f);
                this.subTitle.setVisibility(0);
                this.choiceLayout.setAlpha(1f);
                this.choiceLayout.setVisibility(0);
                this.choiceLayout.setTag(null);
                this.choiceLayout.setTag(R.id.message_prev_choice, null);
                this.choiceLayout.setTag(R.id.message_secondary_screen, null);
                this.sideImage.setImageResource(0);
                a0.setAlpha(1f);
                break;
            }
            case 3: {
                ((android.widget.TextView)a0.findViewById(R.id.title)).setTextAppearance(com.navdy.hud.app.HudApplication.getAppContext(), R.style.glance_message_title);
            }
            case 4: case 5: {
                a0.setAlpha(1f);
                break;
            }
            case 2: {
                a0.setAlpha(1f);
                if (this.scrollView == null) {
                    break;
                }
                this.scrollView.fullScroll(33);
                this.onTop = true;
                this.onBottom = false;
                break;
            }
            case 1: {
                this.mainTitle.setAlpha(1f);
                this.mainTitle.setVisibility(0);
                this.subTitle.setAlpha(1f);
                this.subTitle.setVisibility(0);
                this.choiceLayout.setAlpha(1f);
                this.choiceLayout.setVisibility(0);
                this.choiceLayout.setTag(null);
                this.choiceLayout.setTag(R.id.message_prev_choice, null);
                this.choiceLayout.setTag(R.id.message_secondary_screen, null);
                this.mainImage.setImage(0, (String)null, com.navdy.hud.app.ui.component.image.InitialsImageView$Style.DEFAULT);
                this.mainImage.setTag(null);
                this.sideImage.setImageResource(0);
                a0.setAlpha(1f);
                break;
            }
        }
        com.navdy.hud.app.framework.glance.GlanceViewCache.putView(a, (android.view.View)a0);
    }
    
    private void dismissNotification() {
        com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().removeNotification(this.id);
    }
    
    private java.util.List getCannedReplyMessages() {
        if (this.cannedReplyMessages == null) {
            this.cannedReplyMessages = com.navdy.hud.app.framework.glance.GlanceConstants.getCannedMessages();
        }
        return this.cannedReplyMessages;
    }
    
    private android.view.ViewGroup getReplyView(int i, android.content.Context a) {
        android.view.ViewGroup a0 = null;
        if (i != 0) {
            if (this.replyMsgView != null) {
                this.removeParent((android.view.View)this.replyMsgView);
            } else {
                this.replyMsgView = (android.view.ViewGroup)android.view.LayoutInflater.from(a).inflate(R.layout.glance_large_message_single, (android.view.ViewGroup)null, false);
            }
            java.util.List a1 = this.getCannedReplyMessages();
            if (i > a1.size()) {
                i = a1.size();
            }
            ((android.widget.TextView)this.replyMsgView.findViewById(R.id.title)).setText((CharSequence)com.navdy.hud.app.framework.glance.GlanceConstants.message);
            ((android.widget.TextView)this.replyMsgView.findViewById(R.id.message)).setText((CharSequence)a1.get(i - 1));
            a0 = this.replyMsgView;
        } else {
            if (this.replyExitView != null) {
                this.removeParent((android.view.View)this.replyExitView);
            } else {
                this.replyExitView = (android.view.ViewGroup)android.view.LayoutInflater.from(a).inflate(R.layout.glance_reply_exit, (android.view.ViewGroup)null, false);
            }
            a0 = this.replyExitView;
        }
        return a0;
    }
    
    private boolean hasReplyAction() {
        boolean b = false;
        java.util.List a = this.glanceEvent.actions;
        label2: {
            label0: {
                label1: {
                    if (a == null) {
                        break label1;
                    }
                    if (this.glanceEvent.actions.contains(com.navdy.service.library.events.glances.GlanceEvent$GlanceActions.REPLY)) {
                        break label0;
                    }
                }
                b = false;
                break label2;
            }
            b = true;
        }
        return b;
    }
    
    private void removeParent(android.view.View a) {
        if (a != null) {
            android.view.ViewGroup a0 = (android.view.ViewGroup)a.getParent();
            if (a0 != null) {
                a0.removeView(a);
                sLogger.v("reply view removed");
            }
        }
    }
    
    private void reply(String s, String s0, String s1) {
        this.alive = false;
        com.navdy.hud.app.framework.glance.GlanceHandler a = com.navdy.hud.app.framework.glance.GlanceHandler.getInstance();
        if (a.sendMessage(s0, s, s1)) {
            a.sendSmsSuccessNotification(this.id, s0, s, s1);
        } else {
            a.sendSmsFailedNotification(s0, s, s1);
        }
    }
    
    private void revertChoice() {
        Object a = this.choiceLayout.getTag(R.id.message_secondary_screen);
        Object a0 = null;
        label5: {
            label0: {
                java.util.List a1 = null;
                boolean b = false;
                label2: {
                    if (a == null) {
                        a1 = (java.util.List)a0;
                        b = false;
                        break label2;
                    }
                    Object a2 = this.choiceLayout.getTag(R.id.message_prev_choice);
                    this.choiceLayout.setTag(R.id.message_prev_choice, null);
                    this.choiceLayout.setTag(R.id.message_secondary_screen, null);
                    java.util.List a3 = com.navdy.hud.app.framework.glance.GlanceConstants.numberAndReplyBack_1;
                    label4: {
                        if (a2 != a3) {
                            break label4;
                        }
                        a1 = com.navdy.hud.app.framework.glance.GlanceConstants.numberAndReplyBack_1;
                        b = true;
                        break label2;
                    }
                    java.util.List a4 = com.navdy.hud.app.framework.glance.GlanceConstants.numberAndNoReplyBack;
                    label3: {
                        if (a2 != a4) {
                            break label3;
                        }
                        a1 = com.navdy.hud.app.framework.glance.GlanceConstants.numberAndNoReplyBack;
                        b = true;
                        break label2;
                    }
                    java.util.List a5 = com.navdy.hud.app.framework.glance.GlanceConstants.noMessage;
                    label1: {
                        if (a2 != a5) {
                            break label1;
                        }
                        a1 = com.navdy.hud.app.framework.glance.GlanceConstants.noMessage;
                        b = false;
                        break label2;
                    }
                    if (a2 != com.navdy.hud.app.framework.glance.GlanceConstants.noNumberandNoReplyBack) {
                        break label0;
                    }
                    a1 = com.navdy.hud.app.framework.glance.GlanceConstants.noNumberandNoReplyBack;
                    b = false;
                }
                this.choiceLayout.setAlpha(1f);
                this.choiceLayout.setVisibility(0);
                this.choiceLayout.setChoices(a1, b ? 1 : 0, this.choiceListener, 0.5f);
                this.choiceLayout.setTag(a1);
                break label5;
            }
            this.choiceLayout.setTag(null);
            this.setChoices();
        }
    }
    
    private void sendTts() {
        if (this.controller != null && this.controller.isTtsOn() && !this.ttsSent) {
            sLogger.v(new StringBuilder().append("tts-send [").append(this.id).append("]").toString());
            com.navdy.service.library.events.audio.SpeechRequest a = new com.navdy.service.library.events.audio.SpeechRequest$Builder().words(this.ttsMessage).category(com.navdy.service.library.events.audio.SpeechRequest$Category.SPEECH_MESSAGE_READ_OUT).id(this.id).sendStatus(Boolean.valueOf(true)).build();
            this.bus.post(new com.navdy.hud.app.event.LocalSpeechRequest(a));
            this.ttsSent = true;
        }
    }
    
    private void setChoices() {
        if (this.controller.isExpandedWithStack()) {
            this.switchToMode(com.navdy.hud.app.framework.glance.GlanceNotification$Mode.READ, (String)null);
        } else {
            java.util.List a = null;
            boolean b = false;
            label0: if (com.navdy.hud.app.framework.glance.GlanceNotification$5.$SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp[this.glanceApp.ordinal()] == 8) {
                a = (this.data.get(com.navdy.service.library.events.glances.FuelConstants.NO_ROUTE.name()) != null) ? com.navdy.hud.app.framework.glance.GlanceConstants.fuelChoicesNoRoute : com.navdy.hud.app.framework.glance.GlanceConstants.fuelChoices;
                b = false;
            } else {
                Object a0 = this.choiceLayout.getTag();
                Object a1 = null;
                if (a0 != null) {
                    a = (java.util.List)a1;
                    b = false;
                } else {
                    boolean b0 = this.hasReplyAction();
                    String s = this.number;
                    label1: {
                        label2: {
                            if (s != null) {
                                break label2;
                            }
                            if (this.contacts == null) {
                                break label1;
                            }
                        }
                        if (b0) {
                            a = com.navdy.hud.app.framework.glance.GlanceConstants.numberAndReplyBack_1;
                            b = true;
                            break label0;
                        } else {
                            a = com.navdy.hud.app.framework.glance.GlanceConstants.numberAndNoReplyBack;
                            b = true;
                            break label0;
                        }
                    }
                    if (com.navdy.hud.app.framework.glance.GlanceHelper.isCalendarApp(this.glanceApp)) {
                        a = com.navdy.hud.app.framework.glance.GlanceConstants.calendarOptions;
                        b = false;
                    } else {
                        a = com.navdy.hud.app.framework.glance.GlanceConstants.noNumberandNoReplyBack;
                        b = false;
                    }
                }
            }
            this.choiceLayout.setAlpha(1f);
            this.choiceLayout.setVisibility(0);
            this.choiceLayout.setChoices(a, b ? 1 : 0, this.choiceListener, 0.5f);
            this.choiceLayout.setTag(a);
        }
    }
    
    private void setExpandedContent(android.view.View a) {
        if (this.glanceExtendedContainer != null) {
            switch(com.navdy.hud.app.framework.glance.GlanceNotification$5.$SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp[this.glanceApp.ordinal()]) {
                case 8: {
                    android.widget.TextView a0 = (android.widget.TextView)a.findViewById(R.id.title1);
                    a0.setTextAppearance(a.getContext(), R.style.glance_title_1);
                    android.widget.TextView a1 = (android.widget.TextView)a.findViewById(R.id.title2);
                    a1.setTextAppearance(a.getContext(), R.style.glance_title_2);
                    a0.setText((CharSequence)this.data.get(com.navdy.service.library.events.glances.FuelConstants.GAS_STATION_NAME.name()));
                    if (this.data.get(com.navdy.service.library.events.glances.FuelConstants.NO_ROUTE.name()) != null) {
                        a1.setText((CharSequence)a.getContext().getResources().getString(R.string.i_cannot_add_gasstation));
                    } else {
                        android.content.res.Resources a2 = a.getContext().getResources();
                        Object[] a3 = new Object[2];
                        a3[0] = this.data.get(com.navdy.service.library.events.glances.FuelConstants.GAS_STATION_ADDRESS.name());
                        a3[1] = this.data.get(com.navdy.service.library.events.glances.FuelConstants.GAS_STATION_DISTANCE.name());
                        a1.setText((CharSequence)a2.getString(R.string.gas_station_detail, a3));
                    }
                    a0.setVisibility(0);
                    a1.setVisibility(0);
                    break;
                }
                case 4: case 5: case 6: case 7: case 9: case 10: case 11: case 12: case 13: case 14: case 15: case 16: case 17: case 18: case 19: {
                    android.widget.TextView a4 = (android.widget.TextView)a.findViewById(R.id.title);
                    android.widget.TextView a5 = (android.widget.TextView)a.findViewById(R.id.message);
                    switch(com.navdy.hud.app.framework.glance.GlanceNotification$5.$SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp[this.glanceApp.ordinal()]) {
                        case 4: case 5: case 6: case 7: {
                            a4.setTextColor(com.navdy.hud.app.framework.glance.GlanceConstants.colorWhite);
                            this.subTitle.setVisibility(4);
                            String s = (String)this.data.get(com.navdy.service.library.events.glances.EmailConstants.EMAIL_SUBJECT.name());
                            if (s == null) {
                                s = "";
                            }
                            a4.setText((CharSequence)s);
                            break;
                        }
                        default: {
                            a4.setText((CharSequence)com.navdy.hud.app.framework.glance.GlanceHelper.getTimeStr(System.currentTimeMillis(), this.postTime, this.timeHelper));
                        }
                    }
                    a5.setText((CharSequence)this.messageStr);
                    break;
                }
                case 1: case 2: case 3: {
                    android.widget.TextView a6 = (android.widget.TextView)a.findViewById(R.id.text1);
                    String s0 = (String)this.data.get(com.navdy.service.library.events.glances.CalendarConstants.CALENDAR_TIME_STR.name());
                    if (android.text.TextUtils.isEmpty((CharSequence)s0)) {
                        a6.setVisibility(8);
                    } else {
                        a6.setText((CharSequence)s0);
                        a6.setVisibility(0);
                    }
                    android.widget.TextView a7 = (android.widget.TextView)a.findViewById(R.id.text2);
                    String s1 = (String)this.data.get(com.navdy.service.library.events.glances.CalendarConstants.CALENDAR_TITLE.name());
                    if (android.text.TextUtils.isEmpty((CharSequence)s1)) {
                        a7.setVisibility(8);
                    } else {
                        a7.setText((CharSequence)s1);
                        a7.setVisibility(0);
                    }
                    android.view.ViewGroup a8 = (android.view.ViewGroup)a.findViewById(R.id.locationContainer);
                    String s2 = (String)this.data.get(com.navdy.service.library.events.glances.CalendarConstants.CALENDAR_LOCATION.name());
                    if (android.text.TextUtils.isEmpty((CharSequence)s2)) {
                        a8.setVisibility(8);
                        break;
                    } else {
                        ((android.widget.TextView)a.findViewById(R.id.text3)).setText((CharSequence)s2);
                        a8.setVisibility(0);
                        break;
                    }
                }
            }
        }
    }
    
    private void setMainImage() {
        if (com.navdy.hud.app.framework.glance.GlanceHelper.isCalendarApp(this.glanceApp)) {
            long j = 0L;
            this.colorImageView.setColor(this.glanceApp.getColor());
            this.text1.setText((CharSequence)com.navdy.hud.app.framework.glance.GlanceConstants.starts);
            String s = (String)this.data.get(com.navdy.service.library.events.glances.CalendarConstants.CALENDAR_TIME.name());
            label2: {
                Throwable a = null;
                if (s == null) {
                    j = 0L;
                    break label2;
                } else {
                    try {
                        j = Long.parseLong(s);
                        break label2;
                    } catch(Throwable a0) {
                        a = a0;
                    }
                }
                sLogger.e(a);
                j = 0L;
            }
            if (j <= 0L) {
                this.text1.setText((CharSequence)"");
                this.text2.setTextAppearance(com.navdy.hud.app.HudApplication.getAppContext(), R.style.small_glance_sign_text_2);
                this.text2.setText((CharSequence)com.navdy.hud.app.framework.glance.GlanceConstants.questionMark);
                this.text3.setText((CharSequence)"");
                ((android.view.ViewGroup$MarginLayoutParams)this.text2.getLayoutParams()).topMargin = com.navdy.hud.app.framework.glance.GlanceConstants.calendarNowMargin;
            } else {
                int i = 0;
                String s0 = com.navdy.hud.app.framework.glance.GlanceHelper.getCalendarTime(j, this.stringBuilder1, this.stringBuilder2, this.timeHelper);
                String s1 = this.stringBuilder1.toString();
                if (android.text.TextUtils.isEmpty((CharSequence)s1)) {
                    String s2 = this.stringBuilder2.toString();
                    if (android.text.TextUtils.isEmpty((CharSequence)s2)) {
                        i = com.navdy.hud.app.framework.glance.GlanceConstants.calendarNowMargin;
                        this.text2.setTextAppearance(com.navdy.hud.app.HudApplication.getAppContext(), R.style.small_glance_sign_text_2);
                        this.text2.setText((CharSequence)s0);
                        this.text3.setText((CharSequence)"");
                    } else {
                        i = com.navdy.hud.app.framework.glance.GlanceConstants.calendarTimeMargin;
                        this.text3.setText((CharSequence)"");
                        this.text2.setTextAppearance(com.navdy.hud.app.HudApplication.getAppContext(), R.style.small_glance_sign_text_3);
                        android.text.SpannableStringBuilder a1 = new android.text.SpannableStringBuilder();
                        a1.append((CharSequence)s0);
                        int i0 = a1.length();
                        a1.append((CharSequence)s2);
                        a1.setSpan(new android.text.style.AbsoluteSizeSpan(com.navdy.hud.app.framework.glance.GlanceConstants.calendarpmMarker), i0, a1.length(), 33);
                        this.text2.setText((CharSequence)a1);
                    }
                } else {
                    i = com.navdy.hud.app.framework.glance.GlanceConstants.calendarNormalMargin;
                    this.text2.setTextAppearance(com.navdy.hud.app.HudApplication.getAppContext(), R.style.small_glance_sign_text_1);
                    this.text2.setText((CharSequence)s0);
                    this.text3.setText((CharSequence)s1);
                }
                ((android.view.ViewGroup$MarginLayoutParams)this.text2.getLayoutParams()).topMargin = i;
            }
            this.ttsMessage = com.navdy.hud.app.framework.glance.GlanceHelper.getTtsMessage(this.glanceApp, this.data, this.stringBuilder1, this.stringBuilder2, this.timeHelper);
            this.stringBuilder1.setLength(0);
            this.stringBuilder2.setLength(0);
        } else {
            int i1 = 0;
            int i2 = this.glanceApp.getMainIcon();
            if (this.glanceApp != com.navdy.hud.app.framework.glance.GlanceApp.GENERIC) {
                i1 = i2;
            } else {
                String s3 = (String)this.data.get(com.navdy.service.library.events.glances.GenericConstants.GENERIC_MAIN_ICON.name());
                if (android.text.TextUtils.isEmpty((CharSequence)s3)) {
                    i1 = i2;
                } else {
                    i1 = com.navdy.hud.app.framework.glance.GlanceHelper.getIcon(s3);
                    if (i1 == -1) {
                        i1 = i2;
                    }
                }
            }
            if (i1 == -1) {
                if (this.photoCheckRequired) {
                    java.util.List a2 = this.contacts;
                    label1: {
                        label0: {
                            if (a2 == null) {
                                break label0;
                            }
                            if (this.contacts.size() <= 1) {
                                break label0;
                            }
                            com.navdy.hud.app.framework.contacts.ContactImageHelper a3 = com.navdy.hud.app.framework.contacts.ContactImageHelper.getInstance();
                            int i3 = a3.getContactImageIndex(this.source);
                            this.mainImage.setImage(a3.getResourceId(i3), com.navdy.hud.app.framework.contacts.ContactUtil.getInitials(this.source), com.navdy.hud.app.ui.component.image.InitialsImageView$Style.LARGE);
                            break label1;
                        }
                        com.navdy.hud.app.framework.contacts.ContactUtil.setContactPhoto(this.source, this.number, this.photoCheckRequired, this.mainImage, this.roundTransformation, (com.navdy.hud.app.framework.contacts.ContactUtil$IContactCallback)this);
                    }
                    this.mainImage.requestLayout();
                } else if (this.glanceApp.isDefaultIconBasedOnId()) {
                    com.navdy.hud.app.framework.contacts.ContactImageHelper a4 = com.navdy.hud.app.framework.contacts.ContactImageHelper.getInstance();
                    int i4 = a4.getContactImageIndex(this.source);
                    this.mainImage.setImage(a4.getResourceId(i4), com.navdy.hud.app.framework.contacts.ContactUtil.getInitials(this.source), com.navdy.hud.app.ui.component.image.InitialsImageView$Style.LARGE);
                }
            } else {
                this.mainImage.setImage(i1, (String)null, com.navdy.hud.app.ui.component.image.InitialsImageView$Style.DEFAULT);
            }
        }
    }
    
    private void setSideImage() {
        this.sideImage.setImageResource(this.appIcon);
    }
    
    private void setSubTitle() {
        String s = com.navdy.hud.app.framework.glance.GlanceHelper.getSubTitle(this.glanceApp, this.data);
        if (android.text.TextUtils.isEmpty((CharSequence)s) && this.number != null && !android.text.TextUtils.equals((CharSequence)this.number, (CharSequence)this.source)) {
            s = com.navdy.hud.app.util.PhoneUtil.formatPhoneNumber(this.number);
        }
        this.subTitle.setText((CharSequence)s);
    }
    
    private void setTitle() {
        this.source = com.navdy.hud.app.framework.glance.GlanceHelper.getTitle(this.glanceApp, this.data);
        if (this.number == null) {
            this.number = com.navdy.hud.app.framework.glance.GlanceHelper.getNumber(this.glanceApp, this.data);
        }
        if (this.number != null && android.text.TextUtils.equals((CharSequence)this.source, (CharSequence)this.number)) {
            this.mainTitle.setText((CharSequence)com.navdy.hud.app.util.PhoneUtil.formatPhoneNumber(this.source));
        } else {
            this.mainTitle.setText((CharSequence)this.source);
        }
    }
    
    private void startAudioAnimation() {
        if (this.audioFeedback != null) {
            this.audioFeedback.animate().cancel();
            this.audioFeedback.setAlpha(1f);
            this.audioFeedback.setVisibility(0);
            this.audioFeedback.setImageResource(R.drawable.audio_loop);
            ((android.graphics.drawable.AnimationDrawable)this.audioFeedback.getDrawable()).start();
        }
    }
    
    private void stopAudioAnimation(boolean b) {
        if (this.audioFeedback != null && this.audioFeedback.getVisibility() == 0) {
            android.graphics.drawable.AnimationDrawable a = (android.graphics.drawable.AnimationDrawable)this.audioFeedback.getDrawable();
            if (a.isRunning()) {
                a.stop();
                if (b) {
                    this.audioFeedback.animate().alpha(0.0f).setDuration(300L).withEndAction((Runnable)new com.navdy.hud.app.framework.glance.GlanceNotification$4(this));
                } else {
                    this.audioFeedback.setVisibility(8);
                }
            }
        }
    }
    
    private void switchToMode(com.navdy.hud.app.framework.glance.GlanceNotification$Mode a, String s) {
        if (this.controller != null) {
            this.controller.stopTimeout(true);
            this.choiceLayout.setTag(R.id.message_prev_choice, this.choiceLayout.getTag());
            this.choiceLayout.setTag(R.id.message_secondary_screen, Integer.valueOf(1));
            if (a != com.navdy.hud.app.framework.glance.GlanceNotification$Mode.READ) {
                this.operationMode = a;
                this.choiceLayout.setChoices((java.util.List)null, 0, (com.navdy.hud.app.ui.component.ChoiceLayout2$IListener)null, 0.5f);
                this.choiceLayout.setTag(com.navdy.hud.app.framework.glance.GlanceConstants.backChoice);
                if (!this.controller.isExpandedWithStack()) {
                    this.initialReplyMode = true;
                    this.controller.expandNotification(false);
                }
            } else {
                this.operationMode = a;
                this.choiceLayout.setChoices(com.navdy.hud.app.framework.glance.GlanceConstants.backChoice2, 0, this.choiceListener, 0.5f);
                this.choiceLayout.setTag(com.navdy.hud.app.framework.glance.GlanceConstants.backChoice);
                if (!this.controller.isExpandedWithStack()) {
                    this.controller.expandNotification(true);
                }
            }
        }
    }
    
    private void switchToReplyScreen() {
        android.os.Bundle a = new android.os.Bundle();
        java.util.ArrayList a0 = new java.util.ArrayList();
        if (this.contacts != null) {
            a0.addAll((java.util.Collection)this.contacts);
        } else {
            int i = com.navdy.hud.app.framework.contacts.ContactImageHelper.getInstance().getContactImageIndex(this.number);
            a0.add(new com.navdy.hud.app.framework.contacts.Contact(this.source, this.number, com.navdy.hud.app.framework.contacts.NumberType.OTHER, i, 0L));
        }
        a.putParcelableArrayList("CONTACTS", a0);
        a.putString("NOTIF_ID", this.id);
        a.putInt("MENU_MODE", com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2$MenuMode.REPLY_PICKER.ordinal());
        com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().removeNotification(this.id, false, com.navdy.service.library.events.ui.Screen.SCREEN_MAIN_MENU, a, null);
    }
    
    private void updateState() {
        if (com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getRemoteDeviceInfo() != null) {
            this.setTitle();
            this.setSubTitle();
            this.setSideImage();
            this.setMainImage();
            this.setChoices();
            if (this.controller.isExpandedWithStack()) {
                this.mainTitle.setAlpha(0.0f);
                this.subTitle.setAlpha(0.0f);
                this.choiceLayout.setAlpha(0.0f);
            }
        } else {
            this.dismissNotification();
        }
    }
    
    public boolean canAddToStackIfCurrentExists() {
        return true;
    }
    
    public boolean expandNotification() {
        boolean b = false;
        if (this.choiceLayout.getTag(R.id.message_secondary_screen) != null) {
            b = false;
        } else {
            this.switchToMode(com.navdy.hud.app.framework.glance.GlanceNotification$Mode.READ, (String)null);
            b = true;
        }
        return b;
    }
    
    public int getColor() {
        return this.color;
    }
    
    public android.view.View getExpandedView(android.content.Context a, Object a0) {
        android.view.ViewGroup a1 = null;
        com.navdy.hud.app.framework.glance.GlanceNotification$Mode a2 = this.operationMode;
        com.navdy.hud.app.framework.glance.GlanceNotification$Mode a3 = com.navdy.hud.app.framework.glance.GlanceNotification$Mode.READ;
        label2: {
            int i = 0;
            label0: {
                label1: {
                    if (a2 == a3) {
                        break label1;
                    }
                    if (this.operationMode != null) {
                        break label0;
                    }
                }
                this.glanceExtendedContainer = (android.view.ViewGroup)com.navdy.hud.app.framework.glance.GlanceViewCache.getView(this.largeType, a);
                if (this.supportsScroll) {
                    this.topScrub = this.glanceExtendedContainer.findViewById(R.id.topScrub);
                    this.topScrub.setVisibility(8);
                    this.bottomScrub = this.glanceExtendedContainer.findViewById(R.id.bottomScrub);
                    this.bottomScrub.setVisibility(0);
                    this.scrollView = (com.navdy.hud.app.view.ObservableScrollView)this.glanceExtendedContainer.findViewById(R.id.scrollView);
                    this.scrollView.setScrollListener(this.scrollListener);
                }
                this.setExpandedContent((android.view.View)this.glanceExtendedContainer);
                a1 = this.glanceExtendedContainer;
                break label2;
            }
            com.navdy.hud.app.framework.notifications.NotificationManager a4 = com.navdy.hud.app.framework.notifications.NotificationManager.getInstance();
            if (this.initialReplyMode) {
                this.initialReplyMode = false;
                i = 1;
            } else {
                i = ((Integer)a0).intValue();
            }
            android.view.View a5 = a4.getExpandedViewChild();
            if (a5 != null) {
                this.removeParent(a5);
            }
            a1 = this.getReplyView(i, a);
        }
        return a1;
    }
    
    public int getExpandedViewIndicatorColor() {
        return com.navdy.hud.app.framework.glance.GlanceConstants.colorWhite;
    }
    
    public com.navdy.hud.app.framework.glance.GlanceApp getGlanceApp() {
        return this.glanceApp;
    }
    
    public String getId() {
        return this.id;
    }
    
    public int getTimeout() {
        return 25000;
    }
    
    public com.navdy.hud.app.framework.notifications.NotificationType getType() {
        return this.glanceApp.notificationType;
    }
    
    public android.view.View getView(android.content.Context a) {
        this.glanceContainer = (android.view.ViewGroup)com.navdy.hud.app.framework.glance.GlanceViewCache.getView(this.smallType, a);
        this.mainTitle = (android.widget.TextView)this.glanceContainer.findViewById(R.id.mainTitle);
        this.subTitle = (android.widget.TextView)this.glanceContainer.findViewById(R.id.subTitle);
        this.sideImage = (android.widget.ImageView)this.glanceContainer.findViewById(R.id.sideImage);
        this.choiceLayout = (com.navdy.hud.app.ui.component.ChoiceLayout2)this.glanceContainer.findViewById(R.id.choiceLayout);
        this.audioFeedback = (android.widget.ImageView)this.glanceContainer.findViewById(R.id.audioFeedback);
        if (com.navdy.hud.app.framework.glance.GlanceHelper.isCalendarApp(this.glanceApp)) {
            this.colorImageView = (com.navdy.hud.app.ui.component.image.ColorImageView)this.glanceContainer.findViewById(R.id.mainImage);
            this.text1 = (android.widget.TextView)this.glanceContainer.findViewById(R.id.text1);
            this.text2 = (android.widget.TextView)this.glanceContainer.findViewById(R.id.text2);
            this.text3 = (android.widget.TextView)this.glanceContainer.findViewById(R.id.text3);
        } else {
            this.mainImage = (com.navdy.hud.app.ui.component.image.InitialsImageView)this.glanceContainer.findViewById(R.id.mainImage);
        }
        return this.glanceContainer;
    }
    
    public android.animation.AnimatorSet getViewSwitchAnimation(boolean b) {
        android.animation.AnimatorSet a = new android.animation.AnimatorSet();
        if (b) {
            android.animation.Animator[] a0 = new android.animation.Animator[3];
            android.widget.TextView a1 = this.mainTitle;
            android.util.Property a2 = android.view.View.ALPHA;
            float[] a3 = new float[2];
            a3[0] = 0.0f;
            a3[1] = 1f;
            a0[0] = android.animation.ObjectAnimator.ofFloat(a1, a2, a3);
            android.widget.TextView a4 = this.subTitle;
            android.util.Property a5 = android.view.View.ALPHA;
            float[] a6 = new float[2];
            a6[0] = 0.0f;
            a6[1] = 1f;
            a0[1] = android.animation.ObjectAnimator.ofFloat(a4, a5, a6);
            com.navdy.hud.app.ui.component.ChoiceLayout2 a7 = this.choiceLayout;
            android.util.Property a8 = android.view.View.ALPHA;
            float[] a9 = new float[2];
            a9[0] = 0.0f;
            a9[1] = 1f;
            a0[2] = android.animation.ObjectAnimator.ofFloat(a7, a8, a9);
            a.playTogether(a0);
        } else {
            android.animation.Animator[] a10 = new android.animation.Animator[3];
            android.widget.TextView a11 = this.mainTitle;
            android.util.Property a12 = android.view.View.ALPHA;
            float[] a13 = new float[2];
            a13[0] = 1f;
            a13[1] = 0.0f;
            a10[0] = android.animation.ObjectAnimator.ofFloat(a11, a12, a13);
            android.widget.TextView a14 = this.subTitle;
            android.util.Property a15 = android.view.View.ALPHA;
            float[] a16 = new float[2];
            a16[0] = 1f;
            a16[1] = 0.0f;
            a10[1] = android.animation.ObjectAnimator.ofFloat(a14, a15, a16);
            com.navdy.hud.app.ui.component.ChoiceLayout2 a17 = this.choiceLayout;
            android.util.Property a18 = android.view.View.ALPHA;
            float[] a19 = new float[2];
            a19[0] = 1f;
            a19[1] = 0.0f;
            a10[2] = android.animation.ObjectAnimator.ofFloat(a17, a18, a19);
            a.playTogether(a10);
        }
        return a;
    }
    
    public boolean isAlive() {
        return this.alive;
    }
    
    public boolean isContextValid() {
        return this.controller != null;
    }
    
    public boolean isPurgeable() {
        return false;
    }
    
    public boolean isShowingReadOption() {
        boolean b = false;
        com.navdy.hud.app.ui.component.ChoiceLayout2 a = this.choiceLayout;
        label2: {
            label0: {
                label1: {
                    if (a == null) {
                        break label1;
                    }
                    Object a0 = this.choiceLayout.getTag();
                    if (a0 == com.navdy.hud.app.framework.glance.GlanceConstants.noNumberandNoReplyBack) {
                        break label0;
                    }
                    if (a0 == com.navdy.hud.app.framework.glance.GlanceConstants.numberAndNoReplyBack) {
                        break label0;
                    }
                    if (a0 == com.navdy.hud.app.framework.glance.GlanceConstants.numberAndReplyBack_1) {
                        break label0;
                    }
                }
                b = false;
                break label2;
            }
            b = true;
        }
        return b;
    }
    
    public com.navdy.hud.app.manager.InputManager$IInputHandler nextHandler() {
        return null;
    }
    
    public void onClick() {
    }
    
    public void onConnectionStateChange(com.navdy.service.library.events.connection.ConnectionStateChange a) {
        if (a.state == com.navdy.service.library.events.connection.ConnectionStateChange$ConnectionState.CONNECTION_DISCONNECTED) {
            this.stopAudioAnimation(true);
        }
    }
    
    public void onContactFound(com.navdy.hud.app.framework.recentcall.RecentCallManager$ContactFound a) {
        label2: if (this.controller != null) {
            Object a0 = this.choiceLayout.getTag();
            if (a0 != null && a0 == com.navdy.hud.app.framework.glance.GlanceConstants.noNumberandNoReplyBack && a.contact != null && android.text.TextUtils.equals((CharSequence)a.identifier, (CharSequence)this.source)) {
                Object a1 = a.contact.iterator();
                while(((java.util.Iterator)a1).hasNext()) {
                    com.navdy.service.library.events.contacts.Contact a2 = (com.navdy.service.library.events.contacts.Contact)((java.util.Iterator)a1).next();
                    if (this.contacts == null) {
                        this.contacts = (java.util.List)new java.util.ArrayList();
                    }
                    com.navdy.hud.app.framework.contacts.Contact a3 = new com.navdy.hud.app.framework.contacts.Contact(a2);
                    a3.setName(this.source);
                    this.contacts.add(a3);
                    if (a.contact.size() == 1) {
                        this.number = a2.number;
                    }
                }
                sLogger.v(new StringBuilder().append("contact info found:").append(a.identifier).append(" number:").append(this.number).append(" size:").append(a.contact.size()).toString());
                this.setSubTitle();
                if (this.hasReplyAction()) {
                    this.choiceLayout.setChoices(com.navdy.hud.app.framework.glance.GlanceConstants.numberAndReplyBack_1, 1, this.choiceListener, 0.5f);
                    this.choiceLayout.setTag(com.navdy.hud.app.framework.glance.GlanceConstants.numberAndReplyBack_1);
                } else {
                    this.choiceLayout.setChoices(com.navdy.hud.app.framework.glance.GlanceConstants.numberAndNoReplyBack, 1, this.choiceListener, 0.5f);
                    this.choiceLayout.setTag(com.navdy.hud.app.framework.glance.GlanceConstants.numberAndNoReplyBack);
                }
                java.util.List a4 = this.contacts;
                label0: {
                    label1: {
                        if (a4 == null) {
                            break label1;
                        }
                        if (this.contacts.size() > 1) {
                            break label0;
                        }
                    }
                    com.navdy.hud.app.framework.contacts.ContactUtil.setContactPhoto(this.source, a.identifier, false, this.mainImage, this.roundTransformation, (com.navdy.hud.app.framework.contacts.ContactUtil$IContactCallback)this);
                    break label2;
                }
                com.navdy.hud.app.framework.contacts.ContactImageHelper a5 = com.navdy.hud.app.framework.contacts.ContactImageHelper.getInstance();
                int i = a5.getContactImageIndex(this.source);
                this.mainImage.setImage(a5.getResourceId(i), com.navdy.hud.app.framework.contacts.ContactUtil.getInitials(this.source), com.navdy.hud.app.ui.component.image.InitialsImageView$Style.LARGE);
            }
        }
    }
    
    public void onExpandedNotificationEvent(com.navdy.hud.app.ui.framework.UIStateManager$Mode a) {
        label2: if (a != com.navdy.hud.app.ui.framework.UIStateManager$Mode.COLLAPSE) {
            com.navdy.hud.app.framework.glance.GlanceNotification$Mode a0 = this.operationMode;
            com.navdy.hud.app.framework.glance.GlanceNotification$Mode a1 = com.navdy.hud.app.framework.glance.GlanceNotification$Mode.READ;
            label0: {
                label1: {
                    if (a0 == a1) {
                        break label1;
                    }
                    if (this.operationMode != null) {
                        break label0;
                    }
                }
                this.sendTts();
                break label2;
            }
            com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().updateExpandedIndicator(this.getCannedReplyMessages().size() + 1, 1, com.navdy.hud.app.framework.glance.GlanceConstants.colorWhite);
        } else {
            this.cancelTts();
            this.operationMode = null;
            if (this.choiceLayout.getTag(R.id.message_secondary_screen) != null && this.controller != null) {
                this.revertChoice();
                this.controller.startTimeout(this.getTimeout());
            }
            this.removeParent((android.view.View)this.replyMsgView);
            this.replyMsgView = null;
            this.removeParent((android.view.View)this.replyExitView);
            this.replyExitView = null;
            this.initialReplyMode = false;
            switch(com.navdy.hud.app.framework.glance.GlanceNotification$5.$SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp[this.glanceApp.ordinal()]) {
                case 4: case 5: case 6: case 7: {
                    this.subTitle.setVisibility(0);
                    break;
                }
            }
        }
    }
    
    public void onExpandedNotificationSwitched() {
        if (this.controller != null && this.controller.isExpandedWithStack()) {
            this.sendTts();
        }
    }
    
    public boolean onGesture(com.navdy.service.library.events.input.GestureEvent a) {
        return false;
    }
    
    public boolean onKey(com.navdy.hud.app.manager.InputManager$CustomKeyEvent a) {
        boolean b = false;
        label0: if (this.controller != null) {
            boolean b0 = this.supportsScroll;
            label1: {
                if (!b0) {
                    break label1;
                }
                if (this.scrollView == null) {
                    break label1;
                }
                if (!this.controller.isExpanded()) {
                    break label1;
                }
                switch(com.navdy.hud.app.framework.glance.GlanceNotification$5.$SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent[a.ordinal()]) {
                    case 2: {
                        b = this.scrollView.arrowScroll(130);
                        if (!this.onBottom) {
                            break label0;
                        }
                        b = false;
                        break label0;
                    }
                    case 1: {
                        b = this.scrollView.arrowScroll(33);
                        if (!this.onTop) {
                            break label0;
                        }
                        b = false;
                        break label0;
                    }
                }
            }
            if (this.operationMode != com.navdy.hud.app.framework.glance.GlanceNotification$Mode.REPLY) {
                switch(com.navdy.hud.app.framework.glance.GlanceNotification$5.$SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent[a.ordinal()]) {
                    case 3: {
                        com.navdy.hud.app.ui.component.ChoiceLayout2$Choice a0 = this.choiceLayout.getCurrentSelectedChoice();
                        if (a0 != null) {
                            switch(a0.getId()) {
                                case 6: {
                                    com.navdy.hud.app.analytics.AnalyticsSupport.recordGlanceAction("Glance_Dismiss", (com.navdy.hud.app.framework.notifications.INotification)this, "dial");
                                    break;
                                }
                                case 5: {
                                    com.navdy.hud.app.analytics.AnalyticsSupport.recordGlanceAction("Glance_Open_Mini", (com.navdy.hud.app.framework.notifications.INotification)this, "dial");
                                    break;
                                }
                                case 2: {
                                    com.navdy.hud.app.analytics.AnalyticsSupport.recordGlanceAction("Glance_Open_Full", (com.navdy.hud.app.framework.notifications.INotification)this, "dial");
                                    break;
                                }
                            }
                        }
                        this.choiceLayout.executeSelectedItem();
                        b = true;
                        break;
                    }
                    case 2: {
                        this.choiceLayout.moveSelectionRight();
                        b = true;
                        break;
                    }
                    case 1: {
                        this.choiceLayout.moveSelectionLeft();
                        b = true;
                        break;
                    }
                    default: {
                        b = false;
                    }
                }
            } else {
                int i = com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().getExpandedIndicatorCurrentItem();
                switch(com.navdy.hud.app.framework.glance.GlanceNotification$5.$SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent[a.ordinal()]) {
                    case 3: {
                        if (i != 0) {
                            this.reply((String)this.getCannedReplyMessages().get(i - 1), this.number, this.source);
                        } else {
                            this.controller.collapseNotification(false, false);
                        }
                        b = true;
                        break;
                    }
                    case 2: {
                        b = false;
                        break;
                    }
                    case 1: {
                        b = false;
                        break;
                    }
                    default: {
                        b = true;
                    }
                }
            }
        } else {
            b = true;
        }
        return b;
    }
    
    public void onNotificationEvent(com.navdy.hud.app.ui.framework.UIStateManager$Mode a) {
    }
    
    public void onPhotoDownload(com.navdy.hud.app.framework.contacts.PhoneImageDownloader$PhotoDownloadStatus a) {
        label0: if (this.controller != null && this.photoCheckRequired && a.photoType == com.navdy.service.library.events.photo.PhotoType.PHOTO_CONTACT) {
            boolean b = android.text.TextUtils.equals((CharSequence)a.sourceIdentifier, (CharSequence)this.source);
            label2: {
                if (b) {
                    break label2;
                }
                if (this.number == null) {
                    break label0;
                }
                if (!android.text.TextUtils.equals((CharSequence)a.sourceIdentifier, (CharSequence)this.number)) {
                    break label0;
                }
            }
            boolean b0 = a.alreadyDownloaded;
            label1: {
                if (!b0) {
                    break label1;
                }
                if (this.mainImage.getTag() != null) {
                    break label0;
                }
            }
            sLogger.v("photo available");
            com.navdy.hud.app.framework.contacts.ContactUtil.setContactPhoto(this.source, this.number, false, this.mainImage, this.roundTransformation, (com.navdy.hud.app.framework.contacts.ContactUtil$IContactCallback)this);
        }
    }
    
    public void onShowToast(com.navdy.hud.app.framework.toast.ToastManager$ShowToast a) {
        if (android.text.TextUtils.equals((CharSequence)a.name, (CharSequence)new StringBuilder().append(this.id).append("message-sent-toast").toString())) {
            sLogger.v("reply toast:animateOutExpandedView");
            com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().collapseExpandedNotification(true, true);
        }
    }
    
    public void onSpeechRequestNotification(com.navdy.service.library.events.audio.SpeechRequestStatus a) {
        if (this.audioFeedback != null && !android.text.TextUtils.isEmpty((CharSequence)this.id) && android.text.TextUtils.equals((CharSequence)this.id, (CharSequence)a.id) && a.status != null) {
            switch(com.navdy.hud.app.framework.glance.GlanceNotification$5.$SwitchMap$com$navdy$service$library$events$audio$SpeechRequestStatus$SpeechRequestStatusType[a.status.ordinal()]) {
                case 2: {
                    this.stopAudioAnimation(true);
                    break;
                }
                case 1: {
                    this.startAudioAnimation();
                    break;
                }
            }
        }
    }
    
    public void onStart(com.navdy.hud.app.framework.notifications.INotificationController a) {
        boolean b = a.isExpandedWithStack();
        sLogger.v(new StringBuilder().append("start called:").append(System.identityHashCode(this)).append(" expanded:").append(b).toString());
        this.bus.register(this);
        this.controller = a;
        if (this.choiceLayout != null) {
            this.choiceLayout.setTag(null);
            this.choiceLayout.setTag(R.id.message_prev_choice, null);
            this.choiceLayout.setTag(R.id.message_secondary_screen, null);
        }
        handler.postDelayed(this.updateTimeRunnable, 30000L);
        this.updateState();
    }
    
    public void onStop() {
        sLogger.v(new StringBuilder().append("stop called:").append(System.identityHashCode(this)).toString());
        handler.removeCallbacks(this.updateTimeRunnable);
        this.bus.unregister(this);
        this.cancelTts();
        if (this.choiceLayout != null) {
            this.choiceLayout.clear();
        }
        if (this.supportsScroll && this.scrollView != null) {
            this.scrollView.setScrollListener((com.navdy.hud.app.view.ObservableScrollView$IScrollListener)null);
        }
        this.controller = null;
        if (this.glanceContainer != null) {
            this.cleanupView(this.smallType, this.glanceContainer);
            this.glanceContainer = null;
        }
        if (this.glanceExtendedContainer != null) {
            this.cleanupView(this.largeType, this.glanceExtendedContainer);
            this.glanceExtendedContainer = null;
        }
        this.removeParent((android.view.View)this.replyMsgView);
        this.replyMsgView = null;
        this.removeParent((android.view.View)this.replyExitView);
        this.replyExitView = null;
        this.operationMode = null;
    }
    
    public void onTrackHand(float f) {
    }
    
    public void onUpdate() {
    }
    
    public void setContactList(java.util.List a) {
        this.contacts = a;
    }
    
    public void setListener(com.navdy.hud.app.framework.notifications.IProgressUpdate a) {
        this.progressUpdate = a;
    }
    
    public boolean supportScroll() {
        return this.supportsScroll;
    }
}
