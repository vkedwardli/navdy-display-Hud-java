package com.navdy.hud.app.framework.contacts;

class PhoneImageDownloader$5 {
    final static int[] $SwitchMap$com$navdy$service$library$events$connection$ConnectionStateChange$ConnectionState;
    final static int[] $SwitchMap$com$navdy$service$library$events$photo$PhotoType;
    
    static {
        $SwitchMap$com$navdy$service$library$events$photo$PhotoType = new int[com.navdy.service.library.events.photo.PhotoType.values().length];
        int[] a = $SwitchMap$com$navdy$service$library$events$photo$PhotoType;
        com.navdy.service.library.events.photo.PhotoType a0 = com.navdy.service.library.events.photo.PhotoType.PHOTO_ALBUM_ART;
        try {
            a[a0.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$photo$PhotoType[com.navdy.service.library.events.photo.PhotoType.PHOTO_CONTACT.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException0) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$photo$PhotoType[com.navdy.service.library.events.photo.PhotoType.PHOTO_DRIVER_PROFILE.ordinal()] = 3;
        } catch(NoSuchFieldError ignoredException1) {
        }
        $SwitchMap$com$navdy$service$library$events$connection$ConnectionStateChange$ConnectionState = new int[com.navdy.service.library.events.connection.ConnectionStateChange$ConnectionState.values().length];
        int[] a1 = $SwitchMap$com$navdy$service$library$events$connection$ConnectionStateChange$ConnectionState;
        com.navdy.service.library.events.connection.ConnectionStateChange$ConnectionState a2 = com.navdy.service.library.events.connection.ConnectionStateChange$ConnectionState.CONNECTION_DISCONNECTED;
        try {
            a1[a2.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException2) {
        }
    }
}
