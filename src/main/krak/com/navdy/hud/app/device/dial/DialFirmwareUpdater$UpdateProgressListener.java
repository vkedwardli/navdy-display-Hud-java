package com.navdy.hud.app.device.dial;

abstract public interface DialFirmwareUpdater$UpdateProgressListener {
    abstract public void onFinished(com.navdy.hud.app.device.dial.DialFirmwareUpdater$Error arg, String arg0);
    
    
    abstract public void onProgress(int arg);
}
