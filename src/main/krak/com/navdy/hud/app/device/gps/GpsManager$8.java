package com.navdy.hud.app.device.gps;

class GpsManager$8 implements Runnable {
    final com.navdy.hud.app.device.gps.GpsManager this$0;
    
    GpsManager$8(com.navdy.hud.app.device.gps.GpsManager a) {
        super();
        this.this$0 = a;
    }
    
    public void run() {
        label0: {
            boolean b = false;
            Throwable a = null;
            label1: {
                boolean b0 = false;
                label4: {
                    try {
                        b = true;
                        long j = android.os.SystemClock.elapsedRealtime() - com.navdy.hud.app.device.gps.GpsManager.access$1300(this.this$0);
                        if (j < 3000L) {
                            b0 = true;
                            break label4;
                        }
                        b = true;
                        com.navdy.hud.app.device.gps.GpsManager.access$000().i(new StringBuilder().append("[Gps-loc] locationFixRunnable: lost location fix[").append(j).append("]").toString());
                        boolean b1 = com.navdy.hud.app.device.gps.GpsManager.access$2400(this.this$0);
                        label2: {
                            label3: {
                                if (!b1) {
                                    break label3;
                                }
                                b = true;
                                long j0 = android.os.SystemClock.elapsedRealtime() - com.navdy.hud.app.device.gps.GpsManager.access$2500(this.this$0);
                                com.navdy.hud.app.device.gps.GpsManager.access$000().i(new StringBuilder().append("[Gps-loc] locationFixRunnable: extrapolation on time:").append(j0).toString());
                                if (j0 < 5000L) {
                                    break label2;
                                }
                                b = true;
                                com.navdy.hud.app.device.gps.GpsManager.access$2402(this.this$0, false);
                                com.navdy.hud.app.device.gps.GpsManager.access$2502(this.this$0, 0L);
                                com.navdy.hud.app.device.gps.GpsManager.access$000().i("[Gps-loc] locationFixRunnable: expired");
                            }
                            b = false;
                            com.navdy.hud.app.device.gps.GpsManager.access$1600(this.this$0, (android.location.Location)null);
                            com.navdy.hud.app.device.gps.GpsManager.access$000().e("[Gps-loc] lost gps fix");
                            com.navdy.hud.app.device.gps.GpsManager.access$500(this.this$0).removeCallbacks(com.navdy.hud.app.device.gps.GpsManager.access$600(this.this$0));
                            com.navdy.hud.app.device.gps.GpsManager.access$500(this.this$0).removeCallbacks(com.navdy.hud.app.device.gps.GpsManager.access$400(this.this$0));
                            com.navdy.hud.app.device.gps.GpsManager.access$500(this.this$0).postDelayed(com.navdy.hud.app.device.gps.GpsManager.access$400(this.this$0), 5000L);
                            com.navdy.hud.app.device.gps.GpsManager.access$2100(this.this$0);
                            com.navdy.hud.app.service.ConnectionServiceAnalyticsSupport.recordGpsLostLocation(String.valueOf((android.os.SystemClock.elapsedRealtime() - com.navdy.hud.app.device.gps.GpsManager.access$800(this.this$0)) / 1000L));
                            com.navdy.hud.app.device.gps.GpsManager.access$802(this.this$0, android.os.SystemClock.elapsedRealtime());
                            b0 = false;
                            break label4;
                        }
                        b = true;
                        com.navdy.hud.app.device.gps.GpsManager.access$000().i("[Gps-loc] locationFixRunnable: reset");
                    } catch(Throwable a0) {
                        a = a0;
                        break label1;
                    }
                    com.navdy.hud.app.device.gps.GpsManager.access$500(this.this$0).postDelayed(com.navdy.hud.app.device.gps.GpsManager.access$600(this.this$0), 1000L);
                    break label0;
                }
                if (!b0) {
                    break label0;
                }
                com.navdy.hud.app.device.gps.GpsManager.access$500(this.this$0).postDelayed(com.navdy.hud.app.device.gps.GpsManager.access$600(this.this$0), 1000L);
                break label0;
            }
            try {
                com.navdy.hud.app.device.gps.GpsManager.access$000().e(a);
            } catch(Throwable a1) {
                if (b) {
                    com.navdy.hud.app.device.gps.GpsManager.access$500(this.this$0).postDelayed(com.navdy.hud.app.device.gps.GpsManager.access$600(this.this$0), 1000L);
                }
                throw a1;
            }
            if (b) {
                com.navdy.hud.app.device.gps.GpsManager.access$500(this.this$0).postDelayed(com.navdy.hud.app.device.gps.GpsManager.access$600(this.this$0), 1000L);
            }
        }
    }
}
