package com.navdy.hud.app.device.dial;

public class DialManagerHelper {
    final private static com.navdy.service.library.log.Logger sLogger;
    
    static {
        sLogger = com.navdy.hud.app.device.dial.DialManager.sLogger;
    }
    
    public DialManagerHelper() {
    }
    
    static com.navdy.service.library.log.Logger access$000() {
        return sLogger;
    }
    
    static boolean access$100(android.bluetooth.BluetoothProfile a, android.bluetooth.BluetoothDevice a0) {
        return com.navdy.hud.app.device.dial.DialManagerHelper.forceConnect(a, a0);
    }
    
    static boolean access$200(android.bluetooth.BluetoothProfile a, android.bluetooth.BluetoothDevice a0) {
        return com.navdy.hud.app.device.dial.DialManagerHelper.forceDisconnect(a, a0);
    }
    
    public static void connectToDial(android.bluetooth.BluetoothAdapter a, android.bluetooth.BluetoothDevice a0, com.navdy.hud.app.device.dial.DialManagerHelper$IDialConnection a1) {
        try {
            label0: {
                label1: {
                    if (a == null) {
                        break label1;
                    }
                    if (a0 == null) {
                        break label1;
                    }
                    if (a1 != null) {
                        break label0;
                    }
                }
                throw new IllegalArgumentException();
            }
            boolean b = a.getProfileProxy(com.navdy.hud.app.HudApplication.getAppContext(), (android.bluetooth.BluetoothProfile$ServiceListener)new com.navdy.hud.app.device.dial.DialManagerHelper$4(a0, a1), 4);
            sLogger.v(new StringBuilder().append("[Dial]connectToDial getProfileProxy returned:").append(b).toString());
            if (!b) {
                com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.device.dial.DialManagerHelper$5(a1), 1);
            }
        } catch(Throwable a2) {
            sLogger.e("connectToDial", a2);
            com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.device.dial.DialManagerHelper$6(a1), 1);
        }
    }
    
    public static void disconnectFromDial(android.bluetooth.BluetoothAdapter a, android.bluetooth.BluetoothDevice a0, com.navdy.hud.app.device.dial.DialManagerHelper$IDialConnection a1) {
        try {
            label0: {
                label1: {
                    if (a == null) {
                        break label1;
                    }
                    if (a0 == null) {
                        break label1;
                    }
                    if (a1 != null) {
                        break label0;
                    }
                }
                throw new IllegalArgumentException();
            }
            boolean b = a.getProfileProxy(com.navdy.hud.app.HudApplication.getAppContext(), (android.bluetooth.BluetoothProfile$ServiceListener)new com.navdy.hud.app.device.dial.DialManagerHelper$7(a0, a1), 4);
            sLogger.v(new StringBuilder().append("[Dial]disconnectFromDial getProfileProxy returned:").append(b).toString());
            if (!b) {
                com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.device.dial.DialManagerHelper$8(a1), 1);
            }
        } catch(Throwable a2) {
            sLogger.e("disconnectFromDial", a2);
            com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.device.dial.DialManagerHelper$9(a1), 1);
        }
    }
    
    private static boolean forceConnect(android.bluetooth.BluetoothProfile a, android.bluetooth.BluetoothDevice a0) {
        return com.navdy.hud.app.device.dial.DialManagerHelper.invokeMethod(a, a0, "connect");
    }
    
    private static boolean forceDisconnect(android.bluetooth.BluetoothProfile a, android.bluetooth.BluetoothDevice a0) {
        return com.navdy.hud.app.device.dial.DialManagerHelper.invokeMethod(a, a0, "disconnect");
    }
    
    private static boolean invokeMethod(android.bluetooth.BluetoothProfile a, android.bluetooth.BluetoothDevice a0, String s) {
        boolean b = false;
        Boolean a1 = Boolean.valueOf(false);
        try {
            try {
                Class a2 = Class.forName("android.bluetooth.BluetoothInputDevice");
                Class[] a3 = new Class[1];
                a3[0] = android.bluetooth.BluetoothDevice.class;
                java.lang.reflect.Method a4 = a2.getMethod(s, a3);
                Object[] a5 = new Object[1];
                a5[0] = a0;
                a1 = (Boolean)a4.invoke(a, a5);
            } catch(Throwable a6) {
                sLogger.e("[Dial]", a6);
            }
            b = a1.booleanValue();
        } catch(Throwable a7) {
            sLogger.e("[Dial]", a7);
            b = false;
        }
        return b;
    }
    
    public static void isDialConnected(android.bluetooth.BluetoothAdapter a, android.bluetooth.BluetoothDevice a0, com.navdy.hud.app.device.dial.DialManagerHelper$IDialConnectionStatus a1) {
        try {
            label0: {
                label1: {
                    if (a == null) {
                        break label1;
                    }
                    if (a0 == null) {
                        break label1;
                    }
                    if (a1 != null) {
                        break label0;
                    }
                }
                throw new IllegalArgumentException();
            }
            boolean b = a.getProfileProxy(com.navdy.hud.app.HudApplication.getAppContext(), (android.bluetooth.BluetoothProfile$ServiceListener)new com.navdy.hud.app.device.dial.DialManagerHelper$1(a0, a1), 4);
            sLogger.v(new StringBuilder().append("[Dial]getProfileProxy returned:").append(b).toString());
            if (!b) {
                com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.device.dial.DialManagerHelper$2(a1), 1);
            }
        } catch(Throwable a2) {
            sLogger.e("isDialConnected", a2);
            com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.device.dial.DialManagerHelper$3(a1), 1);
        }
    }
    
    public static void sendLocalyticsEvent(android.os.Handler a, boolean b, boolean b0, int i, boolean b1) {
        com.navdy.hud.app.device.dial.DialManagerHelper.sendLocalyticsEvent(a, b, b0, i, b1, (String)null);
    }
    
    public static void sendLocalyticsEvent(android.os.Handler a, boolean b, boolean b0, int i, boolean b1, String s) {
        a.postDelayed((Runnable)new com.navdy.hud.app.device.dial.DialManagerHelper$11(b, b0, b1, s, com.navdy.hud.app.device.dial.DialManager.getInstance().getDialDevice()), b0 ? (long)i : 0L);
    }
    
    public static void sendRebootLocalyticsEvent(android.os.Handler a, boolean b, android.bluetooth.BluetoothDevice a0, String s) {
        a.post((Runnable)new com.navdy.hud.app.device.dial.DialManagerHelper$10(s, b, a0));
    }
}
