package com.navdy.hud.app.device.dial;

class DialManager$2 implements Runnable {
    final com.navdy.hud.app.device.dial.DialManager this$0;
    
    DialManager$2(com.navdy.hud.app.device.dial.DialManager a) {
        super();
        this.this$0 = a;
    }
    
    public void run() {
        try {
            com.navdy.hud.app.device.dial.DialManager.sLogger.e("[Dial]bond hang detected");
            android.bluetooth.BluetoothDevice a = com.navdy.hud.app.device.dial.DialManager.access$000(this.this$0);
            String s = null;
            if (a != null) {
                s = com.navdy.hud.app.device.dial.DialManager.access$000(this.this$0).getName();
            }
            com.navdy.hud.app.device.dial.DialManager.access$002(this.this$0, (android.bluetooth.BluetoothDevice)null);
            com.navdy.hud.app.device.dial.DialManager.access$100().dialName = s;
            com.navdy.hud.app.device.dial.DialManager.access$200(this.this$0).post(com.navdy.hud.app.device.dial.DialManager.access$100());
        } catch(Throwable a0) {
            com.navdy.hud.app.device.dial.DialManager.sLogger.e("[Dial]", a0);
        }
    }
}
