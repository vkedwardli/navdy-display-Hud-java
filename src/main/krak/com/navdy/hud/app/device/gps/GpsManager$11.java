package com.navdy.hud.app.device.gps;

class GpsManager$11 extends android.content.BroadcastReceiver {
    final com.navdy.hud.app.device.gps.GpsManager this$0;
    
    GpsManager$11(com.navdy.hud.app.device.gps.GpsManager a) {
        super();
        this.this$0 = a;
    }
    
    public void onReceive(android.content.Context a, android.content.Intent a0) {
        try {
            String s = a0.getAction();
            if (s != null) {
                int i = 0;
                com.navdy.hud.app.debug.RouteRecorder a1 = com.navdy.hud.app.debug.RouteRecorder.getInstance();
                int i0 = s.hashCode();
                label0: {
                    switch(i0) {
                        case -47674184: {
                            if (!s.equals("EXTRAPOLATION")) {
                                break;
                            }
                            i = 2;
                            break label0;
                        }
                        case -1788590639: {
                            if (!s.equals("GPS_DR_STOPPED")) {
                                break;
                            }
                            i = 1;
                            break label0;
                        }
                        case -1801456507: {
                            if (!s.equals("GPS_DR_STARTED")) {
                                break;
                            }
                            i = 0;
                            break label0;
                        }
                    }
                    i = -1;
                }
                switch(i) {
                    case 2: {
                        com.navdy.hud.app.device.gps.GpsManager.access$2402(this.this$0, a0.getBooleanExtra("EXTRAPOLATION_FLAG", false));
                        if (com.navdy.hud.app.device.gps.GpsManager.access$2400(this.this$0)) {
                            com.navdy.hud.app.device.gps.GpsManager.access$2502(this.this$0, android.os.SystemClock.elapsedRealtime());
                        } else {
                            com.navdy.hud.app.device.gps.GpsManager.access$2502(this.this$0, 0L);
                        }
                        com.navdy.hud.app.device.gps.GpsManager.access$000().v(new StringBuilder().append("extrapolation is:").append(com.navdy.hud.app.device.gps.GpsManager.access$2400(this.this$0)).append(" time:").append(com.navdy.hud.app.device.gps.GpsManager.access$2500(this.this$0)).toString());
                        break;
                    }
                    case 1: {
                        if (!a1.isRecording()) {
                            break;
                        }
                        a1.injectMarker("GPS_DR_STOPPED");
                        break;
                    }
                    case 0: {
                        if (!a1.isRecording()) {
                            break;
                        }
                        String s0 = a0.getStringExtra("drtype");
                        if (s0 == null) {
                            s0 = "";
                        }
                        a1.injectMarker(new StringBuilder().append("GPS_DR_STARTED ").append(s0).toString());
                        break;
                    }
                }
            }
        } catch(Throwable a2) {
            com.navdy.hud.app.device.gps.GpsManager.access$000().e(a2);
        }
    }
}
