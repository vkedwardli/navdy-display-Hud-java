package com.navdy.hud.app.device.dial;

class DialManager$9 extends android.content.BroadcastReceiver {
    final com.navdy.hud.app.device.dial.DialManager this$0;
    
    DialManager$9(com.navdy.hud.app.device.dial.DialManager a) {
        super();
        this.this$0 = a;
    }
    
    public void onReceive(android.content.Context a, android.content.Intent a0) {
        label0: {
            Throwable a1 = null;
            if (a0 == null) {
                break label0;
            }
            try {
                String s = a0.getAction();
                android.bluetooth.BluetoothDevice a2 = (android.bluetooth.BluetoothDevice)a0.getParcelableExtra("android.bluetooth.device.extra.DEVICE");
                if (a2 == null) {
                    break label0;
                }
                if (this.this$0.isDialDevice(a2)) {
                    boolean b = false;
                    com.navdy.hud.app.device.dial.DialManager.sLogger.v(new StringBuilder().append("[Dial]btConnRecvr [").append(a2.getName()).append("] action [").append(s).append("]").toString());
                    if (com.navdy.hud.app.util.DeviceUtil.isNavdyDevice()) {
                        break label0;
                    }
                    if ("android.bluetooth.input.profile.action.CONNECTION_STATE_CHANGED".equals(s)) {
                        int i = a0.getIntExtra("android.bluetooth.profile.extra.PREVIOUS_STATE", -1);
                        int i0 = a0.getIntExtra("android.bluetooth.profile.extra.STATE", -1);
                        com.navdy.hud.app.device.dial.DialManager.sLogger.v(new StringBuilder().append("[Dial]btConnRecvr: dial connection state changed old[").append(i).append("] new[").append(i0).append("]").toString());
                        b = i0 == 2;
                    } else {
                        b = false;
                    }
                    android.bluetooth.BluetoothDevice a3 = com.navdy.hud.app.device.dial.DialManager.access$300(this.this$0);
                    label3: {
                        if (a3 == null) {
                            break label3;
                        }
                        if (android.text.TextUtils.equals((CharSequence)com.navdy.hud.app.device.dial.DialManager.access$300(this.this$0).getName(), (CharSequence)a2.getName())) {
                            break label3;
                        }
                        com.navdy.hud.app.device.dial.DialManager.sLogger.v("btConnRecvr: notification for not current connected dial");
                        com.navdy.hud.app.device.dial.DialManager.access$400(this.this$0).removeCallbacks(com.navdy.hud.app.device.dial.DialManager.access$600(this.this$0));
                        break label0;
                    }
                    boolean b0 = "android.bluetooth.device.action.ACL_CONNECTED".equals(s);
                    label1: {
                        label2: {
                            if (b0) {
                                break label2;
                            }
                            if (!b) {
                                break label1;
                            }
                        }
                        com.navdy.hud.app.device.dial.DialManager.access$400(this.this$0).removeCallbacks(com.navdy.hud.app.device.dial.DialManager.access$600(this.this$0));
                        com.navdy.hud.app.device.dial.DialManager.access$302(this.this$0, a2);
                        if (com.navdy.hud.app.device.dial.DialManager.access$700(this.this$0, a2)) {
                            com.navdy.hud.app.device.dial.DialManager.sLogger.v("attempt connection reqd");
                            com.navdy.hud.app.device.dial.DialManagerHelper.connectToDial(com.navdy.hud.app.device.dial.DialManager.access$800(this.this$0), a2, (com.navdy.hud.app.device.dial.DialManagerHelper$IDialConnection)new com.navdy.hud.app.device.dial.DialManager$9$1(this, a2));
                            break label0;
                        } else {
                            com.navdy.hud.app.device.dial.DialManager.sLogger.v("attempt connection not read");
                            com.navdy.hud.app.device.dial.DialManager.access$1500(this.this$0);
                            break label0;
                        }
                    }
                    if (!"android.bluetooth.device.action.ACL_DISCONNECTED".equals(s)) {
                        break label0;
                    }
                    com.navdy.hud.app.device.dial.DialManager.access$400(this.this$0).removeCallbacks(com.navdy.hud.app.device.dial.DialManager.access$600(this.this$0));
                    if (com.navdy.hud.app.device.dial.DialManager.access$300(this.this$0) != null) {
                        com.navdy.hud.app.device.dial.DialManager.access$1602(this.this$0, com.navdy.hud.app.device.dial.DialManager.access$300(this.this$0).getName());
                    }
                    com.navdy.hud.app.device.dial.DialManager.access$100().dialName = com.navdy.hud.app.device.dial.DialManager.access$1600(this.this$0);
                    com.navdy.hud.app.device.dial.DialManager.access$200(this.this$0).post(com.navdy.hud.app.device.dial.DialManager.access$100());
                    this.this$0.getBondedDialCount();
                    com.navdy.hud.app.device.dial.DialManager.access$1700(this.this$0);
                    com.navdy.hud.app.device.dial.DialManager.access$1800(this.this$0);
                    com.navdy.hud.app.device.dial.DialManager.sLogger.e(new StringBuilder().append("[Dial]btConnRecvr: dial NOT connected [").append(a2.getName()).append("] addr:").append(a2.getAddress()).toString());
                    break label0;
                } else {
                    com.navdy.hud.app.device.dial.DialManager.sLogger.i(new StringBuilder().append("[Dial]btConnRecvr: notification not for dial:").append(a2.getName()).append(" addr:").append(a2.getAddress()).toString());
                    break label0;
                }
            } catch(Throwable a4) {
                a1 = a4;
            }
            com.navdy.hud.app.device.dial.DialManager.sLogger.e("[Dial]", a1);
        }
    }
}
