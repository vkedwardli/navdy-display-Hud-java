package com.navdy.hud.app.device.dial;

class DialManager$4 extends android.bluetooth.le.ScanCallback {
    final com.navdy.hud.app.device.dial.DialManager this$0;
    
    DialManager$4(com.navdy.hud.app.device.dial.DialManager a) {
        super();
        this.this$0 = a;
    }
    
    public void onScanFailed(int i) {
        com.navdy.hud.app.device.dial.DialManager.sLogger.e(new StringBuilder().append("btle scan failed:").append(i).toString());
        this.this$0.stopScan(true);
    }
    
    public void onScanResult(int i, android.bluetooth.le.ScanResult a) {
        try {
            android.bluetooth.le.ScanRecord a0 = a.getScanRecord();
            com.navdy.hud.app.device.dial.DialManager.access$500(this.this$0, a.getDevice(), a0.getTxPowerLevel(), a0);
        } catch(Throwable a1) {
            com.navdy.hud.app.device.dial.DialManager.sLogger.e("[Dial]", a1);
        }
    }
}
