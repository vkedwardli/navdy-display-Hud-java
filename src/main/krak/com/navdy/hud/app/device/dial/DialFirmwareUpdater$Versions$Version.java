package com.navdy.hud.app.device.dial;

public class DialFirmwareUpdater$Versions$Version {
    public int incrementalVersion;
    public String revisionString;
    final com.navdy.hud.app.device.dial.DialFirmwareUpdater$Versions this$1;
    
    DialFirmwareUpdater$Versions$Version(com.navdy.hud.app.device.dial.DialFirmwareUpdater$Versions a) {
        super();
        this.this$1 = a;
        this.reset();
    }
    
    void reset() {
        this.revisionString = null;
        this.incrementalVersion = -1;
    }
}
