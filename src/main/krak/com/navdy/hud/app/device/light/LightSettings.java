package com.navdy.hud.app.device.light;

public class LightSettings {
    protected int color;
    protected boolean isBlinking;
    protected boolean isTurnedOn;
    
    public LightSettings(int i, boolean b, boolean b0) {
        this.color = i;
        this.isTurnedOn = b;
        this.isBlinking = b0;
    }
    
    public int getColor() {
        return this.color;
    }
    
    public boolean isBlinking() {
        return this.isBlinking;
    }
    
    public boolean isTurnedOn() {
        return this.isTurnedOn;
    }
}
