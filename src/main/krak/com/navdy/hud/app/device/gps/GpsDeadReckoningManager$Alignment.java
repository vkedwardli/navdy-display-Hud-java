package com.navdy.hud.app.device.gps;

class GpsDeadReckoningManager$Alignment {
    boolean done;
    float pitch;
    float roll;
    float yaw;
    
    GpsDeadReckoningManager$Alignment(float f, float f0, float f1, boolean b) {
        this.yaw = f;
        this.pitch = f0;
        this.roll = f1;
        this.done = b;
    }
    
    public String toString() {
        StringBuilder a = new StringBuilder("Alignment{");
        a.append("yaw=").append(this.yaw);
        a.append(", pitch=").append(this.pitch);
        a.append(", roll=").append(this.roll);
        a.append(", done=").append(this.done);
        a.append((char)125);
        return a.toString();
    }
}
