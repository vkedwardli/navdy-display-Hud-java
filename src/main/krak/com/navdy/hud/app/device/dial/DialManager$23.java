package com.navdy.hud.app.device.dial;

class DialManager$23 implements com.navdy.hud.app.device.dial.DialManagerHelper$IDialConnection {
    final com.navdy.hud.app.device.dial.DialManager this$0;
    final android.bluetooth.BluetoothDevice val$device;
    
    DialManager$23(com.navdy.hud.app.device.dial.DialManager a, android.bluetooth.BluetoothDevice a0) {
        super();
        this.this$0 = a;
        this.val$device = a0;
    }
    
    public void onAttemptConnection(boolean b) {
    }
    
    public void onAttemptDisconnection(boolean b) {
        com.navdy.hud.app.device.dial.DialManager.sLogger.e("[DialEvent-encryptfail] disconnected");
        com.navdy.hud.app.device.dial.DialManager.access$400(this.this$0).postDelayed((Runnable)new com.navdy.hud.app.device.dial.DialManager$23$1(this), 3000L);
    }
}
