package com.navdy.hud.app.device.dial;

class DialManager$1 implements Runnable {
    final com.navdy.hud.app.device.dial.DialManager this$0;
    
    DialManager$1(com.navdy.hud.app.device.dial.DialManager a) {
        super();
        this.this$0 = a;
    }
    
    public void run() {
        try {
            com.navdy.hud.app.device.dial.DialManager.sLogger.v("scan hang runnable");
            this.this$0.stopScan(true);
        } catch(Throwable a) {
            com.navdy.hud.app.device.dial.DialManager.sLogger.e("[Dial]", a);
        }
    }
}
