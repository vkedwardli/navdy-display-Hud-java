package com.navdy.hud.app.device.dial;

class DialManager$7 extends android.content.BroadcastReceiver {
    final com.navdy.hud.app.device.dial.DialManager this$0;
    
    DialManager$7(com.navdy.hud.app.device.dial.DialManager a) {
        super();
        this.this$0 = a;
    }
    
    public void onReceive(android.content.Context a, android.content.Intent a0) {
        try {
            if (a0.getAction().equals("android.bluetooth.adapter.action.STATE_CHANGED")) {
                switch(a0.getIntExtra("android.bluetooth.adapter.extra.STATE", -2147483648)) {
                    case 13: {
                        com.navdy.hud.app.device.dial.DialManager.sLogger.v("[Dial]onoff: state turning off");
                        break;
                    }
                    case 12: {
                        com.navdy.hud.app.device.dial.DialManager.sLogger.v("[Dial]onoff: state on");
                        if (com.navdy.hud.app.device.dial.DialManager.access$1000(this.this$0)) {
                            break;
                        }
                        com.navdy.hud.app.device.dial.DialManager.access$1100(this.this$0);
                        break;
                    }
                    case 11: {
                        com.navdy.hud.app.device.dial.DialManager.sLogger.v("[Dial]onoff: state turning on");
                        break;
                    }
                    case 10: {
                        com.navdy.hud.app.device.dial.DialManager.sLogger.v("[Dial]onoff: state off");
                        break;
                    }
                }
            }
        } catch(Throwable a1) {
            com.navdy.hud.app.device.dial.DialManager.sLogger.e("[Dial]", a1);
        }
    }
}
