package com.navdy.hud.app.device.light;

public class LightManager {
    final public static int FRONT_LED = 0;
    final private static String LED_DEVICE = "/sys/class/leds/as3668";
    final private static com.navdy.hud.app.device.light.LightManager sInstance;
    private android.util.SparseArray mLights;
    
    static {
        sInstance = new com.navdy.hud.app.device.light.LightManager();
    }
    
    private LightManager() {
        this.mLights = new android.util.SparseArray();
        com.navdy.hud.app.device.light.LED a = new com.navdy.hud.app.device.light.LED("/sys/class/leds/as3668");
        if (a.isAvailable()) {
            this.mLights.append(0, a);
        }
    }
    
    public static com.navdy.hud.app.device.light.LightManager getInstance() {
        return sInstance;
    }
    
    public com.navdy.hud.app.device.light.ILight getLight(int i) {
        return (com.navdy.hud.app.device.light.ILight)this.mLights.get(i);
    }
}
