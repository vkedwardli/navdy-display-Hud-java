package com.navdy.hud.app.device.dial;

abstract public interface DialManagerHelper$IDialConnection {
    abstract public void onAttemptConnection(boolean arg);
    
    
    abstract public void onAttemptDisconnection(boolean arg);
}
