package com.navdy.hud.app.device.light;
import com.navdy.hud.app.R;

public class HUDLightUtils {
    public static com.navdy.hud.app.device.light.LED$Settings gestureDetectedLedSettings;
    public static com.navdy.hud.app.device.light.LED$Settings pairingLedSettings;
    final public static com.navdy.service.library.log.Logger sLogger;
    public static com.navdy.hud.app.device.light.LED$Settings snapShotCollectionEnd;
    public static com.navdy.hud.app.device.light.LED$Settings snapshotCollectionStart;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.device.light.HUDLightUtils.class);
        int i = com.navdy.hud.app.HudApplication.getAppContext().getResources().getColor(R.color.led_usb_power_color);
        snapshotCollectionStart = new com.navdy.hud.app.device.light.LED$Settings$Builder().setName("SnapshotStart").setColor(i).setIsBlinking(false).build();
        snapShotCollectionEnd = new com.navdy.hud.app.device.light.LED$Settings$Builder().setName("SnapshotEnd").setColor(i).setIsBlinking(true).setBlinkInfinite(false).setBlinkPulseCount(3).setBlinkFrequency(com.navdy.hud.app.device.light.LED$BlinkFrequency.HIGH).build();
        int i0 = com.navdy.hud.app.HudApplication.getAppContext().getResources().getColor(R.color.led_gesture_color);
        gestureDetectedLedSettings = new com.navdy.hud.app.device.light.LED$Settings$Builder().setName("GestureDetected").setColor(i0).setIsBlinking(true).setBlinkInfinite(false).setBlinkPulseCount(2).setBlinkFrequency(com.navdy.hud.app.device.light.LED$BlinkFrequency.HIGH).build();
        int i1 = com.navdy.hud.app.HudApplication.getAppContext().getResources().getColor(R.color.led_dial_pairing_color);
        pairingLedSettings = new com.navdy.hud.app.device.light.LED$Settings$Builder().setName("Pairing").setColor(i1).setIsBlinking(true).setBlinkInfinite(true).setBlinkFrequency(com.navdy.hud.app.device.light.LED$BlinkFrequency.LOW).build();
    }
    
    public HUDLightUtils() {
    }
    
    public static void dialActionDetected(android.content.Context a, com.navdy.hud.app.device.light.LightManager a0) {
        com.navdy.hud.app.device.light.LED a1 = (com.navdy.hud.app.device.light.LED)a0.getLight(0);
        if (a1 != null) {
            sLogger.d("Front LED, showing dial action detected");
            a1.startActivityBlink();
        }
    }
    
    public static void removeSettings(com.navdy.hud.app.device.light.LED$Settings a) {
        if (a != null) {
            com.navdy.hud.app.device.light.LED a0 = (com.navdy.hud.app.device.light.LED)com.navdy.hud.app.device.light.LightManager.getInstance().getLight(0);
            if (a0 != null) {
                a0.removeSetting((com.navdy.hud.app.device.light.LightSettings)a);
            }
        }
    }
    
    public static void resetFrontLED(com.navdy.hud.app.device.light.LightManager a) {
        com.navdy.hud.app.device.light.LED a0 = (com.navdy.hud.app.device.light.LED)a.getLight(0);
        if (a0 != null) {
            sLogger.d("resetFrontLED called");
            a0.reset();
        }
    }
    
    public static void showError(android.content.Context a, com.navdy.hud.app.device.light.LightManager a0) {
        com.navdy.hud.app.device.light.LED a1 = (com.navdy.hud.app.device.light.LED)a0.getLight(0);
        if (a1 != null) {
            sLogger.d("Front LED, showing error, not blinking");
            int i = a.getResources().getColor(R.color.led_error_color);
            a1.pushSetting((com.navdy.hud.app.device.light.LightSettings)new com.navdy.hud.app.device.light.LED$Settings$Builder().setColor(i).setIsBlinking(false).build());
        }
    }
    
    public static void showGestureDetected(android.content.Context a, com.navdy.hud.app.device.light.LightManager a0) {
        com.navdy.hud.app.device.light.LED a1 = (com.navdy.hud.app.device.light.LED)a0.getLight(0);
        if (a1 != null) {
            sLogger.d("Front LED, showing gesture detected");
            a1.pushSetting((com.navdy.hud.app.device.light.LightSettings)gestureDetectedLedSettings);
            a1.removeSetting((com.navdy.hud.app.device.light.LightSettings)gestureDetectedLedSettings);
        }
    }
    
    public static com.navdy.hud.app.device.light.LED$Settings showGestureDetectionEnabled(android.content.Context a, com.navdy.hud.app.device.light.LightManager a0, String s) {
        com.navdy.hud.app.device.light.LED$Settings a1 = null;
        com.navdy.hud.app.device.light.LED a2 = (com.navdy.hud.app.device.light.LED)a0.getLight(0);
        if (a2 == null) {
            a1 = null;
        } else {
            sLogger.d("Front LED, showing gesture detection enabled");
            int i = a.getResources().getColor(R.color.led_gesture_color);
            a1 = new com.navdy.hud.app.device.light.LED$Settings$Builder().setName(new StringBuilder().append("GestureDetectionEnabled-").append(s).toString()).setColor(i).setIsBlinking(true).setBlinkInfinite(true).setBlinkFrequency(com.navdy.hud.app.device.light.LED$BlinkFrequency.LOW).build();
            a2.pushSetting((com.navdy.hud.app.device.light.LightSettings)a1);
        }
        return a1;
    }
    
    public static void showGestureNotRecognized(android.content.Context a, com.navdy.hud.app.device.light.LightManager a0) {
        com.navdy.hud.app.device.light.LED a1 = (com.navdy.hud.app.device.light.LED)a0.getLight(0);
        if (a1 != null) {
            sLogger.d("Front LED, showing gesture not recognized");
            int i = a.getResources().getColor(R.color.led_gesture_not_recognized_color);
            a1.pushSetting((com.navdy.hud.app.device.light.LightSettings)new com.navdy.hud.app.device.light.LED$Settings$Builder().setColor(i).setIsBlinking(false).setBlinkFrequency(com.navdy.hud.app.device.light.LED$BlinkFrequency.LOW).build());
        }
    }
    
    public static void showPairing(android.content.Context a, com.navdy.hud.app.device.light.LightManager a0, boolean b) {
        com.navdy.hud.app.device.light.LED a1 = (com.navdy.hud.app.device.light.LED)a0.getLight(0);
        if (a1 != null) {
            if (b) {
                sLogger.d("Front LED, showing dial pairing");
                a1.pushSetting((com.navdy.hud.app.device.light.LightSettings)pairingLedSettings);
            } else {
                sLogger.d("Front LED, stop showing dial pairing");
                a1.removeSetting((com.navdy.hud.app.device.light.LightSettings)pairingLedSettings);
            }
        }
    }
    
    public static void showShutDown(com.navdy.hud.app.device.light.LightManager a) {
        com.navdy.hud.app.device.light.LED a0 = (com.navdy.hud.app.device.light.LED)a.getLight(0);
        if (a0 != null) {
            sLogger.d("Front LED, showing shutdown");
            int i = com.navdy.hud.app.device.light.LED.DEFAULT_COLOR;
            a0.pushSetting((com.navdy.hud.app.device.light.LightSettings)new com.navdy.hud.app.device.light.LED$Settings$Builder().setColor(i).setIsBlinking(true).setBlinkInfinite(true).setBlinkFrequency(com.navdy.hud.app.device.light.LED$BlinkFrequency.LOW).build());
        }
    }
    
    public static void showSnapshotCollection(android.content.Context a, com.navdy.hud.app.device.light.LightManager a0, boolean b) {
        com.navdy.hud.app.device.light.LED$Settings a1 = b ? snapShotCollectionEnd : snapshotCollectionStart;
        com.navdy.hud.app.device.light.LED a2 = (com.navdy.hud.app.device.light.LED)a0.getLight(0);
        if (a2 != null) {
            a2.pushSetting((com.navdy.hud.app.device.light.LightSettings)a1);
            if (b) {
                a2.removeSetting((com.navdy.hud.app.device.light.LightSettings)snapShotCollectionEnd);
                a2.removeSetting((com.navdy.hud.app.device.light.LightSettings)snapshotCollectionStart);
            }
        }
    }
    
    public static void showUSBPowerOn(android.content.Context a, com.navdy.hud.app.device.light.LightManager a0) {
        com.navdy.hud.app.device.light.LED a1 = (com.navdy.hud.app.device.light.LED)a0.getLight(0);
        if (a1 != null) {
            sLogger.d("Front LED, showing USB power on, not blinking");
            int i = a.getResources().getColor(R.color.led_usb_power_color);
            a1.pushSetting((com.navdy.hud.app.device.light.LightSettings)new com.navdy.hud.app.device.light.LED$Settings$Builder().setColor(i).setIsBlinking(false).build());
        }
    }
    
    public static void showUSBPowerShutDown(android.content.Context a, com.navdy.hud.app.device.light.LightManager a0) {
        com.navdy.hud.app.device.light.LED a1 = (com.navdy.hud.app.device.light.LED)a0.getLight(0);
        if (a1 != null) {
            sLogger.d("Front LED, showing USB power shutdown");
            int i = a.getResources().getColor(R.color.led_usb_power_color);
            a1.pushSetting((com.navdy.hud.app.device.light.LightSettings)new com.navdy.hud.app.device.light.LED$Settings$Builder().setColor(i).setIsBlinking(true).setBlinkInfinite(true).setBlinkFrequency(com.navdy.hud.app.device.light.LED$BlinkFrequency.LOW).build());
        }
    }
    
    public static void showUSBTransfer(android.content.Context a, com.navdy.hud.app.device.light.LightManager a0) {
        com.navdy.hud.app.device.light.LED a1 = (com.navdy.hud.app.device.light.LED)a0.getLight(0);
        if (a1 != null) {
            sLogger.d("Front LED, showing USB transfer");
            int i = a.getResources().getColor(R.color.led_usb_power_color);
            a1.pushSetting((com.navdy.hud.app.device.light.LightSettings)new com.navdy.hud.app.device.light.LED$Settings$Builder().setColor(i).setIsBlinking(true).setBlinkInfinite(true).setBlinkFrequency(com.navdy.hud.app.device.light.LED$BlinkFrequency.HIGH).build());
        }
    }
    
    public static void turnOffFrontLED(com.navdy.hud.app.device.light.LightManager a) {
        com.navdy.hud.app.device.light.LED a0 = (com.navdy.hud.app.device.light.LED)a.getLight(0);
        if (a0 != null) {
            sLogger.d("turnOffFrontLED called");
            a0.turnOff();
        }
    }
}
