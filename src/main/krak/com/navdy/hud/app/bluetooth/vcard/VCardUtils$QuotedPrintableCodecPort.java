package com.navdy.hud.app.bluetooth.vcard;

class VCardUtils$QuotedPrintableCodecPort {
    private static byte ESCAPE_CHAR;
    
    static {
        ESCAPE_CHAR = (byte)61;
    }
    
    private VCardUtils$QuotedPrintableCodecPort() {
    }
    
    final public static byte[] decodeQuotedPrintable(byte[] a) {
        byte[] a0 = null;
        label0: {
            label1: if (a != null) {
                java.io.ByteArrayOutputStream a1 = new java.io.ByteArrayOutputStream();
                int i = 0;
                while(i < a.length) {
                    int i0 = a[i];
                    int i1 = ESCAPE_CHAR;
                    label3: if (i0 != i1) {
                        a1.write(i0);
                    } else {
                        try {
                            int i2 = i + 1;
                            int i3 = a[i2];
                            int i4 = (char)i3;
                            int i5 = Character.digit((char)i4, 16);
                            i = i2 + 1;
                            int i6 = a[i];
                            int i7 = (char)i6;
                            int i8 = Character.digit((char)i7, 16);
                            label2: {
                                if (i5 == -1) {
                                    break label2;
                                }
                                if (i8 == -1) {
                                    break label2;
                                }
                                int i9 = (char)((i5 << 4) + i8);
                                a1.write(i9);
                                break label3;
                            }
                            try {
                                throw new com.navdy.hud.app.bluetooth.vcard.VCardUtils$DecoderException("Invalid quoted-printable encoding");
                            } catch(ArrayIndexOutOfBoundsException ignoredException) {
                                break label1;
                            }
                        } catch(ArrayIndexOutOfBoundsException ignoredException0) {
                            break label1;
                        }
                    }
                    i = i + 1;
                }
                a0 = a1.toByteArray();
                break label0;
            } else {
                a0 = null;
                break label0;
            }
            throw new com.navdy.hud.app.bluetooth.vcard.VCardUtils$DecoderException("Invalid quoted-printable encoding");
        }
        return a0;
    }
}
