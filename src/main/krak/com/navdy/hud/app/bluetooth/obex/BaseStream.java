package com.navdy.hud.app.bluetooth.obex;

abstract public interface BaseStream {
    abstract public boolean continueOperation(boolean arg, boolean arg0);
    
    
    abstract public void ensureNotDone();
    
    
    abstract public void ensureOpen();
    
    
    abstract public void streamClosed(boolean arg);
}
