package com.navdy.hud.app.bluetooth.obex;

final public class PrivateInputStream extends java.io.InputStream {
    private byte[] mData;
    private int mIndex;
    private boolean mOpen;
    private com.navdy.hud.app.bluetooth.obex.BaseStream mParent;
    
    public PrivateInputStream(com.navdy.hud.app.bluetooth.obex.BaseStream a) {
        this.mParent = a;
        this.mData = new byte[0];
        this.mIndex = 0;
        this.mOpen = true;
    }
    
    private void ensureOpen() {
        this.mParent.ensureOpen();
        if (!this.mOpen) {
            throw new java.io.IOException("Input stream is closed");
        }
    }
    
    public int available() {
        int i = 0;
        int i0 = 0;
        synchronized(this) {
            this.ensureOpen();
            i = this.mData.length;
            i0 = this.mIndex;
        }
        /*monexit(this)*/;
        return i - i0;
    }
    
    public void close() {
        this.mOpen = false;
        this.mParent.streamClosed(true);
    }
    
    public int read() {
        int i = 0;
        label2: synchronized(this) {
            byte[] a = null;
            int i0 = 0;
            label1: {
                label0: try {
                    this.ensureOpen();
                    while(this.mData.length == this.mIndex) {
                        if (!this.mParent.continueOperation(true, true)) {
                            break label0;
                        }
                    }
                    a = this.mData;
                    i0 = this.mIndex;
                    this.mIndex = i0 + 1;
                    break label1;
                } catch(Throwable a0) {
                    /*monexit(this)*/;
                    throw a0;
                }
                i = -1;
                break label2;
            }
            int i1 = a[i0];
            i = i1 & 255;
        }
        /*monexit(this)*/;
        return i;
    }
    
    public int read(byte[] a) {
        return this.read(a, 0, a.length);
    }
    
    public int read(byte[] a, int i, int i0) {
        int i1 = 0;
        synchronized(this) {
            if (a == null) {
                throw new java.io.IOException("buffer is null");
            }
            int i2 = i | i0;
            label0: {
                label1: {
                    if (i2 < 0) {
                        break label1;
                    }
                    if (i0 <= a.length - i) {
                        break label0;
                    }
                }
                throw new ArrayIndexOutOfBoundsException("index outof bound");
            }
            this.ensureOpen();
            int i3 = this.mData.length - this.mIndex;
            i1 = 0;
            while(true) {
                if (i3 > i0) {
                    if (i0 > 0) {
                        System.arraycopy(this.mData, this.mIndex, a, i, i0);
                        this.mIndex = this.mIndex + i0;
                        i1 = i1 + i0;
                    }
                    break;
                } else {
                    System.arraycopy(this.mData, this.mIndex, a, i, i3);
                    this.mIndex = this.mIndex + i3;
                    int i4 = i + i3;
                    i1 = i1 + i3;
                    int i5 = i0 - i3;
                    if (this.mParent.continueOperation(true, true)) {
                        i3 = this.mData.length - this.mIndex;
                        i = i4;
                        i0 = i5;
                        continue;
                    }
                    if (i1 != 0) {
                        break;
                    }
                    i1 = -1;
                    break;
                }
            }
        }
        /*monexit(this)*/;
        return i1;
    }
    
    public void writeBytes(byte[] a, int i) {
        synchronized(this) {
            byte[] a0 = new byte[a.length - i + (this.mData.length - this.mIndex)];
            System.arraycopy(this.mData, this.mIndex, a0, 0, this.mData.length - this.mIndex);
            System.arraycopy(a, i, a0, this.mData.length - this.mIndex, a.length - i);
            this.mData = a0;
            this.mIndex = 0;
            (this).notifyAll();
        }
        /*monexit(this)*/;
    }
}
