package com.navdy.hud.app.bluetooth.vcard;

public class VCardConstants {
    final public static int CALL_TYPE_INCOMING = 10;
    final public static int CALL_TYPE_MISSED = 12;
    final public static int CALL_TYPE_OUTGOING = 11;
    final public static int CALL_TYPE_UNKNOWN = 0;
    final public static String DIALED = "DIALED";
    final static String LOG_TAG = "vCard";
    final static int MAX_CHARACTER_NUMS_BASE64_V30 = 75;
    final static int MAX_CHARACTER_NUMS_QP = 76;
    final public static int MAX_DATA_COLUMN = 15;
    final public static String MISSED = "MISSED";
    final public static String PARAM_ADR_TYPE_DOM = "DOM";
    final public static String PARAM_ADR_TYPE_INTL = "INTL";
    final public static String PARAM_ADR_TYPE_PARCEL = "PARCEL";
    final public static String PARAM_CHARSET = "CHARSET";
    final public static String PARAM_ENCODING = "ENCODING";
    final public static String PARAM_ENCODING_7BIT = "7BIT";
    final public static String PARAM_ENCODING_8BIT = "8BIT";
    final public static String PARAM_ENCODING_B = "B";
    final public static String PARAM_ENCODING_BASE64 = "BASE64";
    final public static String PARAM_ENCODING_QP = "QUOTED-PRINTABLE";
    final public static String PARAM_EXTRA_TYPE_COMPANY = "COMPANY";
    final public static String PARAM_LANGUAGE = "LANGUAGE";
    final public static String PARAM_PHONE_EXTRA_TYPE_ASSISTANT = "ASSISTANT";
    final public static String PARAM_PHONE_EXTRA_TYPE_CALLBACK = "CALLBACK";
    final public static String PARAM_PHONE_EXTRA_TYPE_COMPANY_MAIN = "COMPANY-MAIN";
    final public static String PARAM_PHONE_EXTRA_TYPE_OTHER = "OTHER";
    final public static String PARAM_PHONE_EXTRA_TYPE_RADIO = "RADIO";
    final public static String PARAM_PHONE_EXTRA_TYPE_TTY_TDD = "TTY-TDD";
    final public static String PARAM_SORT_AS = "SORT-AS";
    final public static String PARAM_TYPE = "TYPE";
    final public static String PARAM_TYPE_BBS = "BBS";
    final public static String PARAM_TYPE_CAR = "CAR";
    final public static String PARAM_TYPE_CELL = "CELL";
    final public static String PARAM_TYPE_FAX = "FAX";
    final public static String PARAM_TYPE_HOME = "HOME";
    final public static String PARAM_TYPE_INTERNET = "INTERNET";
    final public static String PARAM_TYPE_ISDN = "ISDN";
    final public static String PARAM_TYPE_MODEM = "MODEM";
    final public static String PARAM_TYPE_MSG = "MSG";
    final public static String PARAM_TYPE_PAGER = "PAGER";
    final public static String PARAM_TYPE_PREF = "PREF";
    final public static String PARAM_TYPE_TLX = "TLX";
    final public static String PARAM_TYPE_VIDEO = "VIDEO";
    final public static String PARAM_TYPE_VOICE = "VOICE";
    final public static String PARAM_TYPE_WORK = "WORK";
    final static String PARAM_TYPE_X_IRMC_N = "X-IRMC-N";
    final public static String PARAM_VALUE = "VALUE";
    final public static String PROPERTY_ADR = "ADR";
    final public static String PROPERTY_AGENT = "AGENT";
    final public static String PROPERTY_ANNIVERSARY = "ANNIVERSARY";
    final public static String PROPERTY_BDAY = "BDAY";
    final public static String PROPERTY_BEGIN = "BEGIN";
    final public static String PROPERTY_CALURI = "CALURI";
    final public static String PROPERTY_CATEGORIES = "CATEGORIES";
    final public static String PROPERTY_CLIENTPIDMAP = "CLIENTPIDMAP";
    final public static String PROPERTY_DATETIME = "X-IRMC-CALL-DATETIME";
    final public static String PROPERTY_EMAIL = "EMAIL";
    final public static String PROPERTY_END = "END";
    final public static String PROPERTY_FBURL = "FBURL";
    final public static String PROPERTY_FN = "FN";
    final public static String PROPERTY_GENDER = "GENDER";
    final public static String PROPERTY_IMPP = "IMPP";
    final public static String PROPERTY_LOGO = "LOGO";
    final public static String PROPERTY_N = "N";
    final public static String PROPERTY_NAME = "NAME";
    final public static String PROPERTY_NICKNAME = "NICKNAME";
    final public static String PROPERTY_NOTE = "NOTE";
    final public static String PROPERTY_ORG = "ORG";
    final public static String PROPERTY_PHOTO = "PHOTO";
    final public static String PROPERTY_PRODID = "PRODID";
    final public static String PROPERTY_RELATED = "RELATED";
    final public static String PROPERTY_REV = "REV";
    final public static String PROPERTY_ROLE = "ROLE";
    final public static String PROPERTY_SORT_STRING = "SORT-STRING";
    final public static String PROPERTY_SOUND = "SOUND";
    final public static String PROPERTY_TEL = "TEL";
    final public static String PROPERTY_TITLE = "TITLE";
    final public static String PROPERTY_URL = "URL";
    final public static String PROPERTY_VERSION = "VERSION";
    final public static String PROPERTY_XML = "XML";
    final public static String PROPERTY_X_AIM = "X-AIM";
    final public static String PROPERTY_X_ANDROID_CUSTOM = "X-ANDROID-CUSTOM";
    final public static String PROPERTY_X_CLASS = "X-CLASS";
    final public static String PROPERTY_X_DCM_HMN_MODE = "X-DCM-HMN-MODE";
    final public static String PROPERTY_X_GOOGLE_TALK = "X-GOOGLE-TALK";
    final public static String PROPERTY_X_ICQ = "X-ICQ";
    final public static String PROPERTY_X_JABBER = "X-JABBER";
    final public static String PROPERTY_X_MSN = "X-MSN";
    final public static String PROPERTY_X_NETMEETING = "X-NETMEETING";
    final public static String PROPERTY_X_NO = "X-NO";
    final public static String PROPERTY_X_PHONETIC_FIRST_NAME = "X-PHONETIC-FIRST-NAME";
    final public static String PROPERTY_X_PHONETIC_LAST_NAME = "X-PHONETIC-LAST-NAME";
    final public static String PROPERTY_X_PHONETIC_MIDDLE_NAME = "X-PHONETIC-MIDDLE-NAME";
    final public static String PROPERTY_X_QQ = "X-QQ";
    final public static String PROPERTY_X_REDUCTION = "X-REDUCTION";
    final public static String PROPERTY_X_SIP = "X-SIP";
    final public static String PROPERTY_X_SKYPE_PSTNNUMBER = "X-SKYPE-PSTNNUMBER";
    final public static String PROPERTY_X_SKYPE_USERNAME = "X-SKYPE-USERNAME";
    final public static String PROPERTY_X_YAHOO = "X-YAHOO";
    final public static String RECEIVED = "RECEIVED";
    final public static String VERSION_V21 = "2.1";
    final public static String VERSION_V30 = "3.0";
    final public static String VERSION_V40 = "4.0";
    
    private VCardConstants() {
    }
}
