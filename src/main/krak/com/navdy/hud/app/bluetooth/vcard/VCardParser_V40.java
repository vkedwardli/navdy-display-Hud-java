package com.navdy.hud.app.bluetooth.vcard;

public class VCardParser_V40 extends com.navdy.hud.app.bluetooth.vcard.VCardParser {
    final static java.util.Set sAcceptableEncoding;
    final static java.util.Set sKnownPropertyNameSet;
    final private com.navdy.hud.app.bluetooth.vcard.VCardParserImpl_V40 mVCardParserImpl;
    
    static {
        String[] a = new String[38];
        a[0] = "BEGIN";
        a[1] = "END";
        a[2] = "VERSION";
        a[3] = "SOURCE";
        a[4] = "KIND";
        a[5] = "FN";
        a[6] = "N";
        a[7] = "NICKNAME";
        a[8] = "PHOTO";
        a[9] = "BDAY";
        a[10] = "ANNIVERSARY";
        a[11] = "GENDER";
        a[12] = "ADR";
        a[13] = "TEL";
        a[14] = "EMAIL";
        a[15] = "IMPP";
        a[16] = "LANG";
        a[17] = "TZ";
        a[18] = "GEO";
        a[19] = "TITLE";
        a[20] = "ROLE";
        a[21] = "LOGO";
        a[22] = "ORG";
        a[23] = "MEMBER";
        a[24] = "RELATED";
        a[25] = "CATEGORIES";
        a[26] = "NOTE";
        a[27] = "PRODID";
        a[28] = "REV";
        a[29] = "SOUND";
        a[30] = "UID";
        a[31] = "CLIENTPIDMAP";
        a[32] = "URL";
        a[33] = "KEY";
        a[34] = "FBURL";
        a[35] = "CALENDRURI";
        a[36] = "CALURI";
        a[37] = "XML";
        sKnownPropertyNameSet = java.util.Collections.unmodifiableSet((java.util.Set)new java.util.HashSet((java.util.Collection)java.util.Arrays.asList((Object[])a)));
        String[] a0 = new String[2];
        a0[0] = "8BIT";
        a0[1] = "B";
        sAcceptableEncoding = java.util.Collections.unmodifiableSet((java.util.Set)new java.util.HashSet((java.util.Collection)java.util.Arrays.asList((Object[])a0)));
    }
    
    public VCardParser_V40() {
        this.mVCardParserImpl = new com.navdy.hud.app.bluetooth.vcard.VCardParserImpl_V40();
    }
    
    public VCardParser_V40(int i) {
        this.mVCardParserImpl = new com.navdy.hud.app.bluetooth.vcard.VCardParserImpl_V40(i);
    }
    
    public void addInterpreter(com.navdy.hud.app.bluetooth.vcard.VCardInterpreter a) {
        this.mVCardParserImpl.addInterpreter(a);
    }
    
    public void cancel() {
        this.mVCardParserImpl.cancel();
    }
    
    public void parse(java.io.InputStream a) {
        this.mVCardParserImpl.parse(a);
    }
    
    public void parseOne(java.io.InputStream a) {
        this.mVCardParserImpl.parseOne(a);
    }
}
