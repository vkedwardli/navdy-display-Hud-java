package com.navdy.hud.app.bluetooth.pbap;

class BluetoothPbapSession$RfcommConnectThread extends Thread {
    final private static String TAG = "RfcommConnectThread";
    private android.bluetooth.BluetoothSocket mSocket;
    final com.navdy.hud.app.bluetooth.pbap.BluetoothPbapSession this$0;
    
    public BluetoothPbapSession$RfcommConnectThread(com.navdy.hud.app.bluetooth.pbap.BluetoothPbapSession a) {
        super("RfcommConnectThread");
        this.this$0 = a;
    }
    
    private void closeSocket() {
        com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)this.mSocket);
    }
    
    public void run() {
        if (com.navdy.hud.app.bluetooth.pbap.BluetoothPbapSession.access$000(this.this$0).isDiscovering()) {
            android.util.Log.w("RfcommConnectThread", "pbap device currently discovering, might be slow to connect");
        }
        try {
            this.mSocket = com.navdy.hud.app.bluetooth.pbap.BluetoothPbapSession.access$100(this.this$0).createRfcommSocketToServiceRecord(java.util.UUID.fromString("0000112f-0000-1000-8000-00805f9b34fb"));
            this.mSocket.connect();
            com.navdy.hud.app.bluetooth.pbap.BluetoothPbapObexTransport a = new com.navdy.hud.app.bluetooth.pbap.BluetoothPbapObexTransport(this.mSocket);
            com.navdy.hud.app.bluetooth.pbap.BluetoothPbapSession.access$200(this.this$0).obtainMessage(1, a).sendToTarget();
        } catch(Throwable ignoredException) {
            this.closeSocket();
            com.navdy.hud.app.bluetooth.pbap.BluetoothPbapSession.access$200(this.this$0).obtainMessage(2).sendToTarget();
        }
    }
}
