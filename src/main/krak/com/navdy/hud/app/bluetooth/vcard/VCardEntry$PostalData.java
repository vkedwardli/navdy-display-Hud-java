package com.navdy.hud.app.bluetooth.vcard;

public class VCardEntry$PostalData implements com.navdy.hud.app.bluetooth.vcard.VCardEntry$EntryElement {
    final private static int ADDR_MAX_DATA_SIZE = 7;
    final private String mCountry;
    final private String mExtendedAddress;
    private boolean mIsPrimary;
    final private String mLabel;
    final private String mLocalty;
    final private String mPobox;
    final private String mPostalCode;
    final private String mRegion;
    final private String mStreet;
    final private int mType;
    private int mVCardType;
    
    public VCardEntry$PostalData(String s, String s0, String s1, String s2, String s3, String s4, String s5, int i, String s6, boolean b, int i0) {
        this.mType = i;
        this.mPobox = s;
        this.mExtendedAddress = s0;
        this.mStreet = s1;
        this.mLocalty = s2;
        this.mRegion = s3;
        this.mPostalCode = s4;
        this.mCountry = s5;
        this.mLabel = s6;
        this.mIsPrimary = b;
        this.mVCardType = i0;
    }
    
    public static com.navdy.hud.app.bluetooth.vcard.VCardEntry$PostalData constructPostalData(java.util.List a, int i, String s, boolean b, int i0) {
        String[] a0 = new String[7];
        int i1 = a.size();
        if (i1 > 7) {
            i1 = 7;
        }
        Object a1 = a.iterator();
        int i2 = 0;
        label0: while(true) {
            if (((java.util.Iterator)a1).hasNext()) {
                a0[i2] = (String)((java.util.Iterator)a1).next();
                i2 = i2 + 1;
                if (i2 < i1) {
                    continue label0;
                }
            }
            while(i2 < 7) {
                a0[i2] = null;
                i2 = i2 + 1;
            }
            return new com.navdy.hud.app.bluetooth.vcard.VCardEntry$PostalData(a0[0], a0[1], a0[2], a0[3], a0[4], a0[5], a0[6], i, s, b, i0);
        }
    }
    
    public void constructInsertOperation(java.util.List a, int i) {
        android.content.ContentProviderOperation$Builder a0 = android.content.ContentProviderOperation.newInsert(android.provider.ContactsContract$Data.CONTENT_URI);
        a0.withValueBackReference("raw_contact_id", i);
        a0.withValue("mimetype", "vnd.android.cursor.item/postal-address_v2");
        a0.withValue("data2", Integer.valueOf(this.mType));
        if (this.mType == 0) {
            a0.withValue("data3", this.mLabel);
        }
        String s = (android.text.TextUtils.isEmpty((CharSequence)this.mStreet)) ? (android.text.TextUtils.isEmpty((CharSequence)this.mExtendedAddress)) ? null : this.mExtendedAddress : (android.text.TextUtils.isEmpty((CharSequence)this.mExtendedAddress)) ? this.mStreet : new StringBuilder().append(this.mStreet).append(" ").append(this.mExtendedAddress).toString();
        a0.withValue("data5", this.mPobox);
        a0.withValue("data4", s);
        a0.withValue("data7", this.mLocalty);
        a0.withValue("data8", this.mRegion);
        a0.withValue("data9", this.mPostalCode);
        a0.withValue("data10", this.mCountry);
        a0.withValue("data1", this.getFormattedAddress(this.mVCardType));
        if (this.mIsPrimary) {
            a0.withValue("is_primary", Integer.valueOf(1));
        }
        a.add(a0.build());
    }
    
    public boolean equals(Object a) {
        boolean b = false;
        label0: if (this != a) {
            if (a instanceof com.navdy.hud.app.bluetooth.vcard.VCardEntry$PostalData) {
                com.navdy.hud.app.bluetooth.vcard.VCardEntry$PostalData a0 = (com.navdy.hud.app.bluetooth.vcard.VCardEntry$PostalData)a;
                int i = this.mType;
                int i0 = a0.mType;
                label1: {
                    if (i != i0) {
                        break label1;
                    }
                    int i1 = this.mType;
                    label2: {
                        if (i1 != 0) {
                            break label2;
                        }
                        if (!android.text.TextUtils.equals((CharSequence)this.mLabel, (CharSequence)a0.mLabel)) {
                            break label1;
                        }
                    }
                    if (this.mIsPrimary != a0.mIsPrimary) {
                        break label1;
                    }
                    if (!android.text.TextUtils.equals((CharSequence)this.mPobox, (CharSequence)a0.mPobox)) {
                        break label1;
                    }
                    if (!android.text.TextUtils.equals((CharSequence)this.mExtendedAddress, (CharSequence)a0.mExtendedAddress)) {
                        break label1;
                    }
                    if (!android.text.TextUtils.equals((CharSequence)this.mStreet, (CharSequence)a0.mStreet)) {
                        break label1;
                    }
                    if (!android.text.TextUtils.equals((CharSequence)this.mLocalty, (CharSequence)a0.mLocalty)) {
                        break label1;
                    }
                    if (!android.text.TextUtils.equals((CharSequence)this.mRegion, (CharSequence)a0.mRegion)) {
                        break label1;
                    }
                    if (!android.text.TextUtils.equals((CharSequence)this.mPostalCode, (CharSequence)a0.mPostalCode)) {
                        break label1;
                    }
                    if (android.text.TextUtils.equals((CharSequence)this.mCountry, (CharSequence)a0.mCountry)) {
                        b = true;
                        break label0;
                    }
                }
                b = false;
            } else {
                b = false;
            }
        } else {
            b = true;
        }
        return b;
    }
    
    public String getCountry() {
        return this.mCountry;
    }
    
    final public com.navdy.hud.app.bluetooth.vcard.VCardEntry$EntryLabel getEntryLabel() {
        return com.navdy.hud.app.bluetooth.vcard.VCardEntry$EntryLabel.POSTAL_ADDRESS;
    }
    
    public String getExtendedAddress() {
        return this.mExtendedAddress;
    }
    
    public String getFormattedAddress(int i) {
        StringBuilder a = new StringBuilder();
        String[] a0 = new String[7];
        a0[0] = this.mPobox;
        a0[1] = this.mExtendedAddress;
        a0[2] = this.mStreet;
        a0[3] = this.mLocalty;
        a0[4] = this.mRegion;
        a0[5] = this.mPostalCode;
        a0[6] = this.mCountry;
        if (com.navdy.hud.app.bluetooth.vcard.VCardConfig.isJapaneseDevice(i)) {
            boolean b = true;
            int i0 = 6;
            while(i0 >= 0) {
                String s = a0[i0];
                if (!android.text.TextUtils.isEmpty((CharSequence)s)) {
                    if (b) {
                        b = false;
                    } else {
                        a.append((char)32);
                    }
                    a.append(s);
                }
                i0 = i0 + -1;
            }
        } else {
            boolean b0 = true;
            int i1 = 0;
            while(i1 < 7) {
                String s0 = a0[i1];
                if (!android.text.TextUtils.isEmpty((CharSequence)s0)) {
                    if (b0) {
                        b0 = false;
                    } else {
                        a.append((char)32);
                    }
                    a.append(s0);
                }
                i1 = i1 + 1;
            }
        }
        return a.toString().trim();
    }
    
    public String getLabel() {
        return this.mLabel;
    }
    
    public String getLocalty() {
        return this.mLocalty;
    }
    
    public String getPobox() {
        return this.mPobox;
    }
    
    public String getPostalCode() {
        return this.mPostalCode;
    }
    
    public String getRegion() {
        return this.mRegion;
    }
    
    public String getStreet() {
        return this.mStreet;
    }
    
    public int getType() {
        return this.mType;
    }
    
    public int hashCode() {
        int i = (this.mType * 31 + ((this.mLabel == null) ? 0 : this.mLabel.hashCode())) * 31 + ((this.mIsPrimary) ? 1231 : 1237);
        String[] a = new String[7];
        a[0] = this.mPobox;
        a[1] = this.mExtendedAddress;
        a[2] = this.mStreet;
        a[3] = this.mLocalty;
        a[4] = this.mRegion;
        a[5] = this.mPostalCode;
        a[6] = this.mCountry;
        int i0 = a.length;
        int i1 = 0;
        while(i1 < i0) {
            String s = a[i1];
            i = i * 31 + ((s == null) ? 0 : s.hashCode());
            i1 = i1 + 1;
        }
        return i;
    }
    
    public boolean isEmpty() {
        boolean b = false;
        boolean b0 = android.text.TextUtils.isEmpty((CharSequence)this.mPobox);
        label2: {
            label0: {
                label1: {
                    if (!b0) {
                        break label1;
                    }
                    if (!android.text.TextUtils.isEmpty((CharSequence)this.mExtendedAddress)) {
                        break label1;
                    }
                    if (!android.text.TextUtils.isEmpty((CharSequence)this.mStreet)) {
                        break label1;
                    }
                    if (!android.text.TextUtils.isEmpty((CharSequence)this.mLocalty)) {
                        break label1;
                    }
                    if (!android.text.TextUtils.isEmpty((CharSequence)this.mRegion)) {
                        break label1;
                    }
                    if (!android.text.TextUtils.isEmpty((CharSequence)this.mPostalCode)) {
                        break label1;
                    }
                    if (android.text.TextUtils.isEmpty((CharSequence)this.mCountry)) {
                        break label0;
                    }
                }
                b = false;
                break label2;
            }
            b = true;
        }
        return b;
    }
    
    public boolean isPrimary() {
        return this.mIsPrimary;
    }
    
    public String toString() {
        Object[] a = new Object[10];
        a[0] = Integer.valueOf(this.mType);
        a[1] = this.mLabel;
        a[2] = Boolean.valueOf(this.mIsPrimary);
        a[3] = this.mPobox;
        a[4] = this.mExtendedAddress;
        a[5] = this.mStreet;
        a[6] = this.mLocalty;
        a[7] = this.mRegion;
        a[8] = this.mPostalCode;
        a[9] = this.mCountry;
        return String.format("type: %d, label: %s, isPrimary: %s, pobox: %s, extendedAddress: %s, street: %s, localty: %s, region: %s, postalCode %s, country: %s", a);
    }
}
