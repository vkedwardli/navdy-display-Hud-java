package com.navdy.hud.app.bluetooth.obex;

public class ApplicationParameter$TRIPLET_VALUE$SEARCHATTRIBUTE {
    final public static byte SEARCH_BY_NAME = (byte)0;
    final public static byte SEARCH_BY_NUMBER = (byte)1;
    final public static byte SEARCH_BY_SOUND = (byte)2;
    
    public ApplicationParameter$TRIPLET_VALUE$SEARCHATTRIBUTE() {
    }
}
