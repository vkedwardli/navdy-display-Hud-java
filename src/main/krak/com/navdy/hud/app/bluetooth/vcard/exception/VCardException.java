package com.navdy.hud.app.bluetooth.vcard.exception;

public class VCardException extends Exception {
    public VCardException() {
    }
    
    public VCardException(String s) {
        super(s);
    }
}
