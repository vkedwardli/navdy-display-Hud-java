package com.navdy.hud.app.bluetooth.obex;

abstract public interface ObexTransport {
    abstract public void close();
    
    
    abstract public void connect();
    
    
    abstract public void create();
    
    
    abstract public void disconnect();
    
    
    abstract public int getMaxReceivePacketSize();
    
    
    abstract public int getMaxTransmitPacketSize();
    
    
    abstract public boolean isSrmSupported();
    
    
    abstract public void listen();
    
    
    abstract public java.io.DataInputStream openDataInputStream();
    
    
    abstract public java.io.DataOutputStream openDataOutputStream();
    
    
    abstract public java.io.InputStream openInputStream();
    
    
    abstract public java.io.OutputStream openOutputStream();
}
