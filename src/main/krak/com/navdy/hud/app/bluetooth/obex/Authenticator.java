package com.navdy.hud.app.bluetooth.obex;

abstract public interface Authenticator {
    abstract public com.navdy.hud.app.bluetooth.obex.PasswordAuthentication onAuthenticationChallenge(String arg, boolean arg0, boolean arg1);
    
    
    abstract public byte[] onAuthenticationResponse(byte[] arg);
}
