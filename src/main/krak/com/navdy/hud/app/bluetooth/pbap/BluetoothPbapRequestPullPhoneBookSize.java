package com.navdy.hud.app.bluetooth.pbap;

class BluetoothPbapRequestPullPhoneBookSize extends com.navdy.hud.app.bluetooth.pbap.BluetoothPbapRequest {
    final private static String TAG = "BTPbapReqPullPBookSize";
    final private static String TYPE = "x-bt/phonebook";
    private int mSize;
    
    public BluetoothPbapRequestPullPhoneBookSize(String s) {
        this.mHeaderSet.setHeader(1, s);
        this.mHeaderSet.setHeader(66, "x-bt/phonebook");
        com.navdy.hud.app.bluetooth.pbap.utils.ObexAppParameters a = new com.navdy.hud.app.bluetooth.pbap.utils.ObexAppParameters();
        a.add((byte)4, (short)0);
        a.addToHeaderSet(this.mHeaderSet);
    }
    
    public int getSize() {
        return this.mSize;
    }
    
    protected void readResponseHeaders(com.navdy.hud.app.bluetooth.obex.HeaderSet a) {
        android.util.Log.v("BTPbapReqPullPBookSize", "readResponseHeaders");
        int i = com.navdy.hud.app.bluetooth.pbap.utils.ObexAppParameters.fromHeaderSet(a).getShort((byte)8);
        this.mSize = i;
    }
}
