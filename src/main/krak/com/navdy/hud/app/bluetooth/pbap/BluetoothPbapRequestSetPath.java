package com.navdy.hud.app.bluetooth.pbap;

final class BluetoothPbapRequestSetPath extends com.navdy.hud.app.bluetooth.pbap.BluetoothPbapRequest {
    final private static String TAG = "BTPbapReqSetPath";
    private com.navdy.hud.app.bluetooth.pbap.BluetoothPbapRequestSetPath$SetPathDir mDir;
    
    public BluetoothPbapRequestSetPath(String s) {
        this.mDir = com.navdy.hud.app.bluetooth.pbap.BluetoothPbapRequestSetPath$SetPathDir.DOWN;
        this.mHeaderSet.setHeader(1, s);
    }
    
    public BluetoothPbapRequestSetPath(boolean b) {
        this.mHeaderSet.setEmptyNameHeader();
        if (b) {
            this.mDir = com.navdy.hud.app.bluetooth.pbap.BluetoothPbapRequestSetPath$SetPathDir.UP;
        } else {
            this.mDir = com.navdy.hud.app.bluetooth.pbap.BluetoothPbapRequestSetPath$SetPathDir.ROOT;
        }
    }
    
    public void execute(com.navdy.hud.app.bluetooth.obex.ClientSession a) {
        android.util.Log.v("BTPbapReqSetPath", "execute");
        int[] a0 = com.navdy.hud.app.bluetooth.pbap.BluetoothPbapRequestSetPath$1.$SwitchMap$com$navdy$hud$app$bluetooth$pbap$BluetoothPbapRequestSetPath$SetPathDir;
        try {
            com.navdy.hud.app.bluetooth.obex.HeaderSet a1 = null;
            switch(a0[this.mDir.ordinal()]) {
                case 3: {
                    a1 = a.setPath(this.mHeaderSet, true, false);
                    break;
                }
                case 1: case 2: {
                    a1 = a.setPath(this.mHeaderSet, false, false);
                    break;
                }
                default: {
                    a1 = null;
                }
            }
            this.mResponseCode = a1.getResponseCode();
        } catch(java.io.IOException ignoredException) {
            this.mResponseCode = 208;
        }
    }
}
