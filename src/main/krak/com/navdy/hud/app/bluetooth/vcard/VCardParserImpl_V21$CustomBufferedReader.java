package com.navdy.hud.app.bluetooth.vcard;

final public class VCardParserImpl_V21$CustomBufferedReader extends java.io.BufferedReader {
    private String mNextLine;
    private boolean mNextLineIsValid;
    private long mTime;
    
    public VCardParserImpl_V21$CustomBufferedReader(java.io.Reader a) {
        super(a);
    }
    
    public long getTotalmillisecond() {
        return this.mTime;
    }
    
    public String peekLine() {
        if (!this.mNextLineIsValid) {
            long j = System.currentTimeMillis();
            String s = super.readLine();
            long j0 = System.currentTimeMillis();
            this.mTime = this.mTime + (j0 - j);
            this.mNextLine = s;
            this.mNextLineIsValid = true;
        }
        return this.mNextLine;
    }
    
    public String readLine() {
        String s = null;
        if (this.mNextLineIsValid) {
            s = this.mNextLine;
            this.mNextLine = null;
            this.mNextLineIsValid = false;
        } else {
            long j = System.currentTimeMillis();
            s = super.readLine();
            long j0 = System.currentTimeMillis();
            this.mTime = this.mTime + (j0 - j);
        }
        return s;
    }
}
