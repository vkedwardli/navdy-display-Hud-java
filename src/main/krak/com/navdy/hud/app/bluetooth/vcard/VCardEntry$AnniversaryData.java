package com.navdy.hud.app.bluetooth.vcard;

public class VCardEntry$AnniversaryData implements com.navdy.hud.app.bluetooth.vcard.VCardEntry$EntryElement {
    final private String mAnniversary;
    
    public VCardEntry$AnniversaryData(String s) {
        this.mAnniversary = s;
    }
    
    public void constructInsertOperation(java.util.List a, int i) {
        android.content.ContentProviderOperation$Builder a0 = android.content.ContentProviderOperation.newInsert(android.provider.ContactsContract$Data.CONTENT_URI);
        a0.withValueBackReference("raw_contact_id", i);
        a0.withValue("mimetype", "vnd.android.cursor.item/contact_event");
        a0.withValue("data1", this.mAnniversary);
        a0.withValue("data2", Integer.valueOf(1));
        a.add(a0.build());
    }
    
    public boolean equals(Object a) {
        boolean b = false;
        if (this != a) {
            if (a instanceof com.navdy.hud.app.bluetooth.vcard.VCardEntry$AnniversaryData) {
                com.navdy.hud.app.bluetooth.vcard.VCardEntry$AnniversaryData a0 = (com.navdy.hud.app.bluetooth.vcard.VCardEntry$AnniversaryData)a;
                b = android.text.TextUtils.equals((CharSequence)this.mAnniversary, (CharSequence)a0.mAnniversary);
            } else {
                b = false;
            }
        } else {
            b = true;
        }
        return b;
    }
    
    public String getAnniversary() {
        return this.mAnniversary;
    }
    
    public com.navdy.hud.app.bluetooth.vcard.VCardEntry$EntryLabel getEntryLabel() {
        return com.navdy.hud.app.bluetooth.vcard.VCardEntry$EntryLabel.ANNIVERSARY;
    }
    
    public int hashCode() {
        return (this.mAnniversary == null) ? 0 : this.mAnniversary.hashCode();
    }
    
    public boolean isEmpty() {
        return android.text.TextUtils.isEmpty((CharSequence)this.mAnniversary);
    }
    
    public String toString() {
        return new StringBuilder().append("anniversary: ").append(this.mAnniversary).toString();
    }
}
