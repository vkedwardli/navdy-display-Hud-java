package com.navdy.hud.app.bluetooth.vcard;

abstract public interface VCardEntryHandler {
    abstract public void onEnd();
    
    
    abstract public void onEntryCreated(com.navdy.hud.app.bluetooth.vcard.VCardEntry arg);
    
    
    abstract public void onStart();
}
