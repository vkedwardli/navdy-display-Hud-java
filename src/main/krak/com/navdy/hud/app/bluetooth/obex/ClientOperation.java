package com.navdy.hud.app.bluetooth.obex;

final public class ClientOperation implements com.navdy.hud.app.bluetooth.obex.Operation, com.navdy.hud.app.bluetooth.obex.BaseStream {
    final private static String TAG = "ClientOperation";
    final private static boolean V = false;
    private boolean mEndOfBodySent;
    private String mExceptionMessage;
    private boolean mGetFinalFlag;
    private boolean mGetOperation;
    private boolean mInputOpen;
    private int mMaxPacketSize;
    private boolean mOperationDone;
    private com.navdy.hud.app.bluetooth.obex.ClientSession mParent;
    private com.navdy.hud.app.bluetooth.obex.PrivateInputStream mPrivateInput;
    private boolean mPrivateInputOpen;
    private com.navdy.hud.app.bluetooth.obex.PrivateOutputStream mPrivateOutput;
    private boolean mPrivateOutputOpen;
    private com.navdy.hud.app.bluetooth.obex.HeaderSet mReplyHeader;
    private com.navdy.hud.app.bluetooth.obex.HeaderSet mRequestHeader;
    private boolean mSendBodyHeader;
    private boolean mSrmActive;
    private boolean mSrmEnabled;
    private boolean mSrmWaitingForRemote;
    
    public ClientOperation(int i, com.navdy.hud.app.bluetooth.obex.ClientSession a, com.navdy.hud.app.bluetooth.obex.HeaderSet a0, boolean b) {
        this.mSendBodyHeader = true;
        this.mSrmActive = false;
        this.mSrmEnabled = false;
        this.mSrmWaitingForRemote = true;
        this.mParent = a;
        this.mEndOfBodySent = false;
        this.mInputOpen = true;
        this.mOperationDone = false;
        this.mMaxPacketSize = i;
        this.mGetOperation = b;
        this.mGetFinalFlag = false;
        this.mPrivateInputOpen = false;
        this.mPrivateOutputOpen = false;
        this.mPrivateInput = null;
        this.mPrivateOutput = null;
        this.mReplyHeader = new com.navdy.hud.app.bluetooth.obex.HeaderSet();
        this.mRequestHeader = new com.navdy.hud.app.bluetooth.obex.HeaderSet();
        int[] a1 = a0.getHeaderList();
        if (a1 != null) {
            int i0 = 0;
            while(i0 < a1.length) {
                this.mRequestHeader.setHeader((a1[i0] != 0) ? 1 : 0, a0.getHeader((a1[i0] != 0) ? 1 : 0));
                i0 = i0 + 1;
            }
        }
        if (a0.mAuthChall != null) {
            this.mRequestHeader.mAuthChall = new byte[a0.mAuthChall.length];
            System.arraycopy(a0.mAuthChall, 0, this.mRequestHeader.mAuthChall, 0, a0.mAuthChall.length);
        }
        if (a0.mAuthResp != null) {
            this.mRequestHeader.mAuthResp = new byte[a0.mAuthResp.length];
            System.arraycopy(a0.mAuthResp, 0, this.mRequestHeader.mAuthResp, 0, a0.mAuthResp.length);
        }
        if (a0.mConnectionID != null) {
            this.mRequestHeader.mConnectionID = new byte[4];
            System.arraycopy(a0.mConnectionID, 0, this.mRequestHeader.mConnectionID, 0, 4);
        }
    }
    
    private void checkForSrm() {
        Byte a = (Byte)this.mReplyHeader.getHeader(151);
        if (this.mParent.isSrmSupported() && a != null) {
            int i = a.byteValue();
            if (i == 1) {
                this.mSrmEnabled = true;
            }
        }
        if (this.mSrmEnabled) {
            this.mSrmWaitingForRemote = false;
            Byte a0 = (Byte)this.mReplyHeader.getHeader(152);
            if (a0 != null) {
                int i0 = a0.byteValue();
                if (i0 == 1) {
                    this.mSrmWaitingForRemote = true;
                    this.mReplyHeader.setHeader(152, null);
                }
            }
        }
        if (!this.mSrmWaitingForRemote && this.mSrmEnabled) {
            this.mSrmActive = true;
        }
    }
    
    private boolean sendRequest(int i) {
        boolean b = false;
        java.io.ByteArrayOutputStream a = new java.io.ByteArrayOutputStream();
        byte[] a0 = com.navdy.hud.app.bluetooth.obex.ObexHelper.createHeader(this.mRequestHeader, true);
        int i0 = (this.mPrivateOutput == null) ? -1 : this.mPrivateOutput.size();
        label1: if (a0.length + 3 + 3 <= this.mMaxPacketSize) {
            if (!this.mSendBodyHeader) {
                i = i | 128;
            }
            a.write(a0);
            if (i0 <= 0) {
                b = false;
            } else {
                if (i0 <= this.mMaxPacketSize - a0.length - 6) {
                    b = false;
                } else {
                    i0 = this.mMaxPacketSize - a0.length - 6;
                    b = true;
                }
                byte[] a1 = this.mPrivateOutput.readBytes(i0);
                boolean b0 = this.mPrivateOutput.isClosed();
                label3: {
                    label2: {
                        if (!b0) {
                            break label2;
                        }
                        if (b) {
                            break label2;
                        }
                        if (this.mEndOfBodySent) {
                            break label2;
                        }
                        if ((i & 128) == 0) {
                            break label2;
                        }
                        a.write(73);
                        this.mEndOfBodySent = true;
                        break label3;
                    }
                    a.write(72);
                }
                i0 = i0 + 3;
                int i1 = (byte)(i0 >> 8);
                a.write(i1);
                int i2 = (byte)i0;
                a.write(i2);
                if (a1 != null) {
                    a.write(a1);
                }
            }
            if (this.mPrivateOutputOpen && i0 <= 0 && !this.mEndOfBodySent) {
                if ((i & 128) != 0) {
                    a.write(73);
                    this.mEndOfBodySent = true;
                } else {
                    a.write(72);
                }
                a.write(0);
                a.write(3);
            }
            if (a.size() != 0) {
                int i3 = a.size();
                label0: {
                    if (i3 <= 0) {
                        break label0;
                    }
                    if (this.mParent.sendRequest(i, a.toByteArray(), this.mReplyHeader, this.mPrivateInput, this.mSrmActive)) {
                        break label0;
                    }
                    b = false;
                    break label1;
                }
                this.checkForSrm();
                if (this.mPrivateOutput != null && this.mPrivateOutput.size() > 0) {
                    b = true;
                }
            } else if (this.mParent.sendRequest(i, (byte[])null, this.mReplyHeader, this.mPrivateInput, this.mSrmActive)) {
                this.checkForSrm();
            } else {
                b = false;
            }
        } else {
            int i4 = 0;
            int i5 = 0;
            while(true) {
                if (i4 == a0.length) {
                    this.checkForSrm();
                    b = i0 > 0;
                    break;
                } else {
                    i4 = com.navdy.hud.app.bluetooth.obex.ObexHelper.findHeaderEnd(a0, i5, this.mMaxPacketSize - 3);
                    if (i4 == -1) {
                        this.mOperationDone = true;
                        this.abort();
                        this.mExceptionMessage = "Header larger then can be sent in a packet";
                        this.mInputOpen = false;
                        if (this.mPrivateInput != null) {
                            this.mPrivateInput.close();
                        }
                        if (this.mPrivateOutput != null) {
                            this.mPrivateOutput.close();
                        }
                        throw new java.io.IOException("OBEX Packet exceeds max packet size");
                    }
                    byte[] a2 = new byte[i4 - i5];
                    System.arraycopy(a0, i5, a2, 0, a2.length);
                    if (this.mParent.sendRequest(i, a2, this.mReplyHeader, this.mPrivateInput, false)) {
                        if (this.mReplyHeader.responseCode == 144) {
                            i5 = i4;
                            continue;
                        }
                        b = false;
                        break;
                    } else {
                        b = false;
                        break;
                    }
                }
            }
        }
        return b;
    }
    
    private void startProcessing() {
        synchronized(this) {
            if (this.mPrivateInput == null) {
                this.mPrivateInput = new com.navdy.hud.app.bluetooth.obex.PrivateInputStream((com.navdy.hud.app.bluetooth.obex.BaseStream)this);
            }
            if (this.mGetOperation) {
                if (!this.mOperationDone) {
                    this.mReplyHeader.responseCode = 144;
                    boolean b = true;
                    while(b && this.mReplyHeader.responseCode == 144) {
                        b = this.sendRequest(3);
                    }
                    if (this.mReplyHeader.responseCode == 144) {
                        this.mParent.sendRequest(131, (byte[])null, this.mReplyHeader, this.mPrivateInput, this.mSrmActive);
                    }
                    if (this.mReplyHeader.responseCode == 144) {
                        this.checkForSrm();
                    } else {
                        this.mOperationDone = true;
                    }
                }
            } else {
                if (!this.mOperationDone) {
                    this.mReplyHeader.responseCode = 144;
                    boolean b0 = true;
                    while(b0 && this.mReplyHeader.responseCode == 144) {
                        b0 = this.sendRequest(2);
                    }
                }
                if (this.mReplyHeader.responseCode == 144) {
                    this.mParent.sendRequest(130, (byte[])null, this.mReplyHeader, this.mPrivateInput, this.mSrmActive);
                }
                if (this.mReplyHeader.responseCode != 144) {
                    this.mOperationDone = true;
                }
            }
        }
        /*monexit(this)*/;
    }
    
    private void validateConnection() {
        this.ensureOpen();
        if (this.mPrivateInput == null) {
            this.startProcessing();
        }
    }
    
    public void abort() {
        synchronized(this) {
            this.ensureOpen();
            if (this.mOperationDone && this.mReplyHeader.responseCode != 144) {
                throw new java.io.IOException("Operation has already ended");
            }
            this.mExceptionMessage = "Operation aborted";
            if (!this.mOperationDone && this.mReplyHeader.responseCode == 144) {
                this.mOperationDone = true;
                this.mParent.sendRequest(255, (byte[])null, this.mReplyHeader, (com.navdy.hud.app.bluetooth.obex.PrivateInputStream)null, false);
                if (this.mReplyHeader.responseCode != 160) {
                    throw new java.io.IOException("Invalid response code from server");
                }
                this.mExceptionMessage = null;
            }
            this.close();
        }
        /*monexit(this)*/;
    }
    
    public void close() {
        this.mInputOpen = false;
        this.mPrivateInputOpen = false;
        this.mPrivateOutputOpen = false;
        this.mParent.setRequestInactive();
    }
    
    public boolean continueOperation(boolean b, boolean b0) {
        boolean b1 = false;
        label0: synchronized(this) {
            if (this.mGetOperation) {
                label6: {
                    if (!b0) {
                        break label6;
                    }
                    if (this.mOperationDone) {
                        break label6;
                    }
                    this.mParent.sendRequest(131, (byte[])null, this.mReplyHeader, this.mPrivateInput, this.mSrmActive);
                    if (this.mReplyHeader.responseCode == 144) {
                        this.checkForSrm();
                    } else {
                        this.mOperationDone = true;
                    }
                    b1 = true;
                    break label0;
                }
                label4: {
                    label5: {
                        if (b0) {
                            break label5;
                        }
                        if (!this.mOperationDone) {
                            break label4;
                        }
                    }
                    boolean b2 = this.mOperationDone;
                    b1 = false;
                    break label0;
                }
                if (this.mPrivateInput == null) {
                    this.mPrivateInput = new com.navdy.hud.app.bluetooth.obex.PrivateInputStream((com.navdy.hud.app.bluetooth.obex.BaseStream)this);
                }
                this.sendRequest(3);
                b1 = true;
            } else {
                label2: {
                    label3: {
                        if (b0) {
                            break label3;
                        }
                        if (!this.mOperationDone) {
                            break label2;
                        }
                    }
                    label1: {
                        if (!b0) {
                            break label1;
                        }
                        if (!this.mOperationDone) {
                            b1 = false;
                            break label0;
                        }
                    }
                    boolean b3 = this.mOperationDone;
                    b1 = false;
                    break label0;
                }
                if (this.mReplyHeader.responseCode == -1) {
                    this.mReplyHeader.responseCode = 144;
                }
                this.sendRequest(2);
                b1 = true;
            }
        }
        /*monexit(this)*/;
        return b1;
    }
    
    public void ensureNotDone() {
        if (this.mOperationDone) {
            throw new java.io.IOException("Operation has completed");
        }
    }
    
    public void ensureOpen() {
        this.mParent.ensureOpen();
        if (this.mExceptionMessage != null) {
            throw new java.io.IOException(this.mExceptionMessage);
        }
        if (!this.mInputOpen) {
            throw new java.io.IOException("Operation has already ended");
        }
    }
    
    public String getEncoding() {
        return null;
    }
    
    public int getHeaderLength() {
        return com.navdy.hud.app.bluetooth.obex.ObexHelper.createHeader(this.mRequestHeader, false).length;
    }
    
    public long getLength() {
        long j = 0L;
        try {
            Long a = (Long)this.mReplyHeader.getHeader(195);
            j = (a != null) ? a.longValue() : -1L;
        } catch(java.io.IOException ignoredException) {
            j = -1L;
        }
        return j;
    }
    
    public int getMaxPacketSize() {
        return this.mMaxPacketSize - 6 - this.getHeaderLength();
    }
    
    public com.navdy.hud.app.bluetooth.obex.HeaderSet getReceivedHeader() {
        this.ensureOpen();
        return this.mReplyHeader;
    }
    
    public int getResponseCode() {
        int i = 0;
        synchronized(this) {
            int i0 = this.mReplyHeader.responseCode;
            label0: {
                label1: {
                    if (i0 == -1) {
                        break label1;
                    }
                    if (this.mReplyHeader.responseCode != 144) {
                        break label0;
                    }
                }
                this.validateConnection();
            }
            i = this.mReplyHeader.responseCode;
        }
        /*monexit(this)*/;
        return i;
    }
    
    public String getType() {
        String s = null;
        try {
            s = (String)this.mReplyHeader.getHeader(66);
        } catch(java.io.IOException ignoredException) {
            s = null;
        }
        return s;
    }
    
    public void noBodyHeader() {
        this.mSendBodyHeader = false;
    }
    
    public java.io.DataInputStream openDataInputStream() {
        return new java.io.DataInputStream(this.openInputStream());
    }
    
    public java.io.DataOutputStream openDataOutputStream() {
        return new java.io.DataOutputStream(this.openOutputStream());
    }
    
    public java.io.InputStream openInputStream() {
        this.ensureOpen();
        if (this.mPrivateInputOpen) {
            throw new java.io.IOException("no more input streams available");
        }
        if (this.mGetOperation) {
            this.validateConnection();
        } else if (this.mPrivateInput == null) {
            this.mPrivateInput = new com.navdy.hud.app.bluetooth.obex.PrivateInputStream((com.navdy.hud.app.bluetooth.obex.BaseStream)this);
        }
        this.mPrivateInputOpen = true;
        return this.mPrivateInput;
    }
    
    public java.io.OutputStream openOutputStream() {
        this.ensureOpen();
        this.ensureNotDone();
        if (this.mPrivateOutputOpen) {
            throw new java.io.IOException("no more output streams available");
        }
        if (this.mPrivateOutput == null) {
            this.mPrivateOutput = new com.navdy.hud.app.bluetooth.obex.PrivateOutputStream((com.navdy.hud.app.bluetooth.obex.BaseStream)this, this.getMaxPacketSize());
        }
        this.mPrivateOutputOpen = true;
        return this.mPrivateOutput;
    }
    
    public void sendHeaders(com.navdy.hud.app.bluetooth.obex.HeaderSet a) {
        this.ensureOpen();
        if (this.mOperationDone) {
            throw new java.io.IOException("Operation has already exchanged all data");
        }
        if (a == null) {
            throw new java.io.IOException("Headers may not be null");
        }
        int[] a0 = a.getHeaderList();
        if (a0 != null) {
            int i = 0;
            while(i < a0.length) {
                this.mRequestHeader.setHeader((a0[i] != 0) ? 1 : 0, a.getHeader((a0[i] != 0) ? 1 : 0));
                i = i + 1;
            }
        }
    }
    
    public void setGetFinalFlag(boolean b) {
        this.mGetFinalFlag = b;
    }
    
    public void streamClosed(boolean b) {
        label0: if (this.mGetOperation) {
            label3: {
                label4: {
                    if (!b) {
                        break label4;
                    }
                    if (!this.mOperationDone) {
                        break label3;
                    }
                }
                if (b) {
                    break label0;
                }
                if (this.mOperationDone) {
                    break label0;
                }
                boolean b0 = this.mPrivateOutput == null || this.mPrivateOutput.size() > 0 || com.navdy.hud.app.bluetooth.obex.ObexHelper.createHeader(this.mRequestHeader, false).length > 0;
                if (this.mPrivateInput == null) {
                    this.mPrivateInput = new com.navdy.hud.app.bluetooth.obex.PrivateInputStream((com.navdy.hud.app.bluetooth.obex.BaseStream)this);
                }
                if (this.mPrivateOutput != null && this.mPrivateOutput.size() <= 0) {
                    b0 = false;
                }
                this.mReplyHeader.responseCode = 144;
                while(b0 && this.mReplyHeader.responseCode == 144) {
                    b0 = this.sendRequest(3);
                }
                this.sendRequest(131);
                if (this.mReplyHeader.responseCode == 144) {
                    break label0;
                }
                this.mOperationDone = true;
                break label0;
            }
            if (this.mReplyHeader.responseCode == -1) {
                this.mReplyHeader.responseCode = 144;
            }
            while(this.mReplyHeader.responseCode == 144 && !this.mOperationDone) {
                if (!this.sendRequest(131)) {
                    break;
                }
            }
            while(this.mReplyHeader.responseCode == 144 && !this.mOperationDone) {
                this.mParent.sendRequest(131, (byte[])null, this.mReplyHeader, this.mPrivateInput, false);
            }
            this.mOperationDone = true;
        } else {
            label1: {
                label2: {
                    if (b) {
                        break label2;
                    }
                    if (!this.mOperationDone) {
                        break label1;
                    }
                }
                if (!b) {
                    break label0;
                }
                if (!this.mOperationDone) {
                    break label0;
                }
                this.mOperationDone = true;
                break label0;
            }
            boolean b1 = this.mPrivateOutput == null || this.mPrivateOutput.size() > 0 || com.navdy.hud.app.bluetooth.obex.ObexHelper.createHeader(this.mRequestHeader, false).length > 0;
            if (this.mReplyHeader.responseCode == -1) {
                this.mReplyHeader.responseCode = 144;
            }
            while(b1 && this.mReplyHeader.responseCode == 144) {
                b1 = this.sendRequest(2);
            }
            while(this.mReplyHeader.responseCode == 144) {
                this.sendRequest(130);
            }
            this.mOperationDone = true;
        }
    }
}
