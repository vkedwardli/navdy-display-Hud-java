package com.navdy.hud.app.bluetooth.vcard;

public class VCardEntry$SipData implements com.navdy.hud.app.bluetooth.vcard.VCardEntry$EntryElement {
    final private String mAddress;
    final private boolean mIsPrimary;
    final private String mLabel;
    final private int mType;
    
    public VCardEntry$SipData(String s, int i, String s0, boolean b) {
        if (s.startsWith("sip:")) {
            this.mAddress = s.substring(4);
        } else {
            this.mAddress = s;
        }
        this.mType = i;
        this.mLabel = s0;
        this.mIsPrimary = b;
    }
    
    public void constructInsertOperation(java.util.List a, int i) {
        android.content.ContentProviderOperation$Builder a0 = android.content.ContentProviderOperation.newInsert(android.provider.ContactsContract$Data.CONTENT_URI);
        a0.withValueBackReference("raw_contact_id", i);
        a0.withValue("mimetype", "vnd.android.cursor.item/sip_address");
        a0.withValue("data1", this.mAddress);
        a0.withValue("data2", Integer.valueOf(this.mType));
        if (this.mType == 0) {
            a0.withValue("data3", this.mLabel);
        }
        if (this.mIsPrimary) {
            a0.withValue("is_primary", Boolean.valueOf(this.mIsPrimary));
        }
        a.add(a0.build());
    }
    
    public boolean equals(Object a) {
        boolean b = false;
        label0: if (this != a) {
            if (a instanceof com.navdy.hud.app.bluetooth.vcard.VCardEntry$SipData) {
                com.navdy.hud.app.bluetooth.vcard.VCardEntry$SipData a0 = (com.navdy.hud.app.bluetooth.vcard.VCardEntry$SipData)a;
                int i = this.mType;
                int i0 = a0.mType;
                label1: {
                    if (i != i0) {
                        break label1;
                    }
                    if (!android.text.TextUtils.equals((CharSequence)this.mLabel, (CharSequence)a0.mLabel)) {
                        break label1;
                    }
                    if (!android.text.TextUtils.equals((CharSequence)this.mAddress, (CharSequence)a0.mAddress)) {
                        break label1;
                    }
                    if (this.mIsPrimary == a0.mIsPrimary) {
                        b = true;
                        break label0;
                    }
                }
                b = false;
            } else {
                b = false;
            }
        } else {
            b = true;
        }
        return b;
    }
    
    public String getAddress() {
        return this.mAddress;
    }
    
    public com.navdy.hud.app.bluetooth.vcard.VCardEntry$EntryLabel getEntryLabel() {
        return com.navdy.hud.app.bluetooth.vcard.VCardEntry$EntryLabel.SIP;
    }
    
    public String getLabel() {
        return this.mLabel;
    }
    
    public int getType() {
        return this.mType;
    }
    
    public int hashCode() {
        return ((this.mType * 31 + ((this.mLabel == null) ? 0 : this.mLabel.hashCode())) * 31 + ((this.mAddress == null) ? 0 : this.mAddress.hashCode())) * 31 + ((this.mIsPrimary) ? 1231 : 1237);
    }
    
    public boolean isEmpty() {
        return android.text.TextUtils.isEmpty((CharSequence)this.mAddress);
    }
    
    public String toString() {
        return new StringBuilder().append("sip: ").append(this.mAddress).toString();
    }
}
