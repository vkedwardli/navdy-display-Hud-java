package com.navdy.hud.app.bluetooth.vcard;

public class VCardProperty {
    final private static String LOG_TAG = "vCard";
    private byte[] mByteValue;
    private java.util.List mGroupList;
    private String mName;
    private java.util.Map mParameterMap;
    private String mRawValue;
    private java.util.List mValueList;
    
    public VCardProperty() {
        this.mParameterMap = (java.util.Map)new java.util.HashMap();
    }
    
    public void addGroup(String s) {
        if (this.mGroupList == null) {
            this.mGroupList = (java.util.List)new java.util.ArrayList();
        }
        this.mGroupList.add(s);
    }
    
    public void addParameter(String s, String s0) {
        Object a = null;
        if (this.mParameterMap.containsKey(s)) {
            a = this.mParameterMap.get(s);
        } else {
            java.util.AbstractCollection a0 = (s.equals("TYPE")) ? new java.util.HashSet() : new java.util.ArrayList();
            this.mParameterMap.put(s, a0);
            a = a0;
        }
        ((java.util.Collection)a).add(s0);
    }
    
    public void addValues(java.util.List a) {
        if (this.mValueList != null) {
            this.mValueList.addAll((java.util.Collection)a);
        } else {
            this.mValueList = (java.util.List)new java.util.ArrayList((java.util.Collection)a);
        }
    }
    
    public void addValues(String[] a) {
        if (this.mValueList != null) {
            this.mValueList.addAll((java.util.Collection)java.util.Arrays.asList((Object[])a));
        } else {
            this.mValueList = java.util.Arrays.asList((Object[])a);
        }
    }
    
    public byte[] getByteValue() {
        return this.mByteValue;
    }
    
    public java.util.List getGroupList() {
        return this.mGroupList;
    }
    
    public String getName() {
        return this.mName;
    }
    
    public java.util.Map getParameterMap() {
        return this.mParameterMap;
    }
    
    public java.util.Collection getParameters(String s) {
        return (java.util.Collection)this.mParameterMap.get(s);
    }
    
    public String getRawValue() {
        return this.mRawValue;
    }
    
    public java.util.List getValueList() {
        return this.mValueList;
    }
    
    public void setByteValue(byte[] a) {
        this.mByteValue = a;
    }
    
    public void setName(String s) {
        if (this.mName != null) {
            Object[] a = new Object[2];
            a[0] = this.mName;
            a[1] = s;
            android.util.Log.w("vCard", String.format("Property name is re-defined (existing: %s, requested: %s", a));
        }
        this.mName = s;
    }
    
    public void setParameter(String s, String s0) {
        this.mParameterMap.clear();
        this.addParameter(s, s0);
    }
    
    public void setRawValue(String s) {
        this.mRawValue = s;
    }
    
    public void setValues(java.util.List a) {
        this.mValueList = a;
    }
    
    public void setValues(String[] a) {
        this.mValueList = java.util.Arrays.asList((Object[])a);
    }
}
