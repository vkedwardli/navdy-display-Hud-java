package com.navdy.hud.app.bluetooth.vcard;

class VCardParserImpl_V30 extends com.navdy.hud.app.bluetooth.vcard.VCardParserImpl_V21 {
    final private static String LOG_TAG = "vCard";
    private boolean mEmittedAgentWarning;
    private String mPreviousLine;
    
    public VCardParserImpl_V30() {
        this.mEmittedAgentWarning = false;
    }
    
    public VCardParserImpl_V30(int i) {
        super(i);
        this.mEmittedAgentWarning = false;
    }
    
    private void splitAndPutParam(com.navdy.hud.app.bluetooth.vcard.VCardProperty a, String s, String s0) {
        int i = s0.length();
        StringBuilder a0 = null;
        boolean b = false;
        int i0 = 0;
        while(i0 < i) {
            int i1 = s0.charAt(i0);
            label0: if (i1 != 34) {
                label1: {
                    if (i1 != 44) {
                        break label1;
                    }
                    if (b) {
                        break label1;
                    }
                    if (a0 != null) {
                        a.addParameter(s, this.encodeParamValue(a0.toString()));
                        a0 = null;
                        break label0;
                    } else {
                        android.util.Log.w("vCard", new StringBuilder().append("Comma is used before actual string comes. (").append(s0).append(")").toString());
                        break label0;
                    }
                }
                if (a0 == null) {
                    a0 = new StringBuilder();
                }
                a0.append((char)i1);
            } else if (b) {
                a.addParameter(s, this.encodeParamValue(a0.toString()));
                b = false;
                a0 = null;
            } else {
                if (a0 != null) {
                    if (a0.length() <= 0) {
                        a.addParameter(s, this.encodeParamValue(a0.toString()));
                    } else {
                        android.util.Log.w("vCard", "Unexpected Dquote inside property.");
                    }
                }
                b = true;
            }
            i0 = i0 + 1;
        }
        if (b) {
            android.util.Log.d("vCard", "Dangling Dquote.");
        }
        if (a0 != null) {
            if (a0.length() != 0) {
                a.addParameter(s, this.encodeParamValue(a0.toString()));
            } else {
                android.util.Log.w("vCard", "Unintended behavior. We must not see empty StringBuilder at the end of parameter value parsing.");
            }
        }
    }
    
    public static String unescapeCharacter(char a) {
        String s = null;
        int i = a;
        label2: {
            label0: {
                label1: {
                    if (a == 110) {
                        break label1;
                    }
                    if (i != 78) {
                        break label0;
                    }
                }
                s = "\n";
                break label2;
            }
            s = String.valueOf((char)i);
        }
        return s;
    }
    
    public static String unescapeText(String s) {
        StringBuilder a = new StringBuilder();
        int i = s.length();
        int i0 = 0;
        while(i0 < i) {
            int i1 = s.charAt(i0);
            label2: {
                label3: {
                    label4: {
                        if (i1 != 92) {
                            break label4;
                        }
                        if (i0 < i - 1) {
                            break label3;
                        }
                    }
                    a.append((char)i1);
                    break label2;
                }
                i0 = i0 + 1;
                int i2 = s.charAt(i0);
                label0: {
                    label1: {
                        if (i2 == 110) {
                            break label1;
                        }
                        if (i2 != 78) {
                            break label0;
                        }
                    }
                    a.append("\n");
                    break label2;
                }
                a.append((char)i2);
            }
            i0 = i0 + 1;
        }
        return a.toString();
    }
    
    protected String encodeParamValue(String s) {
        return com.navdy.hud.app.bluetooth.vcard.VCardUtils.convertStringCharset(s, "ISO-8859-1", "UTF-8");
    }
    
    protected String getBase64(String s) {
        return s;
    }
    
    protected java.util.Set getKnownPropertyNameSet() {
        return com.navdy.hud.app.bluetooth.vcard.VCardParser_V30.sKnownPropertyNameSet;
    }
    
    protected String getLine() {
        String s = null;
        if (this.mPreviousLine == null) {
            s = this.mReader.readLine();
        } else {
            s = this.mPreviousLine;
            this.mPreviousLine = null;
        }
        return s;
    }
    
    protected String getNonEmptyLine() {
        StringBuilder a = null;
        while(true) {
            String s = null;
            String s0 = this.mReader.readLine();
            label0: if (s0 != null) {
                if (s0.length() == 0) {
                    continue;
                }
                int i = s0.charAt(0);
                label1: {
                    if (i == 32) {
                        break label1;
                    }
                    int i0 = s0.charAt(0);
                    if (i0 == 9) {
                        break label1;
                    }
                    if (a != null) {
                        break label0;
                    }
                    if (this.mPreviousLine != null) {
                        break label0;
                    }
                    this.mPreviousLine = s0;
                    continue;
                }
                if (a == null) {
                    a = new StringBuilder();
                }
                if (this.mPreviousLine != null) {
                    a.append(this.mPreviousLine);
                    this.mPreviousLine = null;
                }
                a.append(s0.substring(1));
                continue;
            }
            if (a == null) {
                String s1 = this.mPreviousLine;
                s = null;
                if (s1 != null) {
                    s = this.mPreviousLine;
                }
            } else {
                s = a.toString();
            }
            this.mPreviousLine = s0;
            if (s == null) {
                throw new com.navdy.hud.app.bluetooth.vcard.exception.VCardException("Reached end of buffer.");
            }
            return s;
        }
    }
    
    protected int getVersion() {
        return 1;
    }
    
    protected String getVersionString() {
        return "3.0";
    }
    
    protected void handleAgent(com.navdy.hud.app.bluetooth.vcard.VCardProperty a) {
        if (!this.mEmittedAgentWarning) {
            android.util.Log.w("vCard", "AGENT in vCard 3.0 is not supported yet. Ignore it");
            this.mEmittedAgentWarning = true;
        }
    }
    
    protected void handleAnyParam(com.navdy.hud.app.bluetooth.vcard.VCardProperty a, String s, String s0) {
        this.splitAndPutParam(a, s, s0);
    }
    
    protected void handleParamWithoutName(com.navdy.hud.app.bluetooth.vcard.VCardProperty a, String s) {
        this.handleType(a, s);
    }
    
    protected void handleParams(com.navdy.hud.app.bluetooth.vcard.VCardProperty a, String s) {
        try {
            ((com.navdy.hud.app.bluetooth.vcard.VCardParserImpl_V21)this).handleParams(a, s);
        } catch(com.navdy.hud.app.bluetooth.vcard.exception.VCardException ignoredException) {
            String[] a0 = s.split("=", 2);
            if (a0.length != 2) {
                throw new com.navdy.hud.app.bluetooth.vcard.exception.VCardException(new StringBuilder().append("Unknown params value: ").append(s).toString());
            }
            this.handleAnyParam(a, a0[0], a0[1]);
        }
    }
    
    protected void handleType(com.navdy.hud.app.bluetooth.vcard.VCardProperty a, String s) {
        this.splitAndPutParam(a, "TYPE", s);
    }
    
    protected String maybeUnescapeCharacter(char a) {
        return com.navdy.hud.app.bluetooth.vcard.VCardParserImpl_V30.unescapeCharacter(a);
    }
    
    protected String maybeUnescapeText(String s) {
        return com.navdy.hud.app.bluetooth.vcard.VCardParserImpl_V30.unescapeText(s);
    }
    
    protected boolean readBeginVCard(boolean b) {
        return ((com.navdy.hud.app.bluetooth.vcard.VCardParserImpl_V21)this).readBeginVCard(b);
    }
}
