package com.navdy.hud.app.bluetooth.obex;

final public class ServerOperation implements com.navdy.hud.app.bluetooth.obex.Operation, com.navdy.hud.app.bluetooth.obex.BaseStream {
    final private static String TAG = "ServerOperation";
    final private static boolean V = false;
    public boolean finalBitSet;
    public boolean isAborted;
    private boolean mClosed;
    private String mExceptionString;
    private boolean mGetOperation;
    private boolean mHasBody;
    private java.io.InputStream mInput;
    private com.navdy.hud.app.bluetooth.obex.ServerRequestHandler mListener;
    private int mMaxPacketLength;
    private com.navdy.hud.app.bluetooth.obex.ServerSession mParent;
    private com.navdy.hud.app.bluetooth.obex.PrivateInputStream mPrivateInput;
    private com.navdy.hud.app.bluetooth.obex.PrivateOutputStream mPrivateOutput;
    private boolean mPrivateOutputOpen;
    private boolean mRequestFinished;
    private int mResponseSize;
    private boolean mSendBodyHeader;
    private boolean mSrmActive;
    private boolean mSrmEnabled;
    private boolean mSrmLocalWait;
    private boolean mSrmResponseSent;
    private boolean mSrmWaitingForRemote;
    private com.navdy.hud.app.bluetooth.obex.ObexTransport mTransport;
    public com.navdy.hud.app.bluetooth.obex.HeaderSet replyHeader;
    public com.navdy.hud.app.bluetooth.obex.HeaderSet requestHeader;
    
    public ServerOperation(com.navdy.hud.app.bluetooth.obex.ServerSession a, java.io.InputStream a0, int i, int i0, com.navdy.hud.app.bluetooth.obex.ServerRequestHandler a1) {
        this.mSendBodyHeader = true;
        this.mSrmEnabled = false;
        this.mSrmActive = false;
        this.mSrmResponseSent = false;
        this.mSrmWaitingForRemote = true;
        this.mSrmLocalWait = false;
        this.isAborted = false;
        this.mParent = a;
        this.mInput = a0;
        this.mMaxPacketLength = i0;
        this.mClosed = false;
        this.requestHeader = new com.navdy.hud.app.bluetooth.obex.HeaderSet();
        this.replyHeader = new com.navdy.hud.app.bluetooth.obex.HeaderSet();
        this.mPrivateInput = new com.navdy.hud.app.bluetooth.obex.PrivateInputStream((com.navdy.hud.app.bluetooth.obex.BaseStream)this);
        this.mResponseSize = 3;
        this.mListener = a1;
        this.mRequestFinished = false;
        this.mPrivateOutputOpen = false;
        this.mHasBody = false;
        this.mTransport = a.getTransport();
        label2: {
            label3: {
                label4: {
                    if (i == 2) {
                        break label4;
                    }
                    if (i != 130) {
                        break label3;
                    }
                }
                this.mGetOperation = false;
                if ((i & 128) != 0) {
                    this.finalBitSet = true;
                    this.mRequestFinished = true;
                    break label2;
                } else {
                    this.finalBitSet = false;
                    break label2;
                }
            }
            if (i != 3 && i != 131) {
                throw new java.io.IOException("ServerOperation can not handle such request");
            }
            this.mGetOperation = true;
            this.finalBitSet = false;
            if (i == 131) {
                this.mRequestFinished = true;
            }
        }
        com.navdy.hud.app.bluetooth.obex.ObexPacket a2 = com.navdy.hud.app.bluetooth.obex.ObexPacket.read(i, this.mInput);
        if (a2.mLength > com.navdy.hud.app.bluetooth.obex.ObexHelper.getMaxRxPacketSize(this.mTransport)) {
            this.mParent.sendResponse(206, (byte[])null);
            throw new java.io.IOException(new StringBuilder().append("Packet received was too large. Length: ").append(a2.mLength).append(" maxLength: ").append(com.navdy.hud.app.bluetooth.obex.ObexHelper.getMaxRxPacketSize(this.mTransport)).toString());
        }
        int i1 = a2.mLength;
        label0: {
            label1: {
                if (i1 <= 3) {
                    break label1;
                }
                if (!this.handleObexPacket(a2)) {
                    break label0;
                }
                if (!this.mHasBody) {
                    while(!this.mGetOperation && !this.finalBitSet) {
                        this.sendReply(144);
                        if (this.mPrivateInput.available() > 0) {
                            break;
                        }
                    }
                }
            }
            while(!this.mGetOperation && !this.finalBitSet) {
                if (this.mPrivateInput.available() != 0) {
                    break;
                }
                this.sendReply(144);
                if (this.mPrivateInput.available() > 0) {
                    break;
                }
            }
            while(this.mGetOperation && !this.mRequestFinished) {
                this.sendReply(144);
            }
        }
    }
    
    private void checkForSrmWait(int i) {
        label0: if (this.mSrmEnabled) {
            label1: {
                if (i == 3) {
                    break label1;
                }
                if (i == 131) {
                    break label1;
                }
                if (i != 2) {
                    break label0;
                }
            }
            try {
                this.mSrmWaitingForRemote = false;
                Byte a = (Byte)this.requestHeader.getHeader(152);
                if (a != null) {
                    int i0 = a.byteValue();
                    if (i0 == 1) {
                        this.mSrmWaitingForRemote = true;
                        this.requestHeader.setHeader(152, null);
                    }
                }
            } catch(java.io.IOException ignoredException) {
            }
        }
    }
    
    private void checkSrmRemoteAbort() {
        if (this.mInput.available() > 0) {
            com.navdy.hud.app.bluetooth.obex.ObexPacket a = com.navdy.hud.app.bluetooth.obex.ObexPacket.read(this.mInput);
            if (a.mHeaderId != 255) {
                android.util.Log.w("ServerOperation", new StringBuilder().append("Received unexpected request from client - discarding...\n   headerId: ").append(a.mHeaderId).append(" length: ").append(a.mLength).toString());
            } else {
                this.handleRemoteAbort();
            }
        }
    }
    
    private boolean handleObexPacket(com.navdy.hud.app.bluetooth.obex.ObexPacket a) {
        boolean b = false;
        byte[] a0 = this.updateRequestHeaders(a);
        if (a0 != null) {
            this.mHasBody = true;
        }
        long j = this.mListener.getConnectionId();
        int i = (j < -1L) ? -1 : (j == -1L) ? 0 : 1;
        label4: {
            label3: {
                if (i == 0) {
                    break label3;
                }
                if (this.requestHeader.mConnectionID == null) {
                    break label3;
                }
                this.mListener.setConnectionId(com.navdy.hud.app.bluetooth.obex.ObexHelper.convertToLong(this.requestHeader.mConnectionID));
                break label4;
            }
            this.mListener.setConnectionId(1L);
        }
        byte[] a1 = this.requestHeader.mAuthResp;
        label2: {
            label1: {
                if (a1 == null) {
                    break label1;
                }
                boolean b0 = this.mParent.handleAuthResp(this.requestHeader.mAuthResp);
                label0: {
                    if (!b0) {
                        break label0;
                    }
                    this.requestHeader.mAuthResp = null;
                    break label1;
                }
                this.mExceptionString = "Authentication Failed";
                this.mParent.sendResponse(193, (byte[])null);
                this.mClosed = true;
                this.requestHeader.mAuthResp = null;
                b = false;
                break label2;
            }
            if (this.requestHeader.mAuthChall != null) {
                this.mParent.handleAuthChall(this.requestHeader);
                this.replyHeader.mAuthResp = new byte[this.requestHeader.mAuthResp.length];
                System.arraycopy(this.requestHeader.mAuthResp, 0, this.replyHeader.mAuthResp, 0, this.replyHeader.mAuthResp.length);
                this.requestHeader.mAuthResp = null;
                this.requestHeader.mAuthChall = null;
            }
            if (a0 != null) {
                this.mPrivateInput.writeBytes(a0, 1);
            }
            b = true;
        }
        return b;
    }
    
    private void handleRemoteAbort() {
        this.mParent.sendResponse(160, (byte[])null);
        this.mClosed = true;
        this.isAborted = true;
        this.mExceptionString = "Abort Received";
        throw new java.io.IOException("Abort Received");
    }
    
    private byte[] updateRequestHeaders(com.navdy.hud.app.bluetooth.obex.ObexPacket a) {
        byte[] a0 = a.mPayload;
        byte[] a1 = null;
        if (a0 != null) {
            a1 = com.navdy.hud.app.bluetooth.obex.ObexHelper.updateHeaderSet(this.requestHeader, a.mPayload);
        }
        Byte a2 = (Byte)this.requestHeader.getHeader(151);
        if (this.mTransport.isSrmSupported() && a2 != null) {
            int i = a2.byteValue();
            if (i == 1) {
                this.mSrmEnabled = true;
            }
        }
        this.checkForSrmWait(a.mHeaderId);
        if (!this.mSrmWaitingForRemote && this.mSrmEnabled) {
            this.mSrmActive = true;
        }
        return a1;
    }
    
    public void abort() {
        throw new java.io.IOException("Called from a server");
    }
    
    public void close() {
        this.ensureOpen();
        this.mClosed = true;
    }
    
    public boolean continueOperation(boolean b, boolean b0) {
        boolean b1 = false;
        label1: synchronized(this) {
            if (this.mGetOperation) {
                this.sendReply(144);
                b1 = true;
            } else if (this.finalBitSet) {
                b1 = false;
            } else if (b) {
                this.sendReply(144);
                b1 = true;
            } else {
                int i = this.mResponseSize;
                label0: {
                    if (i > 3) {
                        break label0;
                    }
                    if (this.mPrivateOutput.size() > 0) {
                        break label0;
                    }
                    b1 = false;
                    break label1;
                }
                this.sendReply(144);
                b1 = true;
            }
        }
        /*monexit(this)*/;
        return b1;
    }
    
    public void ensureNotDone() {
    }
    
    public void ensureOpen() {
        if (this.mExceptionString != null) {
            throw new java.io.IOException(this.mExceptionString);
        }
        if (this.mClosed) {
            throw new java.io.IOException("Operation has already ended");
        }
    }
    
    public String getEncoding() {
        return null;
    }
    
    public int getHeaderLength() {
        long j = this.mListener.getConnectionId();
        if (j != -1L) {
            this.replyHeader.mConnectionID = com.navdy.hud.app.bluetooth.obex.ObexHelper.convertToByteArray(j);
        } else {
            this.replyHeader.mConnectionID = null;
        }
        return com.navdy.hud.app.bluetooth.obex.ObexHelper.createHeader(this.replyHeader, false).length;
    }
    
    public long getLength() {
        long j = 0L;
        try {
            Long a = (Long)this.requestHeader.getHeader(195);
            j = (a != null) ? a.longValue() : -1L;
        } catch(java.io.IOException ignoredException) {
            j = -1L;
        }
        return j;
    }
    
    public int getMaxPacketSize() {
        return this.mMaxPacketLength - 6 - this.getHeaderLength();
    }
    
    public com.navdy.hud.app.bluetooth.obex.HeaderSet getReceivedHeader() {
        this.ensureOpen();
        return this.requestHeader;
    }
    
    public int getResponseCode() {
        throw new java.io.IOException("Called from a server");
    }
    
    public String getType() {
        String s = null;
        try {
            s = (String)this.requestHeader.getHeader(66);
        } catch(java.io.IOException ignoredException) {
            s = null;
        }
        return s;
    }
    
    public boolean isValidBody() {
        return this.mHasBody;
    }
    
    public void noBodyHeader() {
        this.mSendBodyHeader = false;
    }
    
    public java.io.DataInputStream openDataInputStream() {
        return new java.io.DataInputStream(this.openInputStream());
    }
    
    public java.io.DataOutputStream openDataOutputStream() {
        return new java.io.DataOutputStream(this.openOutputStream());
    }
    
    public java.io.InputStream openInputStream() {
        this.ensureOpen();
        return this.mPrivateInput;
    }
    
    public java.io.OutputStream openOutputStream() {
        this.ensureOpen();
        if (this.mPrivateOutputOpen) {
            throw new java.io.IOException("no more input streams available, stream already opened");
        }
        if (!this.mRequestFinished) {
            throw new java.io.IOException("no  output streams available ,request not finished");
        }
        if (this.mPrivateOutput == null) {
            this.mPrivateOutput = new com.navdy.hud.app.bluetooth.obex.PrivateOutputStream((com.navdy.hud.app.bluetooth.obex.BaseStream)this, this.getMaxPacketSize());
        }
        this.mPrivateOutputOpen = true;
        return this.mPrivateOutput;
    }
    
    public void sendHeaders(com.navdy.hud.app.bluetooth.obex.HeaderSet a) {
        this.ensureOpen();
        if (a == null) {
            throw new java.io.IOException("Headers may not be null");
        }
        int[] a0 = a.getHeaderList();
        if (a0 != null) {
            int i = 0;
            while(i < a0.length) {
                this.replyHeader.setHeader((a0[i] != 0) ? 1 : 0, a.getHeader((a0[i] != 0) ? 1 : 0));
                i = i + 1;
            }
        }
    }
    
    public boolean sendReply(int i) {
        boolean b = false;
        label5: synchronized(this) {
            boolean b0 = false;
            int i0 = 0;
            int i1 = 0;
            java.io.ByteArrayOutputStream a = new java.io.ByteArrayOutputStream();
            long j = this.mListener.getConnectionId();
            if (j != -1L) {
                this.replyHeader.mConnectionID = com.navdy.hud.app.bluetooth.obex.ObexHelper.convertToByteArray(j);
            } else {
                this.replyHeader.mConnectionID = null;
            }
            if (this.mSrmEnabled) {
                if (this.mSrmResponseSent) {
                    b0 = false;
                } else {
                    this.replyHeader.setHeader(151, Byte.valueOf((byte)1));
                    b0 = true;
                }
            } else {
                b0 = false;
            }
            if (this.mSrmEnabled && !this.mGetOperation && this.mSrmLocalWait) {
                this.replyHeader.setHeader(151, Byte.valueOf((byte)1));
            }
            byte[] a0 = com.navdy.hud.app.bluetooth.obex.ObexHelper.createHeader(this.replyHeader, true);
            if (this.mPrivateOutput == null) {
                i0 = -1;
                i1 = -1;
            } else {
                i1 = this.mPrivateOutput.size();
                i0 = i1;
            }
            if (a0.length + 3 <= this.mMaxPacketLength) {
                boolean b1 = false;
                boolean b2 = false;
                a.write(a0);
                if (this.mGetOperation && i == 160) {
                    this.finalBitSet = true;
                }
                label11: if (this.mSrmActive) {
                    boolean b3 = this.mGetOperation;
                    label12: {
                        if (b3) {
                            break label12;
                        }
                        if (i != 144) {
                            break label12;
                        }
                        if (!this.mSrmResponseSent) {
                            break label12;
                        }
                        b1 = true;
                        b2 = false;
                        break label11;
                    }
                    boolean b4 = this.mGetOperation;
                    label10: {
                        if (!b4) {
                            break label10;
                        }
                        if (this.mRequestFinished) {
                            break label10;
                        }
                        if (!this.mSrmResponseSent) {
                            break label10;
                        }
                        b1 = true;
                        b2 = false;
                        break label11;
                    }
                    if (this.mGetOperation) {
                        if (this.mRequestFinished) {
                            b1 = false;
                            b2 = true;
                        } else {
                            b1 = false;
                            b2 = false;
                        }
                    } else {
                        b1 = false;
                        b2 = false;
                    }
                } else {
                    b1 = false;
                    b2 = false;
                }
                if (b0) {
                    this.mSrmResponseSent = true;
                }
                boolean b5 = this.finalBitSet;
                label6: {
                    label9: {
                        if (b5) {
                            break label9;
                        }
                        if (a0.length >= this.mMaxPacketLength - 20) {
                            break label6;
                        }
                    }
                    if (i0 > 0) {
                        if (i0 > this.mMaxPacketLength - a0.length - 6) {
                            i0 = this.mMaxPacketLength - a0.length - 6;
                        }
                        byte[] a1 = this.mPrivateOutput.readBytes(i0);
                        boolean b6 = this.finalBitSet;
                        label7: {
                            label8: {
                                if (b6) {
                                    break label8;
                                }
                                if (!this.mPrivateOutput.isClosed()) {
                                    break label7;
                                }
                            }
                            if (!this.mSendBodyHeader) {
                                break label6;
                            }
                            a.write(73);
                            int i2 = i0 + 3;
                            int i3 = (byte)(i2 >> 8);
                            a.write(i3);
                            int i4 = (byte)i2;
                            a.write(i4);
                            a.write(a1);
                            break label6;
                        }
                        if (this.mSendBodyHeader) {
                            a.write(72);
                            int i5 = i0 + 3;
                            int i6 = (byte)(i5 >> 8);
                            a.write(i6);
                            int i7 = (byte)i5;
                            a.write(i7);
                            a.write(a1);
                        }
                    }
                }
                if (this.finalBitSet && i == 160 && i1 <= 0 && this.mSendBodyHeader) {
                    a.write(73);
                    a.write(0);
                    a.write(3);
                }
                if (!b1) {
                    this.mResponseSize = 3;
                    this.mParent.sendResponse(i, a.toByteArray());
                }
                if (i != 144) {
                    b = false;
                } else {
                    boolean b7 = this.mGetOperation;
                    label0: {
                        label1: {
                            label4: {
                                if (!b7) {
                                    break label4;
                                }
                                if (!b2) {
                                    break label4;
                                }
                                this.checkSrmRemoteAbort();
                                break label1;
                            }
                            com.navdy.hud.app.bluetooth.obex.ObexPacket a2 = com.navdy.hud.app.bluetooth.obex.ObexPacket.read(this.mInput);
                            int i8 = a2.mHeaderId;
                            label3: {
                                if (i8 == 2) {
                                    break label3;
                                }
                                if (i8 == 130) {
                                    break label3;
                                }
                                if (i8 == 3) {
                                    break label3;
                                }
                                if (i8 == 131) {
                                    break label3;
                                }
                                if (i8 != 255) {
                                    this.mParent.sendResponse(192, (byte[])null);
                                    this.mClosed = true;
                                    this.mExceptionString = "Bad Request Received";
                                    throw new java.io.IOException("Bad Request Received");
                                }
                                this.handleRemoteAbort();
                                break label1;
                            }
                            if (i8 != 130) {
                                if (i8 == 131) {
                                    this.mRequestFinished = true;
                                }
                            } else {
                                this.finalBitSet = true;
                            }
                            if (a2.mLength > com.navdy.hud.app.bluetooth.obex.ObexHelper.getMaxRxPacketSize(this.mTransport)) {
                                this.mParent.sendResponse(206, (byte[])null);
                                throw new java.io.IOException("Packet received was too large");
                            }
                            int i9 = a2.mLength;
                            label2: {
                                if (i9 > 3) {
                                    break label2;
                                }
                                if (!this.mSrmEnabled) {
                                    break label1;
                                }
                                if (a2.mLength != 3) {
                                    break label1;
                                }
                            }
                            if (!this.handleObexPacket(a2)) {
                                break label0;
                            }
                        }
                        b = true;
                        break label5;
                    }
                    b = false;
                }
            } else {
                int i10 = 0;
                int i11 = 0;
                while(i10 != a0.length) {
                    i10 = com.navdy.hud.app.bluetooth.obex.ObexHelper.findHeaderEnd(a0, i11, this.mMaxPacketLength - 3);
                    if (i10 != -1) {
                        byte[] a3 = new byte[i10 - i11];
                        System.arraycopy(a0, i11, a3, 0, a3.length);
                        this.mParent.sendResponse(i, a3);
                        i11 = i10;
                    } else {
                        this.mClosed = true;
                        if (this.mPrivateInput != null) {
                            this.mPrivateInput.close();
                        }
                        if (this.mPrivateOutput != null) {
                            this.mPrivateOutput.close();
                        }
                        this.mParent.sendResponse(208, (byte[])null);
                        throw new java.io.IOException("OBEX Packet exceeds max packet size");
                    }
                }
                b = i0 > 0;
            }
        }
        /*monexit(this)*/;
        return b;
    }
    
    public void streamClosed(boolean b) {
    }
}
