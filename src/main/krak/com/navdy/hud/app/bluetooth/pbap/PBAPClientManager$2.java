package com.navdy.hud.app.bluetooth.pbap;

class PBAPClientManager$2 implements Runnable {
    final com.navdy.hud.app.bluetooth.pbap.PBAPClientManager this$0;
    
    PBAPClientManager$2(com.navdy.hud.app.bluetooth.pbap.PBAPClientManager a) {
        super();
        this.this$0 = a;
    }
    
    public void run() {
        try {
            com.navdy.hud.app.bluetooth.pbap.PBAPClientManager.access$000().v("pbap hang detected, reattempting...");
            com.navdy.hud.app.bluetooth.pbap.PBAPClientManager.access$500(this.this$0);
            com.navdy.hud.app.bluetooth.pbap.PBAPClientManager.access$200(this.this$0).removeCallbacks(com.navdy.hud.app.bluetooth.pbap.PBAPClientManager.access$600(this.this$0));
            com.navdy.hud.app.bluetooth.pbap.PBAPClientManager.access$200(this.this$0).post(com.navdy.hud.app.bluetooth.pbap.PBAPClientManager.access$600(this.this$0));
        } catch(Throwable a) {
            com.navdy.hud.app.bluetooth.pbap.PBAPClientManager.access$000().e(a);
        }
    }
}
