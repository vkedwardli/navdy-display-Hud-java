package com.navdy.hud.app.bluetooth.vcard;

public class VCardConfig {
    final public static String DEFAULT_EXPORT_CHARSET = "UTF-8";
    final public static String DEFAULT_IMPORT_CHARSET = "UTF-8";
    final public static String DEFAULT_INTERMEDIATE_CHARSET = "ISO-8859-1";
    final public static int FLAG_APPEND_TYPE_PARAM = 67108864;
    final public static int FLAG_CONVERT_PHONETIC_NAME_STRINGS = 134217728;
    final private static int FLAG_DOCOMO = 536870912;
    final public static int FLAG_REFRAIN_IMAGE_EXPORT = 8388608;
    final public static int FLAG_REFRAIN_PHONE_NUMBER_FORMATTING = 33554432;
    final public static int FLAG_REFRAIN_QP_TO_NAME_PROPERTIES = 268435456;
    final private static int FLAG_USE_ANDROID_PROPERTY = -2147483648;
    final private static int FLAG_USE_DEFACT_PROPERTY = 1073741824;
    final static int LOG_LEVEL = 0;
    final static int LOG_LEVEL_NONE = 0;
    final static int LOG_LEVEL_PERFORMANCE_MEASUREMENT = 1;
    final static int LOG_LEVEL_SHOW_WARNING = 2;
    final static int LOG_LEVEL_VERBOSE = 3;
    final private static String LOG_TAG = "vCard";
    final public static int NAME_ORDER_DEFAULT = 0;
    final public static int NAME_ORDER_EUROPE = 4;
    final public static int NAME_ORDER_JAPANESE = 8;
    final private static int NAME_ORDER_MASK = 12;
    public static int VCARD_TYPE_DEFAULT = 0;
    final public static int VCARD_TYPE_DOCOMO = 939524104;
    final static String VCARD_TYPE_DOCOMO_STR = "docomo";
    final public static int VCARD_TYPE_UNKNOWN = 0;
    final public static int VCARD_TYPE_V21_EUROPE = -1073741820;
    final static String VCARD_TYPE_V21_EUROPE_STR = "v21_europe";
    final public static int VCARD_TYPE_V21_GENERIC = -1073741824;
    static String VCARD_TYPE_V21_GENERIC_STR;
    final public static int VCARD_TYPE_V21_JAPANESE = -1073741816;
    final public static int VCARD_TYPE_V21_JAPANESE_MOBILE = 402653192;
    final static String VCARD_TYPE_V21_JAPANESE_MOBILE_STR = "v21_japanese_mobile";
    final static String VCARD_TYPE_V21_JAPANESE_STR = "v21_japanese_utf8";
    final public static int VCARD_TYPE_V30_EUROPE = -1073741819;
    final static String VCARD_TYPE_V30_EUROPE_STR = "v30_europe";
    final public static int VCARD_TYPE_V30_GENERIC = -1073741823;
    final static String VCARD_TYPE_V30_GENERIC_STR = "v30_generic";
    final public static int VCARD_TYPE_V30_JAPANESE = -1073741815;
    final static String VCARD_TYPE_V30_JAPANESE_STR = "v30_japanese_utf8";
    final public static int VCARD_TYPE_V40_GENERIC = -1073741822;
    final static String VCARD_TYPE_V40_GENERIC_STR = "v40_generic";
    final public static int VERSION_21 = 0;
    final public static int VERSION_30 = 1;
    final public static int VERSION_40 = 2;
    final public static int VERSION_MASK = 3;
    final private static java.util.Set sJapaneseMobileTypeSet;
    final private static java.util.Map sVCardTypeMap;
    
    static {
        VCARD_TYPE_V21_GENERIC_STR = "v21_generic";
        VCARD_TYPE_DEFAULT = -1073741824;
        sVCardTypeMap = (java.util.Map)new java.util.HashMap();
        sVCardTypeMap.put(VCARD_TYPE_V21_GENERIC_STR, Integer.valueOf(-1073741824));
        sVCardTypeMap.put("v30_generic", Integer.valueOf(-1073741823));
        sVCardTypeMap.put("v21_europe", Integer.valueOf(-1073741820));
        sVCardTypeMap.put("v30_europe", Integer.valueOf(-1073741819));
        sVCardTypeMap.put("v21_japanese_utf8", Integer.valueOf(-1073741816));
        sVCardTypeMap.put("v30_japanese_utf8", Integer.valueOf(-1073741815));
        sVCardTypeMap.put("v21_japanese_mobile", Integer.valueOf(402653192));
        sVCardTypeMap.put("docomo", Integer.valueOf(939524104));
        sJapaneseMobileTypeSet = (java.util.Set)new java.util.HashSet();
        sJapaneseMobileTypeSet.add(Integer.valueOf(-1073741816));
        sJapaneseMobileTypeSet.add(Integer.valueOf(-1073741815));
        sJapaneseMobileTypeSet.add(Integer.valueOf(402653192));
        sJapaneseMobileTypeSet.add(Integer.valueOf(939524104));
    }
    
    private VCardConfig() {
    }
    
    public static boolean appendTypeParamName(int i) {
        boolean b = false;
        boolean b0 = com.navdy.hud.app.bluetooth.vcard.VCardConfig.isVersion30(i);
        label2: {
            label0: {
                label1: {
                    if (b0) {
                        break label1;
                    }
                    if ((67108864 & i) == 0) {
                        break label0;
                    }
                }
                b = true;
                break label2;
            }
            b = false;
        }
        return b;
    }
    
    public static int getNameOrderType(int i) {
        return i & 12;
    }
    
    public static int getVCardTypeFromString(String s) {
        int i = 0;
        String s0 = s.toLowerCase();
        if (sVCardTypeMap.containsKey(s0)) {
            i = ((Integer)sVCardTypeMap.get(s0)).intValue();
        } else if ("default".equalsIgnoreCase(s)) {
            i = VCARD_TYPE_DEFAULT;
        } else {
            android.util.Log.e("vCard", new StringBuilder().append("Unknown vCard type String: \"").append(s).append("\"").toString());
            i = VCARD_TYPE_DEFAULT;
        }
        return i;
    }
    
    public static boolean isDoCoMo(int i) {
        return (536870912 & i) != 0;
    }
    
    public static boolean isJapaneseDevice(int i) {
        return sJapaneseMobileTypeSet.contains(Integer.valueOf(i));
    }
    
    public static boolean isVersion21(int i) {
        return (i & 3) == 0;
    }
    
    public static boolean isVersion30(int i) {
        return (i & 3) == 1;
    }
    
    public static boolean isVersion40(int i) {
        return (i & 3) == 2;
    }
    
    public static boolean needsToConvertPhoneticString(int i) {
        return (134217728 & i) != 0;
    }
    
    public static boolean onlyOneNoteFieldIsAvailable(int i) {
        return i == 939524104;
    }
    
    static boolean refrainPhoneNumberFormatting(int i) {
        return (33554432 & i) != 0;
    }
    
    public static boolean shouldRefrainQPToNameProperties(int i) {
        boolean b = false;
        boolean b0 = com.navdy.hud.app.bluetooth.vcard.VCardConfig.shouldUseQuotedPrintable(i);
        label2: {
            label0: {
                label1: {
                    if (!b0) {
                        break label1;
                    }
                    if ((268435456 & i) == 0) {
                        break label0;
                    }
                }
                b = true;
                break label2;
            }
            b = false;
        }
        return b;
    }
    
    public static boolean shouldUseQuotedPrintable(int i) {
        return !com.navdy.hud.app.bluetooth.vcard.VCardConfig.isVersion30(i);
    }
    
    public static boolean showPerformanceLog() {
        return false;
    }
    
    public static boolean usesAndroidSpecificProperty(int i) {
        return (-2147483648 & i) != 0;
    }
    
    public static boolean usesDefactProperty(int i) {
        return (1073741824 & i) != 0;
    }
}
