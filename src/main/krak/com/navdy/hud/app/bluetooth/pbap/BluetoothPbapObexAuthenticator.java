package com.navdy.hud.app.bluetooth.pbap;

class BluetoothPbapObexAuthenticator implements com.navdy.hud.app.bluetooth.obex.Authenticator {
    final private static String TAG = "BTPbapObexAuthenticator";
    final private android.os.Handler mCallback;
    private boolean mReplied;
    private String mSessionKey;
    
    public BluetoothPbapObexAuthenticator(android.os.Handler a) {
        this.mCallback = a;
    }
    
    public com.navdy.hud.app.bluetooth.obex.PasswordAuthentication onAuthenticationChallenge(String s, boolean b, boolean b0) {
        com.navdy.hud.app.bluetooth.obex.PasswordAuthentication a = null;
        this.mReplied = false;
        android.util.Log.d("BTPbapObexAuthenticator", "onAuthenticationChallenge: sending request");
        this.mCallback.obtainMessage(105).sendToTarget();
        synchronized(this) {
            while(!this.mReplied) {
                try {
                    android.util.Log.v("BTPbapObexAuthenticator", "onAuthenticationChallenge: waiting for response");
                    (this).wait();
                    continue;
                } catch(InterruptedException ignoredException) {
                }
                android.util.Log.e("BTPbapObexAuthenticator", "Interrupted while waiting for challenge response");
            }
            /*monexit(this)*/;
        }
        String s0 = this.mSessionKey;
        label1: {
            label0: {
                if (s0 == null) {
                    break label0;
                }
                if (this.mSessionKey.length() == 0) {
                    break label0;
                }
                android.util.Log.v("BTPbapObexAuthenticator", new StringBuilder().append("onAuthenticationChallenge: mSessionKey=").append(this.mSessionKey).toString());
                a = new com.navdy.hud.app.bluetooth.obex.PasswordAuthentication((byte[])null, this.mSessionKey.getBytes());
                break label1;
            }
            android.util.Log.v("BTPbapObexAuthenticator", "onAuthenticationChallenge: mSessionKey is empty, timeout/cancel occured");
            a = null;
        }
        return a;
    }
    
    public byte[] onAuthenticationResponse(byte[] a) {
        return null;
    }
    
    public void setReply(String s) {
        synchronized(this) {
            android.util.Log.d("BTPbapObexAuthenticator", new StringBuilder().append("setReply key=").append(s).toString());
            this.mSessionKey = s;
            this.mReplied = true;
            (this).notify();
        }
        /*monexit(this)*/;
    }
}
