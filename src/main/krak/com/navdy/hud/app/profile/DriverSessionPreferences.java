package com.navdy.hud.app.profile;

public class DriverSessionPreferences {
    final private static com.navdy.service.library.log.Logger sLogger;
    private com.squareup.otto.Bus bus;
    private com.navdy.service.library.events.settings.DateTimeConfiguration clockConfiguration;
    private com.navdy.service.library.events.settings.DateTimeConfiguration defaultClockConfiguration;
    private com.navdy.hud.app.maps.NavSessionPreferences navSessionPreferences;
    private com.navdy.hud.app.common.TimeHelper timeHelper;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.profile.DriverSessionPreferences.class);
    }
    
    public DriverSessionPreferences(com.squareup.otto.Bus a, com.navdy.hud.app.profile.DriverProfile a0, com.navdy.hud.app.common.TimeHelper a1) {
        this.bus = a;
        this.timeHelper = a1;
        this.defaultClockConfiguration = new com.navdy.service.library.events.settings.DateTimeConfiguration(Long.valueOf(0L), (String)null, com.navdy.service.library.events.settings.DateTimeConfiguration$Clock.CLOCK_12_HOUR);
        a1.setFormat(com.navdy.service.library.events.settings.DateTimeConfiguration$Clock.CLOCK_12_HOUR);
        this.navSessionPreferences = new com.navdy.hud.app.maps.NavSessionPreferences(a0.getNavigationPreferences());
        this.setClockConfiguration(this.defaultClockConfiguration);
    }
    
    public com.navdy.hud.app.maps.NavSessionPreferences getNavigationSessionPreference() {
        return this.navSessionPreferences;
    }
    
    public void setClockConfiguration(com.navdy.service.library.events.settings.DateTimeConfiguration$Clock a) {
        if (a != null) {
            switch(com.navdy.hud.app.profile.DriverSessionPreferences$1.$SwitchMap$com$navdy$service$library$events$settings$DateTimeConfiguration$Clock[a.ordinal()]) {
                case 2: {
                    this.timeHelper.setFormat(com.navdy.service.library.events.settings.DateTimeConfiguration$Clock.CLOCK_24_HOUR);
                    break;
                }
                case 1: {
                    this.timeHelper.setFormat(com.navdy.service.library.events.settings.DateTimeConfiguration$Clock.CLOCK_12_HOUR);
                    break;
                }
            }
            sLogger.i(new StringBuilder().append("setClockConfiguration:").append(a).toString());
            this.bus.post(com.navdy.hud.app.common.TimeHelper.CLOCK_CHANGED);
        }
    }
    
    public void setClockConfiguration(com.navdy.service.library.events.settings.DateTimeConfiguration a) {
        this.clockConfiguration = a;
        if (a.format != null) {
            this.setClockConfiguration(a.format);
        }
    }
    
    public void setDefault(com.navdy.hud.app.profile.DriverProfile a, boolean b) {
        com.navdy.service.library.events.preferences.LocalPreferences a0 = a.getLocalPreferences();
        label1: {
            label0: {
                if (a0 == null) {
                    break label0;
                }
                if (a0.clockFormat == null) {
                    break label0;
                }
                this.setClockConfiguration(a0.clockFormat);
                break label1;
            }
            if (b) {
                this.setClockConfiguration(this.defaultClockConfiguration);
            }
        }
        this.navSessionPreferences.setDefault(a.getNavigationPreferences());
    }
}
