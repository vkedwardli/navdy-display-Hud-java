package com.navdy.hud.app.profile;

class DriverProfileManager$6 implements Runnable {
    final com.navdy.hud.app.profile.DriverProfileManager this$0;
    final com.navdy.service.library.events.preferences.NavigationPreferencesUpdate val$update;
    
    DriverProfileManager$6(com.navdy.hud.app.profile.DriverProfileManager a, com.navdy.service.library.events.preferences.NavigationPreferencesUpdate a0) {
        super();
        this.this$0 = a;
        this.val$update = a0;
    }
    
    public void run() {
        com.navdy.hud.app.profile.DriverProfileManager.access$500(this.this$0).setNavigationPreferences(this.val$update.preferences);
        com.navdy.hud.app.profile.DriverProfileManager.access$600(this.this$0).setNavigationPreferences(this.val$update.preferences);
        com.navdy.service.library.events.preferences.NavigationPreferences a = com.navdy.hud.app.profile.DriverProfileManager.access$500(this.this$0).getNavigationPreferences();
        com.navdy.hud.app.analytics.AnalyticsSupport.recordNavigationPreferenceChange(com.navdy.hud.app.profile.DriverProfileManager.access$500(this.this$0));
        com.navdy.hud.app.profile.DriverProfileManager.access$700(this.this$0).getNavigationSessionPreference().setDefault(a);
        com.navdy.hud.app.profile.DriverProfileManager.access$200(this.this$0).post(a);
    }
}
