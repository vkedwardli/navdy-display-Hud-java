package com.navdy.hud.app.profile;
import com.navdy.hud.app.R;

public class HudLocale {
    final public static String DEFAULT_LANGUAGE = "en";
    final public static String DEFAULT_LOCALE_ID = "en_US";
    final private static String LOCALE_SEPARATOR = "_";
    final private static String LOCALE_TOAST_ID = "#locale#toast";
    private static String MAP_ENGINE_PROCESS_NAME;
    final private static String SELECTED_LOCALE = "Locale.Helper.Selected.Language";
    final private static String TAG = "[HUD-locale]";
    final private static java.util.HashSet hudLanguages;
    
    static {
        hudLanguages = new java.util.HashSet();
        hudLanguages.add("en");
        hudLanguages.add("fr");
        hudLanguages.add("de");
        hudLanguages.add("it");
        hudLanguages.add("es");
        MAP_ENGINE_PROCESS_NAME = "global.Here.Map.Service.v2";
    }
    
    public HudLocale() {
    }
    
    static void access$000() {
        com.navdy.hud.app.profile.HudLocale.killSelfAndMapEngine();
    }
    
    public static void dismissToast() {
        com.navdy.hud.app.framework.toast.ToastManager.getInstance().dismissToast("#locale#toast");
    }
    
    public static String getBaseLanguage(String s) {
        int i = 0;
        while(true) {
            if (i < s.length()) {
                int i0 = s.charAt(i);
                if (i0 != 95 && i0 != 45) {
                    i = i + 1;
                    continue;
                }
            }
            if (i > 0 && i < s.length()) {
                s = s.substring(0, i);
            }
            return s;
        }
    }
    
    public static java.util.Locale getCurrentLocale(android.content.Context a) {
        java.util.Locale a0 = null;
        try {
            String s = android.preference.PreferenceManager.getDefaultSharedPreferences(a).getString("Locale.Helper.Selected.Language", "en_US");
            if (s.equals("en")) {
                s = "en_US";
            }
            a0 = com.navdy.hud.app.profile.HudLocale.getLocaleForID(s);
        } catch(Throwable a1) {
            android.util.Log.e("[HUD-locale]", "getCurrentLocale", a1);
            a0 = com.navdy.hud.app.profile.HudLocale.getLocaleForID("en_US");
        }
        return a0;
    }
    
    public static java.util.Locale getLocaleForID(String s) {
        java.util.Locale a = null;
        if (android.text.TextUtils.isEmpty((CharSequence)s)) {
            a = null;
        } else {
            int i = s.indexOf("_");
            if (i < 0) {
                a = new java.util.Locale(s);
            } else {
                String s0 = s.substring(0, i);
                String s1 = s.substring(i + 1);
                int i0 = s1.indexOf("_");
                a = (i0 <= 0) ? new java.util.Locale(s0, s1) : new java.util.Locale(s0, s1.substring(0, i0), s1.substring(i0 + 1));
            }
        }
        return a;
    }
    
    public static boolean isLocaleSupported(java.util.Locale a) {
        boolean b = false;
        if (a != null) {
            String s = com.navdy.hud.app.profile.HudLocale.getBaseLanguage(a.getLanguage());
            b = hudLanguages.contains(s);
        } else {
            b = false;
        }
        return b;
    }
    
    private static void killSelfAndMapEngine() {
        Object a = ((android.app.ActivityManager)com.navdy.hud.app.HudApplication.getAppContext().getSystemService("activity")).getRunningAppProcesses().iterator();
        while(true) {
            if (((java.util.Iterator)a).hasNext()) {
                android.app.ActivityManager.RunningAppProcessInfo a0 = (android.app.ActivityManager.RunningAppProcessInfo)((java.util.Iterator)a).next();
                if (!a0.processName.equals(MAP_ENGINE_PROCESS_NAME)) {
                    continue;
                }
                android.os.Process.killProcess(a0.pid);
            }
            System.exit(0);
            return;
        }
    }
    
    public static android.content.Context onAttach(android.content.Context a) {
        return com.navdy.hud.app.profile.HudLocale.setLocale(a, com.navdy.hud.app.profile.HudLocale.getCurrentLocale(a));
    }
    
    private static void setCurrentLocale(android.content.Context a, java.util.Locale a0) {
        android.content.SharedPreferences$Editor a1 = android.preference.PreferenceManager.getDefaultSharedPreferences(a).edit();
        a1.putString("Locale.Helper.Selected.Language", a0.toString());
        a1.commit();
    }
    
    private static android.content.Context setLocale(android.content.Context a, java.util.Locale a0) {
        android.util.Log.e("[HUD-locale]", new StringBuilder().append("setLocale [").append(a0).append("]").toString());
        return (android.os.Build.VERSION.SDK_INT < 24) ? com.navdy.hud.app.profile.HudLocale.updateResourcesLegacy(a, a0) : com.navdy.hud.app.profile.HudLocale.updateResources(a, a0);
    }
    
    public static void showLocaleChangeToast() {
        com.navdy.hud.app.framework.toast.ToastManager a = com.navdy.hud.app.framework.toast.ToastManager.getInstance();
        android.content.res.Resources a0 = com.navdy.hud.app.HudApplication.getAppContext().getResources();
        android.os.Bundle a1 = new android.os.Bundle();
        a.clearAllPendingToast();
        a.disableToasts(false);
        a.dismissCurrentToast("#locale#toast");
        a1.putInt("8", R.drawable.icon_sm_spinner);
        a1.putString("4", a0.getString(R.string.updating_language));
        a1.putInt("5", R.style.Glances_1);
        a.addToast(new com.navdy.hud.app.framework.toast.ToastManager$ToastParams("#locale#toast", a1, (com.navdy.hud.app.framework.toast.IToastCallback)new com.navdy.hud.app.profile.HudLocale$1(a0), true, true));
        new android.os.Handler(android.os.Looper.getMainLooper()).postDelayed((Runnable)new com.navdy.hud.app.profile.HudLocale$2(), 2000L);
    }
    
    public static void showLocaleNotSupportedToast(String s) {
        com.navdy.hud.app.profile.HudLocale.dismissToast();
        android.content.res.Resources a = com.navdy.hud.app.HudApplication.getAppContext().getResources();
        android.os.Bundle a0 = new android.os.Bundle();
        a0.putBoolean("12", true);
        a0.putInt("8", R.drawable.icon_not_supported);
        Object[] a1 = new Object[1];
        a1[0] = s;
        a0.putString("4", a.getString(R.string.locale_not_supported, a1));
        a0.putInt("5", R.style.Glances_1);
        com.navdy.hud.app.framework.toast.ToastManager.getInstance().addToast(new com.navdy.hud.app.framework.toast.ToastManager$ToastParams("#locale#toast", a0, (com.navdy.hud.app.framework.toast.IToastCallback)null, true, true));
    }
    
    public static boolean switchLocale(android.content.Context a, java.util.Locale a0) {
        boolean b = false;
        try {
            com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
            if (com.navdy.hud.app.profile.HudLocale.isLocaleSupported(a0)) {
                java.util.Locale a1 = com.navdy.hud.app.profile.HudLocale.getCurrentLocale(a);
                if (a0.equals(a1)) {
                    android.util.Log.e("[HUD-locale]", new StringBuilder().append("switchLocale language not changed:").append(a0).toString());
                    b = false;
                } else {
                    com.navdy.hud.app.profile.HudLocale.setCurrentLocale(a, a0);
                    android.util.Log.e("[HUD-locale]", new StringBuilder().append("switchLocale changed language from [").append(a1).append("] to [").append(a0).append("], restarting hud app").toString());
                    b = true;
                }
            } else {
                android.util.Log.e("[HUD-locale]", new StringBuilder().append("switchLocale hud does not support locale:").append(a0).toString());
                b = false;
            }
        } catch(Throwable a2) {
            android.util.Log.e("[HUD-locale]", "switchLocale", a2);
            b = false;
        }
        return b;
    }
    
    private static android.content.Context updateResources(android.content.Context a, java.util.Locale a0) {
        java.util.Locale.setDefault(a0);
        android.content.res.Configuration a1 = a.getResources().getConfiguration();
        a1.setLocale(a0);
        return a.createConfigurationContext(a1);
    }
    
    private static android.content.Context updateResourcesLegacy(android.content.Context a, java.util.Locale a0) {
        java.util.Locale.setDefault(a0);
        android.content.res.Resources a1 = a.getResources();
        android.content.res.Configuration a2 = a1.getConfiguration();
        a2.locale = a0;
        a1.updateConfiguration(a2, a1.getDisplayMetrics());
        return a;
    }
}
