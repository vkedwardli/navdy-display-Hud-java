package com.navdy.hud.app.profile;

public class DriverProfileManager {
    final private static String DEFAULT_PROFILE_NAME = "DefaultProfile";
    final private static com.navdy.hud.app.event.DriverProfileChanged DRIVER_PROFILE_CHANGED;
    final private static String LOCAL_PREFERENCES_SUFFIX = "_preferences";
    final private static com.navdy.service.library.device.NavdyDeviceId MOCK_PROFILE_DEVICE_ID;
    final private static com.navdy.service.library.log.Logger sLogger;
    private String lastUserEmail;
    private String lastUserName;
    private java.util.HashMap mAllProfiles;
    private com.squareup.otto.Bus mBus;
    private com.navdy.hud.app.profile.DriverProfile mCurrentProfile;
    private String mProfileDirectoryName;
    private java.util.Set mPublicProfiles;
    private com.navdy.hud.app.profile.DriverSessionPreferences mSessionPrefs;
    private com.navdy.hud.app.profile.DriverProfile theDefaultProfile;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.profile.DriverProfileManager.class);
        MOCK_PROFILE_DEVICE_ID = com.navdy.service.library.device.NavdyDeviceId.UNKNOWN_ID;
        DRIVER_PROFILE_CHANGED = new com.navdy.hud.app.event.DriverProfileChanged();
    }
    
    public DriverProfileManager(com.squareup.otto.Bus a, com.navdy.hud.app.storage.PathManager a0, com.navdy.hud.app.common.TimeHelper a1) {
        this.mAllProfiles = new java.util.HashMap();
        this.mPublicProfiles = (java.util.Set)new java.util.HashSet();
        this.mProfileDirectoryName = a0.getDriverProfilesDir();
        this.mBus = a;
        this.mBus.register(this);
        sLogger.i(new StringBuilder().append("initializing in ").append(this.mProfileDirectoryName).toString());
        java.io.File[] a2 = new java.io.File(this.mProfileDirectoryName).listFiles((java.io.FileFilter)new com.navdy.hud.app.profile.DriverProfileManager$1(this));
        int i = a2.length;
        int i0 = 0;
        while(true) {
            label1: {
                Exception a3 = null;
                if (i0 >= i) {
                    break label1;
                }
                java.io.File a4 = a2[i0];
                label0: {
                    com.navdy.hud.app.profile.DriverProfile a5 = null;
                    try {
                        a5 = new com.navdy.hud.app.profile.DriverProfile(a4);
                    } catch(Exception a6) {
                        a3 = a6;
                        break label0;
                    }
                    String s = a4.getName();
                    this.mAllProfiles.put(s, a5);
                    if (a5.isProfilePublic()) {
                        this.mPublicProfiles.add(a5);
                    }
                    if (s.equals(this.profileNameForId(MOCK_PROFILE_DEVICE_ID))) {
                        this.theDefaultProfile = a5;
                    }
                    i0 = i0 + 1;
                    continue;
                }
                sLogger.e(new StringBuilder().append("could not load profile from ").append(a4).toString(), (Throwable)a3);
            }
            if (this.theDefaultProfile == null) {
                this.theDefaultProfile = this.createProfileForId(MOCK_PROFILE_DEVICE_ID);
            }
            this.theDefaultProfile.getNavigationPreferences();
            this.mSessionPrefs = new com.navdy.hud.app.profile.DriverSessionPreferences(this.mBus, this.theDefaultProfile, a1);
            this.setCurrentProfile(this.theDefaultProfile);
            return;
        }
    }
    
    static com.navdy.service.library.log.Logger access$000() {
        return sLogger;
    }
    
    static void access$100(com.navdy.hud.app.profile.DriverProfileManager a, boolean b) {
        a.changeLocale(b);
    }
    
    static com.squareup.otto.Bus access$200(com.navdy.hud.app.profile.DriverProfileManager a) {
        return a.mBus;
    }
    
    static void access$300(com.navdy.hud.app.profile.DriverProfileManager a, String s) {
        a.refreshProfileImageIfNeeded(s);
    }
    
    static com.navdy.service.library.events.preferences.DriverProfilePreferences access$400(com.navdy.hud.app.profile.DriverProfileManager a, com.navdy.service.library.events.preferences.DriverProfilePreferences a0) {
        return a.findSupportedLocale(a0);
    }
    
    static com.navdy.hud.app.profile.DriverProfile access$500(com.navdy.hud.app.profile.DriverProfileManager a) {
        return a.mCurrentProfile;
    }
    
    static com.navdy.hud.app.profile.DriverProfile access$600(com.navdy.hud.app.profile.DriverProfileManager a) {
        return a.theDefaultProfile;
    }
    
    static com.navdy.hud.app.profile.DriverSessionPreferences access$700(com.navdy.hud.app.profile.DriverProfileManager a) {
        return a.mSessionPrefs;
    }
    
    private void changeLocale(boolean b) {
        try {
            java.util.Locale a = this.getCurrentLocale();
            sLogger.v(new StringBuilder().append("[HUD-locale] changeLocale current=").append(a).toString());
            if (com.navdy.hud.app.profile.HudLocale.switchLocale(com.navdy.hud.app.HudApplication.getAppContext(), a)) {
                sLogger.v("[HUD-locale] change, restarting...");
                this.mBus.post(new com.navdy.hud.app.event.InitEvents$InitPhase(com.navdy.hud.app.event.InitEvents$Phase.SWITCHING_LOCALE));
                com.navdy.hud.app.profile.HudLocale.showLocaleChangeToast();
            } else {
                sLogger.v("[HUD-locale] not changed");
                this.mBus.post(new com.navdy.hud.app.event.InitEvents$InitPhase(com.navdy.hud.app.event.InitEvents$Phase.LOCALE_UP_TO_DATE));
                if (com.navdy.hud.app.profile.HudLocale.isLocaleSupported(a)) {
                    com.navdy.hud.app.profile.HudLocale.dismissToast();
                } else if (b) {
                    com.navdy.hud.app.profile.HudLocale.showLocaleNotSupportedToast(a.getDisplayLanguage());
                }
            }
        } catch(Throwable a0) {
            sLogger.e("[HUD-locale] changeLocale", a0);
        }
    }
    
    private void copyCurrentProfileToDefault() {
        if (this.mCurrentProfile != this.theDefaultProfile) {
            com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.profile.DriverProfileManager$12(this), 10);
        }
    }
    
    private com.navdy.service.library.events.preferences.DriverProfilePreferences findSupportedLocale(com.navdy.service.library.events.preferences.DriverProfilePreferences a) {
        if (!com.navdy.hud.app.profile.HudLocale.isLocaleSupported(com.navdy.hud.app.profile.HudLocale.getLocaleForID(a.locale)) && a.additionalLocales != null) {
            java.util.Locale a0 = null;
            Object a1 = a.additionalLocales.iterator();
            while(true) {
                boolean b = ((java.util.Iterator)a1).hasNext();
                a0 = null;
                if (!b) {
                    break;
                }
                String s = (String)((java.util.Iterator)a1).next();
                {
                    if (android.text.TextUtils.isEmpty((CharSequence)s)) {
                        continue;
                    }
                    a0 = com.navdy.hud.app.profile.HudLocale.getLocaleForID(s);
                    if (!com.navdy.hud.app.profile.HudLocale.isLocaleSupported(a0)) {
                        sLogger.v(new StringBuilder().append("HUD-locale additional locale not supported:").append(a0).toString());
                        continue;
                    }
                    sLogger.v(new StringBuilder().append("HUD-locale additional locale supported:").append(a0).toString());
                    break;
                }
            }
            if (a0 != null) {
                a = new com.navdy.service.library.events.preferences.DriverProfilePreferences$Builder(a).locale(a0.toString()).build();
            }
        }
        return a;
    }
    
    private void handleUpdateInBackground(com.squareup.wire.Message a, com.navdy.service.library.events.RequestStatus a0, com.squareup.wire.Message a1, Runnable a2) {
        if (this.mCurrentProfile != this.theDefaultProfile) {
            String s = (a).getClass().getSimpleName();
            switch(com.navdy.hud.app.profile.DriverProfileManager$13.$SwitchMap$com$navdy$service$library$events$RequestStatus[a0.ordinal()]) {
                case 2: {
                    sLogger.i(new StringBuilder().append("Received ").append(s).append(", applying").toString());
                    if (this.mCurrentProfile == null) {
                        break;
                    }
                    if (a1 == null) {
                        sLogger.i(new StringBuilder().append(s).append(" with no preferences!").toString());
                        break;
                    } else {
                        com.navdy.service.library.task.TaskManager.getInstance().execute(a2, 10);
                        break;
                    }
                }
                case 1: {
                    sLogger.i(new StringBuilder().append(s).append(" returned up to date!").toString());
                    break;
                }
                default: {
                    sLogger.i(new StringBuilder().append("profile status for ").append(s).append(" was ").append(a0).append(", which I didn't expect; ignoring.").toString());
                }
            }
        }
    }
    
    private String profileNameForId(com.navdy.service.library.device.NavdyDeviceId a) {
        String s = a.getBluetoothAddress();
        if (s == null) {
            s = "DefaultProfile";
        }
        return com.navdy.hud.app.util.GenericUtil.normalizeToFilename(s);
    }
    
    private void refreshProfileImageIfNeeded(String s) {
        com.navdy.hud.app.framework.contacts.PhoneImageDownloader a = com.navdy.hud.app.framework.contacts.PhoneImageDownloader.getInstance();
        String s0 = a.hashFor("DriverImage", com.navdy.service.library.events.photo.PhotoType.PHOTO_DRIVER_PROFILE);
        if (!java.util.Objects.equals(s, s0)) {
            sLogger.d(new StringBuilder().append("asking for new photo - local:").append(s0).append(" remote:").append(s).toString());
            a.submitDownload("DriverImage", com.navdy.hud.app.framework.contacts.PhoneImageDownloader$Priority.NORMAL, com.navdy.service.library.events.photo.PhotoType.PHOTO_DRIVER_PROFILE, (String)null);
        }
    }
    
    private void requestPreferenceUpdates(com.navdy.hud.app.profile.DriverProfile a) {
        sLogger.v(new StringBuilder().append("requestPreferenceUpdates:").append(a.getProfileName()).toString());
        this.sendEventToClient((com.squareup.wire.Message)new com.navdy.service.library.events.preferences.DriverProfilePreferencesRequest$Builder().serial_number(a.mDriverProfilePreferences.serial_number).build());
        sLogger.v(new StringBuilder().append("requestPreferenceUpdates: nav:").append(a.mNavigationPreferences.serial_number).toString());
        this.sendEventToClient((com.squareup.wire.Message)new com.navdy.service.library.events.preferences.NavigationPreferencesRequest$Builder().serial_number(a.mNavigationPreferences.serial_number).build());
        sLogger.v(new StringBuilder().append("requestPreferenceUpdates: notif:").append(a.mNotificationPreferences.serial_number).toString());
        this.sendEventToClient((com.squareup.wire.Message)new com.navdy.service.library.events.preferences.NotificationPreferencesRequest$Builder().serial_number(a.mNotificationPreferences.serial_number).build());
        sLogger.v(new StringBuilder().append("requestPreferenceUpdates: input:").append(a.mInputPreferences.serial_number).toString());
        this.sendEventToClient((com.squareup.wire.Message)new com.navdy.service.library.events.preferences.InputPreferencesRequest$Builder().serial_number(a.mInputPreferences.serial_number).build());
    }
    
    private void requestUpdates(com.navdy.hud.app.profile.DriverProfile a) {
        sLogger.v(new StringBuilder().append("requestUpdates(canned-msgs) [").append(a.mCannedMessages.serial_number).append("]").toString());
        this.sendEventToClient((com.squareup.wire.Message)new com.navdy.service.library.events.glances.CannedMessagesRequest$Builder().serial_number(a.mCannedMessages.serial_number).build());
    }
    
    private void sendEventToClient(com.squareup.wire.Message a) {
        this.mBus.post(new com.navdy.hud.app.event.RemoteEvent(a));
    }
    
    public com.navdy.hud.app.profile.DriverProfile createProfileForId(com.navdy.service.library.device.NavdyDeviceId a) {
        com.navdy.hud.app.profile.DriverProfile a0 = null;
        String s = this.profileNameForId(a);
        label1: {
            java.io.IOException a1 = null;
            label0: {
                try {
                    a0 = com.navdy.hud.app.profile.DriverProfile.createProfileForId(s, this.mProfileDirectoryName);
                } catch(java.io.IOException a2) {
                    a1 = a2;
                    break label0;
                }
                this.mAllProfiles.put(s, a0);
                break label1;
            }
            sLogger.e(new StringBuilder().append("could not create new profile: ").append(a1).toString());
            a0 = null;
        }
        return a0;
    }
    
    public void disableTraffic() {
        this.mCurrentProfile.setTrafficEnabled(false);
        this.theDefaultProfile.setTrafficEnabled(false);
    }
    
    public void enableTraffic() {
        this.mCurrentProfile.setTrafficEnabled(true);
        this.theDefaultProfile.setTrafficEnabled(true);
    }
    
    public java.util.Locale getCurrentLocale() {
        return this.mCurrentProfile.getLocale();
    }
    
    public com.navdy.hud.app.profile.DriverProfile getCurrentProfile() {
        return this.mCurrentProfile;
    }
    
    public String getLastUserEmail() {
        return this.lastUserEmail;
    }
    
    public String getLastUserName() {
        return this.lastUserName;
    }
    
    public com.navdy.service.library.events.preferences.LocalPreferences getLocalPreferences() {
        return this.mCurrentProfile.getLocalPreferences();
    }
    
    public android.content.SharedPreferences getLocalPreferencesForCurrentDriverProfile(android.content.Context a) {
        return this.getLocalPreferencesForDriverProfile(a, this.getCurrentProfile());
    }
    
    public android.content.SharedPreferences getLocalPreferencesForDriverProfile(android.content.Context a, com.navdy.hud.app.profile.DriverProfile a0) {
        return a.getSharedPreferences(new StringBuilder().append(a0.getProfileName()).append("_preferences").toString(), 0);
    }
    
    public com.navdy.hud.app.profile.DriverProfile getProfileForId(com.navdy.service.library.device.NavdyDeviceId a) {
        String s = this.profileNameForId(a);
        return (com.navdy.hud.app.profile.DriverProfile)this.mAllProfiles.get(s);
    }
    
    public java.util.Collection getPublicProfiles() {
        return (java.util.Collection)this.mPublicProfiles;
    }
    
    public com.navdy.hud.app.profile.DriverSessionPreferences getSessionPreferences() {
        return this.mSessionPrefs;
    }
    
    boolean isDefaultProfile(com.navdy.hud.app.profile.DriverProfile a) {
        return a == this.theDefaultProfile;
    }
    
    public boolean isManualZoom() {
        boolean b = false;
        com.navdy.service.library.events.preferences.LocalPreferences a = this.getLocalPreferences();
        label2: {
            label0: {
                label1: {
                    if (a == null) {
                        break label1;
                    }
                    if (Boolean.TRUE.equals(a.manualZoom)) {
                        break label0;
                    }
                }
                b = false;
                break label2;
            }
            b = true;
        }
        return b;
    }
    
    public boolean isTrafficEnabled() {
        return this.mCurrentProfile.isTrafficEnabled();
    }
    
    public void loadProfileForId(com.navdy.service.library.device.NavdyDeviceId a) {
        com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.profile.DriverProfileManager$2(this, a), 10);
    }
    
    public void onCannedMessagesUpdate(com.navdy.service.library.events.glances.CannedMessagesUpdate a) {
        this.handleUpdateInBackground((com.squareup.wire.Message)a, a.status, (com.squareup.wire.Message)a, (Runnable)new com.navdy.hud.app.profile.DriverProfileManager$10(this, a));
    }
    
    public void onDeviceSyncRequired(com.navdy.hud.app.service.ConnectionHandler$DeviceSyncEvent a) {
        sLogger.v("calling requestPreferenceUpdates");
        this.requestPreferenceUpdates(this.mCurrentProfile);
        sLogger.v("calling requestUpdates");
        this.requestUpdates(this.mCurrentProfile);
    }
    
    public void onDisplaySpeakerPreferencesUpdate(com.navdy.service.library.events.preferences.DisplaySpeakerPreferencesUpdate a) {
        this.handleUpdateInBackground((com.squareup.wire.Message)a, a.status, (com.squareup.wire.Message)a.preferences, (Runnable)new com.navdy.hud.app.profile.DriverProfileManager$8(this, a));
    }
    
    public void onDriverProfileChange(com.navdy.hud.app.event.DriverProfileChanged a) {
        this.mBus.post(this.mCurrentProfile.getNavigationPreferences());
    }
    
    public void onDriverProfilePreferencesUpdate(com.navdy.service.library.events.preferences.DriverProfilePreferencesUpdate a) {
        if (this.mCurrentProfile != this.theDefaultProfile) {
            switch(com.navdy.hud.app.profile.DriverProfileManager$13.$SwitchMap$com$navdy$service$library$events$RequestStatus[a.status.ordinal()]) {
                case 2: {
                    sLogger.i("Received profile update, applying");
                    if (this.mCurrentProfile == null) {
                        break;
                    }
                    com.navdy.service.library.events.preferences.DriverProfilePreferences a0 = a.preferences;
                    if (a0 == null) {
                        sLogger.i("preferences update with no preferences!");
                        break;
                    } else {
                        com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.profile.DriverProfileManager$4(this, a0), 10);
                        if (((Boolean)com.squareup.wire.Wire.get(a0.profile_is_public, com.navdy.service.library.events.preferences.DriverProfilePreferences.DEFAULT_PROFILE_IS_PUBLIC)).booleanValue()) {
                            this.mPublicProfiles.add(this.mCurrentProfile);
                        } else {
                            this.mPublicProfiles.remove(this.mCurrentProfile);
                        }
                        com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.profile.DriverProfileManager$5(this, a0), 10);
                        break;
                    }
                }
                case 1: {
                    sLogger.i("prefs are up to date!");
                    com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.profile.DriverProfileManager$3(this), 10);
                    break;
                }
                default: {
                    sLogger.i(new StringBuilder().append("profile status was ").append(a.status).append(", which I didn't expect; ignoring.").toString());
                }
            }
        }
    }
    
    public void onInputPreferencesUpdate(com.navdy.service.library.events.preferences.InputPreferencesUpdate a) {
        this.handleUpdateInBackground((com.squareup.wire.Message)a, a.status, (com.squareup.wire.Message)a.preferences, (Runnable)new com.navdy.hud.app.profile.DriverProfileManager$7(this, a));
    }
    
    public void onNavigationPreferencesUpdate(com.navdy.service.library.events.preferences.NavigationPreferencesUpdate a) {
        this.handleUpdateInBackground((com.squareup.wire.Message)a, a.status, (com.squareup.wire.Message)a.preferences, (Runnable)new com.navdy.hud.app.profile.DriverProfileManager$6(this, a));
    }
    
    public void onNotificationPreferencesUpdate(com.navdy.service.library.events.preferences.NotificationPreferencesUpdate a) {
        this.handleUpdateInBackground((com.squareup.wire.Message)a, a.status, (com.squareup.wire.Message)a.preferences, (Runnable)new com.navdy.hud.app.profile.DriverProfileManager$9(this, a));
    }
    
    public void onPhotoDownload(com.navdy.hud.app.framework.contacts.PhoneImageDownloader$PhotoDownloadStatus a) {
        if (a.photoType == com.navdy.service.library.events.photo.PhotoType.PHOTO_DRIVER_PROFILE && !a.alreadyDownloaded && this.mCurrentProfile != this.theDefaultProfile) {
            if (a.success) {
                sLogger.d("applying new photo");
            } else {
                sLogger.i("driver photo not downloaded");
            }
        }
    }
    
    public void setCurrentProfile(com.navdy.hud.app.profile.DriverProfile a) {
        sLogger.i(new StringBuilder().append("setting current profile to ").append(a).toString());
        if (a == null) {
            a = this.theDefaultProfile;
        }
        if (this.mCurrentProfile != a) {
            this.mCurrentProfile = a;
            this.mSessionPrefs.setDefault(this.mCurrentProfile, this.isDefaultProfile(this.mCurrentProfile));
            this.theDefaultProfile.setTrafficEnabled(this.mCurrentProfile.isTrafficEnabled());
            this.copyCurrentProfileToDefault();
            this.mBus.post(DRIVER_PROFILE_CHANGED);
        }
        this.lastUserName = this.mCurrentProfile.getDriverName();
        this.lastUserEmail = this.mCurrentProfile.getDriverEmail();
        sLogger.v(new StringBuilder().append("lastUserEmail:").append(this.lastUserEmail).toString());
    }
    
    public void updateLocalPreferences(com.navdy.service.library.events.preferences.LocalPreferences a) {
        if (this.mCurrentProfile != null) {
            com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.profile.DriverProfileManager$11(this, a), 10);
        }
    }
}
