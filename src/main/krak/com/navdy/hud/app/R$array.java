package com.navdy.hud.app;
import com.navdy.hud.app.R;

final public class R$array {
    final public static int compass_points = R.array.compass_points;
    final public static int download_app_1 = R.array.download_app_1;
    final public static int download_app_2 = R.array.download_app_2;
    final public static int download_app_icon_app_store = R.array.download_app_icon_app_store;
    final public static int download_app_icon_play_store = R.array.download_app_icon_play_store;
    final public static int drive_state_colors = R.array.drive_state_colors;
    final public static int gesture_small_tips = R.array.gesture_small_tips;
    final public static int gesture_tips = R.array.gesture_tips;
    final public static int gforce_colors = R.array.gforce_colors;
    final public static int smart_dash_engine_temperature_state_colors = R.array.smart_dash_engine_temperature_state_colors;
    final public static int smart_dash_fuel_gauge2_state_colors = R.array.smart_dash_fuel_gauge2_state_colors;
    final public static int smart_dash_fuel_gauge_state_colors = R.array.smart_dash_fuel_gauge_state_colors;
    final public static int smart_dash_heading_gauge_colors = R.array.smart_dash_heading_gauge_colors;
    final public static int smart_dash_music_gauge_gradient = R.array.smart_dash_music_gauge_gradient;
    final public static int smart_dash_speedo_meter_color_table = R.array.smart_dash_speedo_meter_color_table;
    final public static int smart_dash_state_colors = R.array.smart_dash_state_colors;
    final public static int time_labels = R.array.time_labels;
    final public static int time_labels_short = R.array.time_labels_short;
    final public static int title_sizes = R.array.title_sizes;
    final public static int voice_search_tips = R.array.voice_search_tips;
    final public static int voice_search_tips_icons = R.array.voice_search_tips_icons;
    
    public R$array() {
    }
}
