package com.navdy.hud.app;

class IEventListener$Stub$Proxy implements com.navdy.hud.app.IEventListener {
    private android.os.IBinder mRemote;
    
    IEventListener$Stub$Proxy(android.os.IBinder a) {
        this.mRemote = a;
    }
    
    public android.os.IBinder asBinder() {
        return this.mRemote;
    }
    
    public String getInterfaceDescriptor() {
        return "com.navdy.hud.app.IEventListener";
    }
    
    public void onEvent(byte[] a) {
        android.os.Parcel a0 = android.os.Parcel.obtain();
        try {
            a0.writeInterfaceToken("com.navdy.hud.app.IEventListener");
            a0.writeByteArray(a);
            this.mRemote.transact(1, a0, (android.os.Parcel)null, 1);
        } catch(Throwable a1) {
            a0.recycle();
            throw a1;
        }
        a0.recycle();
    }
}
