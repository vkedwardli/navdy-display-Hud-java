package com.navdy.hud.app;

abstract public class IEventListener$Stub extends android.os.Binder implements com.navdy.hud.app.IEventListener {
    final private static String DESCRIPTOR = "com.navdy.hud.app.IEventListener";
    final static int TRANSACTION_onEvent = 1;
    
    public IEventListener$Stub() {
        this.attachInterface((android.os.IInterface)this, "com.navdy.hud.app.IEventListener");
    }
    
    public static com.navdy.hud.app.IEventListener asInterface(android.os.IBinder a) {
        Object a0 = null;
        label1: if (a != null) {
            android.os.IInterface a1 = a.queryLocalInterface("com.navdy.hud.app.IEventListener");
            label0: {
                if (a1 == null) {
                    break label0;
                }
                if (!(a1 instanceof com.navdy.hud.app.IEventListener)) {
                    break label0;
                }
                a0 = a1;
                break label1;
            }
            a0 = new com.navdy.hud.app.IEventListener$Stub$Proxy(a);
        } else {
            a0 = null;
        }
        return (com.navdy.hud.app.IEventListener)a0;
    }
    
    public android.os.IBinder asBinder() {
        return (android.os.IBinder)this;
    }
    
    public boolean onTransact(int i, android.os.Parcel a, android.os.Parcel a0, int i0) {
        boolean b = false;
        switch(i) {
            case 1598968902: {
                a0.writeString("com.navdy.hud.app.IEventListener");
                b = true;
                break;
            }
            case 1: {
                a.enforceInterface("com.navdy.hud.app.IEventListener");
                this.onEvent(a.createByteArray());
                b = true;
                break;
            }
            default: {
                b = super.onTransact(i, a, a0, i0);
            }
        }
        return b;
    }
}
