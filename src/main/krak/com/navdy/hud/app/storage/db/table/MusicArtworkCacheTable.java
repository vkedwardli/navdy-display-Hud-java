package com.navdy.hud.app.storage.db.table;

public class MusicArtworkCacheTable {
    final public static String ALBUM = "album";
    final public static String AUTHOR = "author";
    final public static String FILE_NAME = "file_name";
    final public static String NAME = "name";
    final public static String TABLE_NAME = "music_artwork_cache";
    final public static String TYPE = "type";
    final public static String TYPE_AUDIOBOOK = "audiobook";
    final public static String TYPE_MUSIC = "music";
    final public static String TYPE_PLAYLIST = "playlist";
    final public static String TYPE_PODCAST = "podcast";
    final private static String[] VALID_TYPES;
    final private static com.navdy.service.library.log.Logger sLogger;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.storage.db.table.MusicArtworkCacheTable.class);
        String[] a = new String[4];
        a[0] = "music";
        a[1] = "playlist";
        a[2] = "podcast";
        a[3] = "audiobook";
        VALID_TYPES = a;
    }
    
    public MusicArtworkCacheTable() {
    }
    
    public static void createTable(android.database.sqlite.SQLiteDatabase a) {
        a.execSQL(new StringBuilder().append("CREATE TABLE IF NOT EXISTS ").append("music_artwork_cache").append(" (").append("type").append(" TEXT NOT NULL CHECK(").append("type").append(" IN ").append(com.navdy.hud.app.storage.db.table.MusicArtworkCacheTable.validTypes()).append("), ").append("author").append(" TEXT, ").append("album").append(" TEXT, ").append("name").append(" TEXT, ").append("file_name").append(" TEXT NOT NULL, UNIQUE (").append("type").append(", ").append("author").append(", ").append("album").append(", ").append("name").append(") ON CONFLICT REPLACE);").toString());
        com.navdy.hud.app.storage.db.DatabaseUtil.createIndex(a, "music_artwork_cache", "type", sLogger);
        com.navdy.hud.app.storage.db.DatabaseUtil.createIndex(a, "music_artwork_cache", "album", sLogger);
        com.navdy.hud.app.storage.db.DatabaseUtil.createIndex(a, "music_artwork_cache", "author", sLogger);
        com.navdy.hud.app.storage.db.DatabaseUtil.createIndex(a, "music_artwork_cache", "name", sLogger);
    }
    
    private static String validTypes() {
        StringBuilder a = new StringBuilder();
        a.append("('");
        a.append(VALID_TYPES[0]);
        int i = 1;
        while(i < VALID_TYPES.length) {
            a.append("', '");
            a.append(VALID_TYPES[i]);
            i = i + 1;
        }
        a.append("')");
        return a.toString();
    }
}
