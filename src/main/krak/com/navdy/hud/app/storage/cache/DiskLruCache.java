package com.navdy.hud.app.storage.cache;

public class DiskLruCache {
    private android.content.Context appContext;
    private String cacheName;
    private com.navdy.service.library.log.Logger logger;
    private com.navdy.hud.app.storage.cache.DiskLruCache$MemoryLruCache lruCache;
    private int maxSize;
    private java.io.File rootPath;
    private com.navdy.service.library.task.TaskManager taskManager;
    
    public DiskLruCache(String s, String s0, int i) {
        this.logger = new com.navdy.service.library.log.Logger(new StringBuilder().append("DLC-").append(s).toString());
        this.cacheName = s;
        this.rootPath = new java.io.File(s0);
        this.rootPath.mkdirs();
        this.maxSize = i;
        this.lruCache = new com.navdy.hud.app.storage.cache.DiskLruCache$MemoryLruCache(this, i);
        this.taskManager = com.navdy.service.library.task.TaskManager.getInstance();
        this.appContext = com.navdy.hud.app.HudApplication.getAppContext();
        this.init();
    }
    
    static void access$000(com.navdy.hud.app.storage.cache.DiskLruCache a, com.navdy.hud.app.storage.cache.DiskLruCache$FileEntry a0) {
        a.removeFile(a0);
    }
    
    static String access$100(com.navdy.hud.app.storage.cache.DiskLruCache a, String s) {
        return a.getFilePath(s);
    }
    
    static com.navdy.service.library.log.Logger access$200(com.navdy.hud.app.storage.cache.DiskLruCache a) {
        return a.logger;
    }
    
    static android.content.Context access$300(com.navdy.hud.app.storage.cache.DiskLruCache a) {
        return a.appContext;
    }
    
    private void addFile(String s, byte[] a) {
        com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.storage.cache.DiskLruCache$2(this, s, a), 22);
    }
    
    private String getFilePath(String s) {
        return new StringBuilder().append(this.rootPath).append(java.io.File.separator).append(s).toString();
    }
    
    private void init() {
        this.logger.v("init start");
        java.io.File[] a = this.rootPath.listFiles();
        if (a != null) {
            java.util.ArrayList a0 = new java.util.ArrayList();
            int i = 0;
            while(i < a.length) {
                if (a[i].isFile()) {
                    a0.add(a[i]);
                }
                i = i + 1;
            }
            int i0 = a0.size();
            this.logger.v(new StringBuilder().append("init: files in cache[").append(i0).append("]").toString());
            if (i0 != 0) {
                java.util.Collections.sort((java.util.List)a0, (java.util.Comparator)new com.navdy.hud.app.storage.cache.DiskLruCache$1(this));
                Object a1 = a0.iterator();
                while(((java.util.Iterator)a1).hasNext()) {
                    java.io.File a2 = (java.io.File)((java.util.Iterator)a1).next();
                    String s = a2.getName();
                    int i1 = (int)a2.length();
                    this.lruCache.put(s, new com.navdy.hud.app.storage.cache.DiskLruCache$FileEntry(s, i1));
                }
                this.logger.v("init complete");
            } else {
                this.logger.v("init: cache empty");
            }
        }
    }
    
    private void removeFile(com.navdy.hud.app.storage.cache.DiskLruCache$FileEntry a) {
        com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.storage.cache.DiskLruCache$3(this, a), 22);
    }
    
    public void clear() {
        synchronized(this) {
            this.lruCache.evictAll();
        }
        /*monexit(this)*/;
    }
    
    public boolean contains(String s) {
        com.navdy.hud.app.storage.cache.DiskLruCache$FileEntry a = null;
        synchronized(this) {
            a = (com.navdy.hud.app.storage.cache.DiskLruCache$FileEntry)this.lruCache.get(s);
        }
        boolean b = a != null;
        /*monexit(this)*/;
        return b;
    }
    
    public byte[] get(String s) {
        byte[] a = null;
        label0: synchronized(this) {
            Throwable a0 = null;
            com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
            com.navdy.hud.app.storage.cache.DiskLruCache$FileEntry a1 = (com.navdy.hud.app.storage.cache.DiskLruCache$FileEntry)this.lruCache.get(s);
            label1: if (a1 != null) {
                this.updateFile(a1);
                try {
                    a = com.navdy.service.library.util.IOUtils.readBinaryFile(this.getFilePath(s));
                } catch(Throwable a2) {
                    a0 = a2;
                    break label1;
                }
                break label0;
            } else {
                a = null;
                break label0;
            }
            this.logger.e(a0);
            a = null;
        }
        /*monexit(this)*/;
        return a;
    }
    
    public void put(String s, byte[] a) {
        synchronized(this) {
            com.navdy.hud.app.storage.cache.DiskLruCache$FileEntry a0 = new com.navdy.hud.app.storage.cache.DiskLruCache$FileEntry(s, a.length);
            this.lruCache.put(s, a0);
            if (!a0.removed) {
                this.addFile(s, a);
            }
        }
        /*monexit(this)*/;
    }
    
    public void remove(String s) {
        synchronized(this) {
            this.lruCache.remove(s);
        }
        /*monexit(this)*/;
    }
    
    public void updateFile(com.navdy.hud.app.storage.cache.DiskLruCache$FileEntry a) {
        com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.storage.cache.DiskLruCache$4(this, a), 22);
    }
}
