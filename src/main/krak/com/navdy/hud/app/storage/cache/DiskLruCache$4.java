package com.navdy.hud.app.storage.cache;

class DiskLruCache$4 implements Runnable {
    final com.navdy.hud.app.storage.cache.DiskLruCache this$0;
    final com.navdy.hud.app.storage.cache.DiskLruCache$FileEntry val$entry;
    
    DiskLruCache$4(com.navdy.hud.app.storage.cache.DiskLruCache a, com.navdy.hud.app.storage.cache.DiskLruCache$FileEntry a0) {
        super();
        this.this$0 = a;
        this.val$entry = a0;
    }
    
    public void run() {
        java.io.File a = new java.io.File(com.navdy.hud.app.storage.cache.DiskLruCache.access$100(this.this$0, this.val$entry.name));
        if (!a.setLastModified(System.currentTimeMillis())) {
            com.navdy.hud.app.storage.cache.DiskLruCache.access$200(this.this$0).v(new StringBuilder().append("file time not changed [").append(a.getAbsolutePath()).append("]").toString());
        }
    }
}
