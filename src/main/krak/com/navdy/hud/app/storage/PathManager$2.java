package com.navdy.hud.app.storage;

class PathManager$2 {
    final static int[] $SwitchMap$com$navdy$service$library$events$file$FileType;
    
    static {
        $SwitchMap$com$navdy$service$library$events$file$FileType = new int[com.navdy.service.library.events.file.FileType.values().length];
        int[] a = $SwitchMap$com$navdy$service$library$events$file$FileType;
        com.navdy.service.library.events.file.FileType a0 = com.navdy.service.library.events.file.FileType.FILE_TYPE_OTA;
        try {
            a[a0.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$file$FileType[com.navdy.service.library.events.file.FileType.FILE_TYPE_LOGS.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException0) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$file$FileType[com.navdy.service.library.events.file.FileType.FILE_TYPE_PERF_TEST.ordinal()] = 3;
        } catch(NoSuchFieldError ignoredException1) {
        }
    }
}
