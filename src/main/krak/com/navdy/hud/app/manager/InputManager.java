package com.navdy.hud.app.manager;

public class InputManager {
    final private static com.navdy.service.library.log.Logger sLogger;
    private com.squareup.otto.Bus bus;
    private com.navdy.hud.app.manager.InputManager$IInputHandler defaultHandler;
    private com.navdy.hud.app.device.dial.DialManager dialManager;
    private android.os.Handler handler;
    private com.navdy.hud.app.manager.InputManager$IInputHandler inputHandler;
    private boolean isLongPressDetected;
    private long lastInputEventTime;
    private com.navdy.hud.app.device.PowerManager powerManager;
    private com.navdy.hud.app.ui.framework.UIStateManager uiStateManager;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.manager.InputManager.class);
    }
    
    public InputManager(com.squareup.otto.Bus a, com.navdy.hud.app.device.PowerManager a0, com.navdy.hud.app.ui.framework.UIStateManager a1) {
        this.lastInputEventTime = 0L;
        this.handler = new android.os.Handler(android.os.Looper.getMainLooper());
        this.powerManager = a0;
        this.bus = a;
        this.bus.register(this);
        this.uiStateManager = a1;
    }
    
    static com.navdy.service.library.log.Logger access$000() {
        return sLogger;
    }
    
    static boolean access$100(com.navdy.hud.app.manager.InputManager a, com.navdy.service.library.events.input.GestureEvent a0, com.navdy.hud.app.manager.InputManager$CustomKeyEvent a1) {
        return a.invokeHandler(a0, a1);
    }
    
    static com.navdy.hud.app.ui.framework.UIStateManager access$200(com.navdy.hud.app.manager.InputManager a) {
        return a.uiStateManager;
    }
    
    private com.navdy.hud.app.manager.InputManager$CustomKeyEvent convertKeyToCustomEvent(int i, android.view.KeyEvent a) {
        com.navdy.hud.app.manager.InputManager$CustomKeyEvent a0 = null;
        switch(i) {
            case 10: case 22: case 31: case 32: case 33: case 40: {
                a0 = com.navdy.hud.app.manager.InputManager$CustomKeyEvent.RIGHT;
                break;
            }
            case 9: case 20: case 39: case 47: case 51: case 52: case 66: {
                a0 = (a.isLongPress()) ? com.navdy.hud.app.manager.InputManager$CustomKeyEvent.LONG_PRESS : com.navdy.hud.app.manager.InputManager$CustomKeyEvent.SELECT;
                break;
            }
            case 8: case 21: case 29: case 38: case 45: case 54: {
                a0 = com.navdy.hud.app.manager.InputManager$CustomKeyEvent.LEFT;
                break;
            }
            default: {
                a0 = null;
            }
        }
        return a0;
    }
    
    private boolean invokeHandler(com.navdy.service.library.events.input.GestureEvent a, com.navdy.hud.app.manager.InputManager$CustomKeyEvent a0) {
        boolean b = false;
        label1: {
            label0: {
                if (a != null) {
                    break label0;
                }
                if (a0 != null) {
                    break label0;
                }
                b = false;
                break label1;
            }
            if (this.inputHandler == null) {
                b = false;
            } else {
                com.navdy.hud.app.manager.InputManager$IInputHandler a1 = this.inputHandler;
                while(true) {
                    if (a1 == null) {
                        b = false;
                        break;
                    } else if (a == null) {
                        if (!a1.onKey(a0)) {
                            a1 = a1.nextHandler();
                            continue;
                        }
                        b = true;
                        break;
                    } else {
                        if (!a1.onGesture(a)) {
                            a1 = a1.nextHandler();
                            continue;
                        }
                        com.navdy.hud.app.device.light.HUDLightUtils.showGestureDetected(com.navdy.hud.app.HudApplication.getAppContext(), com.navdy.hud.app.device.light.LightManager.getInstance());
                        b = true;
                        break;
                    }
                }
            }
            if (!b && a0 != null && com.navdy.hud.app.manager.InputManager$2.$SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent[a0.ordinal()] == 2) {
                sLogger.v("long press not handled");
                if (this.defaultHandler == null) {
                    com.navdy.hud.app.ui.activity.Main a2 = this.uiStateManager.getRootScreen();
                    if (a2 != null) {
                        this.defaultHandler = a2.getInputHandler();
                    }
                }
                if (this.defaultHandler != null) {
                    b = this.defaultHandler.onKey(a0);
                }
            }
            if (b) {
                this.lastInputEventTime = android.os.SystemClock.elapsedRealtime();
                com.navdy.hud.app.service.ShutdownMonitor.getInstance().recordInputEvent();
            }
        }
        return b;
    }
    
    public static boolean isCenterKey(int i) {
        boolean b = false;
        label2: {
            label0: {
                label1: {
                    if (i == 47) {
                        break label1;
                    }
                    if (i == 52) {
                        break label1;
                    }
                    if (i == 51) {
                        break label1;
                    }
                    if (i == 66) {
                        break label1;
                    }
                    if (i == 20) {
                        break label1;
                    }
                    if (i == 39) {
                        break label1;
                    }
                    if (i != 9) {
                        break label0;
                    }
                }
                b = true;
                break label2;
            }
            b = false;
        }
        return b;
    }
    
    public static boolean isLeftKey(int i) {
        boolean b = false;
        label2: {
            label0: {
                label1: {
                    if (i == 29) {
                        break label1;
                    }
                    if (i == 54) {
                        break label1;
                    }
                    if (i == 45) {
                        break label1;
                    }
                    if (i == 21) {
                        break label1;
                    }
                    if (i == 38) {
                        break label1;
                    }
                    if (i != 8) {
                        break label0;
                    }
                }
                b = true;
                break label2;
            }
            b = false;
        }
        return b;
    }
    
    public static boolean isRightKey(int i) {
        boolean b = false;
        label2: {
            label0: {
                label1: {
                    if (i == 32) {
                        break label1;
                    }
                    if (i == 31) {
                        break label1;
                    }
                    if (i == 33) {
                        break label1;
                    }
                    if (i == 22) {
                        break label1;
                    }
                    if (i == 40) {
                        break label1;
                    }
                    if (i != 10) {
                        break label0;
                    }
                }
                b = true;
                break label2;
            }
            b = false;
        }
        return b;
    }
    
    public static com.navdy.hud.app.manager.InputManager$IInputHandler nextContainingHandler(android.view.View a) {
        android.view.ViewParent a0 = a.getParent();
        while(true) {
            if (a0 != null && !(a0 instanceof com.navdy.hud.app.manager.InputManager$IInputHandler)) {
                a0 = a0.getParent();
                continue;
            }
            if (a0 == null) {
                a0 = null;
            }
            return (com.navdy.hud.app.manager.InputManager$IInputHandler)a0;
        }
    }
    
    public com.navdy.hud.app.manager.InputManager$IInputHandler getFocus() {
        return this.inputHandler;
    }
    
    public long getLastInputEventTime() {
        return this.lastInputEventTime;
    }
    
    public void injectKey(com.navdy.hud.app.manager.InputManager$CustomKeyEvent a) {
        this.powerManager.wakeUp(com.navdy.hud.app.analytics.AnalyticsSupport$WakeupReason.POWERBUTTON);
        this.handler.post((Runnable)new com.navdy.hud.app.manager.InputManager$1(this, a));
    }
    
    public void onGestureEvent(com.navdy.service.library.events.input.GestureEvent a) {
        this.invokeHandler(a, (com.navdy.hud.app.manager.InputManager$CustomKeyEvent)null);
    }
    
    public boolean onKeyDown(int i, android.view.KeyEvent a) {
        boolean b = false;
        if (sLogger.isLoggable(3)) {
            sLogger.v(new StringBuilder().append("onKeyDown:").append(i).append(" isLongPress:").append(a.isLongPress()).append(" ").append(a).toString());
        }
        this.bus.post(a);
        if (this.dialManager == null) {
            this.dialManager = com.navdy.hud.app.device.dial.DialManager.getInstance();
        }
        if (this.dialManager.reportInputEvent(a)) {
            com.navdy.hud.app.device.light.HUDLightUtils.dialActionDetected(com.navdy.hud.app.HudApplication.getAppContext(), com.navdy.hud.app.device.light.LightManager.getInstance());
        }
        if (a.isLongPress()) {
            b = true;
        } else if (this.isLongPressDetected) {
            b = true;
        } else {
            com.navdy.hud.app.manager.InputManager$CustomKeyEvent a0 = this.convertKeyToCustomEvent(i, a);
            if (a0 == null) {
                b = false;
            } else if (a0 != com.navdy.hud.app.manager.InputManager$CustomKeyEvent.SELECT) {
                b = this.invokeHandler((com.navdy.service.library.events.input.GestureEvent)null, a0);
            } else {
                a.startTracking();
                b = true;
            }
        }
        return b;
    }
    
    public boolean onKeyLongPress(int i, android.view.KeyEvent a) {
        if (sLogger.isLoggable(3)) {
            sLogger.v(new StringBuilder().append("onKeyLongPress:").append(i).append(" ").append(a).toString());
        }
        this.isLongPressDetected = true;
        return this.invokeHandler((com.navdy.service.library.events.input.GestureEvent)null, this.convertKeyToCustomEvent(i, a));
    }
    
    public boolean onKeyUp(int i, android.view.KeyEvent a) {
        boolean b = false;
        if (sLogger.isLoggable(3)) {
            sLogger.v(new StringBuilder().append("onKeyUp:").append(i).append(" isLongPress:").append(a.isLongPress()).append(" ").append(a).toString());
        }
        this.bus.post(a);
        if (this.isLongPressDetected) {
            this.isLongPressDetected = false;
            b = true;
        } else {
            com.navdy.hud.app.manager.InputManager$CustomKeyEvent a0 = this.convertKeyToCustomEvent(i, a);
            if (a0 == null) {
                b = false;
            } else {
                switch(com.navdy.hud.app.manager.InputManager$2.$SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent[a0.ordinal()]) {
                    case 1: case 2: {
                        b = this.invokeHandler((com.navdy.service.library.events.input.GestureEvent)null, a0);
                        break;
                    }
                    default: {
                        b = false;
                    }
                }
            }
        }
        return b;
    }
    
    public void setFocus(com.navdy.hud.app.manager.InputManager$IInputHandler a) {
        this.inputHandler = a;
    }
}
