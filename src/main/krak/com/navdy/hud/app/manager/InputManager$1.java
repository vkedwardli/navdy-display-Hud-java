package com.navdy.hud.app.manager;

class InputManager$1 implements Runnable {
    final com.navdy.hud.app.manager.InputManager this$0;
    final com.navdy.hud.app.manager.InputManager$CustomKeyEvent val$event;
    
    InputManager$1(com.navdy.hud.app.manager.InputManager a, com.navdy.hud.app.manager.InputManager$CustomKeyEvent a0) {
        super();
        this.this$0 = a;
        this.val$event = a0;
    }
    
    public void run() {
        if (this.val$event != null) {
            com.navdy.hud.app.manager.InputManager.access$000().v(new StringBuilder().append("inject key event:").append(this.val$event).toString());
            if (!com.navdy.hud.app.manager.InputManager.access$100(this.this$0, (com.navdy.service.library.events.input.GestureEvent)null, this.val$event)) {
                com.navdy.hud.app.ui.activity.Main a = com.navdy.hud.app.manager.InputManager.access$200(this.this$0).getRootScreen();
                if (a != null) {
                    a.handleKey(this.val$event);
                }
            }
        }
    }
}
