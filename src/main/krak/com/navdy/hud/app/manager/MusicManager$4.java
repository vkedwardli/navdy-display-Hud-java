package com.navdy.hud.app.manager;

class MusicManager$4 implements Runnable {
    final com.navdy.hud.app.manager.MusicManager this$0;
    final com.navdy.hud.app.manager.MusicManager$MusicUpdateListener val$listener;
    
    MusicManager$4(com.navdy.hud.app.manager.MusicManager a, com.navdy.hud.app.manager.MusicManager$MusicUpdateListener a0) {
        super();
        this.this$0 = a;
        this.val$listener = a0;
    }
    
    public void run() {
        com.navdy.hud.app.manager.MusicManager.access$1000(this.this$0).post((Runnable)new com.navdy.hud.app.manager.MusicManager$4$1(this));
        com.navdy.hud.app.manager.MusicManager.access$000().v("album art listeners:");
        Object a = com.navdy.hud.app.manager.MusicManager.access$1200(this.this$0).iterator();
        while(((java.util.Iterator)a).hasNext()) {
            Object a0 = ((java.util.Iterator)a).next();
            com.navdy.hud.app.manager.MusicManager.access$000().d(new StringBuilder().append("- ").append(a0).toString());
        }
    }
}
