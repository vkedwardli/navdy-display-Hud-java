package com.navdy.hud.app.obd;
import com.navdy.hud.app.R;

public class ObdCanBusRecordingPolicy {
    final private static long CAN_BUS_MONITORING_DISTANCE_LIMIT_METERS = 40234L;
    final public static String CAN_PROTOCOL_PREFIX = "ISO 15765-4";
    final public static String FILES_TO_UPLOAD_DIRECTORY = "/sdcard/.canBusLogs/upload";
    final public static int MAX_META_DATA_FILES = 10;
    final public static int MAX_UPLOAD_FILES = 5;
    final public static String META_DATA_FILE = "/sdcard/.canBusLogs/upload/meta_data_";
    final public static int MINIMUM_TIME_FOR_MOTION_DETECTION = 10000;
    final public static int MINIMUM_TIME_FOR_STOP_DETECTION = 10000;
    final private static float MOVING_SPEED_METERS_PER_SECOND = 1.34112f;
    final public static String PREFERENCE_NAVDY_MILES_WHEN_LISTENING_STARTED = "preference_navdy_miles_when_listening_started";
    final public static String PREF_CAN_BUS_DATA_RECORDED_AND_SENT = "canBusDataRecordedAndSent";
    final public static String PREF_CAN_BUS_DATA_SENT_VERSION = "canBusDataSentVersion";
    final public static String PREF_CAN_BUS_DATA_STATUS_REPORTED_VERSION = "canBusDataStatusReportedVersion";
    final private static java.io.File UPLOAD_DIRECTORY_FILE;
    final private static java.text.SimpleDateFormat dateFormat;
    private static java.util.concurrent.PriorityBlockingQueue metaDataFiles;
    final private static com.navdy.service.library.log.Logger sLogger;
    private static java.util.concurrent.PriorityBlockingQueue uploadFiles;
    private int canBusMonitoringDataSize;
    private com.navdy.hud.app.obd.ObdCanBusRecordingPolicy$CanBusMonitoringState canBusMonitoringState;
    private android.content.Context context;
    private java.io.File currentMetaDataFile;
    private long firstTimeMovementDetected;
    private long firstTimeStoppingDetected;
    private android.os.Handler handler;
    private boolean isCanBusMonitoringLimitReached;
    private boolean isCanProtocol;
    private boolean isEngineeringBuild;
    private boolean isInstantaneousModeOn;
    private boolean isObdConnected;
    int lastSpeed;
    private String make;
    private String model;
    private boolean motionDetected;
    private com.navdy.hud.app.obd.ObdManager obdManager;
    private int requiredObdDataVersion;
    private android.content.SharedPreferences sharedPreferences;
    private com.navdy.hud.app.framework.trips.TripManager tripManager;
    private String vin;
    private String year;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.obd.ObdCanBusRecordingPolicy.class);
        UPLOAD_DIRECTORY_FILE = new java.io.File("/sdcard/.canBusLogs/upload");
        dateFormat = new java.text.SimpleDateFormat("yyyy_MM_dd_HH_mm_ss_SSS");
        metaDataFiles = new java.util.concurrent.PriorityBlockingQueue(10, (java.util.Comparator)new com.navdy.hud.app.util.CrashReportService$FilesModifiedTimeComparator());
        uploadFiles = new java.util.concurrent.PriorityBlockingQueue(10, (java.util.Comparator)new com.navdy.hud.app.util.CrashReportService$FilesModifiedTimeComparator());
    }
    
    public ObdCanBusRecordingPolicy(android.content.Context a, android.content.SharedPreferences a0, com.navdy.hud.app.obd.ObdManager a1, com.navdy.hud.app.framework.trips.TripManager a2) {
        this.isEngineeringBuild = !com.navdy.hud.app.util.DeviceUtil.isUserBuild();
        this.firstTimeMovementDetected = 0L;
        this.firstTimeStoppingDetected = 0L;
        this.isObdConnected = false;
        this.isCanProtocol = false;
        this.isInstantaneousModeOn = false;
        this.motionDetected = false;
        this.lastSpeed = -1;
        this.isCanBusMonitoringLimitReached = false;
        this.canBusMonitoringState = com.navdy.hud.app.obd.ObdCanBusRecordingPolicy$CanBusMonitoringState.UNKNOWN;
        this.canBusMonitoringDataSize = 0;
        this.obdManager = a1;
        this.context = a;
        this.tripManager = a2;
        this.sharedPreferences = a0;
        this.requiredObdDataVersion = a.getResources().getInteger(R.integer.obd_data_version);
        if (!UPLOAD_DIRECTORY_FILE.exists()) {
            UPLOAD_DIRECTORY_FILE.mkdirs();
        }
        this.populateFilesQueue();
        this.handler = new android.os.Handler();
    }
    
    private java.io.File getMetaDataFile() {
        long j = System.currentTimeMillis();
        String s = dateFormat.format(new java.util.Date(j));
        java.io.File a = new java.io.File(new StringBuilder().append("/sdcard/.canBusLogs/upload/meta_data_").append(s).append(".txt").toString());
        java.io.File a0 = a.getParentFile();
        if (!a0.exists()) {
            sLogger.d("Upload directory not found, creating");
            a0.mkdirs();
        }
        sLogger.d(new StringBuilder().append("Creating Meta data file ").append(a.getName()).toString());
        boolean b = a.exists();
        label0: {
            java.io.IOException a1 = null;
            if (b) {
                break label0;
            }
            try {
                a.createNewFile();
                break label0;
            } catch(java.io.IOException a2) {
                a1 = a2;
            }
            sLogger.e("IOException while creating file ", (Throwable)a1);
        }
        return a;
    }
    
    private void populateFilesQueue() {
        metaDataFiles.clear();
        uploadFiles.clear();
        java.io.File[] a = UPLOAD_DIRECTORY_FILE.listFiles();
        int i = a.length;
        int i0 = 0;
        while(i0 < i) {
            java.io.File a0 = a[i0];
            boolean b = a0.isFile();
            label0: {
                label1: {
                    label2: {
                        if (!b) {
                            break label2;
                        }
                        if (a0.getName().startsWith("meta_data")) {
                            break label1;
                        }
                    }
                    if (!a0.isFile()) {
                        break label0;
                    }
                    if (!a0.getName().startsWith("CAN_BUS_DATA_")) {
                        break label0;
                    }
                    uploadFiles.add(a0);
                    if (uploadFiles.size() <= 5) {
                        break label0;
                    }
                    java.io.File a1 = (java.io.File)uploadFiles.poll();
                    if (a1 == null) {
                        break label0;
                    }
                    sLogger.d(new StringBuilder().append("Deleting upload file ").append(a1.getName()).append(", As its old").toString());
                    com.navdy.service.library.util.IOUtils.deleteFile(com.navdy.hud.app.HudApplication.getAppContext(), a1.getAbsolutePath());
                    break label0;
                }
                metaDataFiles.add(a0);
                if (metaDataFiles.size() > 10) {
                    java.io.File a2 = (java.io.File)metaDataFiles.poll();
                    if (a2 != null) {
                        sLogger.d(new StringBuilder().append("Deleting meta data file ").append(a2.getName()).append(", As its old").toString());
                        com.navdy.service.library.util.IOUtils.deleteFile(com.navdy.hud.app.HudApplication.getAppContext(), a2.getAbsolutePath());
                    }
                }
            }
            i0 = i0 + 1;
        }
    }
    
    private void reportBusMonitoringState() {
        boolean b = this.sharedPreferences.getInt("canBusDataStatusReportedVersion", -1) == this.requiredObdDataVersion;
        sLogger.d(new StringBuilder().append("reportBusMonitoringState, Already reported ? : ").append(b).toString());
        if (!b) {
            sLogger.d(new StringBuilder().append("Reporting, Make ").append(this.make).append(", Model : ").append(this.model).append(", Year : ").append(this.year).append(", State : ").append(this.canBusMonitoringState).toString());
            if (!android.text.TextUtils.isEmpty((CharSequence)this.make) && !android.text.TextUtils.isEmpty((CharSequence)this.model) && !android.text.TextUtils.isEmpty((CharSequence)this.year) && this.canBusMonitoringState != com.navdy.hud.app.obd.ObdCanBusRecordingPolicy$CanBusMonitoringState.UNKNOWN) {
                com.navdy.hud.app.analytics.AnalyticsSupport.recordObdCanBusMonitoringState(this.vin, this.canBusMonitoringState == com.navdy.hud.app.obd.ObdCanBusRecordingPolicy$CanBusMonitoringState.SUCCESS, (this.canBusMonitoringState != com.navdy.hud.app.obd.ObdCanBusRecordingPolicy$CanBusMonitoringState.SUCCESS) ? 0 : this.canBusMonitoringDataSize, this.requiredObdDataVersion);
                this.sharedPreferences.edit().putInt("canBusDataStatusReportedVersion", this.requiredObdDataVersion).apply();
            }
        }
    }
    
    public boolean isCanBusMonitoringLimitReached() {
        return this.isCanBusMonitoringLimitReached;
    }
    
    public boolean isCanBusMonitoringNeeded() {
        boolean b = false;
        boolean b0 = this.sharedPreferences.getBoolean("canBusDataRecordedAndSent", false);
        int i = this.sharedPreferences.getInt("canBusDataSentVersion", -1);
        sLogger.d(new StringBuilder().append("IsCanBusMonitoringNeeded : ? Is Engineering Build ").append(this.isEngineeringBuild).append("Data record sent : ").append(b0).append(", Sent Data Version : ").append(i).append(", isMoving : ").append(this.motionDetected).append(", InstantaneousMode : ").append(this.isInstantaneousModeOn).append(", Obd Connected : ").append(this.isObdConnected).append(", Is CAN protocol :").append(this.isCanProtocol).toString());
        boolean b1 = this.isEngineeringBuild;
        label3: {
            label0: {
                label1: {
                    if (!b1) {
                        break label1;
                    }
                    label2: {
                        if (!b0) {
                            break label2;
                        }
                        if (i == this.requiredObdDataVersion) {
                            break label1;
                        }
                    }
                    if (!this.motionDetected) {
                        break label1;
                    }
                    if (this.isInstantaneousModeOn) {
                        break label1;
                    }
                    if (!this.isObdConnected) {
                        break label1;
                    }
                    if (this.isCanProtocol) {
                        break label0;
                    }
                }
                b = false;
                break label3;
            }
            b = true;
        }
        if (b) {
            long j = this.sharedPreferences.getLong("preference_navdy_miles_when_listening_started", -1L);
            long j0 = this.tripManager.getTotalDistanceTravelledWithNavdy();
            if (j != -1L) {
                if (j0 - j > 40234L) {
                    sLogger.d(new StringBuilder().append("CAN bus monitoring limit reached, Distance recorded when listening started ").append(j).append(", Current distance travelled ").append(j0).toString());
                    this.isCanBusMonitoringLimitReached = true;
                }
            } else {
                this.sharedPreferences.edit().putLong("preference_navdy_miles_when_listening_started", j0).apply();
            }
        } else {
            this.isCanBusMonitoringLimitReached = false;
        }
        return b;
    }
    
    public void onCanBusMonitorSuccess(int i) {
        this.canBusMonitoringState = com.navdy.hud.app.obd.ObdCanBusRecordingPolicy$CanBusMonitoringState.SUCCESS;
        this.canBusMonitoringDataSize = i;
        this.reportBusMonitoringState();
    }
    
    public void onCanBusMonitoringFailed() {
        double d = (double)com.navdy.hud.app.manager.SpeedManager.getInstance().getObdSpeed();
        double d0 = (double)this.obdManager.getEngineRpm();
        sLogger.d(new StringBuilder().append("onCanBusMonitoringFailed , RawSpeed : ").append(d).append(", RPM : ").append(d0).toString());
        int i = (d > 0.0) ? 1 : (d == 0.0) ? 0 : -1;
        label0: {
            label1: {
                if (i > 0) {
                    break label1;
                }
                if (!(d0 > 0.0)) {
                    break label0;
                }
            }
            this.canBusMonitoringState = com.navdy.hud.app.obd.ObdCanBusRecordingPolicy$CanBusMonitoringState.FAILURE;
            this.reportBusMonitoringState();
        }
    }
    
    public void onCarDetailsAvailable(String s, String s0, String s1) {
        label8: synchronized(this) {
        com.navdy.service.library.log.Logger a = sLogger;
            java.io.FileWriter a0 = null;
            java.io.BufferedWriter a1 = null;
            a.d(new StringBuilder().append("onCarDetailsAvailable Make : ").append(s).append(" , Model : ").append(s0).append(", Year : ").append(s1).toString());
            this.reportBusMonitoringState();
            boolean b = android.text.TextUtils.equals((CharSequence)this.make, (CharSequence)s);
            label9: {
                if (!b) {
                    break label9;
                }
                if (!android.text.TextUtils.equals((CharSequence)this.model, (CharSequence)s0)) {
                    break label9;
                }
                if (!android.text.TextUtils.equals((CharSequence)this.year, (CharSequence)s1)) {
                    break label9;
                }
                sLogger.d("Car details same");
                break label8;
            }
            sLogger.d("Car details changed");
            this.make = s;
            this.model = s0;
            this.year = s1;
            if (!this.isObdConnected) {
                break label8;
            }
            if (this.currentMetaDataFile == null) {
                break label8;
            }
            label7: {
                java.io.BufferedWriter a2 = null;
                java.io.FileWriter a3 = null;
                Throwable a4 = null;
                label0: {
                    label1: {
                        label3: {
                            {
                                label2: { //try {
                                    label5: {
                                        label6: {
                                            try {
                                                a2 = null;
                                                a3 = null;
                                                a0 = new java.io.FileWriter(this.currentMetaDataFile, true);
                                                break label6;
                                            } catch(java.io.IOException ignoredException) {
                                            }
                                            a0 = null;
                                            a1 = null;
                                            break label5;
                                        }
                                        label4: {
                                            try {
                                                try {
                                                    a1 = new java.io.BufferedWriter((java.io.Writer)a0);
                                                    break label4;
                                                } catch(java.io.IOException ignoredException0) {
                                                }
                                            } catch(Throwable a5) {
                                                a4 = a5;
                                                break label3;
                                            }
                                            a1 = null;
                                            break label5;
                                        }
//                                            try {
                                                if (!android.text.TextUtils.isEmpty((CharSequence)s)) {
                                                    a1.write(new StringBuilder().append("Make : ").append(s).append("\n").toString());
                                                }
                                                if (!android.text.TextUtils.isEmpty((CharSequence)s0)) {
                                                    a1.write(new StringBuilder().append("Model : ").append(s0).append("\n").toString());
                                                }
                                                if (!android.text.TextUtils.isEmpty((CharSequence)s1)) {
                                                    a1.write(new StringBuilder().append("Year : ").append(s1).append("\n").toString());
                                                }
                                                a1.flush();
                                                break label2;
                                            } catch(java.io.IOException ignoredException1) {
                                            }
//                                        } catch(Throwable a6) {
//                                            a4 = a6;
//                                            break label1;
//                                        }
                                    }
                                    com.navdy.service.library.log.Logger a7 = sLogger;
                                    a2 = a1;
                                    a3 = a0;
                                    a7.d("Error writing to the metadata file");
                                    break label7;
//                                } catch(Throwable a8) {
//                                    a4 = a8;
//                                    break label0;
//                                }
                                com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a0);
                                com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a1);
                                break label8;
                            }
                        }
                        a2 = null;
                        a3 = a0;
                        break label0;
                    }
                    a2 = a1;
                    a3 = a0;
                }
                com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a3);
                com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a2);
                throw a4;
            }
            com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a0);
            com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a1);
//        } catch(Throwable a9) {
//            /*monexit(this)*/;
//            throw a9;
//        }
        /*monexit(this)*/;
    }
    
    public void onDrivingStateChanged(com.navdy.hud.app.event.DrivingStateChange a) {
        boolean b = a.driving;
        if (this.motionDetected != b) {
            boolean b0 = this.isCanBusMonitoringNeeded();
            this.motionDetected = b;
            if (b0 == this.isCanBusMonitoringNeeded()) {
                sLogger.d("Can BUS monitoring need is not changed due to driving state change");
            } else {
                sLogger.d("Can BUS monitoring need changed, update listener");
                this.obdManager.updateListener();
            }
        }
    }
    
    public void onFileUploaded(java.io.File a) {
        sLogger.d(new StringBuilder().append("File successfully uploaded ").append(a.getAbsolutePath()).toString());
        this.sharedPreferences.edit().putBoolean("canBusDataRecordedAndSent", true).commit();
        this.sharedPreferences.edit().putInt("canBusDataSentVersion", this.requiredObdDataVersion).commit();
        this.sharedPreferences.edit().putLong("preference_navdy_miles_when_listening_started", -1L);
        this.populateFilesQueue();
        com.navdy.hud.app.analytics.AnalyticsSupport.recordObdCanBusDataSent(Integer.toString(this.requiredObdDataVersion));
        this.obdManager.updateListener();
    }
    
    public void onInstantaneousModeChanged(boolean b) {
        this.isInstantaneousModeOn = b;
    }
    
    public void onNewDataAvailable(String s) {
        sLogger.d(new StringBuilder().append("onNewDataAvailable , File path : ").append(s).toString());
        java.io.File a = new java.io.File(s);
        String s0 = dateFormat.format(new java.util.Date(System.currentTimeMillis()));
        if (a.exists()) {
            java.io.File a0 = new java.io.File(new StringBuilder().append("/sdcard/.canBusLogs/upload").append(java.io.File.separator).append(a.getName()).append("").append(s0).append(".log").toString());
            sLogger.d(new StringBuilder().append("Moving From : ").append(a.getAbsolutePath()).append(", TO : ").append(a0.getAbsolutePath()).toString());
            a.renameTo(a0);
        }
        java.io.File[] a1 = new java.io.File("/sdcard/.canBusLogs/upload").listFiles();
        java.util.ArrayList a2 = new java.util.ArrayList();
        if (a1 == null) {
            sLogger.d("No files found in the upload directory");
        } else {
            int i = a1.length;
            int i0 = 0;
            while(i0 < i) {
                java.io.File a3 = a1[i0];
                sLogger.d(new StringBuilder().append("Child ").append(a3.getName()).toString());
                if (a3.isFile() && !a3.getName().endsWith(".zip")) {
                    ((java.util.List)a2).add(a3);
                }
                i0 = i0 + 1;
            }
            if (((java.util.List)a2).size() > 0) {
                java.io.File[] a4 = new java.io.File[((java.util.List)a2).size()];
                ((java.util.List)a2).toArray((Object[])a4);
                StringBuilder a5 = new StringBuilder();
                a5.append("CAN_BUS_DATA_");
                if (!android.text.TextUtils.isEmpty((CharSequence)this.make)) {
                    a5.append(new StringBuilder().append(this.make).append("_").toString());
                }
                if (!android.text.TextUtils.isEmpty((CharSequence)this.model)) {
                    a5.append(new StringBuilder().append(this.model).append("_").toString());
                }
                if (!android.text.TextUtils.isEmpty((CharSequence)this.year)) {
                    a5.append(new StringBuilder().append(this.year).append("_").toString());
                }
                if (!android.text.TextUtils.isEmpty((CharSequence)this.vin)) {
                    a5.append(new StringBuilder().append(this.vin).append("_").toString());
                }
                a5.append(new StringBuilder().append("V(").append(this.requiredObdDataVersion).append(")").toString());
                a5.append(s0).append(".zip");
                java.io.File a6 = new java.io.File(new StringBuilder().append("/sdcard/.canBusLogs/upload").append(java.io.File.separator).append(a5.toString()).toString());
                sLogger.d(new StringBuilder().append("Compressing the files to ").append(a6.getName()).toString());
                com.navdy.service.library.util.IOUtils.compressFilesToZip(com.navdy.hud.app.HudApplication.getAppContext(), a4, a6.getAbsolutePath());
                int i1 = a4.length;
                int i2 = 0;
                while(i2 < i1) {
                    java.io.File a7 = a4[i2];
                    com.navdy.service.library.util.IOUtils.deleteFile(com.navdy.hud.app.HudApplication.getAppContext(), a7.getAbsolutePath());
                    i2 = i2 + 1;
                }
                uploadFiles.add(a6);
                if (uploadFiles.size() > 5) {
                    java.io.File a8 = (java.io.File)uploadFiles.poll();
                    if (a8 != null) {
                        sLogger.d(new StringBuilder().append("Deleting upload file ").append(a8.getName()).append(", As its old").toString());
                        com.navdy.service.library.util.IOUtils.deleteFile(com.navdy.hud.app.HudApplication.getAppContext(), a8.getAbsolutePath());
                    }
                }
                com.navdy.hud.app.service.ObdCANBusDataUploadService.addObdDataFileToQueue(a6);
                com.navdy.hud.app.service.ObdCANBusDataUploadService.syncNow();
            }
        }
    }
    
    public void onObdConnectionStateChanged(boolean b) {
        label0: synchronized(this) {
        com.navdy.service.library.log.Logger a = sLogger;
            java.io.FileWriter a0 = null;
            java.io.BufferedWriter a1 = null;
            a.d(new StringBuilder().append("onObdConnectionStateChanged , Connected : ").append(b).toString());
            this.isObdConnected = b;
            label11: if (b) {
                String s = this.obdManager.getProtocol();
                boolean b0 = android.text.TextUtils.isEmpty((CharSequence)s);
                label10: {
                    if (b0) {
                        break label10;
                    }
                    if (!s.startsWith("ISO 15765-4")) {
                        break label10;
                    }
                    this.isCanProtocol = true;
                    break label11;
                }
                this.isCanProtocol = false;
            } else {
                this.isCanProtocol = false;
            }
            this.canBusMonitoringState = com.navdy.hud.app.obd.ObdCanBusRecordingPolicy$CanBusMonitoringState.UNKNOWN;
            this.canBusMonitoringDataSize = 0;
            label9: {
                java.io.BufferedWriter a2 = null;
                java.io.FileWriter a3 = null;
                Throwable a4 = null;
                label1: {
                    label2: {
                        label5: {
                            if (b) {
                                long j = System.currentTimeMillis();
                                if (metaDataFiles.size() > 10) {
                                    java.io.File a5 = (java.io.File)metaDataFiles.poll();
                                    if (a5 != null) {
                                        sLogger.d(new StringBuilder().append("Deleting file ").append(a5.getName()).append(", As its old").toString());
                                        com.navdy.service.library.util.IOUtils.deleteFile(com.navdy.hud.app.HudApplication.getAppContext(), a5.getAbsolutePath());
                                    }
                                }
                                this.currentMetaDataFile = this.getMetaDataFile();
                                org.json.JSONObject a6 = new org.json.JSONObject();
                                label4: try {
                                    a2 = null;
                                    a3 = null;
                                    label7: {
                                        label8: {
                                            try {
                                                a2 = null;
                                                a3 = null;
                                                a0 = new java.io.FileWriter(this.currentMetaDataFile, false);
                                                break label8;
                                            } catch(java.io.IOException ignoredException) {
                                            }
                                            a0 = null;
                                            a1 = null;
                                            break label7;
                                        }
                                        label6: {
                                            try {
                                                try {
                                                    a1 = new java.io.BufferedWriter((java.io.Writer)a0);
                                                    break label6;
                                                } catch(java.io.IOException ignoredException0) {
                                                }
                                            } catch(Throwable a7) {
                                                a4 = a7;
                                                break label5;
                                            }
                                            a1 = null;
                                            break label7;
                                        }
                                        try {
                                            try {
                                                com.navdy.obd.ICarService a8 = this.obdManager.getCarService();
                                                this.vin = this.obdManager.getVin();
                                                String s0 = dateFormat.format(new java.util.Date(j));
                                                try {
                                                    a6.accumulate("time", s0);
                                                    a6.accumulate("vin", this.vin);
                                                    label3: {
                                                        if (a8 == null) {
                                                            break label3;
                                                        }
                                                        try {
                                                            a6.accumulate("protocol", a8.getProtocol());
                                                            java.util.List a9 = a8.getEcus();
                                                            org.json.JSONArray a10 = new org.json.JSONArray();
                                                            if (a9 == null) {
                                                                break label3;
                                                            }
                                                            Object a11 = a9;
                                                            int i = 0;
                                                            while(i < ((java.util.List)a11).size()) {
                                                                org.json.JSONObject a12 = new org.json.JSONObject();
                                                                com.navdy.obd.ECU a13 = (com.navdy.obd.ECU)((java.util.List)a11).get(i);
                                                                a12.accumulate("Address ", Integer.valueOf(a13.address));
                                                                java.util.List a14 = a13.supportedPids.asList();
                                                                org.json.JSONArray a15 = new org.json.JSONArray();
                                                                if (a14 != null) {
                                                                    Object a16 = a14.iterator();
                                                                    while(((java.util.Iterator)a16).hasNext()) {
                                                                        a15.put(((com.navdy.obd.Pid)((java.util.Iterator)a16).next()).getId());
                                                                    }
                                                                }
                                                                a12.accumulate("PIDs", a15);
                                                                a10.put(a12);
                                                                i = i + 1;
                                                            }
                                                            a6.accumulate("ecus", a10);
                                                            break label3;
                                                        } catch(android.os.RemoteException ignoredException1) {
                                                        }
                                                        sLogger.d("Error get the data from obd service");
                                                    }
                                                    if (!android.text.TextUtils.isEmpty((CharSequence)this.make)) {
                                                        a6.accumulate("Make", this.make);
                                                    }
                                                    if (!android.text.TextUtils.isEmpty((CharSequence)this.model)) {
                                                        a6.accumulate("Model", this.model);
                                                    }
                                                    if (!android.text.TextUtils.isEmpty((CharSequence)this.year)) {
                                                        a6.accumulate("Year", this.year);
                                                    }
                                                } catch(org.json.JSONException a17) {
                                                    sLogger.d("Error writing meta data JSON ", (Throwable)a17);
                                                }
                                                a1.write(a6.toString());
                                                a1.flush();
                                                break label4;
                                            } catch(java.io.IOException ignoredException2) {
                                            }
                                        } catch(Throwable a18) {
                                            a4 = a18;
                                            break label2;
                                        }
                                    }
                                    com.navdy.service.library.log.Logger a19 = sLogger;
                                    a2 = a1;
                                    a3 = a0;
                                    a19.d("Error writing to the metadata file");
                                    break label9;
                                } catch(Throwable a20) {
                                    a4 = a20;
                                    break label1;
                                }
                                com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a0);
                                com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a1);
                                break label0;
                            } else {
                                this.currentMetaDataFile = null;
                                break label0;
                            }
                        }
                        a2 = null;
                        a3 = a0;
                        break label1;
                    }
                    a2 = a1;
                    a3 = a0;
                }
                com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a3);
                com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a2);
                throw a4;
            }
            com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a0);
            com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a1);
        } catch(Throwable a21) {
            /*monexit(this)*/;
            throw a21;
        }
        /*monexit(this)*/;
    }
}
