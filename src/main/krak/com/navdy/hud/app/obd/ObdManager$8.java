package com.navdy.hud.app.obd;

class ObdManager$8 {
    final static int[] $SwitchMap$com$navdy$service$library$events$preferences$DriverProfilePreferences$ObdScanSetting;
    
    static {
        $SwitchMap$com$navdy$service$library$events$preferences$DriverProfilePreferences$ObdScanSetting = new int[com.navdy.service.library.events.preferences.DriverProfilePreferences$ObdScanSetting.values().length];
        int[] a = $SwitchMap$com$navdy$service$library$events$preferences$DriverProfilePreferences$ObdScanSetting;
        com.navdy.service.library.events.preferences.DriverProfilePreferences$ObdScanSetting a0 = com.navdy.service.library.events.preferences.DriverProfilePreferences$ObdScanSetting.SCAN_DEFAULT_ON;
        try {
            a[a0.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$preferences$DriverProfilePreferences$ObdScanSetting[com.navdy.service.library.events.preferences.DriverProfilePreferences$ObdScanSetting.SCAN_ON.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException0) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$preferences$DriverProfilePreferences$ObdScanSetting[com.navdy.service.library.events.preferences.DriverProfilePreferences$ObdScanSetting.SCAN_OFF.ordinal()] = 3;
        } catch(NoSuchFieldError ignoredException1) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$preferences$DriverProfilePreferences$ObdScanSetting[com.navdy.service.library.events.preferences.DriverProfilePreferences$ObdScanSetting.SCAN_DEFAULT_OFF.ordinal()] = 4;
        } catch(NoSuchFieldError ignoredException2) {
        }
    }
}
