package com.navdy.hud.app.obd;

class ObdManager$6 extends com.navdy.obd.IPidListener$Stub {
    final com.navdy.hud.app.obd.ObdManager this$0;
    
    ObdManager$6(com.navdy.hud.app.obd.ObdManager a) {
        super();
        this.this$0 = a;
    }
    
    public void onConnectionStateChange(int i) {
        com.navdy.hud.app.obd.ObdManager.access$2200(this.this$0, i);
    }
    
    public void pidsChanged(java.util.List a) {
    }
    
    public void pidsRead(java.util.List a, java.util.List a0) {
        synchronized(this) {
            boolean b = com.navdy.hud.app.obd.ObdManager.access$1200(this.this$0);
            label0: {
                Throwable a1 = null;
                if (!b) {
                    break label0;
                }
                if (a0 == null) {
                    break label0;
                }
                com.navdy.hud.app.obd.ObdManager.access$1202(this.this$0, false);
                try {
                    com.navdy.obd.ICarService a2 = com.navdy.hud.app.obd.ObdManager.access$1300(this.this$0).getCarApi();
                    com.navdy.hud.app.obd.ObdManager.access$1402(this.this$0, a2.getVIN());
                    com.navdy.hud.app.obd.ObdManager.access$1500(this.this$0, com.navdy.hud.app.obd.ObdManager.access$1400(this.this$0));
                    com.navdy.hud.app.obd.ObdManager.access$1602(this.this$0, a2.getEcus());
                    com.navdy.hud.app.obd.ObdManager.access$1702(this.this$0, a2.getProtocol());
                    java.util.List a3 = com.navdy.hud.app.obd.ObdManager.access$1300(this.this$0).getCarApi().getSupportedPids();
                    com.navdy.hud.app.obd.ObdManager.access$700().d("First scan, got the supported PIDS");
                    if (a3 == null) {
                        com.navdy.hud.app.obd.ObdManager.access$700().d("First scan, got the supported PIDS , list null");
                    } else {
                        com.navdy.hud.app.obd.ObdManager.access$700().d(new StringBuilder().append("Supported PIDS size :").append(a3.size()).toString());
                    }
                    if (a3 == null) {
                        break label0;
                    }
                    com.navdy.obd.PidSet a4 = new com.navdy.obd.PidSet(a3);
                    if (a4.equals(com.navdy.hud.app.obd.ObdManager.access$100(this.this$0))) {
                        com.navdy.hud.app.obd.ObdManager.access$700().v("pid-changed event not sent");
                        break label0;
                    } else {
                        com.navdy.hud.app.obd.ObdManager.access$1800().v(new StringBuilder().append("SUPPORTED VIN: ").append((com.navdy.hud.app.obd.ObdManager.access$1400(this.this$0) == null) ? "" : com.navdy.hud.app.obd.ObdManager.access$1400(this.this$0)).append(", ").append("FUEL: ").append(a4.contains(47)).append(", ").append("SPEED: ").append(a4.contains(13)).append(", ").append("RPM: ").append(a4.contains(12)).append(", ").append("DIST: ").append(a4.contains(49)).append(", ").append("MAF: ").append(a4.contains(16)).append(", ").append("IFC(LPKKM): ").append(a3.contains(Integer.valueOf(256))).toString());
                        com.navdy.hud.app.obd.ObdManager.access$102(this.this$0, a4);
                        this.this$0.fuelPidCheck.reset();
                        this.this$0.bus.post(new com.navdy.hud.app.obd.ObdManager$ObdSupportedPidsChangedEvent());
                        com.navdy.hud.app.obd.ObdManager.access$700().v("pid-changed event sent");
                        break label0;
                    }
                } catch(Throwable a5) {
                    a1 = a5;
                }
                com.navdy.hud.app.obd.ObdManager.access$700().e(a1);
            }
            if (a0 != null) {
                java.util.Iterator a6 = a0.iterator();
                Object a7 = a;
                Object a8 = a0;
                Object a9 = a6;
                while(((java.util.Iterator)a9).hasNext()) {
                    com.navdy.obd.Pid a10 = (com.navdy.obd.Pid)((java.util.Iterator)a9).next();
                    switch(a10.getId()) {
                        case 49: {
                            com.navdy.hud.app.obd.ObdManager.access$000(this.this$0).put(Integer.valueOf(a10.getId()), Double.valueOf(a10.getValue()));
                            com.navdy.hud.app.obd.ObdManager.access$2000(this.this$0, (long)a10.getValue());
                            break;
                        }
                        case 47: {
                            int i = (int)a10.getValue();
                            this.this$0.fuelPidCheck.checkPid((double)i);
                            if (this.this$0.fuelPidCheck.hasIncorrectData()) {
                                com.navdy.hud.app.obd.ObdManager.access$000(this.this$0).remove(Integer.valueOf(47));
                                break;
                            } else {
                                com.navdy.hud.app.obd.ObdManager.access$000(this.this$0).put(Integer.valueOf(a10.getId()), Double.valueOf(a10.getValue()));
                                break;
                            }
                        }
                        case 13: {
                            com.navdy.hud.app.obd.ObdManager.access$1900(this.this$0).setObdSpeed((int)a10.getValue(), a10.getTimeStamp());
                            break;
                        }
                        default: {
                            com.navdy.hud.app.obd.ObdManager.access$000(this.this$0).put(Integer.valueOf(a10.getId()), Double.valueOf(a10.getValue()));
                        }
                    }
                    this.this$0.bus.post(new com.navdy.hud.app.obd.ObdManager$ObdPidChangeEvent(a10));
                }
                if (com.navdy.hud.app.obd.ObdManager.access$2100(this.this$0) != null && a8 != null) {
                    com.navdy.hud.app.obd.ObdManager.access$2100(this.this$0).pidsRead((java.util.List)a7, (java.util.List)a8);
                }
                this.this$0.bus.post(new com.navdy.hud.app.obd.ObdManager$ObdPidReadEvent((java.util.List)a7));
            } else {
                this.this$0.bus.post(new com.navdy.hud.app.obd.ObdManager$ObdPidReadEvent(a));
            }
        }
        /*monexit(this)*/;
    }
}
