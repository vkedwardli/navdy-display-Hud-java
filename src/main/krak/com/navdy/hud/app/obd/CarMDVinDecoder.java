package com.navdy.hud.app.obd;

public class CarMDVinDecoder {
    final public static int APP_VERSION = 1;
    final private static okhttp3.HttpUrl CAR_MD_API_URL;
    final private static String DATA = "data";
    final public static long MAX_SIZE = 5000L;
    final private static String META_CAR_MD_AUTH = "CAR_MD_AUTH";
    final private static String META_CAR_MD_TOKEN = "CAR_MD_TOKEN";
    final public static String VIN = "vin";
    final private static com.navdy.service.library.log.Logger sLogger;
    final private String CAR_MD_AUTH;
    final private String CAR_MD_TOKEN;
    private volatile boolean mDecoding;
    private com.navdy.hud.app.obd.ObdDeviceConfigurationManager mDeviceConfigurationManager;
    private okhttp3.internal.DiskLruCache mDiskLruCache;
    private okhttp3.OkHttpClient mHttpClient;
    @Inject
    com.navdy.service.library.network.http.IHttpManager mHttpManager;
    
    static {
        CAR_MD_API_URL = okhttp3.HttpUrl.parse("https://api2.carmd.com/v2.0/decode");
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.obd.CarMDVinDecoder.class);
    }
    
    public CarMDVinDecoder(com.navdy.hud.app.obd.ObdDeviceConfigurationManager a) {
        this.mDecoding = false;
        mortar.Mortar.inject(com.navdy.hud.app.HudApplication.getAppContext(), this);
        this.mDiskLruCache = okhttp3.internal.DiskLruCache.create(okhttp3.internal.io.FileSystem.SYSTEM, new java.io.File(com.navdy.hud.app.storage.PathManager.getInstance().getCarMdResponseDiskCacheFolder()), 1, 1, 5000L);
        this.mDeviceConfigurationManager = a;
        this.mHttpClient = this.mHttpManager.getClientCopy().readTimeout(120L, java.util.concurrent.TimeUnit.SECONDS).connectTimeout(120L, java.util.concurrent.TimeUnit.SECONDS).build();
        this.CAR_MD_AUTH = com.navdy.service.library.util.CredentialUtil.getCredentials(com.navdy.hud.app.HudApplication.getAppContext(), "CAR_MD_AUTH");
        this.CAR_MD_TOKEN = com.navdy.service.library.util.CredentialUtil.getCredentials(com.navdy.hud.app.HudApplication.getAppContext(), "CAR_MD_TOKEN");
    }
    
    static String access$000(com.navdy.hud.app.obd.CarMDVinDecoder a, String s) {
        return a.getFromCache(s);
    }
    
    static com.navdy.service.library.log.Logger access$100() {
        return sLogger;
    }
    
    static com.navdy.hud.app.obd.ObdDeviceConfigurationManager access$200(com.navdy.hud.app.obd.CarMDVinDecoder a) {
        return a.mDeviceConfigurationManager;
    }
    
    static boolean access$302(com.navdy.hud.app.obd.CarMDVinDecoder a, boolean b) {
        a.mDecoding = b;
        return b;
    }
    
    static boolean access$400(com.navdy.hud.app.obd.CarMDVinDecoder a) {
        return a.isConnected();
    }
    
    static okhttp3.HttpUrl access$500() {
        return CAR_MD_API_URL;
    }
    
    static String access$600(com.navdy.hud.app.obd.CarMDVinDecoder a) {
        return a.CAR_MD_AUTH;
    }
    
    static String access$700(com.navdy.hud.app.obd.CarMDVinDecoder a) {
        return a.CAR_MD_TOKEN;
    }
    
    static void access$800(com.navdy.hud.app.obd.CarMDVinDecoder a, okhttp3.ResponseBody a0) {
        a.parseResponse(a0);
    }
    
    static okhttp3.OkHttpClient access$900(com.navdy.hud.app.obd.CarMDVinDecoder a) {
        return a.mHttpClient;
    }
    
    private String getFromCache(String s) {
        String s0 = null;
        try {
            okhttp3.internal.DiskLruCache$Snapshot a = this.mDiskLruCache.get(s.toLowerCase());
            if (a != null) {
                okio.BufferedSource a0 = okio.Okio.buffer(a.getSource(0));
                s0 = a0.readUtf8();
                a0.close();
            } else {
                s0 = null;
            }
        } catch(java.io.IOException a1) {
            sLogger.d("Exception while retrieving response from cache", (Throwable)a1);
            s0 = null;
        }
        return s0;
    }
    
    private boolean isConnected() {
        boolean b = false;
        boolean b0 = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().isRemoteDeviceConnected();
        boolean b1 = com.navdy.hud.app.framework.network.NetworkStateManager.isConnectedToNetwork(com.navdy.hud.app.HudApplication.getAppContext());
        label2: {
            label0: {
                label1: {
                    if (!b0) {
                        break label1;
                    }
                    if (b1) {
                        break label0;
                    }
                }
                b = false;
                break label2;
            }
            b = true;
        }
        return b;
    }
    
    private void parseResponse(okhttp3.ResponseBody a) {
        label1: {
            java.io.IOException a0 = null;
            if (a == null) {
                break label1;
            }
            label0: {
                org.json.JSONException a1 = null;
                try {
                    try {
                        org.json.JSONObject a2 = new org.json.JSONObject(a.string()).getJSONObject("data");
                        if (a2 == null) {
                            break label1;
                        }
                        String s = a2.toString();
                        com.navdy.hud.app.obd.CarDetails a3 = com.navdy.hud.app.obd.CarDetails.fromJson(s);
                        if (a3 == null) {
                            sLogger.e("Error parsing the CarMD response to car details");
                            break label1;
                        } else {
                            this.putToCache(a3.vin, s);
                            this.mDeviceConfigurationManager.setDecodedCarDetails(a3);
                            break label1;
                        }
                    } catch(org.json.JSONException a4) {
                        a1 = a4;
                    }
                } catch(java.io.IOException a5) {
                    a0 = a5;
                    break label0;
                }
                sLogger.e("Error parsing the response body ", (Throwable)a1);
                break label1;
            }
            sLogger.e("IO Error parsing the response body ", (Throwable)a0);
        }
    }
    
    private void putToCache(String s, String s0) {
        try {
            okhttp3.internal.DiskLruCache$Editor a = this.mDiskLruCache.edit(s.toLowerCase());
            okio.BufferedSink a0 = okio.Okio.buffer(a.newSink(0));
            a0.writeUtf8(s0);
            a0.close();
            a.commit();
        } catch(java.io.IOException a1) {
            sLogger.e("Exception while putting into cache ", (Throwable)a1);
        }
    }
    
    public void decodeVin(String s) {
        synchronized(this) {
            if (!this.mDecoding) {
                this.mDecoding = true;
                com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.obd.CarMDVinDecoder$1(this, s), 1);
            }
        }
        /*monexit(this)*/;
    }
}
