package com.navdy.hud.app.view;

public class FactoryResetView extends android.widget.RelativeLayout implements com.navdy.hud.app.manager.InputManager$IInputHandler {
    @InjectView(R.id.factory_reset_confirmation)
    com.navdy.hud.app.ui.component.ConfirmationLayout factoryResetConfirmation;
    @Inject
    com.navdy.hud.app.screen.FactoryResetScreen$Presenter presenter;
    
    public FactoryResetView(android.content.Context a) {
        this(a, (android.util.AttributeSet)null);
    }
    
    public FactoryResetView(android.content.Context a, android.util.AttributeSet a0) {
        this(a, a0, 0);
    }
    
    public FactoryResetView(android.content.Context a, android.util.AttributeSet a0, int i) {
        super(a, a0, i);
        if (!this.isInEditMode()) {
            mortar.Mortar.inject(a, this);
        }
    }
    
    public com.navdy.hud.app.ui.component.ConfirmationLayout getConfirmation() {
        return this.factoryResetConfirmation;
    }
    
    public com.navdy.hud.app.manager.InputManager$IInputHandler nextHandler() {
        return com.navdy.hud.app.manager.InputManager.nextContainingHandler((android.view.View)this);
    }
    
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (this.presenter != null) {
            this.presenter.takeView(this);
        }
    }
    
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (this.presenter != null) {
            this.presenter.dropView((android.view.View)this);
        }
    }
    
    protected void onFinishInflate() {
        super.onFinishInflate();
        butterknife.ButterKnife.inject((android.view.View)this);
    }
    
    public boolean onGesture(com.navdy.service.library.events.input.GestureEvent a) {
        return false;
    }
    
    public boolean onKey(com.navdy.hud.app.manager.InputManager$CustomKeyEvent a) {
        return this.presenter.handleKey(a);
    }
}
