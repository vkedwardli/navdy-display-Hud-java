package com.navdy.hud.app.view;

final public class DialManagerView$$InjectAdapter extends dagger.internal.Binding implements dagger.MembersInjector {
    private dagger.internal.Binding presenter;
    private dagger.internal.Binding sharedPreferences;
    
    public DialManagerView$$InjectAdapter() {
        super((String)null, "members/com.navdy.hud.app.view.DialManagerView", false, com.navdy.hud.app.view.DialManagerView.class);
    }
    
    public void attach(dagger.internal.Linker a) {
        this.presenter = a.requestBinding("com.navdy.hud.app.screen.DialManagerScreen$Presenter", com.navdy.hud.app.view.DialManagerView.class, (this).getClass().getClassLoader());
        this.sharedPreferences = a.requestBinding("android.content.SharedPreferences", com.navdy.hud.app.view.DialManagerView.class, (this).getClass().getClassLoader());
    }
    
    public void getDependencies(java.util.Set a, java.util.Set a0) {
        a0.add(this.presenter);
        a0.add(this.sharedPreferences);
    }
    
    public void injectMembers(com.navdy.hud.app.view.DialManagerView a) {
        a.presenter = (com.navdy.hud.app.screen.DialManagerScreen$Presenter)this.presenter.get();
        a.sharedPreferences = (android.content.SharedPreferences)this.sharedPreferences.get();
    }
    
    public void injectMembers(Object a) {
        this.injectMembers((com.navdy.hud.app.view.DialManagerView)a);
    }
}
