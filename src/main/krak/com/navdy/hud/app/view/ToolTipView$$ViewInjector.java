package com.navdy.hud.app.view;

public class ToolTipView$$ViewInjector {
    public ToolTipView$$ViewInjector() {
    }
    
    public static void inject(butterknife.ButterKnife$Finder a, com.navdy.hud.app.view.ToolTipView a0, Object a1) {
        a0.toolTipContainer = (android.view.ViewGroup)a.findRequiredView(a1, R.id.tooltip_container, "field 'toolTipContainer'");
        a0.toolTipTextView = (android.widget.TextView)a.findRequiredView(a1, R.id.tooltip_text, "field 'toolTipTextView'");
        a0.toolTipScrim = a.findRequiredView(a1, R.id.tooltip_scrim, "field 'toolTipScrim'");
        a0.toolTipTriangle = a.findRequiredView(a1, R.id.tooltip_triangle, "field 'toolTipTriangle'");
    }
    
    public static void reset(com.navdy.hud.app.view.ToolTipView a) {
        a.toolTipContainer = null;
        a.toolTipTextView = null;
        a.toolTipScrim = null;
        a.toolTipTriangle = null;
    }
}
