package com.navdy.hud.app.view;

public class WelcomeView$$ViewInjector {
    public WelcomeView$$ViewInjector() {
    }
    
    public static void inject(butterknife.ButterKnife$Finder a, com.navdy.hud.app.view.WelcomeView a0, Object a1) {
        a0.titleView = (android.widget.TextView)a.findRequiredView(a1, R.id.title, "field 'titleView'");
        a0.subTitleView = (android.widget.TextView)a.findRequiredView(a1, R.id.subTitle, "field 'subTitleView'");
        a0.messageView = (android.widget.TextView)a.findRequiredView(a1, R.id.message, "field 'messageView'");
        a0.animatorView = (com.navdy.hud.app.ui.component.FluctuatorAnimatorView)a.findRequiredView(a1, R.id.animatorView, "field 'animatorView'");
        a0.carousel = (com.navdy.hud.app.ui.component.carousel.CarouselLayout)a.findRequiredView(a1, R.id.carousel, "field 'carousel'");
        a0.rootContainer = a.findRequiredView(a1, R.id.rootContainer, "field 'rootContainer'");
        a0.downloadAppContainer = (android.widget.RelativeLayout)a.findRequiredView(a1, R.id.download_app, "field 'downloadAppContainer'");
        a0.downloadAppTextView1 = (android.widget.TextView)a.findRequiredView(a1, R.id.download_text_1, "field 'downloadAppTextView1'");
        a0.downloadAppTextView2 = (android.widget.TextView)a.findRequiredView(a1, R.id.download_text_2, "field 'downloadAppTextView2'");
        a0.appStoreImage1 = (android.widget.ImageView)a.findRequiredView(a1, R.id.app_store_img_1, "field 'appStoreImage1'");
        a0.appStoreImage2 = (android.widget.ImageView)a.findRequiredView(a1, R.id.app_store_img_2, "field 'appStoreImage2'");
        a0.playStoreImage1 = (android.widget.ImageView)a.findRequiredView(a1, R.id.play_store_img_1, "field 'playStoreImage1'");
        a0.playStoreImage2 = (android.widget.ImageView)a.findRequiredView(a1, R.id.play_store_img_2, "field 'playStoreImage2'");
        a0.choiceLayout = (com.navdy.hud.app.ui.component.ChoiceLayout)a.findRequiredView(a1, R.id.choiceLayout, "field 'choiceLayout'");
        a0.leftDot = (android.widget.ImageView)a.findRequiredView(a1, R.id.leftDot, "field 'leftDot'");
        a0.rightDot = (android.widget.ImageView)a.findRequiredView(a1, R.id.rightDot, "field 'rightDot'");
    }
    
    public static void reset(com.navdy.hud.app.view.WelcomeView a) {
        a.titleView = null;
        a.subTitleView = null;
        a.messageView = null;
        a.animatorView = null;
        a.carousel = null;
        a.rootContainer = null;
        a.downloadAppContainer = null;
        a.downloadAppTextView1 = null;
        a.downloadAppTextView2 = null;
        a.appStoreImage1 = null;
        a.appStoreImage2 = null;
        a.playStoreImage1 = null;
        a.playStoreImage2 = null;
        a.choiceLayout = null;
        a.leftDot = null;
        a.rightDot = null;
    }
}
