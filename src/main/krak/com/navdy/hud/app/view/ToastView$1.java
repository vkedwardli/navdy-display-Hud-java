package com.navdy.hud.app.view;

class ToastView$1 implements Runnable {
    final com.navdy.hud.app.view.ToastView this$0;
    final String val$id;
    final String val$screenName;
    final String val$tts;
    
    ToastView$1(com.navdy.hud.app.view.ToastView a, String s, String s0, String s1) {
        super();
        this.this$0 = a;
        this.val$tts = s;
        this.val$id = s0;
        this.val$screenName = s1;
    }
    
    public void run() {
        if (this.val$tts != null) {
            com.navdy.hud.app.framework.voice.TTSUtils.sendSpeechRequest(this.val$tts, this.val$id);
        }
        String s = this.val$screenName;
        label0: {
            Throwable a = null;
            if (s == null) {
                break label0;
            }
            try {
                com.navdy.hud.app.view.ToastView.access$000().v(new StringBuilder().append("in dismiss screen:").append(this.val$screenName).toString());
                com.navdy.hud.app.view.ToastView.access$100(this.this$0).post(new com.navdy.service.library.events.ui.ShowScreen$Builder().screen(com.navdy.service.library.events.ui.Screen.valueOf(this.val$screenName)).build());
                com.navdy.hud.app.framework.toast.ToastPresenter.clearScreenName();
                break label0;
            } catch(Throwable a0) {
                a = a0;
            }
            com.navdy.hud.app.view.ToastView.access$000().e(a);
        }
        com.navdy.hud.app.view.ToastView.access$100(this.this$0).post(new com.navdy.hud.app.framework.toast.ToastManager$ShowToast(com.navdy.hud.app.framework.toast.ToastPresenter.getCurrentId()));
        com.navdy.hud.app.view.ToastView.access$202(this.this$0, true);
    }
}
