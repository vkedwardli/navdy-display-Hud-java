package com.navdy.hud.app.view.drawable;
import com.navdy.hud.app.R;

public class TachometerGaugeDrawable extends com.navdy.hud.app.view.drawable.GaugeDrawable {
    final private static float GAUGE_SLICES_START_ANGLE = 146.8f;
    final private static float GAUGE_SLICE_ANGLE = 30.8f;
    final private static float GAUGE_START_ANGLE = 146.8f;
    final private static int GROOVES = 8;
    final private static float LAST_MAX_MARKER_SWEEP_ANGLE = 2f;
    final private static float TOTAL_SWEEP_ANGLE = 246.4f;
    private int fullModeGaugeBackgroundBottomOffset;
    private int fullModeGaugeInternalMarginBottom;
    private int fullModeGaugeInternalMarginLeft;
    private int fullModeGaugeInternalMarginRight;
    private int fullModeGaugeInternalMarginTop;
    private float gaugeBackgroundBorderWidth;
    private int gaugeBackgroundBottomOffset;
    private int gaugeInternalMarginBottom;
    private int gaugeInternalMarginLeft;
    private int gaugeInternalMarginRight;
    private int gaugeInternalMarginTop;
    private int grooveTextOffset;
    private float grooveTextSize;
    private int maxMarkerAlpha;
    private int maxMarkerColor;
    private int maxMarkerValue;
    private int regularColor;
    private int rpmWarningLevel;
    android.graphics.drawable.Drawable tachoMeterBackgroundDrawable;
    private int valueRingWidth;
    private int warningColor;
    
    public TachometerGaugeDrawable(android.content.Context a, int i) {
        super(a, i);
        this.maxMarkerAlpha = 0;
        this.fullModeGaugeInternalMarginTop = a.getResources().getDimensionPixelSize(R.dimen.tachometer_full_mode_gauge_margin_top);
        this.fullModeGaugeInternalMarginLeft = a.getResources().getDimensionPixelSize(R.dimen.tachometer_full_mode_gauge_margin_left);
        this.fullModeGaugeInternalMarginBottom = a.getResources().getDimensionPixelSize(R.dimen.tachometer_full_mode_gauge_margin_bottom);
        this.fullModeGaugeInternalMarginRight = a.getResources().getDimensionPixelSize(R.dimen.tachometer_full_mode_gauge_margin_right);
        this.fullModeGaugeBackgroundBottomOffset = a.getResources().getDimensionPixelSize(R.dimen.tachometer_full_mode_gauge_background_bottom_offset);
        this.gaugeBackgroundBorderWidth = a.getResources().getDimension(R.dimen.tachometer_gauge_background_border_width);
        this.regularColor = a.getResources().getColor(R.color.tachometer_color);
        this.warningColor = a.getResources().getColor(R.color.tachometer_warning_color);
        this.maxMarkerColor = a.getResources().getColor(R.color.cyan);
        this.valueRingWidth = a.getResources().getDimensionPixelSize(R.dimen.tachometer_guage_inner_bar_width);
        this.tachoMeterBackgroundDrawable = a.getResources().getDrawable(R.drawable.asset_tachometer_background);
        this.grooveTextSize = (float)a.getResources().getDimensionPixelSize(R.dimen.tachometer_gauge_groove_text_size);
        this.grooveTextOffset = a.getResources().getDimensionPixelSize(R.dimen.tachometer_gauge_groove_text_offset);
        this.gaugeInternalMarginTop = this.fullModeGaugeInternalMarginTop;
        this.gaugeInternalMarginLeft = this.fullModeGaugeInternalMarginLeft;
        this.gaugeInternalMarginBottom = this.fullModeGaugeInternalMarginBottom;
        this.gaugeInternalMarginRight = this.fullModeGaugeInternalMarginRight;
        this.gaugeBackgroundBottomOffset = this.fullModeGaugeBackgroundBottomOffset;
    }
    
    public TachometerGaugeDrawable(android.content.Context a, int i, int i0) {
        super(a, i, i0);
        this.maxMarkerAlpha = 0;
    }
    
    public void draw(android.graphics.Canvas a) {
        super.draw(a);
        android.graphics.Rect a0 = this.getBounds();
        android.graphics.Rect a1 = new android.graphics.Rect(a0.left + this.gaugeInternalMarginLeft, a0.top + this.gaugeInternalMarginTop, a0.right - this.gaugeInternalMarginRight, a0.bottom - this.gaugeInternalMarginBottom);
        android.graphics.Rect a2 = new android.graphics.Rect(a1.left, a1.top, a1.right, a1.bottom - this.gaugeBackgroundBottomOffset);
        this.tachoMeterBackgroundDrawable.setBounds(a2);
        this.tachoMeterBackgroundDrawable.draw(a);
        float f = (float)(this.valueRingWidth / 2) + this.gaugeBackgroundBorderWidth;
        android.graphics.RectF a3 = new android.graphics.RectF((float)a1.left + f, (float)a1.top + f, (float)a1.right - f, (float)a1.bottom - f);
        float f0 = this.mValue / this.mMaxValue;
        this.mPaint.setAntiAlias(true);
        this.mPaint.setStrokeWidth((float)this.valueRingWidth);
        this.mPaint.setStyle(android.graphics.Paint$Style.STROKE);
        int i = (this.mValue >= (float)this.rpmWarningLevel) ? this.warningColor : this.regularColor;
        this.mPaint.setColor(i);
        a.drawArc(a3, 146.8f, f0 * 246.4f, false, this.mPaint);
        int i0 = a1.width() / 2 + this.grooveTextOffset;
        this.mPaint.setStyle(android.graphics.Paint$Style.FILL);
        this.mPaint.setColor(i);
        this.mPaint.setTextSize(this.grooveTextSize);
        this.mPaint.setTypeface(android.graphics.Typeface.create(android.graphics.Typeface.DEFAULT, 1));
        float f1 = this.mMaxValue / 8f;
        int i1 = (int)Math.floor((double)(this.mValue / f1));
        int i2 = (i1 >= 8) ? i1 + 1 : i1 + 2;
        while(i1 < i2) {
            float f2 = 0.0f;
            float f3 = 0.0f;
            float f4 = 146.8f + (float)i1 * 30.8f;
            String s = Integer.toString(i1);
            android.graphics.Rect a4 = new android.graphics.Rect();
            this.mPaint.getTextBounds(s, 0, s.length(), a4);
            label0: {
                label1: {
                    if (i1 == 0) {
                        break label1;
                    }
                    if (i1 != 8) {
                        f2 = 0.0f;
                        f3 = 0.0f;
                        break label0;
                    }
                }
                f3 = (float)a4.height();
                f2 = (i1 != 0) ? (float)(-a4.width()) : (float)a4.width();
            }
            int i3 = (int)((double)(a1.left - this.grooveTextOffset + i0) + (double)i0 * Math.cos(Math.toRadians((double)f4)) - (double)f2);
            int i4 = (int)((double)(a1.top - this.grooveTextOffset + i0) + (double)i0 * Math.sin(Math.toRadians((double)f4)) - (double)f3);
            int i5 = a4.width();
            int i6 = a4.height();
            a.drawText(Integer.toString(i1), (float)(i3 - i5 / 2), (float)(i6 / 2 + i4), this.mPaint);
            i1 = i1 + 1;
        }
        if ((float)this.maxMarkerValue > this.mValue) {
            int i7 = this.mPaint.getAlpha();
            this.mPaint.setColor(this.maxMarkerColor);
            this.mPaint.setAlpha(this.maxMarkerAlpha);
            this.mPaint.setStrokeWidth((float)this.valueRingWidth);
            this.mPaint.setStyle(android.graphics.Paint$Style.STROKE);
            a.drawArc(a3, 146.8f + (float)this.maxMarkerValue / this.mMaxValue * 246.4f - 2f, 2f, false, this.mPaint);
            this.mPaint.setAlpha(i7);
        }
    }
    
    public int getMaxMarkerAlpha() {
        return this.maxMarkerAlpha;
    }
    
    public int getMaxMarkerValue() {
        return this.maxMarkerValue;
    }
    
    public void setMaxMarkerAlpha(int i) {
        this.maxMarkerAlpha = i;
    }
    
    public void setMaxMarkerValue(int i) {
        this.maxMarkerValue = i;
    }
    
    public void setRPMWarningLevel(int i) {
        this.rpmWarningLevel = i;
    }
}
