package com.navdy.hud.app.view;

class NotificationView$1 implements com.navdy.hud.app.gesture.MultipleClickGestureDetector$IMultipleClickKeyGesture {
    final com.navdy.hud.app.view.NotificationView this$0;
    
    NotificationView$1(com.navdy.hud.app.view.NotificationView a) {
        super();
        this.this$0 = a;
    }
    
    public com.navdy.hud.app.manager.InputManager$IInputHandler nextHandler() {
        return null;
    }
    
    public boolean onGesture(com.navdy.service.library.events.input.GestureEvent a) {
        return false;
    }
    
    public boolean onKey(com.navdy.hud.app.manager.InputManager$CustomKeyEvent a) {
        boolean b = false;
        label2: if (com.navdy.hud.app.view.NotificationView.access$100(this.this$0) == null) {
            b = false;
        } else {
            boolean b0 = com.navdy.hud.app.view.NotificationView.access$200(this.this$0).isExpanded();
            label0: {
                label1: {
                    if (b0) {
                        break label1;
                    }
                    if (!com.navdy.hud.app.view.NotificationView.access$200(this.this$0).isExpandedNotificationVisible()) {
                        break label0;
                    }
                }
                b = com.navdy.hud.app.view.NotificationView.access$200(this.this$0).handleKey(a);
                break label2;
            }
            b = com.navdy.hud.app.view.NotificationView.access$100(this.this$0).onKey(a);
        }
        return b;
    }
    
    public void onMultipleClick(int i) {
        com.navdy.hud.app.view.NotificationView.access$000(this.this$0, false);
    }
}
