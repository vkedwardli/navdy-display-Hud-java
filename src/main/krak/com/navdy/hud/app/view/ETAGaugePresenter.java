package com.navdy.hud.app.view;
import com.navdy.hud.app.R;

public class ETAGaugePresenter extends com.navdy.hud.app.view.DashboardWidgetPresenter {
    final static boolean $assertionsDisabled;
    final private static long TRIP_INFO_UPDATE_INTERVAL;
    @InjectView(R.id.txt_eta_am_pm)
    android.widget.TextView etaAmPmText;
    private com.navdy.hud.app.view.drawable.ETAProgressDrawable etaProgressDrawable;
    @InjectView(R.id.txt_eta)
    android.widget.TextView etaText;
    private String etaTripGaugeName;
    @InjectView(R.id.eta_view)
    android.view.ViewGroup etaView;
    private int farMargin;
    private String feetLabel;
    private int fontSize18;
    private int fontSize30;
    private int gravity;
    private int greyColor;
    private boolean isNavigationActive;
    private String kiloMetersLabel;
    private String[] labelShort;
    private String[] labels;
    private android.os.Handler mHandler;
    private Runnable mTripInfoUpdateRunnable;
    private com.navdy.hud.app.framework.trips.TripManager mTripManager;
    private com.navdy.hud.app.maps.MapEvents$ManeuverDisplay maneuverDisplay;
    private String metersLabel;
    private String milesLabel;
    private int nearMargin;
    @InjectView(R.id.txt_remaining_distance)
    android.widget.TextView remainingDistanceText;
    @InjectView(R.id.txt_remaining_distance_unit)
    android.widget.TextView remainingDistanceUnitText;
    @InjectView(R.id.tripDistance)
    android.widget.TextView tripDistanceView;
    private int tripGaugeIconMargin;
    private int tripGaugeLongMargin;
    private int tripGaugeShortMargin;
    @InjectView(R.id.tripIcon)
    android.widget.ImageView tripIconView;
    @InjectView(R.id.tripOpenMap)
    android.support.constraint.ConstraintLayout tripOpenMapView;
    @InjectView(R.id.tripTime)
    android.widget.TextView tripTimeView;
    @InjectView(R.id.txt_tta1)
    android.widget.TextView ttaText1;
    @InjectView(R.id.txt_tta2)
    android.widget.TextView ttaText2;
    @InjectView(R.id.txt_tta3)
    android.widget.TextView ttaText3;
    @InjectView(R.id.txt_tta4)
    android.widget.TextView ttaText4;
    
    static {
        $assertionsDisabled = !com.navdy.hud.app.view.ETAGaugePresenter.class.desiredAssertionStatus();
        TRIP_INFO_UPDATE_INTERVAL = java.util.concurrent.TimeUnit.MINUTES.toMillis(1L);
    }
    
    public ETAGaugePresenter(android.content.Context a) {
        android.content.res.Resources a0 = a.getResources();
        this.mTripManager = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getTripManager();
        this.labels = a0.getStringArray(R.array.time_labels);
        this.labelShort = a0.getStringArray(R.array.time_labels_short);
        this.metersLabel = a.getResources().getString(R.string.unit_meters);
        this.kiloMetersLabel = a.getResources().getString(R.string.unit_kilometers);
        this.feetLabel = a.getResources().getString(R.string.unit_feet);
        this.milesLabel = a.getResources().getString(R.string.unit_miles);
        this.farMargin = a0.getDimensionPixelSize(R.dimen.far_margin);
        this.nearMargin = a0.getDimensionPixelSize(R.dimen.near_margin);
        this.etaTripGaugeName = a.getResources().getString(R.string.widget_trip_eta);
        this.tripGaugeShortMargin = a0.getDimensionPixelSize(R.dimen.trip_gauge_short_margin);
        this.tripGaugeLongMargin = a0.getDimensionPixelSize(R.dimen.trip_gauge_long_margin);
        this.tripGaugeIconMargin = a0.getDimensionPixelSize(R.dimen.trip_gauge_icon_margin);
        this.greyColor = android.support.v4.content.ContextCompat.getColor(a, R.color.grey_ababab);
        this.fontSize18 = a0.getDimensionPixelSize(R.dimen.active_trip_18);
        this.fontSize30 = a0.getDimensionPixelSize(R.dimen.size_30);
        this.mHandler = new android.os.Handler();
        this.mTripInfoUpdateRunnable = (Runnable)new com.navdy.hud.app.view.ETAGaugePresenter$1(this);
        this.etaProgressDrawable = new com.navdy.hud.app.view.drawable.ETAProgressDrawable(a);
        this.etaProgressDrawable.setMinValue(0.0f);
        this.etaProgressDrawable.setMaxGaugeValue(100f);
    }
    
    static Runnable access$000(com.navdy.hud.app.view.ETAGaugePresenter a) {
        return a.mTripInfoUpdateRunnable;
    }
    
    static long access$100() {
        return TRIP_INFO_UPDATE_INTERVAL;
    }
    
    static android.os.Handler access$200(com.navdy.hud.app.view.ETAGaugePresenter a) {
        return a.mHandler;
    }
    
    private void addPadding(android.text.SpannableStringBuilder a) {
        a.append((CharSequence)" ");
        a.append((CharSequence)" ");
        a.append((CharSequence)" ");
    }
    
    private android.text.SpannableStringBuilder getDistance(String s, String s0) {
        android.text.SpannableStringBuilder a = new android.text.SpannableStringBuilder();
        if (this.gravity == 0) {
            this.addPadding(a);
        }
        a.append((CharSequence)s);
        int i = a.length();
        a.setSpan(new android.text.style.AbsoluteSizeSpan(this.fontSize18), 0, i, 34);
        a.setSpan(new android.text.style.ForegroundColorSpan(-1), 0, i, 34);
        a.append((CharSequence)" ");
        int i0 = a.length();
        a.append((CharSequence)s0);
        int i1 = a.length();
        a.setSpan(new android.text.style.AbsoluteSizeSpan(this.fontSize18), i0, i1, 34);
        a.setSpan(new android.text.style.ForegroundColorSpan(this.greyColor), i0, i1, 34);
        if (this.gravity == 2) {
            this.addPadding(a);
        }
        return a;
    }
    
    private android.text.SpannableStringBuilder getTime(String[] a) {
        android.text.SpannableStringBuilder a0 = new android.text.SpannableStringBuilder();
        if (this.gravity == 0) {
            this.addPadding(a0);
        }
        int i = 0;
        while(i < a.length) {
            if (i != 0) {
                a0.append((CharSequence)" ");
            }
            String s = a[i];
            String s0 = a[i + 1];
            i = i + 2;
            int i0 = a0.length();
            a0.append((CharSequence)s);
            int i1 = a0.length();
            a0.setSpan(new android.text.style.AbsoluteSizeSpan(this.fontSize30), i0, i1, 34);
            a0.setSpan(new android.text.style.ForegroundColorSpan(-1), i0, i1, 34);
            a0.append((CharSequence)" ");
            int i2 = a0.length();
            a0.append((CharSequence)s0);
            int i3 = a0.length();
            a0.setSpan(new android.text.style.AbsoluteSizeSpan(this.fontSize18), i2, i3, 34);
            a0.setSpan(new android.text.style.ForegroundColorSpan(this.greyColor), i2, i3, 34);
        }
        if (this.gravity == 2) {
            this.addPadding(a0);
        }
        return a0;
    }
    
    private boolean parseSeconds(long j, int[] a, byte[] a0) {
        a[3] = (int)java.util.concurrent.TimeUnit.SECONDS.toDays(j);
        a[2] = (int)(java.util.concurrent.TimeUnit.SECONDS.toHours(j) - (long)(a[3] * 24));
        a[1] = (int)(java.util.concurrent.TimeUnit.SECONDS.toMinutes(j) - java.util.concurrent.TimeUnit.SECONDS.toHours(j) * 60L);
        a[0] = (int)(j - java.util.concurrent.TimeUnit.SECONDS.toMinutes(j) * 60L);
        a0[0] = (byte)0;
        a0[1] = (byte)0;
        int i = 3;
        while(true) {
            label2: if (i > 0) {
                boolean b = a[i] != 0;
                label0: {
                    label1: {
                        if (b > 0) {
                            break label1;
                        }
                        int i0 = a0[0];
                        if (i0 == 0) {
                            break label0;
                        }
                        break label2;
                    }
                    int i1 = (byte)i;
                    a0[1] = (byte)i1;
                    int i2 = a0[0];
                    if (i2 == 0) {
                        int i3 = (byte)i;
                        a0[0] = (byte)i3;
                    }
                }
                i = (byte)(i - 1);
                continue;
            }
            if (!$assertionsDisabled) {
                int i4 = a0[0];
                int i5 = a0[1];
                if (i4 != i5) {
                    int i6 = a0[0];
                    int i7 = a0[1];
                    if (i6 != i7 + 1) {
                        throw new AssertionError();
                    }
                }
            }
            int i8 = a0[0];
            int i9 = a0[1];
            return i8 > i9;
        }
    }
    
    private void setTTAText(long j) {
        long j0 = Math.max(60L, j / 1000L);
        int[] a = new int[4];
        byte[] a0 = new byte[2];
        boolean b = this.parseSeconds(j0, a, a0);
        int i = a0[0];
        int i0 = a0[1];
        if (b) {
            this.ttaText1.setText((CharSequence)Integer.toString((a[i] != 0) ? 1 : 0));
            android.widget.TextView a1 = this.ttaText2;
            int i1 = this.labels[i].charAt(0);
            a1.setText((CharSequence)Character.toString((char)i1));
            this.ttaText3.setVisibility(0);
            this.ttaText4.setVisibility(0);
            this.ttaText3.setText((CharSequence)Integer.toString((a[i0] != 0) ? 1 : 0));
            android.widget.TextView a2 = this.ttaText4;
            int i2 = this.labels[i0].charAt(0);
            a2.setText((CharSequence)Character.toString((char)i2));
        } else {
            this.ttaText1.setText((CharSequence)Integer.toString((a[i] != 0) ? 1 : 0));
            this.ttaText2.setText((CharSequence)this.labels[i]);
            this.ttaText3.setVisibility(8);
            this.ttaText4.setVisibility(8);
        }
    }
    
    private void setTripTimeText(long j) {
        long j0 = j / 1000L;
        if (j0 != 0L) {
            android.text.SpannableStringBuilder a = null;
            long j1 = Math.max(60L, j0);
            int[] a0 = new int[4];
            byte[] a1 = new byte[2];
            boolean b = this.parseSeconds(j1, a0, a1);
            int i = a1[0];
            int i0 = a1[1];
            if (b) {
                String[] a2 = new String[4];
                a2[0] = Integer.toString((a0[i] != 0) ? 1 : 0);
                int i1 = this.labelShort[i].charAt(0);
                a2[1] = String.valueOf((char)i1);
                a2[2] = Integer.toString((a0[i0] != 0) ? 1 : 0);
                int i2 = this.labelShort[i0].charAt(0);
                a2[3] = String.valueOf((char)i2);
                a = this.getTime(a2);
            } else {
                String[] a3 = new String[2];
                a3[0] = Integer.toString((a0[i] != 0) ? 1 : 0);
                a3[1] = String.valueOf(this.labels[i]);
                a = this.getTime(a3);
            }
            this.tripTimeView.setText((CharSequence)a);
        } else {
            this.tripDistanceView.setVisibility(8);
            android.widget.TextView a4 = this.tripTimeView;
            String[] a5 = new String[2];
            a5[0] = String.valueOf(0);
            a5[1] = this.labelShort[0];
            a4.setText((CharSequence)this.getTime(a5));
        }
    }
    
    public android.graphics.drawable.Drawable getDrawable() {
        return this.etaProgressDrawable;
    }
    
    public String getWidgetIdentifier() {
        return "ETA_GAUGE_ID";
    }
    
    public String getWidgetName() {
        return this.etaTripGaugeName;
    }
    
    protected boolean isRegisteringToBusRequired() {
        return true;
    }
    
    public void onManeuverDisplay(com.navdy.hud.app.maps.MapEvents$ManeuverDisplay a) {
        if (!a.isEmpty()) {
            com.navdy.hud.app.ui.framework.UIStateManager a0 = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getUiStateManager();
            this.maneuverDisplay = a;
            if (this.maneuverDisplay == null) {
                this.isNavigationActive = a0.isNavigationActive();
            } else {
                this.isNavigationActive = this.maneuverDisplay.isNavigating();
            }
            this.reDraw();
        }
    }
    
    public void onNavigationModeChange(com.navdy.hud.app.maps.NavigationMode a) {
        this.isNavigationActive = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getUiStateManager().isNavigationActive();
        this.maneuverDisplay = null;
        this.reDraw();
    }
    
    public void setView(com.navdy.hud.app.view.DashboardWidgetView a, android.os.Bundle a0) {
        if (a == null) {
            super.setView(a, a0);
            this.reDraw();
        } else {
            this.gravity = (a0 == null) ? 0 : a0.getInt("EXTRA_GRAVITY", 0);
            a.setContentView(R.layout.eta_gauge_layout);
            butterknife.ButterKnife.inject(this, (android.view.View)a);
            android.view.ViewGroup$MarginLayoutParams a1 = (android.view.ViewGroup$MarginLayoutParams)this.etaView.getLayoutParams();
            android.support.constraint.ConstraintSet a2 = new android.support.constraint.ConstraintSet();
            a2.clone(this.tripOpenMapView);
            int i = this.tripOpenMapView.getId();
            int i0 = this.tripIconView.getId();
            switch(this.gravity) {
                case 2: {
                    a1.leftMargin = this.farMargin;
                    a1.rightMargin = this.nearMargin;
                    a2.connect(i0, 7, i, 7, 0);
                    a2.connect(this.tripTimeView.getId(), 2, i0, 1, this.tripGaugeIconMargin);
                    break;
                }
                case 0: {
                    a1.leftMargin = this.nearMargin;
                    a1.rightMargin = this.farMargin;
                    a2.connect(i0, 6, i, 6, 0);
                    a2.connect(this.tripTimeView.getId(), 1, i0, 2, this.tripGaugeIconMargin);
                    break;
                }
            }
            a2.applyTo(this.tripOpenMapView);
            this.etaView.setLayoutParams((android.view.ViewGroup$LayoutParams)a1);
            this.isNavigationActive = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getUiStateManager().isNavigationActive();
            com.navdy.hud.app.maps.here.HereMapsManager.getInstance().postManeuverDisplay();
            super.setView(a, a0);
            this.reDraw();
        }
    }
    
    protected void updateGauge() {
        boolean b = this.isNavigationActive;
        label5: {
            int i = 0;
            label3: {
                String s = null;
                label4: {
                    if (!b) {
                        break label4;
                    }
                    if (this.maneuverDisplay == null) {
                        break label4;
                    }
                    if (this.maneuverDisplay.etaDate == null) {
                        break label4;
                    }
                    if (this.maneuverDisplay.totalDistanceUnit == null) {
                        break label4;
                    }
                    if (this.maneuverDisplay.totalDistanceRemainingUnit != null) {
                        break label3;
                    }
                }
                this.tripOpenMapView.setVisibility(0);
                long j = (this.mTripManager.getCurrentTripStartTime() <= 0L) ? 0L : System.currentTimeMillis() - this.mTripManager.getCurrentTripStartTime();
                long j0 = (long)this.mTripManager.getCurrentDistanceTraveled();
                com.navdy.hud.app.manager.SpeedManager a = com.navdy.hud.app.manager.SpeedManager.getInstance();
                com.navdy.hud.app.maps.util.DistanceConverter$Distance a0 = new com.navdy.hud.app.maps.util.DistanceConverter$Distance();
                com.navdy.hud.app.maps.util.DistanceConverter.convertToDistance(a.getSpeedUnit(), (float)j0, a0);
                switch(com.navdy.hud.app.view.ETAGaugePresenter$2.$SwitchMap$com$navdy$service$library$events$navigation$DistanceUnit[a0.unit.ordinal()]) {
                    case 4: {
                        s = this.feetLabel;
                        break;
                    }
                    case 3: {
                        s = this.milesLabel;
                        break;
                    }
                    case 2: {
                        s = this.kiloMetersLabel;
                        break;
                    }
                    case 1: {
                        s = this.metersLabel;
                        break;
                    }
                    default: {
                        s = "";
                    }
                }
                this.tripDistanceView.setText((CharSequence)this.getDistance(Float.toString(a0.value), s));
                this.tripDistanceView.setVisibility(0);
                this.setTripTimeText(j);
                this.mHandler.removeCallbacks(this.mTripInfoUpdateRunnable);
                this.mHandler.postDelayed(this.mTripInfoUpdateRunnable, TRIP_INFO_UPDATE_INTERVAL);
                this.etaView.setVisibility(8);
                break label5;
            }
            this.mHandler.removeCallbacks(this.mTripInfoUpdateRunnable);
            this.tripOpenMapView.setVisibility(8);
            this.etaView.setVisibility(0);
            long j1 = System.currentTimeMillis();
            long j2 = this.maneuverDisplay.etaDate.getTime();
            this.setTTAText((j2 <= j1) ? 0L : j2 - j1);
            this.etaText.setText((CharSequence)this.maneuverDisplay.eta);
            this.etaAmPmText.setText((CharSequence)this.maneuverDisplay.etaAmPm);
            float f = com.navdy.hud.app.maps.util.DistanceConverter.convertToMeters(this.maneuverDisplay.totalDistance, this.maneuverDisplay.totalDistanceUnit);
            float f0 = f - com.navdy.hud.app.maps.util.DistanceConverter.convertToMeters(this.maneuverDisplay.totalDistanceRemaining, this.maneuverDisplay.totalDistanceRemainingUnit);
            int i0 = (f > 0.0f) ? 1 : (f == 0.0f) ? 0 : -1;
            label2: {
                label0: {
                    label1: {
                        if (i0 <= 0) {
                            break label1;
                        }
                        if (f > f0) {
                            break label0;
                        }
                    }
                    i = 0;
                    break label2;
                }
                i = (int)(f0 / f * 100f);
            }
            this.etaProgressDrawable.setGaugeValue((float)i);
            this.remainingDistanceText.setText((CharSequence)Float.toString(this.maneuverDisplay.totalDistanceRemaining));
            switch(com.navdy.hud.app.view.ETAGaugePresenter$2.$SwitchMap$com$navdy$service$library$events$navigation$DistanceUnit[this.maneuverDisplay.totalDistanceRemainingUnit.ordinal()]) {
                case 4: {
                    this.remainingDistanceUnitText.setText((CharSequence)this.feetLabel);
                    break;
                }
                case 3: {
                    this.remainingDistanceUnitText.setText((CharSequence)this.milesLabel);
                    break;
                }
                case 2: {
                    this.remainingDistanceUnitText.setText((CharSequence)this.kiloMetersLabel);
                    break;
                }
                case 1: {
                    this.remainingDistanceUnitText.setText((CharSequence)this.metersLabel);
                    break;
                }
            }
        }
    }
}
