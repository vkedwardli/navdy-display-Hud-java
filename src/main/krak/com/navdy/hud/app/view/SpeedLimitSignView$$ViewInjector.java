package com.navdy.hud.app.view;

public class SpeedLimitSignView$$ViewInjector {
    public SpeedLimitSignView$$ViewInjector() {
    }
    
    public static void inject(butterknife.ButterKnife$Finder a, com.navdy.hud.app.view.SpeedLimitSignView a0, Object a1) {
        a0.usSpeedLimitSignView = (android.view.ViewGroup)a.findRequiredView(a1, R.id.speed_limit_sign_us, "field 'usSpeedLimitSignView'");
        a0.euSpeedLimitSignView = (android.view.ViewGroup)a.findRequiredView(a1, R.id.speed_limit_sign_eu, "field 'euSpeedLimitSignView'");
    }
    
    public static void reset(com.navdy.hud.app.view.SpeedLimitSignView a) {
        a.usSpeedLimitSignView = null;
        a.euSpeedLimitSignView = null;
    }
}
