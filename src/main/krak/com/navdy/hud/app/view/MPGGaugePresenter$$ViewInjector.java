package com.navdy.hud.app.view;
import com.navdy.hud.app.R;

public class MPGGaugePresenter$$ViewInjector {
    public MPGGaugePresenter$$ViewInjector() {
    }
    
    public static void inject(butterknife.ButterKnife$Finder a, com.navdy.hud.app.view.MPGGaugePresenter a0, Object a1) {
        a0.mFuelConsumptionUnit = (android.widget.TextView)a.findRequiredView(a1, R.id.txt_unit, "field 'mFuelConsumptionUnit'");
        a0.mFuelConsumptionValue = (android.widget.TextView)a.findRequiredView(a1, R.id.txt_value, "field 'mFuelConsumptionValue'");
    }
    
    public static void reset(com.navdy.hud.app.view.MPGGaugePresenter a) {
        a.mFuelConsumptionUnit = null;
        a.mFuelConsumptionValue = null;
    }
}
