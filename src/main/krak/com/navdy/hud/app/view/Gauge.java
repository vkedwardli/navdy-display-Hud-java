package com.navdy.hud.app.view;

public class Gauge extends android.view.View implements com.navdy.hud.app.view.SerialValueAnimator$SerialValueAnimatorAdapter {
    final private static int ANIMATION_DURATION = 100;
    final private static int DEFAULT_SHADOW_THICKNESS_DP = 0;
    final private static float DEFAULT_SUB_TEXT_SIZE = 40f;
    final private static float DEFAULT_TEXT_SIZE = 90f;
    final private static int DEFAULT_THICKNESS_DP = 40;
    final private static int DEFAULT_TIC_LENGTH_DP = 4;
    final private static int SHADOW_START_ANGLE = 2;
    final private static int TIC_STYLE_CIRCLE = 2;
    final private static int TIC_STYLE_LINE = 1;
    final private static int TIC_STYLE_NONE = 0;
    final private static com.navdy.service.library.log.Logger sLogger;
    private boolean AntialiasSetting;
    private boolean mAnimateValues;
    protected int mBackgroundColor;
    private android.graphics.Paint mBackgroundPaint;
    protected int mBackgroundThickness;
    protected com.navdy.hud.app.util.CustomDimension mBackgroundThicknessAttribute;
    protected String mCenterSubtext;
    protected String mCenterText;
    protected int mEndColor;
    protected int mMaxValue;
    protected int mMinValue;
    private com.navdy.hud.app.view.SerialValueAnimator mSerialValueAnimator;
    protected int mShadowColor;
    protected int mShadowThickness;
    protected com.navdy.hud.app.util.CustomDimension mShadowThicknessAttribute;
    protected int mStartAngle;
    protected int mStartColor;
    protected float mSubTextSize;
    protected com.navdy.hud.app.util.CustomDimension mSubTextSizeAttribute;
    protected int mSweepAngle;
    protected int mTextColor;
    protected float mTextSize;
    protected com.navdy.hud.app.util.CustomDimension mTextSizeAttribute;
    protected int mThickness;
    protected com.navdy.hud.app.util.CustomDimension mThicknessAttribute;
    protected int mTicColor;
    private int mTicInterval;
    private int mTicLength;
    protected int mTicPadding;
    private android.graphics.Paint mTicPaint;
    protected int mTicStyle;
    protected int mValue;
    protected int mWarningColor;
    protected int mWarningValue;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.view.Gauge.class);
    }
    
    public Gauge(android.content.Context a) {
        this(a, (android.util.AttributeSet)null);
    }
    
    public Gauge(android.content.Context a, android.util.AttributeSet a0) {
        this(a, a0, 0);
    }
    
    public Gauge(android.content.Context a, android.util.AttributeSet a0, int i) {
        super(a, a0, i);
        this.mTicColor = -1;
        this.AntialiasSetting = true;
        this.mAnimateValues = true;
        this.initFromAttributes(a, a0);
        this.initDrawingTools();
        this.mSerialValueAnimator = new com.navdy.hud.app.view.SerialValueAnimator((com.navdy.hud.app.view.SerialValueAnimator$SerialValueAnimatorAdapter)this, 100);
    }
    
    private int chooseDimension(int i, int i0) {
        if (i != -2147483648 && i != 1073741824) {
            i0 = this.getPreferredSize();
        }
        return i0;
    }
    
    private int convertDpToPx(int i) {
        return (int)android.util.TypedValue.applyDimension(1, (float)i, this.getResources().getDisplayMetrics());
    }
    
    private void drawText(android.graphics.Canvas a) {
        String s = this.mCenterText;
        label0: {
            label1: {
                if (s != null) {
                    break label1;
                }
                if (this.mCenterSubtext == null) {
                    break label0;
                }
            }
            android.graphics.Paint a0 = new android.graphics.Paint();
            a0.setColor(this.mTextColor);
            float f = this.mTextSize;
            float f0 = this.mSubTextSize;
            a0.setTextSize(f);
            a0.setTextAlign(android.graphics.Paint$Align.CENTER);
            float f1 = (float)this.getWidth() / 2f;
            float f2 = (float)this.getHeight() / 2f;
            if (this.mCenterText != null) {
                a.drawText(this.mCenterText, f1, f2, a0);
            }
            if (this.mCenterSubtext != null) {
                a0.setTextSize(f0);
                a.drawText(this.mCenterSubtext, f1, f2 + f0 + 10f, a0);
            }
        }
    }
    
    private void drawTics(android.graphics.Canvas a) {
        if (this.mTicStyle != 0) {
            float f = (float)Math.round((float)this.getWidth() / 2f);
            float f0 = (float)Math.round((float)this.getHeight() / 2f);
            float f1 = this.getRadius();
            a.save(1);
            a.rotate((float)this.mStartAngle, f, f0);
            float f2 = (float)this.mStartAngle;
            float f3 = this.deltaToAngle(this.mTicInterval);
            float f4 = (float)(this.mStartAngle + this.mSweepAngle);
            float f5 = f3 / 2f;
            float f6 = this.valueToAngle(this.mValue);
            while(f2 <= f4) {
                int i = 0;
                if (f6 > f2) {
                    i = 255;
                } else {
                    i = (f6 > f2 - f5) ? 128 + Math.round((float)127 * (f6 - (f2 - f5)) / f5) : 128;
                }
                this.mTicPaint.setAlpha(i);
                if (this.mTicStyle != 1) {
                    if (this.mTicStyle == 2) {
                        a.drawCircle(f + f1 - (float)this.mThickness / 2f - (float)this.mTicLength / 2f - (float)this.mTicPadding, f0, (float)this.mTicLength / 2f, this.mTicPaint);
                    }
                } else {
                    a.drawLine(f + f1 - (float)this.mThickness / 2f - (float)this.mTicLength - (float)this.mTicPadding, f0, f + f1 - (float)this.mThickness / 2f - (float)this.mTicPadding, f0, this.mTicPaint);
                }
                a.rotate(f3, f, f0);
                f2 = f2 + f3;
            }
            a.restore();
        }
    }
    
    private void evaluateDimensions() {
        int i = Math.min(this.getWidth(), this.getHeight());
        this.mThickness = (int)this.mThicknessAttribute.getSize((android.view.View)this, (float)i, 0.0f);
        this.mShadowThickness = (int)this.mShadowThicknessAttribute.getSize((android.view.View)this, (float)i, 0.0f);
        if (this.mThickness <= this.mShadowThickness) {
            sLogger.e("Shadow is set to be bigger than gauge's thickness - removing shadow");
            this.mShadowThickness = 0;
        }
        this.mBackgroundThickness = (int)this.mBackgroundThicknessAttribute.getSize((android.view.View)this, (float)i, 0.0f);
        this.mTextSize = this.mTextSizeAttribute.getSize((android.view.View)this, (float)i, 0.0f);
        this.mSubTextSize = this.mSubTextSizeAttribute.getSize((android.view.View)this, (float)i, 0.0f);
    }
    
    private int getPreferredSize() {
        return 300;
    }
    
    private float getRadius(int i) {
        return (float)(Math.min(this.getHeight(), this.getWidth()) - i) / 2f;
    }
    
    private void initFromAttributes(android.content.Context a, android.util.AttributeSet a0) {
        android.content.res.TypedArray a1 = a.getTheme().obtainStyledAttributes(a0, com.navdy.hud.app.R$styleable.Gauge, 0, 0);
        int i = this.convertDpToPx(40);
        int i0 = this.convertDpToPx(0);
        int i1 = this.convertDpToPx(4);
        try {
            this.mMinValue = a1.getInteger(5, 0);
            this.mValue = a1.getInteger(7, 0);
            this.mMaxValue = a1.getInteger(6, 120);
            this.mStartAngle = a1.getInteger(0, 150);
            this.mSweepAngle = a1.getInteger(1, 240);
            this.mTicPadding = a1.getDimensionPixelOffset(20, 0);
            this.mThicknessAttribute = com.navdy.hud.app.util.CustomDimension.getDimension((android.view.View)this, a1, 2, (float)i);
            if (com.navdy.hud.app.util.CustomDimension.hasDimension((android.view.View)this, a1, 3)) {
                this.mBackgroundThicknessAttribute = com.navdy.hud.app.util.CustomDimension.getDimension((android.view.View)this, a1, 3, (float)i);
            } else {
                this.mBackgroundThicknessAttribute = com.navdy.hud.app.util.CustomDimension.getDimension((android.view.View)this, a1, 2, (float)i);
            }
            this.mShadowThicknessAttribute = com.navdy.hud.app.util.CustomDimension.getDimension((android.view.View)this, a1, 4, (float)i0);
            this.mTextSizeAttribute = com.navdy.hud.app.util.CustomDimension.getDimension((android.view.View)this, a1, 14, 90f);
            this.mSubTextSizeAttribute = com.navdy.hud.app.util.CustomDimension.getDimension((android.view.View)this, a1, 15, 40f);
            this.mTicStyle = a1.getInteger(19, 0);
            this.mTicLength = a1.getDimensionPixelSize(21, i1);
            this.mTicInterval = a1.getInteger(22, 10);
            this.mBackgroundColor = a1.getColor(8, -7829368);
            this.mWarningColor = a1.getColor(10, -65536);
            this.mTextColor = a1.getColor(11, -1);
            this.mWarningValue = a1.getInteger(9, 75);
            this.mStartColor = a1.getColor(16, -16777216);
            this.mEndColor = a1.getColor(17, -1);
            this.mShadowColor = a1.getColor(18, -7829368);
            this.mCenterText = a1.getString(12);
            this.mCenterSubtext = a1.getString(13);
        } catch(Throwable a2) {
            a1.recycle();
            throw a2;
        }
        a1.recycle();
        if (this.mMaxValue < this.mMinValue) {
            this.mMaxValue = this.mMinValue + 1;
        }
        if (this.mSweepAngle <= 0) {
            this.mSweepAngle = 1;
        }
        if (this.mTicInterval <= 0) {
            this.mTicInterval = 1;
        }
    }
    
    private float valueToAngle(int i) {
        return (float)this.mStartAngle + (float)this.mSweepAngle * (float)(i - this.mMinValue) / (float)(this.mMaxValue - this.mMinValue);
    }
    
    public void animationComplete(float f) {
    }
    
    public void clearAnimationQueue() {
        this.mSerialValueAnimator.release();
    }
    
    protected float deltaToAngle(int i) {
        return (float)this.mSweepAngle * (float)i / (float)(this.mMaxValue - this.mMinValue);
    }
    
    protected void drawBackground(android.graphics.Canvas a) {
        float f = (float)this.getWidth();
        float f0 = (float)this.getHeight();
        float f1 = this.getRadius();
        android.graphics.Paint a0 = this.mBackgroundPaint;
        android.graphics.RectF a1 = new android.graphics.RectF();
        a1.set(f / 2f - f1, f0 / 2f - f1, f / 2f + f1, f0 / 2f + f1);
        a0.setColor(this.mBackgroundColor);
        float f2 = (float)this.mSweepAngle;
        if (this.mWarningValue != 0) {
            float f3 = this.deltaToAngle(this.mMaxValue - this.mWarningValue);
            f2 = f2 - f3;
            a0.setColor(this.mWarningColor);
            a.drawArc(a1, this.valueToAngle(this.mWarningValue), f3, false, a0);
        }
        a0.setColor(this.mBackgroundColor);
        a.drawArc(a1, (float)this.mStartAngle, f2, false, a0);
    }
    
    protected void drawGauge(android.graphics.Canvas a) {
        this.drawIndicator(a, this.mMinValue, this.mValue);
    }
    
    public void drawIndicator(android.graphics.Canvas a) {
        float f = 0.0f;
        float f0 = 0.0f;
        float f1 = (float)this.getWidth();
        float f2 = (float)this.getHeight();
        float f3 = this.getRadius();
        android.graphics.Paint a0 = new android.graphics.Paint();
        a0.setColor(this.mStartColor);
        a0.setStrokeWidth((float)this.mThickness);
        a0.setAntiAlias(true);
        a0.setStrokeCap(android.graphics.Paint$Cap.BUTT);
        a0.setStyle(android.graphics.Paint$Style.STROKE);
        float f4 = this.deltaToAngle(this.mValue - this.mMinValue);
        if (this.mStartColor != this.mEndColor) {
            int i = this.mStartColor;
            int i0 = this.mStartColor;
            int i1 = this.mEndColor;
            int i2 = this.mEndColor;
            float f5 = (float)this.mStartAngle / 360f;
            float f6 = ((float)this.mStartAngle + f4) / 360f;
            float f7 = f1 / 2f;
            float f8 = f2 / 2f;
            int[] a1 = new int[4];
            a1[0] = i;
            a1[1] = i0;
            a1[2] = i1;
            a1[3] = i2;
            float[] a2 = new float[4];
            a2[0] = 0.0f;
            a2[1] = f5;
            a2[2] = f6;
            a2[3] = 1f;
            a0.setShader((android.graphics.Shader)new android.graphics.SweepGradient(f7, f8, a1, a2));
        }
        float f9 = f1 / 2f - f3;
        float f10 = f2 / 2f - f3;
        float f11 = f1 / 2f + f3;
        float f12 = f2 / 2f + f3;
        if (this.mShadowThickness <= 0) {
            f = 0.0f;
            f0 = 0.0f;
        } else {
            android.graphics.Paint a3 = new android.graphics.Paint(a0);
            a3.setColor(this.mShadowColor);
            a.drawArc(new android.graphics.RectF(f9, f10, f11, f12), (float)this.mStartAngle, f4, false, a3);
            int i3 = this.mThickness - this.mShadowThickness;
            a0.setStrokeWidth((float)i3);
            f = f3 - this.getRadius(i3);
            f0 = 2f;
        }
        if (f4 > f0) {
            android.graphics.RectF a4 = new android.graphics.RectF();
            a4.set(f9 + f, f10 + f, f11 - f, f12 - f);
            a.drawArc(a4, (float)this.mStartAngle + f0, f4 - f0, false, a0);
        }
    }
    
    public void drawIndicator(android.graphics.Canvas a, int i, int i0) {
        float f = 0.0f;
        float f0 = 0.0f;
        float f1 = (float)this.getWidth();
        float f2 = (float)this.getHeight();
        float f3 = this.getRadius();
        android.graphics.Paint a0 = new android.graphics.Paint();
        a0.setColor(this.mStartColor);
        a0.setStrokeWidth((float)this.mThickness);
        a0.setAntiAlias(true);
        a0.setStrokeCap(android.graphics.Paint$Cap.BUTT);
        a0.setStyle(android.graphics.Paint$Style.STROKE);
        float f4 = this.deltaToAngle(i0 - i);
        float f5 = (float)this.mStartAngle + this.deltaToAngle(i - this.mMinValue);
        if (this.mStartColor != this.mEndColor) {
            int i1 = this.mStartColor;
            int i2 = this.mStartColor;
            int i3 = this.mEndColor;
            int i4 = this.mEndColor;
            float f6 = f5 / 360f;
            float f7 = (f5 + f4) / 360f;
            float f8 = f1 / 2f;
            float f9 = f2 / 2f;
            int[] a1 = new int[4];
            a1[0] = i1;
            a1[1] = i2;
            a1[2] = i3;
            a1[3] = i4;
            float[] a2 = new float[4];
            a2[0] = 0.0f;
            a2[1] = f6;
            a2[2] = f7;
            a2[3] = 1f;
            a0.setShader((android.graphics.Shader)new android.graphics.SweepGradient(f8, f9, a1, a2));
        }
        float f10 = f1 / 2f - f3;
        float f11 = f2 / 2f - f3;
        float f12 = f1 / 2f + f3;
        float f13 = f2 / 2f + f3;
        if (this.mShadowThickness <= 0) {
            f = 0.0f;
            f0 = 0.0f;
        } else {
            android.graphics.Paint a3 = new android.graphics.Paint(a0);
            a3.setColor(this.mShadowColor);
            a.drawArc(new android.graphics.RectF(f10, f11, f12, f13), f5, f4, false, a3);
            int i5 = this.mThickness - this.mShadowThickness;
            a0.setStrokeWidth((float)i5);
            f0 = f3 - this.getRadius(i5);
            f = 2f;
        }
        if (f4 > f) {
            android.graphics.RectF a4 = new android.graphics.RectF();
            a4.set(f10 + f0, f11 + f0, f12 - f0, f13 - f0);
            a.drawArc(a4, f5 + f, f4 - f, false, a0);
        }
    }
    
    public float getRadius() {
        return this.getRadius(this.mThickness);
    }
    
    public float getValue() {
        return (float)this.mValue;
    }
    
    protected void initDrawingTools() {
        this.evaluateDimensions();
        this.mBackgroundPaint = new android.graphics.Paint();
        this.mBackgroundPaint.setStrokeWidth((float)this.mBackgroundThickness);
        this.mBackgroundPaint.setAntiAlias(true);
        this.mBackgroundPaint.setStrokeCap(android.graphics.Paint$Cap.BUTT);
        this.mBackgroundPaint.setStyle(android.graphics.Paint$Style.STROKE);
        this.mTicPaint = new android.graphics.Paint();
        this.mTicPaint.setColor(this.mTicColor);
        this.mTicPaint.setStyle((this.mTicStyle != 1) ? android.graphics.Paint$Style.FILL : android.graphics.Paint$Style.STROKE);
        this.mTicPaint.setStrokeWidth(0.0f);
        this.mTicPaint.setAntiAlias(this.AntialiasSetting);
    }
    
    protected void onDraw(android.graphics.Canvas a) {
        this.drawBackground(a);
        this.drawTics(a);
        this.drawText(a);
        this.drawGauge(a);
    }
    
    protected void onMeasure(int i, int i0) {
        int i1 = android.view.View$MeasureSpec.getMode(i);
        int i2 = android.view.View$MeasureSpec.getSize(i);
        int i3 = android.view.View$MeasureSpec.getMode(i0);
        int i4 = android.view.View$MeasureSpec.getSize(i0);
        int i5 = Math.min(this.chooseDimension(i1, i2), this.chooseDimension(i3, i4));
        this.setMeasuredDimension(i5, i5);
    }
    
    protected void onSizeChanged(int i, int i0, int i1, int i2) {
        this.initDrawingTools();
    }
    
    public void setAnimated(boolean b) {
        this.mAnimateValues = b;
    }
    
    public void setBackgroundColor(int i) {
        this.mBackgroundColor = i;
        this.invalidate();
    }
    
    public void setCenterSubtext(String s) {
        this.mCenterSubtext = s;
        this.invalidate();
    }
    
    public void setCenterText(String s) {
        this.mCenterText = s;
        this.invalidate();
    }
    
    public void setMaxValue(int i) {
        this.mMaxValue = i;
    }
    
    public void setValue(float f) {
        this.mValue = (int)f;
        this.invalidate();
    }
    
    public void setValue(int i) {
        if (i < this.mMinValue) {
            i = this.mMinValue;
        }
        if (i > this.mMaxValue) {
            i = this.mMaxValue;
        }
        if (this.mAnimateValues) {
            this.mSerialValueAnimator.setValue((float)i);
        } else {
            this.mValue = i;
            this.invalidate();
        }
    }
}
