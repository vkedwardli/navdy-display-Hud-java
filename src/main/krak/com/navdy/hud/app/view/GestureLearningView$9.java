package com.navdy.hud.app.view;

class GestureLearningView$9 implements android.animation.ValueAnimator$AnimatorUpdateListener {
    final com.navdy.hud.app.view.GestureLearningView this$0;
    final android.widget.TextView val$directionText;
    final android.widget.ImageView val$overLay;
    
    GestureLearningView$9(com.navdy.hud.app.view.GestureLearningView a, android.widget.TextView a0, android.widget.ImageView a1) {
        super();
        this.this$0 = a;
        this.val$directionText = a0;
        this.val$overLay = a1;
    }
    
    public void onAnimationUpdate(android.animation.ValueAnimator a) {
        this.val$directionText.setAlpha(((Float)a.getAnimatedValue()).floatValue());
        this.val$overLay.setAlpha(((Float)a.getAnimatedValue()).floatValue());
    }
}
