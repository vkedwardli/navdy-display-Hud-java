package com.navdy.hud.app.view;
import com.navdy.hud.app.R;

class MusicWidgetPresenter$2 implements Runnable {
    final com.navdy.hud.app.view.MusicWidgetPresenter this$0;
    final okio.ByteString val$photo;
    
    MusicWidgetPresenter$2(com.navdy.hud.app.view.MusicWidgetPresenter a, okio.ByteString a0) {
        super();
        this.this$0 = a;
        this.val$photo = a0;
    }
    
    public void run() {
        com.navdy.hud.app.view.MusicWidgetPresenter.access$000().d(new StringBuilder().append("PhotoUpdate runnable ").append(this.val$photo).toString());
        byte[] a = this.val$photo.toByteArray();
        label0: {
            android.graphics.Bitmap a0 = null;
            label2: {
                label3: {
                    if (a == null) {
                        break label3;
                    }
                    if (a.length > 0) {
                        break label2;
                    }
                }
                com.navdy.hud.app.view.MusicWidgetPresenter.access$000().i("Received photo has null or empty byte array");
                com.navdy.hud.app.view.MusicWidgetPresenter.access$100(this.this$0);
                com.navdy.hud.app.view.MusicWidgetPresenter.access$200(this.this$0);
                break label0;
            }
            int i = com.navdy.hud.app.HudApplication.getAppContext().getResources().getDimensionPixelSize(R.dimen.music_gauge_album_art_size);
            com.navdy.service.library.util.ScalingUtilities$ScalingLogic a1 = com.navdy.service.library.util.ScalingUtilities$ScalingLogic.FIT;
            label1: {
                android.graphics.Bitmap a2 = null;
                try {
                    Exception a3 = null;
                    try {
                        a2 = null;
                        a0 = null;
                        a0 = com.navdy.service.library.util.ScalingUtilities.decodeByteArray(a, i, i, a1);
                        a2 = a0;
                        com.navdy.hud.app.view.MusicWidgetPresenter.access$302(this.this$0, com.navdy.service.library.util.ScalingUtilities.createScaledBitmap(a0, i, i, com.navdy.service.library.util.ScalingUtilities$ScalingLogic.FIT));
                        a2 = a0;
                        com.navdy.hud.app.view.MusicWidgetPresenter.access$200(this.this$0);
                        break label1;
                    } catch(Exception a4) {
                        a3 = a4;
                    }
                    a2 = a0;
                    com.navdy.hud.app.view.MusicWidgetPresenter.access$000().e("Error updating the art work received ", (Throwable)a3);
                    com.navdy.hud.app.view.MusicWidgetPresenter.access$100(this.this$0);
                    com.navdy.hud.app.view.MusicWidgetPresenter.access$200(this.this$0);
                } catch(Throwable a5) {
                    if (a2 != null && a2 != com.navdy.hud.app.view.MusicWidgetPresenter.access$300(this.this$0)) {
                        a2.recycle();
                    }
                    throw a5;
                }
                if (a0 == null) {
                    break label0;
                }
                if (a0 == com.navdy.hud.app.view.MusicWidgetPresenter.access$300(this.this$0)) {
                    break label0;
                }
                a0.recycle();
                break label0;
            }
            if (a0 != null && a0 != com.navdy.hud.app.view.MusicWidgetPresenter.access$300(this.this$0)) {
                a0.recycle();
            }
        }
    }
}
