package com.navdy.hud.app.view;


public enum MainView$CustomAnimationMode {
    SHRINK_LEFT(0),
    EXPAND(1),
    SHRINK_MODE(2);

    private int value;
    MainView$CustomAnimationMode(int value) {
        this.value = value;
    }
    public int getValue() {
        return value;
    }
}

//final public class MainView$CustomAnimationMode extends Enum {
//    final private static com.navdy.hud.app.view.MainView$CustomAnimationMode[] $VALUES;
//    final public static com.navdy.hud.app.view.MainView$CustomAnimationMode EXPAND;
//    final public static com.navdy.hud.app.view.MainView$CustomAnimationMode SHRINK_LEFT;
//    final public static com.navdy.hud.app.view.MainView$CustomAnimationMode SHRINK_MODE;
//    
//    static {
//        SHRINK_LEFT = new com.navdy.hud.app.view.MainView$CustomAnimationMode("SHRINK_LEFT", 0);
//        EXPAND = new com.navdy.hud.app.view.MainView$CustomAnimationMode("EXPAND", 1);
//        SHRINK_MODE = new com.navdy.hud.app.view.MainView$CustomAnimationMode("SHRINK_MODE", 2);
//        com.navdy.hud.app.view.MainView$CustomAnimationMode[] a = new com.navdy.hud.app.view.MainView$CustomAnimationMode[3];
//        a[0] = SHRINK_LEFT;
//        a[1] = EXPAND;
//        a[2] = SHRINK_MODE;
//        $VALUES = a;
//    }
//    
//    private MainView$CustomAnimationMode(String s, int i) {
//        super(s, i);
//    }
//    
//    public static com.navdy.hud.app.view.MainView$CustomAnimationMode valueOf(String s) {
//        return (com.navdy.hud.app.view.MainView$CustomAnimationMode)Enum.valueOf(com.navdy.hud.app.view.MainView$CustomAnimationMode.class, s);
//    }
//    
//    public static com.navdy.hud.app.view.MainView$CustomAnimationMode[] values() {
//        return $VALUES.clone();
//    }
//}
//