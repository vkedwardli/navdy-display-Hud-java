package com.navdy.hud.app.view;

final public class MusicWidgetPresenter$$InjectAdapter extends dagger.internal.Binding implements dagger.MembersInjector {
    private dagger.internal.Binding musicManager;
    private dagger.internal.Binding supertype;
    
    public MusicWidgetPresenter$$InjectAdapter() {
        super((String)null, "members/com.navdy.hud.app.view.MusicWidgetPresenter", false, com.navdy.hud.app.view.MusicWidgetPresenter.class);
    }
    
    public void attach(dagger.internal.Linker a) {
        this.musicManager = a.requestBinding("com.navdy.hud.app.manager.MusicManager", com.navdy.hud.app.view.MusicWidgetPresenter.class, (this).getClass().getClassLoader());
        this.supertype = a.requestBinding("members/com.navdy.hud.app.view.DashboardWidgetPresenter", com.navdy.hud.app.view.MusicWidgetPresenter.class, (this).getClass().getClassLoader(), false, true);
    }
    
    public void getDependencies(java.util.Set a, java.util.Set a0) {
        a0.add(this.musicManager);
        a0.add(this.supertype);
    }
    
    public void injectMembers(com.navdy.hud.app.view.MusicWidgetPresenter a) {
        a.musicManager = (com.navdy.hud.app.manager.MusicManager)this.musicManager.get();
        this.supertype.injectMembers(a);
    }
    
    public void injectMembers(Object a) {
        this.injectMembers((com.navdy.hud.app.view.MusicWidgetPresenter)a);
    }
}
