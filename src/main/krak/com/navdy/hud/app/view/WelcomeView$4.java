package com.navdy.hud.app.view;

class WelcomeView$4 implements com.navdy.hud.app.ui.component.carousel.Carousel$Listener {
    final com.navdy.hud.app.view.WelcomeView this$0;
    
    WelcomeView$4(com.navdy.hud.app.view.WelcomeView a) {
        super();
        this.this$0 = a;
    }
    
    public void onCurrentItemChanged(int i, int i0) {
        this.this$0.leftDot.setVisibility((i <= 1) ? 8 : 0);
        this.this$0.rightDot.setVisibility((i + 2 >= this.this$0.carousel.getCount()) ? 8 : 0);
        com.navdy.hud.app.ui.component.carousel.Carousel$Model a = this.this$0.carousel.getModel(i);
        com.navdy.hud.app.view.WelcomeView.access$1100(this.this$0, a);
        if (this.this$0.presenter != null) {
            this.this$0.presenter.onCurrentItemChanged(i, a.id);
        }
    }
    
    public void onCurrentItemChanging(int i, int i0, int i1) {
        if (i0 <= i) {
            this.this$0.leftDot.setVisibility((i0 <= 1) ? 8 : 0);
        } else {
            this.this$0.rightDot.setVisibility((i0 + 2 >= this.this$0.carousel.getCount()) ? 8 : 0);
        }
        com.navdy.hud.app.view.WelcomeView.access$1100(this.this$0, this.this$0.carousel.getModel(i0));
    }
    
    public void onExecuteItem(int i, int i0) {
        if (this.this$0.presenter != null) {
            this.this$0.presenter.executeItem(i, i0);
        }
    }
    
    public void onExit() {
    }
}
