package com.navdy.hud.app.view;

final public class EngineTemperaturePresenter$WhenMappings {
    final public static int[] $EnumSwitchMapping$0;
    final public static int[] $EnumSwitchMapping$1;
    
    static {
        $EnumSwitchMapping$0 = new int[com.navdy.service.library.events.preferences.DriverProfilePreferences$UnitSystem.values().length];
        $EnumSwitchMapping$0[com.navdy.service.library.events.preferences.DriverProfilePreferences$UnitSystem.UNIT_SYSTEM_METRIC.ordinal()] = 1;
        $EnumSwitchMapping$0[com.navdy.service.library.events.preferences.DriverProfilePreferences$UnitSystem.UNIT_SYSTEM_IMPERIAL.ordinal()] = 2;
        $EnumSwitchMapping$1 = new int[com.navdy.service.library.events.preferences.DriverProfilePreferences$UnitSystem.values().length];
        $EnumSwitchMapping$1[com.navdy.service.library.events.preferences.DriverProfilePreferences$UnitSystem.UNIT_SYSTEM_METRIC.ordinal()] = 1;
        $EnumSwitchMapping$1[com.navdy.service.library.events.preferences.DriverProfilePreferences$UnitSystem.UNIT_SYSTEM_IMPERIAL.ordinal()] = 2;
    }
}
