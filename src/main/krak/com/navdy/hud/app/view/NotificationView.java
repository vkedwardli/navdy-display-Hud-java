package com.navdy.hud.app.view;

public class NotificationView extends android.widget.RelativeLayout implements com.navdy.hud.app.manager.InputManager$IInputHandler, com.navdy.hud.app.gesture.GestureDetector$GestureListener {
    final private static int CLICK_COUNT = 2;
    final private static com.navdy.service.library.log.Logger sLogger;
    @InjectView(R.id.border)
    public com.navdy.hud.app.ui.component.ShrinkingBorderView border;
    private com.navdy.hud.app.ui.component.ShrinkingBorderView$IListener borderListener;
    private com.navdy.hud.app.gesture.MultipleClickGestureDetector clickGestureDetector;
    private com.navdy.hud.app.gesture.MultipleClickGestureDetector$IMultipleClickKeyGesture clickGestureListener;
    @InjectView(R.id.customNotificationContainer)
    android.widget.FrameLayout customNotificationContainer;
    private int defaultColor;
    private boolean initialized;
    @InjectView(R.id.nextNotificationColorView)
    public com.navdy.hud.app.ui.component.image.ColorImageView nextNotificationColorView;
    private com.navdy.hud.app.framework.notifications.INotification notification;
    private com.navdy.hud.app.framework.notifications.NotificationManager notificationManager;
    @Inject
    com.navdy.hud.app.presenter.NotificationPresenter presenter;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.view.NotificationView.class);
    }
    
    public NotificationView(android.content.Context a) {
        this(a, (android.util.AttributeSet)null);
    }
    
    public NotificationView(android.content.Context a, android.util.AttributeSet a0) {
        super(a, a0);
        this.clickGestureListener = (com.navdy.hud.app.gesture.MultipleClickGestureDetector$IMultipleClickKeyGesture)new com.navdy.hud.app.view.NotificationView$1(this);
        this.borderListener = (com.navdy.hud.app.ui.component.ShrinkingBorderView$IListener)new com.navdy.hud.app.view.NotificationView$2(this);
        if (!this.isInEditMode()) {
            mortar.Mortar.inject(a, this);
            this.notificationManager = com.navdy.hud.app.framework.notifications.NotificationManager.getInstance();
            this.clickGestureDetector = new com.navdy.hud.app.gesture.MultipleClickGestureDetector(2, this.clickGestureListener);
        }
    }
    
    static void access$000(com.navdy.hud.app.view.NotificationView a, boolean b) {
        a.takeNotificationAction(b);
    }
    
    static com.navdy.hud.app.framework.notifications.INotification access$100(com.navdy.hud.app.view.NotificationView a) {
        return a.notification;
    }
    
    static com.navdy.hud.app.framework.notifications.NotificationManager access$200(com.navdy.hud.app.view.NotificationView a) {
        return a.notificationManager;
    }
    
    static boolean access$300(com.navdy.hud.app.view.NotificationView a) {
        return a.initialized;
    }
    
    private void takeNotificationAction(boolean b) {
        label2: if (this.notification != null) {
            boolean b0 = this.notificationManager.isExpanded();
            label0: {
                boolean b1 = false;
                label1: {
                    if (b0) {
                        break label1;
                    }
                    if (!this.notificationManager.isExpandedNotificationVisible()) {
                        break label0;
                    }
                }
                if (this.notificationManager.isCurrentItemDeleteAll()) {
                    com.navdy.hud.app.analytics.AnalyticsSupport.recordGlanceAction("Glance_Dismiss", (com.navdy.hud.app.framework.notifications.INotification)null, b ? "swipe" : "doubleclick");
                    b = true;
                    b1 = true;
                } else {
                    com.navdy.hud.app.analytics.AnalyticsSupport.recordGlanceAction("Glance_Open_Mini", this.notification, b ? "swipe" : "doubleclick");
                    b1 = false;
                }
                this.notificationManager.collapseExpandedNotification(b1, b);
                break label2;
            }
            com.navdy.hud.app.analytics.AnalyticsSupport.recordGlanceAction("Glance_Dismiss", this.notification, b ? "swipe" : "doubleclick");
            this.notificationManager.collapseNotification();
        }
    }
    
    public void addCustomView(com.navdy.hud.app.framework.notifications.INotification a, android.view.View a0) {
        this.removeCustomView();
        android.widget.FrameLayout$LayoutParams a1 = new android.widget.FrameLayout$LayoutParams(-1, -1);
        this.customNotificationContainer.addView(a0, (android.view.ViewGroup$LayoutParams)a1);
        this.notification = a;
        this.initialized = true;
    }
    
    public void clearNotification() {
        this.customNotificationContainer.removeAllViews();
    }
    
    public android.view.View getCurrentNotificationViewChild() {
        android.view.View a = (this.customNotificationContainer.getChildCount() <= 0) ? null : this.customNotificationContainer.getChildAt(0);
        return a;
    }
    
    public android.view.ViewGroup getNotificationContainer() {
        return this.customNotificationContainer;
    }
    
    public void hideNextNotificationColor() {
        this.nextNotificationColorView.setVisibility(4);
    }
    
    public com.navdy.hud.app.manager.InputManager$IInputHandler nextHandler() {
        return null;
    }
    
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.defaultColor = this.getContext().getResources().getColor(17170444);
        if (this.presenter != null) {
            this.presenter.takeView(this);
        }
    }
    
    public void onClick() {
    }
    
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (this.presenter != null) {
            this.presenter.dropView((android.view.View)this);
        }
    }
    
    protected void onFinishInflate() {
        super.onFinishInflate();
        butterknife.ButterKnife.inject((android.view.View)this);
        this.border.setListener(this.borderListener);
        this.resetNextNotificationColor();
    }
    
    public boolean onGesture(com.navdy.service.library.events.input.GestureEvent a) {
        boolean b = false;
        if (this.initialized) {
            if (this.notification == null) {
                b = false;
            } else if (a == null) {
                b = false;
            } else if (a.gesture == null) {
                b = false;
            } else if (this.notificationManager.isAnimating()) {
                sLogger.v("ignore gesture, animation in progress");
                b = true;
            } else {
                switch(com.navdy.hud.app.view.NotificationView$3.$SwitchMap$com$navdy$service$library$events$input$Gesture[a.gesture.ordinal()]) {
                    case 2: {
                        if (this.notification != null && !this.notificationManager.isExpanded() && !this.notificationManager.isExpandedNotificationVisible() && this.notification.onGesture(a)) {
                            sLogger.v(new StringBuilder().append("gesture handled by notif:").append(this.notification.getType()).toString());
                            break;
                        } else {
                            this.takeNotificationAction(true);
                            break;
                        }
                    }
                    case 1: {
                        boolean b0 = this.notificationManager.isExpanded();
                        label1: {
                            if (b0) {
                                break label1;
                            }
                            if (this.notificationManager.isExpandedNotificationVisible()) {
                                break label1;
                            }
                            if (this.notification.onGesture(a)) {
                                sLogger.v(new StringBuilder().append("gesture handled by notif:").append(this.notification.getType()).toString());
                                break;
                            } else {
                                com.navdy.hud.app.analytics.AnalyticsSupport.recordGlanceAction("Glance_Open_Full", this.notification, "swipe");
                                boolean b1 = this.notification.expandNotification();
                                sLogger.v(new StringBuilder().append("gesture expanded:").append(b1).toString());
                                break;
                            }
                        }
                        boolean b2 = this.notificationManager.isExpanded();
                        label0: {
                            if (b2) {
                                break label0;
                            }
                            if (!this.notificationManager.isExpandedNotificationVisible()) {
                                break;
                            }
                        }
                        this.notificationManager.moveNext(true);
                        break;
                    }
                }
                b = true;
            }
        } else {
            b = false;
        }
        return b;
    }
    
    public boolean onKey(com.navdy.hud.app.manager.InputManager$CustomKeyEvent a) {
        return this.initialized && this.clickGestureDetector.onKey(a);
    }
    
    public void onTrackHand(float f) {
    }
    
    public void removeCustomView() {
        this.customNotificationContainer.removeAllViews();
        this.border.stopTimeout(false, (Runnable)null);
        this.initialized = false;
    }
    
    public void resetNextNotificationColor() {
        this.nextNotificationColorView.setColor(this.defaultColor);
    }
    
    public void setNextNotificationColor(int i) {
        this.nextNotificationColorView.setColor(i);
    }
    
    public void showNextNotificationColor() {
        this.nextNotificationColorView.setVisibility(0);
    }
    
    public void switchNotfication(com.navdy.hud.app.framework.notifications.INotification a) {
        this.notification = a;
    }
}
