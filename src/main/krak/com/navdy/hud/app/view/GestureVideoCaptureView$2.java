package com.navdy.hud.app.view;

class GestureVideoCaptureView$2 implements Runnable {
    final com.navdy.hud.app.view.GestureVideoCaptureView this$0;
    final com.navdy.hud.app.gesture.GestureServiceConnector$RecordingSaved val$recordingSaved;
    final String val$sessionPath;
    final String val$userTag;
    final String val$videoArchivePath;
    
    GestureVideoCaptureView$2(com.navdy.hud.app.view.GestureVideoCaptureView a, String s, com.navdy.hud.app.gesture.GestureServiceConnector$RecordingSaved a0, String s0, String s1) {
        super();
        this.this$0 = a;
        this.val$sessionPath = s;
        this.val$recordingSaved = a0;
        this.val$videoArchivePath = s0;
        this.val$userTag = s1;
    }
    
    public void run() {
        java.io.File a = new java.io.File(this.val$sessionPath);
        if (!a.exists()) {
            a.mkdirs();
        }
        java.io.File[] a0 = new java.io.File(this.val$recordingSaved.path).listFiles();
        com.navdy.service.library.util.IOUtils.compressFilesToZip(this.this$0.getContext(), a0, this.val$videoArchivePath);
        com.navdy.hud.app.view.GestureVideoCaptureView.sLogger.d(new StringBuilder().append("Compressed file : ").append(this.val$videoArchivePath).toString());
        com.navdy.hud.app.service.GestureVideosSyncService.addGestureVideoToUploadQueue(new java.io.File(this.val$videoArchivePath), this.val$userTag);
        com.navdy.hud.app.service.GestureVideosSyncService.syncNow();
    }
}
