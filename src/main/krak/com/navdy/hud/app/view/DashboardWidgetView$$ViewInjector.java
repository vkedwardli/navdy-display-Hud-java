package com.navdy.hud.app.view;
import com.navdy.hud.app.R;

public class DashboardWidgetView$$ViewInjector {
    public DashboardWidgetView$$ViewInjector() {
    }
    
    public static void inject(butterknife.ButterKnife$Finder a, com.navdy.hud.app.view.DashboardWidgetView a0, Object a1) {
        a0.mCustomView = a.findOptionalView(a1, R.id.custom_drawable);
    }
    
    public static void reset(com.navdy.hud.app.view.DashboardWidgetView a) {
        a.mCustomView = null;
    }
}
