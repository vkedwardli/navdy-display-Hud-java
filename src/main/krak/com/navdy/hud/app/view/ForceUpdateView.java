package com.navdy.hud.app.view;
import com.navdy.hud.app.R;

public class ForceUpdateView extends android.widget.RelativeLayout implements com.navdy.hud.app.ui.component.ChoiceLayout$IListener, com.navdy.hud.app.manager.InputManager$IInputHandler {
    final private static int TAG_DISMISS = 2;
    final private static int TAG_INSTALL = 1;
    final private static int TAG_SHUT_DOWN = 0;
    final private static int UPDATE_MESSAGE_MAX_WIDTH = 350;
    final private static com.navdy.service.library.log.Logger sLogger;
    @InjectView(R.id.choiceLayout)
    com.navdy.hud.app.ui.component.ChoiceLayout mChoiceLayout;
    @InjectView(R.id.image)
    android.widget.ImageView mIcon;
    @Inject
    com.navdy.hud.app.screen.ForceUpdateScreen$Presenter mPresenter;
    @InjectView(R.id.mainTitle)
    android.widget.TextView mScreenTitleText;
    @InjectView(R.id.title1)
    android.widget.TextView mTextView1;
    @InjectView(R.id.title2)
    android.widget.TextView mTextView2;
    @InjectView(R.id.title3)
    android.widget.TextView mTextView3;
    private boolean waitingForUpdate;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.view.ForceUpdateView.class);
    }
    
    public ForceUpdateView(android.content.Context a) {
        super(a);
        this.waitingForUpdate = false;
        if (!this.isInEditMode()) {
            mortar.Mortar.inject(a, this);
        }
    }
    
    public ForceUpdateView(android.content.Context a, android.util.AttributeSet a0) {
        super(a, a0);
        this.waitingForUpdate = false;
        if (!this.isInEditMode()) {
            mortar.Mortar.inject(a, this);
        }
    }
    
    public ForceUpdateView(android.content.Context a, android.util.AttributeSet a0, int i) {
        super(a, a0, i);
        this.waitingForUpdate = false;
        if (!this.isInEditMode()) {
            mortar.Mortar.inject(a, this);
        }
    }
    
    private void performAction(int i) {
        switch(i) {
            case 2: {
                this.mPresenter.dismiss();
                break;
            }
            case 1: {
                this.mPresenter.install();
                break;
            }
            case 0: {
                this.mPresenter.shutDown();
                break;
            }
            default: {
                sLogger.e(new StringBuilder().append("unknown action received: ").append(i).toString());
            }
        }
    }
    
    public void executeItem(int i, int i0) {
        this.performAction(i0);
    }
    
    public void itemSelected(int i, int i0) {
    }
    
    public com.navdy.hud.app.manager.InputManager$IInputHandler nextHandler() {
        return null;
    }
    
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.mPresenter.getBus().unregister(this);
    }
    
    protected void onFinishInflate() {
        super.onFinishInflate();
        boolean b = this.mPresenter.isSoftwareUpdatePending();
        java.util.ArrayList a = new java.util.ArrayList();
        butterknife.ButterKnife.inject((android.view.View)this);
        this.mPresenter.takeView(this);
        this.mPresenter.getBus().register(this);
        ((com.navdy.hud.app.view.MaxWidthLinearLayout)this.findViewById(R.id.infoContainer)).setMaxWidth(350);
        this.mIcon.setImageResource(R.drawable.icon_software_update);
        this.mTextView1.setVisibility(8);
        this.mTextView2.setVisibility(8);
        this.mTextView3.setSingleLine(false);
        this.mTextView3.setVisibility(0);
        if (com.navdy.hud.app.ui.activity.Main.mProtocolStatus != com.navdy.hud.app.ui.activity.Main$ProtocolStatus.PROTOCOL_REMOTE_NEEDS_UPDATE) {
            this.mScreenTitleText.setText(R.string.title_display_update_required);
            if (b) {
                this.mTextView3.setText(R.string.hud_update_install);
                ((java.util.List)a).add(new com.navdy.hud.app.ui.component.ChoiceLayout$Choice(this.getContext().getString(R.string.install), 1));
            } else {
                this.mTextView3.setText(R.string.hud_update_download);
                this.waitingForUpdate = true;
            }
            ((java.util.List)a).add(new com.navdy.hud.app.ui.component.ChoiceLayout$Choice(this.getContext().getString(R.string.shutdown), 0));
        } else {
            this.mScreenTitleText.setText(R.string.title_app_update_required);
            this.mTextView3.setText(R.string.phone_update_required);
            ((java.util.List)a).add(new com.navdy.hud.app.ui.component.ChoiceLayout$Choice(this.getContext().getString(R.string.dismiss), 2));
        }
        this.mChoiceLayout.setChoices(com.navdy.hud.app.ui.component.ChoiceLayout$Mode.LABEL, (java.util.List)a, 0, (com.navdy.hud.app.ui.component.ChoiceLayout$IListener)this);
    }
    
    public boolean onGesture(com.navdy.service.library.events.input.GestureEvent a) {
        return false;
    }
    
    public boolean onKey(com.navdy.hud.app.manager.InputManager$CustomKeyEvent a) {
        boolean b = false;
        com.navdy.hud.app.ui.component.ChoiceLayout a0 = this.mChoiceLayout;
        label2: {
            label0: {
                label1: {
                    if (a0 == null) {
                        break label1;
                    }
                    if (this.mChoiceLayout.getVisibility() == 0) {
                        break label0;
                    }
                }
                b = false;
                break label2;
            }
            switch(com.navdy.hud.app.view.ForceUpdateView$1.$SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent[a.ordinal()]) {
                case 3: {
                    this.mChoiceLayout.executeSelectedItem(true);
                    b = true;
                    break;
                }
                case 2: {
                    this.mChoiceLayout.moveSelectionRight();
                    b = true;
                    break;
                }
                case 1: {
                    this.mChoiceLayout.moveSelectionLeft();
                    b = true;
                    break;
                }
                default: {
                    b = true;
                }
            }
        }
        return b;
    }
    
    public void onUpdateReady(com.navdy.hud.app.util.OTAUpdateService$UpdateVerified a) {
        if (this.waitingForUpdate) {
            java.util.ArrayList a0 = new java.util.ArrayList();
            ((java.util.List)a0).add(new com.navdy.hud.app.ui.component.ChoiceLayout$Choice(this.getContext().getString(R.string.install), 1));
            ((java.util.List)a0).add(new com.navdy.hud.app.ui.component.ChoiceLayout$Choice(this.getContext().getString(R.string.shutdown), 0));
            this.mChoiceLayout.setChoices(com.navdy.hud.app.ui.component.ChoiceLayout$Mode.LABEL, (java.util.List)a0, 0, (com.navdy.hud.app.ui.component.ChoiceLayout$IListener)this);
            this.mTextView3.setText(R.string.hud_update_install);
            this.waitingForUpdate = false;
        }
    }
}
