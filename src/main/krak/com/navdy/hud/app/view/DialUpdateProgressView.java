package com.navdy.hud.app.view;

public class DialUpdateProgressView extends android.widget.RelativeLayout implements com.navdy.hud.app.manager.InputManager$IInputHandler {
    final private static long UPDATE_TIMEOUT = 480000L;
    final private static com.navdy.service.library.log.Logger sLogger;
    private android.os.Handler handler;
    @Inject
    com.navdy.hud.app.screen.DialUpdateProgressScreen$Presenter mPresenter;
    @InjectView(R.id.progress)
    android.widget.ProgressBar mProgress;
    private Runnable timeout;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.view.DialUpdateProgressView.class);
    }
    
    public DialUpdateProgressView(android.content.Context a) {
        super(a, (android.util.AttributeSet)null);
        this.handler = new android.os.Handler();
        this.timeout = (Runnable)new com.navdy.hud.app.view.DialUpdateProgressView$1(this);
        if (!this.isInEditMode()) {
            mortar.Mortar.inject(a, this);
        }
    }
    
    public DialUpdateProgressView(android.content.Context a, android.util.AttributeSet a0) {
        super(a, a0);
        this.handler = new android.os.Handler();
        this.timeout = (Runnable)new com.navdy.hud.app.view.DialUpdateProgressView$1(this);
        if (!this.isInEditMode()) {
            mortar.Mortar.inject(a, this);
        }
    }
    
    static com.navdy.service.library.log.Logger access$000() {
        return sLogger;
    }
    
    public com.navdy.hud.app.manager.InputManager$IInputHandler nextHandler() {
        return null;
    }
    
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
    }
    
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        com.navdy.hud.app.framework.toast.ToastManager.getInstance().disableToasts(false);
        com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().enableNotifications(true);
        if (this.mPresenter != null) {
            this.handler.removeCallbacks(this.timeout);
            this.mPresenter.dropView((android.view.View)this);
        }
    }
    
    protected void onFinishInflate() {
        super.onFinishInflate();
        butterknife.ButterKnife.inject((android.view.View)this);
        if (this.mPresenter != null) {
            this.mPresenter.takeView(this);
            com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().enableNotifications(false);
            com.navdy.hud.app.framework.toast.ToastManager.getInstance().disableToasts(true);
            this.mPresenter.startUpdate();
            this.handler.removeCallbacks(this.timeout);
            this.handler.postDelayed(this.timeout, 480000L);
        }
        this.setProgress(0);
    }
    
    public boolean onGesture(com.navdy.service.library.events.input.GestureEvent a) {
        return false;
    }
    
    public boolean onKey(com.navdy.hud.app.manager.InputManager$CustomKeyEvent a) {
        return true;
    }
    
    public void setProgress(int i) {
        this.mProgress.setProgress(i);
    }
}
