package com.navdy.hud.app.view.drawable;
import com.navdy.hud.app.R;

public class AnalogClockDrawable extends com.navdy.hud.app.view.drawable.CustomDrawable {
    private int centerPointWidth;
    private int dateTextMargin;
    private int dateTextSize;
    private int dayOfMonth;
    private int frameColor;
    private int hour;
    private int hourHandColor;
    private float hourHandLengthFraction;
    private int hourHandStrokeWidth;
    private int minute;
    private int minuteHandColor;
    private float minuteHandLengthFraction;
    private int minuteHandStrokeWidth;
    private int seconds;
    
    public AnalogClockDrawable(android.content.Context a) {
        this.hourHandLengthFraction = 0.75f;
        this.minuteHandLengthFraction = 0.8f;
        this.frameColor = a.getResources().getColor(R.color.analog_clock_frame_color);
        this.hourHandColor = a.getResources().getColor(R.color.cyan);
        this.hourHandStrokeWidth = a.getResources().getDimensionPixelSize(R.dimen.analog_clock_hour_hand_width);
        this.minuteHandStrokeWidth = a.getResources().getDimensionPixelSize(R.dimen.analog_clock_minute_hand_width);
        this.centerPointWidth = a.getResources().getDimensionPixelSize(R.dimen.analog_clock_center_point_width);
        this.minuteHandColor = -1;
        this.dateTextSize = a.getResources().getDimensionPixelSize(R.dimen.analog_clock_date_text_size);
        this.dateTextMargin = a.getResources().getDimensionPixelSize(R.dimen.analog_clock_date_text_margin);
    }
    
    public void draw(android.graphics.Canvas a) {
        super.draw(a);
        android.graphics.Rect a0 = this.getBounds();
        android.graphics.RectF a1 = new android.graphics.RectF(a0);
        this.mPaint.setAntiAlias(true);
        this.mPaint.setStyle(android.graphics.Paint$Style.FILL);
        this.mPaint.setColor(this.frameColor);
        a.drawArc(a1, 0.0f, 360f, true, this.mPaint);
        this.mPaint.setStyle(android.graphics.Paint$Style.STROKE);
        this.mPaint.setColor(this.hourHandColor);
        this.mPaint.setStrokeCap(android.graphics.Paint$Cap.ROUND);
        this.mPaint.setStrokeWidth((float)this.hourHandStrokeWidth);
        this.mPaint.setShadowLayer(10f, 10f, 10f, -16777216);
        float f = com.navdy.hud.app.util.DateUtil.getClockAngleForHour(this.hour, this.minute);
        float f0 = (float)(a0.width() / 2 - 15);
        int i = (int)((double)a0.centerX() + (double)f0 * Math.cos(Math.toRadians((double)f)));
        int i0 = (int)((double)a0.centerY() + (double)f0 * Math.sin(Math.toRadians((double)f)));
        a.drawLine((float)a0.centerX(), (float)a0.centerY(), (float)i, (float)i0, this.mPaint);
        this.mPaint.setShadowLayer(0.0f, 0.0f, 0.0f, this.mPaint.getColor());
        this.mPaint.setStyle(android.graphics.Paint$Style.STROKE);
        this.mPaint.setColor(this.minuteHandColor);
        this.mPaint.setStrokeCap(android.graphics.Paint$Cap.ROUND);
        this.mPaint.setStrokeWidth((float)this.minuteHandStrokeWidth);
        float f1 = com.navdy.hud.app.util.DateUtil.getClockAngleForMinutes(this.minute);
        float f2 = (float)(a0.width() / 2 - 10);
        int i1 = (int)((double)a0.centerX() + (double)f2 * Math.cos(Math.toRadians((double)f1)));
        int i2 = (int)((double)a0.centerY() + (double)f2 * Math.sin(Math.toRadians((double)f1)));
        a.drawLine((float)a0.centerX(), (float)a0.centerY(), (float)i1, (float)i2, this.mPaint);
        this.mPaint.setStyle(android.graphics.Paint$Style.FILL);
        this.mPaint.setStrokeWidth(1f);
        this.mPaint.setColor(this.minuteHandColor);
        a.drawArc((float)(a0.centerX() - this.centerPointWidth), (float)(a0.centerY() - this.centerPointWidth), (float)(a0.centerX() + this.centerPointWidth), (float)(a0.centerY() + this.centerPointWidth), 0.0f, 360f, true, this.mPaint);
        float f3 = (float)(a0.width() / 2 - this.dateTextMargin);
        this.mPaint.setStyle(android.graphics.Paint$Style.FILL);
        this.mPaint.setTypeface(android.graphics.Typeface.create(android.graphics.Typeface.DEFAULT, 1));
        this.mPaint.setTextSize((float)this.dateTextSize);
        String s = Integer.toString(this.dayOfMonth);
        android.graphics.Rect a2 = new android.graphics.Rect();
        this.mPaint.getTextBounds(s, 0, s.length(), a2);
        int i3 = this.hour % 12;
        label0: {
            label6: {
                label8: {
                    if (i3 < 7) {
                        break label8;
                    }
                    if (i3 < 11) {
                        break label6;
                    }
                }
                int i4 = this.minute;
                label7: {
                    if (i4 < 35) {
                        break label7;
                    }
                    if (this.minute < 55) {
                        break label6;
                    }
                }
                int i5 = (int)((double)a0.centerX() + (double)f3 * Math.cos(Math.toRadians(180.0)));
                int i6 = (int)((double)a0.centerY() + (double)f3 * Math.sin(Math.toRadians(180.0)));
                a.drawText(s, (float)i5, (float)(a2.height() / 2 + i6), this.mPaint);
                break label0;
            }
            label3: {
                label5: {
                    if (i3 < 1) {
                        break label5;
                    }
                    if (i3 < 4) {
                        break label3;
                    }
                }
                int i7 = this.minute;
                label4: {
                    if (i7 < 5) {
                        break label4;
                    }
                    if (this.minute < 20) {
                        break label3;
                    }
                }
                int i8 = (int)((double)a0.centerX() + (double)f3 * Math.cos(Math.toRadians(0.0)));
                int i9 = (int)((double)a0.centerY() + (double)f3 * Math.sin(Math.toRadians(0.0)));
                a.drawText(s, (float)(i8 - a2.width()), (float)(a2.height() / 2 + i9), this.mPaint);
                break label0;
            }
            label2: {
                if (i3 < 4) {
                    break label2;
                }
                if (i3 < 7) {
                    break label0;
                }
            }
            int i10 = this.minute;
            label1: {
                if (i10 < 20) {
                    break label1;
                }
                if (this.minute < 35) {
                    break label0;
                }
            }
            int i11 = (int)((double)a0.centerX() + (double)f3 * Math.cos(Math.toRadians(90.0)));
            int i12 = (int)((double)a0.centerY() + (double)f3 * Math.sin(Math.toRadians(90.0)));
            a.drawText(s, (float)(i11 - a2.width() / 2), (float)i12, this.mPaint);
        }
    }
    
    public void setTime(int i, int i0, int i1, int i2) {
        this.dayOfMonth = i;
        this.hour = i0;
        this.minute = i1;
        this.seconds = i2;
    }
}
