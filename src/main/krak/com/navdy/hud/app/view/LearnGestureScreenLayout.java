package com.navdy.hud.app.view;
import com.navdy.hud.app.R;

public class LearnGestureScreenLayout extends android.widget.FrameLayout implements com.navdy.hud.app.manager.InputManager$IInputHandler, com.navdy.hud.app.ui.component.ChoiceLayout$IListener {
    final public static int TAG_BACK = 0;
    final public static int TAG_TIPS = 1;
    @InjectView(R.id.sensor_blocked_message)
    android.view.ViewGroup mCameraBlockedMessage;
    com.navdy.hud.app.ui.component.ChoiceLayout mCameraSensorBlockedChoiceLayout;
    private com.navdy.hud.app.view.LearnGestureScreenLayout$Mode mCurrentMode;
    @InjectView(R.id.gesture_learning_view)
    com.navdy.hud.app.view.GestureLearningView mGestureLearningView;
    @Inject
    com.navdy.hud.app.screen.GestureLearningScreen$Presenter mPresenter;
    @InjectView(R.id.scrollable_text_presenter)
    com.navdy.hud.app.view.ScrollableTextPresenterLayout mScrollableTextPresenter;
    @InjectView(R.id.capture_instructions_lyt)
    com.navdy.hud.app.view.GestureVideoCaptureView mVideoCaptureInstructionsLayout;
    
    public LearnGestureScreenLayout(android.content.Context a) {
        this(a, (android.util.AttributeSet)null);
    }
    
    public LearnGestureScreenLayout(android.content.Context a, android.util.AttributeSet a0) {
        this(a, a0, 0);
    }
    
    public LearnGestureScreenLayout(android.content.Context a, android.util.AttributeSet a0, int i) {
        super(a, a0, i);
        this.mCurrentMode = com.navdy.hud.app.view.LearnGestureScreenLayout$Mode.GESTURE;
        if (!this.isInEditMode()) {
            mortar.Mortar.inject(a, this);
        }
    }
    
    public void executeItem(int i, int i0) {
        switch(i0) {
            case 1: {
                this.mPresenter.showTips();
                break;
            }
            case 0: {
                this.mPresenter.hideCameraSensorBlocked();
                break;
            }
        }
    }
    
    public void hideCaptureGestureVideosView() {
        this.mGestureLearningView.setVisibility(0);
        this.mCameraBlockedMessage.setVisibility(8);
        this.mVideoCaptureInstructionsLayout.setVisibility(8);
        this.mScrollableTextPresenter.setVisibility(8);
        this.mCurrentMode = com.navdy.hud.app.view.LearnGestureScreenLayout$Mode.GESTURE;
    }
    
    public void hideSensorBlocked() {
        this.mGestureLearningView.setVisibility(0);
        this.mCameraBlockedMessage.setVisibility(8);
        this.mVideoCaptureInstructionsLayout.setVisibility(8);
        this.mScrollableTextPresenter.setVisibility(8);
        this.mCurrentMode = com.navdy.hud.app.view.LearnGestureScreenLayout$Mode.GESTURE;
    }
    
    public void hideTips() {
        this.mGestureLearningView.setVisibility(0);
        this.mVideoCaptureInstructionsLayout.setVisibility(8);
        this.mCameraBlockedMessage.setVisibility(8);
        this.mScrollableTextPresenter.setVisibility(8);
        this.mCurrentMode = com.navdy.hud.app.view.LearnGestureScreenLayout$Mode.GESTURE;
    }
    
    public void itemSelected(int i, int i0) {
    }
    
    public com.navdy.hud.app.manager.InputManager$IInputHandler nextHandler() {
        android.widget.RelativeLayout a = null;
        switch(com.navdy.hud.app.view.LearnGestureScreenLayout$1.$SwitchMap$com$navdy$hud$app$view$LearnGestureScreenLayout$Mode[this.mCurrentMode.ordinal()]) {
            case 3: {
                a = this.mVideoCaptureInstructionsLayout;
                break;
            }
            case 2: {
                a = this.mScrollableTextPresenter;
                break;
            }
            case 1: {
                a = this.mGestureLearningView;
                break;
            }
            default: {
                a = null;
            }
        }
        return (com.navdy.hud.app.manager.InputManager$IInputHandler)a;
    }
    
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (this.mPresenter != null) {
            this.mPresenter.dropView((android.view.View)this);
        }
    }
    
    protected void onFinishInflate() {
        super.onFinishInflate();
        butterknife.ButterKnife.inject((android.view.View)this);
        android.widget.ImageView a = (android.widget.ImageView)this.mCameraBlockedMessage.findViewById(R.id.sideImage);
        a.setVisibility(0);
        a.setImageResource(R.drawable.icon_alert_sensor_blocked);
        android.widget.ImageView a0 = (android.widget.ImageView)this.mCameraBlockedMessage.findViewById(R.id.image);
        a0.setVisibility(0);
        a0.setImageResource(R.drawable.icon_settings_learning_to_gesture_gray);
        ((android.widget.TextView)this.mCameraBlockedMessage.findViewById(R.id.title1)).setText(R.string.sensor_is_blocked);
        android.widget.TextView a1 = (android.widget.TextView)this.mCameraBlockedMessage.findViewById(R.id.title3);
        ((com.navdy.hud.app.view.MaxWidthLinearLayout)this.mCameraBlockedMessage.findViewById(R.id.infoContainer)).setMaxWidth(this.getResources().getDimensionPixelSize(R.dimen.gesture_sensor_blocked_max_width));
        a1.setText(R.string.sensor_is_blocked_message);
        this.mCameraBlockedMessage.findViewById(R.id.title2).setVisibility(8);
        this.mCameraBlockedMessage.findViewById(R.id.title4).setVisibility(8);
        a1.setSingleLine(false);
        a1.setTextSize(1, 17f);
        java.util.ArrayList a2 = new java.util.ArrayList();
        ((java.util.List)a2).add(new com.navdy.hud.app.ui.component.ChoiceLayout$Choice(this.getContext().getString(R.string.back), 0));
        ((java.util.List)a2).add(new com.navdy.hud.app.ui.component.ChoiceLayout$Choice(this.getContext().getString(R.string.tips), 1));
        this.mCameraSensorBlockedChoiceLayout = (com.navdy.hud.app.ui.component.ChoiceLayout)this.mCameraBlockedMessage.findViewById(R.id.choiceLayout);
        this.mCameraSensorBlockedChoiceLayout.setChoices(com.navdy.hud.app.ui.component.ChoiceLayout$Mode.LABEL, (java.util.List)a2, 0, (com.navdy.hud.app.ui.component.ChoiceLayout$IListener)this);
        this.mCameraSensorBlockedChoiceLayout.setHighlightPersistent(true);
        String[] a3 = this.getResources().getStringArray(R.array.gesture_tips);
        Object[] a4 = new CharSequence[a3.length];
        int i = 0;
        while(i < a3.length) {
            a4[i] = a3[i];
            i = i + 1;
        }
        this.mScrollableTextPresenter.setTextContents((CharSequence[])a4);
        if (this.mPresenter != null) {
            this.mPresenter.takeView(this);
        }
        this.mGestureLearningView.setVisibility(0);
    }
    
    public boolean onGesture(com.navdy.service.library.events.input.GestureEvent a) {
        return false;
    }
    
    public boolean onKey(com.navdy.hud.app.manager.InputManager$CustomKeyEvent a) {
        boolean b = false;
        if (this.mCurrentMode != com.navdy.hud.app.view.LearnGestureScreenLayout$Mode.SENSOR_BLOCKED) {
            b = false;
        } else {
            switch(com.navdy.hud.app.view.LearnGestureScreenLayout$1.$SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent[a.ordinal()]) {
                case 3: {
                    this.mCameraSensorBlockedChoiceLayout.executeSelectedItem(true);
                    b = true;
                    break;
                }
                case 2: {
                    this.mCameraSensorBlockedChoiceLayout.moveSelectionRight();
                    b = true;
                    break;
                }
                case 1: {
                    this.mCameraSensorBlockedChoiceLayout.moveSelectionLeft();
                    b = true;
                    break;
                }
                default: {
                    b = true;
                }
            }
        }
        return b;
    }
    
    public void showCaptureGestureVideosView() {
        this.mGestureLearningView.setVisibility(8);
        this.mCameraBlockedMessage.setVisibility(8);
        this.mVideoCaptureInstructionsLayout.setVisibility(0);
        this.mScrollableTextPresenter.setVisibility(8);
        this.mCurrentMode = com.navdy.hud.app.view.LearnGestureScreenLayout$Mode.CAPTURE;
    }
    
    public void showSensorBlocked() {
        this.mGestureLearningView.setVisibility(8);
        this.mVideoCaptureInstructionsLayout.setVisibility(8);
        this.mCameraBlockedMessage.setVisibility(0);
        this.mScrollableTextPresenter.setVisibility(8);
        this.mCurrentMode = com.navdy.hud.app.view.LearnGestureScreenLayout$Mode.SENSOR_BLOCKED;
    }
    
    public void showTips() {
        this.mGestureLearningView.setVisibility(8);
        this.mCameraBlockedMessage.setVisibility(8);
        this.mVideoCaptureInstructionsLayout.setVisibility(8);
        this.mScrollableTextPresenter.setVisibility(0);
        this.mCurrentMode = com.navdy.hud.app.view.LearnGestureScreenLayout$Mode.TIPS;
    }
}
