package com.navdy.hud.app.view.drawable;
import com.navdy.hud.app.R;

public class SpeedoMeterGaugeDrawable2 extends com.navdy.hud.app.view.drawable.GaugeDrawable {
    final private static float GAUGE_START_ANGLE = 146.2f;
    final private static float SPEED_LIMIT_MARKER_ANGLE_WIDTH = 4f;
    final public static int TEXT_MARGIN = 15;
    private int mGaugeMargin;
    private int mOuterRingColor;
    private int mSpeedLimit;
    private int mSpeedLimitMarkerColor;
    private int mSpeedLimitTextColor;
    private int mSpeedLimitTextSize;
    private int mValueRingWidth;
    public int speedLimitTextAlpha;
    
    public SpeedoMeterGaugeDrawable2(android.content.Context a, int i, int i0, int i1) {
        super(a, i, i0);
        this.speedLimitTextAlpha = 0;
        this.mValueRingWidth = a.getResources().getDimensionPixelSize(i1);
        this.mGaugeMargin = a.getResources().getDimensionPixelSize(R.dimen.speedometer_gauge_margin);
        this.mSpeedLimitTextColor = a.getResources().getColor(17170443);
        this.mOuterRingColor = this.mColorTable[3];
        this.mSpeedLimitMarkerColor = this.mColorTable[4];
        this.mSpeedLimitTextSize = a.getResources().getDimensionPixelSize(R.dimen.speedometer_gauge_speed_limit_text_size);
    }
    
    public void draw(android.graphics.Canvas a) {
        super.draw(a);
        android.graphics.Rect a0 = this.getBounds();
        int i = a0.width();
        int i0 = this.mValueRingWidth / 2;
        android.graphics.RectF a1 = new android.graphics.RectF((float)(a0.left + i0), (float)(a0.top + i0), (float)(a0.right - i0), (float)(a0.top + i - i0));
        a1.inset((float)this.mGaugeMargin, (float)this.mGaugeMargin);
        this.mPaint.setStrokeWidth((float)this.mValueRingWidth);
        this.mPaint.setColor(this.mOuterRingColor);
        this.mPaint.setStyle(android.graphics.Paint$Style.STROKE);
        a.drawArc(a1, 146.2f, 247.6f, false, this.mPaint);
        this.mPaint.setStrokeWidth((float)this.mValueRingWidth);
        this.mPaint.setStyle(android.graphics.Paint$Style.STROKE);
        this.mPaint.setColor(this.mDefaultColor);
        a.drawArc(a1, 146.2f, this.mValue / this.mMaxValue * 247.6f, false, this.mPaint);
        if (this.mSpeedLimit > 0) {
            this.mPaint.setColor(this.mSpeedLimitMarkerColor);
            float f = (146.2f + (float)this.mSpeedLimit / this.mMaxValue * 247.6f - 2f) % 360f;
            a.drawArc(a1, f, 4f, false, this.mPaint);
            android.graphics.RectF a2 = new android.graphics.RectF(a0);
            a2.inset(15f, 15f);
            int i1 = (int)(a2.width() / 2f);
            int i2 = this.mPaint.getAlpha();
            this.mPaint.setStyle(android.graphics.Paint$Style.FILL);
            this.mPaint.setColor(this.mSpeedLimitTextColor);
            this.mPaint.setTextSize((float)this.mSpeedLimitTextSize);
            this.mPaint.setAlpha(this.speedLimitTextAlpha);
            this.mPaint.setTypeface(android.graphics.Typeface.create(android.graphics.Typeface.DEFAULT, 1));
            String s = Integer.toString(this.mSpeedLimit);
            android.graphics.Rect a3 = new android.graphics.Rect();
            this.mPaint.getTextBounds(s, 0, s.length(), a3);
            int i3 = (int)((double)a2.centerX() + (double)i1 * Math.cos(Math.toRadians((double)f)));
            int i4 = (int)((double)a2.centerY() + (double)i1 * Math.sin(Math.toRadians((double)f)));
            int i5 = a3.width();
            int i6 = a3.height();
            a.drawText(s, (float)(i3 - i5 / 2), (float)(i6 / 2 + i4), this.mPaint);
            this.mPaint.setAlpha(i2);
        }
    }
    
    public void setSpeedLimit(int i) {
        this.mSpeedLimit = i;
    }
    
    public void setState(int i) {
        super.setState(i);
        switch(i) {
            case 2: {
                this.mSpeedLimitMarkerColor = this.mColorTable[4];
                break;
            }
            case 1: {
                this.mSpeedLimitMarkerColor = this.mColorTable[4];
                break;
            }
            case 0: {
                this.mSpeedLimitMarkerColor = this.mColorTable[1];
                break;
            }
        }
    }
}
