package com.navdy.hud.app.view;
import com.navdy.hud.app.R;

public class GestureLearningView extends android.widget.RelativeLayout implements com.navdy.hud.app.ui.component.ChoiceLayout$IListener, com.navdy.hud.app.manager.InputManager$IInputHandler {
    final public static int MOVE_IN_DURATION = 600;
    final public static int MOVE_OUT_DURATION = 250;
    final private static int PROGRESS_INDICATOR_HIDE_INTERVAL = 100;
    final private static int SCROLL_DURATION = 1000;
    final private static int SCROLL_INTERVAL = 4000;
    final private static int SENSOR_BLOCKED_MESSAGE_DELAY = 30000;
    final private static String SMOOTHING_COEFF_KEY = "persist.sys.gesture_smooth";
    final public static int TAG_CAPTURE = 2;
    final public static int TAG_DONE = 0;
    final public static int TAG_TIPS = 1;
    private String[] TIPS;
    @Inject
    com.squareup.otto.Bus mBus;
    @InjectView(R.id.center_image)
    android.widget.ImageView mCenterImage;
    @InjectView(R.id.choiceLayout)
    com.navdy.hud.app.ui.component.ChoiceLayout mChoiceLayout;
    private int mCurrentSmallTipIndex;
    private float mGestureIndicatorHalfWidth;
    private float mGestureIndicatorProgressSpan;
    @InjectView(R.id.gesture_progress_indicator)
    android.view.View mGestureProgressIndicator;
    private float mHandIconMargin;
    private android.os.Handler mHandler;
    private android.animation.AnimatorSet mLeftGestureAnimation;
    @InjectView(R.id.lyt_left_swipe)
    android.widget.LinearLayout mLeftSwipeLayout;
    final private float mLowPassCoeff;
    private float mNeutralX;
    @Inject
    com.navdy.hud.app.screen.GestureLearningScreen$Presenter mPresenter;
    private Runnable mProgressIndicatorRunnable;
    private android.animation.AnimatorSet mRightGestureAnimation;
    @InjectView(R.id.lyt_right_swipe)
    android.widget.LinearLayout mRightSwipeLayout;
    private android.animation.ValueAnimator mScrollAnimator;
    private Runnable mShowSensorBlockedMessageRunnable;
    private float mSmoothedProgress;
    private boolean mStopAnimation;
    @InjectView(R.id.tips_scroller)
    android.widget.LinearLayout mTipsScroller;
    int mTipsScrollerHeight;
    private Runnable mTipsScrollerRunnable;
    @InjectView(R.id.tipsText1)
    android.widget.TextView mTipsTextView1;
    @InjectView(R.id.tipsText2)
    android.widget.TextView mTipsTextView2;
    
    public GestureLearningView(android.content.Context a) {
        this(a, (android.util.AttributeSet)null);
    }
    
    public GestureLearningView(android.content.Context a, android.util.AttributeSet a0) {
        this(a, a0, 0);
    }
    
    public GestureLearningView(android.content.Context a, android.util.AttributeSet a0, int i) {
        super(a, a0, i);
        this.mCurrentSmallTipIndex = 0;
        this.mStopAnimation = false;
        this.mLowPassCoeff = Float.parseFloat(com.navdy.hud.app.util.os.SystemProperties.get("persist.sys.gesture_smooth", "1.0"));
        this.mSmoothedProgress = 0.0f;
        if (!this.isInEditMode()) {
            mortar.Mortar.inject(a, this);
        }
    }
    
    static float access$002(com.navdy.hud.app.view.GestureLearningView a, float f) {
        a.mNeutralX = f;
        return f;
    }
    
    static float access$102(com.navdy.hud.app.view.GestureLearningView a, float f) {
        a.mGestureIndicatorProgressSpan = f;
        return f;
    }
    
    static float access$200(com.navdy.hud.app.view.GestureLearningView a) {
        return a.mHandIconMargin;
    }
    
    static float access$302(com.navdy.hud.app.view.GestureLearningView a, float f) {
        a.mGestureIndicatorHalfWidth = f;
        return f;
    }
    
    static int access$400(com.navdy.hud.app.view.GestureLearningView a) {
        return a.mCurrentSmallTipIndex;
    }
    
    static int access$402(com.navdy.hud.app.view.GestureLearningView a, int i) {
        a.mCurrentSmallTipIndex = i;
        return i;
    }
    
    static String[] access$500(com.navdy.hud.app.view.GestureLearningView a) {
        return a.TIPS;
    }
    
    static boolean access$600(com.navdy.hud.app.view.GestureLearningView a) {
        return a.mStopAnimation;
    }
    
    static Runnable access$700(com.navdy.hud.app.view.GestureLearningView a) {
        return a.mTipsScrollerRunnable;
    }
    
    static android.os.Handler access$800(com.navdy.hud.app.view.GestureLearningView a) {
        return a.mHandler;
    }
    
    static android.animation.ValueAnimator access$900(com.navdy.hud.app.view.GestureLearningView a) {
        return a.mScrollAnimator;
    }
    
    private android.animation.AnimatorSet prepareGestureAnimation(boolean b, android.view.ViewGroup a) {
        android.widget.TextView a0 = null;
        android.widget.ImageView a1 = null;
        if (b) {
            a0 = (android.widget.TextView)a.findViewById(R.id.txt_right);
            a1 = (android.widget.ImageView)a.findViewById(R.id.img_right_hand_overlay);
        } else {
            a0 = (android.widget.TextView)a.findViewById(R.id.txt_left);
            a1 = (android.widget.ImageView)a.findViewById(R.id.img_left_hand_overlay);
        }
        android.animation.AnimatorSet a2 = new android.animation.AnimatorSet();
        android.animation.ValueAnimator a3 = new android.animation.ValueAnimator();
        a3.setDuration(250L);
        int i = this.getResources().getDimensionPixelSize(R.dimen.gesture_icon_animate_distance);
        int[] a4 = new int[2];
        a4[0] = (int)this.mHandIconMargin;
        a4[1] = (int)this.mHandIconMargin + i;
        a3.setIntValues(a4);
        a3.addUpdateListener((android.animation.ValueAnimator$AnimatorUpdateListener)new com.navdy.hud.app.view.GestureLearningView$7(this, a, b));
        android.animation.ValueAnimator a5 = new android.animation.ValueAnimator();
        a5.setDuration(600L);
        int[] a6 = new int[2];
        a6[0] = (int)this.mHandIconMargin + i;
        a6[1] = (int)this.mHandIconMargin;
        a5.setIntValues(a6);
        a5.addUpdateListener((android.animation.ValueAnimator$AnimatorUpdateListener)new com.navdy.hud.app.view.GestureLearningView$8(this, a, b));
        android.animation.ValueAnimator a7 = new android.animation.ValueAnimator();
        a7.setDuration(600L);
        float[] a8 = new float[2];
        a8[0] = 1f;
        a8[1] = 0.0f;
        a7.setFloatValues(a8);
        a7.addUpdateListener((android.animation.ValueAnimator$AnimatorUpdateListener)new com.navdy.hud.app.view.GestureLearningView$9(this, a0, a1));
        a2.play((android.animation.Animator)a3).before((android.animation.Animator)a7).before((android.animation.Animator)a5);
        a2.addListener((android.animation.Animator$AnimatorListener)new com.navdy.hud.app.view.GestureLearningView$10(this, a1, a0));
        return a2;
    }
    
    public void executeItem(int i, int i0) {
        switch(i0) {
            case 2: {
                this.mPresenter.showCaptureView();
                break;
            }
            case 1: {
                this.mPresenter.showTips();
                break;
            }
            case 0: {
                this.mPresenter.finish();
                break;
            }
        }
    }
    
    public void itemSelected(int i, int i0) {
    }
    
    public com.navdy.hud.app.manager.InputManager$IInputHandler nextHandler() {
        return null;
    }
    
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.mBus.unregister(this);
        this.mStopAnimation = true;
        this.mScrollAnimator.cancel();
        this.mHandler.removeCallbacks(this.mTipsScrollerRunnable);
        this.mHandler.removeCallbacks(this.mShowSensorBlockedMessageRunnable);
    }
    
    protected void onFinishInflate() {
        super.onFinishInflate();
        butterknife.ButterKnife.inject((android.view.View)this);
        this.mChoiceLayout.setHighlightPersistent(true);
        android.content.res.Resources a = this.getResources();
        this.mTipsScrollerHeight = a.getDimensionPixelSize(R.dimen.gesture_learning_tips_text_scroll_height);
        this.mHandIconMargin = a.getDimension(R.dimen.gesture_icon_margin_from_center_image);
        this.mLeftGestureAnimation = this.prepareGestureAnimation(false, (android.view.ViewGroup)this.mLeftSwipeLayout);
        this.mRightGestureAnimation = this.prepareGestureAnimation(true, (android.view.ViewGroup)this.mRightSwipeLayout);
        this.TIPS = a.getStringArray(R.array.gesture_small_tips);
        this.mHandler = new android.os.Handler();
        this.mScrollAnimator = new android.animation.ValueAnimator();
        android.animation.ValueAnimator a0 = this.mScrollAnimator;
        int[] a1 = new int[2];
        a1[0] = 0;
        a1[1] = -this.mTipsScrollerHeight;
        a0.setIntValues(a1);
        this.mScrollAnimator.setDuration(1000L);
        this.getViewTreeObserver().addOnGlobalLayoutListener((android.view.ViewTreeObserver$OnGlobalLayoutListener)new com.navdy.hud.app.view.GestureLearningView$1(this));
        this.mScrollAnimator.addUpdateListener((android.animation.ValueAnimator$AnimatorUpdateListener)new com.navdy.hud.app.view.GestureLearningView$2(this));
        this.mScrollAnimator.addListener((android.animation.Animator$AnimatorListener)new com.navdy.hud.app.view.GestureLearningView$3(this));
        String s = this.TIPS[this.mCurrentSmallTipIndex];
        String s0 = this.TIPS[(this.mCurrentSmallTipIndex + 1) % this.TIPS.length];
        this.mTipsTextView1.setText((CharSequence)s);
        this.mTipsTextView2.setText((CharSequence)s0);
        this.mTipsScrollerRunnable = (Runnable)new com.navdy.hud.app.view.GestureLearningView$4(this);
        this.mProgressIndicatorRunnable = (Runnable)new com.navdy.hud.app.view.GestureLearningView$5(this);
        this.mShowSensorBlockedMessageRunnable = (Runnable)new com.navdy.hud.app.view.GestureLearningView$6(this);
        this.mHandler.postDelayed(this.mTipsScrollerRunnable, 4000L);
        java.util.ArrayList a2 = new java.util.ArrayList();
        ((java.util.List)a2).add(new com.navdy.hud.app.ui.component.ChoiceLayout$Choice(this.getContext().getString(R.string.done), 0));
        ((java.util.List)a2).add(new com.navdy.hud.app.ui.component.ChoiceLayout$Choice(this.getContext().getString(R.string.tips), 1));
        this.mChoiceLayout.setChoices(com.navdy.hud.app.ui.component.ChoiceLayout$Mode.LABEL, (java.util.List)a2, 0, (com.navdy.hud.app.ui.component.ChoiceLayout$IListener)this);
        if (!this.isInEditMode()) {
            this.mBus.register(this);
        }
    }
    
    public boolean onGesture(com.navdy.service.library.events.input.GestureEvent a) {
        this.mHandler.removeCallbacks(this.mShowSensorBlockedMessageRunnable);
        switch(com.navdy.hud.app.view.GestureLearningView$11.$SwitchMap$com$navdy$service$library$events$input$Gesture[a.gesture.ordinal()]) {
            case 2: {
                this.mRightGestureAnimation.start();
                break;
            }
            case 1: {
                this.mLeftGestureAnimation.start();
                break;
            }
        }
        return true;
    }
    
    public void onGestureProgress(com.navdy.hud.app.gesture.GestureServiceConnector$GestureProgress a) {
        this.mHandler.removeCallbacks(this.mProgressIndicatorRunnable);
        float f = a.progress;
        if (a.direction == com.navdy.hud.app.gesture.GestureServiceConnector$GestureDirection.LEFT) {
            f = f * -1f;
        }
        float f0 = this.mLowPassCoeff * f + this.mSmoothedProgress * (1f - this.mLowPassCoeff);
        if (f0 != this.mSmoothedProgress) {
            this.mGestureProgressIndicator.setVisibility(0);
            this.mSmoothedProgress = f0;
            this.mGestureProgressIndicator.setX(this.mNeutralX + this.mGestureIndicatorProgressSpan * this.mSmoothedProgress - this.mGestureIndicatorHalfWidth);
        }
        this.mHandler.postDelayed(this.mProgressIndicatorRunnable, 100L);
    }
    
    public boolean onKey(com.navdy.hud.app.manager.InputManager$CustomKeyEvent a) {
        switch(com.navdy.hud.app.view.GestureLearningView$11.$SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent[a.ordinal()]) {
            case 3: {
                this.mChoiceLayout.executeSelectedItem(true);
                break;
            }
            case 2: {
                this.mChoiceLayout.moveSelectionRight();
                break;
            }
            case 1: {
                this.mChoiceLayout.moveSelectionLeft();
                break;
            }
        }
        return false;
    }
    
    public void setVisibility(int i) {
        super.setVisibility(i);
        if (i != 8) {
            if (i == 0) {
                this.mHandler.removeCallbacks(this.mShowSensorBlockedMessageRunnable);
                this.mHandler.postDelayed(this.mShowSensorBlockedMessageRunnable, 30000L);
            }
        } else {
            this.mHandler.removeCallbacks(this.mShowSensorBlockedMessageRunnable);
        }
    }
}
