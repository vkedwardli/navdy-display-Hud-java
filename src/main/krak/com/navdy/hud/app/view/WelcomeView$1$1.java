package com.navdy.hud.app.view;

class WelcomeView$1$1 implements Runnable {
    final com.navdy.hud.app.view.WelcomeView$1 this$1;
    final android.widget.ImageView val$appView;
    final android.widget.ImageView val$playView;
    final android.widget.TextView val$txtView;
    
    WelcomeView$1$1(com.navdy.hud.app.view.WelcomeView$1 a, android.widget.TextView a0, android.widget.ImageView a1, android.widget.ImageView a2) {
        super();
        this.this$1 = a;
        this.val$txtView = a0;
        this.val$appView = a1;
        this.val$playView = a2;
    }
    
    public void run() {
        com.navdy.hud.app.view.WelcomeView.access$008(this.this$1.this$0);
        if (com.navdy.hud.app.view.WelcomeView.access$000(this.this$1.this$0) == com.navdy.hud.app.view.WelcomeView.access$300().length) {
            com.navdy.hud.app.view.WelcomeView.access$002(this.this$1.this$0, 0);
        }
        com.navdy.hud.app.view.WelcomeView.access$500(this.this$1.this$0).removeCallbacks(com.navdy.hud.app.view.WelcomeView.access$400(this.this$1.this$0));
        com.navdy.hud.app.view.WelcomeView.access$500(this.this$1.this$0).postDelayed(com.navdy.hud.app.view.WelcomeView.access$400(this.this$1.this$0), (long)com.navdy.hud.app.view.WelcomeView.access$600());
        com.navdy.hud.app.view.WelcomeView.access$700(this.this$1.this$0, this.val$txtView, com.navdy.hud.app.view.WelcomeView.access$000(this.this$1.this$0));
        com.navdy.hud.app.view.WelcomeView.access$800(this.this$1.this$0, this.val$appView, (com.navdy.hud.app.view.WelcomeView.access$300()[com.navdy.hud.app.view.WelcomeView.access$000(this.this$1.this$0)] != 0) ? 1 : 0);
        com.navdy.hud.app.view.WelcomeView.access$800(this.this$1.this$0, this.val$playView, (com.navdy.hud.app.view.WelcomeView.access$900()[com.navdy.hud.app.view.WelcomeView.access$000(this.this$1.this$0)] != 0) ? 1 : 0);
    }
}
