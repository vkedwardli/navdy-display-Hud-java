package com.navdy.hud.app.view;

public class UpdateConfirmationView extends android.widget.RelativeLayout implements com.navdy.hud.app.ui.component.ChoiceLayout$IListener, com.navdy.hud.app.manager.InputManager$IInputHandler {
    final private static long CONFIRMATION_TIMEOUT = 30000L;
    final private static int UPDATE_MESSAGE_MAX_WIDTH = 360;
    final private static com.navdy.service.library.log.Logger sLogger;
    private android.os.Handler handler;
    private boolean isReminder;
    @InjectView(R.id.choiceLayout)
    com.navdy.hud.app.ui.component.ChoiceLayout mChoiceLayout;
    @InjectView(R.id.image)
    android.widget.ImageView mIcon;
    @InjectView(R.id.title3)
    android.widget.TextView mInfoText;
    @InjectView(R.id.leftSwipe)
    android.widget.ImageView mLefttSwipe;
    @InjectView(R.id.title2)
    android.widget.TextView mMainTitleText;
    @Inject
    com.navdy.hud.app.screen.OSUpdateConfirmationScreen$Presenter mPresenter;
    @InjectView(R.id.rightSwipe)
    android.widget.ImageView mRightSwipe;
    @InjectView(R.id.mainTitle)
    android.widget.TextView mScreenTitleText;
    @InjectView(R.id.title4)
    android.widget.TextView mVersionText;
    @InjectView(R.id.mainSection)
    android.widget.RelativeLayout mainSection;
    private Runnable timeout;
    private String updateVersion;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.view.UpdateConfirmationView.class);
    }
    
    public UpdateConfirmationView(android.content.Context a) {
        this(a, (android.util.AttributeSet)null, 0);
    }
    
    public UpdateConfirmationView(android.content.Context a, android.util.AttributeSet a0) {
        this(a, a0, 0);
    }
    
    public UpdateConfirmationView(android.content.Context a, android.util.AttributeSet a0, int i) {
        super(a, a0, i);
        this.handler = new android.os.Handler();
        this.timeout = (Runnable)new com.navdy.hud.app.view.UpdateConfirmationView$1(this);
        this.updateVersion = "1.3.2887";
        this.isReminder = false;
        if (!this.isInEditMode()) {
            mortar.Mortar.inject(a, this);
        }
    }
    
    static com.navdy.service.library.log.Logger access$000() {
        return sLogger;
    }
    
    public void executeItem(int i, int i0) {
        this.handler.removeCallbacks(this.timeout);
        switch(i) {
            case 1: {
                this.mPresenter.finish();
                if (!this.isReminder) {
                    break;
                }
                com.navdy.hud.app.analytics.AnalyticsSupport.recordUpdatePrompt(false, false, this.updateVersion);
                break;
            }
            case 0: {
                this.mPresenter.install();
                this.mPresenter.finish();
                if (!this.isReminder) {
                    break;
                }
                com.navdy.hud.app.analytics.AnalyticsSupport.recordUpdatePrompt(true, false, this.updateVersion);
                break;
            }
        }
    }
    
    public void itemSelected(int i, int i0) {
    }
    
    public com.navdy.hud.app.manager.InputManager$IInputHandler nextHandler() {
        return null;
    }
    
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
    }
    
    protected void onDetachedFromWindow() {
        this.handler.removeCallbacks(this.timeout);
        super.onDetachedFromWindow();
        com.navdy.hud.app.framework.toast.ToastManager.getInstance().disableToasts(false);
        com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().enableNotifications(true);
        if (this.mPresenter != null) {
            this.mPresenter.dropView((android.view.View)this);
        }
    }
    
    protected void onFinishInflate() {
        String s = null;
        super.onFinishInflate();
        butterknife.ButterKnife.inject((android.view.View)this);
        android.content.res.Resources a = this.getContext().getResources();
        if (this.mPresenter == null) {
            s = "1.2.2884";
        } else {
            this.mPresenter.takeView(this);
            com.navdy.hud.app.framework.toast.ToastManager.getInstance().disableToasts(true);
            com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().enableNotifications(false);
            this.updateVersion = this.mPresenter.getUpdateVersion();
            s = this.mPresenter.getCurrentVersion();
            this.isReminder = this.mPresenter.isReminder();
        }
        android.widget.TextView a0 = this.mVersionText;
        String s0 = a.getString(R.string.update_navdy_will_upgrade_from_to);
        Object[] a1 = new Object[2];
        a1[0] = this.updateVersion;
        a1[1] = s;
        a0.setText((CharSequence)String.format(s0, a1));
        this.mVersionText.setVisibility(0);
        this.mScreenTitleText.setVisibility(8);
        this.findViewById(R.id.title1).setVisibility(8);
        this.mMainTitleText.setText(R.string.update_ready_to_install);
        com.navdy.hud.app.util.ViewUtil.adjustPadding((android.view.View)this.mainSection, 0, 0, 0, 10);
        com.navdy.hud.app.util.ViewUtil.autosize(this.mMainTitleText, 2, 360, R.array.title_sizes);
        this.mMainTitleText.setVisibility(0);
        this.mInfoText.setText(R.string.ota_update_installation_will_take);
        this.mInfoText.setVisibility(0);
        this.mIcon.setImageResource(R.drawable.icon_software_update);
        ((com.navdy.hud.app.view.MaxWidthLinearLayout)this.findViewById(R.id.infoContainer)).setMaxWidth(360);
        java.util.ArrayList a2 = new java.util.ArrayList();
        if (this.isReminder) {
            ((java.util.List)a2).add(new com.navdy.hud.app.ui.component.ChoiceLayout$Choice(a.getString(R.string.install_now), 0));
            ((java.util.List)a2).add(new com.navdy.hud.app.ui.component.ChoiceLayout$Choice(a.getString(R.string.install_later), 1));
        } else {
            ((java.util.List)a2).add(new com.navdy.hud.app.ui.component.ChoiceLayout$Choice(a.getString(R.string.install), 0));
            ((java.util.List)a2).add(new com.navdy.hud.app.ui.component.ChoiceLayout$Choice(a.getString(R.string.cancel), 1));
        }
        this.mChoiceLayout.setChoices(com.navdy.hud.app.ui.component.ChoiceLayout$Mode.LABEL, (java.util.List)a2, 1, (com.navdy.hud.app.ui.component.ChoiceLayout$IListener)this);
        this.mRightSwipe.setVisibility(0);
        this.mLefttSwipe.setVisibility(0);
        this.handler.removeCallbacks(this.timeout);
        this.handler.postDelayed(this.timeout, 30000L);
    }
    
    public boolean onGesture(com.navdy.service.library.events.input.GestureEvent a) {
        boolean b = false;
        com.navdy.service.library.events.input.Gesture a0 = a.gesture;
        label0: {
            label1: {
                if (a0 == null) {
                    break label1;
                }
                switch(com.navdy.hud.app.view.UpdateConfirmationView$2.$SwitchMap$com$navdy$service$library$events$input$Gesture[a.gesture.ordinal()]) {
                    case 2: {
                        this.executeItem(1, 0);
                        b = true;
                        break label0;
                    }
                    case 1: {
                        this.executeItem(0, 0);
                        b = true;
                        break label0;
                    }
                }
            }
            b = false;
        }
        return b;
    }
    
    public boolean onKey(com.navdy.hud.app.manager.InputManager$CustomKeyEvent a) {
        boolean b = false;
        this.handler.removeCallbacks(this.timeout);
        com.navdy.hud.app.ui.component.ChoiceLayout a0 = this.mChoiceLayout;
        label2: {
            label0: {
                label1: {
                    if (a0 == null) {
                        break label1;
                    }
                    if (this.mChoiceLayout.getVisibility() == 0) {
                        break label0;
                    }
                }
                b = false;
                break label2;
            }
            switch(com.navdy.hud.app.view.UpdateConfirmationView$2.$SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent[a.ordinal()]) {
                case 3: {
                    this.mChoiceLayout.executeSelectedItem(true);
                    b = true;
                    break;
                }
                case 2: {
                    this.mChoiceLayout.moveSelectionRight();
                    b = true;
                    break;
                }
                case 1: {
                    this.mChoiceLayout.moveSelectionLeft();
                    b = true;
                    break;
                }
                default: {
                    b = true;
                }
            }
        }
        return b;
    }
}
