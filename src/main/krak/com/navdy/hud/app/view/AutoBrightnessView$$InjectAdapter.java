package com.navdy.hud.app.view;

final public class AutoBrightnessView$$InjectAdapter extends dagger.internal.Binding implements dagger.MembersInjector {
    private dagger.internal.Binding presenter;
    
    public AutoBrightnessView$$InjectAdapter() {
        super((String)null, "members/com.navdy.hud.app.view.AutoBrightnessView", false, com.navdy.hud.app.view.AutoBrightnessView.class);
    }
    
    public void attach(dagger.internal.Linker a) {
        this.presenter = a.requestBinding("com.navdy.hud.app.screen.AutoBrightnessScreen$Presenter", com.navdy.hud.app.view.AutoBrightnessView.class, (this).getClass().getClassLoader());
    }
    
    public void getDependencies(java.util.Set a, java.util.Set a0) {
        a0.add(this.presenter);
    }
    
    public void injectMembers(com.navdy.hud.app.view.AutoBrightnessView a) {
        a.presenter = (com.navdy.hud.app.screen.AutoBrightnessScreen$Presenter)this.presenter.get();
    }
    
    public void injectMembers(Object a) {
        this.injectMembers((com.navdy.hud.app.view.AutoBrightnessView)a);
    }
}
