package com.navdy.hud.app.view;

class ToastView$2 implements Runnable {
    final com.navdy.hud.app.view.ToastView this$0;
    final String val$screenName;
    
    ToastView$2(com.navdy.hud.app.view.ToastView a, String s) {
        super();
        this.this$0 = a;
        this.val$screenName = s;
    }
    
    public void run() {
        com.navdy.hud.app.view.ToastView.access$000().v(new StringBuilder().append("toast:animationIn :").append(this.val$screenName).toString());
        com.navdy.hud.app.view.ToastView.access$000().v("[focus] toastView");
        this.this$0.inputManager.setFocus((com.navdy.hud.app.manager.InputManager$IInputHandler)this.this$0);
        this.this$0.setVisibility(0);
        com.navdy.hud.app.framework.toast.ToastManager.getInstance().setToastDisplayFlag(true);
    }
}
