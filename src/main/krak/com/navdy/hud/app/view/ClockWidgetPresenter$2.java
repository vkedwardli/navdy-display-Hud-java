package com.navdy.hud.app.view;

class ClockWidgetPresenter$2 {
    final static int[] $SwitchMap$com$navdy$hud$app$view$ClockWidgetPresenter$ClockType;
    
    static {
        $SwitchMap$com$navdy$hud$app$view$ClockWidgetPresenter$ClockType = new int[com.navdy.hud.app.view.ClockWidgetPresenter$ClockType.values().length];
        int[] a = $SwitchMap$com$navdy$hud$app$view$ClockWidgetPresenter$ClockType;
        com.navdy.hud.app.view.ClockWidgetPresenter$ClockType a0 = com.navdy.hud.app.view.ClockWidgetPresenter$ClockType.ANALOG;
        try {
            a[a0.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$view$ClockWidgetPresenter$ClockType[com.navdy.hud.app.view.ClockWidgetPresenter$ClockType.DIGITAL1.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException0) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$view$ClockWidgetPresenter$ClockType[com.navdy.hud.app.view.ClockWidgetPresenter$ClockType.DIGITAL2.ordinal()] = 3;
        } catch(NoSuchFieldError ignoredException1) {
        }
    }
}
