package com.navdy.hud.app.view;

class ETAGaugePresenter$2 {
    final static int[] $SwitchMap$com$navdy$service$library$events$navigation$DistanceUnit;
    
    static {
        $SwitchMap$com$navdy$service$library$events$navigation$DistanceUnit = new int[com.navdy.service.library.events.navigation.DistanceUnit.values().length];
        int[] a = $SwitchMap$com$navdy$service$library$events$navigation$DistanceUnit;
        com.navdy.service.library.events.navigation.DistanceUnit a0 = com.navdy.service.library.events.navigation.DistanceUnit.DISTANCE_METERS;
        try {
            a[a0.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$navigation$DistanceUnit[com.navdy.service.library.events.navigation.DistanceUnit.DISTANCE_KMS.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException0) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$navigation$DistanceUnit[com.navdy.service.library.events.navigation.DistanceUnit.DISTANCE_MILES.ordinal()] = 3;
        } catch(NoSuchFieldError ignoredException1) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$navigation$DistanceUnit[com.navdy.service.library.events.navigation.DistanceUnit.DISTANCE_FEET.ordinal()] = 4;
        } catch(NoSuchFieldError ignoredException2) {
        }
    }
}
