package com.navdy.hud.app.presenter;

public class NotificationPresenter extends com.navdy.hud.app.ui.framework.BasePresenter {
    final private static com.navdy.service.library.log.Logger sLogger;
    @Inject
    com.squareup.otto.Bus bus;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.presenter.NotificationPresenter.class);
    }
    
    public NotificationPresenter() {
    }
    
    public void onLoad(android.os.Bundle a) {
        super.onLoad(a);
        this.bus.register(this);
    }
}
