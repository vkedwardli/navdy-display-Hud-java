package com.navdy.hud.app.maps.here;
import com.navdy.hud.app.R;

public class HereNavigationManager {
    final private static com.navdy.hud.app.maps.MapEvents$ArrivalEvent ARRIVAL_EVENT;
    final private static int ARRIVAL_GEO_FENCE_THRESHOLD = 804;
    final static String EMPTY_STR = "";
    final private static int ETA_CALCULATION_INTERVAL = 30000;
    final private static int ETA_UPDATE_THRESHOLD = 60000;
    private static java.util.Map LANGUAGE_HIERARCHY;
    final static com.navdy.hud.app.maps.MapEvents$SpeedWarning SPEED_EXCEEDED;
    final static com.navdy.hud.app.maps.MapEvents$SpeedWarning SPEED_NORMAL;
    final private static String TAG_AUDIO_CALLBACK = "[CB-AUDIO]";
    final private static String TAG_GPS_CALLBACK = "[CB-GPS]";
    final private static String TAG_NAV_MANAGER_CALLBACK = "[CB-NAVM]";
    final private static String TAG_NEW_MANEUVER_CALLBACK = "[CB-NEWM]";
    final private static String TAG_POS_UPDATE_CALLBACK = "[CB-POS-UPDATE]";
    final private static String TAG_REROUTE_CALLBACK = "[CB-REROUTE]";
    final private static String TAG_SAFETY_CALLBACK = "[CB-SAFE]";
    final private static String TAG_SHOWLANE_INFO_CALLBACK = "[CB-LANEINFO]";
    final private static String TAG_SPEED_CALLBACK = "[CB-SPD]";
    final private static String TAG_TRAFFIC_ETA_TRACKER = "[CB-TRAFFIC-ETA]";
    final private static String TAG_TRAFFIC_REROUTE_CALLBACK = "[CB-TRAFFIC-REROUTE]";
    final private static String TAG_TRAFFIC_UPDATE = "[CB-TRAFFIC-UPDATE]";
    final private static String TAG_TRAFFIC_WARN = "[CB-TRAFFIC-WARN]";
    final private static String TAG_TTS = "[CB-TTS]";
    final private static String TBT_LANGUAGE_CODE = "en-US";
    final private static boolean VERBOSE = false;
    private static android.content.Context context;
    final private static com.navdy.hud.app.maps.here.HereMapsManager hereMapsManager;
    final private static com.navdy.hud.app.maps.MapsEventHandler mapsEventHandler;
    final static com.navdy.service.library.log.Logger sLogger;
    final private static com.navdy.hud.app.maps.here.HereNavigationManager sSingleton;
    final private static com.navdy.hud.app.manager.SpeedManager speedManager;
    final private static Object trafficRerouteLock;
    final com.navdy.hud.app.maps.MapEvents$GpsStatusChange BUS_GPS_SIGNAL_LOST;
    final com.navdy.hud.app.maps.MapEvents$GpsStatusChange BUS_GPS_SIGNAL_RESTORED;
    final private String HERE_TBT_AUDIO_DESTINATION_REACHED_PATTERN;
    private String INVALID_ROUTE_ID;
    private String INVALID_STATE;
    private String NO_CURRENT_POSITION;
    String TTS_KMS;
    String TTS_METERS;
    String TTS_MILES;
    com.navdy.hud.app.event.LocalSpeechRequest TTS_REROUTING;
    com.navdy.hud.app.event.LocalSpeechRequest TTS_REROUTING_FAILED;
    private com.here.android.mpa.guidance.AudioPlayerDelegate audioPlayerDelegate;
    private com.navdy.hud.app.framework.network.NetworkBandwidthController bandwidthController;
    private com.squareup.otto.Bus bus;
    private com.here.android.mpa.routing.Maneuver currentManeuver;
    private com.navdy.hud.app.maps.here.HereNavigationInfo currentNavigationInfo;
    private com.navdy.hud.app.maps.NavigationMode currentNavigationMode;
    private com.navdy.service.library.events.navigation.NavigationSessionState currentSessionState;
    private String debugManeuverDistance;
    private int debugManeuverIcon;
    private String debugManeuverInstruction;
    private com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder$ManeuverState debugManeuverState;
    private int debugNextManeuverIcon;
    private com.navdy.hud.app.profile.DriverSessionPreferences driverSessionPreferences;
    private Runnable etaCalcRunnable;
    private Boolean generatePhoneticTTS;
    private com.navdy.hud.app.maps.here.HereGpsSignalListener gpsSignalListener;
    private android.os.Handler handler;
    private com.navdy.hud.app.maps.here.HereNavController hereNavController;
    private com.navdy.hud.app.maps.here.HereRealisticViewListener hereRealisticViewListener;
    private com.navdy.hud.app.maps.here.HereLaneInfoListener laneInfoListener;
    private com.here.android.mpa.routing.Maneuver maneuverAfterCurrent;
    private com.navdy.hud.app.maps.here.HereMapController mapController;
    private com.here.android.mpa.mapping.MapView mapView;
    private com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager navdyTrafficRerouteManager;
    private com.here.android.mpa.guidance.NavigationManager navigationManager;
    private com.navdy.hud.app.maps.here.HereNavigationEventListener navigationManagerEventListener;
    private com.navdy.hud.app.ui.component.homescreen.NavigationView navigationView;
    private com.navdy.hud.app.maps.here.HereNewManeuverListener newManeuverEventListener;
    private com.navdy.hud.app.maps.here.HerePositionUpdateListener positionUpdateListener;
    private com.here.android.mpa.routing.Maneuver prevManeuver;
    private com.navdy.hud.app.maps.here.HereRerouteListener reRouteListener;
    private com.navdy.hud.app.maps.here.HereSafetySpotListener safetySpotListener;
    private android.content.SharedPreferences sharedPreferences;
    private com.navdy.hud.app.maps.here.HereSpeedWarningManager speedWarningManager;
    private StringBuilder stringBuilder;
    private boolean switchingToNewRoute;
    private com.navdy.hud.app.common.TimeHelper timeHelper;
    private com.navdy.hud.app.maps.here.HereTrafficETATracker trafficEtaTracker;
    private com.navdy.hud.app.maps.here.HereTrafficRerouteListener trafficRerouteListener;
    private com.navdy.hud.app.maps.here.HereTrafficUpdater2 trafficUpdater;
    private boolean trafficUpdaterStarted;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.maps.here.HereNavigationManager.class);
        LANGUAGE_HIERARCHY = (java.util.Map)new java.util.HashMap();
        LANGUAGE_HIERARCHY.put("en-AU", "en-GB");
        LANGUAGE_HIERARCHY.put("en-GB", "en");
        LANGUAGE_HIERARCHY.put("en-US", "en");
        context = com.navdy.hud.app.HudApplication.getAppContext();
        hereMapsManager = com.navdy.hud.app.maps.here.HereMapsManager.getInstance();
        mapsEventHandler = com.navdy.hud.app.maps.MapsEventHandler.getInstance();
        speedManager = com.navdy.hud.app.manager.SpeedManager.getInstance();
        trafficRerouteLock = new Object();
        sSingleton = new com.navdy.hud.app.maps.here.HereNavigationManager();
        ARRIVAL_EVENT = new com.navdy.hud.app.maps.MapEvents$ArrivalEvent();
        SPEED_EXCEEDED = new com.navdy.hud.app.maps.MapEvents$SpeedWarning(true);
        SPEED_NORMAL = new com.navdy.hud.app.maps.MapEvents$SpeedWarning(false);
    }
    
    private HereNavigationManager() {
        this.currentNavigationInfo = new com.navdy.hud.app.maps.here.HereNavigationInfo();
        this.bus = com.navdy.hud.app.maps.MapsEventHandler.getInstance().getBus();
        this.sharedPreferences = com.navdy.hud.app.maps.MapsEventHandler.getInstance().getSharedPreferences();
        this.BUS_GPS_SIGNAL_LOST = new com.navdy.hud.app.maps.MapEvents$GpsStatusChange(false);
        this.BUS_GPS_SIGNAL_RESTORED = new com.navdy.hud.app.maps.MapEvents$GpsStatusChange(true);
        this.stringBuilder = new StringBuilder();
        this.etaCalcRunnable = (Runnable)new com.navdy.hud.app.maps.here.HereNavigationManager$1(this);
        this.bandwidthController = com.navdy.hud.app.framework.network.NetworkBandwidthController.getInstance();
        this.handler = new android.os.Handler(android.os.Looper.getMainLooper());
        String s = java.util.Locale.getDefault().getLanguage();
        android.content.res.Resources a = com.navdy.hud.app.HudApplication.getAppContext().getResources();
        this.TTS_REROUTING = new com.navdy.hud.app.event.LocalSpeechRequest(new com.navdy.service.library.events.audio.SpeechRequest$Builder().words(a.getString(R.string.tts_recalculate_route)).category(com.navdy.service.library.events.audio.SpeechRequest$Category.SPEECH_REROUTE).language(s).build());
        this.TTS_REROUTING_FAILED = new com.navdy.hud.app.event.LocalSpeechRequest(new com.navdy.service.library.events.audio.SpeechRequest$Builder().words(a.getString(R.string.tts_recalculate_route_failed)).category(com.navdy.service.library.events.audio.SpeechRequest$Category.SPEECH_REROUTE).language(s).build());
        this.TTS_MILES = a.getString(R.string.tts_miles_per_hour);
        this.TTS_KMS = a.getString(R.string.tts_kms_per_hour);
        this.TTS_METERS = a.getString(R.string.tts_meters_per_second);
        this.INVALID_STATE = a.getString(R.string.invalid_state);
        this.NO_CURRENT_POSITION = a.getString(R.string.no_current_position);
        this.INVALID_ROUTE_ID = a.getString(R.string.invalid_routeid);
        this.HERE_TBT_AUDIO_DESTINATION_REACHED_PATTERN = a.getString(R.string.tbt_audio_destination_reached_pattern).toLowerCase();
        this.currentSessionState = com.navdy.service.library.events.navigation.NavigationSessionState.NAV_SESSION_ENGINE_NOT_READY;
        this.timeHelper = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getTimeHelper();
        this.initNavigationManager();
    }
    
    static com.navdy.hud.app.maps.NavigationMode access$000(com.navdy.hud.app.maps.here.HereNavigationManager a) {
        return a.currentNavigationMode;
    }
    
    static com.navdy.hud.app.maps.here.HereNavigationInfo access$100(com.navdy.hud.app.maps.here.HereNavigationManager a) {
        return a.currentNavigationInfo;
    }
    
    static void access$1000(com.navdy.hud.app.maps.here.HereNavigationManager a, com.here.android.mpa.routing.Maneuver a0, com.here.android.mpa.routing.Maneuver a1, com.here.android.mpa.routing.Maneuver a2, boolean b) {
        a.updateNavigationInfoInternal(a0, a1, a2, b);
    }
    
    static void access$1100(com.navdy.hud.app.maps.here.HereNavigationManager a) {
        a.setNavigationManagerUnit();
    }
    
    static com.navdy.hud.app.maps.MapEvents$ArrivalEvent access$1200() {
        return ARRIVAL_EVENT;
    }
    
    static void access$1300(com.navdy.hud.app.maps.here.HereNavigationManager a) {
        a.postArrivedManeuver();
    }
    
    static com.navdy.service.library.events.navigation.NavigationSessionState access$1402(com.navdy.hud.app.maps.here.HereNavigationManager a, com.navdy.service.library.events.navigation.NavigationSessionState a0) {
        a.currentSessionState = a0;
        return a0;
    }
    
    static android.os.Handler access$200(com.navdy.hud.app.maps.here.HereNavigationManager a) {
        return a.handler;
    }
    
    static String access$300(com.navdy.hud.app.maps.here.HereNavigationManager a) {
        return a.HERE_TBT_AUDIO_DESTINATION_REACHED_PATTERN;
    }
    
    static android.content.Context access$400() {
        return context;
    }
    
    static com.squareup.otto.Bus access$500(com.navdy.hud.app.maps.here.HereNavigationManager a) {
        return a.bus;
    }
    
    static com.navdy.hud.app.maps.here.HereMapsManager access$600() {
        return hereMapsManager;
    }
    
    static com.navdy.hud.app.maps.here.HereNavController access$700(com.navdy.hud.app.maps.here.HereNavigationManager a) {
        return a.hereNavController;
    }
    
    static com.navdy.hud.app.maps.MapsEventHandler access$800() {
        return mapsEventHandler;
    }
    
    static void access$900(com.navdy.hud.app.maps.here.HereNavigationManager a, com.navdy.service.library.events.navigation.NavigationSessionRequest a0) {
        a.handleNavigationSessionRequestInternal(a0);
    }
    
    private void addDestinationMarker(com.here.android.mpa.common.GeoCoordinate a, int i) {
        try {
            if (this.currentNavigationInfo.mapDestinationMarker != null) {
                this.removeDestinationMarker();
                this.currentNavigationInfo.mapDestinationMarker = null;
            }
            com.here.android.mpa.common.Image a0 = new com.here.android.mpa.common.Image();
            a0.setImageResource(i);
            this.currentNavigationInfo.mapDestinationMarker = new com.here.android.mpa.mapping.MapMarker(a, a0);
            this.mapController.addMapObject((com.here.android.mpa.mapping.MapObject)this.currentNavigationInfo.mapDestinationMarker);
            hereMapsManager.getLocationFixManager().addMarkers(this.mapController);
            sLogger.v("added destination marker");
        } catch(Throwable a1) {
            sLogger.e(a1);
        }
    }
    
    private void dumpVoiceSkinInfo(java.util.List a) {
        StringBuffer a0 = new StringBuffer("Supported voice skins: ");
        Object a1 = a.iterator();
        while(((java.util.Iterator)a1).hasNext()) {
            com.here.android.mpa.guidance.VoiceSkin a2 = (com.here.android.mpa.guidance.VoiceSkin)((java.util.Iterator)a1).next();
            a0.append("[language:");
            a0.append(a2.getLanguage());
            a0.append(",code:");
            a0.append(a2.getLanguageCode());
            a0.append("],");
        }
        sLogger.i(a0.toString());
    }
    
    private com.here.android.mpa.guidance.VoiceSkin findSkinSupporting(java.util.List a, String s) {
        com.here.android.mpa.guidance.VoiceSkin a0 = null;
        if (a != null) {
            if (s != null) {
                String s0 = s.toLowerCase(java.util.Locale.US);
                Object a1 = a.iterator();
                while(true) {
                    if (((java.util.Iterator)a1).hasNext()) {
                        a0 = (com.here.android.mpa.guidance.VoiceSkin)((java.util.Iterator)a1).next();
                        if (!a0.getLanguageCode().toLowerCase(java.util.Locale.US).startsWith(s0)) {
                            continue;
                        }
                        break;
                    } else {
                        a0 = null;
                        break;
                    }
                }
            } else {
                a0 = null;
            }
        } else {
            a0 = null;
        }
        return a0;
    }
    
    private static com.here.android.mpa.guidance.NavigationManager$MapUpdateMode getHereMapUpdateMode() {
        return com.here.android.mpa.guidance.NavigationManager$MapUpdateMode.NONE;
    }
    
    public static com.navdy.hud.app.maps.here.HereNavigationManager getInstance() {
        return sSingleton;
    }
    
    private void handleNavigationSessionRequestInternal(com.navdy.service.library.events.navigation.NavigationSessionRequest a) {
        label0: synchronized(this) {
            com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
            com.navdy.service.library.log.Logger a0 = sLogger;
            try {
                Throwable a1 = null;
                a0.v(new StringBuilder().append("NavigationSessionRequest id[").append(a.routeId).append("] newState[").append(a.newState).append("] label[").append(a.label).append("] sim-speed[").append(a.simulationSpeed).append("] currentState[").append(this.currentSessionState).append("]").toString());
                boolean b = com.navdy.hud.app.maps.here.HereMapUtil.isValidNavigationState(a.newState);
                label1: {
                    Throwable a2 = null;
                    if (b) {
                        if (hereMapsManager.getLocationFixManager().getLastGeoCoordinate() != null) {
                            boolean b0 = false;
                            String s = a.routeId;
                            com.navdy.hud.app.maps.here.HereRouteCache$RouteInfo a3 = null;
                            if (s == null) {
                                b0 = false;
                            } else {
                                a3 = com.navdy.hud.app.maps.here.HereRouteCache.getInstance().getRoute(a.routeId);
                                b0 = true;
                            }
                            if (this.isNavigationModeOn()) {
                                sLogger.v("navigation is active");
                                switch(com.navdy.hud.app.maps.here.HereNavigationManager$12.$SwitchMap$com$navdy$service$library$events$navigation$NavigationSessionState[a.newState.ordinal()]) {
                                    case 3: {
                                        boolean b1 = false;
                                        label5: {
                                            if (a3 != null) {
                                                break label5;
                                            }
                                            if (!b0) {
                                                break label5;
                                            }
                                            this.returnResponse(com.navdy.service.library.events.RequestStatus.REQUEST_INVALID_REQUEST, this.INVALID_ROUTE_ID, (com.navdy.service.library.events.navigation.NavigationSessionState)null, a.routeId);
                                            break label0;
                                        }
                                        String s0 = a.routeId;
                                        label4: {
                                            label2: {
                                                label3: {
                                                    if (s0 == null) {
                                                        break label3;
                                                    }
                                                    if (!android.text.TextUtils.equals((CharSequence)a.routeId, (CharSequence)this.currentNavigationInfo.routeId)) {
                                                        break label2;
                                                    }
                                                }
                                                b1 = true;
                                                break label4;
                                            }
                                            b1 = false;
                                        }
                                        if (b1) {
                                            sLogger.v("same route");
                                            if (this.currentSessionState != com.navdy.service.library.events.navigation.NavigationSessionState.NAV_SESSION_STARTED) {
                                                sLogger.v("resume navigation");
                                                com.here.android.mpa.guidance.NavigationManager$Error a4 = this.hereNavController.resume();
                                                if (a4 != com.here.android.mpa.guidance.NavigationManager$Error.NONE) {
                                                    sLogger.e(new StringBuilder().append("cannot resume navigation:").append(a4).toString());
                                                    com.navdy.service.library.events.RequestStatus a5 = com.navdy.service.library.events.RequestStatus.REQUEST_SERVICE_ERROR;
                                                    android.content.Context a6 = context;
                                                    Object[] a7 = new Object[1];
                                                    a7[0] = a4;
                                                    this.returnResponse(a5, a6.getString(R.string.unable_to_resume, a7), com.navdy.service.library.events.navigation.NavigationSessionState.NAV_SESSION_STARTED, a.routeId);
                                                    if (this.setNavigationMode(com.navdy.hud.app.maps.NavigationMode.MAP, (com.navdy.hud.app.maps.here.HereNavigationInfo)null, (StringBuilder)null)) {
                                                        break label0;
                                                    }
                                                    sLogger.w("cannot even go to tracking mode");
                                                    break label0;
                                                } else {
                                                    this.currentSessionState = com.navdy.service.library.events.navigation.NavigationSessionState.NAV_SESSION_STARTED;
                                                    this.returnResponse(com.navdy.service.library.events.RequestStatus.REQUEST_SUCCESS, "", com.navdy.service.library.events.navigation.NavigationSessionState.NAV_SESSION_STARTED, a.routeId);
                                                    this.postNavigationSessionStatusEvent(false);
                                                    break label0;
                                                }
                                            } else {
                                                this.returnResponse(com.navdy.service.library.events.RequestStatus.REQUEST_SUCCESS, "", com.navdy.service.library.events.navigation.NavigationSessionState.NAV_SESSION_STARTED, a.routeId);
                                                break label0;
                                            }
                                        } else {
                                            sLogger.v("different route: switching to new route");
                                            StringBuilder a8 = new StringBuilder();
                                            sLogger.v("stop navigation");
                                            try {
                                                long j = android.os.SystemClock.elapsedRealtime();
                                                this.switchingToNewRoute = true;
                                                if (this.setNavigationMode(com.navdy.hud.app.maps.NavigationMode.MAP, (com.navdy.hud.app.maps.here.HereNavigationInfo)null, a8)) {
                                                    sLogger.v("start navigation");
                                                    this.startNavigation(a, a3);
                                                    long j0 = android.os.SystemClock.elapsedRealtime();
                                                    sLogger.v(new StringBuilder().append("navigation took[").append(j0 - j).append("]").toString());
                                                } else {
                                                    this.returnResponse(com.navdy.service.library.events.RequestStatus.REQUEST_SERVICE_ERROR, a8.toString(), (com.navdy.service.library.events.navigation.NavigationSessionState)null, a.routeId);
                                                }
                                            } catch(Throwable a9) {
                                                a1 = a9;
                                                break label1;
                                            }
                                            this.switchingToNewRoute = false;
                                            break label0;
                                        }
                                    }
                                    case 2: {
                                        sLogger.v("stopping navigation");
                                        StringBuilder a10 = new StringBuilder();
                                        if (this.setNavigationMode(com.navdy.hud.app.maps.NavigationMode.MAP, (com.navdy.hud.app.maps.here.HereNavigationInfo)null, a10)) {
                                            this.navigationView.resetMapOverview();
                                            this.returnResponse(com.navdy.service.library.events.RequestStatus.REQUEST_SUCCESS, "", this.currentSessionState, a.routeId);
                                            break label0;
                                        } else {
                                            this.returnResponse(com.navdy.service.library.events.RequestStatus.REQUEST_SERVICE_ERROR, a10.toString(), (com.navdy.service.library.events.navigation.NavigationSessionState)null, a.routeId);
                                            break label0;
                                        }
                                    }
                                    case 1: {
                                        if (this.currentSessionState != com.navdy.service.library.events.navigation.NavigationSessionState.NAV_SESSION_STARTED) {
                                            sLogger.v("already paused");
                                            this.returnResponse(com.navdy.service.library.events.RequestStatus.REQUEST_SUCCESS, "", com.navdy.service.library.events.navigation.NavigationSessionState.NAV_SESSION_PAUSED, (String)null);
                                            break label0;
                                        } else {
                                            sLogger.v("pausing navigation");
                                            this.hereNavController.pause();
                                            this.currentSessionState = com.navdy.service.library.events.navigation.NavigationSessionState.NAV_SESSION_PAUSED;
                                            this.returnResponse(com.navdy.service.library.events.RequestStatus.REQUEST_SUCCESS, "", com.navdy.service.library.events.navigation.NavigationSessionState.NAV_SESSION_PAUSED, a.routeId);
                                            this.postNavigationSessionStatusEvent(false);
                                            break label0;
                                        }
                                    }
                                    default: {
                                        break label0;
                                    }
                                }
                            } else {
                                sLogger.v("navigation is not active");
                                switch(com.navdy.hud.app.maps.here.HereNavigationManager$12.$SwitchMap$com$navdy$service$library$events$navigation$NavigationSessionState[a.newState.ordinal()]) {
                                    case 3: {
                                        if (a3 != null) {
                                            try {
                                                long j1 = android.os.SystemClock.elapsedRealtime();
                                                this.startNavigation(a, a3);
                                                long j2 = android.os.SystemClock.elapsedRealtime();
                                                sLogger.v(new StringBuilder().append("navigation took[").append(j2 - j1).append("]").toString());
                                            } catch(Throwable a11) {
                                                a2 = a11;
                                                break;
                                            }
                                            break label0;
                                        } else {
                                            sLogger.e("valid route id required");
                                            this.returnResponse(com.navdy.service.library.events.RequestStatus.REQUEST_INVALID_REQUEST, this.INVALID_ROUTE_ID, (com.navdy.service.library.events.navigation.NavigationSessionState)null, a.routeId);
                                            break label0;
                                        }
                                    }
                                    case 2: {
                                        sLogger.v("already stopped");
                                        this.returnResponse(com.navdy.service.library.events.RequestStatus.REQUEST_SUCCESS, "", com.navdy.service.library.events.navigation.NavigationSessionState.NAV_SESSION_STOPPED, (String)null);
                                        break label0;
                                    }
                                    case 1: {
                                        sLogger.e(new StringBuilder().append("cannot pause navigation current state:").append(this.currentSessionState).toString());
                                        com.navdy.service.library.events.RequestStatus a12 = com.navdy.service.library.events.RequestStatus.REQUEST_INVALID_STATE;
                                        android.content.Context a13 = context;
                                        Object[] a14 = new Object[2];
                                        a14[0] = this.currentSessionState.name();
                                        a14[1] = a.newState.name();
                                        this.returnResponse(a12, a13.getString(R.string.cannot_move_state, a14), (com.navdy.service.library.events.navigation.NavigationSessionState)null, a.routeId);
                                        break label0;
                                    }
                                    default: {
                                        break label0;
                                    }
                                }
                            }
                        } else {
                            this.returnResponse(com.navdy.service.library.events.RequestStatus.REQUEST_NO_LOCATION_SERVICE, this.NO_CURRENT_POSITION, (com.navdy.service.library.events.navigation.NavigationSessionState)null, a.routeId);
                            break label0;
                        }
                    } else {
                        this.returnResponse(com.navdy.service.library.events.RequestStatus.REQUEST_INVALID_REQUEST, this.INVALID_STATE, (com.navdy.service.library.events.navigation.NavigationSessionState)null, a.routeId);
                        break label0;
                    }
                    throw a2;
                }
                this.switchingToNewRoute = false;
                throw a1;
            } catch(Throwable a15) {
                sLogger.e("handleNavigationSessionRequest", a15);
                this.returnResponse(com.navdy.service.library.events.RequestStatus.REQUEST_SERVICE_ERROR, a15.toString(), (com.navdy.service.library.events.navigation.NavigationSessionState)null, a.routeId);
            }
        }
        /*monexit(this)*/;
    }
    
    private void initNavigationManager() {
        this.navigationManager = com.here.android.mpa.guidance.NavigationManager.getInstance();
        this.hereNavController = new com.navdy.hud.app.maps.here.HereNavController(this.navigationManager, this.bus);
        this.mapController = hereMapsManager.getMapController();
        this.navigationManagerEventListener = new com.navdy.hud.app.maps.here.HereNavigationEventListener(sLogger, "[CB-NAVM]", this);
        this.navigationManager.addNavigationManagerEventListener(new java.lang.ref.WeakReference(this.navigationManagerEventListener));
        this.newManeuverEventListener = new com.navdy.hud.app.maps.here.HereNewManeuverListener(sLogger, "[CB-NEWM]", false, this.hereNavController, this, this.bus);
        this.navigationManager.addNewInstructionEventListener(new java.lang.ref.WeakReference(this.newManeuverEventListener));
        this.gpsSignalListener = new com.navdy.hud.app.maps.here.HereGpsSignalListener(sLogger, "[CB-GPS]", this.bus, mapsEventHandler, this);
        this.navigationManager.addGpsSignalListener(new java.lang.ref.WeakReference(this.gpsSignalListener));
        this.speedWarningManager = new com.navdy.hud.app.maps.here.HereSpeedWarningManager("[CB-SPD]", this.bus, mapsEventHandler, this);
        this.safetySpotListener = new com.navdy.hud.app.maps.here.HereSafetySpotListener(this.bus, this.mapController);
        this.navigationManager.addSafetySpotListener(new java.lang.ref.WeakReference(this.safetySpotListener));
        this.reRouteListener = new com.navdy.hud.app.maps.here.HereRerouteListener(sLogger, "[CB-REROUTE]", this, this.bus);
        this.navigationManager.addRerouteListener(new java.lang.ref.WeakReference(this.reRouteListener));
        if (!com.navdy.hud.app.util.DeviceUtil.isUserBuild() && com.navdy.hud.app.maps.MapSettings.isLaneGuidanceEnabled()) {
            this.laneInfoListener = new com.navdy.hud.app.maps.here.HereLaneInfoListener(this.bus, this.hereNavController);
        }
        this.hereRealisticViewListener = new com.navdy.hud.app.maps.here.HereRealisticViewListener(this.bus, this.hereNavController);
        this.positionUpdateListener = new com.navdy.hud.app.maps.here.HerePositionUpdateListener("[CB-POS-UPDATE]", this.hereNavController, this, this.bus);
        this.navigationManager.addPositionListener(new java.lang.ref.WeakReference(this.positionUpdateListener));
        this.trafficRerouteListener = new com.navdy.hud.app.maps.here.HereTrafficRerouteListener("[CB-TRAFFIC-REROUTE]", true, this.hereNavController, this, this.bus);
        this.navdyTrafficRerouteManager = new com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager(this, this.trafficRerouteListener, this.bus);
        this.trafficRerouteListener.setNavdyTrafficRerouteManager(this.navdyTrafficRerouteManager);
        this.trafficUpdater = new com.navdy.hud.app.maps.here.HereTrafficUpdater2(this.bus);
        this.trafficEtaTracker = new com.navdy.hud.app.maps.here.HereTrafficETATracker(this.hereNavController, this.bus);
        this.navigationManager.addNavigationManagerEventListener(new java.lang.ref.WeakReference(this.trafficEtaTracker));
        this.setMapUpdateMode();
        this.setupNavigationTTS();
        this.setNavigationManagerUnit();
        this.driverSessionPreferences = com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getDriverProfileManager().getSessionPreferences();
        if (this.bandwidthController.isLimitBandwidthModeOn()) {
            sLogger.v("limitbandwidth: initial on");
            this.setBandwidthPreferences();
        } else {
            this.setTrafficRerouteMode(mapsEventHandler.getNavigationPreferences().rerouteForTraffic);
        }
        this.setGeneratePhoneticTTS(Boolean.TRUE.equals(mapsEventHandler.getNavigationPreferences().phoneticTurnByTurn));
        this.bus.register(this);
        sLogger.v("start speed warning manager");
        this.speedWarningManager.start();
        if (!this.setNavigationMode(com.navdy.hud.app.maps.NavigationMode.MAP, (com.navdy.hud.app.maps.here.HereNavigationInfo)null, (StringBuilder)null)) {
            sLogger.e("navigation mode cannot be switched to map");
        }
    }
    
    private void postArrivedManeuver() {
        com.navdy.hud.app.maps.MapEvents$ManeuverDisplay a = this.currentNavigationInfo.arrivedManeuverDisplay;
        if (a == null) {
            a = com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.getArrivedManeuverDisplay();
            this.currentNavigationInfo.arrivedManeuverDisplay = a;
            sLogger.v("arrival maneuver created");
        }
        this.currentNavigationInfo.lastManeuverPostTime = System.currentTimeMillis();
        this.bus.post(a);
    }
    
    private void removeDestinationMarker() {
        if (this.currentNavigationInfo.mapDestinationMarker != null) {
            sLogger.v("removed destination marker");
            this.mapController.removeMapObject((com.here.android.mpa.mapping.MapObject)this.currentNavigationInfo.mapDestinationMarker);
            hereMapsManager.getLocationFixManager().removeMarkers(this.mapController);
        }
    }
    
    private void setGeneratePhoneticTTS(boolean b) {
        this.generatePhoneticTTS = Boolean.valueOf(b);
        com.here.android.mpa.guidance.NavigationManager$TtsOutputFormat a = b ? com.here.android.mpa.guidance.NavigationManager$TtsOutputFormat.NUANCE : com.here.android.mpa.guidance.NavigationManager$TtsOutputFormat.RAW;
        this.navigationManager.setTtsOutputFormat(a);
    }
    
    private void setNavigationManagerUnit() {
        com.navdy.hud.app.manager.SpeedManager$SpeedUnit a = speedManager.getSpeedUnit();
        if (com.navdy.hud.app.maps.here.HereNavigationManager$12.$SwitchMap$com$navdy$hud$app$manager$SpeedManager$SpeedUnit[a.ordinal()] != 0) {
            this.hereNavController.setDistanceUnit(com.here.android.mpa.guidance.NavigationManager$UnitSystem.METRIC);
        } else {
            this.hereNavController.setDistanceUnit(com.here.android.mpa.guidance.NavigationManager$UnitSystem.IMPERIAL_US);
        }
    }
    
    private void setTrafficOverlay() {
        sLogger.v(new StringBuilder().append("enableTraffic on map:").append(true).toString());
        hereMapsManager.setTrafficOverlay(this.currentNavigationMode);
    }
    
    private void setTrafficRerouteMode(com.navdy.service.library.events.preferences.NavigationPreferences$RerouteForTraffic a) {
        this.navigationManager.setTrafficAvoidanceMode(com.here.android.mpa.guidance.NavigationManager$TrafficAvoidanceMode.MANUAL);
    }
    
    private void setupNavigationTTS() {
        java.util.Locale a = com.navdy.hud.app.profile.HudLocale.getCurrentLocale(com.navdy.hud.app.HudApplication.getAppContext());
        String s = a.toLanguageTag();
        String s0 = com.navdy.hud.app.profile.HudLocale.getBaseLanguage(s);
        java.util.List a0 = com.here.android.mpa.guidance.VoiceCatalog.getInstance().getLocalVoiceSkins();
        com.here.android.mpa.guidance.VoiceSkin a1 = null;
        if (a0 != null) {
            Object a2 = a0;
            a1 = null;
            while(s != null && a1 == null) {
                a1 = this.findSkinSupporting((java.util.List)a2, s);
                s = (String)LANGUAGE_HIERARCHY.get(s);
            }
            if (a1 == null) {
                a1 = this.findSkinSupporting((java.util.List)a2, s0);
            }
            if (a1 == null) {
                a1 = this.findSkinSupporting((java.util.List)a2, "en-US");
            }
        }
        if (a1 != null) {
            sLogger.d(new StringBuilder().append("Found voice skin lang[").append(a1.getLanguage()).append(" code[").append(a1.getLanguageCode()).append("] for locale:").append(a).toString());
            this.navigationManager.setVoiceSkin(a1);
            hereMapsManager.setVoiceSkinsLoaded();
            this.audioPlayerDelegate = (com.here.android.mpa.guidance.AudioPlayerDelegate)new com.navdy.hud.app.maps.here.HereNavigationManager$2(this);
            this.navigationManager.getAudioPlayer().setDelegate(this.audioPlayerDelegate);
        } else {
            sLogger.e(new StringBuilder().append("No voice skin found for default locale:").append(com.navdy.hud.app.maps.here.HereMapUtil.TBT_ISO3_LANG_CODE).toString());
        }
    }
    
    private void startNavigation(com.navdy.service.library.events.navigation.NavigationSessionRequest a, com.navdy.hud.app.maps.here.HereRouteCache$RouteInfo a0) {
        com.navdy.hud.app.maps.here.HereNavigationInfo a1 = new com.navdy.hud.app.maps.here.HereNavigationInfo();
        a1.navigationRouteRequest = a0.routeRequest;
        a1.startLocation = a0.routeStartPoint;
        a1.routeOptions = hereMapsManager.getRouteOptions();
        a1.waypoints = (java.util.List)new java.util.ArrayList();
        Object a2 = a0.routeRequest.waypoints.iterator();
        while(((java.util.Iterator)a2).hasNext()) {
            com.navdy.service.library.events.location.Coordinate a3 = (com.navdy.service.library.events.location.Coordinate)((java.util.Iterator)a2).next();
            a1.waypoints.add(new com.here.android.mpa.common.GeoCoordinate(a3.latitude.doubleValue(), a3.longitude.doubleValue()));
        }
        a1.destination = new com.here.android.mpa.common.GeoCoordinate(a0.routeRequest.destination.latitude.doubleValue(), a0.routeRequest.destination.longitude.doubleValue());
        a1.route = a0.route;
        a1.simulationSpeed = a.simulationSpeed.intValue();
        a1.routeId = a.routeId;
        a1.deviceId = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getDeviceId();
        com.navdy.service.library.device.NavdyDeviceId a4 = com.navdy.hud.app.maps.here.HereMapUtil.getLastKnownDeviceId();
        if (a1.deviceId == null) {
            a1.deviceId = a4;
            sLogger.v(new StringBuilder().append("lastknown device id = ").append(a1.deviceId).toString());
        }
        if (a1.deviceId == null) {
            com.navdy.service.library.events.DeviceInfo a5 = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getConnectionHandler().getLastConnectedDeviceInfo();
            if (a5 != null) {
                a1.deviceId = new com.navdy.service.library.device.NavdyDeviceId(a5.deviceId);
                sLogger.v(new StringBuilder().append("device id is null , adding last device:").append(a5.deviceId).toString());
            }
        }
        if (a0.routeRequest.destination_identifier != null) {
            a1.destinationIdentifier = a0.routeRequest.destination_identifier;
        }
        com.navdy.service.library.events.navigation.NavigationRouteRequest a6 = a1.navigationRouteRequest;
        String s = null;
        if (a6 != null) {
            s = a6.streetAddress;
            a1.destinationLabel = a6.label;
        }
        a1.streetAddress = com.navdy.hud.app.maps.here.HereMapUtil.parseStreetAddress(s);
        StringBuilder a7 = new StringBuilder();
        if (this.setNavigationMode(com.navdy.hud.app.maps.NavigationMode.MAP_ON_ROUTE, a1, a7)) {
            this.returnResponse(com.navdy.service.library.events.RequestStatus.REQUEST_SUCCESS, (String)null, this.currentSessionState, a.routeId);
        } else {
            this.returnResponse(com.navdy.service.library.events.RequestStatus.REQUEST_SERVICE_ERROR, a7.toString(), this.currentSessionState, a.routeId);
        }
    }
    
    private void tearDownCurrentRoute() {
        this.removeCurrentRoute();
        this.removeDestinationMarker();
        this.currentNavigationInfo.mapDestinationMarker = null;
        this.trafficUpdater.setRoute((com.here.android.mpa.routing.Route)null);
        this.trafficRerouteListener.dismissReroute();
        this.bus.post(new com.navdy.hud.app.maps.MapEvents$TrafficRerouteDismissEvent());
        this.bus.post(new com.navdy.hud.app.maps.MapEvents$LiveTrafficDismissEvent());
    }
    
    private void updateNavigationInfoInternal(com.here.android.mpa.routing.Maneuver a, com.here.android.mpa.routing.Maneuver a0, com.here.android.mpa.routing.Maneuver a1, boolean b) {
        label2: synchronized(this) {
            if (this.currentNavigationInfo.hasArrived) {
                if (this.currentNavigationInfo.ignoreArrived) {
                    sLogger.v("ignore arrived");
                } else {
                    label5: {
                        try {
                            com.here.android.mpa.common.GeoPosition a2 = hereMapsManager.getLastGeoPosition();
                            if (a2 == null) {
                                break label5;
                            }
                            int i = (int)a2.getCoordinate().distanceTo(this.currentNavigationInfo.destination);
                            if (i < 804) {
                                break label5;
                            }
                            sLogger.v(new StringBuilder().append("we have exceeded geo threshold:").append(i).append(" , stopping nav").toString());
                            this.stopNavigation();
                        } catch(Throwable a3) {
                            sLogger.e(a3);
                            break label5;
                        }
                        break label2;
                    }
                    this.postArrivedManeuver();
                }
            } else {
                com.navdy.service.library.events.navigation.NavigationSessionState a4 = this.currentSessionState;
                com.navdy.service.library.events.navigation.NavigationSessionState a5 = com.navdy.service.library.events.navigation.NavigationSessionState.NAV_SESSION_STARTED;
                label3: {
                    boolean b0 = false;
                    label4: {
                        if (a4 == a5) {
                            break label4;
                        }
                        if (this.currentSessionState != com.navdy.service.library.events.navigation.NavigationSessionState.NAV_SESSION_PAUSED) {
                            break label3;
                        }
                    }
                    if (a != null) {
                        if (b) {
                            sLogger.v("setNewRoute: cleared");
                            this.prevManeuver = null;
                        } else {
                            this.prevManeuver = this.currentManeuver;
                        }
                        this.currentManeuver = a;
                        this.maneuverAfterCurrent = a0;
                        this.currentNavigationInfo.maneuverAfterCurrent = a0;
                        this.currentNavigationInfo.maneuverAfterCurrentIconid = -1;
                        this.currentNavigationInfo.maneuverState = null;
                        b0 = true;
                    } else {
                        com.here.android.mpa.routing.Maneuver a6 = this.currentManeuver;
                        com.here.android.mpa.routing.Maneuver dummy = this.maneuverAfterCurrent;
                        com.here.android.mpa.routing.Maneuver dummy0 = this.prevManeuver;
                        if (this.maneuverAfterCurrent == null) {
                            a = a6;
                            b0 = true;
                        } else if (this.currentNavigationInfo.maneuverAfterCurrent != this.maneuverAfterCurrent) {
                            a = a6;
                            b0 = true;
                        } else if (this.currentNavigationInfo.maneuverState == null) {
                            a = a6;
                            b0 = true;
                        } else if (this.currentNavigationInfo.maneuverState == com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder$ManeuverState.STAY) {
                            a = a6;
                            b0 = true;
                        } else {
                            a = a6;
                            b0 = false;
                        }
                    }
                    if (a == null) {
                        sLogger.w("there is no next maneuver");
                        break label2;
                    } else {
                        boolean b1 = false;
                        boolean b2 = false;
                        boolean b3 = false;
                        com.here.android.mpa.routing.Maneuver$Action a7 = a.getAction();
                        com.here.android.mpa.routing.Maneuver$Action a8 = com.here.android.mpa.routing.Maneuver$Action.END;
                        label0: {
                            label1: {
                                if (a7 == a8) {
                                    break label1;
                                }
                                if (a.getIcon() != com.here.android.mpa.routing.Maneuver$Icon.END) {
                                    b1 = false;
                                    break label0;
                                }
                            }
                            b1 = true;
                        }
                        if (b1) {
                            if (this.currentNavigationInfo.destinationDirection == null) {
                                b2 = true;
                                b3 = false;
                            } else {
                                b2 = false;
                                b3 = true;
                            }
                        } else {
                            b2 = false;
                            b3 = false;
                        }
                        long j = this.hereNavController.getNextManeuverDistance();
                        com.navdy.hud.app.maps.MapEvents$ManeuverDisplay a9 = com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.getManeuverDisplay(this.currentManeuver, b1, j, (String)null, this.prevManeuver, b2 ? this.currentNavigationInfo.navigationRouteRequest : null, this.maneuverAfterCurrent, b0, true, this.currentNavigationInfo.destinationDirection);
                        if (!b1) {
                            if (this.debugManeuverState != null) {
                                a9.maneuverState = this.debugManeuverState;
                            }
                            if (this.debugManeuverInstruction != null) {
                                a9.pendingRoad = this.debugManeuverInstruction;
                            }
                            if (this.debugManeuverIcon != 0) {
                                a9.turnIconId = this.debugManeuverIcon;
                            }
                            if (this.debugManeuverDistance != null) {
                                a9.distanceToPendingRoadText = this.debugManeuverDistance;
                            }
                            if (this.debugNextManeuverIcon != 0) {
                                a9.nextTurnIconId = this.debugNextManeuverIcon;
                            }
                        }
                        if (b1 && !this.currentNavigationInfo.lastManeuver) {
                            sLogger.v(new StringBuilder().append("last maneuver marked distance:").append(j).toString());
                            this.setLastManeuver();
                        }
                        if (b1) {
                            if (b3) {
                                if (this.currentNavigationInfo.destinationDirection != com.navdy.hud.app.maps.MapEvents$DestinationDirection.UNKNOWN) {
                                    a9.pendingRoad = this.currentNavigationInfo.destinationDirectionStr;
                                    a9.destinationIconId = this.currentNavigationInfo.destinationIconId;
                                }
                            } else {
                                this.currentNavigationInfo.destinationDirection = a9.direction;
                                this.currentNavigationInfo.destinationDirectionStr = a9.pendingRoad;
                                this.currentNavigationInfo.destinationIconId = a9.destinationIconId;
                                if (this.currentNavigationInfo.destinationDirection != com.navdy.hud.app.maps.MapEvents$DestinationDirection.UNKNOWN) {
                                    sLogger.v(new StringBuilder().append("we have destination marker info: ").append(this.currentNavigationInfo.destinationDirection).toString());
                                    this.removeDestinationMarker();
                                    this.currentNavigationInfo.mapDestinationMarker = null;
                                    if (this.currentNavigationInfo.route != null) {
                                        this.addDestinationMarker(this.currentNavigationInfo.route.getDestination(), this.currentNavigationInfo.destinationIconId);
                                    }
                                }
                            }
                        }
                        if (this.currentNavigationInfo.maneuverAfterCurrentIconid != -1) {
                            a9.nextTurnIconId = this.currentNavigationInfo.maneuverAfterCurrentIconid;
                        } else {
                            this.currentNavigationInfo.maneuverAfterCurrentIconid = a9.nextTurnIconId;
                            this.currentNavigationInfo.maneuverState = a9.maneuverState;
                        }
                        java.util.Date a10 = this.hereNavController.getEta(true, com.here.android.mpa.routing.Route$TrafficPenaltyMode.OPTIMAL);
                        if (!com.navdy.hud.app.maps.here.HereMapUtil.isValidEtaDate(a10)) {
                            a10 = this.hereNavController.getTtaDate(true, com.here.android.mpa.routing.Route$TrafficPenaltyMode.OPTIMAL);
                            if (com.navdy.hud.app.maps.here.HereMapUtil.isValidEtaDate(a10)) {
                                sLogger.v(new StringBuilder().append("intermediate maneuver eta-tta date = ").append(a10).toString());
                            } else {
                                sLogger.v(new StringBuilder().append("intermediate maneuver eta-tta date invalid = ").append(a10).toString());
                                a10 = null;
                            }
                        }
                        if (a10 != null) {
                            a9.eta = this.timeHelper.formatTime(a10, this.stringBuilder);
                            a9.etaAmPm = this.stringBuilder.toString();
                            a9.etaDate = a10;
                        }
                        com.here.android.mpa.common.RoadElement a11 = com.navdy.hud.app.maps.here.HereMapUtil.getCurrentRoadElement();
                        boolean b4 = a11 != null && com.navdy.hud.app.maps.here.HereMapUtil.isCurrentRoadHighway(a11);
                        if (com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.getManeuverState(this.hereNavController.getNextManeuverDistance(), b4) == com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder$ManeuverState.SOON) {
                            this.bus.post(new com.navdy.hud.app.maps.MapEvents$ManeuverSoonEvent(a));
                        }
                        if (sLogger.isLoggable(2)) {
                            sLogger.d(new StringBuilder().append("ManeuverDisplay-nav:").append(this.getHereNavigationState()).append(" :").append(a9.toString()).toString());
                            sLogger.v(new StringBuilder().append("[ManeuverDisplay-nav-short] ").append(a9.pendingTurn).append(" ").append(a9.pendingRoad).toString());
                        }
                        this.currentNavigationInfo.lastManeuverPostTime = System.currentTimeMillis();
                        this.bus.post(a9);
                        break label2;
                    }
                }
                if (this.currentSessionState != com.navdy.service.library.events.navigation.NavigationSessionState.NAV_SESSION_STOPPED) {
                    this.prevManeuver = null;
                    this.currentManeuver = null;
                    this.maneuverAfterCurrent = null;
                    sLogger.e(new StringBuilder().append("Invalid state:").append(this.currentSessionState).append(" maneuver=").append(a).toString());
                } else {
                    this.prevManeuver = null;
                    this.currentManeuver = null;
                    this.maneuverAfterCurrent = null;
                    com.navdy.hud.app.maps.MapEvents$ManeuverDisplay a12 = new com.navdy.hud.app.maps.MapEvents$ManeuverDisplay();
                    a12.currentRoad = com.navdy.hud.app.maps.here.HereMapUtil.getCurrentRoadName();
                    a12.currentSpeedLimit = com.navdy.hud.app.maps.here.HereMapUtil.getCurrentSpeedLimit();
                    if (sLogger.isLoggable(2)) {
                        sLogger.d(new StringBuilder().append("ManeuverDisplay:").append(this.getHereNavigationState()).append(" :").append(a12.toString()).toString());
                    }
                    this.bus.post(a12);
                }
            }
        }
        /*monexit(this)*/;
    }
    
    public void addCurrentRoute(com.here.android.mpa.mapping.MapRoute a) {
        synchronized(this) {
            label0: try {
                if (this.currentNavigationInfo.mapRoute != null) {
                    this.removeCurrentRoute();
                }
                if (a == null) {
                    break label1;
                }
                this.currentNavigationInfo.mapRoute = a;
                this.mapController.addMapObject((com.here.android.mpa.mapping.MapObject)a);
                sLogger.v(new StringBuilder().append("addCurrentRoute:").append(a).toString());
                break label1;
            } catch(Throwable a1) {
                try {
                    sLogger.e(a1);
                } catch(Throwable a2) {
                    a0 = a2;
                    break label0;
                }
                break label1;
            }
            /*monexit(this)*/;
//            throw a0;
        }
        /*monexit(this)*/;
    }
    
    public com.here.android.mpa.guidance.NavigationManager$Error addNewArrivalRoute(java.util.ArrayList a) {
        com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
        return this.addNewRoute(a, com.navdy.service.library.events.navigation.NavigationSessionRouteChange$RerouteReason.NAV_SESSION_ARRIVAL_REROUTE, (com.navdy.service.library.events.navigation.NavigationRouteRequest)null);
    }
    
    public com.here.android.mpa.guidance.NavigationManager$Error addNewRoute(java.util.ArrayList a, com.navdy.service.library.events.navigation.NavigationSessionRouteChange$RerouteReason a0, com.navdy.service.library.events.navigation.NavigationRouteRequest a1) {
        com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
        return this.addNewRoute(a, a0, a1, -1L);
    }
    
    public com.here.android.mpa.guidance.NavigationManager$Error addNewRoute(java.util.ArrayList a, com.navdy.service.library.events.navigation.NavigationSessionRouteChange$RerouteReason a0, com.navdy.service.library.events.navigation.NavigationRouteRequest a1, long j) {
        com.here.android.mpa.guidance.NavigationManager$Error a2 = null;
        com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
        label2: {
            label0: {
                label1: {
                    if (a == null) {
                        break label1;
                    }
                    if (a.size() > 0) {
                        break label0;
                    }
                }
                sLogger.e("addNewRoute calc no result, end trip:");
                this.stopNavigation();
                a2 = com.here.android.mpa.guidance.NavigationManager$Error.NOT_FOUND;
                break label2;
            }
            sLogger.v(new StringBuilder().append("addNewRoute calculated=").append(a0).toString());
            String s = ((com.navdy.service.library.events.navigation.NavigationRouteResult)a.get(0)).routeId;
            com.navdy.hud.app.maps.here.HereRouteCache$RouteInfo a3 = com.navdy.hud.app.maps.here.HereRouteCache.getInstance().getRoute(s);
            if (a3 == null) {
                sLogger.e(new StringBuilder().append("addNewRoute calc could not found route:").append(s).toString());
                this.stopNavigation();
                a2 = com.here.android.mpa.guidance.NavigationManager$Error.NOT_FOUND;
            } else {
                this.updateMapRoute(a3.route, a0, a1, (String)null, (String)null, false, a3.traffic);
                sLogger.i("setNavigationMode: set transition mode");
                a2 = this.hereNavController.startNavigation(a3.route, (int)j);
                if (a2 != com.here.android.mpa.guidance.NavigationManager$Error.NONE) {
                    sLogger.e(new StringBuilder().append("addNewRoute navigation started failed:").append(a2).toString());
                    this.stopNavigation();
                } else {
                    sLogger.v("addNewRoute navigation started");
                }
            }
        }
        return a2;
    }
    
    public void arrived() {
        com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.maps.here.HereNavigationManager$9(this), 20);
    }
    
    public void clearNavManeuver() {
        this.newManeuverEventListener.clearNavManeuver();
    }
    
    public String getCurrentDestinationIdentifier() {
        return this.currentNavigationInfo.destinationIdentifier;
    }
    
    public com.here.android.mpa.mapping.MapRoute getCurrentMapRoute() {
        return this.currentNavigationInfo.mapRoute;
    }
    
    public com.navdy.service.library.device.NavdyDeviceId getCurrentNavigationDeviceId() {
        return this.currentNavigationInfo.deviceId;
    }
    
    public com.navdy.hud.app.maps.NavigationMode getCurrentNavigationMode() {
        return this.currentNavigationMode;
    }
    
    public com.navdy.service.library.events.navigation.NavigationRouteRequest getCurrentNavigationRouteRequest() {
        return this.currentNavigationInfo.navigationRouteRequest;
    }
    
    public com.navdy.service.library.events.navigation.NavigationSessionState getCurrentNavigationState() {
        return this.currentSessionState;
    }
    
    public com.navdy.service.library.events.navigation.NavigationSessionRouteChange$RerouteReason getCurrentRerouteReason() {
        return this.currentNavigationInfo.currentRerouteReason;
    }
    
    public com.here.android.mpa.routing.Route getCurrentRoute() {
        return this.currentNavigationInfo.route;
    }
    
    public String getCurrentRouteId() {
        return this.currentNavigationInfo.routeId;
    }
    
    public int getCurrentTrafficRerouteCount() {
        return this.currentNavigationInfo.getTrafficReRouteCount();
    }
    
    public String getCurrentVia() {
        String s = this.getCurrentRouteId();
        String s0 = null;
        if (s != null) {
            com.navdy.hud.app.maps.here.HereRouteCache$RouteInfo a = com.navdy.hud.app.maps.here.HereRouteCache.getInstance().getRoute(s);
            s0 = null;
            if (a != null) {
                com.navdy.service.library.events.navigation.NavigationRouteResult a0 = a.routeResult;
                s0 = null;
                if (a0 != null) {
                    s0 = a.routeResult.via;
                }
            }
        }
        return s0;
    }
    
    public String getDestinationLabel() {
        return this.currentNavigationInfo.destinationLabel;
    }
    
    public com.here.android.mpa.mapping.MapMarker getDestinationMarker() {
        return this.currentNavigationInfo.mapDestinationMarker;
    }
    
    public String getDestinationStreetAddress() {
        return this.currentNavigationInfo.streetAddress;
    }
    
    public long getFirstManeuverShowntime() {
        return this.currentNavigationInfo.firstManeuverShownTime;
    }
    
    public String getFullStreetAddress() {
        com.navdy.service.library.events.navigation.NavigationRouteRequest a = this.currentNavigationInfo.navigationRouteRequest;
        String s = (a == null) ? null : a.streetAddress;
        return s;
    }
    
    public Boolean getGeneratePhoneticTTS() {
        return this.generatePhoneticTTS;
    }
    
    public com.here.android.mpa.guidance.NavigationManager$NavigationMode getHereNavigationState() {
        com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
        return this.navigationManager.getNavigationMode();
    }
    
    public long getLastTrafficRerouteTime() {
        return this.currentNavigationInfo.getLastTrafficRerouteTime();
    }
    
    public com.navdy.hud.app.maps.here.HereNavController getNavController() {
        return this.hereNavController;
    }
    
    public com.here.android.mpa.routing.Maneuver getNavManeuver() {
        return this.newManeuverEventListener.getNavManeuver();
    }
    
    public com.navdy.hud.app.maps.NavSessionPreferences getNavigationSessionPreference() {
        return this.driverSessionPreferences.getNavigationSessionPreference();
    }
    
    public com.navdy.hud.app.maps.here.HereNavController$State getNavigationState() {
        return this.hereNavController.getState();
    }
    
    public com.navdy.hud.app.ui.component.homescreen.NavigationView getNavigationView() {
        return this.navigationView;
    }
    
    public com.here.android.mpa.routing.Maneuver getNextManeuver() {
        return (this.isNavigationModeOn()) ? this.currentManeuver : null;
    }
    
    public com.here.android.mpa.routing.Maneuver getPrevManeuver() {
        return (this.isNavigationModeOn()) ? this.prevManeuver : null;
    }
    
    public int getRerouteInterval() {
        return (this.bandwidthController.isLimitBandwidthModeOn()) ? com.navdy.hud.app.maps.here.HereTrafficRerouteListener.ROUTE_CHECK_INTERVAL_LIMIT_BANDWIDTH : com.navdy.hud.app.maps.here.HereTrafficRerouteListener.ROUTE_CHECK_INTERVAL;
    }
    
    public com.here.android.mpa.routing.RouteOptions getRouteOptions() {
        return this.currentNavigationInfo.routeOptions;
    }
    
    public com.navdy.hud.app.maps.here.HereSafetySpotListener getSafetySpotListener() {
        return this.safetySpotListener;
    }
    
    public com.here.android.mpa.common.GeoCoordinate getStartLocation() {
        return this.currentNavigationInfo.startLocation;
    }
    
    public void handleConnectionState(boolean b) {
        label2: if (b) {
            sLogger.i("client connected");
            if (this.isNavigationModeOn()) {
                com.navdy.service.library.device.NavdyDeviceId a = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getDeviceId();
                label0: {
                    label1: {
                        if (a == null) {
                            break label1;
                        }
                        if (!a.equals(this.currentNavigationInfo.deviceId)) {
                            break label0;
                        }
                    }
                    sLogger.i("navigation still on, device id has not changed");
                    break label2;
                }
                com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.maps.here.HereNavigationManager$6(this, a), 20);
            }
        } else {
            sLogger.i("client not connected");
        }
    }
    
    public void handleNavigationSessionRequest(com.navdy.service.library.events.navigation.NavigationSessionRequest a) {
        com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.maps.here.HereNavigationManager$5(this, a), 20);
    }
    
    public boolean hasArrived() {
        return this.isNavigationModeOn() && this.currentNavigationInfo.hasArrived;
    }
    
    public boolean hasTrafficRerouteOnce() {
        return this.currentNavigationInfo.trafficRerouteOnce;
    }
    
    public void incrCurrentTrafficRerouteCount() {
        this.currentNavigationInfo.incrTrafficRerouteCount();
    }
    
    public boolean isLastManeuver() {
        return this.currentNavigationInfo.lastManeuver;
    }
    
    public boolean isNavigationModeOn() {
        return this.hereNavController.getState() == com.navdy.hud.app.maps.here.HereNavController$State.NAVIGATING;
    }
    
    public boolean isOnGasRoute() {
        boolean b = false;
        if (this.isNavigationModeOn()) {
            com.navdy.service.library.events.navigation.NavigationRouteRequest a = this.currentNavigationInfo.navigationRouteRequest;
            b = a != null && a.routeAttributes != null && a.routeAttributes.contains(com.navdy.service.library.events.navigation.NavigationRouteRequest$RouteAttribute.ROUTE_ATTRIBUTE_GAS);
        } else {
            b = false;
        }
        return b;
    }
    
    public boolean isRerouting() {
        return this.reRouteListener.isRecalculating();
    }
    
    public boolean isShownFirstManeuver() {
        return this.currentNavigationInfo.firstManeuverShown;
    }
    
    public boolean isSwitchingToNewRoute() {
        return this.switchingToNewRoute;
    }
    
    public boolean isTrafficConsidered(String s) {
        boolean b = false;
        label1: {
            label0: try {
                com.navdy.hud.app.maps.here.HereRouteCache$RouteInfo a = com.navdy.hud.app.maps.here.HereRouteCache.getInstance().getRoute(s);
                if (a == null) {
                    break label0;
                }
                b = a.traffic;
                break label1;
            } catch(Throwable a0) {
                sLogger.e(a0);
            }
            b = false;
        }
        return b;
    }
    
    public boolean isTrafficUpdaterRunning() {
        return this.trafficUpdater.isRunning();
    }
    
    public void onDriverProfileChanged(com.navdy.hud.app.event.DriverProfileChanged a) {
        sLogger.v("driver profile changed");
    }
    
    public void onNavigationPreferences(com.navdy.service.library.events.preferences.NavigationPreferences a) {
        this.setGeneratePhoneticTTS(Boolean.TRUE.equals(a.phoneticTurnByTurn));
    }
    
    public void onNotificationPreference(com.navdy.service.library.events.preferences.NotificationPreferences a) {
        if (a.enabled.booleanValue()) {
            if (com.navdy.hud.app.framework.glance.GlanceHelper.isTrafficNotificationEnabled()) {
                sLogger.v("glances, enabled, turn traffic notif on if not on");
                this.startTrafficRerouteListener();
            } else {
                sLogger.v("traffic glances disabled, turn traffic notif off");
                this.resetTrafficRerouteListener();
                this.stopTrafficRerouteListener();
            }
        } else {
            sLogger.v("glances, disabled, turn traffic notif off");
            this.resetTrafficRerouteListener();
            this.stopTrafficRerouteListener();
        }
    }
    
    public void onSpeedUnitChanged(com.navdy.hud.app.manager.SpeedManager$SpeedUnitChanged a) {
        sLogger.i(new StringBuilder().append("speed unit setting has changed to ").append(speedManager.getSpeedUnit().name()).append(", refreshing").toString());
        this.refreshNavigationInfo();
        com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.maps.here.HereNavigationManager$8(this), 3);
    }
    
    public void onTimeSettingsChange(com.navdy.hud.app.common.TimeHelper$UpdateClock a) {
        sLogger.v("time setting changed");
        if (this.isNavigationModeOn()) {
            this.refreshNavigationInfo();
        }
    }
    
    public void onTrafficRerouteConfirm(com.navdy.hud.app.maps.MapEvents$TrafficRerouteAction a) {
        if (a != com.navdy.hud.app.maps.MapEvents$TrafficRerouteAction.REROUTE) {
            this.trafficRerouteListener.dismissReroute();
        } else {
            this.trafficRerouteListener.confirmReroute();
        }
    }
    
    public void postManeuverDisplay() {
        this.refreshNavigationInfo();
    }
    
    public void postNavigationSessionStatusEvent(com.navdy.service.library.events.navigation.NavigationSessionState a, boolean b) {
        com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.maps.here.HereNavigationManager$4(this, b, a), 2);
    }
    
    public void postNavigationSessionStatusEvent(boolean b) {
        this.postNavigationSessionStatusEvent(this.currentSessionState, b);
    }
    
    void refreshNavigationInfo() {
        this.updateNavigationInfo((com.here.android.mpa.routing.Maneuver)null, (com.here.android.mpa.routing.Maneuver)null, (com.here.android.mpa.routing.Maneuver)null, false);
    }
    
    public boolean removeCurrentRoute() {
        boolean b = false;
        label1: synchronized(this) {
            label0: {
                Throwable a = null;
                try {
                    com.here.android.mpa.mapping.MapRoute a0 = this.currentNavigationInfo.mapRoute;
                    if (a0 == null) {
                        break label0;
                    }
                    sLogger.v(new StringBuilder().append("removed map route:").append(a0).toString());
                    this.currentNavigationInfo.mapRoute = null;
                    this.mapController.removeMapObject((com.here.android.mpa.mapping.MapObject)a0);
                    b = true;
                    break label1;
                } catch(Throwable a1) {
                    try {
                        sLogger.e(a1);
                        break label0;
                    } catch(Throwable a2) {
                        a = a2;
                    }
                }
                /*monexit(this)*/;
                throw a;
            }
            b = false;
        }
        /*monexit(this)*/;
        return b;
    }
    
    public void resetTrafficRerouteListener() {
        synchronized(trafficRerouteLock) {
            this.navdyTrafficRerouteManager.reset();
            /*monexit(a)*/;
        }
    }
    
    protected void returnResponse(com.navdy.service.library.events.RequestStatus a, String s, com.navdy.service.library.events.navigation.NavigationSessionState a0, String s0) {
        if (a == com.navdy.service.library.events.RequestStatus.REQUEST_SUCCESS) {
            sLogger.i("Success");
        } else {
            sLogger.e(new StringBuilder().append(a).append(": ").append(s).toString());
        }
        mapsEventHandler.sendEventToClient((com.squareup.wire.Message)new com.navdy.service.library.events.navigation.NavigationSessionResponse(a, s, a0, s0));
    }
    
    public void sendStartManeuver(com.here.android.mpa.routing.Route a) {
        com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
        if (a != null) {
            java.util.List a0 = a.getManeuvers();
            if (a0.size() < 2) {
                sLogger.i("start maneuver not sent");
            } else {
                com.navdy.hud.app.maps.MapEvents$ManeuverDisplay a1 = com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.getStartManeuverDisplay((com.here.android.mpa.routing.Maneuver)a0.get(0), (com.here.android.mpa.routing.Maneuver)a0.get(1));
                java.util.Date a2 = this.hereNavController.getEta(true, com.here.android.mpa.routing.Route$TrafficPenaltyMode.OPTIMAL);
                if (com.navdy.hud.app.maps.here.HereMapUtil.isValidEtaDate(a2)) {
                    sLogger.v(new StringBuilder().append("start maneuver eta date = ").append(a2).toString());
                } else {
                    a2 = this.hereNavController.getTtaDate(true, com.here.android.mpa.routing.Route$TrafficPenaltyMode.OPTIMAL);
                    if (com.navdy.hud.app.maps.here.HereMapUtil.isValidEtaDate(a2)) {
                        sLogger.v(new StringBuilder().append("start maneuver eta-tta date = ").append(a2).toString());
                    } else {
                        a2 = com.navdy.hud.app.maps.here.HereMapUtil.getRouteTtaDate(a);
                        sLogger.v(new StringBuilder().append("start maneuver tta date = ").append(a2).toString());
                    }
                }
                if (a2 != null) {
                    a1.etaDate = a2;
                    a1.eta = this.timeHelper.formatTime(a2, this.stringBuilder);
                    a1.etaAmPm = this.stringBuilder.toString();
                }
                if (sLogger.isLoggable(2)) {
                    sLogger.d(new StringBuilder().append("ManeuverDisplay:START:").append(this.getHereNavigationState()).append(" :").append(a1.toString()).toString());
                }
                this.currentNavigationInfo.lastManeuverPostTime = System.currentTimeMillis();
                this.bus.post(a1);
            }
        }
    }
    
    void setBandwidthPreferences() {
        Object a = null;
        Throwable a0 = null;
        boolean b = this.bandwidthController.isLimitBandwidthModeOn();
        com.navdy.hud.app.profile.DriverProfileManager a1 = com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getDriverProfileManager();
        label1: {
            Object a2 = null;
            Throwable a3 = null;
            label0: {
                if (b) {
                    sLogger.v("limitbandwidth: on");
                    sLogger.v("limitbandwidth: turn map traffic off");
                    hereMapsManager.clearMapTraffic();
                    a1.disableTraffic();
                    sLogger.v("limitbandwidth: turn HERE traffic listener off");
                    synchronized(trafficRerouteLock) {
                        this.trafficRerouteListener.stop();
                        /*monexit(a)*/;
                    }
                    com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.maps.here.HereNavigationManager$10(this), 3);
                } else {
                    sLogger.v("limitbandwidth: off");
                    sLogger.v("limitbandwidth: turn map traffic on");
                    hereMapsManager.setTrafficOverlay(this.getCurrentNavigationMode());
                    a1.enableTraffic();
                    synchronized(trafficRerouteLock) {
                        if (com.navdy.hud.app.framework.glance.GlanceHelper.isTrafficNotificationEnabled()) {
                            sLogger.v("limitbandwidth: turn HERE traffic listener on");
                            this.trafficRerouteListener.start();
                        } else {
                            sLogger.v("limitbandwidth: HERE traffic listener off, not enabled");
                        }
                        /*monexit(a2)*/;
                    }
                    com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.maps.here.HereNavigationManager$11(this), 3);
                }
                return;
            }

        }

    }
    
    public void setDebugManeuverDistance(String s) {
        sLogger.v(new StringBuilder().append("debugManeuverDistance:").append(s).toString());
        this.debugManeuverDistance = s;
    }
    
    public void setDebugManeuverIcon(int i) {
        sLogger.v(new StringBuilder().append("debugManeuverIcon:").append(i).toString());
        this.debugManeuverIcon = i;
    }
    
    public void setDebugManeuverInstruction(String s) {
        sLogger.v(new StringBuilder().append("debugManeuverInstruction:").append(s).toString());
        this.debugManeuverInstruction = s;
    }
    
    public void setDebugManeuverState(com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder$ManeuverState a) {
        sLogger.v(new StringBuilder().append("debugManeuverState:").append(a).toString());
        this.debugManeuverState = a;
    }
    
    public void setDebugNextManeuverIcon(int i) {
        sLogger.v(new StringBuilder().append("debugNextManeuverIcon:").append(i).toString());
        if (i == 0) {
            this.currentNavigationInfo.maneuverAfterCurrentIconid = -1;
        }
        this.debugNextManeuverIcon = i;
    }
    
    public void setIgnoreArrived(boolean b) {
        if (this.isNavigationModeOn() && this.currentNavigationInfo.hasArrived) {
            sLogger.v(new StringBuilder().append("ignore arrived:").append(b).toString());
            this.currentNavigationInfo.ignoreArrived = b;
        }
    }
    
    public void setLastManeuver() {
        this.currentNavigationInfo.lastManeuver = true;
    }
    
    public void setLastTrafficRerouteTime(long j) {
        this.currentNavigationInfo.setLastTrafficRerouteTime(j);
    }
    
    public void setMapUpdateMode() {
        synchronized(this) {
            com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
            com.here.android.mpa.guidance.NavigationManager$MapUpdateMode a = com.navdy.hud.app.maps.here.HereNavigationManager.getHereMapUpdateMode();
            sLogger.v(new StringBuilder().append("setting here map update mode to[").append(a).append("]").toString());
            this.navigationManager.setMapUpdateMode(a);
        } catch(Throwable a0) {
            /*monexit(this)*/;
            throw a0;
        }
        /*monexit(this)*/;
    }
    
    public void setMapView(com.here.android.mpa.mapping.MapView a, com.navdy.hud.app.ui.component.homescreen.NavigationView a0) {
        this.mapView = a;
        this.navigationView = a0;
    }
    
    boolean setNavigationMode(com.navdy.hud.app.maps.NavigationMode a, com.navdy.hud.app.maps.here.HereNavigationInfo a0, StringBuilder a1) {
        boolean b = false;
        label2: synchronized(this) {
            com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
            sLogger.i(new StringBuilder().append("setNavigationMode:").append(a).toString());
            if (this.currentNavigationMode != a) {
                boolean b0 = false;
                com.here.android.mpa.guidance.NavigationManager$Error a2 = null;
                label7: {
                    if (a0 != null) {
                        break label7;
                    }
                    if (a == com.navdy.hud.app.maps.NavigationMode.MAP) {
                        break label7;
                    }
                    sLogger.e("setNavigationMode: navigation info cannot be null");
                    if (a1 != null) {
                        a1.append(context.getString(R.string.invalid_param));
                    }
                    b = false;
                    break label2;
                }
                com.navdy.service.library.events.navigation.NavigationSessionState a3 = this.currentSessionState;
                label6: if (this.currentNavigationMode == null) {
                    b0 = true;
                } else {
                    com.navdy.hud.app.maps.NavigationMode a4 = this.currentNavigationMode;
                    com.navdy.hud.app.maps.NavigationMode a5 = com.navdy.hud.app.maps.NavigationMode.MAP_ON_ROUTE;
                    label3: {
                        label4: {
                            label5: {
                                if (a4 != a5) {
                                    break label5;
                                }
                                if (a == com.navdy.hud.app.maps.NavigationMode.TBT_ON_ROUTE) {
                                    break label4;
                                }
                            }
                            if (this.currentNavigationMode != com.navdy.hud.app.maps.NavigationMode.TBT_ON_ROUTE) {
                                break label3;
                            }
                            if (a != com.navdy.hud.app.maps.NavigationMode.MAP_ON_ROUTE) {
                                break label3;
                            }
                        }
                        sLogger.v(new StringBuilder().append("navigation switch not reqd from= ").append(this.currentNavigationMode).append(" to=").append(a).toString());
                        this.currentNavigationMode = a;
                        b0 = false;
                        break label6;
                    }
                    boolean b1 = this.isNavigationModeOn();
                    sLogger.v(new StringBuilder().append("remove route before calling nav:stop navon=").append(b1).toString());
                    if (b1) {
                        sLogger.v(new StringBuilder().append("navigation stopped from:").append(this.currentSessionState).toString());
                        this.updateMapRoute((com.here.android.mpa.routing.Route)null, (com.navdy.service.library.events.navigation.NavigationSessionRouteChange$RerouteReason)null, (String)null, (String)null, false, false);
                        com.navdy.hud.app.maps.here.HereMapUtil.removeRouteInfo(sLogger, false);
                    }
                    this.hereNavController.stopNavigation(true);
                    this.currentSessionState = com.navdy.service.library.events.navigation.NavigationSessionState.NAV_SESSION_STOPPED;
                    if (a3 != com.navdy.service.library.events.navigation.NavigationSessionState.NAV_SESSION_STOPPED) {
                        this.postNavigationSessionStatusEvent(false);
                    }
                    a3 = this.currentSessionState;
                    b0 = true;
                }
                this.handler.removeCallbacks(this.etaCalcRunnable);
                this.currentNavigationInfo.clear();
                this.debugManeuverState = null;
                label0: {
                    boolean b2 = false;
                    label1: {
                        com.here.android.mpa.guidance.NavigationManager$Error a6 = null;
                        if (!b0) {
                            b2 = false;
                            break label1;
                        }
                        switch(com.navdy.hud.app.maps.here.HereNavigationManager$12.$SwitchMap$com$navdy$hud$app$maps$NavigationMode[a.ordinal()]) {
                            case 2: case 3: {
                                boolean b3 = this.isTrafficConsidered(a0.routeId);
                                this.updateMapRoute(a0.route, (com.navdy.service.library.events.navigation.NavigationSessionRouteChange$RerouteReason)null, (String)null, (String)null, false, b3);
                                a2 = this.hereNavController.startNavigation(a0.route, a0.simulationSpeed);
                                a6 = com.here.android.mpa.guidance.NavigationManager$Error.NONE;
                                break;
                            }
                            case 1: {
                                this.hereNavController.stopNavigation(false);
                                this.bus.post(new com.navdy.hud.app.maps.MapEvents$GPSSpeedEvent());
                                if (!com.navdy.hud.app.util.DeviceUtil.isUserBuild() && com.navdy.hud.app.maps.MapSettings.isLaneGuidanceEnabled()) {
                                    this.laneInfoListener.stop();
                                }
                                this.hereRealisticViewListener.stop();
                                this.currentNavigationInfo.copy(a0);
                                this.currentNavigationMode = com.navdy.hud.app.maps.NavigationMode.MAP;
                                this.currentSessionState = com.navdy.service.library.events.navigation.NavigationSessionState.NAV_SESSION_STOPPED;
                                this.mapController.setMapScheme(hereMapsManager.getTrackingMapScheme());
                                b2 = false;
                                break label1;
                            }
                            default: {
                                b2 = false;
                                break label1;
                            }
                        }
                        if (a2 != a6) {
                            break label0;
                        }
                        sLogger.v(new StringBuilder().append("switched navigation mode from:").append(this.currentNavigationMode).append(" to=").append(a).append(" simulating=").append((a0.simulationSpeed <= 0) ? "false" : "true").toString());
                        this.currentNavigationInfo.copy(a0);
                        this.currentNavigationMode = a;
                        this.currentSessionState = com.navdy.service.library.events.navigation.NavigationSessionState.NAV_SESSION_STARTED;
                        if (!com.navdy.hud.app.util.DeviceUtil.isUserBuild() && com.navdy.hud.app.maps.MapSettings.isLaneGuidanceEnabled()) {
                            this.laneInfoListener.start();
                        }
                        this.hereRealisticViewListener.start();
                        this.mapController.setMapScheme(hereMapsManager.getEnrouteMapScheme());
                        this.handler.postDelayed(this.etaCalcRunnable, 30000L);
                        if (this.currentNavigationInfo.navigationRouteRequest.routeAttributes.contains(com.navdy.service.library.events.navigation.NavigationRouteRequest$RouteAttribute.ROUTE_ATTRIBUTE_GAS)) {
                            com.navdy.hud.app.maps.here.HereMapUtil.saveGasRouteInfo(a0.navigationRouteRequest, a0.deviceId, sLogger);
                            b2 = true;
                        } else {
                            com.navdy.hud.app.maps.here.HereMapUtil.saveRouteInfo(a0.navigationRouteRequest, a0.deviceId, sLogger);
                            b2 = true;
                        }
                    }
                    this.setTrafficOverlay();
                    this.bus.post(new com.navdy.hud.app.maps.MapEvents$NavigationModeChange(this.currentNavigationMode));
                    if (b2) {
                        sLogger.v("send start maneuver");
                        this.sendStartManeuver(this.currentNavigationInfo.route);
                    }
                    if (a3 != this.currentSessionState) {
                        this.postNavigationSessionStatusEvent(false);
                    }
                    b = true;
                    break label2;
                }
                this.updateMapRoute((com.here.android.mpa.routing.Route)null, (com.navdy.service.library.events.navigation.NavigationSessionRouteChange$RerouteReason)null, (String)null, (String)null, false, false);
                this.currentSessionState = com.navdy.service.library.events.navigation.NavigationSessionState.NAV_SESSION_ERROR;
                sLogger.e(new StringBuilder().append("cannot switch navigation mode from:").append(this.currentNavigationMode).append(" to=").append(a).append(" error=").append(a2).append(" simulating=").append((a0.simulationSpeed <= 0) ? "false" : "true").toString());
                this.postNavigationSessionStatusEvent(false);
                if (a1 != null) {
                    a1.append(a2.name());
                }
                b = false;
            } else {
                sLogger.i(new StringBuilder().append("setNavigationMode: already in ").append(a).toString());
                b = true;
            }
        }
        /*monexit(this)*/;
        return b;
    }
    
    public void setReroute(com.here.android.mpa.routing.Route a, com.navdy.service.library.events.navigation.NavigationSessionRouteChange$RerouteReason a0, String s, String s0, boolean b, boolean b0) {
        synchronized(this) {
            com.navdy.service.library.log.Logger a1 = sLogger;
            a1.v(new StringBuilder().append("setReroute:").append(b).toString());
            if (this.isNavigationModeOn()) {
                this.clearNavManeuver();
                com.here.android.mpa.guidance.NavigationManager$Error a2 = this.hereNavController.setRoute(a);
                if (a2 != com.here.android.mpa.guidance.NavigationManager$Error.NONE) {
                    sLogger.e(new StringBuilder().append("setReroute: route could not be set:").append(a2).toString());
                } else {
                    this.updateMapRoute(a, a0, s, s0, b, b0);
                    sLogger.i("setReroute: done");
                }
            } else {
                sLogger.v("setReroute: nav mode ended");
            }
        }
        /*monexit(this)*/;
    }
    
    public void setShownFirstManeuver(boolean b) {
        this.currentNavigationInfo.firstManeuverShown = b;
        this.currentNavigationInfo.firstManeuverShownTime = android.os.SystemClock.elapsedRealtime();
    }
    
    public void setTrafficToMode() {
        com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
        try {
            com.navdy.hud.app.maps.here.HereMapController$State a = this.mapController.getState();
            if (com.navdy.hud.app.maps.here.HereNavigationManager$12.$SwitchMap$com$navdy$hud$app$maps$here$HereMapController$State[a.ordinal()] != 0) {
                sLogger.v(new StringBuilder().append("setTrafficToMode: ").append(a).toString());
                if (com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getDriverProfileManager().isTrafficEnabled()) {
                    this.mapController.setTrafficInfoVisible(true);
                    sLogger.v("setTrafficToMode: map traffic enabled");
                    com.here.android.mpa.mapping.MapRoute a0 = this.getCurrentMapRoute();
                    if (a0 != null) {
                        a0.setTrafficEnabled(true);
                        sLogger.v("setTrafficToMode: route traffic enabled");
                    }
                } else {
                    sLogger.v("setTrafficToMode: traffic is not enabled");
                }
            } else {
                sLogger.v(new StringBuilder().append("setTrafficToMode: ").append(a).append("::  no-op").toString());
            }
        } catch(Throwable a1) {
            sLogger.e(a1);
        }
    }
    
    public void startTrafficRerouteListener() {
        synchronized(trafficRerouteLock) {
            this.navdyTrafficRerouteManager.start();
            if (com.navdy.hud.app.framework.network.NetworkBandwidthController.getInstance().isLimitBandwidthModeOn()) {
                sLogger.v("startTrafficRerouteListener: limit b/w not starting here traffic reroute");
            } else {
                this.trafficRerouteListener.start();
            }
            /*monexit(a)*/;
        }
    }
    
    void startTrafficUpdater() {
        if (this.trafficUpdater != null && !this.trafficUpdaterStarted) {
            this.trafficUpdaterStarted = true;
            this.trafficUpdater.start();
        }
    }
    
    public void stopAndRemoveAllRoutes() {
        sLogger.v("stopAndRemoveAllRoutes");
        com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
        com.navdy.hud.app.maps.here.HereMapUtil.removeRouteInfo(sLogger, true);
        this.stopNavigation();
        com.navdy.hud.app.analytics.AnalyticsSupport.recordNavigationCancelled();
    }
    
    public void stopNavigation() {
        if (this.isNavigationModeOn()) {
            sLogger.v("stopNavigation: nav mode is on");
            com.navdy.service.library.events.navigation.NavigationRouteRequest a = this.currentNavigationInfo.navigationRouteRequest;
            String s = null;
            if (a != null) {
                s = a.label;
            }
            this.handleNavigationSessionRequest(new com.navdy.service.library.events.navigation.NavigationSessionRequest(com.navdy.service.library.events.navigation.NavigationSessionState.NAV_SESSION_STOPPED, s, this.currentNavigationInfo.routeId, Integer.valueOf(0), Boolean.valueOf(true)));
        } else {
            sLogger.v("stopNavigation: nav mode not on");
        }
    }
    
    public void stopTrafficRerouteListener() {
        synchronized(trafficRerouteLock) {
            this.navdyTrafficRerouteManager.stop();
            this.trafficRerouteListener.stop();
            /*monexit(a)*/;
        }
    }
    
    public void updateMapRoute(com.here.android.mpa.routing.Route a, com.navdy.service.library.events.navigation.NavigationSessionRouteChange$RerouteReason a0, com.navdy.service.library.events.navigation.NavigationRouteRequest a1, String s, String s0, boolean b, boolean b0) {
        synchronized(this) {
            boolean b1 = false;
            com.here.android.mpa.mapping.MapRoute a2 = this.currentNavigationInfo.mapRoute;
            label0: {
                label2: {
                    if (a2 != null) {
                        break label2;
                    }
                    if (this.currentNavigationInfo.mapDestinationMarker == null) {
                        b1 = false;
                        break label0;
                    }
                }
                this.tearDownCurrentRoute();
                this.currentNavigationInfo.destinationDirection = null;
                this.currentNavigationInfo.destinationDirectionStr = null;
                this.currentNavigationInfo.destinationIconId = -1;
                label1: {
                    if (a == null) {
                        break label1;
                    }
                    if (a0 != null) {
                        b1 = false;
                        break label0;
                    }
                }
                this.currentNavigationInfo.hasArrived = false;
                this.currentNavigationInfo.ignoreArrived = false;
                this.currentNavigationInfo.lastManeuver = false;
                this.currentNavigationInfo.arrivedManeuverDisplay = null;
                b1 = true;
            }
            if (a == null) {
                hereMapsManager.getLocationFixManager().addMarkers(this.mapController);
            } else {
                com.here.android.mpa.mapping.MapRoute a3 = new com.here.android.mpa.mapping.MapRoute(a);
                a3.setTrafficEnabled(true);
                this.addCurrentRoute(a3);
                this.currentNavigationInfo.mapRoute.setTrafficEnabled(true);
                this.bus.post(new com.navdy.hud.app.maps.MapEvents$NewRouteAdded(a0));
                this.newManeuverEventListener.setNewRoute();
                if (b) {
                    this.sendStartManeuver(a);
                    sLogger.v("updateMapRoute:sending start maneuver");
                }
                sLogger.v(new StringBuilder().append("map route added:").append(this.currentNavigationInfo.mapRoute).toString());
                this.removeDestinationMarker();
                this.currentNavigationInfo.mapDestinationMarker = null;
                this.currentNavigationInfo.currentRerouteReason = a0;
                this.addDestinationMarker(a.getDestination(), R.drawable.icon_pin_dot_destination);
                this.trafficUpdater.setRoute(this.currentNavigationInfo.mapRoute.getRoute());
                if (a0 != null) {
                    if (s == null) {
                        s = java.util.UUID.randomUUID().toString();
                    }
                    String s1 = this.currentNavigationInfo.routeId;
                    com.navdy.service.library.events.navigation.NavigationRouteRequest a4 = this.currentNavigationInfo.navigationRouteRequest;
                    if (a0 != com.navdy.service.library.events.navigation.NavigationSessionRouteChange$RerouteReason.NAV_SESSION_FUEL_REROUTE) {
                        if (a0 == com.navdy.service.library.events.navigation.NavigationSessionRouteChange$RerouteReason.NAV_SESSION_TRAFFIC_REROUTE) {
                            this.currentNavigationInfo.trafficRerouteOnce = true;
                        }
                    } else {
                        this.currentNavigationInfo.navigationRouteRequest = a1;
                        this.currentNavigationInfo.routeOptions = hereMapsManager.getRouteOptions();
                        this.currentNavigationInfo.streetAddress = com.navdy.hud.app.maps.here.HereMapUtil.parseStreetAddress(a1.streetAddress);
                        this.currentNavigationInfo.destinationLabel = null;
                    }
                    com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.maps.here.HereNavigationManager$3(this, s0, a, s, b0, s1, a0, a4), 2);
                }
            }
            if (b1) {
                this.refreshNavigationInfo();
            }
        }
        /*monexit(this)*/;
    }
    
    public void updateMapRoute(com.here.android.mpa.routing.Route a, com.navdy.service.library.events.navigation.NavigationSessionRouteChange$RerouteReason a0, String s, String s0, boolean b, boolean b0) {
        this.updateMapRoute(a, a0, this.currentNavigationInfo.navigationRouteRequest, s, s0, b, b0);
    }
    
    void updateNavigationInfo(com.here.android.mpa.routing.Maneuver a, com.here.android.mpa.routing.Maneuver a0, com.here.android.mpa.routing.Maneuver a1, boolean b) {
        com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.maps.here.HereNavigationManager$7(this, a, a0, a1, b), 4);
    }
}
