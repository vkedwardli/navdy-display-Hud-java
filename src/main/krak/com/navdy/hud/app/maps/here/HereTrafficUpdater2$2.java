package com.navdy.hud.app.maps.here;

class HereTrafficUpdater2$2 implements com.here.android.mpa.guidance.TrafficUpdater$GetEventsListener {
    final com.navdy.hud.app.maps.here.HereTrafficUpdater2 this$0;
    
    HereTrafficUpdater2$2(com.navdy.hud.app.maps.here.HereTrafficUpdater2 a) {
        super();
        this.this$0 = a;
    }
    
    public void onComplete(java.util.List a, com.here.android.mpa.guidance.TrafficUpdater$Error a0) {
        label4: {
            label2: {
                boolean b = false;
                try {
                    b = false;
                    com.navdy.hud.app.maps.here.HereTrafficUpdater2.access$000().i(new StringBuilder().append("onGetEventsListener::").append(a0).toString());
                    com.here.android.mpa.routing.Route a1 = com.navdy.hud.app.maps.here.HereTrafficUpdater2.access$100(this.this$0);
                    label3: {
                        if (a1 != null) {
                            break label3;
                        }
                        b = false;
                        com.navdy.hud.app.maps.here.HereTrafficUpdater2.access$000().i("onGetEventsListener: route is null");
                        break label4;
                    }
                    b = true;
                    com.navdy.hud.app.maps.here.HereTrafficUpdater2.access$202(this.this$0, (com.here.android.mpa.guidance.TrafficUpdater$RequestInfo)null);
                    com.navdy.hud.app.maps.here.HereTrafficUpdater2.access$302(this.this$0, 0L);
                    if (a0 != com.here.android.mpa.guidance.TrafficUpdater$Error.NONE) {
                        b = true;
                        com.navdy.hud.app.maps.here.HereTrafficUpdater2.access$000().i(new StringBuilder().append("onGetEventsListener: error ").append(a0).toString());
                        com.navdy.hud.app.maps.here.HereTrafficUpdater2.access$800(this.this$0).post(com.navdy.hud.app.maps.here.HereTrafficUpdater2.access$700());
                        break label2;
                    } else {
                        label0: {
                            label1: {
                                if (a == null) {
                                    break label1;
                                }
                                b = true;
                                if (a.size() != 0) {
                                    break label0;
                                }
                            }
                            b = true;
                            com.navdy.hud.app.maps.here.HereTrafficUpdater2.access$000().i("onGetEventsListener: no events");
                            com.navdy.hud.app.maps.here.HereTrafficUpdater2.access$800(this.this$0).post(com.navdy.hud.app.maps.here.HereTrafficUpdater2.access$700());
                            break label2;
                        }
                        b = true;
                        com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.maps.here.HereTrafficUpdater2$2$1(this, a1, a), 2);
                        break label2;
                    }
                } catch(Throwable a2) {
                    if (b) {
                        com.navdy.hud.app.maps.here.HereTrafficUpdater2.access$400(this.this$0, this.this$0.getRefereshInterval());
                    }
                    throw a2;
                }
            }
            com.navdy.hud.app.maps.here.HereTrafficUpdater2.access$400(this.this$0, this.this$0.getRefereshInterval());
        }
    }
}
