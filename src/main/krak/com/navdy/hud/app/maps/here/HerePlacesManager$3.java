package com.navdy.hud.app.maps.here;

final class HerePlacesManager$3 implements com.here.android.mpa.search.ResultListener {
    final com.navdy.hud.app.maps.here.HerePlacesManager$AutoCompleteCallback val$callBack;
    
    HerePlacesManager$3(com.navdy.hud.app.maps.here.HerePlacesManager$AutoCompleteCallback a) {
        super();
        this.val$callBack = a;
    }
    
    public void onCompleted(Object a, com.here.android.mpa.search.ErrorCode a0) {
        this.onCompleted((java.util.List)a, a0);
    }
    
    public void onCompleted(java.util.List a, com.here.android.mpa.search.ErrorCode a0) {
        try {
            this.val$callBack.result(a0, a);
        } catch(Throwable a1) {
            com.navdy.hud.app.maps.here.HerePlacesManager.access$000().e(a1);
        }
    }
}
