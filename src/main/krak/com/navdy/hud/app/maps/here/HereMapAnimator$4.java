package com.navdy.hud.app.maps.here;

class HereMapAnimator$4 implements Runnable {
    final com.navdy.hud.app.maps.here.HereMapAnimator this$0;
    final float val$tilt;
    
    HereMapAnimator$4(com.navdy.hud.app.maps.here.HereMapAnimator a, float f) {
        super();
        this.this$0 = a;
        this.val$tilt = f;
    }
    
    public void run() {
        Object a = null;
        Throwable a0 = null;
        long j = (com.navdy.hud.app.maps.here.HereMapAnimator.access$1500(this.this$0) <= 0L) ? 0L : android.os.SystemClock.elapsedRealtime() - com.navdy.hud.app.maps.here.HereMapAnimator.access$1500(this.this$0);
        boolean b = com.navdy.hud.app.maps.here.HereMapAnimator.access$1600(this.this$0, this.val$tilt);
        label0: {
            if (b) {
                com.navdy.hud.app.maps.here.HereMapAnimator.access$1502(this.this$0, android.os.SystemClock.elapsedRealtime());
                synchronized(com.navdy.hud.app.maps.here.HereMapAnimator.access$1700()) {
                    com.navdy.hud.app.maps.here.HereMapAnimator.access$1802(this.this$0, j);
                    com.navdy.hud.app.maps.here.HereMapAnimator.access$1902(this.this$0, this.val$tilt);
                    /*monexit(a)*/;
                }
            } else {
                com.navdy.hud.app.maps.here.HereMapAnimator.access$400().v(new StringBuilder().append("filtering out spurious tilt: ").append(this.val$tilt).toString());
            }
            return;
        }

    }
}
