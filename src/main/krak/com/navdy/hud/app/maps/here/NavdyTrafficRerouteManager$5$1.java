package com.navdy.hud.app.maps.here;

class NavdyTrafficRerouteManager$5$1 implements Runnable {
    final com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager$5 this$1;
    final java.util.ArrayList val$outgoingResults;
    
    NavdyTrafficRerouteManager$5$1(com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager$5 a, java.util.ArrayList a0) {
        super();
        this.this$1 = a;
        this.val$outgoingResults = a0;
    }
    
    public void run() {
        label2: try {
            if (com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$800(this.this$1.this$0)) {
                java.util.ArrayList a = this.val$outgoingResults;
                label0: {
                    label1: {
                        if (a == null) {
                            break label1;
                        }
                        if (this.val$outgoingResults.size() != 0) {
                            break label0;
                        }
                    }
                    com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$100().v("reCalculateCurrentRoute: success, but no result");
                    com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$600(this.this$1.this$0);
                    com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$000(this.this$1.this$0).fasterRouteNotFound(this.this$1.val$notifFromHereTraffic, this.this$1.val$diff, this.this$1.val$via, this.this$1.val$currentVia);
                    break label2;
                }
                com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$100().v("reCalculateCurrentRoute: success");
                com.navdy.service.library.events.navigation.NavigationRouteResult a0 = (com.navdy.service.library.events.navigation.NavigationRouteResult)this.val$outgoingResults.get(0);
                com.navdy.hud.app.maps.here.HereRouteCache a1 = com.navdy.hud.app.maps.here.HereRouteCache.getInstance();
                com.navdy.hud.app.maps.here.HereRouteCache$RouteInfo a2 = a1.getRoute(a0.routeId);
                if (a2 != null) {
                    a1.removeRoute(a0.routeId);
                    if (a0.duration_traffic.intValue() != 0) {
                        com.here.android.mpa.routing.RouteTta a3 = a2.route.getTta(com.here.android.mpa.routing.Route$TrafficPenaltyMode.OPTIMAL, 268435455);
                        if (a3 != null) {
                            int i = a3.getDuration();
                            int i0 = this.this$1.val$fasterRouteTta.getDuration();
                            com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$100().v(new StringBuilder().append("reCalculateCurrentRoute: currentDuration=").append(i).append(" fasterDuration=").append(i0).toString());
                            long j = (long)(i - i0);
                            int i1 = com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$000(this.this$1.this$0).getRerouteMinDuration();
                            if (j < (long)i1) {
                                com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$100().v(new StringBuilder().append("reCalculateCurrentRoute: duration:").append(j).append(" < threshold:").append(i1).toString());
                                com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$600(this.this$1.this$0);
                                com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$000(this.this$1.this$0).fasterRouteNotFound(this.this$1.val$notifFromHereTraffic, j, this.this$1.val$via, this.this$1.val$currentVia);
                            } else {
                                com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$100().v(new StringBuilder().append("reCalculateCurrentRoute: set faster route, duration:").append(j).append(" >= threshold:").append(i1).toString());
                                long j0 = System.currentTimeMillis();
                                long j1 = j0 + (long)(i0 * 1000);
                                java.util.Date a4 = new java.util.Date(j1);
                                java.util.Date a5 = new java.util.Date(j0);
                                java.util.Date a6 = new java.util.Date(this.this$1.val$etaUtc);
                                com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$100().v(new StringBuilder().append("reCalculateCurrentRoute: newEta:").append(a4).append(" now=").append(a5).append(" currentEta=").append(a6).toString());
                                com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$000(this.this$1.this$0).setFasterRoute(this.this$1.val$fasterRoute, this.this$1.val$notifFromHereTraffic, this.this$1.val$via, 0L, this.this$1.val$additionalVia, 0L, this.this$1.val$distanceDiff, j1);
                            }
                        } else {
                            com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$100().v("reCalculateCurrentRoute: could not get traffic route tta");
                            com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$600(this.this$1.this$0);
                            com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$000(this.this$1.this$0).fasterRouteNotFound(this.this$1.val$notifFromHereTraffic, this.this$1.val$diff, this.this$1.val$via, this.this$1.val$currentVia);
                        }
                    } else {
                        com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$100().v("reCalculateCurrentRoute: did not get traffic duration");
                        com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$600(this.this$1.this$0);
                        com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$000(this.this$1.this$0).fasterRouteNotFound(this.this$1.val$notifFromHereTraffic, this.this$1.val$diff, this.this$1.val$via, this.this$1.val$currentVia);
                    }
                } else {
                    com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$100().v("reCalculateCurrentRoute: did not get routeinfo");
                    com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$600(this.this$1.this$0);
                    com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$000(this.this$1.this$0).fasterRouteNotFound(this.this$1.val$notifFromHereTraffic, this.this$1.val$diff, this.this$1.val$via, this.this$1.val$currentVia);
                }
            } else {
                com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$100().v("reCalculateCurrentRoute: success,route manager stopped");
                com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$600(this.this$1.this$0);
                com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$000(this.this$1.this$0).fasterRouteNotFound(this.this$1.val$notifFromHereTraffic, this.this$1.val$diff, this.this$1.val$via, this.this$1.val$currentVia);
            }
        } catch(Throwable a7) {
            com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$100().e(a7);
        }
    }
}
