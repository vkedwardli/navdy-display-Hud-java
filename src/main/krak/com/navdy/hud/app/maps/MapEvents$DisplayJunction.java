package com.navdy.hud.app.maps;

public class MapEvents$DisplayJunction {
    public com.here.android.mpa.common.Image junction;
    public com.here.android.mpa.common.Image signpost;
    
    public MapEvents$DisplayJunction(com.here.android.mpa.common.Image a, com.here.android.mpa.common.Image a0) {
        this.junction = a;
        this.signpost = a0;
    }
}
