package com.navdy.hud.app.maps.here;

class HereMapController$1 implements Runnable {
    final com.navdy.hud.app.maps.here.HereMapController this$0;
    final float val$tilt;
    
    HereMapController$1(com.navdy.hud.app.maps.here.HereMapController a, float f) {
        super();
        this.this$0 = a;
        this.val$tilt = f;
    }
    
    public void run() {
        com.navdy.hud.app.maps.here.HereMapController.access$000(this.this$0).setTilt(this.val$tilt);
    }
}
