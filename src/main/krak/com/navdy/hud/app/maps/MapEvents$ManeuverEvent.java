package com.navdy.hud.app.maps;

public class MapEvents$ManeuverEvent {
    public com.here.android.mpa.routing.Maneuver maneuver;
    public com.navdy.hud.app.maps.MapEvents$ManeuverEvent$Type type;
    
    public MapEvents$ManeuverEvent(com.navdy.hud.app.maps.MapEvents$ManeuverEvent$Type a, com.here.android.mpa.routing.Maneuver a0) {
        this.type = a;
        this.maneuver = a0;
    }
}
