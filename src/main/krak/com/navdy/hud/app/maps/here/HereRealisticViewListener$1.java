package com.navdy.hud.app.maps.here;

class HereRealisticViewListener$1 implements Runnable {
    final com.navdy.hud.app.maps.here.HereRealisticViewListener this$0;
    
    HereRealisticViewListener$1(com.navdy.hud.app.maps.here.HereRealisticViewListener a) {
        super();
        this.this$0 = a;
    }
    
    public void run() {
        com.navdy.hud.app.maps.here.HereRealisticViewListener.access$000(this.this$0).setRealisticViewMode(com.here.android.mpa.guidance.NavigationManager$RealisticViewMode.NIGHT);
        com.navdy.hud.app.maps.here.HereRealisticViewListener.access$000(this.this$0).addRealisticViewAspectRatio(com.here.android.mpa.guidance.NavigationManager$AspectRatio.AR_3x5);
        com.navdy.hud.app.maps.here.HereRealisticViewListener.access$000(this.this$0).addRealisticViewListener(new java.lang.ref.WeakReference(this.this$0));
        com.navdy.hud.app.maps.here.HereRealisticViewListener.access$100().v("added realistic view listener");
    }
}
