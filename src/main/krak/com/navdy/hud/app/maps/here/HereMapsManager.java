package com.navdy.hud.app.maps.here;

public class HereMapsManager {
    final public static double DEFAULT_LATITUDE = 37.802086;
    final public static double DEFAULT_LONGITUDE = -122.419015;
    final public static float DEFAULT_TILT = 60f;
    final public static float DEFAULT_ZOOM_LEVEL = 16.5f;
    final private static String DISABLE_MAPS_PROPERTY = "persist.sys.map.disable";
    final private static boolean ENABLE_MAPS;
    final private static String ENROUTE_MAP_SCHEME = "hybrid.night";
    final private static int GPS_SPEED_LAST_LOCATION_THRESHOLD = 2000;
    final static int MIN_SAME_POSITION_UPDATE_THRESHOLD = 500;
    final private static String MW_CONFIG_EXCEPTION_MSG = "Invalid configuration file. Check MWConfig!";
    final private static String NAVDY_HERE_MAP_SERVICE_NAME = "com.navdy.HereMapService";
    final public static float ROUTE_CALC_START_ZOOM_LEVEL = 15.5f;
    final private static String TRACKING_MAP_SCHEME = "carnav.night";
    final private static String TRAFFIC_REROUTE_PROPERTY = "persist.sys.hud_traffic_reroute";
    private static boolean recalcCurrentRouteForTraffic;
    final private static com.navdy.service.library.log.Logger sLogger;
    final private static com.navdy.hud.app.maps.here.HereMapsManager singleton;
    final private android.os.HandlerThread bkLocationReceiverHandlerThread;
    @Inject
    com.squareup.otto.Bus bus;
    private android.content.Context context;
    private com.here.android.mpa.common.OnEngineInitListener engineInitListener;
    private volatile boolean extrapolationOn;
    private android.os.Handler handler;
    private com.navdy.hud.app.maps.here.HereLocationFixManager hereLocationFixManager;
    private com.navdy.hud.app.maps.here.HereMapAnimator hereMapAnimator;
    private Object initLock;
    private boolean initialMapRendering;
    private com.here.android.mpa.common.GeoPosition lastGeoPosition;
    private long lastGeoPositionTime;
    private android.content.SharedPreferences$OnSharedPreferenceChangeListener listener;
    private volatile boolean lowBandwidthDetected;
    @Inject
    com.navdy.hud.app.profile.DriverProfileManager mDriverProfileManager;
    private com.here.android.mpa.mapping.Map map;
    private com.navdy.hud.app.maps.here.HereMapController mapController;
    private boolean mapDataVerified;
    private com.here.android.mpa.common.MapEngine mapEngine;
    private long mapEngineStartTime;
    private com.here.android.mpa.common.OnEngineInitListener$Error mapError;
    private boolean mapInitLoaderComplete;
    private boolean mapInitialized;
    private boolean mapInitializing;
    private com.here.android.mpa.odml.MapLoader mapLoader;
    private int mapPackageCount;
    private com.here.android.mpa.mapping.MapView mapView;
    private com.navdy.hud.app.ui.component.homescreen.NavigationView navigationView;
    private com.navdy.hud.app.maps.here.HereOfflineMapsVersion offlineMapsVersion;
    private com.here.android.mpa.common.PositioningManager$OnPositionChangedListener positionChangedListener;
    private com.here.android.mpa.common.PositioningManager positioningManager;
    private boolean positioningManagerInstalled;
    @Inject
    com.navdy.hud.app.device.PowerManager powerManager;
    private com.navdy.hud.app.maps.notification.RouteCalculationEventHandler routeCalcEventHandler;
    private com.here.android.mpa.common.GeoCoordinate routeStartPoint;
    @Inject
    android.content.SharedPreferences sharedPreferences;
    private com.navdy.hud.app.manager.SpeedManager speedManager;
    private volatile boolean turnEngineOn;
    private boolean voiceSkinsLoaded;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.maps.here.HereMapsManager.class);
        ENABLE_MAPS = !com.navdy.hud.app.util.os.SystemProperties.getBoolean("persist.sys.map.disable", false);
        recalcCurrentRouteForTraffic = false;
        singleton = new com.navdy.hud.app.maps.here.HereMapsManager();
    }
    
    private HereMapsManager() {
        this.initLock = new Object();
        this.initialMapRendering = true;
        this.lowBandwidthDetected = false;
        this.turnEngineOn = false;
        this.mapInitializing = true;
        this.handler = new android.os.Handler(android.os.Looper.getMainLooper());
        this.engineInitListener = (com.here.android.mpa.common.OnEngineInitListener)new com.navdy.hud.app.maps.here.HereMapsManager$1(this);
        this.positionChangedListener = (com.here.android.mpa.common.PositioningManager$OnPositionChangedListener)new com.navdy.hud.app.maps.here.HereMapsManager$3(this);
        this.listener = (android.content.SharedPreferences$OnSharedPreferenceChangeListener)new com.navdy.hud.app.maps.here.HereMapsManager$4(this);
        this.context = com.navdy.hud.app.HudApplication.getAppContext();
        mortar.Mortar.inject(this.context, this);
        this.speedManager = com.navdy.hud.app.manager.SpeedManager.getInstance();
        if (ENABLE_MAPS) {
            this.checkforNetworkProvider();
        }
        this.bkLocationReceiverHandlerThread = new android.os.HandlerThread("here_bk_location");
        this.bkLocationReceiverHandlerThread.start();
        this.bus.register(this);
    }
    
    static long access$000(com.navdy.hud.app.maps.here.HereMapsManager a) {
        return a.mapEngineStartTime;
    }
    
    static com.navdy.service.library.log.Logger access$100() {
        return sLogger;
    }
    
    static void access$1000(com.navdy.hud.app.maps.here.HereMapsManager a) {
        a.registerConnectivityReceiver();
    }
    
    static com.navdy.hud.app.maps.here.HereMapAnimator access$1100(com.navdy.hud.app.maps.here.HereMapsManager a) {
        return a.hereMapAnimator;
    }
    
    static com.navdy.hud.app.maps.here.HereMapAnimator access$1102(com.navdy.hud.app.maps.here.HereMapsManager a, com.navdy.hud.app.maps.here.HereMapAnimator a0) {
        a.hereMapAnimator = a0;
        return a0;
    }
    
    static boolean access$1200(com.navdy.hud.app.maps.here.HereMapsManager a) {
        return a.initialMapRendering;
    }
    
    static com.navdy.hud.app.maps.here.HereLocationFixManager access$1300(com.navdy.hud.app.maps.here.HereMapsManager a) {
        return a.hereLocationFixManager;
    }
    
    static com.navdy.hud.app.maps.here.HereLocationFixManager access$1302(com.navdy.hud.app.maps.here.HereMapsManager a, com.navdy.hud.app.maps.here.HereLocationFixManager a0) {
        a.hereLocationFixManager = a0;
        return a0;
    }
    
    static com.navdy.hud.app.maps.notification.RouteCalculationEventHandler access$1402(com.navdy.hud.app.maps.here.HereMapsManager a, com.navdy.hud.app.maps.notification.RouteCalculationEventHandler a0) {
        a.routeCalcEventHandler = a0;
        return a0;
    }
    
    static void access$1500(com.navdy.hud.app.maps.here.HereMapsManager a) {
        a.initMapLoader();
    }
    
    static com.navdy.hud.app.maps.here.HereOfflineMapsVersion access$1602(com.navdy.hud.app.maps.here.HereMapsManager a, com.navdy.hud.app.maps.here.HereOfflineMapsVersion a0) {
        a.offlineMapsVersion = a0;
        return a0;
    }
    
    static Object access$1700(com.navdy.hud.app.maps.here.HereMapsManager a) {
        return a.initLock;
    }
    
    static boolean access$1802(com.navdy.hud.app.maps.here.HereMapsManager a, boolean b) {
        a.mapInitializing = b;
        return b;
    }
    
    static boolean access$1900(com.navdy.hud.app.maps.here.HereMapsManager a) {
        return a.mapInitialized;
    }
    
    static boolean access$1902(com.navdy.hud.app.maps.here.HereMapsManager a, boolean b) {
        a.mapInitialized = b;
        return b;
    }
    
    static com.here.android.mpa.common.MapEngine access$200(com.navdy.hud.app.maps.here.HereMapsManager a) {
        return a.mapEngine;
    }
    
    static void access$2000(com.navdy.hud.app.maps.here.HereMapsManager a) {
        a.setEngineOnlineState();
    }
    
    static void access$2100(com.navdy.hud.app.maps.here.HereMapsManager a) {
        a.startTrafficUpdater();
    }
    
    static com.here.android.mpa.common.OnEngineInitListener$Error access$2202(com.navdy.hud.app.maps.here.HereMapsManager a, com.here.android.mpa.common.OnEngineInitListener$Error a0) {
        a.mapError = a0;
        return a0;
    }
    
    static com.here.android.mpa.odml.MapLoader access$2300(com.navdy.hud.app.maps.here.HereMapsManager a) {
        return a.mapLoader;
    }
    
    static com.here.android.mpa.odml.MapLoader access$2302(com.navdy.hud.app.maps.here.HereMapsManager a, com.here.android.mpa.odml.MapLoader a0) {
        a.mapLoader = a0;
        return a0;
    }
    
    static void access$2400(com.navdy.hud.app.maps.here.HereMapsManager a, com.here.android.mpa.odml.MapPackage a0, java.util.EnumSet a1) {
        a.printMapPackages(a0, a1);
    }
    
    static int access$2500(com.navdy.hud.app.maps.here.HereMapsManager a) {
        return a.mapPackageCount;
    }
    
    static int access$2502(com.navdy.hud.app.maps.here.HereMapsManager a, int i) {
        a.mapPackageCount = i;
        return i;
    }
    
    static boolean access$2602(com.navdy.hud.app.maps.here.HereMapsManager a, boolean b) {
        a.mapDataVerified = b;
        return b;
    }
    
    static void access$2700(com.navdy.hud.app.maps.here.HereMapsManager a) {
        a.invokeMapLoader();
    }
    
    static boolean access$2800(com.navdy.hud.app.maps.here.HereMapsManager a) {
        return a.extrapolationOn;
    }
    
    static boolean access$2802(com.navdy.hud.app.maps.here.HereMapsManager a, boolean b) {
        a.extrapolationOn = b;
        return b;
    }
    
    static void access$2900(com.navdy.hud.app.maps.here.HereMapsManager a) {
        a.sendExtrapolationEvent();
    }
    
    static com.here.android.mpa.mapping.Map access$300(com.navdy.hud.app.maps.here.HereMapsManager a) {
        return a.map;
    }
    
    static com.here.android.mpa.common.GeoPosition access$3000(com.navdy.hud.app.maps.here.HereMapsManager a) {
        return a.lastGeoPosition;
    }
    
    static com.here.android.mpa.common.GeoPosition access$3002(com.navdy.hud.app.maps.here.HereMapsManager a, com.here.android.mpa.common.GeoPosition a0) {
        a.lastGeoPosition = a0;
        return a0;
    }
    
    static com.here.android.mpa.mapping.Map access$302(com.navdy.hud.app.maps.here.HereMapsManager a, com.here.android.mpa.mapping.Map a0) {
        a.map = a0;
        return a0;
    }
    
    static long access$3100(com.navdy.hud.app.maps.here.HereMapsManager a) {
        return a.lastGeoPositionTime;
    }
    
    static long access$3102(com.navdy.hud.app.maps.here.HereMapsManager a, long j) {
        a.lastGeoPositionTime = j;
        return j;
    }
    
    static com.navdy.hud.app.manager.SpeedManager access$3200(com.navdy.hud.app.maps.here.HereMapsManager a) {
        return a.speedManager;
    }
    
    static void access$3300(com.navdy.hud.app.maps.here.HereMapsManager a) {
        a.initialize();
    }
    
    static android.os.Handler access$400(com.navdy.hud.app.maps.here.HereMapsManager a) {
        return a.handler;
    }
    
    static com.navdy.hud.app.maps.here.HereMapController access$500(com.navdy.hud.app.maps.here.HereMapsManager a) {
        return a.mapController;
    }
    
    static com.navdy.hud.app.maps.here.HereMapController access$502(com.navdy.hud.app.maps.here.HereMapsManager a, com.navdy.hud.app.maps.here.HereMapController a0) {
        a.mapController = a0;
        return a0;
    }
    
    static void access$600(com.navdy.hud.app.maps.here.HereMapsManager a, com.here.android.mpa.common.GeoCoordinate a0) {
        a.setMapAttributes(a0);
    }
    
    static void access$700(com.navdy.hud.app.maps.here.HereMapsManager a) {
        a.setMapTraffic();
    }
    
    static void access$800(com.navdy.hud.app.maps.here.HereMapsManager a, com.here.android.mpa.mapping.Map a0) {
        a.setMapPoiLayer(a0);
    }
    
    static com.here.android.mpa.common.PositioningManager access$900(com.navdy.hud.app.maps.here.HereMapsManager a) {
        return a.positioningManager;
    }
    
    static com.here.android.mpa.common.PositioningManager access$902(com.navdy.hud.app.maps.here.HereMapsManager a, com.here.android.mpa.common.PositioningManager a0) {
        a.positioningManager = a0;
        return a0;
    }
    
    private void checkforNetworkProvider() {
        this.handler.post((Runnable)new com.navdy.hud.app.maps.here.HereMapsManager$5(this));
    }
    
    private float getDefaultTiltLevel() {
        return 60f;
    }
    
    private double getDefaultZoomLevel() {
        return 16.5;
    }
    
    public static com.navdy.hud.app.maps.here.HereMapsManager getInstance() {
        return singleton;
    }
    
    private void initMapLoader() {
        com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.maps.here.HereMapsManager$2(this), 2);
    }
    
    private void initialize() {
        sLogger.v(":initializing voice skins");
        com.navdy.hud.app.maps.here.VoiceSkinsConfigurator.updateVoiceSkins();
        sLogger.v(":initializing event handlers");
        com.navdy.hud.app.maps.MapsEventHandler.getInstance();
        if (com.navdy.hud.app.maps.MapSettings.isDebugHereLocation()) {
            com.navdy.hud.app.debug.DebugReceiver.setHereDebugLocation(true);
        }
        com.here.android.mpa.mapping.Map.setMaximumFps(com.navdy.hud.app.maps.MapSettings.getMapFps());
        com.here.android.mpa.mapping.Map.enableMaximumFpsLimit(true);
        sLogger.v(new StringBuilder().append("map-fps [").append(com.here.android.mpa.mapping.Map.getMaximumFps()).append("] enabled[").append(com.here.android.mpa.mapping.Map.isMaximumFpsLimited()).append("]").toString());
        try {
            java.lang.reflect.Field a = com.nokia.maps.MapSettings.class.getDeclaredField("g");
            a.setAccessible(true);
            sLogger.e(new StringBuilder().append("enable worker thread before is ").append(a.get(null)).toString());
            a.set(null, com.nokia.maps.MapSettings$b.a);
            sLogger.e(new StringBuilder().append("enable worker thread after is ").append(a.get(null)).toString());
        } catch(Throwable a0) {
            sLogger.e("enable worker thread", a0);
        }
        this.mapEngine = com.here.android.mpa.common.MapEngine.getInstance();
        sLogger.v("calling maps engine init");
        this.sharedPreferences.registerOnSharedPreferenceChangeListener(this.listener);
        recalcCurrentRouteForTraffic = com.navdy.hud.app.util.os.SystemProperties.getBoolean("persist.sys.hud_traffic_reroute", false);
        sLogger.v(new StringBuilder().append("persist.sys.hud_traffic_reroute=").append(recalcCurrentRouteForTraffic).toString());
        String s = com.navdy.hud.app.storage.PathManager.getInstance().getMapsPartitionPath();
        boolean b = s.isEmpty();
        label0: {
            Exception a1 = null;
            if (b) {
                break label0;
            }
            try {
                if (com.here.android.mpa.common.MapSettings.setIsolatedDiskCacheRootPath(new StringBuilder().append(s).append(java.io.File.separator).append(".here-maps").toString(), "com.navdy.HereMapService")) {
                    break label0;
                }
                sLogger.e("setIsolatedDiskCacheRootPath() failed");
                break label0;
            } catch(Exception a2) {
                a1 = a2;
            }
            sLogger.e("exception in setIsolatedDiskCacheRootPath()", (Throwable)a1);
        }
        this.mapEngineStartTime = android.os.SystemClock.elapsedRealtime();
        this.mapEngine.init(this.context, this.engineInitListener);
    }
    
    private void invokeMapLoader() {
        if (this.mapLoader != null) {
            if (com.navdy.service.library.util.SystemUtils.isConnectedToNetwork(com.navdy.hud.app.HudApplication.getAppContext())) {
                if (this.mapInitLoaderComplete) {
                    sLogger.v("invokeMapLoader: already complete");
                } else {
                    this.mapInitLoaderComplete = true;
                    boolean b = this.mapLoader.getMapPackages();
                    sLogger.v(new StringBuilder().append("initMapLoader called getMapPackages:").append(b).toString());
                }
            } else {
                sLogger.v("invokeMapLoader: not connected to n/w");
            }
        } else {
            sLogger.v("invokeMapLoader: no maploader");
        }
    }
    
    private boolean isTrafficOverlayEnabled(com.navdy.hud.app.maps.NavigationMode a) {
        com.here.android.mpa.mapping.MapTrafficLayer a0 = this.map.getMapTrafficLayer();
        return (a != com.navdy.hud.app.maps.NavigationMode.MAP) ? a == com.navdy.hud.app.maps.NavigationMode.MAP_ON_ROUTE && a0.isEnabled(com.here.android.mpa.mapping.MapTrafficLayer$RenderLayer.ONROUTE) : a0.isEnabled(com.here.android.mpa.mapping.MapTrafficLayer$RenderLayer.FLOW) && a0.isEnabled(com.here.android.mpa.mapping.MapTrafficLayer$RenderLayer.INCIDENT);
    }
    
    private boolean isTrafficOverlayVisible() {
        return this.isTrafficOverlayEnabled(com.navdy.hud.app.maps.here.HereNavigationManager.getInstance().getCurrentNavigationMode());
    }
    
    private void printMapPackages(com.here.android.mpa.odml.MapPackage a, java.util.EnumSet a0) {
        if (a != null) {
            if (a0.contains(a.getInstallationState())) {
                this.mapPackageCount = this.mapPackageCount + 1;
            }
            java.util.List a1 = a.getChildren();
            if (a1 != null) {
                Object a2 = a1.iterator();
                while(((java.util.Iterator)a2).hasNext()) {
                    this.printMapPackages((com.here.android.mpa.odml.MapPackage)((java.util.Iterator)a2).next(), a0);
                }
            }
        }
    }
    
    private void registerConnectivityReceiver() {
        android.content.IntentFilter a = new android.content.IntentFilter("android.net.conn.CONNECTIVITY_CHANGE");
        com.navdy.hud.app.maps.here.HereMapsManager$11 a0 = new com.navdy.hud.app.maps.here.HereMapsManager$11(this);
        this.context.registerReceiver((android.content.BroadcastReceiver)a0, a);
    }
    
    private void sendExtrapolationEvent() {
        android.os.Bundle a = new android.os.Bundle();
        a.putBoolean("EXTRAPOLATION_FLAG", this.extrapolationOn);
        com.navdy.hud.app.device.gps.GpsUtils.sendEventBroadcast("EXTRAPOLATION", a);
    }
    
    private void setEngineOnlineState() {
        boolean b = com.navdy.hud.app.framework.network.NetworkStateManager.isConnectedToNetwork(this.context);
        this.lowBandwidthDetected = false;
        sLogger.i(new StringBuilder().append("setEngineOnlineState:setting maps engine online state:").append(b).toString());
        if (b) {
            com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.maps.here.HereMapsManager$6(this), 1);
        } else {
            com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.maps.here.HereMapsManager$7(this), 1);
        }
    }
    
    private void setMapAttributes(com.here.android.mpa.common.GeoCoordinate a) {
        this.map.setExtrudedBuildingsVisible(false);
        this.map.setLandmarksVisible(false);
        this.map.setStreetLevelCoverageVisible(false);
        this.map.setMapScheme(this.getTrackingMapScheme());
        if (a == null) {
            sLogger.v("geoCoordinate not available");
        } else {
            this.map.setCenter(a, com.here.android.mpa.mapping.Map$Animation.NONE, this.getDefaultZoomLevel(), -1f, this.getDefaultTiltLevel());
            sLogger.v(new StringBuilder().append("setcenterDefault:").append(a).toString());
        }
    }
    
    private void setMapOnRouteTraffic() {
        this.mapController.execute((Runnable)new com.navdy.hud.app.maps.here.HereMapsManager$9(this));
    }
    
    private void setMapPoiLayer(com.here.android.mpa.mapping.Map a) {
        a.setCartoMarkersVisible(false);
        a.getMapTransitLayer().setMode(com.here.android.mpa.mapping.MapTransitLayer$Mode.NOTHING);
        com.here.android.mpa.mapping.Map$LayerCategory a0 = com.here.android.mpa.mapping.Map$LayerCategory.ICON_PUBLIC_TRANSIT_STATION;
        com.here.android.mpa.mapping.Map$LayerCategory[] a1 = new com.here.android.mpa.mapping.Map$LayerCategory[15];
        a1[0] = com.here.android.mpa.mapping.Map$LayerCategory.PUBLIC_TRANSIT_LINE;
        a1[1] = com.here.android.mpa.mapping.Map$LayerCategory.LABEL_PUBLIC_TRANSIT_STATION;
        a1[2] = com.here.android.mpa.mapping.Map$LayerCategory.LABEL_PUBLIC_TRANSIT_LINE;
        a1[3] = com.here.android.mpa.mapping.Map$LayerCategory.BEACH;
        a1[4] = com.here.android.mpa.mapping.Map$LayerCategory.WOODLAND;
        a1[5] = com.here.android.mpa.mapping.Map$LayerCategory.DESERT;
        a1[6] = com.here.android.mpa.mapping.Map$LayerCategory.GLACIER;
        a1[7] = com.here.android.mpa.mapping.Map$LayerCategory.AMUSEMENT_PARK;
        a1[8] = com.here.android.mpa.mapping.Map$LayerCategory.ANIMAL_PARK;
        a1[9] = com.here.android.mpa.mapping.Map$LayerCategory.BUILTUP;
        a1[10] = com.here.android.mpa.mapping.Map$LayerCategory.CEMETERY;
        a1[11] = com.here.android.mpa.mapping.Map$LayerCategory.BUILDING;
        a1[12] = com.here.android.mpa.mapping.Map$LayerCategory.NEIGHBORHOOD_AREA;
        a1[13] = com.here.android.mpa.mapping.Map$LayerCategory.NATIONAL_PARK;
        a1[14] = com.here.android.mpa.mapping.Map$LayerCategory.NATIVE_RESERVATION;
        a.setVisibleLayers(java.util.EnumSet.of((Enum)a0, (Enum[])a1), false);
        java.util.EnumSet a2 = a.getVisibleLayers();
        sLogger.v(new StringBuilder().append("==== visible layers == [").append(a2.size()).append("]").toString());
        StringBuilder a3 = new StringBuilder();
        Object a4 = a2.iterator();
        boolean b = true;
        while(((java.util.Iterator)a4).hasNext()) {
            com.here.android.mpa.mapping.Map$LayerCategory a5 = (com.here.android.mpa.mapping.Map$LayerCategory)((java.util.Iterator)a4).next();
            if (!b) {
                a3.append(", ");
            }
            a3.append(a5.name());
            b = false;
        }
        sLogger.v(a3.toString());
    }
    
    private void setMapTraffic() {
        this.mapController.execute((Runnable)new com.navdy.hud.app.maps.here.HereMapsManager$8(this));
    }
    
    private void startTrafficUpdater() {
        try {
            com.navdy.hud.app.maps.here.HereNavigationManager.getInstance().startTrafficUpdater();
        } catch(Throwable a) {
            sLogger.e(a);
        }
    }
    
    public void checkForMapDataUpdate() {
        try {
            if (this.isInitialized()) {
                if (this.mapLoader != null) {
                    if (this.mapInitLoaderComplete) {
                        boolean b = this.mapLoader.checkForMapDataUpdate();
                        sLogger.v(new StringBuilder().append("checkForMapDataUpdate MapLoader: called checkForMapDataUpdate:").append(b).toString());
                    } else {
                        sLogger.v("checkForMapDataUpdate MapLoader: loader not initialized");
                    }
                } else {
                    sLogger.v("checkForMapDataUpdate MapLoader: not available");
                }
            } else {
                sLogger.v("checkForMapDataUpdate MapLoader: engine not initialized");
            }
        } catch(Throwable a) {
            sLogger.e(a);
        }
    }
    
    void clearMapTraffic() {
        this.mapController.execute((Runnable)new com.navdy.hud.app.maps.here.HereMapsManager$10(this));
    }
    
    public void clearTrafficOverlay() {
        this.clearMapTraffic();
    }
    
    public android.os.Looper getBkLocationReceiverLooper() {
        return this.bkLocationReceiverHandlerThread.getLooper();
    }
    
    String getEnrouteMapScheme() {
        return "hybrid.night";
    }
    
    public com.here.android.mpa.common.OnEngineInitListener$Error getError() {
        return this.mapError;
    }
    
    public com.here.android.mpa.common.GeoPosition getLastGeoPosition() {
        return this.lastGeoPosition;
    }
    
    public com.navdy.hud.app.maps.here.HereLocationFixManager getLocationFixManager() {
        return this.hereLocationFixManager;
    }
    
    public com.navdy.hud.app.maps.here.HereMapAnimator getMapAnimator() {
        return this.hereMapAnimator;
    }
    
    public com.navdy.hud.app.maps.here.HereMapController getMapController() {
        return this.mapController;
    }
    
    public int getMapPackageCount() {
        return this.mapPackageCount;
    }
    
    public com.here.android.mpa.mapping.MapView getMapView() {
        return this.mapView;
    }
    
    public String getOfflineMapsData() {
        String s = null;
        try {
            java.io.File a = new java.io.File(com.navdy.hud.app.storage.PathManager.getInstance().getHereMapsDataDirectory());
            if (a.exists()) {
                java.io.File a0 = new java.io.File(a, "meta.json");
                boolean b = a0.exists();
                s = null;
                if (b) {
                    s = com.navdy.service.library.util.IOUtils.convertFileToString(a0.getAbsolutePath());
                }
            } else {
                s = null;
            }
        } catch(Throwable a1) {
            sLogger.e(a1);
            s = null;
        }
        return s;
    }
    
    public com.navdy.hud.app.maps.here.HereOfflineMapsVersion getOfflineMapsVersion() {
        return this.offlineMapsVersion;
    }
    
    public com.here.android.mpa.common.PositioningManager getPositioningManager() {
        return this.positioningManager;
    }
    
    public com.navdy.hud.app.maps.notification.RouteCalculationEventHandler getRouteCalcEventHandler() {
        return this.routeCalcEventHandler;
    }
    
    public com.here.android.mpa.routing.RouteOptions getRouteOptions() {
        com.here.android.mpa.routing.RouteOptions a = new com.here.android.mpa.routing.RouteOptions();
        a.setTransportMode(com.here.android.mpa.routing.RouteOptions$TransportMode.CAR);
        com.navdy.service.library.events.preferences.NavigationPreferences a0 = this.mDriverProfileManager.getCurrentProfile().getNavigationPreferences();
        switch(com.navdy.hud.app.maps.here.HereMapsManager$14.$SwitchMap$com$navdy$service$library$events$preferences$NavigationPreferences$RoutingType[a0.routingType.ordinal()]) {
            case 2: {
                a.setRouteType(com.here.android.mpa.routing.RouteOptions$Type.SHORTEST);
                break;
            }
            case 1: {
                a.setRouteType(com.here.android.mpa.routing.RouteOptions$Type.FASTEST);
                break;
            }
        }
        a.setHighwaysAllowed(Boolean.TRUE.equals(a0.allowHighways));
        a.setTollRoadsAllowed(Boolean.TRUE.equals(a0.allowTollRoads));
        a.setFerriesAllowed(false);
        a.setTunnelsAllowed(Boolean.TRUE.equals(a0.allowTunnels));
        a.setDirtRoadsAllowed(Boolean.TRUE.equals(a0.allowUnpavedRoads));
        a.setCarShuttleTrainsAllowed(Boolean.TRUE.equals(a0.allowAutoTrains));
        a.setCarpoolAllowed(false);
        return a;
    }
    
    public com.here.android.mpa.common.GeoCoordinate getRouteStartPoint() {
        return this.routeStartPoint;
    }
    
    String getTrackingMapScheme() {
        return "carnav.night";
    }
    
    public String getVersion() {
        return (this.isInitialized()) ? com.here.android.mpa.common.Version.getSdkVersion() : null;
    }
    
    public void handleBandwidthPreferenceChange() {
        if (this.isInitialized()) {
            com.navdy.hud.app.maps.here.HereNavigationManager.getInstance().setBandwidthPreferences();
        }
    }
    
    public void hideTraffic() {
        if (this.isInitialized()) {
            if (com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getDriverProfileManager().isTrafficEnabled()) {
                com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.maps.here.HereMapsManager$12(this), 3);
            } else {
                sLogger.v("hidetraffic: traffic is not enabled");
            }
        }
    }
    
    public void initNavigation() {
        if (this.isInitialized()) {
            com.navdy.hud.app.maps.here.HereNavigationManager.getInstance().getNavController().initialize();
        }
    }
    
    public void installPositionListener() {
        synchronized(this) {
            if (!this.positioningManagerInstalled) {
                this.positioningManager.addListener(new java.lang.ref.WeakReference(this.positionChangedListener));
                sLogger.v("position manager listener installed");
                this.positioningManagerInstalled = true;
            }
        }
        /*monexit(this)*/;
    }
    
    public boolean isEngineOnline() {
        boolean b = false;
        boolean b0 = this.isInitialized();
        label2: {
            label0: {
                label1: {
                    if (!b0) {
                        break label1;
                    }
                    if (this.mapEngine == null) {
                        break label1;
                    }
                    if (com.here.android.mpa.common.MapEngine.isOnlineEnabled()) {
                        break label0;
                    }
                }
                b = false;
                break label2;
            }
            b = true;
        }
        return b;
    }
    
    public boolean isInitialized() {
        boolean b = false;
        synchronized(this.initLock) {
            b = this.mapInitialized;
            /*monexit(a)*/;
        }
        return b;
    }
    
    public boolean isInitializing() {
        boolean b = false;
        synchronized(this.initLock) {
            b = this.mapInitializing;
            /*monexit(a)*/;
        }
        return b;
    }
    
    public boolean isMapDataVerified() {
        return this.mapDataVerified;
    }
    
    public boolean isNavigationModeOn() {
        return this.isInitialized() && com.navdy.hud.app.maps.here.HereNavigationManager.getInstance().isNavigationModeOn();
    }
    
    public boolean isNavigationPossible() {
        return this.isInitialized() && this.hereLocationFixManager.getLastGeoCoordinate() != null;
    }
    
    public boolean isRecalcCurrentRouteForTrafficEnabled() {
        return recalcCurrentRouteForTraffic;
    }
    
    public boolean isRenderingAllowed() {
        return !this.powerManager.inQuietMode();
    }
    
    public boolean isVoiceSkinsLoaded() {
        return this.voiceSkinsLoaded;
    }
    
    public void onLinkPropertiesChanged(com.navdy.service.library.events.connection.LinkPropertiesChanged a) {
        if (a != null && a.bandwidthLevel != null) {
            sLogger.d("Link Properties changed");
            if (a.bandwidthLevel.intValue() > 0) {
                sLogger.d("Switching back to normal bandwidth mode");
                this.lowBandwidthDetected = false;
                this.updateEngineOnlineState();
            } else {
                sLogger.d("Switching to low bandwidth mode");
                this.lowBandwidthDetected = true;
                this.updateEngineOnlineState();
            }
        }
    }
    
    public void onWakeup(com.navdy.hud.app.event.Wakeup a) {
        if (this.isInitialized()) {
            this.startTrafficUpdater();
        }
    }
    
    public void postManeuverDisplay() {
        if (this.isInitialized()) {
            com.navdy.hud.app.maps.here.HereNavigationManager.getInstance().postManeuverDisplay();
        }
    }
    
    public void requestMapsEngineOnlineStateChange(boolean b) {
        sLogger.d(new StringBuilder().append("Request to set online state ").append(b).toString());
        this.turnEngineOn = b;
        this.updateEngineOnlineState();
    }
    
    public void setMapView(com.here.android.mpa.mapping.MapView a, com.navdy.hud.app.ui.component.homescreen.NavigationView a0) {
        this.mapView = a;
        this.navigationView = a0;
        com.navdy.hud.app.maps.here.HereNavigationManager.getInstance().setMapView(a, a0);
        com.navdy.hud.app.maps.here.HereMapAnimator$AnimationMode a1 = this.hereMapAnimator.getAnimationMode();
        if (a1 == com.navdy.hud.app.maps.here.HereMapAnimator$AnimationMode.NONE) {
            sLogger.v(new StringBuilder().append("not installing map render listener:").append(a1).toString());
        } else {
            sLogger.v(new StringBuilder().append("installing map render listener:").append(a1).toString());
            this.mapView.addOnMapRenderListener(this.hereMapAnimator.getMapRenderListener());
        }
    }
    
    public void setRouteStartPoint(com.here.android.mpa.common.GeoCoordinate a) {
        this.routeStartPoint = a;
    }
    
    public void setTrafficOverlay(com.navdy.hud.app.maps.NavigationMode a) {
        if (a != com.navdy.hud.app.maps.NavigationMode.MAP) {
            if (a == com.navdy.hud.app.maps.NavigationMode.MAP_ON_ROUTE) {
                this.setMapOnRouteTraffic();
            }
        } else {
            this.setMapTraffic();
        }
    }
    
    public void setVoiceSkinsLoaded() {
        this.voiceSkinsLoaded = true;
    }
    
    public void showTraffic() {
        if (this.isInitialized()) {
            if (com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getDriverProfileManager().isTrafficEnabled()) {
                com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.maps.here.HereMapsManager$13(this), 3);
            } else {
                sLogger.v("showTraffic: traffic is not enabled");
            }
        }
    }
    
    public void startMapRendering() {
        if (this.hereMapAnimator == null) {
            this.initialMapRendering = true;
        } else {
            this.hereMapAnimator.startMapRendering();
        }
    }
    
    public void stopMapRendering() {
        if (this.hereMapAnimator == null) {
            this.initialMapRendering = false;
        } else {
            this.hereMapAnimator.stopMapRendering();
        }
    }
    
    public void turnOffline() {
        synchronized(this) {
            if (this.isInitialized()) {
                this.requestMapsEngineOnlineStateChange(false);
                com.navdy.hud.app.maps.here.HereNavigationManager.getInstance().stopTrafficRerouteListener();
                sLogger.v(new StringBuilder().append("isOnline:").append(com.here.android.mpa.common.MapEngine.isOnlineEnabled()).toString());
            } else {
                sLogger.i("turnOffline: engine not yet initialized");
            }
        }
        /*monexit(this)*/;
    }
    
    public void turnOnline() {
        synchronized(this) {
            if (this.isInitialized()) {
                sLogger.i("turnOnline: enabling traffic info");
                this.requestMapsEngineOnlineStateChange(true);
                if (com.navdy.hud.app.framework.glance.GlanceHelper.isTrafficNotificationEnabled()) {
                    sLogger.i("turnOnline: enabling traffic notifications");
                    com.navdy.hud.app.maps.here.HereNavigationManager.getInstance().startTrafficRerouteListener();
                } else {
                    sLogger.i("turnOnline: not enabling traffic notifications, glance off");
                }
                sLogger.v("turnOnline invokeMapLoader");
                this.invokeMapLoader();
                sLogger.v(new StringBuilder().append("isOnline:").append(com.here.android.mpa.common.MapEngine.isOnlineEnabled()).toString());
            } else {
                sLogger.i("turnOnline: engine not yet initialized");
            }
        }
        /*monexit(this)*/;
    }
    
    public void updateEngineOnlineState() {
        synchronized(this) {
            com.navdy.service.library.log.Logger a = sLogger;
            a.d(new StringBuilder().append("Updating the engine online state LowBandwidthDetected :").append(this.lowBandwidthDetected).append(", TurnEngineOn :").append(this.turnEngineOn).toString());
            if (this.isInitialized()) {
                boolean b = false;
                boolean b0 = false;
                com.navdy.service.library.log.Logger a0 = sLogger;
                StringBuilder a1 = new StringBuilder().append("Turning engine on :");
                boolean b1 = this.lowBandwidthDetected;
                label5: {
                    label3: {
                        label4: {
                            if (b1) {
                                break label4;
                            }
                            if (this.turnEngineOn) {
                                break label3;
                            }
                        }
                        b = false;
                        break label5;
                    }
                    b = true;
                }
                a0.d(a1.append(b).toString());
                boolean b2 = this.lowBandwidthDetected;
                label2: {
                    label0: {
                        label1: {
                            if (b2) {
                                break label1;
                            }
                            if (this.turnEngineOn) {
                                break label0;
                            }
                        }
                        b0 = false;
                        break label2;
                    }
                    b0 = true;
                }
                com.here.android.mpa.common.MapEngine.setOnline(b0);
            }
        }
        /*monexit(this)*/;
    }
}
