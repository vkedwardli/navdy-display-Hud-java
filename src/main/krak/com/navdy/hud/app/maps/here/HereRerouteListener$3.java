package com.navdy.hud.app.maps.here;

class HereRerouteListener$3 implements Runnable {
    final com.navdy.hud.app.maps.here.HereRerouteListener this$0;
    
    HereRerouteListener$3(com.navdy.hud.app.maps.here.HereRerouteListener a) {
        super();
        this.this$0 = a;
    }
    
    public void run() {
        com.navdy.hud.app.maps.here.HereRerouteListener.access$402(this.this$0, false);
        com.navdy.hud.app.maps.here.HereRerouteListener.access$002(this.this$0, (String)null);
        com.navdy.hud.app.maps.here.HereRerouteListener.access$300(this.this$0).w(new StringBuilder().append(com.navdy.hud.app.maps.here.HereRerouteListener.access$200(this.this$0)).append(" reroute-failed").toString());
        if (com.navdy.hud.app.maps.here.HereRerouteListener.access$100(this.this$0).getNavigationSessionPreference().spokenTurnByTurn && !com.navdy.hud.app.maps.here.HereRerouteListener.access$100(this.this$0).hasArrived() && !com.navdy.hud.app.maps.MapSettings.isTtsRecalculationDisabled()) {
            com.navdy.hud.app.maps.here.HereRerouteListener.access$500(this.this$0).post(com.navdy.hud.app.maps.here.HereRerouteListener.access$100(this.this$0).TTS_REROUTING_FAILED);
        }
        com.navdy.hud.app.maps.here.HereRerouteListener.access$500(this.this$0).post(com.navdy.hud.app.maps.here.HereRerouteListener.REROUTE_FAILED);
    }
}
