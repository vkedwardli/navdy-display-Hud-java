package com.navdy.hud.app.maps.here;

class HereMapCameraManager$8$1 implements Runnable {
    final com.navdy.hud.app.maps.here.HereMapCameraManager$8 this$1;
    
    HereMapCameraManager$8$1(com.navdy.hud.app.maps.here.HereMapCameraManager$8 a) {
        super();
        this.this$1 = a;
    }
    
    public void run() {
        com.here.android.mpa.common.GeoCoordinate a = (com.navdy.hud.app.maps.here.HereMapCameraManager.access$1300(this.this$1.this$0) != null) ? com.navdy.hud.app.maps.here.HereMapCameraManager.access$1300(this.this$1.this$0).getCoordinate() : com.navdy.hud.app.maps.here.HereMapCameraManager.access$300(this.this$1.this$0).getCenter();
        double d = com.navdy.hud.app.maps.here.HereMapCameraManager.access$300(this.this$1.this$0).getZoomLevel() + this.this$1.val$zoomStep;
        float f = com.navdy.hud.app.maps.here.HereMapCameraManager.access$300(this.this$1.this$0).getTilt() + this.this$1.val$tiltStep;
        if (d > 16.5) {
            d = 16.5;
        }
        if (f > 70f) {
            f = 70f;
        }
        com.navdy.hud.app.maps.here.HereMapCameraManager.access$300(this.this$1.this$0).setCenter(a, com.here.android.mpa.mapping.Map$Animation.NONE, d, -1f, f);
        com.navdy.hud.app.maps.here.HereMapCameraManager.access$2208(this.this$1.this$0);
        if (com.navdy.hud.app.maps.here.HereMapCameraManager.access$2200(this.this$1.this$0) != 5) {
            com.navdy.hud.app.maps.here.HereMapCameraManager.access$100(this.this$1.this$0).postDelayed(com.navdy.hud.app.maps.here.HereMapCameraManager.access$2300(this.this$1.this$0, this.this$1.val$zoomStep, this.this$1.val$tiltStep), 200L);
        } else {
            com.navdy.hud.app.maps.here.HereMapCameraManager.access$1402(this.this$1.this$0, false);
            com.navdy.hud.app.maps.here.HereMapCameraManager.access$2202(this.this$1.this$0, 0);
        }
    }
}
