package com.navdy.hud.app.maps.notification;

class TrafficNotificationManager$2 implements Runnable {
    final com.navdy.hud.app.maps.notification.TrafficNotificationManager this$0;
    
    TrafficNotificationManager$2(com.navdy.hud.app.maps.notification.TrafficNotificationManager a) {
        super();
        this.this$0 = a;
    }
    
    public void run() {
        try {
            com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getUiStateManager().getHomescreenView().ejectRightSection();
            com.navdy.hud.app.maps.notification.TrafficNotificationManager.sLogger.i("junction view mode Ejected");
        } catch(Throwable a) {
            com.navdy.hud.app.maps.notification.TrafficNotificationManager.sLogger.e("junction view mode", a);
        }
    }
}
