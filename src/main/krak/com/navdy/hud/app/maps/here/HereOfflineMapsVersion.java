package com.navdy.hud.app.maps.here;

public class HereOfflineMapsVersion {
    final static com.navdy.service.library.log.Logger sLogger;
    private java.util.Date date;
    private java.util.List packages;
    private String rawDate;
    private String version;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.maps.here.HereOfflineMapsVersion.class);
    }
    
    public HereOfflineMapsVersion(String s) {
        try {
            org.json.JSONObject a = new org.json.JSONObject(s);
            this.rawDate = a.getString("version");
            this.date = this.parseMapDate(this.rawDate);
            this.version = a.getString("here_map_version");
            org.json.JSONArray a0 = a.getJSONArray("map_packages");
            int i = a0.length();
            this.packages = (java.util.List)new java.util.ArrayList(i);
            int i0 = 0;
            while(i0 < i) {
                this.packages.add(a0.getString(i0));
                i0 = i0 + 1;
            }
        } catch(Exception a1) {
            sLogger.e("Failed to parse off-line maps version info", (Throwable)a1);
        }
    }
    
    private java.util.Date parseMapDate(String s) {
        java.util.Date a = null;
        java.util.StringTokenizer a0 = new java.util.StringTokenizer(s, ".");
        int i = -1;
        int i0 = -1;
        int i1 = -1;
        while(a0.hasMoreElements()) {
            String s0 = a0.nextToken();
            if (i != -1) {
                if (i0 != -1) {
                    i1 = Integer.parseInt(s0);
                } else {
                    i0 = Integer.parseInt(s0);
                }
            } else {
                i = Integer.parseInt(s0);
            }
        }
        label2: {
            label0: {
                label1: {
                    if (i == -1) {
                        break label1;
                    }
                    if (i0 == -1) {
                        break label1;
                    }
                    if (i1 != -1) {
                        break label0;
                    }
                }
                a = null;
                break label2;
            }
            java.util.Calendar a1 = java.util.Calendar.getInstance();
            a1.set(1, i);
            a1.set(2, i0 - 1);
            a1.set(5, i1);
            a = a1.getTime();
        }
        return a;
    }
    
    public java.util.Date getDate() {
        return this.date;
    }
    
    public java.util.List getPackages() {
        return this.packages;
    }
    
    public String getRawDate() {
        return this.rawDate;
    }
    
    public String getVersion() {
        return this.version;
    }
}
