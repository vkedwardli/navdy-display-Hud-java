package com.navdy.hud.app.maps;


public enum MapEvents$TrafficRerouteAction {
    REROUTE(0),
    DISMISS(1);

    private int value;
    MapEvents$TrafficRerouteAction(int value) {
        this.value = value;
    }
    public int getValue() {
        return value;
    }
}

//final public class MapEvents$TrafficRerouteAction extends Enum {
//    final private static com.navdy.hud.app.maps.MapEvents$TrafficRerouteAction[] $VALUES;
//    final public static com.navdy.hud.app.maps.MapEvents$TrafficRerouteAction DISMISS;
//    final public static com.navdy.hud.app.maps.MapEvents$TrafficRerouteAction REROUTE;
//    
//    static {
//        REROUTE = new com.navdy.hud.app.maps.MapEvents$TrafficRerouteAction("REROUTE", 0);
//        DISMISS = new com.navdy.hud.app.maps.MapEvents$TrafficRerouteAction("DISMISS", 1);
//        com.navdy.hud.app.maps.MapEvents$TrafficRerouteAction[] a = new com.navdy.hud.app.maps.MapEvents$TrafficRerouteAction[2];
//        a[0] = REROUTE;
//        a[1] = DISMISS;
//        $VALUES = a;
//    }
//    
//    private MapEvents$TrafficRerouteAction(String s, int i) {
//        super(s, i);
//    }
//    
//    public static com.navdy.hud.app.maps.MapEvents$TrafficRerouteAction valueOf(String s) {
//        return (com.navdy.hud.app.maps.MapEvents$TrafficRerouteAction)Enum.valueOf(com.navdy.hud.app.maps.MapEvents$TrafficRerouteAction.class, s);
//    }
//    
//    public static com.navdy.hud.app.maps.MapEvents$TrafficRerouteAction[] values() {
//        return $VALUES.clone();
//    }
//}
//