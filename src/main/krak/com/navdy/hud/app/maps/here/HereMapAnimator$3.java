package com.navdy.hud.app.maps.here;

class HereMapAnimator$3 implements Runnable {
    final com.navdy.hud.app.maps.here.HereMapAnimator this$0;
    final double val$zoom;
    
    HereMapAnimator$3(com.navdy.hud.app.maps.here.HereMapAnimator a, double d) {
        super();
        this.this$0 = a;
        this.val$zoom = d;
    }
    
    public void run() {
        Object a = null;
        Throwable a0 = null;
        long j = (com.navdy.hud.app.maps.here.HereMapAnimator.access$1000(this.this$0) <= 0L) ? 0L : android.os.SystemClock.elapsedRealtime() - com.navdy.hud.app.maps.here.HereMapAnimator.access$1000(this.this$0);
        boolean b = com.navdy.hud.app.maps.here.HereMapAnimator.access$1100(this.this$0, this.val$zoom);
        label0: {
            if (b) {
                com.navdy.hud.app.maps.here.HereMapAnimator.access$1002(this.this$0, android.os.SystemClock.elapsedRealtime());
                synchronized(com.navdy.hud.app.maps.here.HereMapAnimator.access$1200()) {
                    com.navdy.hud.app.maps.here.HereMapAnimator.access$1302(this.this$0, j);
                    com.navdy.hud.app.maps.here.HereMapAnimator.access$1402(this.this$0, this.val$zoom);
                    /*monexit(a)*/;
                }
            } else {
                com.navdy.hud.app.maps.here.HereMapAnimator.access$400().v(new StringBuilder().append("filtering out spurious zoom: ").append(this.val$zoom).toString());
            }
            return;
        }

    }
}
