package com.navdy.hud.app.maps.here;

class HereMapController$2 implements Runnable {
    final com.navdy.hud.app.maps.here.HereMapController this$0;
    final double val$zoomLevel;
    
    HereMapController$2(com.navdy.hud.app.maps.here.HereMapController a, double d) {
        super();
        this.this$0 = a;
        this.val$zoomLevel = d;
    }
    
    public void run() {
        try {
            com.navdy.hud.app.maps.here.HereMapController.access$000(this.this$0).setZoomLevel(this.val$zoomLevel);
        } catch(Throwable a) {
            com.navdy.hud.app.maps.here.HereMapController.access$100().e(a);
        }
    }
}
