package com.navdy.hud.app.maps.here;

class HereManeuverDisplayBuilder$1 {
    final static int[] $SwitchMap$com$here$android$mpa$routing$Maneuver$Turn;
    final static int[] $SwitchMap$com$navdy$hud$app$manager$SpeedManager$SpeedUnit;
    final static int[] $SwitchMap$com$navdy$hud$app$maps$MapEvents$DestinationDirection;
    final static int[] $SwitchMap$com$navdy$hud$app$maps$here$HereManeuverDisplayBuilder$ManeuverState;
    final static int[] $SwitchMap$com$navdy$service$library$events$navigation$DistanceUnit;
    
    static {
        $SwitchMap$com$navdy$service$library$events$navigation$DistanceUnit = new int[com.navdy.service.library.events.navigation.DistanceUnit.values().length];
        int[] a = $SwitchMap$com$navdy$service$library$events$navigation$DistanceUnit;
        com.navdy.service.library.events.navigation.DistanceUnit a0 = com.navdy.service.library.events.navigation.DistanceUnit.DISTANCE_MILES;
        try {
            a[a0.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$navigation$DistanceUnit[com.navdy.service.library.events.navigation.DistanceUnit.DISTANCE_FEET.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException0) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$navigation$DistanceUnit[com.navdy.service.library.events.navigation.DistanceUnit.DISTANCE_KMS.ordinal()] = 3;
        } catch(NoSuchFieldError ignoredException1) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$navigation$DistanceUnit[com.navdy.service.library.events.navigation.DistanceUnit.DISTANCE_METERS.ordinal()] = 4;
        } catch(NoSuchFieldError ignoredException2) {
        }
        $SwitchMap$com$navdy$hud$app$manager$SpeedManager$SpeedUnit = new int[com.navdy.hud.app.manager.SpeedManager$SpeedUnit.values().length];
        int[] a1 = $SwitchMap$com$navdy$hud$app$manager$SpeedManager$SpeedUnit;
        com.navdy.hud.app.manager.SpeedManager$SpeedUnit a2 = com.navdy.hud.app.manager.SpeedManager$SpeedUnit.MILES_PER_HOUR;
        try {
            a1[a2.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException3) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$manager$SpeedManager$SpeedUnit[com.navdy.hud.app.manager.SpeedManager$SpeedUnit.KILOMETERS_PER_HOUR.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException4) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$manager$SpeedManager$SpeedUnit[com.navdy.hud.app.manager.SpeedManager$SpeedUnit.METERS_PER_SECOND.ordinal()] = 3;
        } catch(NoSuchFieldError ignoredException5) {
        }
        $SwitchMap$com$navdy$hud$app$maps$here$HereManeuverDisplayBuilder$ManeuverState = new int[com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder$ManeuverState.values().length];
        int[] a3 = $SwitchMap$com$navdy$hud$app$maps$here$HereManeuverDisplayBuilder$ManeuverState;
        com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder$ManeuverState a4 = com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder$ManeuverState.STAY;
        try {
            a3[a4.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException6) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$maps$here$HereManeuverDisplayBuilder$ManeuverState[com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder$ManeuverState.NEXT.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException7) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$maps$here$HereManeuverDisplayBuilder$ManeuverState[com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder$ManeuverState.SOON.ordinal()] = 3;
        } catch(NoSuchFieldError ignoredException8) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$maps$here$HereManeuverDisplayBuilder$ManeuverState[com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder$ManeuverState.NOW.ordinal()] = 4;
        } catch(NoSuchFieldError ignoredException9) {
        }
        $SwitchMap$com$navdy$hud$app$maps$MapEvents$DestinationDirection = new int[com.navdy.hud.app.maps.MapEvents$DestinationDirection.values().length];
        int[] a5 = $SwitchMap$com$navdy$hud$app$maps$MapEvents$DestinationDirection;
        com.navdy.hud.app.maps.MapEvents$DestinationDirection a6 = com.navdy.hud.app.maps.MapEvents$DestinationDirection.LEFT;
        try {
            a5[a6.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException10) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$maps$MapEvents$DestinationDirection[com.navdy.hud.app.maps.MapEvents$DestinationDirection.RIGHT.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException11) {
        }
        $SwitchMap$com$here$android$mpa$routing$Maneuver$Turn = new int[com.here.android.mpa.routing.Maneuver$Turn.values().length];
        int[] a7 = $SwitchMap$com$here$android$mpa$routing$Maneuver$Turn;
        com.here.android.mpa.routing.Maneuver$Turn a8 = com.here.android.mpa.routing.Maneuver$Turn.QUITE_LEFT;
        try {
            a7[a8.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException12) {
        }
        try {
            $SwitchMap$com$here$android$mpa$routing$Maneuver$Turn[com.here.android.mpa.routing.Maneuver$Turn.QUITE_RIGHT.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException13) {
        }
        try {
            $SwitchMap$com$here$android$mpa$routing$Maneuver$Turn[com.here.android.mpa.routing.Maneuver$Turn.HEAVY_LEFT.ordinal()] = 3;
        } catch(NoSuchFieldError ignoredException14) {
        }
        try {
            $SwitchMap$com$here$android$mpa$routing$Maneuver$Turn[com.here.android.mpa.routing.Maneuver$Turn.HEAVY_RIGHT.ordinal()] = 4;
        } catch(NoSuchFieldError ignoredException15) {
        }
        try {
            $SwitchMap$com$here$android$mpa$routing$Maneuver$Turn[com.here.android.mpa.routing.Maneuver$Turn.KEEP_RIGHT.ordinal()] = 5;
        } catch(NoSuchFieldError ignoredException16) {
        }
        try {
            $SwitchMap$com$here$android$mpa$routing$Maneuver$Turn[com.here.android.mpa.routing.Maneuver$Turn.KEEP_MIDDLE.ordinal()] = 6;
        } catch(NoSuchFieldError ignoredException17) {
        }
        try {
            $SwitchMap$com$here$android$mpa$routing$Maneuver$Turn[com.here.android.mpa.routing.Maneuver$Turn.KEEP_LEFT.ordinal()] = 7;
        } catch(NoSuchFieldError ignoredException18) {
        }
    }
}
