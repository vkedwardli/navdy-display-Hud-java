package com.navdy.hud.app.maps.widget;
import com.navdy.hud.app.R;

class TrafficIncidentWidgetPresenter$1$1 implements Runnable {
    final com.navdy.hud.app.maps.widget.TrafficIncidentWidgetPresenter$1 this$1;
    final android.text.SpannableStringBuilder val$builder;
    
    TrafficIncidentWidgetPresenter$1$1(com.navdy.hud.app.maps.widget.TrafficIncidentWidgetPresenter$1 a, android.text.SpannableStringBuilder a0) {
        super();
        this.this$1 = a;
        this.val$builder = a0;
    }
    
    public void run() {
        if (com.navdy.hud.app.maps.widget.TrafficIncidentWidgetPresenter.access$000(this.this$1.this$0) != null) {
            com.navdy.hud.app.maps.widget.TrafficIncidentWidgetPresenter.access$000(this.this$1.this$0).setTextAppearance(com.navdy.hud.app.HudApplication.getAppContext(), R.style.incident_2);
            com.navdy.hud.app.maps.widget.TrafficIncidentWidgetPresenter.access$000(this.this$1.this$0).setText((CharSequence)this.val$builder);
        }
    }
}
