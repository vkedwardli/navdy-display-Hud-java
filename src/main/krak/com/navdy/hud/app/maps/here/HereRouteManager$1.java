package com.navdy.hud.app.maps.here;
import com.navdy.hud.app.R;

final class HereRouteManager$1 implements Runnable {
    final com.navdy.service.library.events.navigation.NavigationRouteRequest val$routeRequest;
    
    HereRouteManager$1(com.navdy.service.library.events.navigation.NavigationRouteRequest a) {
        super();
        this.val$routeRequest = a;
    }
    
    public void run() {
        label0: try {
            Object a = null;
            Throwable a0 = null;
            com.navdy.hud.app.maps.here.HereRouteManager.printRouteRequest(this.val$routeRequest);
            label1: if (com.navdy.hud.app.maps.here.HereRouteManager.access$000().isInitialized()) {
                com.navdy.hud.app.maps.here.HereLocationFixManager a1 = com.navdy.hud.app.maps.here.HereRouteManager.access$000().getLocationFixManager();
                if (a1 != null) {
                    if (com.navdy.hud.app.framework.network.NetworkStateManager.getInstance().isNetworkStateInitialized()) {
                        com.here.android.mpa.common.GeoCoordinate a2 = com.navdy.hud.app.maps.here.HereRouteManager.access$000().getRouteStartPoint();
                        if (a2 != null) {
                            com.navdy.hud.app.maps.here.HereRouteManager.access$100().v(new StringBuilder().append("using debug start point:").append(a2).toString());
                        } else {
                            a2 = a1.getLastGeoCoordinate();
                        }
                        if (a2 != null) {
                            if (this.val$routeRequest.requestId != null) {
                                com.navdy.hud.app.maps.here.HereRouteManager.access$100().i("reset traffic reroute listener");
                                com.navdy.hud.app.maps.here.HereNavigationManager.getInstance().resetTrafficRerouteListener();
                                label3: synchronized(com.navdy.hud.app.maps.here.HereRouteManager.access$400()) {
                                    String s = null;
                                    com.navdy.service.library.events.navigation.NavigationRouteRequest a3 = com.navdy.hud.app.maps.here.HereRouteManager.access$500();
                                    label5: {
                                        label6: {
                                            if (a3 != null) {
                                                break label6;
                                            }
                                            boolean b = com.navdy.hud.app.maps.here.HereRouteManager.isUIShowingRouteCalculation();
                                            s = null;
                                            if (!b) {
                                                break label5;
                                            }
                                        }
                                        boolean b0 = com.navdy.hud.app.maps.here.HereMapUtil.isSavedRoute(this.val$routeRequest);
                                        label4: {
                                            if (!b0) {
                                                break label4;
                                            }
                                            com.navdy.hud.app.maps.here.HereRouteManager.access$100().i("saved route cannot override an existing route calculation");
                                            com.navdy.hud.app.maps.here.HereRouteManager.access$300(com.navdy.service.library.events.RequestStatus.REQUEST_ALREADY_IN_PROGRESS, this.val$routeRequest, (String)null);
                                            /*monexit(a)*/;
                                            break label0;
                                        }
                                        com.navdy.service.library.events.navigation.NavigationRouteRequest a4 = com.navdy.hud.app.maps.here.HereRouteManager.access$500();
                                        s = null;
                                        if (a4 != null) {
                                            s = com.navdy.hud.app.maps.here.HereRouteManager.access$500().requestId;
                                        }
                                    }
                                    boolean b1 = com.navdy.hud.app.maps.here.HereRouteManager.access$600().contains(this.val$routeRequest.requestId);
                                    label2: {
                                        if (b1) {
                                            break label2;
                                        }
                                        if (android.text.TextUtils.equals((CharSequence)this.val$routeRequest.requestId, (CharSequence)s)) {
                                            break label2;
                                        }
                                        /*monexit(a)*/;
                                        break label3;
                                    }
                                    com.navdy.hud.app.maps.here.HereRouteManager.access$100().v(new StringBuilder().append("route request[").append(this.val$routeRequest.requestId).append("] already received").toString());
                                    /*monexit(a)*/;
                                    break label0;
                                }
                                com.navdy.hud.app.maps.here.HereNavigationManager a6 = com.navdy.hud.app.maps.here.HereNavigationManager.getInstance();
                                if (a6.hasArrived()) {
                                    com.navdy.hud.app.maps.here.HereRouteManager.access$100().v("stop arrival mode");
                                    a6.setIgnoreArrived(true);
                                    a6.stopNavigation();
                                }
                                com.navdy.hud.app.maps.here.HereRouteManager.access$700(this.val$routeRequest, a2);
                                break label0;
                            } else {
                                com.navdy.hud.app.maps.here.HereRouteManager.access$100().i("no request id specified");
                                com.navdy.hud.app.maps.here.HereRouteManager.access$300(com.navdy.service.library.events.RequestStatus.REQUEST_INVALID_REQUEST, this.val$routeRequest, "no route id");
                                break label0;
                            }
                        } else {
                            com.navdy.hud.app.maps.here.HereRouteManager.access$100().i("start location n/a");
                            com.navdy.hud.app.maps.here.HereRouteManager.access$300(com.navdy.service.library.events.RequestStatus.REQUEST_NO_LOCATION_SERVICE, this.val$routeRequest, com.navdy.hud.app.maps.here.HereRouteManager.access$200().getString(R.string.no_current_position));
                            break label0;
                        }
                    } else {
                        com.navdy.hud.app.maps.here.HereRouteManager.access$100().i("network state is not initialized yet");
                        com.navdy.hud.app.maps.here.HereRouteManager.access$300(com.navdy.service.library.events.RequestStatus.REQUEST_NOT_READY, this.val$routeRequest, com.navdy.hud.app.maps.here.HereRouteManager.access$200().getString(R.string.network_not_initialized));
                        break label0;
                    }
                } else {
                    com.navdy.hud.app.maps.here.HereRouteManager.access$100().i("engine not ready, location fix manager n/a");
                    com.navdy.hud.app.maps.here.HereRouteManager.access$300(com.navdy.service.library.events.RequestStatus.REQUEST_NOT_READY, this.val$routeRequest, com.navdy.hud.app.maps.here.HereRouteManager.access$200().getString(R.string.map_engine_not_ready));
                    break label0;
                }
            } else {
                com.navdy.hud.app.maps.here.HereRouteManager.access$100().i("engine not ready");
                com.navdy.hud.app.maps.here.HereRouteManager.access$300(com.navdy.service.library.events.RequestStatus.REQUEST_NOT_READY, this.val$routeRequest, com.navdy.hud.app.maps.here.HereRouteManager.access$200().getString(R.string.map_engine_not_ready));
                break label0;
            }

        } catch(Throwable a9) {
            com.navdy.hud.app.maps.here.HereRouteManager.access$100().e("", a9);
            com.navdy.hud.app.maps.here.HereRouteManager.access$300(com.navdy.service.library.events.RequestStatus.REQUEST_UNKNOWN_ERROR, this.val$routeRequest, com.navdy.hud.app.maps.here.HereRouteManager.access$200().getString(R.string.autoCompleteSearch));
        }
    }
}
