package com.navdy.hud.app.maps.here;

class HereLaneInfoListener$3 implements Runnable {
    final com.navdy.hud.app.maps.here.HereLaneInfoListener this$0;
    final java.util.List val$laneInfoList;
    
    HereLaneInfoListener$3(com.navdy.hud.app.maps.here.HereLaneInfoListener a, java.util.List a0) {
        super();
        this.this$0 = a;
        this.val$laneInfoList = a0;
    }
    
    public void run() {
        java.util.List a = this.val$laneInfoList;
        label0: {
            label13: {
                if (a == null) {
                    break label13;
                }
                if (this.val$laneInfoList.size() == 0) {
                    break label13;
                }
                try {
                    int i = this.val$laneInfoList.size();
                    if (com.navdy.hud.app.maps.here.HereLaneInfoListener.access$100().isLoggable(2)) {
                        com.navdy.hud.app.maps.here.HereLaneInfoListener.access$100().v(new StringBuilder().append("onShowLaneInfo: size=").append(i).toString());
                        Object a0 = this.val$laneInfoList.iterator();
                        while(((java.util.Iterator)a0).hasNext()) {
                            com.here.android.mpa.guidance.LaneInformation a1 = (com.here.android.mpa.guidance.LaneInformation)((java.util.Iterator)a0).next();
                            com.here.android.mpa.guidance.LaneInformation$RecommendationState a2 = a1.getRecommendationState();
                            com.navdy.hud.app.maps.here.HereLaneInfoListener.access$100().v(new StringBuilder().append("onShowLaneInfo:recommended state:").append(a2.name()).toString());
                            java.util.EnumSet a3 = a1.getDirections();
                            if (a3 != null) {
                                Object a4 = a3.iterator();
                                while(((java.util.Iterator)a4).hasNext()) {
                                    com.here.android.mpa.guidance.LaneInformation$Direction a5 = (com.here.android.mpa.guidance.LaneInformation$Direction)((java.util.Iterator)a4).next();
                                    com.navdy.hud.app.maps.here.HereLaneInfoListener.access$100().v(new StringBuilder().append("onShowLaneInfo:direction:").append(a5.name()).toString());
                                }
                            }
                        }
                    }
                    int i0 = 0;
                    int i1 = 0;
                    int i2 = 0;
                    while(i2 < i) {
                        com.here.android.mpa.guidance.LaneInformation$RecommendationState a6 = ((com.here.android.mpa.guidance.LaneInformation)this.val$laneInfoList.get(i2)).getRecommendationState();
                        switch(com.navdy.hud.app.maps.here.HereLaneInfoListener$4.$SwitchMap$com$here$android$mpa$guidance$LaneInformation$RecommendationState[a6.ordinal()]) {
                            case 3: {
                                i1 = i1 + 1;
                                break;
                            }
                            case 2: {
                                i0 = i0 + 1;
                                break;
                            }
                            case 1: {
                                break;
                            }
                        }
                        i2 = i2 + 1;
                    }
                    label12: {
                        if (i0 != 0) {
                            break label12;
                        }
                        if (i1 != 0) {
                            break label12;
                        }
                        if (com.navdy.hud.app.maps.here.HereLaneInfoListener.access$200(this.this$0) != null) {
                            com.navdy.hud.app.maps.here.HereLaneInfoListener.access$202(this.this$0, (com.navdy.hud.app.maps.MapEvents$DisplayTrafficLaneInfo)null);
                            com.navdy.hud.app.maps.here.HereLaneInfoListener.access$400(this.this$0).post(com.navdy.hud.app.maps.here.HereLaneInfoListener.access$300());
                        }
                        if (!com.navdy.hud.app.maps.here.HereLaneInfoListener.access$100().isLoggable(2)) {
                            break label0;
                        }
                        com.navdy.hud.app.maps.here.HereLaneInfoListener.access$100().v("Invalid lane data");
                        break label0;
                    }
                    java.util.EnumSet a7 = null;
                    com.here.android.mpa.guidance.LaneInformation$Direction a8 = null;
                    com.here.android.mpa.guidance.LaneInformation$Direction a9 = null;
                    java.util.EnumSet a10 = null;
                    int i3 = 0;
                    boolean b = false;
                    label11: while(true) {
                        java.util.EnumSet a11 = a7;
                        java.util.EnumSet a12 = a10;
                        if (i3 >= i) {
                            break label11;
                        }
                        com.here.android.mpa.guidance.LaneInformation a13 = (com.here.android.mpa.guidance.LaneInformation)this.val$laneInfoList.get(i3);
                        com.here.android.mpa.guidance.LaneInformation$RecommendationState a14 = a13.getRecommendationState();
                        int i4 = com.navdy.hud.app.maps.here.HereLaneInfoListener$4.$SwitchMap$com$here$android$mpa$guidance$LaneInformation$RecommendationState[a14.ordinal()];
                        a7 = a11;
                        a10 = a12;
                        switch(i4) {
                            case 3: {
                                if (a8 != null) {
                                    a10 = a12;
                                    a7 = a11;
                                    break;
                                } else {
                                    a7 = a13.getDirections();
                                    a10 = a12;
                                    if (a7 == null) {
                                        a7 = a11;
                                        break;
                                    } else {
                                        int i5 = a7.size();
                                        a10 = a12;
                                        if (i5 <= 0) {
                                            a7 = a11;
                                            break;
                                        } else {
                                            int i6 = a7.size();
                                            a10 = a12;
                                            if (i6 != 1) {
                                                a10 = a12;
                                                b = true;
                                                break;
                                            } else {
                                                a8 = (com.here.android.mpa.guidance.LaneInformation$Direction)a7.iterator().next();
                                                boolean b0 = com.navdy.hud.app.maps.here.HereLaneInfoListener.access$100().isLoggable(2);
                                                a7 = a11;
                                                if (b0) {
                                                    com.navdy.hud.app.maps.here.HereLaneInfoListener.access$100().v(new StringBuilder().append("onRouteDirection:").append(null).toString());
                                                    break;
                                                } else {
                                                    a7 = a11;
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            case 2: {
                                a7 = a11;
                                if (a9 != null) {
                                    a10 = a12;
                                    a7 = a11;
                                    break;
                                } else {
                                    a10 = a13.getDirections();
                                    if (a10 == null) {
                                        a10 = a12;
                                        break;
                                    } else if (a10.size() <= 0) {
                                        a10 = a12;
                                        break;
                                    } else if (a10.size() != 1) {
                                        b = true;
                                        break;
                                    } else {
                                        a9 = (com.here.android.mpa.guidance.LaneInformation$Direction)a10.iterator().next();
                                        boolean b1 = com.navdy.hud.app.maps.here.HereLaneInfoListener.access$100().isLoggable(2);
                                        a10 = a12;
                                        if (!b1) {
                                            break;
                                        }
                                        com.navdy.hud.app.maps.here.HereLaneInfoListener.access$100().v(new StringBuilder().append("onRouteDirection:").append(null).toString());
                                        a10 = a12;
                                        break;
                                    }
                                }
                            }
                        }
                        i3 = i3 + 1;
                    }
                    label10: if (i0 <= 0) {
                        if (a8 == null) {
                            a8 = null;
                        } else {
                            a7 = null;
                        }
                    } else if (a9 == null) {
                        label8: {
                            label9: {
                                if (a8 == null) {
                                    break label9;
                                }
                                if (a10.contains(a8)) {
                                    break label8;
                                }
                            }
                            a7 = a10;
                            a8 = null;
                            break label10;
                        }
                        a7 = null;
                    } else {
                        a7 = null;
                        a8 = a9;
                    }
                    label6: {
                        if (a8 != null) {
                            break label6;
                        }
                        if (!b) {
                            break label6;
                        }
                        if (com.navdy.hud.app.maps.here.HereLaneInfoListener.access$100().isLoggable(2)) {
                            com.navdy.hud.app.maps.here.HereLaneInfoListener.access$100().v("onRouteDirection is needed, but not available");
                        }
                        a8 = com.navdy.hud.app.maps.here.HereLaneInfoListener.access$500(this.this$0, a7);
                        label7: {
                            if (a8 == null) {
                                break label7;
                            }
                            if (!com.navdy.hud.app.maps.here.HereLaneInfoListener.access$100().isLoggable(2)) {
                                break label6;
                            }
                            com.navdy.hud.app.maps.here.HereLaneInfoListener.access$100().v(new StringBuilder().append("onRouteDirection:got on route direction:").append(a8).toString());
                            break label6;
                        }
                        if (com.navdy.hud.app.maps.here.HereLaneInfoListener.access$100().isLoggable(2)) {
                            com.navdy.hud.app.maps.here.HereLaneInfoListener.access$100().v("onRouteDirection:could not get onroute direction from maneuver");
                        }
                        if (com.navdy.hud.app.maps.here.HereLaneInfoListener.access$200(this.this$0) == null) {
                            break label0;
                        }
                        com.navdy.hud.app.maps.here.HereLaneInfoListener.access$202(this.this$0, (com.navdy.hud.app.maps.MapEvents$DisplayTrafficLaneInfo)null);
                        com.navdy.hud.app.maps.here.HereLaneInfoListener.access$400(this.this$0).post(com.navdy.hud.app.maps.here.HereLaneInfoListener.access$300());
                        break label0;
                    }
                    com.navdy.hud.app.maps.here.HereLaneInfoListener.access$600(this.this$0).clear();
                    int i7 = 0;
                    while(i7 < i) {
                        com.here.android.mpa.guidance.LaneInformation a15 = (com.here.android.mpa.guidance.LaneInformation)this.val$laneInfoList.get(i7);
                        java.util.EnumSet a16 = a15.getDirections();
                        label5: {
                            label3: {
                                label4: {
                                    if (a16 == null) {
                                        break label4;
                                    }
                                    if (a16.size() != 0) {
                                        break label3;
                                    }
                                }
                                com.navdy.hud.app.maps.here.HereLaneInfoListener.access$600(this.this$0).add(com.navdy.hud.app.maps.here.HereLaneInfoListener.access$700());
                                break label5;
                            }
                            com.navdy.hud.app.maps.here.HereLaneInfoListener.access$800(this.this$0).clear();
                            Object a17 = a16.iterator();
                            while(((java.util.Iterator)a17).hasNext()) {
                                com.here.android.mpa.guidance.LaneInformation$Direction a18 = (com.here.android.mpa.guidance.LaneInformation$Direction)((java.util.Iterator)a17).next();
                                if (com.navdy.hud.app.maps.here.HereLaneInfoListener$4.$SwitchMap$com$here$android$mpa$guidance$LaneInformation$Direction[a18.ordinal()] == 0) {
                                    com.navdy.hud.app.maps.here.HereLaneInfoListener.access$800(this.this$0).add(a18);
                                }
                            }
                            if (com.navdy.hud.app.maps.here.HereLaneInfoListener.access$800(this.this$0).size() != 0) {
                                com.here.android.mpa.guidance.LaneInformation$RecommendationState a19 = a15.getRecommendationState();
                                switch(com.navdy.hud.app.maps.here.HereLaneInfoListener$4.$SwitchMap$com$here$android$mpa$guidance$LaneInformation$RecommendationState[a19.ordinal()]) {
                                    case 4: {
                                        com.navdy.hud.app.maps.here.HereLaneInfoListener.access$600(this.this$0).add(new com.navdy.hud.app.maps.MapEvents$LaneData(com.navdy.hud.app.maps.MapEvents$LaneData$Position.OFF_ROUTE, com.navdy.hud.app.maps.here.HereLaneInfoBuilder.getDrawable((java.util.List)com.navdy.hud.app.maps.here.HereLaneInfoListener.access$800(this.this$0), com.navdy.hud.app.maps.MapEvents$LaneData$Position.OFF_ROUTE, (com.here.android.mpa.guidance.LaneInformation$Direction)null)));
                                        break;
                                    }
                                    case 3: {
                                        if (a8 != null && com.navdy.hud.app.maps.here.HereLaneInfoListener.access$800(this.this$0).size() > 1) {
                                            com.navdy.hud.app.maps.here.HereLaneInfoListener.access$900(this.this$0).clear();
                                            Object a20 = com.navdy.hud.app.maps.here.HereLaneInfoListener.access$800(this.this$0).iterator();
                                            while(((java.util.Iterator)a20).hasNext()) {
                                                com.here.android.mpa.guidance.LaneInformation$Direction a21 = (com.here.android.mpa.guidance.LaneInformation$Direction)((java.util.Iterator)a20).next();
                                                if (a21 == a8) {
                                                    com.navdy.hud.app.maps.here.HereLaneInfoListener.access$900(this.this$0).add(a21);
                                                    ((java.util.Iterator)a20).remove();
                                                }
                                            }
                                            com.navdy.hud.app.maps.here.HereLaneInfoListener.access$800(this.this$0).addAll((java.util.Collection)com.navdy.hud.app.maps.here.HereLaneInfoListener.access$900(this.this$0));
                                        }
                                        if (i0 != 0) {
                                            com.navdy.hud.app.maps.here.HereLaneInfoListener.access$600(this.this$0).add(new com.navdy.hud.app.maps.MapEvents$LaneData(com.navdy.hud.app.maps.MapEvents$LaneData$Position.OFF_ROUTE, com.navdy.hud.app.maps.here.HereLaneInfoBuilder.getDrawable((java.util.List)com.navdy.hud.app.maps.here.HereLaneInfoListener.access$800(this.this$0), com.navdy.hud.app.maps.MapEvents$LaneData$Position.OFF_ROUTE, (com.here.android.mpa.guidance.LaneInformation$Direction)null)));
                                            break;
                                        } else {
                                            com.navdy.hud.app.maps.here.HereLaneInfoListener.access$600(this.this$0).add(new com.navdy.hud.app.maps.MapEvents$LaneData(com.navdy.hud.app.maps.MapEvents$LaneData$Position.ON_ROUTE, com.navdy.hud.app.maps.here.HereLaneInfoBuilder.getDrawable((java.util.List)com.navdy.hud.app.maps.here.HereLaneInfoListener.access$800(this.this$0), com.navdy.hud.app.maps.MapEvents$LaneData$Position.ON_ROUTE, a8)));
                                            break;
                                        }
                                    }
                                    case 2: {
                                        if (a8 != null && com.navdy.hud.app.maps.here.HereLaneInfoListener.access$800(this.this$0).size() > 1) {
                                            com.navdy.hud.app.maps.here.HereLaneInfoListener.access$900(this.this$0).clear();
                                            Object a22 = com.navdy.hud.app.maps.here.HereLaneInfoListener.access$800(this.this$0).iterator();
                                            while(((java.util.Iterator)a22).hasNext()) {
                                                com.here.android.mpa.guidance.LaneInformation$Direction a23 = (com.here.android.mpa.guidance.LaneInformation$Direction)((java.util.Iterator)a22).next();
                                                if (a23 == a8) {
                                                    com.navdy.hud.app.maps.here.HereLaneInfoListener.access$900(this.this$0).add(a23);
                                                    ((java.util.Iterator)a22).remove();
                                                }
                                            }
                                            com.navdy.hud.app.maps.here.HereLaneInfoListener.access$800(this.this$0).addAll((java.util.Collection)com.navdy.hud.app.maps.here.HereLaneInfoListener.access$900(this.this$0));
                                        }
                                        com.navdy.hud.app.maps.here.HereLaneInfoListener.access$600(this.this$0).add(new com.navdy.hud.app.maps.MapEvents$LaneData(com.navdy.hud.app.maps.MapEvents$LaneData$Position.ON_ROUTE, com.navdy.hud.app.maps.here.HereLaneInfoBuilder.getDrawable((java.util.List)com.navdy.hud.app.maps.here.HereLaneInfoListener.access$800(this.this$0), com.navdy.hud.app.maps.MapEvents$LaneData$Position.ON_ROUTE, a8)));
                                        break;
                                    }
                                    case 1: {
                                        com.navdy.hud.app.maps.here.HereLaneInfoListener.access$600(this.this$0).add(new com.navdy.hud.app.maps.MapEvents$LaneData(com.navdy.hud.app.maps.MapEvents$LaneData$Position.OFF_ROUTE, com.navdy.hud.app.maps.here.HereLaneInfoBuilder.getDrawable((java.util.List)com.navdy.hud.app.maps.here.HereLaneInfoListener.access$800(this.this$0), com.navdy.hud.app.maps.MapEvents$LaneData$Position.OFF_ROUTE, (com.here.android.mpa.guidance.LaneInformation$Direction)null)));
                                        break;
                                    }
                                }
                            } else {
                                com.navdy.hud.app.maps.here.HereLaneInfoListener.access$600(this.this$0).add(com.navdy.hud.app.maps.here.HereLaneInfoListener.access$700());
                            }
                        }
                        i7 = i7 + 1;
                    }
                    if (com.navdy.hud.app.maps.here.HereLaneInfoListener.access$100().isLoggable(2)) {
                        com.navdy.hud.app.maps.here.HereLaneInfoListener.access$100().v(new StringBuilder().append("LaneData: size=").append(com.navdy.hud.app.maps.here.HereLaneInfoListener.access$600(this.this$0).size()).toString());
                        Object a24 = com.navdy.hud.app.maps.here.HereLaneInfoListener.access$600(this.this$0).iterator();
                        while(((java.util.Iterator)a24).hasNext()) {
                            com.navdy.hud.app.maps.MapEvents$LaneData a25 = (com.navdy.hud.app.maps.MapEvents$LaneData)((java.util.Iterator)a24).next();
                            com.navdy.hud.app.maps.here.HereLaneInfoListener.access$100().v(new StringBuilder().append("LaneData:position: ").append(a25.position).toString());
                            if (a25.icons == null) {
                                com.navdy.hud.app.maps.here.HereLaneInfoListener.access$100().v("LaneData:icons: null");
                            } else {
                                int i8 = 0;
                                while(i8 < a25.icons.length) {
                                    com.navdy.hud.app.maps.here.HereLaneInfoListener.access$100().v(new StringBuilder().append("LaneData:icons").append(i8 + 1).append(" = ").append(a25.icons[i8]).toString());
                                    i8 = i8 + 1;
                                }
                            }
                        }
                    }
                    com.navdy.hud.app.maps.MapEvents$DisplayTrafficLaneInfo a26 = com.navdy.hud.app.maps.here.HereLaneInfoListener.access$200(this.this$0);
                    label1: {
                        label2: {
                            if (a26 == null) {
                                break label2;
                            }
                            if (com.navdy.hud.app.maps.here.HereLaneInfoBuilder.compareLaneData(com.navdy.hud.app.maps.here.HereLaneInfoListener.access$200(this.this$0).laneData, com.navdy.hud.app.maps.here.HereLaneInfoListener.access$600(this.this$0))) {
                                break label1;
                            }
                        }
                        java.util.ArrayList a27 = new java.util.ArrayList((java.util.Collection)com.navdy.hud.app.maps.here.HereLaneInfoListener.access$600(this.this$0));
                        com.navdy.hud.app.maps.here.HereLaneInfoListener.access$202(this.this$0, new com.navdy.hud.app.maps.MapEvents$DisplayTrafficLaneInfo(a27));
                        com.navdy.hud.app.maps.here.HereLaneInfoListener.access$600(this.this$0).clear();
                        com.navdy.hud.app.maps.here.HereLaneInfoListener.access$100().v(new StringBuilder().append("onShowLaneInfo::").append(a27.size()).toString());
                        if (!com.navdy.hud.app.maps.here.HereLaneInfoListener.access$1000(this.this$0)) {
                            break label0;
                        }
                        com.navdy.hud.app.maps.here.HereLaneInfoListener.access$400(this.this$0).post(com.navdy.hud.app.maps.here.HereLaneInfoListener.access$200(this.this$0));
                        break label0;
                    }
                    if (com.navdy.hud.app.maps.here.HereLaneInfoListener.access$100().isLoggable(2)) {
                        com.navdy.hud.app.maps.here.HereLaneInfoListener.access$100().v("LaneData: lanedata is same as last");
                    }
                    com.navdy.hud.app.maps.here.HereLaneInfoListener.access$600(this.this$0).clear();
                    break label0;
                } catch(Throwable a28) {
                    com.navdy.hud.app.maps.here.HereLaneInfoListener.access$100().e(a28);
                    break label0;
                }
            }
            if (com.navdy.hud.app.maps.here.HereLaneInfoListener.access$200(this.this$0) != null) {
                com.navdy.hud.app.maps.here.HereLaneInfoListener.access$202(this.this$0, (com.navdy.hud.app.maps.MapEvents$DisplayTrafficLaneInfo)null);
                this.this$0.hideLaneInfo();
            }
        }
    }
}
