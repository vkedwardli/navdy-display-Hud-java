package com.navdy.hud.app.maps.here;

final class HereRouteManager$7 implements Runnable {
    final com.navdy.service.library.events.navigation.NavigationRouteRequest val$pendingRequest;
    
    HereRouteManager$7(com.navdy.service.library.events.navigation.NavigationRouteRequest a) {
        super();
        this.val$pendingRequest = a;
    }
    
    public void run() {
        com.navdy.hud.app.maps.here.HereRouteManager.access$100().v("launched pending request");
        com.navdy.hud.app.maps.here.HereRouteManager.handleRouteRequest(this.val$pendingRequest);
    }
}
