package com.navdy.hud.app.maps.here;

abstract public interface HerePlacesManager$OnCategoriesSearchListener {
    abstract public void onCompleted(java.util.List arg);
    
    
    abstract public void onError(com.navdy.hud.app.maps.here.HerePlacesManager$Error arg);
}
