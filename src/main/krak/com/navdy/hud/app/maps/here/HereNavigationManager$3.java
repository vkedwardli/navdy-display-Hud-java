package com.navdy.hud.app.maps.here;

class HereNavigationManager$3 implements Runnable {
    final com.navdy.hud.app.maps.here.HereNavigationManager this$0;
    final String val$id;
    final boolean val$includedTraffic;
    final com.here.android.mpa.routing.Route val$newRoute;
    final String val$oldRouteId;
    final com.navdy.service.library.events.navigation.NavigationSessionRouteChange$RerouteReason val$reason;
    final com.navdy.service.library.events.navigation.NavigationRouteRequest val$routeRequest;
    final String val$viaStr;
    
    HereNavigationManager$3(com.navdy.hud.app.maps.here.HereNavigationManager a, String s, com.here.android.mpa.routing.Route a0, String s0, boolean b, String s1, com.navdy.service.library.events.navigation.NavigationSessionRouteChange$RerouteReason a1, com.navdy.service.library.events.navigation.NavigationRouteRequest a2) {
        super();
        this.this$0 = a;
        this.val$viaStr = s;
        this.val$newRoute = a0;
        this.val$id = s0;
        this.val$includedTraffic = b;
        this.val$oldRouteId = s1;
        this.val$reason = a1;
        this.val$routeRequest = a2;
    }
    
    public void run() {
        java.util.List a = null;
        com.navdy.hud.app.maps.here.HereRouteCalculator a0 = new com.navdy.hud.app.maps.here.HereRouteCalculator(com.navdy.hud.app.maps.here.HereNavigationManager.sLogger, false);
        String s = this.val$viaStr;
        if (android.text.TextUtils.isEmpty((CharSequence)s)) {
            s = com.navdy.hud.app.maps.here.HereRouteViaGenerator.getViaString(this.val$newRoute);
            if (android.text.TextUtils.isEmpty((CharSequence)s)) {
                s = "";
            }
        }
        com.navdy.service.library.events.navigation.NavigationRouteRequest a1 = com.navdy.hud.app.maps.here.HereNavigationManager.access$100(this.this$0).navigationRouteRequest;
        Object a2 = null;
        String s0 = null;
        String s1 = null;
        if (a1 == null) {
            a = (java.util.List)a2;
        } else {
            s0 = a1.label;
            s1 = a1.streetAddress;
            a = a1.routeAttributes;
        }
        com.navdy.service.library.events.navigation.NavigationRouteResult a3 = a0.getRouteResultFromRoute(this.val$id, this.val$newRoute, s, s0, s1, true);
        com.navdy.hud.app.maps.here.HereNavigationManager.access$100(this.this$0).routeId = this.val$id;
        com.navdy.hud.app.maps.here.HereNavigationManager.access$100(this.this$0).route = this.val$newRoute;
        com.navdy.hud.app.maps.here.HereRouteCache$RouteInfo a4 = new com.navdy.hud.app.maps.here.HereRouteCache$RouteInfo(this.val$newRoute, a1, a3, this.val$includedTraffic, com.navdy.hud.app.maps.here.HereNavigationManager.access$600().getLocationFixManager().getLastGeoCoordinate());
        com.navdy.hud.app.maps.here.HereRouteCache.getInstance().addRoute(this.val$id, a4);
        com.navdy.hud.app.maps.here.HereNavigationManager.sLogger.v("new route added to cache");
        this.this$0.postNavigationSessionStatusEvent(com.navdy.service.library.events.navigation.NavigationSessionState.NAV_SESSION_REROUTED, false);
        com.navdy.service.library.events.navigation.NavigationSessionRouteChange a5 = new com.navdy.service.library.events.navigation.NavigationSessionRouteChange(this.val$oldRouteId, a3, this.val$reason);
        if (a != null && a.contains(com.navdy.service.library.events.navigation.NavigationRouteRequest$RouteAttribute.ROUTE_ATTRIBUTE_GAS)) {
            com.navdy.hud.app.maps.here.HereMapUtil.saveGasRouteInfo(a1, com.navdy.hud.app.maps.here.HereNavigationManager.access$100(this.this$0).deviceId, com.navdy.hud.app.maps.here.HereNavigationManager.sLogger);
        }
        com.navdy.hud.app.maps.here.HereNavigationManager.access$500(this.this$0).post(a5);
        com.navdy.hud.app.maps.here.HereNavigationManager.access$500(this.this$0).post(new com.navdy.hud.app.event.RemoteEvent((com.squareup.wire.Message)a5));
        com.navdy.hud.app.maps.here.HereNavigationManager.sLogger.v(new StringBuilder().append("NavigationSessionRouteChange sent to client oldRoute[").append(this.val$oldRouteId).append("] newRoute[").append(this.val$id).append("] reason[").append(this.val$reason).append("] via[").append(s).append("]").toString());
        if (com.navdy.hud.app.maps.here.HereNavigationManager.sLogger.isLoggable(2)) {
            com.navdy.hud.app.maps.here.HereMapUtil.printRouteDetails(this.val$newRoute, this.val$routeRequest.streetAddress, this.val$routeRequest, (StringBuilder)null);
        }
    }
}
