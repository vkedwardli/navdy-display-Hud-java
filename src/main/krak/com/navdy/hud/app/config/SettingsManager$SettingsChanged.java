package com.navdy.hud.app.config;

public class SettingsManager$SettingsChanged {
    final public String path;
    final public com.navdy.hud.app.config.Setting setting;
    
    public SettingsManager$SettingsChanged(String s, com.navdy.hud.app.config.Setting a) {
        this.path = s;
        this.setting = a;
    }
}
