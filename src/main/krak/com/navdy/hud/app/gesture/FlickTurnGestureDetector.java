package com.navdy.hud.app.gesture;

public class FlickTurnGestureDetector extends com.navdy.hud.app.gesture.MultipleClickGestureDetector {
    final private static java.util.List FLICKABLE_EVENTS;
    final private static int FLICK_EVENT_COUNT = 4;
    final private static int FLICK_PAIRED_EVENT_TIMEOUT_MS = 500;
    final private static com.navdy.service.library.log.Logger sLogger;
    private short consecutiveTurnEventCount;
    private com.navdy.hud.app.gesture.FlickTurnGestureDetector$IFlickTurnKeyGesture listener;
    private com.navdy.hud.app.manager.InputManager$CustomKeyEvent previousKeyEvent;
    private long previousKeyEventTime;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.gesture.FlickTurnGestureDetector.class);
        FLICKABLE_EVENTS = (java.util.List)new com.navdy.hud.app.gesture.FlickTurnGestureDetector$1();
    }
    
    public FlickTurnGestureDetector(com.navdy.hud.app.gesture.MultipleClickGestureDetector$IMultipleClickKeyGesture a) {
        super(2, a);
        this.previousKeyEvent = null;
        this.previousKeyEventTime = 0L;
        this.consecutiveTurnEventCount = (short)0;
        if (a != null && a instanceof com.navdy.hud.app.gesture.FlickTurnGestureDetector$IFlickTurnKeyGesture) {
            this.listener = (com.navdy.hud.app.gesture.FlickTurnGestureDetector$IFlickTurnKeyGesture)a;
            return;
        }
        throw new IllegalArgumentException();
    }
    
    public com.navdy.hud.app.manager.InputManager$IInputHandler nextHandler() {
        return null;
    }
    
    public boolean onGesture(com.navdy.service.library.events.input.GestureEvent a) {
        return false;
    }
    
    public boolean onKey(com.navdy.hud.app.manager.InputManager$CustomKeyEvent a) {
        boolean b = super.onKey(a);
        long j = System.currentTimeMillis();
        boolean b0 = FLICKABLE_EVENTS.contains(a);
        label2: {
            label0: {
                label1: {
                    if (!b0) {
                        break label1;
                    }
                    if (this.previousKeyEvent != a) {
                        break label1;
                    }
                    if (j < this.previousKeyEventTime + 500L) {
                        break label0;
                    }
                }
                this.consecutiveTurnEventCount = (short)1;
                break label2;
            }
            int i = this.consecutiveTurnEventCount;
            int i0 = (short)(i + 1);
            int i1 = (short)i0;
            this.consecutiveTurnEventCount = (short)i1;
            if (i0 == 4) {
                sLogger.d(new StringBuilder().append("Combined key event recognized from a single ").append(a).toString());
                this.consecutiveTurnEventCount = (short)0;
                if (com.navdy.hud.app.manager.InputManager$CustomKeyEvent.LEFT.equals(a)) {
                    this.listener.onFlickLeft();
                } else {
                    if (!com.navdy.hud.app.manager.InputManager$CustomKeyEvent.RIGHT.equals(a)) {
                        throw new UnsupportedOperationException(new StringBuilder().append("Flick for ").append(a).append(" not handled").toString());
                    }
                    this.listener.onFlickRight();
                }
            }
        }
        this.previousKeyEvent = a;
        this.previousKeyEventTime = j;
        return b;
    }
}
