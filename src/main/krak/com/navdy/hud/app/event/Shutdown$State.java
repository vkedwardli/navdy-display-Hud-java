package com.navdy.hud.app.event;


public enum Shutdown$State {
    CONFIRMATION(0),
    CANCELED(1),
    CONFIRMED(2),
    SHUTTING_DOWN(3);

    private int value;
    Shutdown$State(int value) {
        this.value = value;
    }
    public int getValue() {
        return value;
    }
}

//final public class Shutdown$State extends Enum {
//    final private static com.navdy.hud.app.event.Shutdown$State[] $VALUES;
//    final public static com.navdy.hud.app.event.Shutdown$State CANCELED;
//    final public static com.navdy.hud.app.event.Shutdown$State CONFIRMATION;
//    final public static com.navdy.hud.app.event.Shutdown$State CONFIRMED;
//    final public static com.navdy.hud.app.event.Shutdown$State SHUTTING_DOWN;
//    
//    static {
//        CONFIRMATION = new com.navdy.hud.app.event.Shutdown$State("CONFIRMATION", 0);
//        CANCELED = new com.navdy.hud.app.event.Shutdown$State("CANCELED", 1);
//        CONFIRMED = new com.navdy.hud.app.event.Shutdown$State("CONFIRMED", 2);
//        SHUTTING_DOWN = new com.navdy.hud.app.event.Shutdown$State("SHUTTING_DOWN", 3);
//        com.navdy.hud.app.event.Shutdown$State[] a = new com.navdy.hud.app.event.Shutdown$State[4];
//        a[0] = CONFIRMATION;
//        a[1] = CANCELED;
//        a[2] = CONFIRMED;
//        a[3] = SHUTTING_DOWN;
//        $VALUES = a;
//    }
//    
//    private Shutdown$State(String s, int i) {
//        super(s, i);
//    }
//    
//    public static com.navdy.hud.app.event.Shutdown$State valueOf(String s) {
//        return (com.navdy.hud.app.event.Shutdown$State)Enum.valueOf(com.navdy.hud.app.event.Shutdown$State.class, s);
//    }
//    
//    public static com.navdy.hud.app.event.Shutdown$State[] values() {
//        return $VALUES.clone();
//    }
//}
//