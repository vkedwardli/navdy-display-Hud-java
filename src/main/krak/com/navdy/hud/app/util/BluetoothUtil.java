package com.navdy.hud.app.util;

public class BluetoothUtil {
    final public static int PAIRING_CONSENT = 3;
    final public static int PAIRING_VARIANT_DISPLAY_PASSKEY = 4;
    final public static int PAIRING_VARIANT_DISPLAY_PIN = 5;
    final public static com.navdy.service.library.log.Logger sLogger;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.util.BluetoothUtil.class);
    }
    
    public BluetoothUtil() {
    }
    
    public static void cancelBondProcess(android.bluetooth.BluetoothDevice a) {
        try {
            java.lang.reflect.Method a0 = (a).getClass().getMethod("cancelBondProcess", (Class[])null);
            if (a0 == null) {
                sLogger.e("cannot get to cancelBondProcess api");
            } else {
                sLogger.i(new StringBuilder().append("cancellingBond for ").append(a).toString());
                a0.invoke(a, (Object[])null);
                sLogger.i(new StringBuilder().append("cancelledBond for ").append(a).toString());
            }
        } catch(Throwable a1) {
            sLogger.e("Failed to cancelBondProcess", a1);
        }
    }
    
    public static void cancelUserConfirmation(android.bluetooth.BluetoothDevice a) {
        try {
            sLogger.e("cancelling cancelPairingUserInput");
            Class a0 = (a).getClass();
            Class[] a1 = new Class[1];
            a1[0] = Boolean.TYPE;
            a0.getMethod("cancelPairingUserInput", a1).invoke(a, new Object[0]);
            sLogger.e("cancelled cancelPairingUserInput");
        } catch(Throwable a2) {
            sLogger.e(a2);
        }
    }
    
    public static void confirmPairing(android.bluetooth.BluetoothDevice a, int i, int i0) {
        sLogger.i(new StringBuilder().append("Confirming pairing for ").append(a).append(" variant").append(i).append(" pin:").append(i0).toString());
        label2: {
            label0: {
                label1: {
                    if (i == 4) {
                        break label1;
                    }
                    if (i != 2) {
                        break label0;
                    }
                }
                a.setPairingConfirmation(true);
                break label2;
            }
            if (i != 5) {
                if (i == 3) {
                    a.setPairingConfirmation(true);
                }
            } else {
                Object[] a0 = new Object[1];
                a0[0] = Integer.valueOf(i0);
                a.setPin(com.navdy.hud.app.util.BluetoothUtil.convertPinToBytes(String.format("%04d", a0)));
            }
        }
    }
    
    public static byte[] convertPinToBytes(String s) {
        byte[] a = null;
        label0: {
            label2: if (s != null) {
                try {
                    a = s.getBytes("UTF-8");
                } catch(java.io.UnsupportedEncodingException ignoredException) {
                    break label2;
                }
                int i = a.length;
                label1: {
                    if (i <= 0) {
                        break label1;
                    }
                    if (a.length <= 16) {
                        break label0;
                    }
                }
                a = null;
                break label0;
            } else {
                a = null;
                break label0;
            }
            sLogger.e("UTF-8 not supported?!?");
            a = null;
        }
        return a;
    }
    
    public static void removeBond(android.bluetooth.BluetoothDevice a) {
        try {
            java.lang.reflect.Method a0 = (a).getClass().getMethod("removeBond", (Class[])null);
            if (a0 == null) {
                sLogger.e("cannot get to removeBond api");
            } else {
                sLogger.i(new StringBuilder().append("removingBond for ").append(a).toString());
                a0.invoke(a, (Object[])null);
                sLogger.i(new StringBuilder().append("removedBond for ").append(a).toString());
            }
        } catch(Throwable a1) {
            sLogger.e("Failed to removeBond", a1);
        }
    }
}
