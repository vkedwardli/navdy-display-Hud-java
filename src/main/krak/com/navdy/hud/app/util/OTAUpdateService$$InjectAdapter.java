package com.navdy.hud.app.util;

final public class OTAUpdateService$$InjectAdapter extends dagger.internal.Binding implements javax.inject.Provider, dagger.MembersInjector {
    private dagger.internal.Binding bus;
    private dagger.internal.Binding sharedPreferences;
    
    public OTAUpdateService$$InjectAdapter() {
        super("com.navdy.hud.app.util.OTAUpdateService", "members/com.navdy.hud.app.util.OTAUpdateService", false, com.navdy.hud.app.util.OTAUpdateService.class);
    }
    
    public void attach(dagger.internal.Linker a) {
        this.sharedPreferences = a.requestBinding("android.content.SharedPreferences", com.navdy.hud.app.util.OTAUpdateService.class, (this).getClass().getClassLoader());
        this.bus = a.requestBinding("com.squareup.otto.Bus", com.navdy.hud.app.util.OTAUpdateService.class, (this).getClass().getClassLoader());
    }
    
    public com.navdy.hud.app.util.OTAUpdateService get() {
        com.navdy.hud.app.util.OTAUpdateService a = new com.navdy.hud.app.util.OTAUpdateService();
        this.injectMembers(a);
        return a;
    }
    
    public Object get() {
        return this.get();
    }
    
    public void getDependencies(java.util.Set a, java.util.Set a0) {
        a0.add(this.sharedPreferences);
        a0.add(this.bus);
    }
    
    public void injectMembers(com.navdy.hud.app.util.OTAUpdateService a) {
        a.sharedPreferences = (android.content.SharedPreferences)this.sharedPreferences.get();
        a.bus = (com.squareup.otto.Bus)this.bus.get();
    }
    
    public void injectMembers(Object a) {
        this.injectMembers((com.navdy.hud.app.util.OTAUpdateService)a);
    }
}
