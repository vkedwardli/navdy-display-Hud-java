package com.navdy.hud.app.util.picasso;

public class PicassoCacheMap extends java.util.LinkedHashMap {
    private com.navdy.hud.app.util.picasso.PicassoItemCacheListener listener;
    
    PicassoCacheMap(int i, float f) {
        super(i, f, true);
    }
    
    public Object put(Object a, Object a0) {
        Object a1 = super.put(a, a0);
        this.listener.itemAdded(a);
        return a1;
    }
    
    public Object remove(Object a) {
        this.listener.itemRemoved(a);
        return super.remove(a);
    }
    
    void setListener(com.navdy.hud.app.util.picasso.PicassoItemCacheListener a) {
        this.listener = a;
    }
}
