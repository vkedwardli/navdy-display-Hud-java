package com.navdy.hud.app.util;

public class RemoteCapabilitiesUtil {
    public RemoteCapabilitiesUtil() {
    }
    
    private static com.navdy.service.library.events.Capabilities getRemoteCapabilities() {
        com.navdy.service.library.events.DeviceInfo a = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getRemoteDeviceInfo();
        com.navdy.service.library.events.Capabilities a0 = (a == null) ? null : a.capabilities;
        return a0;
    }
    
    public static boolean supportsNavigationCoordinateLookup() {
        boolean b = false;
        com.navdy.service.library.events.Capabilities a = com.navdy.hud.app.util.RemoteCapabilitiesUtil.getRemoteCapabilities();
        label2: {
            label0: {
                label1: {
                    if (a == null) {
                        break label1;
                    }
                    if (Boolean.TRUE.equals(a.navCoordsLookup)) {
                        break label0;
                    }
                }
                b = false;
                break label2;
            }
            b = true;
        }
        return b;
    }
    
    public static boolean supportsPlaceSearch() {
        com.navdy.service.library.events.Capabilities a = com.navdy.hud.app.util.RemoteCapabilitiesUtil.getRemoteCapabilities();
        return (a == null) ? com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().doesRemoteDeviceHasCapability(com.navdy.service.library.events.LegacyCapability.CAPABILITY_PLACE_TYPE_SEARCH) : Boolean.TRUE.equals(a.placeTypeSearch);
    }
    
    public static boolean supportsVoiceSearch() {
        com.navdy.service.library.events.Capabilities a = com.navdy.hud.app.util.RemoteCapabilitiesUtil.getRemoteCapabilities();
        return a != null && Boolean.TRUE.equals(a.voiceSearch);
    }
    
    public static boolean supportsVoiceSearchNewIOSPauseBehaviors() {
        boolean b = false;
        com.navdy.service.library.events.Capabilities a = com.navdy.hud.app.util.RemoteCapabilitiesUtil.getRemoteCapabilities();
        label2: {
            label0: {
                label1: {
                    if (a == null) {
                        break label1;
                    }
                    if (Boolean.TRUE.equals(a.voiceSearchNewIOSPauseBehaviors)) {
                        break label0;
                    }
                }
                b = false;
                break label2;
            }
            b = true;
        }
        return b;
    }
}
