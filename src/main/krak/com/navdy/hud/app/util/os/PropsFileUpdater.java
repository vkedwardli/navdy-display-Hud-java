package com.navdy.hud.app.util.os;

public class PropsFileUpdater {
    final private static String CHAR_SET = "UTF-8";
    final private static String COMMAND_GETPROP = "getprop";
    final private static int PROPS_FILE_INTENT = 2;
    final private static String PROPS_FILE_NAME = "system_info.json";
    final private static String PROPS_FILE_PATH_PROP_NAME = "ro.maps_partition";
    final private static java.util.regex.Pattern PROPS_KEY_VALUE_SEPARATOR;
    final private static int PROPS_READOUT_DELAY = 15000;
    final private static String PROPS_TEMP_FILE_SUFFIX = "~";
    final private static com.navdy.service.library.log.Logger sLogger;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.util.os.PropsFileUpdater.class);
        PROPS_KEY_VALUE_SEPARATOR = java.util.regex.Pattern.compile("\\]: \\[");
    }
    
    public PropsFileUpdater() {
    }
    
    static void access$000(String s) {
        com.navdy.hud.app.util.os.PropsFileUpdater.updatePropsFile(s);
    }
    
    static com.navdy.service.library.log.Logger access$100() {
        return sLogger;
    }
    
    private static org.json.JSONObject propsToJSON(String s) {
        String[] a = s.split(System.getProperty("line.separator"));
        org.json.JSONObject a0 = new org.json.JSONObject();
        int i = a.length;
        int i0 = 0;
        while(i0 < i) {
            String s0 = a[i0];
            String s1 = s0.trim().substring(1, s0.length() - 1);
            String[] a1 = PROPS_KEY_VALUE_SEPARATOR.split((CharSequence)s1, 2);
            try {
                a0.put(a1[0], a1[1]);
            } catch(org.json.JSONException a2) {
                sLogger.e(new StringBuilder().append("Cannot create json for prop: ").append(s0).append(" - skipping").toString(), (Throwable)a2);
            }
            i0 = i0 + 1;
        }
        return a0;
    }
    
    public static String readProps() {
        String s = null;
        label0: {
            Process a = null;
            Throwable a0 = null;
            label1: {
                try {
                    a = null;
                    ProcessBuilder a1 = new ProcessBuilder(new String[0]);
                    String[] a2 = new String[1];
                    a2[0] = "getprop";
                    a = a1.command(a2).start();
                    a.waitFor();
                    sLogger.d(new StringBuilder().append("Reading props process ended with exit value: ").append(a.exitValue()).toString());
                    s = com.navdy.service.library.util.IOUtils.convertInputStreamToString(a.getInputStream(), "UTF-8");
                } catch(Throwable a3) {
                    a0 = a3;
                    break label1;
                }
                if (a == null) {
                    break label0;
                }
                a.destroy();
                break label0;
            }
            try {
                sLogger.e("Cannot execute getprop command", a0);
            } catch(Throwable a4) {
                if (a != null) {
                    a.destroy();
                }
                throw a4;
            }
            s = null;
            if (a != null) {
                a.destroy();
                s = null;
            }
        }
        return s;
    }
    
    public static void run() {
        new android.os.Handler(android.os.Looper.getMainLooper()).postDelayed((Runnable)new com.navdy.hud.app.util.os.PropsFileUpdater$1(), 15000L);
    }
    
    private static void updatePropsFile(String s) {
        label0: {
            org.json.JSONException a = null;
            label1: if (s != null) {
                try {
                    org.json.JSONObject a0 = com.navdy.hud.app.util.os.PropsFileUpdater.propsToJSON(s);
                    com.navdy.hud.app.util.os.PropsFileUpdater.updatePropsFile(a0.getString("ro.maps_partition"), a0.toString(2));
                } catch(org.json.JSONException a1) {
                    a = a1;
                    break label1;
                }
                break label0;
            } else {
                sLogger.w("Cannot update file with empty props string");
                break label0;
            }
            sLogger.e("Cannot build JSON string with props or read path for output file", (Throwable)a);
        }
    }
    
    private static void updatePropsFile(String s, String s0) {
        String s1 = new java.io.File(s, "system_info.json").getAbsolutePath();
        String s2 = new StringBuilder().append(s1).append("~").toString();
        label1: {
            label0: {
                java.io.IOException a = null;
                try {
                    com.navdy.service.library.util.IOUtils.copyFile(s2, (java.io.InputStream)new java.io.ByteArrayInputStream(s0.getBytes()));
                    break label0;
                } catch(java.io.IOException a0) {
                    a = a0;
                }
                sLogger.e(new StringBuilder().append("Exception while writing into file: ").append(s2).toString(), (Throwable)a);
                break label1;
            }
            try {
                android.system.Os.rename(s2, s1);
                sLogger.i("Props file updated successfully");
            } catch(android.system.ErrnoException a1) {
                sLogger.e("Cannot overwrite props file with the new one", (Throwable)a1);
            }
        }
    }
}
