package com.navdy.hud.app.util;

class ScreenConductor$2 implements android.view.animation.Animation$AnimationListener {
    final com.navdy.hud.app.util.ScreenConductor this$0;
    final com.navdy.hud.app.screen.BaseScreen val$newScreen;
    final com.navdy.hud.app.screen.BaseScreen val$oldScreen;
    
    ScreenConductor$2(com.navdy.hud.app.util.ScreenConductor a, com.navdy.hud.app.screen.BaseScreen a0, com.navdy.hud.app.screen.BaseScreen a1) {
        super();
        this.this$0 = a;
        this.val$newScreen = a0;
        this.val$oldScreen = a1;
    }
    
    public void onAnimationEnd(android.view.animation.Animation a) {
        this.val$newScreen.onAnimationInEnd();
        this.this$0.uiStateManager.postScreenAnimationEvent(false, this.val$newScreen, this.val$oldScreen);
    }
    
    public void onAnimationRepeat(android.view.animation.Animation a) {
    }
    
    public void onAnimationStart(android.view.animation.Animation a) {
        this.val$newScreen.onAnimationInStart();
        this.this$0.uiStateManager.postScreenAnimationEvent(true, this.val$newScreen, this.val$oldScreen);
    }
}
