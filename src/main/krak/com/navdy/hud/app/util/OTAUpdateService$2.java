package com.navdy.hud.app.util;

class OTAUpdateService$2 implements Runnable {
    final com.navdy.hud.app.util.OTAUpdateService this$0;
    
    OTAUpdateService$2(com.navdy.hud.app.util.OTAUpdateService a) {
        super();
        this.this$0 = a;
    }
    
    public void run() {
        com.navdy.hud.app.util.OTAUpdateService.access$000().d("Checking the update");
        java.io.File a = com.navdy.hud.app.util.OTAUpdateService.access$100((android.content.Context)this.this$0, this.this$0.sharedPreferences);
        label2: if (a != null) {
            boolean b = com.navdy.hud.app.util.OTAUpdateService.access$400(this.this$0, a);
            com.navdy.hud.app.util.OTAUpdateService.access$000().d(new StringBuilder().append("Current update valid ? :").append(b).toString());
            if (b) {
                boolean b0 = com.navdy.hud.app.util.OTAUpdateService.isOTAUpdateVerified(this.this$0.sharedPreferences);
                label0: {
                    label1: {
                        if (b0) {
                            break label1;
                        }
                        if (!this.this$0.bVerifyUpdate(a, this.this$0.sharedPreferences)) {
                            break label0;
                        }
                        this.this$0.sharedPreferences.edit().putBoolean("verified", true).commit();
                        this.this$0.bus.post(new com.navdy.hud.app.util.OTAUpdateService$UpdateVerified());
                    }
                    com.navdy.hud.app.util.OTAUpdateService.access$502(com.navdy.hud.app.util.OTAUpdateService.access$600(this.this$0.sharedPreferences));
                    com.navdy.hud.app.util.OTAUpdateService.access$000().v(new StringBuilder().append("verified swVersion:").append(com.navdy.hud.app.util.OTAUpdateService.access$500()).toString());
                    break label2;
                }
                com.navdy.hud.app.util.OTAUpdateService.clearUpdate(a, (android.content.Context)this.this$0, this.this$0.sharedPreferences);
            } else {
                com.navdy.hud.app.util.OTAUpdateService.clearUpdate(a, (android.content.Context)this.this$0, this.this$0.sharedPreferences);
            }
        } else {
            com.navdy.hud.app.util.OTAUpdateService.access$000().e("No update file found");
            com.navdy.hud.app.util.OTAUpdateService.access$400(this.this$0, (java.io.File)null);
            com.navdy.hud.app.util.OTAUpdateService.clearUpdate(a, (android.content.Context)this.this$0, this.this$0.sharedPreferences);
        }
    }
}
