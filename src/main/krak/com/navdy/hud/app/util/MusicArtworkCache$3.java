package com.navdy.hud.app.util;

class MusicArtworkCache$3 {
    final static int[] $SwitchMap$com$navdy$service$library$events$audio$MusicCollectionType;
    
    static {
        $SwitchMap$com$navdy$service$library$events$audio$MusicCollectionType = new int[com.navdy.service.library.events.audio.MusicCollectionType.values().length];
        int[] a = $SwitchMap$com$navdy$service$library$events$audio$MusicCollectionType;
        com.navdy.service.library.events.audio.MusicCollectionType a0 = com.navdy.service.library.events.audio.MusicCollectionType.COLLECTION_TYPE_UNKNOWN;
        try {
            a[a0.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$audio$MusicCollectionType[com.navdy.service.library.events.audio.MusicCollectionType.COLLECTION_TYPE_PLAYLISTS.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException0) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$audio$MusicCollectionType[com.navdy.service.library.events.audio.MusicCollectionType.COLLECTION_TYPE_ARTISTS.ordinal()] = 3;
        } catch(NoSuchFieldError ignoredException1) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$audio$MusicCollectionType[com.navdy.service.library.events.audio.MusicCollectionType.COLLECTION_TYPE_ALBUMS.ordinal()] = 4;
        } catch(NoSuchFieldError ignoredException2) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$audio$MusicCollectionType[com.navdy.service.library.events.audio.MusicCollectionType.COLLECTION_TYPE_PODCASTS.ordinal()] = 5;
        } catch(NoSuchFieldError ignoredException3) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$audio$MusicCollectionType[com.navdy.service.library.events.audio.MusicCollectionType.COLLECTION_TYPE_AUDIOBOOKS.ordinal()] = 6;
        } catch(NoSuchFieldError ignoredException4) {
        }
    }
}
