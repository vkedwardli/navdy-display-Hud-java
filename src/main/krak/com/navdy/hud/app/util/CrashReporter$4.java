package com.navdy.hud.app.util;

class CrashReporter$4 implements Runnable {
    final com.navdy.hud.app.util.CrashReporter this$0;
    final String[] val$kernelCrashFiles;
    
    CrashReporter$4(com.navdy.hud.app.util.CrashReporter a, String[] a0) {
        super();
        this.this$0 = a;
        this.val$kernelCrashFiles = a0;
    }
    
    public void run() {
        label0: {
            Throwable a = null;
            label1: {
                String[] a0 = null;
                int i = 0;
                int i0 = 0;
                java.io.File a1 = null;
                try {
                    com.navdy.hud.app.util.CrashReporter.access$400().v("checking for kernel crashes");
                    a0 = this.val$kernelCrashFiles;
                    i = a0.length;
                    i0 = 0;
                } catch(Throwable a2) {
                    a = a2;
                    break label1;
                }
                while(true) {
                    boolean b = false;
                    a1 = null;
                    if (i0 >= i) {
                        break;
                    }
                    String s = a0[i0];
                    try {
                        a1 = new java.io.File(s);
                        b = a1.exists();
                    } catch(Throwable a3) {
                        a = a3;
                        break label1;
                    }
                    {
                        if (!b) {
                            i0 = i0 + 1;
                            continue;
                        }
                        try {
                            com.navdy.hud.app.util.CrashReporter.access$400().v(new StringBuilder().append("found file:").append(a1.getAbsolutePath()).toString());
                            break;
                        } catch(Throwable a4) {
                            a = a4;
                            break label1;
                        }
                    }
                }
                try {
                    if (a1 == null) {
                        com.navdy.hud.app.util.CrashReporter.access$400().v("no kernel crash files");
                        break label0;
                    } else {
                        String s0 = com.navdy.hud.app.util.CrashReporter.access$900(this.this$0, a1);
                        if (!android.text.TextUtils.isEmpty((CharSequence)s0)) {
                            com.navdy.hud.app.util.CrashReporter$KernelCrashException a5 = new com.navdy.hud.app.util.CrashReporter$KernelCrashException(s0);
                            this.this$0.reportNonFatalException((Throwable)a5);
                            com.navdy.hud.app.util.CrashReporter.access$400().v("kernel crash created");
                        }
                        com.navdy.hud.app.util.CrashReporter.access$1000(this.this$0, this.val$kernelCrashFiles);
                        break label0;
                    }
                } catch(Throwable a6) {
                    a = a6;
                }
            }
            com.navdy.hud.app.util.CrashReporter.access$400().e(a);
        }
    }
}
