package com.navdy.hud.app.util;


    public enum DeviceUtil$HereSdkVersion {
        SDK(0);

        private int value;
        DeviceUtil$HereSdkVersion(int value) {
            this.value = value;
        }
        public int getValue() {
            return value;
        }
    }
    
//final class DeviceUtil$HereSdkVersion extends Enum {
//    final private static com.navdy.hud.app.util.DeviceUtil$HereSdkVersion[] $VALUES;
//    final public static com.navdy.hud.app.util.DeviceUtil$HereSdkVersion SDK;
//    private String folderName;
//    private String version;
//    
//    static {
//        SDK = new com.navdy.hud.app.util.DeviceUtil$HereSdkVersion("SDK", 0, "3.3.1", "00a584764c918d799f376cc41e46674ba558f8b4");
//        com.navdy.hud.app.util.DeviceUtil$HereSdkVersion[] a = new com.navdy.hud.app.util.DeviceUtil$HereSdkVersion[1];
//        a[0] = SDK;
//        $VALUES = a;
//    }
//    
//    private DeviceUtil$HereSdkVersion(String s, int i, String s0, String s1) {
//        super(s, i);
//        this.version = s0;
//        this.folderName = s1;
//    }
//    
//    static String access$000(com.navdy.hud.app.util.DeviceUtil$HereSdkVersion a) {
//        return a.version;
//    }
//    
//    static String access$100(com.navdy.hud.app.util.DeviceUtil$HereSdkVersion a) {
//        return a.folderName;
//    }
//    
//    public static com.navdy.hud.app.util.DeviceUtil$HereSdkVersion valueOf(String s) {
//        return (com.navdy.hud.app.util.DeviceUtil$HereSdkVersion)Enum.valueOf(com.navdy.hud.app.util.DeviceUtil$HereSdkVersion.class, s);
//    }
//    
//    public static com.navdy.hud.app.util.DeviceUtil$HereSdkVersion[] values() {
//        return $VALUES.clone();
//    }
//}
//