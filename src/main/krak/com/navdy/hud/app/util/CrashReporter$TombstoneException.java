package com.navdy.hud.app.util;

public class CrashReporter$TombstoneException extends com.navdy.hud.app.util.CrashReporter$CrashException {
    final private static java.util.regex.Pattern stackMatcher;
    final public String description;
    
    static {
        stackMatcher = java.util.regex.Pattern.compile("Abort message: ('[^']+'\n)|#00 pc \\w+\\s+([^\n]+\n)");
    }
    
    public CrashReporter$TombstoneException(String s) {
        super("");
        this.description = s;
    }
    
    public String getCrashLocation() {
        return this.extractCrashLocation(this.description);
    }
    
    public java.util.regex.Pattern getLocationPattern() {
        return stackMatcher;
    }
}
