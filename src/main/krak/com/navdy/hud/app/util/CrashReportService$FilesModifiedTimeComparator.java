package com.navdy.hud.app.util;

public class CrashReportService$FilesModifiedTimeComparator implements java.util.Comparator {
    public CrashReportService$FilesModifiedTimeComparator() {
    }
    
    public int compare(java.io.File a, java.io.File a0) {
        return (int)(a.lastModified() - a0.lastModified());
    }
    
    public int compare(Object a, Object a0) {
        return this.compare((java.io.File)a, (java.io.File)a0);
    }
}
