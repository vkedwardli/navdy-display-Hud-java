package com.navdy.hud.app.util;

public class ScreenConductor implements com.navdy.hud.app.util.CanShowScreen {
    final private android.view.ViewGroup container;
    final private android.content.Context context;
    com.navdy.hud.app.ui.component.homescreen.HomeScreenView homeScreenView;
    final private com.navdy.service.library.log.Logger logger;
    com.navdy.hud.app.ui.framework.UIStateManager uiStateManager;
    
    public ScreenConductor(android.content.Context a, android.view.ViewGroup a0, com.navdy.hud.app.ui.framework.UIStateManager a1) {
        this.logger = new com.navdy.service.library.log.Logger((this).getClass());
        this.context = a;
        this.container = a0;
        this.uiStateManager = a1;
    }
    
    public void createScreens() {
        if (this.homeScreenView == null) {
            com.navdy.hud.app.ui.component.homescreen.HomeScreen a = new com.navdy.hud.app.ui.component.homescreen.HomeScreen();
            this.homeScreenView = (com.navdy.hud.app.ui.component.homescreen.HomeScreenView)flow.Layouts.createView(mortar.Mortar.getScope(this.context).requireChild((mortar.Blueprint)a).createContext(this.context), a);
            this.homeScreenView.setVisibility(4);
            this.container.addView((android.view.View)this.homeScreenView, 0);
            this.uiStateManager.setHomescreenView(this.homeScreenView);
            this.logger.v("createScreens:homescreen created and added");
        }
    }
    
    public android.view.View getChildView() {
        android.view.View a = null;
        int i = this.container.getChildCount();
        if (i != 0) {
            int i0 = i + -1;
            while(true) {
                if (i0 < 0) {
                    a = null;
                    break;
                } else {
                    a = this.container.getChildAt(i0);
                    if (a.getVisibility() == 0) {
                        break;
                    }
                    i0 = i0 + -1;
                }
            }
        } else {
            a = null;
        }
        return a;
    }
    
    protected void setAnimation(flow.Flow$Direction a, android.view.View a0, android.view.View a1, com.navdy.hud.app.screen.BaseScreen a2, com.navdy.hud.app.screen.BaseScreen a3, int i, int i0) {
        label1: if (a0 != null) {
            label0: {
                if (i != -1) {
                    break label0;
                }
                if (i0 != -1) {
                    break label0;
                }
                this.uiStateManager.postScreenAnimationEvent(true, a3, a2);
                break label1;
            }
            if (i0 != -1) {
                android.view.animation.Animation a4 = android.view.animation.AnimationUtils.loadAnimation(this.context, i0);
                if (a2 != null) {
                    a4.setAnimationListener((android.view.animation.Animation$AnimationListener)new com.navdy.hud.app.util.ScreenConductor$1(this, a2, i, a3));
                }
                a0.setAnimation(a4);
            }
            if (i != -1) {
                android.view.animation.Animation a5 = android.view.animation.AnimationUtils.loadAnimation(this.context, i);
                if (a3 != null) {
                    a5.setAnimationListener((android.view.animation.Animation$AnimationListener)new com.navdy.hud.app.util.ScreenConductor$2(this, a3, a2));
                }
                a1.setAnimation(a5);
            }
        } else if (a3 != null) {
            a3.onAnimationInStart();
            this.uiStateManager.postScreenAnimationEvent(true, a3, (com.navdy.hud.app.screen.BaseScreen)null);
            a3.onAnimationInEnd();
            this.uiStateManager.postScreenAnimationEvent(false, a3, (com.navdy.hud.app.screen.BaseScreen)null);
        }
    }
    
    public void showScreen(mortar.Blueprint a, flow.Flow$Direction a0, int i, int i0) {
        mortar.MortarScope a1 = mortar.Mortar.getScope(this.context).requireChild(a);
        android.view.View a2 = this.getChildView();
        label1: {
            boolean b = false;
            com.navdy.hud.app.screen.BaseScreen a3 = null;
            label0: {
                if (a2 == null) {
                    break label0;
                }
                if (!mortar.Mortar.getScope(a2.getContext()).getName().equals(a.getMortarScopeName())) {
                    break label0;
                }
                if (a2.getVisibility() != 0) {
                    break label0;
                }
                com.navdy.hud.app.screen.BaseScreen a4 = (com.navdy.hud.app.screen.BaseScreen)a;
                this.uiStateManager.postScreenAnimationEvent(true, a4, a4);
                this.uiStateManager.postScreenAnimationEvent(false, a4, a4);
                break label1;
            }
            boolean b0 = a instanceof com.navdy.hud.app.ui.component.homescreen.HomeScreen;
            com.navdy.hud.app.ui.component.homescreen.HomeScreenView a5 = null;
            if (b0) {
                com.navdy.hud.app.ui.component.homescreen.HomeScreenView a6 = this.homeScreenView;
                a5 = null;
                if (a6 != null) {
                    this.logger.v("reusing homescreen view");
                    a5 = this.homeScreenView;
                }
            }
            android.view.View a7 = a5;
            if (a5 != null) {
                b = false;
            } else {
                a7 = flow.Layouts.createView(a1.createContext(this.context), a);
                if (b0) {
                    com.navdy.hud.app.ui.component.homescreen.HomeScreenView a8 = (com.navdy.hud.app.ui.component.homescreen.HomeScreenView)a7;
                    a7 = a8;
                    this.homeScreenView = a8;
                    b = true;
                } else {
                    b = false;
                }
            }
            com.navdy.hud.app.screen.BaseScreen a9 = this.uiStateManager.getCurrentScreen();
            com.navdy.hud.app.screen.BaseScreen a10 = (com.navdy.hud.app.screen.BaseScreen)a;
            this.setAnimation(a0, a2, a7, a9, a10, i, i0);
            if (a2 != null && a2 != this.homeScreenView) {
                this.container.removeView(a2);
            }
            if (b0) {
                if (this.homeScreenView != null) {
                    this.homeScreenView.setVisibility(4);
                }
                if (b) {
                    this.logger.v("home screen added");
                    this.container.addView(a7);
                }
                this.homeScreenView.onResume();
                this.homeScreenView.setVisibility(0);
                a3 = a10;
            } else {
                if (this.homeScreenView == null) {
                    a3 = a10;
                } else {
                    com.navdy.hud.app.ui.framework.UIStateManager dummy = this.uiStateManager;
                    a3 = (com.navdy.hud.app.screen.BaseScreen)a10;
                    if (com.navdy.hud.app.ui.framework.UIStateManager.isPauseHomescreen(a3.getScreen())) {
                        this.homeScreenView.onPause();
                        this.homeScreenView.setVisibility(4);
                    }
                }
                this.container.addView(a7);
            }
            com.navdy.hud.app.util.GenericUtil.clearInputMethodManagerFocusLeak();
            com.navdy.hud.app.ui.framework.UIStateManager a11 = this.uiStateManager;
            com.navdy.hud.app.screen.BaseScreen a12 = (com.navdy.hud.app.screen.BaseScreen)a3;
            a11.setMainActiveScreen(a12);
            com.navdy.hud.app.screen.BaseScreen a13 = (com.navdy.hud.app.screen.BaseScreen)a12;
            if (a13.getScreen() != com.navdy.service.library.events.ui.Screen.SCREEN_BACK) {
                com.navdy.hud.app.analytics.AnalyticsSupport.recordScreen((com.navdy.hud.app.screen.BaseScreen)a13);
            }
            if (i == -1 && i0 == -1) {
                this.uiStateManager.postScreenAnimationEvent(false, a10, a9);
            }
        }
    }
}
