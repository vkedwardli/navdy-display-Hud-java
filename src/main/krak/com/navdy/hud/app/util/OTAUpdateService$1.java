package com.navdy.hud.app.util;

class OTAUpdateService$1 implements Runnable {
    final com.navdy.hud.app.util.OTAUpdateService this$0;
    final String val$postUpdateCommand;
    
    OTAUpdateService$1(com.navdy.hud.app.util.OTAUpdateService a, String s) {
        super();
        this.this$0 = a;
        this.val$postUpdateCommand = s;
    }
    
    public void run() {
        label3: {
            label1: {
                label2: {
                    label0: try {
                        Exception a = null;
                        try {
                            Throwable a0 = null;
                            com.navdy.hud.app.util.OTAUpdateService.access$000().d("Applying the update");
                            java.io.File a1 = com.navdy.hud.app.util.OTAUpdateService.access$100((android.content.Context)this.this$0, this.this$0.sharedPreferences);
                            if (a1 == null) {
                                com.navdy.hud.app.util.OTAUpdateService.clearUpdate(a1, (android.content.Context)this.this$0, this.this$0.sharedPreferences);
                                break label0;
                            } else {
                                try {
                                    int i = this.this$0.sharedPreferences.getInt("retry_count", 0);
                                    this.this$0.sharedPreferences.edit().putInt("retry_count", i + 1).putString("update_from_version", android.os.Build.VERSION.INCREMENTAL).commit();
                                    com.navdy.hud.app.util.OTAUpdateService.access$000().d(new StringBuilder().append("Trying to the install the update, retry count :").append(i + 1).toString());
                                    try {
                                        com.navdy.hud.app.analytics.AnalyticsSupport.recordInstallOTAUpdate(this.this$0.sharedPreferences);
                                        com.navdy.hud.app.util.CrashReporter.getInstance().stopCrashReporting(true);
                                    } catch(Throwable a2) {
                                        com.navdy.hud.app.util.OTAUpdateService.access$000().e("Exception while reporting the update", a2);
                                    }
                                    com.navdy.hud.app.util.OTAUpdateService.access$000().i("NAVDY_NOT_A_CRASH");
                                    com.navdy.hud.app.obd.ObdDeviceConfigurationManager.turnOffOBDSleep();
                                    com.navdy.hud.app.device.dial.DialManager.getInstance().requestDialReboot(false);
                                    Thread.sleep(2000L);
                                    com.navdy.hud.app.util.OTAUpdateService.access$200(this.this$0, (android.content.Context)this.this$0, a1, this.val$postUpdateCommand);
                                    break label0;
                                } catch(Throwable a3) {
                                    a0 = a3;
                                }
                            }
                            com.navdy.hud.app.util.OTAUpdateService.access$000().e("Exception while running update OTA package", a0);
                            com.navdy.hud.app.util.CrashReporter.getInstance().reportNonFatalException(a0);
                            break label1;
                        } catch(Exception a4) {
                            a = a4;
                        }
                        com.navdy.hud.app.util.OTAUpdateService.access$000().e("Failed to install OTA package", (Throwable)a);
                        break label2;
                    } catch(Throwable a5) {
                        com.navdy.hud.app.util.OTAUpdateService.access$302(this.this$0, false);
                        throw a5;
                    }
                    com.navdy.hud.app.util.OTAUpdateService.access$302(this.this$0, false);
                    break label3;
                }
                com.navdy.hud.app.util.OTAUpdateService.access$302(this.this$0, false);
                break label3;
            }
            com.navdy.hud.app.util.OTAUpdateService.access$302(this.this$0, false);
        }
    }
}
