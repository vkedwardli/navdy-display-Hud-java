package com.navdy.hud.app.util;

public class ImageUtil {
    public ImageUtil() {
    }
    
    public static android.graphics.Bitmap applySaturation(android.graphics.Bitmap a, float f) {
        com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
        android.graphics.Bitmap a0 = android.graphics.Bitmap.createBitmap(a.getWidth(), a.getHeight(), a.getConfig());
        android.graphics.Canvas a1 = new android.graphics.Canvas(a0);
        android.graphics.Paint a2 = new android.graphics.Paint();
        android.graphics.ColorMatrix a3 = new android.graphics.ColorMatrix();
        a3.setSaturation(f);
        a2.setColorFilter((android.graphics.ColorFilter)new android.graphics.ColorMatrixColorFilter(a3));
        a1.drawBitmap(a, 0.0f, 0.0f, a2);
        return a0;
    }
    
    public static android.graphics.Bitmap blend(android.graphics.Bitmap a, android.graphics.Bitmap a0) {
        com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
        android.graphics.Bitmap a1 = a.copy(a.getConfig(), true);
        android.graphics.Paint a2 = new android.graphics.Paint();
        a2.setXfermode((android.graphics.Xfermode)new android.graphics.PorterDuffXfermode(android.graphics.PorterDuff$Mode.MULTIPLY));
        new android.graphics.Canvas(a1).drawBitmap(a0, 0.0f, 0.0f, a2);
        return a1;
    }
    
    public static android.graphics.Bitmap hueShift(android.graphics.Bitmap a, float f) {
        android.graphics.Bitmap a0 = android.graphics.Bitmap.createBitmap(a.getWidth(), a.getHeight(), a.getConfig());
        android.graphics.Canvas a1 = new android.graphics.Canvas(a0);
        android.graphics.Paint a2 = new android.graphics.Paint();
        android.graphics.ColorMatrix a3 = new android.graphics.ColorMatrix();
        float f0 = Math.min(f, Math.max(-f, 180f)) / 180f * 3.1415927410125732f;
        if (f0 != 0.0f) {
            float f1 = (float)Math.cos((double)f0);
            float f2 = (float)Math.sin((double)f0);
            float[] a4 = new float[25];
            a4[0] = (1f - 0.213f) * f1 + 0.213f + -0.213f * f2;
            a4[1] = -0.715f * f1 + 0.715f + -0.715f * f2;
            a4[2] = -0.072f * f1 + 0.072f + (1f - 0.072f) * f2;
            a4[3] = 0.0f;
            a4[4] = 0.0f;
            a4[5] = -0.213f * f1 + 0.213f + 0.143f * f2;
            a4[6] = (1f - 0.715f) * f1 + 0.715f + 0.14f * f2;
            a4[7] = -0.072f * f1 + 0.072f + -0.283f * f2;
            a4[8] = 0.0f;
            a4[9] = 0.0f;
            a4[10] = -0.213f * f1 + 0.213f + -(1f - 0.213f) * f2;
            a4[11] = -0.715f * f1 + 0.715f + f2 * 0.715f;
            a4[12] = (1f - 0.072f) * f1 + 0.072f + f2 * 0.072f;
            a4[13] = 0.0f;
            a4[14] = 0.0f;
            a4[15] = 0.0f;
            a4[16] = 0.0f;
            a4[17] = 0.0f;
            a4[18] = 1f;
            a4[19] = 0.0f;
            a4[20] = 0.0f;
            a4[21] = 0.0f;
            a4[22] = 0.0f;
            a4[23] = 0.0f;
            a4[24] = 1f;
            a3.postConcat(new android.graphics.ColorMatrix(a4));
            a2.setColorFilter((android.graphics.ColorFilter)new android.graphics.ColorMatrixColorFilter(a3));
            a1.drawBitmap(a, 0.0f, 0.0f, a2);
        } else {
            a0 = null;
        }
        return a0;
    }
}
