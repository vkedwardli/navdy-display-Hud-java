package com.navdy.hud.app.util;

class FeatureUtil$1 extends android.content.BroadcastReceiver {
    final com.navdy.hud.app.util.FeatureUtil this$0;
    
    FeatureUtil$1(com.navdy.hud.app.util.FeatureUtil a) {
        super();
        this.this$0 = a;
    }
    
    public void onReceive(android.content.Context a, android.content.Intent a0) {
        try {
            if ("com.navdy.hud.app.feature".equalsIgnoreCase(a0.getAction())) {
                android.os.Bundle a1 = a0.getExtras();
                if (a1 != null && a1.size() == 1) {
                    com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.util.FeatureUtil$1$1(this, a1), 1);
                }
            }
        } catch(Throwable a2) {
            com.navdy.hud.app.util.FeatureUtil.access$100().e(a2);
        }
    }
}
