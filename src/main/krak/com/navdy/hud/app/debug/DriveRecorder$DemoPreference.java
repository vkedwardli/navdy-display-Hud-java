package com.navdy.hud.app.debug;


public enum DriveRecorder$DemoPreference {
    NA(0),
    DISABLED(1),
    ENABLED(2);

    private int value;
    DriveRecorder$DemoPreference(int value) {
        this.value = value;
    }
    public int getValue() {
        return value;
    }
}

//final public class DriveRecorder$DemoPreference extends Enum {
//    final private static com.navdy.hud.app.debug.DriveRecorder$DemoPreference[] $VALUES;
//    final public static com.navdy.hud.app.debug.DriveRecorder$DemoPreference DISABLED;
//    final public static com.navdy.hud.app.debug.DriveRecorder$DemoPreference ENABLED;
//    final public static com.navdy.hud.app.debug.DriveRecorder$DemoPreference NA;
//    
//    static {
//        NA = new com.navdy.hud.app.debug.DriveRecorder$DemoPreference("NA", 0);
//        DISABLED = new com.navdy.hud.app.debug.DriveRecorder$DemoPreference("DISABLED", 1);
//        ENABLED = new com.navdy.hud.app.debug.DriveRecorder$DemoPreference("ENABLED", 2);
//        com.navdy.hud.app.debug.DriveRecorder$DemoPreference[] a = new com.navdy.hud.app.debug.DriveRecorder$DemoPreference[3];
//        a[0] = NA;
//        a[1] = DISABLED;
//        a[2] = ENABLED;
//        $VALUES = a;
//    }
//    
//    private DriveRecorder$DemoPreference(String s, int i) {
//        super(s, i);
//    }
//    
//    public static com.navdy.hud.app.debug.DriveRecorder$DemoPreference valueOf(String s) {
//        return (com.navdy.hud.app.debug.DriveRecorder$DemoPreference)Enum.valueOf(com.navdy.hud.app.debug.DriveRecorder$DemoPreference.class, s);
//    }
//    
//    public static com.navdy.hud.app.debug.DriveRecorder$DemoPreference[] values() {
//        return $VALUES.clone();
//    }
//}
//