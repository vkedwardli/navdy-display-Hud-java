package com.navdy.hud.app.debug;

class RouteRecorder$6 implements Runnable {
    final com.navdy.hud.app.debug.RouteRecorder this$0;
    final String val$fileName;
    final boolean val$secondary;
    
    RouteRecorder$6(com.navdy.hud.app.debug.RouteRecorder a, String s, boolean b) {
        super();
        this.this$0 = a;
        this.val$fileName = s;
        this.val$secondary = b;
    }
    
    public void run() {
        if (this.this$0.bPrepare(this.val$fileName, this.val$secondary)) {
            com.navdy.hud.app.debug.RouteRecorder.sLogger.e("Prepare succeeded ");
        } else {
            com.navdy.hud.app.debug.RouteRecorder.sLogger.e("Prepare failed ");
        }
    }
}
