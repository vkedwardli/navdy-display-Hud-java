package com.navdy.hud.app.debug;

final public class GestureEngine$$InjectAdapter extends dagger.internal.Binding implements dagger.MembersInjector {
    private dagger.internal.Binding gestureServiceConnector;
    
    public GestureEngine$$InjectAdapter() {
        super((String)null, "members/com.navdy.hud.app.debug.GestureEngine", false, com.navdy.hud.app.debug.GestureEngine.class);
    }
    
    public void attach(dagger.internal.Linker a) {
        this.gestureServiceConnector = a.requestBinding("com.navdy.hud.app.gesture.GestureServiceConnector", com.navdy.hud.app.debug.GestureEngine.class, (this).getClass().getClassLoader());
    }
    
    public void getDependencies(java.util.Set a, java.util.Set a0) {
        a0.add(this.gestureServiceConnector);
    }
    
    public void injectMembers(com.navdy.hud.app.debug.GestureEngine a) {
        a.gestureServiceConnector = (com.navdy.hud.app.gesture.GestureServiceConnector)this.gestureServiceConnector.get();
    }
    
    public void injectMembers(Object a) {
        this.injectMembers((com.navdy.hud.app.debug.GestureEngine)a);
    }
}
