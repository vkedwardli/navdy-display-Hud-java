package com.navdy.hud.app.debug;

class DriveRecorder$7 implements Runnable {
    final com.navdy.hud.app.debug.DriveRecorder this$0;
    final String val$fileName;
    
    DriveRecorder$7(com.navdy.hud.app.debug.DriveRecorder a, String s) {
        super();
        this.this$0 = a;
        this.val$fileName = s;
    }
    
    public void run() {
        java.io.File a = com.navdy.hud.app.debug.DriveRecorder.getDriveLogsDir(this.val$fileName);
        if (!a.exists()) {
            a.mkdirs();
        }
        java.io.File a0 = new java.io.File(a, this.val$fileName);
        java.io.File a1 = new java.io.File(new StringBuilder().append(a0.getAbsolutePath()).append(".obd").toString());
        java.io.File a2 = new java.io.File(new StringBuilder().append(a0.getAbsolutePath()).append(".drive").toString());
        try {
            if (a1.createNewFile()) {
                this.this$0.obdAsyncBufferedFileWriter = new com.navdy.hud.app.debug.AsyncBufferedFileWriter(a1.getAbsolutePath(), (com.navdy.hud.app.debug.SerialExecutor)this.this$0, 250);
                com.navdy.hud.app.obd.ObdManager.getInstance().setRecordingPidListener((com.navdy.obd.IPidListener)null);
                com.navdy.hud.app.debug.DriveRecorder.access$800(this.this$0);
                com.navdy.hud.app.obd.ObdManager.getInstance().setRecordingPidListener(com.navdy.hud.app.debug.DriveRecorder.access$900(this.this$0));
            }
            if (a2.createNewFile()) {
                this.this$0.driveScoreDataBufferedFileWriter = new com.navdy.hud.app.debug.AsyncBufferedFileWriter(a2.getAbsolutePath(), (com.navdy.hud.app.debug.SerialExecutor)this.this$0, 500);
            }
        } catch(java.io.IOException a3) {
            com.navdy.hud.app.debug.DriveRecorder.sLogger.e((Throwable)a3);
            com.navdy.hud.app.debug.DriveRecorder.access$002(this.this$0, false);
            com.navdy.hud.app.debug.DriveRecorder.access$102(this.this$0, false);
        }
    }
}
