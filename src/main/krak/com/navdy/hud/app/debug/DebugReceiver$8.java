package com.navdy.hud.app.debug;

class DebugReceiver$8 implements Runnable {
    final com.navdy.hud.app.debug.DebugReceiver this$0;
    
    DebugReceiver$8(com.navdy.hud.app.debug.DebugReceiver a) {
        super();
        this.this$0 = a;
    }
    
    public void run() {
        label0: {
            Throwable a = null;
            label1: {
                String[] a0 = null;
                try {
                    a0 = new String[10];
                } catch(OutOfMemoryError a1) {
                    a = a1;
                    break label1;
                }
                a0[0] = "version.hybrid.api.here.com";
                a0[1] = "reverse.geocoder.api.here.com";
                a0[2] = "tpeg.traffic.api.here.com";
                a0[3] = "tpeg.hybrid.api.here.com";
                a0[4] = "download.hybrid.api.here.com";
                a0[5] = "v102-62-30-8.route.hybrid.api.here.com";
                a0[6] = "analytics.localytics.com";
                a0[7] = "profile.localytics.com";
                a0[8] = "sdk.hockeyapp.net";
                a0[9] = "navdyhud.atlassian.net";
                com.navdy.service.library.log.Logger a2 = com.navdy.hud.app.debug.DebugReceiver.sLogger;
                try {
                    a2.v("dns_lookup_test");
                    int i = 0;
                    while(i < a0.length) {
                        try {
                            java.net.InetAddress[] a3 = java.net.InetAddress.getAllByName(a0[i]);
                            if (a3 == null) {
                                com.navdy.hud.app.debug.DebugReceiver.sLogger.v(new StringBuilder().append("dns_lookup_test[").append(a0[i]).append("] no address").toString());
                            } else {
                                com.navdy.hud.app.debug.DebugReceiver.sLogger.v(new StringBuilder().append("dns_lookup_test[").append(a0[i]).append("] addresses = ").append(a3.length).toString());
                                int i0 = 0;
                                while(i0 < a3.length) {
                                    com.navdy.hud.app.debug.DebugReceiver.sLogger.v(new StringBuilder().append("dns_lookup_test[").append(a0[i]).append("]  addr[").append(a3[i0].getHostAddress()).append("]").toString());
                                    i0 = i0 + 1;
                                }
                            }
                        } catch(Throwable a4) {
                            com.navdy.hud.app.debug.DebugReceiver.sLogger.v("dns_lookup_test", a4);
                        }
                        i = i + 1;
                    }
                    break label0;
                } catch(Throwable a5) {
                    a = a5;
                }
            }
            com.navdy.hud.app.debug.DebugReceiver.sLogger.e(a);
        }
    }
}
