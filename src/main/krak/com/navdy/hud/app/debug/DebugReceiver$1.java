package com.navdy.hud.app.debug;

class DebugReceiver$1 implements Runnable {
    final com.navdy.hud.app.debug.DebugReceiver this$0;
    
    DebugReceiver$1(com.navdy.hud.app.debug.DebugReceiver a) {
        super();
        this.this$0 = a;
    }
    
    public void run() {
        com.navdy.hud.app.maps.here.HereMapsManager a = com.navdy.hud.app.maps.here.HereMapsManager.getInstance();
        if (a.isInitialized()) {
            com.navdy.hud.app.maps.here.HereMapController a0 = a.getMapController();
            com.navdy.hud.app.debug.DebugReceiver.sLogger.v(new StringBuilder().append("[MAP-ZOOM] Zoom[").append(a0.getZoomLevel()).append("] Tilt[").append(a0.getTilt()).append("]").toString());
        } else {
            com.navdy.hud.app.debug.DebugReceiver.sLogger.v("[MAP-ZOOM] HERE Maps engine not initialized yet");
        }
    }
}
