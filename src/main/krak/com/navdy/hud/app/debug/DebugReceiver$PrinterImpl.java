package com.navdy.hud.app.debug;

class DebugReceiver$PrinterImpl implements android.util.Printer {
    private DebugReceiver$PrinterImpl() {
    }
    
    DebugReceiver$PrinterImpl(com.navdy.hud.app.debug.DebugReceiver$1 a) {
        this();
    }
    
    public void println(String s) {
        com.navdy.hud.app.debug.DebugReceiver.sLogger.v(s);
    }
}
