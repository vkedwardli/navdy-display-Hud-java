package com.navdy.hud.app.debug;

class DebugReceiver$7 implements Runnable {
    final com.navdy.hud.app.debug.DebugReceiver this$0;
    final String val$host;
    
    DebugReceiver$7(com.navdy.hud.app.debug.DebugReceiver a, String s) {
        super();
        this.this$0 = a;
        this.val$host = s;
    }
    
    public void run() {
        try {
            String s = this.val$host;
            if (s == null) {
                s = "analytics.localytics.com";
            }
            com.navdy.hud.app.debug.DebugReceiver.sLogger.v(new StringBuilder().append("dnslookup :").append(s).toString());
            java.net.InetAddress[] a = java.net.InetAddress.getAllByName(s);
            if (a == null) {
                com.navdy.hud.app.debug.DebugReceiver.sLogger.v("dnslookup no address");
            } else {
                com.navdy.hud.app.debug.DebugReceiver.sLogger.v(new StringBuilder().append("dnslookup total:").append(a.length).toString());
                int i = 0;
                while(i < a.length) {
                    com.navdy.hud.app.debug.DebugReceiver.sLogger.v(new StringBuilder().append("dnslookup address :").append(a[i].getHostAddress()).toString());
                    i = i + 1;
                }
            }
        } catch(Throwable a0) {
            com.navdy.hud.app.debug.DebugReceiver.sLogger.e(a0);
        }
    }
}
