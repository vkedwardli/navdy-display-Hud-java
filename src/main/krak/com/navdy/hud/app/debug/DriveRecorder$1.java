package com.navdy.hud.app.debug;

class DriveRecorder$1 implements Runnable {
    final com.navdy.hud.app.debug.DriveRecorder this$0;
    
    DriveRecorder$1(com.navdy.hud.app.debug.DriveRecorder a) {
        super();
        this.this$0 = a;
    }
    
    public void run() {
        com.navdy.hud.app.debug.DriveRecorder.access$002(this.this$0, false);
        com.navdy.hud.app.debug.DriveRecorder.access$102(this.this$0, false);
        com.navdy.hud.app.obd.ObdManager.getInstance().setRecordingPidListener((com.navdy.obd.IPidListener)null);
        this.this$0.obdAsyncBufferedFileWriter.flushAndClose();
        this.this$0.driveScoreDataBufferedFileWriter.flushAndClose();
    }
}
