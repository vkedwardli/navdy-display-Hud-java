package com.navdy.hud.app.debug;

public class DriveRecorder implements android.content.ServiceConnection, com.navdy.hud.app.debug.SerialExecutor {
    final private static String AUTO_DRIVERECORD_PROP = "persist.sys.driverecord.auto";
    final public static String AUTO_RECORD_LABEL = "auto";
    final public static int BUFFER_SIZE = 500;
    final public static int BUFFER_SIZE_OBD_LOG = 250;
    final public static String DEMO_LOG_FILE_NAME = "demo_log.log";
    final public static String DRIVE_LOGS_FOLDER = "drive_logs";
    final public static int MAXIMUM_FILES_IN_THE_FOLDER = 20;
    final public static com.navdy.service.library.log.Logger sLogger;
    com.squareup.otto.Bus bus;
    private com.navdy.hud.app.service.ConnectionHandler connectionHandler;
    final private android.content.Context context;
    private volatile int currentObdDataInjectionIndex;
    boolean demoObdLogFileExists;
    boolean demoRouteLogFileExists;
    com.navdy.hud.app.debug.AsyncBufferedFileWriter driveScoreDataBufferedFileWriter;
    private StringBuilder gforceDataBuilder;
    final private android.os.Handler handler;
    final private Runnable injectFakeObdData;
    private boolean isEngineInitialized;
    private volatile boolean isLooping;
    private volatile boolean isRecording;
    private volatile boolean isSupportedPidsWritten;
    com.navdy.hud.app.debug.AsyncBufferedFileWriter obdAsyncBufferedFileWriter;
    private android.os.Handler obdDataPlaybackHandler;
    private android.os.HandlerThread obdPlaybackThread;
    final private com.navdy.obd.IPidListener pidListener;
    private volatile com.navdy.hud.app.debug.DriveRecorder$State playbackState;
    private volatile long preparedFileLastModifiedTime;
    private volatile String preparedFileName;
    volatile boolean realObdConnected;
    private java.util.List recordedObdData;
    private com.navdy.hud.app.debug.IRouteRecorder routeRecorder;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.debug.DriveRecorder.class);
    }
    
    public DriveRecorder(com.squareup.otto.Bus a, com.navdy.hud.app.service.ConnectionHandler a0) {
        this.gforceDataBuilder = new StringBuilder(40);
        this.playbackState = com.navdy.hud.app.debug.DriveRecorder$State.STOPPED;
        this.isLooping = false;
        this.realObdConnected = false;
        this.demoRouteLogFileExists = false;
        this.demoObdLogFileExists = false;
        this.injectFakeObdData = (Runnable)new com.navdy.hud.app.debug.DriveRecorder$5(this);
        this.pidListener = (com.navdy.obd.IPidListener)new com.navdy.hud.app.debug.DriveRecorder$6(this);
        this.context = com.navdy.hud.app.HudApplication.getAppContext();
        this.handler = new android.os.Handler(android.os.Looper.getMainLooper());
        this.connectionHandler = a0;
        this.isRecording = false;
        this.playbackState = com.navdy.hud.app.debug.DriveRecorder$State.STOPPED;
        this.isSupportedPidsWritten = false;
        this.bus = a;
        this.bus.register(this);
        com.navdy.hud.app.obd.ObdManager.getInstance().setDriveRecorder(this);
        android.content.Intent a1 = new android.content.Intent(this.context, com.navdy.hud.app.debug.RouteRecorderService.class);
        this.context.bindService(a1, (android.content.ServiceConnection)this, 1);
    }
    
    static boolean access$000(com.navdy.hud.app.debug.DriveRecorder a) {
        return a.isRecording;
    }
    
    static boolean access$002(com.navdy.hud.app.debug.DriveRecorder a, boolean b) {
        a.isRecording = b;
        return b;
    }
    
    static boolean access$100(com.navdy.hud.app.debug.DriveRecorder a) {
        return a.isSupportedPidsWritten;
    }
    
    static void access$1000(com.navdy.hud.app.debug.DriveRecorder a, boolean b) {
        a.playDemo(b);
    }
    
    static boolean access$102(com.navdy.hud.app.debug.DriveRecorder a, boolean b) {
        a.isSupportedPidsWritten = b;
        return b;
    }
    
    static void access$1100(com.navdy.hud.app.debug.DriveRecorder a) {
        a.pauseDemo();
    }
    
    static void access$1200(com.navdy.hud.app.debug.DriveRecorder a) {
        a.resumeDemo();
    }
    
    static void access$1300(com.navdy.hud.app.debug.DriveRecorder a, boolean b) {
        a.restartDemo(b);
    }
    
    static void access$1400(com.navdy.hud.app.debug.DriveRecorder a) {
        a.stopDemo();
    }
    
    static void access$1500(com.navdy.hud.app.debug.DriveRecorder a) {
        a.prepareDemo();
    }
    
    static com.navdy.hud.app.service.ConnectionHandler access$1600(com.navdy.hud.app.debug.DriveRecorder a) {
        return a.connectionHandler;
    }
    
    static int access$200(com.navdy.hud.app.debug.DriveRecorder a) {
        return a.currentObdDataInjectionIndex;
    }
    
    static int access$202(com.navdy.hud.app.debug.DriveRecorder a, int i) {
        a.currentObdDataInjectionIndex = i;
        return i;
    }
    
    static int access$204(com.navdy.hud.app.debug.DriveRecorder a) {
        int i = a.currentObdDataInjectionIndex + 1;
        a.currentObdDataInjectionIndex = i;
        return i;
    }
    
    static android.os.HandlerThread access$300(com.navdy.hud.app.debug.DriveRecorder a) {
        return a.obdPlaybackThread;
    }
    
    static android.os.HandlerThread access$302(com.navdy.hud.app.debug.DriveRecorder a, android.os.HandlerThread a0) {
        a.obdPlaybackThread = a0;
        return a0;
    }
    
    static android.os.Handler access$400(com.navdy.hud.app.debug.DriveRecorder a) {
        return a.obdDataPlaybackHandler;
    }
    
    static android.os.Handler access$402(com.navdy.hud.app.debug.DriveRecorder a, android.os.Handler a0) {
        a.obdDataPlaybackHandler = a0;
        return a0;
    }
    
    static Runnable access$500(com.navdy.hud.app.debug.DriveRecorder a) {
        return a.injectFakeObdData;
    }
    
    static java.util.List access$600(com.navdy.hud.app.debug.DriveRecorder a) {
        return a.recordedObdData;
    }
    
    static boolean access$700(com.navdy.hud.app.debug.DriveRecorder a) {
        return a.isLooping;
    }
    
    static void access$800(com.navdy.hud.app.debug.DriveRecorder a) {
        a.bRecordSupportedPids();
    }
    
    static com.navdy.obd.IPidListener access$900(com.navdy.hud.app.debug.DriveRecorder a) {
        return a.pidListener;
    }
    
    private void bRecordSupportedPids() {
        com.navdy.hud.app.obd.ObdManager a = com.navdy.hud.app.obd.ObdManager.getInstance();
        com.navdy.obd.PidSet a0 = a.getSupportedPids();
        if (a0 != null) {
            java.util.List a1 = a0.asList();
            label2: {
                label0: {
                    label1: {
                        if (a1 == null) {
                            break label1;
                        }
                        if (a1.size() > 0) {
                            break label0;
                        }
                    }
                    this.obdAsyncBufferedFileWriter.write("-1\n");
                    break label2;
                }
                Object a2 = a1.iterator();
                Object a3 = a1;
                while(((java.util.Iterator)a2).hasNext()) {
                    com.navdy.obd.Pid a4 = (com.navdy.obd.Pid)((java.util.Iterator)a2).next();
                    if (a4 == ((java.util.List)a3).get(((java.util.List)a3).size() - 1)) {
                        this.obdAsyncBufferedFileWriter.write(new StringBuilder().append(a4.getId()).append("\n").toString());
                    } else {
                        this.obdAsyncBufferedFileWriter.write(new StringBuilder().append(a4.getId()).append(",").toString());
                    }
                }
            }
            this.obdAsyncBufferedFileWriter.write(new StringBuilder().append(System.currentTimeMillis()).append(",").toString());
            this.obdAsyncBufferedFileWriter.write(new StringBuilder().append("47:").append(a.getFuelLevel()).append(",").toString());
            this.obdAsyncBufferedFileWriter.write(new StringBuilder().append("13:").append(com.navdy.hud.app.manager.SpeedManager.getInstance().getObdSpeed()).append(",").toString());
            this.obdAsyncBufferedFileWriter.write(new StringBuilder().append("12:").append(a.getEngineRpm()).append(",").toString());
            this.obdAsyncBufferedFileWriter.write(new StringBuilder().append("256:").append(a.getInstantFuelConsumption()).append("\n").toString(), true);
            this.isSupportedPidsWritten = true;
            sLogger.d("Obd data record created successfully and starting to listening to the PID changes");
        }
    }
    
    private void checkAndStartAutoRecording() {
        if (com.navdy.hud.app.debug.DriveRecorder.isAutoRecordingEnabled()) {
            com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.debug.DriveRecorder$9(this), 9);
        }
    }
    
    private Runnable createDriveRecord(String s) {
        return (Runnable)new com.navdy.hud.app.debug.DriveRecorder$7(this, s);
    }
    
    public static java.io.File getDriveLogsDir(String s) {
        return ("demo_log.log".equals(s)) ? new java.io.File(com.navdy.hud.app.storage.PathManager.getInstance().getMapsPartitionPath()) : new java.io.File(new StringBuilder().append(com.navdy.hud.app.storage.PathManager.getInstance().getMapsPartitionPath()).append(java.io.File.separator).append("drive_logs").toString());
    }
    
    private static double getLatitude() {
        double d = 0.0;
        boolean b = com.navdy.hud.app.maps.here.HereMapsManager.getInstance().isInitialized();
        label2: {
            com.here.android.mpa.common.GeoCoordinate a = null;
            label0: {
                label1: {
                    if (!b) {
                        break label1;
                    }
                    a = com.navdy.hud.app.maps.here.HereMapsManager.getInstance().getLocationFixManager().getLastGeoCoordinate();
                    if (a != null) {
                        break label0;
                    }
                }
                d = 0.0;
                break label2;
            }
            d = a.getLatitude();
        }
        return d;
    }
    
    private static double getLongitude() {
        double d = 0.0;
        boolean b = com.navdy.hud.app.maps.here.HereMapsManager.getInstance().isInitialized();
        label2: {
            com.here.android.mpa.common.GeoCoordinate a = null;
            label0: {
                label1: {
                    if (!b) {
                        break label1;
                    }
                    a = com.navdy.hud.app.maps.here.HereMapsManager.getInstance().getLocationFixManager().getLastGeoCoordinate();
                    if (a != null) {
                        break label0;
                    }
                }
                d = 0.0;
                break label2;
            }
            d = a.getLongitude();
        }
        return d;
    }
    
    public static boolean isAutoRecordingEnabled() {
        boolean b = false;
        boolean b0 = com.navdy.hud.app.util.os.SystemProperties.getBoolean("persist.sys.driverecord.auto", false);
        label0: {
            label1: {
                if (b0) {
                    break label1;
                }
                if (com.navdy.hud.app.util.DeviceUtil.isUserBuild()) {
                    b = false;
                    break label0;
                }
            }
            b = true;
        }
        return b;
    }
    
    private boolean isPaused() {
        return this.playbackState == com.navdy.hud.app.debug.DriveRecorder$State.PAUSED;
    }
    
    private boolean isPlaying() {
        return this.playbackState == com.navdy.hud.app.debug.DriveRecorder$State.PLAYING;
    }
    
    private boolean isStopped() {
        return this.playbackState == com.navdy.hud.app.debug.DriveRecorder$State.STOPPED;
    }
    
    private void pauseDemo() {
        this.stopRecordingBeforeDemo();
        if (this.isPlaying()) {
            this.pausePlayback();
        }
        try {
            if (this.routeRecorder.isPlaying()) {
                this.routeRecorder.pausePlayback();
            }
        } catch(Exception ignoredException) {
            sLogger.e("Error pausing the route playback");
        }
    }
    
    private void playDemo(boolean b) {
        this.stopRecordingBeforeDemo();
        if (!this.isPlaying()) {
            if (this.isPaused()) {
                this.resumePlayback();
            } else {
                this.startPlayback("demo_log.log", b);
            }
        }
        try {
            if (this.routeRecorder.isPaused()) {
                this.routeRecorder.resumePlayback();
            } else if (!this.routeRecorder.isPlaying()) {
                this.routeRecorder.startPlayback("demo_log.log", false, b);
            }
        } catch(Throwable a) {
            sLogger.e("Error playing the route playback", a);
        }
    }
    
    private void prepareDemo() {
        this.prepare("demo_log.log");
        try {
            this.routeRecorder.prepare("demo_log.log", false);
        } catch(Throwable a) {
            sLogger.e("Error preparing the route recorder for demo", a);
        }
    }
    
    private void restartDemo(boolean b) {
        this.stopRecordingBeforeDemo();
        if (!this.restartPlayback()) {
            this.startPlayback("demo_log.log", b);
        }
        try {
            if (!this.routeRecorder.restartPlayback()) {
                this.routeRecorder.startPlayback("demo_log.log", false, b);
            }
        } catch(Exception a) {
            sLogger.e("Error restarting the route playback", (Throwable)a);
        }
    }
    
    private void resumeDemo() {
        this.stopRecordingBeforeDemo();
        if (this.isPaused()) {
            this.resumePlayback();
        }
        try {
            if (this.routeRecorder.isPaused()) {
                this.routeRecorder.resumePlayback();
            }
        } catch(Exception a) {
            sLogger.e("Error resuming the route playback", (Throwable)a);
        }
    }
    
    private void stopDemo() {
        if (this.isPlaying()) {
            this.stopPlayback();
        }
        label0: try {
            boolean b = this.routeRecorder.isPlaying();
            label1: {
                if (b) {
                    break label1;
                }
                if (!this.routeRecorder.isPaused()) {
                    break label0;
                }
            }
            this.routeRecorder.stopPlayback();
        } catch(Exception a) {
            sLogger.e("Error stopping the route playback", (Throwable)a);
        }
    }
    
    private void stopRecordingBeforeDemo() {
        boolean b = this.isRecording;
        label1: {
            InterruptedException a = null;
            if (!b) {
                break label1;
            }
            this.stopRecording();
            try {
                Thread.sleep(1000L);
                break label1;
            } catch(InterruptedException a0) {
                a = a0;
            }
            sLogger.e("Interrupted ", (Throwable)a);
        }
        label0: try {
            if (!this.routeRecorder.isRecording()) {
                break label0;
            }
            this.routeRecorder.stopRecording();
            try {
                Thread.sleep(1000L);
                break label0;
            } catch(InterruptedException ignoredException) {
            }
            sLogger.e("Interrupted while stopping the recording");
        } catch(Exception a1) {
            sLogger.e("Exception while stopping the recording", (Throwable)a1);
        }
    }
    
    public boolean bPrepare(String s) {
        boolean b = false;
        java.io.File a = new java.io.File(com.navdy.hud.app.debug.DriveRecorder.getDriveLogsDir(s), new StringBuilder().append(s).append(".obd").toString());
        boolean b0 = a.exists();
        label0: {
            java.io.BufferedReader a0 = null;
            Throwable a1 = null;
            label1: {
                java.io.BufferedReader a2 = null;
                label5: {
                    if (b0) {
                        com.navdy.obd.PidSet a3 = null;
                        long j = a.lastModified();
                        boolean b1 = android.text.TextUtils.equals((CharSequence)this.preparedFileName, (CharSequence)s);
                        label6: {
                            if (!b1) {
                                break label6;
                            }
                            if (this.preparedFileLastModifiedTime != j) {
                                break label6;
                            }
                            sLogger.d("File already prepared");
                            b = true;
                            break label0;
                        }
                        this.preparedFileName = s;
                        this.preparedFileLastModifiedTime = j;
                        label4: {
                            String[] a4 = null;
                            int i = 0;
                            int i0 = 0;
                            label3: try {
                                a0 = null;
                                a0 = null;
                                a0 = null;
                                a0 = null;
                                java.io.FileInputStream a5 = new java.io.FileInputStream(a);
                                a0 = null;
                                java.io.InputStreamReader a6 = new java.io.InputStreamReader((java.io.InputStream)a5, "utf-8");
                                a0 = null;
                                a2 = new java.io.BufferedReader((java.io.Reader)a6);
                                com.navdy.service.library.log.Logger a7 = sLogger;
                                try {
                                    a7.v("startPlayback of Obd data");
                                    this.recordedObdData = (java.util.List)new java.util.ArrayList();
                                    a0 = new java.io.BufferedReader((java.io.Reader)new java.io.InputStreamReader((java.io.InputStream)new java.io.FileInputStream(a), "utf-8"));
                                } catch(Throwable a8) {
                                    a1 = a8;
                                    break label5;
                                }
                                String s0 = a0.readLine();
                                a3 = new com.navdy.obd.PidSet();
                                if (s0 == null) {
                                    break label4;
                                }
                                sLogger.d(new StringBuilder().append("Supported pids recorded ").append(s0).toString());
                                boolean b2 = s0.trim().equals("-1");
                                label2: {
                                    if (b2) {
                                        break label2;
                                    }
                                    a4 = s0.split(",");
                                    i = a4.length;
                                    i0 = 0;
                                    break label3;
                                }
                                sLogger.d("There are no supported pids when trying to playback the data");
                                break label4;
                            } catch(Throwable a9) {
                                a1 = a9;
                                break label1;
                            }
                            while(i0 < i) {
                                String s1 = a4[i0];
                                try {
                                    try {
                                        a3.add(Integer.parseInt(s1));
                                    } catch(Exception ignoredException) {
                                        sLogger.e("Exception while parsing the supported Pids");
                                    }
                                    i0 = i0 + 1;
                                } catch(Throwable a10) {
                                    a1 = a10;
                                    break label1;
                                }
                            }
                        }
                        try {
                            com.navdy.hud.app.obd.ObdManager.getInstance().setSupportedPidSet(a3);
                            while(true) {
                                NumberFormatException a11 = null;
                                String s2 = a0.readLine();
                                if (s2 == null) {
                                    break;
                                }
                                String[] a12 = s2.split(",");
                                if (a12 == null) {
                                    continue;
                                }
                                if (a12.length <= 1) {
                                    continue;
                                }
                                try {
                                    long j0 = Long.parseLong(a12[0]);
                                    java.util.ArrayList a13 = new java.util.ArrayList();
                                    int i1 = 1;
                                    while(i1 < a12.length) {
                                        String[] a14 = a12[i1].split(":");
                                        int i2 = Integer.parseInt(a14[0]);
                                        double d = Double.parseDouble(a14[1]);
                                        com.navdy.obd.Pid a15 = new com.navdy.obd.Pid(i2);
                                        a15.setValue(d);
                                        ((java.util.List)a13).add(a15);
                                        this.recordedObdData.add(new java.util.AbstractMap$SimpleEntry(Long.valueOf(j0), a13));
                                        i1 = i1 + 1;
                                    }
                                    continue;
                                } catch(NumberFormatException a16) {
                                    a11 = a16;
                                }
                                sLogger.e("Error parsing the Obd data from the file ", (Throwable)a11);
                            }
                        } catch(Throwable a17) {
                            a1 = a17;
                            break label1;
                        }
                        com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a0);
                        b = true;
                        break label0;
                    } else {
                        b = false;
                        break label0;
                    }
                }
                a0 = a2;
            }
            try {
                this.stopPlayback();
                sLogger.e(a1);
            } catch(Throwable a18) {
                com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a0);
                throw a18;
            }
            com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a0);
            b = false;
        }
        return b;
    }
    
    public void execute(Runnable a) {
        com.navdy.service.library.task.TaskManager.getInstance().execute(a, 9);
    }
    
    public void flushRecordings() {
        this.handler.post((Runnable)new com.navdy.hud.app.debug.DriveRecorder$4(this));
    }
    
    public boolean isDemoAvailable() {
        boolean b = false;
        boolean b0 = this.demoRouteLogFileExists;
        label1: {
            label0: {
                if (!b0) {
                    break label0;
                }
                if (this.realObdConnected) {
                    break label0;
                }
                b = true;
                break label1;
            }
            sLogger.d((this.demoRouteLogFileExists) ? "Obd has been connected so skipping the demo" : "Demo log file does not exist in the map partition. So Demo preference : NA");
            b = false;
        }
        return b;
    }
    
    public boolean isDemoPaused() {
        boolean b = false;
        label0: try {
            boolean b0 = this.isPaused();
            label1: {
                if (b0) {
                    break label1;
                }
                if (!this.routeRecorder.isPaused()) {
                    b = false;
                    break label0;
                }
            }
            b = true;
        } catch(Throwable a) {
            sLogger.e(a);
            b = false;
        }
        return b;
    }
    
    public boolean isDemoPlaying() {
        boolean b = false;
        label0: try {
            boolean b0 = this.isPlaying();
            label1: {
                if (b0) {
                    break label1;
                }
                if (!this.routeRecorder.isPlaying()) {
                    b = false;
                    break label0;
                }
            }
            b = true;
        } catch(Throwable a) {
            sLogger.e(a);
            b = false;
        }
        return b;
    }
    
    public boolean isDemoStopped() {
        boolean b = false;
        try {
            b = this.isStopped() && this.routeRecorder.isStopped();
        } catch(Throwable a) {
            sLogger.e(a);
            b = false;
        }
        return b;
    }
    
    public void load() {
        sLogger.d("Loading the Obd Data recorder");
        this.demoRouteLogFileExists = new java.io.File(new StringBuilder().append(com.navdy.hud.app.storage.PathManager.getInstance().getMapsPartitionPath()).append(java.io.File.separator).append("demo_log.log").toString()).exists();
        sLogger.d(new StringBuilder().append("Demo file exists : ").append(this.demoRouteLogFileExists).toString());
        this.demoObdLogFileExists = new java.io.File(new StringBuilder().append(com.navdy.hud.app.storage.PathManager.getInstance().getMapsPartitionPath()).append(java.io.File.separator).append("demo_log.log").append(".obd").toString()).exists();
        sLogger.d(new StringBuilder().append("Demo obd file exists : ").append(this.demoObdLogFileExists).toString());
    }
    
    public void onCalibratedGForceData(com.navdy.hud.app.device.gps.CalibratedGForceData a) {
        if (this.isRecording && this.driveScoreDataBufferedFileWriter != null) {
            this.gforceDataBuilder.setLength(0);
            this.driveScoreDataBufferedFileWriter.write(this.gforceDataBuilder.append(System.currentTimeMillis()).append(",").append("G,").append(a.getXAccel()).append(",").append(a.getYAccel()).append(",").append(a.getZAccel()).append("\n").toString());
        }
    }
    
    public void onDriveScoreUpdatedEvent(com.navdy.hud.app.analytics.TelemetryDataManager$DriveScoreUpdated a) {
        if (this.isRecording && this.driveScoreDataBufferedFileWriter != null) {
            this.gforceDataBuilder.setLength(0);
            com.navdy.hud.app.debug.AsyncBufferedFileWriter a0 = this.driveScoreDataBufferedFileWriter;
            StringBuilder a1 = this.gforceDataBuilder.append(System.currentTimeMillis()).append(",").append("D,").append("DS:").append(a.getDriveScore()).append(",").append("DE:");
            com.navdy.hud.app.analytics.TelemetrySession$InterestingEvent a2 = a.getInterestingEvent();
            a0.write(a1.append(a2).append(",").append("SD:").append(com.navdy.hud.app.analytics.TelemetrySession.INSTANCE.getSessionDuration()).append(",").append("RD:").append(com.navdy.hud.app.analytics.TelemetrySession.INSTANCE.getSessionRollingDuration()).append(",").append("SPD:").append(com.navdy.hud.app.analytics.TelemetrySession.INSTANCE.getSpeedingDuration()).append(",").append("ESD:").append(com.navdy.hud.app.analytics.TelemetrySession.INSTANCE.getExcessiveSpeedingDuration()).append(",").append("HA:").append(com.navdy.hud.app.analytics.TelemetrySession.INSTANCE.getSessionHardAccelerationCount()).append(",").append("HB:").append(com.navdy.hud.app.analytics.TelemetrySession.INSTANCE.getSessionHardBrakingCount()).append(",").append("HG:").append(com.navdy.hud.app.analytics.TelemetrySession.INSTANCE.getSessionHighGCount()).append(",").append("LT:").append(com.navdy.hud.app.debug.DriveRecorder.getLatitude()).append(",").append("LN:").append(com.navdy.hud.app.debug.DriveRecorder.getLongitude()).append("\n").toString());
        }
    }
    
    public void onKeyEvent(android.view.KeyEvent a) {
        label1: {
            label2: {
                if (a == null) {
                    break label2;
                }
                if (a.getKeyCode() != 36) {
                    break label2;
                }
                if (a.getAction() != 1) {
                    break label2;
                }
                this.performDemoPlaybackAction(com.navdy.hud.app.debug.DriveRecorder$Action.STOP, false);
                break label1;
            }
            label0: {
                if (a == null) {
                    break label0;
                }
                if (a.getKeyCode() != 53) {
                    break label0;
                }
                if (a.getAction() != 1) {
                    break label0;
                }
                this.performDemoPlaybackAction(com.navdy.hud.app.debug.DriveRecorder$Action.RESTART, false);
                break label1;
            }
            if (a != null && a.getKeyCode() == 44 && a.getAction() == 1) {
                this.performDemoPlaybackAction(com.navdy.hud.app.debug.DriveRecorder$Action.PRELOAD, false);
            }
        }
    }
    
    public void onMapEngineInitialized(com.navdy.hud.app.maps.MapEvents$MapEngineInitialize a) {
        sLogger.d(new StringBuilder().append("onMapEnigneInitialzed : message received. Initialized :").append(a.initialized).toString());
        this.isEngineInitialized = a.initialized;
        this.checkAndStartAutoRecording();
    }
    
    public void onServiceConnected(android.content.ComponentName a, android.os.IBinder a0) {
        this.routeRecorder = com.navdy.hud.app.debug.IRouteRecorder$Stub.asInterface(a0);
    }
    
    public void onServiceDisconnected(android.content.ComponentName a) {
        this.routeRecorder = null;
    }
    
    public void onStartPlayback(com.navdy.service.library.events.debug.StartDrivePlaybackEvent a) {
        sLogger.d("Start playback event received");
        this.startPlayback(a.label, false);
    }
    
    public void onStartRecordingEvent(com.navdy.service.library.events.debug.StartDriveRecordingEvent a) {
        sLogger.d("Start recording event received");
        this.startRecording(a.label);
    }
    
    public void onStopPlayback(com.navdy.service.library.events.debug.StopDrivePlaybackEvent a) {
        sLogger.d("Stop playback event received");
        this.stopPlayback();
    }
    
    public void onStopRecordingEvent(com.navdy.service.library.events.debug.StopDriveRecordingEvent a) {
        sLogger.d("Stop recording event received");
        this.stopRecording();
    }
    
    public void pausePlayback() {
        if (this.isPlaying()) {
            sLogger.d("Pausing the playback");
            this.obdDataPlaybackHandler.removeCallbacks(this.injectFakeObdData);
            this.playbackState = com.navdy.hud.app.debug.DriveRecorder$State.PAUSED;
        } else {
            sLogger.d("Playback is not happening so not pausing");
        }
    }
    
    public void performDemoPlaybackAction(com.navdy.hud.app.debug.DriveRecorder$Action a, boolean b) {
        sLogger.d(":performDemoPlaybackAction");
        if (this.isEngineInitialized) {
            com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.debug.DriveRecorder$8(this, a, b), 1);
        } else {
            sLogger.e("Engine is not initialized so can not start the playback");
        }
    }
    
    public void prepare(String s) {
        boolean b = this.isPlaying();
        label2: {
            label0: {
                label1: {
                    if (b) {
                        break label1;
                    }
                    if (!this.isPaused()) {
                        break label0;
                    }
                }
                sLogger.e("Cannot prepare for playback as the playback is in progress");
                break label2;
            }
            com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.debug.DriveRecorder$2(this, s), 9);
        }
    }
    
    public void release() {
        this.stopPlayback();
        if (this.recordedObdData != null) {
            this.recordedObdData = null;
        }
        this.preparedFileName = null;
        this.preparedFileLastModifiedTime = -1L;
    }
    
    public boolean restartPlayback() {
        boolean b = false;
        boolean b0 = this.isPlaying();
        label0: {
            label1: {
                if (b0) {
                    break label1;
                }
                if (!this.isPaused()) {
                    b = false;
                    break label0;
                }
            }
            this.playbackState = com.navdy.hud.app.debug.DriveRecorder$State.PLAYING;
            this.currentObdDataInjectionIndex = 0;
            this.obdDataPlaybackHandler.removeCallbacks(this.injectFakeObdData);
            this.obdDataPlaybackHandler.post(this.injectFakeObdData);
            b = true;
        }
        return b;
    }
    
    public void resumePlayback() {
        if (this.isPaused()) {
            this.playbackState = com.navdy.hud.app.debug.DriveRecorder$State.PLAYING;
            this.obdDataPlaybackHandler.post(this.injectFakeObdData);
        }
    }
    
    public void setRealObdConnected(boolean b) {
        this.realObdConnected = b;
        sLogger.d(new StringBuilder().append("setRealObdConnected : state ").append(b).toString());
        if (this.realObdConnected) {
            this.performDemoPlaybackAction(com.navdy.hud.app.debug.DriveRecorder$Action.STOP, true);
        }
    }
    
    public void startPlayback(String s, boolean b) {
        boolean b0 = this.isPlaying();
        label2: {
            label0: {
                label1: {
                    if (b0) {
                        break label1;
                    }
                    if (!this.isRecording) {
                        break label0;
                    }
                }
                sLogger.v("already busy, no-op");
                break label2;
            }
            sLogger.d("Starting the playback");
            this.playbackState = com.navdy.hud.app.debug.DriveRecorder$State.PLAYING;
            this.isLooping = b;
            com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.debug.DriveRecorder$3(this, s), 9);
        }
    }
    
    public void startRecording(String s) {
        boolean b = this.isRecording;
        label2: {
            label0: {
                label1: {
                    if (b) {
                        break label1;
                    }
                    if (!this.isPlaying()) {
                        break label0;
                    }
                }
                sLogger.v("Obd data recorder is already busy");
                break label2;
            }
            sLogger.d("Starting the recording");
            this.isRecording = true;
            this.isSupportedPidsWritten = false;
            com.navdy.service.library.task.TaskManager.getInstance().execute(this.createDriveRecord(s), 9);
        }
    }
    
    public void stopPlayback() {
        if (this.isPlaying()) {
            sLogger.d("Stopping the playback");
            this.currentObdDataInjectionIndex = 0;
            if (this.obdDataPlaybackHandler != null) {
                this.obdDataPlaybackHandler.removeCallbacks(this.injectFakeObdData);
            }
            if (this.obdPlaybackThread != null && this.obdPlaybackThread.isAlive()) {
                boolean b = this.obdPlaybackThread.quit();
                sLogger.v(new StringBuilder().append("obd handler thread quit:").append(b).toString());
            }
            this.obdPlaybackThread = null;
            this.isLooping = false;
            this.playbackState = com.navdy.hud.app.debug.DriveRecorder$State.STOPPED;
        } else {
            sLogger.v("already stopped, no-op");
        }
    }
    
    public void stopRecording() {
        if (this.isRecording) {
            sLogger.d("Stopping the Obd recording");
            this.handler.post((Runnable)new com.navdy.hud.app.debug.DriveRecorder$1(this));
        } else {
            sLogger.v("already stopped, no-op");
        }
    }
}
