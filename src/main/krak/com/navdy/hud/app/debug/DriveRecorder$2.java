package com.navdy.hud.app.debug;

class DriveRecorder$2 implements Runnable {
    final com.navdy.hud.app.debug.DriveRecorder this$0;
    final String val$fileName;
    
    DriveRecorder$2(com.navdy.hud.app.debug.DriveRecorder a, String s) {
        super();
        this.this$0 = a;
        this.val$fileName = s;
    }
    
    public void run() {
        if (this.this$0.bPrepare(this.val$fileName)) {
            com.navdy.hud.app.debug.DriveRecorder.sLogger.d("Prepare succeeded");
        } else {
            com.navdy.hud.app.debug.DriveRecorder.sLogger.e("Prepare failed");
        }
    }
}
