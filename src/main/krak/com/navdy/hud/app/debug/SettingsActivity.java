package com.navdy.hud.app.debug;
import com.navdy.hud.app.R;

public class SettingsActivity extends android.preference.PreferenceActivity {
    final public static String RESTART_ON_CRASH_KEY = "restart_on_crash_preference";
    final public static String START_ON_BOOT_KEY = "start_on_boot_preference";
    final public static String START_VIDEO_ON_BOOT_KEY = "start_video_on_boot_preference";
    
    public SettingsActivity() {
    }
    
    public void onBuildHeaders(java.util.List a) {
        this.loadHeadersFromResource(R.xml.preference_headers, a);
    }
    
    protected void onCreate(android.os.Bundle a) {
        super.onCreate(a);
    }
}
