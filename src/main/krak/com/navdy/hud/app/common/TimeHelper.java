package com.navdy.hud.app.common;
import com.navdy.hud.app.R;

public class TimeHelper {
    final public static com.navdy.hud.app.common.TimeHelper$UpdateClock CLOCK_CHANGED;
    final public static com.navdy.hud.app.common.TimeHelper$DateTimeAvailableEvent DATE_TIME_AVAILABLE_EVENT;
    final private static com.navdy.service.library.log.Logger sLogger;
    private String am;
    private String amCompact;
    private com.squareup.otto.Bus bus;
    private java.util.Calendar calendar;
    private java.text.SimpleDateFormat currentTimeFormat;
    private java.text.SimpleDateFormat dateFormat;
    private java.text.SimpleDateFormat dayFormat;
    private String pm;
    private String pmCompact;
    private java.text.SimpleDateFormat timeFormat12;
    private java.text.SimpleDateFormat timeFormat24;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.common.TimeHelper.class);
        CLOCK_CHANGED = new com.navdy.hud.app.common.TimeHelper$UpdateClock();
        DATE_TIME_AVAILABLE_EVENT = new com.navdy.hud.app.common.TimeHelper$DateTimeAvailableEvent();
    }
    
    public TimeHelper(android.content.Context a, com.squareup.otto.Bus a0) {
        this.bus = a0;
        android.content.res.Resources a1 = a.getResources();
        this.pmCompact = a1.getString(R.string.pm_marker);
        this.pm = a1.getString(R.string.pm);
        this.amCompact = a1.getString(R.string.am_marker);
        this.am = a1.getString(R.string.am);
        this.updateLocale();
    }
    
    public String formatTime(java.util.Date a, StringBuilder a0) {
        String s = null;
        synchronized(this) {
            s = this.currentTimeFormat.format(a);
            if (a0 != null) {
                a0.setLength(0);
                if (this.currentTimeFormat == this.timeFormat12) {
                    this.calendar.setTime(a);
                    if (this.calendar.get(9) == 1) {
                        a0.append(this.pmCompact);
                    }
                }
            }
        }
        /*monexit(this)*/;
        return s;
    }
    
    public String formatTime12Hour(java.util.Date a, StringBuilder a0, boolean b) {
        String s = null;
        synchronized(this) {
            s = this.timeFormat12.format(a);
            if (a0 != null) {
                a0.setLength(0);
                this.calendar.setTime(a);
                if (this.calendar.get(9) != 1) {
                    if (b) {
                        a0.append(this.amCompact);
                    } else {
                        a0.append(this.am);
                    }
                } else if (b) {
                    a0.append(this.pmCompact);
                } else {
                    a0.append(this.pm);
                }
            }
        }
        /*monexit(this)*/;
        return s;
    }
    
    public String getDate() {
        String s = null;
        synchronized(this) {
            s = this.dateFormat.format(new java.util.Date());
        }
        /*monexit(this)*/;
        return s;
    }
    
    public String getDay() {
        String s = null;
        synchronized(this) {
            s = this.dayFormat.format(new java.util.Date());
        }
        /*monexit(this)*/;
        return s;
    }
    
    public com.navdy.service.library.events.settings.DateTimeConfiguration$Clock getFormat() {
        return (this.currentTimeFormat != this.timeFormat12) ? com.navdy.service.library.events.settings.DateTimeConfiguration$Clock.CLOCK_24_HOUR : com.navdy.service.library.events.settings.DateTimeConfiguration$Clock.CLOCK_12_HOUR;
    }
    
    public java.util.TimeZone getTimeZone() {
        java.util.TimeZone a = null;
        synchronized(this) {
            String s = java.util.TimeZone.getDefault().getID();
            sLogger.v(new StringBuilder().append("timezone is ").append(s).toString());
            a = java.util.TimeZone.getTimeZone(s);
        }
        /*monexit(this)*/;
        return a;
    }
    
    public void setFormat(com.navdy.service.library.events.settings.DateTimeConfiguration$Clock a) {
        switch(com.navdy.hud.app.common.TimeHelper$1.$SwitchMap$com$navdy$service$library$events$settings$DateTimeConfiguration$Clock[a.ordinal()]) {
            case 2: {
                this.currentTimeFormat = this.timeFormat12;
                break;
            }
            case 1: {
                this.currentTimeFormat = this.timeFormat24;
                break;
            }
        }
        this.calendar = java.util.Calendar.getInstance();
    }
    
    public void updateLocale() {
        synchronized(this) {
            String s = java.util.TimeZone.getDefault().getID();
            sLogger.v(new StringBuilder().append("timezone is ").append(s).toString());
            java.util.TimeZone a = java.util.TimeZone.getTimeZone(s);
            this.dayFormat = new java.text.SimpleDateFormat("EEE");
            this.dayFormat.setTimeZone(a);
            this.dateFormat = new java.text.SimpleDateFormat("d MMM");
            this.dateFormat.setTimeZone(a);
            boolean b = this.currentTimeFormat != this.timeFormat24;
            this.timeFormat24 = new java.text.SimpleDateFormat("H:mm");
            this.timeFormat24.setTimeZone(a);
            this.timeFormat12 = new java.text.SimpleDateFormat("h:mm");
            this.timeFormat12.setTimeZone(a);
            this.calendar = java.util.Calendar.getInstance();
            this.calendar.setTimeZone(a);
            if (b) {
                this.currentTimeFormat = this.timeFormat12;
            } else {
                this.currentTimeFormat = this.timeFormat24;
            }
            this.bus.post(CLOCK_CHANGED);
        }
        /*monexit(this)*/;
    }
}
