package com.navdy.hud.app.common;

final public class ProdModule$$ModuleAdapter$ProvideSharedPreferencesProvidesAdapter extends dagger.internal.ProvidesBinding implements javax.inject.Provider {
    final private com.navdy.hud.app.common.ProdModule module;
    
    public ProdModule$$ModuleAdapter$ProvideSharedPreferencesProvidesAdapter(com.navdy.hud.app.common.ProdModule a) {
        super("android.content.SharedPreferences", true, "com.navdy.hud.app.common.ProdModule", "provideSharedPreferences");
        this.module = a;
        this.setLibrary(true);
    }
    
    public android.content.SharedPreferences get() {
        return this.module.provideSharedPreferences();
    }
    
    public Object get() {
        return this.get();
    }
}
