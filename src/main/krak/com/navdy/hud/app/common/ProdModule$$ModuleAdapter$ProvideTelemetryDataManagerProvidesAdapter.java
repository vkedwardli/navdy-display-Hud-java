package com.navdy.hud.app.common;

final public class ProdModule$$ModuleAdapter$ProvideTelemetryDataManagerProvidesAdapter extends dagger.internal.ProvidesBinding implements javax.inject.Provider {
    private dagger.internal.Binding bus;
    final private com.navdy.hud.app.common.ProdModule module;
    private dagger.internal.Binding powerManager;
    private dagger.internal.Binding sharedPreferences;
    private dagger.internal.Binding tripManager;
    private dagger.internal.Binding uiStateManager;
    
    public ProdModule$$ModuleAdapter$ProvideTelemetryDataManagerProvidesAdapter(com.navdy.hud.app.common.ProdModule a) {
        super("com.navdy.hud.app.analytics.TelemetryDataManager", true, "com.navdy.hud.app.common.ProdModule", "provideTelemetryDataManager");
        this.module = a;
        this.setLibrary(true);
    }
    
    public void attach(dagger.internal.Linker a) {
        this.uiStateManager = a.requestBinding("com.navdy.hud.app.ui.framework.UIStateManager", com.navdy.hud.app.common.ProdModule.class, (this).getClass().getClassLoader());
        this.powerManager = a.requestBinding("com.navdy.hud.app.device.PowerManager", com.navdy.hud.app.common.ProdModule.class, (this).getClass().getClassLoader());
        this.bus = a.requestBinding("com.squareup.otto.Bus", com.navdy.hud.app.common.ProdModule.class, (this).getClass().getClassLoader());
        this.sharedPreferences = a.requestBinding("android.content.SharedPreferences", com.navdy.hud.app.common.ProdModule.class, (this).getClass().getClassLoader());
        this.tripManager = a.requestBinding("com.navdy.hud.app.framework.trips.TripManager", com.navdy.hud.app.common.ProdModule.class, (this).getClass().getClassLoader());
    }
    
    public com.navdy.hud.app.analytics.TelemetryDataManager get() {
        return this.module.provideTelemetryDataManager((com.navdy.hud.app.ui.framework.UIStateManager)this.uiStateManager.get(), (com.navdy.hud.app.device.PowerManager)this.powerManager.get(), (com.squareup.otto.Bus)this.bus.get(), (android.content.SharedPreferences)this.sharedPreferences.get(), (com.navdy.hud.app.framework.trips.TripManager)this.tripManager.get());
    }
    
    public Object get() {
        return this.get();
    }
    
    public void getDependencies(java.util.Set a, java.util.Set a0) {
        a.add(this.uiStateManager);
        a.add(this.powerManager);
        a.add(this.bus);
        a.add(this.sharedPreferences);
        a.add(this.tripManager);
    }
}
