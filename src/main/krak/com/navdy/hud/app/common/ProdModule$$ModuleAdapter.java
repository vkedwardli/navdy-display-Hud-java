package com.navdy.hud.app.common;

final public class ProdModule$$ModuleAdapter extends dagger.internal.ModuleAdapter {
    final private static Class[] INCLUDES;
    final private static String[] INJECTS;
    final private static Class[] STATIC_INJECTIONS;
    
    static {
        String[] a = new String[29];
        a[0] = "members/com.navdy.hud.app.service.ConnectionServiceProxy";
        a[1] = "members/com.navdy.hud.app.maps.here.HereMapsManager";
        a[2] = "members/com.navdy.hud.app.maps.here.HereRegionManager";
        a[3] = "members/com.navdy.hud.app.obd.ObdManager";
        a[4] = "members/com.navdy.hud.app.util.OTAUpdateService";
        a[5] = "members/com.navdy.hud.app.maps.MapsEventHandler";
        a[6] = "members/com.navdy.hud.app.manager.RemoteDeviceManager";
        a[7] = "members/com.navdy.hud.app.maps.MapSchemeController";
        a[8] = "members/com.navdy.hud.app.framework.DriverProfileHelper";
        a[9] = "members/com.navdy.hud.app.framework.notifications.NotificationManager";
        a[10] = "members/com.navdy.hud.app.framework.trips.TripManager";
        a[11] = "members/com.navdy.hud.app.ui.activity.Main$Presenter$DeferredServices";
        a[12] = "members/com.navdy.hud.app.framework.DriverProfileHelper";
        a[13] = "members/com.navdy.hud.app.debug.DebugReceiver";
        a[14] = "members/com.navdy.hud.app.framework.network.NetworkStateManager";
        a[15] = "members/com.navdy.hud.app.service.ShutdownMonitor";
        a[16] = "members/com.navdy.hud.app.util.ReportIssueService";
        a[17] = "members/com.navdy.hud.app.obd.CarMDVinDecoder";
        a[18] = "members/com.navdy.hud.app.debug.DriveRecorder";
        a[19] = "members/com.navdy.hud.app.HudApplication";
        a[20] = "members/com.navdy.hud.app.manager.SpeedManager";
        a[21] = "members/com.navdy.hud.app.view.MusicWidgetPresenter";
        a[22] = "members/com.navdy.hud.app.ui.component.mainmenu.MusicMenu2";
        a[23] = "members/com.navdy.hud.app.manager.UpdateReminderManager";
        a[24] = "members/com.navdy.hud.app.analytics.AnalyticsSupport$TemperatureReporter";
        a[25] = "members/com.navdy.hud.app.analytics.AnalyticsSupport$NotificationReceiver";
        a[26] = "members/com.navdy.hud.app.service.GestureVideosSyncService";
        a[27] = "members/com.navdy.hud.app.service.ObdCANBusDataUploadService";
        a[28] = "members/com.navdy.hud.app.view.EngineTemperaturePresenter";
        INJECTS = a;
        STATIC_INJECTIONS = new Class[0];
        INCLUDES = new Class[0];
    }
    
    public ProdModule$$ModuleAdapter() {
        super(com.navdy.hud.app.common.ProdModule.class, INJECTS, STATIC_INJECTIONS, false, INCLUDES, true, true);
    }
    
    public void getBindings(dagger.internal.BindingsGroup a, com.navdy.hud.app.common.ProdModule a0) {
        a.contributeProvidesBinding("com.squareup.otto.Bus", (dagger.internal.ProvidesBinding)new com.navdy.hud.app.common.ProdModule$$ModuleAdapter$ProvideBusProvidesAdapter(a0));
        a.contributeProvidesBinding("android.content.SharedPreferences", (dagger.internal.ProvidesBinding)new com.navdy.hud.app.common.ProdModule$$ModuleAdapter$ProvideSharedPreferencesProvidesAdapter(a0));
        a.contributeProvidesBinding("com.navdy.hud.app.service.ConnectionServiceProxy", (dagger.internal.ProvidesBinding)new com.navdy.hud.app.common.ProdModule$$ModuleAdapter$ProvideConnectionServiceProxyProvidesAdapter(a0));
        a.contributeProvidesBinding("com.navdy.hud.app.common.TimeHelper", (dagger.internal.ProvidesBinding)new com.navdy.hud.app.common.ProdModule$$ModuleAdapter$ProvideTimeHelperProvidesAdapter(a0));
        a.contributeProvidesBinding("com.navdy.hud.app.device.PowerManager", (dagger.internal.ProvidesBinding)new com.navdy.hud.app.common.ProdModule$$ModuleAdapter$ProvidePowerManagerProvidesAdapter(a0));
        a.contributeProvidesBinding("com.navdy.hud.app.config.SettingsManager", (dagger.internal.ProvidesBinding)new com.navdy.hud.app.common.ProdModule$$ModuleAdapter$ProvideSettingsManagerProvidesAdapter(a0));
        a.contributeProvidesBinding("com.navdy.hud.app.ancs.AncsServiceConnector", (dagger.internal.ProvidesBinding)new com.navdy.hud.app.common.ProdModule$$ModuleAdapter$ProvideAncsServiceConnectorProvidesAdapter(a0));
        a.contributeProvidesBinding("com.navdy.hud.app.manager.InputManager", (dagger.internal.ProvidesBinding)new com.navdy.hud.app.common.ProdModule$$ModuleAdapter$ProvideInputManagerProvidesAdapter(a0));
        a.contributeProvidesBinding("com.navdy.hud.app.device.dial.DialSimulatorMessagesHandler", (dagger.internal.ProvidesBinding)new com.navdy.hud.app.common.ProdModule$$ModuleAdapter$ProvideDialSimulatorMessagesHandlerProvidesAdapter(a0));
        a.contributeProvidesBinding("com.navdy.hud.app.framework.phonecall.CallManager", (dagger.internal.ProvidesBinding)new com.navdy.hud.app.common.ProdModule$$ModuleAdapter$ProvideCallManagerProvidesAdapter(a0));
        a.contributeProvidesBinding("com.navdy.hud.app.service.pandora.PandoraManager", (dagger.internal.ProvidesBinding)new com.navdy.hud.app.common.ProdModule$$ModuleAdapter$ProvidePandoraManagerProvidesAdapter(a0));
        a.contributeProvidesBinding("com.navdy.hud.app.manager.MusicManager", (dagger.internal.ProvidesBinding)new com.navdy.hud.app.common.ProdModule$$ModuleAdapter$ProvideMusicManagerProvidesAdapter(a0));
        a.contributeProvidesBinding("com.navdy.hud.app.service.ConnectionHandler", (dagger.internal.ProvidesBinding)new com.navdy.hud.app.common.ProdModule$$ModuleAdapter$ProvideConnectionHandlerProvidesAdapter(a0));
        a.contributeProvidesBinding("com.navdy.hud.app.gesture.GestureServiceConnector", (dagger.internal.ProvidesBinding)new com.navdy.hud.app.common.ProdModule$$ModuleAdapter$ProvideGestureServiceConnectorProvidesAdapter(a0));
        a.contributeProvidesBinding("com.navdy.hud.app.ui.framework.UIStateManager", (dagger.internal.ProvidesBinding)new com.navdy.hud.app.common.ProdModule$$ModuleAdapter$ProvideUIStateManagerProvidesAdapter(a0));
        a.contributeProvidesBinding("com.navdy.hud.app.storage.PathManager", (dagger.internal.ProvidesBinding)new com.navdy.hud.app.common.ProdModule$$ModuleAdapter$ProvidePathManagerProvidesAdapter(a0));
        a.contributeProvidesBinding("com.navdy.hud.app.profile.DriverProfileManager", (dagger.internal.ProvidesBinding)new com.navdy.hud.app.common.ProdModule$$ModuleAdapter$ProvideDriverProfileManagerProvidesAdapter(a0));
        a.contributeProvidesBinding("com.navdy.hud.app.framework.trips.TripManager", (dagger.internal.ProvidesBinding)new com.navdy.hud.app.common.ProdModule$$ModuleAdapter$ProvideTripManagerProvidesAdapter(a0));
        a.contributeProvidesBinding("com.navdy.hud.app.manager.PairingManager", (dagger.internal.ProvidesBinding)new com.navdy.hud.app.common.ProdModule$$ModuleAdapter$ProvidePairingManagerProvidesAdapter(a0));
        a.contributeProvidesBinding("com.navdy.service.library.network.http.IHttpManager", (dagger.internal.ProvidesBinding)new com.navdy.hud.app.common.ProdModule$$ModuleAdapter$ProvideHttpManagerProvidesAdapter(a0));
        a.contributeProvidesBinding("com.navdy.hud.app.debug.DriveRecorder", (dagger.internal.ProvidesBinding)new com.navdy.hud.app.common.ProdModule$$ModuleAdapter$ProvideObdDataReceorderProvidesAdapter(a0));
        a.contributeProvidesBinding("com.navdy.hud.app.util.MusicArtworkCache", (dagger.internal.ProvidesBinding)new com.navdy.hud.app.common.ProdModule$$ModuleAdapter$ProvideArtworkCacheProvidesAdapter(a0));
        a.contributeProvidesBinding("com.navdy.hud.app.framework.voice.VoiceSearchHandler", (dagger.internal.ProvidesBinding)new com.navdy.hud.app.common.ProdModule$$ModuleAdapter$ProvideVoiceSearchHandlerProvidesAdapter(a0));
        a.contributeProvidesBinding("com.navdy.hud.app.util.FeatureUtil", (dagger.internal.ProvidesBinding)new com.navdy.hud.app.common.ProdModule$$ModuleAdapter$ProvideFeatureUtilProvidesAdapter(a0));
        a.contributeProvidesBinding("com.navdy.hud.app.analytics.TelemetryDataManager", (dagger.internal.ProvidesBinding)new com.navdy.hud.app.common.ProdModule$$ModuleAdapter$ProvideTelemetryDataManagerProvidesAdapter(a0));
        a.contributeProvidesBinding("com.navdy.hud.app.storage.cache.MessageCache<com.navdy.service.library.events.audio.MusicCollectionResponse>", (dagger.internal.ProvidesBinding)new com.navdy.hud.app.common.ProdModule$$ModuleAdapter$ProvideMusicCollectionResponseMessageCacheProvidesAdapter(a0));
    }
    
    public void getBindings(dagger.internal.BindingsGroup a, Object a0) {
        this.getBindings(a, (com.navdy.hud.app.common.ProdModule)a0);
    }
}
