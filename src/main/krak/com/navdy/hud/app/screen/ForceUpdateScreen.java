package com.navdy.hud.app.screen;

@Layout(R.layout.force_update_lyt)
public class ForceUpdateScreen extends com.navdy.hud.app.screen.BaseScreen {
    final private static com.navdy.service.library.log.Logger sLogger;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.screen.ForceUpdateScreen.class);
    }
    
    public ForceUpdateScreen() {
    }
    
    public Object getDaggerModule() {
        return new com.navdy.hud.app.screen.ForceUpdateScreen$Module(this);
    }
    
    public String getMortarScopeName() {
        return com.navdy.hud.app.screen.ForceUpdateScreen.class.getName();
    }
    
    public com.navdy.service.library.events.ui.Screen getScreen() {
        return com.navdy.service.library.events.ui.Screen.SCREEN_FORCE_UPDATE;
    }
}
