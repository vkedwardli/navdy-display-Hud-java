package com.navdy.hud.app.screen;

class WelcomeScreen$Presenter$1 implements Runnable {
    final com.navdy.hud.app.screen.WelcomeScreen$Presenter this$0;
    
    WelcomeScreen$Presenter$1(com.navdy.hud.app.screen.WelcomeScreen$Presenter a) {
        super();
        this.this$0 = a;
    }
    
    public void run() {
        com.navdy.hud.app.screen.WelcomeScreen.access$000().i(new StringBuilder().append("checking connection timeout - connected:").append(com.navdy.hud.app.screen.WelcomeScreen$Presenter.access$100(this.this$0)).toString());
        if (!com.navdy.hud.app.screen.WelcomeScreen$Presenter.access$100(this.this$0)) {
            com.navdy.hud.app.screen.WelcomeScreen$Presenter.access$200(this.this$0, com.navdy.hud.app.screen.WelcomeScreen$State.CONNECTION_FAILED);
            this.this$0.updateView();
        }
    }
}
