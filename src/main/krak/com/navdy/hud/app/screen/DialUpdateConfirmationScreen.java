package com.navdy.hud.app.screen;

@Layout(R.layout.screen_dial_update_confirmation)
public class DialUpdateConfirmationScreen extends com.navdy.hud.app.screen.BaseScreen {
    final private static com.navdy.service.library.log.Logger sLogger;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.screen.DialUpdateConfirmationScreen.class);
    }
    
    public DialUpdateConfirmationScreen() {
    }
    
    static com.navdy.service.library.log.Logger access$000() {
        return sLogger;
    }
    
    public Object getDaggerModule() {
        return new com.navdy.hud.app.screen.DialUpdateConfirmationScreen$Module(this);
    }
    
    public String getMortarScopeName() {
        return com.navdy.hud.app.screen.DialUpdateConfirmationScreen.class.getName();
    }
    
    public com.navdy.service.library.events.ui.Screen getScreen() {
        return com.navdy.service.library.events.ui.Screen.SCREEN_DIAL_UPDATE_CONFIRMATION;
    }
}
