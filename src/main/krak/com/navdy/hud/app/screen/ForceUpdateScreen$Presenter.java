package com.navdy.hud.app.screen;
import javax.inject.Inject;

public class ForceUpdateScreen$Presenter extends com.navdy.hud.app.ui.framework.BasePresenter {
    @Inject
    com.navdy.hud.app.gesture.GestureServiceConnector gestureServiceConnector;
    @Inject
    com.squareup.otto.Bus mBus;
    @Inject
    android.content.SharedPreferences mPreferences;
    @Inject
    com.navdy.hud.app.screen.ForceUpdateScreen mScreen;
    @Inject
    com.navdy.hud.app.ui.framework.UIStateManager uiStateManager;
    
    public ForceUpdateScreen$Presenter() {
    }
    
    public void dismiss() {
        this.mBus.post(new com.navdy.service.library.events.ui.ShowScreen$Builder().screen(this.uiStateManager.getDefaultMainActiveScreen()).build());
    }
    
    public com.squareup.otto.Bus getBus() {
        return this.mBus;
    }
    
    public void install() {
        android.content.Intent a = new android.content.Intent(com.navdy.hud.app.HudApplication.getAppContext(), com.navdy.hud.app.util.OTAUpdateService.class);
        a.putExtra("COMMAND", "INSTALL_UPDATE");
        com.navdy.hud.app.HudApplication.getAppContext().startService(a);
    }
    
    public boolean isSoftwareUpdatePending() {
        return com.navdy.hud.app.util.OTAUpdateService.isUpdateAvailable();
    }
    
    public void onLoad(android.os.Bundle a) {
        super.onLoad(a);
        this.uiStateManager.enableSystemTray(false);
        this.uiStateManager.enableNotificationColor(false);
        com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().enableNotifications(false);
    }
    
    protected void onUnload() {
        this.uiStateManager.enableSystemTray(true);
        this.uiStateManager.enableNotificationColor(true);
        com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().enableNotifications(true);
        super.onUnload();
    }
    
    public void shutDown() {
        this.mBus.post(new com.navdy.hud.app.event.Shutdown(com.navdy.hud.app.event.Shutdown$Reason.FORCED_UPDATE));
    }
}
