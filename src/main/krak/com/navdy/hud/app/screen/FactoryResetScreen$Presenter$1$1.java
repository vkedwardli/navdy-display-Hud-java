package com.navdy.hud.app.screen;

class FactoryResetScreen$Presenter$1$1 implements Runnable {
    final com.navdy.hud.app.screen.FactoryResetScreen$Presenter$1 this$1;
    
    FactoryResetScreen$Presenter$1$1(com.navdy.hud.app.screen.FactoryResetScreen$Presenter$1 a) {
        super();
        this.this$1 = a;
    }
    
    public void run() {
        try {
            android.os.RecoverySystem.rebootWipeUserData(com.navdy.hud.app.HudApplication.getAppContext());
        } catch(Exception a) {
            com.navdy.hud.app.screen.FactoryResetScreen.access$000().e("Failed to do factory reset", (Throwable)a);
            this.this$1.this$0.bus.post(new com.navdy.service.library.events.ui.ShowScreen$Builder().screen(com.navdy.service.library.events.ui.Screen.SCREEN_BACK).build());
        }
    }
}
