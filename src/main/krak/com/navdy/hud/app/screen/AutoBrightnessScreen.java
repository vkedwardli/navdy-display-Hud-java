package com.navdy.hud.app.screen;

@Layout(R.layout.screen_auto_brightness)
public class AutoBrightnessScreen extends com.navdy.hud.app.screen.BaseScreen {
    final private static com.navdy.service.library.log.Logger sLogger;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.screen.AutoBrightnessScreen.class);
    }
    
    public AutoBrightnessScreen() {
    }
    
    public Object getDaggerModule() {
        return new com.navdy.hud.app.screen.AutoBrightnessScreen$Module(this);
    }
    
    public String getMortarScopeName() {
        return (this).getClass().getName();
    }
    
    public com.navdy.service.library.events.ui.Screen getScreen() {
        return com.navdy.service.library.events.ui.Screen.SCREEN_AUTO_BRIGHTNESS;
    }
}
