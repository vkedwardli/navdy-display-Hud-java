package com.navdy.hud.app.screen;

final public class DialUpdateConfirmationScreen$Presenter$$InjectAdapter extends dagger.internal.Binding implements javax.inject.Provider, dagger.MembersInjector {
    private dagger.internal.Binding mBus;
    private dagger.internal.Binding supertype;
    
    public DialUpdateConfirmationScreen$Presenter$$InjectAdapter() {
        super("com.navdy.hud.app.screen.DialUpdateConfirmationScreen$Presenter", "members/com.navdy.hud.app.screen.DialUpdateConfirmationScreen$Presenter", true, com.navdy.hud.app.screen.DialUpdateConfirmationScreen$Presenter.class);
    }
    
    public void attach(dagger.internal.Linker a) {
        this.mBus = a.requestBinding("com.squareup.otto.Bus", com.navdy.hud.app.screen.DialUpdateConfirmationScreen$Presenter.class, (this).getClass().getClassLoader());
        this.supertype = a.requestBinding("members/com.navdy.hud.app.ui.framework.BasePresenter", com.navdy.hud.app.screen.DialUpdateConfirmationScreen$Presenter.class, (this).getClass().getClassLoader(), false, true);
    }
    
    public com.navdy.hud.app.screen.DialUpdateConfirmationScreen$Presenter get() {
        com.navdy.hud.app.screen.DialUpdateConfirmationScreen$Presenter a = new com.navdy.hud.app.screen.DialUpdateConfirmationScreen$Presenter();
        this.injectMembers(a);
        return a;
    }
    
    public Object get() {
        return this.get();
    }
    
    public void getDependencies(java.util.Set a, java.util.Set a0) {
        a0.add(this.mBus);
        a0.add(this.supertype);
    }
    
    public void injectMembers(com.navdy.hud.app.screen.DialUpdateConfirmationScreen$Presenter a) {
        a.mBus = (com.squareup.otto.Bus)this.mBus.get();
        this.supertype.injectMembers(a);
    }
    
    public void injectMembers(Object a) {
        this.injectMembers((com.navdy.hud.app.screen.DialUpdateConfirmationScreen$Presenter)a);
    }
}
