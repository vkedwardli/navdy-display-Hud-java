package com.navdy.hud.app.screen;

@Layout(R.layout.screen_factory_reset)
public class FactoryResetScreen extends com.navdy.hud.app.screen.BaseScreen {
    final private static com.navdy.service.library.log.Logger sLogger;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.screen.FactoryResetScreen.class);
    }
    
    public FactoryResetScreen() {
    }
    
    static com.navdy.service.library.log.Logger access$000() {
        return sLogger;
    }
    
    public Object getDaggerModule() {
        return new com.navdy.hud.app.screen.FactoryResetScreen$Module(this);
    }
    
    public String getMortarScopeName() {
        return (this).getClass().getName();
    }
    
    public com.navdy.service.library.events.ui.Screen getScreen() {
        return com.navdy.service.library.events.ui.Screen.SCREEN_FACTORY_RESET;
    }
}
