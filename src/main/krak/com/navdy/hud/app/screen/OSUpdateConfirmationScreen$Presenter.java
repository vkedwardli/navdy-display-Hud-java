package com.navdy.hud.app.screen;
import javax.inject.Inject;

public class OSUpdateConfirmationScreen$Presenter extends com.navdy.hud.app.ui.framework.BasePresenter {
    private boolean isReminder;
    @Inject
    com.squareup.otto.Bus mBus;
    @Inject
    android.content.SharedPreferences mPreferences;
    
    public OSUpdateConfirmationScreen$Presenter() {
    }
    
    public void doNotPrompt() {
        android.content.Intent a = new android.content.Intent(com.navdy.hud.app.HudApplication.getAppContext(), com.navdy.hud.app.util.OTAUpdateService.class);
        a.putExtra("COMMAND", "DO_NOT_PROMPT");
        com.navdy.hud.app.HudApplication.getAppContext().startService(a);
    }
    
    public void finish() {
        this.mBus.post(new com.navdy.service.library.events.ui.ShowScreen$Builder().screen(com.navdy.service.library.events.ui.Screen.SCREEN_BACK).build());
    }
    
    public String getCurrentVersion() {
        return com.navdy.hud.app.util.OTAUpdateService.getCurrentVersion();
    }
    
    public String getUpdateVersion() {
        return com.navdy.hud.app.util.OTAUpdateService.getSWUpdateVersion();
    }
    
    public void install() {
        android.content.Intent a = new android.content.Intent(com.navdy.hud.app.HudApplication.getAppContext(), com.navdy.hud.app.util.OTAUpdateService.class);
        a.putExtra("COMMAND", "INSTALL_UPDATE");
        com.navdy.hud.app.HudApplication.getAppContext().startService(a);
    }
    
    public boolean isReminder() {
        return this.isReminder;
    }
    
    public void onLoad(android.os.Bundle a) {
        super.onLoad(a);
        if (a == null) {
            com.navdy.hud.app.screen.OSUpdateConfirmationScreen.access$000().e("null bundle passed to onLoad()");
            this.isReminder = false;
        } else {
            this.isReminder = a.getBoolean("UPDATE_REMINDER", false);
        }
    }
}
