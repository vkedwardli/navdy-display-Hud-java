package com.navdy.hud.app.screen;

class DialUpdateProgressScreen$Presenter$1 implements Runnable {
    final com.navdy.hud.app.screen.DialUpdateProgressScreen$Presenter this$0;
    
    DialUpdateProgressScreen$Presenter$1(com.navdy.hud.app.screen.DialUpdateProgressScreen$Presenter a) {
        super();
        this.this$0 = a;
    }
    
    public void run() {
        this.this$0.mBus.post(new com.navdy.hud.app.event.Shutdown(com.navdy.hud.app.event.Shutdown$Reason.DIAL_OTA));
    }
}
