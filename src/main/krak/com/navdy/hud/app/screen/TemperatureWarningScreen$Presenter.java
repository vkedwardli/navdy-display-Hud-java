package com.navdy.hud.app.screen;
import javax.inject.Inject;

public class TemperatureWarningScreen$Presenter extends com.navdy.hud.app.ui.framework.BasePresenter {
    @Inject
    com.squareup.otto.Bus mBus;
    
    public TemperatureWarningScreen$Presenter() {
    }
    
    public void finish() {
        this.mBus.post(new com.navdy.service.library.events.ui.ShowScreen$Builder().screen(com.navdy.service.library.events.ui.Screen.SCREEN_BACK).build());
    }
}
