package com.navdy.hud.app.screen;

@Layout(R.layout.screen_dial_update_progress)
public class DialUpdateProgressScreen extends com.navdy.hud.app.screen.BaseScreen {
    final private static String DIAL_UPDATE_ERROR_TOAST_ID = "dial-fw-update-err";
    final public static String EXTRA_PROGRESS_CAUSE = "PROGRESS_CAUSE";
    final public static int POST_OTA_PAIRING_DELAY = 6000;
    final public static int POST_OTA_SHUTDOWN_DELAY = 30000;
    final public static int SETTINGS_SCREEN = 1;
    final public static int SHUTDOWN_SCREEN = 2;
    private static android.os.Handler handler;
    final private static com.navdy.service.library.log.Logger sLogger;
    public static boolean updateStarted;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.screen.DialUpdateProgressScreen.class);
        handler = new android.os.Handler(android.os.Looper.getMainLooper());
        updateStarted = false;
    }
    
    public DialUpdateProgressScreen() {
    }
    
    static android.os.Handler access$000() {
        return handler;
    }
    
    static com.navdy.service.library.log.Logger access$200() {
        return sLogger;
    }
    
    public Object getDaggerModule() {
        return new com.navdy.hud.app.screen.DialUpdateProgressScreen$Module(this);
    }
    
    public String getMortarScopeName() {
        return com.navdy.hud.app.screen.DialUpdateProgressScreen.class.getName();
    }
    
    public com.navdy.service.library.events.ui.Screen getScreen() {
        return com.navdy.service.library.events.ui.Screen.SCREEN_DIAL_UPDATE_PROGRESS;
    }
}
