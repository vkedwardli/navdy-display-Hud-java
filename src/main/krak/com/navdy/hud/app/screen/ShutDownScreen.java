package com.navdy.hud.app.screen;

@Layout(R.layout.shutdown_confirmation_lyt)
public class ShutDownScreen extends com.navdy.hud.app.screen.BaseScreen {
    final private static com.navdy.service.library.log.Logger sLogger;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.screen.ShutDownScreen.class);
    }
    
    public ShutDownScreen() {
    }
    
    static com.navdy.service.library.log.Logger access$000() {
        return sLogger;
    }
    
    public Object getDaggerModule() {
        return new com.navdy.hud.app.screen.ShutDownScreen$Module(this);
    }
    
    public String getMortarScopeName() {
        return com.navdy.hud.app.screen.ShutDownScreen.class.getName();
    }
    
    public com.navdy.service.library.events.ui.Screen getScreen() {
        return com.navdy.service.library.events.ui.Screen.SCREEN_SHUTDOWN_CONFIRMATION;
    }
}
