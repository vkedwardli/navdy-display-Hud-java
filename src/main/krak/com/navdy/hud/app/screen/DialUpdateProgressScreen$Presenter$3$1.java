package com.navdy.hud.app.screen;

class DialUpdateProgressScreen$Presenter$3$1 implements Runnable {
    final com.navdy.hud.app.screen.DialUpdateProgressScreen$Presenter$3 this$1;
    final int val$percentage;
    
    DialUpdateProgressScreen$Presenter$3$1(com.navdy.hud.app.screen.DialUpdateProgressScreen$Presenter$3 a, int i) {
        super();
        this.this$1 = a;
        this.val$percentage = i;
    }
    
    public void run() {
        com.navdy.hud.app.view.DialUpdateProgressView a = (com.navdy.hud.app.view.DialUpdateProgressView)com.navdy.hud.app.screen.DialUpdateProgressScreen$Presenter.access$100(this.this$1.this$0);
        if (a != null) {
            a.setProgress(this.val$percentage);
        }
    }
}
