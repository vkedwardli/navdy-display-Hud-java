package com.navdy.hud.app.screen;

class WelcomeScreen$Presenter$4 implements Runnable {
    final com.navdy.hud.app.screen.WelcomeScreen$Presenter this$0;
    
    WelcomeScreen$Presenter$4(com.navdy.hud.app.screen.WelcomeScreen$Presenter a) {
        super();
        this.this$0 = a;
    }
    
    public void run() {
        com.navdy.hud.app.view.WelcomeView a = (com.navdy.hud.app.view.WelcomeView)com.navdy.hud.app.screen.WelcomeScreen$Presenter.access$500(this.this$0);
        com.navdy.hud.app.screen.WelcomeScreen.access$000().i(new StringBuilder().append("State timeout fired - current state:").append(com.navdy.hud.app.screen.WelcomeScreen$Presenter.access$600(this.this$0)).append(" current view:").append(a).toString());
        if (a != null) {
            switch(com.navdy.hud.app.screen.WelcomeScreen$1.$SwitchMap$com$navdy$hud$app$screen$WelcomeScreen$State[com.navdy.hud.app.screen.WelcomeScreen$Presenter.access$600(this.this$0).ordinal()]) {
                case 4: {
                    com.navdy.hud.app.screen.WelcomeScreen$Presenter.access$200(this.this$0, com.navdy.hud.app.screen.WelcomeScreen$State.PICKING);
                    this.this$0.updateView();
                    break;
                }
                case 1: {
                    com.navdy.hud.app.screen.WelcomeScreen.access$000().i("Welcome timed out - going back");
                    com.navdy.hud.app.screen.WelcomeScreen$Presenter.access$702(this.this$0, (com.navdy.service.library.device.NavdyDeviceId)null);
                    this.this$0.finish();
                    break;
                }
            }
        }
    }
}
