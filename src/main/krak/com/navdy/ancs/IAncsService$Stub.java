package com.navdy.ancs;

abstract public class IAncsService$Stub extends android.os.Binder implements com.navdy.ancs.IAncsService {
    final private static String DESCRIPTOR = "com.navdy.ancs.IAncsService";
    final static int TRANSACTION_addListener = 1;
    final static int TRANSACTION_connect = 3;
    final static int TRANSACTION_connectToDevice = 9;
    final static int TRANSACTION_connectToService = 4;
    final static int TRANSACTION_disconnect = 5;
    final static int TRANSACTION_getState = 7;
    final static int TRANSACTION_performNotificationAction = 6;
    final static int TRANSACTION_removeListener = 2;
    final static int TRANSACTION_setNotificationFilter = 8;
    
    public IAncsService$Stub() {
        this.attachInterface((android.os.IInterface)this, "com.navdy.ancs.IAncsService");
    }
    
    public static com.navdy.ancs.IAncsService asInterface(android.os.IBinder a) {
        Object a0 = null;
        label1: if (a != null) {
            android.os.IInterface a1 = a.queryLocalInterface("com.navdy.ancs.IAncsService");
            label0: {
                if (a1 == null) {
                    break label0;
                }
                if (!(a1 instanceof com.navdy.ancs.IAncsService)) {
                    break label0;
                }
                a0 = a1;
                break label1;
            }
            a0 = new com.navdy.ancs.IAncsService$Stub$Proxy(a);
        } else {
            a0 = null;
        }
        return (com.navdy.ancs.IAncsService)a0;
    }
    
    public android.os.IBinder asBinder() {
        return (android.os.IBinder)this;
    }
    
    public boolean onTransact(int i, android.os.Parcel a, android.os.Parcel a0, int i0) {
        boolean b = false;
        switch(i) {
            case 1598968902: {
                a0.writeString("com.navdy.ancs.IAncsService");
                b = true;
                break;
            }
            case 9: {
                a.enforceInterface("com.navdy.ancs.IAncsService");
                android.os.ParcelUuid a1 = (a.readInt() == 0) ? null : (android.os.ParcelUuid)android.os.ParcelUuid.CREATOR.createFromParcel(a);
                this.connectToDevice(a1, a.readString());
                a0.writeNoException();
                b = true;
                break;
            }
            case 8: {
                a.enforceInterface("com.navdy.ancs.IAncsService");
                this.setNotificationFilter((java.util.List)a.createStringArrayList(), a.readLong());
                a0.writeNoException();
                b = true;
                break;
            }
            case 7: {
                a.enforceInterface("com.navdy.ancs.IAncsService");
                int i1 = this.getState();
                a0.writeNoException();
                a0.writeInt(i1);
                b = true;
                break;
            }
            case 6: {
                a.enforceInterface("com.navdy.ancs.IAncsService");
                this.performNotificationAction(a.readInt(), a.readInt());
                a0.writeNoException();
                b = true;
                break;
            }
            case 5: {
                a.enforceInterface("com.navdy.ancs.IAncsService");
                this.disconnect();
                a0.writeNoException();
                b = true;
                break;
            }
            case 4: {
                a.enforceInterface("com.navdy.ancs.IAncsService");
                android.os.ParcelUuid a2 = (a.readInt() == 0) ? null : (android.os.ParcelUuid)android.os.ParcelUuid.CREATOR.createFromParcel(a);
                this.connectToService(a2);
                a0.writeNoException();
                b = true;
                break;
            }
            case 3: {
                a.enforceInterface("com.navdy.ancs.IAncsService");
                this.connect(a.readString());
                a0.writeNoException();
                b = true;
                break;
            }
            case 2: {
                a.enforceInterface("com.navdy.ancs.IAncsService");
                this.removeListener(com.navdy.ancs.IAncsServiceListener$Stub.asInterface(a.readStrongBinder()));
                a0.writeNoException();
                b = true;
                break;
            }
            case 1: {
                a.enforceInterface("com.navdy.ancs.IAncsService");
                this.addListener(com.navdy.ancs.IAncsServiceListener$Stub.asInterface(a.readStrongBinder()));
                a0.writeNoException();
                b = true;
                break;
            }
            default: {
                b = super.onTransact(i, a, a0, i0);
            }
        }
        return b;
    }
}
