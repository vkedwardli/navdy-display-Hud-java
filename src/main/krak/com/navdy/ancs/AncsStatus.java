package com.navdy.ancs;

public class AncsStatus {
    final public static int STATE_BOND_REMOVED = 5;
    final public static int STATE_CONNECTED = 2;
    final public static int STATE_CONNECTING = 1;
    final public static int STATE_DISCONNECTED = 0;
    final public static int STATE_DISCONNECTING = 3;
    final public static int STATE_PAIRING_AUTH_FAILED = 4;
    
    public AncsStatus() {
    }
}
