package com.navdy.service.library.device.connection;

class AcceptorListener$AcceptThread extends com.navdy.service.library.device.connection.ConnectionListener$AcceptThread {
    private volatile boolean closing;
    final com.navdy.service.library.device.connection.AcceptorListener this$0;
    
    public AcceptorListener$AcceptThread(com.navdy.service.library.device.connection.AcceptorListener a) {
        super((com.navdy.service.library.device.connection.ConnectionListener)a);
        this.this$0 = a;
        this.setName(new StringBuilder().append("AcceptThread-").append((com.navdy.service.library.device.connection.AcceptorListener.access$000(a)).getClass().getSimpleName()).toString());
    }
    
    public void cancel() {
        this.this$0.logger.d(new StringBuilder().append("Socket cancel ").append(this).toString());
        this.closing = true;
        com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)com.navdy.service.library.device.connection.AcceptorListener.access$000(this.this$0));
    }
    
    public void run() {
        this.this$0.logger.d(new StringBuilder().append(this.getName()).append(" started, closing:").append(this.closing).toString());
        this.this$0.dispatchStarted();
        label1: {
            com.navdy.service.library.network.SocketAdapter a = null;
            Throwable a0 = null;
            label0: {
                try {
                    a = null;
                    a = com.navdy.service.library.device.connection.AcceptorListener.access$000(this.this$0).accept();
                    com.navdy.service.library.device.connection.SocketConnection a1 = new com.navdy.service.library.device.connection.SocketConnection(com.navdy.service.library.device.connection.AcceptorListener.access$000(this.this$0).getRemoteConnectionInfo(a, com.navdy.service.library.device.connection.AcceptorListener.access$100(this.this$0)), a);
                    this.this$0.dispatchConnected((com.navdy.service.library.device.connection.Connection)a1);
                } catch(Throwable a2) {
                    a0 = a2;
                    break label0;
                }
                this.cancel();
                break label1;
            }
            try {
                if (!this.closing) {
                    this.this$0.logger.e("Socket accept() failed", a0);
                }
                com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a);
            } catch(Throwable a3) {
                this.cancel();
                throw a3;
            }
            this.cancel();
        }
        this.this$0.dispatchStopped();
        this.this$0.logger.i(new StringBuilder().append("END ").append(this.getName()).toString());
    }
}
