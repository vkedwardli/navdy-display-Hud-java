package com.navdy.service.library.device.connection;

abstract public class ConnectionInfo {
    final private static int DEVICE_ID_GROUP = 1;
    final public static com.navdy.service.library.util.RuntimeTypeAdapterFactory connectionInfoAdapter;
    final private static java.util.regex.Pattern deviceIdFromServiceName;
    final public static com.navdy.service.library.log.Logger sLogger;
    final private com.navdy.service.library.device.connection.ConnectionType connectionType;
    final protected com.navdy.service.library.device.NavdyDeviceId mDeviceId;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.service.library.device.connection.ConnectionInfo.class);
        deviceIdFromServiceName = java.util.regex.Pattern.compile("Navdy\\s(.*)");
        connectionInfoAdapter = com.navdy.service.library.util.RuntimeTypeAdapterFactory.of(com.navdy.service.library.device.connection.ConnectionInfo.class).registerSubtype(com.navdy.service.library.device.connection.BTConnectionInfo.class, "BT").registerSubtype(com.navdy.service.library.device.connection.TCPConnectionInfo.class, "TCP").registerSubtypeAlias(com.navdy.service.library.device.connection.BTConnectionInfo.class, "EA");
    }
    
    public ConnectionInfo(android.net.nsd.NsdServiceInfo a) {
        if (a == null) {
            throw new IllegalArgumentException("serviceInfo can't be null");
        }
        String s = this.parseDeviceIdStringFromServiceInfo(a);
        if (s == null) {
            throw new IllegalArgumentException("Service info doesn't contain valid device id");
        }
        this.connectionType = com.navdy.service.library.device.connection.ConnectionType.TCP_PROTOBUF;
        this.mDeviceId = new com.navdy.service.library.device.NavdyDeviceId(s);
    }
    
    public ConnectionInfo(com.navdy.service.library.device.NavdyDeviceId a, com.navdy.service.library.device.connection.ConnectionType a0) {
        if (a == null) {
            throw new IllegalArgumentException("Device id required.");
        }
        this.mDeviceId = a;
        this.connectionType = a0;
    }
    
    protected ConnectionInfo(com.navdy.service.library.device.connection.ConnectionType a) {
        this.connectionType = a;
        this.mDeviceId = com.navdy.service.library.device.NavdyDeviceId.UNKNOWN_ID;
    }
    
    public static com.navdy.service.library.device.connection.ConnectionInfo fromConnection(com.navdy.service.library.device.connection.Connection a) {
        return (a != null) ? a.getConnectionInfo() : null;
    }
    
    public static com.navdy.service.library.device.connection.ConnectionInfo fromServiceInfo(android.net.nsd.NsdServiceInfo a) {
        com.navdy.service.library.device.connection.TCPConnectionInfo a0 = null;
        String s = a.getServiceType();
        if (s.startsWith(".")) {
            s = s.substring(1);
        }
        if (!s.endsWith(".")) {
            s = new StringBuilder().append(s).append(".").toString();
        }
        com.navdy.service.library.device.connection.ConnectionType a1 = com.navdy.service.library.device.connection.ConnectionType.fromServiceType(s);
        if (a1 != null) {
            if (com.navdy.service.library.device.connection.ConnectionInfo$1.$SwitchMap$com$navdy$service$library$device$connection$ConnectionType[a1.ordinal()] != 0) {
                a0 = new com.navdy.service.library.device.connection.TCPConnectionInfo(a);
            } else {
                sLogger.e(new StringBuilder().append("No connectioninfo from mDNS info for type: ").append(a1).toString());
                a0 = null;
            }
        } else {
            a0 = null;
        }
        return a0;
    }
    
    public static boolean isValidNavdyServiceInfo(android.net.nsd.NsdServiceInfo a) {
        boolean b = false;
        if (com.navdy.service.library.device.connection.ConnectionType.getServiceTypes().contains(a.getServiceType())) {
            if (deviceIdFromServiceName.matcher((CharSequence)a.getServiceName()).matches()) {
                b = true;
            } else {
                sLogger.d(new StringBuilder().append("Not a Navdy: ").append(a.getServiceName()).toString());
                b = false;
            }
        } else {
            sLogger.d(new StringBuilder().append("Unknown service type: ").append(a.getServiceType()).toString());
            b = false;
        }
        return b;
    }
    
    public boolean equals(Object a) {
        boolean b = false;
        label2: if (this != a) {
            label0: {
                label1: {
                    if (a == null) {
                        break label1;
                    }
                    if ((this).getClass() == a.getClass()) {
                        break label0;
                    }
                }
                b = false;
                break label2;
            }
            com.navdy.service.library.device.connection.ConnectionInfo a0 = (com.navdy.service.library.device.connection.ConnectionInfo)a;
            b = this.mDeviceId.equals(a0.mDeviceId);
        } else {
            b = true;
        }
        return b;
    }
    
    abstract public com.navdy.service.library.device.connection.ServiceAddress getAddress();
    
    
    public com.navdy.service.library.device.NavdyDeviceId getDeviceId() {
        return this.mDeviceId;
    }
    
    public com.navdy.service.library.device.connection.ConnectionType getType() {
        return this.connectionType;
    }
    
    public int hashCode() {
        return this.mDeviceId.hashCode();
    }
    
    protected String parseDeviceIdStringFromServiceInfo(android.net.nsd.NsdServiceInfo a) {
        String s = null;
        String s0 = a.getServiceName().replace((CharSequence)"\\032", (CharSequence)" ");
        if (android.text.TextUtils.isEmpty((CharSequence)s0)) {
            s = null;
        } else {
            java.util.regex.Matcher a0 = deviceIdFromServiceName.matcher((CharSequence)s0);
            boolean b = a0.matches();
            s = null;
            if (b) {
                int i = a0.groupCount();
                s = null;
                if (i >= 1) {
                    s = a0.group(1);
                }
            }
        }
        return s;
    }
    
    public String toString() {
        return new StringBuilder().append(this.getType()).append(" id: ").append(this.mDeviceId).append(" ").append(this.getAddress()).toString();
    }
}
