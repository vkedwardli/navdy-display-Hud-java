package com.navdy.service.library.device.connection;

public class TCPConnectionInfo extends com.navdy.service.library.device.connection.ConnectionInfo {
    final public static String FORMAT_SERVICE_NAME_WITH_DEVICE_ID = "Navdy %s";
    final public static int SERVICE_PORT = 21301;
    final public static String SERVICE_TYPE = "_navdybus._tcp.";
    String mHostName;
    
    public TCPConnectionInfo(android.net.nsd.NsdServiceInfo a) {
        super(a);
        String s = a.getHost().getHostAddress();
        if (s == null) {
            throw new IllegalArgumentException("hostname not found in service record");
        }
        this.mHostName = s;
    }
    
    public TCPConnectionInfo(com.navdy.service.library.device.NavdyDeviceId a, String s) {
        super(a, com.navdy.service.library.device.connection.ConnectionType.TCP_PROTOBUF);
        if (s == null) {
            throw new IllegalArgumentException("hostname required");
        }
        this.mHostName = s;
    }
    
    public static android.net.nsd.NsdServiceInfo nsdServiceWithDeviceId(com.navdy.service.library.device.NavdyDeviceId a) {
        android.net.nsd.NsdServiceInfo a0 = new android.net.nsd.NsdServiceInfo();
        Object[] a1 = new Object[1];
        a1[0] = a.toString();
        a0.setServiceName(String.format("Navdy %s", a1));
        a0.setServiceType("_navdybus._tcp.");
        a0.setPort(21301);
        return a0;
    }
    
    public boolean equals(Object a) {
        boolean b = false;
        if (this != a) {
            if (a == null) {
                b = false;
            } else if ((this).getClass() != a.getClass()) {
                b = false;
            } else if (super.equals(a)) {
                com.navdy.service.library.device.connection.TCPConnectionInfo a0 = (com.navdy.service.library.device.connection.TCPConnectionInfo)a;
                b = this.mHostName.equals(a0.mHostName);
            } else {
                b = false;
            }
        } else {
            b = true;
        }
        return b;
    }
    
    public com.navdy.service.library.device.connection.ServiceAddress getAddress() {
        return new com.navdy.service.library.device.connection.ServiceAddress(this.mHostName, String.valueOf(21301));
    }
    
    public int hashCode() {
        return super.hashCode() * 31 + this.mHostName.hashCode();
    }
}
