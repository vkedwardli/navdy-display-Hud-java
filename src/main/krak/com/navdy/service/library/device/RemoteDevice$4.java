package com.navdy.service.library.device;

class RemoteDevice$4 implements com.navdy.service.library.device.RemoteDevice$EventDispatcher {
    final com.navdy.service.library.device.RemoteDevice this$0;
    final com.navdy.service.library.device.connection.Connection$DisconnectCause val$cause;
    
    RemoteDevice$4(com.navdy.service.library.device.RemoteDevice a, com.navdy.service.library.device.connection.Connection$DisconnectCause a0) {
        super();
        this.this$0 = a;
        this.val$cause = a0;
    }
    
    public void dispatchEvent(com.navdy.service.library.device.RemoteDevice a, com.navdy.service.library.device.RemoteDevice$Listener a0) {
        a0.onDeviceDisconnected(a, this.val$cause);
    }
    
    public void dispatchEvent(com.navdy.service.library.util.Listenable a, com.navdy.service.library.util.Listenable$Listener a0) {
        this.dispatchEvent((com.navdy.service.library.device.RemoteDevice)a, (com.navdy.service.library.device.RemoteDevice$Listener)a0);
    }
}
