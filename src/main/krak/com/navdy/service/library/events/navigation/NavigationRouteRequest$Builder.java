package com.navdy.service.library.events.navigation;

final public class NavigationRouteRequest$Builder extends com.squareup.wire.Message.Builder {
    public Boolean autoNavigate;
    public Boolean cancelCurrent;
    public com.navdy.service.library.events.location.Coordinate destination;
    public com.navdy.service.library.events.location.Coordinate destinationDisplay;
    public com.navdy.service.library.events.destination.Destination$FavoriteType destinationType;
    public String destination_identifier;
    public Boolean geoCodeStreetAddress;
    public String label;
    public Boolean originDisplay;
    public com.navdy.service.library.events.destination.Destination requestDestination;
    public String requestId;
    public java.util.List routeAttributes;
    public String streetAddress;
    public java.util.List waypoints;
    
    public NavigationRouteRequest$Builder() {
    }
    
    public NavigationRouteRequest$Builder(com.navdy.service.library.events.navigation.NavigationRouteRequest a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.destination = a.destination;
            this.label = a.label;
            this.waypoints = com.navdy.service.library.events.navigation.NavigationRouteRequest.access$000(a.waypoints);
            this.streetAddress = a.streetAddress;
            this.geoCodeStreetAddress = a.geoCodeStreetAddress;
            this.autoNavigate = a.autoNavigate;
            this.destination_identifier = a.destination_identifier;
            this.destinationType = a.destinationType;
            this.originDisplay = a.originDisplay;
            this.cancelCurrent = a.cancelCurrent;
            this.requestId = a.requestId;
            this.destinationDisplay = a.destinationDisplay;
            this.routeAttributes = com.navdy.service.library.events.navigation.NavigationRouteRequest.access$100(a.routeAttributes);
            this.requestDestination = a.requestDestination;
        }
    }
    
    public com.navdy.service.library.events.navigation.NavigationRouteRequest$Builder autoNavigate(Boolean a) {
        this.autoNavigate = a;
        return this;
    }
    
    public com.navdy.service.library.events.navigation.NavigationRouteRequest build() {
        this.checkRequiredFields();
        return new com.navdy.service.library.events.navigation.NavigationRouteRequest(this, (com.navdy.service.library.events.navigation.NavigationRouteRequest$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
    
    public com.navdy.service.library.events.navigation.NavigationRouteRequest$Builder cancelCurrent(Boolean a) {
        this.cancelCurrent = a;
        return this;
    }
    
    public com.navdy.service.library.events.navigation.NavigationRouteRequest$Builder destination(com.navdy.service.library.events.location.Coordinate a) {
        this.destination = a;
        return this;
    }
    
    public com.navdy.service.library.events.navigation.NavigationRouteRequest$Builder destinationDisplay(com.navdy.service.library.events.location.Coordinate a) {
        this.destinationDisplay = a;
        return this;
    }
    
    public com.navdy.service.library.events.navigation.NavigationRouteRequest$Builder destinationType(com.navdy.service.library.events.destination.Destination$FavoriteType a) {
        this.destinationType = a;
        return this;
    }
    
    public com.navdy.service.library.events.navigation.NavigationRouteRequest$Builder destination_identifier(String s) {
        this.destination_identifier = s;
        return this;
    }
    
    public com.navdy.service.library.events.navigation.NavigationRouteRequest$Builder geoCodeStreetAddress(Boolean a) {
        this.geoCodeStreetAddress = a;
        return this;
    }
    
    public com.navdy.service.library.events.navigation.NavigationRouteRequest$Builder label(String s) {
        this.label = s;
        return this;
    }
    
    public com.navdy.service.library.events.navigation.NavigationRouteRequest$Builder originDisplay(Boolean a) {
        this.originDisplay = a;
        return this;
    }
    
    public com.navdy.service.library.events.navigation.NavigationRouteRequest$Builder requestDestination(com.navdy.service.library.events.destination.Destination a) {
        this.requestDestination = a;
        return this;
    }
    
    public com.navdy.service.library.events.navigation.NavigationRouteRequest$Builder requestId(String s) {
        this.requestId = s;
        return this;
    }
    
    public com.navdy.service.library.events.navigation.NavigationRouteRequest$Builder routeAttributes(java.util.List a) {
        this.routeAttributes = com.navdy.service.library.events.navigation.NavigationRouteRequest$Builder.checkForNulls(a);
        return this;
    }
    
    public com.navdy.service.library.events.navigation.NavigationRouteRequest$Builder streetAddress(String s) {
        this.streetAddress = s;
        return this;
    }
    
    public com.navdy.service.library.events.navigation.NavigationRouteRequest$Builder waypoints(java.util.List a) {
        this.waypoints = com.navdy.service.library.events.navigation.NavigationRouteRequest$Builder.checkForNulls(a);
        return this;
    }
}
