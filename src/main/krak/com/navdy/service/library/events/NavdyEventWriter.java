package com.navdy.service.library.events;

public class NavdyEventWriter {
    final private static com.navdy.service.library.log.Logger sLogger;
    protected java.io.OutputStream mOutputStream;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.service.library.events.NavdyEventWriter.class);
    }
    
    public NavdyEventWriter(java.io.OutputStream a) {
        if (a == null) {
            throw new IllegalArgumentException();
        }
        this.mOutputStream = a;
    }
    
    public void write(byte[] a) {
        com.navdy.service.library.events.Frame a0 = new com.navdy.service.library.events.Frame(Integer.valueOf(a.length));
        this.mOutputStream.write(a0.toByteArray());
        this.mOutputStream.write(a);
        this.mOutputStream.flush();
        if (sLogger.isLoggable(2)) {
            sLogger.v(new StringBuilder().append("[Outgoing-Event] size:").append(a.length).toString());
        }
    }
}
