package com.navdy.service.library.events.messaging;

final public class SmsMessageRequest extends com.squareup.wire.Message {
    final public static String DEFAULT_ID = "";
    final public static String DEFAULT_MESSAGE = "";
    final public static String DEFAULT_NAME = "";
    final public static String DEFAULT_NUMBER = "";
    final private static long serialVersionUID = 0L;
    final public String id;
    final public String message;
    final public String name;
    final public String number;
    
    private SmsMessageRequest(com.navdy.service.library.events.messaging.SmsMessageRequest$Builder a) {
        this(a.number, a.message, a.name, a.id);
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    SmsMessageRequest(com.navdy.service.library.events.messaging.SmsMessageRequest$Builder a, com.navdy.service.library.events.messaging.SmsMessageRequest$1 a0) {
        this(a);
    }
    
    public SmsMessageRequest(String s, String s0, String s1, String s2) {
        this.number = s;
        this.message = s0;
        this.name = s1;
        this.id = s2;
    }
    
    public boolean equals(Object a) {
        boolean b = false;
        label0: if (a != this) {
            if (a instanceof com.navdy.service.library.events.messaging.SmsMessageRequest) {
                com.navdy.service.library.events.messaging.SmsMessageRequest a0 = (com.navdy.service.library.events.messaging.SmsMessageRequest)a;
                boolean b0 = this.equals(this.number, a0.number);
                label1: {
                    if (!b0) {
                        break label1;
                    }
                    if (!this.equals(this.message, a0.message)) {
                        break label1;
                    }
                    if (!this.equals(this.name, a0.name)) {
                        break label1;
                    }
                    if (this.equals(this.id, a0.id)) {
                        b = true;
                        break label0;
                    }
                }
                b = false;
            } else {
                b = false;
            }
        } else {
            b = true;
        }
        return b;
    }
    
    public int hashCode() {
        int i = this.hashCode;
        if (i == 0) {
            i = ((((this.number == null) ? 0 : this.number.hashCode()) * 37 + ((this.message == null) ? 0 : this.message.hashCode())) * 37 + ((this.name == null) ? 0 : this.name.hashCode())) * 37 + ((this.id == null) ? 0 : this.id.hashCode());
            this.hashCode = i;
        }
        return i;
    }
}
