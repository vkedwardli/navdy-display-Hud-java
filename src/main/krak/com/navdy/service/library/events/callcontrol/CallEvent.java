package com.navdy.service.library.events.callcontrol;

final public class CallEvent extends com.squareup.wire.Message {
    final public static Boolean DEFAULT_ANSWERED;
    final public static String DEFAULT_CONTACT_NAME = "";
    final public static Long DEFAULT_DURATION;
    final public static Boolean DEFAULT_INCOMING;
    final public static String DEFAULT_NUMBER = "";
    final public static Long DEFAULT_START_TIME;
    final private static long serialVersionUID = 0L;
    final public Boolean answered;
    final public String contact_name;
    final public Long duration;
    final public Boolean incoming;
    final public String number;
    final public Long start_time;
    
    static {
        DEFAULT_INCOMING = Boolean.valueOf(false);
        DEFAULT_ANSWERED = Boolean.valueOf(false);
        DEFAULT_START_TIME = Long.valueOf(0L);
        DEFAULT_DURATION = Long.valueOf(0L);
    }
    
    private CallEvent(com.navdy.service.library.events.callcontrol.CallEvent$Builder a) {
        this(a.incoming, a.answered, a.number, a.contact_name, a.start_time, a.duration);
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    CallEvent(com.navdy.service.library.events.callcontrol.CallEvent$Builder a, com.navdy.service.library.events.callcontrol.CallEvent$1 a0) {
        this(a);
    }
    
    public CallEvent(Boolean a, Boolean a0, String s, String s0, Long a1, Long a2) {
        this.incoming = a;
        this.answered = a0;
        this.number = s;
        this.contact_name = s0;
        this.start_time = a1;
        this.duration = a2;
    }
    
    public boolean equals(Object a) {
        boolean b = false;
        label0: if (a != this) {
            if (a instanceof com.navdy.service.library.events.callcontrol.CallEvent) {
                com.navdy.service.library.events.callcontrol.CallEvent a0 = (com.navdy.service.library.events.callcontrol.CallEvent)a;
                boolean b0 = this.equals(this.incoming, a0.incoming);
                label1: {
                    if (!b0) {
                        break label1;
                    }
                    if (!this.equals(this.answered, a0.answered)) {
                        break label1;
                    }
                    if (!this.equals(this.number, a0.number)) {
                        break label1;
                    }
                    if (!this.equals(this.contact_name, a0.contact_name)) {
                        break label1;
                    }
                    if (!this.equals(this.start_time, a0.start_time)) {
                        break label1;
                    }
                    if (this.equals(this.duration, a0.duration)) {
                        b = true;
                        break label0;
                    }
                }
                b = false;
            } else {
                b = false;
            }
        } else {
            b = true;
        }
        return b;
    }
    
    public int hashCode() {
        int i = this.hashCode;
        if (i == 0) {
            i = ((((((this.incoming == null) ? 0 : this.incoming.hashCode()) * 37 + ((this.answered == null) ? 0 : this.answered.hashCode())) * 37 + ((this.number == null) ? 0 : this.number.hashCode())) * 37 + ((this.contact_name == null) ? 0 : this.contact_name.hashCode())) * 37 + ((this.start_time == null) ? 0 : this.start_time.hashCode())) * 37 + ((this.duration == null) ? 0 : this.duration.hashCode());
            this.hashCode = i;
        }
        return i;
    }
}
