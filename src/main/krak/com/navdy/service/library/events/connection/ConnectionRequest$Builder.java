package com.navdy.service.library.events.connection;

final public class ConnectionRequest$Builder extends com.squareup.wire.Message.Builder {
    public com.navdy.service.library.events.connection.ConnectionRequest$Action action;
    public String remoteDeviceId;
    
    public ConnectionRequest$Builder() {
    }
    
    public ConnectionRequest$Builder(com.navdy.service.library.events.connection.ConnectionRequest a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.action = a.action;
            this.remoteDeviceId = a.remoteDeviceId;
        }
    }
    
    public com.navdy.service.library.events.connection.ConnectionRequest$Builder action(com.navdy.service.library.events.connection.ConnectionRequest$Action a) {
        this.action = a;
        return this;
    }
    
    public com.navdy.service.library.events.connection.ConnectionRequest build() {
        this.checkRequiredFields();
        return new com.navdy.service.library.events.connection.ConnectionRequest(this, (com.navdy.service.library.events.connection.ConnectionRequest$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
    
    public com.navdy.service.library.events.connection.ConnectionRequest$Builder remoteDeviceId(String s) {
        this.remoteDeviceId = s;
        return this;
    }
}
