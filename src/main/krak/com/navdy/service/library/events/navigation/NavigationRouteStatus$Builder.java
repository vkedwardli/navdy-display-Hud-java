package com.navdy.service.library.events.navigation;

final public class NavigationRouteStatus$Builder extends com.squareup.wire.Message.Builder {
    public String handle;
    public Integer progress;
    public String requestId;
    
    public NavigationRouteStatus$Builder() {
    }
    
    public NavigationRouteStatus$Builder(com.navdy.service.library.events.navigation.NavigationRouteStatus a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.handle = a.handle;
            this.progress = a.progress;
            this.requestId = a.requestId;
        }
    }
    
    public com.navdy.service.library.events.navigation.NavigationRouteStatus build() {
        return new com.navdy.service.library.events.navigation.NavigationRouteStatus(this, (com.navdy.service.library.events.navigation.NavigationRouteStatus$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
    
    public com.navdy.service.library.events.navigation.NavigationRouteStatus$Builder handle(String s) {
        this.handle = s;
        return this;
    }
    
    public com.navdy.service.library.events.navigation.NavigationRouteStatus$Builder progress(Integer a) {
        this.progress = a;
        return this;
    }
    
    public com.navdy.service.library.events.navigation.NavigationRouteStatus$Builder requestId(String s) {
        this.requestId = s;
        return this;
    }
}
