package com.navdy.service.library.events.places;

final public class SuggestedDestination extends com.squareup.wire.Message {
    final public static Integer DEFAULT_DURATION_TRAFFIC;
    final public static com.navdy.service.library.events.places.SuggestedDestination$SuggestionType DEFAULT_TYPE;
    final private static long serialVersionUID = 0L;
    final public com.navdy.service.library.events.destination.Destination destination;
    final public Integer duration_traffic;
    final public com.navdy.service.library.events.places.SuggestedDestination$SuggestionType type;
    
    static {
        DEFAULT_DURATION_TRAFFIC = Integer.valueOf(0);
        DEFAULT_TYPE = com.navdy.service.library.events.places.SuggestedDestination$SuggestionType.SUGGESTION_RECOMMENDATION;
    }
    
    public SuggestedDestination(com.navdy.service.library.events.destination.Destination a, Integer a0, com.navdy.service.library.events.places.SuggestedDestination$SuggestionType a1) {
        this.destination = a;
        this.duration_traffic = a0;
        this.type = a1;
    }
    
    private SuggestedDestination(com.navdy.service.library.events.places.SuggestedDestination$Builder a) {
        this(a.destination, a.duration_traffic, a.type);
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    SuggestedDestination(com.navdy.service.library.events.places.SuggestedDestination$Builder a, com.navdy.service.library.events.places.SuggestedDestination$1 a0) {
        this(a);
    }
    
    public boolean equals(Object a) {
        boolean b = false;
        label0: if (a != this) {
            if (a instanceof com.navdy.service.library.events.places.SuggestedDestination) {
                com.navdy.service.library.events.places.SuggestedDestination a0 = (com.navdy.service.library.events.places.SuggestedDestination)a;
                boolean b0 = this.equals(this.destination, a0.destination);
                label1: {
                    if (!b0) {
                        break label1;
                    }
                    if (!this.equals(this.duration_traffic, a0.duration_traffic)) {
                        break label1;
                    }
                    if (this.equals(this.type, a0.type)) {
                        b = true;
                        break label0;
                    }
                }
                b = false;
            } else {
                b = false;
            }
        } else {
            b = true;
        }
        return b;
    }
    
    public int hashCode() {
        int i = this.hashCode;
        if (i == 0) {
            i = (((this.destination == null) ? 0 : this.destination.hashCode()) * 37 + ((this.duration_traffic == null) ? 0 : this.duration_traffic.hashCode())) * 37 + ((this.type == null) ? 0 : this.type.hashCode());
            this.hashCode = i;
        }
        return i;
    }
}
