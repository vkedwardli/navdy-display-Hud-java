package com.navdy.service.library.events.notification;

final public class NotificationSetting extends com.squareup.wire.Message {
    final public static String DEFAULT_APP = "";
    final public static Boolean DEFAULT_ENABLED;
    final private static long serialVersionUID = 0L;
    final public String app;
    final public Boolean enabled;
    
    static {
        DEFAULT_ENABLED = Boolean.valueOf(false);
    }
    
    private NotificationSetting(com.navdy.service.library.events.notification.NotificationSetting$Builder a) {
        this(a.app, a.enabled);
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    NotificationSetting(com.navdy.service.library.events.notification.NotificationSetting$Builder a, com.navdy.service.library.events.notification.NotificationSetting$1 a0) {
        this(a);
    }
    
    public NotificationSetting(String s, Boolean a) {
        this.app = s;
        this.enabled = a;
    }
    
    public boolean equals(Object a) {
        boolean b = false;
        label0: if (a != this) {
            if (a instanceof com.navdy.service.library.events.notification.NotificationSetting) {
                com.navdy.service.library.events.notification.NotificationSetting a0 = (com.navdy.service.library.events.notification.NotificationSetting)a;
                boolean b0 = this.equals(this.app, a0.app);
                label1: {
                    if (!b0) {
                        break label1;
                    }
                    if (this.equals(this.enabled, a0.enabled)) {
                        b = true;
                        break label0;
                    }
                }
                b = false;
            } else {
                b = false;
            }
        } else {
            b = true;
        }
        return b;
    }
    
    public int hashCode() {
        int i = this.hashCode;
        if (i == 0) {
            i = ((this.app == null) ? 0 : this.app.hashCode()) * 37 + ((this.enabled == null) ? 0 : this.enabled.hashCode());
            this.hashCode = i;
        }
        return i;
    }
}
