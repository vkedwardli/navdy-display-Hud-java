package com.navdy.service.library.events.debug;

final public class StopDriveRecordingEvent extends com.squareup.wire.Message {
    final private static long serialVersionUID = 0L;
    
    public StopDriveRecordingEvent() {
    }
    
    private StopDriveRecordingEvent(com.navdy.service.library.events.debug.StopDriveRecordingEvent$Builder a) {
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    StopDriveRecordingEvent(com.navdy.service.library.events.debug.StopDriveRecordingEvent$Builder a, com.navdy.service.library.events.debug.StopDriveRecordingEvent$1 a0) {
        this(a);
    }
    
    public boolean equals(Object a) {
        return a instanceof com.navdy.service.library.events.debug.StopDriveRecordingEvent;
    }
    
    public int hashCode() {
        return 0;
    }
}
