package com.navdy.service.library.events.photo;

final public class PhotoUpdateQueryResponse extends com.squareup.wire.Message {
    final public static String DEFAULT_IDENTIFIER = "";
    final public static Boolean DEFAULT_UPDATEREQUIRED;
    final private static long serialVersionUID = 0L;
    final public String identifier;
    final public Boolean updateRequired;
    
    static {
        DEFAULT_UPDATEREQUIRED = Boolean.valueOf(false);
    }
    
    private PhotoUpdateQueryResponse(com.navdy.service.library.events.photo.PhotoUpdateQueryResponse$Builder a) {
        this(a.identifier, a.updateRequired);
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    PhotoUpdateQueryResponse(com.navdy.service.library.events.photo.PhotoUpdateQueryResponse$Builder a, com.navdy.service.library.events.photo.PhotoUpdateQueryResponse$1 a0) {
        this(a);
    }
    
    public PhotoUpdateQueryResponse(String s, Boolean a) {
        this.identifier = s;
        this.updateRequired = a;
    }
    
    public boolean equals(Object a) {
        boolean b = false;
        label0: if (a != this) {
            if (a instanceof com.navdy.service.library.events.photo.PhotoUpdateQueryResponse) {
                com.navdy.service.library.events.photo.PhotoUpdateQueryResponse a0 = (com.navdy.service.library.events.photo.PhotoUpdateQueryResponse)a;
                boolean b0 = this.equals(this.identifier, a0.identifier);
                label1: {
                    if (!b0) {
                        break label1;
                    }
                    if (this.equals(this.updateRequired, a0.updateRequired)) {
                        b = true;
                        break label0;
                    }
                }
                b = false;
            } else {
                b = false;
            }
        } else {
            b = true;
        }
        return b;
    }
    
    public int hashCode() {
        int i = this.hashCode;
        if (i == 0) {
            i = ((this.identifier == null) ? 0 : this.identifier.hashCode()) * 37 + ((this.updateRequired == null) ? 0 : this.updateRequired.hashCode());
            this.hashCode = i;
        }
        return i;
    }
}
