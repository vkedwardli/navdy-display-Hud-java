package com.navdy.service.library.events.calendars;

final public class CalendarEvent$Builder extends com.squareup.wire.Message.Builder {
    public Boolean all_day;
    public String calendar_name;
    public Integer color;
    public com.navdy.service.library.events.destination.Destination destination;
    public String display_name;
    public Long end_time;
    public String location;
    public Long start_time;
    
    public CalendarEvent$Builder() {
    }
    
    public CalendarEvent$Builder(com.navdy.service.library.events.calendars.CalendarEvent a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.display_name = a.display_name;
            this.start_time = a.start_time;
            this.end_time = a.end_time;
            this.all_day = a.all_day;
            this.color = a.color;
            this.location = a.location;
            this.calendar_name = a.calendar_name;
            this.destination = a.destination;
        }
    }
    
    public com.navdy.service.library.events.calendars.CalendarEvent$Builder all_day(Boolean a) {
        this.all_day = a;
        return this;
    }
    
    public com.navdy.service.library.events.calendars.CalendarEvent build() {
        return new com.navdy.service.library.events.calendars.CalendarEvent(this, (com.navdy.service.library.events.calendars.CalendarEvent$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
    
    public com.navdy.service.library.events.calendars.CalendarEvent$Builder calendar_name(String s) {
        this.calendar_name = s;
        return this;
    }
    
    public com.navdy.service.library.events.calendars.CalendarEvent$Builder color(Integer a) {
        this.color = a;
        return this;
    }
    
    public com.navdy.service.library.events.calendars.CalendarEvent$Builder destination(com.navdy.service.library.events.destination.Destination a) {
        this.destination = a;
        return this;
    }
    
    public com.navdy.service.library.events.calendars.CalendarEvent$Builder display_name(String s) {
        this.display_name = s;
        return this;
    }
    
    public com.navdy.service.library.events.calendars.CalendarEvent$Builder end_time(Long a) {
        this.end_time = a;
        return this;
    }
    
    public com.navdy.service.library.events.calendars.CalendarEvent$Builder location(String s) {
        this.location = s;
        return this;
    }
    
    public com.navdy.service.library.events.calendars.CalendarEvent$Builder start_time(Long a) {
        this.start_time = a;
        return this;
    }
}
