package com.navdy.service.library.events;

final public class TripUpdate extends com.squareup.wire.Message {
    final public static String DEFAULT_ARRIVED_AT_DESTINATION_ID = "";
    final public static Float DEFAULT_BEARING;
    final public static String DEFAULT_CHOSEN_DESTINATION_ID = "";
    final public static Integer DEFAULT_DISTANCE_TO_DESTINATION;
    final public static Integer DEFAULT_DISTANCE_TRAVELED;
    final public static Double DEFAULT_ELEVATION;
    final public static Float DEFAULT_ELEVATION_ACCURACY;
    final public static Integer DEFAULT_ESTIMATED_TIME_REMAINING;
    final public static Double DEFAULT_EXCESSIVE_SPEEDING_RATIO;
    final public static Float DEFAULT_GPS_SPEED;
    final public static Integer DEFAULT_HARD_ACCELERATION_COUNT;
    final public static Integer DEFAULT_HARD_BREAKING_COUNT;
    final public static Integer DEFAULT_HIGH_G_COUNT;
    final public static Float DEFAULT_HORIZONTAL_ACCURACY;
    final public static Integer DEFAULT_METERS_TRAVELED_SINCE_BOOT;
    final public static Integer DEFAULT_OBD_SPEED;
    final public static String DEFAULT_ROAD_ELEMENT = "";
    final public static Integer DEFAULT_SEQUENCE_NUMBER;
    final public static Double DEFAULT_SPEEDING_RATIO;
    final public static Long DEFAULT_TIMESTAMP;
    final public static Long DEFAULT_TRIP_NUMBER;
    final private static long serialVersionUID = 0L;
    final public String arrived_at_destination_id;
    final public Float bearing;
    final public String chosen_destination_id;
    final public com.navdy.service.library.events.location.LatLong current_position;
    final public Integer distance_to_destination;
    final public Integer distance_traveled;
    final public Double elevation;
    final public Float elevation_accuracy;
    final public Integer estimated_time_remaining;
    final public Double excessive_speeding_ratio;
    final public Float gps_speed;
    final public Integer hard_acceleration_count;
    final public Integer hard_breaking_count;
    final public Integer high_g_count;
    final public Float horizontal_accuracy;
    final public com.navdy.service.library.events.location.Coordinate last_raw_coordinate;
    final public Integer meters_traveled_since_boot;
    final public Integer obd_speed;
    final public String road_element;
    final public Integer sequence_number;
    final public Double speeding_ratio;
    final public Long timestamp;
    final public Long trip_number;
    
    static {
        DEFAULT_TRIP_NUMBER = Long.valueOf(0L);
        DEFAULT_SEQUENCE_NUMBER = Integer.valueOf(0);
        DEFAULT_TIMESTAMP = Long.valueOf(0L);
        DEFAULT_DISTANCE_TRAVELED = Integer.valueOf(0);
        DEFAULT_ELEVATION = Double.valueOf(0.0);
        DEFAULT_BEARING = Float.valueOf(0.0f);
        DEFAULT_GPS_SPEED = Float.valueOf(0.0f);
        DEFAULT_OBD_SPEED = Integer.valueOf(0);
        DEFAULT_ESTIMATED_TIME_REMAINING = Integer.valueOf(0);
        DEFAULT_DISTANCE_TO_DESTINATION = Integer.valueOf(0);
        DEFAULT_HIGH_G_COUNT = Integer.valueOf(0);
        DEFAULT_HARD_BREAKING_COUNT = Integer.valueOf(0);
        DEFAULT_HARD_ACCELERATION_COUNT = Integer.valueOf(0);
        DEFAULT_SPEEDING_RATIO = Double.valueOf(0.0);
        DEFAULT_EXCESSIVE_SPEEDING_RATIO = Double.valueOf(0.0);
        DEFAULT_METERS_TRAVELED_SINCE_BOOT = Integer.valueOf(0);
        DEFAULT_HORIZONTAL_ACCURACY = Float.valueOf(0.0f);
        DEFAULT_ELEVATION_ACCURACY = Float.valueOf(0.0f);
    }
    
    private TripUpdate(com.navdy.service.library.events.TripUpdate$Builder a) {
        this(a.trip_number, a.sequence_number, a.timestamp, a.distance_traveled, a.current_position, a.elevation, a.bearing, a.gps_speed, a.obd_speed, a.road_element, a.chosen_destination_id, a.arrived_at_destination_id, a.estimated_time_remaining, a.distance_to_destination, a.high_g_count, a.hard_breaking_count, a.hard_acceleration_count, a.speeding_ratio, a.excessive_speeding_ratio, a.meters_traveled_since_boot, a.horizontal_accuracy, a.elevation_accuracy, a.last_raw_coordinate);
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    TripUpdate(com.navdy.service.library.events.TripUpdate$Builder a, com.navdy.service.library.events.TripUpdate$1 a0) {
        this(a);
    }
    
    public TripUpdate(Long a, Integer a0, Long a1, Integer a2, com.navdy.service.library.events.location.LatLong a3, Double a4, Float a5, Float a6, Integer a7, String s, String s0, String s1, Integer a8, Integer a9, Integer a10, Integer a11, Integer a12, Double a13, Double a14, Integer a15, Float a16, Float a17, com.navdy.service.library.events.location.Coordinate a18) {
        this.trip_number = a;
        this.sequence_number = a0;
        this.timestamp = a1;
        this.distance_traveled = a2;
        this.current_position = a3;
        this.elevation = a4;
        this.bearing = a5;
        this.gps_speed = a6;
        this.obd_speed = a7;
        this.road_element = s;
        this.chosen_destination_id = s0;
        this.arrived_at_destination_id = s1;
        this.estimated_time_remaining = a8;
        this.distance_to_destination = a9;
        this.high_g_count = a10;
        this.hard_breaking_count = a11;
        this.hard_acceleration_count = a12;
        this.speeding_ratio = a13;
        this.excessive_speeding_ratio = a14;
        this.meters_traveled_since_boot = a15;
        this.horizontal_accuracy = a16;
        this.elevation_accuracy = a17;
        this.last_raw_coordinate = a18;
    }
    
    public boolean equals(Object a) {
        boolean b = false;
        label0: if (a != this) {
            if (a instanceof com.navdy.service.library.events.TripUpdate) {
                com.navdy.service.library.events.TripUpdate a0 = (com.navdy.service.library.events.TripUpdate)a;
                boolean b0 = this.equals(this.trip_number, a0.trip_number);
                label1: {
                    if (!b0) {
                        break label1;
                    }
                    if (!this.equals(this.sequence_number, a0.sequence_number)) {
                        break label1;
                    }
                    if (!this.equals(this.timestamp, a0.timestamp)) {
                        break label1;
                    }
                    if (!this.equals(this.distance_traveled, a0.distance_traveled)) {
                        break label1;
                    }
                    if (!this.equals(this.current_position, a0.current_position)) {
                        break label1;
                    }
                    if (!this.equals(this.elevation, a0.elevation)) {
                        break label1;
                    }
                    if (!this.equals(this.bearing, a0.bearing)) {
                        break label1;
                    }
                    if (!this.equals(this.gps_speed, a0.gps_speed)) {
                        break label1;
                    }
                    if (!this.equals(this.obd_speed, a0.obd_speed)) {
                        break label1;
                    }
                    if (!this.equals(this.road_element, a0.road_element)) {
                        break label1;
                    }
                    if (!this.equals(this.chosen_destination_id, a0.chosen_destination_id)) {
                        break label1;
                    }
                    if (!this.equals(this.arrived_at_destination_id, a0.arrived_at_destination_id)) {
                        break label1;
                    }
                    if (!this.equals(this.estimated_time_remaining, a0.estimated_time_remaining)) {
                        break label1;
                    }
                    if (!this.equals(this.distance_to_destination, a0.distance_to_destination)) {
                        break label1;
                    }
                    if (!this.equals(this.high_g_count, a0.high_g_count)) {
                        break label1;
                    }
                    if (!this.equals(this.hard_breaking_count, a0.hard_breaking_count)) {
                        break label1;
                    }
                    if (!this.equals(this.hard_acceleration_count, a0.hard_acceleration_count)) {
                        break label1;
                    }
                    if (!this.equals(this.speeding_ratio, a0.speeding_ratio)) {
                        break label1;
                    }
                    if (!this.equals(this.excessive_speeding_ratio, a0.excessive_speeding_ratio)) {
                        break label1;
                    }
                    if (!this.equals(this.meters_traveled_since_boot, a0.meters_traveled_since_boot)) {
                        break label1;
                    }
                    if (!this.equals(this.horizontal_accuracy, a0.horizontal_accuracy)) {
                        break label1;
                    }
                    if (!this.equals(this.elevation_accuracy, a0.elevation_accuracy)) {
                        break label1;
                    }
                    if (this.equals(this.last_raw_coordinate, a0.last_raw_coordinate)) {
                        b = true;
                        break label0;
                    }
                }
                b = false;
            } else {
                b = false;
            }
        } else {
            b = true;
        }
        return b;
    }
    
    public int hashCode() {
        int i = this.hashCode;
        if (i == 0) {
            int i0 = (this.trip_number == null) ? 0 : this.trip_number.hashCode();
            int i1 = (this.sequence_number == null) ? 0 : this.sequence_number.hashCode();
            int i2 = (this.timestamp == null) ? 0 : this.timestamp.hashCode();
            int i3 = (this.distance_traveled == null) ? 0 : this.distance_traveled.hashCode();
            int i4 = (this.current_position == null) ? 0 : this.current_position.hashCode();
            int i5 = (this.elevation == null) ? 0 : this.elevation.hashCode();
            int i6 = (this.bearing == null) ? 0 : this.bearing.hashCode();
            int i7 = (this.gps_speed == null) ? 0 : this.gps_speed.hashCode();
            int i8 = (this.obd_speed == null) ? 0 : this.obd_speed.hashCode();
            int i9 = (this.road_element == null) ? 0 : this.road_element.hashCode();
            int i10 = (this.chosen_destination_id == null) ? 0 : this.chosen_destination_id.hashCode();
            int i11 = (this.arrived_at_destination_id == null) ? 0 : this.arrived_at_destination_id.hashCode();
            int i12 = (this.estimated_time_remaining == null) ? 0 : this.estimated_time_remaining.hashCode();
            int i13 = (this.distance_to_destination == null) ? 0 : this.distance_to_destination.hashCode();
            int i14 = (this.high_g_count == null) ? 0 : this.high_g_count.hashCode();
            int i15 = (this.hard_breaking_count == null) ? 0 : this.hard_breaking_count.hashCode();
            int i16 = (this.hard_acceleration_count == null) ? 0 : this.hard_acceleration_count.hashCode();
            int i17 = (this.speeding_ratio == null) ? 0 : this.speeding_ratio.hashCode();
            int i18 = (this.excessive_speeding_ratio == null) ? 0 : this.excessive_speeding_ratio.hashCode();
            int i19 = (this.meters_traveled_since_boot == null) ? 0 : this.meters_traveled_since_boot.hashCode();
            int i20 = (this.horizontal_accuracy == null) ? 0 : this.horizontal_accuracy.hashCode();
            int i21 = (this.elevation_accuracy == null) ? 0 : this.elevation_accuracy.hashCode();
            int i22 = (this.last_raw_coordinate == null) ? 0 : this.last_raw_coordinate.hashCode();
            int i23 = ((((((i0 * 37 + i1) * 37 + i2) * 37 + i3) * 37 + i4) * 37 + i5) * 37 + i6) * 37 + i7;
            i = ((((((((((((((i23 * 37 + i8) * 37 + i9) * 37 + i10) * 37 + i11) * 37 + i12) * 37 + i13) * 37 + i14) * 37 + i15) * 37 + i16) * 37 + i17) * 37 + i18) * 37 + i19) * 37 + i20) * 37 + i21) * 37 + i22;
            this.hashCode = i;
        }
        return i;
    }
}
