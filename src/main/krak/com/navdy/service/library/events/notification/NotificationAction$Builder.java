package com.navdy.service.library.events.notification;

final public class NotificationAction$Builder extends com.squareup.wire.Message.Builder {
    public Integer actionId;
    public String handler;
    public Integer iconResource;
    public Integer iconResourceFocused;
    public Integer id;
    public String label;
    public Integer labelId;
    public Integer notificationId;
    
    public NotificationAction$Builder() {
    }
    
    public NotificationAction$Builder(com.navdy.service.library.events.notification.NotificationAction a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.handler = a.handler;
            this.id = a.id;
            this.notificationId = a.notificationId;
            this.actionId = a.actionId;
            this.labelId = a.labelId;
            this.label = a.label;
            this.iconResource = a.iconResource;
            this.iconResourceFocused = a.iconResourceFocused;
        }
    }
    
    public com.navdy.service.library.events.notification.NotificationAction$Builder actionId(Integer a) {
        this.actionId = a;
        return this;
    }
    
    public com.navdy.service.library.events.notification.NotificationAction build() {
        this.checkRequiredFields();
        return new com.navdy.service.library.events.notification.NotificationAction(this, (com.navdy.service.library.events.notification.NotificationAction$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
    
    public com.navdy.service.library.events.notification.NotificationAction$Builder handler(String s) {
        this.handler = s;
        return this;
    }
    
    public com.navdy.service.library.events.notification.NotificationAction$Builder iconResource(Integer a) {
        this.iconResource = a;
        return this;
    }
    
    public com.navdy.service.library.events.notification.NotificationAction$Builder iconResourceFocused(Integer a) {
        this.iconResourceFocused = a;
        return this;
    }
    
    public com.navdy.service.library.events.notification.NotificationAction$Builder id(Integer a) {
        this.id = a;
        return this;
    }
    
    public com.navdy.service.library.events.notification.NotificationAction$Builder label(String s) {
        this.label = s;
        return this;
    }
    
    public com.navdy.service.library.events.notification.NotificationAction$Builder labelId(Integer a) {
        this.labelId = a;
        return this;
    }
    
    public com.navdy.service.library.events.notification.NotificationAction$Builder notificationId(Integer a) {
        this.notificationId = a;
        return this;
    }
}
