package com.navdy.service.library.events.glances;

final public class CalendarConstants extends Enum implements com.squareup.wire.ProtoEnum {
    final private static com.navdy.service.library.events.glances.CalendarConstants[] $VALUES;
    final public static com.navdy.service.library.events.glances.CalendarConstants CALENDAR_LOCATION;
    final public static com.navdy.service.library.events.glances.CalendarConstants CALENDAR_TIME;
    final public static com.navdy.service.library.events.glances.CalendarConstants CALENDAR_TIME_STR;
    final public static com.navdy.service.library.events.glances.CalendarConstants CALENDAR_TITLE;
    final private int value;
    
    static {
        CALENDAR_TITLE = new com.navdy.service.library.events.glances.CalendarConstants("CALENDAR_TITLE", 0, 0);
        CALENDAR_TIME = new com.navdy.service.library.events.glances.CalendarConstants("CALENDAR_TIME", 1, 1);
        CALENDAR_TIME_STR = new com.navdy.service.library.events.glances.CalendarConstants("CALENDAR_TIME_STR", 2, 2);
        CALENDAR_LOCATION = new com.navdy.service.library.events.glances.CalendarConstants("CALENDAR_LOCATION", 3, 3);
        com.navdy.service.library.events.glances.CalendarConstants[] a = new com.navdy.service.library.events.glances.CalendarConstants[4];
        a[0] = CALENDAR_TITLE;
        a[1] = CALENDAR_TIME;
        a[2] = CALENDAR_TIME_STR;
        a[3] = CALENDAR_LOCATION;
        $VALUES = a;
    }
    
    private CalendarConstants(String s, int i, int i0) {
        super(s, i);
        this.value = i0;
    }
    
    public static com.navdy.service.library.events.glances.CalendarConstants valueOf(String s) {
        return (com.navdy.service.library.events.glances.CalendarConstants)Enum.valueOf(com.navdy.service.library.events.glances.CalendarConstants.class, s);
    }
    
    public static com.navdy.service.library.events.glances.CalendarConstants[] values() {
        return $VALUES.clone();
    }
    
    public int getValue() {
        return this.value;
    }
}
