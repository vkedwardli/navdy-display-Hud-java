package com.navdy.service.library.events.notification;

final public class NotificationsError extends Enum implements com.squareup.wire.ProtoEnum {
    final private static com.navdy.service.library.events.notification.NotificationsError[] $VALUES;
    final public static com.navdy.service.library.events.notification.NotificationsError NOTIFICATIONS_ERROR_AUTH_FAILED;
    final public static com.navdy.service.library.events.notification.NotificationsError NOTIFICATIONS_ERROR_BOND_REMOVED;
    final public static com.navdy.service.library.events.notification.NotificationsError NOTIFICATIONS_ERROR_UNKNOWN;
    final private int value;
    
    static {
        NOTIFICATIONS_ERROR_UNKNOWN = new com.navdy.service.library.events.notification.NotificationsError("NOTIFICATIONS_ERROR_UNKNOWN", 0, 1);
        NOTIFICATIONS_ERROR_AUTH_FAILED = new com.navdy.service.library.events.notification.NotificationsError("NOTIFICATIONS_ERROR_AUTH_FAILED", 1, 2);
        NOTIFICATIONS_ERROR_BOND_REMOVED = new com.navdy.service.library.events.notification.NotificationsError("NOTIFICATIONS_ERROR_BOND_REMOVED", 2, 3);
        com.navdy.service.library.events.notification.NotificationsError[] a = new com.navdy.service.library.events.notification.NotificationsError[3];
        a[0] = NOTIFICATIONS_ERROR_UNKNOWN;
        a[1] = NOTIFICATIONS_ERROR_AUTH_FAILED;
        a[2] = NOTIFICATIONS_ERROR_BOND_REMOVED;
        $VALUES = a;
    }
    
    private NotificationsError(String s, int i, int i0) {
        super(s, i);
        this.value = i0;
    }
    
    public static com.navdy.service.library.events.notification.NotificationsError valueOf(String s) {
        return (com.navdy.service.library.events.notification.NotificationsError)Enum.valueOf(com.navdy.service.library.events.notification.NotificationsError.class, s);
    }
    
    public static com.navdy.service.library.events.notification.NotificationsError[] values() {
        return $VALUES.clone();
    }
    
    public int getValue() {
        return this.value;
    }
}
