package com.navdy.service.library.events.preferences;

final public class DriverProfilePreferencesRequest extends com.squareup.wire.Message {
    final public static Long DEFAULT_SERIAL_NUMBER;
    final private static long serialVersionUID = 0L;
    final public Long serial_number;
    
    static {
        DEFAULT_SERIAL_NUMBER = Long.valueOf(0L);
    }
    
    private DriverProfilePreferencesRequest(com.navdy.service.library.events.preferences.DriverProfilePreferencesRequest$Builder a) {
        this(a.serial_number);
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    DriverProfilePreferencesRequest(com.navdy.service.library.events.preferences.DriverProfilePreferencesRequest$Builder a, com.navdy.service.library.events.preferences.DriverProfilePreferencesRequest$1 a0) {
        this(a);
    }
    
    public DriverProfilePreferencesRequest(Long a) {
        this.serial_number = a;
    }
    
    public boolean equals(Object a) {
        return a == this || a instanceof com.navdy.service.library.events.preferences.DriverProfilePreferencesRequest && this.equals(this.serial_number, ((com.navdy.service.library.events.preferences.DriverProfilePreferencesRequest)a).serial_number);
    }
    
    public int hashCode() {
        int i = this.hashCode;
        if (i == 0) {
            i = (this.serial_number == null) ? 0 : this.serial_number.hashCode();
            this.hashCode = i;
        }
        return i;
    }
}
