package com.navdy.service.library.events.preferences;

final public class NavigationPreferencesRequest$Builder extends com.squareup.wire.Message.Builder {
    public Long serial_number;
    
    public NavigationPreferencesRequest$Builder() {
    }
    
    public NavigationPreferencesRequest$Builder(com.navdy.service.library.events.preferences.NavigationPreferencesRequest a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.serial_number = a.serial_number;
        }
    }
    
    public com.navdy.service.library.events.preferences.NavigationPreferencesRequest build() {
        return new com.navdy.service.library.events.preferences.NavigationPreferencesRequest(this, (com.navdy.service.library.events.preferences.NavigationPreferencesRequest$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
    
    public com.navdy.service.library.events.preferences.NavigationPreferencesRequest$Builder serial_number(Long a) {
        this.serial_number = a;
        return this;
    }
}
