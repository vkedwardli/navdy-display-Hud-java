package com.navdy.service.library.events.preferences;

final public class DisplaySpeakerPreferencesRequest$Builder extends com.squareup.wire.Message.Builder {
    public Long serial_number;
    
    public DisplaySpeakerPreferencesRequest$Builder() {
    }
    
    public DisplaySpeakerPreferencesRequest$Builder(com.navdy.service.library.events.preferences.DisplaySpeakerPreferencesRequest a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.serial_number = a.serial_number;
        }
    }
    
    public com.navdy.service.library.events.preferences.DisplaySpeakerPreferencesRequest build() {
        return new com.navdy.service.library.events.preferences.DisplaySpeakerPreferencesRequest(this, (com.navdy.service.library.events.preferences.DisplaySpeakerPreferencesRequest$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
    
    public com.navdy.service.library.events.preferences.DisplaySpeakerPreferencesRequest$Builder serial_number(Long a) {
        this.serial_number = a;
        return this;
    }
}
