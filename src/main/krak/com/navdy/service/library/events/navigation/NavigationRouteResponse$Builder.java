package com.navdy.service.library.events.navigation;

final public class NavigationRouteResponse$Builder extends com.squareup.wire.Message.Builder {
    public Boolean consideredTraffic;
    public com.navdy.service.library.events.location.Coordinate destination;
    public String label;
    public String requestId;
    public java.util.List results;
    public com.navdy.service.library.events.RequestStatus status;
    public String statusDetail;
    
    public NavigationRouteResponse$Builder() {
    }
    
    public NavigationRouteResponse$Builder(com.navdy.service.library.events.navigation.NavigationRouteResponse a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.status = a.status;
            this.statusDetail = a.statusDetail;
            this.destination = a.destination;
            this.label = a.label;
            this.results = com.navdy.service.library.events.navigation.NavigationRouteResponse.access$000(a.results);
            this.consideredTraffic = a.consideredTraffic;
            this.requestId = a.requestId;
        }
    }
    
    public com.navdy.service.library.events.navigation.NavigationRouteResponse build() {
        this.checkRequiredFields();
        return new com.navdy.service.library.events.navigation.NavigationRouteResponse(this, (com.navdy.service.library.events.navigation.NavigationRouteResponse$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
    
    public com.navdy.service.library.events.navigation.NavigationRouteResponse$Builder consideredTraffic(Boolean a) {
        this.consideredTraffic = a;
        return this;
    }
    
    public com.navdy.service.library.events.navigation.NavigationRouteResponse$Builder destination(com.navdy.service.library.events.location.Coordinate a) {
        this.destination = a;
        return this;
    }
    
    public com.navdy.service.library.events.navigation.NavigationRouteResponse$Builder label(String s) {
        this.label = s;
        return this;
    }
    
    public com.navdy.service.library.events.navigation.NavigationRouteResponse$Builder requestId(String s) {
        this.requestId = s;
        return this;
    }
    
    public com.navdy.service.library.events.navigation.NavigationRouteResponse$Builder results(java.util.List a) {
        this.results = com.navdy.service.library.events.navigation.NavigationRouteResponse$Builder.checkForNulls(a);
        return this;
    }
    
    public com.navdy.service.library.events.navigation.NavigationRouteResponse$Builder status(com.navdy.service.library.events.RequestStatus a) {
        this.status = a;
        return this;
    }
    
    public com.navdy.service.library.events.navigation.NavigationRouteResponse$Builder statusDetail(String s) {
        this.statusDetail = s;
        return this;
    }
}
