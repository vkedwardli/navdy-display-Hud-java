package com.navdy.service.library.events.preferences;

final public class NavigationPreferencesRequest extends com.squareup.wire.Message {
    final public static Long DEFAULT_SERIAL_NUMBER;
    final private static long serialVersionUID = 0L;
    final public Long serial_number;
    
    static {
        DEFAULT_SERIAL_NUMBER = Long.valueOf(0L);
    }
    
    private NavigationPreferencesRequest(com.navdy.service.library.events.preferences.NavigationPreferencesRequest$Builder a) {
        this(a.serial_number);
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    NavigationPreferencesRequest(com.navdy.service.library.events.preferences.NavigationPreferencesRequest$Builder a, com.navdy.service.library.events.preferences.NavigationPreferencesRequest$1 a0) {
        this(a);
    }
    
    public NavigationPreferencesRequest(Long a) {
        this.serial_number = a;
    }
    
    public boolean equals(Object a) {
        return a == this || a instanceof com.navdy.service.library.events.preferences.NavigationPreferencesRequest && this.equals(this.serial_number, ((com.navdy.service.library.events.preferences.NavigationPreferencesRequest)a).serial_number);
    }
    
    public int hashCode() {
        int i = this.hashCode;
        if (i == 0) {
            i = (this.serial_number == null) ? 0 : this.serial_number.hashCode();
            this.hashCode = i;
        }
        return i;
    }
}
