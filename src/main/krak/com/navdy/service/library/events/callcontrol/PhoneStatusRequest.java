package com.navdy.service.library.events.callcontrol;

final public class PhoneStatusRequest extends com.squareup.wire.Message {
    final private static long serialVersionUID = 0L;
    
    public PhoneStatusRequest() {
    }
    
    private PhoneStatusRequest(com.navdy.service.library.events.callcontrol.PhoneStatusRequest$Builder a) {
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    PhoneStatusRequest(com.navdy.service.library.events.callcontrol.PhoneStatusRequest$Builder a, com.navdy.service.library.events.callcontrol.PhoneStatusRequest$1 a0) {
        this(a);
    }
    
    public boolean equals(Object a) {
        return a instanceof com.navdy.service.library.events.callcontrol.PhoneStatusRequest;
    }
    
    public int hashCode() {
        return 0;
    }
}
