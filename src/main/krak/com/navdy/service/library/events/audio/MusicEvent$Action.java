package com.navdy.service.library.events.audio;

final public class MusicEvent$Action extends Enum implements com.squareup.wire.ProtoEnum {
    final private static com.navdy.service.library.events.audio.MusicEvent$Action[] $VALUES;
    final public static com.navdy.service.library.events.audio.MusicEvent$Action MUSIC_ACTION_FAST_FORWARD_START;
    final public static com.navdy.service.library.events.audio.MusicEvent$Action MUSIC_ACTION_FAST_FORWARD_STOP;
    final public static com.navdy.service.library.events.audio.MusicEvent$Action MUSIC_ACTION_MODE_CHANGE;
    final public static com.navdy.service.library.events.audio.MusicEvent$Action MUSIC_ACTION_NEXT;
    final public static com.navdy.service.library.events.audio.MusicEvent$Action MUSIC_ACTION_PAUSE;
    final public static com.navdy.service.library.events.audio.MusicEvent$Action MUSIC_ACTION_PLAY;
    final public static com.navdy.service.library.events.audio.MusicEvent$Action MUSIC_ACTION_PREVIOUS;
    final public static com.navdy.service.library.events.audio.MusicEvent$Action MUSIC_ACTION_REWIND_START;
    final public static com.navdy.service.library.events.audio.MusicEvent$Action MUSIC_ACTION_REWIND_STOP;
    final private int value;
    
    static {
        MUSIC_ACTION_PLAY = new com.navdy.service.library.events.audio.MusicEvent$Action("MUSIC_ACTION_PLAY", 0, 1);
        MUSIC_ACTION_PAUSE = new com.navdy.service.library.events.audio.MusicEvent$Action("MUSIC_ACTION_PAUSE", 1, 2);
        MUSIC_ACTION_NEXT = new com.navdy.service.library.events.audio.MusicEvent$Action("MUSIC_ACTION_NEXT", 2, 3);
        MUSIC_ACTION_PREVIOUS = new com.navdy.service.library.events.audio.MusicEvent$Action("MUSIC_ACTION_PREVIOUS", 3, 4);
        MUSIC_ACTION_REWIND_START = new com.navdy.service.library.events.audio.MusicEvent$Action("MUSIC_ACTION_REWIND_START", 4, 5);
        MUSIC_ACTION_FAST_FORWARD_START = new com.navdy.service.library.events.audio.MusicEvent$Action("MUSIC_ACTION_FAST_FORWARD_START", 5, 6);
        MUSIC_ACTION_REWIND_STOP = new com.navdy.service.library.events.audio.MusicEvent$Action("MUSIC_ACTION_REWIND_STOP", 6, 7);
        MUSIC_ACTION_FAST_FORWARD_STOP = new com.navdy.service.library.events.audio.MusicEvent$Action("MUSIC_ACTION_FAST_FORWARD_STOP", 7, 8);
        MUSIC_ACTION_MODE_CHANGE = new com.navdy.service.library.events.audio.MusicEvent$Action("MUSIC_ACTION_MODE_CHANGE", 8, 9);
        com.navdy.service.library.events.audio.MusicEvent$Action[] a = new com.navdy.service.library.events.audio.MusicEvent$Action[9];
        a[0] = MUSIC_ACTION_PLAY;
        a[1] = MUSIC_ACTION_PAUSE;
        a[2] = MUSIC_ACTION_NEXT;
        a[3] = MUSIC_ACTION_PREVIOUS;
        a[4] = MUSIC_ACTION_REWIND_START;
        a[5] = MUSIC_ACTION_FAST_FORWARD_START;
        a[6] = MUSIC_ACTION_REWIND_STOP;
        a[7] = MUSIC_ACTION_FAST_FORWARD_STOP;
        a[8] = MUSIC_ACTION_MODE_CHANGE;
        $VALUES = a;
    }
    
    private MusicEvent$Action(String s, int i, int i0) {
        super(s, i);
        this.value = i0;
    }
    
    public static com.navdy.service.library.events.audio.MusicEvent$Action valueOf(String s) {
        return (com.navdy.service.library.events.audio.MusicEvent$Action)Enum.valueOf(com.navdy.service.library.events.audio.MusicEvent$Action.class, s);
    }
    
    public static com.navdy.service.library.events.audio.MusicEvent$Action[] values() {
        return $VALUES.clone();
    }
    
    public int getValue() {
        return this.value;
    }
}
