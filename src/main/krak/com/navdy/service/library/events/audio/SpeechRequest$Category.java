package com.navdy.service.library.events.audio;

final public class SpeechRequest$Category extends Enum implements com.squareup.wire.ProtoEnum {
    final private static com.navdy.service.library.events.audio.SpeechRequest$Category[] $VALUES;
    final public static com.navdy.service.library.events.audio.SpeechRequest$Category SPEECH_CAMERA_WARNING;
    final public static com.navdy.service.library.events.audio.SpeechRequest$Category SPEECH_GPS_CONNECTIVITY;
    final public static com.navdy.service.library.events.audio.SpeechRequest$Category SPEECH_MESSAGE_READ_OUT;
    final public static com.navdy.service.library.events.audio.SpeechRequest$Category SPEECH_NOTIFICATION;
    final public static com.navdy.service.library.events.audio.SpeechRequest$Category SPEECH_REROUTE;
    final public static com.navdy.service.library.events.audio.SpeechRequest$Category SPEECH_SPEED_WARNING;
    final public static com.navdy.service.library.events.audio.SpeechRequest$Category SPEECH_TURN_BY_TURN;
    final public static com.navdy.service.library.events.audio.SpeechRequest$Category SPEECH_WELCOME_MESSAGE;
    final private int value;
    
    static {
        SPEECH_TURN_BY_TURN = new com.navdy.service.library.events.audio.SpeechRequest$Category("SPEECH_TURN_BY_TURN", 0, 1);
        SPEECH_REROUTE = new com.navdy.service.library.events.audio.SpeechRequest$Category("SPEECH_REROUTE", 1, 2);
        SPEECH_SPEED_WARNING = new com.navdy.service.library.events.audio.SpeechRequest$Category("SPEECH_SPEED_WARNING", 2, 3);
        SPEECH_GPS_CONNECTIVITY = new com.navdy.service.library.events.audio.SpeechRequest$Category("SPEECH_GPS_CONNECTIVITY", 3, 4);
        SPEECH_MESSAGE_READ_OUT = new com.navdy.service.library.events.audio.SpeechRequest$Category("SPEECH_MESSAGE_READ_OUT", 4, 5);
        SPEECH_WELCOME_MESSAGE = new com.navdy.service.library.events.audio.SpeechRequest$Category("SPEECH_WELCOME_MESSAGE", 5, 6);
        SPEECH_NOTIFICATION = new com.navdy.service.library.events.audio.SpeechRequest$Category("SPEECH_NOTIFICATION", 6, 7);
        SPEECH_CAMERA_WARNING = new com.navdy.service.library.events.audio.SpeechRequest$Category("SPEECH_CAMERA_WARNING", 7, 8);
        com.navdy.service.library.events.audio.SpeechRequest$Category[] a = new com.navdy.service.library.events.audio.SpeechRequest$Category[8];
        a[0] = SPEECH_TURN_BY_TURN;
        a[1] = SPEECH_REROUTE;
        a[2] = SPEECH_SPEED_WARNING;
        a[3] = SPEECH_GPS_CONNECTIVITY;
        a[4] = SPEECH_MESSAGE_READ_OUT;
        a[5] = SPEECH_WELCOME_MESSAGE;
        a[6] = SPEECH_NOTIFICATION;
        a[7] = SPEECH_CAMERA_WARNING;
        $VALUES = a;
    }
    
    private SpeechRequest$Category(String s, int i, int i0) {
        super(s, i);
        this.value = i0;
    }
    
    public static com.navdy.service.library.events.audio.SpeechRequest$Category valueOf(String s) {
        return (com.navdy.service.library.events.audio.SpeechRequest$Category)Enum.valueOf(com.navdy.service.library.events.audio.SpeechRequest$Category.class, s);
    }
    
    public static com.navdy.service.library.events.audio.SpeechRequest$Category[] values() {
        return $VALUES.clone();
    }
    
    public int getValue() {
        return this.value;
    }
}
