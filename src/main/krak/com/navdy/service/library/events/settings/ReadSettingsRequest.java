package com.navdy.service.library.events.settings;

final public class ReadSettingsRequest extends com.squareup.wire.Message {
    final public static java.util.List DEFAULT_SETTINGS;
    final private static long serialVersionUID = 0L;
    final public java.util.List settings;
    
    static {
        DEFAULT_SETTINGS = java.util.Collections.emptyList();
    }
    
    private ReadSettingsRequest(com.navdy.service.library.events.settings.ReadSettingsRequest$Builder a) {
        this(a.settings);
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    ReadSettingsRequest(com.navdy.service.library.events.settings.ReadSettingsRequest$Builder a, com.navdy.service.library.events.settings.ReadSettingsRequest$1 a0) {
        this(a);
    }
    
    public ReadSettingsRequest(java.util.List a) {
        this.settings = com.navdy.service.library.events.settings.ReadSettingsRequest.immutableCopyOf(a);
    }
    
    static java.util.List access$000(java.util.List a) {
        return com.navdy.service.library.events.settings.ReadSettingsRequest.copyOf(a);
    }
    
    public boolean equals(Object a) {
        return a == this || a instanceof com.navdy.service.library.events.settings.ReadSettingsRequest && this.equals(this.settings, ((com.navdy.service.library.events.settings.ReadSettingsRequest)a).settings);
    }
    
    public int hashCode() {
        int i = this.hashCode;
        if (i == 0) {
            i = (this.settings == null) ? 1 : this.settings.hashCode();
            this.hashCode = i;
        }
        return i;
    }
}
