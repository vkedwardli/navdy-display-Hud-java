package com.navdy.service.library.events.notification;

final public class ShowCustomNotification extends com.squareup.wire.Message {
    final public static String DEFAULT_ID = "";
    final private static long serialVersionUID = 0L;
    final public String id;
    
    private ShowCustomNotification(com.navdy.service.library.events.notification.ShowCustomNotification$Builder a) {
        this(a.id);
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    ShowCustomNotification(com.navdy.service.library.events.notification.ShowCustomNotification$Builder a, com.navdy.service.library.events.notification.ShowCustomNotification$1 a0) {
        this(a);
    }
    
    public ShowCustomNotification(String s) {
        this.id = s;
    }
    
    public boolean equals(Object a) {
        return a == this || a instanceof com.navdy.service.library.events.notification.ShowCustomNotification && this.equals(this.id, ((com.navdy.service.library.events.notification.ShowCustomNotification)a).id);
    }
    
    public int hashCode() {
        int i = this.hashCode;
        if (i == 0) {
            i = (this.id == null) ? 0 : this.id.hashCode();
            this.hashCode = i;
        }
        return i;
    }
}
