package com.navdy.service.library.events.debug;

final public class StartDrivePlaybackResponse extends com.squareup.wire.Message {
    final public static com.navdy.service.library.events.RequestStatus DEFAULT_STATUS;
    final private static long serialVersionUID = 0L;
    final public com.navdy.service.library.events.RequestStatus status;
    
    static {
        DEFAULT_STATUS = com.navdy.service.library.events.RequestStatus.REQUEST_SUCCESS;
    }
    
    public StartDrivePlaybackResponse(com.navdy.service.library.events.RequestStatus a) {
        this.status = a;
    }
    
    private StartDrivePlaybackResponse(com.navdy.service.library.events.debug.StartDrivePlaybackResponse$Builder a) {
        this(a.status);
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    StartDrivePlaybackResponse(com.navdy.service.library.events.debug.StartDrivePlaybackResponse$Builder a, com.navdy.service.library.events.debug.StartDrivePlaybackResponse$1 a0) {
        this(a);
    }
    
    public boolean equals(Object a) {
        return a == this || a instanceof com.navdy.service.library.events.debug.StartDrivePlaybackResponse && this.equals(this.status, ((com.navdy.service.library.events.debug.StartDrivePlaybackResponse)a).status);
    }
    
    public int hashCode() {
        int i = this.hashCode;
        if (i == 0) {
            i = (this.status == null) ? 0 : this.status.hashCode();
            this.hashCode = i;
        }
        return i;
    }
}
