package com.navdy.service.library.events.preferences;

final public class NavigationPreferences$RoutingType extends Enum implements com.squareup.wire.ProtoEnum {
    final private static com.navdy.service.library.events.preferences.NavigationPreferences$RoutingType[] $VALUES;
    final public static com.navdy.service.library.events.preferences.NavigationPreferences$RoutingType ROUTING_FASTEST;
    final public static com.navdy.service.library.events.preferences.NavigationPreferences$RoutingType ROUTING_SHORTEST;
    final private int value;
    
    static {
        ROUTING_FASTEST = new com.navdy.service.library.events.preferences.NavigationPreferences$RoutingType("ROUTING_FASTEST", 0, 0);
        ROUTING_SHORTEST = new com.navdy.service.library.events.preferences.NavigationPreferences$RoutingType("ROUTING_SHORTEST", 1, 1);
        com.navdy.service.library.events.preferences.NavigationPreferences$RoutingType[] a = new com.navdy.service.library.events.preferences.NavigationPreferences$RoutingType[2];
        a[0] = ROUTING_FASTEST;
        a[1] = ROUTING_SHORTEST;
        $VALUES = a;
    }
    
    private NavigationPreferences$RoutingType(String s, int i, int i0) {
        super(s, i);
        this.value = i0;
    }
    
    public static com.navdy.service.library.events.preferences.NavigationPreferences$RoutingType valueOf(String s) {
        return (com.navdy.service.library.events.preferences.NavigationPreferences$RoutingType)Enum.valueOf(com.navdy.service.library.events.preferences.NavigationPreferences$RoutingType.class, s);
    }
    
    public static com.navdy.service.library.events.preferences.NavigationPreferences$RoutingType[] values() {
        return $VALUES.clone();
    }
    
    public int getValue() {
        return this.value;
    }
}
