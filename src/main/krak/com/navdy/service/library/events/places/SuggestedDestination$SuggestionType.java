package com.navdy.service.library.events.places;

final public class SuggestedDestination$SuggestionType extends Enum implements com.squareup.wire.ProtoEnum {
    final private static com.navdy.service.library.events.places.SuggestedDestination$SuggestionType[] $VALUES;
    final public static com.navdy.service.library.events.places.SuggestedDestination$SuggestionType SUGGESTION_CALENDAR;
    final public static com.navdy.service.library.events.places.SuggestedDestination$SuggestionType SUGGESTION_RECOMMENDATION;
    final private int value;
    
    static {
        SUGGESTION_RECOMMENDATION = new com.navdy.service.library.events.places.SuggestedDestination$SuggestionType("SUGGESTION_RECOMMENDATION", 0, 0);
        SUGGESTION_CALENDAR = new com.navdy.service.library.events.places.SuggestedDestination$SuggestionType("SUGGESTION_CALENDAR", 1, 1);
        com.navdy.service.library.events.places.SuggestedDestination$SuggestionType[] a = new com.navdy.service.library.events.places.SuggestedDestination$SuggestionType[2];
        a[0] = SUGGESTION_RECOMMENDATION;
        a[1] = SUGGESTION_CALENDAR;
        $VALUES = a;
    }
    
    private SuggestedDestination$SuggestionType(String s, int i, int i0) {
        super(s, i);
        this.value = i0;
    }
    
    public static com.navdy.service.library.events.places.SuggestedDestination$SuggestionType valueOf(String s) {
        return (com.navdy.service.library.events.places.SuggestedDestination$SuggestionType)Enum.valueOf(com.navdy.service.library.events.places.SuggestedDestination$SuggestionType.class, s);
    }
    
    public static com.navdy.service.library.events.places.SuggestedDestination$SuggestionType[] values() {
        return $VALUES.clone();
    }
    
    public int getValue() {
        return this.value;
    }
}
