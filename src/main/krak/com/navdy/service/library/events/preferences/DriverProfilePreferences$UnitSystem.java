package com.navdy.service.library.events.preferences;

final public class DriverProfilePreferences$UnitSystem extends Enum implements com.squareup.wire.ProtoEnum {
    final private static com.navdy.service.library.events.preferences.DriverProfilePreferences$UnitSystem[] $VALUES;
    final public static com.navdy.service.library.events.preferences.DriverProfilePreferences$UnitSystem UNIT_SYSTEM_IMPERIAL;
    final public static com.navdy.service.library.events.preferences.DriverProfilePreferences$UnitSystem UNIT_SYSTEM_METRIC;
    final private int value;
    
    static {
        UNIT_SYSTEM_METRIC = new com.navdy.service.library.events.preferences.DriverProfilePreferences$UnitSystem("UNIT_SYSTEM_METRIC", 0, 0);
        UNIT_SYSTEM_IMPERIAL = new com.navdy.service.library.events.preferences.DriverProfilePreferences$UnitSystem("UNIT_SYSTEM_IMPERIAL", 1, 1);
        com.navdy.service.library.events.preferences.DriverProfilePreferences$UnitSystem[] a = new com.navdy.service.library.events.preferences.DriverProfilePreferences$UnitSystem[2];
        a[0] = UNIT_SYSTEM_METRIC;
        a[1] = UNIT_SYSTEM_IMPERIAL;
        $VALUES = a;
    }
    
    private DriverProfilePreferences$UnitSystem(String s, int i, int i0) {
        super(s, i);
        this.value = i0;
    }
    
    public static com.navdy.service.library.events.preferences.DriverProfilePreferences$UnitSystem valueOf(String s) {
        return (com.navdy.service.library.events.preferences.DriverProfilePreferences$UnitSystem)Enum.valueOf(com.navdy.service.library.events.preferences.DriverProfilePreferences$UnitSystem.class, s);
    }
    
    public static com.navdy.service.library.events.preferences.DriverProfilePreferences$UnitSystem[] values() {
        return $VALUES.clone();
    }
    
    public int getValue() {
        return this.value;
    }
}
