package com.navdy.service.library.events.debug;

final public class StartDrivePlaybackResponse$Builder extends com.squareup.wire.Message.Builder {
    public com.navdy.service.library.events.RequestStatus status;
    
    public StartDrivePlaybackResponse$Builder() {
    }
    
    public StartDrivePlaybackResponse$Builder(com.navdy.service.library.events.debug.StartDrivePlaybackResponse a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.status = a.status;
        }
    }
    
    public com.navdy.service.library.events.debug.StartDrivePlaybackResponse build() {
        this.checkRequiredFields();
        return new com.navdy.service.library.events.debug.StartDrivePlaybackResponse(this, (com.navdy.service.library.events.debug.StartDrivePlaybackResponse$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
    
    public com.navdy.service.library.events.debug.StartDrivePlaybackResponse$Builder status(com.navdy.service.library.events.RequestStatus a) {
        this.status = a;
        return this;
    }
}
