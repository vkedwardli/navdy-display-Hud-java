package com.navdy.service.library.events.preferences;

final public class DisplaySpeakerPreferences$Builder extends com.squareup.wire.Message.Builder {
    public Boolean feedback_sound;
    public Boolean master_sound;
    public Boolean notification_sound;
    public Long serial_number;
    public Integer volume_level;
    
    public DisplaySpeakerPreferences$Builder() {
    }
    
    public DisplaySpeakerPreferences$Builder(com.navdy.service.library.events.preferences.DisplaySpeakerPreferences a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.serial_number = a.serial_number;
            this.master_sound = a.master_sound;
            this.volume_level = a.volume_level;
            this.feedback_sound = a.feedback_sound;
            this.notification_sound = a.notification_sound;
        }
    }
    
    public com.navdy.service.library.events.preferences.DisplaySpeakerPreferences build() {
        this.checkRequiredFields();
        return new com.navdy.service.library.events.preferences.DisplaySpeakerPreferences(this, (com.navdy.service.library.events.preferences.DisplaySpeakerPreferences$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
    
    public com.navdy.service.library.events.preferences.DisplaySpeakerPreferences$Builder feedback_sound(Boolean a) {
        this.feedback_sound = a;
        return this;
    }
    
    public com.navdy.service.library.events.preferences.DisplaySpeakerPreferences$Builder master_sound(Boolean a) {
        this.master_sound = a;
        return this;
    }
    
    public com.navdy.service.library.events.preferences.DisplaySpeakerPreferences$Builder notification_sound(Boolean a) {
        this.notification_sound = a;
        return this;
    }
    
    public com.navdy.service.library.events.preferences.DisplaySpeakerPreferences$Builder serial_number(Long a) {
        this.serial_number = a;
        return this;
    }
    
    public com.navdy.service.library.events.preferences.DisplaySpeakerPreferences$Builder volume_level(Integer a) {
        this.volume_level = a;
        return this;
    }
}
