package com.navdy.service.library.events.hudcontrol;

final public class AccelerateShutdown$AccelerateReason extends Enum implements com.squareup.wire.ProtoEnum {
    final private static com.navdy.service.library.events.hudcontrol.AccelerateShutdown$AccelerateReason[] $VALUES;
    final public static com.navdy.service.library.events.hudcontrol.AccelerateShutdown$AccelerateReason ACCELERATE_REASON_HFP_DISCONNECT;
    final public static com.navdy.service.library.events.hudcontrol.AccelerateShutdown$AccelerateReason ACCELERATE_REASON_UNKNOWN;
    final public static com.navdy.service.library.events.hudcontrol.AccelerateShutdown$AccelerateReason ACCELERATE_REASON_WALKING;
    final private int value;
    
    static {
        ACCELERATE_REASON_UNKNOWN = new com.navdy.service.library.events.hudcontrol.AccelerateShutdown$AccelerateReason("ACCELERATE_REASON_UNKNOWN", 0, 1);
        ACCELERATE_REASON_HFP_DISCONNECT = new com.navdy.service.library.events.hudcontrol.AccelerateShutdown$AccelerateReason("ACCELERATE_REASON_HFP_DISCONNECT", 1, 2);
        ACCELERATE_REASON_WALKING = new com.navdy.service.library.events.hudcontrol.AccelerateShutdown$AccelerateReason("ACCELERATE_REASON_WALKING", 2, 3);
        com.navdy.service.library.events.hudcontrol.AccelerateShutdown$AccelerateReason[] a = new com.navdy.service.library.events.hudcontrol.AccelerateShutdown$AccelerateReason[3];
        a[0] = ACCELERATE_REASON_UNKNOWN;
        a[1] = ACCELERATE_REASON_HFP_DISCONNECT;
        a[2] = ACCELERATE_REASON_WALKING;
        $VALUES = a;
    }
    
    private AccelerateShutdown$AccelerateReason(String s, int i, int i0) {
        super(s, i);
        this.value = i0;
    }
    
    public static com.navdy.service.library.events.hudcontrol.AccelerateShutdown$AccelerateReason valueOf(String s) {
        return (com.navdy.service.library.events.hudcontrol.AccelerateShutdown$AccelerateReason)Enum.valueOf(com.navdy.service.library.events.hudcontrol.AccelerateShutdown$AccelerateReason.class, s);
    }
    
    public static com.navdy.service.library.events.hudcontrol.AccelerateShutdown$AccelerateReason[] values() {
        return $VALUES.clone();
    }
    
    public int getValue() {
        return this.value;
    }
}
