package com.navdy.service.library.events.audio;

final public class NowPlayingUpdateRequest$Builder extends com.squareup.wire.Message.Builder {
    public Boolean start;
    
    public NowPlayingUpdateRequest$Builder() {
    }
    
    public NowPlayingUpdateRequest$Builder(com.navdy.service.library.events.audio.NowPlayingUpdateRequest a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.start = a.start;
        }
    }
    
    public com.navdy.service.library.events.audio.NowPlayingUpdateRequest build() {
        this.checkRequiredFields();
        return new com.navdy.service.library.events.audio.NowPlayingUpdateRequest(this, (com.navdy.service.library.events.audio.NowPlayingUpdateRequest$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
    
    public com.navdy.service.library.events.audio.NowPlayingUpdateRequest$Builder start(Boolean a) {
        this.start = a;
        return this;
    }
}
