package com.navdy.service.library.events.preferences;

final public class NotificationPreferences extends com.squareup.wire.Message {
    final public static Boolean DEFAULT_ENABLED;
    final public static Boolean DEFAULT_READALOUD;
    final public static Long DEFAULT_SERIAL_NUMBER;
    final public static java.util.List DEFAULT_SETTINGS;
    final public static Boolean DEFAULT_SHOWCONTENT;
    final private static long serialVersionUID = 0L;
    final public Boolean enabled;
    final public Boolean readAloud;
    final public Long serial_number;
    final public java.util.List settings;
    final public Boolean showContent;
    
    static {
        DEFAULT_SERIAL_NUMBER = Long.valueOf(0L);
        DEFAULT_SETTINGS = java.util.Collections.emptyList();
        DEFAULT_ENABLED = Boolean.valueOf(false);
        DEFAULT_READALOUD = Boolean.valueOf(true);
        DEFAULT_SHOWCONTENT = Boolean.valueOf(true);
    }
    
    private NotificationPreferences(com.navdy.service.library.events.preferences.NotificationPreferences$Builder a) {
        this(a.serial_number, a.settings, a.enabled, a.readAloud, a.showContent);
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    NotificationPreferences(com.navdy.service.library.events.preferences.NotificationPreferences$Builder a, com.navdy.service.library.events.preferences.NotificationPreferences$1 a0) {
        this(a);
    }
    
    public NotificationPreferences(Long a, java.util.List a0, Boolean a1, Boolean a2, Boolean a3) {
        this.serial_number = a;
        this.settings = com.navdy.service.library.events.preferences.NotificationPreferences.immutableCopyOf(a0);
        this.enabled = a1;
        this.readAloud = a2;
        this.showContent = a3;
    }
    
    static java.util.List access$000(java.util.List a) {
        return com.navdy.service.library.events.preferences.NotificationPreferences.copyOf(a);
    }
    
    public boolean equals(Object a) {
        boolean b = false;
        label0: if (a != this) {
            if (a instanceof com.navdy.service.library.events.preferences.NotificationPreferences) {
                com.navdy.service.library.events.preferences.NotificationPreferences a0 = (com.navdy.service.library.events.preferences.NotificationPreferences)a;
                boolean b0 = this.equals(this.serial_number, a0.serial_number);
                label1: {
                    if (!b0) {
                        break label1;
                    }
                    if (!this.equals(this.settings, a0.settings)) {
                        break label1;
                    }
                    if (!this.equals(this.enabled, a0.enabled)) {
                        break label1;
                    }
                    if (!this.equals(this.readAloud, a0.readAloud)) {
                        break label1;
                    }
                    if (this.equals(this.showContent, a0.showContent)) {
                        b = true;
                        break label0;
                    }
                }
                b = false;
            } else {
                b = false;
            }
        } else {
            b = true;
        }
        return b;
    }
    
    public int hashCode() {
        int i = this.hashCode;
        if (i == 0) {
            i = (((((this.serial_number == null) ? 0 : this.serial_number.hashCode()) * 37 + ((this.settings == null) ? 1 : this.settings.hashCode())) * 37 + ((this.enabled == null) ? 0 : this.enabled.hashCode())) * 37 + ((this.readAloud == null) ? 0 : this.readAloud.hashCode())) * 37 + ((this.showContent == null) ? 0 : this.showContent.hashCode());
            this.hashCode = i;
        }
        return i;
    }
}
