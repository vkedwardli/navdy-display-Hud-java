package com.navdy.service.library.events.glances;

final public class MessageConstants extends Enum implements com.squareup.wire.ProtoEnum {
    final private static com.navdy.service.library.events.glances.MessageConstants[] $VALUES;
    final public static com.navdy.service.library.events.glances.MessageConstants MESSAGE_BODY;
    final public static com.navdy.service.library.events.glances.MessageConstants MESSAGE_DOMAIN;
    final public static com.navdy.service.library.events.glances.MessageConstants MESSAGE_FROM;
    final public static com.navdy.service.library.events.glances.MessageConstants MESSAGE_FROM_NUMBER;
    final public static com.navdy.service.library.events.glances.MessageConstants MESSAGE_IS_SMS;
    final private int value;
    
    static {
        MESSAGE_FROM = new com.navdy.service.library.events.glances.MessageConstants("MESSAGE_FROM", 0, 0);
        MESSAGE_FROM_NUMBER = new com.navdy.service.library.events.glances.MessageConstants("MESSAGE_FROM_NUMBER", 1, 1);
        MESSAGE_BODY = new com.navdy.service.library.events.glances.MessageConstants("MESSAGE_BODY", 2, 2);
        MESSAGE_DOMAIN = new com.navdy.service.library.events.glances.MessageConstants("MESSAGE_DOMAIN", 3, 3);
        MESSAGE_IS_SMS = new com.navdy.service.library.events.glances.MessageConstants("MESSAGE_IS_SMS", 4, 4);
        com.navdy.service.library.events.glances.MessageConstants[] a = new com.navdy.service.library.events.glances.MessageConstants[5];
        a[0] = MESSAGE_FROM;
        a[1] = MESSAGE_FROM_NUMBER;
        a[2] = MESSAGE_BODY;
        a[3] = MESSAGE_DOMAIN;
        a[4] = MESSAGE_IS_SMS;
        $VALUES = a;
    }
    
    private MessageConstants(String s, int i, int i0) {
        super(s, i);
        this.value = i0;
    }
    
    public static com.navdy.service.library.events.glances.MessageConstants valueOf(String s) {
        return (com.navdy.service.library.events.glances.MessageConstants)Enum.valueOf(com.navdy.service.library.events.glances.MessageConstants.class, s);
    }
    
    public static com.navdy.service.library.events.glances.MessageConstants[] values() {
        return $VALUES.clone();
    }
    
    public int getValue() {
        return this.value;
    }
}
