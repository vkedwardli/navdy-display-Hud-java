package com.navdy.service.library.events.photo;

final public class PhotoResponse$Builder extends com.squareup.wire.Message.Builder {
    public String identifier;
    public okio.ByteString photo;
    public String photoChecksum;
    public com.navdy.service.library.events.photo.PhotoType photoType;
    public com.navdy.service.library.events.RequestStatus status;
    public String statusDetail;
    
    public PhotoResponse$Builder() {
    }
    
    public PhotoResponse$Builder(com.navdy.service.library.events.photo.PhotoResponse a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.photo = a.photo;
            this.status = a.status;
            this.statusDetail = a.statusDetail;
            this.identifier = a.identifier;
            this.photoChecksum = a.photoChecksum;
            this.photoType = a.photoType;
        }
    }
    
    public com.navdy.service.library.events.photo.PhotoResponse build() {
        this.checkRequiredFields();
        return new com.navdy.service.library.events.photo.PhotoResponse(this, (com.navdy.service.library.events.photo.PhotoResponse$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
    
    public com.navdy.service.library.events.photo.PhotoResponse$Builder identifier(String s) {
        this.identifier = s;
        return this;
    }
    
    public com.navdy.service.library.events.photo.PhotoResponse$Builder photo(okio.ByteString a) {
        this.photo = a;
        return this;
    }
    
    public com.navdy.service.library.events.photo.PhotoResponse$Builder photoChecksum(String s) {
        this.photoChecksum = s;
        return this;
    }
    
    public com.navdy.service.library.events.photo.PhotoResponse$Builder photoType(com.navdy.service.library.events.photo.PhotoType a) {
        this.photoType = a;
        return this;
    }
    
    public com.navdy.service.library.events.photo.PhotoResponse$Builder status(com.navdy.service.library.events.RequestStatus a) {
        this.status = a;
        return this;
    }
    
    public com.navdy.service.library.events.photo.PhotoResponse$Builder statusDetail(String s) {
        this.statusDetail = s;
        return this;
    }
}
