package com.navdy.service.library.events.navigation;

final public class NavigationSessionRequest extends com.squareup.wire.Message {
    final public static String DEFAULT_LABEL = "";
    final public static com.navdy.service.library.events.navigation.NavigationSessionState DEFAULT_NEWSTATE;
    final public static Boolean DEFAULT_ORIGINDISPLAY;
    final public static String DEFAULT_ROUTEID = "";
    final public static Integer DEFAULT_SIMULATIONSPEED;
    final private static long serialVersionUID = 0L;
    final public String label;
    final public com.navdy.service.library.events.navigation.NavigationSessionState newState;
    final public Boolean originDisplay;
    final public String routeId;
    final public Integer simulationSpeed;
    
    static {
        DEFAULT_NEWSTATE = com.navdy.service.library.events.navigation.NavigationSessionState.NAV_SESSION_ENGINE_NOT_READY;
        DEFAULT_SIMULATIONSPEED = Integer.valueOf(0);
        DEFAULT_ORIGINDISPLAY = Boolean.valueOf(false);
    }
    
    private NavigationSessionRequest(com.navdy.service.library.events.navigation.NavigationSessionRequest$Builder a) {
        this(a.newState, a.label, a.routeId, a.simulationSpeed, a.originDisplay);
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    NavigationSessionRequest(com.navdy.service.library.events.navigation.NavigationSessionRequest$Builder a, com.navdy.service.library.events.navigation.NavigationSessionRequest$1 a0) {
        this(a);
    }
    
    public NavigationSessionRequest(com.navdy.service.library.events.navigation.NavigationSessionState a, String s, String s0, Integer a0, Boolean a1) {
        this.newState = a;
        this.label = s;
        this.routeId = s0;
        this.simulationSpeed = a0;
        this.originDisplay = a1;
    }
    
    public boolean equals(Object a) {
        boolean b = false;
        label0: if (a != this) {
            if (a instanceof com.navdy.service.library.events.navigation.NavigationSessionRequest) {
                com.navdy.service.library.events.navigation.NavigationSessionRequest a0 = (com.navdy.service.library.events.navigation.NavigationSessionRequest)a;
                boolean b0 = this.equals(this.newState, a0.newState);
                label1: {
                    if (!b0) {
                        break label1;
                    }
                    if (!this.equals(this.label, a0.label)) {
                        break label1;
                    }
                    if (!this.equals(this.routeId, a0.routeId)) {
                        break label1;
                    }
                    if (!this.equals(this.simulationSpeed, a0.simulationSpeed)) {
                        break label1;
                    }
                    if (this.equals(this.originDisplay, a0.originDisplay)) {
                        b = true;
                        break label0;
                    }
                }
                b = false;
            } else {
                b = false;
            }
        } else {
            b = true;
        }
        return b;
    }
    
    public int hashCode() {
        int i = this.hashCode;
        if (i == 0) {
            i = (((((this.newState == null) ? 0 : this.newState.hashCode()) * 37 + ((this.label == null) ? 0 : this.label.hashCode())) * 37 + ((this.routeId == null) ? 0 : this.routeId.hashCode())) * 37 + ((this.simulationSpeed == null) ? 0 : this.simulationSpeed.hashCode())) * 37 + ((this.originDisplay == null) ? 0 : this.originDisplay.hashCode());
            this.hashCode = i;
        }
        return i;
    }
}
