package com.navdy.service.library.events.audio;

final public class SpeechRequestStatus extends com.squareup.wire.Message {
    final public static String DEFAULT_ID = "";
    final public static com.navdy.service.library.events.audio.SpeechRequestStatus$SpeechRequestStatusType DEFAULT_STATUS;
    final private static long serialVersionUID = 0L;
    final public String id;
    final public com.navdy.service.library.events.audio.SpeechRequestStatus$SpeechRequestStatusType status;
    
    static {
        DEFAULT_STATUS = com.navdy.service.library.events.audio.SpeechRequestStatus$SpeechRequestStatusType.SPEECH_REQUEST_STARTING;
    }
    
    private SpeechRequestStatus(com.navdy.service.library.events.audio.SpeechRequestStatus$Builder a) {
        this(a.id, a.status);
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    SpeechRequestStatus(com.navdy.service.library.events.audio.SpeechRequestStatus$Builder a, com.navdy.service.library.events.audio.SpeechRequestStatus$1 a0) {
        this(a);
    }
    
    public SpeechRequestStatus(String s, com.navdy.service.library.events.audio.SpeechRequestStatus$SpeechRequestStatusType a) {
        this.id = s;
        this.status = a;
    }
    
    public boolean equals(Object a) {
        boolean b = false;
        label0: if (a != this) {
            if (a instanceof com.navdy.service.library.events.audio.SpeechRequestStatus) {
                com.navdy.service.library.events.audio.SpeechRequestStatus a0 = (com.navdy.service.library.events.audio.SpeechRequestStatus)a;
                boolean b0 = this.equals(this.id, a0.id);
                label1: {
                    if (!b0) {
                        break label1;
                    }
                    if (this.equals(this.status, a0.status)) {
                        b = true;
                        break label0;
                    }
                }
                b = false;
            } else {
                b = false;
            }
        } else {
            b = true;
        }
        return b;
    }
    
    public int hashCode() {
        int i = this.hashCode;
        if (i == 0) {
            i = ((this.id == null) ? 0 : this.id.hashCode()) * 37 + ((this.status == null) ? 0 : this.status.hashCode());
            this.hashCode = i;
        }
        return i;
    }
}
