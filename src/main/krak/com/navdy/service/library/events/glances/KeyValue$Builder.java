package com.navdy.service.library.events.glances;

final public class KeyValue$Builder extends com.squareup.wire.Message.Builder {
    public String key;
    public String value;
    
    public KeyValue$Builder() {
    }
    
    public KeyValue$Builder(com.navdy.service.library.events.glances.KeyValue a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.key = a.key;
            this.value = a.value;
        }
    }
    
    public com.navdy.service.library.events.glances.KeyValue build() {
        return new com.navdy.service.library.events.glances.KeyValue(this, (com.navdy.service.library.events.glances.KeyValue$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
    
    public com.navdy.service.library.events.glances.KeyValue$Builder key(String s) {
        this.key = s;
        return this;
    }
    
    public com.navdy.service.library.events.glances.KeyValue$Builder value(String s) {
        this.value = s;
        return this;
    }
}
