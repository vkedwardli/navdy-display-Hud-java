package com.navdy.service.library.events.notification;

final public class NotificationsState extends Enum implements com.squareup.wire.ProtoEnum {
    final private static com.navdy.service.library.events.notification.NotificationsState[] $VALUES;
    final public static com.navdy.service.library.events.notification.NotificationsState NOTIFICATIONS_CONNECTING;
    final public static com.navdy.service.library.events.notification.NotificationsState NOTIFICATIONS_ENABLED;
    final public static com.navdy.service.library.events.notification.NotificationsState NOTIFICATIONS_PAIRING_FAILED;
    final public static com.navdy.service.library.events.notification.NotificationsState NOTIFICATIONS_SERVICE_UNAVAILABLE;
    final public static com.navdy.service.library.events.notification.NotificationsState NOTIFICATIONS_STOPPED;
    final private int value;
    
    static {
        NOTIFICATIONS_ENABLED = new com.navdy.service.library.events.notification.NotificationsState("NOTIFICATIONS_ENABLED", 0, 1);
        NOTIFICATIONS_STOPPED = new com.navdy.service.library.events.notification.NotificationsState("NOTIFICATIONS_STOPPED", 1, 2);
        NOTIFICATIONS_CONNECTING = new com.navdy.service.library.events.notification.NotificationsState("NOTIFICATIONS_CONNECTING", 2, 3);
        NOTIFICATIONS_PAIRING_FAILED = new com.navdy.service.library.events.notification.NotificationsState("NOTIFICATIONS_PAIRING_FAILED", 3, 4);
        NOTIFICATIONS_SERVICE_UNAVAILABLE = new com.navdy.service.library.events.notification.NotificationsState("NOTIFICATIONS_SERVICE_UNAVAILABLE", 4, 5);
        com.navdy.service.library.events.notification.NotificationsState[] a = new com.navdy.service.library.events.notification.NotificationsState[5];
        a[0] = NOTIFICATIONS_ENABLED;
        a[1] = NOTIFICATIONS_STOPPED;
        a[2] = NOTIFICATIONS_CONNECTING;
        a[3] = NOTIFICATIONS_PAIRING_FAILED;
        a[4] = NOTIFICATIONS_SERVICE_UNAVAILABLE;
        $VALUES = a;
    }
    
    private NotificationsState(String s, int i, int i0) {
        super(s, i);
        this.value = i0;
    }
    
    public static com.navdy.service.library.events.notification.NotificationsState valueOf(String s) {
        return (com.navdy.service.library.events.notification.NotificationsState)Enum.valueOf(com.navdy.service.library.events.notification.NotificationsState.class, s);
    }
    
    public static com.navdy.service.library.events.notification.NotificationsState[] values() {
        return $VALUES.clone();
    }
    
    public int getValue() {
        return this.value;
    }
}
