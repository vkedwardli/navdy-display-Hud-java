package com.navdy.service.library.events.notification;

final public class NotificationEvent extends com.squareup.wire.Message {
    final public static java.util.List DEFAULT_ACTIONS;
    final public static String DEFAULT_APPID = "";
    final public static String DEFAULT_APPNAME = "";
    final public static Boolean DEFAULT_CANNOTREPLYBACK;
    final public static com.navdy.service.library.events.notification.NotificationCategory DEFAULT_CATEGORY;
    final public static String DEFAULT_ICONRESOURCENAME = "";
    final public static Integer DEFAULT_ID;
    final public static String DEFAULT_IMAGERESOURCENAME = "";
    final public static String DEFAULT_MESSAGE = "";
    final public static okio.ByteString DEFAULT_PHOTO;
    final public static String DEFAULT_SOURCEIDENTIFIER = "";
    final public static String DEFAULT_SUBTITLE = "";
    final public static String DEFAULT_TITLE = "";
    final private static long serialVersionUID = 0L;
    final public java.util.List actions;
    final public String appId;
    final public String appName;
    final public Boolean cannotReplyBack;
    final public com.navdy.service.library.events.notification.NotificationCategory category;
    final public String iconResourceName;
    final public Integer id;
    final public String imageResourceName;
    final public String message;
    final public okio.ByteString photo;
    final public String sourceIdentifier;
    final public String subtitle;
    final public String title;
    
    static {
        DEFAULT_ID = Integer.valueOf(0);
        DEFAULT_CATEGORY = com.navdy.service.library.events.notification.NotificationCategory.CATEGORY_OTHER;
        DEFAULT_ACTIONS = java.util.Collections.emptyList();
        DEFAULT_PHOTO = okio.ByteString.EMPTY;
        DEFAULT_CANNOTREPLYBACK = Boolean.valueOf(false);
    }
    
    private NotificationEvent(com.navdy.service.library.events.notification.NotificationEvent$Builder a) {
        this(a.id, a.category, a.title, a.subtitle, a.message, a.appId, a.actions, a.photo, a.iconResourceName, a.imageResourceName, a.sourceIdentifier, a.cannotReplyBack, a.appName);
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    NotificationEvent(com.navdy.service.library.events.notification.NotificationEvent$Builder a, com.navdy.service.library.events.notification.NotificationEvent$1 a0) {
        this(a);
    }
    
    public NotificationEvent(Integer a, com.navdy.service.library.events.notification.NotificationCategory a0, String s, String s0, String s1, String s2, java.util.List a1, okio.ByteString a2, String s3, String s4, String s5, Boolean a3, String s6) {
        this.id = a;
        this.category = a0;
        this.title = s;
        this.subtitle = s0;
        this.message = s1;
        this.appId = s2;
        this.actions = com.navdy.service.library.events.notification.NotificationEvent.immutableCopyOf(a1);
        this.photo = a2;
        this.iconResourceName = s3;
        this.imageResourceName = s4;
        this.sourceIdentifier = s5;
        this.cannotReplyBack = a3;
        this.appName = s6;
    }
    
    static java.util.List access$000(java.util.List a) {
        return com.navdy.service.library.events.notification.NotificationEvent.copyOf(a);
    }
    
    public boolean equals(Object a) {
        boolean b = false;
        label0: if (a != this) {
            if (a instanceof com.navdy.service.library.events.notification.NotificationEvent) {
                com.navdy.service.library.events.notification.NotificationEvent a0 = (com.navdy.service.library.events.notification.NotificationEvent)a;
                boolean b0 = this.equals(this.id, a0.id);
                label1: {
                    if (!b0) {
                        break label1;
                    }
                    if (!this.equals(this.category, a0.category)) {
                        break label1;
                    }
                    if (!this.equals(this.title, a0.title)) {
                        break label1;
                    }
                    if (!this.equals(this.subtitle, a0.subtitle)) {
                        break label1;
                    }
                    if (!this.equals(this.message, a0.message)) {
                        break label1;
                    }
                    if (!this.equals(this.appId, a0.appId)) {
                        break label1;
                    }
                    if (!this.equals(this.actions, a0.actions)) {
                        break label1;
                    }
                    if (!this.equals(this.photo, a0.photo)) {
                        break label1;
                    }
                    if (!this.equals(this.iconResourceName, a0.iconResourceName)) {
                        break label1;
                    }
                    if (!this.equals(this.imageResourceName, a0.imageResourceName)) {
                        break label1;
                    }
                    if (!this.equals(this.sourceIdentifier, a0.sourceIdentifier)) {
                        break label1;
                    }
                    if (!this.equals(this.cannotReplyBack, a0.cannotReplyBack)) {
                        break label1;
                    }
                    if (this.equals(this.appName, a0.appName)) {
                        b = true;
                        break label0;
                    }
                }
                b = false;
            } else {
                b = false;
            }
        } else {
            b = true;
        }
        return b;
    }
    
    public int hashCode() {
        int i = this.hashCode;
        if (i == 0) {
            i = (((((((((((((this.id == null) ? 0 : this.id.hashCode()) * 37 + ((this.category == null) ? 0 : this.category.hashCode())) * 37 + ((this.title == null) ? 0 : this.title.hashCode())) * 37 + ((this.subtitle == null) ? 0 : this.subtitle.hashCode())) * 37 + ((this.message == null) ? 0 : this.message.hashCode())) * 37 + ((this.appId == null) ? 0 : this.appId.hashCode())) * 37 + ((this.actions == null) ? 1 : this.actions.hashCode())) * 37 + ((this.photo == null) ? 0 : this.photo.hashCode())) * 37 + ((this.iconResourceName == null) ? 0 : this.iconResourceName.hashCode())) * 37 + ((this.imageResourceName == null) ? 0 : this.imageResourceName.hashCode())) * 37 + ((this.sourceIdentifier == null) ? 0 : this.sourceIdentifier.hashCode())) * 37 + ((this.cannotReplyBack == null) ? 0 : this.cannotReplyBack.hashCode())) * 37 + ((this.appName == null) ? 0 : this.appName.hashCode());
            this.hashCode = i;
        }
        return i;
    }
}
