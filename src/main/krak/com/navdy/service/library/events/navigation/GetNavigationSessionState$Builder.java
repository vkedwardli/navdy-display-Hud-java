package com.navdy.service.library.events.navigation;

final public class GetNavigationSessionState$Builder extends com.squareup.wire.Message.Builder {
    public GetNavigationSessionState$Builder() {
    }
    
    public GetNavigationSessionState$Builder(com.navdy.service.library.events.navigation.GetNavigationSessionState a) {
        super((com.squareup.wire.Message)a);
    }
    
    public com.navdy.service.library.events.navigation.GetNavigationSessionState build() {
        return new com.navdy.service.library.events.navigation.GetNavigationSessionState(this, (com.navdy.service.library.events.navigation.GetNavigationSessionState$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
}
