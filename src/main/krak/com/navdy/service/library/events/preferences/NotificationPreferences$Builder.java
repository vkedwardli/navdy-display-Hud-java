package com.navdy.service.library.events.preferences;

final public class NotificationPreferences$Builder extends com.squareup.wire.Message.Builder {
    public Boolean enabled;
    public Boolean readAloud;
    public Long serial_number;
    public java.util.List settings;
    public Boolean showContent;
    
    public NotificationPreferences$Builder() {
    }
    
    public NotificationPreferences$Builder(com.navdy.service.library.events.preferences.NotificationPreferences a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.serial_number = a.serial_number;
            this.settings = com.navdy.service.library.events.preferences.NotificationPreferences.access$000(a.settings);
            this.enabled = a.enabled;
            this.readAloud = a.readAloud;
            this.showContent = a.showContent;
        }
    }
    
    public com.navdy.service.library.events.preferences.NotificationPreferences build() {
        this.checkRequiredFields();
        return new com.navdy.service.library.events.preferences.NotificationPreferences(this, (com.navdy.service.library.events.preferences.NotificationPreferences$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
    
    public com.navdy.service.library.events.preferences.NotificationPreferences$Builder enabled(Boolean a) {
        this.enabled = a;
        return this;
    }
    
    public com.navdy.service.library.events.preferences.NotificationPreferences$Builder readAloud(Boolean a) {
        this.readAloud = a;
        return this;
    }
    
    public com.navdy.service.library.events.preferences.NotificationPreferences$Builder serial_number(Long a) {
        this.serial_number = a;
        return this;
    }
    
    public com.navdy.service.library.events.preferences.NotificationPreferences$Builder settings(java.util.List a) {
        this.settings = com.navdy.service.library.events.preferences.NotificationPreferences$Builder.checkForNulls(a);
        return this;
    }
    
    public com.navdy.service.library.events.preferences.NotificationPreferences$Builder showContent(Boolean a) {
        this.showContent = a;
        return this;
    }
}
