package com.navdy.service.library.events.audio;

final public class MusicCollectionSourceUpdate$Builder extends com.squareup.wire.Message.Builder {
    public com.navdy.service.library.events.audio.MusicCollectionSource collectionSource;
    public Long serial_number;
    
    public MusicCollectionSourceUpdate$Builder() {
    }
    
    public MusicCollectionSourceUpdate$Builder(com.navdy.service.library.events.audio.MusicCollectionSourceUpdate a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.collectionSource = a.collectionSource;
            this.serial_number = a.serial_number;
        }
    }
    
    public com.navdy.service.library.events.audio.MusicCollectionSourceUpdate build() {
        return new com.navdy.service.library.events.audio.MusicCollectionSourceUpdate(this, (com.navdy.service.library.events.audio.MusicCollectionSourceUpdate$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
    
    public com.navdy.service.library.events.audio.MusicCollectionSourceUpdate$Builder collectionSource(com.navdy.service.library.events.audio.MusicCollectionSource a) {
        this.collectionSource = a;
        return this;
    }
    
    public com.navdy.service.library.events.audio.MusicCollectionSourceUpdate$Builder serial_number(Long a) {
        this.serial_number = a;
        return this;
    }
}
