package com.navdy.service.library.events.calendars;

final public class CalendarEventUpdates extends com.squareup.wire.Message {
    final public static java.util.List DEFAULT_CALENDAR_EVENTS;
    final private static long serialVersionUID = 0L;
    final public java.util.List calendar_events;
    
    static {
        DEFAULT_CALENDAR_EVENTS = java.util.Collections.emptyList();
    }
    
    private CalendarEventUpdates(com.navdy.service.library.events.calendars.CalendarEventUpdates$Builder a) {
        this(a.calendar_events);
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    CalendarEventUpdates(com.navdy.service.library.events.calendars.CalendarEventUpdates$Builder a, com.navdy.service.library.events.calendars.CalendarEventUpdates$1 a0) {
        this(a);
    }
    
    public CalendarEventUpdates(java.util.List a) {
        this.calendar_events = com.navdy.service.library.events.calendars.CalendarEventUpdates.immutableCopyOf(a);
    }
    
    static java.util.List access$000(java.util.List a) {
        return com.navdy.service.library.events.calendars.CalendarEventUpdates.copyOf(a);
    }
    
    public boolean equals(Object a) {
        return a == this || a instanceof com.navdy.service.library.events.calendars.CalendarEventUpdates && this.equals(this.calendar_events, ((com.navdy.service.library.events.calendars.CalendarEventUpdates)a).calendar_events);
    }
    
    public int hashCode() {
        int i = this.hashCode;
        if (i == 0) {
            i = (this.calendar_events == null) ? 1 : this.calendar_events.hashCode();
            this.hashCode = i;
        }
        return i;
    }
}
