package com.navdy.service.library.events.audio;

final public class MusicCapability extends com.squareup.wire.Message {
    final public static com.navdy.service.library.events.audio.MusicCapability$MusicAuthorizationStatus DEFAULT_AUTHORIZATIONSTATUS;
    final public static com.navdy.service.library.events.audio.MusicCollectionSource DEFAULT_COLLECTIONSOURCE;
    final public static java.util.List DEFAULT_COLLECTIONTYPES;
    final public static Long DEFAULT_SERIAL_NUMBER;
    final public static java.util.List DEFAULT_SUPPORTEDREPEATMODES;
    final public static java.util.List DEFAULT_SUPPORTEDSHUFFLEMODES;
    final private static long serialVersionUID = 0L;
    final public com.navdy.service.library.events.audio.MusicCapability$MusicAuthorizationStatus authorizationStatus;
    final public com.navdy.service.library.events.audio.MusicCollectionSource collectionSource;
    final public java.util.List collectionTypes;
    final public Long serial_number;
    final public java.util.List supportedRepeatModes;
    final public java.util.List supportedShuffleModes;
    
    static {
        DEFAULT_COLLECTIONSOURCE = com.navdy.service.library.events.audio.MusicCollectionSource.COLLECTION_SOURCE_UNKNOWN;
        DEFAULT_COLLECTIONTYPES = java.util.Collections.emptyList();
        DEFAULT_AUTHORIZATIONSTATUS = com.navdy.service.library.events.audio.MusicCapability$MusicAuthorizationStatus.MUSIC_AUTHORIZATION_NOT_DETERMINED;
        DEFAULT_SUPPORTEDSHUFFLEMODES = java.util.Collections.emptyList();
        DEFAULT_SUPPORTEDREPEATMODES = java.util.Collections.emptyList();
        DEFAULT_SERIAL_NUMBER = Long.valueOf(0L);
    }
    
    private MusicCapability(com.navdy.service.library.events.audio.MusicCapability$Builder a) {
        this(a.collectionSource, a.collectionTypes, a.authorizationStatus, a.supportedShuffleModes, a.supportedRepeatModes, a.serial_number);
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    MusicCapability(com.navdy.service.library.events.audio.MusicCapability$Builder a, com.navdy.service.library.events.audio.MusicCapability$1 a0) {
        this(a);
    }
    
    public MusicCapability(com.navdy.service.library.events.audio.MusicCollectionSource a, java.util.List a0, com.navdy.service.library.events.audio.MusicCapability$MusicAuthorizationStatus a1, java.util.List a2, java.util.List a3, Long a4) {
        this.collectionSource = a;
        this.collectionTypes = com.navdy.service.library.events.audio.MusicCapability.immutableCopyOf(a0);
        this.authorizationStatus = a1;
        this.supportedShuffleModes = com.navdy.service.library.events.audio.MusicCapability.immutableCopyOf(a2);
        this.supportedRepeatModes = com.navdy.service.library.events.audio.MusicCapability.immutableCopyOf(a3);
        this.serial_number = a4;
    }
    
    static java.util.List access$000(java.util.List a) {
        return com.navdy.service.library.events.audio.MusicCapability.copyOf(a);
    }
    
    static java.util.List access$100(java.util.List a) {
        return com.navdy.service.library.events.audio.MusicCapability.copyOf(a);
    }
    
    static java.util.List access$200(java.util.List a) {
        return com.navdy.service.library.events.audio.MusicCapability.copyOf(a);
    }
    
    public boolean equals(Object a) {
        boolean b = false;
        label0: if (a != this) {
            if (a instanceof com.navdy.service.library.events.audio.MusicCapability) {
                com.navdy.service.library.events.audio.MusicCapability a0 = (com.navdy.service.library.events.audio.MusicCapability)a;
                boolean b0 = this.equals(this.collectionSource, a0.collectionSource);
                label1: {
                    if (!b0) {
                        break label1;
                    }
                    if (!this.equals(this.collectionTypes, a0.collectionTypes)) {
                        break label1;
                    }
                    if (!this.equals(this.authorizationStatus, a0.authorizationStatus)) {
                        break label1;
                    }
                    if (!this.equals(this.supportedShuffleModes, a0.supportedShuffleModes)) {
                        break label1;
                    }
                    if (!this.equals(this.supportedRepeatModes, a0.supportedRepeatModes)) {
                        break label1;
                    }
                    if (this.equals(this.serial_number, a0.serial_number)) {
                        b = true;
                        break label0;
                    }
                }
                b = false;
            } else {
                b = false;
            }
        } else {
            b = true;
        }
        return b;
    }
    
    public int hashCode() {
        int i = this.hashCode;
        if (i == 0) {
            i = ((((((this.collectionSource == null) ? 0 : this.collectionSource.hashCode()) * 37 + ((this.collectionTypes == null) ? 1 : this.collectionTypes.hashCode())) * 37 + ((this.authorizationStatus == null) ? 0 : this.authorizationStatus.hashCode())) * 37 + ((this.supportedShuffleModes == null) ? 1 : this.supportedShuffleModes.hashCode())) * 37 + ((this.supportedRepeatModes == null) ? 1 : this.supportedRepeatModes.hashCode())) * 37 + ((this.serial_number == null) ? 0 : this.serial_number.hashCode());
            this.hashCode = i;
        }
        return i;
    }
}
