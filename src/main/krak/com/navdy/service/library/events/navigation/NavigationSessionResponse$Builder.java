package com.navdy.service.library.events.navigation;

final public class NavigationSessionResponse$Builder extends com.squareup.wire.Message.Builder {
    public com.navdy.service.library.events.navigation.NavigationSessionState pendingSessionState;
    public String routeId;
    public com.navdy.service.library.events.RequestStatus status;
    public String statusDetail;
    
    public NavigationSessionResponse$Builder() {
    }
    
    public NavigationSessionResponse$Builder(com.navdy.service.library.events.navigation.NavigationSessionResponse a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.status = a.status;
            this.statusDetail = a.statusDetail;
            this.pendingSessionState = a.pendingSessionState;
            this.routeId = a.routeId;
        }
    }
    
    public com.navdy.service.library.events.navigation.NavigationSessionResponse build() {
        this.checkRequiredFields();
        return new com.navdy.service.library.events.navigation.NavigationSessionResponse(this, (com.navdy.service.library.events.navigation.NavigationSessionResponse$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
    
    public com.navdy.service.library.events.navigation.NavigationSessionResponse$Builder pendingSessionState(com.navdy.service.library.events.navigation.NavigationSessionState a) {
        this.pendingSessionState = a;
        return this;
    }
    
    public com.navdy.service.library.events.navigation.NavigationSessionResponse$Builder routeId(String s) {
        this.routeId = s;
        return this;
    }
    
    public com.navdy.service.library.events.navigation.NavigationSessionResponse$Builder status(com.navdy.service.library.events.RequestStatus a) {
        this.status = a;
        return this;
    }
    
    public com.navdy.service.library.events.navigation.NavigationSessionResponse$Builder statusDetail(String s) {
        this.statusDetail = s;
        return this;
    }
}
