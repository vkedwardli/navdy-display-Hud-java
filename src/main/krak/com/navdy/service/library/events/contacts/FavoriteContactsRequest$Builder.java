package com.navdy.service.library.events.contacts;

final public class FavoriteContactsRequest$Builder extends com.squareup.wire.Message.Builder {
    public Integer maxContacts;
    
    public FavoriteContactsRequest$Builder() {
    }
    
    public FavoriteContactsRequest$Builder(com.navdy.service.library.events.contacts.FavoriteContactsRequest a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.maxContacts = a.maxContacts;
        }
    }
    
    public com.navdy.service.library.events.contacts.FavoriteContactsRequest build() {
        return new com.navdy.service.library.events.contacts.FavoriteContactsRequest(this, (com.navdy.service.library.events.contacts.FavoriteContactsRequest$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
    
    public com.navdy.service.library.events.contacts.FavoriteContactsRequest$Builder maxContacts(Integer a) {
        this.maxContacts = a;
        return this;
    }
}
