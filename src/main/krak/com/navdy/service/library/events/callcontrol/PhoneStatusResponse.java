package com.navdy.service.library.events.callcontrol;

final public class PhoneStatusResponse extends com.squareup.wire.Message {
    final public static com.navdy.service.library.events.RequestStatus DEFAULT_STATUS;
    final private static long serialVersionUID = 0L;
    final public com.navdy.service.library.events.callcontrol.PhoneEvent callStatus;
    final public com.navdy.service.library.events.RequestStatus status;
    
    static {
        DEFAULT_STATUS = com.navdy.service.library.events.RequestStatus.REQUEST_SUCCESS;
    }
    
    public PhoneStatusResponse(com.navdy.service.library.events.RequestStatus a, com.navdy.service.library.events.callcontrol.PhoneEvent a0) {
        this.status = a;
        this.callStatus = a0;
    }
    
    private PhoneStatusResponse(com.navdy.service.library.events.callcontrol.PhoneStatusResponse$Builder a) {
        this(a.status, a.callStatus);
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    PhoneStatusResponse(com.navdy.service.library.events.callcontrol.PhoneStatusResponse$Builder a, com.navdy.service.library.events.callcontrol.PhoneStatusResponse$1 a0) {
        this(a);
    }
    
    public boolean equals(Object a) {
        boolean b = false;
        label0: if (a != this) {
            if (a instanceof com.navdy.service.library.events.callcontrol.PhoneStatusResponse) {
                com.navdy.service.library.events.callcontrol.PhoneStatusResponse a0 = (com.navdy.service.library.events.callcontrol.PhoneStatusResponse)a;
                boolean b0 = this.equals(this.status, a0.status);
                label1: {
                    if (!b0) {
                        break label1;
                    }
                    if (this.equals(this.callStatus, a0.callStatus)) {
                        b = true;
                        break label0;
                    }
                }
                b = false;
            } else {
                b = false;
            }
        } else {
            b = true;
        }
        return b;
    }
    
    public int hashCode() {
        int i = this.hashCode;
        if (i == 0) {
            i = ((this.status == null) ? 0 : this.status.hashCode()) * 37 + ((this.callStatus == null) ? 0 : this.callStatus.hashCode());
            this.hashCode = i;
        }
        return i;
    }
}
