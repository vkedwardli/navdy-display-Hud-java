package com.navdy.service.library.events.contacts;

final public class FavoriteContactsRequest extends com.squareup.wire.Message {
    final public static Integer DEFAULT_MAXCONTACTS;
    final private static long serialVersionUID = 0L;
    final public Integer maxContacts;
    
    static {
        DEFAULT_MAXCONTACTS = Integer.valueOf(0);
    }
    
    private FavoriteContactsRequest(com.navdy.service.library.events.contacts.FavoriteContactsRequest$Builder a) {
        this(a.maxContacts);
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    FavoriteContactsRequest(com.navdy.service.library.events.contacts.FavoriteContactsRequest$Builder a, com.navdy.service.library.events.contacts.FavoriteContactsRequest$1 a0) {
        this(a);
    }
    
    public FavoriteContactsRequest(Integer a) {
        this.maxContacts = a;
    }
    
    public boolean equals(Object a) {
        return a == this || a instanceof com.navdy.service.library.events.contacts.FavoriteContactsRequest && this.equals(this.maxContacts, ((com.navdy.service.library.events.contacts.FavoriteContactsRequest)a).maxContacts);
    }
    
    public int hashCode() {
        int i = this.hashCode;
        if (i == 0) {
            i = (this.maxContacts == null) ? 0 : this.maxContacts.hashCode();
            this.hashCode = i;
        }
        return i;
    }
}
