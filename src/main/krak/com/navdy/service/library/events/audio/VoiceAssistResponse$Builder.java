package com.navdy.service.library.events.audio;

final public class VoiceAssistResponse$Builder extends com.squareup.wire.Message.Builder {
    public com.navdy.service.library.events.audio.VoiceAssistResponse$VoiceAssistState state;
    
    public VoiceAssistResponse$Builder() {
    }
    
    public VoiceAssistResponse$Builder(com.navdy.service.library.events.audio.VoiceAssistResponse a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.state = a.state;
        }
    }
    
    public com.navdy.service.library.events.audio.VoiceAssistResponse build() {
        this.checkRequiredFields();
        return new com.navdy.service.library.events.audio.VoiceAssistResponse(this, (com.navdy.service.library.events.audio.VoiceAssistResponse$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
    
    public com.navdy.service.library.events.audio.VoiceAssistResponse$Builder state(com.navdy.service.library.events.audio.VoiceAssistResponse$VoiceAssistState a) {
        this.state = a;
        return this;
    }
}
