package com.navdy.service.library.events.audio;

final public class VoiceSearchResponse$Builder extends com.squareup.wire.Message.Builder {
    public Integer confidenceLevel;
    public com.navdy.service.library.events.audio.VoiceSearchResponse$VoiceSearchError error;
    public Boolean listeningOverBluetooth;
    public String locale;
    public String prefix;
    public String recognizedWords;
    public java.util.List results;
    public com.navdy.service.library.events.audio.VoiceSearchResponse$VoiceSearchState state;
    public Integer volumeLevel;
    
    public VoiceSearchResponse$Builder() {
    }
    
    public VoiceSearchResponse$Builder(com.navdy.service.library.events.audio.VoiceSearchResponse a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.state = a.state;
            this.recognizedWords = a.recognizedWords;
            this.volumeLevel = a.volumeLevel;
            this.listeningOverBluetooth = a.listeningOverBluetooth;
            this.error = a.error;
            this.confidenceLevel = a.confidenceLevel;
            this.results = com.navdy.service.library.events.audio.VoiceSearchResponse.access$000(a.results);
            this.prefix = a.prefix;
            this.locale = a.locale;
        }
    }
    
    public com.navdy.service.library.events.audio.VoiceSearchResponse build() {
        this.checkRequiredFields();
        return new com.navdy.service.library.events.audio.VoiceSearchResponse(this, (com.navdy.service.library.events.audio.VoiceSearchResponse$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
    
    public com.navdy.service.library.events.audio.VoiceSearchResponse$Builder confidenceLevel(Integer a) {
        this.confidenceLevel = a;
        return this;
    }
    
    public com.navdy.service.library.events.audio.VoiceSearchResponse$Builder error(com.navdy.service.library.events.audio.VoiceSearchResponse$VoiceSearchError a) {
        this.error = a;
        return this;
    }
    
    public com.navdy.service.library.events.audio.VoiceSearchResponse$Builder listeningOverBluetooth(Boolean a) {
        this.listeningOverBluetooth = a;
        return this;
    }
    
    public com.navdy.service.library.events.audio.VoiceSearchResponse$Builder locale(String s) {
        this.locale = s;
        return this;
    }
    
    public com.navdy.service.library.events.audio.VoiceSearchResponse$Builder prefix(String s) {
        this.prefix = s;
        return this;
    }
    
    public com.navdy.service.library.events.audio.VoiceSearchResponse$Builder recognizedWords(String s) {
        this.recognizedWords = s;
        return this;
    }
    
    public com.navdy.service.library.events.audio.VoiceSearchResponse$Builder results(java.util.List a) {
        this.results = com.navdy.service.library.events.audio.VoiceSearchResponse$Builder.checkForNulls(a);
        return this;
    }
    
    public com.navdy.service.library.events.audio.VoiceSearchResponse$Builder state(com.navdy.service.library.events.audio.VoiceSearchResponse$VoiceSearchState a) {
        this.state = a;
        return this;
    }
    
    public com.navdy.service.library.events.audio.VoiceSearchResponse$Builder volumeLevel(Integer a) {
        this.volumeLevel = a;
        return this;
    }
}
