package com.navdy.service.library.events.audio;

final public class MusicArtworkResponse$Builder extends com.squareup.wire.Message.Builder {
    public String album;
    public String author;
    public String collectionId;
    public com.navdy.service.library.events.audio.MusicCollectionSource collectionSource;
    public com.navdy.service.library.events.audio.MusicCollectionType collectionType;
    public String name;
    public okio.ByteString photo;
    
    public MusicArtworkResponse$Builder() {
    }
    
    public MusicArtworkResponse$Builder(com.navdy.service.library.events.audio.MusicArtworkResponse a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.collectionSource = a.collectionSource;
            this.name = a.name;
            this.album = a.album;
            this.author = a.author;
            this.photo = a.photo;
            this.collectionType = a.collectionType;
            this.collectionId = a.collectionId;
        }
    }
    
    public com.navdy.service.library.events.audio.MusicArtworkResponse$Builder album(String s) {
        this.album = s;
        return this;
    }
    
    public com.navdy.service.library.events.audio.MusicArtworkResponse$Builder author(String s) {
        this.author = s;
        return this;
    }
    
    public com.navdy.service.library.events.audio.MusicArtworkResponse build() {
        return new com.navdy.service.library.events.audio.MusicArtworkResponse(this, (com.navdy.service.library.events.audio.MusicArtworkResponse$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
    
    public com.navdy.service.library.events.audio.MusicArtworkResponse$Builder collectionId(String s) {
        this.collectionId = s;
        return this;
    }
    
    public com.navdy.service.library.events.audio.MusicArtworkResponse$Builder collectionSource(com.navdy.service.library.events.audio.MusicCollectionSource a) {
        this.collectionSource = a;
        return this;
    }
    
    public com.navdy.service.library.events.audio.MusicArtworkResponse$Builder collectionType(com.navdy.service.library.events.audio.MusicCollectionType a) {
        this.collectionType = a;
        return this;
    }
    
    public com.navdy.service.library.events.audio.MusicArtworkResponse$Builder name(String s) {
        this.name = s;
        return this;
    }
    
    public com.navdy.service.library.events.audio.MusicArtworkResponse$Builder photo(okio.ByteString a) {
        this.photo = a;
        return this;
    }
}
