package com.navdy.service.library.events.notification;

final public class NotificationListRequest extends com.squareup.wire.Message {
    final private static long serialVersionUID = 0L;
    
    public NotificationListRequest() {
    }
    
    private NotificationListRequest(com.navdy.service.library.events.notification.NotificationListRequest$Builder a) {
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    NotificationListRequest(com.navdy.service.library.events.notification.NotificationListRequest$Builder a, com.navdy.service.library.events.notification.NotificationListRequest$1 a0) {
        this(a);
    }
    
    public boolean equals(Object a) {
        return a instanceof com.navdy.service.library.events.notification.NotificationListRequest;
    }
    
    public int hashCode() {
        return 0;
    }
}
