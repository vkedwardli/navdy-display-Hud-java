package com.navdy.service.library.events.callcontrol;

final public class PhoneBatteryStatus$BatteryStatus extends Enum implements com.squareup.wire.ProtoEnum {
    final private static com.navdy.service.library.events.callcontrol.PhoneBatteryStatus$BatteryStatus[] $VALUES;
    final public static com.navdy.service.library.events.callcontrol.PhoneBatteryStatus$BatteryStatus BATTERY_EXTREMELY_LOW;
    final public static com.navdy.service.library.events.callcontrol.PhoneBatteryStatus$BatteryStatus BATTERY_LOW;
    final public static com.navdy.service.library.events.callcontrol.PhoneBatteryStatus$BatteryStatus BATTERY_OK;
    final private int value;
    
    static {
        BATTERY_OK = new com.navdy.service.library.events.callcontrol.PhoneBatteryStatus$BatteryStatus("BATTERY_OK", 0, 1);
        BATTERY_LOW = new com.navdy.service.library.events.callcontrol.PhoneBatteryStatus$BatteryStatus("BATTERY_LOW", 1, 2);
        BATTERY_EXTREMELY_LOW = new com.navdy.service.library.events.callcontrol.PhoneBatteryStatus$BatteryStatus("BATTERY_EXTREMELY_LOW", 2, 3);
        com.navdy.service.library.events.callcontrol.PhoneBatteryStatus$BatteryStatus[] a = new com.navdy.service.library.events.callcontrol.PhoneBatteryStatus$BatteryStatus[3];
        a[0] = BATTERY_OK;
        a[1] = BATTERY_LOW;
        a[2] = BATTERY_EXTREMELY_LOW;
        $VALUES = a;
    }
    
    private PhoneBatteryStatus$BatteryStatus(String s, int i, int i0) {
        super(s, i);
        this.value = i0;
    }
    
    public static com.navdy.service.library.events.callcontrol.PhoneBatteryStatus$BatteryStatus valueOf(String s) {
        return (com.navdy.service.library.events.callcontrol.PhoneBatteryStatus$BatteryStatus)Enum.valueOf(com.navdy.service.library.events.callcontrol.PhoneBatteryStatus$BatteryStatus.class, s);
    }
    
    public static com.navdy.service.library.events.callcontrol.PhoneBatteryStatus$BatteryStatus[] values() {
        return $VALUES.clone();
    }
    
    public int getValue() {
        return this.value;
    }
}
