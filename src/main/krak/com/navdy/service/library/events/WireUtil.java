package com.navdy.service.library.events;

public class WireUtil {
    final private static com.navdy.service.library.events.NavdyEvent$MessageType[] sMessageTypes;
    
    static {
        sMessageTypes = com.navdy.service.library.events.NavdyEvent$MessageType.values();
    }
    
    public WireUtil() {
    }
    
    public static com.navdy.service.library.events.NavdyEvent$MessageType getEventType(byte[] a) {
        com.navdy.service.library.events.NavdyEvent$MessageType a0 = null;
        int i = com.navdy.service.library.events.WireUtil.parseEventType(a);
        label2: {
            label0: {
                label1: {
                    if (i <= 0) {
                        break label1;
                    }
                    if (i <= sMessageTypes.length) {
                        break label0;
                    }
                }
                a0 = null;
                break label2;
            }
            a0 = sMessageTypes[i - 1];
        }
        return a0;
    }
    
    public static int getEventTypeIndex(byte[] a) {
        return com.navdy.service.library.events.WireUtil.parseEventType(a);
    }
    
    public static int parseEventType(byte[] a) {
        int i = a[0];
        int i0 = i & 7;
        if (com.squareup.wire.WireType.VARINT.value() != i0) {
            throw new RuntimeException(new StringBuilder().append("expecting varint:").append(i0).toString());
        }
        int i1 = i >> 3;
        if (i1 != 2) {
            throw new RuntimeException(new StringBuilder().append("unexpected tag:").append(i1).toString());
        }
        return com.navdy.service.library.events.WireUtil.readVarint32(a);
    }
    
    public static int readVarint32(byte[] a) {
        int i = a[1];
        if (i < 0) {
            int i0 = i & 127;
            int i1 = a[2];
            if (i1 < 0) {
                int i2 = i0 | (i1 & 127) << 7;
                int i3 = a[3];
                if (i3 < 0) {
                    int i4 = i2 | (i3 & 127) << 14;
                    int i5 = a[4];
                    if (i5 < 0) {
                        int i6 = a[5];
                        i = i4 | (i5 & 127) << 21 | i6 << 28;
                        if (i6 < 0) {
                            int i7 = 5;
                            int i8 = 0;
                            while(true) {
                                if (i8 >= 5) {
                                    throw new RuntimeException("marlformed varint");
                                }
                                i7 = i7 + 1;
                                int i9 = a[i7];
                                if (i9 >= 0) {
                                    break;
                                }
                                i8 = i8 + 1;
                            }
                        }
                    } else {
                        i = i4 | i5 << 21;
                    }
                } else {
                    i = i2 | i3 << 14;
                }
            } else {
                i = i0 | i1 << 7;
            }
        }
        return i;
    }
}
