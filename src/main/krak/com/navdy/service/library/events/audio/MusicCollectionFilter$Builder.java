package com.navdy.service.library.events.audio;

final public class MusicCollectionFilter$Builder extends com.squareup.wire.Message.Builder {
    public String field;
    public String value;
    
    public MusicCollectionFilter$Builder() {
    }
    
    public MusicCollectionFilter$Builder(com.navdy.service.library.events.audio.MusicCollectionFilter a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.field = a.field;
            this.value = a.value;
        }
    }
    
    public com.navdy.service.library.events.audio.MusicCollectionFilter build() {
        return new com.navdy.service.library.events.audio.MusicCollectionFilter(this, (com.navdy.service.library.events.audio.MusicCollectionFilter$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
    
    public com.navdy.service.library.events.audio.MusicCollectionFilter$Builder field(String s) {
        this.field = s;
        return this;
    }
    
    public com.navdy.service.library.events.audio.MusicCollectionFilter$Builder value(String s) {
        this.value = s;
        return this;
    }
}
