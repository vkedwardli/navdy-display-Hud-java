package com.navdy.service.library.events.settings;

final public class ReadSettingsRequest$Builder extends com.squareup.wire.Message.Builder {
    public java.util.List settings;
    
    public ReadSettingsRequest$Builder() {
    }
    
    public ReadSettingsRequest$Builder(com.navdy.service.library.events.settings.ReadSettingsRequest a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.settings = com.navdy.service.library.events.settings.ReadSettingsRequest.access$000(a.settings);
        }
    }
    
    public com.navdy.service.library.events.settings.ReadSettingsRequest build() {
        return new com.navdy.service.library.events.settings.ReadSettingsRequest(this, (com.navdy.service.library.events.settings.ReadSettingsRequest$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
    
    public com.navdy.service.library.events.settings.ReadSettingsRequest$Builder settings(java.util.List a) {
        this.settings = com.navdy.service.library.events.settings.ReadSettingsRequest$Builder.checkForNulls(a);
        return this;
    }
}
