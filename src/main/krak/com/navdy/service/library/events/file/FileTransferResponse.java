package com.navdy.service.library.events.file;

final public class FileTransferResponse extends com.squareup.wire.Message {
    final public static String DEFAULT_CHECKSUM = "";
    final public static String DEFAULT_DESTINATIONFILENAME = "";
    final public static com.navdy.service.library.events.file.FileTransferError DEFAULT_ERROR;
    final public static com.navdy.service.library.events.file.FileType DEFAULT_FILETYPE;
    final public static Integer DEFAULT_MAXCHUNKSIZE;
    final public static Long DEFAULT_OFFSET;
    final public static Boolean DEFAULT_SUCCESS;
    final public static Boolean DEFAULT_SUPPORTSACKS;
    final public static Integer DEFAULT_TRANSFERID;
    final private static long serialVersionUID = 0L;
    final public String checksum;
    final public String destinationFileName;
    final public com.navdy.service.library.events.file.FileTransferError error;
    final public com.navdy.service.library.events.file.FileType fileType;
    final public Integer maxChunkSize;
    final public Long offset;
    final public Boolean success;
    final public Boolean supportsAcks;
    final public Integer transferId;
    
    static {
        DEFAULT_TRANSFERID = Integer.valueOf(0);
        DEFAULT_OFFSET = Long.valueOf(0L);
        DEFAULT_SUCCESS = Boolean.valueOf(false);
        DEFAULT_MAXCHUNKSIZE = Integer.valueOf(0);
        DEFAULT_ERROR = com.navdy.service.library.events.file.FileTransferError.FILE_TRANSFER_NO_ERROR;
        DEFAULT_FILETYPE = com.navdy.service.library.events.file.FileType.FILE_TYPE_OTA;
        DEFAULT_SUPPORTSACKS = Boolean.valueOf(false);
    }
    
    private FileTransferResponse(com.navdy.service.library.events.file.FileTransferResponse$Builder a) {
        this(a.transferId, a.offset, a.checksum, a.success, a.maxChunkSize, a.error, a.fileType, a.destinationFileName, a.supportsAcks);
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    FileTransferResponse(com.navdy.service.library.events.file.FileTransferResponse$Builder a, com.navdy.service.library.events.file.FileTransferResponse$1 a0) {
        this(a);
    }
    
    public FileTransferResponse(Integer a, Long a0, String s, Boolean a1, Integer a2, com.navdy.service.library.events.file.FileTransferError a3, com.navdy.service.library.events.file.FileType a4, String s0, Boolean a5) {
        this.transferId = a;
        this.offset = a0;
        this.checksum = s;
        this.success = a1;
        this.maxChunkSize = a2;
        this.error = a3;
        this.fileType = a4;
        this.destinationFileName = s0;
        this.supportsAcks = a5;
    }
    
    public boolean equals(Object a) {
        boolean b = false;
        label0: if (a != this) {
            if (a instanceof com.navdy.service.library.events.file.FileTransferResponse) {
                com.navdy.service.library.events.file.FileTransferResponse a0 = (com.navdy.service.library.events.file.FileTransferResponse)a;
                boolean b0 = this.equals(this.transferId, a0.transferId);
                label1: {
                    if (!b0) {
                        break label1;
                    }
                    if (!this.equals(this.offset, a0.offset)) {
                        break label1;
                    }
                    if (!this.equals(this.checksum, a0.checksum)) {
                        break label1;
                    }
                    if (!this.equals(this.success, a0.success)) {
                        break label1;
                    }
                    if (!this.equals(this.maxChunkSize, a0.maxChunkSize)) {
                        break label1;
                    }
                    if (!this.equals(this.error, a0.error)) {
                        break label1;
                    }
                    if (!this.equals(this.fileType, a0.fileType)) {
                        break label1;
                    }
                    if (!this.equals(this.destinationFileName, a0.destinationFileName)) {
                        break label1;
                    }
                    if (this.equals(this.supportsAcks, a0.supportsAcks)) {
                        b = true;
                        break label0;
                    }
                }
                b = false;
            } else {
                b = false;
            }
        } else {
            b = true;
        }
        return b;
    }
    
    public int hashCode() {
        int i = this.hashCode;
        if (i == 0) {
            i = (((((((((this.transferId == null) ? 0 : this.transferId.hashCode()) * 37 + ((this.offset == null) ? 0 : this.offset.hashCode())) * 37 + ((this.checksum == null) ? 0 : this.checksum.hashCode())) * 37 + ((this.success == null) ? 0 : this.success.hashCode())) * 37 + ((this.maxChunkSize == null) ? 0 : this.maxChunkSize.hashCode())) * 37 + ((this.error == null) ? 0 : this.error.hashCode())) * 37 + ((this.fileType == null) ? 0 : this.fileType.hashCode())) * 37 + ((this.destinationFileName == null) ? 0 : this.destinationFileName.hashCode())) * 37 + ((this.supportsAcks == null) ? 0 : this.supportsAcks.hashCode());
            this.hashCode = i;
        }
        return i;
    }
}
