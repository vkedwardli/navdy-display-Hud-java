package com.navdy.service.library.events.location;

final public class TransmitLocation$Builder extends com.squareup.wire.Message.Builder {
    public Boolean sendLocation;
    
    public TransmitLocation$Builder() {
    }
    
    public TransmitLocation$Builder(com.navdy.service.library.events.location.TransmitLocation a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.sendLocation = a.sendLocation;
        }
    }
    
    public com.navdy.service.library.events.location.TransmitLocation build() {
        return new com.navdy.service.library.events.location.TransmitLocation(this, (com.navdy.service.library.events.location.TransmitLocation$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
    
    public com.navdy.service.library.events.location.TransmitLocation$Builder sendLocation(Boolean a) {
        this.sendLocation = a;
        return this;
    }
}
