package com.navdy.service.library.events.preferences;

final public class NavigationPreferences extends com.squareup.wire.Message {
    final public static Boolean DEFAULT_ALLOWAUTOTRAINS;
    final public static Boolean DEFAULT_ALLOWFERRIES;
    final public static Boolean DEFAULT_ALLOWHIGHWAYS;
    final public static Boolean DEFAULT_ALLOWHOVLANES;
    final public static Boolean DEFAULT_ALLOWTOLLROADS;
    final public static Boolean DEFAULT_ALLOWTUNNELS;
    final public static Boolean DEFAULT_ALLOWUNPAVEDROADS;
    final public static Boolean DEFAULT_PHONETICTURNBYTURN;
    final public static com.navdy.service.library.events.preferences.NavigationPreferences$RerouteForTraffic DEFAULT_REROUTEFORTRAFFIC;
    final public static com.navdy.service.library.events.preferences.NavigationPreferences$RoutingType DEFAULT_ROUTINGTYPE;
    final public static Long DEFAULT_SERIAL_NUMBER;
    final public static Boolean DEFAULT_SHOWTRAFFICINOPENMAP;
    final public static Boolean DEFAULT_SHOWTRAFFICWHILENAVIGATING;
    final public static Boolean DEFAULT_SPOKENCAMERAWARNINGS;
    final public static Boolean DEFAULT_SPOKENSPEEDLIMITWARNINGS;
    final public static Boolean DEFAULT_SPOKENTURNBYTURN;
    final private static long serialVersionUID = 0L;
    final public Boolean allowAutoTrains;
    final public Boolean allowFerries;
    final public Boolean allowHOVLanes;
    final public Boolean allowHighways;
    final public Boolean allowTollRoads;
    final public Boolean allowTunnels;
    final public Boolean allowUnpavedRoads;
    final public Boolean phoneticTurnByTurn;
    final public com.navdy.service.library.events.preferences.NavigationPreferences$RerouteForTraffic rerouteForTraffic;
    final public com.navdy.service.library.events.preferences.NavigationPreferences$RoutingType routingType;
    final public Long serial_number;
    final public Boolean showTrafficInOpenMap;
    final public Boolean showTrafficWhileNavigating;
    final public Boolean spokenCameraWarnings;
    final public Boolean spokenSpeedLimitWarnings;
    final public Boolean spokenTurnByTurn;
    
    static {
        DEFAULT_SERIAL_NUMBER = Long.valueOf(0L);
        DEFAULT_REROUTEFORTRAFFIC = com.navdy.service.library.events.preferences.NavigationPreferences$RerouteForTraffic.REROUTE_CONFIRM;
        DEFAULT_ROUTINGTYPE = com.navdy.service.library.events.preferences.NavigationPreferences$RoutingType.ROUTING_FASTEST;
        DEFAULT_SPOKENTURNBYTURN = Boolean.valueOf(true);
        DEFAULT_SPOKENSPEEDLIMITWARNINGS = Boolean.valueOf(true);
        DEFAULT_SHOWTRAFFICINOPENMAP = Boolean.valueOf(true);
        DEFAULT_SHOWTRAFFICWHILENAVIGATING = Boolean.valueOf(true);
        DEFAULT_ALLOWHIGHWAYS = Boolean.valueOf(true);
        DEFAULT_ALLOWTOLLROADS = Boolean.valueOf(true);
        DEFAULT_ALLOWFERRIES = Boolean.valueOf(true);
        DEFAULT_ALLOWTUNNELS = Boolean.valueOf(true);
        DEFAULT_ALLOWUNPAVEDROADS = Boolean.valueOf(true);
        DEFAULT_ALLOWAUTOTRAINS = Boolean.valueOf(true);
        DEFAULT_ALLOWHOVLANES = Boolean.valueOf(true);
        DEFAULT_SPOKENCAMERAWARNINGS = Boolean.valueOf(true);
        DEFAULT_PHONETICTURNBYTURN = Boolean.valueOf(false);
    }
    
    private NavigationPreferences(com.navdy.service.library.events.preferences.NavigationPreferences$Builder a) {
        this(a.serial_number, a.rerouteForTraffic, a.routingType, a.spokenTurnByTurn, a.spokenSpeedLimitWarnings, a.showTrafficInOpenMap, a.showTrafficWhileNavigating, a.allowHighways, a.allowTollRoads, a.allowFerries, a.allowTunnels, a.allowUnpavedRoads, a.allowAutoTrains, a.allowHOVLanes, a.spokenCameraWarnings, a.phoneticTurnByTurn);
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    NavigationPreferences(com.navdy.service.library.events.preferences.NavigationPreferences$Builder a, com.navdy.service.library.events.preferences.NavigationPreferences$1 a0) {
        this(a);
    }
    
    public NavigationPreferences(Long a, com.navdy.service.library.events.preferences.NavigationPreferences$RerouteForTraffic a0, com.navdy.service.library.events.preferences.NavigationPreferences$RoutingType a1, Boolean a2, Boolean a3, Boolean a4, Boolean a5, Boolean a6, Boolean a7, Boolean a8, Boolean a9, Boolean a10, Boolean a11, Boolean a12, Boolean a13, Boolean a14) {
        this.serial_number = a;
        this.rerouteForTraffic = a0;
        this.routingType = a1;
        this.spokenTurnByTurn = a2;
        this.spokenSpeedLimitWarnings = a3;
        this.showTrafficInOpenMap = a4;
        this.showTrafficWhileNavigating = a5;
        this.allowHighways = a6;
        this.allowTollRoads = a7;
        this.allowFerries = a8;
        this.allowTunnels = a9;
        this.allowUnpavedRoads = a10;
        this.allowAutoTrains = a11;
        this.allowHOVLanes = a12;
        this.spokenCameraWarnings = a13;
        this.phoneticTurnByTurn = a14;
    }
    
    public boolean equals(Object a) {
        boolean b = false;
        label0: if (a != this) {
            if (a instanceof com.navdy.service.library.events.preferences.NavigationPreferences) {
                com.navdy.service.library.events.preferences.NavigationPreferences a0 = (com.navdy.service.library.events.preferences.NavigationPreferences)a;
                boolean b0 = this.equals(this.serial_number, a0.serial_number);
                label1: {
                    if (!b0) {
                        break label1;
                    }
                    if (!this.equals(this.rerouteForTraffic, a0.rerouteForTraffic)) {
                        break label1;
                    }
                    if (!this.equals(this.routingType, a0.routingType)) {
                        break label1;
                    }
                    if (!this.equals(this.spokenTurnByTurn, a0.spokenTurnByTurn)) {
                        break label1;
                    }
                    if (!this.equals(this.spokenSpeedLimitWarnings, a0.spokenSpeedLimitWarnings)) {
                        break label1;
                    }
                    if (!this.equals(this.showTrafficInOpenMap, a0.showTrafficInOpenMap)) {
                        break label1;
                    }
                    if (!this.equals(this.showTrafficWhileNavigating, a0.showTrafficWhileNavigating)) {
                        break label1;
                    }
                    if (!this.equals(this.allowHighways, a0.allowHighways)) {
                        break label1;
                    }
                    if (!this.equals(this.allowTollRoads, a0.allowTollRoads)) {
                        break label1;
                    }
                    if (!this.equals(this.allowFerries, a0.allowFerries)) {
                        break label1;
                    }
                    if (!this.equals(this.allowTunnels, a0.allowTunnels)) {
                        break label1;
                    }
                    if (!this.equals(this.allowUnpavedRoads, a0.allowUnpavedRoads)) {
                        break label1;
                    }
                    if (!this.equals(this.allowAutoTrains, a0.allowAutoTrains)) {
                        break label1;
                    }
                    if (!this.equals(this.allowHOVLanes, a0.allowHOVLanes)) {
                        break label1;
                    }
                    if (!this.equals(this.spokenCameraWarnings, a0.spokenCameraWarnings)) {
                        break label1;
                    }
                    if (this.equals(this.phoneticTurnByTurn, a0.phoneticTurnByTurn)) {
                        b = true;
                        break label0;
                    }
                }
                b = false;
            } else {
                b = false;
            }
        } else {
            b = true;
        }
        return b;
    }
    
    public int hashCode() {
        int i = this.hashCode;
        if (i == 0) {
            int i0 = (this.serial_number == null) ? 0 : this.serial_number.hashCode();
            int i1 = (this.rerouteForTraffic == null) ? 0 : this.rerouteForTraffic.hashCode();
            int i2 = (this.routingType == null) ? 0 : this.routingType.hashCode();
            int i3 = (this.spokenTurnByTurn == null) ? 0 : this.spokenTurnByTurn.hashCode();
            int i4 = (this.spokenSpeedLimitWarnings == null) ? 0 : this.spokenSpeedLimitWarnings.hashCode();
            int i5 = (this.showTrafficInOpenMap == null) ? 0 : this.showTrafficInOpenMap.hashCode();
            int i6 = (this.showTrafficWhileNavigating == null) ? 0 : this.showTrafficWhileNavigating.hashCode();
            int i7 = (this.allowHighways == null) ? 0 : this.allowHighways.hashCode();
            int i8 = (this.allowTollRoads == null) ? 0 : this.allowTollRoads.hashCode();
            int i9 = (this.allowFerries == null) ? 0 : this.allowFerries.hashCode();
            int i10 = (this.allowTunnels == null) ? 0 : this.allowTunnels.hashCode();
            int i11 = (this.allowUnpavedRoads == null) ? 0 : this.allowUnpavedRoads.hashCode();
            int i12 = (this.allowAutoTrains == null) ? 0 : this.allowAutoTrains.hashCode();
            int i13 = (this.allowHOVLanes == null) ? 0 : this.allowHOVLanes.hashCode();
            int i14 = (this.spokenCameraWarnings == null) ? 0 : this.spokenCameraWarnings.hashCode();
            int i15 = (this.phoneticTurnByTurn == null) ? 0 : this.phoneticTurnByTurn.hashCode();
            i = ((((((((((((((i0 * 37 + i1) * 37 + i2) * 37 + i3) * 37 + i4) * 37 + i5) * 37 + i6) * 37 + i7) * 37 + i8) * 37 + i9) * 37 + i10) * 37 + i11) * 37 + i12) * 37 + i13) * 37 + i14) * 37 + i15;
            this.hashCode = i;
        }
        return i;
    }
}
