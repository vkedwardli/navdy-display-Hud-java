package com.navdy.service.library.events.settings;

final public class DateTimeConfiguration$Clock extends Enum implements com.squareup.wire.ProtoEnum {
    final private static com.navdy.service.library.events.settings.DateTimeConfiguration$Clock[] $VALUES;
    final public static com.navdy.service.library.events.settings.DateTimeConfiguration$Clock CLOCK_12_HOUR;
    final public static com.navdy.service.library.events.settings.DateTimeConfiguration$Clock CLOCK_24_HOUR;
    final private int value;
    
    static {
        CLOCK_24_HOUR = new com.navdy.service.library.events.settings.DateTimeConfiguration$Clock("CLOCK_24_HOUR", 0, 1);
        CLOCK_12_HOUR = new com.navdy.service.library.events.settings.DateTimeConfiguration$Clock("CLOCK_12_HOUR", 1, 2);
        com.navdy.service.library.events.settings.DateTimeConfiguration$Clock[] a = new com.navdy.service.library.events.settings.DateTimeConfiguration$Clock[2];
        a[0] = CLOCK_24_HOUR;
        a[1] = CLOCK_12_HOUR;
        $VALUES = a;
    }
    
    private DateTimeConfiguration$Clock(String s, int i, int i0) {
        super(s, i);
        this.value = i0;
    }
    
    public static com.navdy.service.library.events.settings.DateTimeConfiguration$Clock valueOf(String s) {
        return (com.navdy.service.library.events.settings.DateTimeConfiguration$Clock)Enum.valueOf(com.navdy.service.library.events.settings.DateTimeConfiguration$Clock.class, s);
    }
    
    public static com.navdy.service.library.events.settings.DateTimeConfiguration$Clock[] values() {
        return $VALUES.clone();
    }
    
    public int getValue() {
        return this.value;
    }
}
