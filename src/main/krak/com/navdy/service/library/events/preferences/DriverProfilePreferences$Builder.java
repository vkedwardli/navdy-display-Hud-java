package com.navdy.service.library.events.preferences;

final public class DriverProfilePreferences$Builder extends com.squareup.wire.Message.Builder {
    public java.util.List additionalLocales;
    public Boolean auto_on_enabled;
    public String car_make;
    public String car_model;
    public String car_year;
    public String device_name;
    public com.navdy.service.library.events.preferences.DriverProfilePreferences$DialLongPressAction dial_long_press_action;
    public com.navdy.service.library.events.preferences.DriverProfilePreferences$DisplayFormat display_format;
    public String driver_email;
    public String driver_name;
    public com.navdy.service.library.events.preferences.DriverProfilePreferences$FeatureMode feature_mode;
    public Boolean limit_bandwidth;
    public String locale;
    public Long obdBlacklistLastModified;
    public com.navdy.service.library.events.preferences.DriverProfilePreferences$ObdScanSetting obdScanSetting;
    public String photo_checksum;
    public Boolean profile_is_public;
    public Long serial_number;
    public com.navdy.service.library.events.preferences.DriverProfilePreferences$UnitSystem unit_system;
    
    public DriverProfilePreferences$Builder() {
    }
    
    public DriverProfilePreferences$Builder(com.navdy.service.library.events.preferences.DriverProfilePreferences a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.serial_number = a.serial_number;
            this.driver_name = a.driver_name;
            this.device_name = a.device_name;
            this.profile_is_public = a.profile_is_public;
            this.photo_checksum = a.photo_checksum;
            this.driver_email = a.driver_email;
            this.car_make = a.car_make;
            this.car_model = a.car_model;
            this.car_year = a.car_year;
            this.auto_on_enabled = a.auto_on_enabled;
            this.display_format = a.display_format;
            this.locale = a.locale;
            this.unit_system = a.unit_system;
            this.feature_mode = a.feature_mode;
            this.obdScanSetting = a.obdScanSetting;
            this.limit_bandwidth = a.limit_bandwidth;
            this.obdBlacklistLastModified = a.obdBlacklistLastModified;
            this.dial_long_press_action = a.dial_long_press_action;
            this.additionalLocales = com.navdy.service.library.events.preferences.DriverProfilePreferences.access$000(a.additionalLocales);
        }
    }
    
    public com.navdy.service.library.events.preferences.DriverProfilePreferences$Builder additionalLocales(java.util.List a) {
        this.additionalLocales = com.navdy.service.library.events.preferences.DriverProfilePreferences$Builder.checkForNulls(a);
        return this;
    }
    
    public com.navdy.service.library.events.preferences.DriverProfilePreferences$Builder auto_on_enabled(Boolean a) {
        this.auto_on_enabled = a;
        return this;
    }
    
    public com.navdy.service.library.events.preferences.DriverProfilePreferences build() {
        this.checkRequiredFields();
        return new com.navdy.service.library.events.preferences.DriverProfilePreferences(this, (com.navdy.service.library.events.preferences.DriverProfilePreferences$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
    
    public com.navdy.service.library.events.preferences.DriverProfilePreferences$Builder car_make(String s) {
        this.car_make = s;
        return this;
    }
    
    public com.navdy.service.library.events.preferences.DriverProfilePreferences$Builder car_model(String s) {
        this.car_model = s;
        return this;
    }
    
    public com.navdy.service.library.events.preferences.DriverProfilePreferences$Builder car_year(String s) {
        this.car_year = s;
        return this;
    }
    
    public com.navdy.service.library.events.preferences.DriverProfilePreferences$Builder device_name(String s) {
        this.device_name = s;
        return this;
    }
    
    public com.navdy.service.library.events.preferences.DriverProfilePreferences$Builder dial_long_press_action(com.navdy.service.library.events.preferences.DriverProfilePreferences$DialLongPressAction a) {
        this.dial_long_press_action = a;
        return this;
    }
    
    public com.navdy.service.library.events.preferences.DriverProfilePreferences$Builder display_format(com.navdy.service.library.events.preferences.DriverProfilePreferences$DisplayFormat a) {
        this.display_format = a;
        return this;
    }
    
    public com.navdy.service.library.events.preferences.DriverProfilePreferences$Builder driver_email(String s) {
        this.driver_email = s;
        return this;
    }
    
    public com.navdy.service.library.events.preferences.DriverProfilePreferences$Builder driver_name(String s) {
        this.driver_name = s;
        return this;
    }
    
    public com.navdy.service.library.events.preferences.DriverProfilePreferences$Builder feature_mode(com.navdy.service.library.events.preferences.DriverProfilePreferences$FeatureMode a) {
        this.feature_mode = a;
        return this;
    }
    
    public com.navdy.service.library.events.preferences.DriverProfilePreferences$Builder limit_bandwidth(Boolean a) {
        this.limit_bandwidth = a;
        return this;
    }
    
    public com.navdy.service.library.events.preferences.DriverProfilePreferences$Builder locale(String s) {
        this.locale = s;
        return this;
    }
    
    public com.navdy.service.library.events.preferences.DriverProfilePreferences$Builder obdBlacklistLastModified(Long a) {
        this.obdBlacklistLastModified = a;
        return this;
    }
    
    public com.navdy.service.library.events.preferences.DriverProfilePreferences$Builder obdScanSetting(com.navdy.service.library.events.preferences.DriverProfilePreferences$ObdScanSetting a) {
        this.obdScanSetting = a;
        return this;
    }
    
    public com.navdy.service.library.events.preferences.DriverProfilePreferences$Builder photo_checksum(String s) {
        this.photo_checksum = s;
        return this;
    }
    
    public com.navdy.service.library.events.preferences.DriverProfilePreferences$Builder profile_is_public(Boolean a) {
        this.profile_is_public = a;
        return this;
    }
    
    public com.navdy.service.library.events.preferences.DriverProfilePreferences$Builder serial_number(Long a) {
        this.serial_number = a;
        return this;
    }
    
    public com.navdy.service.library.events.preferences.DriverProfilePreferences$Builder unit_system(com.navdy.service.library.events.preferences.DriverProfilePreferences$UnitSystem a) {
        this.unit_system = a;
        return this;
    }
}
