package com.navdy.service.library.events.preferences;

final public class LocalPreferences$Builder extends com.squareup.wire.Message.Builder {
    public com.navdy.service.library.events.settings.DateTimeConfiguration$Clock clockFormat;
    public Boolean manualZoom;
    public Float manualZoomLevel;
    
    public LocalPreferences$Builder() {
    }
    
    public LocalPreferences$Builder(com.navdy.service.library.events.preferences.LocalPreferences a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.manualZoom = a.manualZoom;
            this.manualZoomLevel = a.manualZoomLevel;
            this.clockFormat = a.clockFormat;
        }
    }
    
    public com.navdy.service.library.events.preferences.LocalPreferences build() {
        return new com.navdy.service.library.events.preferences.LocalPreferences(this, (com.navdy.service.library.events.preferences.LocalPreferences$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
    
    public com.navdy.service.library.events.preferences.LocalPreferences$Builder clockFormat(com.navdy.service.library.events.settings.DateTimeConfiguration$Clock a) {
        this.clockFormat = a;
        return this;
    }
    
    public com.navdy.service.library.events.preferences.LocalPreferences$Builder manualZoom(Boolean a) {
        this.manualZoom = a;
        return this;
    }
    
    public com.navdy.service.library.events.preferences.LocalPreferences$Builder manualZoomLevel(Float a) {
        this.manualZoomLevel = a;
        return this;
    }
}
