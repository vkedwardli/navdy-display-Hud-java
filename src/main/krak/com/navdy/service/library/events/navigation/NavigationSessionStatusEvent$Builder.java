package com.navdy.service.library.events.navigation;

final public class NavigationSessionStatusEvent$Builder extends com.squareup.wire.Message.Builder {
    public String destination_identifier;
    public String label;
    public com.navdy.service.library.events.navigation.NavigationRouteResult route;
    public String routeId;
    public com.navdy.service.library.events.navigation.NavigationSessionState sessionState;
    
    public NavigationSessionStatusEvent$Builder() {
    }
    
    public NavigationSessionStatusEvent$Builder(com.navdy.service.library.events.navigation.NavigationSessionStatusEvent a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.sessionState = a.sessionState;
            this.label = a.label;
            this.routeId = a.routeId;
            this.route = a.route;
            this.destination_identifier = a.destination_identifier;
        }
    }
    
    public com.navdy.service.library.events.navigation.NavigationSessionStatusEvent build() {
        this.checkRequiredFields();
        return new com.navdy.service.library.events.navigation.NavigationSessionStatusEvent(this, (com.navdy.service.library.events.navigation.NavigationSessionStatusEvent$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
    
    public com.navdy.service.library.events.navigation.NavigationSessionStatusEvent$Builder destination_identifier(String s) {
        this.destination_identifier = s;
        return this;
    }
    
    public com.navdy.service.library.events.navigation.NavigationSessionStatusEvent$Builder label(String s) {
        this.label = s;
        return this;
    }
    
    public com.navdy.service.library.events.navigation.NavigationSessionStatusEvent$Builder route(com.navdy.service.library.events.navigation.NavigationRouteResult a) {
        this.route = a;
        return this;
    }
    
    public com.navdy.service.library.events.navigation.NavigationSessionStatusEvent$Builder routeId(String s) {
        this.routeId = s;
        return this;
    }
    
    public com.navdy.service.library.events.navigation.NavigationSessionStatusEvent$Builder sessionState(com.navdy.service.library.events.navigation.NavigationSessionState a) {
        this.sessionState = a;
        return this;
    }
}
