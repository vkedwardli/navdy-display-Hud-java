package com.navdy.service.library.events.audio;

final public class MusicCapabilitiesResponse extends com.squareup.wire.Message {
    final public static java.util.List DEFAULT_CAPABILITIES;
    final private static long serialVersionUID = 0L;
    final public java.util.List capabilities;
    
    static {
        DEFAULT_CAPABILITIES = java.util.Collections.emptyList();
    }
    
    private MusicCapabilitiesResponse(com.navdy.service.library.events.audio.MusicCapabilitiesResponse$Builder a) {
        this(a.capabilities);
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    MusicCapabilitiesResponse(com.navdy.service.library.events.audio.MusicCapabilitiesResponse$Builder a, com.navdy.service.library.events.audio.MusicCapabilitiesResponse$1 a0) {
        this(a);
    }
    
    public MusicCapabilitiesResponse(java.util.List a) {
        this.capabilities = com.navdy.service.library.events.audio.MusicCapabilitiesResponse.immutableCopyOf(a);
    }
    
    static java.util.List access$000(java.util.List a) {
        return com.navdy.service.library.events.audio.MusicCapabilitiesResponse.copyOf(a);
    }
    
    public boolean equals(Object a) {
        return a == this || a instanceof com.navdy.service.library.events.audio.MusicCapabilitiesResponse && this.equals(this.capabilities, ((com.navdy.service.library.events.audio.MusicCapabilitiesResponse)a).capabilities);
    }
    
    public int hashCode() {
        int i = this.hashCode;
        if (i == 0) {
            i = (this.capabilities == null) ? 1 : this.capabilities.hashCode();
            this.hashCode = i;
        }
        return i;
    }
}
