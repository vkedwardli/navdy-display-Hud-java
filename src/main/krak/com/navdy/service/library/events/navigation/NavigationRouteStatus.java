package com.navdy.service.library.events.navigation;

final public class NavigationRouteStatus extends com.squareup.wire.Message {
    final public static String DEFAULT_HANDLE = "";
    final public static Integer DEFAULT_PROGRESS;
    final public static String DEFAULT_REQUESTID = "";
    final private static long serialVersionUID = 0L;
    final public String handle;
    final public Integer progress;
    final public String requestId;
    
    static {
        DEFAULT_PROGRESS = Integer.valueOf(0);
    }
    
    private NavigationRouteStatus(com.navdy.service.library.events.navigation.NavigationRouteStatus$Builder a) {
        this(a.handle, a.progress, a.requestId);
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    NavigationRouteStatus(com.navdy.service.library.events.navigation.NavigationRouteStatus$Builder a, com.navdy.service.library.events.navigation.NavigationRouteStatus$1 a0) {
        this(a);
    }
    
    public NavigationRouteStatus(String s, Integer a, String s0) {
        this.handle = s;
        this.progress = a;
        this.requestId = s0;
    }
    
    public boolean equals(Object a) {
        boolean b = false;
        label0: if (a != this) {
            if (a instanceof com.navdy.service.library.events.navigation.NavigationRouteStatus) {
                com.navdy.service.library.events.navigation.NavigationRouteStatus a0 = (com.navdy.service.library.events.navigation.NavigationRouteStatus)a;
                boolean b0 = this.equals(this.handle, a0.handle);
                label1: {
                    if (!b0) {
                        break label1;
                    }
                    if (!this.equals(this.progress, a0.progress)) {
                        break label1;
                    }
                    if (this.equals(this.requestId, a0.requestId)) {
                        b = true;
                        break label0;
                    }
                }
                b = false;
            } else {
                b = false;
            }
        } else {
            b = true;
        }
        return b;
    }
    
    public int hashCode() {
        int i = this.hashCode;
        if (i == 0) {
            i = (((this.handle == null) ? 0 : this.handle.hashCode()) * 37 + ((this.progress == null) ? 0 : this.progress.hashCode())) * 37 + ((this.requestId == null) ? 0 : this.requestId.hashCode());
            this.hashCode = i;
        }
        return i;
    }
}
