package com.navdy.service.library.events.navigation;

final public class NavigationRouteResult extends com.squareup.wire.Message {
    final public static String DEFAULT_ADDRESS = "";
    final public static Integer DEFAULT_DURATION;
    final public static Integer DEFAULT_DURATION_TRAFFIC;
    final public static java.util.List DEFAULT_GEOPOINTS_OBSOLETE;
    final public static String DEFAULT_LABEL = "";
    final public static Integer DEFAULT_LENGTH;
    final public static String DEFAULT_ROUTEID = "";
    final public static java.util.List DEFAULT_ROUTELATLONGS;
    final public static String DEFAULT_VIA = "";
    final private static long serialVersionUID = 0L;
    final public String address;
    final public Integer duration;
    final public Integer duration_traffic;
    final public java.util.List geoPoints_OBSOLETE;
    final public String label;
    final public Integer length;
    final public String routeId;
    final public java.util.List routeLatLongs;
    final public String via;
    
    static {
        DEFAULT_GEOPOINTS_OBSOLETE = java.util.Collections.emptyList();
        DEFAULT_LENGTH = Integer.valueOf(0);
        DEFAULT_DURATION = Integer.valueOf(0);
        DEFAULT_DURATION_TRAFFIC = Integer.valueOf(0);
        DEFAULT_ROUTELATLONGS = java.util.Collections.emptyList();
    }
    
    private NavigationRouteResult(com.navdy.service.library.events.navigation.NavigationRouteResult$Builder a) {
        this(a.routeId, a.label, a.geoPoints_OBSOLETE, a.length, a.duration, a.duration_traffic, a.routeLatLongs, a.via, a.address);
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    NavigationRouteResult(com.navdy.service.library.events.navigation.NavigationRouteResult$Builder a, com.navdy.service.library.events.navigation.NavigationRouteResult$1 a0) {
        this(a);
    }
    
    public NavigationRouteResult(String s, String s0, java.util.List a, Integer a0, Integer a1, Integer a2, java.util.List a3, String s1, String s2) {
        this.routeId = s;
        this.label = s0;
        this.geoPoints_OBSOLETE = com.navdy.service.library.events.navigation.NavigationRouteResult.immutableCopyOf(a);
        this.length = a0;
        this.duration = a1;
        this.duration_traffic = a2;
        this.routeLatLongs = com.navdy.service.library.events.navigation.NavigationRouteResult.immutableCopyOf(a3);
        this.via = s1;
        this.address = s2;
    }
    
    static java.util.List access$000(java.util.List a) {
        return com.navdy.service.library.events.navigation.NavigationRouteResult.copyOf(a);
    }
    
    static java.util.List access$100(java.util.List a) {
        return com.navdy.service.library.events.navigation.NavigationRouteResult.copyOf(a);
    }
    
    public boolean equals(Object a) {
        boolean b = false;
        label0: if (a != this) {
            if (a instanceof com.navdy.service.library.events.navigation.NavigationRouteResult) {
                com.navdy.service.library.events.navigation.NavigationRouteResult a0 = (com.navdy.service.library.events.navigation.NavigationRouteResult)a;
                boolean b0 = this.equals(this.routeId, a0.routeId);
                label1: {
                    if (!b0) {
                        break label1;
                    }
                    if (!this.equals(this.label, a0.label)) {
                        break label1;
                    }
                    if (!this.equals(this.geoPoints_OBSOLETE, a0.geoPoints_OBSOLETE)) {
                        break label1;
                    }
                    if (!this.equals(this.length, a0.length)) {
                        break label1;
                    }
                    if (!this.equals(this.duration, a0.duration)) {
                        break label1;
                    }
                    if (!this.equals(this.duration_traffic, a0.duration_traffic)) {
                        break label1;
                    }
                    if (!this.equals(this.routeLatLongs, a0.routeLatLongs)) {
                        break label1;
                    }
                    if (!this.equals(this.via, a0.via)) {
                        break label1;
                    }
                    if (this.equals(this.address, a0.address)) {
                        b = true;
                        break label0;
                    }
                }
                b = false;
            } else {
                b = false;
            }
        } else {
            b = true;
        }
        return b;
    }
    
    public int hashCode() {
        int i = this.hashCode;
        if (i == 0) {
            i = (((((((((this.routeId == null) ? 0 : this.routeId.hashCode()) * 37 + ((this.label == null) ? 0 : this.label.hashCode())) * 37 + ((this.geoPoints_OBSOLETE == null) ? 1 : this.geoPoints_OBSOLETE.hashCode())) * 37 + ((this.length == null) ? 0 : this.length.hashCode())) * 37 + ((this.duration == null) ? 0 : this.duration.hashCode())) * 37 + ((this.duration_traffic == null) ? 0 : this.duration_traffic.hashCode())) * 37 + ((this.routeLatLongs == null) ? 1 : this.routeLatLongs.hashCode())) * 37 + ((this.via == null) ? 0 : this.via.hashCode())) * 37 + ((this.address == null) ? 0 : this.address.hashCode());
            this.hashCode = i;
        }
        return i;
    }
}
