package com.navdy.service.library.events.audio;

final public class MusicEvent$Builder extends com.squareup.wire.Message.Builder {
    public com.navdy.service.library.events.audio.MusicEvent$Action action;
    public String collectionId;
    public com.navdy.service.library.events.audio.MusicCollectionSource collectionSource;
    public com.navdy.service.library.events.audio.MusicCollectionType collectionType;
    public com.navdy.service.library.events.audio.MusicDataSource dataSource;
    public Integer index;
    public com.navdy.service.library.events.audio.MusicRepeatMode repeatMode;
    public com.navdy.service.library.events.audio.MusicShuffleMode shuffleMode;
    
    public MusicEvent$Builder() {
    }
    
    public MusicEvent$Builder(com.navdy.service.library.events.audio.MusicEvent a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.action = a.action;
            this.dataSource = a.dataSource;
            this.collectionSource = a.collectionSource;
            this.collectionType = a.collectionType;
            this.collectionId = a.collectionId;
            this.index = a.index;
            this.shuffleMode = a.shuffleMode;
            this.repeatMode = a.repeatMode;
        }
    }
    
    public com.navdy.service.library.events.audio.MusicEvent$Builder action(com.navdy.service.library.events.audio.MusicEvent$Action a) {
        this.action = a;
        return this;
    }
    
    public com.navdy.service.library.events.audio.MusicEvent build() {
        this.checkRequiredFields();
        return new com.navdy.service.library.events.audio.MusicEvent(this, (com.navdy.service.library.events.audio.MusicEvent$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
    
    public com.navdy.service.library.events.audio.MusicEvent$Builder collectionId(String s) {
        this.collectionId = s;
        return this;
    }
    
    public com.navdy.service.library.events.audio.MusicEvent$Builder collectionSource(com.navdy.service.library.events.audio.MusicCollectionSource a) {
        this.collectionSource = a;
        return this;
    }
    
    public com.navdy.service.library.events.audio.MusicEvent$Builder collectionType(com.navdy.service.library.events.audio.MusicCollectionType a) {
        this.collectionType = a;
        return this;
    }
    
    public com.navdy.service.library.events.audio.MusicEvent$Builder dataSource(com.navdy.service.library.events.audio.MusicDataSource a) {
        this.dataSource = a;
        return this;
    }
    
    public com.navdy.service.library.events.audio.MusicEvent$Builder index(Integer a) {
        this.index = a;
        return this;
    }
    
    public com.navdy.service.library.events.audio.MusicEvent$Builder repeatMode(com.navdy.service.library.events.audio.MusicRepeatMode a) {
        this.repeatMode = a;
        return this;
    }
    
    public com.navdy.service.library.events.audio.MusicEvent$Builder shuffleMode(com.navdy.service.library.events.audio.MusicShuffleMode a) {
        this.shuffleMode = a;
        return this;
    }
}
