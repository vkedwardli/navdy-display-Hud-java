package com.navdy.service.library.events.notification;

final public class NotificationListResponse extends com.squareup.wire.Message {
    final public static java.util.List DEFAULT_IDS;
    final public static com.navdy.service.library.events.RequestStatus DEFAULT_STATUS;
    final public static String DEFAULT_STATUS_DETAIL = "";
    final private static long serialVersionUID = 0L;
    final public java.util.List ids;
    final public com.navdy.service.library.events.RequestStatus status;
    final public String status_detail;
    
    static {
        DEFAULT_STATUS = com.navdy.service.library.events.RequestStatus.REQUEST_SUCCESS;
        DEFAULT_IDS = java.util.Collections.emptyList();
    }
    
    public NotificationListResponse(com.navdy.service.library.events.RequestStatus a, String s, java.util.List a0) {
        this.status = a;
        this.status_detail = s;
        this.ids = com.navdy.service.library.events.notification.NotificationListResponse.immutableCopyOf(a0);
    }
    
    private NotificationListResponse(com.navdy.service.library.events.notification.NotificationListResponse$Builder a) {
        this(a.status, a.status_detail, a.ids);
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    NotificationListResponse(com.navdy.service.library.events.notification.NotificationListResponse$Builder a, com.navdy.service.library.events.notification.NotificationListResponse$1 a0) {
        this(a);
    }
    
    static java.util.List access$000(java.util.List a) {
        return com.navdy.service.library.events.notification.NotificationListResponse.copyOf(a);
    }
    
    public boolean equals(Object a) {
        boolean b = false;
        label0: if (a != this) {
            if (a instanceof com.navdy.service.library.events.notification.NotificationListResponse) {
                com.navdy.service.library.events.notification.NotificationListResponse a0 = (com.navdy.service.library.events.notification.NotificationListResponse)a;
                boolean b0 = this.equals(this.status, a0.status);
                label1: {
                    if (!b0) {
                        break label1;
                    }
                    if (!this.equals(this.status_detail, a0.status_detail)) {
                        break label1;
                    }
                    if (this.equals(this.ids, a0.ids)) {
                        b = true;
                        break label0;
                    }
                }
                b = false;
            } else {
                b = false;
            }
        } else {
            b = true;
        }
        return b;
    }
    
    public int hashCode() {
        int i = this.hashCode;
        if (i == 0) {
            i = (((this.status == null) ? 0 : this.status.hashCode()) * 37 + ((this.status_detail == null) ? 0 : this.status_detail.hashCode())) * 37 + ((this.ids == null) ? 1 : this.ids.hashCode());
            this.hashCode = i;
        }
        return i;
    }
}
