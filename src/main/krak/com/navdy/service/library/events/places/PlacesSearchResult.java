package com.navdy.service.library.events.places;

final public class PlacesSearchResult extends com.squareup.wire.Message {
    final public static String DEFAULT_ADDRESS = "";
    final public static String DEFAULT_CATEGORY = "";
    final public static Double DEFAULT_DISTANCE;
    final public static String DEFAULT_LABEL = "";
    final private static long serialVersionUID = 0L;
    final public String address;
    final public String category;
    final public com.navdy.service.library.events.location.Coordinate destinationLocation;
    final public Double distance;
    final public String label;
    final public com.navdy.service.library.events.location.Coordinate navigationPosition;
    
    static {
        DEFAULT_DISTANCE = Double.valueOf(0.0);
    }
    
    private PlacesSearchResult(com.navdy.service.library.events.places.PlacesSearchResult$Builder a) {
        this(a.label, a.navigationPosition, a.destinationLocation, a.address, a.category, a.distance);
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    PlacesSearchResult(com.navdy.service.library.events.places.PlacesSearchResult$Builder a, com.navdy.service.library.events.places.PlacesSearchResult$1 a0) {
        this(a);
    }
    
    public PlacesSearchResult(String s, com.navdy.service.library.events.location.Coordinate a, com.navdy.service.library.events.location.Coordinate a0, String s0, String s1, Double a1) {
        this.label = s;
        this.navigationPosition = a;
        this.destinationLocation = a0;
        this.address = s0;
        this.category = s1;
        this.distance = a1;
    }
    
    public boolean equals(Object a) {
        boolean b = false;
        label0: if (a != this) {
            if (a instanceof com.navdy.service.library.events.places.PlacesSearchResult) {
                com.navdy.service.library.events.places.PlacesSearchResult a0 = (com.navdy.service.library.events.places.PlacesSearchResult)a;
                boolean b0 = this.equals(this.label, a0.label);
                label1: {
                    if (!b0) {
                        break label1;
                    }
                    if (!this.equals(this.navigationPosition, a0.navigationPosition)) {
                        break label1;
                    }
                    if (!this.equals(this.destinationLocation, a0.destinationLocation)) {
                        break label1;
                    }
                    if (!this.equals(this.address, a0.address)) {
                        break label1;
                    }
                    if (!this.equals(this.category, a0.category)) {
                        break label1;
                    }
                    if (this.equals(this.distance, a0.distance)) {
                        b = true;
                        break label0;
                    }
                }
                b = false;
            } else {
                b = false;
            }
        } else {
            b = true;
        }
        return b;
    }
    
    public int hashCode() {
        int i = this.hashCode;
        if (i == 0) {
            i = ((((((this.label == null) ? 0 : this.label.hashCode()) * 37 + ((this.navigationPosition == null) ? 0 : this.navigationPosition.hashCode())) * 37 + ((this.destinationLocation == null) ? 0 : this.destinationLocation.hashCode())) * 37 + ((this.address == null) ? 0 : this.address.hashCode())) * 37 + ((this.category == null) ? 0 : this.category.hashCode())) * 37 + ((this.distance == null) ? 0 : this.distance.hashCode());
            this.hashCode = i;
        }
        return i;
    }
}
