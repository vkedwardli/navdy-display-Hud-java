package com.navdy.service.library.events.input;

final public class DialSimulationEvent$DialInputAction extends Enum implements com.squareup.wire.ProtoEnum {
    final private static com.navdy.service.library.events.input.DialSimulationEvent$DialInputAction[] $VALUES;
    final public static com.navdy.service.library.events.input.DialSimulationEvent$DialInputAction DIAL_CLICK;
    final public static com.navdy.service.library.events.input.DialSimulationEvent$DialInputAction DIAL_LEFT_TURN;
    final public static com.navdy.service.library.events.input.DialSimulationEvent$DialInputAction DIAL_LONG_CLICK;
    final public static com.navdy.service.library.events.input.DialSimulationEvent$DialInputAction DIAL_RIGHT_TURN;
    final private int value;
    
    static {
        DIAL_CLICK = new com.navdy.service.library.events.input.DialSimulationEvent$DialInputAction("DIAL_CLICK", 0, 1);
        DIAL_LONG_CLICK = new com.navdy.service.library.events.input.DialSimulationEvent$DialInputAction("DIAL_LONG_CLICK", 1, 2);
        DIAL_LEFT_TURN = new com.navdy.service.library.events.input.DialSimulationEvent$DialInputAction("DIAL_LEFT_TURN", 2, 3);
        DIAL_RIGHT_TURN = new com.navdy.service.library.events.input.DialSimulationEvent$DialInputAction("DIAL_RIGHT_TURN", 3, 4);
        com.navdy.service.library.events.input.DialSimulationEvent$DialInputAction[] a = new com.navdy.service.library.events.input.DialSimulationEvent$DialInputAction[4];
        a[0] = DIAL_CLICK;
        a[1] = DIAL_LONG_CLICK;
        a[2] = DIAL_LEFT_TURN;
        a[3] = DIAL_RIGHT_TURN;
        $VALUES = a;
    }
    
    private DialSimulationEvent$DialInputAction(String s, int i, int i0) {
        super(s, i);
        this.value = i0;
    }
    
    public static com.navdy.service.library.events.input.DialSimulationEvent$DialInputAction valueOf(String s) {
        return (com.navdy.service.library.events.input.DialSimulationEvent$DialInputAction)Enum.valueOf(com.navdy.service.library.events.input.DialSimulationEvent$DialInputAction.class, s);
    }
    
    public static com.navdy.service.library.events.input.DialSimulationEvent$DialInputAction[] values() {
        return $VALUES.clone();
    }
    
    public int getValue() {
        return this.value;
    }
}
