package com.navdy.service.library.events.obd;

final public class ObdStatusRequest extends com.squareup.wire.Message {
    final private static long serialVersionUID = 0L;
    
    public ObdStatusRequest() {
    }
    
    private ObdStatusRequest(com.navdy.service.library.events.obd.ObdStatusRequest$Builder a) {
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    ObdStatusRequest(com.navdy.service.library.events.obd.ObdStatusRequest$Builder a, com.navdy.service.library.events.obd.ObdStatusRequest$1 a0) {
        this(a);
    }
    
    public boolean equals(Object a) {
        return a instanceof com.navdy.service.library.events.obd.ObdStatusRequest;
    }
    
    public int hashCode() {
        return 0;
    }
}
