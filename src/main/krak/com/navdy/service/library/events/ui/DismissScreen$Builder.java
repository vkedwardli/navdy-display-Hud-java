package com.navdy.service.library.events.ui;

final public class DismissScreen$Builder extends com.squareup.wire.Message.Builder {
    public com.navdy.service.library.events.ui.Screen screen;
    
    public DismissScreen$Builder() {
    }
    
    public DismissScreen$Builder(com.navdy.service.library.events.ui.DismissScreen a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.screen = a.screen;
        }
    }
    
    public com.navdy.service.library.events.ui.DismissScreen build() {
        this.checkRequiredFields();
        return new com.navdy.service.library.events.ui.DismissScreen(this, (com.navdy.service.library.events.ui.DismissScreen$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
    
    public com.navdy.service.library.events.ui.DismissScreen$Builder screen(com.navdy.service.library.events.ui.Screen a) {
        this.screen = a;
        return this;
    }
}
