package com.navdy.service.library.events.settings;

final public class UpdateSettings$Builder extends com.squareup.wire.Message.Builder {
    public com.navdy.service.library.events.settings.ScreenConfiguration screenConfiguration;
    public java.util.List settings;
    
    public UpdateSettings$Builder() {
    }
    
    public UpdateSettings$Builder(com.navdy.service.library.events.settings.UpdateSettings a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.screenConfiguration = a.screenConfiguration;
            this.settings = com.navdy.service.library.events.settings.UpdateSettings.access$000(a.settings);
        }
    }
    
    public com.navdy.service.library.events.settings.UpdateSettings build() {
        return new com.navdy.service.library.events.settings.UpdateSettings(this, (com.navdy.service.library.events.settings.UpdateSettings$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
    
    public com.navdy.service.library.events.settings.UpdateSettings$Builder screenConfiguration(com.navdy.service.library.events.settings.ScreenConfiguration a) {
        this.screenConfiguration = a;
        return this;
    }
    
    public com.navdy.service.library.events.settings.UpdateSettings$Builder settings(java.util.List a) {
        this.settings = com.navdy.service.library.events.settings.UpdateSettings$Builder.checkForNulls(a);
        return this;
    }
}
