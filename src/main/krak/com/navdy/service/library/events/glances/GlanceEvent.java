package com.navdy.service.library.events.glances;

final public class GlanceEvent extends com.squareup.wire.Message {
    final public static java.util.List DEFAULT_ACTIONS;
    final public static java.util.List DEFAULT_GLANCEDATA;
    final public static com.navdy.service.library.events.glances.GlanceEvent$GlanceType DEFAULT_GLANCETYPE;
    final public static String DEFAULT_ID = "";
    final public static String DEFAULT_LANGUAGE = "";
    final public static Long DEFAULT_POSTTIME;
    final public static String DEFAULT_PROVIDER = "";
    final private static long serialVersionUID = 0L;
    final public java.util.List actions;
    final public java.util.List glanceData;
    final public com.navdy.service.library.events.glances.GlanceEvent$GlanceType glanceType;
    final public String id;
    final public String language;
    final public Long postTime;
    final public String provider;
    
    static {
        DEFAULT_GLANCETYPE = com.navdy.service.library.events.glances.GlanceEvent$GlanceType.GLANCE_TYPE_CALENDAR;
        DEFAULT_POSTTIME = Long.valueOf(0L);
        DEFAULT_GLANCEDATA = java.util.Collections.emptyList();
        DEFAULT_ACTIONS = java.util.Collections.emptyList();
    }
    
    private GlanceEvent(com.navdy.service.library.events.glances.GlanceEvent$Builder a) {
        this(a.glanceType, a.provider, a.id, a.postTime, a.glanceData, a.actions, a.language);
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    GlanceEvent(com.navdy.service.library.events.glances.GlanceEvent$Builder a, com.navdy.service.library.events.glances.GlanceEvent$1 a0) {
        this(a);
    }
    
    public GlanceEvent(com.navdy.service.library.events.glances.GlanceEvent$GlanceType a, String s, String s0, Long a0, java.util.List a1, java.util.List a2, String s1) {
        this.glanceType = a;
        this.provider = s;
        this.id = s0;
        this.postTime = a0;
        this.glanceData = com.navdy.service.library.events.glances.GlanceEvent.immutableCopyOf(a1);
        this.actions = com.navdy.service.library.events.glances.GlanceEvent.immutableCopyOf(a2);
        this.language = s1;
    }
    
    static java.util.List access$000(java.util.List a) {
        return com.navdy.service.library.events.glances.GlanceEvent.copyOf(a);
    }
    
    static java.util.List access$100(java.util.List a) {
        return com.navdy.service.library.events.glances.GlanceEvent.copyOf(a);
    }
    
    public boolean equals(Object a) {
        boolean b = false;
        label0: if (a != this) {
            if (a instanceof com.navdy.service.library.events.glances.GlanceEvent) {
                com.navdy.service.library.events.glances.GlanceEvent a0 = (com.navdy.service.library.events.glances.GlanceEvent)a;
                boolean b0 = this.equals(this.glanceType, a0.glanceType);
                label1: {
                    if (!b0) {
                        break label1;
                    }
                    if (!this.equals(this.provider, a0.provider)) {
                        break label1;
                    }
                    if (!this.equals(this.id, a0.id)) {
                        break label1;
                    }
                    if (!this.equals(this.postTime, a0.postTime)) {
                        break label1;
                    }
                    if (!this.equals(this.glanceData, a0.glanceData)) {
                        break label1;
                    }
                    if (!this.equals(this.actions, a0.actions)) {
                        break label1;
                    }
                    if (this.equals(this.language, a0.language)) {
                        b = true;
                        break label0;
                    }
                }
                b = false;
            } else {
                b = false;
            }
        } else {
            b = true;
        }
        return b;
    }
    
    public int hashCode() {
        int i = this.hashCode;
        if (i == 0) {
            i = (((((((this.glanceType == null) ? 0 : this.glanceType.hashCode()) * 37 + ((this.provider == null) ? 0 : this.provider.hashCode())) * 37 + ((this.id == null) ? 0 : this.id.hashCode())) * 37 + ((this.postTime == null) ? 0 : this.postTime.hashCode())) * 37 + ((this.glanceData == null) ? 1 : this.glanceData.hashCode())) * 37 + ((this.actions == null) ? 1 : this.actions.hashCode())) * 37 + ((this.language == null) ? 0 : this.language.hashCode());
            this.hashCode = i;
        }
        return i;
    }
}
