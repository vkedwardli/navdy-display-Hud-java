package com.navdy.service.library.events.connection;

final public class LinkPropertiesChanged extends com.squareup.wire.Message {
    final public static Integer DEFAULT_BANDWIDTHLEVEL;
    final private static long serialVersionUID = 0L;
    final public Integer bandwidthLevel;
    
    static {
        DEFAULT_BANDWIDTHLEVEL = Integer.valueOf(0);
    }
    
    private LinkPropertiesChanged(com.navdy.service.library.events.connection.LinkPropertiesChanged$Builder a) {
        this(a.bandwidthLevel);
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    LinkPropertiesChanged(com.navdy.service.library.events.connection.LinkPropertiesChanged$Builder a, com.navdy.service.library.events.connection.LinkPropertiesChanged$1 a0) {
        this(a);
    }
    
    public LinkPropertiesChanged(Integer a) {
        this.bandwidthLevel = a;
    }
    
    public boolean equals(Object a) {
        return a == this || a instanceof com.navdy.service.library.events.connection.LinkPropertiesChanged && this.equals(this.bandwidthLevel, ((com.navdy.service.library.events.connection.LinkPropertiesChanged)a).bandwidthLevel);
    }
    
    public int hashCode() {
        int i = this.hashCode;
        if (i == 0) {
            i = (this.bandwidthLevel == null) ? 0 : this.bandwidthLevel.hashCode();
            this.hashCode = i;
        }
        return i;
    }
}
