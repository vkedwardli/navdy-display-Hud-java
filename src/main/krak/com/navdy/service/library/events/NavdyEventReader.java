package com.navdy.service.library.events;

public class NavdyEventReader {
    final private static int FRAME_LENGTH = 5;
    final private static com.navdy.service.library.log.Logger sLogger;
    protected java.io.DataInputStream mInputStream;
    protected com.squareup.wire.Wire mWire;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.service.library.events.NavdyEventReader.class);
    }
    
    public NavdyEventReader(java.io.InputStream a) {
        this.mInputStream = new java.io.DataInputStream(a);
        Class[] a0 = new Class[1];
        a0[0] = com.navdy.service.library.events.Ext_NavdyEvent.class;
        this.mWire = new com.squareup.wire.Wire(a0);
    }
    
    private byte[] readBytes(int i) {
        byte[] a = new byte[i];
        this.mInputStream.readFully(a);
        return a;
    }
    
    private int readSize() {
        int i = 0;
        label1: {
            byte[] a = null;
            com.navdy.service.library.events.Frame a0 = null;
            label0: {
                try {
                    a = this.readBytes(5);
                    break label0;
                } catch(Exception ignoredException) {
                }
                i = 0;
                break label1;
            }
            try {
                a0 = (com.navdy.service.library.events.Frame)this.mWire.parseFrom(a, com.navdy.service.library.events.Frame.class);
            } catch(Throwable a1) {
                sLogger.e("Failed to parse frame", a1);
                a0 = null;
            }
            i = (a0 != null) ? a0.size.intValue() : 0;
        }
        return i;
    }
    
    public byte[] readBytes() {
        int i = this.readSize();
        return (i > 0) ? this.readBytes(i) : null;
    }
}
