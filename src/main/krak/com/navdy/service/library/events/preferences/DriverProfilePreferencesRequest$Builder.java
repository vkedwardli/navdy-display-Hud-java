package com.navdy.service.library.events.preferences;

final public class DriverProfilePreferencesRequest$Builder extends com.squareup.wire.Message.Builder {
    public Long serial_number;
    
    public DriverProfilePreferencesRequest$Builder() {
    }
    
    public DriverProfilePreferencesRequest$Builder(com.navdy.service.library.events.preferences.DriverProfilePreferencesRequest a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.serial_number = a.serial_number;
        }
    }
    
    public com.navdy.service.library.events.preferences.DriverProfilePreferencesRequest build() {
        return new com.navdy.service.library.events.preferences.DriverProfilePreferencesRequest(this, (com.navdy.service.library.events.preferences.DriverProfilePreferencesRequest$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
    
    public com.navdy.service.library.events.preferences.DriverProfilePreferencesRequest$Builder serial_number(Long a) {
        this.serial_number = a;
        return this;
    }
}
