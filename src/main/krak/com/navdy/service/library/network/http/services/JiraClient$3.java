package com.navdy.service.library.network.http.services;

class JiraClient$3 implements okhttp3.Callback {
    final com.navdy.service.library.network.http.services.JiraClient this$0;
    final com.navdy.service.library.network.http.services.JiraClient$ResultCallback val$callback;
    
    JiraClient$3(com.navdy.service.library.network.http.services.JiraClient a, com.navdy.service.library.network.http.services.JiraClient$ResultCallback a0) {
        super();
        this.this$0 = a;
        this.val$callback = a0;
    }
    
    public void onFailure(okhttp3.Call a, java.io.IOException a0) {
        if (this.val$callback != null) {
            this.val$callback.onError((Throwable)a0);
        }
    }
    
    public void onResponse(okhttp3.Call a, okhttp3.Response a0) {
        label0: try {
            int i = a0.code();
            label1: {
                label2: {
                    if (i == 200) {
                        break label2;
                    }
                    if (i != 201) {
                        break label1;
                    }
                }
                String s = new org.json.JSONObject(a0.body().string()).getString("key");
                if (this.val$callback == null) {
                    break label0;
                }
                this.val$callback.onSuccess(s);
                break label0;
            }
            if (this.val$callback != null) {
                this.val$callback.onError((Throwable)new java.io.IOException(new StringBuilder().append("Jira request failed with ").append(i).toString()));
            }
        } catch(Throwable a1) {
            if (this.val$callback != null) {
                this.val$callback.onError(a1);
            }
        }
    }
}
