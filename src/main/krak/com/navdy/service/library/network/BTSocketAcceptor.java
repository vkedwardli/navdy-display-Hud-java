package com.navdy.service.library.network;

public class BTSocketAcceptor implements com.navdy.service.library.network.SocketAcceptor {
    final private static boolean needsPatch;
    private static com.navdy.service.library.log.Logger sLogger;
    android.bluetooth.BluetoothServerSocket btServerSocket;
    private com.navdy.service.library.network.BTSocketAdapter internalSocket;
    final private String sdpName;
    final private boolean secure;
    final private java.util.UUID serviceUUID;
    
    static {
        boolean b = false;
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.service.library.network.BTSocketAcceptor.class);
        int i = android.os.Build.VERSION.SDK_INT;
        label2: {
            label0: {
                label1: {
                    if (i < 17) {
                        break label1;
                    }
                    if (android.os.Build.VERSION.SDK_INT <= 20) {
                        break label0;
                    }
                }
                b = false;
                break label2;
            }
            b = true;
        }
        needsPatch = b;
    }
    
    public BTSocketAcceptor(String s, java.util.UUID a) {
        this(s, a, true);
    }
    
    public BTSocketAcceptor(String s, java.util.UUID a, boolean b) {
        this.sdpName = s;
        this.serviceUUID = a;
        this.secure = b;
    }
    
    private void storeSocket() {
        try {
            java.lang.reflect.Field a = (this.btServerSocket).getClass().getDeclaredField("mSocket");
            a.setAccessible(true);
            this.internalSocket = new com.navdy.service.library.network.BTSocketAdapter((android.bluetooth.BluetoothSocket)a.get(this.btServerSocket));
        } catch(Exception a0) {
            sLogger.w("Failed to close server socket", (Throwable)a0);
        }
    }
    
    public com.navdy.service.library.network.SocketAdapter accept() {
        if (this.btServerSocket == null) {
            android.bluetooth.BluetoothAdapter a = android.bluetooth.BluetoothAdapter.getDefaultAdapter();
            if (a == null) {
                throw new java.io.IOException("Bluetooth unavailable");
            }
            if (this.secure) {
                this.btServerSocket = a.listenUsingRfcommWithServiceRecord(this.sdpName, this.serviceUUID);
            } else {
                this.btServerSocket = a.listenUsingInsecureRfcommWithServiceRecord(this.sdpName, this.serviceUUID);
            }
            if (needsPatch) {
                this.storeSocket();
            }
        }
        return (com.navdy.service.library.network.SocketAdapter)new com.navdy.service.library.network.BTSocketAdapter(this.btServerSocket.accept());
    }
    
    public void close() {
        if (this.btServerSocket != null) {
            this.btServerSocket.close();
            if (this.internalSocket != null) {
                this.internalSocket.close();
                this.internalSocket = null;
            }
            this.btServerSocket = null;
        }
    }
    
    public com.navdy.service.library.device.connection.ConnectionInfo getRemoteConnectionInfo(com.navdy.service.library.network.SocketAdapter a, com.navdy.service.library.device.connection.ConnectionType a0) {
        com.navdy.service.library.device.NavdyDeviceId a1 = a.getRemoteDevice();
        com.navdy.service.library.device.connection.BTConnectionInfo a2 = (a1 == null) ? null : new com.navdy.service.library.device.connection.BTConnectionInfo(a1, this.serviceUUID, a0);
        return a2;
    }
}
