package com.navdy.service.library.network;

public class BTSocketAdapter implements com.navdy.service.library.network.SocketAdapter {
    final private static boolean needsPatch;
    private static com.navdy.service.library.log.Logger sLogger;
    private boolean closed;
    private int mFd;
    private android.os.ParcelFileDescriptor mPfd;
    final android.bluetooth.BluetoothSocket socket;
    
    static {
        boolean b = false;
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.service.library.network.BTSocketAdapter.class);
        int i = android.os.Build.VERSION.SDK_INT;
        label2: {
            label0: {
                label1: {
                    if (i < 17) {
                        break label1;
                    }
                    if (android.os.Build.VERSION.SDK_INT <= 20) {
                        break label0;
                    }
                }
                b = false;
                break label2;
            }
            b = true;
        }
        needsPatch = b;
    }
    
    public BTSocketAdapter(android.bluetooth.BluetoothSocket a) {
        this.mFd = -1;
        this.closed = false;
        if (a == null) {
            throw new IllegalArgumentException("Socket must not be null");
        }
        this.socket = a;
        if (needsPatch) {
            this.storeFd();
        }
    }
    
    private void cleanupFd() {
        synchronized(this) {
            if (this.mPfd != null) {
                sLogger.d("Closing mPfd");
                this.mPfd.close();
                this.mPfd = null;
            }
            if (this.mFd != -1) {
                sLogger.d(new StringBuilder().append("Closing mFd:").append(this.mFd).toString());
                com.navdy.service.library.util.IOUtils.closeFD(this.mFd);
                this.mFd = -1;
            }
        }
        /*monexit(this)*/;
    }
    
    private void storeFd() {
        label0: synchronized(this) {
            android.os.ParcelFileDescriptor a = this.mPfd;
            label1: {
                if (a == null) {
                    break label1;
                }
                if (this.mFd != -1) {
                    break label0;
                }
            }
            try {
                java.lang.reflect.Field a0 = (this.socket).getClass().getDeclaredField("mPfd");
                a0.setAccessible(true);
                android.os.ParcelFileDescriptor a1 = (android.os.ParcelFileDescriptor)a0.get(this.socket);
                if (a1 != null) {
                    this.mPfd = a1;
                }
                int i = com.navdy.service.library.util.IOUtils.getSocketFD(this.socket);
                if (i != -1) {
                    this.mFd = i;
                }
                sLogger.d(new StringBuilder().append("Stored ").append(this.mPfd).append(" fd:").append(this.mFd).toString());
            } catch(Exception a2) {
                sLogger.w("Exception storing socket internals", (Throwable)a2);
            }
        }
        /*monexit(this)*/;
    }
    
    public void close() {
        synchronized(this.socket) {
            if (!this.closed) {
                if (needsPatch) {
                    this.storeFd();
                }
                this.socket.close();
                if (needsPatch) {
                    this.cleanupFd();
                }
                this.closed = true;
            }
            /*monexit(a)*/;
        }
    }
    
    public void connect() {
        boolean b = false;
        try {
            this.socket.connect();
            b = needsPatch;
        } catch(java.io.IOException a) {
            if (needsPatch) {
                this.storeFd();
            }
            throw a;
        }
        if (b) {
            this.storeFd();
        }
    }
    
    public java.io.InputStream getInputStream() {
        return this.socket.getInputStream();
    }
    
    public java.io.OutputStream getOutputStream() {
        return this.socket.getOutputStream();
    }
    
    public com.navdy.service.library.device.NavdyDeviceId getRemoteDevice() {
        android.bluetooth.BluetoothDevice a = this.socket.getRemoteDevice();
        com.navdy.service.library.device.NavdyDeviceId a0 = (a == null) ? null : new com.navdy.service.library.device.NavdyDeviceId(a);
        return a0;
    }
    
    public boolean isConnected() {
        return this.socket.isConnected();
    }
}
