package com.navdy.service.library.network.http;

public class HttpUtils {
    final public static okhttp3.MediaType JSON;
    final public static String MIME_TYPE_IMAGE_PNG = "image/png";
    final public static String MIME_TYPE_TEXT = "text/plain";
    final public static String MIME_TYPE_ZIP = "application/zip";
    
    static {
        JSON = okhttp3.MediaType.parse("application/json; charset=utf-8");
    }
    
    public HttpUtils() {
    }
    
    public static okhttp3.RequestBody getJsonRequestBody(String s) {
        return okhttp3.RequestBody.create(JSON, s);
    }
}
