package com.navdy.service.library.network;

public class TCPSocketAcceptor implements com.navdy.service.library.network.SocketAcceptor {
    final private static com.navdy.service.library.log.Logger sLogger;
    final private boolean local;
    final private int port;
    private java.net.ServerSocket serverSocket;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.service.library.network.TCPSocketAcceptor.class);
    }
    
    public TCPSocketAcceptor(int i) {
        this(i, false);
    }
    
    public TCPSocketAcceptor(int i, boolean b) {
        this.local = b;
        this.port = i;
    }
    
    public com.navdy.service.library.network.SocketAdapter accept() {
        if (this.serverSocket == null) {
            if (this.local) {
                byte[] a = new byte[4];
                a[0] = (byte)127;
                a[1] = (byte)0;
                a[2] = (byte)0;
                a[3] = (byte)1;
                java.net.InetAddress a0 = java.net.InetAddress.getByAddress(a);
                this.serverSocket = new java.net.ServerSocket(this.port, 10, a0);
            } else {
                this.serverSocket = new java.net.ServerSocket(this.port, 10);
            }
            sLogger.d(new StringBuilder().append("Socket bound to ").append((this.local) ? "localhost" : "all interfaces").append(", port ").append(this.port).toString());
        }
        return (com.navdy.service.library.network.SocketAdapter)new com.navdy.service.library.network.TCPSocketAdapter(this.serverSocket.accept());
    }
    
    public void close() {
        if (this.serverSocket != null) {
            this.serverSocket.close();
            this.serverSocket = null;
        }
    }
    
    public com.navdy.service.library.device.connection.ConnectionInfo getRemoteConnectionInfo(com.navdy.service.library.network.SocketAdapter a, com.navdy.service.library.device.connection.ConnectionType a0) {
        com.navdy.service.library.device.connection.TCPConnectionInfo a1 = null;
        boolean b = a instanceof com.navdy.service.library.network.TCPSocketAdapter;
        label2: {
            com.navdy.service.library.network.TCPSocketAdapter a2 = null;
            com.navdy.service.library.device.NavdyDeviceId a3 = null;
            label0: {
                label1: {
                    if (!b) {
                        break label1;
                    }
                    a2 = (com.navdy.service.library.network.TCPSocketAdapter)a;
                    a3 = ((com.navdy.service.library.network.SocketAdapter)a2).getRemoteDevice();
                    if (a3 != null) {
                        break label0;
                    }
                }
                a1 = null;
                break label2;
            }
            a1 = new com.navdy.service.library.device.connection.TCPConnectionInfo(a3, a2.getRemoteAddress());
        }
        return a1;
    }
}
