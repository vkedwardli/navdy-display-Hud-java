package com.navdy.service.library.network;

public class TCPSocketFactory implements com.navdy.service.library.network.SocketFactory {
    final private String host;
    final private int port;
    
    public TCPSocketFactory(com.navdy.service.library.device.connection.ServiceAddress a) {
        this(a.getAddress(), Integer.parseInt(a.getService()));
    }
    
    public TCPSocketFactory(String s, int i) {
        this.host = s;
        this.port = i;
    }
    
    public com.navdy.service.library.network.SocketAdapter build() {
        return (com.navdy.service.library.network.SocketAdapter)new com.navdy.service.library.network.TCPSocketAdapter(this.host, this.port);
    }
}
