package com.navdy.service.library;

public class Version {
    final public static com.navdy.service.library.Version PROTOCOL_VERSION;
    final public int majorVersion;
    final public int minorVersion;
    
    static {
        PROTOCOL_VERSION = new com.navdy.service.library.Version(1, 0);
    }
    
    public Version(int i, int i0) {
        this.majorVersion = i;
        this.minorVersion = i0;
    }
    
    public String toString() {
        return new StringBuilder().append(this.majorVersion).append(".").append(this.minorVersion).toString();
    }
}
