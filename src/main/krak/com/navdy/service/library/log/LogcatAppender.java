package com.navdy.service.library.log;

public class LogcatAppender implements com.navdy.service.library.log.LogAppender {
    public LogcatAppender() {
    }
    
    public void close() {
    }
    
    public void d(String s, String s0) {
        android.util.Log.d(s, s0);
    }
    
    public void d(String s, String s0, Throwable a) {
        android.util.Log.d(s, s0, a);
    }
    
    public void e(String s, String s0) {
        android.util.Log.e(s, s0);
    }
    
    public void e(String s, String s0, Throwable a) {
        android.util.Log.e(s, s0, a);
    }
    
    public void flush() {
    }
    
    public void i(String s, String s0) {
        android.util.Log.i(s, s0);
    }
    
    public void i(String s, String s0, Throwable a) {
        android.util.Log.i(s, s0, a);
    }
    
    public void v(String s, String s0) {
        android.util.Log.v(s, s0);
    }
    
    public void v(String s, String s0, Throwable a) {
        android.util.Log.v(s, s0, a);
    }
    
    public void w(String s, String s0) {
        android.util.Log.w(s, s0);
    }
    
    public void w(String s, String s0, Throwable a) {
        android.util.Log.w(s, s0, a);
    }
}
