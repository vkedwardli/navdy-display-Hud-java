package com.navdy.service.library.log;

final class FastPrintWriter$1 extends java.io.Writer {
    FastPrintWriter$1() {
    }
    
    public void close() {
        throw new UnsupportedOperationException("Shouldn't be here");
    }
    
    public void flush() {
        this.close();
    }
    
    public void write(char[] a, int i, int i0) {
        this.close();
    }
}
