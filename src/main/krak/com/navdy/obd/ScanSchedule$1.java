package com.navdy.obd;

final class ScanSchedule$1 implements android.os.Parcelable$Creator {
    ScanSchedule$1() {
    }
    
    public com.navdy.obd.ScanSchedule createFromParcel(android.os.Parcel a) {
        return new com.navdy.obd.ScanSchedule(a, (com.navdy.obd.ScanSchedule$1)null);
    }
    
    public Object createFromParcel(android.os.Parcel a) {
        return this.createFromParcel(a);
    }
    
    public com.navdy.obd.ScanSchedule[] newArray(int i) {
        return new com.navdy.obd.ScanSchedule[i];
    }
    
    public Object[] newArray(int i) {
        return this.newArray(i);
    }
}
