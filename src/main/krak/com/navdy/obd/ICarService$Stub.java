package com.navdy.obd;

abstract public class ICarService$Stub extends android.os.Binder implements com.navdy.obd.ICarService {
    final private static String DESCRIPTOR = "com.navdy.obd.ICarService";
    final static int TRANSACTION_addListener = 6;
    final static int TRANSACTION_applyConfiguration = 10;
    final static int TRANSACTION_getBatteryVoltage = 13;
    final static int TRANSACTION_getConnectionState = 1;
    final static int TRANSACTION_getCurrentConfigurationName = 9;
    final static int TRANSACTION_getEcus = 3;
    final static int TRANSACTION_getMode = 21;
    final static int TRANSACTION_getObdChipFirmwareVersion = 19;
    final static int TRANSACTION_getProtocol = 4;
    final static int TRANSACTION_getReadings = 5;
    final static int TRANSACTION_getSupportedPids = 2;
    final static int TRANSACTION_getTroubleCodes = 27;
    final static int TRANSACTION_getVIN = 8;
    final static int TRANSACTION_isCheckEngineLightOn = 26;
    final static int TRANSACTION_isObdPidsScanningEnabled = 15;
    final static int TRANSACTION_removeListener = 7;
    final static int TRANSACTION_rescan = 12;
    final static int TRANSACTION_setCANBusMonitoringListener = 23;
    final static int TRANSACTION_setMode = 22;
    final static int TRANSACTION_setObdPidsScanningEnabled = 14;
    final static int TRANSACTION_setVoltageSettings = 18;
    final static int TRANSACTION_sleep = 16;
    final static int TRANSACTION_startCanBusMonitoring = 24;
    final static int TRANSACTION_stopCanBusMonitoring = 25;
    final static int TRANSACTION_updateFirmware = 20;
    final static int TRANSACTION_updateScan = 11;
    final static int TRANSACTION_wakeup = 17;
    
    public ICarService$Stub() {
        this.attachInterface((android.os.IInterface)this, "com.navdy.obd.ICarService");
    }
    
    public static com.navdy.obd.ICarService asInterface(android.os.IBinder a) {
        Object a0 = null;
        label1: if (a != null) {
            android.os.IInterface a1 = a.queryLocalInterface("com.navdy.obd.ICarService");
            label0: {
                if (a1 == null) {
                    break label0;
                }
                if (!(a1 instanceof com.navdy.obd.ICarService)) {
                    break label0;
                }
                a0 = a1;
                break label1;
            }
            a0 = new com.navdy.obd.ICarService$Stub$Proxy(a);
        } else {
            a0 = null;
        }
        return (com.navdy.obd.ICarService)a0;
    }
    
    public android.os.IBinder asBinder() {
        return (android.os.IBinder)this;
    }
    
    public boolean onTransact(int i, android.os.Parcel a, android.os.Parcel a0, int i0) {
        boolean b = false;
        switch(i) {
            case 1598968902: {
                a0.writeString("com.navdy.obd.ICarService");
                b = true;
                break;
            }
            case 27: {
                a.enforceInterface("com.navdy.obd.ICarService");
                java.util.List a1 = this.getTroubleCodes();
                a0.writeNoException();
                a0.writeStringList(a1);
                b = true;
                break;
            }
            case 26: {
                a.enforceInterface("com.navdy.obd.ICarService");
                boolean b0 = this.isCheckEngineLightOn();
                a0.writeNoException();
                a0.writeInt(b0 ? 1 : 0);
                b = true;
                break;
            }
            case 25: {
                a.enforceInterface("com.navdy.obd.ICarService");
                this.stopCanBusMonitoring();
                a0.writeNoException();
                b = true;
                break;
            }
            case 24: {
                a.enforceInterface("com.navdy.obd.ICarService");
                this.startCanBusMonitoring();
                a0.writeNoException();
                b = true;
                break;
            }
            case 23: {
                a.enforceInterface("com.navdy.obd.ICarService");
                this.setCANBusMonitoringListener(com.navdy.obd.ICanBusMonitoringListener$Stub.asInterface(a.readStrongBinder()));
                a0.writeNoException();
                b = true;
                break;
            }
            case 22: {
                a.enforceInterface("com.navdy.obd.ICarService");
                this.setMode(a.readInt(), a.readInt() != 0);
                a0.writeNoException();
                b = true;
                break;
            }
            case 21: {
                a.enforceInterface("com.navdy.obd.ICarService");
                int i1 = this.getMode();
                a0.writeNoException();
                a0.writeInt(i1);
                b = true;
                break;
            }
            case 20: {
                a.enforceInterface("com.navdy.obd.ICarService");
                this.updateFirmware(a.readString(), a.readString());
                a0.writeNoException();
                b = true;
                break;
            }
            case 19: {
                a.enforceInterface("com.navdy.obd.ICarService");
                String s = this.getObdChipFirmwareVersion();
                a0.writeNoException();
                a0.writeString(s);
                b = true;
                break;
            }
            case 18: {
                a.enforceInterface("com.navdy.obd.ICarService");
                com.navdy.obd.VoltageSettings a2 = (a.readInt() == 0) ? null : (com.navdy.obd.VoltageSettings)com.navdy.obd.VoltageSettings.CREATOR.createFromParcel(a);
                this.setVoltageSettings(a2);
                a0.writeNoException();
                b = true;
                break;
            }
            case 17: {
                a.enforceInterface("com.navdy.obd.ICarService");
                this.wakeup();
                a0.writeNoException();
                b = true;
                break;
            }
            case 16: {
                a.enforceInterface("com.navdy.obd.ICarService");
                this.sleep(a.readInt() != 0);
                a0.writeNoException();
                b = true;
                break;
            }
            case 15: {
                a.enforceInterface("com.navdy.obd.ICarService");
                boolean b1 = this.isObdPidsScanningEnabled();
                a0.writeNoException();
                a0.writeInt(b1 ? 1 : 0);
                b = true;
                break;
            }
            case 14: {
                a.enforceInterface("com.navdy.obd.ICarService");
                this.setObdPidsScanningEnabled(a.readInt() != 0);
                a0.writeNoException();
                b = true;
                break;
            }
            case 13: {
                a.enforceInterface("com.navdy.obd.ICarService");
                double d = this.getBatteryVoltage();
                a0.writeNoException();
                a0.writeDouble(d);
                b = true;
                break;
            }
            case 12: {
                a.enforceInterface("com.navdy.obd.ICarService");
                this.rescan();
                a0.writeNoException();
                b = true;
                break;
            }
            case 11: {
                a.enforceInterface("com.navdy.obd.ICarService");
                com.navdy.obd.ScanSchedule a3 = (a.readInt() == 0) ? null : (com.navdy.obd.ScanSchedule)com.navdy.obd.ScanSchedule.CREATOR.createFromParcel(a);
                this.updateScan(a3, com.navdy.obd.IPidListener$Stub.asInterface(a.readStrongBinder()));
                a0.writeNoException();
                b = true;
                break;
            }
            case 10: {
                a.enforceInterface("com.navdy.obd.ICarService");
                boolean b2 = this.applyConfiguration(a.readString());
                a0.writeNoException();
                a0.writeInt(b2 ? 1 : 0);
                b = true;
                break;
            }
            case 9: {
                a.enforceInterface("com.navdy.obd.ICarService");
                String s0 = this.getCurrentConfigurationName();
                a0.writeNoException();
                a0.writeString(s0);
                b = true;
                break;
            }
            case 8: {
                a.enforceInterface("com.navdy.obd.ICarService");
                String s1 = this.getVIN();
                a0.writeNoException();
                a0.writeString(s1);
                b = true;
                break;
            }
            case 7: {
                a.enforceInterface("com.navdy.obd.ICarService");
                this.removeListener(com.navdy.obd.IPidListener$Stub.asInterface(a.readStrongBinder()));
                a0.writeNoException();
                b = true;
                break;
            }
            case 6: {
                a.enforceInterface("com.navdy.obd.ICarService");
                this.addListener((java.util.List)a.createTypedArrayList(com.navdy.obd.Pid.CREATOR), com.navdy.obd.IPidListener$Stub.asInterface(a.readStrongBinder()));
                a0.writeNoException();
                b = true;
                break;
            }
            case 5: {
                a.enforceInterface("com.navdy.obd.ICarService");
                java.util.List a4 = this.getReadings((java.util.List)a.createTypedArrayList(com.navdy.obd.Pid.CREATOR));
                a0.writeNoException();
                a0.writeTypedList(a4);
                b = true;
                break;
            }
            case 4: {
                a.enforceInterface("com.navdy.obd.ICarService");
                String s2 = this.getProtocol();
                a0.writeNoException();
                a0.writeString(s2);
                b = true;
                break;
            }
            case 3: {
                a.enforceInterface("com.navdy.obd.ICarService");
                java.util.List a5 = this.getEcus();
                a0.writeNoException();
                a0.writeTypedList(a5);
                b = true;
                break;
            }
            case 2: {
                a.enforceInterface("com.navdy.obd.ICarService");
                java.util.List a6 = this.getSupportedPids();
                a0.writeNoException();
                a0.writeTypedList(a6);
                b = true;
                break;
            }
            case 1: {
                a.enforceInterface("com.navdy.obd.ICarService");
                int i2 = this.getConnectionState();
                a0.writeNoException();
                a0.writeInt(i2);
                b = true;
                break;
            }
            default: {
                b = super.onTransact(i, a, a0, i0);
            }
        }
        return b;
    }
}
