package com.navdy.obd;

final class VehicleInfo$1 implements android.os.Parcelable$Creator {
    VehicleInfo$1() {
    }
    
    public com.navdy.obd.VehicleInfo createFromParcel(android.os.Parcel a) {
        return new com.navdy.obd.VehicleInfo(a);
    }
    
    public Object createFromParcel(android.os.Parcel a) {
        return this.createFromParcel(a);
    }
    
    public com.navdy.obd.VehicleInfo[] newArray(int i) {
        return new com.navdy.obd.VehicleInfo[i];
    }
    
    public Object[] newArray(int i) {
        return this.newArray(i);
    }
}
