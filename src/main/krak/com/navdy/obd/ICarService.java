package com.navdy.obd;

abstract public interface ICarService extends android.os.IInterface {
    abstract public void addListener(java.util.List arg, com.navdy.obd.IPidListener arg0);
    
    
    abstract public boolean applyConfiguration(String arg);
    
    
    abstract public double getBatteryVoltage();
    
    
    abstract public int getConnectionState();
    
    
    abstract public String getCurrentConfigurationName();
    
    
    abstract public java.util.List getEcus();
    
    
    abstract public int getMode();
    
    
    abstract public String getObdChipFirmwareVersion();
    
    
    abstract public String getProtocol();
    
    
    abstract public java.util.List getReadings(java.util.List arg);
    
    
    abstract public java.util.List getSupportedPids();
    
    
    abstract public java.util.List getTroubleCodes();
    
    
    abstract public String getVIN();
    
    
    abstract public boolean isCheckEngineLightOn();
    
    
    abstract public boolean isObdPidsScanningEnabled();
    
    
    abstract public void removeListener(com.navdy.obd.IPidListener arg);
    
    
    abstract public void rescan();
    
    
    abstract public void setCANBusMonitoringListener(com.navdy.obd.ICanBusMonitoringListener arg);
    
    
    abstract public void setMode(int arg, boolean arg0);
    
    
    abstract public void setObdPidsScanningEnabled(boolean arg);
    
    
    abstract public void setVoltageSettings(com.navdy.obd.VoltageSettings arg);
    
    
    abstract public void sleep(boolean arg);
    
    
    abstract public void startCanBusMonitoring();
    
    
    abstract public void stopCanBusMonitoring();
    
    
    abstract public void updateFirmware(String arg, String arg0);
    
    
    abstract public void updateScan(com.navdy.obd.ScanSchedule arg, com.navdy.obd.IPidListener arg0);
    
    
    abstract public void wakeup();
}
