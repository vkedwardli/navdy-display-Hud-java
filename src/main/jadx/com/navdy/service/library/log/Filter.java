package com.navdy.service.library.log;

public interface Filter {
    boolean matches(int i, String str, String str2);
}
