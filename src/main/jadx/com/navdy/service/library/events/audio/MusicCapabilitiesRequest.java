package com.navdy.service.library.events.audio;

import com.squareup.wire.Message;

public final class MusicCapabilitiesRequest extends Message {
    private static final long serialVersionUID = 0;

    public static final class Builder extends com.squareup.wire.Message.Builder<MusicCapabilitiesRequest> {
        public Builder(MusicCapabilitiesRequest message) {
            super(message);
        }

        public MusicCapabilitiesRequest build() {
            return new MusicCapabilitiesRequest();
        }
    }

    private MusicCapabilitiesRequest(Builder builder) {
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        return other instanceof MusicCapabilitiesRequest;
    }

    public int hashCode() {
        return 0;
    }
}
