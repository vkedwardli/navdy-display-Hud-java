package com.navdy.service.library.events.file;

import com.squareup.wire.Message;
import com.squareup.wire.Message.Datatype;
import com.squareup.wire.Message.Label;
import com.squareup.wire.ProtoField;
import okio.ByteString;

public final class FileTransferData extends Message {
    public static final Integer DEFAULT_CHUNKINDEX = Integer.valueOf(0);
    public static final ByteString DEFAULT_DATABYTES = ByteString.EMPTY;
    public static final String DEFAULT_FILECHECKSUM = "";
    public static final Boolean DEFAULT_LASTCHUNK = Boolean.valueOf(false);
    public static final Integer DEFAULT_TRANSFERID = Integer.valueOf(0);
    private static final long serialVersionUID = 0;
    @ProtoField(label = Label.REQUIRED, tag = 2, type = Datatype.INT32)
    public final Integer chunkIndex;
    @ProtoField(label = Label.REQUIRED, tag = 3, type = Datatype.BYTES)
    public final ByteString dataBytes;
    @ProtoField(tag = 5, type = Datatype.STRING)
    public final String fileCheckSum;
    @ProtoField(label = Label.REQUIRED, tag = 4, type = Datatype.BOOL)
    public final Boolean lastChunk;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.INT32)
    public final Integer transferId;

    public static final class Builder extends com.squareup.wire.Message.Builder<FileTransferData> {
        public Integer chunkIndex;
        public ByteString dataBytes;
        public String fileCheckSum;
        public Boolean lastChunk;
        public Integer transferId;

        public Builder(FileTransferData message) {
            super(message);
            if (message != null) {
                this.transferId = message.transferId;
                this.chunkIndex = message.chunkIndex;
                this.dataBytes = message.dataBytes;
                this.lastChunk = message.lastChunk;
                this.fileCheckSum = message.fileCheckSum;
            }
        }

        public Builder transferId(Integer transferId) {
            this.transferId = transferId;
            return this;
        }

        public Builder chunkIndex(Integer chunkIndex) {
            this.chunkIndex = chunkIndex;
            return this;
        }

        public Builder dataBytes(ByteString dataBytes) {
            this.dataBytes = dataBytes;
            return this;
        }

        public Builder lastChunk(Boolean lastChunk) {
            this.lastChunk = lastChunk;
            return this;
        }

        public Builder fileCheckSum(String fileCheckSum) {
            this.fileCheckSum = fileCheckSum;
            return this;
        }

        public FileTransferData build() {
            checkRequiredFields();
            return new FileTransferData();
        }
    }

    public FileTransferData(Integer transferId, Integer chunkIndex, ByteString dataBytes, Boolean lastChunk, String fileCheckSum) {
        this.transferId = transferId;
        this.chunkIndex = chunkIndex;
        this.dataBytes = dataBytes;
        this.lastChunk = lastChunk;
        this.fileCheckSum = fileCheckSum;
    }

    private FileTransferData(Builder builder) {
        this(builder.transferId, builder.chunkIndex, builder.dataBytes, builder.lastChunk, builder.fileCheckSum);
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof FileTransferData)) {
            return false;
        }
        FileTransferData o = (FileTransferData) other;
        if (equals( this.transferId,  o.transferId) && equals( this.chunkIndex,  o.chunkIndex) && equals( this.dataBytes,  o.dataBytes) && equals( this.lastChunk,  o.lastChunk) && equals( this.fileCheckSum,  o.fileCheckSum)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int i = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode;
        int hashCode2 = (this.transferId != null ? this.transferId.hashCode() : 0) * 37;
        if (this.chunkIndex != null) {
            hashCode = this.chunkIndex.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.dataBytes != null) {
            hashCode = this.dataBytes.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.lastChunk != null) {
            hashCode = this.lastChunk.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode = (hashCode2 + hashCode) * 37;
        if (this.fileCheckSum != null) {
            i = this.fileCheckSum.hashCode();
        }
        result = hashCode + i;
        this.hashCode = result;
        return result;
    }
}
