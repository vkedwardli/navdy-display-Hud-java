package com.navdy.service.library.events;

import com.navdy.service.library.log.Logger;
import java.io.IOException;
import java.io.OutputStream;

public class NavdyEventWriter {
    private static final Logger sLogger = new Logger(NavdyEventWriter.class);
    protected OutputStream mOutputStream;

    public NavdyEventWriter(OutputStream output) {
        if (output == null) {
            throw new IllegalArgumentException();
        }
        this.mOutputStream = output;
    }

    public void write(byte[] eventData) throws IOException {
        this.mOutputStream.write(new Frame(Integer.valueOf(eventData.length)).toByteArray());
        this.mOutputStream.write(eventData);
        this.mOutputStream.flush();
        if (sLogger.isLoggable(2)) {
            sLogger.v("[Outgoing-Event] size:" + eventData.length);
        }
    }
}
