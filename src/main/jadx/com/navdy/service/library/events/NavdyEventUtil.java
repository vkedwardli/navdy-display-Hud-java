package com.navdy.service.library.events;

import com.navdy.service.library.events.NavdyEvent.MessageType;
import com.navdy.service.library.log.Logger;
import com.squareup.wire.Extension;
import com.squareup.wire.Message;
import com.squareup.wire.Message.Builder;
import com.squareup.wire.Wire;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class NavdyEventUtil {
    private static final int EXTENSION_BASE = 101;
    private static HashMap<Class, Extension> classToExtensionMap = new HashMap();
    private static int minTag;
    private static final Logger sLogger = new Logger(NavdyEventUtil.class);
    private static Extension[] tagToExtensionMap = cacheExtensions();

    public static abstract class Initializer<T extends Message> {
        public abstract T build(Builder<T> builder);
    }

    private static Extension[] cacheExtensions() {
        Extension extension;
        List<Extension> list = new ArrayList();
        minTag = Integer.MAX_VALUE;
        int maxTag = -1;
        for (Field field : Ext_NavdyEvent.class.getDeclaredFields()) {
            if (field.getType().equals(Extension.class)) {
                try {
                    extension = (Extension) field.get(null);
                    classToExtensionMap.put(extension.getMessageType(), extension);
                    int tag = extension.getTag();
                    if (tag < minTag) {
                        minTag = tag;
                    }
                    if (tag > maxTag) {
                        maxTag = tag;
                    }
                    list.add(extension);
                } catch (IllegalAccessException e) {
                    sLogger.d("Skipping field " + field.getName());
                }
            }
        }
        Extension[] result = new Extension[((maxTag - minTag) + 1)];
        for (Extension extension2 : list) {
            result[extension2.getTag() - minTag] = extension2;
        }
        return result;
    }

    private static Extension findExtension(Message message) {
        return (Extension) classToExtensionMap.get(message.getClass());
    }

    private static Extension findExtension(NavdyEvent event) {
        if (event.type == null) {
            return null;
        }
        int offset = (event.type.ordinal() + 101) - minTag;
        if (offset < 0 || offset >= tagToExtensionMap.length) {
            return null;
        }
        return tagToExtensionMap[offset];
    }

    public static NavdyEvent eventFromMessage(Message message) {
        Extension extension = findExtension(message);
        if (extension == null) {
            sLogger.e("Not a valid NavdyEvent protobuf message: " + message);
            throw new IllegalArgumentException("Not a valid NavdyEvent protobuf message: " + message);
        }
        return new NavdyEvent.Builder().type((MessageType) Message.enumFromInt(MessageType.class, extension.getTag() - 100)).setExtension(extension,  message).build();
    }

    public static Message messageFromEvent(NavdyEvent event) {
        Extension extension = findExtension(event);
        if (extension != null) {
            return (Message) event.getExtension(extension);
        }
        sLogger.e("Unable to find extension for event: " + event);
        return null;
    }

    public static <T extends Message> T applyDefaults(T message) {
        return merge(message, getDefault(message.getClass()));
    }

    public static <T extends Message> T getDefault(Class<T> type) {
        return getDefault(type, null);
    }

    public static <T extends Message> T getDefault(Class<T> type, Initializer<T> initializer) {
        if (type == null) {
            return null;
        }
        Class builderType = null;
        Builder<T> msgBuilder = null;
        try {
            builderType = getBuilder(type);
            msgBuilder = (Builder) builderType.getConstructor(new Class[0]).newInstance(new Object[0]);
        } catch (ClassNotFoundException e) {
            sLogger.e(e.getMessage(), e);
        } catch (InvocationTargetException e2) {
            sLogger.e(e2.getMessage(), e2);
        } catch (NoSuchMethodException e3) {
            sLogger.e(e3.getMessage(), e3);
        } catch (InstantiationException e4) {
            sLogger.e(e4.getMessage(), e4);
        } catch (IllegalAccessException e5) {
            sLogger.e(e5.getMessage(), e5);
        }
        if (builderType == null || msgBuilder == null) {
            return null;
        }
        for (Field field : builderType.getDeclaredFields()) {
            Object resultingValue = null;
            try {
                if (Message.class.isAssignableFrom(field.getType())) {
                    resultingValue = getDefault(field.getType());
                } else if (!(Modifier.isFinal(field.getModifiers()) || field.getName().startsWith("$"))) {
                    resultingValue = type.getField("DEFAULT_" + field.getName().toUpperCase()).get(null);
                }
                if (resultingValue != null) {
                    field.set(msgBuilder, resultingValue);
                }
            } catch (IllegalAccessException e52) {
                sLogger.e(e52.getMessage(), e52);
            } catch (NoSuchFieldException e6) {
                sLogger.e(e6.getMessage(), e6);
            }
        }
        if (initializer != null) {
            return initializer.build(msgBuilder);
        }
        return msgBuilder.build();
    }

    public static <T extends Message> T merge(T msg, T defaultMsg) {
        if (msg == null || defaultMsg == null) {
            return null;
        }
        Builder<T> msgBuilder = null;
        Builder<T> defaultMsgBuilder = null;
        Class builderType = null;
        try {
            builderType = getBuilder(msg.getClass());
            msgBuilder = (Builder) builderType.getConstructor(new Class[]{type}).newInstance(new Object[]{msg});
            defaultMsgBuilder = (Builder) builderType.getConstructor(new Class[]{type}).newInstance(new Object[]{defaultMsg});
        } catch (InstantiationException e) {
            sLogger.e(e.getMessage(), e);
        } catch (IllegalAccessException e2) {
            sLogger.e(e2.getMessage(), e2);
        } catch (InvocationTargetException e3) {
            sLogger.e(e3.getMessage(), e3);
        } catch (NoSuchMethodException e4) {
            sLogger.e(e4.getMessage(), e4);
        } catch (ClassNotFoundException e5) {
            sLogger.e(e5.getMessage(), e5);
        }
        if (builderType == null || msgBuilder == null || defaultMsgBuilder == null) {
            return null;
        }
        for (Field field : builderType.getDeclaredFields()) {
            try {
                Object messageFieldValue = field.get(msgBuilder);
                Object defaultMessageFieldValue = field.get(defaultMsgBuilder);
                Object resultingValue = null;
                if (Message.class.isAssignableFrom(field.getType())) {
                    resultingValue = merge((Message) messageFieldValue, (Message) defaultMessageFieldValue);
                } else if (!Modifier.isFinal(field.getModifiers())) {
                    resultingValue = Wire.get(messageFieldValue, defaultMessageFieldValue);
                }
                if (resultingValue != null) {
                    field.set(msgBuilder, resultingValue);
                }
            } catch (IllegalAccessException e22) {
                sLogger.e(e22.getMessage(), e22);
            }
        }
        return msgBuilder.build();
    }

    private static <T extends Message> Class getBuilder(Class<T> type) throws ClassNotFoundException {
        if (type == null) {
            return null;
        }
        return Class.forName(type.getName() + "$Builder");
    }
}
