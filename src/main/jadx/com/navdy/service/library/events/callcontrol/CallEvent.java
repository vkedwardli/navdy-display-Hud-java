package com.navdy.service.library.events.callcontrol;

import com.squareup.wire.Message;
import com.squareup.wire.Message.Datatype;
import com.squareup.wire.Message.Label;
import com.squareup.wire.ProtoField;

public final class CallEvent extends Message {
    public static final Boolean DEFAULT_ANSWERED = Boolean.valueOf(false);
    public static final String DEFAULT_CONTACT_NAME = "";
    public static final Long DEFAULT_DURATION = Long.valueOf(0);
    public static final Boolean DEFAULT_INCOMING = Boolean.valueOf(false);
    public static final String DEFAULT_NUMBER = "";
    public static final Long DEFAULT_START_TIME = Long.valueOf(0);
    private static final long serialVersionUID = 0;
    @ProtoField(label = Label.REQUIRED, tag = 2, type = Datatype.BOOL)
    public final Boolean answered;
    @ProtoField(tag = 4, type = Datatype.STRING)
    public final String contact_name;
    @ProtoField(label = Label.REQUIRED, tag = 6, type = Datatype.INT64)
    public final Long duration;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.BOOL)
    public final Boolean incoming;
    @ProtoField(label = Label.REQUIRED, tag = 3, type = Datatype.STRING)
    public final String number;
    @ProtoField(label = Label.REQUIRED, tag = 5, type = Datatype.INT64)
    public final Long start_time;

    public static final class Builder extends com.squareup.wire.Message.Builder<CallEvent> {
        public Boolean answered;
        public String contact_name;
        public Long duration;
        public Boolean incoming;
        public String number;
        public Long start_time;

        public Builder(CallEvent message) {
            super(message);
            if (message != null) {
                this.incoming = message.incoming;
                this.answered = message.answered;
                this.number = message.number;
                this.contact_name = message.contact_name;
                this.start_time = message.start_time;
                this.duration = message.duration;
            }
        }

        public Builder incoming(Boolean incoming) {
            this.incoming = incoming;
            return this;
        }

        public Builder answered(Boolean answered) {
            this.answered = answered;
            return this;
        }

        public Builder number(String number) {
            this.number = number;
            return this;
        }

        public Builder contact_name(String contact_name) {
            this.contact_name = contact_name;
            return this;
        }

        public Builder start_time(Long start_time) {
            this.start_time = start_time;
            return this;
        }

        public Builder duration(Long duration) {
            this.duration = duration;
            return this;
        }

        public CallEvent build() {
            checkRequiredFields();
            return new CallEvent();
        }
    }

    public CallEvent(Boolean incoming, Boolean answered, String number, String contact_name, Long start_time, Long duration) {
        this.incoming = incoming;
        this.answered = answered;
        this.number = number;
        this.contact_name = contact_name;
        this.start_time = start_time;
        this.duration = duration;
    }

    private CallEvent(Builder builder) {
        this(builder.incoming, builder.answered, builder.number, builder.contact_name, builder.start_time, builder.duration);
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof CallEvent)) {
            return false;
        }
        CallEvent o = (CallEvent) other;
        if (equals( this.incoming,  o.incoming) && equals( this.answered,  o.answered) && equals( this.number,  o.number) && equals( this.contact_name,  o.contact_name) && equals( this.start_time,  o.start_time) && equals( this.duration,  o.duration)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int i = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode;
        int hashCode2 = (this.incoming != null ? this.incoming.hashCode() : 0) * 37;
        if (this.answered != null) {
            hashCode = this.answered.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.number != null) {
            hashCode = this.number.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.contact_name != null) {
            hashCode = this.contact_name.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.start_time != null) {
            hashCode = this.start_time.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode = (hashCode2 + hashCode) * 37;
        if (this.duration != null) {
            i = this.duration.hashCode();
        }
        result = hashCode + i;
        this.hashCode = result;
        return result;
    }
}
