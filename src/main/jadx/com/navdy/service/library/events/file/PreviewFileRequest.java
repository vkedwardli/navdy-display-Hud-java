package com.navdy.service.library.events.file;

import com.squareup.wire.Message;
import com.squareup.wire.Message.Datatype;
import com.squareup.wire.Message.Label;
import com.squareup.wire.ProtoField;

public final class PreviewFileRequest extends Message {
    public static final String DEFAULT_FILENAME = "";
    private static final long serialVersionUID = 0;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.STRING)
    public final String filename;

    public static final class Builder extends com.squareup.wire.Message.Builder<PreviewFileRequest> {
        public String filename;

        public Builder(PreviewFileRequest message) {
            super(message);
            if (message != null) {
                this.filename = message.filename;
            }
        }

        public Builder filename(String filename) {
            this.filename = filename;
            return this;
        }

        public PreviewFileRequest build() {
            checkRequiredFields();
            return new PreviewFileRequest();
        }
    }

    public PreviewFileRequest(String filename) {
        this.filename = filename;
    }

    private PreviewFileRequest(Builder builder) {
        this(builder.filename);
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (other instanceof PreviewFileRequest) {
            return equals( this.filename,  ((PreviewFileRequest) other).filename);
        }
        return false;
    }

    public int hashCode() {
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode = this.filename != null ? this.filename.hashCode() : 0;
        this.hashCode = hashCode;
        return hashCode;
    }
}
