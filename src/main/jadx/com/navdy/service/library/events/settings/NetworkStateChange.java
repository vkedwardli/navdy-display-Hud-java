package com.navdy.service.library.events.settings;

import com.squareup.wire.Message;
import com.squareup.wire.Message.Datatype;
import com.squareup.wire.ProtoField;

public final class NetworkStateChange extends Message {
    public static final Boolean DEFAULT_CELLNETWORK = Boolean.valueOf(false);
    public static final Boolean DEFAULT_NETWORKAVAILABLE = Boolean.valueOf(false);
    public static final Boolean DEFAULT_REACHABILITY = Boolean.valueOf(false);
    public static final Boolean DEFAULT_WIFINETWORK = Boolean.valueOf(false);
    private static final long serialVersionUID = 0;
    @ProtoField(tag = 2, type = Datatype.BOOL)
    public final Boolean cellNetwork;
    @ProtoField(tag = 1, type = Datatype.BOOL)
    public final Boolean networkAvailable;
    @ProtoField(tag = 4, type = Datatype.BOOL)
    public final Boolean reachability;
    @ProtoField(tag = 3, type = Datatype.BOOL)
    public final Boolean wifiNetwork;

    public static final class Builder extends com.squareup.wire.Message.Builder<NetworkStateChange> {
        public Boolean cellNetwork;
        public Boolean networkAvailable;
        public Boolean reachability;
        public Boolean wifiNetwork;

        public Builder(NetworkStateChange message) {
            super(message);
            if (message != null) {
                this.networkAvailable = message.networkAvailable;
                this.cellNetwork = message.cellNetwork;
                this.wifiNetwork = message.wifiNetwork;
                this.reachability = message.reachability;
            }
        }

        public Builder networkAvailable(Boolean networkAvailable) {
            this.networkAvailable = networkAvailable;
            return this;
        }

        public Builder cellNetwork(Boolean cellNetwork) {
            this.cellNetwork = cellNetwork;
            return this;
        }

        public Builder wifiNetwork(Boolean wifiNetwork) {
            this.wifiNetwork = wifiNetwork;
            return this;
        }

        public Builder reachability(Boolean reachability) {
            this.reachability = reachability;
            return this;
        }

        public NetworkStateChange build() {
            return new NetworkStateChange();
        }
    }

    public NetworkStateChange(Boolean networkAvailable, Boolean cellNetwork, Boolean wifiNetwork, Boolean reachability) {
        this.networkAvailable = networkAvailable;
        this.cellNetwork = cellNetwork;
        this.wifiNetwork = wifiNetwork;
        this.reachability = reachability;
    }

    private NetworkStateChange(Builder builder) {
        this(builder.networkAvailable, builder.cellNetwork, builder.wifiNetwork, builder.reachability);
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof NetworkStateChange)) {
            return false;
        }
        NetworkStateChange o = (NetworkStateChange) other;
        if (equals( this.networkAvailable,  o.networkAvailable) && equals( this.cellNetwork,  o.cellNetwork) && equals( this.wifiNetwork,  o.wifiNetwork) && equals( this.reachability,  o.reachability)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int i = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode;
        int hashCode2 = (this.networkAvailable != null ? this.networkAvailable.hashCode() : 0) * 37;
        if (this.cellNetwork != null) {
            hashCode = this.cellNetwork.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.wifiNetwork != null) {
            hashCode = this.wifiNetwork.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode = (hashCode2 + hashCode) * 37;
        if (this.reachability != null) {
            i = this.reachability.hashCode();
        }
        result = hashCode + i;
        this.hashCode = result;
        return result;
    }
}
