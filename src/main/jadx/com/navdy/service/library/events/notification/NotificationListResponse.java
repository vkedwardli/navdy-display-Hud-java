package com.navdy.service.library.events.notification;

import com.navdy.service.library.events.RequestStatus;
import com.squareup.wire.Message;
import com.squareup.wire.Message.Datatype;
import com.squareup.wire.Message.Label;
import com.squareup.wire.ProtoField;
import java.util.Collections;
import java.util.List;

public final class NotificationListResponse extends Message {
    public static final List<String> DEFAULT_IDS = Collections.emptyList();
    public static final RequestStatus DEFAULT_STATUS = RequestStatus.REQUEST_SUCCESS;
    public static final String DEFAULT_STATUS_DETAIL = "";
    private static final long serialVersionUID = 0;
    @ProtoField(label = Label.REPEATED, tag = 3, type = Datatype.STRING)
    public final List<String> ids;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.ENUM)
    public final RequestStatus status;
    @ProtoField(tag = 2, type = Datatype.STRING)
    public final String status_detail;

    public static final class Builder extends com.squareup.wire.Message.Builder<NotificationListResponse> {
        public List<String> ids;
        public RequestStatus status;
        public String status_detail;

        public Builder(NotificationListResponse message) {
            super(message);
            if (message != null) {
                this.status = message.status;
                this.status_detail = message.status_detail;
                this.ids = Message.copyOf(message.ids);
            }
        }

        public Builder status(RequestStatus status) {
            this.status = status;
            return this;
        }

        public Builder status_detail(String status_detail) {
            this.status_detail = status_detail;
            return this;
        }

        public Builder ids(List<String> ids) {
            this.ids = com.squareup.wire.Message.Builder.checkForNulls(ids);
            return this;
        }

        public NotificationListResponse build() {
            checkRequiredFields();
            return new NotificationListResponse();
        }
    }

    public NotificationListResponse(RequestStatus status, String status_detail, List<String> ids) {
        this.status = status;
        this.status_detail = status_detail;
        this.ids = Message.immutableCopyOf(ids);
    }

    private NotificationListResponse(Builder builder) {
        this(builder.status, builder.status_detail, builder.ids);
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof NotificationListResponse)) {
            return false;
        }
        NotificationListResponse o = (NotificationListResponse) other;
        if (equals( this.status,  o.status) && equals( this.status_detail,  o.status_detail) && equals(this.ids, o.ids)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int i = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode = (this.status != null ? this.status.hashCode() : 0) * 37;
        if (this.status_detail != null) {
            i = this.status_detail.hashCode();
        }
        result = ((hashCode + i) * 37) + (this.ids != null ? this.ids.hashCode() : 1);
        this.hashCode = result;
        return result;
    }
}
