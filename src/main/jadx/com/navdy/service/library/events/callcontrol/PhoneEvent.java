package com.navdy.service.library.events.callcontrol;

import com.squareup.wire.Message;
import com.squareup.wire.Message.Datatype;
import com.squareup.wire.Message.Label;
import com.squareup.wire.ProtoField;

public final class PhoneEvent extends Message {
    public static final String DEFAULT_CALLUUID = "";
    public static final String DEFAULT_CONTACT_NAME = "";
    public static final String DEFAULT_LABEL = "";
    public static final String DEFAULT_NUMBER = "";
    public static final PhoneStatus DEFAULT_STATUS = PhoneStatus.PHONE_IDLE;
    private static final long serialVersionUID = 0;
    @ProtoField(tag = 5, type = Datatype.STRING)
    public final String callUUID;
    @ProtoField(tag = 3, type = Datatype.STRING)
    public final String contact_name;
    @ProtoField(tag = 4, type = Datatype.STRING)
    public final String label;
    @ProtoField(tag = 2, type = Datatype.STRING)
    public final String number;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.ENUM)
    public final PhoneStatus status;

    public static final class Builder extends com.squareup.wire.Message.Builder<PhoneEvent> {
        public String callUUID;
        public String contact_name;
        public String label;
        public String number;
        public PhoneStatus status;

        public Builder(PhoneEvent message) {
            super(message);
            if (message != null) {
                this.status = message.status;
                this.number = message.number;
                this.contact_name = message.contact_name;
                this.label = message.label;
                this.callUUID = message.callUUID;
            }
        }

        public Builder status(PhoneStatus status) {
            this.status = status;
            return this;
        }

        public Builder number(String number) {
            this.number = number;
            return this;
        }

        public Builder contact_name(String contact_name) {
            this.contact_name = contact_name;
            return this;
        }

        public Builder label(String label) {
            this.label = label;
            return this;
        }

        public Builder callUUID(String callUUID) {
            this.callUUID = callUUID;
            return this;
        }

        public PhoneEvent build() {
            checkRequiredFields();
            return new PhoneEvent();
        }
    }

    public PhoneEvent(PhoneStatus status, String number, String contact_name, String label, String callUUID) {
        this.status = status;
        this.number = number;
        this.contact_name = contact_name;
        this.label = label;
        this.callUUID = callUUID;
    }

    private PhoneEvent(Builder builder) {
        this(builder.status, builder.number, builder.contact_name, builder.label, builder.callUUID);
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof PhoneEvent)) {
            return false;
        }
        PhoneEvent o = (PhoneEvent) other;
        if (equals( this.status,  o.status) && equals( this.number,  o.number) && equals( this.contact_name,  o.contact_name) && equals( this.label,  o.label) && equals( this.callUUID,  o.callUUID)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int i = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode;
        int hashCode2 = (this.status != null ? this.status.hashCode() : 0) * 37;
        if (this.number != null) {
            hashCode = this.number.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.contact_name != null) {
            hashCode = this.contact_name.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.label != null) {
            hashCode = this.label.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode = (hashCode2 + hashCode) * 37;
        if (this.callUUID != null) {
            i = this.callUUID.hashCode();
        }
        result = hashCode + i;
        this.hashCode = result;
        return result;
    }
}
