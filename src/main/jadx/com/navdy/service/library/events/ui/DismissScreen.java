package com.navdy.service.library.events.ui;

import com.squareup.wire.Message;
import com.squareup.wire.Message.Datatype;
import com.squareup.wire.Message.Label;
import com.squareup.wire.ProtoField;

public final class DismissScreen extends Message {
    public static final Screen DEFAULT_SCREEN = Screen.SCREEN_DASHBOARD;
    private static final long serialVersionUID = 0;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.ENUM)
    public final Screen screen;

    public static final class Builder extends com.squareup.wire.Message.Builder<DismissScreen> {
        public Screen screen;

        public Builder(DismissScreen message) {
            super(message);
            if (message != null) {
                this.screen = message.screen;
            }
        }

        public Builder screen(Screen screen) {
            this.screen = screen;
            return this;
        }

        public DismissScreen build() {
            checkRequiredFields();
            return new DismissScreen();
        }
    }

    public DismissScreen(Screen screen) {
        this.screen = screen;
    }

    private DismissScreen(Builder builder) {
        this(builder.screen);
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (other instanceof DismissScreen) {
            return equals( this.screen,  ((DismissScreen) other).screen);
        }
        return false;
    }

    public int hashCode() {
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode = this.screen != null ? this.screen.hashCode() : 0;
        this.hashCode = hashCode;
        return hashCode;
    }
}
