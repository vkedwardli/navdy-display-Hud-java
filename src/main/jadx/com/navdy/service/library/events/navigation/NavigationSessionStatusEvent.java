package com.navdy.service.library.events.navigation;

import com.squareup.wire.Message;
import com.squareup.wire.Message.Datatype;
import com.squareup.wire.Message.Label;
import com.squareup.wire.ProtoField;

public final class NavigationSessionStatusEvent extends Message {
    public static final String DEFAULT_DESTINATION_IDENTIFIER = "";
    public static final String DEFAULT_LABEL = "";
    public static final String DEFAULT_ROUTEID = "";
    public static final NavigationSessionState DEFAULT_SESSIONSTATE = NavigationSessionState.NAV_SESSION_ENGINE_NOT_READY;
    private static final long serialVersionUID = 0;
    @ProtoField(tag = 5, type = Datatype.STRING)
    public final String destination_identifier;
    @ProtoField(tag = 2, type = Datatype.STRING)
    public final String label;
    @ProtoField(tag = 4)
    public final NavigationRouteResult route;
    @ProtoField(tag = 3, type = Datatype.STRING)
    public final String routeId;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.ENUM)
    public final NavigationSessionState sessionState;

    public static final class Builder extends com.squareup.wire.Message.Builder<NavigationSessionStatusEvent> {
        public String destination_identifier;
        public String label;
        public NavigationRouteResult route;
        public String routeId;
        public NavigationSessionState sessionState;

        public Builder(NavigationSessionStatusEvent message) {
            super(message);
            if (message != null) {
                this.sessionState = message.sessionState;
                this.label = message.label;
                this.routeId = message.routeId;
                this.route = message.route;
                this.destination_identifier = message.destination_identifier;
            }
        }

        public Builder sessionState(NavigationSessionState sessionState) {
            this.sessionState = sessionState;
            return this;
        }

        public Builder label(String label) {
            this.label = label;
            return this;
        }

        public Builder routeId(String routeId) {
            this.routeId = routeId;
            return this;
        }

        public Builder route(NavigationRouteResult route) {
            this.route = route;
            return this;
        }

        public Builder destination_identifier(String destination_identifier) {
            this.destination_identifier = destination_identifier;
            return this;
        }

        public NavigationSessionStatusEvent build() {
            checkRequiredFields();
            return new NavigationSessionStatusEvent();
        }
    }

    public NavigationSessionStatusEvent(NavigationSessionState sessionState, String label, String routeId, NavigationRouteResult route, String destination_identifier) {
        this.sessionState = sessionState;
        this.label = label;
        this.routeId = routeId;
        this.route = route;
        this.destination_identifier = destination_identifier;
    }

    private NavigationSessionStatusEvent(Builder builder) {
        this(builder.sessionState, builder.label, builder.routeId, builder.route, builder.destination_identifier);
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof NavigationSessionStatusEvent)) {
            return false;
        }
        NavigationSessionStatusEvent o = (NavigationSessionStatusEvent) other;
        if (equals( this.sessionState,  o.sessionState) && equals( this.label,  o.label) && equals( this.routeId,  o.routeId) && equals( this.route,  o.route) && equals( this.destination_identifier,  o.destination_identifier)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int i = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode;
        int hashCode2 = (this.sessionState != null ? this.sessionState.hashCode() : 0) * 37;
        if (this.label != null) {
            hashCode = this.label.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.routeId != null) {
            hashCode = this.routeId.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.route != null) {
            hashCode = this.route.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode = (hashCode2 + hashCode) * 37;
        if (this.destination_identifier != null) {
            i = this.destination_identifier.hashCode();
        }
        result = hashCode + i;
        this.hashCode = result;
        return result;
    }
}
