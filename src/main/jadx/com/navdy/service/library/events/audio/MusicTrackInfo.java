package com.navdy.service.library.events.audio;

import com.squareup.wire.Message;
import com.squareup.wire.Message.Datatype;
import com.squareup.wire.Message.Label;
import com.squareup.wire.ProtoField;

public final class MusicTrackInfo extends Message {
    public static final String DEFAULT_ALBUM = "";
    public static final String DEFAULT_AUTHOR = "";
    public static final String DEFAULT_COLLECTIONID = "";
    public static final MusicCollectionSource DEFAULT_COLLECTIONSOURCE = MusicCollectionSource.COLLECTION_SOURCE_UNKNOWN;
    public static final MusicCollectionType DEFAULT_COLLECTIONTYPE = MusicCollectionType.COLLECTION_TYPE_UNKNOWN;
    public static final Integer DEFAULT_CURRENTPOSITION = Integer.valueOf(0);
    public static final MusicDataSource DEFAULT_DATASOURCE = MusicDataSource.MUSIC_SOURCE_NONE;
    public static final Integer DEFAULT_DURATION = Integer.valueOf(0);
    public static final Long DEFAULT_INDEX = Long.valueOf(0);
    public static final Boolean DEFAULT_ISNEXTALLOWED = Boolean.valueOf(false);
    public static final Boolean DEFAULT_ISONLINESTREAM = Boolean.valueOf(false);
    public static final Boolean DEFAULT_ISPREVIOUSALLOWED = Boolean.valueOf(false);
    public static final String DEFAULT_NAME = "";
    public static final MusicPlaybackState DEFAULT_PLAYBACKSTATE = MusicPlaybackState.PLAYBACK_NONE;
    public static final Integer DEFAULT_PLAYCOUNT = Integer.valueOf(0);
    public static final MusicRepeatMode DEFAULT_REPEATMODE = MusicRepeatMode.MUSIC_REPEAT_MODE_UNKNOWN;
    public static final MusicShuffleMode DEFAULT_SHUFFLEMODE = MusicShuffleMode.MUSIC_SHUFFLE_MODE_UNKNOWN;
    public static final String DEFAULT_TRACKID = "";
    private static final long serialVersionUID = 0;
    @ProtoField(tag = 5, type = Datatype.STRING)
    public final String album;
    @ProtoField(tag = 4, type = Datatype.STRING)
    public final String author;
    @ProtoField(tag = 14, type = Datatype.STRING)
    public final String collectionId;
    @ProtoField(tag = 12, type = Datatype.ENUM)
    public final MusicCollectionSource collectionSource;
    @ProtoField(tag = 13, type = Datatype.ENUM)
    public final MusicCollectionType collectionType;
    @ProtoField(tag = 7, type = Datatype.INT32)
    public final Integer currentPosition;
    @ProtoField(tag = 10, type = Datatype.ENUM)
    public final MusicDataSource dataSource;
    @ProtoField(tag = 6, type = Datatype.INT32)
    public final Integer duration;
    @ProtoField(tag = 2, type = Datatype.INT64)
    public final Long index;
    @ProtoField(tag = 9, type = Datatype.BOOL)
    public final Boolean isNextAllowed;
    @ProtoField(tag = 11, type = Datatype.BOOL)
    public final Boolean isOnlineStream;
    @ProtoField(tag = 8, type = Datatype.BOOL)
    public final Boolean isPreviousAllowed;
    @ProtoField(tag = 3, type = Datatype.STRING)
    public final String name;
    @ProtoField(tag = 16, type = Datatype.INT32)
    public final Integer playCount;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.ENUM)
    public final MusicPlaybackState playbackState;
    @ProtoField(tag = 18, type = Datatype.ENUM)
    public final MusicRepeatMode repeatMode;
    @ProtoField(tag = 17, type = Datatype.ENUM)
    public final MusicShuffleMode shuffleMode;
    @ProtoField(tag = 15, type = Datatype.STRING)
    public final String trackId;

    public static final class Builder extends com.squareup.wire.Message.Builder<MusicTrackInfo> {
        public String album;
        public String author;
        public String collectionId;
        public MusicCollectionSource collectionSource;
        public MusicCollectionType collectionType;
        public Integer currentPosition;
        public MusicDataSource dataSource;
        public Integer duration;
        public Long index;
        public Boolean isNextAllowed;
        public Boolean isOnlineStream;
        public Boolean isPreviousAllowed;
        public String name;
        public Integer playCount;
        public MusicPlaybackState playbackState;
        public MusicRepeatMode repeatMode;
        public MusicShuffleMode shuffleMode;
        public String trackId;

        public Builder(MusicTrackInfo message) {
            super(message);
            if (message != null) {
                this.playbackState = message.playbackState;
                this.index = message.index;
                this.name = message.name;
                this.author = message.author;
                this.album = message.album;
                this.duration = message.duration;
                this.currentPosition = message.currentPosition;
                this.isPreviousAllowed = message.isPreviousAllowed;
                this.isNextAllowed = message.isNextAllowed;
                this.dataSource = message.dataSource;
                this.isOnlineStream = message.isOnlineStream;
                this.collectionSource = message.collectionSource;
                this.collectionType = message.collectionType;
                this.collectionId = message.collectionId;
                this.trackId = message.trackId;
                this.playCount = message.playCount;
                this.shuffleMode = message.shuffleMode;
                this.repeatMode = message.repeatMode;
            }
        }

        public Builder playbackState(MusicPlaybackState playbackState) {
            this.playbackState = playbackState;
            return this;
        }

        public Builder index(Long index) {
            this.index = index;
            return this;
        }

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        public Builder author(String author) {
            this.author = author;
            return this;
        }

        public Builder album(String album) {
            this.album = album;
            return this;
        }

        public Builder duration(Integer duration) {
            this.duration = duration;
            return this;
        }

        public Builder currentPosition(Integer currentPosition) {
            this.currentPosition = currentPosition;
            return this;
        }

        public Builder isPreviousAllowed(Boolean isPreviousAllowed) {
            this.isPreviousAllowed = isPreviousAllowed;
            return this;
        }

        public Builder isNextAllowed(Boolean isNextAllowed) {
            this.isNextAllowed = isNextAllowed;
            return this;
        }

        public Builder dataSource(MusicDataSource dataSource) {
            this.dataSource = dataSource;
            return this;
        }

        public Builder isOnlineStream(Boolean isOnlineStream) {
            this.isOnlineStream = isOnlineStream;
            return this;
        }

        public Builder collectionSource(MusicCollectionSource collectionSource) {
            this.collectionSource = collectionSource;
            return this;
        }

        public Builder collectionType(MusicCollectionType collectionType) {
            this.collectionType = collectionType;
            return this;
        }

        public Builder collectionId(String collectionId) {
            this.collectionId = collectionId;
            return this;
        }

        public Builder trackId(String trackId) {
            this.trackId = trackId;
            return this;
        }

        public Builder playCount(Integer playCount) {
            this.playCount = playCount;
            return this;
        }

        public Builder shuffleMode(MusicShuffleMode shuffleMode) {
            this.shuffleMode = shuffleMode;
            return this;
        }

        public Builder repeatMode(MusicRepeatMode repeatMode) {
            this.repeatMode = repeatMode;
            return this;
        }

        public MusicTrackInfo build() {
            checkRequiredFields();
            return new MusicTrackInfo();
        }
    }

    public MusicTrackInfo(MusicPlaybackState playbackState, Long index, String name, String author, String album, Integer duration, Integer currentPosition, Boolean isPreviousAllowed, Boolean isNextAllowed, MusicDataSource dataSource, Boolean isOnlineStream, MusicCollectionSource collectionSource, MusicCollectionType collectionType, String collectionId, String trackId, Integer playCount, MusicShuffleMode shuffleMode, MusicRepeatMode repeatMode) {
        this.playbackState = playbackState;
        this.index = index;
        this.name = name;
        this.author = author;
        this.album = album;
        this.duration = duration;
        this.currentPosition = currentPosition;
        this.isPreviousAllowed = isPreviousAllowed;
        this.isNextAllowed = isNextAllowed;
        this.dataSource = dataSource;
        this.isOnlineStream = isOnlineStream;
        this.collectionSource = collectionSource;
        this.collectionType = collectionType;
        this.collectionId = collectionId;
        this.trackId = trackId;
        this.playCount = playCount;
        this.shuffleMode = shuffleMode;
        this.repeatMode = repeatMode;
    }

    private MusicTrackInfo(Builder builder) {
        this(builder.playbackState, builder.index, builder.name, builder.author, builder.album, builder.duration, builder.currentPosition, builder.isPreviousAllowed, builder.isNextAllowed, builder.dataSource, builder.isOnlineStream, builder.collectionSource, builder.collectionType, builder.collectionId, builder.trackId, builder.playCount, builder.shuffleMode, builder.repeatMode);
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof MusicTrackInfo)) {
            return false;
        }
        MusicTrackInfo o = (MusicTrackInfo) other;
        if (equals( this.playbackState,  o.playbackState) && equals( this.index,  o.index) && equals( this.name,  o.name) && equals( this.author,  o.author) && equals( this.album,  o.album) && equals( this.duration,  o.duration) && equals( this.currentPosition,  o.currentPosition) && equals( this.isPreviousAllowed,  o.isPreviousAllowed) && equals( this.isNextAllowed,  o.isNextAllowed) && equals( this.dataSource,  o.dataSource) && equals( this.isOnlineStream,  o.isOnlineStream) && equals( this.collectionSource,  o.collectionSource) && equals( this.collectionType,  o.collectionType) && equals( this.collectionId,  o.collectionId) && equals( this.trackId,  o.trackId) && equals( this.playCount,  o.playCount) && equals( this.shuffleMode,  o.shuffleMode) && equals( this.repeatMode,  o.repeatMode)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int i = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode;
        int hashCode2 = (this.playbackState != null ? this.playbackState.hashCode() : 0) * 37;
        if (this.index != null) {
            hashCode = this.index.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.name != null) {
            hashCode = this.name.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.author != null) {
            hashCode = this.author.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.album != null) {
            hashCode = this.album.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.duration != null) {
            hashCode = this.duration.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.currentPosition != null) {
            hashCode = this.currentPosition.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.isPreviousAllowed != null) {
            hashCode = this.isPreviousAllowed.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.isNextAllowed != null) {
            hashCode = this.isNextAllowed.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.dataSource != null) {
            hashCode = this.dataSource.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.isOnlineStream != null) {
            hashCode = this.isOnlineStream.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.collectionSource != null) {
            hashCode = this.collectionSource.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.collectionType != null) {
            hashCode = this.collectionType.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.collectionId != null) {
            hashCode = this.collectionId.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.trackId != null) {
            hashCode = this.trackId.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.playCount != null) {
            hashCode = this.playCount.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.shuffleMode != null) {
            hashCode = this.shuffleMode.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode = (hashCode2 + hashCode) * 37;
        if (this.repeatMode != null) {
            i = this.repeatMode.hashCode();
        }
        result = hashCode + i;
        this.hashCode = result;
        return result;
    }
}
