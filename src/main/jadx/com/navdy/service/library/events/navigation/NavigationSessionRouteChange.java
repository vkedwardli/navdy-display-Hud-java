package com.navdy.service.library.events.navigation;

import com.squareup.wire.Message;
import com.squareup.wire.Message.Datatype;
import com.squareup.wire.ProtoEnum;
import com.squareup.wire.ProtoField;

public final class NavigationSessionRouteChange extends Message {
    public static final String DEFAULT_OLDROUTEID = "";
    public static final RerouteReason DEFAULT_REASON = RerouteReason.NAV_SESSION_OFF_REROUTE;
    private static final long serialVersionUID = 0;
    @ProtoField(tag = 2)
    public final NavigationRouteResult newRoute;
    @ProtoField(tag = 1, type = Datatype.STRING)
    public final String oldRouteId;
    @ProtoField(tag = 3, type = Datatype.ENUM)
    public final RerouteReason reason;

    public static final class Builder extends com.squareup.wire.Message.Builder<NavigationSessionRouteChange> {
        public NavigationRouteResult newRoute;
        public String oldRouteId;
        public RerouteReason reason;

        public Builder(NavigationSessionRouteChange message) {
            super(message);
            if (message != null) {
                this.oldRouteId = message.oldRouteId;
                this.newRoute = message.newRoute;
                this.reason = message.reason;
            }
        }

        public Builder oldRouteId(String oldRouteId) {
            this.oldRouteId = oldRouteId;
            return this;
        }

        public Builder newRoute(NavigationRouteResult newRoute) {
            this.newRoute = newRoute;
            return this;
        }

        public Builder reason(RerouteReason reason) {
            this.reason = reason;
            return this;
        }

        public NavigationSessionRouteChange build() {
            return new NavigationSessionRouteChange();
        }
    }

    public enum RerouteReason implements ProtoEnum {
        NAV_SESSION_OFF_REROUTE(1),
        NAV_SESSION_TRAFFIC_REROUTE(2),
        NAV_SESSION_ARRIVAL_REROUTE(3),
        NAV_SESSION_FUEL_REROUTE(4),
        NAV_SESSION_ROUTE_PICKER(5);
        
        private final int value;

        private RerouteReason(int value) {
            this.value = value;
        }

        public int getValue() {
            return this.value;
        }
    }

    public NavigationSessionRouteChange(String oldRouteId, NavigationRouteResult newRoute, RerouteReason reason) {
        this.oldRouteId = oldRouteId;
        this.newRoute = newRoute;
        this.reason = reason;
    }

    private NavigationSessionRouteChange(Builder builder) {
        this(builder.oldRouteId, builder.newRoute, builder.reason);
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof NavigationSessionRouteChange)) {
            return false;
        }
        NavigationSessionRouteChange o = (NavigationSessionRouteChange) other;
        if (equals( this.oldRouteId,  o.oldRouteId) && equals( this.newRoute,  o.newRoute) && equals( this.reason,  o.reason)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int i = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode;
        int hashCode2 = (this.oldRouteId != null ? this.oldRouteId.hashCode() : 0) * 37;
        if (this.newRoute != null) {
            hashCode = this.newRoute.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode = (hashCode2 + hashCode) * 37;
        if (this.reason != null) {
            i = this.reason.hashCode();
        }
        result = hashCode + i;
        this.hashCode = result;
        return result;
    }
}
