package com.navdy.service.library.events.audio;

import com.squareup.wire.ProtoEnum;

public enum MusicRepeatMode implements ProtoEnum {
    MUSIC_REPEAT_MODE_UNKNOWN(1),
    MUSIC_REPEAT_MODE_OFF(2),
    MUSIC_REPEAT_MODE_ONE(3),
    MUSIC_REPEAT_MODE_ALL(4);
    
    private final int value;

    private MusicRepeatMode(int value) {
        this.value = value;
    }

    public int getValue() {
        return this.value;
    }
}
