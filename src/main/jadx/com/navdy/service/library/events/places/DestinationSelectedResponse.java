package com.navdy.service.library.events.places;

import com.navdy.service.library.events.RequestStatus;
import com.navdy.service.library.events.destination.Destination;
import com.squareup.wire.Message;
import com.squareup.wire.Message.Datatype;
import com.squareup.wire.ProtoField;

public final class DestinationSelectedResponse extends Message {
    public static final String DEFAULT_REQUEST_ID = "";
    public static final RequestStatus DEFAULT_REQUEST_STATUS = RequestStatus.REQUEST_SUCCESS;
    private static final long serialVersionUID = 0;
    @ProtoField(tag = 3)
    public final Destination destination;
    @ProtoField(tag = 1, type = Datatype.STRING)
    public final String request_id;
    @ProtoField(tag = 2, type = Datatype.ENUM)
    public final RequestStatus request_status;

    public static final class Builder extends com.squareup.wire.Message.Builder<DestinationSelectedResponse> {
        public Destination destination;
        public String request_id;
        public RequestStatus request_status;

        public Builder(DestinationSelectedResponse message) {
            super(message);
            if (message != null) {
                this.request_id = message.request_id;
                this.request_status = message.request_status;
                this.destination = message.destination;
            }
        }

        public Builder request_id(String request_id) {
            this.request_id = request_id;
            return this;
        }

        public Builder request_status(RequestStatus request_status) {
            this.request_status = request_status;
            return this;
        }

        public Builder destination(Destination destination) {
            this.destination = destination;
            return this;
        }

        public DestinationSelectedResponse build() {
            return new DestinationSelectedResponse();
        }
    }

    public DestinationSelectedResponse(String request_id, RequestStatus request_status, Destination destination) {
        this.request_id = request_id;
        this.request_status = request_status;
        this.destination = destination;
    }

    private DestinationSelectedResponse(Builder builder) {
        this(builder.request_id, builder.request_status, builder.destination);
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof DestinationSelectedResponse)) {
            return false;
        }
        DestinationSelectedResponse o = (DestinationSelectedResponse) other;
        if (equals( this.request_id,  o.request_id) && equals( this.request_status,  o.request_status) && equals( this.destination,  o.destination)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int i = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode;
        int hashCode2 = (this.request_id != null ? this.request_id.hashCode() : 0) * 37;
        if (this.request_status != null) {
            hashCode = this.request_status.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode = (hashCode2 + hashCode) * 37;
        if (this.destination != null) {
            i = this.destination.hashCode();
        }
        result = hashCode + i;
        this.hashCode = result;
        return result;
    }
}
