package com.navdy.service.library.device.discovery;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import com.navdy.service.library.device.NavdyDeviceId;
import com.navdy.service.library.device.NavdyDeviceId.Type;
import com.navdy.service.library.device.connection.BTConnectionInfo;
import com.navdy.service.library.device.connection.ConnectionService;
import com.navdy.service.library.device.connection.ConnectionType;
import com.navdy.service.library.device.connection.ServiceAddress;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.task.TaskManager;
import java.util.HashSet;
import java.util.Set;

public class BTRemoteDeviceScanner extends RemoteDeviceScanner {
    private static final Logger sLogger = new Logger(BTRemoteDeviceScanner.class);
    private BluetoothAdapter btAdapter;
    private Set<String> devicesSeen;
    private Runnable handleStartScan = new Runnable() {
        public void run() {
            if (!BTRemoteDeviceScanner.this.scanning) {
                BTRemoteDeviceScanner.this.scanning = true;
                IntentFilter filter = new IntentFilter();
                filter.addAction("android.bluetooth.device.action.FOUND");
                filter.addAction("android.bluetooth.device.action.UUID");
                filter.addAction("android.bluetooth.device.action.NAME_CHANGED");
                filter.addAction("android.bluetooth.adapter.action.DISCOVERY_FINISHED");
                BTRemoteDeviceScanner.this.mContext.registerReceiver(BTRemoteDeviceScanner.this.mReceiver, filter);
                BTRemoteDeviceScanner.this.devicesSeen = new HashSet();
                if (BTRemoteDeviceScanner.this.btAdapter != null) {
                    if (BTRemoteDeviceScanner.this.btAdapter.isDiscovering()) {
                        BTRemoteDeviceScanner.this.btAdapter.cancelDiscovery();
                    }
                    BTRemoteDeviceScanner.this.btAdapter.startDiscovery();
                    BTRemoteDeviceScanner.this.dispatchOnScanStarted();
                }
            }
        }
    };
    private Runnable handleStopScan = new Runnable() {
        public void run() {
            if (BTRemoteDeviceScanner.this.scanning) {
                BTRemoteDeviceScanner.this.scanning = false;
                if (BTRemoteDeviceScanner.this.btAdapter != null) {
                    BTRemoteDeviceScanner.this.btAdapter.cancelDiscovery();
                }
                BTRemoteDeviceScanner.this.mContext.unregisterReceiver(BTRemoteDeviceScanner.this.mReceiver);
            }
        }
    };
    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if ("android.bluetooth.device.action.FOUND".equals(action) || "android.bluetooth.device.action.NAME_CHANGED".equals(action)) {
                BluetoothDevice device = (BluetoothDevice) intent.getParcelableExtra("android.bluetooth.device.extra.DEVICE");
                String name = intent.getStringExtra("android.bluetooth.device.extra.NAME");
                String address = device.getAddress();
                BTRemoteDeviceScanner.sLogger.d("Scanned: " + address + " - name = " + name);
                if (name != null && BTDeviceBroadcaster.isDisplay(name) && !BTRemoteDeviceScanner.this.devicesSeen.contains(address)) {
                    BTRemoteDeviceScanner.this.devicesSeen.add(address);
                    BTRemoteDeviceScanner.this.dispatchOnDiscovered(new BTConnectionInfo(new NavdyDeviceId(Type.BT, address, name), new ServiceAddress(address, ConnectionService.NAVDY_PROTO_SERVICE_UUID.toString()), ConnectionType.BT_PROTOBUF));
                }
            } else if ("android.bluetooth.adapter.action.DISCOVERY_FINISHED".equals(action)) {
                BTRemoteDeviceScanner.this.dispatchOnScanStopped();
            }
        }
    };
    private boolean scanning = false;
    private int taskQueue;

    public BTRemoteDeviceScanner(Context context, int serialTaskQueue) {
        super(context);
        this.taskQueue = serialTaskQueue;
        this.btAdapter = BluetoothAdapter.getDefaultAdapter();
    }

    public boolean startScan() {
        TaskManager.getInstance().execute(this.handleStartScan, this.taskQueue);
        return true;
    }

    public boolean stopScan() {
        TaskManager.getInstance().execute(this.handleStopScan, this.taskQueue);
        return true;
    }
}
