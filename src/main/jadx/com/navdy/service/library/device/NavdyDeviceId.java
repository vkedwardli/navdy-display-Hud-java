package com.navdy.service.library.device;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.provider.Settings.Secure;
import android.text.TextUtils;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import java.io.IOException;
import java.util.Locale;

@JsonAdapter(Adapter.class)
public class NavdyDeviceId {
    protected static final String ATTRIBUTE_NAME = "N";
    protected static final String ATTRIBUTE_SEPARATOR = ";";
    protected static final String RESERVED_CHARACTER_REGEX = ";|/";
    public static final NavdyDeviceId UNKNOWN_ID = new NavdyDeviceId("UNK/FF:FF:FF:FF:FF:FF;N/Unknown");
    protected static final String VALUE_SEPARATOR = "/";
    static NavdyDeviceId sThisDeviceId;
    protected String mDeviceId;
    protected Type mDeviceIdType;
    protected String mDeviceName;
    protected String mSerializedDeviceId;

    public static class Adapter extends TypeAdapter {
        public void write(JsonWriter out, Object value) throws IOException {
            out.value(value.toString());
        }

        public Object read(JsonReader in) throws IOException {
            return new NavdyDeviceId(in.nextString());
        }
    }

    public enum Type {
        UNK,
        BT,
        EMU,
        EA
    }

    public NavdyDeviceId(String serializedDeviceId) {
        if (serializedDeviceId == null || serializedDeviceId.equals("")) {
            throw new IllegalArgumentException("Cannot create empty NavdyDeviceId");
        } else if (parseDeviceIdString(serializedDeviceId)) {
            this.mSerializedDeviceId = createSerializedDeviceId();
        } else {
            throw new IllegalArgumentException("Invalid device ID string.");
        }
    }

    public NavdyDeviceId(Type type, String id, String deviceName) {
        this.mDeviceIdType = type;
        this.mDeviceId = normalizeDeviceIdString(id);
        this.mDeviceName = normalizeDeviceName(deviceName);
        this.mSerializedDeviceId = createSerializedDeviceId();
    }

    public NavdyDeviceId(BluetoothDevice device) {
        this(Type.BT, device.getAddress(), TextUtils.isEmpty(device.getName()) ? "unknown" : device.getName());
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        NavdyDeviceId deviceId = (NavdyDeviceId) o;
        if (this.mDeviceId.equals(deviceId.mDeviceId) && this.mDeviceIdType == deviceId.mDeviceIdType) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        return (this.mDeviceId.hashCode() * 31) + this.mDeviceIdType.hashCode();
    }

    public String toString() {
        return this.mSerializedDeviceId;
    }

    private String createSerializedDeviceId() {
        String deviceId = this.mDeviceIdType.toString() + "/" + this.mDeviceId;
        if (TextUtils.isEmpty(this.mDeviceName)) {
            return deviceId;
        }
        return deviceId + ";N/" + this.mDeviceName;
    }

    protected String normalizeDeviceIdString(String deviceId) {
        return deviceId.toUpperCase(Locale.US);
    }

    protected String normalizeDeviceName(String deviceName) {
        if (deviceName == null) {
            return null;
        }
        String name = deviceName.replaceAll(RESERVED_CHARACTER_REGEX, "");
        if (name.equals("")) {
            return null;
        }
        return name;
    }

    protected boolean parseDeviceIdString(String deviceIdString) {
        String[] attributes = deviceIdString.split(ATTRIBUTE_SEPARATOR);
        if (attributes.length == 0) {
            return false;
        }
        for (String unparsedAttribute : attributes) {
            String[] idParts = unparsedAttribute.split("/");
            if (idParts.length < 2) {
                return false;
            }
            String attributeText = idParts[0];
            String valueText = idParts[1];
            if (attributeText.equals("N")) {
                this.mDeviceName = normalizeDeviceName(valueText);
            } else {
                try {
                    this.mDeviceIdType = Type.valueOf(idParts[0]);
                    this.mDeviceId = normalizeDeviceIdString(idParts[1]);
                } catch (IllegalArgumentException e) {
                    throw new IllegalArgumentException("Invalid device ID type");
                }
            }
        }
        return true;
    }

    public String getBluetoothAddress() {
        if (this.mDeviceIdType == Type.BT || this.mDeviceIdType == Type.EA) {
            return this.mDeviceId;
        }
        return null;
    }

    public String getDisplayName() {
        return TextUtils.isEmpty(this.mDeviceName) ? toString() : this.mDeviceName;
    }

    public String getDeviceName() {
        return this.mDeviceName;
    }

    public static NavdyDeviceId getThisDevice(Context context) {
        if (sThisDeviceId == null || sThisDeviceId.equals(UNKNOWN_ID)) {
            BluetoothAdapter btAdapter = BluetoothAdapter.getDefaultAdapter();
            if (btAdapter != null) {
                String btAddr = btAdapter.getAddress();
                if (TextUtils.isEmpty(btAddr)) {
                    sThisDeviceId = UNKNOWN_ID;
                } else {
                    sThisDeviceId = new NavdyDeviceId(Type.BT, btAddr, btAdapter.getName());
                }
            } else if (context != null) {
                String androidId = Secure.getString(context.getContentResolver(), "android_id");
                if (TextUtils.isEmpty(androidId)) {
                    sThisDeviceId = UNKNOWN_ID;
                } else {
                    sThisDeviceId = new NavdyDeviceId(Type.EMU, androidId, "emulator");
                }
            }
        }
        return sThisDeviceId;
    }
}
