package com.navdy.service.library.device.link;

import com.navdy.service.library.device.RemoteDevice.PostEventStatus;
import com.navdy.service.library.events.NavdyEventWriter;
import com.navdy.service.library.util.IOUtils;
import java.io.IOException;
import java.io.OutputStream;
import java.util.concurrent.LinkedBlockingDeque;

public class WriterThread extends IOThread {
    private final EventProcessor mEventProcessor;
    private final LinkedBlockingDeque<EventRequest> mEventQueue;
    private final NavdyEventWriter mmEventWriter = new NavdyEventWriter(this.mmOutStream);
    private OutputStream mmOutStream;

    public interface EventProcessor {
        byte[] processEvent(byte[] bArr);
    }

    public WriterThread(LinkedBlockingDeque<EventRequest> queue, OutputStream outputStream, EventProcessor processor) {
        setName("WriterThread");
        this.logger.d("create WriterThread");
        this.mmOutStream = outputStream;
        this.mEventQueue = queue;
        this.mEventProcessor = processor;
    }

    public void run() {
        this.logger.i("starting WriterThread loop.");
        while (!this.closing) {
            EventRequest request = null;
            try {
                request = (EventRequest) this.mEventQueue.take();
            } catch (InterruptedException e) {
                this.logger.i("WriterThread interrupted");
            }
            if (this.closing) {
                break;
            } else if (request == null) {
                this.logger.e("WriterThread: unable to retrieve event from queue");
            } else {
                byte[] eventData = request.eventData;
                if (eventData.length > 524288) {
                    throw new RuntimeException("writer Max packet size exceeded [" + eventData.length + "] bytes[" + IOUtils.bytesToHexString(eventData, 0, 50) + "]");
                }
                if (this.mEventProcessor != null) {
                    eventData = this.mEventProcessor.processEvent(eventData);
                }
                if (eventData != null) {
                    boolean sent = false;
                    try {
                        this.mmEventWriter.write(request.eventData);
                        sent = true;
                    } catch (IOException e2) {
                        if (!this.closing) {
                            this.logger.e("Error writing event:" + e2.getMessage());
                        }
                    }
                    if (!sent) {
                        this.logger.i("send failed");
                        request.callCompletionHandlers(PostEventStatus.SEND_FAILED);
                    }
                }
            }
        }
        this.logger.i("Writer thread ending");
    }

    public void cancel() {
        super.cancel();
        IOUtils.closeStream(this.mmOutStream);
        this.mmOutStream = null;
    }
}
