package com.navdy.service.library.util;

import com.navdy.service.library.log.Logger;
import java.util.concurrent.atomic.AtomicLong;

public class NetworkActivityTracker extends Thread {
    private static final int LOGGING_INTERVAL = 60000;
    private static final NetworkActivityTracker sInstance = new NetworkActivityTracker();
    private static final Logger sLogger = new Logger(NetworkActivityTracker.class);
    private AtomicLong bytesReceived = new AtomicLong(0);
    private AtomicLong bytesSent = new AtomicLong(0);
    private double highestReceivedBandwidth = 0.0d;
    private double highestSentBandwidth = 0.0d;
    private long starttime = System.currentTimeMillis();

    public static NetworkActivityTracker getInstance() {
        return sInstance;
    }

    private NetworkActivityTracker() {
    }

    public void addBytesSent(int bytes) {
        this.bytesSent.addAndGet((long) bytes);
    }

    public void addBytesReceived(int bytes) {
        this.bytesReceived.addAndGet((long) bytes);
    }

    public void run() {
        long lastBytesSent = this.bytesSent.get();
        long lastBytesReceived = this.bytesReceived.get();
        long lastTime = this.starttime;
        boolean endLoop = false;
        do {
            try {
                sleep(60000);
            } catch (InterruptedException e) {
                endLoop = true;
            }
            long currentByesSent = this.bytesSent.get();
            long currentBytesReceived = this.bytesReceived.get();
            long currentTime = System.currentTimeMillis();
            long intervalBytesSent = currentByesSent - lastBytesSent;
            long intervalBytesReceived = currentBytesReceived - lastBytesReceived;
            long interval = currentTime - lastTime;
            long sinceStart = currentTime - this.starttime;
            double intervalReceivedBandwidth = (((double) intervalBytesReceived) * 1000.0d) / ((double) interval);
            lastBytesSent = currentByesSent;
            lastBytesReceived = currentBytesReceived;
            lastTime = currentTime;
            this.highestSentBandwidth = Math.max(this.highestSentBandwidth, (((double) intervalBytesSent) * 1000.0d) / ((double) interval));
            this.highestReceivedBandwidth = Math.max(this.highestReceivedBandwidth, intervalReceivedBandwidth);
            if (intervalBytesSent > 0 || intervalBytesReceived > 0) {
                sLogger.i(String.format("Total time: %dms interval: %dms sent: %d bytes received: %d bytes totalSent: %d bytes totalReceived: %d bytes sentBandwidth: %.2f bytes/sec receivedBandwidth: %.2f bytes/sec maxSentBandwidth: %.2f bytes/sec maxReceivedBandwidth: %.2f bytes/sec", new Object[]{Long.valueOf(sinceStart), Long.valueOf(interval), Long.valueOf(intervalBytesSent), Long.valueOf(intervalBytesReceived), Long.valueOf(currentByesSent), Long.valueOf(currentBytesReceived), Double.valueOf(intervalSentBandwidth), Double.valueOf(intervalReceivedBandwidth), Double.valueOf(this.highestSentBandwidth), Double.valueOf(this.highestReceivedBandwidth)}));
                continue;
            }
        } while (!endLoop);
    }
}
