package com.navdy.obd;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class Pid implements Parcelable {
    public static final Creator<Pid> CREATOR = new Creator<Pid>() {
        public Pid createFromParcel(Parcel source) {
            return new Pid(source);
        }

        public Pid[] newArray(int size) {
            return new Pid[size];
        }
    };
    public static final int NO_DATA = Integer.MIN_VALUE;
    protected DataType dataType;
    protected int id;
    protected String name;
    protected long timeStamp;
    protected Units units;
    protected double value;

    enum DataType {
        INT,
        FLOAT,
        PERCENTAGE,
        BOOL
    }

    public Pid(int id) {
        this(id, null, 0.0d, DataType.INT, Units.NONE);
    }

    public Pid(int id, String name) {
        this(id, name, 0.0d, DataType.INT, Units.NONE);
    }

    public Pid(int id, String name, double value, DataType dataType, Units units) {
        this(id, name, 0.0d, DataType.INT, Units.NONE, 0);
    }

    public Pid(int id, String name, double value, DataType dataType, Units units, long timeStamp) {
        this.id = id;
        this.name = name;
        this.value = value;
        this.dataType = dataType;
        this.units = units;
        this.timeStamp = timeStamp;
    }

    public int getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public double getValue() {
        return this.value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public DataType getDataType() {
        return this.dataType;
    }

    public int describeContents() {
        return 0;
    }

    public Pid(Parcel in) {
        this(in.readInt(), in.readString(), in.readDouble(), DataType.valueOf(in.readString()), Units.valueOf(in.readString()), in.readLong());
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.name);
        dest.writeDouble(this.value);
        dest.writeString(this.dataType.name());
        dest.writeString(this.units.name());
        dest.writeLong(this.timeStamp);
    }

    public boolean equals(Object o) {
        if (o instanceof Pid) {
            return ((Pid) o).id == this.id;
        } else {
            return super.equals(o);
        }
    }

    public String toString() {
        return "Pid{id=" + Integer.toHexString(this.id) + '}';
    }

    public long getTimeStamp() {
        return this.timeStamp;
    }

    public void setTimeStamp(long timeStamp) {
        this.timeStamp = timeStamp;
    }
}
