package com.navdy.hud.app.device.dial;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanRecord;
import android.bluetooth.le.ScanResult;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.HandlerThread;
import android.text.TextUtils;
import android.view.InputDevice;
import android.view.KeyEvent;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.bluetooth.utils.BluetoothUtils;
import com.navdy.hud.app.device.dial.DialConstants.DialConnectionStatus;
import com.navdy.hud.app.device.dial.DialConstants.DialConnectionStatus.Status;
import com.navdy.hud.app.device.dial.DialConstants.NotificationReason;
import com.navdy.hud.app.device.dial.DialFirmwareUpdater.UpdateListener;
import com.navdy.hud.app.device.dial.DialManagerHelper.IDialConnection;
import com.navdy.hud.app.device.dial.DialManagerHelper.IDialConnectionStatus;
import com.navdy.hud.app.device.dial.DialManagerHelper.IDialForgotten;
import com.navdy.hud.app.framework.toast.ToastManager;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder;
import com.navdy.hud.app.screen.BaseScreen;
import com.navdy.hud.app.util.DeviceUtil;
import com.navdy.hud.app.util.GenericUtil;
import com.navdy.hud.app.util.os.SystemProperties;
import com.navdy.service.library.events.ui.Screen;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.task.TaskManager;
import com.navdy.service.library.util.IOUtils;
import com.squareup.otto.Bus;
import java.lang.reflect.Method;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;
import net.hockeyapp.android.tasks.LoginTask;

public class DialManager {
    private static final String BALANCED = "balanced";
    private static final DialConnectionStatus DIAL_CONNECTED = new DialConnectionStatus(Status.CONNECTED);
    private static final DialConnectionStatus DIAL_CONNECTING = new DialConnectionStatus(Status.CONNECTING);
    private static final DialConnectionStatus DIAL_CONNECTION_FAILED = new DialConnectionStatus(Status.CONNECTION_FAILED);
    private static final DialConnectionStatus DIAL_NOT_CONNECTED = new DialConnectionStatus(Status.DISCONNECTED);
    private static final DialConnectionStatus DIAL_NOT_FOUND = new DialConnectionStatus(Status.NO_DIAL_FOUND);
    private static final String DIAL_POWER_SETTING = "persist.sys.bt.dial.pwr";
    private static final String HIGH_POWER = "high";
    private static final String LOW_POWER = "low";
    private static final Map<String, Integer> PowerMap = new HashMap();
    static final String TAG_DIAL = "[Dial]";
    static final Logger sLogger = new Logger(DialManager.class);
    private static final DialManager singleton = new DialManager();
    private BluetoothGattCharacteristic batteryCharateristic;
    private Runnable batteryReadRunnable = new Runnable() {
        public void run() {
            try {
                if (DialManager.this.batteryCharateristic != null) {
                    DialManager.this.queueRead(DialManager.this.batteryCharateristic);
                }
                if (DialManager.this.rawBatteryCharateristic != null) {
                    DialManager.this.queueRead(DialManager.this.rawBatteryCharateristic);
                }
                if (DialManager.this.temperatureCharateristic != null) {
                    DialManager.this.queueRead(DialManager.this.temperatureCharateristic);
                }
                DialManager.this.handler.postDelayed(this, 300000);
            } catch (Throwable t) {
                DialManager.sLogger.e(DialManager.TAG_DIAL, t);
            }
        }
    };
    private BluetoothAdapter bluetoothAdapter;
    private BroadcastReceiver bluetoothBondingReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            try {
                String action = intent.getAction();
                final BluetoothDevice device = (BluetoothDevice) intent.getParcelableExtra("android.bluetooth.device.extra.DEVICE");
                if (device != null && DialManager.this.bondingDial != null) {
                    if (DialManager.this.isDialDevice(device)) {
                        DialManager.sLogger.v("[Dial]btBondRecvr [" + device.getName() + "] action[" + action + "]");
                        if ("android.bluetooth.device.action.BOND_STATE_CHANGED".equals(action)) {
                            int state = intent.getIntExtra("android.bluetooth.device.extra.BOND_STATE", Integer.MIN_VALUE);
                            DialManager.sLogger.v("[Dial]btBondRecvr: prev state:" + intent.getIntExtra("android.bluetooth.device.extra.PREVIOUS_BOND_STATE", Integer.MIN_VALUE) + " new state:" + state);
                            if (state == 12) {
                                DialManager.sLogger.v("btBondRecvr removed hang runnable");
                                DialManager.this.handler.removeCallbacks(DialManager.this.bondHangRunnable);
                                DialManager.sLogger.v("[Dial]btBondRecvr: got pairing event");
                                if (DialManager.this.bondingDial == null || !DialManager.this.bondingDial.equals(device)) {
                                    DialManager.sLogger.v("[Dial]no bonding active bondDial[" + DialManager.this.bondingDial + "] device[" + device + "]");
                                    return;
                                }
                                DialManager.sLogger.v("[Dial]btBondRecvr: bonding active bondDial[" + DialManager.this.bondingDial + "] device[" + device + "]");
                                DialManager.sLogger.v("btBondRecvr:" + device + "state is:" + device.getBondState());
                                if (DialManager.this.addtoBondList(device)) {
                                    DialManager.sLogger.v("btBondRecvr: added to bond list");
                                } else {
                                    DialManager.sLogger.v("btConnRecvr: already in bond list");
                                }
                                DialManager.sLogger.v("btConnRecvr: attempt connection reqd");
                                DialManager.this.bondingDial = null;
                                DialManagerHelper.connectToDial(DialManager.this.bluetoothAdapter, device, new IDialConnection() {
                                    public void onAttemptConnection(boolean success) {
                                        DialManager.sLogger.v("btConnRecvr: attempt connection [" + device.getName() + " ] success[" + success + "]");
                                    }

                                    public void onAttemptDisconnection(boolean success) {
                                    }
                                });
                                return;
                            } else if (state == 10) {
                                DialManager.sLogger.e("[Dial]btBondRecvr: not paired tries[" + DialManager.this.bondTries + "]");
                                if (DialManager.this.bondTries == 3) {
                                    DialManager.sLogger.v("btBondRecvr removed runnable");
                                    DialManager.this.handler.removeCallbacks(DialManager.this.bondHangRunnable);
                                    String dialName = null;
                                    if (DialManager.this.bondingDial != null) {
                                        dialName = DialManager.this.bondingDial.getName();
                                    }
                                    DialManager.this.bondingDial = null;
                                    DialManager.sLogger.e("[Dial]btBondRecvr: giving up");
                                    DialManager.DIAL_NOT_CONNECTED.dialName = dialName;
                                    DialManager.this.bus.post(DialManager.DIAL_NOT_CONNECTED);
                                    return;
                                }
                                DialManager.sLogger.e("[Dial]btBondRecvr: trying to bond again");
                                DialManager.this.bondTries = DialManager.this.bondTries + 1;
                                DialManager.this.handler.postDelayed(new Runnable() {
                                    public void run() {
                                        DialManager.sLogger.v("btBondRecvr removed runnable");
                                        DialManager.this.handler.removeCallbacks(DialManager.this.bondHangRunnable);
                                        device.createBond();
                                        DialManager.this.handler.postDelayed(DialManager.this.bondHangRunnable, 30000);
                                        DialManager.sLogger.v("btBondRecvr posted runnable");
                                    }
                                }, 4000);
                                return;
                            } else {
                                return;
                            }
                        }
                        return;
                    }
                    DialManager.sLogger.i("[Dial]btBondRecvr: notification not for dial:" + device.getName() + " addr:" + device.getAddress());
                }
            } catch (Throwable t) {
                DialManager.sLogger.e(DialManager.TAG_DIAL, t);
            }
        }
    };
    private BroadcastReceiver bluetoothConnectivityReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            if (intent != null) {
                try {
                    String action = intent.getAction();
                    final BluetoothDevice device = (BluetoothDevice) intent.getParcelableExtra("android.bluetooth.device.extra.DEVICE");
                    if (device == null) {
                        return;
                    }
                    if (DialManager.this.isDialDevice(device)) {
                        DialManager.sLogger.v("[Dial]btConnRecvr [" + device.getName() + "] action [" + action + "]");
                        if (!DeviceUtil.isNavdyDevice()) {
                            boolean isConnected = false;
                            if (DialConstants.ACTION_CONNECTION_STATE_CHANGED.equals(action)) {
                                int oldState = intent.getIntExtra("android.bluetooth.profile.extra.PREVIOUS_STATE", -1);
                                int newState = intent.getIntExtra("android.bluetooth.profile.extra.STATE", -1);
                                DialManager.sLogger.v("[Dial]btConnRecvr: dial connection state changed old[" + oldState + "] new[" + newState + "]");
                                if (newState == 2) {
                                    isConnected = true;
                                }
                            }
                            if (DialManager.this.connectedDial != null && !TextUtils.equals(DialManager.this.connectedDial.getName(), device.getName())) {
                                DialManager.sLogger.v("btConnRecvr: notification for not current connected dial");
                                DialManager.this.handler.removeCallbacks(DialManager.this.bondHangRunnable);
                                return;
                            } else if ("android.bluetooth.device.action.ACL_CONNECTED".equals(action) || isConnected) {
                                DialManager.this.handler.removeCallbacks(DialManager.this.bondHangRunnable);
                                DialManager.this.connectedDial = device;
                                if (DialManager.this.addtoBondList(device)) {
                                    DialManager.sLogger.v("attempt connection reqd");
                                    DialManagerHelper.connectToDial(DialManager.this.bluetoothAdapter, device, new IDialConnection() {
                                        public void onAttemptConnection(boolean success) {
                                            DialManager.sLogger.v("attempt connection [" + device.getName() + " ] success[" + success + "]");
                                            if (success) {
                                                DialManager.this.handleDialConnection();
                                                DialManager.sLogger.i("[Dial]btConnRecvr: dial connected [" + device.getName() + "] addr:" + device.getAddress());
                                            }
                                        }

                                        public void onAttemptDisconnection(boolean success) {
                                        }
                                    });
                                    return;
                                }
                                DialManager.sLogger.v("attempt connection not read");
                                DialManager.this.handleDialConnection();
                                return;
                            } else if ("android.bluetooth.device.action.ACL_DISCONNECTED".equals(action)) {
                                DialManager.this.handler.removeCallbacks(DialManager.this.bondHangRunnable);
                                if (DialManager.this.connectedDial != null) {
                                    DialManager.this.disconnectedDial = DialManager.this.connectedDial.getName();
                                }
                                DialManager.DIAL_NOT_CONNECTED.dialName = DialManager.this.disconnectedDial;
                                DialManager.this.bus.post(DialManager.DIAL_NOT_CONNECTED);
                                if (DialManager.this.getBondedDialCount() > 0) {
                                    DialManager.this.clearConnectedDial();
                                    DialManager.this.stopGATTClient();
                                    DialManager.sLogger.e("[Dial]btConnRecvr: dial NOT connected [" + device.getName() + "] addr:" + device.getAddress());
                                } else {
                                    DialManager.this.clearConnectedDial();
                                    DialManager.this.stopGATTClient();
                                    DialManager.sLogger.e("[Dial]btConnRecvr: dial NOT connected [" + device.getName() + "] addr:" + device.getAddress());
                                }
                                return;
                            } else {
                                return;
                            }
                        }
                        return;
                    }
                    DialManager.sLogger.i("[Dial]btConnRecvr: notification not for dial:" + device.getName() + " addr:" + device.getAddress());
                } catch (Throwable t) {
                    DialManager.sLogger.e(DialManager.TAG_DIAL, t);
                }
            }
        }
    };
    private BluetoothGatt bluetoothGatt;
    private BluetoothLeScanner bluetoothLEScanner;
    private BluetoothManager bluetoothManager;
    private BroadcastReceiver bluetoothOnOffReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            try {
                if (intent.getAction().equals("android.bluetooth.adapter.action.STATE_CHANGED")) {
                    switch (intent.getIntExtra("android.bluetooth.adapter.extra.STATE", Integer.MIN_VALUE)) {
                        case 10:
                            DialManager.sLogger.v("[Dial]onoff: state off");
                            return;
                        case 11:
                            DialManager.sLogger.v("[Dial]onoff: state turning on");
                            return;
                        case 12:
                            DialManager.sLogger.v("[Dial]onoff: state on");
                            if (!DialManager.this.dialManagerInitialized) {
                                DialManager.this.init();
                                return;
                            }
                            return;
                        case 13:
                            DialManager.sLogger.v("[Dial]onoff: state turning off");
                            return;
                        default:
                            return;
                    }
                    DialManager.sLogger.e(DialManager.TAG_DIAL, t);
                }
            } catch (Throwable t) {
                DialManager.sLogger.e(DialManager.TAG_DIAL, t);
            }
        }
    };
    private Runnable bondHangRunnable = new Runnable() {
        public void run() {
            try {
                DialManager.sLogger.e("[Dial]bond hang detected");
                String dialName = null;
                if (DialManager.this.bondingDial != null) {
                    dialName = DialManager.this.bondingDial.getName();
                }
                DialManager.this.bondingDial = null;
                DialManager.DIAL_NOT_CONNECTED.dialName = dialName;
                DialManager.this.bus.post(DialManager.DIAL_NOT_CONNECTED);
            } catch (Throwable t) {
                DialManager.sLogger.e(DialManager.TAG_DIAL, t);
            }
        }
    };
    private int bondTries;
    private ArrayList<BluetoothDevice> bondedDials = new ArrayList();
    private BluetoothDevice bondingDial;
    private Bus bus;
    private volatile BluetoothDevice connectedDial;
    private Runnable connectionErrorRunnable = new Runnable() {
        public void run() {
            DialManager.sLogger.v("connectionErrorRunnable no input descriptor event:" + DialManager.this.tempConnectedDial);
            if (DialManager.this.tempConnectedDial != null) {
                final String dialName = DialManager.this.tempConnectedDial.getName();
                DialManager.this.disconectAndRemoveBond(DialManager.this.tempConnectedDial, new IDialForgotten() {
                    public void onForgotten() {
                        DialManager.sLogger.v("connectionErrorRunnable: dial forgotten");
                        DialManager.DIAL_CONNECTION_FAILED.dialName = dialName;
                        DialManager.this.bus.post(DialManager.DIAL_CONNECTION_FAILED);
                        DialManager.this.handler.post(new Runnable() {
                            public void run() {
                                DialManagerHelper.sendLocalyticsEvent(DialManager.this.handler, false, false, 0, false, "Connection error");
                            }
                        });
                    }
                }, 2000);
                return;
            }
            DialManager.sLogger.v("connectionErrorRunnable no temp connected dial");
        }
    };
    private Runnable deviceInfoGattReadRunnable = new Runnable() {
        public void run() {
            try {
                if (DialManager.this.firmwareVersionCharacteristic != null) {
                    DialManager.this.queueRead(DialManager.this.firmwareVersionCharacteristic);
                }
                if (DialManager.this.hardwareVersionCharacteristic != null) {
                    DialManager.this.queueRead(DialManager.this.hardwareVersionCharacteristic);
                }
            } catch (Throwable t) {
                DialManager.sLogger.e(DialManager.TAG_DIAL, t);
            }
        }
    };
    private Thread dialEventsListenerThread;
    private DialFirmwareUpdater dialFirmwareUpdater;
    private BluetoothGattCharacteristic dialForgetKeysCharacteristic;
    private volatile boolean dialManagerInitialized;
    private BluetoothGattCharacteristic dialRebootCharacteristic;
    private String disconnectedDial;
    private int encryptionRetryCount;
    private String firmWareVersion;
    private BluetoothGattCharacteristic firmwareVersionCharacteristic;
    private BluetoothGattCallback gattCallback = new BluetoothGattCallback() {
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            Integer powerLevel = null;
            try {
                DialManager.sLogger.v("[Dial]gatt state change:" + status + " newState=" + newState);
                if (newState == 2) {
                    if (gatt != null) {
                        if (DeviceUtil.isNavdyDevice()) {
                            String powerSetting = SystemProperties.get(DialManager.DIAL_POWER_SETTING);
                            if (powerSetting != null) {
                                powerLevel = (Integer) DialManager.PowerMap.get(powerSetting);
                            }
                            if (powerLevel != null) {
                                DialManager.sLogger.v("[Dial] setting BT power to " + powerSetting);
                                gatt.requestConnectionPriority(powerLevel.intValue());
                            }
                        }
                        DialManager.this.gattCharacteristicProcessor = new CharacteristicCommandProcessor(gatt, DialManager.this.handler);
                        DialManager.sLogger.v("[Dial]gatt connected, discover");
                        gatt.discoverServices();
                        return;
                    }
                    DialManager.sLogger.v("[Dial]gatt is null, cannot discover");
                } else if (newState == 0) {
                    DialManager.sLogger.v("[Dial]gatt disconnected");
                    if (DialManager.this.gattCharacteristicProcessor != null) {
                        DialManager.this.gattCharacteristicProcessor.release();
                        DialManager.this.gattCharacteristicProcessor = null;
                    }
                    DialManager.this.stopGATTClient();
                }
            } catch (Throwable t) {
                DialManager.sLogger.e(DialManager.TAG_DIAL, t);
            }
        }

        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            DialManager.this.hardwareVersionCharacteristic = null;
            DialManager.this.firmwareVersionCharacteristic = null;
            DialManager.this.hidHostReadyCharacteristic = null;
            DialManager.this.dialForgetKeysCharacteristic = null;
            DialManager.this.dialRebootCharacteristic = null;
            DialManager.this.batteryCharateristic = null;
            DialManager.this.rawBatteryCharateristic = null;
            DialManager.this.temperatureCharateristic = null;
            try {
                if (!DialManager.this.isGattOn) {
                    DialManager.sLogger.w("gatt is not on but onServicesDiscovered called");
                } else if (status == 0) {
                    DialManager.sLogger.i("[Dial]onServicesDiscovered");
                    DialManager.this.dialFirmwareUpdater.onServicesDiscovered(gatt);
                    for (BluetoothGattService service : gatt.getServices()) {
                        UUID uuid = service.getUuid();
                        if (DialConstants.HID_SERVICE_UUID.equals(uuid)) {
                            DialManager.sLogger.d("Hid service found " + uuid);
                            DialManager.this.hidHostReadyCharacteristic = service.getCharacteristic(DialConstants.HID_HOST_READY_UUID);
                            if (DialManager.this.hidHostReadyCharacteristic == null) {
                                DialManager.sLogger.e("Hid host ready Characteristic is null");
                            } else {
                                DialManager.sLogger.v("Found Hid host ready Characteristic");
                                DialManager.this.sharedPreferences.edit().putBoolean(DialConstants.VIDEO_SHOWN_PREF, true).apply();
                            }
                            DialManager.this.handler.post(DialManager.this.hidHostReadyReadRunnable);
                        } else if (DialConstants.BATTERY_SERVICE_UUID.equals(uuid)) {
                            DialManager.sLogger.i("[Dial]Service: " + service.getUuid().toString());
                            List<BluetoothGattCharacteristic> characteristics = service.getCharacteristics();
                            if (characteristics == null) {
                                DialManager.sLogger.i("[Dial]No characteristic");
                                return;
                            }
                            for (BluetoothGattCharacteristic characteristic : characteristics) {
                                if (DialConstants.BATTERY_LEVEL_UUID.equals(characteristic.getUuid())) {
                                    DialManager.this.batteryCharateristic = characteristic;
                                } else if (DialConstants.RAW_BATTERY_LEVEL_UUID.equals(characteristic.getUuid())) {
                                    DialManager.this.rawBatteryCharateristic = characteristic;
                                } else if (DialConstants.SYSTEM_TEMPERATURE_UUID.equals(characteristic.getUuid())) {
                                    DialManager.this.temperatureCharateristic = characteristic;
                                }
                            }
                            if (DialManager.this.batteryCharateristic != null) {
                                DialManager.this.handler.postDelayed(DialManager.this.batteryReadRunnable, 5000);
                                DialManager.this.handler.postDelayed(DialManager.this.sendLocalyticsSuccessRunnable, 10000);
                            } else {
                                DialManager.sLogger.v("[Dial] battery characteristic not found");
                            }
                        } else if (DialConstants.DEVICE_INFO_SERVICE_UUID.equals(uuid)) {
                            DialManager.sLogger.d("Device info service found " + uuid);
                            DialManager.this.firmwareVersionCharacteristic = service.getCharacteristic(DialConstants.DEVICE_INFO_FIRMWARE_VERSION_UUID);
                            if (DialManager.this.firmwareVersionCharacteristic == null) {
                                DialManager.sLogger.e("Firmware Version Characteristic is null");
                            }
                            DialManager.this.hardwareVersionCharacteristic = service.getCharacteristic(DialConstants.DEVICE_INFO_HARDWARE_VERSION_UUID);
                            if (DialManager.this.hardwareVersionCharacteristic == null) {
                                DialManager.sLogger.e("Hardware Version Characteristic is null");
                            }
                            DialManager.this.handler.postDelayed(DialManager.this.deviceInfoGattReadRunnable, 5000);
                        } else if (DialConstants.OTA_SERVICE_UUID.equals(uuid)) {
                            DialManager.this.dialForgetKeysCharacteristic = service.getCharacteristic(DialConstants.DIAL_FORGET_KEYS_UUID);
                            if (DialManager.this.dialForgetKeysCharacteristic == null) {
                                DialManager.sLogger.d("Dial key forget is null");
                            }
                            DialManager.this.dialRebootCharacteristic = service.getCharacteristic(DialConstants.DIAL_REBOOT_UUID);
                            if (DialManager.this.dialRebootCharacteristic == null) {
                                DialManager.sLogger.d("Dial reboot is null");
                            }
                        } else {
                            DialManager.sLogger.v("[Dial]Ignoring service:" + uuid);
                        }
                    }
                } else {
                    DialManager.sLogger.e("[Dial]onServicesDiscovered error:" + status);
                }
            } catch (Throwable t) {
                DialManager.sLogger.e(DialManager.TAG_DIAL, t);
            }
        }

        public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            DialManager.this.dialFirmwareUpdater.onCharacteristicRead(characteristic, status);
            if (status == 0) {
                print(characteristic);
            } else {
                DialManager.sLogger.v("[Dial]onCharacteristicRead fail:" + status);
            }
            DialManager.sLogger.d("onCharacteristicRead finished");
            if (DialManager.this.gattCharacteristicProcessor != null) {
                DialManager.this.gattCharacteristicProcessor.commandFinished();
            }
        }

        public void onCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            DialManager.this.dialFirmwareUpdater.onCharacteristicWrite(gatt, characteristic, status);
            if (characteristic == DialManager.this.dialForgetKeysCharacteristic) {
                DialManager.this.forgetDial(gatt.getDevice());
            }
            if (characteristic == DialManager.this.dialRebootCharacteristic) {
                boolean successful = status == 0;
                if (successful) {
                    DialManager.this.sharedPreferences.edit().putLong(DialConstants.DIAL_REBOOT_PROPERTY, System.currentTimeMillis()).apply();
                }
                DialManagerHelper.sendRebootLocalyticsEvent(DialManager.this.handler, successful, gatt.getDevice(), null);
            }
            if (DialManager.this.gattCharacteristicProcessor != null) {
                DialManager.this.gattCharacteristicProcessor.commandFinished();
            }
        }

        public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
            DialManager.sLogger.v("[Dial]onCharacteristicChanged");
            print(characteristic);
        }

        public void onDescriptorRead(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status) {
            DialManager.sLogger.v("[Dial]onDescriptorRead");
        }

        public void onDescriptorWrite(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status) {
            DialManager.sLogger.v("[Dial]onDescriptorWrite : " + (status == 0 ? LoginTask.BUNDLE_SUCCESS : "fail"));
        }

        void print(BluetoothGattCharacteristic characteristic) {
            Integer level;
            String value;
            if (DialConstants.HID_HOST_READY_UUID.equals(characteristic.getUuid())) {
                DialManager.sLogger.v("[Dial]onCharacteristicRead hid host ready, length " + characteristic.getValue().length);
            } else if (DialConstants.BATTERY_LEVEL_UUID.equals(characteristic.getUuid())) {
                DialManager.sLogger.v("[Dial]onCharacteristicRead battery");
                level = characteristic.getIntValue(33, 0);
                if (level != null) {
                    DialManager.sLogger.v("[Dial]battery level is [" + level + "] %");
                    DialManager.this.processBatteryLevel(level.intValue());
                    return;
                }
                DialManager.sLogger.v("[Dial]battery level no data");
            } else if (DialConstants.RAW_BATTERY_LEVEL_UUID.equals(characteristic.getUuid())) {
                DialManager.sLogger.v("[Dial]onCharacteristicRead rawBattery");
                level = characteristic.getIntValue(34, 0);
                if (level != null) {
                    DialManager.sLogger.v("[Dial]raw battery level is [" + level + "]");
                    DialManager.this.processRawBatteryLevel(level);
                    return;
                }
                DialManager.sLogger.v("[Dial]raw battery level no data");
            } else if (DialConstants.SYSTEM_TEMPERATURE_UUID.equals(characteristic.getUuid())) {
                DialManager.sLogger.v("[Dial]onCharacteristicRead temperature");
                level = characteristic.getIntValue(34, 0);
                if (level != null) {
                    DialManager.sLogger.v("[Dial]system temperature is [" + level + "]");
                    DialManager.this.processSystemTemperature(level);
                    return;
                }
                DialManager.sLogger.v("[Dial]system temperature no data");
            } else if (DialConstants.DEVICE_INFO_HARDWARE_VERSION_UUID.equals(characteristic.getUuid())) {
                DialManager.sLogger.v("[Dial]onCharacteristicRead Hardware version");
                value = characteristic.getStringValue(0);
                if (!TextUtils.isEmpty(value)) {
                    DialManager.this.hardwareVersion = value.trim();
                    DialManager.sLogger.d("Device Info hardware version received: " + DialManager.this.hardwareVersion);
                }
            } else if (DialConstants.DEVICE_INFO_FIRMWARE_VERSION_UUID.equals(characteristic.getUuid())) {
                DialManager.sLogger.v("[Dial]onCharacteristicRead Firmware version");
                value = characteristic.getStringValue(0);
                if (!TextUtils.isEmpty(value)) {
                    DialManager.this.firmWareVersion = value.trim();
                    DialManager.sLogger.d("Device Info firmware version received: " + DialManager.this.firmWareVersion);
                }
            } else {
                DialManager.sLogger.v("[Dial]onCharacteristicRead:" + characteristic.getUuid());
            }
        }
    };
    private CharacteristicCommandProcessor gattCharacteristicProcessor;
    private Handler handler;
    private HandlerThread handlerThread;
    private String hardwareVersion;
    private BluetoothGattCharacteristic hardwareVersionCharacteristic;
    private BluetoothGattCharacteristic hidHostReadyCharacteristic;
    private Runnable hidHostReadyReadRunnable = new Runnable() {
        public void run() {
            try {
                if (DialManager.this.hidHostReadyCharacteristic != null) {
                    DialManager.sLogger.v("queuing hid host ready characteristic");
                    DialManager.this.queueRead(DialManager.this.hidHostReadyCharacteristic);
                    return;
                }
                DialManager.sLogger.v("not queuing hid host ready characteristic: null");
            } catch (Throwable t) {
                DialManager.sLogger.e(DialManager.TAG_DIAL, t);
            }
        }
    };
    private Set<BluetoothDevice> ignoredNonNavdyDials = new HashSet();
    private volatile boolean isGattOn;
    private volatile boolean isScanning;
    private int lastKnownBatteryLevel = -1;
    private Integer lastKnownRawBatteryLevel;
    private Integer lastKnownSystemTemperature;
    private ScanCallback leScanCallback = new ScanCallback() {
        public void onScanResult(int callbackType, ScanResult result) {
            try {
                ScanRecord record = result.getScanRecord();
                DialManager.this.handleScannedDevice(result.getDevice(), record.getTxPowerLevel(), record);
            } catch (Throwable t) {
                DialManager.sLogger.e(DialManager.TAG_DIAL, t);
            }
        }

        public void onScanFailed(int errorCode) {
            DialManager.sLogger.e("btle scan failed:" + errorCode);
            DialManager.this.stopScan(true);
        }
    };
    private NotificationReason notificationReasonBattery = NotificationReason.OK_BATTERY;
    private BluetoothGattCharacteristic rawBatteryCharateristic;
    private Runnable scanHangRunnable = new Runnable() {
        public void run() {
            try {
                DialManager.sLogger.v("scan hang runnable");
                DialManager.this.stopScan(true);
            } catch (Throwable t) {
                DialManager.sLogger.e(DialManager.TAG_DIAL, t);
            }
        }
    };
    private Map<BluetoothDevice, ScanRecord> scannedNavdyDials = new LinkedHashMap();
    private Runnable sendLocalyticsSuccessRunnable = new Runnable() {
        public void run() {
            if (DialManager.this.connectedDial != null) {
                DialManagerHelper.sendLocalyticsEvent(DialManager.this.handler, false, true, 0, false);
            }
        }
    };
    private SharedPreferences sharedPreferences;
    private Runnable stopScanRunnable = new Runnable() {
        public void run() {
            try {
                DialManager.this.stopScan(false);
            } catch (Throwable t) {
                DialManager.sLogger.e(DialManager.TAG_DIAL, t);
            }
        }
    };
    private volatile BluetoothDevice tempConnectedDial;
    private BluetoothGattCharacteristic temperatureCharateristic;

    static class CharacteristicCommandProcessor {
        private BluetoothGatt bluetoothGatt;
        private LinkedList<Command> commandsQueue = new LinkedList();
        private Handler handler;

        static abstract class Command {
            BluetoothGattCharacteristic characteristic;

            public abstract void process(BluetoothGatt bluetoothGatt);

            public Command(BluetoothGattCharacteristic characteristic) {
                this.characteristic = characteristic;
            }
        }

        static class ReadCommand extends Command {
            public ReadCommand(BluetoothGattCharacteristic characteristic) {
                super(characteristic);
            }

            public void process(BluetoothGatt gatt) {
                if (gatt.readCharacteristic(this.characteristic)) {
                    DialManager.sLogger.d("Processing GATT read succeeded " + this.characteristic.getUuid().toString());
                } else {
                    DialManager.sLogger.d("Processing GATT read failed " + this.characteristic.getUuid().toString());
                }
            }
        }

        static class WriteCommand extends Command {
            byte[] data;

            public WriteCommand(BluetoothGattCharacteristic characteristic, byte[] data) {
                super(characteristic);
                this.data = data;
            }

            public void process(BluetoothGatt gatt) {
                this.characteristic.setValue(this.data);
                if (!gatt.writeCharacteristic(this.characteristic)) {
                    DialManager.sLogger.d("Processing GATT write failed " + this.characteristic.getUuid().toString());
                } else if (!this.characteristic.getUuid().equals(DialConstants.OTA_DATA_CHARACTERISTIC_UUID)) {
                    DialManager.sLogger.d("Processing GATT write succeeded " + this.characteristic.getUuid().toString());
                }
            }
        }

        public CharacteristicCommandProcessor(BluetoothGatt bluetoothGatt, Handler handler) {
            this.bluetoothGatt = bluetoothGatt;
            this.handler = handler;
        }

        public synchronized void process(Command command) {
            this.commandsQueue.add(command);
            if (this.commandsQueue.size() == 1) {
                submitNext();
            }
        }

        public synchronized void submitNext() {
            final Command command = (Command) this.commandsQueue.peek();
            if (command != null) {
                this.handler.post(new Runnable() {
                    public void run() {
                        try {
                            if (CharacteristicCommandProcessor.this.bluetoothGatt != null) {
                                command.process(CharacteristicCommandProcessor.this.bluetoothGatt);
                            }
                        } catch (Throwable t) {
                            DialManager.sLogger.d("Error while executing GATT command", t);
                        }
                    }
                });
            }
        }

        public synchronized void commandFinished() {
            if (this.commandsQueue.size() > 0) {
                Command command = (Command) this.commandsQueue.poll();
                submitNext();
            }
        }

        public void release() {
            this.commandsQueue.clear();
            this.handler = null;
            this.bluetoothGatt = null;
        }
    }

    public static class DialUpdateStatus {
        public boolean available;

        DialUpdateStatus(boolean available) {
            this.available = available;
        }
    }

    static {
        PowerMap.put(LOW_POWER, Integer.valueOf(2));
        PowerMap.put(BALANCED, Integer.valueOf(0));
        PowerMap.put(HIGH_POWER, Integer.valueOf(1));
    }

    public static DialManager getInstance() {
        return singleton;
    }

    public void queueRead(BluetoothGattCharacteristic characteristic) {
        if (this.gattCharacteristicProcessor == null) {
            sLogger.w("gattCharacteristicProcessor not initialized");
        } else {
            this.gattCharacteristicProcessor.process(new ReadCommand(characteristic));
        }
    }

    public void queueWrite(BluetoothGattCharacteristic characteristic, byte[] data) {
        if (this.gattCharacteristicProcessor == null) {
            sLogger.w("gattCharacteristicProcessor not initialized");
        } else {
            this.gattCharacteristicProcessor.process(new WriteCommand(characteristic, data));
        }
    }

    public DialManager() {
        sLogger.v("[Dial]initializing...");
        if (HudApplication.getApplication() != null) {
            this.bus = RemoteDeviceManager.getInstance().getBus();
            this.sharedPreferences = RemoteDeviceManager.getInstance().getSharedPreferences();
            this.bluetoothManager = (BluetoothManager) HudApplication.getAppContext().getSystemService("bluetooth");
            if (this.bluetoothManager == null) {
                sLogger.e("[Dial]Bluetooth manager not available");
                return;
            }
            this.bluetoothAdapter = this.bluetoothManager.getAdapter();
            if (this.bluetoothAdapter == null) {
                sLogger.e("[Dial]Bluetooth is not available");
                return;
            }
            this.handlerThread = new HandlerThread("DialHandlerThread");
            this.handlerThread.start();
            this.handler = new Handler(this.handlerThread.getLooper());
            if (this.bluetoothAdapter.isEnabled()) {
                sLogger.i("[Dial]Bluetooth is enabled");
                init();
            } else {
                sLogger.i("[Dial]Bluetooth is not enabled, should be booting up, wait for the event");
            }
            startDialEventListener();
            registerReceivers();
        }
    }

    private synchronized void init() {
        sLogger.v("[Dial]Client Address:" + this.bluetoothAdapter.getAddress() + " name:" + this.bluetoothAdapter.getName());
        this.bluetoothLEScanner = this.bluetoothAdapter.getBluetoothLeScanner();
        sLogger.v("[Dial]btle scanner:" + this.bluetoothLEScanner);
        this.handler.post(new Runnable() {
            public void run() {
                if (!DialManager.this.dialManagerInitialized) {
                    DialManager.this.buildDialList();
                    DialManager.this.checkDialConnections();
                    DialManager.this.dialManagerInitialized = true;
                    DialManager.this.bus.post(DialConstants.INIT_EVENT);
                    DialManager.sLogger.v("dial manager initialized");
                }
            }
        });
        this.handler.post(new Runnable() {
            public void run() {
                if (DialManager.this.dialFirmwareUpdater == null) {
                    DialManager.this.dialFirmwareUpdater = new DialFirmwareUpdater(DialManager.this, DialManager.this.handler);
                    DialManager.this.dialFirmwareUpdater.setUpdateListener(new UpdateListener() {
                        public void onUpdateState(boolean available) {
                            DialManager.this.bus.post(new DialUpdateStatus(available));
                        }
                    });
                }
            }
        });
    }

    BluetoothDevice getDialDevice() {
        return this.connectedDial;
    }

    private void buildDialList() {
        try {
            sLogger.v("[Dial]trying to find paired dials");
            Set<BluetoothDevice> bondedDevices = this.bluetoothAdapter.getBondedDevices();
            sLogger.v("[Dial]Bonded devices:" + (bondedDevices == null ? 0 : bondedDevices.size()));
            ArrayList<BluetoothDevice> foundDevices = new ArrayList(8);
            for (BluetoothDevice device : bondedDevices) {
                sLogger.v("[Dial]Bonded Device Address[" + device.getAddress() + "] name[" + device.getName() + " type[" + BluetoothUtils.getDeviceType(device.getType()) + "]");
                if (isDialDeviceName(device.getName())) {
                    sLogger.v("[Dial]found paired dial Address[" + device + "] name[" + device.getName() + "]");
                    foundDevices.add(device);
                }
            }
            if (foundDevices.size() == 0) {
                sLogger.v("[Dial]no dial device found");
                return;
            }
            sLogger.v("[Dial]found paired dials:" + foundDevices.size());
            synchronized (this.bondedDials) {
                this.bondedDials.clear();
                this.bondedDials.addAll(foundDevices);
            }
        } catch (Throwable t) {
            sLogger.e(TAG_DIAL, t);
        }
    }

    private void checkDialConnections() {
        try {
            synchronized (this.bondedDials) {
                Iterator it = this.bondedDials.iterator();
                while (it.hasNext()) {
                    final BluetoothDevice device = (BluetoothDevice) it.next();
                    DialManagerHelper.isDialConnected(this.bluetoothAdapter, device, new IDialConnectionStatus() {
                        public void onStatus(boolean connected) {
                            if (connected) {
                                DialManager.sLogger.v("checkDialConnections: dial " + device.getName() + " connected");
                                if (DialManager.this.connectedDial == null) {
                                    DialManager.this.connectedDial = device;
                                    DialManager.sLogger.v("marking dial [" + device.getName() + "] as connected");
                                    DialManager.this.handleDialConnection();
                                    return;
                                }
                                return;
                            }
                            DialManager.sLogger.v("checkDialConnections: dial " + device.getName() + " not connected");
                        }
                    });
                }
            }
        } catch (Throwable t) {
            sLogger.e(TAG_DIAL, t);
        }
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void handleScannedDevice(BluetoothDevice device, int rssi, ScanRecord scanRecord) {
        if (device.getType() == 2) {
            if (this.isScanning) {
                synchronized (this.scannedNavdyDials) {
                    if (this.scannedNavdyDials.containsKey(device)) {
                    }
                }
            } else {
                sLogger.i("[Dial]not scanning anymore [" + device.getName() + "]");
            }
        }
    }

    public void startScan() {
        try {
            if (this.isScanning) {
                sLogger.v("scan already running");
                return;
            }
            this.handler.removeCallbacks(this.scanHangRunnable);
            this.bondingDial = null;
            synchronized (this.scannedNavdyDials) {
                this.scannedNavdyDials.clear();
                this.ignoredNonNavdyDials.clear();
            }
            this.bluetoothLEScanner.startScan(this.leScanCallback);
            this.handler.postDelayed(this.scanHangRunnable, 30000);
            this.isScanning = true;
        } catch (Throwable t) {
            sLogger.e(t);
        }
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void stopScan(boolean sendEvent) {
        if (this.isScanning) {
            sLogger.v("[Dial]stopping btle scan...");
            this.handler.removeCallbacks(this.scanHangRunnable);
            this.isScanning = false;
            try {
                this.bluetoothLEScanner.stopScan(this.leScanCallback);
            } catch (Throwable t) {
                sLogger.e(t);
            }
            synchronized (this.scannedNavdyDials) {
                this.ignoredNonNavdyDials.clear();
                sLogger.v("[Dial]stopscan dials found:" + this.scannedNavdyDials.size());
                if (this.scannedNavdyDials.size() == 0) {
                    sLogger.i("[Dial]No dials found");
                    if (sendEvent) {
                        this.bus.post(DIAL_NOT_FOUND);
                    }
                } else {
                    Entry<BluetoothDevice, ScanRecord> entry = (Entry) this.scannedNavdyDials.entrySet().iterator().next();
                    BluetoothDevice deviceToBond = (BluetoothDevice) entry.getKey();
                    ScanRecord deviceScanRecord = (ScanRecord) entry.getValue();
                    this.scannedNavdyDials.clear();
                    bondWithDial(deviceToBond, deviceScanRecord);
                    return;
                }
            }
        }
        sLogger.w("[Dial]not currently scanning");
    }

    private void registerReceivers() {
        Context context = HudApplication.getAppContext();
        context.registerReceiver(this.bluetoothBondingReceiver, new IntentFilter("android.bluetooth.device.action.BOND_STATE_CHANGED"));
        IntentFilter intent = new IntentFilter();
        intent.addAction("android.bluetooth.device.action.ACL_CONNECTED");
        intent.addAction("android.bluetooth.device.action.ACL_DISCONNECTED");
        intent.addAction(DialConstants.ACTION_CONNECTION_STATE_CHANGED);
        context.registerReceiver(this.bluetoothConnectivityReceiver, intent);
        context.registerReceiver(this.bluetoothOnOffReceiver, new IntentFilter("android.bluetooth.adapter.action.STATE_CHANGED"));
    }

    private void bondWithDial(final BluetoothDevice device, ScanRecord scanRecord) {
        this.bondTries = 0;
        this.bondingDial = device;
        sLogger.v("[Dial]calling createBond [" + device.getName() + "] addr[" + device.getAddress() + "]" + " rssi[" + scanRecord.getTxPowerLevel() + "] state[" + BluetoothUtils.getBondState(device.getBondState()) + "]");
        boolean removed = removeFromBondList(device);
        if (removed || device.getBondState() == 12) {
            if (removed) {
                sLogger.v("[Dial]present in bond list:" + device.getName() + " invalid state");
            }
            if (device.getBondState() == 12) {
                sLogger.v("[Dial]already bonded:" + device.getName() + " invalid state");
            }
            this.bondingDial = null;
            disconectAndRemoveBond(device, new IDialForgotten() {
                public void onForgotten() {
                    synchronized (DialManager.this.bondedDials) {
                        Iterator<BluetoothDevice> iterator = DialManager.this.bondedDials.iterator();
                        while (iterator.hasNext()) {
                            if (TextUtils.equals(((BluetoothDevice) iterator.next()).getName(), device.getName())) {
                                iterator.remove();
                                break;
                            }
                        }
                    }
                    DialManager.this.bondTries = 0;
                    DialManager.DIAL_NOT_CONNECTED.dialName = device.getName();
                    DialManager.this.bus.post(DialManager.DIAL_NOT_CONNECTED);
                }
            }, 2000);
            return;
        }
        this.bondTries++;
        this.handler.removeCallbacks(this.bondHangRunnable);
        DIAL_CONNECTING.dialName = device.getName();
        this.bus.post(DIAL_CONNECTING);
        device.createBond();
        this.handler.postDelayed(this.bondHangRunnable, 30000);
    }

    private synchronized void startGATTClient() {
        try {
            if (this.bluetoothGatt == null) {
                sLogger.v("[Dial]startGATTClient, launched gatt");
                this.isGattOn = true;
                this.bluetoothGatt = this.connectedDial.connectGatt(HudApplication.getAppContext(), true, this.gattCallback);
            }
        } catch (Throwable t) {
            sLogger.e(TAG_DIAL, t);
            this.isGattOn = false;
        }
        return;
    }

    private synchronized void stopGATTClient() {
        try {
            if (this.bluetoothGatt == null) {
                this.bluetoothGatt = null;
                this.isGattOn = false;
            } else {
                this.handler.removeCallbacks(this.batteryReadRunnable);
                this.bluetoothGatt.close();
                sLogger.v("[Dial]stopGATTClient, gatt closed");
                this.dialFirmwareUpdater.cancelUpdate();
                this.bluetoothGatt = null;
                this.isGattOn = false;
            }
        } catch (Throwable th) {
            this.bluetoothGatt = null;
            this.isGattOn = false;
        }
    }

    private void disconectAndRemoveBond(final BluetoothDevice device, final IDialForgotten callback, final int delay) {
        DialManagerHelper.disconnectFromDial(this.bluetoothAdapter, device, new IDialConnection() {
            public void onAttemptConnection(boolean success) {
            }

            public void onAttemptDisconnection(boolean success) {
                GenericUtil.checkNotOnMainThread();
                DialManager.sLogger.v("attempt dis-connection [" + device.getName() + " ] success[" + success + "] ");
                if (delay > 0) {
                    GenericUtil.sleep(delay);
                }
                DialManager.this.removeBond(device);
                if (callback != null) {
                    callback.onForgotten();
                }
            }
        });
    }

    private void removeBond(BluetoothDevice device) {
        try {
            Method m = device.getClass().getMethod("removeBond", (Class[]) null);
            if (m != null) {
                sLogger.e("[Dial]removingBond [" + device.getName() + "]");
                m.invoke(device, (Object[]) null);
                sLogger.e("[Dial]removedBond");
                return;
            }
            sLogger.e("[Dial]cannot get to removeBond api");
        } catch (Throwable e) {
            sLogger.e(TAG_DIAL, e);
        }
    }

    public void requestDialForgetKeys() {
        try {
            if (this.dialForgetKeysCharacteristic != null) {
                sLogger.v("Queueing dial forget keys request");
                queueWrite(this.dialForgetKeysCharacteristic, DialConstants.DIAL_FORGET_KEYS_MAGIC);
                return;
            }
            sLogger.v("Not queuing dial forget keys request: null");
        } catch (Throwable t) {
            sLogger.e(TAG_DIAL, t);
        }
    }

    public void requestDialReboot(boolean force) {
        if (force || System.currentTimeMillis() - this.sharedPreferences.getLong(DialConstants.DIAL_REBOOT_PROPERTY, 0) >= DialConstants.MIN_TIME_BETWEEN_REBOOTS) {
            try {
                if (this.dialRebootCharacteristic != null) {
                    sLogger.v("Queueing dial reboot request");
                    queueWrite(this.dialRebootCharacteristic, DialConstants.DIAL_REBOOT_MAGIC);
                    return;
                }
                sLogger.v("Not queuing dial reboot request: null");
                return;
            } catch (Throwable t) {
                sLogger.e(TAG_DIAL, t);
                return;
            }
        }
        sLogger.v("Not rebooting dial, last reboot too recent.");
    }

    public NotificationReason getBatteryNotificationReason() {
        return this.notificationReasonBattery;
    }

    public static NotificationReason getBatteryLevelCategory(int level) {
        if (level > 50 && level <= 80) {
            return NotificationReason.LOW_BATTERY;
        }
        if (level > 10 && level <= 50) {
            return NotificationReason.VERY_LOW_BATTERY;
        }
        if (level <= 10) {
            return NotificationReason.EXTREMELY_LOW_BATTERY;
        }
        return NotificationReason.OK_BATTERY;
    }

    public static int getDisplayBatteryLevel(int level) {
        return (int) (((float) level) * 0.1f);
    }

    private void processBatteryLevel(int level) {
        NotificationReason reason = getBatteryLevelCategory(level);
        this.lastKnownBatteryLevel = level;
        sLogger.v("battery status current[" + reason + "] previous[" + this.notificationReasonBattery + "]");
        if (reason == NotificationReason.OK_BATTERY) {
            if (!(this.notificationReasonBattery == null || this.notificationReasonBattery == NotificationReason.OK_BATTERY)) {
                this.notificationReasonBattery = null;
            }
            DialNotification.dismissAllBatteryToasts();
        } else if (this.notificationReasonBattery == reason) {
            sLogger.v("battery no change");
        } else {
            switch (reason) {
                case EXTREMELY_LOW_BATTERY:
                    sLogger.v("battery notification change");
                    this.notificationReasonBattery = reason;
                    return;
                case LOW_BATTERY:
                    if (this.notificationReasonBattery == NotificationReason.EXTREMELY_LOW_BATTERY || this.notificationReasonBattery == NotificationReason.VERY_LOW_BATTERY) {
                        sLogger.v("battery threshold change-ignore");
                        return;
                    }
                    sLogger.v("battery notification change");
                    this.notificationReasonBattery = reason;
                    return;
                case VERY_LOW_BATTERY:
                    if (this.notificationReasonBattery == NotificationReason.EXTREMELY_LOW_BATTERY) {
                        sLogger.v("battery threshold change-ignore");
                        return;
                    }
                    sLogger.v("battery notification change");
                    this.notificationReasonBattery = reason;
                    return;
                default:
                    return;
            }
        }
    }

    private void processRawBatteryLevel(Integer level) {
        this.lastKnownRawBatteryLevel = level;
    }

    private void processSystemTemperature(Integer level) {
        this.lastKnownSystemTemperature = level;
    }

    public boolean isInitialized() {
        return this.dialManagerInitialized;
    }

    private void handleDialConnection() {
        this.encryptionRetryCount = 0;
        ToastManager toastManager = ToastManager.getInstance();
        DialNotification.dismissAllBatteryToasts();
        toastManager.dismissCurrentToast(DialNotification.DIAL_DISCONNECT_ID);
        toastManager.clearPendingToast(DialNotification.DIAL_DISCONNECT_ID);
        DIAL_CONNECTED.dialName = getDialName();
        this.bus.post(DIAL_CONNECTED);
        startGATTClient();
    }

    public int getLastKnownBatteryLevel() {
        return this.lastKnownBatteryLevel;
    }

    public Integer getLastKnownRawBatteryLevel() {
        return this.lastKnownRawBatteryLevel;
    }

    public Integer getLastKnownSystemTemperature() {
        return this.lastKnownSystemTemperature;
    }

    public String getDialName() {
        if (this.connectedDial != null) {
            return this.connectedDial.getName();
        }
        return null;
    }

    public boolean reportInputEvent(KeyEvent event) {
        InputDevice device = event.getDevice();
        if (device == null || device.isVirtual() || !isDialDeviceName(device.getName())) {
            return false;
        }
        if (isDialConnected()) {
            BaseScreen screen = RemoteDeviceManager.getInstance().getUiStateManager().getCurrentScreen();
            if (screen != null && screen.getScreen() == Screen.SCREEN_DIAL_PAIRING) {
                sLogger.v("reportInputEvent: sent connected event");
                DIAL_CONNECTED.dialName = getDialName();
                this.bus.post(DIAL_CONNECTED);
            }
        } else {
            sLogger.v("reportInputEvent:got dial connection via input:" + device.getName());
            this.connectedDial = getBluetoothDevice(device.getName());
            this.handler.removeCallbacks(this.connectionErrorRunnable);
            this.handler.removeCallbacks(this.bondHangRunnable);
            sLogger.v("reportInputEvent remove connectionErrorRunnable");
            if (this.connectedDial != null) {
                sLogger.v("reportInputEvent: got device from bonded list");
                handleDialConnection();
            } else {
                sLogger.i("reportInputEvent: did not get device from bonded list");
                buildDialList();
                this.connectedDial = getBluetoothDevice(device.getName());
                if (this.connectedDial != null) {
                    sLogger.v("reportInputEvent: got device from new bonded list");
                    handleDialConnection();
                } else {
                    sLogger.v("reportInputEvent: did not get device from new bonded list");
                }
            }
        }
        return true;
    }

    public DialFirmwareUpdater getDialFirmwareUpdater() {
        return this.dialFirmwareUpdater;
    }

    public String getFirmWareVersion() {
        return this.firmWareVersion;
    }

    public String getHardwareVersion() {
        return this.hardwareVersion;
    }

    public boolean isDialConnected() {
        return this.connectedDial != null;
    }

    private void clearConnectedDial() {
        this.firmWareVersion = null;
        this.hardwareVersion = null;
        this.lastKnownBatteryLevel = -1;
        this.lastKnownRawBatteryLevel = null;
        this.lastKnownSystemTemperature = null;
        this.connectedDial = null;
    }

    public boolean isDialPaired() {
        return getBondedDialCount() > 0;
    }

    public int getBondedDialCount() {
        int size;
        synchronized (this.bondedDials) {
            size = this.bondedDials.size();
        }
        return size;
    }

    public String getBondedDialList() {
        StringBuilder builder = new StringBuilder();
        synchronized (this.bondedDials) {
            int len = this.bondedDials.size();
            for (int i = 0; i < len; i++) {
                builder.append(((BluetoothDevice) this.bondedDials.get(i)).getName());
                if (i + 1 < len) {
                    builder.append(", ");
                }
            }
        }
        return builder.toString();
    }

    private BluetoothDevice getBluetoothDevice(String name) {
        synchronized (this.bondedDials) {
            Iterator it = this.bondedDials.iterator();
            while (it.hasNext()) {
                BluetoothDevice device = (BluetoothDevice) it.next();
                if (TextUtils.equals(device.getName(), name)) {
                    return device;
                }
            }
            return null;
        }
    }

    public boolean isDialDevice(BluetoothDevice device) {
        if (device == null) {
            return false;
        }
        String name = device.getName();
        if (name == null || !isDialDeviceName(name)) {
            return false;
        }
        return true;
    }

    public static boolean isDialDeviceName(String name) {
        if (name == null) {
            return false;
        }
        for (CharSequence contains : DialConstants.NAVDY_DIAL_FILTER) {
            if (name.contains(contains)) {
                return true;
            }
        }
        return false;
    }

    public void forgetAllDials(boolean showToast, final IDialForgotten callback) {
        boolean z = true;
        int count = getBondedDialCount();
        String dialName = getBondedDialList();
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                DialManager.this.forgetAllDials(callback);
            }
        }, 1);
        if (showToast) {
            ToastManager toastManager = ToastManager.getInstance();
            toastManager.dismissCurrentToast();
            toastManager.clearAllPendingToast();
            toastManager.disableToasts(false);
            if (count <= 1) {
                z = false;
            }
            DialNotification.showForgottenToast(z, dialName);
        }
    }

    public void forgetDial(BluetoothDevice dial) {
        removeBond(dial);
        removeFromBondList(dial);
    }

    public void forgetAllDials(final IDialForgotten callback) {
        requestDialForgetKeys();
        this.handler.removeCallbacks(this.bondHangRunnable);
        synchronized (this.bondedDials) {
            sLogger.v("forget all dials:" + this.bondedDials.size());
            ArrayList<BluetoothDevice> copy = new ArrayList(this.bondedDials);
            this.bondedDials.clear();
            clearConnectedDial();
            final AtomicInteger counter = new AtomicInteger(copy.size());
            Iterator it = copy.iterator();
            while (it.hasNext()) {
                disconectAndRemoveBond((BluetoothDevice) it.next(), new IDialForgotten() {
                    public void onForgotten() {
                        int val = counter.decrementAndGet();
                        DialManager.sLogger.v("forgetAllDials counter:" + val);
                        if (val == 0) {
                            DialManager.DIAL_NOT_CONNECTED.dialName = null;
                            DialManager.this.bus.post(DialManager.DIAL_NOT_CONNECTED);
                            if (callback != null) {
                                callback.onForgotten();
                            }
                        }
                    }
                }, 2000);
            }
        }
    }

    public boolean isScanning() {
        return this.isScanning;
    }

    private boolean addtoBondList(BluetoothDevice device) {
        boolean z = false;
        if (device != null) {
            synchronized (this.bondedDials) {
                boolean found = false;
                Iterator it = this.bondedDials.iterator();
                while (it.hasNext()) {
                    if (((BluetoothDevice) it.next()).equals(device)) {
                        found = true;
                    }
                }
                if (found) {
                } else {
                    this.bondedDials.add(device);
                    sLogger.v("bonded device added to list [" + device.getName() + "]");
                    z = true;
                }
            }
        }
        return z;
    }

    private boolean removeFromBondList(BluetoothDevice device) {
        boolean z = false;
        if (device != null) {
            synchronized (this.bondedDials) {
                boolean found = false;
                Iterator it = this.bondedDials.iterator();
                while (it.hasNext()) {
                    if (((BluetoothDevice) it.next()).equals(device)) {
                        found = true;
                    }
                }
                if (found) {
                    this.bondedDials.remove(device);
                    z = true;
                }
            }
        }
        return z;
    }

    private boolean isPresentInBondList(BluetoothDevice device) {
        boolean z = false;
        if (device != null) {
            synchronized (this.bondedDials) {
                Iterator it = this.bondedDials.iterator();
                while (it.hasNext()) {
                    if (((BluetoothDevice) it.next()).equals(device)) {
                        z = true;
                        break;
                    }
                }
            }
        }
        return z;
    }

    private synchronized void startDialEventListener() {
        if (this.dialEventsListenerThread == null) {
            this.dialEventsListenerThread = new Thread(new Runnable() {
                public void run() {
                    Throwable t;
                    DatagramSocket socket = null;
                    try {
                        DialManager.sLogger.v("DialEventsListener thread enter");
                        DatagramSocket socket2 = new DatagramSocket(23654, InetAddress.getByName("127.0.0.1"));
                        try {
                            byte[] data = new byte[56];
                            DatagramPacket packet = new DatagramPacket(data, data.length);
                            while (true) {
                                socket2.receive(packet);
                                String str = new String(data, 0, packet.getLength());
                                DialManager.sLogger.v("[DialEventsListener] [" + str + "]");
                                int index = str.indexOf(HereManeuverDisplayBuilder.COMMA);
                                if (index >= 0) {
                                    String event = str.substring(0, index);
                                    String addr = str.substring(index + 1).toUpperCase();
                                    DialManager.this.bluetoothAdapter;
                                    if (BluetoothAdapter.checkBluetoothAddress(addr)) {
                                        BluetoothDevice bluetoothDevice = DialManager.this.bluetoothAdapter.getRemoteDevice(addr);
                                        DialManager.sLogger.v("[DialEventsListener] event for [" + bluetoothDevice + "]");
                                        if (DialManager.this.isPresentInBondList(bluetoothDevice)) {
                                            Object obj = -1;
                                            switch (event.hashCode()) {
                                                case -1425556143:
                                                    if (event.equals(DialConstants.DIAL_EVENT_INPUT_REPORT_DESCRIPTOR)) {
                                                        obj = 3;
                                                        break;
                                                    }
                                                    break;
                                                case -1324212103:
                                                    if (event.equals("ENCRYPTION_FAILED")) {
                                                        obj = 2;
                                                        break;
                                                    }
                                                    break;
                                                case 1015497884:
                                                    if (event.equals("DISCONNECT")) {
                                                        obj = 1;
                                                        break;
                                                    }
                                                    break;
                                                case 1669334218:
                                                    if (event.equals("CONNECT")) {
                                                        obj = null;
                                                        break;
                                                    }
                                                    break;
                                            }
                                            switch (obj) {
                                                case null:
                                                    DialManager.this.connectEvent(bluetoothDevice);
                                                    break;
                                                case 1:
                                                    DialManager.this.disconnectEvent(bluetoothDevice);
                                                    break;
                                                case 2:
                                                    DialManager.this.encryptionFailedEvent(bluetoothDevice);
                                                    break;
                                                case 3:
                                                    DialManager.this.handleInputReportDescriptor(bluetoothDevice);
                                                    break;
                                                default:
                                                    break;
                                            }
                                        }
                                        DialManager.sLogger.v("[DialEventsListener] " + addr + " not present in bond list, ignore");
                                    } else {
                                        DialManager.sLogger.e("[DialEventsListener] not a valid bluetooth address");
                                    }
                                } else {
                                    DialManager.sLogger.v("[DialEventsListener] invalid data");
                                }
                            }
                        } catch (Throwable th) {
                            t = th;
                            socket = socket2;
                        }
                    } catch (Throwable th2) {
                        t = th2;
                        DialManager.sLogger.e(t);
                        if (socket != null) {
                            IOUtils.closeStream(socket);
                        }
                        DialManager.sLogger.v("DialEventsListener thread exit");
                    }
                }
            });
            this.dialEventsListenerThread.setName("dialEventListener");
            this.dialEventsListenerThread.start();
            sLogger.v("dialEventListener thread started");
        }
    }

    private void connectEvent(BluetoothDevice device) {
        sLogger.v("[DialEvent-connect] " + device);
        this.handler.removeCallbacks(this.bondHangRunnable);
        this.handler.removeCallbacks(this.connectionErrorRunnable);
        if (this.connectedDial != null) {
            sLogger.v("[DialEvent-connect] ignore already connected dial:" + this.connectedDial);
        } else if (isPresentInBondList(device)) {
            sLogger.v("[DialEvent-connect] not on dial pairing screen, mark as connected");
            this.connectedDial = device;
            handleDialConnection();
        } else {
            sLogger.v("launch connectionErrorRunnable");
            this.tempConnectedDial = device;
            this.handler.postDelayed(this.connectionErrorRunnable, 5000);
        }
    }

    private void handleInputReportDescriptor(BluetoothDevice device) {
        this.handler.removeCallbacks(this.connectionErrorRunnable);
        sLogger.v("[DialEvent-handleInputReportDescriptor] " + device);
        if (this.tempConnectedDial == null) {
            sLogger.v("[DialEvent-handleInputReportDescriptor] no temp connected dial");
        } else if (this.connectedDial != null) {
            sLogger.v("[DialEvent-handleInputReportDescriptor] dial already connected[" + this.connectedDial + "]");
            if (this.connectedDial.equals(device)) {
                DIAL_CONNECTED.dialName = getDialName();
                this.bus.post(DIAL_CONNECTED);
            }
        } else {
            sLogger.v("[DialEvent-handleInputReportDescriptor] done");
            this.tempConnectedDial = null;
            this.connectedDial = device;
            handleDialConnection();
        }
    }

    private void disconnectEvent(BluetoothDevice device) {
        this.handler.removeCallbacks(this.bondHangRunnable);
        this.handler.removeCallbacks(this.connectionErrorRunnable);
        this.tempConnectedDial = null;
        if (this.connectedDial == null) {
            sLogger.v("[DialEvent-disconnect] no connected dial");
        } else if (this.connectedDial.equals(device)) {
            this.disconnectedDial = this.connectedDial.getName();
            clearConnectedDial();
            stopGATTClient();
            DIAL_NOT_CONNECTED.dialName = this.disconnectedDial;
            this.bus.post(DIAL_NOT_CONNECTED);
            sLogger.v("[DialEvent-disconnect] dial marked not connected");
        } else {
            sLogger.v("[DialEvent-disconnect] notif not for connected dial connected[" + this.connectedDial + "]");
        }
    }

    private void encryptionFailedEvent(final BluetoothDevice device) {
        if (!isPresentInBondList(device)) {
            this.encryptionRetryCount = 0;
            sLogger.e("[DialEventsListener] device not in bonded dial list");
        } else if (this.encryptionRetryCount < 2) {
            this.encryptionRetryCount++;
            sLogger.e("[DialEvent-encryptfail] Attempt " + this.encryptionRetryCount + " will retry.");
            stopGATTClient();
            GenericUtil.sleep(2000);
            DialManagerHelper.disconnectFromDial(this.bluetoothAdapter, device, new IDialConnection() {
                public void onAttemptConnection(boolean success) {
                }

                public void onAttemptDisconnection(boolean success) {
                    DialManagerHelper.sendLocalyticsEvent(DialManager.this.handler, false, false, 0, false, "Encryption failed, retrying");
                }
            });
        } else {
            removeFromBondList(device);
            sLogger.e("[DialEvent-encryptfail] device found in bonded dial list, stop gatt");
            stopGATTClient();
            sLogger.e("[DialEvent-encryptfail] sleeping");
            GenericUtil.sleep(2000);
            sLogger.e("[DialEvent-encryptfail] sleep");
            DialManagerHelper.disconnectFromDial(this.bluetoothAdapter, device, new IDialConnection() {
                public void onAttemptConnection(boolean success) {
                }

                public void onAttemptDisconnection(boolean success) {
                    DialManager.sLogger.e("[DialEvent-encryptfail] disconnected");
                    DialManager.this.handler.postDelayed(new Runnable() {
                        public void run() {
                            DialManager.this.forgetDial(device);
                            DialManager.sLogger.e("[DialEvent-encryptfail] bond removed :-/ connected[" + DialManager.this.connectedDial + "] event[" + device + "]");
                            if (device.equals(DialManager.this.connectedDial)) {
                                DialManager.this.disconnectedDial = DialManager.this.connectedDial.getName();
                                DialManager.this.clearConnectedDial();
                                DialManager.DIAL_CONNECTION_FAILED.dialName = DialManager.this.disconnectedDial;
                                DialManager.this.bus.post(DialManager.DIAL_CONNECTION_FAILED);
                                DialManager.sLogger.e("[DialEvent-encryptfail] current dial is disconnected");
                            }
                            DialManagerHelper.sendLocalyticsEvent(DialManager.this.handler, false, false, 0, false, "Encryption failed, deleting bond");
                        }
                    }, 3000);
                }
            });
        }
    }
}
