package com.navdy.hud.app.device.gps;

import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.HandlerThread;
import android.os.Message;
import android.os.SystemClock;
import android.text.TextUtils;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.R;
import com.navdy.hud.app.analytics.AnalyticsSupport;
import com.navdy.hud.app.event.DrivingStateChange;
import com.navdy.hud.app.framework.glance.GlanceConstants;
import com.navdy.hud.app.framework.voice.TTSUtils;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.manager.SpeedManager;
import com.navdy.hud.app.manager.SpeedManager.SpeedUnit;
import com.navdy.hud.app.obd.ObdManager;
import com.navdy.hud.app.obd.ObdManager.ObdConnectionStatusEvent;
import com.navdy.hud.app.obd.ObdManager.ObdSupportedPidsChangedEvent;
import com.navdy.hud.app.service.pandora.messages.BaseMessage;
import com.navdy.hud.app.storage.db.helper.VinInformationHelper;
import com.navdy.hud.app.storage.db.table.VinInformationTable;
import com.navdy.hud.app.util.GForceRawSensorDataProcessor;
import com.navdy.hud.app.util.GenericUtil;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.util.IOUtils;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.concurrent.atomic.AtomicInteger;
import org.json.JSONObject;

public class GpsDeadReckoningManager implements Runnable {
    private static final int ALIGNMENT_DONE = 3;
    private static final int ALIGNMENT_PACKET_LENGTH = 24;
    private static final int ALIGNMENT_PACKET_PAYLOAD_LENGTH = 16;
    private static final byte[] ALIGNMENT_PATTERN = new byte[]{(byte) -75, (byte) 98, (byte) 16, BaseMessage.PNDR_GET_TRACK_ALBUM_ART};
    private static final long ALIGNMENT_POLL_FREQUENCY = 30000;
    private static final String ALIGNMENT_TIME = "alignment_time";
    private static final byte[] AUTO_ALIGNMENT = new byte[]{(byte) -75, (byte) 98, (byte) 6, (byte) 86, (byte) 12, (byte) 0, (byte) 0, (byte) 1, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 105, (byte) 29};
    private static final byte[] CFG_ESF_RAW = new byte[]{(byte) -75, (byte) 98, (byte) 6, (byte) 1, (byte) 3, (byte) 0, (byte) 16, (byte) 3, (byte) 1, (byte) 0, (byte) 0};
    private static final byte[] CFG_ESF_RAW_OFF = new byte[]{(byte) -75, (byte) 98, (byte) 6, (byte) 1, (byte) 3, (byte) 0, (byte) 16, (byte) 3, (byte) 0, (byte) 0, (byte) 0};
    private static final byte[] CFG_RST_COLD = new byte[]{(byte) -75, (byte) 98, (byte) 6, (byte) 4, (byte) 4, (byte) 0, (byte) -1, (byte) -1, (byte) 2, (byte) 0, (byte) 14, (byte) 97};
    private static final byte[] CFG_RST_WARM = new byte[]{(byte) -75, (byte) 98, (byte) 6, (byte) 4, (byte) 4, (byte) 0, (byte) 1, (byte) 0, (byte) 2, (byte) 0, (byte) 17, (byte) 108};
    private static final byte DEAD_RECKONING_ONLY = (byte) 1;
    private static final int ESF_PACKET_LENGTH = 19;
    private static final byte[] ESF_RAW_PATTERN = new byte[]{(byte) -75, (byte) 98, (byte) 16, (byte) 3};
    private static final byte[] ESF_STATUS_PATTERN = new byte[]{(byte) -75, (byte) 98, (byte) 16, (byte) 16};
    private static final int FAILURE_RETRY_INTERVAL = 10000;
    private static final byte FIX_2D = (byte) 2;
    private static final byte FIX_3D = (byte) 3;
    private static final int FUSION_DONE = 1;
    private static final byte[] GET_ALIGNMENT = new byte[]{(byte) -75, (byte) 98, (byte) 16, BaseMessage.PNDR_GET_TRACK_ALBUM_ART, (byte) 0, (byte) 0, (byte) 36, (byte) 124};
    private static final byte[] GET_ESF_STATUS = new byte[]{(byte) -75, (byte) 98, (byte) 16, (byte) 16, (byte) 0, (byte) 0, (byte) 32, (byte) 112};
    private static final String GPS = "gps";
    private static final byte GPS_DEAD_RECKONING_COMBINED = (byte) 4;
    private static final String GPS_LOG = "gps.log";
    private static final char[] HEX = new char[]{'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
    private static final byte[] INJECT_OBD_SPEED = new byte[]{(byte) -75, (byte) 98, (byte) 16, (byte) 2, (byte) 12, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 11, (byte) 0, (byte) 0};
    private static final int MSG_ALIGNMENT_VALUE = 1;
    private static final byte[] NAV_STATUS_PATTERN = new byte[]{(byte) -75, (byte) 98, (byte) 1, (byte) 3};
    private static final byte NO_FIX = (byte) 0;
    private static final byte[] PASSPHRASE = new byte[]{(byte) 110, (byte) 97, (byte) 118, (byte) 100, (byte) 121};
    private static final String PITCH = "pitch";
    private static final long POLL_FREQUENCY = 10000;
    private static final byte[] READ_BUF = new byte[8192];
    private static final String ROLL = "roll";
    private static final int SPEED_INJECTION_FREQUENCY = 100;
    private static final int SPEED_TIME_TAG_COUNTER = 100;
    private static final String TCP_SERVER_HOST = "127.0.0.1";
    private static final int TCP_SERVER_PORT = 42434;
    private static final byte[] TEMP_BUF = new byte[128];
    private static final AtomicInteger THREAD_COUNTER = new AtomicInteger(1);
    private static final byte TIME_ONLY = (byte) 5;
    private static final String YAW = "yaw";
    private static final GpsDeadReckoningManager sInstance = new GpsDeadReckoningManager();
    private static final Logger sLogger = new Logger(GpsDeadReckoningManager.class);
    private volatile boolean alignmentChecked;
    private Alignment alignmentInfo;
    private final Bus bus;
    private volatile boolean deadReckoningInjectionStarted;
    private boolean deadReckoningOn;
    private byte deadReckoningType = (byte) -1;
    private Runnable enableEsfRunnable = new Runnable() {
        public void run() {
            GpsDeadReckoningManager.this.invokeUblox(GpsDeadReckoningManager.CFG_ESF_RAW, "ESF-RAW");
            GpsDeadReckoningManager.this.sensorDataProcessor.setCalibrated(false);
            GpsDeadReckoningManager.this.bus.post(new CalibratedGForceData(0.0f, 0.0f, 0.0f));
        }
    };
    private final Runnable getAlignmentRunnable = new Runnable() {
        public void run() {
            if (GpsDeadReckoningManager.this.invokeUblox(GpsDeadReckoningManager.GET_ALIGNMENT, "get alignment")) {
                GpsDeadReckoningManager.this.handler.removeCallbacks(GpsDeadReckoningManager.this.getAlignmentRunnableRetry);
                GpsDeadReckoningManager.this.handler.postDelayed(GpsDeadReckoningManager.this.getAlignmentRunnableRetry, 30000);
                return;
            }
            GpsDeadReckoningManager.this.postAlignmentRunnable(true);
        }
    };
    private final Runnable getAlignmentRunnableRetry = new Runnable() {
        public void run() {
            GpsDeadReckoningManager.sLogger.v("getAlignmentRunnableRetry");
            GpsDeadReckoningManager.this.getAlignmentRunnable.run();
        }
    };
    private final Runnable getFusionStatusRunnable = new Runnable() {
        public void run() {
            if (GpsDeadReckoningManager.this.invokeUblox(GpsDeadReckoningManager.GET_ESF_STATUS, "get fusion status")) {
                GpsDeadReckoningManager.this.handler.removeCallbacks(GpsDeadReckoningManager.this.getFusionStatusRunnableRetry);
                GpsDeadReckoningManager.this.handler.postDelayed(GpsDeadReckoningManager.this.getFusionStatusRunnableRetry, GpsDeadReckoningManager.POLL_FREQUENCY);
                return;
            }
            GpsDeadReckoningManager.this.postFusionRunnable(true);
        }
    };
    private final Runnable getFusionStatusRunnableRetry = new Runnable() {
        public void run() {
            GpsDeadReckoningManager.sLogger.v("getFusionStatusRunnableRetry");
            GpsDeadReckoningManager.this.getFusionStatusRunnable.run();
        }
    };
    private Handler handler;
    private HandlerThread handlerThread;
    private final Runnable injectRunnable = new Runnable() {
        public void run() {
            GpsDeadReckoningManager.this.invokeUblox(GpsDeadReckoningManager.this.injectSpeed);
            if (GpsDeadReckoningManager.this.deadReckoningInjectionStarted && GpsDeadReckoningManager.this.lastConnectionFailure == 0) {
                GpsDeadReckoningManager.this.handler.postDelayed(this, 100);
            } else {
                GpsDeadReckoningManager.this.handler.removeCallbacks(this);
            }
        }
    };
    private final CommandWriter injectSpeed = new CommandWriter() {
        public void run() throws IOException {
            GpsDeadReckoningManager.this.sendObdSpeed();
        }
    };
    private InputStream inputStream;
    private long lastConnectionFailure;
    private OutputStream outputStream;
    private Runnable resetRunnable = new Runnable() {
        public void run() {
            GpsDeadReckoningManager.this.invokeUblox(GpsDeadReckoningManager.CFG_RST_WARM, "RST-WARM (warm reset)");
        }
    };
    private JSONObject rootInfo;
    private volatile boolean runThread;
    private GForceRawSensorDataProcessor sensorDataProcessor;
    private Socket socket;
    private final SpeedManager speedManager = SpeedManager.getInstance();
    private Thread thread;
    private int timeStampCounter = 0;
    private boolean waitForAutoAlignment;
    private boolean waitForFusionStatus;

    interface CommandWriter {
        void run() throws IOException;
    }

    private static class Alignment {
        boolean done;
        float pitch;
        float roll;
        float yaw;

        Alignment(float yaw, float pitch, float roll, boolean done) {
            this.yaw = yaw;
            this.pitch = pitch;
            this.roll = roll;
            this.done = done;
        }

        public String toString() {
            StringBuilder sb = new StringBuilder("Alignment{");
            sb.append("yaw=").append(this.yaw);
            sb.append(", pitch=").append(this.pitch);
            sb.append(", roll=").append(this.roll);
            sb.append(", done=").append(this.done);
            sb.append('}');
            return sb.toString();
        }
    }

    static {
        calculateChecksum(CFG_ESF_RAW);
        calculateChecksum(CFG_ESF_RAW_OFF);
    }

    public static GpsDeadReckoningManager getInstance() {
        return sInstance;
    }

    private GpsDeadReckoningManager() {
        sLogger.v("ctor()");
        this.sensorDataProcessor = new GForceRawSensorDataProcessor();
        this.handlerThread = new HandlerThread("gpsDeadReckoningHandler");
        this.handlerThread.start();
        this.handler = new Handler(this.handlerThread.getLooper(), new Callback() {
            public boolean handleMessage(Message msg) {
                switch (msg.what) {
                    case 1:
                        try {
                            GpsDeadReckoningManager.this.handleAutoAlignmentResult(msg.obj);
                            break;
                        } catch (Throwable t) {
                            GpsDeadReckoningManager.sLogger.e(t);
                            if (GpsDeadReckoningManager.this.waitForAutoAlignment) {
                                if (GpsDeadReckoningManager.this.waitForFusionStatus) {
                                    GpsDeadReckoningManager.this.postFusionRunnable(true);
                                    break;
                                }
                            }
                            GpsDeadReckoningManager.this.postAlignmentRunnable(true);
                            break;
                        }
                        break;
                }
                return false;
            }
        });
        ObdManager obdManager = ObdManager.getInstance();
        if (obdManager.isConnected()) {
            sLogger.v("ctor() obd is connected");
            if (obdManager.isSpeedPidAvailable()) {
                sLogger.v("ctor() speed pid is available");
                checkAlignment();
                startDeadReckoning();
            } else {
                sLogger.v("ctor() speed pid is not available");
            }
        } else {
            sLogger.v("ctor() obd is not connected, waiting for obd to connect");
        }
        this.bus = RemoteDeviceManager.getInstance().getBus();
        this.bus.register(this);
        sLogger.v("ctor() initialized");
        this.handler.post(this.enableEsfRunnable);
    }

    private boolean invokeUblox(final byte[] message, final String description) {
        return invokeUblox(new CommandWriter() {
            public void run() throws IOException {
                if (GpsDeadReckoningManager.this.outputStream != null) {
                    if (description != null) {
                        GpsDeadReckoningManager.sLogger.v(description + " [" + GpsDeadReckoningManager.bytesToHex(message, 0, message.length) + "]");
                    }
                    GpsDeadReckoningManager.this.outputStream.write(message);
                    return;
                }
                throw new IOException("Disconnected socket - failed to " + description);
            }
        });
    }

    private boolean invokeUblox(CommandWriter callback) {
        boolean succeeded = false;
        try {
            if (this.lastConnectionFailure > 0) {
                long diff = SystemClock.elapsedRealtime() - this.lastConnectionFailure;
                if (diff < POLL_FREQUENCY) {
                    if (sLogger.isLoggable(2)) {
                        sLogger.v("would retry time[" + diff + "]");
                    }
                    return false;
                }
            }
            if (connectSocket()) {
                this.lastConnectionFailure = 0;
                callback.run();
                succeeded = true;
            } else {
                this.lastConnectionFailure = SystemClock.elapsedRealtime();
            }
        } catch (IOException e) {
            sLogger.e("Failed to ");
            closeSocket();
            this.lastConnectionFailure = SystemClock.elapsedRealtime();
        }
        return succeeded;
    }

    @Subscribe
    public void onSupportedPidEventsChange(ObdSupportedPidsChangedEvent event) {
        initDeadReckoning();
    }

    @Subscribe
    public void ObdConnectionStatusEvent(ObdConnectionStatusEvent event) {
        if (event.connected) {
            sLogger.v("got obd connected");
            initDeadReckoning();
            return;
        }
        sLogger.v("got obd dis-connected");
        stopDeadReckoning();
    }

    private void initDeadReckoning() {
        if (ObdManager.getInstance().isSpeedPidAvailable()) {
            sLogger.v("speed pid is available");
            checkAlignment();
            startDeadReckoning();
            return;
        }
        sLogger.v("speed pid is not available");
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void closeSocket() {
        this.runThread = false;
        IOUtils.closeStream(this.inputStream);
        IOUtils.closeStream(this.outputStream);
        IOUtils.closeStream(this.socket);
        this.inputStream = null;
        this.outputStream = null;
        this.socket = null;
        if (this.thread != null) {
            try {
                if (this.thread.isAlive()) {
                    this.thread.interrupt();
                    sLogger.v("waiting for thread");
                    this.thread.join();
                    sLogger.v("waited");
                } else {
                    sLogger.v("thread is not alive");
                }
                this.thread = null;
            } catch (Throwable th) {
                this.thread = null;
            }
        }
        this.deadReckoningOn = false;
        this.deadReckoningType = (byte) -1;
    }

    private boolean connectSocket() {
        try {
            if (this.socket != null) {
                return true;
            }
            this.socket = new Socket(TCP_SERVER_HOST, TCP_SERVER_PORT);
            this.inputStream = this.socket.getInputStream();
            this.outputStream = this.socket.getOutputStream();
            sLogger.v("connected to 42434");
            this.outputStream.write(PASSPHRASE);
            sLogger.v("sent passphrase");
            this.thread = new Thread(this);
            this.thread.setName("GpsDeadReckoningManager-" + THREAD_COUNTER.getAndIncrement());
            this.runThread = true;
            this.thread.start();
            GenericUtil.sleep(2000);
            return true;
        } catch (Throwable t) {
            sLogger.e(t);
            closeSocket();
            return false;
        }
    }

    private void sendObdSpeed() throws IOException {
        boolean verbose = sLogger.isLoggable(2);
        long speed = (long) this.speedManager.getRawObdSpeed();
        if (speed >= 0) {
            this.timeStampCounter += 100;
            if (this.timeStampCounter < 0) {
                this.timeStampCounter = 100;
            }
            INJECT_OBD_SPEED[6] = (byte) (this.timeStampCounter >> 0);
            INJECT_OBD_SPEED[7] = (byte) (this.timeStampCounter >> 8);
            INJECT_OBD_SPEED[8] = (byte) (this.timeStampCounter >> 16);
            INJECT_OBD_SPEED[9] = (byte) (this.timeStampCounter >> 24);
            speed = (long) ((int) (SpeedManager.convertWithPrecision((double) speed, SpeedUnit.KILOMETERS_PER_HOUR, SpeedUnit.METERS_PER_SECOND) * 1000.0f));
            INJECT_OBD_SPEED[14] = (byte) ((int) (speed >> 0));
            INJECT_OBD_SPEED[15] = (byte) ((int) (speed >> 8));
            INJECT_OBD_SPEED[16] = (byte) ((int) (speed >> 16));
            calculateChecksum(INJECT_OBD_SPEED);
            if (verbose) {
                sLogger.v("inject obd speed [" + bytesToHex(INJECT_OBD_SPEED, 0, INJECT_OBD_SPEED.length) + "]");
            }
            this.outputStream.write(INJECT_OBD_SPEED);
        } else if (verbose) {
            sLogger.i("invalid obd speed:" + speed);
        }
    }

    private static void calculateChecksum(byte[] message) {
        int length = message.length;
        byte ck_a = (byte) 0;
        byte ck_b = (byte) 0;
        for (int i = 2; i < length - 2; i++) {
            ck_a = (byte) (message[i] + ck_a);
            ck_b = (byte) (ck_b + ck_a);
        }
        message[length - 2] = ck_a;
        message[length - 1] = ck_b;
    }

    private static String bytesToHex(byte[] bytes, int offset, int count) {
        char[] hexChars = new char[(count * 2)];
        for (int i = 0; i < count; i++) {
            int v = bytes[i + offset] & 255;
            hexChars[i * 2] = HEX[v >>> 4];
            hexChars[(i * 2) + 1] = HEX[v & 15];
        }
        return new String(hexChars);
    }

    public void run() {
        sLogger.v("start thread");
        while (this.runThread) {
            try {
                int nread = this.inputStream.read(READ_BUF);
                if (nread == -1) {
                    sLogger.v("eof");
                    this.runThread = false;
                } else if (nread > 0) {
                    int index = GenericUtil.indexOf(READ_BUF, ESF_RAW_PATTERN, 0, nread - 1);
                    if (index != -1) {
                        handleEsfRaw(index, nread);
                    } else {
                        index = GenericUtil.indexOf(READ_BUF, NAV_STATUS_PATTERN, 0, nread - 1);
                        if (index != -1) {
                            handleNavStatus(index, nread);
                        } else {
                            if (!this.alignmentChecked) {
                                index = GenericUtil.indexOf(READ_BUF, ALIGNMENT_PATTERN, 0, nread - 1);
                                if (index != -1) {
                                    try {
                                        handleAlignment(index, nread);
                                    } catch (Throwable t) {
                                        sLogger.e(t);
                                        postAlignmentRunnable(true);
                                    }
                                }
                            }
                            if (this.waitForFusionStatus) {
                                index = GenericUtil.indexOf(READ_BUF, ESF_STATUS_PATTERN, 0, nread - 1);
                                if (index != -1) {
                                    try {
                                        handleFusion(index, nread);
                                    } catch (Throwable t2) {
                                        sLogger.e(t2);
                                        postFusionRunnable(true);
                                    }
                                }
                            }
                        }
                    }
                }
            } catch (Throwable t22) {
                if (!(t22 instanceof InterruptedException)) {
                    sLogger.e(t22);
                }
                this.runThread = false;
            }
        }
        sLogger.v("end thread");
    }

    private void startDeadReckoning() {
        if (!this.deadReckoningInjectionStarted) {
            sLogger.v("starting dead reckoning injection");
            this.deadReckoningInjectionStarted = true;
            this.handler.removeCallbacks(this.injectRunnable);
            this.handler.postDelayed(this.injectRunnable, 100);
        }
    }

    private void stopDeadReckoning() {
        if (this.deadReckoningInjectionStarted) {
            this.waitForAutoAlignment = false;
            this.waitForFusionStatus = false;
            this.alignmentInfo = null;
            this.rootInfo = null;
            this.alignmentChecked = false;
            sLogger.v("stopping dead rekoning injection");
            this.deadReckoningInjectionStarted = false;
            this.handler.removeCallbacks(this.injectRunnable);
            this.handler.removeCallbacks(this.getAlignmentRunnable);
            this.handler.removeCallbacks(this.getAlignmentRunnableRetry);
            this.handler.removeCallbacks(this.getFusionStatusRunnable);
            this.handler.removeCallbacks(this.getFusionStatusRunnableRetry);
        }
    }

    public void sendWarmReset() {
        this.handler.postAtFrontOfQueue(this.resetRunnable);
        this.handler.postDelayed(this.enableEsfRunnable, 2000);
    }

    public void enableEsfRaw() {
        this.handler.post(this.enableEsfRunnable);
    }

    private static String getDRType(byte b) {
        switch (b) {
            case (byte) 0:
                return "NO_FIX";
            case (byte) 1:
                return GpsConstants.DEAD_RECKONING_ONLY;
            case (byte) 2:
                return "FIX_2D";
            case (byte) 3:
                return "FIX_3D";
            case (byte) 4:
                return GpsConstants.GPS_DEAD_RECKONING_COMBINED;
            case (byte) 5:
                return "TIME_ONLY";
            default:
                return "UNKNOWN";
        }
    }

    private void checkAlignment() {
        if (this.alignmentChecked) {
            sLogger.v("alignment already checked");
            return;
        }
        sLogger.v("checking alignment");
        postAlignmentRunnable(false);
    }

    private void sendAutoAlignment() throws IOException {
        if (ObdManager.getInstance().isConnected()) {
            sLogger.v("send auto alignment [" + bytesToHex(AUTO_ALIGNMENT, 0, AUTO_ALIGNMENT.length) + "]");
            this.waitForAutoAlignment = true;
            this.outputStream.write(AUTO_ALIGNMENT);
            postAlignmentRunnable(true);
            AnalyticsSupport.localyticsSendEvent(AnalyticsSupport.ANALYTICS_EVENT_GPS_CALIBRATION_START, new String[0]);
            TTSUtils.debugShowGpsCalibrationStarted();
            return;
        }
        sLogger.v("not connected to obd manager any more");
    }

    private void handleEsfRaw(int index, int nread) {
        this.sensorDataProcessor.onRawData(new RawSensorData(READ_BUF, index));
        if (this.sensorDataProcessor.isCalibrated()) {
            this.bus.post(new CalibratedGForceData(this.sensorDataProcessor.xAccel, this.sensorDataProcessor.yAccel, this.sensorDataProcessor.zAccel));
        }
    }

    @Subscribe
    public void onDrivingStateChange(DrivingStateChange event) {
        if (!event.driving) {
            this.sensorDataProcessor.setCalibrated(false);
        }
    }

    private void handleNavStatus(int index, int nread) throws Throwable {
        int gpsFixIndex = index + 10;
        if (gpsFixIndex <= nread - 1) {
            byte val = READ_BUF[gpsFixIndex];
            switch (val) {
                case (byte) 1:
                case (byte) 4:
                    String drType;
                    Bundle bundle;
                    if (!this.deadReckoningOn) {
                        this.deadReckoningOn = true;
                        this.deadReckoningType = val;
                        drType = getDRType(val);
                        TTSUtils.debugShowDRStarted(drType);
                        sLogger.v("dead reckoning on[" + val + "] " + drType);
                        bundle = new Bundle();
                        bundle.putString(GpsConstants.GPS_EXTRA_DR_TYPE, val == (byte) 1 ? GpsConstants.DEAD_RECKONING_ONLY : GpsConstants.GPS_DEAD_RECKONING_COMBINED);
                        GpsUtils.sendEventBroadcast(GpsConstants.GPS_EVENT_DEAD_RECKONING_STARTED, bundle);
                        return;
                    } else if (this.deadReckoningType != val) {
                        drType = getDRType(val);
                        sLogger.v("dead reckoning type changed from [" + this.deadReckoningType + "] to [" + val + "] " + drType);
                        this.deadReckoningType = val;
                        TTSUtils.debugShowDRStarted(drType);
                        bundle = new Bundle();
                        bundle.putString(GpsConstants.GPS_EXTRA_DR_TYPE, val == (byte) 1 ? GpsConstants.DEAD_RECKONING_ONLY : GpsConstants.GPS_DEAD_RECKONING_COMBINED);
                        GpsUtils.sendEventBroadcast(GpsConstants.GPS_EVENT_DEAD_RECKONING_STARTED, bundle);
                        return;
                    } else {
                        return;
                    }
                default:
                    if (this.deadReckoningOn) {
                        this.deadReckoningOn = false;
                        this.deadReckoningType = (byte) -1;
                        TTSUtils.debugShowDREnded();
                        sLogger.v("dead reckoning stopped:" + val);
                        GpsUtils.sendEventBroadcast(GpsConstants.GPS_EVENT_DEAD_RECKONING_STOPPED, null);
                        return;
                    }
                    return;
            }
        }
    }

    private void handleAlignment(int index, int nread) throws Throwable {
        if (index + 24 <= nread) {
            sLogger.v("alignment raw data[" + bytesToHex(READ_BUF, index, 24) + "]");
            ByteBuffer buffer = ByteBuffer.wrap(READ_BUF, index, 24);
            buffer.order(ByteOrder.LITTLE_ENDIAN);
            buffer.get(TEMP_BUF, 0, ALIGNMENT_PATTERN.length);
            short len = buffer.getShort();
            if (len != (short) 16) {
                throw new RuntimeException("len[" + len + "] expected [" + 16 + "]");
            }
            buffer.get(TEMP_BUF, 0, 4);
            int version = buffer.get();
            byte flags = buffer.get();
            int alignmentStatus = (flags & 14) >> 1;
            boolean autoAlignment = (flags & 1) == 1;
            boolean done = alignmentStatus == 3;
            int error = buffer.get();
            buffer.get();
            float yaw = ((float) buffer.getInt()) / 100.0f;
            float pitch = ((float) buffer.getShort()) / 100.0f;
            float roll = ((float) buffer.getShort()) / 100.0f;
            Message msg = Message.obtain();
            msg.what = 1;
            Alignment alignment = new Alignment(yaw, pitch, roll, done);
            msg.obj = alignment;
            sLogger.v("Alignment bitField[" + flags + "] alignment[" + alignmentStatus + "] autoAlignment[" + autoAlignment + "] version[" + version + "] " + alignment);
            this.handler.removeCallbacks(this.getAlignmentRunnableRetry);
            this.handler.sendMessage(msg);
            return;
        }
        postAlignmentRunnable(true);
    }

    private void handleAutoAlignmentResult(Alignment alignment) {
        if (!this.waitForAutoAlignment) {
            sLogger.v("waiting for align:false");
            String vin = ObdManager.getInstance().getVin();
            if (vin == null) {
                vin = VinInformationTable.UNKNOWN_VIN;
            }
            sLogger.v("Vin is " + vin);
            String info = VinInformationHelper.getVinInfo(vin);
            boolean triggerAlignment = false;
            if (TextUtils.isEmpty(info)) {
                sLogger.v("no info found for vin,  need auto alignment");
                triggerAlignment = true;
            } else {
                try {
                    sLogger.v("VinInfo is " + info);
                    if (alignment.yaw == 0.0f && alignment.pitch == 0.0f && alignment.roll == 0.0f) {
                        sLogger.w("VinInfo exists but u-blox alignment is lost, trigger alignment");
                        AnalyticsSupport.localyticsSendEvent(AnalyticsSupport.ANALYTICS_EVENT_GPS_CALIBRATION_LOST, new String[0]);
                        triggerAlignment = true;
                        VinInformationHelper.deleteVinInfo(vin);
                    } else {
                        this.rootInfo = new JSONObject(info);
                        sLogger.v("elapsed=" + (System.currentTimeMillis() - Long.parseLong(this.rootInfo.getJSONObject(GPS).getString(ALIGNMENT_TIME))));
                    }
                } catch (Throwable t) {
                    sLogger.e(t);
                }
            }
            String lastActiveVin = VinInformationHelper.getVinPreference().getString("vin", null);
            sLogger.v("last vin:" + lastActiveVin + " current vin:" + vin);
            if (TextUtils.isEmpty(lastActiveVin) || TextUtils.equals(lastActiveVin, vin)) {
                sLogger.v("vin switch not detected");
            } else {
                sLogger.v("vin has switched: trigger auto alignment");
                triggerAlignment = true;
                AnalyticsSupport.localyticsSendEvent(AnalyticsSupport.ANALYTICS_EVENT_GPS_CALIBRATION_VIN_SWITCH, new String[0]);
            }
            if (triggerAlignment) {
                sLogger.v("alignment required");
                VinInformationHelper.getVinPreference().edit().remove("vin").commit();
                sLogger.v("last pref removed");
                if (!invokeUblox(new CommandWriter() {
                    public void run() throws IOException {
                        GpsDeadReckoningManager.this.sendAutoAlignment();
                    }
                })) {
                    postAlignmentRunnable(true);
                    return;
                }
                return;
            }
            this.alignmentChecked = true;
            sLogger.v("alignment not reqd");
            TTSUtils.debugShowGpsSensorCalibrationNotNeeded();
        } else if (alignment.done) {
            this.alignmentInfo = alignment;
            AnalyticsSupport.localyticsSendEvent(AnalyticsSupport.ANALYTICS_EVENT_GPS_CALIBRATION_IMU_DONE, new String[0]);
            TTSUtils.debugShowGpsImuCalibrationDone();
            this.waitForAutoAlignment = false;
            this.waitForFusionStatus = true;
            postFusionRunnable(false);
            sLogger.v("got alignment, wait for fusion status");
        } else {
            sLogger.v("alignment not done yet, try again");
            postAlignmentRunnable(true);
        }
    }

    private void handleFusion(int index, int nread) throws Throwable {
        if (index + 19 <= nread) {
            sLogger.v("esf raw data[" + bytesToHex(READ_BUF, index, 19) + "]");
            ByteBuffer buffer = ByteBuffer.wrap(READ_BUF, index, 19);
            buffer.order(ByteOrder.LITTLE_ENDIAN);
            buffer.get(TEMP_BUF, 0, ALIGNMENT_PATTERN.length);
            short len = buffer.getShort();
            buffer.get(TEMP_BUF, 0, 4);
            int version = buffer.get();
            int initStatus1 = buffer.get();
            int initStatus2 = buffer.get();
            buffer.get(TEMP_BUF, 0, 5);
            int fusionMode = buffer.get();
            this.handler.removeCallbacks(this.getFusionStatusRunnableRetry);
            if (fusionMode != 1) {
                sLogger.v("Fusion not done retry len[" + len + "] fusionMode[" + fusionMode + "] initStatus1[" + initStatus1 + "] initStatus2[" + initStatus2 + "] version[" + version + "]");
                postFusionRunnable(true);
                return;
            }
            sLogger.v("Fusion Done len[" + len + "] fusionMode[" + fusionMode + "] initStatus1[" + initStatus1 + "] initStatus2[" + initStatus2 + "] version[" + version + "]");
            storeVinInfoInDb();
            return;
        }
        postFusionRunnable(true);
    }

    private void storeVinInfoInDb() {
        try {
            JSONObject gps;
            String vin = ObdManager.getInstance().getVin();
            if (vin == null) {
                vin = VinInformationTable.UNKNOWN_VIN;
            }
            sLogger.v("store alignment info in db for vin[" + vin + "]");
            if (this.rootInfo == null) {
                this.rootInfo = new JSONObject();
                gps = new JSONObject();
                this.rootInfo.put(GPS, gps);
            } else {
                gps = this.rootInfo.getJSONObject(GPS);
            }
            gps.put(YAW, String.valueOf(this.alignmentInfo.yaw));
            gps.put(PITCH, String.valueOf(this.alignmentInfo.pitch));
            gps.put(ROLL, String.valueOf(this.alignmentInfo.roll));
            gps.put(ALIGNMENT_TIME, String.valueOf(System.currentTimeMillis()));
            VinInformationHelper.storeVinInfo(vin, this.rootInfo.toString());
            VinInformationHelper.getVinPreference().edit().putString("vin", vin).commit();
            sLogger.v("store last vin pref [" + vin + "]");
            this.waitForAutoAlignment = false;
            this.waitForFusionStatus = false;
            this.alignmentChecked = true;
            AnalyticsSupport.localyticsSendEvent(AnalyticsSupport.ANALYTICS_EVENT_GPS_CALIBRATION_SENSOR_DONE, new String[0]);
            TTSUtils.debugShowGpsSensorCalibrationDone();
        } catch (Throwable t) {
            sLogger.e(t);
        }
    }

    public void dumpGpsInfo(String stagingPath) {
        Throwable t;
        Throwable th;
        FileOutputStream fout = null;
        try {
            FileOutputStream fout2 = new FileOutputStream(stagingPath + File.separator + GPS_LOG);
            try {
                PrintWriter writer = new PrintWriter(new OutputStreamWriter(fout2));
                ObdManager obdManager = ObdManager.getInstance();
                writer.write("obd_connected=" + obdManager.isConnected() + GlanceConstants.NEWLINE);
                if (this.deadReckoningInjectionStarted) {
                    String vin = obdManager.getVin();
                    if (vin == null) {
                        vin = VinInformationTable.UNKNOWN_VIN;
                    }
                    writer.write("vin=" + vin + GlanceConstants.NEWLINE);
                    writer.write("calibrated=" + (this.rootInfo == null ? "no" : this.rootInfo.toString()));
                    writer.write(GlanceConstants.NEWLINE);
                }
                writer.flush();
                sLogger.i("gps log written:" + stagingPath);
                IOUtils.fileSync(fout2);
                IOUtils.closeStream(fout2);
                fout = fout2;
            } catch (Throwable th2) {
                th = th2;
                fout = fout2;
                IOUtils.fileSync(fout);
                IOUtils.closeStream(fout);
                throw th;
            }
        } catch (Throwable th3) {
            t = th3;
            try {
                sLogger.e(t);
                IOUtils.fileSync(fout);
                IOUtils.closeStream(fout);
            } catch (Throwable th4) {
                th = th4;
                IOUtils.fileSync(fout);
                IOUtils.closeStream(fout);
                throw th;
            }
        }
    }

    public String getDeadReckoningStatus() {
        Resources resources = HudApplication.getAppContext().getResources();
        ObdManager obdManager = ObdManager.getInstance();
        if (!obdManager.isConnected()) {
            return resources.getString(R.string.obd_not_connected);
        }
        if (!obdManager.isSpeedPidAvailable()) {
            return resources.getString(R.string.obd_no_speed_pid);
        }
        if (!this.deadReckoningInjectionStarted) {
            return resources.getString(R.string.gps_calibration_unknown);
        }
        if (this.rootInfo != null) {
            return resources.getString(R.string.gps_calibrated) + " " + this.rootInfo.toString();
        }
        if (this.waitForAutoAlignment) {
            return resources.getString(R.string.gps_calibrating_sensor);
        }
        if (this.waitForFusionStatus) {
            return resources.getString(R.string.gps_calibrating_fusion);
        }
        return resources.getString(R.string.gps_calibration_unknown);
    }

    private void postAlignmentRunnable(boolean delayed) {
        this.handler.removeCallbacks(this.getAlignmentRunnable);
        this.handler.removeCallbacks(this.getAlignmentRunnableRetry);
        if (delayed) {
            this.handler.postDelayed(this.getAlignmentRunnable, 30000);
        } else {
            this.handler.post(this.getAlignmentRunnable);
        }
    }

    private void postFusionRunnable(boolean delayed) {
        this.handler.removeCallbacks(this.getFusionStatusRunnable);
        this.handler.removeCallbacks(this.getFusionStatusRunnableRetry);
        if (delayed) {
            this.handler.postDelayed(this.getFusionStatusRunnable, POLL_FREQUENCY);
        } else {
            this.handler.post(this.getFusionStatusRunnable);
        }
    }
}
