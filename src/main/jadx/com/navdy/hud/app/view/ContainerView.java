package com.navdy.hud.app.view;

import android.animation.Animator;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import com.navdy.hud.app.R;
import com.navdy.hud.app.ui.framework.UIStateManager;
import com.navdy.hud.app.util.CanShowScreen;
import com.navdy.hud.app.util.Pauseable;
import com.navdy.hud.app.util.ScreenConductor;
import com.navdy.hud.app.view.MainView.CustomAnimationMode;
import flow.Flow.Direction;
import javax.inject.Inject;
import mortar.Blueprint;
import mortar.Mortar;

public class ContainerView extends FrameLayout implements CanShowScreen<Blueprint>, Pauseable {
    private ScreenConductor<Blueprint> screenMaestro;
    @Inject
    UIStateManager uiStateManager;

    public ContainerView(Context context, AttributeSet attrs) {
        super(context, attrs);
        if (!isInEditMode()) {
            Mortar.inject(context, this);
        }
    }

    protected void onFinishInflate() {
        super.onFinishInflate();
        this.screenMaestro = new ScreenConductor(getContext(), (ViewGroup) findViewById(R.id.screenContainer), this.uiStateManager);
    }

    public void showScreen(Blueprint screen, Direction direction, int animIn, int animOut) {
        this.screenMaestro.showScreen(screen, direction, animIn, animOut);
    }

    public void onPause() {
        View view = getCurrentView();
        if (view instanceof Pauseable) {
            ((Pauseable) view).onPause();
        }
    }

    public void onResume() {
        View view = getCurrentView();
        if (view instanceof Pauseable) {
            ((Pauseable) view).onResume();
        }
    }

    public View getCurrentView() {
        return this.screenMaestro.getChildView();
    }

    public Animator getCustomContainerAnimator(CustomAnimationMode mode) {
        View view = getCurrentView();
        if (view == null || !(view instanceof ICustomAnimator)) {
            return null;
        }
        return ((ICustomAnimator) view).getCustomAnimator(mode);
    }

    public void createScreens() {
        this.screenMaestro.createScreens();
    }
}
