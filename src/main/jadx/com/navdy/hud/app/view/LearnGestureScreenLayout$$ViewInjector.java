package com.navdy.hud.app.view;

import android.view.ViewGroup;
import butterknife.ButterKnife.Finder;
import com.navdy.hud.app.R;

public class LearnGestureScreenLayout$$ViewInjector {
    public static void inject(Finder finder, LearnGestureScreenLayout target, Object source) {
        target.mGestureLearningView = (GestureLearningView) finder.findRequiredView(source, R.id.gesture_learning_view, "field 'mGestureLearningView'");
        target.mScrollableTextPresenter = (ScrollableTextPresenterLayout) finder.findRequiredView(source, R.id.scrollable_text_presenter, "field 'mScrollableTextPresenter'");
        target.mCameraBlockedMessage = (ViewGroup) finder.findRequiredView(source, R.id.sensor_blocked_message, "field 'mCameraBlockedMessage'");
        target.mVideoCaptureInstructionsLayout = (GestureVideoCaptureView) finder.findRequiredView(source, R.id.capture_instructions_lyt, "field 'mVideoCaptureInstructionsLayout'");
    }

    public static void reset(LearnGestureScreenLayout target) {
        target.mGestureLearningView = null;
        target.mScrollableTextPresenter = null;
        target.mCameraBlockedMessage = null;
        target.mVideoCaptureInstructionsLayout = null;
    }
}
