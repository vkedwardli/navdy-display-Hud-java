package com.navdy.hud.app.view;

import android.widget.FrameLayout;
import butterknife.ButterKnife.Finder;
import com.navdy.hud.app.R;
import com.navdy.hud.app.ui.component.ShrinkingBorderView;
import com.navdy.hud.app.ui.component.image.ColorImageView;

public class NotificationView$$ViewInjector {
    public static void inject(Finder finder, NotificationView target, Object source) {
        target.customNotificationContainer = (FrameLayout) finder.findRequiredView(source, R.id.customNotificationContainer, "field 'customNotificationContainer'");
        target.border = (ShrinkingBorderView) finder.findRequiredView(source, R.id.border, "field 'border'");
        target.nextNotificationColorView = (ColorImageView) finder.findRequiredView(source, R.id.nextNotificationColorView, "field 'nextNotificationColorView'");
    }

    public static void reset(NotificationView target) {
        target.customNotificationContainer = null;
        target.border = null;
        target.nextNotificationColorView = null;
    }
}
