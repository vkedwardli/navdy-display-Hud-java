package com.navdy.hud.app.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.util.AttributeSet;
import android.view.View;
import com.navdy.hud.app.R;

public class HighlightView extends View {
    private Paint paint;
    private int radius;
    private float strokeWidth;

    public HighlightView(Context context) {
        this(context, null);
    }

    public HighlightView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initFromAttributes(context, attrs);
        this.paint = new Paint();
        this.paint.setStrokeWidth(this.strokeWidth);
        this.paint.setAntiAlias(true);
        this.paint.setColor(getResources().getColor(R.color.hud_cyan));
    }

    private void initFromAttributes(Context context, AttributeSet attrs) {
        TypedArray a = context.getTheme().obtainStyledAttributes(attrs, R.styleable.Indicator, 0, 0);
        this.strokeWidth = a.getDimension(3, (float) ((int) getResources().getDimension(R.dimen.default_highlight_stroke_width)));
        a.recycle();
    }

    public void setStrokeWidth(float strokeWidth) {
        this.strokeWidth = strokeWidth;
    }

    public void setPaintStyle(Style paintStyle) {
        if (paintStyle == Style.FILL_AND_STROKE) {
            throw new IllegalArgumentException();
        }
        this.paint.setStyle(paintStyle);
    }

    public void setRadius(int radius) {
        this.radius = radius;
    }

    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawCircle((float) (getWidth() / 2), (float) (getHeight() / 2), (float) this.radius, this.paint);
    }
}
