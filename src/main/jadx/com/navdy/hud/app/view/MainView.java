package com.navdy.hud.app.view;

import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.animation.AnimatorSet;
import android.animation.AnimatorSet.Builder;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.content.res.Resources;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import butterknife.ButterKnife;
import butterknife.InjectView;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.R;
import com.navdy.hud.app.framework.glance.GlanceConstants;
import com.navdy.hud.app.framework.notifications.INotification;
import com.navdy.hud.app.framework.notifications.NotificationManager;
import com.navdy.hud.app.framework.notifications.NotificationType;
import com.navdy.hud.app.manager.InputManager.CustomKeyEvent;
import com.navdy.hud.app.manager.InputManager.IInputHandler;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.screen.BaseScreen;
import com.navdy.hud.app.settings.AdaptiveBrightnessControl;
import com.navdy.hud.app.settings.BrightnessControl;
import com.navdy.hud.app.settings.HUDSettings;
import com.navdy.hud.app.ui.activity.Main.INotificationExtensionView;
import com.navdy.hud.app.ui.activity.Main.Presenter;
import com.navdy.hud.app.ui.component.SystemTrayView;
import com.navdy.hud.app.ui.component.carousel.CarouselIndicator;
import com.navdy.hud.app.ui.component.carousel.CarouselIndicator.Orientation;
import com.navdy.hud.app.ui.component.carousel.ProgressIndicator;
import com.navdy.hud.app.ui.component.image.ColorImageView;
import com.navdy.hud.app.ui.component.main.MainLowerView;
import com.navdy.hud.app.ui.framework.AnimationQueue;
import com.navdy.hud.app.ui.framework.DefaultAnimationListener;
import com.navdy.hud.app.ui.framework.INotificationAnimationListener;
import com.navdy.hud.app.ui.framework.IScreenAnimationListener;
import com.navdy.hud.app.ui.framework.UIStateManager;
import com.navdy.hud.app.ui.framework.UIStateManager.Mode;
import com.navdy.hud.app.util.DeviceUtil;
import com.navdy.hud.app.util.os.SystemProperties;
import com.navdy.service.library.events.input.GestureEvent;
import com.navdy.service.library.events.preferences.DriverProfilePreferences.DisplayFormat;
import com.navdy.service.library.log.Logger;
import javax.inject.Inject;
import mortar.Mortar;

public class MainView extends FrameLayout implements IInputHandler {
    private static final String COMPACT_MODE_SCALE_PROPERTY = "persist.sys.compact.scale";
    private static final float DEFAULT_COMPACT_MODE_SCALE = 0.87f;
    private static final int SHOW_OPTION_LAG = 350;
    private static final Logger sLogger = new Logger(MainView.class);
    private AnimationQueue animationQueue;
    private int basePaddingTop;
    private OnSharedPreferenceChangeListener brightnessControl;
    @InjectView(R.id.container)
    ContainerView containerView;
    @InjectView(R.id.expandedNotifCoverView)
    View expandedNotificationCoverView;
    @InjectView(R.id.expandedNotifView)
    FrameLayout expandedNotificationView;
    private DisplayFormat format;
    private Handler handler;
    private boolean isNotificationCollapsing;
    private boolean isNotificationExpanding;
    private boolean isScreenAnimating;
    @InjectView(R.id.mainLowerView)
    MainLowerView mainLowerView;
    private int mainPanelWidth;
    @InjectView(R.id.notifIndicator)
    CarouselIndicator notifIndicator;
    @InjectView(R.id.notifScrollIndicator)
    ProgressIndicator notifScrollIndicator;
    private INotificationAnimationListener notificationAnimationListener;
    @InjectView(R.id.notificationColorView)
    ColorImageView notificationColorView;
    @InjectView(R.id.notificationExtensionView)
    FrameLayout notificationExtensionView;
    @InjectView(R.id.notification)
    NotificationView notificationView;
    @Inject
    SharedPreferences preferences;
    @Inject
    Presenter presenter;
    private int rightPanelStart;
    private IScreenAnimationListener screenAnimationListener;
    @InjectView(R.id.screenContainer)
    FrameLayout screenContainer;
    private boolean showingNotificationExtension;
    private int sidePanelWidth;
    @InjectView(R.id.splitter)
    FrameLayout splitterView;
    @InjectView(R.id.systemTray)
    SystemTrayView systemTray;
    @InjectView(R.id.toastView)
    ToastView toastView;
    @Inject
    UIStateManager uiStateManager;
    private int verticalOffset;

    public enum AnimationMode {
        MOVE_LEFT_SHRINK,
        RIGHT_EXPAND
    }

    public enum CustomAnimationMode {
        SHRINK_LEFT,
        EXPAND,
        SHRINK_MODE
    }

    public MainView(Context context) {
        this(context, null);
    }

    public MainView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public MainView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.basePaddingTop = -1;
        this.animationQueue = new AnimationQueue();
        this.handler = new Handler();
        this.notificationAnimationListener = new INotificationAnimationListener() {
            public void onStart(String id, NotificationType type, Mode mode) {
                if (mode == Mode.EXPAND) {
                    MainView.this.isNotificationExpanding = true;
                } else {
                    MainView.this.isNotificationCollapsing = true;
                }
            }

            public void onStop(String id, NotificationType type, Mode mode) {
                if (mode == Mode.EXPAND) {
                    MainView.this.isNotificationExpanding = false;
                } else {
                    MainView.this.isNotificationCollapsing = false;
                }
            }
        };
        this.screenAnimationListener = new IScreenAnimationListener() {
            public void onStart(BaseScreen in, BaseScreen out) {
                MainView.this.isScreenAnimating = true;
            }

            public void onStop(BaseScreen in, BaseScreen out) {
                MainView.this.isScreenAnimating = false;
            }
        };
        if (!isInEditMode()) {
            Mortar.inject(context, this);
        }
        initLayout();
    }

    private void initLayout() {
        getViewTreeObserver().addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
            public void onGlobalLayout() {
                MainView.this.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                Resources resources = MainView.this.getResources();
                int measuredHt = (int) resources.getDimension(R.dimen.dashboard_height);
                MainView.this.basePaddingTop = (measuredHt / 2) - (((int) MainView.this.getResources().getDimension(R.dimen.dashboard_box_height)) / 2);
                ((LayoutParams) MainView.this.splitterView.getLayoutParams()).topMargin = MainView.this.basePaddingTop + MainView.this.verticalOffset;
                MainView.this.mainPanelWidth = (int) resources.getDimension(R.dimen.dashboard_width);
                if (DeviceUtil.isNavdyDevice()) {
                    MainView.this.sidePanelWidth = (int) resources.getDimension(R.dimen.side_panel_width);
                } else {
                    MainView.this.sidePanelWidth = (int) (((float) MainView.this.mainPanelWidth) * 0.33f);
                }
                MainView.this.uiStateManager.setMainPanelWidth(MainView.this.mainPanelWidth);
                MainView.this.uiStateManager.setSidePanelWidth(MainView.this.sidePanelWidth);
                MainView.sLogger.v("side panel width=" + MainView.this.sidePanelWidth);
                MainView.this.rightPanelStart = MainView.this.mainPanelWidth - MainView.this.sidePanelWidth;
                DisplayMetrics metrics = MainView.this.getResources().getDisplayMetrics();
                MainView.sLogger.v("Density:" + metrics.density + ", dpi:" + metrics.densityDpi + ", width:" + metrics.widthPixels + ", height:" + metrics.heightPixels);
                MainView.sLogger.v("onGlobalLayout mainPanelWidth = " + MainView.this.mainPanelWidth + " sidePanelWidth = " + MainView.this.sidePanelWidth + " rightPanelStart = " + MainView.this.rightPanelStart + " paddingTop = " + MainView.this.basePaddingTop + " mh =" + measuredHt);
                MainView.this.containerView.setX(0.0f);
                MainView.sLogger.v("containerView x: 0");
                ((LayoutParams) MainView.this.containerView.getLayoutParams()).width = MainView.this.mainPanelWidth;
                ((LayoutParams) MainView.this.notificationView.getLayoutParams()).width = MainView.this.sidePanelWidth;
                MainView.this.notificationView.setX((float) MainView.this.mainPanelWidth);
                MainView.sLogger.v("notifView x: " + MainView.this.mainPanelWidth);
                MainView.this.expandedNotificationView.setX((float) MainView.this.sidePanelWidth);
                MainView.sLogger.v("expand notifView x: " + MainView.this.sidePanelWidth);
                int expandX = (MainView.this.sidePanelWidth + ((int) resources.getDimension(R.dimen.expand_notif_width))) + ((int) resources.getDimension(R.dimen.expand_notif_indicator_left_margin));
                MainView.this.notifIndicator.setOrientation(Orientation.VERTICAL);
                MainView.this.notifIndicator.setX((float) expandX);
                MainView.sLogger.v("notif indicator x: " + expandX);
                MainView.this.notifScrollIndicator.setOrientation(Orientation.VERTICAL);
                MainView.this.notifScrollIndicator.setProperties(GlanceConstants.scrollingRoundSize, GlanceConstants.scrollingIndicatorProgressSize, 0, 0, GlanceConstants.colorWhite, GlanceConstants.colorWhite, true, GlanceConstants.scrollingIndicatorPadding, -1, -1);
                MainView.this.notifScrollIndicator.setItemCount(100);
                MainView.this.handler.postDelayed(new Runnable() {
                    public void run() {
                        MainView.this.notificationView.setX((float) MainView.this.mainPanelWidth);
                        MainView.sLogger.v("notifView x: " + MainView.this.mainPanelWidth);
                    }
                }, 2000);
                MainView.this.presenter.createScreens();
                MainView.this.presenter.setInputFocus();
            }
        });
    }

    public void setDisplayFormat(DisplayFormat format) {
        if (format != this.format) {
            this.format = format;
            float scale = format == DisplayFormat.DISPLAY_FORMAT_COMPACT ? compactScale() : 1.0f;
            setScaleX(scale);
            setScaleY(scale);
        }
    }

    private float compactScale() {
        return SystemProperties.getFloat(COMPACT_MODE_SCALE_PROPERTY, DEFAULT_COMPACT_MODE_SCALE);
    }

    public int getBasePaddingTop() {
        return this.basePaddingTop;
    }

    public int getVerticalOffset() {
        return this.verticalOffset;
    }

    public void setVerticalOffset(int offset) {
        if (offset != this.verticalOffset) {
            this.verticalOffset = offset;
            if (this.basePaddingTop != -1) {
                int newPadding = this.basePaddingTop + this.verticalOffset;
                if (newPadding >= 0) {
                    FrameLayout boxView = getBoxView();
                    LayoutParams params = (LayoutParams) boxView.getLayoutParams();
                    params.topMargin = newPadding;
                    boxView.setLayoutParams(params);
                }
            }
        }
    }

    protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.inject((View) this);
        if (!isInEditMode()) {
            if (!DeviceUtil.isNavdyDevice()) {
                addDebugMarkers(getContext());
            }
            this.uiStateManager.setToastView(this.toastView);
            this.uiStateManager.addScreenAnimationListener(this.screenAnimationListener);
            this.uiStateManager.addNotificationAnimationListener(this.notificationAnimationListener);
        }
    }

    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (this.presenter != null) {
            this.presenter.takeView(this);
        }
        if (this.preferences == null) {
            return;
        }
        if (HUDSettings.USE_ADAPTIVE_AUTOBRIGHTNESS) {
            this.brightnessControl = new AdaptiveBrightnessControl(getContext(), RemoteDeviceManager.getInstance().getBus(), this.preferences, HUDSettings.BRIGHTNESS, HUDSettings.AUTO_BRIGHTNESS, HUDSettings.AUTO_BRIGHTNESS_ADJUSTMENT, HUDSettings.LED_BRIGHTNESS);
        } else {
            this.brightnessControl = new BrightnessControl(getContext(), RemoteDeviceManager.getInstance().getBus(), this.preferences, HUDSettings.BRIGHTNESS, HUDSettings.AUTO_BRIGHTNESS, HUDSettings.AUTO_BRIGHTNESS_ADJUSTMENT, HUDSettings.LED_BRIGHTNESS);
        }
    }

    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (this.presenter != null) {
            this.presenter.dropView((View) this);
        }
        if (this.preferences != null) {
            this.preferences.unregisterOnSharedPreferenceChangeListener(this.brightnessControl);
            this.brightnessControl = null;
        }
    }

    public ContainerView getContainerView() {
        return this.containerView;
    }

    public NotificationView getNotificationView() {
        return this.notificationView;
    }

    public FrameLayout getExpandedNotificationView() {
        return this.expandedNotificationView;
    }

    public FrameLayout getNotificationExtensionView() {
        return this.notificationExtensionView;
    }

    public View getExpandedNotificationCoverView() {
        return this.expandedNotificationCoverView;
    }

    public CarouselIndicator getNotificationIndicator() {
        return this.notifIndicator;
    }

    public ProgressIndicator getNotificationScrollIndicator() {
        return this.notifScrollIndicator;
    }

    public FrameLayout getBoxView() {
        return this.splitterView;
    }

    public boolean isMainUIShrunk() {
        return (this.isNotificationExpanding || isNotificationViewShowing()) && !this.isNotificationCollapsing;
    }

    public boolean isScreenAnimating() {
        return this.isScreenAnimating;
    }

    public boolean isNotificationViewShowing() {
        return this.notificationView.getX() < ((float) this.mainPanelWidth);
    }

    public boolean isExpandedNotificationViewShowing() {
        return this.expandedNotificationView.getVisibility() == 0;
    }

    public boolean isNotificationExpanding() {
        return this.isNotificationExpanding;
    }

    public boolean isNotificationCollapsing() {
        return this.isNotificationCollapsing;
    }

    private ObjectAnimator getXPositionAnimator(ViewGroup viewGroup, int xPos) {
        return ObjectAnimator.ofFloat(viewGroup, "x", new float[]{(float) xPos});
    }

    public boolean onGesture(GestureEvent event) {
        return false;
    }

    public boolean onKey(CustomKeyEvent event) {
        return false;
    }

    public IInputHandler nextHandler() {
        return this.presenter;
    }

    private void addDebugMarkers(Context context) {
        int color = getResources().getColor(17170443);
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((WindowManager) HudApplication.getAppContext().getSystemService("window")).getDefaultDisplay().getMetrics(displayMetrics);
        int dip = (int) TypedValue.applyDimension(1, 1.0f, displayMetrics);
        LayoutParams params = new LayoutParams(-1, dip);
        params.gravity = 48;
        View view = new View(context);
        view.setBackgroundColor(color);
        this.splitterView.addView(view, params);
        params = new LayoutParams(-1, dip);
        params.gravity = 80;
        view = new View(context);
        view.setBackgroundColor(color);
        this.splitterView.addView(view, params);
    }

    public void showNotification(final String notifId, final NotificationType type) {
        AnimatorSet set = new AnimatorSet();
        AnimatorSet set2 = new AnimatorSet();
        set2.playTogether(new Animator[]{rightExpand(), middleAnimate(AnimationMode.MOVE_LEFT_SHRINK)});
        if (this.notificationView.getX() < ((float) this.mainPanelWidth)) {
            this.uiStateManager.postNotificationAnimationEvent(true, notifId, type, Mode.EXPAND);
            this.uiStateManager.postNotificationAnimationEvent(false, notifId, type, Mode.EXPAND);
            return;
        }
        set.play(set2);
        BaseScreen screen = this.uiStateManager.getCurrentScreen();
        set.addListener(new DefaultAnimationListener() {
            public void onAnimationStart(Animator animation) {
                MainView.this.uiStateManager.postNotificationAnimationEvent(true, notifId, type, Mode.EXPAND);
            }

            public void onAnimationEnd(Animator animation) {
                MainView.this.uiStateManager.postNotificationAnimationEvent(false, notifId, type, Mode.EXPAND);
            }
        });
        this.animationQueue.startAnimation(set);
    }

    public void dismissNotification() {
        AnimatorSet set = new AnimatorSet();
        sLogger.d("dismissNotification");
        if (this.notificationView.getX() < ((float) this.mainPanelWidth)) {
            Builder builder = set.play(rightCollapse()).with(middleAnimate(AnimationMode.RIGHT_EXPAND));
            if (this.showingNotificationExtension) {
                sLogger.d("hiding notification extension along with notification");
                builder.with(getRemoveNotificationExtensionAnimator());
            }
        }
        set.addListener(new DefaultAnimationListener() {
            public void onAnimationStart(Animator animation) {
                MainView.this.postNotificationCollapseEvent(true);
            }

            public void onAnimationEnd(Animator animation) {
                MainView.this.postNotificationCollapseEvent(false);
            }
        });
        this.animationQueue.startAnimation(set);
    }

    public void addNotificationExtensionView(final View view) {
        sLogger.d("addNotificationExtensionView");
        final ViewGroup notificationExtensionView = getNotificationExtensionView();
        notificationExtensionView.addView(view);
        Animator objectAnimator = AnimatorInflater.loadAnimator(notificationExtensionView.getContext(), 17498112);
        objectAnimator.setTarget(notificationExtensionView);
        objectAnimator.addListener(new DefaultAnimationListener() {
            public void onAnimationStart(Animator animation) {
                super.onAnimationStart(animation);
                notificationExtensionView.setVisibility(0);
                MainView.this.showingNotificationExtension = true;
            }

            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                if (view instanceof INotificationExtensionView) {
                    ((INotificationExtensionView) view).onStart();
                }
            }
        });
        objectAnimator.start();
    }

    public void removeNotificationExtensionView() {
        sLogger.d("addNotificationExtensionView, showing extension ? : " + this.showingNotificationExtension);
        ViewGroup notificationExtension = getNotificationExtensionView();
        if (notificationExtension == null) {
            return;
        }
        if (this.showingNotificationExtension) {
            getRemoveNotificationExtensionAnimator().start();
            return;
        }
        this.notificationExtensionView.setVisibility(8);
        notificationExtension.removeAllViews();
    }

    private Animator getRemoveNotificationExtensionAnimator() {
        final ViewGroup notificationExtension = getNotificationExtensionView();
        Animator objectAnimator = AnimatorInflater.loadAnimator(notificationExtension.getContext(), 17498113);
        objectAnimator.setTarget(notificationExtension);
        objectAnimator.addListener(new DefaultAnimationListener() {
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                View view = notificationExtension.getChildAt(0);
                if (view instanceof INotificationExtensionView) {
                    ((INotificationExtensionView) view).onStop();
                }
                notificationExtension.setVisibility(8);
                notificationExtension.removeAllViews();
            }
        });
        return objectAnimator;
    }

    private Animator rightCollapse() {
        return getXPositionAnimator(this.notificationView, this.mainPanelWidth);
    }

    private ObjectAnimator rightExpand() {
        return getXPositionAnimator(this.notificationView, this.rightPanelStart);
    }

    private Animator middleAnimate(AnimationMode animationMode) {
        Animator customAnimator = null;
        CustomAnimationMode customAnimationMode = null;
        if (animationMode == AnimationMode.MOVE_LEFT_SHRINK) {
            customAnimationMode = CustomAnimationMode.SHRINK_LEFT;
        } else if (animationMode == AnimationMode.RIGHT_EXPAND) {
            customAnimationMode = CustomAnimationMode.EXPAND;
        }
        if (customAnimationMode != null) {
            customAnimator = this.containerView.getCustomContainerAnimator(customAnimationMode);
        }
        AnimatorSet set = new AnimatorSet();
        Animator xAnimator;
        switch (animationMode) {
            case MOVE_LEFT_SHRINK:
                xAnimator = getXPositionAnimator(this.containerView, -this.sidePanelWidth);
                if (customAnimator != null) {
                    set.playTogether(new Animator[]{xAnimator, customAnimator});
                    return set;
                }
                set.playTogether(new Animator[]{xAnimator});
                return set;
            case RIGHT_EXPAND:
                xAnimator = getXPositionAnimator(this.containerView, 0);
                if (customAnimator != null) {
                    set.playTogether(new Animator[]{xAnimator, customAnimator});
                    return set;
                }
                set.play(xAnimator);
                return set;
            default:
                return null;
        }
    }

    public void setNotificationColor(int color) {
        this.notificationColorView.setColor(color);
    }

    public void setNotificationColorVisibility(int visibility) {
        this.notificationColorView.setVisibility(visibility);
    }

    public int getNotificationColorVisibility() {
        return this.notificationColorView.getVisibility();
    }

    public SystemTrayView getSystemTray() {
        return this.systemTray;
    }

    public ToastView getToastView() {
        return this.toastView;
    }

    private void postNotificationCollapseEvent(boolean starting) {
        String s = null;
        NotificationType t = null;
        INotification notification = NotificationManager.getInstance().getCurrentNotification();
        if (notification != null) {
            s = notification.getId();
            t = notification.getType();
        }
        this.uiStateManager.postNotificationAnimationEvent(starting, s, t, Mode.COLLAPSE);
    }

    public void injectMainLowerView(View view) {
        this.mainLowerView.injectView(view);
    }

    public void ejectMainLowerView() {
        this.mainLowerView.ejectView();
    }

    public boolean isMainLowerViewVisible() {
        return this.mainLowerView.getVisibility() == 0;
    }
}
