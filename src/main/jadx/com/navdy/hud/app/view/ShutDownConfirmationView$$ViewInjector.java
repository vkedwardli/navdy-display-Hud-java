package com.navdy.hud.app.view;

import android.widget.ImageView;
import android.widget.TextView;
import butterknife.ButterKnife.Finder;
import com.navdy.hud.app.R;
import com.navdy.hud.app.ui.component.ChoiceLayout;

public class ShutDownConfirmationView$$ViewInjector {
    public static void inject(Finder finder, ShutDownConfirmationView target, Object source) {
        target.mScreenTitleText = (TextView) finder.findRequiredView(source, R.id.mainTitle, "field 'mScreenTitleText'");
        target.mTextView1 = (TextView) finder.findRequiredView(source, R.id.title1, "field 'mTextView1'");
        target.mMainTitleText = (TextView) finder.findRequiredView(source, R.id.title2, "field 'mMainTitleText'");
        target.mInfoText = (TextView) finder.findRequiredView(source, R.id.title3, "field 'mInfoText'");
        target.mSummaryText = (TextView) finder.findRequiredView(source, R.id.title4, "field 'mSummaryText'");
        target.mIcon = (ImageView) finder.findRequiredView(source, R.id.image, "field 'mIcon'");
        target.mChoiceLayout = (ChoiceLayout) finder.findRequiredView(source, R.id.choiceLayout, "field 'mChoiceLayout'");
        target.mRightSwipe = (ImageView) finder.findRequiredView(source, R.id.rightSwipe, "field 'mRightSwipe'");
        target.mLefttSwipe = (ImageView) finder.findRequiredView(source, R.id.leftSwipe, "field 'mLefttSwipe'");
    }

    public static void reset(ShutDownConfirmationView target) {
        target.mScreenTitleText = null;
        target.mTextView1 = null;
        target.mMainTitleText = null;
        target.mInfoText = null;
        target.mSummaryText = null;
        target.mIcon = null;
        target.mChoiceLayout = null;
        target.mRightSwipe = null;
        target.mLefttSwipe = null;
    }
}
