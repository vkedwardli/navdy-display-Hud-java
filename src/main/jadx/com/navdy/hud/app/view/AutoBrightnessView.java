package com.navdy.hud.app.view;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.text.SpannableStringBuilder;
import android.text.style.StyleSpan;
import android.text.style.TypefaceSpan;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup.MarginLayoutParams;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.ButterKnife;
import butterknife.InjectView;
import com.navdy.hud.app.R;
import com.navdy.hud.app.manager.InputManager;
import com.navdy.hud.app.manager.InputManager.CustomKeyEvent;
import com.navdy.hud.app.manager.InputManager.IInputHandler;
import com.navdy.hud.app.screen.AutoBrightnessScreen.Presenter;
import com.navdy.hud.app.ui.component.ChoiceLayout;
import com.navdy.hud.app.ui.component.ChoiceLayout.Choice;
import com.navdy.hud.app.ui.component.ChoiceLayout.IListener;
import com.navdy.hud.app.ui.component.ChoiceLayout.Mode;
import com.navdy.service.library.events.input.GestureEvent;
import java.util.List;
import javax.inject.Inject;
import mortar.Mortar;

public class AutoBrightnessView extends FrameLayout implements IInputHandler {
    @InjectView(R.id.choiceLayout)
    ChoiceLayout autoBrightnessChoices;
    @InjectView(R.id.image)
    ImageView autoBrightnessIcon;
    @InjectView(R.id.mainTitle)
    TextView autoBrightnessTitle;
    @InjectView(R.id.title2)
    TextView brightnessTitle;
    @InjectView(R.id.title3)
    TextView brightnessTitleDesc;
    @InjectView(R.id.infoContainer)
    LinearLayout infoContainer;
    @Inject
    Presenter presenter;
    String text;
    @InjectView(R.id.title1)
    TextView title1;

    public AutoBrightnessView(Context context) {
        this(context, null);
    }

    public AutoBrightnessView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public AutoBrightnessView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        if (!isInEditMode()) {
            Mortar.inject(context, this);
        }
    }

    protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.inject((View) this);
        Resources resources = getResources();
        int width = (int) resources.getDimension(R.dimen.auto_brightness_section_width);
        ((MarginLayoutParams) this.infoContainer.getLayoutParams()).width = width;
        this.infoContainer.setMinimumWidth(width);
        ((MarginLayoutParams) this.autoBrightnessChoices.getLayoutParams()).bottomMargin = (int) resources.getDimension(R.dimen.auto_brightness_choice_margin_bottom);
        this.autoBrightnessTitle.setTypeface(Typeface.create("sans-serif-medium", 0));
        this.title1.setVisibility(8);
        this.autoBrightnessTitle.setTypeface(Typeface.create("sans-serif-light", 0));
        this.autoBrightnessTitle.setTypeface(Typeface.create("sans-serif", 0));
        this.brightnessTitleDesc.setSingleLine(false);
        this.brightnessTitleDesc.setMaxLines(3);
        this.brightnessTitleDesc.setText(resources.getString(R.string.auto_brightness_description_text));
        this.text = resources.getString(R.string.auto_brightness_status_label_text);
    }

    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (this.presenter != null) {
            this.presenter.takeView(this);
        }
    }

    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (this.presenter != null) {
            this.presenter.dropView((View) this);
        }
    }

    public boolean onGesture(GestureEvent event) {
        return false;
    }

    public boolean onKey(CustomKeyEvent event) {
        return this.presenter.handleKey(event);
    }

    public IInputHandler nextHandler() {
        return InputManager.nextContainingHandler(this);
    }

    public void setTitle(String title) {
        this.autoBrightnessTitle.setText(title);
    }

    public void setStatusLabel(String statusLabel) {
        SpannableStringBuilder builder = new SpannableStringBuilder();
        builder.append(this.text);
        builder.setSpan(new TypefaceSpan("sans-serif-light"), 0, builder.length(), 33);
        int start = builder.length();
        builder.append(statusLabel);
        builder.setSpan(new StyleSpan(1), start, builder.length(), 33);
        this.brightnessTitle.setText(builder);
    }

    public void setIcon(Drawable drawable) {
        this.autoBrightnessIcon.setImageDrawable(drawable);
    }

    public void setChoices(List<Choice> choicesList, IListener choicesListener) {
        this.autoBrightnessChoices.setChoices(Mode.LABEL, choicesList, 0, choicesListener);
    }

    public void moveSelectionLeft() {
        this.autoBrightnessChoices.moveSelectionLeft();
    }

    public void moveSelectionRight() {
        this.autoBrightnessChoices.moveSelectionRight();
    }

    public void executeSelectedItem() {
        this.autoBrightnessChoices.executeSelectedItem(true);
    }
}
