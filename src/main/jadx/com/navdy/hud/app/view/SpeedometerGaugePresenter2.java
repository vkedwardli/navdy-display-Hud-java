package com.navdy.hud.app.view;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.os.Handler;
import android.os.SystemClock;
import android.util.TypedValue;
import com.navdy.hud.app.R;
import com.navdy.hud.app.manager.SpeedManager;
import com.navdy.hud.app.manager.SpeedManager.SpeedUnit;
import com.navdy.hud.app.view.drawable.SpeedoMeterGaugeDrawable2;

public class SpeedometerGaugePresenter2 extends GaugeViewPresenter implements SerialValueAnimatorAdapter {
    private static final boolean ALWAYS_SHOW_SPEED_LIMIT = true;
    public static final String ID = "SPEEDO_METER_WIDGET_2";
    private static final int MAX_ANIMATION_DURATION = 100;
    public static final int MAX_SPEED = 158;
    private static final int MIN_ANIMATION_DURATION = 20;
    public static final int RESAMPLE_INTERVAL = 1000;
    public static final int SAFE = 0;
    private static final int SPEED_LIMIT_ANIMATION_DELAY = 500;
    private static final int SPEED_LIMIT_ANIMATION_DURATION = 1000;
    public static final int SPEED_LIMIT_EXCEEDED = 1;
    public static final int SPEED_LIMIT_TRESHOLD_EXCEEDED = 2;
    private int animationDuration = 100;
    private Handler handler = new Handler();
    private final String kmhString;
    private int lastSpeed = Integer.MIN_VALUE;
    private long lastSpeedSampleArrivalTime = 0;
    private SerialValueAnimator mSerialValueAnimator;
    private int mSpeed;
    private int mSpeedLimit = 0;
    private SpeedoMeterGaugeDrawable2 mSpeedoMeterGaugeDrawable;
    private final String mphString;
    private boolean showingSpeedLimitAnimation;
    private int speedLimitAlpha;
    private Runnable speedLimitFadeOutAnimationRunnable;
    private ValueAnimator speedLimitFadeOutAnimator;
    private boolean speedLimitMarkerNeedsTobeRemoved = false;
    private SpeedManager speedManager = SpeedManager.getInstance();
    private final String speedoMeterWidgetName;
    private int textSize2Chars;
    private int textSize3Chars;
    private boolean updateText = true;

    public SpeedometerGaugePresenter2(Context context) {
        this.mSpeedoMeterGaugeDrawable = new SpeedoMeterGaugeDrawable2(context, 0, R.array.smart_dash_speedo_meter_color_table, R.dimen.speedo_meter_guage_inner_bar_width);
        this.mSpeedoMeterGaugeDrawable.setMaxGaugeValue(158.0f);
        this.mSerialValueAnimator = new SerialValueAnimator(this, 100);
        this.speedLimitFadeOutAnimator = new ValueAnimator();
        this.speedLimitFadeOutAnimationRunnable = new Runnable() {
            public void run() {
                if (SpeedometerGaugePresenter2.this.speedLimitFadeOutAnimator.isRunning()) {
                    SpeedometerGaugePresenter2.this.speedLimitFadeOutAnimator.cancel();
                }
                SpeedometerGaugePresenter2.this.speedLimitFadeOutAnimator.setIntValues(new int[]{255, 0});
                SpeedometerGaugePresenter2.this.speedLimitFadeOutAnimator.setDuration(1000);
                SpeedometerGaugePresenter2.this.speedLimitFadeOutAnimator.addUpdateListener(new AnimatorUpdateListener() {
                    public void onAnimationUpdate(ValueAnimator animation) {
                        SpeedometerGaugePresenter2.this.speedLimitAlpha = ((Integer) animation.getAnimatedValue()).intValue();
                        SpeedometerGaugePresenter2.this.reDraw();
                    }
                });
                SpeedometerGaugePresenter2.this.speedLimitFadeOutAnimator.addListener(new AnimatorListener() {
                    public void onAnimationStart(Animator animation) {
                    }

                    public void onAnimationEnd(Animator animation) {
                        SpeedometerGaugePresenter2.this.speedLimitAlpha = 0;
                        SpeedometerGaugePresenter2.this.showingSpeedLimitAnimation = false;
                        SpeedometerGaugePresenter2.this.reDraw();
                    }

                    public void onAnimationCancel(Animator animation) {
                    }

                    public void onAnimationRepeat(Animator animation) {
                    }
                });
                SpeedometerGaugePresenter2.this.speedLimitFadeOutAnimator.start();
            }
        };
        Resources resources = context.getResources();
        this.mphString = resources.getString(R.string.mph);
        this.kmhString = resources.getString(R.string.kilometers_per_hour);
        this.textSize2Chars = resources.getDimensionPixelSize(R.dimen.tachometer_guage_speed_text_size);
        this.textSize3Chars = resources.getDimensionPixelSize(R.dimen.tachometer_guage_speed_text_size_2);
        this.speedoMeterWidgetName = resources.getString(R.string.gauge_speedo_meter);
    }

    public void setSpeed(int speed) {
        if (speed != -1) {
            long now = SystemClock.elapsedRealtime();
            long newTimeDifferenceBetweenSamples = now - this.lastSpeedSampleArrivalTime;
            this.lastSpeedSampleArrivalTime = now;
            if (newTimeDifferenceBetweenSamples > 1000) {
                this.animationDuration = 100;
            } else {
                this.animationDuration = (int) Math.max(20, Math.min((long) this.animationDuration, newTimeDifferenceBetweenSamples));
            }
            if (this.lastSpeed != speed) {
                int speedVal;
                this.lastSpeed = speed;
                if (speed > MAX_SPEED) {
                    speedVal = MAX_SPEED;
                } else {
                    speedVal = speed;
                }
                this.mSerialValueAnimator.setDuration(this.animationDuration);
                this.mSerialValueAnimator.setValue((float) speedVal);
            }
        }
    }

    public void updateGauge() {
        if (this.mGaugeView != null) {
            int speedLimitThreshold;
            Resources res = this.mGaugeView.getResources();
            this.mSpeedoMeterGaugeDrawable.setGaugeValue((float) this.mSpeed);
            this.mSpeedoMeterGaugeDrawable.setSpeedLimit(this.mSpeedLimit);
            SpeedUnit speedUnit = this.speedManager.getSpeedUnit();
            String unitText = this.mphString;
            switch (speedUnit) {
                case KILOMETERS_PER_HOUR:
                    unitText = this.kmhString;
                    speedLimitThreshold = 13;
                    break;
                default:
                    speedLimitThreshold = 8;
                    unitText = this.mphString;
                    break;
            }
            if (this.updateText) {
                this.mGaugeView.getValueTextView().setTextSize(2, this.mSpeed >= 100 ? (float) this.textSize3Chars : (float) this.textSize2Chars);
                this.mGaugeView.setValueText(Integer.toString(this.mSpeed));
            }
            int state = 0;
            if (this.mSpeedLimit == 0) {
                state = 0;
            } else {
                if (this.mSpeed > this.mSpeedLimit) {
                    state = 1;
                }
                if (this.mSpeed > this.mSpeedLimit + speedLimitThreshold) {
                    state = 2;
                }
            }
            int colorRes = 17170443;
            boolean showSpeedLimitLabel = false;
            switch (state) {
                case 0:
                    colorRes = 17170443;
                    break;
                case 1:
                    colorRes = R.color.cyan;
                    showSpeedLimitLabel = true;
                    break;
                case 2:
                    colorRes = R.color.speed_very_fast_warning_color;
                    showSpeedLimitLabel = true;
                    break;
            }
            this.mGaugeView.getUnitTextView().setTextColor(res.getColor(colorRes));
            this.mSpeedoMeterGaugeDrawable.setState(state);
            this.mSpeedoMeterGaugeDrawable.speedLimitTextAlpha = this.speedLimitAlpha;
            if (showSpeedLimitLabel) {
                this.mGaugeView.setUnitText(res.getString(R.string.speed_limit_with_unit, new Object[]{Integer.valueOf(this.mSpeedLimit), unitText}));
            } else {
                this.mGaugeView.setUnitText(unitText);
            }
        }
    }

    public String getWidgetIdentifier() {
        return ID;
    }

    public String getWidgetName() {
        return this.speedoMeterWidgetName;
    }

    public void setSpeedLimit(int speedLimit) {
        if (speedLimit != this.mSpeedLimit) {
            this.mSpeedLimit = speedLimit;
            showSpeedLimitText();
            if (this.mSpeed < this.mSpeedLimit) {
                showSpeedLimitTextAndFadeOut();
            }
            reDraw();
        }
    }

    private void showSpeedLimitText() {
        this.handler.removeCallbacks(this.speedLimitFadeOutAnimationRunnable);
        this.speedLimitAlpha = 255;
    }

    private void showSpeedLimitTextAndFadeOut() {
        this.speedLimitAlpha = 255;
    }

    public void setView(DashboardWidgetView dashboardWidgetView) {
        if (dashboardWidgetView != null) {
            dashboardWidgetView.setContentView((int) R.layout.speedo_meter_gauge_2);
        }
        super.setView(dashboardWidgetView);
        if (this.mGaugeView != null) {
            if (VERSION.SDK_INT >= 21) {
                TypedValue outValue = new TypedValue();
                this.mGaugeView.getResources().getValue(R.dimen.speedometer_text_letter_spacing, outValue, true);
                this.mGaugeView.getValueTextView().setLetterSpacing(outValue.getFloat());
            }
            reDraw();
        }
    }

    public Drawable getDrawable() {
        return this.mSpeedoMeterGaugeDrawable;
    }

    public float getValue() {
        return (float) this.mSpeed;
    }

    public void setValue(float newValue) {
        int newSpeed = (int) newValue;
        if (newSpeed != this.mSpeed) {
            this.mSpeed = newSpeed;
            this.updateText = false;
            setSpeedValueInternal((float) this.mSpeed);
        }
    }

    private void setSpeedValueInternal(float speed) {
        if (this.mSpeed >= this.mSpeedLimit) {
            showSpeedLimitText();
        } else if (!this.showingSpeedLimitAnimation && this.speedLimitMarkerNeedsTobeRemoved) {
            showSpeedLimitTextAndFadeOut();
        }
        reDraw();
    }

    public void animationComplete(float newValue) {
        this.updateText = true;
        setSpeedValueInternal(newValue);
    }
}
