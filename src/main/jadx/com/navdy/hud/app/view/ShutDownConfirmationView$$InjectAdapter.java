package com.navdy.hud.app.view;

import com.navdy.hud.app.screen.ShutDownScreen.Presenter;
import dagger.MembersInjector;
import dagger.internal.Binding;
import dagger.internal.Linker;
import java.util.Set;

public final class ShutDownConfirmationView$$InjectAdapter extends Binding<ShutDownConfirmationView> implements MembersInjector<ShutDownConfirmationView> {
    private Binding<Presenter> mPresenter;

    public ShutDownConfirmationView$$InjectAdapter() {
        super(null, "members/com.navdy.hud.app.view.ShutDownConfirmationView", false, ShutDownConfirmationView.class);
    }

    public void attach(Linker linker) {
        this.mPresenter = linker.requestBinding("com.navdy.hud.app.screen.ShutDownScreen$Presenter", ShutDownConfirmationView.class, getClass().getClassLoader());
    }

    public void getDependencies(Set<Binding<?>> set, Set<Binding<?>> injectMembersBindings) {
        injectMembersBindings.add(this.mPresenter);
    }

    public void injectMembers(ShutDownConfirmationView object) {
        object.mPresenter = (Presenter) this.mPresenter.get();
    }
}
