package com.navdy.hud.app.view;

import com.navdy.hud.app.screen.AutoBrightnessScreen.Presenter;
import dagger.MembersInjector;
import dagger.internal.Binding;
import dagger.internal.Linker;
import java.util.Set;

public final class AutoBrightnessView$$InjectAdapter extends Binding<AutoBrightnessView> implements MembersInjector<AutoBrightnessView> {
    private Binding<Presenter> presenter;

    public AutoBrightnessView$$InjectAdapter() {
        super(null, "members/com.navdy.hud.app.view.AutoBrightnessView", false, AutoBrightnessView.class);
    }

    public void attach(Linker linker) {
        this.presenter = linker.requestBinding("com.navdy.hud.app.screen.AutoBrightnessScreen$Presenter", AutoBrightnessView.class, getClass().getClassLoader());
    }

    public void getDependencies(Set<Binding<?>> set, Set<Binding<?>> injectMembersBindings) {
        injectMembersBindings.add(this.presenter);
    }

    public void injectMembers(AutoBrightnessView object) {
        object.presenter = (Presenter) this.presenter.get();
    }
}
