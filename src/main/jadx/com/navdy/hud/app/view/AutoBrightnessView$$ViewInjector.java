package com.navdy.hud.app.view;

import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.ButterKnife.Finder;
import com.navdy.hud.app.R;
import com.navdy.hud.app.ui.component.ChoiceLayout;

public class AutoBrightnessView$$ViewInjector {
    public static void inject(Finder finder, AutoBrightnessView target, Object source) {
        target.autoBrightnessTitle = (TextView) finder.findRequiredView(source, R.id.mainTitle, "field 'autoBrightnessTitle'");
        target.autoBrightnessIcon = (ImageView) finder.findRequiredView(source, R.id.image, "field 'autoBrightnessIcon'");
        target.title1 = (TextView) finder.findRequiredView(source, R.id.title1, "field 'title1'");
        target.brightnessTitle = (TextView) finder.findRequiredView(source, R.id.title2, "field 'brightnessTitle'");
        target.brightnessTitleDesc = (TextView) finder.findRequiredView(source, R.id.title3, "field 'brightnessTitleDesc'");
        target.infoContainer = (LinearLayout) finder.findRequiredView(source, R.id.infoContainer, "field 'infoContainer'");
        target.autoBrightnessChoices = (ChoiceLayout) finder.findRequiredView(source, R.id.choiceLayout, "field 'autoBrightnessChoices'");
    }

    public static void reset(AutoBrightnessView target) {
        target.autoBrightnessTitle = null;
        target.autoBrightnessIcon = null;
        target.title1 = null;
        target.brightnessTitle = null;
        target.brightnessTitleDesc = null;
        target.infoContainer = null;
        target.autoBrightnessChoices = null;
    }
}
