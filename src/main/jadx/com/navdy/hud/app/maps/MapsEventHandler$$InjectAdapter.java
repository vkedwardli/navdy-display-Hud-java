package com.navdy.hud.app.maps;

import android.content.SharedPreferences;
import com.navdy.hud.app.profile.DriverProfileManager;
import com.navdy.hud.app.service.ConnectionHandler;
import com.squareup.otto.Bus;
import dagger.MembersInjector;
import dagger.internal.Binding;
import dagger.internal.Linker;
import java.util.Set;

public final class MapsEventHandler$$InjectAdapter extends Binding<MapsEventHandler> implements MembersInjector<MapsEventHandler> {
    private Binding<Bus> bus;
    private Binding<ConnectionHandler> connectionHandler;
    private Binding<DriverProfileManager> mDriverProfileManager;
    private Binding<SharedPreferences> sharedPreferences;

    public MapsEventHandler$$InjectAdapter() {
        super(null, "members/com.navdy.hud.app.maps.MapsEventHandler", false, MapsEventHandler.class);
    }

    public void attach(Linker linker) {
        this.connectionHandler = linker.requestBinding("com.navdy.hud.app.service.ConnectionHandler", MapsEventHandler.class, getClass().getClassLoader());
        this.bus = linker.requestBinding("com.squareup.otto.Bus", MapsEventHandler.class, getClass().getClassLoader());
        this.sharedPreferences = linker.requestBinding("android.content.SharedPreferences", MapsEventHandler.class, getClass().getClassLoader());
        this.mDriverProfileManager = linker.requestBinding("com.navdy.hud.app.profile.DriverProfileManager", MapsEventHandler.class, getClass().getClassLoader());
    }

    public void getDependencies(Set<Binding<?>> set, Set<Binding<?>> injectMembersBindings) {
        injectMembersBindings.add(this.connectionHandler);
        injectMembersBindings.add(this.bus);
        injectMembersBindings.add(this.sharedPreferences);
        injectMembersBindings.add(this.mDriverProfileManager);
    }

    public void injectMembers(MapsEventHandler object) {
        object.connectionHandler = (ConnectionHandler) this.connectionHandler.get();
        object.bus = (Bus) this.bus.get();
        object.sharedPreferences = (SharedPreferences) this.sharedPreferences.get();
        object.mDriverProfileManager = (DriverProfileManager) this.mDriverProfileManager.get();
    }
}
