package com.navdy.hud.app.maps.here;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.TextAppearanceSpan;
import com.here.android.mpa.common.GeoCoordinate;
import com.here.android.mpa.common.Image;
import com.here.android.mpa.common.PositioningManager;
import com.here.android.mpa.common.RoadElement;
import com.here.android.mpa.common.RoadElement.Attribute;
import com.here.android.mpa.guidance.TrafficNotification;
import com.here.android.mpa.guidance.TrafficNotificationInfo;
import com.here.android.mpa.mapping.TrafficEvent;
import com.here.android.mpa.routing.Maneuver;
import com.here.android.mpa.routing.Maneuver.Action;
import com.here.android.mpa.routing.Maneuver.Icon;
import com.here.android.mpa.routing.Maneuver.TrafficDirection;
import com.here.android.mpa.routing.Route;
import com.here.android.mpa.routing.Route.TrafficPenaltyMode;
import com.here.android.mpa.routing.RouteElement;
import com.here.android.mpa.routing.RouteTta;
import com.here.android.mpa.routing.Signpost;
import com.here.android.mpa.routing.Signpost.LocalizedLabel;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.R;
import com.navdy.hud.app.framework.glance.GlanceConstants;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.maps.MapEvents.ManeuverDisplay;
import com.navdy.hud.app.profile.HudLocale;
import com.navdy.hud.app.storage.PathManager;
import com.navdy.hud.app.ui.component.homescreen.HomeScreenView;
import com.navdy.hud.app.util.GenericUtil;
import com.navdy.service.library.device.NavdyDeviceId;
import com.navdy.service.library.events.Ext_NavdyEvent;
import com.navdy.service.library.events.navigation.NavigationRouteRequest;
import com.navdy.service.library.events.navigation.NavigationRouteRequest.Builder;
import com.navdy.service.library.events.navigation.NavigationRouteRequest.RouteAttribute;
import com.navdy.service.library.events.navigation.NavigationSessionState;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.task.TaskManager;
import com.navdy.service.library.util.IOUtils;
import com.navdy.service.library.util.MusicDataUtils;
import com.squareup.wire.Wire;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

public class HereMapUtil {
    private static final String ACTIVE_GAS_ROUTE_INFO_REQUEST = "gasrouterequest.bin";
    private static final String ACTIVE_ROUTE_INFO_DEVICE_ID = "routerequest.device";
    private static final String ACTIVE_ROUTE_INFO_REQUEST = "routerequest.bin";
    private static final int EQUALITY_DISTANCE_VARIATION = 500;
    private static final long INVALID_ETA_TIME_DIFF = 360000;
    public static final double MILES_TO_METERS = 1609.34d;
    public static final int SHORT_DISTANCE_IN_METERS = 20;
    private static final long STALE_ROUTE_TIME = TimeUnit.HOURS.toMillis(3);
    public static final String TBT_ISO3_LANG_CODE = new Locale(HudLocale.DEFAULT_LANGUAGE, "US").getISO3Language().toUpperCase();
    private static final boolean VERBOSE = false;
    private static final String day;
    private static final String hr;
    private static String lastKnownDeviceId;
    private static final String min;
    private static final String min_short;
    private static final Logger sLogger = new Logger(HereMapUtil.class);
    private static final StringBuilder signPostBuilder = new StringBuilder();
    private static final StringBuilder signPostBuilder2 = new StringBuilder();
    private static final HashSet<String> signPostSet = new HashSet();
    private static final TextAppearanceSpan sp_20_b_1;
    private static final TextAppearanceSpan sp_20_b_2;
    private static final TextAppearanceSpan sp_24_1;
    private static final TextAppearanceSpan sp_24_2;
    private static final TextAppearanceSpan sp_26_1;
    private static final TextAppearanceSpan sp_26_2;
    private static final StringBuilder stringBuilder = new StringBuilder();
    private static final StringBuilder trafficEventBuilder = new StringBuilder();
    private static final Wire wire = new Wire(Ext_NavdyEvent.class);

    public interface IImageLoadCallback {
        void result(Image image, Bitmap bitmap);
    }

    public static class LongContainer {
        public long val;
    }

    public static class SavedRouteData {
        public final boolean isGasRoute;
        public final NavigationRouteRequest navigationRouteRequest;

        public SavedRouteData(NavigationRouteRequest navigationRouteRequest, boolean isGasRoute) {
            this.navigationRouteRequest = navigationRouteRequest;
            this.isGasRoute = isGasRoute;
        }
    }

    public static class SignPostInfo {
        public boolean hasNameInSignPost;
        public boolean hasNumberInSignPost;
    }

    static {
        Context context = HudApplication.getAppContext();
        Resources resources = context.getResources();
        sp_20_b_1 = new TextAppearanceSpan(context, R.style.route_duration_20_b);
        sp_20_b_2 = new TextAppearanceSpan(context, R.style.route_duration_20_b);
        sp_24_1 = new TextAppearanceSpan(context, R.style.route_duration_24);
        sp_24_2 = new TextAppearanceSpan(context, R.style.route_duration_24);
        sp_26_1 = new TextAppearanceSpan(context, R.style.route_duration_26);
        sp_26_2 = new TextAppearanceSpan(context, R.style.route_duration_26);
        min = resources.getString(R.string.route_time_min);
        min_short = resources.getString(R.string.route_time_min_short);
        hr = resources.getString(R.string.route_time_hr);
        day = resources.getString(R.string.route_time_day);
    }

    public static RoadElement getCurrentRoadElement() {
        PositioningManager positioningManager = HereMapsManager.getInstance().getPositioningManager();
        if (positioningManager == null) {
            return null;
        }
        return positioningManager.getRoadElement();
    }

    public static String getCurrentRoadName() {
        PositioningManager positioningManager = HereMapsManager.getInstance().getPositioningManager();
        if (positioningManager == null) {
            return null;
        }
        RoadElement roadElement = positioningManager.getRoadElement();
        if (roadElement != null) {
            return getRoadName(roadElement);
        }
        return null;
    }

    public static String getRoadName(List<RoadElement> roadElements) {
        if (roadElements == null || roadElements.size() == 0) {
            return "";
        }
        for (RoadElement roadElement : roadElements) {
            String name = roadElement.getRoadName();
            if (!TextUtils.isEmpty(name)) {
                return name;
            }
            name = roadElement.getRouteName();
            if (!TextUtils.isEmpty(name)) {
                return name;
            }
        }
        return "";
    }

    public static String getRoadName(Maneuver maneuver) {
        return concatText(maneuver.getRoadNumber(), maneuver.getRoadName(), true, isCurrentRoadHighway(maneuver));
    }

    public static String getNextRoadName(Maneuver maneuver) {
        return concatText(maneuver.getNextRoadNumber(), maneuver.getNextRoadName(), true, isCurrentRoadHighway(maneuver));
    }

    public static String getRoadName(RoadElement roadElement) {
        if (roadElement == null) {
            return null;
        }
        return concatText(roadElement.getRouteName(), roadElement.getRoadName(), true, isCurrentRoadHighway(roadElement));
    }

    public static float getCurrentSpeedLimit() {
        RoadElement roadElement = getCurrentRoadElement();
        if (roadElement != null) {
            return roadElement.getSpeedLimit();
        }
        return 0.0f;
    }

    public static boolean isCurrentRoadHighway(RoadElement roadElement) {
        if (roadElement != null && roadElement.getAttributes().contains(Attribute.HIGHWAY)) {
            return true;
        }
        return false;
    }

    public static boolean isCurrentRoadHighway(Maneuver maneuver) {
        if (maneuver == null) {
            return false;
        }
        if (isManeuverActionHighway(maneuver.getAction())) {
            return true;
        }
        List<RoadElement> elements = maneuver.getRoadElements();
        if (elements == null || elements.size() == 0) {
            return false;
        }
        return isCurrentRoadHighway((RoadElement) elements.get(0));
    }

    public static boolean isManeuverActionHighway(Action action) {
        if (action == null) {
            return false;
        }
        switch (action) {
            case CONTINUE_HIGHWAY:
            case CHANGE_HIGHWAY:
            case ENTER_HIGHWAY:
            case ENTER_HIGHWAY_FROM_LEFT:
            case ENTER_HIGHWAY_FROM_RIGHT:
                return true;
            default:
                return false;
        }
    }

    public static boolean isManeuverShort(Maneuver maneuver) {
        if (maneuver.getDistanceToNextManeuver() <= 20) {
            return true;
        }
        return false;
    }

    public static boolean isStartManeuver(Maneuver maneuver) {
        if (maneuver != null && maneuver.getIcon() == Icon.START) {
            return true;
        }
        return false;
    }

    public static void printRouteDetails(Route route, String streetAddress, NavigationRouteRequest request, StringBuilder buffer) {
        printRouteDetails(route, streetAddress, request, buffer, null);
    }

    public static void printRouteDetails(Route route, String streetAddress, NavigationRouteRequest request, StringBuilder buffer, @Nullable Maneuver printFromManeuver) {
        if (route != null) {
            long l1 = SystemClock.elapsedRealtime();
            String routeInfo = "[Route] start[" + route.getStart() + "] end[" + route.getDestination() + "] length[" + route.getLength() + " meters] tta[" + route.getTta(TrafficPenaltyMode.OPTIMAL, 268435455).getDuration() + " seconds]";
            if (buffer == null) {
                sLogger.v(routeInfo);
            } else {
                buffer.append(routeInfo + GlanceConstants.NEWLINE);
            }
            List<Maneuver> maneuvers = route.getManeuvers();
            if (printFromManeuver != null) {
                int fromManeuverIndex = findManeuverIndex(maneuvers, printFromManeuver);
                if (fromManeuverIndex >= 0) {
                    maneuvers = maneuvers.subList(fromManeuverIndex, maneuvers.size());
                } else {
                    return;
                }
            }
            int len = maneuvers.size();
            Maneuver previous = null;
            Maneuver current = null;
            for (int i = 0; i < len; i++) {
                ManeuverDisplay display;
                if (current != null) {
                    previous = current;
                }
                current = (Maneuver) maneuvers.get(i);
                Maneuver after = null;
                if (i < len - 1) {
                    after = (Maneuver) maneuvers.get(i + 1);
                }
                String iconStr = "";
                Icon icon = current.getIcon();
                if (icon != null) {
                    iconStr = icon.name();
                }
                String directionStr = "";
                TrafficDirection trafficDirection = current.getTrafficDirection();
                if (trafficDirection != null) {
                    directionStr = trafficDirection.name();
                }
                String maneuverRaw = "    [Maneuver-Raw] turn[" + current.getTurn().name() + "] to[" + current.getNextRoadNumber() + " " + current.getNextRoadName() + "] distance[" + current.getDistanceToNextManeuver() + " meters] action[" + current.getAction() + "] current[" + current.getRoadNumber() + " " + current.getRoadName() + "] traffic-directon[" + directionStr + "] angle[" + current.getAngle() + "] orientation[" + current.getMapOrientation() + "] distanceFromStart[" + current.getDistanceFromStart() + " meters] Icon[" + iconStr + "]";
                if (buffer != null) {
                    buffer.append(maneuverRaw + GlanceConstants.NEWLINE);
                } else {
                    sLogger.v(maneuverRaw);
                }
                Signpost signpost = current.getSignpost();
                if (signpost != null) {
                    String rawSignPost = "    [Maneuver-Raw-SignPost] exitNumber[" + signpost.getExitNumber() + "] exitText[" + signpost.getExitText() + "]";
                    if (buffer != null) {
                        buffer.append(rawSignPost);
                    } else {
                        sLogger.v(rawSignPost);
                    }
                    for (LocalizedLabel label : signpost.getExitDirections()) {
                        String signpostLabel = "     [Maneuver-Raw-SignPost-Label]  text[" + label.getText() + "] routeDirection[" + label.getRouteDirection() + "] routeName[" + label.getRouteName() + "]";
                        if (buffer != null) {
                            buffer.append(signpostLabel + GlanceConstants.NEWLINE);
                        } else {
                            sLogger.v(signpostLabel);
                        }
                    }
                }
                if (i == 0) {
                    display = HereManeuverDisplayBuilder.getStartManeuverDisplay(current, after);
                } else {
                    display = HereManeuverDisplayBuilder.getManeuverDisplay(current, after == null, (long) current.getDistanceToNextManeuver(), streetAddress, previous, request, after, true, false, null);
                }
                sLogger.v("[Maneuver-Display] " + display.toString());
                String shortTbt = "[Maneuver-Display-short] " + display.pendingTurn + " " + display.pendingRoad + " ( " + display.distanceToPendingRoadText + " )";
                if (buffer != null) {
                    buffer.append(shortTbt + GlanceConstants.NEWLINE);
                } else {
                    sLogger.v(shortTbt);
                }
            }
            sLogger.v("Time to print route:" + (SystemClock.elapsedRealtime() - l1));
        }
    }

    public static void printManeuverDetails(Maneuver maneuver, String tag) {
        sLogger.v(tag + " turn[" + maneuver.getTurn().name() + "] to[" + maneuver.getNextRoadNumber() + " " + maneuver.getNextRoadName() + "] distance[" + maneuver.getDistanceToNextManeuver() + " meters] action[" + maneuver.getAction() + "] current[" + maneuver.getRoadNumber() + " " + maneuver.getRoadName() + "] distanceFromStart[" + maneuver.getDistanceFromStart() + " meters]");
    }

    static boolean isValidNavigationState(NavigationSessionState navigationSessionState) {
        if (navigationSessionState == null || (navigationSessionState != NavigationSessionState.NAV_SESSION_STARTED && navigationSessionState != NavigationSessionState.NAV_SESSION_PAUSED && navigationSessionState != NavigationSessionState.NAV_SESSION_STOPPED)) {
            return false;
        }
        return true;
    }

    public static String parseStreetAddress(String streetAddress) {
        if (streetAddress == null) {
            return null;
        }
        int index = streetAddress.indexOf(HereManeuverDisplayBuilder.COMMA);
        if (index >= 0) {
            return streetAddress.substring(0, index);
        }
        return streetAddress;
    }

    public static boolean areManeuverEqual(Maneuver maneuver, Maneuver other) {
        if (maneuver == null || other == null) {
            return false;
        }
        boolean ret = maneuver.getRoadElements().equals(other.getRoadElements());
        if (ret) {
            return ret;
        }
        try {
            if (maneuver.getTurn() != other.getTurn() || !TextUtils.equals(maneuver.getNextRoadNumber(), other.getNextRoadNumber()) || !TextUtils.equals(maneuver.getNextRoadName(), other.getNextRoadName()) || !TextUtils.equals(maneuver.getRoadNumber(), other.getRoadNumber()) || !TextUtils.equals(maneuver.getRoadName(), other.getRoadName()) || maneuver.getAction() != other.getAction() || Math.abs(maneuver.getDistanceToNextManeuver() - other.getDistanceToNextManeuver()) > 500) {
                return ret;
            }
            if (!sLogger.isLoggable(2)) {
                return true;
            }
            sLogger.v("areManeuverEqual: distance diff=" + Math.abs(maneuver.getDistanceToNextManeuver() - other.getDistanceToNextManeuver()) + " allowance=" + 500);
            return true;
        } catch (Throwable t) {
            sLogger.e(t);
            return ret;
        }
    }

    static List<Maneuver> getDifferentManeuver(Route route, Route other, int n, List<Maneuver> otherManeuverDifference) {
        List<Maneuver> result = new ArrayList();
        List<Maneuver> routeManeuvers = route.getManeuvers();
        List<Maneuver> otherManeuvers = other.getManeuvers();
        int size1 = routeManeuvers.size();
        int size2 = otherManeuvers.size();
        int maxIndex = size1 > size2 ? size2 - 1 : size1 - 1;
        int i = 0;
        while (i <= maxIndex) {
            Maneuver routeManeuver = (Maneuver) routeManeuvers.get(i);
            Maneuver otherManeuver = (Maneuver) otherManeuvers.get(i);
            if (sLogger.isLoggable(2)) {
                printManeuverDetails(routeManeuver, "getDifferentManeuver-1");
                printManeuverDetails(otherManeuver, "getDifferentManeuver-2");
            }
            if (!areManeuverEqual(routeManeuver, otherManeuver)) {
                String s = getNextRoadName(routeManeuver);
                if (!TextUtils.isEmpty(s)) {
                    if (sLogger.isLoggable(2)) {
                        sLogger.v("getDifferentManeuver:diff:" + s);
                    }
                    result.add(routeManeuver);
                    otherManeuverDifference.add(otherManeuver);
                    n--;
                    if (n == 0) {
                        break;
                    }
                }
                i++;
            }
            i++;
        }
        return result;
    }

    static boolean areManeuversEqual(List<Maneuver> first, int firstOffset, List<Maneuver> second, int secondOffset, List<Maneuver> difference) {
        int len1 = first.size() - firstOffset;
        int len2 = second.size() - secondOffset;
        int secondOffset2 = secondOffset;
        int firstOffset2 = firstOffset;
        while (len1 > 0 && len2 > 0) {
            firstOffset = firstOffset2 + 1;
            Maneuver m1 = (Maneuver) first.get(firstOffset2);
            secondOffset = secondOffset2 + 1;
            Maneuver m2 = (Maneuver) second.get(secondOffset2);
            if (areManeuverEqual(m1, m2)) {
                if (sLogger.isLoggable(2)) {
                    sLogger.v("maneuver are equal");
                    printManeuverDetails(m1, "[maneuver-1]");
                    printManeuverDetails(m2, "[maneuver-2]");
                }
                len1--;
                len2--;
                secondOffset2 = secondOffset;
                firstOffset2 = firstOffset;
            } else {
                sLogger.v("maneuver are NOT equal");
                printManeuverDetails(m1, "[maneuver-1]");
                printManeuverDetails(m2, "[maneuver-2]");
                if (difference == null) {
                    return false;
                }
                difference.add(m1);
                return false;
            }
        }
        if (len1 > 0 || len2 > 0) {
            if (len1 > 0 && difference != null) {
                difference.add(first.get(firstOffset2));
            }
            secondOffset = secondOffset2;
            firstOffset = firstOffset2;
            return false;
        }
        secondOffset = secondOffset2;
        firstOffset = firstOffset2;
        return true;
    }

    static void printTrafficNotification(TrafficNotification trafficNotification, String tag) {
        List<TrafficNotificationInfo> list = trafficNotification.getInfoList();
        if (list != null) {
            for (TrafficNotificationInfo trafficNotificationInfo : list) {
                sLogger.v(tag + " type:" + trafficNotificationInfo.getType().name());
                sLogger.v(tag + " distance:" + trafficNotificationInfo.getDistanceInMeters());
                printTrafficEvent(trafficNotificationInfo.getEvent(), tag, null);
            }
        }
    }

    static void printTrafficEvent(TrafficEvent event, String tag, Route route) {
        synchronized (trafficEventBuilder) {
            Object obj;
            trafficEventBuilder.setLength(0);
            trafficEventBuilder.append(tag);
            trafficEventBuilder.append(":: ");
            trafficEventBuilder.append("severity:" + event.getSeverity().name());
            trafficEventBuilder.append(", short-text:" + event.getShortText());
            trafficEventBuilder.append(", text:" + event.getEventText());
            trafficEventBuilder.append(", isActive:" + event.isActive());
            trafficEventBuilder.append(", isFlow:" + event.isFlow());
            trafficEventBuilder.append(", isIncident:" + event.isIncident());
            trafficEventBuilder.append(", isVisible:" + event.isVisible());
            StringBuilder stringBuilder = trafficEventBuilder;
            StringBuilder append = new StringBuilder().append(", isOnRoute:");
            if (route == null) {
                obj = "n/a";
            } else {
                obj = Boolean.valueOf(event.isOnRoute(route));
            }
            stringBuilder.append(append.append(obj).toString());
            trafficEventBuilder.append(", isReroutable:" + event.isReroutable());
            trafficEventBuilder.append(", affected len:" + event.getAffectedLength());
            trafficEventBuilder.append(", offRoute icon:" + event.getIconOffRoute());
            trafficEventBuilder.append(", onRoute icon:" + event.getIconOnRoute());
            trafficEventBuilder.append(", firstAffectedStreet:" + event.getFirstAffectedStreet());
            trafficEventBuilder.append(", activateDate:" + event.getActivationDate());
            trafficEventBuilder.append(", updateDate:" + event.getUpdateDate());
            trafficEventBuilder.append(", now:" + new Date());
            List<String> affectedStreets = event.getAffectedStreets();
            trafficEventBuilder.append(", affectedStreet: ");
            if (affectedStreets != null) {
                for (String s : affectedStreets) {
                    trafficEventBuilder.append(s);
                    trafficEventBuilder.append(", ");
                }
            }
            sLogger.v(trafficEventBuilder.toString());
            trafficEventBuilder.setLength(0);
        }
    }

    public static Maneuver getLastEqualManeuver(Route route, Route other) {
        int maxIndex;
        List<Maneuver> routeManeuvers = route.getManeuvers();
        List<Maneuver> otherManeuvers = other.getManeuvers();
        if (routeManeuvers.size() < otherManeuvers.size()) {
            maxIndex = routeManeuvers.size() - 1;
        } else {
            maxIndex = otherManeuvers.size() - 1;
        }
        int i = 0;
        while (i <= maxIndex) {
            if (areManeuverEqual((Maneuver) routeManeuvers.get(i), (Maneuver) otherManeuvers.get(i)) && i > 0) {
                return (Maneuver) routeManeuvers.get(i - 1);
            }
            i++;
        }
        return null;
    }

    public static boolean isValidEtaDate(Date etaDate) {
        if (etaDate == null) {
            return false;
        }
        int year = etaDate.getYear();
        if (etaDate == null || year <= 71) {
            sLogger.e("invalid eta date[" + etaDate + "]");
            return false;
        }
        long timeDiff = System.currentTimeMillis() - etaDate.getTime();
        if (timeDiff >= INVALID_ETA_TIME_DIFF) {
            sLogger.e("invalid eta timediff[" + timeDiff + "]");
            return false;
        }
        if (timeDiff > 0) {
            sLogger.i("eta is behind timediff[" + timeDiff + "]");
        }
        return true;
    }

    public static Date getRouteTtaDate(Route route) {
        if (route == null) {
            return null;
        }
        try {
            RouteTta tta = route.getTta(TrafficPenaltyMode.OPTIMAL, 268435455);
            if (tta != null && tta.getDuration() == 0) {
                tta = null;
            }
            if (tta == null) {
                tta = route.getTta(TrafficPenaltyMode.DISABLED, 268435455);
            }
            if (tta == null) {
                return null;
            }
            return new Date(System.currentTimeMillis() + TimeUnit.SECONDS.toMillis((long) tta.getDuration()));
        } catch (Throwable t) {
            sLogger.e(t);
            return null;
        }
    }

    public static void saveRouteInfo(NavigationRouteRequest request, NavdyDeviceId deviceId, Logger logger) {
        saveRouteInfoInternal(request, deviceId, logger, ACTIVE_ROUTE_INFO_REQUEST);
    }

    public static void saveGasRouteInfo(NavigationRouteRequest request, NavdyDeviceId deviceId, Logger sLogger) {
        saveRouteInfoInternal(request, deviceId, sLogger, ACTIVE_GAS_ROUTE_INFO_REQUEST);
    }

    private static synchronized void saveRouteInfoInternal(NavigationRouteRequest r, NavdyDeviceId deviceId, Logger logger, String fileName) {
        Throwable t;
        List<RouteAttribute> routeAttributes;
        NavigationRouteRequest request;
        String path;
        FileOutputStream file;
        byte[] data;
        Throwable th;
        synchronized (HereMapUtil.class) {
            FileOutputStream file2 = null;
            lastKnownDeviceId = null;
            if (deviceId == null) {
                sLogger.v("saveRouteInfo: null deviceid");
                try {
                    NavdyDeviceId deviceId2 = new NavdyDeviceId(RemoteDeviceManager.getInstance().getConnectionHandler().getLastConnectedDeviceInfo().deviceId);
                    try {
                        sLogger.v("saveRouteInfo: last deviceid found:" + deviceId2);
                        deviceId = deviceId2;
                    } catch (Throwable th2) {
                        th = th2;
                        deviceId = deviceId2;
                        throw th;
                    }
                } catch (Throwable th3) {
                    t = th3;
                    sLogger.e("saveRouteInfo", t);
                    logger.v("saveRouteInfo request[" + r + "] deviceId[" + deviceId + "]");
                    if (r.destination == null) {
                        routeAttributes = new ArrayList(r.routeAttributes);
                        if (routeAttributes.contains(RouteAttribute.ROUTE_ATTRIBUTE_SAVED_ROUTE)) {
                            routeAttributes.add(RouteAttribute.ROUTE_ATTRIBUTE_SAVED_ROUTE);
                        }
                        request = new Builder(r).routeAttributes(routeAttributes).build();
                        sLogger.v("saveRouteInfo added saved route attribute");
                        path = PathManager.getInstance().getActiveRouteInfoDir();
                        file = new FileOutputStream(path + File.separator + fileName);
                        data = request.toByteArray();
                        file.write(data);
                        IOUtils.fileSync(file);
                        IOUtils.closeStream(file);
                        file2 = null;
                        logger.v("saveRouteInfo: route info size[" + data.length + "]");
                        if (deviceId != null) {
                            logger.v("saveRouteInfo: device id not written:null");
                        } else {
                            file = new FileOutputStream(path + File.separator + ACTIVE_ROUTE_INFO_DEVICE_ID);
                            data = deviceId.toString().getBytes();
                            file.write(data);
                            IOUtils.fileSync(file);
                            IOUtils.closeStream(file);
                            file2 = null;
                            lastKnownDeviceId = deviceId.toString();
                            logger.v("saveRouteInfo: device id size[" + data.length + "]");
                        }
                    } else {
                        logger.w("route does not have destination");
                        removeRouteInfoFiles(sLogger, true);
                    }
                    return;
                }
            }
            logger.v("saveRouteInfo request[" + r + "] deviceId[" + deviceId + "]");
            if (r.destination == null) {
                logger.w("route does not have destination");
                removeRouteInfoFiles(sLogger, true);
            } else {
                routeAttributes = new ArrayList(r.routeAttributes);
                if (routeAttributes.contains(RouteAttribute.ROUTE_ATTRIBUTE_SAVED_ROUTE)) {
                    routeAttributes.add(RouteAttribute.ROUTE_ATTRIBUTE_SAVED_ROUTE);
                }
                request = new Builder(r).routeAttributes(routeAttributes).build();
                sLogger.v("saveRouteInfo added saved route attribute");
                path = PathManager.getInstance().getActiveRouteInfoDir();
                file = new FileOutputStream(path + File.separator + fileName);
                data = request.toByteArray();
                file.write(data);
                IOUtils.fileSync(file);
                IOUtils.closeStream(file);
                file2 = null;
                logger.v("saveRouteInfo: route info size[" + data.length + "]");
                if (deviceId != null) {
                    file = new FileOutputStream(path + File.separator + ACTIVE_ROUTE_INFO_DEVICE_ID);
                    data = deviceId.toString().getBytes();
                    file.write(data);
                    IOUtils.fileSync(file);
                    IOUtils.closeStream(file);
                    file2 = null;
                    lastKnownDeviceId = deviceId.toString();
                    logger.v("saveRouteInfo: device id size[" + data.length + "]");
                } else {
                    logger.v("saveRouteInfo: device id not written:null");
                }
            }
        }
    }

    public static synchronized void removeRouteInfo(Logger logger, boolean clearAll) {
        synchronized (HereMapUtil.class) {
            removeRouteInfoFiles(logger, clearAll);
        }
    }

    private static void removeRouteInfoFiles(Logger logger, boolean clearAll) {
        try {
            Context context = HudApplication.getAppContext();
            String path = PathManager.getInstance().getActiveRouteInfoDir();
            boolean b1 = IOUtils.deleteFile(context, path + File.separator + ACTIVE_GAS_ROUTE_INFO_REQUEST);
            logger.v("removeRouteInfo delete gasRoute[" + b1 + "]");
            if (!b1 || clearAll) {
                boolean b2 = IOUtils.deleteFile(context, path + File.separator + ACTIVE_ROUTE_INFO_REQUEST);
                logger.v("removeRouteInfo delete route[" + b2 + "] delete id[" + IOUtils.deleteFile(context, path + File.separator + ACTIVE_ROUTE_INFO_DEVICE_ID) + "]");
            }
        } catch (Throwable t) {
            sLogger.e(t);
        }
    }

    public static synchronized SavedRouteData getSavedRouteData(Logger logger) {
        SavedRouteData savedRouteData;
        synchronized (HereMapUtil.class) {
            GenericUtil.checkNotOnMainThread();
            NavigationRouteRequest request = getSavedRouteRequest(logger);
            boolean isGasRoute = request != null && request.routeAttributes.contains(RouteAttribute.ROUTE_ATTRIBUTE_GAS);
            savedRouteData = new SavedRouteData(request, isGasRoute);
        }
        return savedRouteData;
    }

    private static NavigationRouteRequest getSavedRouteRequest(Logger logger) {
        Throwable t;
        GenericUtil.checkNotOnMainThread();
        logger.v("getSavedRouteRequest");
        FileInputStream file = null;
        try {
            long modified;
            Logger logger2;
            lastKnownDeviceId = null;
            String path = PathManager.getInstance().getActiveRouteInfoDir();
            File f = new File(path + File.separator + ACTIVE_GAS_ROUTE_INFO_REQUEST);
            boolean gasRouteExists = false;
            if (f.exists()) {
                if (isRouteFileStale(f)) {
                    modified = f.lastModified();
                    logger2 = logger;
                    logger2.v("getSavedRouteRequest: gas route is stale file-time =" + modified + " now=" + System.currentTimeMillis() + " deleted=" + IOUtils.deleteFile(HudApplication.getAppContext(), f.getAbsolutePath()));
                } else {
                    gasRouteExists = true;
                    logger.v("getSavedRouteRequest: gas route is good, file-time =" + f.lastModified() + " now=" + System.currentTimeMillis());
                }
            }
            if (!gasRouteExists) {
                logger.v("getSavedRouteRequest: no saved gas route");
                f = new File(path + File.separator + ACTIVE_ROUTE_INFO_REQUEST);
                if (!f.exists()) {
                    logger.v("getSavedRouteRequest: no saved route");
                    return null;
                } else if (isRouteFileStale(f)) {
                    modified = f.lastModified();
                    logger2 = logger;
                    logger2.v("getSavedRouteRequest: route is stale file-time =" + modified + " now=" + System.currentTimeMillis() + " deleted=" + IOUtils.deleteFile(HudApplication.getAppContext(), f.getAbsolutePath()));
                    return null;
                } else {
                    logger.v("getSavedRouteRequest: route is good file-time =" + f.lastModified() + " now=" + System.currentTimeMillis());
                }
            }
            InputStream file2 = new FileInputStream(f);
            try {
                NavigationRouteRequest request = new Builder((NavigationRouteRequest) wire.parseFrom(file2, NavigationRouteRequest.class)).requestId(UUID.randomUUID().toString()).build();
                IOUtils.closeStream(file2);
                file = null;
                f = new File(path + File.separator + ACTIVE_ROUTE_INFO_DEVICE_ID);
                if (f.exists()) {
                    String deviceId = IOUtils.convertFileToString(f.getAbsolutePath());
                    NavdyDeviceId id = RemoteDeviceManager.getInstance().getDeviceId();
                    if (id == null || id.equals(new NavdyDeviceId(deviceId))) {
                        lastKnownDeviceId = deviceId;
                        logger.v("getSavedRouteRequest: returning route");
                        return request;
                    }
                    logger.v("getSavedRouteRequest: device id is different now[" + id + "] stored[" + deviceId + "]");
                    removeRouteInfo(logger, true);
                    return null;
                }
                logger.v("getSavedRouteRequest: no saved device id");
                return request;
            } catch (Throwable th) {
                t = th;
                InputStream file3 = file2;
                sLogger.e(t);
                IOUtils.closeStream(file);
                removeRouteInfo(logger, true);
                lastKnownDeviceId = null;
                return null;
            }
        } catch (Throwable th2) {
            t = th2;
            sLogger.e(t);
            IOUtils.closeStream(file);
            removeRouteInfo(logger, true);
            lastKnownDeviceId = null;
            return null;
        }
    }

    public static NavdyDeviceId getLastKnownDeviceId() {
        if (lastKnownDeviceId == null) {
            return null;
        }
        try {
            NavdyDeviceId id = new NavdyDeviceId(lastKnownDeviceId);
            lastKnownDeviceId = null;
            return id;
        } catch (Throwable t) {
            lastKnownDeviceId = null;
            sLogger.e(t);
            return null;
        }
    }

    public static boolean isCoordinateEqual(GeoCoordinate g1, GeoCoordinate g2) {
        double lat1 = g1.getLatitude();
        double lng1 = g1.getLongitude();
        double lat2 = g2.getLatitude();
        double lng2 = g2.getLongitude();
        if (lat1 == lat2 && lng1 == lng2) {
            return true;
        }
        return false;
    }

    public static String concatText(String number, String name, boolean addParenthesis, boolean isHighway) {
        String stringBuilder;
        synchronized (stringBuilder) {
            stringBuilder.setLength(0);
            boolean hasNumber = false;
            if (TextUtils.isEmpty(number)) {
                isHighway = false;
            } else {
                stringBuilder.append(number);
                hasNumber = true;
            }
            if (!(TextUtils.isEmpty(name) || isHighway)) {
                if (hasNumber) {
                    stringBuilder.append(" ");
                    if (addParenthesis) {
                        stringBuilder.append(HereManeuverDisplayBuilder.OPEN_BRACKET);
                    }
                } else {
                    addParenthesis = false;
                }
                stringBuilder.append(name);
                if (addParenthesis) {
                    stringBuilder.append(HereManeuverDisplayBuilder.CLOSE_BRACKET);
                }
            }
            stringBuilder = stringBuilder.toString();
        }
        return stringBuilder;
    }

    public static boolean hasSameSignpostAndToRoad(Maneuver first, Maneuver second) {
        if (first == null || second == null) {
            return false;
        }
        String toRoad1 = concatText(first.getNextRoadNumber(), first.getNextRoadName(), true, false);
        String toRoad2 = concatText(second.getNextRoadNumber(), second.getNextRoadName(), true, false);
        if (TextUtils.isEmpty(toRoad1) || TextUtils.isEmpty(toRoad2) || !TextUtils.equals(toRoad1, toRoad2)) {
            return false;
        }
        String signPostText1 = getSignpostText(first.getSignpost(), first, null, null);
        String signPostText2 = getSignpostText(second.getSignpost(), second, null, null);
        if (TextUtils.isEmpty(signPostText1) || TextUtils.isEmpty(signPostText2) || !TextUtils.equals(signPostText1, signPostText2)) {
            return false;
        }
        return true;
    }

    public static String getSignpostText(Signpost signpost, Maneuver maneuver, SignPostInfo info, ArrayList<String> parsedSignpost) {
        if (signpost == null) {
            return null;
        }
        String stringBuilder;
        synchronized (signPostBuilder) {
            signPostBuilder.setLength(0);
            signPostBuilder2.setLength(0);
            signPostSet.clear();
            String exitNumber = signpost.getExitNumber();
            String exitText = signpost.getExitText();
            boolean hasExitNumber = false;
            boolean leaveHighway = false;
            if (!TextUtils.isEmpty(exitNumber)) {
                if (maneuver.getAction() != Action.LEAVE_HIGHWAY) {
                    signPostBuilder.append(HereManeuverDisplayBuilder.EXIT_NUMBER);
                    signPostBuilder.append(" ");
                    hasExitNumber = false;
                    leaveHighway = true;
                } else {
                    hasExitNumber = true;
                }
                signPostBuilder.append(exitNumber);
                if (parsedSignpost != null) {
                    parsedSignpost.add(exitNumber);
                }
            }
            if (!TextUtils.isEmpty(exitText)) {
                if (needSeparator(signPostBuilder)) {
                    signPostBuilder.append(" ");
                }
                signPostBuilder.append(exitText);
                if (parsedSignpost != null) {
                    parsedSignpost.add(exitText);
                }
            }
            if (hasExitNumber) {
                signPostBuilder.append(" ");
            }
            for (LocalizedLabel label : signpost.getExitDirections()) {
                if (label.getLanguage().equalsIgnoreCase(TBT_ISO3_LANG_CODE)) {
                    String routeName = label.getRouteName();
                    String direction = label.getRouteDirection();
                    String text = label.getText();
                    if (!TextUtils.isEmpty(routeName)) {
                        if (info != null) {
                            info.hasNumberInSignPost = true;
                        }
                        if (leaveHighway) {
                            leaveHighway = false;
                            if (needSeparator(signPostBuilder)) {
                                signPostBuilder.append(HereManeuverDisplayBuilder.SLASH);
                            }
                        }
                        if (needSeparator(signPostBuilder)) {
                            signPostBuilder.append(HereManeuverDisplayBuilder.SLASH);
                        }
                        signPostBuilder.append(routeName);
                    }
                    if (!TextUtils.isEmpty(direction)) {
                        if (info != null && info.hasNumberInSignPost) {
                            signPostBuilder.append(" ");
                        }
                        if (!(parsedSignpost == null || TextUtils.isEmpty(routeName))) {
                            parsedSignpost.add(routeName + " " + direction);
                        }
                        signPostBuilder.append(direction);
                    }
                    if (!(TextUtils.isEmpty(text) || signPostSet.contains(text))) {
                        if (info != null) {
                            info.hasNameInSignPost = true;
                        }
                        if (needSeparator(signPostBuilder2)) {
                            signPostBuilder2.append(HereManeuverDisplayBuilder.SLASH);
                        }
                        signPostBuilder2.append(text);
                        signPostSet.add(text);
                    }
                }
            }
            if (signPostBuilder2.length() > 0) {
                if (needSeparator(signPostBuilder)) {
                    signPostBuilder.append(HereManeuverDisplayBuilder.SLASH);
                }
                signPostBuilder.append(signPostBuilder2.toString());
            }
            stringBuilder = signPostBuilder.toString();
        }
        return stringBuilder;
    }

    public static boolean needSeparator(StringBuilder builder) {
        if (builder == null) {
            return false;
        }
        int len = builder.length();
        if (len == 0) {
            return false;
        }
        char ch = builder.charAt(len - 1);
        if (ch == HereManeuverDisplayBuilder.SLASH_CHAR || ch == HereManeuverDisplayBuilder.SPACE_CHAR) {
            return false;
        }
        return true;
    }

    public static boolean isHighwayAction(Maneuver maneuver) {
        if (maneuver == null) {
            return false;
        }
        Action action = maneuver.getAction();
        if (action == null) {
            return false;
        }
        switch (action) {
            case ENTER_HIGHWAY:
            case ENTER_HIGHWAY_FROM_LEFT:
            case ENTER_HIGHWAY_FROM_RIGHT:
                return true;
            default:
                return false;
        }
    }

    public static String getAllRoadNames(List<Maneuver> maneuvers, int startOffset) {
        if (maneuvers == null || startOffset < 0) {
            return "";
        }
        int len = maneuvers.size();
        if (startOffset >= len) {
            return "";
        }
        StringBuilder builder = new StringBuilder();
        for (int i = startOffset; i < len; i++) {
            builder.append("[");
            builder.append(getRoadName((Maneuver) maneuvers.get(i)));
            builder.append("]");
            builder.append(" ");
        }
        return builder.toString();
    }

    public static long getManeuverDriveTime(Maneuver maneuver, LongContainer distance) {
        if (maneuver == null) {
            return 0;
        }
        List<RoadElement> roadElements = maneuver.getRoadElements();
        if (roadElements == null || roadElements.size() == 0) {
            return 0;
        }
        long time = 0;
        if (distance != null) {
            distance.val = 0;
        }
        for (RoadElement roadElement : roadElements) {
            if (roadElement != null && roadElement.getDefaultSpeed() > 0.0f) {
                double length = roadElement.getGeometryLength();
                time += Math.round(length / ((double) roadElement.getDefaultSpeed()));
                if (distance != null) {
                    distance.val = (long) (((double) distance.val) + length);
                }
            }
        }
        return time;
    }

    public static boolean isInTunnel(RoadElement roadElement) {
        if (roadElement != null) {
            EnumSet<Attribute> attributes = roadElement.getAttributes();
            if (attributes != null) {
                return attributes.contains(Attribute.TUNNEL);
            }
        }
        return false;
    }

    public static double roundToN(double val, int N) {
        return ((double) ((long) (((double) N) * val))) / ((double) N);
    }

    public static int findManeuverIndex(List<Maneuver> list, Maneuver maneuver) {
        if (list == null || maneuver == null) {
            return -1;
        }
        int len = list.size();
        for (int i = 0; i < len; i++) {
            if (areManeuverEqual((Maneuver) list.get(i), maneuver)) {
                return i;
            }
        }
        return -1;
    }

    public static List<RouteElement> getAheadRouteElements(Route route, RoadElement currentRoadElement, Maneuver prev, Maneuver next) {
        if (route == null || currentRoadElement == null || next == null) {
            sLogger.i("getAheadRouteElements:invalid arg");
            return null;
        }
        List<Maneuver> maneuvers = route.getManeuvers();
        int index = findManeuverIndex(maneuvers, next);
        if (index == -1) {
            sLogger.i("getAheadRouteElements:maneuver index not found");
            return null;
        }
        int len;
        int i;
        sLogger.i("getAheadRouteElements:found maneuver index:" + index + " total:" + maneuvers.size());
        List<RouteElement> aheadRouteElements = new ArrayList();
        if (prev != null) {
            List<RouteElement> routeElements = prev.getRouteElements();
            if (routeElements != null && routeElements.size() > 0) {
                len = routeElements.size();
                boolean found = false;
                for (i = 0; i < len; i++) {
                    RouteElement routeElement = (RouteElement) routeElements.get(i);
                    if (found) {
                        aheadRouteElements.add(routeElement);
                    } else {
                        RoadElement re = routeElement.getRoadElement();
                        if (re != null && re.equals(currentRoadElement)) {
                            found = true;
                        }
                    }
                }
            }
        }
        len = maneuvers.size();
        for (i = index; i < len; i++) {
            aheadRouteElements.addAll(((Maneuver) maneuvers.get(i)).getRouteElements());
        }
        return aheadRouteElements;
    }

    public static Maneuver getLastManeuver(List<Maneuver> list) {
        if (list == null || list.size() <= 1) {
            return null;
        }
        return (Maneuver) list.get(list.size() - 2);
    }

    public static long getDistanceToRoadElement(RoadElement from, RoadElement to, List<Maneuver> maneuvers) {
        if (from == null || to == null || maneuvers == null) {
            return -1;
        }
        long distance = -1;
        boolean fromFound = false;
        int maneuverLen = maneuvers.size();
        for (int i = 0; i < maneuverLen; i++) {
            List<RoadElement> roadElements = ((Maneuver) maneuvers.get(i)).getRoadElements();
            int elementLen = roadElements.size();
            for (int j = 0; j < elementLen; j++) {
                RoadElement re = (RoadElement) roadElements.get(j);
                if (fromFound) {
                    if (re.equals(to)) {
                        return distance;
                    }
                    distance += (long) re.getGeometryLength();
                } else if (re.equals(from)) {
                    fromFound = true;
                    distance = (long) re.getGeometryLength();
                }
            }
        }
        return -1;
    }

    public static void loadImage(final Image image, final int width, final int height, final IImageLoadCallback callback) {
        if (image == null || callback == null || width <= 0 || height <= 0) {
            throw new IllegalArgumentException();
        }
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                try {
                    callback.result(image, image.getBitmap(width, height));
                } catch (Throwable t) {
                    HereMapUtil.sLogger.e(t);
                    callback.result(image, null);
                }
            }
        }, 1);
    }

    public static boolean isSavedRoute(NavigationRouteRequest request) {
        if (request == null || request.routeAttributes == null || !request.routeAttributes.contains(RouteAttribute.ROUTE_ATTRIBUTE_SAVED_ROUTE)) {
            return false;
        }
        return true;
    }

    public static boolean isGeoCoordinateEquals(GeoCoordinate g1, GeoCoordinate g2) {
        if (g1.getLatitude() == g2.getLatitude() && g1.getLongitude() == g2.getLongitude()) {
            return true;
        }
        return false;
    }

    static float roundToIntegerStep(int minStep, float value) {
        return (float) (minStep * Math.round(value / ((float) minStep)));
    }

    static boolean isRouteFileStale(File f) {
        long l = System.currentTimeMillis() - f.lastModified();
        if (l >= STALE_ROUTE_TIME || l < 0) {
            return true;
        }
        return false;
    }

    public static void generateRouteIcons(String id, String label, Route route) {
        GenericUtil.checkNotOnMainThread();
        try {
            Context context = HudApplication.getAppContext();
            String path = PathManager.getInstance().getMapsPartitionPath() + File.separator + "routeicons" + File.separator + id + MusicDataUtils.ALTERNATE_SEPARATOR + GenericUtil.normalizeToFilename(label);
            sLogger.v("generateRouteIcons: " + path);
            File f = new File(path);
            if (f.exists()) {
                IOUtils.deleteDirectory(context, f);
            }
            f.mkdirs();
            int iconCount = 0;
            for (Maneuver maneuver : route.getManeuvers()) {
                Image icon = maneuver.getNextRoadImage();
                if (icon != null) {
                    int w = (int) icon.getWidth();
                    int h = (int) icon.getHeight();
                    sLogger.v("generateRouteIcons: w=" + w + " h=" + h);
                    Bitmap bitmap = icon.getBitmap(w * 3, h * 3);
                    if (bitmap != null) {
                        String filename = "man_" + GenericUtil.normalizeToFilename(getNextRoadName(maneuver)) + MusicDataUtils.ALTERNATE_SEPARATOR + w + MusicDataUtils.ALTERNATE_SEPARATOR + h + ".png";
                        ByteArrayOutputStream bout = new ByteArrayOutputStream();
                        bitmap.compress(CompressFormat.PNG, 100, bout);
                        IOUtils.copyFile(path + File.separator + filename, bout.toByteArray());
                        iconCount++;
                        sLogger.v("icon generated");
                    }
                }
            }
            sLogger.v("total icons generated:" + iconCount);
        } catch (Throwable t) {
            sLogger.e(t);
        }
    }

    public static SpannableStringBuilder getEtaString(Route route, String routeId, boolean addSpaceBetweenUnits) {
        SpannableStringBuilder tripDuration = new SpannableStringBuilder();
        if (route != null) {
            Date ttaDate = getRouteTtaDate(route);
            long duration = 0;
            if (ttaDate != null) {
                duration = ttaDate.getTime() - System.currentTimeMillis();
            } else {
                sLogger.i("tta date is null for route:" + routeId);
            }
            if (duration > 0) {
                long minutes = TimeUnit.MILLISECONDS.toMinutes(duration);
                if (minutes < 60) {
                    formatString(tripDuration, String.valueOf(minutes), sp_26_1, min, sp_20_b_1, null, null, null, null, true);
                } else {
                    long hours = TimeUnit.MILLISECONDS.toHours(duration);
                    minutes = TimeUnit.MILLISECONDS.toMinutes(duration - TimeUnit.HOURS.toMillis(hours));
                    if (hours < 10) {
                        formatString(tripDuration, String.valueOf(hours), sp_26_1, hr, sp_20_b_1, String.valueOf(minutes), sp_26_2, min_short, sp_20_b_2, addSpaceBetweenUnits);
                    } else if (hours < 24) {
                        formatString(tripDuration, String.valueOf(hours), sp_24_1, hr, sp_20_b_1, String.valueOf(minutes), sp_24_2, min_short, sp_20_b_2, addSpaceBetweenUnits);
                    } else {
                        long days = TimeUnit.MILLISECONDS.toDays(duration);
                        hours = TimeUnit.MILLISECONDS.toHours(duration - TimeUnit.DAYS.toMillis(days));
                        if (days == 1) {
                            formatString(tripDuration, String.valueOf(days), sp_26_1, day, sp_20_b_1, String.valueOf(hours), sp_26_2, hr, sp_20_b_2, addSpaceBetweenUnits);
                        } else {
                            formatString(tripDuration, String.valueOf(days), sp_24_1, day, sp_20_b_1, String.valueOf(hours), sp_24_2, hr, sp_20_b_2, addSpaceBetweenUnits);
                        }
                    }
                }
            }
        }
        return tripDuration;
    }

    private static void formatString(SpannableStringBuilder builder, String s1, TextAppearanceSpan s1Span, String s2, TextAppearanceSpan s2Span, String s3, TextAppearanceSpan s3Span, String s4, TextAppearanceSpan s4Span, boolean addSpaceBetweenUnits) {
        int offset;
        if (s1 != null) {
            builder.append(s1);
            builder.setSpan(s1Span, 0, builder.length(), 33);
        }
        if (s2 != null) {
            if (addSpaceBetweenUnits) {
                builder.append(" ");
            }
            offset = builder.length();
            builder.append(s2);
            builder.setSpan(s2Span, offset, builder.length(), 33);
        }
        if (s3 != null) {
            offset = builder.length();
            builder.append(" ");
            builder.append(s3);
            builder.setSpan(s3Span, offset, builder.length(), 33);
        }
        if (s4 != null) {
            if (addSpaceBetweenUnits) {
                builder.append(" ");
            }
            offset = builder.length();
            builder.append(s4);
            builder.setSpan(s4Span, offset, builder.length(), 33);
        }
    }

    public static String getCurrentEtaStringWithDestination() {
        try {
            if (!HereMapsManager.getInstance().isInitialized()) {
                return null;
            }
            HereNavigationManager hereNavigationManager = HereNavigationManager.getInstance();
            if (!hereNavigationManager.isNavigationModeOn()) {
                return null;
            }
            String label = hereNavigationManager.getDestinationLabel();
            if (TextUtils.isEmpty(label)) {
                label = hereNavigationManager.getDestinationStreetAddress();
                if (TextUtils.isEmpty(label)) {
                    label = null;
                }
            }
            Resources resources = HudApplication.getAppContext().getResources();
            if (!HereNavigationManager.getInstance().hasArrived()) {
                String eta = getCurrentEtaString();
                if (eta == null) {
                    return null;
                }
                if (label == null) {
                    return eta;
                }
                return HudApplication.getAppContext().getString(R.string.mm_active_trip_eta, new Object[]{eta, label});
            } else if (label == null) {
                return resources.getString(R.string.mm_active_trip_arrived_no_label);
            } else {
                return resources.getString(R.string.mm_active_trip_arrived, new Object[]{label});
            }
        } catch (Throwable t) {
            sLogger.e(t);
            return null;
        }
    }

    public static String getCurrentEtaString() {
        try {
            if (!HereMapsManager.getInstance().isInitialized() || !HereNavigationManager.getInstance().isNavigationModeOn()) {
                return null;
            }
            HomeScreenView view = RemoteDeviceManager.getInstance().getUiStateManager().getHomescreenView();
            if (view != null) {
                return view.getEtaView().getLastEtaDate();
            }
            return null;
        } catch (Throwable t) {
            sLogger.e(t);
            return null;
        }
    }

    public static String convertDateToEta(Date etaDate) {
        if (!isValidEtaDate(etaDate)) {
            return null;
        }
        long duration = etaDate.getTime() - System.currentTimeMillis();
        if (duration <= 0) {
            return null;
        }
        long minutes = TimeUnit.MILLISECONDS.toMinutes(duration);
        if (minutes < 60) {
            return String.valueOf(minutes) + " " + min;
        }
        long hours = TimeUnit.MILLISECONDS.toHours(duration);
        minutes = TimeUnit.MILLISECONDS.toMinutes(duration - TimeUnit.HOURS.toMillis(hours));
        if (hours < 24) {
            return String.valueOf(hours) + " " + hr + " " + String.valueOf(minutes) + " " + min_short;
        }
        long days = TimeUnit.MILLISECONDS.toDays(duration);
        return String.valueOf(days) + " " + day + " " + String.valueOf(TimeUnit.MILLISECONDS.toHours(duration - TimeUnit.DAYS.toMillis(days))) + " " + hr;
    }
}
