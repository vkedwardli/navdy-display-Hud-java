package com.navdy.hud.app.maps.here;

import android.graphics.PointF;
import android.os.Handler;
import android.os.HandlerThread;
import com.here.android.mpa.common.GeoBoundingBox;
import com.here.android.mpa.common.GeoCoordinate;
import com.here.android.mpa.common.ViewRect;
import com.here.android.mpa.mapping.Map;
import com.here.android.mpa.mapping.Map.Animation;
import com.here.android.mpa.mapping.Map.OnTransformListener;
import com.here.android.mpa.mapping.Map.PixelResult;
import com.here.android.mpa.mapping.MapObject;
import com.here.android.mpa.mapping.MapRoute;
import com.here.android.mpa.mapping.PositionIndicator;
import com.here.android.mpa.routing.Route;
import com.navdy.hud.app.util.GenericUtil;
import com.navdy.service.library.log.Logger;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class HereMapController {
    private static final AtomicInteger counter = new AtomicInteger(1);
    private static final Logger sLogger = new Logger(HereMapController.class);
    private final Map map;
    private final Handler mapBkHandler;
    private int mapRouteCount;
    private volatile State state;
    private PointF transformCenter;

    public interface Callback {
        void finish();
    }

    public enum State {
        NONE,
        TRANSITION,
        ROUTE_PICKER,
        OVERVIEW,
        AR_MODE
    }

    public HereMapController(Map map, State state) {
        GenericUtil.checkNotOnMainThread();
        this.map = map;
        this.state = state;
        HandlerThread handlerThread = new HandlerThread("HereMapController-" + counter.getAndIncrement());
        handlerThread.start();
        this.mapBkHandler = new Handler(handlerThread.getLooper());
    }

    public Map getMap() {
        return this.map;
    }

    public State getState() {
        return this.state;
    }

    public void setState(State state) {
        this.state = state;
        sLogger.v("state:" + state);
    }

    public GeoCoordinate getCenter() {
        GenericUtil.checkNotOnMainThread();
        return this.map.getCenter();
    }

    public double getZoomLevel() {
        GenericUtil.checkNotOnMainThread();
        return this.map.getZoomLevel();
    }

    public float getTilt() {
        GenericUtil.checkNotOnMainThread();
        return this.map.getTilt();
    }

    public void setTilt(final float tilt) {
        this.mapBkHandler.post(new Runnable() {
            public void run() {
                HereMapController.this.map.setTilt(tilt);
            }
        });
    }

    public float getOrientation() {
        GenericUtil.checkNotOnMainThread();
        return this.map.getOrientation();
    }

    public void setZoomLevel(final double zoomLevel) {
        this.mapBkHandler.post(new Runnable() {
            public void run() {
                try {
                    HereMapController.this.map.setZoomLevel(zoomLevel);
                } catch (Throwable t) {
                    HereMapController.sLogger.e(t);
                }
            }
        });
    }

    public void setCenter(GeoCoordinate center, Animation animation, double zoom, float orientation, float tilt) {
        if (this.state == State.AR_MODE) {
            final GeoCoordinate geoCoordinate = center;
            final Animation animation2 = animation;
            final double d = zoom;
            final float f = orientation;
            final float f2 = tilt;
            this.mapBkHandler.post(new Runnable() {
                public void run() {
                    HereMapController.this.map.setCenter(geoCoordinate, animation2, d, f, f2);
                }
            });
        }
    }

    public void setCenterForState(State state, GeoCoordinate center, Animation animation, double zoom, float orientation, float tilt) {
        if (this.state == state) {
            final GeoCoordinate geoCoordinate = center;
            final Animation animation2 = animation;
            final double d = zoom;
            final float f = orientation;
            final float f2 = tilt;
            this.mapBkHandler.post(new Runnable() {
                public void run() {
                    HereMapController.this.map.setCenter(geoCoordinate, animation2, d, f, f2);
                }
            });
        }
    }

    public void addMapObject(final MapObject mapObject) {
        this.mapBkHandler.post(new Runnable() {
            public void run() {
                HereMapController.this.addMapObjectInternal(mapObject);
            }
        });
    }

    private void addMapObjectInternal(MapObject mapObject) {
        if (mapObject instanceof MapRoute) {
            MapRoute mapRoute = (MapRoute) mapObject;
            Route route = mapRoute.getRoute();
            this.mapRouteCount++;
            sLogger.v("[map-route-added] maproute=" + System.identityHashCode(mapRoute) + " route=" + System.identityHashCode(route) + " count=" + this.mapRouteCount);
        }
        this.map.addMapObject(mapObject);
    }

    public void addMapObjects(final List<MapObject> mapObjects) {
        this.mapBkHandler.post(new Runnable() {
            public void run() {
                for (MapObject mapObject : mapObjects) {
                    HereMapController.this.addMapObjectInternal(mapObject);
                }
            }
        });
    }

    public void removeMapObject(final MapObject mapObject) {
        this.mapBkHandler.post(new Runnable() {
            public void run() {
                HereMapController.this.removeMapObjectInternal(mapObject);
            }
        });
    }

    public void removeMapObjects(final List<MapObject> list) {
        this.mapBkHandler.post(new Runnable() {
            public void run() {
                for (MapObject mapObject : list) {
                    HereMapController.this.removeMapObjectInternal(mapObject);
                }
            }
        });
    }

    private void removeMapObjectInternal(MapObject mapObject) {
        if (mapObject instanceof MapRoute) {
            this.mapRouteCount--;
            MapRoute mapRoute = (MapRoute) mapObject;
            sLogger.v("[map-route-removed] maproute=" + System.identityHashCode(mapRoute) + " route=" + System.identityHashCode(mapRoute.getRoute()) + " count=" + this.mapRouteCount);
        }
        if (mapObject != null) {
            this.map.removeMapObject(mapObject);
        }
    }

    public void setTransformCenter(final PointF pointF) {
        this.transformCenter = pointF;
        this.mapBkHandler.post(new Runnable() {
            public void run() {
                HereMapController.this.map.setTransformCenter(pointF);
            }
        });
    }

    public PixelResult projectToPixel(GeoCoordinate latlng) {
        GenericUtil.checkNotOnMainThread();
        return this.map.projectToPixel(latlng);
    }

    public void addTransformListener(final OnTransformListener listener) {
        this.mapBkHandler.post(new Runnable() {
            public void run() {
                HereMapController.this.map.addTransformListener(listener);
            }
        });
    }

    public void removeTransformListener(final OnTransformListener listener) {
        this.mapBkHandler.post(new Runnable() {
            public void run() {
                HereMapController.this.map.removeTransformListener(listener);
            }
        });
    }

    public void zoomTo(GeoBoundingBox boundingBox, ViewRect viewRect, Animation animation, float orientation) {
        final GeoBoundingBox geoBoundingBox = boundingBox;
        final ViewRect viewRect2 = viewRect;
        final Animation animation2 = animation;
        final float f = orientation;
        this.mapBkHandler.post(new Runnable() {
            public void run() {
                try {
                    HereMapController.this.map.zoomTo(geoBoundingBox, viewRect2, animation2, f);
                } catch (Throwable t) {
                    HereMapController.sLogger.e(t);
                }
            }
        });
    }

    public PositionIndicator getPositionIndicator() {
        GenericUtil.checkNotOnMainThread();
        return this.map.getPositionIndicator();
    }

    public String getMapScheme() {
        return this.map.getMapScheme();
    }

    public void setMapScheme(final String mapScheme) {
        this.mapBkHandler.post(new Runnable() {
            public void run() {
                HereMapController.this.map.setMapScheme(mapScheme);
            }
        });
    }

    public void execute(Runnable runnable) {
        this.mapBkHandler.post(runnable);
    }

    public void execute(final Runnable runnable, final Callback cb) {
        this.mapBkHandler.post(new Runnable() {
            public void run() {
                runnable.run();
                if (cb != null) {
                    cb.finish();
                }
            }
        });
    }

    public void setTrafficInfoVisible(final boolean b) {
        this.mapBkHandler.post(new Runnable() {
            public void run() {
                HereMapController.this.map.setTrafficInfoVisible(b);
            }
        });
    }
}
