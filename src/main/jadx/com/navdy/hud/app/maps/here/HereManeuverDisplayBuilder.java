package com.navdy.hud.app.maps.here;

import android.content.res.Resources;
import android.text.TextUtils;
import com.here.android.mpa.common.GeoCoordinate;
import com.here.android.mpa.common.RoadElement;
import com.here.android.mpa.routing.Maneuver;
import com.here.android.mpa.routing.Maneuver.Action;
import com.here.android.mpa.routing.Maneuver.Icon;
import com.here.android.mpa.routing.Maneuver.Turn;
import com.here.android.mpa.routing.Route;
import com.here.android.mpa.routing.Signpost;
import com.here.posclient.UpdateOptions;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.R;
import com.navdy.hud.app.manager.SpeedManager;
import com.navdy.hud.app.manager.SpeedManager.SpeedUnit;
import com.navdy.hud.app.maps.MapEvents.DestinationDirection;
import com.navdy.hud.app.maps.MapEvents.ManeuverDisplay;
import com.navdy.hud.app.maps.MapSettings;
import com.navdy.hud.app.maps.here.HereMapUtil.SignPostInfo;
import com.navdy.hud.app.maps.util.DistanceConverter;
import com.navdy.hud.app.maps.util.DistanceConverter.Distance;
import com.navdy.service.library.events.location.Coordinate;
import com.navdy.service.library.events.navigation.DistanceUnit;
import com.navdy.service.library.events.navigation.NavigationRouteRequest;
import com.navdy.service.library.events.navigation.NavigationTurn;
import com.navdy.service.library.log.Logger;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

public class HereManeuverDisplayBuilder {
    public static final String ARRIVED = resources.getString(R.string.arrived);
    public static final String CLOSE_BRACKET = ")";
    public static final boolean COMBINE_HIGHWAY_MANEUVER = true;
    public static final String COMMA = ",";
    static final String DESTINATION_LEFT = resources.getString(R.string.left);
    private static final int DESTINATION_POINT_A_MIN_DISTANCE = 30;
    static final String DESTINATION_RIGHT = resources.getString(R.string.right);
    private static final Distance DISTANCE = new Distance();
    public static final String EMPTY = "";
    public static final ManeuverDisplay EMPTY_MANEUVER_DISPLAY = new ManeuverDisplay();
    private static final String END_TURN = resources.getString(R.string.end_maneuver_turn);
    private static final String ENTER_HIGHWAY = resources.getString(R.string.enter_highway);
    private static final String EXIT = resources.getString(R.string.exit);
    public static final String EXIT_NUMBER = resources.getString(R.string.exit_number);
    private static final String FEET_METER_FORMAT = "%.0f %s";
    public static final float FT_IN_METER = 3.28084f;
    private static final String HYPHEN = resources.getString(R.string.hyphen);
    public static final float KM_DISPLAY_THRESHOLD = 400.0f;
    private static final String KM_FORMAT = "%.1f %s";
    private static final String KM_FORMAT_SHORT = "%.0f %s";
    public static final float METERS_IN_KM = 1000.0f;
    public static final float METERS_IN_MI = 1609.34f;
    public static final float MILES_DISPLAY_THRESHOLD = 160.934f;
    private static final String MILES_FORMAT = "%.1f %s";
    private static final String MILES_FORMAT_SHORT = "%.0f %s";
    private static final int MIN_STEP_FEET = 10;
    private static final int MIN_STEP_METERS = 5;
    private static final long NEXT_DISTANCE_THRESHOLD = 6437;
    private static final long NEXT_DISTANCE_THRESHOLD_HIGHWAY = 16093;
    private static final long NOW_DISTANCE_THRESHOLD = 30;
    private static final long NOW_DISTANCE_THRESHOLD_HIGHWAY = 100;
    private static final String ONTO = resources.getString(R.string.onto);
    public static final String OPEN_BRACKET = "(";
    public static final int SHORT_DISTANCE_DISPLAY_THRESHOLD = 10;
    public static final boolean SHOW_DESTINATION_DIRECTION = true;
    public static final String SLASH = "/";
    public static final char SLASH_CHAR = '/';
    private static final float SMALL_UNITS_THRESHOLD = 0.1f;
    public static final boolean SMART_MANEUVER_CALCULATION = true;
    private static final long SOON_DISTANCE_THRESHOLD = 1287;
    private static final long SOON_DISTANCE_THRESHOLD_HIGHWAY = 3218;
    public static final String SPACE = " ";
    public static final char SPACE_CHAR = ' ';
    public static final String START_TURN = resources.getString(R.string.start_maneuver_turn);
    private static final String STAY_ON = resources.getString(R.string.stay_on);
    private static final long THEN_MANEUVER_THRESHOLD = 250;
    private static final long THEN_MANEUVER_THRESHOLD_HIGHWAY = 1000;
    public static final String TOWARDS = resources.getString(R.string.toward);
    private static final String UNIT_FEET = resources.getString(R.string.unit_feet);
    private static final String UNIT_FEET_EXTENDED = resources.getString(R.string.unit_feet_ext);
    private static final String UNIT_FEET_EXTENDED_SINGULAR = resources.getString(R.string.unit_feet_ext_singular);
    private static final String UNIT_KILOMETERS = resources.getString(R.string.unit_kilometers);
    private static final String UNIT_KILOMETERS_EXTENDED = resources.getString(R.string.unit_kilometers_ext);
    private static final String UNIT_KILOMETERS_EXTENDED_SINGULAR = resources.getString(R.string.unit_kilometers_ext_singular);
    private static final String UNIT_METERS = resources.getString(R.string.unit_meters);
    private static final String UNIT_METERS_EXTENDED = resources.getString(R.string.unit_meters_ext);
    private static final String UNIT_METERS_EXTENDED_SINGULAR = resources.getString(R.string.unit_meters_ext_singular);
    private static final String UNIT_MILES = resources.getString(R.string.unit_miles);
    private static final String UNIT_MILES_EXTENDED = resources.getString(R.string.unit_miles_ext);
    private static final String UNIT_MILES_EXTENDED_SINGULAR = resources.getString(R.string.unit_miles_ext_singular);
    private static final String UTURN = resources.getString(R.string.uturn);
    private static final HashSet<String> allowedTurnTextMap = new HashSet();
    private static Maneuver lastStayManeuver;
    private static String lastStayManeuverRoadName;
    private static final StringBuilder mainSignPostBuilder = new StringBuilder();
    private static final Resources resources = HudApplication.getAppContext().getResources();
    private static final HashMap<Icon, NavigationTurn> sHereIconToNavigationIconMap = new HashMap();
    private static final HashMap<Turn, String> sHereTurnToNavigationTurnMap = new HashMap();
    private static final Logger sLogger = new Logger(HereManeuverDisplayBuilder.class);
    private static final Map<NavigationTurn, Integer> sTurnIconImageMap = new HashMap();
    private static final Map<NavigationTurn, Integer> sTurnIconSoonImageMap = new HashMap();
    private static final Map<NavigationTurn, Integer> sTurnIconSoonImageMapGrey = new HashMap();
    private static final SpeedManager speedManager = SpeedManager.getInstance();

    public enum ManeuverState {
        STAY,
        NEXT,
        SOON,
        NOW
    }

    public static class PendingRoadInfo {
        public boolean dontUseSignpostText;
        public boolean enterHighway;
        public boolean hasTo;
        public boolean usePrevManeuverInfo;
    }

    static {
        sHereIconToNavigationIconMap.put(Icon.UNDEFINED, NavigationTurn.NAV_TURN_STRAIGHT);
        sHereIconToNavigationIconMap.put(Icon.GO_STRAIGHT, NavigationTurn.NAV_TURN_STRAIGHT);
        sHereIconToNavigationIconMap.put(Icon.KEEP_MIDDLE, NavigationTurn.NAV_TURN_STRAIGHT);
        sHereIconToNavigationIconMap.put(Icon.KEEP_RIGHT, NavigationTurn.NAV_TURN_KEEP_RIGHT);
        sHereIconToNavigationIconMap.put(Icon.LIGHT_RIGHT, NavigationTurn.NAV_TURN_EASY_RIGHT);
        sHereIconToNavigationIconMap.put(Icon.QUITE_RIGHT, NavigationTurn.NAV_TURN_RIGHT);
        sHereIconToNavigationIconMap.put(Icon.HEAVY_RIGHT, NavigationTurn.NAV_TURN_SHARP_RIGHT);
        sHereIconToNavigationIconMap.put(Icon.KEEP_LEFT, NavigationTurn.NAV_TURN_KEEP_LEFT);
        sHereIconToNavigationIconMap.put(Icon.LIGHT_LEFT, NavigationTurn.NAV_TURN_EASY_LEFT);
        sHereIconToNavigationIconMap.put(Icon.QUITE_LEFT, NavigationTurn.NAV_TURN_LEFT);
        sHereIconToNavigationIconMap.put(Icon.HEAVY_LEFT, NavigationTurn.NAV_TURN_SHARP_LEFT);
        sHereIconToNavigationIconMap.put(Icon.UTURN_RIGHT, NavigationTurn.NAV_TURN_UTURN_RIGHT);
        sHereIconToNavigationIconMap.put(Icon.UTURN_LEFT, NavigationTurn.NAV_TURN_UTURN_LEFT);
        sHereIconToNavigationIconMap.put(Icon.ROUNDABOUT_1, NavigationTurn.NAV_TURN_ROUNDABOUT_S);
        sHereIconToNavigationIconMap.put(Icon.ROUNDABOUT_2, NavigationTurn.NAV_TURN_ROUNDABOUT_S);
        sHereIconToNavigationIconMap.put(Icon.ROUNDABOUT_3, NavigationTurn.NAV_TURN_ROUNDABOUT_S);
        sHereIconToNavigationIconMap.put(Icon.ROUNDABOUT_4, NavigationTurn.NAV_TURN_ROUNDABOUT_S);
        sHereIconToNavigationIconMap.put(Icon.ROUNDABOUT_5, NavigationTurn.NAV_TURN_ROUNDABOUT_S);
        sHereIconToNavigationIconMap.put(Icon.ROUNDABOUT_6, NavigationTurn.NAV_TURN_ROUNDABOUT_S);
        sHereIconToNavigationIconMap.put(Icon.ROUNDABOUT_7, NavigationTurn.NAV_TURN_ROUNDABOUT_S);
        sHereIconToNavigationIconMap.put(Icon.ROUNDABOUT_8, NavigationTurn.NAV_TURN_ROUNDABOUT_S);
        sHereIconToNavigationIconMap.put(Icon.ROUNDABOUT_9, NavigationTurn.NAV_TURN_ROUNDABOUT_S);
        sHereIconToNavigationIconMap.put(Icon.ROUNDABOUT_10, NavigationTurn.NAV_TURN_ROUNDABOUT_S);
        sHereIconToNavigationIconMap.put(Icon.ROUNDABOUT_11, NavigationTurn.NAV_TURN_ROUNDABOUT_S);
        sHereIconToNavigationIconMap.put(Icon.ROUNDABOUT_12, NavigationTurn.NAV_TURN_ROUNDABOUT_S);
        sHereIconToNavigationIconMap.put(Icon.ENTER_HIGHWAY_RIGHT_LANE, NavigationTurn.NAV_TURN_MERGE_LEFT);
        sHereIconToNavigationIconMap.put(Icon.ENTER_HIGHWAY_LEFT_LANE, NavigationTurn.NAV_TURN_MERGE_RIGHT);
        sHereIconToNavigationIconMap.put(Icon.LEAVE_HIGHWAY_RIGHT_LANE, NavigationTurn.NAV_TURN_EXIT_RIGHT);
        sHereIconToNavigationIconMap.put(Icon.LEAVE_HIGHWAY_LEFT_LANE, NavigationTurn.NAV_TURN_EXIT_LEFT);
        sHereIconToNavigationIconMap.put(Icon.HIGHWAY_KEEP_RIGHT, NavigationTurn.NAV_TURN_KEEP_RIGHT);
        sHereIconToNavigationIconMap.put(Icon.HIGHWAY_KEEP_LEFT, NavigationTurn.NAV_TURN_KEEP_LEFT);
        sHereIconToNavigationIconMap.put(Icon.ROUNDABOUT_1_LH, NavigationTurn.NAV_TURN_ROUNDABOUT_N);
        sHereIconToNavigationIconMap.put(Icon.ROUNDABOUT_2_LH, NavigationTurn.NAV_TURN_ROUNDABOUT_N);
        sHereIconToNavigationIconMap.put(Icon.ROUNDABOUT_3_LH, NavigationTurn.NAV_TURN_ROUNDABOUT_N);
        sHereIconToNavigationIconMap.put(Icon.ROUNDABOUT_4_LH, NavigationTurn.NAV_TURN_ROUNDABOUT_N);
        sHereIconToNavigationIconMap.put(Icon.ROUNDABOUT_5_LH, NavigationTurn.NAV_TURN_ROUNDABOUT_N);
        sHereIconToNavigationIconMap.put(Icon.ROUNDABOUT_6_LH, NavigationTurn.NAV_TURN_ROUNDABOUT_N);
        sHereIconToNavigationIconMap.put(Icon.ROUNDABOUT_7_LH, NavigationTurn.NAV_TURN_ROUNDABOUT_N);
        sHereIconToNavigationIconMap.put(Icon.ROUNDABOUT_8_LH, NavigationTurn.NAV_TURN_ROUNDABOUT_N);
        sHereIconToNavigationIconMap.put(Icon.ROUNDABOUT_9_LH, NavigationTurn.NAV_TURN_ROUNDABOUT_N);
        sHereIconToNavigationIconMap.put(Icon.ROUNDABOUT_10_LH, NavigationTurn.NAV_TURN_ROUNDABOUT_N);
        sHereIconToNavigationIconMap.put(Icon.ROUNDABOUT_11_LH, NavigationTurn.NAV_TURN_ROUNDABOUT_N);
        sHereIconToNavigationIconMap.put(Icon.ROUNDABOUT_12_LH, NavigationTurn.NAV_TURN_ROUNDABOUT_N);
        sHereIconToNavigationIconMap.put(Icon.START, NavigationTurn.NAV_TURN_START);
        sHereIconToNavigationIconMap.put(Icon.END, NavigationTurn.NAV_TURN_END);
        sHereIconToNavigationIconMap.put(Icon.FERRY, NavigationTurn.NAV_TURN_FERRY);
        sHereIconToNavigationIconMap.put(Icon.HEAD_TO, NavigationTurn.NAV_TURN_STRAIGHT);
        sHereTurnToNavigationTurnMap.put(Turn.UNDEFINED, resources.getString(R.string.undefined_turn));
        sHereTurnToNavigationTurnMap.put(Turn.NO_TURN, resources.getString(R.string.no_turn));
        sHereTurnToNavigationTurnMap.put(Turn.KEEP_MIDDLE, resources.getString(R.string.keep_middle));
        sHereTurnToNavigationTurnMap.put(Turn.KEEP_RIGHT, resources.getString(R.string.keep_right));
        sHereTurnToNavigationTurnMap.put(Turn.LIGHT_RIGHT, resources.getString(R.string.slight_right));
        sHereTurnToNavigationTurnMap.put(Turn.QUITE_RIGHT, resources.getString(R.string.quite_right));
        sHereTurnToNavigationTurnMap.put(Turn.HEAVY_RIGHT, resources.getString(R.string.quite_right));
        sHereTurnToNavigationTurnMap.put(Turn.KEEP_LEFT, resources.getString(R.string.keep_left));
        sHereTurnToNavigationTurnMap.put(Turn.LIGHT_LEFT, resources.getString(R.string.slight_left));
        sHereTurnToNavigationTurnMap.put(Turn.QUITE_LEFT, resources.getString(R.string.quite_left));
        sHereTurnToNavigationTurnMap.put(Turn.HEAVY_LEFT, resources.getString(R.string.quite_left));
        sHereTurnToNavigationTurnMap.put(Turn.RETURN, UTURN);
        sHereTurnToNavigationTurnMap.put(Turn.ROUNDABOUT_1, resources.getString(R.string.roundabout_1));
        sHereTurnToNavigationTurnMap.put(Turn.ROUNDABOUT_2, resources.getString(R.string.roundabout_2));
        sHereTurnToNavigationTurnMap.put(Turn.ROUNDABOUT_3, resources.getString(R.string.roundabout_3));
        sHereTurnToNavigationTurnMap.put(Turn.ROUNDABOUT_4, resources.getString(R.string.roundabout_4));
        sHereTurnToNavigationTurnMap.put(Turn.ROUNDABOUT_5, resources.getString(R.string.roundabout_5));
        sHereTurnToNavigationTurnMap.put(Turn.ROUNDABOUT_6, resources.getString(R.string.roundabout_6));
        sHereTurnToNavigationTurnMap.put(Turn.ROUNDABOUT_7, resources.getString(R.string.roundabout_7));
        sHereTurnToNavigationTurnMap.put(Turn.ROUNDABOUT_8, resources.getString(R.string.roundabout_8));
        sHereTurnToNavigationTurnMap.put(Turn.ROUNDABOUT_9, resources.getString(R.string.roundabout_9));
        sHereTurnToNavigationTurnMap.put(Turn.ROUNDABOUT_10, resources.getString(R.string.roundabout_10));
        sHereTurnToNavigationTurnMap.put(Turn.ROUNDABOUT_11, resources.getString(R.string.roundabout_11));
        sHereTurnToNavigationTurnMap.put(Turn.ROUNDABOUT_12, resources.getString(R.string.roundabout_12));
        sTurnIconImageMap.put(NavigationTurn.NAV_TURN_START, Integer.valueOf(R.drawable.tbt_straight));
        sTurnIconImageMap.put(NavigationTurn.NAV_TURN_LEFT, Integer.valueOf(R.drawable.tbt_turn_left));
        sTurnIconImageMap.put(NavigationTurn.NAV_TURN_RIGHT, Integer.valueOf(R.drawable.tbt_turn_right));
        sTurnIconImageMap.put(NavigationTurn.NAV_TURN_EASY_RIGHT, Integer.valueOf(R.drawable.tbt_easy_right));
        sTurnIconImageMap.put(NavigationTurn.NAV_TURN_EASY_LEFT, Integer.valueOf(R.drawable.tbt_easy_left));
        sTurnIconImageMap.put(NavigationTurn.NAV_TURN_SHARP_LEFT, Integer.valueOf(R.drawable.tbt_sharp_left));
        sTurnIconImageMap.put(NavigationTurn.NAV_TURN_SHARP_RIGHT, Integer.valueOf(R.drawable.tbt_sharp_right));
        sTurnIconImageMap.put(NavigationTurn.NAV_TURN_EXIT_RIGHT, Integer.valueOf(R.drawable.tbt_exit_right));
        sTurnIconImageMap.put(NavigationTurn.NAV_TURN_EXIT_LEFT, Integer.valueOf(R.drawable.tbt_exit_left));
        sTurnIconImageMap.put(NavigationTurn.NAV_TURN_KEEP_LEFT, Integer.valueOf(R.drawable.tbt_stay_left));
        sTurnIconImageMap.put(NavigationTurn.NAV_TURN_KEEP_RIGHT, Integer.valueOf(R.drawable.tbt_stay_right));
        sTurnIconImageMap.put(NavigationTurn.NAV_TURN_END, Integer.valueOf(R.drawable.tbt_end));
        sTurnIconImageMap.put(NavigationTurn.NAV_TURN_STRAIGHT, Integer.valueOf(R.drawable.tbt_go_straight));
        sTurnIconImageMap.put(NavigationTurn.NAV_TURN_UTURN_LEFT, Integer.valueOf(R.drawable.tbt_u_turn_left));
        sTurnIconImageMap.put(NavigationTurn.NAV_TURN_UTURN_RIGHT, Integer.valueOf(R.drawable.tbt_u_turn_right));
        sTurnIconImageMap.put(NavigationTurn.NAV_TURN_ROUNDABOUT_S, Integer.valueOf(R.drawable.tbt_left_roundabout));
        sTurnIconImageMap.put(NavigationTurn.NAV_TURN_ROUNDABOUT_N, Integer.valueOf(R.drawable.tbt_right_roundabout));
        sTurnIconImageMap.put(NavigationTurn.NAV_TURN_MERGE_LEFT, Integer.valueOf(R.drawable.tbt_merge_left));
        sTurnIconImageMap.put(NavigationTurn.NAV_TURN_MERGE_RIGHT, Integer.valueOf(R.drawable.tbt_merge_right));
        sTurnIconSoonImageMap.put(NavigationTurn.NAV_TURN_START, Integer.valueOf(R.drawable.tbt_straight_soon));
        sTurnIconSoonImageMap.put(NavigationTurn.NAV_TURN_LEFT, Integer.valueOf(R.drawable.tbt_turn_left_soon));
        sTurnIconSoonImageMap.put(NavigationTurn.NAV_TURN_RIGHT, Integer.valueOf(R.drawable.tbt_turn_right_soon));
        sTurnIconSoonImageMap.put(NavigationTurn.NAV_TURN_EASY_RIGHT, Integer.valueOf(R.drawable.tbt_easy_right_soon));
        sTurnIconSoonImageMap.put(NavigationTurn.NAV_TURN_EASY_LEFT, Integer.valueOf(R.drawable.tbt_easy_left_soon));
        sTurnIconSoonImageMap.put(NavigationTurn.NAV_TURN_SHARP_LEFT, Integer.valueOf(R.drawable.tbt_sharp_left_soon));
        sTurnIconSoonImageMap.put(NavigationTurn.NAV_TURN_SHARP_RIGHT, Integer.valueOf(R.drawable.tbt_sharp_right_soon));
        sTurnIconSoonImageMap.put(NavigationTurn.NAV_TURN_EXIT_RIGHT, Integer.valueOf(R.drawable.tbt_exit_right_soon));
        sTurnIconSoonImageMap.put(NavigationTurn.NAV_TURN_EXIT_LEFT, Integer.valueOf(R.drawable.tbt_exit_left_soon));
        sTurnIconSoonImageMap.put(NavigationTurn.NAV_TURN_KEEP_LEFT, Integer.valueOf(R.drawable.tbt_stay_left_soon));
        sTurnIconSoonImageMap.put(NavigationTurn.NAV_TURN_KEEP_RIGHT, Integer.valueOf(R.drawable.tbt_stay_right_soon));
        sTurnIconSoonImageMap.put(NavigationTurn.NAV_TURN_END, Integer.valueOf(R.drawable.tbt_end_soon));
        sTurnIconSoonImageMap.put(NavigationTurn.NAV_TURN_STRAIGHT, Integer.valueOf(R.drawable.tbt_go_straight_soon));
        sTurnIconSoonImageMap.put(NavigationTurn.NAV_TURN_UTURN_LEFT, Integer.valueOf(R.drawable.tbt_u_turn_left_soon));
        sTurnIconSoonImageMap.put(NavigationTurn.NAV_TURN_UTURN_RIGHT, Integer.valueOf(R.drawable.tbt_u_turn_right_soon));
        sTurnIconSoonImageMap.put(NavigationTurn.NAV_TURN_ROUNDABOUT_S, Integer.valueOf(R.drawable.tbt_left_roundabout_soon));
        sTurnIconSoonImageMap.put(NavigationTurn.NAV_TURN_ROUNDABOUT_N, Integer.valueOf(R.drawable.tbt_right_roundabout_soon));
        sTurnIconSoonImageMap.put(NavigationTurn.NAV_TURN_MERGE_LEFT, Integer.valueOf(R.drawable.tbt_merge_left_soon));
        sTurnIconSoonImageMap.put(NavigationTurn.NAV_TURN_MERGE_RIGHT, Integer.valueOf(R.drawable.tbt_merge_right_soon));
        sTurnIconSoonImageMapGrey.put(NavigationTurn.NAV_TURN_START, Integer.valueOf(R.drawable.icon_tbt_start_grey));
        sTurnIconSoonImageMapGrey.put(NavigationTurn.NAV_TURN_LEFT, Integer.valueOf(R.drawable.icon_tbt_quite_left_grey));
        sTurnIconSoonImageMapGrey.put(NavigationTurn.NAV_TURN_RIGHT, Integer.valueOf(R.drawable.icon_tbt_quite_right_grey));
        sTurnIconSoonImageMapGrey.put(NavigationTurn.NAV_TURN_EASY_RIGHT, Integer.valueOf(R.drawable.icon_tbt_light_right_grey));
        sTurnIconSoonImageMapGrey.put(NavigationTurn.NAV_TURN_EASY_LEFT, Integer.valueOf(R.drawable.icon_tbt_light_left_grey));
        sTurnIconSoonImageMapGrey.put(NavigationTurn.NAV_TURN_SHARP_LEFT, Integer.valueOf(R.drawable.icon_tbt_heavy_left_grey));
        sTurnIconSoonImageMapGrey.put(NavigationTurn.NAV_TURN_SHARP_RIGHT, Integer.valueOf(R.drawable.icon_tbt_heavy_right_grey));
        sTurnIconSoonImageMapGrey.put(NavigationTurn.NAV_TURN_EXIT_RIGHT, Integer.valueOf(R.drawable.icon_tbt_leave_highway_grey));
        sTurnIconSoonImageMapGrey.put(NavigationTurn.NAV_TURN_EXIT_LEFT, Integer.valueOf(R.drawable.icon_tbt_leave_highway_left_lane_grey));
        sTurnIconSoonImageMapGrey.put(NavigationTurn.NAV_TURN_KEEP_LEFT, Integer.valueOf(R.drawable.icon_tbt_keep_left_grey));
        sTurnIconSoonImageMapGrey.put(NavigationTurn.NAV_TURN_KEEP_RIGHT, Integer.valueOf(R.drawable.icon_tbt_keep_right_grey));
        sTurnIconSoonImageMapGrey.put(NavigationTurn.NAV_TURN_END, Integer.valueOf(R.drawable.icon_tbt_arrive_grey));
        sTurnIconSoonImageMapGrey.put(NavigationTurn.NAV_TURN_STRAIGHT, Integer.valueOf(R.drawable.icon_tbt_go_straight_grey));
        sTurnIconSoonImageMapGrey.put(NavigationTurn.NAV_TURN_UTURN_LEFT, Integer.valueOf(R.drawable.tbt_icon_return_grey));
        sTurnIconSoonImageMapGrey.put(NavigationTurn.NAV_TURN_UTURN_RIGHT, Integer.valueOf(R.drawable.icon_tbt_uturn_right_grey));
        sTurnIconSoonImageMapGrey.put(NavigationTurn.NAV_TURN_ROUNDABOUT_S, Integer.valueOf(R.drawable.tbt_left_roundabout_soon));
        sTurnIconSoonImageMapGrey.put(NavigationTurn.NAV_TURN_ROUNDABOUT_N, Integer.valueOf(R.drawable.tbt_right_roundabout_soon));
        sTurnIconSoonImageMapGrey.put(NavigationTurn.NAV_TURN_MERGE_LEFT, Integer.valueOf(R.drawable.icon_tbt_merge_left_grey));
        sTurnIconSoonImageMapGrey.put(NavigationTurn.NAV_TURN_MERGE_RIGHT, Integer.valueOf(R.drawable.icon_tbt_merge_right_grey));
        EMPTY_MANEUVER_DISPLAY.empty = true;
        allowedTurnTextMap.add(EXIT);
        allowedTurnTextMap.add(UTURN);
        allowedTurnTextMap.add(START_TURN);
        allowedTurnTextMap.add(END_TURN);
        allowedTurnTextMap.add(ARRIVED);
        allowedTurnTextMap.add(sHereTurnToNavigationTurnMap.get(Turn.ROUNDABOUT_1));
        allowedTurnTextMap.add(sHereTurnToNavigationTurnMap.get(Turn.ROUNDABOUT_2));
        allowedTurnTextMap.add(sHereTurnToNavigationTurnMap.get(Turn.ROUNDABOUT_3));
        allowedTurnTextMap.add(sHereTurnToNavigationTurnMap.get(Turn.ROUNDABOUT_4));
        allowedTurnTextMap.add(sHereTurnToNavigationTurnMap.get(Turn.ROUNDABOUT_5));
        allowedTurnTextMap.add(sHereTurnToNavigationTurnMap.get(Turn.ROUNDABOUT_6));
        allowedTurnTextMap.add(sHereTurnToNavigationTurnMap.get(Turn.ROUNDABOUT_7));
        allowedTurnTextMap.add(sHereTurnToNavigationTurnMap.get(Turn.ROUNDABOUT_8));
        allowedTurnTextMap.add(sHereTurnToNavigationTurnMap.get(Turn.ROUNDABOUT_9));
        allowedTurnTextMap.add(sHereTurnToNavigationTurnMap.get(Turn.ROUNDABOUT_10));
        allowedTurnTextMap.add(sHereTurnToNavigationTurnMap.get(Turn.ROUNDABOUT_11));
        allowedTurnTextMap.add(sHereTurnToNavigationTurnMap.get(Turn.ROUNDABOUT_12));
    }

    public static ManeuverDisplay getManeuverDisplay(Maneuver current, boolean last, long distance, String streetAddress, Maneuver previousManeuver, NavigationRouteRequest request, Maneuver nextManeuver, boolean calculateEarlyManeuver, boolean calcManeuverState, DestinationDirection destinationDirection) {
        PendingRoadInfo info = new PendingRoadInfo();
        ManeuverDisplay display = new ManeuverDisplay();
        display.maneuverId = current != null ? String.valueOf(System.identityHashCode(current)) : "unknown";
        RoadElement currentRoadElement = HereMapUtil.getCurrentRoadElement();
        boolean isHighway = HereMapUtil.isCurrentRoadHighway(current);
        if (!isHighway) {
            isHighway = HereMapUtil.isCurrentRoadHighway(currentRoadElement);
        }
        if (calcManeuverState) {
            if (distance >= UpdateOptions.SOURCE_ANY) {
                distance = 0;
            }
            display.maneuverState = getManeuverState(distance, isHighway);
        } else {
            display.maneuverState = ManeuverState.NOW;
        }
        setNavigationTurnIconId(current, distance, display.maneuverState, display, calculateEarlyManeuver ? nextManeuver : null, isHighway, destinationDirection);
        setNavigationDistance(distance, display, false, !calcManeuverState);
        if (display.maneuverState == ManeuverState.STAY) {
            if (currentRoadElement == null) {
                display.currentRoad = null;
            } else {
                display.currentRoad = HereMapUtil.concatText(currentRoadElement.getRouteName(), currentRoadElement.getRoadName(), true, isHighway);
                if (!TextUtils.isEmpty(display.currentRoad)) {
                    lastStayManeuverRoadName = display.currentRoad;
                    lastStayManeuver = current;
                }
            }
            if (TextUtils.isEmpty(display.currentRoad)) {
                if (lastStayManeuver != current) {
                    lastStayManeuver = null;
                    lastStayManeuverRoadName = null;
                } else if (lastStayManeuverRoadName != null) {
                    display.currentRoad = lastStayManeuverRoadName;
                }
            }
            if (TextUtils.isEmpty(display.currentRoad)) {
                if (previousManeuver != null) {
                    display.currentRoad = HereMapUtil.concatText(previousManeuver.getNextRoadNumber(), previousManeuver.getNextRoadName(), true, isHighway);
                }
                if (display.currentRoad == null) {
                    display.currentRoad = "";
                }
            }
        } else {
            lastStayManeuver = null;
            lastStayManeuverRoadName = null;
            display.currentRoad = HereMapUtil.concatText(current.getRoadNumber(), current.getRoadName(), true, isHighway);
            if (TextUtils.isEmpty(display.currentRoad)) {
                display.currentRoad = getPendingRoadText(current, previousManeuver, nextManeuver, info);
            }
        }
        display.currentSpeedLimit = HereMapUtil.getCurrentSpeedLimit();
        if (last) {
            display.pendingTurn = END_TURN;
            String currentRoad = HereNavigationManager.getInstance().getDestinationLabel();
            if (TextUtils.isEmpty(currentRoad)) {
                if (streetAddress != null) {
                    currentRoad = HereMapUtil.parseStreetAddress(streetAddress);
                } else {
                    currentRoad = HereNavigationManager.getInstance().getDestinationStreetAddress();
                }
                if (TextUtils.isEmpty(currentRoad)) {
                    currentRoad = current.getRoadName();
                }
            }
            DestinationDirection direction = null;
            if (request != null) {
                Coordinate c1 = request.destination;
                Coordinate c2 = request.destinationDisplay;
                if (c1 == null || c2 == null) {
                    display.direction = DestinationDirection.UNKNOWN;
                    sLogger.v("dest(1) cannot calculate direction pos not available");
                } else {
                    GeoCoordinate M = new GeoCoordinate(request.destinationDisplay.latitude.doubleValue(), request.destinationDisplay.longitude.doubleValue());
                    GeoCoordinate B = new GeoCoordinate(request.destination.latitude.doubleValue(), request.destination.longitude.doubleValue());
                    if (previousManeuver == null) {
                        Route route = HereNavigationManager.getInstance().getCurrentRoute();
                        if (route != null) {
                            List<Maneuver> maneuvers = route.getManeuvers();
                            if (maneuvers != null && maneuvers.size() == 2) {
                                previousManeuver = (Maneuver) maneuvers.get(0);
                            }
                        }
                    }
                    GeoCoordinate A = getEstimatedPointAfromManeuver(previousManeuver, B);
                    if (A != null) {
                        DestinationDirection direction2;
                        double position = Math.sin(((B.getLatitude() - A.getLatitude()) * (M.getLongitude() - A.getLongitude())) - ((B.getLongitude() - A.getLongitude()) * (M.getLatitude() - A.getLatitude())));
                        if (position >= 0.0d) {
                            direction = DestinationDirection.RIGHT;
                        } else if (position < 0.0d) {
                            direction = DestinationDirection.LEFT;
                        } else {
                            direction = DestinationDirection.UNKNOWN;
                        }
                        sLogger.v("dest(1) position=" + position + " DIRECTION = " + direction);
                        display.direction = direction;
                        if (direction != DestinationDirection.UNKNOWN) {
                            display.destinationIconId = getDestinationIcon(direction);
                        }
                        double navToDisplayAngle = B.getHeading(M);
                        double estimatedToNavAngle = A.getHeading(B);
                        int diffAngle = (int) (navToDisplayAngle - estimatedToNavAngle);
                        sLogger.v("dest(2): navToDisplayAngle=" + ((int) navToDisplayAngle) + " currentHereAngle=" + ((int) estimatedToNavAngle) + " diffAngle =" + diffAngle);
                        if (diffAngle < 0) {
                            diffAngle += 360;
                        }
                        if (diffAngle >= 1 && diffAngle <= 179) {
                            direction2 = DestinationDirection.RIGHT;
                        } else if (diffAngle < 181 || diffAngle > 359) {
                            direction2 = DestinationDirection.UNKNOWN;
                        } else {
                            direction2 = DestinationDirection.LEFT;
                        }
                        sLogger.v("dest(2): diffAngle=" + diffAngle + " direction=" + direction2);
                    } else {
                        display.direction = DestinationDirection.UNKNOWN;
                        sLogger.v("dest(1) cannot calculate direction, A pos not available");
                    }
                }
            }
            Resources resources;
            Object[] objArr;
            if (direction != null) {
                resources = resources;
                objArr = new Object[2];
                if (TextUtils.isEmpty(currentRoad)) {
                    currentRoad = resources.getString(R.string.end_maneuver_direction_no_road);
                }
                objArr[0] = currentRoad;
                objArr[1] = getDestinationDirection(direction);
                display.pendingRoad = resources.getString(R.string.end_maneuver_direction, objArr);
            } else {
                resources = resources;
                objArr = new Object[1];
                if (TextUtils.isEmpty(currentRoad)) {
                    currentRoad = resources.getString(R.string.end_maneuver_direction_no_road);
                }
                objArr[0] = currentRoad;
                display.pendingRoad = resources.getString(R.string.end_maneuver, objArr);
            }
        } else {
            display.pendingTurn = getTurnText(current, info, display.maneuverState, previousManeuver, nextManeuver);
            if (display.maneuverState == ManeuverState.STAY) {
                display.pendingRoad = display.currentRoad;
            } else {
                String pendingRoadText = getPendingRoadText(current, previousManeuver, nextManeuver, info);
                if (TextUtils.isEmpty(pendingRoadText)) {
                    pendingRoadText = HereMapUtil.getRoadName(current.getRoadElements());
                }
                if (TextUtils.isEmpty(pendingRoadText)) {
                    display.pendingRoad = "";
                } else {
                    display.pendingRoad = pendingRoadText;
                }
            }
        }
        return display;
    }

    public static ManeuverDisplay getStartManeuverDisplay(Maneuver current, Maneuver next) {
        ManeuverDisplay display = new ManeuverDisplay();
        boolean isCurrentHighway = HereMapUtil.isCurrentRoadHighway(current);
        boolean isNextHighway = HereMapUtil.isCurrentRoadHighway(next);
        String currentRoad = HereMapUtil.concatText(current.getRoadNumber(), current.getRoadName(), true, isCurrentHighway);
        String nextRoad = HereMapUtil.concatText(next.getRoadNumber(), next.getRoadName(), true, isNextHighway);
        if (TextUtils.isEmpty(currentRoad)) {
            currentRoad = nextRoad;
            String str = HereMapUtil.concatText(next.getNextRoadNumber(), next.getNextRoadName(), isNextHighway, isNextHighway);
            if (!TextUtils.isEmpty(str)) {
                nextRoad = str;
            }
        }
        if (TextUtils.isEmpty(currentRoad)) {
            currentRoad = getPendingRoadText(current, null, null, null);
        }
        display.turnIconId = ((Integer) sTurnIconImageMap.get(NavigationTurn.NAV_TURN_START)).intValue();
        display.pendingTurn = START_TURN;
        display.currentRoad = HereMapUtil.concatText(current.getRoadNumber(), current.getRoadName(), true, isCurrentHighway);
        display.currentSpeedLimit = HereMapUtil.getCurrentSpeedLimit();
        if (!TextUtils.equals(currentRoad, nextRoad)) {
            display.pendingRoad = resources.getString(R.string.start_maneuver, new Object[]{currentRoad, nextRoad});
        } else if (TextUtils.isEmpty(currentRoad)) {
            display.pendingRoad = "";
        } else {
            display.pendingRoad = resources.getString(R.string.start_maneuver_no_next_road, new Object[]{currentRoad});
        }
        setNavigationDistance(0, display, false, false);
        display.totalDistanceRemaining = display.totalDistance;
        display.totalDistanceRemainingUnit = display.totalDistanceUnit;
        display.distanceToPendingRoadText = "";
        display.navigationTurn = NavigationTurn.NAV_TURN_START;
        display.maneuverId = current != null ? String.valueOf(System.identityHashCode(current)) : "start";
        return display;
    }

    public static ManeuverDisplay getArrivedManeuverDisplay() {
        ManeuverDisplay display = new ManeuverDisplay();
        display.maneuverId = "arrived";
        display.turnIconId = R.drawable.icon_tbt_arrive;
        display.pendingTurn = ARRIVED;
        HereNavigationManager hereNavigationManager = HereNavigationManager.getInstance();
        String currentRoad = hereNavigationManager.getDestinationLabel();
        if (TextUtils.isEmpty(currentRoad)) {
            currentRoad = hereNavigationManager.getDestinationStreetAddress();
            if (TextUtils.isEmpty(currentRoad)) {
                currentRoad = "";
            }
        }
        display.pendingRoad = currentRoad;
        return display;
    }

    private static String getTurnText(Maneuver current, PendingRoadInfo roadInfo, ManeuverState maneuverState, Maneuver prevManeuver, Maneuver nextManeuver) {
        Action action = current.getAction();
        Turn turn = current.getTurn();
        if (maneuverState == ManeuverState.STAY) {
            return STAY_ON;
        }
        if (HereMapUtil.isHighwayAction(current)) {
            if (action == Action.ENTER_HIGHWAY_FROM_LEFT || action == Action.ENTER_HIGHWAY_FROM_RIGHT) {
                return ENTER_HIGHWAY;
            }
            if (!(prevManeuver == null || nextManeuver == null)) {
                if (HereMapUtil.isHighwayAction(prevManeuver)) {
                    roadInfo.enterHighway = true;
                    roadInfo.usePrevManeuverInfo = true;
                    turn = prevManeuver.getTurn();
                } else if (HereMapUtil.isHighwayAction(nextManeuver)) {
                    roadInfo.enterHighway = true;
                }
            }
        } else if (action == Action.LEAVE_HIGHWAY) {
            String from = HereMapUtil.getRoadName(current);
            String to = HereMapUtil.getNextRoadName(current);
            if (!TextUtils.equals(from, to)) {
                return EXIT;
            }
            sLogger.v("ignoring action[" + action + "] using[" + turn + "] from[" + from + "] to[" + to + "]");
        } else if (action == Action.UTURN) {
            return UTURN;
        }
        switch (turn) {
            case QUITE_LEFT:
            case QUITE_RIGHT:
            case HEAVY_LEFT:
            case HEAVY_RIGHT:
                roadInfo.dontUseSignpostText = true;
                break;
            case KEEP_RIGHT:
            case KEEP_MIDDLE:
            case KEEP_LEFT:
                roadInfo.hasTo = true;
                break;
        }
        return (String) sHereTurnToNavigationTurnMap.get(turn);
    }

    private static String getPendingRoadText(Maneuver maneuver, Maneuver before, Maneuver after, PendingRoadInfo pendingRoadInfo) {
        String stringBuilder;
        if (!(pendingRoadInfo == null || !pendingRoadInfo.usePrevManeuverInfo || before == null)) {
            maneuver = before;
            before = null;
            after = null;
        }
        String number = maneuver.getNextRoadNumber();
        String name = maneuver.getNextRoadName();
        Signpost signpost = maneuver.getSignpost();
        SignPostInfo info = new SignPostInfo();
        String signPostText = HereMapUtil.getSignpostText(signpost, maneuver, info, null);
        if (!(pendingRoadInfo == null || !pendingRoadInfo.dontUseSignpostText || (TextUtils.isEmpty(number) && TextUtils.isEmpty(name)))) {
            signPostText = null;
            info.hasNameInSignPost = false;
            info.hasNumberInSignPost = false;
        }
        boolean useToRoad = false;
        boolean useSignpost = false;
        boolean toInstructionAdded = false;
        boolean ontoInstructionAdded = false;
        boolean maneuverMatch = HereMapUtil.hasSameSignpostAndToRoad(maneuver, before);
        if (maneuverMatch) {
            useToRoad = true;
        } else {
            maneuverMatch = HereMapUtil.hasSameSignpostAndToRoad(maneuver, after);
            if (maneuverMatch) {
                useSignpost = true;
            }
        }
        synchronized (mainSignPostBuilder) {
            mainSignPostBuilder.setLength(0);
            if (!(TextUtils.isEmpty(number) || info.hasNumberInSignPost)) {
                mainSignPostBuilder.append(number);
            }
            boolean hasSignPost = false;
            if (!TextUtils.isEmpty(signPostText)) {
                boolean addSignpost = true;
                if (maneuverMatch && !useSignpost) {
                    addSignpost = false;
                    signPostText = "";
                }
                hasSignPost = true;
                if (addSignpost) {
                    if (HereMapUtil.needSeparator(mainSignPostBuilder)) {
                        mainSignPostBuilder.append(SLASH);
                    }
                    mainSignPostBuilder.append(signPostText);
                }
            }
            String toInstruction = null;
            if (!TextUtils.isEmpty(name)) {
                if (hasSignPost && !signPostText.contains(name)) {
                    boolean addOnto = true;
                    if (maneuverMatch && !useToRoad) {
                        addOnto = false;
                    }
                    if (addOnto) {
                        if (pendingRoadInfo != null && pendingRoadInfo.hasTo) {
                            toInstruction = mainSignPostBuilder.toString();
                            mainSignPostBuilder.setLength(0);
                        } else if (HereMapUtil.needSeparator(mainSignPostBuilder)) {
                            mainSignPostBuilder.append(" ");
                        }
                        if (useToRoad) {
                            if (HereMapUtil.needSeparator(mainSignPostBuilder)) {
                                mainSignPostBuilder.append(" ");
                            }
                        } else if (!MapSettings.isTbtOntoDisabled()) {
                            mainSignPostBuilder.append(ONTO);
                            mainSignPostBuilder.append(" ");
                            ontoInstructionAdded = true;
                        }
                        if (TextUtils.isEmpty(toInstruction) && pendingRoadInfo != null && pendingRoadInfo.hasTo) {
                            pendingRoadInfo.hasTo = false;
                            if (!MapSettings.doNotShowTurnTextInTBT()) {
                                mainSignPostBuilder.append(TOWARDS);
                                mainSignPostBuilder.append(" ");
                            }
                            toInstructionAdded = true;
                        }
                        if (!MapSettings.isTbtOntoDisabled()) {
                            mainSignPostBuilder.append(HereMapUtil.concatText(number, name, true, HereMapUtil.isCurrentRoadHighway(maneuver)));
                        } else if ((pendingRoadInfo == null || !pendingRoadInfo.hasTo) && TextUtils.isEmpty(signPostText)) {
                            mainSignPostBuilder.append(HereMapUtil.concatText(number, name, true, HereMapUtil.isCurrentRoadHighway(maneuver)));
                        }
                    }
                    info.hasNameInSignPost = true;
                }
                if (!info.hasNameInSignPost) {
                    boolean needName = true;
                    if (mainSignPostBuilder.length() > 0 && HereMapUtil.isCurrentRoadHighway(maneuver)) {
                        needName = false;
                    }
                    if (needName) {
                        boolean bracket = false;
                        if (HereMapUtil.needSeparator(mainSignPostBuilder)) {
                            mainSignPostBuilder.append(" ");
                        }
                        if (!TextUtils.isEmpty(number)) {
                            bracket = true;
                            mainSignPostBuilder.append(OPEN_BRACKET);
                        }
                        mainSignPostBuilder.append(name);
                        if (bracket) {
                            mainSignPostBuilder.append(CLOSE_BRACKET);
                        }
                    }
                }
                if (!TextUtils.isEmpty(toInstruction)) {
                    String onto = mainSignPostBuilder.toString();
                    int index = onto.indexOf(ONTO + " ");
                    if (index != -1) {
                        onto = onto.substring((ONTO.length() + index) + " ".length()).trim();
                    }
                    if (!TextUtils.isEmpty(onto) && toInstruction.contains(onto)) {
                        mainSignPostBuilder.setLength(0);
                    }
                    if (HereMapUtil.needSeparator(mainSignPostBuilder)) {
                        mainSignPostBuilder.append(" ");
                    }
                    if (!MapSettings.doNotShowTurnTextInTBT()) {
                        mainSignPostBuilder.append(TOWARDS);
                        mainSignPostBuilder.append(" ");
                    }
                    mainSignPostBuilder.append(toInstruction);
                    toInstructionAdded = true;
                    pendingRoadInfo.hasTo = false;
                } else if (pendingRoadInfo != null && pendingRoadInfo.hasTo) {
                    toInstruction = mainSignPostBuilder.toString();
                    mainSignPostBuilder.setLength(0);
                    if (!MapSettings.doNotShowTurnTextInTBT()) {
                        mainSignPostBuilder.append(TOWARDS);
                        mainSignPostBuilder.append(" ");
                    }
                    mainSignPostBuilder.append(toInstruction);
                    toInstructionAdded = true;
                    pendingRoadInfo.hasTo = false;
                }
            }
            if (pendingRoadInfo != null) {
                String str;
                if (!MapSettings.isTbtOntoDisabled() && pendingRoadInfo.enterHighway && !ontoInstructionAdded && !toInstructionAdded && mainSignPostBuilder.length() > 0) {
                    str = mainSignPostBuilder.toString();
                    mainSignPostBuilder.setLength(0);
                    mainSignPostBuilder.append(ONTO);
                    mainSignPostBuilder.append(" ");
                    mainSignPostBuilder.append(str);
                } else if (pendingRoadInfo.hasTo && mainSignPostBuilder.length() > 0) {
                    str = mainSignPostBuilder.toString();
                    mainSignPostBuilder.setLength(0);
                    if (!MapSettings.doNotShowTurnTextInTBT()) {
                        mainSignPostBuilder.append(TOWARDS);
                        mainSignPostBuilder.append(" ");
                    }
                    mainSignPostBuilder.append(str);
                }
            }
            stringBuilder = mainSignPostBuilder.toString();
        }
        return stringBuilder;
    }

    private static NavigationTurn getNavigationTurn(Maneuver maneuver) {
        Icon icon = maneuver.getIcon();
        if (icon == null) {
            return null;
        }
        return (NavigationTurn) sHereIconToNavigationIconMap.get(icon);
    }

    private static void setNavigationTurnIconId(Maneuver maneuver, long distance, ManeuverState maneuverState, ManeuverDisplay display, Maneuver nextManeuver, boolean isHighway, DestinationDirection destinationDirection) {
        Integer resourceId = null;
        Integer resourceSoonId = null;
        Integer resourceNowId = null;
        switch (maneuverState) {
            case STAY:
                resourceId = Integer.valueOf(R.drawable.tbt_go_straight_soon);
                display.navigationTurn = NavigationTurn.NAV_TURN_STRAIGHT;
                break;
            case NEXT:
            case SOON:
            case NOW:
                NavigationTurn navigationTurn = getNavigationTurn(maneuver);
                display.navigationTurn = navigationTurn;
                if (maneuverState == ManeuverState.NOW || maneuverState == ManeuverState.SOON) {
                    resourceId = (Integer) sTurnIconImageMap.get(navigationTurn);
                } else {
                    resourceId = (Integer) sTurnIconSoonImageMap.get(navigationTurn);
                }
                resourceNowId = (Integer) sTurnIconImageMap.get(navigationTurn);
                resourceSoonId = (Integer) sTurnIconSoonImageMapGrey.get(navigationTurn);
                if (!(display.navigationTurn != NavigationTurn.NAV_TURN_END || destinationDirection == null || destinationDirection == DestinationDirection.UNKNOWN)) {
                    if (maneuverState != ManeuverState.NOW) {
                        switch (destinationDirection) {
                            case LEFT:
                                resourceId = Integer.valueOf(R.drawable.tbt_arrive_left_soon);
                                break;
                            case RIGHT:
                                resourceId = Integer.valueOf(R.drawable.tbt_arrive_right_soon);
                                break;
                        }
                    }
                    switch (destinationDirection) {
                        case LEFT:
                            resourceId = Integer.valueOf(R.drawable.tbt_arrive_left);
                            break;
                        case RIGHT:
                            resourceId = Integer.valueOf(R.drawable.tbt_arrive_right);
                            break;
                    }
                }
                if (nextManeuver == null) {
                    display.turnIconId = -1;
                    display.turnIconNowId = 0;
                    display.turnIconSoonId = 0;
                    break;
                }
                long threshold;
                if (isHighway) {
                    threshold = SOON_DISTANCE_THRESHOLD_HIGHWAY;
                } else {
                    threshold = SOON_DISTANCE_THRESHOLD;
                }
                if (distance <= threshold) {
                    long nextDistance = (long) nextManeuver.getDistanceFromPreviousManeuver();
                    if (isHighway) {
                        threshold = 1000;
                    } else {
                        threshold = 250;
                    }
                    if (nextDistance <= threshold) {
                        Integer nextResourceId = (Integer) sTurnIconSoonImageMap.get(getNavigationTurn(nextManeuver));
                        if (resourceId == null) {
                            display.nextTurnIconId = -1;
                        } else {
                            display.nextTurnIconId = nextResourceId.intValue();
                        }
                        sLogger.v("distance is less than threshold " + nextDistance);
                        break;
                    }
                }
                display.nextTurnIconId = -1;
                break;
                break;
        }
        if (resourceId == null) {
            display.turnIconId = -1;
            display.turnIconNowId = 0;
            display.turnIconSoonId = 0;
            return;
        }
        display.turnIconId = resourceId.intValue();
        display.turnIconSoonId = resourceSoonId == null ? 0 : resourceSoonId.intValue();
        display.turnIconNowId = resourceNowId == null ? 0 : resourceNowId.intValue();
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void setNavigationDistance(long meters, ManeuverDisplay maneuverDisplay, boolean extendedUnit, boolean calculateDistanceUnitOnly) {
        synchronized (DISTANCE) {
            SpeedUnit speedUnit = speedManager.getSpeedUnit();
            if (meters >= UpdateOptions.SOURCE_ANY) {
                meters = 0;
            }
            maneuverDisplay.distanceInMeters = meters;
            DistanceConverter.convertToDistance(speedUnit, (float) meters, DISTANCE);
            maneuverDisplay.distance = DISTANCE.value;
            maneuverDisplay.distanceUnit = DISTANCE.unit;
            maneuverDisplay.distanceToPendingRoadText = getFormattedDistance(maneuverDisplay.distance, maneuverDisplay.distanceUnit, extendedUnit);
            if (calculateDistanceUnitOnly) {
                return;
            }
            float totalDistance = -1.0f;
            float remainingDistance = -1.0f;
            HereNavigationManager hereNavigationManager = HereNavigationManager.getInstance();
            if (hereNavigationManager.isNavigationModeOn()) {
                Route route = hereNavigationManager.getCurrentRoute();
                if (route != null) {
                    remainingDistance = (float) hereNavigationManager.getNavController().getDestinationDistance();
                    if (remainingDistance < 0.0f) {
                        remainingDistance = 0.0f;
                    }
                    totalDistance = (float) route.getLength();
                }
            }
            if (totalDistance != -1.0f) {
                DistanceConverter.convertToDistance(speedUnit, totalDistance, DISTANCE);
                maneuverDisplay.totalDistance = DISTANCE.value;
                maneuverDisplay.totalDistanceUnit = DISTANCE.unit;
                DistanceConverter.convertToDistance(speedUnit, remainingDistance, DISTANCE);
                maneuverDisplay.totalDistanceRemaining = DISTANCE.value;
                maneuverDisplay.totalDistanceRemainingUnit = DISTANCE.unit;
            }
        }
    }

    static ManeuverState getManeuverState(long distance, boolean isHighway) {
        double nextThreshold;
        double soonThreshold;
        long nowThreshold;
        ManeuverState maneuverState;
        if (isHighway) {
            nextThreshold = 16093.0d;
            soonThreshold = 3218.0d;
            nowThreshold = NOW_DISTANCE_THRESHOLD_HIGHWAY;
        } else {
            nextThreshold = 6437.0d;
            soonThreshold = 1287.0d;
            nowThreshold = 30;
        }
        if (distance <= nowThreshold) {
            maneuverState = ManeuverState.NOW;
        } else if (((double) distance) <= soonThreshold) {
            maneuverState = ManeuverState.SOON;
        } else if (((double) distance) <= nextThreshold) {
            maneuverState = ManeuverState.NEXT;
        } else {
            maneuverState = ManeuverState.STAY;
        }
        if (sLogger.isLoggable(2)) {
            sLogger.v("maneuverState=" + maneuverState + " distance=" + distance + " isHighway=" + isHighway);
        }
        return maneuverState;
    }

    public static String getUnitDisplayStr(SpeedUnit unit) {
        switch (unit) {
            case MILES_PER_HOUR:
                return UNIT_MILES;
            case KILOMETERS_PER_HOUR:
                return UNIT_KILOMETERS;
            case METERS_PER_SECOND:
                return UNIT_METERS;
            default:
                return "";
        }
    }

    public static String getDestinationDirection(DestinationDirection direction) {
        switch (direction) {
            case LEFT:
                return DESTINATION_LEFT;
            case RIGHT:
                return DESTINATION_RIGHT;
            default:
                return null;
        }
    }

    public static int getDestinationIcon(DestinationDirection direction) {
        switch (direction) {
            case LEFT:
                return R.drawable.icon_pin_dot_destination_left;
            case RIGHT:
                return R.drawable.icon_pin_dot_destination_right;
            default:
                return R.drawable.icon_pin_dot_destination;
        }
    }

    private static GeoCoordinate getEstimatedPointAfromManeuver(Maneuver maneuver, GeoCoordinate destination) {
        if (maneuver == null || destination == null) {
            return null;
        }
        GeoCoordinate last = null;
        List<GeoCoordinate> geoCoordinates = maneuver.getManeuverGeometry();
        for (int i = geoCoordinates.size() - 1; i >= 0; i--) {
            GeoCoordinate geo = (GeoCoordinate) geoCoordinates.get(i);
            int distance = (int) geo.distanceTo(destination);
            if (distance >= 30) {
                sLogger.v("remaining=" + distance);
                return geo;
            }
            last = geo;
        }
        if (last != null) {
            sLogger.v("remaining=" + ((int) last.distanceTo(destination)));
        }
        return last;
    }

    public static String getFormattedDistance(float distance, DistanceUnit unit, boolean extendedUnit) {
        String distanceFormat;
        Object[] objArr;
        String str;
        String str2;
        Object[] objArr2;
        switch (unit) {
            case DISTANCE_MILES:
                if (distance < 10.0f) {
                    distanceFormat = "%.1f %s";
                } else {
                    distanceFormat = "%.0f %s";
                }
                objArr = new Object[2];
                objArr[0] = Float.valueOf(distance);
                str = !extendedUnit ? UNIT_MILES : distance <= 1.0f ? UNIT_MILES_EXTENDED_SINGULAR : UNIT_MILES_EXTENDED;
                objArr[1] = str;
                return String.format(distanceFormat, objArr);
            case DISTANCE_FEET:
                str2 = "%.0f %s";
                objArr2 = new Object[2];
                objArr2[0] = Float.valueOf(HereMapUtil.roundToIntegerStep(10, distance));
                str = !extendedUnit ? UNIT_FEET : distance <= 1.0f ? UNIT_FEET_EXTENDED_SINGULAR : UNIT_FEET_EXTENDED;
                objArr2[1] = str;
                return String.format(str2, objArr2);
            case DISTANCE_KMS:
                if (distance < 10.0f) {
                    distanceFormat = "%.1f %s";
                } else {
                    distanceFormat = "%.0f %s";
                }
                objArr = new Object[2];
                objArr[0] = Float.valueOf(distance);
                str = !extendedUnit ? UNIT_KILOMETERS : distance <= 1.0f ? UNIT_KILOMETERS_EXTENDED_SINGULAR : UNIT_KILOMETERS_EXTENDED;
                objArr[1] = str;
                return String.format(distanceFormat, objArr);
            case DISTANCE_METERS:
                str2 = "%.0f %s";
                objArr2 = new Object[2];
                objArr2[0] = Float.valueOf(HereMapUtil.roundToIntegerStep(5, distance));
                str = !extendedUnit ? UNIT_METERS : distance <= 1.0f ? UNIT_METERS_EXTENDED_SINGULAR : UNIT_METERS_EXTENDED;
                objArr2[1] = str;
                return String.format(str2, objArr2);
            default:
                return String.valueOf((int) distance);
        }
    }

    public static boolean canShowTurnText(String turnText) {
        if (!MapSettings.doNotShowTurnTextInTBT() || allowedTurnTextMap.contains(turnText)) {
            return true;
        }
        return false;
    }

    public static boolean shouldShowTurnText(String turnText) {
        return allowedTurnTextMap.contains(turnText);
    }
}
