package com.navdy.hud.app.maps.notification;

import com.navdy.hud.app.framework.voice.TTSUtils;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.maps.MapEvents.RouteCalculationEvent;
import com.navdy.hud.app.maps.here.HereNavigationManager;
import com.navdy.hud.app.maps.here.HereRouteCache;
import com.navdy.hud.app.maps.here.HereRouteCache.RouteInfo;
import com.navdy.hud.app.maps.here.HereRouteManager;
import com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.DestinationPickerState;
import com.navdy.hud.app.ui.component.destination.IDestinationPicker;
import com.navdy.service.library.events.audio.SpeechRequest.Category;
import com.navdy.service.library.events.navigation.NavigationRouteResult;
import com.navdy.service.library.events.navigation.NavigationSessionRouteChange.RerouteReason;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.task.TaskManager;
import com.squareup.otto.Bus;

public class RouteCalcPickerHandler implements IDestinationPicker {
    private static final Logger sLogger = new Logger(RouteCalcPickerHandler.class);
    private RouteCalculationEvent event;

    public RouteCalcPickerHandler(RouteCalculationEvent event) {
        this.event = event;
    }

    public boolean onItemClicked(int id, int pos, DestinationPickerState state) {
        RemoteDeviceManager.getInstance().getUiStateManager().getNavigationView().cleanupMapOverviewRoutes();
        if (pos == 0) {
            sLogger.v("map-route- onItemClicked current route");
        } else {
            sLogger.v("map-route- onItemClicked new route");
            int len = this.event.response.results.size();
            if (pos >= len) {
                sLogger.w("map-route- invalid pos:" + pos + " len=" + len);
            } else {
                final NavigationRouteResult result = (NavigationRouteResult) this.event.response.results.get(pos);
                final RouteInfo routeInfo = HereRouteCache.getInstance().getRoute(result.routeId);
                if (routeInfo == null) {
                    sLogger.w("map-route- route not found in cache:" + result.routeId);
                } else {
                    state.doNotAddOriginalRoute = true;
                    TaskManager.getInstance().execute(new Runnable() {
                        public void run() {
                            HereNavigationManager hereNavigationManager = HereNavigationManager.getInstance();
                            hereNavigationManager.setReroute(routeInfo.route, RerouteReason.NAV_SESSION_ROUTE_PICKER, result.routeId, result.via, true, hereNavigationManager.isTrafficConsidered(result.routeId));
                            if (hereNavigationManager.getNavigationSessionPreference().spokenTurnByTurn) {
                                String tts = HereRouteManager.buildChangeRouteTTS(result.routeId);
                                RouteCalcPickerHandler.sLogger.v("tts[" + tts + "]");
                                Bus bus = RemoteDeviceManager.getInstance().getBus();
                                bus.post(RouteCalculationNotification.CANCEL_TBT_TTS);
                                bus.post(RouteCalculationNotification.CANCEL_CALC_TTS);
                                TTSUtils.sendSpeechRequest(tts, Category.SPEECH_TURN_BY_TURN, RouteCalculationNotification.ROUTE_CALC_TTS_ID);
                            }
                            RouteCalcPickerHandler.sLogger.w("new route set: " + routeInfo.routeRequest);
                        }
                    }, 20);
                }
            }
        }
        return true;
    }

    public boolean onItemSelected(int id, int pos, DestinationPickerState state) {
        return false;
    }

    public void onDestinationPickerClosed() {
    }
}
