package com.navdy.hud.app.maps;

import com.navdy.service.library.events.preferences.NavigationPreferences;

public class NavSessionPreferences {
    public boolean spokenTurnByTurn;

    public NavSessionPreferences(NavigationPreferences prefs) {
        setDefault(prefs);
    }

    public void setDefault(NavigationPreferences prefs) {
        this.spokenTurnByTurn = Boolean.TRUE.equals(prefs.spokenTurnByTurn);
    }
}
