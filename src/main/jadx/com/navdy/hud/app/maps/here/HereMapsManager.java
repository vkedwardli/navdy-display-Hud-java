package com.navdy.hud.app.maps.here;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.SystemClock;
import com.amazonaws.services.s3.internal.Constants;
import com.here.android.mpa.common.GeoCoordinate;
import com.here.android.mpa.common.GeoPosition;
import com.here.android.mpa.common.MapEngine;
import com.here.android.mpa.common.MatchedGeoPosition;
import com.here.android.mpa.common.OnEngineInitListener;
import com.here.android.mpa.common.OnEngineInitListener.Error;
import com.here.android.mpa.common.PositioningManager;
import com.here.android.mpa.common.PositioningManager.LocationMethod;
import com.here.android.mpa.common.PositioningManager.LocationStatus;
import com.here.android.mpa.common.PositioningManager.OnPositionChangedListener;
import com.here.android.mpa.common.Version;
import com.here.android.mpa.mapping.Map;
import com.here.android.mpa.mapping.Map.Animation;
import com.here.android.mpa.mapping.Map.LayerCategory;
import com.here.android.mpa.mapping.Map.Projection;
import com.here.android.mpa.mapping.MapRoute;
import com.here.android.mpa.mapping.MapTrafficLayer;
import com.here.android.mpa.mapping.MapTrafficLayer.RenderLayer;
import com.here.android.mpa.mapping.MapTransitLayer.Mode;
import com.here.android.mpa.mapping.MapView;
import com.here.android.mpa.mapping.TrafficEvent.Severity;
import com.here.android.mpa.odml.MapLoader;
import com.here.android.mpa.odml.MapLoader.Listener;
import com.here.android.mpa.odml.MapLoader.ResultCode;
import com.here.android.mpa.odml.MapPackage;
import com.here.android.mpa.odml.MapPackage.InstallationState;
import com.here.android.mpa.routing.RouteOptions;
import com.here.android.mpa.routing.RouteOptions.TransportMode;
import com.here.android.mpa.routing.RouteOptions.Type;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.analytics.AnalyticsSupport;
import com.navdy.hud.app.debug.DebugReceiver;
import com.navdy.hud.app.device.PowerManager;
import com.navdy.hud.app.device.gps.GpsConstants;
import com.navdy.hud.app.device.gps.GpsUtils;
import com.navdy.hud.app.event.Wakeup;
import com.navdy.hud.app.framework.DriverProfileHelper;
import com.navdy.hud.app.framework.glance.GlanceHelper;
import com.navdy.hud.app.framework.network.NetworkStateManager;
import com.navdy.hud.app.manager.SpeedManager;
import com.navdy.hud.app.maps.MapEvents.GPSSpeedEvent;
import com.navdy.hud.app.maps.MapEvents.MapEngineInitialize;
import com.navdy.hud.app.maps.MapSettings;
import com.navdy.hud.app.maps.MapsEventHandler;
import com.navdy.hud.app.maps.NavigationMode;
import com.navdy.hud.app.maps.here.HereMapController.State;
import com.navdy.hud.app.maps.notification.RouteCalculationEventHandler;
import com.navdy.hud.app.maps.notification.TrafficNotificationManager;
import com.navdy.hud.app.profile.DriverProfileManager;
import com.navdy.hud.app.settings.HUDSettings;
import com.navdy.hud.app.storage.PathManager;
import com.navdy.hud.app.ui.activity.Main;
import com.navdy.hud.app.ui.component.homescreen.NavigationView;
import com.navdy.hud.app.util.DeviceUtil;
import com.navdy.hud.app.util.os.SystemProperties;
import com.navdy.service.library.events.connection.LinkPropertiesChanged;
import com.navdy.service.library.events.preferences.NavigationPreferences;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.task.TaskManager;
import com.navdy.service.library.util.IOUtils;
import com.navdy.service.library.util.SystemUtils;
import com.nokia.maps.MapSettings.b;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;
import java.io.File;
import java.lang.ref.WeakReference;
import java.lang.reflect.Field;
import java.util.EnumSet;
import java.util.Iterator;
import java.util.List;
import javax.inject.Inject;
import mortar.Mortar;

public class HereMapsManager {
    public static final double DEFAULT_LATITUDE = 37.802086d;
    public static final double DEFAULT_LONGITUDE = -122.419015d;
    public static final float DEFAULT_TILT = 60.0f;
    public static final float DEFAULT_ZOOM_LEVEL = 16.5f;
    private static final String DISABLE_MAPS_PROPERTY = "persist.sys.map.disable";
    private static final boolean ENABLE_MAPS = (!SystemProperties.getBoolean(DISABLE_MAPS_PROPERTY, false));
    private static final String ENROUTE_MAP_SCHEME = "hybrid.night";
    private static final int GPS_SPEED_LAST_LOCATION_THRESHOLD = 2000;
    static final int MIN_SAME_POSITION_UPDATE_THRESHOLD = 500;
    private static final String MW_CONFIG_EXCEPTION_MSG = "Invalid configuration file. Check MWConfig!";
    private static final String NAVDY_HERE_MAP_SERVICE_NAME = "com.navdy.HereMapService";
    public static final float ROUTE_CALC_START_ZOOM_LEVEL = 15.5f;
    private static final String TRACKING_MAP_SCHEME = "carnav.night";
    private static final String TRAFFIC_REROUTE_PROPERTY = "persist.sys.hud_traffic_reroute";
    private static boolean recalcCurrentRouteForTraffic = false;
    private static final Logger sLogger = new Logger(HereMapsManager.class);
    private static final HereMapsManager singleton = new HereMapsManager();
    private final HandlerThread bkLocationReceiverHandlerThread;
    @Inject
    Bus bus;
    private Context context = HudApplication.getAppContext();
    private OnEngineInitListener engineInitListener = new OnEngineInitListener() {
        public void onEngineInitializationCompleted(Error error) {
            final long t1 = SystemClock.elapsedRealtime();
            HereMapsManager.sLogger.v("MAP-ENGINE-INIT took [" + (t1 - HereMapsManager.this.mapEngineStartTime) + "]");
            HereMapsManager.sLogger.v("MAP-ENGINE-INIT HERE-SDK version:" + Version.getSdkVersion());
            if (error == Error.NONE) {
                TaskManager.getInstance().execute(new Runnable() {
                    public void run() {
                        HereMapsManager.this.mapEngine.onResume();
                        HereMapsManager.sLogger.v(":initializing maps config");
                        HereMapsConfigurator.getInstance().updateMapsConfig();
                        HereMapsManager.sLogger.v("creating geopos");
                        GeoPosition pos = new GeoPosition(new GeoCoordinate(37.802086d, -122.419015d));
                        HereMapsManager.sLogger.v("created geopos");
                        if (!DeviceUtil.isUserBuild()) {
                            long l1 = SystemClock.elapsedRealtime();
                            HereLaneInfoBuilder.init();
                            HereMapsManager.sLogger.v("time to init HereLaneInfoBuilder [" + (SystemClock.elapsedRealtime() - l1) + "]");
                        }
                        HereMapsManager.sLogger.v("engine initialized refcount:" + HereMapsManager.this.mapEngine.getResourceReferenceCount());
                        try {
                            HereMapsManager.this.map = new Map();
                        } catch (final Throwable t) {
                            if (t.getMessage().contains(HereMapsManager.MW_CONFIG_EXCEPTION_MSG)) {
                                HereMapsManager.sLogger.e("MWConfig is corrupted! Cleaning up and restarting HUD app.");
                                HereMapsConfigurator.getInstance().removeMWConfigFolder();
                            }
                            HereMapsManager.this.handler.post(new Runnable() {
                                public void run() {
                                    throw new MWConfigCorruptException(t);
                                }
                            });
                        }
                        HereMapsManager.this.map.setProjectionMode(Projection.MERCATOR);
                        HereMapsManager.this.map.setTrafficInfoVisible(true);
                        HereMapsManager.this.mapController = new HereMapController(HereMapsManager.this.map, State.NONE);
                        HereMapsManager.this.setMapAttributes(new GeoCoordinate(37.802086d, -122.419015d));
                        HereMapsManager.sLogger.v("setting default map attributes");
                        HereMapsManager.this.setMapTraffic();
                        HereMapsManager.this.setMapPoiLayer(HereMapsManager.this.map);
                        HereMapsManager.this.positioningManager = PositioningManager.getInstance();
                        HereMapsManager.sLogger.v("MAP-ENGINE-INIT-2 took [" + (SystemClock.elapsedRealtime() - t1) + "]");
                        final long t2 = SystemClock.elapsedRealtime();
                        HereMapsManager.this.handler.post(new Runnable() {
                            public void run() {
                                HereMapsManager.this.positioningManager.start(LocationMethod.GPS_NETWORK);
                                HereMapsManager.sLogger.v("position manager started");
                                HereMapsManager.sLogger.v("MAP-ENGINE-INIT-3 took [" + (SystemClock.elapsedRealtime() - t2) + "]");
                                final long t3 = SystemClock.elapsedRealtime();
                                TaskManager.getInstance().execute(new Runnable() {
                                    public void run() {
                                        AnimationMode animationMode;
                                        HereNavigationManager hereNavigationManager = HereNavigationManager.getInstance();
                                        HereMapsManager.this.registerConnectivityReceiver();
                                        HereMapsManager.sLogger.v("map created");
                                        TrafficNotificationManager.getInstance();
                                        if (MapSettings.isCustomAnimationEnabled()) {
                                            animationMode = AnimationMode.ANIMATION;
                                        } else if (MapSettings.isFullCustomAnimatonEnabled()) {
                                            animationMode = AnimationMode.ZOOM_TILT_ANIMATION;
                                        } else {
                                            animationMode = AnimationMode.NONE;
                                        }
                                        HereMapsManager.sLogger.v("HereMapAnimator mode is [" + animationMode + "]");
                                        HereMapsManager.this.hereMapAnimator = new HereMapAnimator(animationMode, HereMapsManager.this.mapController, hereNavigationManager.getNavController());
                                        if (!HereMapsManager.this.initialMapRendering) {
                                            HereMapsManager.sLogger.v("HereMapAnimator MapRendering initial disabled");
                                            HereMapsManager.this.hereMapAnimator.stopMapRendering();
                                        }
                                        HereMapCameraManager.getInstance().initialize(HereMapsManager.this.mapController, HereMapsManager.this.bus, HereMapsManager.this.hereMapAnimator);
                                        HereMapsManager.this.hereLocationFixManager = new HereLocationFixManager(HereMapsManager.this.bus);
                                        HereMapsManager.sLogger.v("MAP-ENGINE-INIT-4 took [" + (SystemClock.elapsedRealtime() - t3) + "]");
                                        HereMapsManager.this.routeCalcEventHandler = new RouteCalculationEventHandler(HereMapsManager.this.bus);
                                        HereMapsManager.this.initMapLoader();
                                        HereMapsManager.this.offlineMapsVersion = new HereOfflineMapsVersion(HereMapsManager.this.getOfflineMapsData());
                                        synchronized (HereMapsManager.this.initLock) {
                                            HereMapsManager.this.mapInitializing = false;
                                            HereMapsManager.this.mapInitialized = true;
                                            HereMapsManager.this.bus.post(new MapEngineInitialize(HereMapsManager.this.mapInitialized));
                                            HereMapsManager.sLogger.v("MAP-ENGINE-INIT event sent");
                                        }
                                        HereMapsManager.sLogger.v("setEngineOnlineState initial");
                                        HereMapsManager.this.setEngineOnlineState();
                                        if (!HereMapsManager.this.powerManager.inQuietMode()) {
                                            HereMapsManager.this.startTrafficUpdater();
                                        }
                                    }
                                }, 3);
                            }
                        });
                    }
                }, 3);
                return;
            }
            String errString = error.toString();
            AnalyticsSupport.recordKeyFailure("HereMaps", errString);
            HereMapsManager.sLogger.e("MAP-ENGINE-INIT engine NOT initialized:" + error);
            HereMapsManager.this.mapError = error;
            synchronized (HereMapsManager.this.initLock) {
                HereMapsManager.this.mapInitializing = false;
                HereMapsManager.this.bus.post(new MapEngineInitialize(HereMapsManager.this.mapInitialized));
            }
            if (error == Error.USAGE_EXPIRED || error == Error.MISSING_APP_CREDENTIAL) {
                Main.handleLibraryInitializationError("Here Maps initialization failure: " + errString);
            }
        }
    };
    private volatile boolean extrapolationOn;
    private Handler handler = new Handler(Looper.getMainLooper());
    private HereLocationFixManager hereLocationFixManager;
    private HereMapAnimator hereMapAnimator;
    private Object initLock = new Object();
    private boolean initialMapRendering = true;
    private GeoPosition lastGeoPosition;
    private long lastGeoPositionTime;
    private OnSharedPreferenceChangeListener listener = new OnSharedPreferenceChangeListener() {
        public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
            HereMapsManager.sLogger.d("onSharedPreferenceChanged: " + key);
            if (HereMapsManager.this.isInitialized()) {
                int i = -1;
                switch (key.hashCode()) {
                    case -1094616929:
                        if (key.equals(HUDSettings.MAP_ANIMATION_MODE)) {
                            i = 3;
                            break;
                        }
                        break;
                    case -285821321:
                        if (key.equals(HUDSettings.MAP_SCHEME)) {
                            i = 2;
                            break;
                        }
                        break;
                    case 133816335:
                        if (key.equals(HUDSettings.MAP_TILT)) {
                            i = 0;
                            break;
                        }
                        break;
                    case 134000933:
                        if (key.equals(HUDSettings.MAP_ZOOM)) {
                            i = 1;
                            break;
                        }
                        break;
                }
                String val;
                switch (i) {
                    case 0:
                        val = sharedPreferences.getString(key, null);
                        if (val == null) {
                            HereMapsManager.sLogger.v("onSharedPreferenceChanged:no tilt");
                            return;
                        }
                        try {
                            float tilt = Float.parseFloat(val);
                            HereMapsManager.sLogger.w("onSharedPreferenceChanged:tilt:" + tilt);
                            HereMapsManager.this.mapController.setTilt(tilt);
                            return;
                        } catch (Throwable t) {
                            HereMapsManager.sLogger.e("onSharedPreferenceChanged:tilt", t);
                            return;
                        }
                    case 1:
                        val = sharedPreferences.getString(key, null);
                        if (val == null) {
                            HereMapsManager.sLogger.v("onSharedPreferenceChanged:no zoom");
                            return;
                        }
                        try {
                            float zoom = Float.parseFloat(val);
                            HereMapsManager.sLogger.w("onSharedPreferenceChanged:zoom:" + zoom);
                            HereMapsManager.this.mapController.setZoomLevel((double) zoom);
                            return;
                        } catch (Throwable t2) {
                            HereMapsManager.sLogger.e("onSharedPreferenceChanged:zoom", t2);
                            return;
                        }
                    case 2:
                        TaskManager.getInstance().execute(new Runnable() {
                            public void run() {
                                if (!HereMapsManager.this.mapController.getMapScheme().equals("hybrid.night")) {
                                    HereMapsManager.this.mapController.setMapScheme(HereMapsManager.this.getTrackingMapScheme());
                                }
                            }
                        }, 2);
                        return;
                    default:
                        return;
                }
            }
            HereMapsManager.sLogger.w("onSharedPreferenceChanged: map engine not intialized:" + key);
        }
    };
    private volatile boolean lowBandwidthDetected = false;
    @Inject
    DriverProfileManager mDriverProfileManager;
    private Map map;
    private HereMapController mapController;
    private boolean mapDataVerified;
    private MapEngine mapEngine;
    private long mapEngineStartTime;
    private Error mapError;
    private boolean mapInitLoaderComplete;
    private boolean mapInitialized;
    private boolean mapInitializing = true;
    private MapLoader mapLoader;
    private int mapPackageCount;
    private MapView mapView;
    private NavigationView navigationView;
    private HereOfflineMapsVersion offlineMapsVersion;
    private OnPositionChangedListener positionChangedListener = new OnPositionChangedListener() {
        public void onPositionUpdated(final LocationMethod locationMethod, final GeoPosition geoPosition, final boolean isMapMatched) {
            if (geoPosition != null) {
                TaskManager.getInstance().execute(new Runnable() {
                    public void run() {
                        try {
                            if (geoPosition instanceof MatchedGeoPosition) {
                                if (HereMapsManager.this.extrapolationOn) {
                                    if (HereMapUtil.isInTunnel(HereMapsManager.this.positioningManager.getRoadElement())) {
                                        HereMapsManager.this.sendExtrapolationEvent();
                                    } else {
                                        HereMapsManager.sLogger.i("TUNNEL extrapolation off");
                                        HereMapsManager.this.extrapolationOn = false;
                                        HereMapsManager.this.sendExtrapolationEvent();
                                    }
                                }
                                if (HereMapsManager.this.lastGeoPosition != null) {
                                    long t = SystemClock.elapsedRealtime() - HereMapsManager.this.lastGeoPositionTime;
                                    if (t < 500 && HereMapUtil.isCoordinateEqual(HereMapsManager.this.lastGeoPosition.getCoordinate(), geoPosition.getCoordinate())) {
                                        if (HereMapsManager.sLogger.isLoggable(2)) {
                                            HereMapsManager.sLogger.v("GEO-Here same pos as last:" + t);
                                            return;
                                        }
                                        return;
                                    }
                                }
                                HereMapsManager.this.lastGeoPosition = geoPosition;
                                HereMapsManager.this.lastGeoPositionTime = SystemClock.elapsedRealtime();
                                HereMapsManager.this.hereMapAnimator.setGeoPosition(geoPosition);
                                HereMapsManager.this.hereLocationFixManager.onHerePositionUpdated(locationMethod, geoPosition, isMapMatched);
                                HereMapCameraManager.getInstance().onGeoPositionChange(geoPosition);
                                if (HereMapsManager.this.speedManager.getObdSpeed() == -1 && SystemClock.elapsedRealtime() - HereMapsManager.this.hereLocationFixManager.getLastLocationTime() >= 2000 && (geoPosition instanceof MatchedGeoPosition) && ((MatchedGeoPosition) geoPosition).isExtrapolated() && HereMapsManager.this.speedManager.setGpsSpeed((float) geoPosition.getSpeed(), SystemClock.elapsedRealtimeNanos() / 1000000)) {
                                    HereMapsManager.this.bus.post(new GPSSpeedEvent());
                                }
                                HereMapsManager.this.bus.post(geoPosition);
                                if (HereMapsManager.sLogger.isLoggable(2)) {
                                    GeoCoordinate geoCoordinate = geoPosition.getCoordinate();
                                    HereMapsManager.sLogger.v("GEO-Here speed-mps[" + geoPosition.getSpeed() + "] " + "] lat=[" + geoCoordinate.getLatitude() + "] lon=[" + geoCoordinate.getLongitude() + "] provider=[" + locationMethod.name() + "] timestamp:[" + geoPosition.getTimestamp().getTime() + "]");
                                    return;
                                }
                                return;
                            }
                            GeoCoordinate coordinate = geoPosition.getCoordinate();
                            HereMapsManager.sLogger.v("position not map-matched lat:" + coordinate.getLatitude() + " lng:" + coordinate.getLongitude() + " speed:" + geoPosition.getSpeed());
                        } catch (Throwable t2) {
                            HereMapsManager.sLogger.e(t2);
                        }
                    }
                }, 18);
            }
        }

        public void onPositionFixChanged(final LocationMethod locationMethod, final LocationStatus locationStatus) {
            TaskManager.getInstance().execute(new Runnable() {
                public void run() {
                    try {
                        if (HereMapsManager.sLogger.isLoggable(2)) {
                            HereMapsManager.sLogger.d("onPositionFixChanged: method:" + locationMethod + " status: " + locationStatus);
                        }
                        if (locationMethod == LocationMethod.GPS && HereMapUtil.isInTunnel(HereMapsManager.this.positioningManager.getRoadElement()) && !HereMapsManager.this.extrapolationOn) {
                            HereMapsManager.sLogger.d("TUNNEL extrapolation on");
                            HereMapsManager.this.extrapolationOn = true;
                            HereMapsManager.this.sendExtrapolationEvent();
                        }
                    } catch (Throwable t) {
                        HereMapsManager.sLogger.e(t);
                    }
                }
            }, 18);
        }
    };
    private PositioningManager positioningManager;
    private boolean positioningManagerInstalled;
    @Inject
    PowerManager powerManager;
    private RouteCalculationEventHandler routeCalcEventHandler;
    private GeoCoordinate routeStartPoint;
    @Inject
    SharedPreferences sharedPreferences;
    private SpeedManager speedManager;
    private volatile boolean turnEngineOn = false;
    private boolean voiceSkinsLoaded;

    public static class MWConfigCorruptException extends RuntimeException {
        public MWConfigCorruptException(Throwable t) {
            super(t);
        }
    }

    public static HereMapsManager getInstance() {
        return singleton;
    }

    private void startTrafficUpdater() {
        try {
            HereNavigationManager.getInstance().startTrafficUpdater();
        } catch (Throwable t) {
            sLogger.e(t);
        }
    }

    public HereOfflineMapsVersion getOfflineMapsVersion() {
        return this.offlineMapsVersion;
    }

    private void initMapLoader() {
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                long l1 = SystemClock.elapsedRealtime();
                HereMapsManager.this.mapLoader = MapLoader.getInstance();
                HereMapsManager.sLogger.v("initMapLoader took [" + (SystemClock.elapsedRealtime() - l1) + "]");
                HereMapsManager.this.mapLoader.addListener(new Listener() {
                    public void onProgress(int progressPercentage) {
                        HereMapsManager.sLogger.v("MapLoader: progress[" + progressPercentage + "]");
                    }

                    public void onInstallationSize(long diskSize, long networkSize) {
                        HereMapsManager.sLogger.v("MapLoader: onInstallationSize disk[" + diskSize + "] network[" + networkSize + "]");
                    }

                    public void onGetMapPackagesComplete(final MapPackage mapPackage, ResultCode resultCode) {
                        if (resultCode != ResultCode.OPERATION_SUCCESSFUL) {
                            HereMapsManager.sLogger.e("initMapLoader: get map package failed:" + resultCode.name());
                            return;
                        }
                        HereMapsManager.sLogger.v("initMapLoader: get map package success");
                        TaskManager.getInstance().execute(new Runnable() {
                            public void run() {
                                HereMapsManager.this.printMapPackages(mapPackage, EnumSet.of(InstallationState.INSTALLED, InstallationState.PARTIALLY_INSTALLED));
                                HereMapsManager.sLogger.v("initMapLoader: map package count:" + HereMapsManager.this.mapPackageCount);
                                HereMapsManager.this.mapDataVerified = true;
                            }
                        }, 2);
                    }

                    public void onCheckForUpdateComplete(boolean updateAvailable, String currentMapVersion, String newestMapVersion, ResultCode mapLoaderResultCode) {
                        HereMapsManager.sLogger.v("MapLoader: updateAvailable[" + updateAvailable + "] currentMapVersion[" + currentMapVersion + "] newestMapVersion[" + newestMapVersion + "] resultCode[" + mapLoaderResultCode + "]");
                    }

                    public void onPerformMapDataUpdateComplete(MapPackage mapPackage, ResultCode resultCode) {
                        HereMapsManager.sLogger.v("MapLoader: onPerformMapDataUpdateComplete result[" + resultCode.name() + "] package[" + (mapPackage != null ? mapPackage.getTitle() : Constants.NULL_VERSION_ID) + "]");
                    }

                    public void onInstallMapPackagesComplete(MapPackage mapPackage, ResultCode resultCode) {
                        HereMapsManager.sLogger.v("MapLoader: onInstallMapPackagesComplete result[" + resultCode.name() + "] package[" + (mapPackage != null ? mapPackage.getTitle() : Constants.NULL_VERSION_ID) + "]");
                    }

                    public void onUninstallMapPackagesComplete(MapPackage mapPackage, ResultCode resultCode) {
                    }
                });
                HereMapsManager.this.mapPackageCount = 0;
                HereMapsManager.this.invokeMapLoader();
            }
        }, 2);
    }

    private void invokeMapLoader() {
        if (this.mapLoader == null) {
            sLogger.v("invokeMapLoader: no maploader");
        } else if (!SystemUtils.isConnectedToNetwork(HudApplication.getAppContext())) {
            sLogger.v("invokeMapLoader: not connected to n/w");
        } else if (this.mapInitLoaderComplete) {
            sLogger.v("invokeMapLoader: already complete");
        } else {
            this.mapInitLoaderComplete = true;
            sLogger.v("initMapLoader called getMapPackages:" + this.mapLoader.getMapPackages());
        }
    }

    public synchronized void installPositionListener() {
        if (!this.positioningManagerInstalled) {
            this.positioningManager.addListener(new WeakReference(this.positionChangedListener));
            sLogger.v("position manager listener installed");
            this.positioningManagerInstalled = true;
        }
    }

    private HereMapsManager() {
        Mortar.inject(this.context, this);
        this.speedManager = SpeedManager.getInstance();
        if (ENABLE_MAPS) {
            checkforNetworkProvider();
        }
        this.bkLocationReceiverHandlerThread = new HandlerThread("here_bk_location");
        this.bkLocationReceiverHandlerThread.start();
        this.bus.register(this);
    }

    private void checkforNetworkProvider() {
        this.handler.post(new Runnable() {
            /* JADX WARNING: inconsistent code. */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public void run() {
                boolean checkAgain = false;
                try {
                    if (((LocationManager) HudApplication.getAppContext().getSystemService("location")).getProvider("network") == null) {
                        HereMapsManager.sLogger.v("n/w provider not found yet, check again");
                        checkAgain = true;
                    } else {
                        HereMapsManager.sLogger.v("n/w provider found, initialize here");
                        TaskManager.getInstance().execute(new Runnable() {
                            public void run() {
                                HereMapsManager.this.initialize();
                            }
                        }, 2);
                    }
                    if (checkAgain) {
                        HereMapsManager.this.handler.postDelayed(this, 1000);
                    }
                } catch (Throwable th) {
                    if (null != null) {
                        HereMapsManager.this.handler.postDelayed(this, 1000);
                    }
                }
            }
        });
    }

    private void initialize() {
        sLogger.v(":initializing voice skins");
        VoiceSkinsConfigurator.updateVoiceSkins();
        sLogger.v(":initializing event handlers");
        MapsEventHandler.getInstance();
        if (MapSettings.isDebugHereLocation()) {
            DebugReceiver.setHereDebugLocation(true);
        }
        Map.setMaximumFps(MapSettings.getMapFps());
        Map.enableMaximumFpsLimit(true);
        sLogger.v("map-fps [" + Map.getMaximumFps() + "] enabled[" + Map.isMaximumFpsLimited() + "]");
        try {
            Field f = com.nokia.maps.MapSettings.class.getDeclaredField("g");
            f.setAccessible(true);
            sLogger.e("enable worker thread before is " + f.get(null));
            f.set(null, b.EWorkerThread);
            sLogger.e("enable worker thread after is " + f.get(null));
        } catch (Throwable t) {
            sLogger.e("enable worker thread", t);
        }
        this.mapEngine = MapEngine.getInstance();
        sLogger.v("calling maps engine init");
        this.sharedPreferences.registerOnSharedPreferenceChangeListener(this.listener);
        recalcCurrentRouteForTraffic = SystemProperties.getBoolean(TRAFFIC_REROUTE_PROPERTY, false);
        sLogger.v("persist.sys.hud_traffic_reroute=" + recalcCurrentRouteForTraffic);
        String mapsPartition = PathManager.getInstance().getMapsPartitionPath();
        if (!mapsPartition.isEmpty()) {
            try {
                if (!com.here.android.mpa.common.MapSettings.setIsolatedDiskCacheRootPath(mapsPartition + File.separator + ".here-maps", NAVDY_HERE_MAP_SERVICE_NAME)) {
                    sLogger.e("setIsolatedDiskCacheRootPath() failed");
                }
            } catch (Exception e) {
                sLogger.e("exception in setIsolatedDiskCacheRootPath()", e);
            }
        }
        this.mapEngineStartTime = SystemClock.elapsedRealtime();
        this.mapEngine.init(this.context, this.engineInitListener);
    }

    public boolean isInitialized() {
        boolean z;
        synchronized (this.initLock) {
            z = this.mapInitialized;
        }
        return z;
    }

    public boolean isInitializing() {
        boolean z;
        synchronized (this.initLock) {
            z = this.mapInitializing;
        }
        return z;
    }

    public Error getError() {
        return this.mapError;
    }

    public boolean isRenderingAllowed() {
        return !this.powerManager.inQuietMode();
    }

    public HereMapController getMapController() {
        return this.mapController;
    }

    public MapView getMapView() {
        return this.mapView;
    }

    public void setMapView(MapView mapView, NavigationView navigationView) {
        this.mapView = mapView;
        this.navigationView = navigationView;
        HereNavigationManager.getInstance().setMapView(mapView, navigationView);
        AnimationMode mode = this.hereMapAnimator.getAnimationMode();
        if (mode != AnimationMode.NONE) {
            sLogger.v("installing map render listener:" + mode);
            this.mapView.addOnMapRenderListener(this.hereMapAnimator.getMapRenderListener());
            return;
        }
        sLogger.v("not installing map render listener:" + mode);
    }

    public RouteOptions getRouteOptions() {
        RouteOptions routeOptions = new RouteOptions();
        routeOptions.setTransportMode(TransportMode.CAR);
        NavigationPreferences preferences = this.mDriverProfileManager.getCurrentProfile().getNavigationPreferences();
        switch (preferences.routingType) {
            case ROUTING_FASTEST:
                routeOptions.setRouteType(Type.FASTEST);
                break;
            case ROUTING_SHORTEST:
                routeOptions.setRouteType(Type.SHORTEST);
                break;
        }
        routeOptions.setHighwaysAllowed(Boolean.TRUE.equals(preferences.allowHighways));
        routeOptions.setTollRoadsAllowed(Boolean.TRUE.equals(preferences.allowTollRoads));
        routeOptions.setFerriesAllowed(false);
        routeOptions.setTunnelsAllowed(Boolean.TRUE.equals(preferences.allowTunnels));
        routeOptions.setDirtRoadsAllowed(Boolean.TRUE.equals(preferences.allowUnpavedRoads));
        routeOptions.setCarShuttleTrainsAllowed(Boolean.TRUE.equals(preferences.allowAutoTrains));
        routeOptions.setCarpoolAllowed(false);
        return routeOptions;
    }

    public GeoPosition getLastGeoPosition() {
        return this.lastGeoPosition;
    }

    String getTrackingMapScheme() {
        return "carnav.night";
    }

    String getEnrouteMapScheme() {
        return "hybrid.night";
    }

    private double getDefaultZoomLevel() {
        return 16.5d;
    }

    private float getDefaultTiltLevel() {
        return 60.0f;
    }

    private void setMapAttributes(GeoCoordinate geoCoordinate) {
        this.map.setExtrudedBuildingsVisible(false);
        this.map.setLandmarksVisible(false);
        this.map.setStreetLevelCoverageVisible(false);
        this.map.setMapScheme(getTrackingMapScheme());
        if (geoCoordinate != null) {
            this.map.setCenter(geoCoordinate, Animation.NONE, getDefaultZoomLevel(), -1.0f, getDefaultTiltLevel());
            sLogger.v("setcenterDefault:" + geoCoordinate);
            return;
        }
        sLogger.v("geoCoordinate not available");
    }

    public void setTrafficOverlay(NavigationMode navigationMode) {
        if (navigationMode == NavigationMode.MAP) {
            setMapTraffic();
        } else if (navigationMode == NavigationMode.MAP_ON_ROUTE) {
            setMapOnRouteTraffic();
        }
    }

    public void clearTrafficOverlay() {
        clearMapTraffic();
    }

    public PositioningManager getPositioningManager() {
        return this.positioningManager;
    }

    private void setEngineOnlineState() {
        boolean online = NetworkStateManager.isConnectedToNetwork(this.context);
        this.lowBandwidthDetected = false;
        sLogger.i("setEngineOnlineState:setting maps engine online state:" + online);
        if (online) {
            TaskManager.getInstance().execute(new Runnable() {
                public void run() {
                    HereMapsManager.this.turnOnline();
                }
            }, 1);
        } else {
            TaskManager.getInstance().execute(new Runnable() {
                public void run() {
                    HereMapsManager.this.turnOffline();
                }
            }, 1);
        }
    }

    private void setMapPoiLayer(Map map) {
        map.setCartoMarkersVisible(false);
        map.getMapTransitLayer().setMode(Mode.NOTHING);
        map.setVisibleLayers(EnumSet.of(LayerCategory.ICON_PUBLIC_TRANSIT_STATION, new LayerCategory[]{LayerCategory.PUBLIC_TRANSIT_LINE, LayerCategory.LABEL_PUBLIC_TRANSIT_STATION, LayerCategory.LABEL_PUBLIC_TRANSIT_LINE, LayerCategory.BEACH, LayerCategory.WOODLAND, LayerCategory.DESERT, LayerCategory.GLACIER, LayerCategory.AMUSEMENT_PARK, LayerCategory.ANIMAL_PARK, LayerCategory.BUILTUP, LayerCategory.CEMETERY, LayerCategory.BUILDING, LayerCategory.NEIGHBORHOOD_AREA, LayerCategory.NATIONAL_PARK, LayerCategory.NATIVE_RESERVATION}), false);
        EnumSet<LayerCategory> visibleLayers = map.getVisibleLayers();
        sLogger.v("==== visible layers == [" + visibleLayers.size() + "]");
        StringBuilder layers = new StringBuilder();
        boolean first = true;
        Iterator it = visibleLayers.iterator();
        while (it.hasNext()) {
            LayerCategory layerCategory = (LayerCategory) it.next();
            if (!first) {
                layers.append(", ");
            }
            layers.append(layerCategory.name());
            first = false;
        }
        sLogger.v(layers.toString());
    }

    private void setMapTraffic() {
        this.mapController.execute(new Runnable() {
            public void run() {
                MapTrafficLayer trafficLayer = HereMapsManager.this.map.getMapTrafficLayer();
                trafficLayer.setDisplayFilter(Severity.NORMAL);
                trafficLayer.setEnabled(RenderLayer.FLOW, true);
                trafficLayer.setEnabled(RenderLayer.INCIDENT, true);
                trafficLayer.setEnabled(RenderLayer.ONROUTE, true);
                HereMapsManager.this.map.setTrafficInfoVisible(true);
            }
        });
    }

    private void setMapOnRouteTraffic() {
        this.mapController.execute(new Runnable() {
            public void run() {
                MapTrafficLayer trafficLayer = HereMapsManager.this.map.getMapTrafficLayer();
                trafficLayer.setDisplayFilter(Severity.NORMAL);
                trafficLayer.setEnabled(RenderLayer.ONROUTE, true);
                trafficLayer.setEnabled(RenderLayer.FLOW, true);
                trafficLayer.setEnabled(RenderLayer.INCIDENT, true);
                HereMapsManager.this.map.setTrafficInfoVisible(true);
            }
        });
    }

    void clearMapTraffic() {
        this.mapController.execute(new Runnable() {
            public void run() {
                HereMapsManager.this.map.setTrafficInfoVisible(false);
                MapTrafficLayer trafficLayer = HereMapsManager.this.map.getMapTrafficLayer();
                trafficLayer.setDisplayFilter(Severity.NORMAL);
                trafficLayer.setEnabled(RenderLayer.ONROUTE, false);
                trafficLayer.setEnabled(RenderLayer.FLOW, false);
                trafficLayer.setEnabled(RenderLayer.INCIDENT, false);
            }
        });
    }

    private boolean isTrafficOverlayVisible() {
        return isTrafficOverlayEnabled(HereNavigationManager.getInstance().getCurrentNavigationMode());
    }

    private boolean isTrafficOverlayEnabled(NavigationMode navigationMode) {
        MapTrafficLayer trafficLayer = this.map.getMapTrafficLayer();
        if (navigationMode == NavigationMode.MAP) {
            if (trafficLayer.isEnabled(RenderLayer.FLOW) && trafficLayer.isEnabled(RenderLayer.INCIDENT)) {
                return true;
            }
            return false;
        } else if (navigationMode == NavigationMode.MAP_ON_ROUTE) {
            return trafficLayer.isEnabled(RenderLayer.ONROUTE);
        } else {
            return false;
        }
    }

    public boolean isEngineOnline() {
        return isInitialized() && this.mapEngine != null && MapEngine.isOnlineEnabled();
    }

    private void registerConnectivityReceiver() {
        IntentFilter filter = new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE");
        this.context.registerReceiver(new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
                if ("android.net.conn.CONNECTIVITY_CHANGE".equals(intent.getAction())) {
                    HereMapsManager.sLogger.v("setEngineOnlineState n/w state change");
                    HereMapsManager.this.setEngineOnlineState();
                }
            }
        }, filter);
    }

    public HereLocationFixManager getLocationFixManager() {
        return this.hereLocationFixManager;
    }

    public RouteCalculationEventHandler getRouteCalcEventHandler() {
        return this.routeCalcEventHandler;
    }

    public synchronized void turnOnline() {
        if (isInitialized()) {
            sLogger.i("turnOnline: enabling traffic info");
            requestMapsEngineOnlineStateChange(true);
            if (GlanceHelper.isTrafficNotificationEnabled()) {
                sLogger.i("turnOnline: enabling traffic notifications");
                HereNavigationManager.getInstance().startTrafficRerouteListener();
            } else {
                sLogger.i("turnOnline: not enabling traffic notifications, glance off");
            }
            sLogger.v("turnOnline invokeMapLoader");
            invokeMapLoader();
            sLogger.v("isOnline:" + MapEngine.isOnlineEnabled());
        } else {
            sLogger.i("turnOnline: engine not yet initialized");
        }
    }

    public synchronized void turnOffline() {
        if (isInitialized()) {
            requestMapsEngineOnlineStateChange(false);
            HereNavigationManager.getInstance().stopTrafficRerouteListener();
            sLogger.v("isOnline:" + MapEngine.isOnlineEnabled());
        } else {
            sLogger.i("turnOffline: engine not yet initialized");
        }
    }

    public void requestMapsEngineOnlineStateChange(boolean turnOn) {
        sLogger.d("Request to set online state " + turnOn);
        this.turnEngineOn = turnOn;
        updateEngineOnlineState();
    }

    public synchronized void updateEngineOnlineState() {
        boolean z = true;
        synchronized (this) {
            sLogger.d("Updating the engine online state LowBandwidthDetected :" + this.lowBandwidthDetected + ", TurnEngineOn :" + this.turnEngineOn);
            if (isInitialized()) {
                boolean z2;
                Logger logger = sLogger;
                StringBuilder append = new StringBuilder().append("Turning engine on :");
                if (this.lowBandwidthDetected || !this.turnEngineOn) {
                    z2 = false;
                } else {
                    z2 = true;
                }
                logger.d(append.append(z2).toString());
                if (this.lowBandwidthDetected || !this.turnEngineOn) {
                    z = false;
                }
                MapEngine.setOnline(z);
            }
        }
    }

    @Subscribe
    public void onWakeup(Wakeup event) {
        if (isInitialized()) {
            startTrafficUpdater();
        }
    }

    @Subscribe
    public void onLinkPropertiesChanged(LinkPropertiesChanged propertiesChanged) {
        if (propertiesChanged != null && propertiesChanged.bandwidthLevel != null) {
            sLogger.d("Link Properties changed");
            if (propertiesChanged.bandwidthLevel.intValue() <= 0) {
                sLogger.d("Switching to low bandwidth mode");
                this.lowBandwidthDetected = true;
                updateEngineOnlineState();
                return;
            }
            sLogger.d("Switching back to normal bandwidth mode");
            this.lowBandwidthDetected = false;
            updateEngineOnlineState();
        }
    }

    public GeoCoordinate getRouteStartPoint() {
        return this.routeStartPoint;
    }

    public void setRouteStartPoint(GeoCoordinate geoCoordinate) {
        this.routeStartPoint = geoCoordinate;
    }

    public boolean isRecalcCurrentRouteForTrafficEnabled() {
        return recalcCurrentRouteForTraffic;
    }

    private void sendExtrapolationEvent() {
        Bundle bundle = new Bundle();
        bundle.putBoolean(GpsConstants.EXTRAPOLATION_FLAG, this.extrapolationOn);
        GpsUtils.sendEventBroadcast(GpsConstants.EXTRAPOLATION, bundle);
    }

    public void postManeuverDisplay() {
        if (isInitialized()) {
            HereNavigationManager.getInstance().postManeuverDisplay();
        }
    }

    public Looper getBkLocationReceiverLooper() {
        return this.bkLocationReceiverHandlerThread.getLooper();
    }

    public String getOfflineMapsData() {
        try {
            File dir = new File(PathManager.getInstance().getHereMapsDataDirectory());
            if (!dir.exists()) {
                return null;
            }
            File metaData = new File(dir, PathManager.HERE_MAP_META_JSON_FILE);
            if (metaData.exists()) {
                return IOUtils.convertFileToString(metaData.getAbsolutePath());
            }
            return null;
        } catch (Throwable t) {
            sLogger.e(t);
            return null;
        }
    }

    public void startMapRendering() {
        if (this.hereMapAnimator != null) {
            this.hereMapAnimator.startMapRendering();
        } else {
            this.initialMapRendering = true;
        }
    }

    public void stopMapRendering() {
        if (this.hereMapAnimator != null) {
            this.hereMapAnimator.stopMapRendering();
        } else {
            this.initialMapRendering = false;
        }
    }

    public void hideTraffic() {
        if (!isInitialized()) {
            return;
        }
        if (DriverProfileHelper.getInstance().getDriverProfileManager().isTrafficEnabled()) {
            TaskManager.getInstance().execute(new Runnable() {
                public void run() {
                    HereMapsManager.this.mapController.setTrafficInfoVisible(false);
                    HereMapsManager.sLogger.v("hidetraffic: map off");
                    MapRoute mapRoute = HereNavigationManager.getInstance().getCurrentMapRoute();
                    if (mapRoute != null) {
                        mapRoute.setTrafficEnabled(false);
                        HereMapsManager.sLogger.v("hidetraffic: route off");
                    }
                    HereMapsManager.sLogger.v("hidetraffic: traffic is disabled");
                }
            }, 3);
        } else {
            sLogger.v("hidetraffic: traffic is not enabled");
        }
    }

    public void showTraffic() {
        if (!isInitialized()) {
            return;
        }
        if (DriverProfileHelper.getInstance().getDriverProfileManager().isTrafficEnabled()) {
            TaskManager.getInstance().execute(new Runnable() {
                public void run() {
                    HereNavigationManager.getInstance().setTrafficToMode();
                    HereMapsManager.sLogger.v("showTraffic: traffic is enabled");
                }
            }, 3);
        } else {
            sLogger.v("showTraffic: traffic is not enabled");
        }
    }

    public HereMapAnimator getMapAnimator() {
        return this.hereMapAnimator;
    }

    public void initNavigation() {
        if (isInitialized()) {
            HereNavigationManager.getInstance().getNavController().initialize();
        }
    }

    public void handleBandwidthPreferenceChange() {
        if (isInitialized()) {
            HereNavigationManager.getInstance().setBandwidthPreferences();
        }
    }

    public boolean isNavigationModeOn() {
        if (isInitialized()) {
            return HereNavigationManager.getInstance().isNavigationModeOn();
        }
        return false;
    }

    public boolean isNavigationPossible() {
        if (!isInitialized() || this.hereLocationFixManager.getLastGeoCoordinate() == null) {
            return false;
        }
        return true;
    }

    public String getVersion() {
        if (isInitialized()) {
            return Version.getSdkVersion();
        }
        return null;
    }

    public boolean isVoiceSkinsLoaded() {
        return this.voiceSkinsLoaded;
    }

    public void setVoiceSkinsLoaded() {
        this.voiceSkinsLoaded = true;
    }

    public boolean isMapDataVerified() {
        return this.mapDataVerified;
    }

    public int getMapPackageCount() {
        return this.mapPackageCount;
    }

    private void printMapPackages(MapPackage mapPackage, EnumSet<InstallationState> state) {
        if (mapPackage != null) {
            if (state.contains(mapPackage.getInstallationState())) {
                this.mapPackageCount++;
            }
            List<MapPackage> children = mapPackage.getChildren();
            if (children != null) {
                for (MapPackage child : children) {
                    printMapPackages(child, state);
                }
            }
        }
    }

    public void checkForMapDataUpdate() {
        try {
            if (!isInitialized()) {
                sLogger.v("checkForMapDataUpdate MapLoader: engine not initialized");
            } else if (this.mapLoader == null) {
                sLogger.v("checkForMapDataUpdate MapLoader: not available");
            } else if (this.mapInitLoaderComplete) {
                sLogger.v("checkForMapDataUpdate MapLoader: called checkForMapDataUpdate:" + this.mapLoader.checkForMapDataUpdate());
            } else {
                sLogger.v("checkForMapDataUpdate MapLoader: loader not initialized");
            }
        } catch (Throwable t) {
            sLogger.e(t);
        }
    }
}
