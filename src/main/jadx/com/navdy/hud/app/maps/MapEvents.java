package com.navdy.hud.app.maps;

import android.graphics.drawable.Drawable;
import com.here.android.mpa.common.Image;
import com.here.android.mpa.routing.Maneuver;
import com.here.android.mpa.routing.RouteOptions;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.R;
import com.navdy.hud.app.framework.destinations.Destination;
import com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.ManeuverState;
import com.navdy.service.library.events.navigation.DistanceUnit;
import com.navdy.service.library.events.navigation.NavigationRouteRequest;
import com.navdy.service.library.events.navigation.NavigationRouteResponse;
import com.navdy.service.library.events.navigation.NavigationSessionRouteChange.RerouteReason;
import com.navdy.service.library.events.navigation.NavigationTurn;
import com.navdy.service.library.log.Logger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MapEvents {
    private static final Logger sLogger = new Logger(MapEvents.class);

    public static class ArrivalEvent {
    }

    public enum DestinationDirection {
        LEFT,
        RIGHT,
        UNKNOWN
    }

    public static class DialMapZoom {
        public final Type type;

        public enum Type {
            IN,
            OUT
        }

        public DialMapZoom(Type type) {
            this.type = type;
        }
    }

    public static class DisplayJunction {
        public Image junction;
        public Image signpost;

        public DisplayJunction(Image image1, Image image2) {
            this.junction = image1;
            this.signpost = image2;
        }
    }

    public static class DisplayTrafficIncident {
        public static final String ACCIDENT = "ACCIDENT";
        public static final String CLOSURE = "CLOSURE";
        public static final String CONGESTION = "CONGESTION";
        public static final String FLOW = "FLOW";
        public static final String OTHER = "OTHER";
        public static final String ROADWORKS = "ROADWORKS";
        public static final String UNDEFINED = "UNDEFINED";
        public String affectedStreet;
        public Category category;
        public String description;
        public long distanceToIncident;
        public Image icon;
        public Date reported;
        public String title;
        public Type type;
        public Date updated;

        public enum Category {
            ACCIDENT,
            CLOSURE,
            ROADWORKS,
            CONGESTION,
            FLOW,
            UNDEFINED,
            OTHER
        }

        public enum Type {
            BLOCKING,
            VERY_HIGH,
            HIGH,
            NORMAL,
            FAILED,
            INACTIVE
        }

        public DisplayTrafficIncident(Type type, Category category) {
            this.type = type;
            this.category = category;
        }

        public DisplayTrafficIncident(Type type, Category category, String title, String description, String affectedStreet, long distanceToIncident, Date reported, Date updated, Image icon) {
            this.type = type;
            this.category = category;
            this.title = title;
            this.description = description;
            this.affectedStreet = affectedStreet;
            this.distanceToIncident = distanceToIncident;
            this.reported = reported;
            this.updated = updated;
            this.icon = icon;
        }
    }

    public static class DisplayTrafficLaneInfo {
        public ArrayList<LaneData> laneData;

        public DisplayTrafficLaneInfo(ArrayList<LaneData> laneData) {
            this.laneData = laneData;
        }
    }

    public static class GPSSpeedEvent {
    }

    public static class GpsStatusChange {
        public boolean connected;

        public GpsStatusChange(boolean connected) {
            this.connected = connected;
        }
    }

    public static class HideSignPostJunction {
    }

    public static class HideTrafficLaneInfo {
    }

    public static class LaneData {
        public Drawable[] icons;
        public Position position;

        public enum Position {
            ON_ROUTE,
            OFF_ROUTE
        }

        public LaneData(Position position, Drawable[] icons) {
            this.position = position;
            this.icons = icons;
        }
    }

    public static class LiveTrafficDismissEvent {
    }

    public static class LiveTrafficEvent {
        public final Severity severity;
        public final Type type;

        public enum Severity {
            HIGH(0),
            VERY_HIGH(1);
            
            public final int value;

            private Severity(int value) {
                this.value = value;
            }
        }

        public enum Type {
            CONGESTION,
            INCIDENT
        }

        public LiveTrafficEvent(Type type, Severity severity) {
            this.type = type;
            this.severity = severity;
        }
    }

    public static class LocationFix {
        public boolean locationAvailable;
        public boolean usingLocalGpsLocation;
        public boolean usingPhoneLocation;

        public LocationFix(boolean locationAvailable, boolean usingPhoneLocation, boolean usingLocalGpsLocation) {
            this.locationAvailable = locationAvailable;
            this.usingPhoneLocation = usingPhoneLocation;
            this.usingLocalGpsLocation = usingLocalGpsLocation;
        }
    }

    public static class ManeuverDisplay {
        static final String NOT_AVAILABLE = "N/A";
        public String currentRoad;
        public float currentSpeedLimit;
        public int destinationIconId;
        public DestinationDirection direction;
        public float distance;
        public long distanceInMeters;
        public String distanceToPendingRoadText;
        public DistanceUnit distanceUnit;
        public boolean empty;
        public String eta;
        public String etaAmPm;
        public Date etaDate;
        public String maneuverId;
        public ManeuverState maneuverState;
        public NavigationTurn navigationTurn;
        public int nextTurnIconId = -1;
        public String pendingRoad;
        public String pendingTurn;
        public float totalDistance;
        public float totalDistanceRemaining;
        public DistanceUnit totalDistanceRemainingUnit;
        public DistanceUnit totalDistanceUnit;
        public int turnIconId = -1;
        public int turnIconNowId;
        public int turnIconSoonId;

        public String toString() {
            String str;
            String resourceName = NOT_AVAILABLE;
            if (this.turnIconId != -1) {
                try {
                    resourceName = HudApplication.getAppContext().getResources().getResourceName(this.turnIconId);
                } catch (Throwable t) {
                    MapEvents.sLogger.e(t);
                }
            }
            StringBuilder append = new StringBuilder().append("TurnIcon [").append(resourceName).append("] ").append("TurnIcon [").append(this.turnIconId).append("] ").append("PendingTurn[").append(this.pendingTurn).append("] ").append("PendingRoad[").append(this.pendingRoad).append("] ").append("Distanceleft[").append(this.distanceToPendingRoadText).append("] ").append("DistanceleftInMeters[").append(this.distanceInMeters).append("] ").append("CurrentRoad[").append(this.currentRoad).append("] ").append("eta[").append(this.eta).append("] ").append("etaUtc[").append(this.etaDate).append("] ").append("currentSpeedLimit[").append(this.currentSpeedLimit).append("] ").append("earlyManeuver[");
            if (this.nextTurnIconId == -1) {
                str = "not_available] ";
            } else {
                str = "available] ";
            }
            return append.append(str).append("destinationDirection[").append(this.direction).append("] ").append("totalDistance [").append(this.totalDistance).append(" ").append(this.totalDistanceUnit).append("] ").append("totalDistanceRemain [").append(this.totalDistanceRemaining).append(" ").append(this.totalDistanceRemainingUnit).append("] ").append("empty[").append(this.empty).append("] ").append("navigating[").append(isNavigating()).append("] ").append("arrived [").append(isArrived()).append("] ").toString();
        }

        public boolean isEmpty() {
            return this.empty;
        }

        public boolean isArrived() {
            return this.turnIconId == R.drawable.icon_tbt_arrive;
        }

        public boolean isNavigating() {
            return this.turnIconId != -1;
        }
    }

    public static class ManeuverEvent {
        public Maneuver maneuver;
        public Type type;

        public enum Type {
            FIRST,
            LAST,
            INTERMEDIATE
        }

        public ManeuverEvent(Type type, Maneuver maneuver) {
            this.type = type;
            this.maneuver = maneuver;
        }
    }

    public static class ManeuverSoonEvent {
        public final Maneuver maneuver;

        public ManeuverSoonEvent(Maneuver maneuver) {
            this.maneuver = maneuver;
        }
    }

    public static class MapEngineInitialize {
        public boolean initialized;

        public MapEngineInitialize(boolean initialized) {
            this.initialized = initialized;
        }
    }

    public static class MapEngineReady {
    }

    public static class MapUIReady {
    }

    public static class NavigationModeChange {
        public NavigationMode navigationMode;

        public NavigationModeChange(NavigationMode navigationMode) {
            this.navigationMode = navigationMode;
        }
    }

    public static class NewRouteAdded {
        public final RerouteReason rerouteReason;

        public NewRouteAdded(RerouteReason rerouteReason) {
            this.rerouteReason = rerouteReason;
        }
    }

    public static class RegionEvent {
        public final String country;
        public final String state;

        public RegionEvent(String state, String country) {
            this.state = state;
            this.country = country;
        }

        public boolean equals(Object o) {
            if (o == this) {
                return true;
            }
            if (!(o instanceof RegionEvent)) {
                return false;
            }
            RegionEvent r = (RegionEvent) o;
            boolean stateEquals = this.state == null ? r.state == null : this.state.equals(r.state);
            if (stateEquals) {
                if (this.country == null) {
                    if (r.country == null) {
                        return true;
                    }
                } else if (this.country.equals(r.country)) {
                    return true;
                }
            }
            return false;
        }

        public int hashCode() {
            return (((this.state == null ? 0 : this.state.hashCode()) + 527) * 31) + (this.country == null ? 0 : this.country.hashCode());
        }

        public String toString() {
            return "RegionEvent: " + this.state + ", " + this.country;
        }
    }

    public static class RerouteEvent {
        public RouteEventType routeEventType;

        public RerouteEvent(RouteEventType routeEventType) {
            this.routeEventType = routeEventType;
        }
    }

    public static class RouteCalculationEvent {
        public boolean abortOriginDisplay;
        public Object end;
        public Destination lookupDestination;
        public String pendingNavigationRequestId;
        public int progress;
        public int progressIncrement;
        public NavigationRouteRequest request;
        public NavigationRouteResponse response;
        public RouteOptions routeOptions;
        public Object start;
        public RouteCalculationState state;
        public boolean stopped;
        public List<?> waypoints;
    }

    public enum RouteCalculationState {
        NONE,
        STARTED,
        STOPPED,
        IN_PROGRESS,
        ABORT_NAVIGATION,
        FINDING_NAV_COORDINATES
    }

    public enum RouteEventType {
        STARTED,
        FINISHED,
        FAILED
    }

    public static class SpeedWarning {
        public boolean exceed;

        public SpeedWarning(boolean exceed) {
            this.exceed = exceed;
        }
    }

    public static class TrafficDelayDismissEvent {
    }

    public static class TrafficDelayEvent {
        public final long etaDifference;

        public TrafficDelayEvent(long etaDifference) {
            this.etaDifference = etaDifference;
        }
    }

    public static class TrafficJamDismissEvent {
    }

    public static class TrafficJamProgressEvent {
        public final int remainingTime;

        public TrafficJamProgressEvent(int remainingTime) {
            this.remainingTime = remainingTime;
        }
    }

    public enum TrafficRerouteAction {
        REROUTE,
        DISMISS
    }

    public static class TrafficRerouteDismissEvent {
    }

    public static class TrafficRerouteEvent {
        public String additionalVia;
        public long currentEta;
        public long distanceDifference;
        public long etaDifference;
        public long newEta;
        public String via;

        public TrafficRerouteEvent(String via, String additionalVia, long etaDifference, long currentEta, long distanceDifference, long newEta) {
            this.via = via;
            this.additionalVia = additionalVia;
            this.etaDifference = etaDifference;
            this.currentEta = currentEta;
            this.distanceDifference = distanceDifference;
            this.newEta = newEta;
        }
    }
}
