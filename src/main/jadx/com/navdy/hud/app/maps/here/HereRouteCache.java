package com.navdy.hud.app.maps.here;

import android.util.LruCache;
import com.here.android.mpa.common.GeoCoordinate;
import com.here.android.mpa.routing.Route;
import com.navdy.service.library.events.navigation.NavigationRouteRequest;
import com.navdy.service.library.events.navigation.NavigationRouteResult;
import com.navdy.service.library.log.Logger;

public class HereRouteCache extends LruCache<String, RouteInfo> {
    public static final int MAX_CACHE_ROUTE_COUNT = 20;
    private static final Logger sLogger = new Logger(HereRouteCache.class);
    private static final HereRouteCache sSingleton = new HereRouteCache();

    public static class RouteInfo {
        public Route route;
        public NavigationRouteRequest routeRequest;
        public NavigationRouteResult routeResult;
        public GeoCoordinate routeStartPoint;
        public boolean traffic;

        public RouteInfo(Route route, NavigationRouteRequest routeRequest, NavigationRouteResult routeResult, boolean traffic, GeoCoordinate routeStartPoint) {
            this.route = route;
            this.routeRequest = routeRequest;
            this.routeResult = routeResult;
            this.traffic = traffic;
            this.routeStartPoint = routeStartPoint;
        }
    }

    public static HereRouteCache getInstance() {
        return sSingleton;
    }

    private HereRouteCache() {
        super(20);
    }

    protected int sizeOf(String key, Route value) {
        return 1;
    }

    public void addRoute(String id, RouteInfo routeInfo) {
        super.put(id, routeInfo);
    }

    public RouteInfo removeRoute(String id) {
        return (RouteInfo) super.remove(id);
    }

    public RouteInfo getRoute(String id) {
        if (id != null) {
            return (RouteInfo) super.get(id);
        }
        return null;
    }

    protected void entryRemoved(boolean evicted, String key, Route oldValue, Route newValue) {
        if (evicted && oldValue != null) {
            sLogger.v("route removed from cache:" + key);
        }
    }
}
