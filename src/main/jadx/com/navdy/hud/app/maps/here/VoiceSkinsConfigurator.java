package com.navdy.hud.app.maps.here;

import android.content.Context;
import android.os.SystemClock;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.R;
import com.navdy.hud.app.storage.PathManager;
import com.navdy.hud.app.util.PackagedResource;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.util.IOUtils;

class VoiceSkinsConfigurator {
    private static final PackagedResource resource = new PackagedResource("voiceskins", PathManager.getInstance().getHereVoiceSkinsPath());
    private static final Logger sLogger = new Logger(VoiceSkinsConfigurator.class);

    VoiceSkinsConfigurator() {
    }

    static void updateVoiceSkins() {
        try {
            Context context = HudApplication.getAppContext();
            IOUtils.checkIntegrity(context, PathManager.getInstance().getHereVoiceSkinsPath(), R.raw.here_voices_integrity);
            sLogger.i("Voice skins update: starting");
            long l1 = SystemClock.elapsedRealtime();
            resource.updateFromResources(context, R.raw.here_voices, R.raw.here_voices_md5, true);
            sLogger.i("Voice skins update: time took " + (SystemClock.elapsedRealtime() - l1) + " ms");
        } catch (Throwable throwable) {
            sLogger.e("Voice skins error", throwable);
        }
    }
}
