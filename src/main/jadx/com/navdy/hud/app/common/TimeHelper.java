package com.navdy.hud.app.common;

import android.content.Context;
import android.content.res.Resources;
import com.navdy.hud.app.R;
import com.navdy.service.library.events.settings.DateTimeConfiguration.Clock;
import com.navdy.service.library.log.Logger;
import com.squareup.otto.Bus;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class TimeHelper {
    public static final UpdateClock CLOCK_CHANGED = new UpdateClock();
    public static final DateTimeAvailableEvent DATE_TIME_AVAILABLE_EVENT = new DateTimeAvailableEvent();
    private static final Logger sLogger = new Logger(TimeHelper.class);
    private String am;
    private String amCompact;
    private Bus bus;
    private Calendar calendar;
    private SimpleDateFormat currentTimeFormat;
    private SimpleDateFormat dateFormat;
    private SimpleDateFormat dayFormat;
    private String pm;
    private String pmCompact;
    private SimpleDateFormat timeFormat12;
    private SimpleDateFormat timeFormat24;

    public static class DateTimeAvailableEvent {
    }

    public static class UpdateClock {
    }

    public TimeHelper(Context context, Bus bus) {
        this.bus = bus;
        Resources resources = context.getResources();
        this.pmCompact = resources.getString(R.string.pm_marker);
        this.pm = resources.getString(R.string.pm);
        this.amCompact = resources.getString(R.string.am_marker);
        this.am = resources.getString(R.string.am);
        updateLocale();
    }

    public void setFormat(Clock format) {
        switch (format) {
            case CLOCK_24_HOUR:
                this.currentTimeFormat = this.timeFormat24;
                break;
            case CLOCK_12_HOUR:
                this.currentTimeFormat = this.timeFormat12;
                break;
        }
        this.calendar = Calendar.getInstance();
    }

    public Clock getFormat() {
        if (this.currentTimeFormat == this.timeFormat12) {
            return Clock.CLOCK_12_HOUR;
        }
        return Clock.CLOCK_24_HOUR;
    }

    public synchronized String formatTime(Date date, StringBuilder amPmMarker) {
        String ret;
        ret = this.currentTimeFormat.format(date);
        if (amPmMarker != null) {
            amPmMarker.setLength(0);
            if (this.currentTimeFormat == this.timeFormat12) {
                this.calendar.setTime(date);
                if (this.calendar.get(9) == 1) {
                    amPmMarker.append(this.pmCompact);
                }
            }
        }
        return ret;
    }

    public synchronized String formatTime12Hour(Date date, StringBuilder amPmMarker, boolean compact_AM_PM) {
        String ret;
        ret = this.timeFormat12.format(date);
        if (amPmMarker != null) {
            amPmMarker.setLength(0);
            this.calendar.setTime(date);
            if (this.calendar.get(9) == 1) {
                if (compact_AM_PM) {
                    amPmMarker.append(this.pmCompact);
                } else {
                    amPmMarker.append(this.pm);
                }
            } else if (compact_AM_PM) {
                amPmMarker.append(this.amCompact);
            } else {
                amPmMarker.append(this.am);
            }
        }
        return ret;
    }

    public synchronized void updateLocale() {
        String tz = TimeZone.getDefault().getID();
        sLogger.v("timezone is " + tz);
        TimeZone timeZone = TimeZone.getTimeZone(tz);
        this.dayFormat = new SimpleDateFormat("EEE");
        this.dayFormat.setTimeZone(timeZone);
        this.dateFormat = new SimpleDateFormat("d MMM");
        this.dateFormat.setTimeZone(timeZone);
        boolean twelveHour = true;
        if (this.currentTimeFormat == this.timeFormat24) {
            twelveHour = false;
        }
        this.timeFormat24 = new SimpleDateFormat("H:mm");
        this.timeFormat24.setTimeZone(timeZone);
        this.timeFormat12 = new SimpleDateFormat("h:mm");
        this.timeFormat12.setTimeZone(timeZone);
        this.calendar = Calendar.getInstance();
        this.calendar.setTimeZone(timeZone);
        if (twelveHour) {
            this.currentTimeFormat = this.timeFormat12;
        } else {
            this.currentTimeFormat = this.timeFormat24;
        }
        this.bus.post(CLOCK_CHANGED);
    }

    public synchronized String getDay() {
        return this.dayFormat.format(new Date());
    }

    public synchronized String getDate() {
        return this.dateFormat.format(new Date());
    }

    public synchronized TimeZone getTimeZone() {
        String tz;
        tz = TimeZone.getDefault().getID();
        sLogger.v("timezone is " + tz);
        return TimeZone.getTimeZone(tz);
    }
}
