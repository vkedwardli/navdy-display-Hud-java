package com.navdy.hud.app.common;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;
import com.navdy.hud.app.profile.HudLocale;
import com.navdy.service.library.log.Logger;
import mortar.Blueprint;
import mortar.Mortar;
import mortar.MortarActivityScope;

public abstract class BaseActivity extends Activity {
    private MortarActivityScope activityScope;
    protected Logger logger = new Logger(getClass());

    protected void onCreate(Bundle savedInstanceState) {
        this.logger.v("::OnCreate");
        super.onCreate(savedInstanceState);
        Blueprint blueprint = getBlueprint();
        if (blueprint != null) {
            this.activityScope = Mortar.requireActivityScope(Mortar.getScope(getApplication()), blueprint);
            Mortar.inject(this, this);
            this.activityScope.onCreate(savedInstanceState);
        }
    }

    protected void attachBaseContext(Context base) {
        super.attachBaseContext(HudLocale.onAttach(base));
    }

    public void onResume() {
        this.logger.v("::onResume");
        super.onResume();
    }

    protected void onPause() {
        this.logger.v("::onPause");
        super.onPause();
    }

    protected void onDestroy() {
        this.logger.v("::onDestroy");
        super.onDestroy();
        if (isFinishing() && this.activityScope != null) {
            Mortar.getScope(getApplication()).destroyChild(this.activityScope);
            this.activityScope = null;
        }
    }

    public void onConfigurationChanged(Configuration newConfig) {
        this.logger.v("::onConfigurationChanged");
        super.onConfigurationChanged(newConfig);
        HudLocale.onAttach(this);
    }

    public void onSaveInstanceState(Bundle outState) {
        this.logger.v("::onSaveInstanceState");
        super.onSaveInstanceState(outState);
        if (this.activityScope != null) {
            this.activityScope.onSaveInstanceState(outState);
        }
    }

    public boolean isActivityDestroyed() {
        return isFinishing() || isDestroyed();
    }

    public Object getSystemService(String name) {
        if (!Mortar.isScopeSystemService(name)) {
            return super.getSystemService(name);
        }
        if (this.activityScope != null) {
            return this.activityScope;
        }
        throw new IllegalArgumentException("BaseActivity must override getBlueprint to use Mortar internally");
    }

    protected Blueprint getBlueprint() {
        return null;
    }
}
