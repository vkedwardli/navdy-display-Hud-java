package com.navdy.hud.app.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.navdy.service.library.log.Logger;

public class LogLevelReceiver extends BroadcastReceiver {
    private static final Logger sLogger = new Logger(LogLevelReceiver.class);

    public void onReceive(Context context, Intent intent) {
        try {
            if (intent.getAction().equals(Logger.ACTION_RELOAD)) {
                sLogger.i("Forcing loggers to reload cached log levels");
                Logger.reloadLogLevels();
            }
        } catch (Throwable t) {
            sLogger.e(t);
        }
    }
}
