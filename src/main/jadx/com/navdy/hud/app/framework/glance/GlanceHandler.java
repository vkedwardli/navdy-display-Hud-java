package com.navdy.hud.app.framework.glance;

import com.navdy.hud.app.analytics.AnalyticsSupport;
import com.navdy.hud.app.event.RemoteEvent;
import com.navdy.hud.app.framework.contacts.ContactUtil;
import com.navdy.hud.app.framework.message.SmsNotification;
import com.navdy.hud.app.framework.message.SmsNotification.Mode;
import com.navdy.hud.app.framework.notifications.NotificationManager;
import com.navdy.hud.app.framework.twilio.TwilioSmsManager;
import com.navdy.hud.app.framework.twilio.TwilioSmsManager.Callback;
import com.navdy.hud.app.framework.twilio.TwilioSmsManager.ErrorCode;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.util.GenericUtil;
import com.navdy.service.library.events.RequestStatus;
import com.navdy.service.library.events.glances.GlanceEvent;
import com.navdy.service.library.events.glances.GlanceEvent.GlanceActions;
import com.navdy.service.library.events.glances.KeyValue;
import com.navdy.service.library.events.messaging.SmsMessageRequest;
import com.navdy.service.library.events.messaging.SmsMessageResponse;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.task.TaskManager;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.UUID;

public class GlanceHandler {
    private static final String EMPTY = "";
    private static final SimpleDateFormat dateFormat1 = new SimpleDateFormat("hh:mm a");
    private static final SimpleDateFormat dateFormat2 = new SimpleDateFormat("hh:mm");
    private static final SimpleDateFormat dateFormat3 = new SimpleDateFormat("MMM d, hh:mm a");
    private static final SimpleDateFormat dateFormat4 = new SimpleDateFormat("h");
    private static final SimpleDateFormat dateFormat5 = new SimpleDateFormat("h a");
    private static final Object lockObj = new Object();
    private static final GlanceHandler sInstance = new GlanceHandler();
    public static final Logger sLogger = new Logger(GlanceHandler.class);
    private Bus bus = RemoteDeviceManager.getInstance().getBus();
    private GlanceTracker glanceTracker = new GlanceTracker();
    private HashMap<String, String> messagesSent = new HashMap();

    /* renamed from: com.navdy.hud.app.framework.glance.GlanceHandler$3 */
    static /* synthetic */ class AnonymousClass3 {
        static final /* synthetic */ int[] $SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp = new int[GlanceApp.values().length];

        static {
            try {
                $SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp[GlanceApp.SMS.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                $SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp[GlanceApp.IMESSAGE.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                $SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp[GlanceApp.GOOGLE_HANGOUT.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                $SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp[GlanceApp.SLACK.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
            try {
                $SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp[GlanceApp.WHATS_APP.ordinal()] = 5;
            } catch (NoSuchFieldError e5) {
            }
            try {
                $SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp[GlanceApp.FACEBOOK_MESSENGER.ordinal()] = 6;
            } catch (NoSuchFieldError e6) {
            }
            try {
                $SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp[GlanceApp.GENERIC_MESSAGE.ordinal()] = 7;
            } catch (NoSuchFieldError e7) {
            }
            try {
                $SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp[GlanceApp.FACEBOOK.ordinal()] = 8;
            } catch (NoSuchFieldError e8) {
            }
            try {
                $SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp[GlanceApp.TWITTER.ordinal()] = 9;
            } catch (NoSuchFieldError e9) {
            }
            try {
                $SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp[GlanceApp.GENERIC_SOCIAL.ordinal()] = 10;
            } catch (NoSuchFieldError e10) {
            }
            try {
                $SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp[GlanceApp.GENERIC.ordinal()] = 11;
            } catch (NoSuchFieldError e11) {
            }
            try {
                $SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp[GlanceApp.GOOGLE_MAIL.ordinal()] = 12;
            } catch (NoSuchFieldError e12) {
            }
            try {
                $SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp[GlanceApp.GOOGLE_INBOX.ordinal()] = 13;
            } catch (NoSuchFieldError e13) {
            }
            try {
                $SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp[GlanceApp.APPLE_MAIL.ordinal()] = 14;
            } catch (NoSuchFieldError e14) {
            }
            try {
                $SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp[GlanceApp.GENERIC_MAIL.ordinal()] = 15;
            } catch (NoSuchFieldError e15) {
            }
        }
    }

    public static GlanceHandler getInstance() {
        return sInstance;
    }

    private GlanceHandler() {
        this.bus.register(this);
    }

    @Subscribe
    public void onGlanceEvent(final GlanceEvent event) {
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                try {
                    GlanceHandler.this.handleGlancEventInternal(event);
                } catch (Throwable t) {
                    GlanceHandler.sLogger.e("handleGlancEventInternal", t);
                }
            }
        }, 14);
    }

    private void handleGlancEventInternal(com.navdy.service.library.events.glances.GlanceEvent r39) {
        /* JADX: method processing error */
/*
Error: jadx.core.utils.exceptions.JadxRuntimeException: Unknown predecessor block by arg (r20_0 'glanceNotification' com.navdy.hud.app.framework.glance.GlanceNotification) in PHI: PHI: (r20_1 'glanceNotification' com.navdy.hud.app.framework.glance.GlanceNotification) = (r20_0 'glanceNotification' com.navdy.hud.app.framework.glance.GlanceNotification), (r20_2 'glanceNotification' com.navdy.hud.app.framework.glance.GlanceNotification) binds: {(r20_0 'glanceNotification' com.navdy.hud.app.framework.glance.GlanceNotification)=B:20:0x0062, (r20_2 'glanceNotification' com.navdy.hud.app.framework.glance.GlanceNotification)=B:63:0x0344}
	at jadx.core.dex.instructions.PhiInsn.replaceArg(PhiInsn.java:78)
	at jadx.core.dex.visitors.ModVisitor.processInvoke(ModVisitor.java:222)
	at jadx.core.dex.visitors.ModVisitor.replaceStep(ModVisitor.java:83)
	at jadx.core.dex.visitors.ModVisitor.visit(ModVisitor.java:68)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:27)
	at jadx.core.dex.visitors.DepthTraversal.lambda$visit$1(DepthTraversal.java:14)
	at java.util.ArrayList.forEach(Unknown Source)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.ProcessClass.process(ProcessClass.java:34)
	at jadx.core.ProcessClass.lambda$processDependencies$0(ProcessClass.java:59)
	at java.lang.Iterable.forEach(Unknown Source)
	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:59)
	at jadx.core.ProcessClass.process(ProcessClass.java:39)
	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:282)
	at jadx.api.JavaClass.decompile(JavaClass.java:62)
	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:200)
*/
        /*
        r38 = this;
        printGlance(r39);
        r0 = r39;
        r6 = r0.id;
        r6 = android.text.TextUtils.isEmpty(r6);
        if (r6 != 0) goto L_0x002d;
    L_0x000d:
        r0 = r39;
        r6 = r0.provider;
        r6 = android.text.TextUtils.isEmpty(r6);
        if (r6 != 0) goto L_0x002d;
    L_0x0017:
        r0 = r39;
        r6 = r0.glanceType;
        if (r6 == 0) goto L_0x002d;
    L_0x001d:
        r0 = r39;
        r6 = r0.glanceData;
        if (r6 == 0) goto L_0x002d;
    L_0x0023:
        r0 = r39;
        r6 = r0.glanceData;
        r6 = r6.size();
        if (r6 != 0) goto L_0x0035;
    L_0x002d:
        r6 = sLogger;
        r8 = "invalid glance event";
        r6.v(r8);
    L_0x0034:
        return;
    L_0x0035:
        r19 = com.navdy.hud.app.framework.glance.GlanceHelper.getGlancesApp(r39);
        if (r19 != 0) goto L_0x003d;
    L_0x003b:
        r19 = com.navdy.hud.app.framework.glance.GlanceApp.GENERIC;
    L_0x003d:
        r17 = com.navdy.hud.app.framework.glance.GlanceHelper.buildDataMap(r39);
        r16 = 0;
        r6 = com.navdy.hud.app.framework.glance.GlanceHandler.AnonymousClass3.$SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp;
        r8 = r19.ordinal();
        r6 = r6[r8];
        switch(r6) {
            case 1: goto L_0x00ec;
            case 2: goto L_0x00ec;
            case 3: goto L_0x00ec;
            case 4: goto L_0x0270;
            case 5: goto L_0x0270;
            case 6: goto L_0x0270;
            case 7: goto L_0x0270;
            case 8: goto L_0x02a0;
            case 9: goto L_0x02a0;
            case 10: goto L_0x02a0;
            case 11: goto L_0x02d0;
            case 12: goto L_0x0300;
            case 13: goto L_0x0300;
            case 14: goto L_0x0300;
            case 15: goto L_0x0300;
            default: goto L_0x004e;
        };
    L_0x004e:
        r37 = com.navdy.hud.app.framework.glance.GlanceHelper.usesMessageLayout(r19);
        if (r37 == 0) goto L_0x0353;
    L_0x0054:
        r0 = r19;
        r1 = r17;
        r36 = com.navdy.hud.app.framework.glance.GlanceHelper.getGlanceMessage(r0, r1);
        r28 = com.navdy.hud.app.framework.glance.GlanceHelper.needsScrollLayout(r36);
        if (r28 == 0) goto L_0x0344;
    L_0x0062:
        r20 = new com.navdy.hud.app.framework.glance.GlanceNotification;
        r6 = com.navdy.hud.app.framework.glance.GlanceViewCache.ViewType.BIG_GLANCE_MESSAGE;
        r0 = r20;
        r1 = r39;
        r2 = r19;
        r3 = r17;
        r0.<init>(r1, r2, r6, r3);
    L_0x0071:
        r6 = new java.lang.StringBuilder;
        r6.<init>();
        r8 = "posted glance [";
        r6 = r6.append(r8);
        r0 = r39;
        r8 = r0.id;
        r6 = r6.append(r8);
        r8 = "] scroll[";
        r6 = r6.append(r8);
        r0 = r28;
        r6 = r6.append(r0);
        r8 = "]";
        r6 = r6.append(r8);
        r24 = r6.toString();
    L_0x009a:
        r0 = r38;
        r6 = r0.glanceTracker;
        r0 = r39;
        r1 = r19;
        r2 = r17;
        r32 = r6.isNotificationSeen(r0, r1, r2);
        r8 = 0;
        r6 = (r32 > r8 ? 1 : (r32 == r8 ? 0 : -1));
        if (r6 <= 0) goto L_0x042e;
    L_0x00ae:
        r6 = sLogger;
        r8 = new java.lang.StringBuilder;
        r8.<init>();
        r9 = "glance already seen [";
        r8 = r8.append(r9);
        r0 = r39;
        r9 = r0.id;
        r8 = r8.append(r9);
        r9 = "] at [";
        r8 = r8.append(r9);
        r0 = r32;
        r8 = r8.append(r0);
        r9 = "] now [";
        r8 = r8.append(r9);
        r10 = android.os.SystemClock.elapsedRealtime();
        r8 = r8.append(r10);
        r9 = "]";
        r8 = r8.append(r9);
        r8 = r8.toString();
        r6.v(r8);
        goto L_0x0034;
    L_0x00ec:
        r21 = 0;
        r6 = com.navdy.service.library.events.glances.MessageConstants.MESSAGE_BODY;
        r6 = r6.name();
        r0 = r17;
        r25 = r0.get(r6);
        r25 = (java.lang.String) r25;
        r6 = android.text.TextUtils.isEmpty(r25);
        if (r6 == 0) goto L_0x011e;
    L_0x0102:
        r6 = sLogger;
        r8 = new java.lang.StringBuilder;
        r8.<init>();
        r9 = "glance message does not exist:";
        r8 = r8.append(r9);
        r0 = r19;
        r8 = r8.append(r0);
        r8 = r8.toString();
        r6.v(r8);
        goto L_0x0034;
    L_0x011e:
        r0 = r19;
        r1 = r17;
        r7 = com.navdy.hud.app.framework.glance.GlanceHelper.getNumber(r0, r1);
        r0 = r19;
        r1 = r17;
        r5 = com.navdy.hud.app.framework.glance.GlanceHelper.getFrom(r0, r1);
        r6 = com.navdy.hud.app.framework.contacts.ContactUtil.isValidNumber(r7);
        if (r6 == 0) goto L_0x0203;
    L_0x0134:
        r21 = 1;
        r6 = sLogger;
        r8 = new java.lang.StringBuilder;
        r8.<init>();
        r9 = "hasNumber:";
        r8 = r8.append(r9);
        r8 = r8.append(r7);
        r8 = r8.toString();
        r6.v(r8);
        if (r5 != 0) goto L_0x0151;
    L_0x0150:
        r5 = r7;
    L_0x0151:
        if (r7 == 0) goto L_0x004e;
    L_0x0153:
        r4 = new com.navdy.hud.app.framework.recentcall.RecentCall;
        r6 = com.navdy.hud.app.framework.recentcall.RecentCall.Category.MESSAGE;
        r8 = com.navdy.hud.app.framework.contacts.NumberType.OTHER;
        r9 = new java.util.Date;
        r9.<init>();
        r10 = com.navdy.hud.app.framework.recentcall.RecentCall.CallType.INCOMING;
        r11 = -1;
        r12 = 0;
        r4.<init>(r5, r6, r7, r8, r9, r10, r11, r12);
        r30 = com.navdy.hud.app.framework.recentcall.RecentCallManager.getInstance();
        r0 = r30;
        r31 = r0.handleNewCall(r4);
        if (r31 != 0) goto L_0x0196;
    L_0x0172:
        r0 = r30;
        r16 = r0.getContactsFromId(r7);
        r6 = sLogger;
        r8 = new java.lang.StringBuilder;
        r8.<init>();
        r9 = "recent call contact list found [";
        r8 = r8.append(r9);
        r8 = r8.append(r7);
        r9 = "]";
        r8 = r8.append(r9);
        r8 = r8.toString();
        r6.v(r8);
    L_0x0196:
        r6 = sLogger;
        r8 = new java.lang.StringBuilder;
        r8.<init>();
        r9 = "recent call id[";
        r8 = r8.append(r9);
        r8 = r8.append(r7);
        r9 = "]";
        r8 = r8.append(r9);
        r8 = r8.toString();
        r6.v(r8);
        if (r21 != 0) goto L_0x004e;
    L_0x01b6:
        if (r31 == 0) goto L_0x0256;
    L_0x01b8:
        r6 = sLogger;
        r8 = new java.lang.StringBuilder;
        r8.<init>();
        r9 = "got Number:";
        r8 = r8.append(r9);
        r9 = r4.number;
        r8 = r8.append(r9);
        r8 = r8.toString();
        r6.v(r8);
        r29 = new java.util.ArrayList;
        r29.<init>();
        r0 = r39;
        r6 = r0.glanceData;
        r6 = r6.iterator();
    L_0x01df:
        r8 = r6.hasNext();
        if (r8 == 0) goto L_0x0207;
    L_0x01e5:
        r22 = r6.next();
        r22 = (com.navdy.service.library.events.glances.KeyValue) r22;
        r0 = r22;
        r8 = r0.key;
        r9 = com.navdy.service.library.events.glances.MessageConstants.MESSAGE_FROM_NUMBER;
        r9 = r9.name();
        r8 = android.text.TextUtils.equals(r8, r9);
        if (r8 != 0) goto L_0x01df;
    L_0x01fb:
        r0 = r29;
        r1 = r22;
        r0.add(r1);
        goto L_0x01df;
    L_0x0203:
        r7 = r5;
        r5 = 0;
        goto L_0x0151;
    L_0x0207:
        r6 = new com.navdy.service.library.events.glances.KeyValue;
        r8 = com.navdy.service.library.events.glances.MessageConstants.MESSAGE_FROM_NUMBER;
        r8 = r8.name();
        r9 = r4.number;
        r6.<init>(r8, r9);
        r0 = r29;
        r0.add(r6);
        r6 = new com.navdy.service.library.events.glances.GlanceEvent$Builder;
        r6.<init>();
        r0 = r39;
        r8 = r0.id;
        r6 = r6.id(r8);
        r0 = r39;
        r8 = r0.actions;
        r6 = r6.actions(r8);
        r0 = r39;
        r8 = r0.postTime;
        r6 = r6.postTime(r8);
        r0 = r39;
        r8 = r0.glanceType;
        r6 = r6.glanceType(r8);
        r0 = r39;
        r8 = r0.provider;
        r6 = r6.provider(r8);
        r0 = r29;
        r6 = r6.glanceData(r0);
        r39 = r6.build();
        r17 = com.navdy.hud.app.framework.glance.GlanceHelper.buildDataMap(r39);
        goto L_0x004e;
    L_0x0256:
        r6 = sLogger;
        r8 = new java.lang.StringBuilder;
        r8.<init>();
        r9 = "no Number yet:";
        r8 = r8.append(r9);
        r8 = r8.append(r7);
        r8 = r8.toString();
        r6.v(r8);
        goto L_0x004e;
    L_0x0270:
        r6 = com.navdy.service.library.events.glances.MessageConstants.MESSAGE_BODY;
        r6 = r6.name();
        r0 = r17;
        r25 = r0.get(r6);
        r25 = (java.lang.String) r25;
        r6 = android.text.TextUtils.isEmpty(r25);
        if (r6 == 0) goto L_0x004e;
    L_0x0284:
        r6 = sLogger;
        r8 = new java.lang.StringBuilder;
        r8.<init>();
        r9 = "glance message does not exist:";
        r8 = r8.append(r9);
        r0 = r19;
        r8 = r8.append(r0);
        r8 = r8.toString();
        r6.v(r8);
        goto L_0x0034;
    L_0x02a0:
        r6 = com.navdy.service.library.events.glances.SocialConstants.SOCIAL_MESSAGE;
        r6 = r6.name();
        r0 = r17;
        r25 = r0.get(r6);
        r25 = (java.lang.String) r25;
        r6 = android.text.TextUtils.isEmpty(r25);
        if (r6 == 0) goto L_0x004e;
    L_0x02b4:
        r6 = sLogger;
        r8 = new java.lang.StringBuilder;
        r8.<init>();
        r9 = "glance message does not exist:";
        r8 = r8.append(r9);
        r0 = r19;
        r8 = r8.append(r0);
        r8 = r8.toString();
        r6.v(r8);
        goto L_0x0034;
    L_0x02d0:
        r6 = com.navdy.service.library.events.glances.GenericConstants.GENERIC_MESSAGE;
        r6 = r6.name();
        r0 = r17;
        r25 = r0.get(r6);
        r25 = (java.lang.String) r25;
        r6 = android.text.TextUtils.isEmpty(r25);
        if (r6 == 0) goto L_0x004e;
    L_0x02e4:
        r6 = sLogger;
        r8 = new java.lang.StringBuilder;
        r8.<init>();
        r9 = "glance message does not exist:";
        r8 = r8.append(r9);
        r0 = r19;
        r8 = r8.append(r0);
        r8 = r8.toString();
        r6.v(r8);
        goto L_0x0034;
    L_0x0300:
        r6 = com.navdy.service.library.events.glances.EmailConstants.EMAIL_BODY;
        r6 = r6.name();
        r0 = r17;
        r14 = r0.get(r6);
        r14 = (java.lang.String) r14;
        r6 = com.navdy.service.library.events.glances.EmailConstants.EMAIL_SUBJECT;
        r6 = r6.name();
        r0 = r17;
        r34 = r0.get(r6);
        r34 = (java.lang.String) r34;
        r6 = android.text.TextUtils.isEmpty(r14);
        if (r6 == 0) goto L_0x004e;
    L_0x0322:
        r6 = android.text.TextUtils.isEmpty(r34);
        if (r6 == 0) goto L_0x004e;
    L_0x0328:
        r6 = sLogger;
        r8 = new java.lang.StringBuilder;
        r8.<init>();
        r9 = "glance message does not exist:";
        r8 = r8.append(r9);
        r0 = r19;
        r8 = r8.append(r0);
        r8 = r8.toString();
        r6.v(r8);
        goto L_0x0034;
    L_0x0344:
        r20 = new com.navdy.hud.app.framework.glance.GlanceNotification;
        r0 = r20;
        r1 = r39;
        r2 = r19;
        r3 = r17;
        r0.<init>(r1, r2, r3);
        goto L_0x0071;
    L_0x0353:
        r6 = com.navdy.hud.app.framework.glance.GlanceHelper.isCalendarApp(r19);
        if (r6 == 0) goto L_0x0402;
    L_0x0359:
        r6 = com.navdy.service.library.events.glances.CalendarConstants.CALENDAR_TIME;
        r6 = r6.name();
        r0 = r17;
        r18 = r0.get(r6);
        r18 = (java.lang.String) r18;
        r26 = 0;
        if (r18 == 0) goto L_0x036f;
    L_0x036b:
        r26 = java.lang.Long.parseLong(r18);	 Catch:{ Throwable -> 0x03f8 }
    L_0x036f:
        r8 = 0;
        r6 = (r26 > r8 ? 1 : (r26 == r8 ? 0 : -1));
        if (r6 != 0) goto L_0x03c0;
    L_0x0375:
        r6 = com.navdy.service.library.events.glances.CalendarConstants.CALENDAR_TIME_STR;
        r6 = r6.name();
        r0 = r17;
        r18 = r0.get(r6);
        r18 = (java.lang.String) r18;
        r6 = sLogger;
        r8 = new java.lang.StringBuilder;
        r8.<init>();
        r9 = "event-time-str =";
        r8 = r8.append(r9);
        r0 = r18;
        r8 = r8.append(r0);
        r8 = r8.toString();
        r6.v(r8);
        r6 = android.text.TextUtils.isEmpty(r18);
        if (r6 != 0) goto L_0x03ab;
    L_0x03a3:
        r0 = r38;
        r1 = r18;
        r26 = r0.extractTime(r1);
    L_0x03ab:
        r8 = 0;
        r6 = (r26 > r8 ? 1 : (r26 == r8 ? 0 : -1));
        if (r6 == 0) goto L_0x03c0;
    L_0x03b1:
        r6 = com.navdy.service.library.events.glances.CalendarConstants.CALENDAR_TIME;
        r6 = r6.name();
        r8 = java.lang.String.valueOf(r26);
        r0 = r17;
        r0.put(r6, r8);
    L_0x03c0:
        r20 = new com.navdy.hud.app.framework.glance.GlanceNotification;
        r0 = r20;
        r1 = r39;
        r2 = r19;
        r3 = r17;
        r0.<init>(r1, r2, r3);
        r6 = new java.lang.StringBuilder;
        r6.<init>();
        r8 = "posted glance [";
        r6 = r6.append(r8);
        r0 = r39;
        r8 = r0.id;
        r6 = r6.append(r8);
        r8 = "] time[";
        r6 = r6.append(r8);
        r0 = r26;
        r6 = r6.append(r0);
        r8 = "]";
        r6 = r6.append(r8);
        r24 = r6.toString();
        goto L_0x009a;
    L_0x03f8:
        r35 = move-exception;
        r6 = sLogger;
        r0 = r35;
        r6.e(r0);
        goto L_0x036f;
    L_0x0402:
        r20 = new com.navdy.hud.app.framework.glance.GlanceNotification;
        r0 = r20;
        r1 = r39;
        r2 = r19;
        r3 = r17;
        r0.<init>(r1, r2, r3);
        r6 = new java.lang.StringBuilder;
        r6.<init>();
        r8 = "posted glance [";
        r6 = r6.append(r8);
        r0 = r39;
        r8 = r0.id;
        r6 = r6.append(r8);
        r8 = "]";
        r6 = r6.append(r8);
        r24 = r6.toString();
        goto L_0x009a;
    L_0x042e:
        if (r16 == 0) goto L_0x0457;
    L_0x0430:
        r23 = new java.util.ArrayList;
        r23.<init>();
        r6 = r16.iterator();
    L_0x0439:
        r8 = r6.hasNext();
        if (r8 == 0) goto L_0x0450;
    L_0x043f:
        r15 = r6.next();
        r15 = (com.navdy.service.library.events.contacts.Contact) r15;
        r8 = new com.navdy.hud.app.framework.contacts.Contact;
        r8.<init>(r15);
        r0 = r23;
        r0.add(r8);
        goto L_0x0439;
    L_0x0450:
        r0 = r20;
        r1 = r23;
        r0.setContactList(r1);
    L_0x0457:
        r6 = com.navdy.hud.app.framework.notifications.NotificationManager.getInstance();
        r0 = r20;
        r6.addNotification(r0);
        r6 = sLogger;
        r0 = r24;
        r6.v(r0);
        goto L_0x0034;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.navdy.hud.app.framework.glance.GlanceHandler.handleGlancEventInternal(com.navdy.service.library.events.glances.GlanceEvent):void");
    }

    private static void printGlance(GlanceEvent event) {
        int i = 0;
        if (event == null) {
            sLogger.v("null glance event");
            return;
        }
        int i2;
        Logger logger = sLogger;
        StringBuilder append = new StringBuilder().append("[glance-event] id[").append(event.id).append("] type[").append(event.glanceType).append("] provider[").append(event.provider).append("] data-size [");
        if (event.glanceData == null) {
            i2 = 0;
        } else {
            i2 = event.glanceData.size();
        }
        StringBuilder append2 = append.append(i2).append("] action-size [");
        if (event.actions != null) {
            i = event.actions.size();
        }
        logger.v(append2.append(i).append("]").toString());
        if (event.glanceData != null && event.glanceData.size() > 0) {
            for (KeyValue keyValue : event.glanceData) {
                sLogger.v("[glance-event] data key[" + keyValue.key + "] val[" + keyValue.value + "]");
            }
        }
        if (event.actions != null && event.actions.size() > 0) {
            for (GlanceActions action : event.actions) {
                sLogger.v("[glance-event] action[" + action + "]");
            }
        }
    }

    @Subscribe
    public void onMessage(SmsMessageResponse event) {
        try {
            if (event.status == RequestStatus.REQUEST_SUCCESS) {
                AnalyticsSupport.recordSmsSent(true, AnalyticsSupport.ATTR_PLATFORM_ANDROID, GlanceConstants.areMessageCanned());
                return;
            }
            AnalyticsSupport.recordSmsSent(false, AnalyticsSupport.ATTR_PLATFORM_ANDROID, GlanceConstants.areMessageCanned());
            String msg = removeMessage(event.id);
            if (msg != null) {
                getInstance().sendSmsFailedNotification(event.number, msg, event.name);
            } else {
                sLogger.e("sms message with id:" + event.id + " not found");
            }
        } catch (Throwable t) {
            sLogger.e(t);
        }
    }

    public boolean sendMessage(final String number, final String message, final String name) {
        try {
            if (!GenericUtil.isClientiOS()) {
                String msgId = UUID.randomUUID().toString();
                addMessage(msgId, message);
                this.bus.post(new RemoteEvent(new SmsMessageRequest(number, message, name, msgId)));
                sLogger.v("sms sent");
                return true;
            } else if (TwilioSmsManager.getInstance().sendSms(number, message, new Callback() {
                public void result(ErrorCode code) {
                    if (code != ErrorCode.SUCCESS) {
                        GlanceHandler.this.sendSmsFailedNotification(number, message, name);
                    }
                }
            }) == ErrorCode.SUCCESS) {
                sLogger.v("sms-twilio queued");
                return true;
            } else {
                AnalyticsSupport.recordSmsSent(false, AnalyticsSupport.ATTR_PLATFORM_IOS, GlanceConstants.areMessageCanned());
                return false;
            }
        } catch (Throwable t) {
            sLogger.e(t);
            return false;
        }
    }

    private long extractTime(String str) {
        String firstPart;
        Calendar date;
        Date eventDate;
        long time;
        Calendar cal2;
        String secondPart = null;
        int index = str.indexOf("\u2013");
        if (index != -1) {
            secondPart = str.substring(index + 1).trim();
            firstPart = str.substring(0, index).trim();
            secondPart = ContactUtil.sanitizeString(secondPart);
            firstPart = ContactUtil.sanitizeString(firstPart);
        } else {
            firstPart = ContactUtil.sanitizeString(str);
        }
        synchronized (lockObj) {
            date = Calendar.getInstance();
            Calendar d;
            try {
                d = Calendar.getInstance();
                d.setTime(dateFormat1.parse(firstPart));
                date.set(10, d.get(10));
                date.set(12, d.get(12));
                date.set(13, d.get(13));
                date.set(14, 0);
                date.set(9, d.get(9));
                eventDate = date.getTime();
                sLogger.v("event-time-date = " + eventDate.toString());
                time = eventDate.getTime();
            } catch (Throwable th) {
            }
        }
        return time;
        eventDate = date.getTime();
        sLogger.v("event-time-date = " + eventDate.toString());
        return eventDate.getTime();
    }

    public void clearState() {
        this.glanceTracker.clearState();
        clearAllMessage();
    }

    public void saveState() {
        this.glanceTracker.saveState();
    }

    public void restoreState() {
        this.glanceTracker.restoreState();
    }

    public void sendSmsFailedNotification(String number, String message, String name) {
        NotificationManager.getInstance().addNotification(new SmsNotification(Mode.Failed, UUID.randomUUID().toString(), number, message, name));
    }

    public void sendSmsSuccessNotification(String id, String number, String message, String name) {
        NotificationManager.getInstance().addNotification(new SmsNotification(Mode.Success, id, number, message, name));
    }

    public void addMessage(String id, String message) {
        if (id != null && message != null) {
            synchronized (this.messagesSent) {
                this.messagesSent.put(id, message);
            }
        }
    }

    public String removeMessage(String id) {
        if (id == null) {
            return null;
        }
        String str;
        synchronized (this.messagesSent) {
            str = (String) this.messagesSent.remove(id);
        }
        return str;
    }

    private void clearAllMessage() {
        synchronized (this.messagesSent) {
            this.messagesSent.clear();
        }
    }
}
