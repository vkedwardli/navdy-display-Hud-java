package com.navdy.hud.app.framework.trips;

import android.content.SharedPreferences;
import com.squareup.otto.Bus;
import dagger.internal.Binding;
import dagger.internal.Linker;
import java.util.Set;
import javax.inject.Provider;

public final class TripManager$$InjectAdapter extends Binding<TripManager> implements Provider<TripManager> {
    private Binding<Bus> bus;
    private Binding<SharedPreferences> preferences;

    public TripManager$$InjectAdapter() {
        super("com.navdy.hud.app.framework.trips.TripManager", "members/com.navdy.hud.app.framework.trips.TripManager", true, TripManager.class);
    }

    public void attach(Linker linker) {
        this.bus = linker.requestBinding("com.squareup.otto.Bus", TripManager.class, getClass().getClassLoader());
        this.preferences = linker.requestBinding("android.content.SharedPreferences", TripManager.class, getClass().getClassLoader());
    }

    public void getDependencies(Set<Binding<?>> getBindings, Set<Binding<?>> set) {
        getBindings.add(this.bus);
        getBindings.add(this.preferences);
    }

    public TripManager get() {
        return new TripManager((Bus) this.bus.get(), (SharedPreferences) this.preferences.get());
    }
}
