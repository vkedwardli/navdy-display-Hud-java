package com.navdy.hud.app.framework.recentcall;

import android.text.TextUtils;
import com.navdy.hud.app.event.RemoteEvent;
import com.navdy.hud.app.framework.DriverProfileHelper;
import com.navdy.hud.app.framework.contacts.ContactUtil;
import com.navdy.hud.app.framework.contacts.NumberType;
import com.navdy.hud.app.framework.recentcall.RecentCall.CallType;
import com.navdy.hud.app.framework.recentcall.RecentCall.Category;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.storage.db.helper.RecentCallPersistenceHelper;
import com.navdy.hud.app.util.GenericUtil;
import com.navdy.service.library.events.RequestStatus;
import com.navdy.service.library.events.callcontrol.CallEvent;
import com.navdy.service.library.events.contacts.Contact;
import com.navdy.service.library.events.contacts.ContactRequest;
import com.navdy.service.library.events.contacts.ContactResponse;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.task.TaskManager;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class RecentCallManager {
    private static final RecentCallChanged RECENT_CALL_CHANGED = new RecentCallChanged();
    private static final RecentCallManager sInstance = new RecentCallManager();
    private static final Logger sLogger = new Logger(RecentCallManager.class);
    private Bus bus = RemoteDeviceManager.getInstance().getBus();
    private HashMap<String, RecentCall> contactRequestMap = new HashMap();
    private Map<Long, RecentCall> numberMap = new HashMap();
    private volatile List<RecentCall> recentCalls;
    private HashMap<String, List<Contact>> tempContactLookupMap = new HashMap();

    public static class ContactFound {
        public List<Contact> contact;
        public String identifier;

        public ContactFound(String identifier, List<Contact> contact) {
            this.identifier = identifier;
            this.contact = contact;
        }
    }

    public static class RecentCallChanged {
    }

    public static RecentCallManager getInstance() {
        return sInstance;
    }

    private RecentCallManager() {
        this.bus.register(this);
    }

    public List<RecentCall> getRecentCalls() {
        return this.recentCalls;
    }

    public int getRecentCallsSize() {
        if (this.recentCalls != null) {
            return this.recentCalls.size();
        }
        return 0;
    }

    public void setRecentCalls(List<RecentCall> recentCalls) {
        this.recentCalls = recentCalls;
        buildMap();
        this.bus.post(RECENT_CALL_CHANGED);
    }

    public void clearRecentCalls() {
        setRecentCalls(null);
    }

    public void buildRecentCalls() {
        synchronized (this.contactRequestMap) {
            this.contactRequestMap.clear();
        }
        if (GenericUtil.isMainThread()) {
            TaskManager.getInstance().execute(new Runnable() {
                public void run() {
                    RecentCallManager.this.load();
                }
            }, 1);
        } else {
            load();
        }
    }

    private void load() {
        try {
            String id = DriverProfileHelper.getInstance().getCurrentProfile().getProfileName();
            this.recentCalls = RecentCallPersistenceHelper.getRecentsCalls(id);
            sLogger.v("load recentCall id[" + id + "] calls[" + this.recentCalls.size() + "]");
        } catch (Throwable t) {
            this.recentCalls = null;
            sLogger.e(t);
        } finally {
            buildMap();
            this.bus.post(RECENT_CALL_CHANGED);
        }
    }

    @Subscribe
    public void onCallEvent(CallEvent event) {
        if (TextUtils.isEmpty(event.number)) {
            sLogger.e("call event does not have a number, " + event.contact_name);
            return;
        }
        CallType callType;
        if (!event.incoming.booleanValue()) {
            callType = CallType.OUTGOING;
        } else if (event.answered.booleanValue()) {
            callType = CallType.INCOMING;
        } else {
            callType = CallType.MISSED;
        }
        handleNewCall(new RecentCall(event.contact_name, Category.PHONE_CALL, event.number, NumberType.OTHER, new Date(), callType, -1, 0));
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean handleNewCall(RecentCall call) {
        sLogger.v("handle new call:" + call.number);
        if (ContactUtil.isValidNumber(call.number)) {
            storeContactInfo(call);
            return true;
        }
        String number = null;
        synchronized (this.tempContactLookupMap) {
            List<Contact> list = (List) this.tempContactLookupMap.get(call.number);
            if (list != null) {
                if (list.size() > 1) {
                    return false;
                }
                number = ((Contact) list.get(0)).number;
            }
        }
    }

    private void requestContactInfo(RecentCall call) {
        sLogger.v("number not available for [" + call.number + "]");
        synchronized (this.contactRequestMap) {
            if (this.contactRequestMap.containsKey(call.number)) {
                sLogger.v("contact [" + call.number + "] request already pending");
                return;
            }
            this.contactRequestMap.put(call.number, call);
            sLogger.v("contact [" + call.number + "] request sent");
            this.bus.post(new RemoteEvent(new ContactRequest(call.number)));
        }
    }

    @Subscribe
    public void onContactResponse(final ContactResponse event) {
        TaskManager.getInstance().execute(new Runnable() {
            /* JADX WARNING: inconsistent code. */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public void run() {
                try {
                    Throwable th;
                    if (TextUtils.isEmpty(event.identifier)) {
                        RecentCallManager.sLogger.w("invalid contact repsonse");
                        return;
                    }
                    List<RecentCall> callList = null;
                    List<Contact> contactList = null;
                    synchronized (RecentCallManager.this.contactRequestMap) {
                        RecentCall call = (RecentCall) RecentCallManager.this.contactRequestMap.remove(event.identifier);
                        if (call == null) {
                            RecentCallManager.sLogger.i("identifer not found[" + event.identifier + "]");
                            return;
                        } else if (event.status != RequestStatus.REQUEST_SUCCESS) {
                            RecentCallManager.sLogger.i("request failed for [" + event.identifier + "] status[" + event.status + "]");
                        } else if (event.contacts == null || event.contacts.size() == 0) {
                            RecentCallManager.sLogger.i("no contact returned for [" + event.identifier + "] status[" + event.status + "]");
                            return;
                        } else {
                            RecentCallManager.sLogger.v("contacts returned [" + event.contacts.size() + "]");
                            Iterator it = event.contacts.iterator();
                            while (true) {
                                List<Contact> contactList2;
                                List<RecentCall> callList2;
                                try {
                                    contactList2 = contactList;
                                    callList2 = callList;
                                    if (!it.hasNext()) {
                                        contactList = contactList2;
                                        callList = callList2;
                                        break;
                                    }
                                    Contact obj = (Contact) it.next();
                                    if (TextUtils.isEmpty(obj.number) || !ContactUtil.isValidNumber(obj.number)) {
                                        RecentCallManager.sLogger.i("no number for  [" + event.identifier + "] status[" + event.status + "]");
                                        contactList = contactList2;
                                        callList = callList2;
                                    } else {
                                        RecentCall copy = new RecentCall(call);
                                        if (ContactUtil.isDisplayNameValid(obj.name, obj.number, ContactUtil.getPhoneNumber(obj.number))) {
                                            copy.name = obj.name;
                                        } else {
                                            copy.name = null;
                                        }
                                        copy.number = obj.number;
                                        copy.numberType = ContactUtil.getNumberType(obj.numberType);
                                        copy.validateArguments();
                                        if (callList2 == null) {
                                            callList = new ArrayList();
                                            try {
                                                contactList = new ArrayList();
                                            } catch (Throwable th2) {
                                                th = th2;
                                                contactList = contactList2;
                                            }
                                        } else {
                                            contactList = contactList2;
                                            callList = callList2;
                                        }
                                        try {
                                            callList.add(copy);
                                            contactList.add(obj);
                                        } catch (Throwable th3) {
                                            th = th3;
                                        }
                                    }
                                } catch (Throwable th4) {
                                    th = th4;
                                    contactList = contactList2;
                                    callList = callList2;
                                }
                            }
                        }
                    }
                    throw th;
                } catch (Throwable t) {
                    RecentCallManager.sLogger.e(t);
                }
            }
        }, 1);
    }

    public void clearContactLookupMap() {
        synchronized (this.tempContactLookupMap) {
            this.tempContactLookupMap.clear();
        }
    }

    public List<Contact> getContactsFromId(String id) {
        List<Contact> list;
        synchronized (this.tempContactLookupMap) {
            list = (List) this.tempContactLookupMap.get(id);
        }
        return list;
    }

    private void storeContactInfo(RecentCall call) {
        List list = new ArrayList();
        list.add(call);
        storeContactInfo(list);
    }

    private void storeContactInfo(final List<RecentCall> calls) {
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                RecentCallPersistenceHelper.storeRecentCalls(DriverProfileHelper.getInstance().getCurrentProfile().getProfileName(), calls, false);
                for (RecentCall c : calls) {
                    RecentCallManager.sLogger.v("added contact [" + c.number + "]");
                }
            }
        }, 1);
    }

    public int getDefaultContactImage(long number) {
        RecentCall call = (RecentCall) this.numberMap.get(Long.valueOf(number));
        if (call == null) {
            return -1;
        }
        return call.defaultImageIndex;
    }

    private void buildMap() {
        this.numberMap.clear();
        if (this.recentCalls != null && this.recentCalls.size() != 0) {
            for (RecentCall call : this.recentCalls) {
                this.numberMap.put(Long.valueOf(call.numericNumber), call);
            }
        }
    }
}
