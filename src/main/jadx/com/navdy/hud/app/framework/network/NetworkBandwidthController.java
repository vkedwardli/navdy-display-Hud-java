package com.navdy.hud.app.framework.network;

import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import android.text.TextUtils;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.event.DriverProfileChanged;
import com.navdy.hud.app.event.DriverProfileUpdated;
import com.navdy.hud.app.event.DriverProfileUpdated.State;
import com.navdy.hud.app.framework.DriverProfileHelper;
import com.navdy.hud.app.framework.network.NetworkStatCache.NetworkStatCacheInfo;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.maps.here.HereMapsManager;
import com.navdy.hud.app.util.DeviceUtil;
import com.navdy.hud.app.util.os.SystemProperties;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.task.TaskManager;
import com.navdy.service.library.util.IOUtils;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.json.JSONArray;
import org.json.JSONObject;

public class NetworkBandwidthController {
    private static final int ACTIVE = ((int) TimeUnit.SECONDS.toMillis(15));
    private static final UserBandwidthSettingChanged BW_SETTING_CHANGED = new UserBandwidthSettingChanged();
    private static final String DATA = "data";
    private static final int DATA_COLLECTION_INITIAL_INTERVAL = ((int) TimeUnit.SECONDS.toMillis(25));
    private static final int DATA_COLLECTION_INTERVAL = ((int) TimeUnit.MINUTES.toMillis(5));
    private static final int DATA_COLLECTION_INTERVAL_ENG = ((int) TimeUnit.MINUTES.toMillis(1));
    private static final String EVENT = "event";
    private static final String EVENT_DNS_INFO = "dnslookup";
    private static final String EVENT_NETSTAT = "netstat";
    private static final String FD = "fd";
    private static final String FROM = "from";
    private static final String GOOGLE_DNS_IP = "8.8.4.4";
    private static final String HERE_ROUTE_END_POINT_PATTERN = "route.hybrid.api.here.com";
    private static final String HOST = "host";
    private static final String IP = "ip";
    private static final int NETWORK_STAT_INFO_PORT = 23655;
    private static final String PERM_DISABLE_NETWORK = "persist.sys.perm_disable_nw";
    private static final String RX = "rx";
    private static final String TO = "to";
    private static final String TX = "tx";
    private static final boolean networkDisabled = SystemProperties.getBoolean(PERM_DISABLE_NETWORK, false);
    private static final Logger sLogger = new Logger("NetworkBandwidthControl");
    private static final NetworkBandwidthController singleton = new NetworkBandwidthController();
    private Bus bus = RemoteDeviceManager.getInstance().getBus();
    private final HashMap<Component, ComponentInfo> componentInfoMap = new HashMap();
    private Runnable dataCollectionRunnable = new Runnable() {
        public void run() {
            TaskManager.getInstance().execute(NetworkBandwidthController.this.dataCollectionRunnableBk, 1);
        }
    };
    private Runnable dataCollectionRunnableBk = new Runnable() {
        public void run() {
            try {
                NetworkBandwidthController.this.statCache.dump(NetworkBandwidthController.sLogger, true);
            } catch (Throwable t) {
                NetworkBandwidthController.sLogger.e(t);
            } finally {
                NetworkBandwidthController.this.handler.postDelayed(this, DeviceUtil.isUserBuild() ? (long) NetworkBandwidthController.DATA_COLLECTION_INTERVAL : (long) NetworkBandwidthController.DATA_COLLECTION_INTERVAL_ENG);
            }
        }
    };
    private DnsCache dnsCache = new DnsCache();
    private Handler handler = new Handler(Looper.getMainLooper());
    private boolean limitBandwidthModeOn;
    private Runnable networkStatRunnable = new Runnable() {
        /* JADX WARNING: inconsistent code. */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void run() {
            Throwable t;
            DatagramSocket socket = null;
            try {
                NetworkBandwidthController.sLogger.v("networkStat thread enter");
                DatagramSocket datagramSocket = new DatagramSocket(NetworkBandwidthController.NETWORK_STAT_INFO_PORT, InetAddress.getByName("127.0.0.1"));
                try {
                    datagramSocket.setReceiveBufferSize(16384);
                    byte[] data = new byte[2048];
                    DatagramPacket datagramPacket = new DatagramPacket(data, data.length);
                    while (true) {
                        String host;
                        JSONArray ips;
                        int nIps;
                        int i;
                        String ip;
                        JSONArray netStatData;
                        JSONObject dataObj;
                        String from;
                        String to;
                        int tx;
                        int rx;
                        int fd;
                        String destIP;
                        String dnsHost;
                        Component component;
                        ComponentInfo componentInfo;
                        datagramSocket.receive(datagramPacket);
                        JSONObject jSONObject = new JSONObject(new String(data, 0, datagramPacket.getLength()));
                        String event = jSONObject.getString("event");
                        Object obj = -1;
                        switch (event.hashCode()) {
                            case 260369379:
                                if (event.equals(NetworkBandwidthController.EVENT_DNS_INFO)) {
                                    obj = null;
                                }
                            case 1843370353:
                                if (event.equals(NetworkBandwidthController.EVENT_NETSTAT)) {
                                    obj = 1;
                                }
                                switch (obj) {
                                    case null:
                                        host = jSONObject.getString(NetworkBandwidthController.HOST);
                                        if ("localhost".equals(host)) {
                                            break;
                                        }
                                        ips = jSONObject.getJSONArray(NetworkBandwidthController.IP);
                                        nIps = ips.length();
                                        if (nIps == 0) {
                                            break;
                                        }
                                        for (i = 0; i < nIps; i++) {
                                            ip = ips.getString(i);
                                            NetworkBandwidthController.this.dnsCache.addEntry(ip, host);
                                            if (NetworkBandwidthController.sLogger.isLoggable(2)) {
                                                NetworkBandwidthController.sLogger.v("dnslookup " + host + " : " + ip);
                                            }
                                        }
                                        break;
                                    case 1:
                                        netStatData = jSONObject.getJSONArray(NetworkBandwidthController.DATA);
                                        if (netStatData.length() == 0) {
                                            break;
                                        }
                                        dataObj = netStatData.getJSONObject(0);
                                        from = dataObj.getString("from");
                                        to = dataObj.getString(NetworkBandwidthController.TO);
                                        tx = dataObj.getInt(NetworkBandwidthController.TX);
                                        rx = dataObj.getInt(NetworkBandwidthController.RX);
                                        fd = dataObj.getInt(NetworkBandwidthController.FD);
                                        if (NetworkBandwidthController.sLogger.isLoggable(2)) {
                                            NetworkBandwidthController.sLogger.v("[" + fd + "] tx[" + tx + "] rx[" + rx + "] from[" + from + "] to[" + to + "]");
                                        }
                                        destIP = to;
                                        if (TextUtils.isEmpty(destIP)) {
                                            destIP = from;
                                        }
                                        if (NetworkBandwidthController.GOOGLE_DNS_IP.equals(destIP)) {
                                            break;
                                        }
                                        NetworkBandwidthController.this.statCache.addStat(destIP, tx, rx, fd);
                                        if (tx > 0 && rx <= 0) {
                                            break;
                                        }
                                        dnsHost = NetworkBandwidthController.this.dnsCache.getHostnamefromIP(destIP);
                                        if (dnsHost == null) {
                                            break;
                                        }
                                        component = (Component) NetworkBandwidthController.this.urlToComponentMap.get(dnsHost);
                                        if (component == null && dnsHost.contains(NetworkBandwidthController.HERE_ROUTE_END_POINT_PATTERN)) {
                                            component = Component.HERE_ROUTE;
                                        }
                                        if (component != null) {
                                            if (!NetworkBandwidthController.this.trafficDataDownloadedOnce && component == Component.HERE_TRAFFIC && rx > 0) {
                                                NetworkBandwidthController.sLogger.i("traffic data downloaded once");
                                                NetworkBandwidthController.this.trafficDataDownloadedOnce = true;
                                            }
                                            componentInfo = (ComponentInfo) NetworkBandwidthController.this.componentInfoMap.get(component);
                                            synchronized (componentInfo) {
                                                componentInfo.lastActivity = SystemClock.elapsedRealtime();
                                            }
                                            break;
                                        }
                                        break;
                                        break;
                                    default:
                                        NetworkBandwidthController.sLogger.v("invalid command:" + event);
                                        break;
                                }
                        }
                        switch (obj) {
                            case null:
                                host = jSONObject.getString(NetworkBandwidthController.HOST);
                                if ("localhost".equals(host)) {
                                    ips = jSONObject.getJSONArray(NetworkBandwidthController.IP);
                                    nIps = ips.length();
                                    if (nIps == 0) {
                                        for (i = 0; i < nIps; i++) {
                                            ip = ips.getString(i);
                                            NetworkBandwidthController.this.dnsCache.addEntry(ip, host);
                                            if (NetworkBandwidthController.sLogger.isLoggable(2)) {
                                                NetworkBandwidthController.sLogger.v("dnslookup " + host + " : " + ip);
                                            }
                                        }
                                        break;
                                    }
                                    break;
                                }
                                break;
                            case 1:
                                netStatData = jSONObject.getJSONArray(NetworkBandwidthController.DATA);
                                if (netStatData.length() == 0) {
                                    dataObj = netStatData.getJSONObject(0);
                                    from = dataObj.getString("from");
                                    to = dataObj.getString(NetworkBandwidthController.TO);
                                    tx = dataObj.getInt(NetworkBandwidthController.TX);
                                    rx = dataObj.getInt(NetworkBandwidthController.RX);
                                    fd = dataObj.getInt(NetworkBandwidthController.FD);
                                    if (NetworkBandwidthController.sLogger.isLoggable(2)) {
                                        NetworkBandwidthController.sLogger.v("[" + fd + "] tx[" + tx + "] rx[" + rx + "] from[" + from + "] to[" + to + "]");
                                    }
                                    destIP = to;
                                    if (TextUtils.isEmpty(destIP)) {
                                        destIP = from;
                                    }
                                    if (NetworkBandwidthController.GOOGLE_DNS_IP.equals(destIP)) {
                                        NetworkBandwidthController.this.statCache.addStat(destIP, tx, rx, fd);
                                        if (tx > 0) {
                                        }
                                        dnsHost = NetworkBandwidthController.this.dnsCache.getHostnamefromIP(destIP);
                                        if (dnsHost == null) {
                                            component = (Component) NetworkBandwidthController.this.urlToComponentMap.get(dnsHost);
                                            component = Component.HERE_ROUTE;
                                            if (component != null) {
                                                break;
                                            }
                                            NetworkBandwidthController.sLogger.i("traffic data downloaded once");
                                            NetworkBandwidthController.this.trafficDataDownloadedOnce = true;
                                            componentInfo = (ComponentInfo) NetworkBandwidthController.this.componentInfoMap.get(component);
                                            synchronized (componentInfo) {
                                                componentInfo.lastActivity = SystemClock.elapsedRealtime();
                                            }
                                            break;
                                            break;
                                        }
                                        break;
                                    }
                                    break;
                                }
                                break;
                                break;
                            default:
                                NetworkBandwidthController.sLogger.v("invalid command:" + event);
                                break;
                        }
                    }
                } catch (Throwable th) {
                    t = th;
                    socket = datagramSocket;
                }
            } catch (Throwable th2) {
                t = th2;
                NetworkBandwidthController.sLogger.e(t);
                if (socket != null) {
                    IOUtils.closeStream(socket);
                }
                NetworkBandwidthController.sLogger.v("networkStat thread exit");
            }
        }
    };
    private Thread networkStatThread;
    private NetworkStatCache statCache = new NetworkStatCache(this.dnsCache);
    private boolean trafficDataDownloadedOnce;
    private final HashMap<String, Component> urlToComponentMap = new HashMap();

    public enum Component {
        LOCALYTICS,
        HERE_ROUTE,
        HERE_TRAFFIC,
        HERE_MAP_DOWNLOAD,
        HERE_REVERSE_GEO,
        HOCKEY,
        JIRA
    }

    private static class ComponentInfo {
        public long lastActivity;

        private ComponentInfo() {
        }

        /* synthetic */ ComponentInfo(AnonymousClass1 x0) {
            this();
        }
    }

    public static class UserBandwidthSettingChanged {
    }

    static {
        if (networkDisabled) {
            sLogger.v("n/w disabled permanently");
        }
    }

    public static NetworkBandwidthController getInstance() {
        return singleton;
    }

    private NetworkBandwidthController() {
        this.urlToComponentMap.put("v102-62-30-8.route.hybrid.api.here.com", Component.HERE_ROUTE);
        this.urlToComponentMap.put("tpeg.traffic.api.here.com", Component.HERE_TRAFFIC);
        this.urlToComponentMap.put("tpeg.hybrid.api.here.com", Component.HERE_TRAFFIC);
        this.urlToComponentMap.put("download.hybrid.api.here.com", Component.HERE_MAP_DOWNLOAD);
        this.urlToComponentMap.put("reverse.geocoder.api.here.com", Component.HERE_REVERSE_GEO);
        this.urlToComponentMap.put("analytics.localytics.com", Component.LOCALYTICS);
        this.urlToComponentMap.put("profile.localytics.com", Component.LOCALYTICS);
        this.urlToComponentMap.put("sdk.hockeyapp.net", Component.HOCKEY);
        this.urlToComponentMap.put("navdyhud.atlassian.net", Component.JIRA);
        this.componentInfoMap.put(Component.LOCALYTICS, new ComponentInfo());
        this.componentInfoMap.put(Component.HERE_ROUTE, new ComponentInfo());
        this.componentInfoMap.put(Component.HERE_TRAFFIC, new ComponentInfo());
        this.componentInfoMap.put(Component.HERE_MAP_DOWNLOAD, new ComponentInfo());
        this.componentInfoMap.put(Component.HERE_REVERSE_GEO, new ComponentInfo());
        this.componentInfoMap.put(Component.HOCKEY, new ComponentInfo());
        this.componentInfoMap.put(Component.JIRA, new ComponentInfo());
        this.networkStatThread = new Thread(this.networkStatRunnable);
        this.networkStatThread.setName("hudNetStatThread");
        this.networkStatThread.start();
        sLogger.v("networkStat thread started");
        this.handler.postDelayed(this.dataCollectionRunnable, (long) DATA_COLLECTION_INITIAL_INTERVAL);
        this.limitBandwidthModeOn = DriverProfileHelper.getInstance().getDriverProfileManager().getCurrentProfile().isLimitBandwidthModeOn();
        this.bus.register(this);
        sLogger.v("registered bus");
    }

    public void netStat() {
        this.handler.removeCallbacks(this.dataCollectionRunnable);
        this.dataCollectionRunnable.run();
    }

    public long getLastActivityTime(Component component) {
        ComponentInfo componentInfo = (ComponentInfo) this.componentInfoMap.get(component);
        if (componentInfo == null) {
            return 0;
        }
        long j;
        synchronized (componentInfo) {
            j = componentInfo.lastActivity;
        }
        return j;
    }

    public boolean isTrafficDataDownloadedOnce() {
        return this.trafficDataDownloadedOnce;
    }

    public boolean isNetworkAccessAllowed(Component component) {
        if (NetworkStateManager.isConnectedToNetwork(HudApplication.getAppContext())) {
            switch (component) {
                case LOCALYTICS:
                case JIRA:
                case HOCKEY:
                    if (isComponentActive(Component.HERE_ROUTE)) {
                        sLogger.v("n/w access not allowed:" + component);
                        return false;
                    } else if (isComponentActive(Component.HERE_TRAFFIC)) {
                        sLogger.v("n/w access not allowed:" + component);
                        return false;
                    } else {
                        sLogger.v("n/w access allowed:" + component);
                        return true;
                    }
                default:
                    sLogger.v("n/w access allowed:" + component);
                    return true;
            }
        }
        sLogger.v("n/w access: not connected to network:" + component);
        return false;
    }

    private boolean isComponentActive(Component component) {
        long l = getLastActivityTime(component);
        if (l > 0) {
            long diff = SystemClock.elapsedRealtime() - l;
            if (diff <= ((long) ACTIVE)) {
                sLogger.v("component is ACTIVE:" + component + " diff:" + diff);
                return true;
            }
        }
        return false;
    }

    public List<NetworkStatCacheInfo> getSessionStat() {
        return this.statCache.getSessionStat();
    }

    public List<NetworkStatCacheInfo> getBootStat() {
        return this.statCache.getBootStat();
    }

    public boolean isNetworkDisabled() {
        return networkDisabled;
    }

    public boolean isLimitBandwidthModeOn() {
        return this.limitBandwidthModeOn;
    }

    @Subscribe
    public void onDriverProfileChanged(DriverProfileChanged profileChanged) {
        sLogger.v("driver profile changed");
        handleBandwidthPreferenceChange();
    }

    @Subscribe
    public void onDriverProfileUpdated(DriverProfileUpdated event) {
        sLogger.v("driver profile updated");
        if (event.state == State.UPDATED) {
            handleBandwidthPreferenceChange();
        }
    }

    private void handleBandwidthPreferenceChange() {
        boolean val = DriverProfileHelper.getInstance().getDriverProfileManager().getCurrentProfile().isLimitBandwidthModeOn();
        if (val == this.limitBandwidthModeOn) {
            sLogger.v("limitbandwidth: no-op current[" + this.limitBandwidthModeOn + "] new [" + val + "]");
            return;
        }
        sLogger.v("limitbandwidth: changed current[" + this.limitBandwidthModeOn + "] new [" + val + "]");
        this.limitBandwidthModeOn = val;
        if (val) {
            sLogger.v("limitbandwidth: on");
        } else {
            sLogger.v("limitbandwidth: off");
        }
        HereMapsManager.getInstance().handleBandwidthPreferenceChange();
        this.bus.post(BW_SETTING_CHANGED);
    }
}
