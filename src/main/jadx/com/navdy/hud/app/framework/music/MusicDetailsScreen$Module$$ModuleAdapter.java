package com.navdy.hud.app.framework.music;

import com.navdy.hud.app.framework.music.MusicDetailsScreen.Module;
import dagger.internal.ModuleAdapter;

public final class MusicDetailsScreen$Module$$ModuleAdapter extends ModuleAdapter<Module> {
    private static final Class<?>[] INCLUDES = new Class[0];
    private static final String[] INJECTS = new String[]{"members/com.navdy.hud.app.framework.music.MusicDetailsView"};
    private static final Class<?>[] STATIC_INJECTIONS = new Class[0];

    public MusicDetailsScreen$Module$$ModuleAdapter() {
        super(Module.class, INJECTS, STATIC_INJECTIONS, false, INCLUDES, true, false);
    }
}
