package com.navdy.hud.app.framework.fuel;

import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import com.here.android.mpa.common.GeoCoordinate;
import com.here.android.mpa.common.RoadElement;
import com.here.android.mpa.guidance.NavigationManager.Error;
import com.here.android.mpa.routing.Route;
import com.here.android.mpa.routing.RouteElement;
import com.here.android.mpa.routing.RouteElements;
import com.here.android.mpa.routing.RouteOptions;
import com.here.android.mpa.routing.RouteOptions.TransportMode;
import com.here.android.mpa.routing.RouteOptions.Type;
import com.here.android.mpa.routing.RoutingError;
import com.here.android.mpa.search.CategoryFilter;
import com.here.android.mpa.search.Location;
import com.here.android.mpa.search.Place;
import com.here.services.location.network.NetworkLocationApi.Options;
import com.loopj.android.http.AsyncHttpClient;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.R;
import com.navdy.hud.app.framework.glance.GlanceConstants;
import com.navdy.hud.app.framework.glance.GlanceHelper;
import com.navdy.hud.app.framework.notifications.NotificationManager;
import com.navdy.hud.app.framework.toast.ToastManager;
import com.navdy.hud.app.framework.toast.ToastManager.ToastParams;
import com.navdy.hud.app.framework.toast.ToastPresenter;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.maps.MapEvents.NewRouteAdded;
import com.navdy.hud.app.maps.here.HereMapsManager;
import com.navdy.hud.app.maps.here.HereNavigationManager;
import com.navdy.hud.app.maps.here.HerePlacesManager;
import com.navdy.hud.app.maps.here.HerePlacesManager.OnCategoriesSearchListener;
import com.navdy.hud.app.maps.here.HereRouteCache;
import com.navdy.hud.app.maps.here.HereRouteCalculator;
import com.navdy.hud.app.maps.here.HereRouteCalculator.RouteCalculatorListener;
import com.navdy.hud.app.obd.ObdManager;
import com.navdy.hud.app.obd.ObdManager.ObdPidChangeEvent;
import com.navdy.hud.app.util.GenericUtil;
import com.navdy.service.library.events.destination.Destination.FavoriteType;
import com.navdy.service.library.events.glances.FuelConstants;
import com.navdy.service.library.events.glances.GlanceEvent;
import com.navdy.service.library.events.glances.GlanceEvent.GlanceType;
import com.navdy.service.library.events.glances.KeyValue;
import com.navdy.service.library.events.location.Coordinate;
import com.navdy.service.library.events.location.Coordinate.Builder;
import com.navdy.service.library.events.navigation.NavigationRouteRequest;
import com.navdy.service.library.events.navigation.NavigationRouteRequest.RouteAttribute;
import com.navdy.service.library.events.navigation.NavigationRouteResult;
import com.navdy.service.library.events.navigation.NavigationSessionRequest;
import com.navdy.service.library.events.navigation.NavigationSessionRouteChange.RerouteReason;
import com.navdy.service.library.events.navigation.NavigationSessionState;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.task.TaskManager;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

public final class FuelRoutingManager {
    private static final int CANCEL_NOTIFICATION_THRESHOLD = 20;
    private static final int DEFAULT_LOW_FUEL_THRESHOLD = 15;
    public static final String FINDING_FAST_STATION_TOAST_ID = "toast#find#gas";
    private static final long FUEL_GLANCE_REDISPLAY_THRESHOLD = TimeUnit.MINUTES.toMillis(TEST_SIMULATION_SPEED);
    public static final CategoryFilter GAS_CATEGORY = new CategoryFilter();
    private static final String GAS_STATION_CATEGORY = "petrol-station";
    public static final String LOW_FUEL_ID = GlanceHelper.getNotificationId(GlanceType.GLANCE_TYPE_FUEL, LOW_FUEL_ID_STR);
    private static final String LOW_FUEL_ID_STR = "low#fuel#level";
    private static final int N_GAS_STATIONS_REQUESTED = 3;
    private static final int ONE_MINUTE_SECONDS = 60;
    private static final long TEST_SIMULATION_SPEED = 20;
    private static FuelRoutingManager sInstance = new FuelRoutingManager();
    private static final Logger sLogger = new Logger(FuelRoutingManager.class);
    private boolean available;
    private final Bus bus = RemoteDeviceManager.getInstance().getBus();
    private Place currentGasStation;
    private int currentLowFuelThreshold = 15;
    private NavigationRouteRequest currentNavigationRouteRequest;
    private ArrayList<NavigationRouteResult> currentOutgoingResults;
    private long fuelGlanceDismissTime;
    private RouteOptions gasRouteOptions;
    private final Handler handler = new Handler(Looper.getMainLooper());
    private final HereMapsManager hereMapsManager = HereMapsManager.getInstance();
    private HereNavigationManager hereNavigationManager;
    private HereRouteCalculator hereRouteCalculator;
    private boolean isCalculatingFuelRoute;
    private boolean isInTestMode = false;
    private final ObdManager obdManager = ObdManager.getInstance();

    public interface OnRouteToGasStationCallback {

        public enum Error {
            BAD_REQUEST,
            RESPONSE_ERROR,
            NO_USER_LOCATION,
            NO_ROUTES,
            UNKNOWN_ERROR,
            INVALID_STATE
        }

        void onError(Error error);

        void onOptimalRouteCalculationComplete(RouteCacheItem routeCacheItem, ArrayList<NavigationRouteResult> arrayList, NavigationRouteRequest navigationRouteRequest);
    }

    public static class ClearTestObdLowFuelLevel {
    }

    public static class FuelAddedTestEvent {
    }

    private class FuelRoutingAction implements Runnable {
        private int currentRouteCacheSize;
        private final GeoCoordinate endPoint;
        private final OnRouteToGasStationCallback onRouteToGasStationCallback;
        private final Place place;
        private final List<RouteCacheItem> routeCache;
        private final Queue<FuelRoutingAction> routingQueue;
        private final GeoCoordinate routingStart;
        private final List<GeoCoordinate> waypoints;

        public FuelRoutingAction(int placesSize, Place place, Queue<FuelRoutingAction> routingQueue, List<RouteCacheItem> routeCache, OnRouteToGasStationCallback onRouteToGasStationCallback, GeoCoordinate routingStart, List<GeoCoordinate> waypoints, GeoCoordinate endPoint) {
            this.place = place;
            this.routingQueue = routingQueue;
            this.routeCache = routeCache;
            this.onRouteToGasStationCallback = onRouteToGasStationCallback;
            this.routingStart = routingStart;
            this.waypoints = waypoints;
            this.endPoint = endPoint;
            this.currentRouteCacheSize = placesSize;
        }

        public void run() {
            if (FuelRoutingManager.this.isCalculatingFuelRoute) {
                FuelRoutingManager.this.hereRouteCalculator.calculateRoute(null, this.routingStart, this.waypoints, this.endPoint, false, new RouteCalculatorListener() {
                    public void preSuccess() {
                    }

                    public void postSuccess(ArrayList<NavigationRouteResult> outgoingResults) {
                        if (FuelRoutingManager.sLogger.isLoggable(2)) {
                            FuelRoutingManager.sLogger.v("Calculated route to " + FuelRoutingAction.this.place.getName() + " with no errors");
                        }
                        FuelRoutingAction.this.routeCache.add(new RouteCacheItem((NavigationRouteResult) outgoingResults.get(0), FuelRoutingAction.this.place));
                        if (FuelRoutingAction.this.routeCache.size() == FuelRoutingAction.this.currentRouteCacheSize) {
                            FuelRoutingManager.this.calculateOptimalGasStation(FuelRoutingAction.this.routeCache, FuelRoutingAction.this.onRouteToGasStationCallback);
                        } else {
                            FuelRoutingManager.this.handler.post((Runnable) FuelRoutingAction.this.routingQueue.poll());
                        }
                    }

                    public void error(RoutingError error, Throwable t) {
                        FuelRoutingManager.sLogger.w("Calculated route to " + FuelRoutingAction.this.place.getName() + " with error " + error.name());
                        FuelRoutingAction.this.currentRouteCacheSize = FuelRoutingAction.this.currentRouteCacheSize - 1;
                        if (FuelRoutingAction.this.routeCache.size() == FuelRoutingAction.this.currentRouteCacheSize) {
                            FuelRoutingManager.this.calculateOptimalGasStation(FuelRoutingAction.this.routeCache, FuelRoutingAction.this.onRouteToGasStationCallback);
                        } else {
                            FuelRoutingManager.this.handler.post((Runnable) FuelRoutingAction.this.routingQueue.poll());
                        }
                    }

                    public void progress(int progress) {
                        if (FuelRoutingManager.sLogger.isLoggable(2)) {
                            FuelRoutingManager.sLogger.v("Calculating route to " + FuelRoutingAction.this.place.getName() + "; progress %: " + progress);
                        }
                    }
                }, 1, FuelRoutingManager.this.gasRouteOptions, false, false, false);
            }
        }
    }

    public interface OnNearestGasStationCallback {
        void onComplete(NavigationRouteResult navigationRouteResult);

        void onError(Error error);
    }

    private static class RouteCacheItem {
        Place gasStation;
        NavigationRouteResult route;

        public RouteCacheItem(NavigationRouteResult route, Place gasStation) {
            this.route = route;
            this.gasStation = gasStation;
        }
    }

    public static class TestObdLowFuelLevel {
    }

    static {
        GAS_CATEGORY.add("petrol-station");
    }

    @Nullable
    public static FuelRoutingManager getInstance() {
        return sInstance;
    }

    private FuelRoutingManager() {
    }

    @Subscribe
    public void onObdPidChangeEvent(ObdPidChangeEvent event) {
        if (event.pid.getId() == 47) {
            checkFuelLevel();
        }
    }

    @Subscribe
    public void onTestObdLowFuelLevel(TestObdLowFuelLevel event) {
        if (!this.isInTestMode) {
            this.isInTestMode = true;
            checkFuelLevel();
        }
    }

    @Subscribe
    public void onFuelAddedTestEvent(FuelAddedTestEvent event) {
        setFuelLevelBackToNormal();
    }

    @Subscribe
    public void onClearTestObdLowFuelLevel(ClearTestObdLowFuelLevel event) {
        reset();
        this.fuelGlanceDismissTime = 0;
    }

    @Subscribe
    public void onNewRouteAdded(NewRouteAdded event) {
        if (sLogger.isLoggable(2)) {
            sLogger.v("new route added, resetting gas routes");
        }
        NotificationManager notificationManager = NotificationManager.getInstance();
        if (event.rerouteReason == null && notificationManager.isNotificationPresent(LOW_FUEL_ID)) {
            notificationManager.removeNotification(LOW_FUEL_ID);
        }
    }

    public void routeToGasStation() {
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                FuelRoutingManager.sLogger.v("routeToGasStation");
                if (!FuelRoutingManager.this.available) {
                    FuelRoutingManager.sLogger.w("Fuel manager not available");
                } else if (FuelRoutingManager.this.currentOutgoingResults == null || FuelRoutingManager.this.currentOutgoingResults.size() == 0) {
                    FuelRoutingManager.sLogger.w("outgoing results not available");
                    FuelRoutingManager.this.reset();
                } else if (FuelRoutingManager.this.hereNavigationManager.isNavigationModeOn()) {
                    FuelRoutingManager.sLogger.v("navon: saving current non-gas route to storage");
                    long simulationSpeed = FuelRoutingManager.this.isInTestMode ? FuelRoutingManager.TEST_SIMULATION_SPEED : -1;
                    if (FuelRoutingManager.this.hereNavigationManager.hasArrived()) {
                        FuelRoutingManager.sLogger.v("navon: already arrived, set ignore flag");
                        FuelRoutingManager.this.hereNavigationManager.setIgnoreArrived(true);
                    }
                    Error navError = FuelRoutingManager.this.hereNavigationManager.addNewRoute(FuelRoutingManager.this.currentOutgoingResults, RerouteReason.NAV_SESSION_FUEL_REROUTE, FuelRoutingManager.this.currentNavigationRouteRequest, simulationSpeed);
                    if (navError == Error.NONE) {
                        FuelRoutingManager.sLogger.v("navon:gas route navigation started");
                        return;
                    }
                    FuelRoutingManager.sLogger.e("navon:arrival route navigation started failed:" + navError);
                    FuelRoutingManager.this.reset();
                    NotificationManager.getInstance().removeNotification(FuelRoutingManager.LOW_FUEL_ID);
                } else {
                    NavigationRouteResult result = (NavigationRouteResult) FuelRoutingManager.this.currentOutgoingResults.get(0);
                    if (HereRouteCache.getInstance().getRoute(result.routeId) == null) {
                        FuelRoutingManager.sLogger.w("navoff:route not available in cache:" + result.routeId);
                        FuelRoutingManager.this.reset();
                        return;
                    }
                    FuelRoutingManager.this.hereNavigationManager.handleNavigationSessionRequest(new NavigationSessionRequest(NavigationSessionState.NAV_SESSION_STARTED, result.label, result.routeId, Integer.valueOf(0), Boolean.valueOf(true)));
                    NotificationManager.getInstance().removeNotification(FuelRoutingManager.LOW_FUEL_ID);
                    FuelRoutingManager.sLogger.v("navoff:gas route navigation started");
                }
            }
        }, 21);
    }

    public void dismissGasRoute() {
        sLogger.v("dismissGasRoute");
        int i = this.currentLowFuelThreshold / 2;
        this.currentLowFuelThreshold = i;
        reset(i);
        this.fuelGlanceDismissTime = SystemClock.elapsedRealtime();
    }

    public Place getCurrentGasStation() {
        return this.currentGasStation;
    }

    public void findNearestGasStation(final OnNearestGasStationCallback callback) {
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                if (!FuelRoutingManager.this.available) {
                    FuelRoutingManager.sLogger.w("Fuel manager not available");
                } else if (callback == null) {
                    throw new IllegalArgumentException();
                } else if (FuelRoutingManager.this.isCalculatingFuelRoute) {
                    callback.onError(Error.INVALID_STATE);
                } else {
                    FuelRoutingManager.this.isCalculatingFuelRoute = true;
                    FuelRoutingManager.this.calculateGasStationRoutes(new OnRouteToGasStationCallback() {
                        public void onOptimalRouteCalculationComplete(RouteCacheItem optimal, ArrayList<NavigationRouteResult> outgoingResults, NavigationRouteRequest navigationRouteRequest) {
                            callback.onComplete((NavigationRouteResult) outgoingResults.get(0));
                        }

                        public void onError(Error error) {
                            callback.onError(Error.RESPONSE_ERROR);
                        }
                    });
                }
            }
        }, 21);
    }

    private void reset(int fuelThreshold) {
        this.hereRouteCalculator.cancel();
        if (this.hereNavigationManager.isOnGasRoute()) {
            this.hereNavigationManager.arrived();
        }
        this.currentLowFuelThreshold = fuelThreshold;
        this.isInTestMode = false;
        this.currentOutgoingResults = null;
        this.currentNavigationRouteRequest = null;
        this.currentGasStation = null;
        if (sLogger.isLoggable(2)) {
            sLogger.v("reset, state is now TRACKING");
        }
    }

    public void reset() {
        sLogger.v("reset");
        reset(15);
    }

    private void checkFuelLevel() {
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                int fuelLevel = FuelRoutingManager.this.obdManager.getFuelLevel();
                if (!FuelRoutingManager.this.isInTestMode && fuelLevel == -1) {
                    FuelRoutingManager.sLogger.d("checkFuelLevel:fuel level is not available from OBD Manager");
                    NotificationManager.getInstance().removeNotification(FuelRoutingManager.LOW_FUEL_ID);
                } else if (FuelRoutingManager.this.isCalculatingFuelRoute) {
                    FuelRoutingManager.sLogger.d("checkFuelLevel: already calculating fuel route");
                } else if (FuelRoutingManager.this.hereNavigationManager.isOnGasRoute()) {
                    FuelRoutingManager.sLogger.d("checkFuelLevel: already on fuel route");
                } else {
                    if (FuelRoutingManager.this.fuelGlanceDismissTime > 0) {
                        long elapsed = SystemClock.elapsedRealtime() - FuelRoutingManager.this.fuelGlanceDismissTime;
                        if (elapsed < FuelRoutingManager.FUEL_GLANCE_REDISPLAY_THRESHOLD) {
                            FuelRoutingManager.sLogger.d("checkFuelLevel: fuel glance dismiss threshold still valid:" + elapsed);
                            return;
                        } else {
                            FuelRoutingManager.sLogger.d("checkFuelLevel: fuel glance dismiss threshold expired:" + elapsed);
                            FuelRoutingManager.this.fuelGlanceDismissTime = 0;
                        }
                    }
                    FuelRoutingManager.sLogger.i("checkFuelLevel: Fuel level:" + fuelLevel + " threshold:" + FuelRoutingManager.this.currentLowFuelThreshold + " testMode:" + FuelRoutingManager.this.isInTestMode);
                    if (FuelRoutingManager.this.isInTestMode || fuelLevel <= FuelRoutingManager.this.currentLowFuelThreshold) {
                        if (!FuelRoutingManager.this.isInTestMode && !GlanceHelper.isFuelNotificationEnabled()) {
                            FuelRoutingManager.sLogger.i("checkFuelLevel:fuel notifications are not enabled");
                        } else if (NotificationManager.getInstance().isNotificationPresent(FuelRoutingManager.LOW_FUEL_ID)) {
                            FuelRoutingManager.sLogger.i("checkFuelLevel:low fuel glance already shown to user");
                        } else {
                            FuelRoutingManager.this.findGasStations(fuelLevel, true);
                        }
                    } else if (fuelLevel > 20) {
                        FuelRoutingManager.this.setFuelLevelBackToNormal();
                    } else if (FuelRoutingManager.sLogger.isLoggable(2)) {
                        FuelRoutingManager.sLogger.i("checkFuelLevel: no-op");
                    }
                }
            }
        }, 21);
    }

    private void setFuelLevelBackToNormal() {
        sLogger.i("checkFuelLevel:Fuel level normal");
        reset();
        NotificationManager.getInstance().removeNotification(LOW_FUEL_ID);
        this.fuelGlanceDismissTime = 0;
    }

    private void calculateGasStationRoutes(final OnRouteToGasStationCallback onRouteToGasStationCallback) {
        if (onRouteToGasStationCallback == null) {
            throw new IllegalArgumentException();
        }
        final GeoCoordinate gasCalculationCoords = getBestInitialGeo();
        if (gasCalculationCoords == null) {
            sLogger.w("No user location while calculating routes to gas station");
            onRouteToGasStationCallback.onError(Error.NO_USER_LOCATION);
            return;
        }
        HerePlacesManager.handleCategoriesRequest(GAS_CATEGORY, 3, new OnCategoriesSearchListener() {
            public void onCompleted(List<Place> places) {
                try {
                    GenericUtil.checkNotOnMainThread();
                    if (FuelRoutingManager.this.isCalculatingFuelRoute) {
                        if (places.size() > 1) {
                            Collections.sort(places, new Comparator<Place>() {
                                public int compare(Place lhs, Place rhs) {
                                    return (int) (gasCalculationCoords.distanceTo(lhs.getLocation().getCoordinate()) - gasCalculationCoords.distanceTo(rhs.getLocation().getCoordinate()));
                                }
                            });
                        }
                        for (Place p : places) {
                            Location location = p.getLocation();
                            GeoCoordinate gasNavGeo = HerePlacesManager.getPlaceEntry(p);
                            String address = location.getAddress().toString().replace("<br/>", ", ").replace(GlanceConstants.NEWLINE, "");
                            FuelRoutingManager.sLogger.v("gas station name [" + p.getName() + "]" + " address [" + address + "]" + " distance [" + ((int) gasCalculationCoords.distanceTo(gasNavGeo)) + "] meters" + " displayPos [" + location.getCoordinate() + "]" + " navPos [" + gasNavGeo + "]");
                        }
                        Queue<FuelRoutingAction> routingQueue = new LinkedList();
                        List<RouteCacheItem> routeCache = new ArrayList(3);
                        for (Place place : places) {
                            GeoCoordinate endPoint;
                            List<GeoCoordinate> waypoints = new ArrayList();
                            GeoCoordinate placeEntry = HerePlacesManager.getPlaceEntry(place);
                            if (FuelRoutingManager.this.hereNavigationManager.isNavigationModeOn()) {
                                waypoints.add(placeEntry);
                                endPoint = FuelRoutingManager.this.hereNavigationManager.getCurrentRoute().getDestination();
                            } else {
                                endPoint = placeEntry;
                            }
                            routingQueue.add(new FuelRoutingAction(places.size(), place, routingQueue, routeCache, onRouteToGasStationCallback, gasCalculationCoords, waypoints, endPoint));
                        }
                        FuelRoutingManager.sLogger.v("posting to handler");
                        FuelRoutingManager.this.handler.post((Runnable) routingQueue.poll());
                        return;
                    }
                    onRouteToGasStationCallback.onError(Error.INVALID_STATE);
                } catch (Throwable t) {
                    FuelRoutingManager.sLogger.e("calculateGasStationRoutes", t);
                    onRouteToGasStationCallback.onError(Error.UNKNOWN_ERROR);
                }
            }

            public void onError(HerePlacesManager.Error error) {
                GenericUtil.checkNotOnMainThread();
                FuelRoutingManager.sLogger.e("calculateGasStationRoutes error:" + error);
                onRouteToGasStationCallback.onError(Error.RESPONSE_ERROR);
            }
        });
    }

    private GeoCoordinate getBestInitialGeo() {
        GenericUtil.checkNotOnMainThread();
        GeoCoordinate bestInitialGeo = this.hereMapsManager.getLocationFixManager().getLastGeoCoordinate();
        Route route = this.hereNavigationManager.getCurrentRoute();
        if (route == null) {
            return bestInitialGeo;
        }
        RouteElements routeElements = route.getRouteElementsFromLength((int) this.hereNavigationManager.getNavController().getElapsedDistance());
        if (routeElements == null) {
            return bestInitialGeo;
        }
        List<RouteElement> routeElementsList = routeElements.getElements();
        if (routeElementsList == null) {
            return bestInitialGeo;
        }
        GeoCoordinate oneMinuteFromNowGeo = null;
        int timeCount = 0;
        for (RouteElement routeElement : routeElementsList) {
            RoadElement roadElement = routeElement.getRoadElement();
            if (roadElement != null && roadElement.getDefaultSpeed() > 0.0f) {
                timeCount = (int) (((long) timeCount) + Math.round(roadElement.getGeometryLength() / ((double) roadElement.getDefaultSpeed())));
            }
            if (timeCount > 60) {
                List<GeoCoordinate> geometry = routeElement.getGeometry();
                if (geometry != null && geometry.size() > 0) {
                    oneMinuteFromNowGeo = (GeoCoordinate) geometry.get(geometry.size() - 1);
                    break;
                }
            }
        }
        if (oneMinuteFromNowGeo != null) {
            return oneMinuteFromNowGeo;
        }
        return bestInitialGeo;
    }

    private void calculateOptimalGasStation(List<RouteCacheItem> routeCache, OnRouteToGasStationCallback onRouteToGasStationCallback) {
        RouteCacheItem optimal = null;
        int minTta = -1;
        for (RouteCacheItem routeCacheItem : routeCache) {
            int tta = routeCacheItem.route.duration.intValue();
            int distance = (int) routeCacheItem.gasStation.getLocation().getCoordinate().distanceTo(this.hereMapsManager.getLocationFixManager().getLastGeoCoordinate());
            sLogger.v("evaluating [" + routeCacheItem.gasStation.getName() + "] distance [" + distance + "] tta [" + tta + "]");
            if (minTta < 0 || tta < minTta) {
                minTta = tta;
                optimal = routeCacheItem;
            }
        }
        if (optimal != null) {
            sLogger.v("Optimal route: \n\tName:" + optimal.gasStation.getName() + "\n\tAddress: " + optimal.gasStation.getLocation().getAddress() + "\n\tTTA: " + minTta + " s\n\tGas station distance: " + optimal.gasStation.getLocation().getCoordinate().distanceTo(this.hereMapsManager.getLocationFixManager().getLastGeoCoordinate()) + " m");
            requestOptimalRoute(optimal, onRouteToGasStationCallback);
            return;
        }
        sLogger.w("No routes to any gas stations");
        onRouteToGasStationCallback.onError(Error.NO_ROUTES);
    }

    private void requestOptimalRoute(RouteCacheItem gasRoute, OnRouteToGasStationCallback onRouteToGasStationCallback) {
        try {
            GeoCoordinate gasNavGeo = HerePlacesManager.getPlaceEntry(gasRoute.gasStation);
            GeoCoordinate gasDisplayGeo = gasRoute.gasStation.getLocation().getCoordinate();
            Coordinate navigationPosition = new Builder().latitude(Double.valueOf(gasNavGeo.getLatitude())).longitude(Double.valueOf(gasNavGeo.getLongitude())).build();
            Coordinate displayPosition = new Builder().latitude(Double.valueOf(gasDisplayGeo.getLatitude())).longitude(Double.valueOf(gasDisplayGeo.getLongitude())).build();
            List<RouteAttribute> arrayList = new ArrayList(1);
            arrayList.add(RouteAttribute.ROUTE_ATTRIBUTE_GAS);
            final NavigationRouteRequest navigationRouteRequest = new NavigationRouteRequest.Builder().destination(navigationPosition).label(gasRoute.gasStation.getName()).streetAddress(gasRoute.gasStation.getLocation().getAddress().toString()).destination_identifier(gasRoute.gasStation.getId()).originDisplay(Boolean.valueOf(true)).geoCodeStreetAddress(Boolean.valueOf(false)).destinationType(FavoriteType.FAVORITE_NONE).requestId(UUID.randomUUID().toString()).destinationDisplay(displayPosition).routeAttributes(arrayList).build();
            final OnRouteToGasStationCallback onRouteToGasStationCallback2 = onRouteToGasStationCallback;
            final RouteCacheItem routeCacheItem = gasRoute;
            this.hereRouteCalculator.calculateRoute(navigationRouteRequest, this.hereMapsManager.getLocationFixManager().getLastGeoCoordinate(), null, gasNavGeo, true, new RouteCalculatorListener() {
                public void preSuccess() {
                }

                public void postSuccess(ArrayList<NavigationRouteResult> outgoingResults) {
                    onRouteToGasStationCallback2.onOptimalRouteCalculationComplete(routeCacheItem, outgoingResults, navigationRouteRequest);
                }

                public void error(RoutingError error, Throwable t) {
                    onRouteToGasStationCallback2.onError(Error.RESPONSE_ERROR);
                }

                public void progress(int progress) {
                    if (FuelRoutingManager.sLogger.isLoggable(2)) {
                        FuelRoutingManager.sLogger.v("calculating route to optimal gas station progress %: " + progress);
                    }
                }
            }, 1, this.gasRouteOptions, true, true, false);
        } catch (Throwable t) {
            sLogger.e("requestOptimalRoute", t);
            onRouteToGasStationCallback.onError(Error.UNKNOWN_ERROR);
        }
    }

    public void findGasStations(final int fuelLevel, final boolean postNotificationOnError) {
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                if (FuelRoutingManager.this.available) {
                    FuelRoutingManager.this.isCalculatingFuelRoute = true;
                    FuelRoutingManager.this.calculateGasStationRoutes(new OnRouteToGasStationCallback() {
                        public void onOptimalRouteCalculationComplete(final RouteCacheItem optimal, final ArrayList<NavigationRouteResult> outgoingResults, final NavigationRouteRequest navigationRouteRequest) {
                            TaskManager.getInstance().execute(new Runnable() {
                                public void run() {
                                    FuelRoutingManager.this.currentGasStation = optimal.gasStation;
                                    FuelRoutingManager.this.isCalculatingFuelRoute = false;
                                    FuelRoutingManager.this.currentOutgoingResults = outgoingResults;
                                    FuelRoutingManager.this.currentNavigationRouteRequest = navigationRouteRequest;
                                    String distanceString = String.valueOf(((double) Math.round((optimal.gasStation.getLocation().getCoordinate().distanceTo(FuelRoutingManager.this.hereMapsManager.getLocationFixManager().getLastGeoCoordinate()) * 10.0d) / 1609.34d)) / 10.0d);
                                    String addressString = optimal.gasStation.getLocation().getAddress().getText().split("<br/>")[0];
                                    List<KeyValue> data = new ArrayList(1);
                                    if (fuelLevel != -1) {
                                        data.add(new KeyValue(FuelConstants.FUEL_LEVEL.name(), String.valueOf(FuelRoutingManager.this.obdManager.getFuelLevel())));
                                    }
                                    data.add(new KeyValue(FuelConstants.GAS_STATION_NAME.name(), optimal.gasStation.getName()));
                                    data.add(new KeyValue(FuelConstants.GAS_STATION_ADDRESS.name(), addressString));
                                    data.add(new KeyValue(FuelConstants.GAS_STATION_DISTANCE.name(), distanceString));
                                    FuelRoutingManager.this.bus.post(new GlanceEvent.Builder().glanceType(GlanceType.GLANCE_TYPE_FUEL).id(FuelRoutingManager.LOW_FUEL_ID_STR).postTime(Long.valueOf(System.currentTimeMillis())).provider(GlanceHelper.FUEL_PACKAGE).glanceData(data).build());
                                    if (FuelRoutingManager.sLogger.isLoggable(2)) {
                                        FuelRoutingManager.sLogger.v("state is now LOW_FUEL, posting low fuel glance");
                                    }
                                }
                            }, 21);
                        }

                        public void onError(final Error error) {
                            TaskManager.getInstance().execute(new Runnable() {
                                public void run() {
                                    FuelRoutingManager.sLogger.w("received an error on calculateGasStationRoutes " + error.name());
                                    FuelRoutingManager.this.isCalculatingFuelRoute = false;
                                    if (postNotificationOnError) {
                                        List<KeyValue> data = new ArrayList(1);
                                        data.add(new KeyValue(FuelConstants.FUEL_LEVEL.name(), String.valueOf(fuelLevel)));
                                        data.add(new KeyValue(FuelConstants.NO_ROUTE.name(), ""));
                                        FuelRoutingManager.this.bus.post(new GlanceEvent.Builder().glanceType(GlanceType.GLANCE_TYPE_FUEL).id(FuelRoutingManager.LOW_FUEL_ID_STR).postTime(Long.valueOf(System.currentTimeMillis())).provider(GlanceHelper.FUEL_PACKAGE).glanceData(data).build());
                                        if (FuelRoutingManager.sLogger.isLoggable(2)) {
                                            FuelRoutingManager.sLogger.v("posting low fuel glance with no routes");
                                        }
                                        FuelRoutingManager.this.reset();
                                    }
                                }
                            }, 21);
                        }
                    });
                    return;
                }
                FuelRoutingManager.sLogger.w("Fuel manager not available");
            }
        }, 21);
    }

    public void showFindingGasStationToast() {
        Resources resources = HudApplication.getAppContext().getResources();
        ToastManager toastManager = ToastManager.getInstance();
        toastManager.dismissCurrentToast(FINDING_FAST_STATION_TOAST_ID);
        toastManager.clearPendingToast(FINDING_FAST_STATION_TOAST_ID);
        Bundle bundle = new Bundle();
        bundle.putInt(ToastPresenter.EXTRA_TIMEOUT, AsyncHttpClient.DEFAULT_RETRY_SLEEP_TIME_MILLIS);
        bundle.putInt(ToastPresenter.EXTRA_MAIN_IMAGE, R.drawable.icon_glance_fuel_low);
        bundle.putString(ToastPresenter.EXTRA_MAIN_TITLE_2, resources.getString(R.string.search_gas_station));
        bundle.putInt(ToastPresenter.EXTRA_MAIN_TITLE_2_STYLE, R.style.Glances_1);
        toastManager.addToast(new ToastParams(FINDING_FAST_STATION_TOAST_ID, bundle, null, true, false, false));
    }

    public void markAvailable() {
        if (!this.available) {
            this.available = true;
            this.hereNavigationManager = HereNavigationManager.getInstance();
            this.hereRouteCalculator = new HereRouteCalculator(sLogger, false);
            this.gasRouteOptions = new RouteOptions();
            this.gasRouteOptions.setRouteCount(1);
            this.gasRouteOptions.setTransportMode(TransportMode.CAR);
            this.gasRouteOptions.setRouteType(Type.FASTEST);
            this.bus.register(this);
            this.handler.postDelayed(new Runnable() {
                public void run() {
                    FuelRoutingManager.this.checkFuelLevel();
                }
            }, Options.MIN_DESIRED_INTERVAL);
            sLogger.v("mark available");
        }
    }

    public boolean isAvailable() {
        return this.available;
    }

    public boolean isBusy() {
        return this.isCalculatingFuelRoute || this.hereNavigationManager.isOnGasRoute() || NotificationManager.getInstance().isNotificationPresent(LOW_FUEL_ID);
    }

    public long getFuelGlanceDismissTime() {
        return this.fuelGlanceDismissTime;
    }
}
