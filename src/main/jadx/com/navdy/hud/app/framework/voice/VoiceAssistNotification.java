package com.navdy.hud.app.framework.voice;

import android.animation.AnimatorSet;
import android.content.Context;
import android.content.res.Resources;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.R;
import com.navdy.hud.app.event.RemoteEvent;
import com.navdy.hud.app.framework.notifications.INotification;
import com.navdy.hud.app.framework.notifications.INotificationController;
import com.navdy.hud.app.framework.notifications.NotificationId;
import com.navdy.hud.app.framework.notifications.NotificationManager;
import com.navdy.hud.app.framework.notifications.NotificationType;
import com.navdy.hud.app.framework.toast.ToastManager;
import com.navdy.hud.app.manager.InputManager.CustomKeyEvent;
import com.navdy.hud.app.manager.InputManager.IInputHandler;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.ui.component.FluctuatorAnimatorView;
import com.navdy.hud.app.ui.framework.UIStateManager.Mode;
import com.navdy.service.library.events.DeviceInfo.Platform;
import com.navdy.service.library.events.audio.VoiceAssistRequest;
import com.navdy.service.library.events.audio.VoiceAssistResponse;
import com.navdy.service.library.events.input.GestureEvent;
import com.navdy.service.library.events.input.KeyEvent;
import com.navdy.service.library.events.input.MediaRemoteKey;
import com.navdy.service.library.events.input.MediaRemoteKeyEvent;
import com.navdy.service.library.log.Logger;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

public class VoiceAssistNotification implements INotification {
    private static final int DEFAULT_VOICE_ASSIST_TIMEOUT = 4000;
    private static final String EMPTY = "";
    private static final int SIRI_STARTING_TIMEOUT = 5000;
    private static final int SIRI_TIMEOUT = 200000;
    private static final Bus bus = RemoteDeviceManager.getInstance().getBus();
    private static int gnow_color;
    private static String google_now;
    private static final Logger sLogger = new Logger(VoiceAssistNotification.class);
    private static String siri;
    private static String siriIsActive;
    private static String siriIsStarting;
    private static int siri_color;
    private static String voice_assist_disabled;
    private static String voice_assist_na;
    private INotificationController controller;
    private FluctuatorAnimatorView fluctuator;
    private ImageView image;
    private ViewGroup layout;
    private Handler mSiriTimeoutHandler;
    private Runnable mSiriTimeoutRunnable;
    private TextView status;
    private TextView subStatus;
    private boolean waitingAfterTrigger = false;

    static {
        Resources resources = HudApplication.getAppContext().getResources();
        google_now = resources.getString(R.string.google_now);
        siri = resources.getString(R.string.siri);
        voice_assist_na = resources.getString(R.string.voice_assist_not_available);
        voice_assist_disabled = resources.getString(R.string.voice_assist_disabled);
        siriIsActive = resources.getString(R.string.siri_active);
        siriIsStarting = resources.getString(R.string.siri_starting);
        gnow_color = resources.getColor(R.color.grey_fluctuator);
        siri_color = resources.getColor(17170443);
    }

    public NotificationType getType() {
        return NotificationType.VOICE_ASSIST;
    }

    public String getId() {
        return NotificationId.VOICE_ASSIST_NOTIFICATION_ID;
    }

    public View getExpandedView(Context context, Object data) {
        return null;
    }

    public int getExpandedViewIndicatorColor() {
        return 0;
    }

    public void onStart(INotificationController controller) {
        if (this.layout != null) {
            this.controller = controller;
            bus.register(this);
            int color = 0;
            RemoteDeviceManager remoteDeviceManager = RemoteDeviceManager.getInstance();
            boolean deviceConnected = remoteDeviceManager.isRemoteDeviceConnected();
            Platform remoteDevicePlatform = deviceConnected ? remoteDeviceManager.getRemoteDevicePlatform() : null;
            if (!deviceConnected || remoteDevicePlatform == null) {
                this.status.setText(voice_assist_na);
                this.subStatus.setText("");
                this.image.setImageResource(R.drawable.icon_voice);
            } else {
                switch (remoteDevicePlatform) {
                    case PLATFORM_Android:
                        this.status.setText(google_now);
                        this.subStatus.setText("");
                        this.image.setImageResource(R.drawable.icon_googlenow_off);
                        color = gnow_color;
                        break;
                    case PLATFORM_iOS:
                        color = siri_color;
                        this.status.setText(siri);
                        this.subStatus.setText(siriIsStarting);
                        this.image.setImageResource(R.drawable.icon_siri);
                        break;
                }
                sLogger.i("sending VoiceAssistRequest");
                bus.post(new RemoteEvent(new VoiceAssistRequest(Boolean.valueOf(false))));
            }
            this.fluctuator.setFillColor(color);
            this.fluctuator.setStrokeColor(color);
            if (false) {
                this.fluctuator.setVisibility(0);
                this.fluctuator.start();
                return;
            }
            this.fluctuator.setVisibility(4);
            this.fluctuator.stop();
        }
    }

    public void onStop() {
        this.controller = null;
        bus.unregister(this);
        if (this.layout != null) {
            this.fluctuator.stop();
        }
        if (this.mSiriTimeoutHandler != null) {
            this.mSiriTimeoutHandler.removeCallbacks(this.mSiriTimeoutRunnable);
        }
        ToastManager.getInstance().disableToasts(false);
        sLogger.v("startVoiceAssistance: enabled toast");
    }

    public View getView(Context context) {
        if (this.layout == null) {
            this.layout = (ViewGroup) LayoutInflater.from(context).inflate(R.layout.notification_voice_assist, null);
            this.status = (TextView) this.layout.findViewById(R.id.status);
            this.subStatus = (TextView) this.layout.findViewById(R.id.subStatus);
            this.fluctuator = (FluctuatorAnimatorView) this.layout.findViewById(R.id.fluctuator);
            this.image = (ImageView) this.layout.findViewById(R.id.image);
        }
        return this.layout;
    }

    public void onUpdate() {
    }

    public int getTimeout() {
        RemoteDeviceManager remoteDeviceManager = RemoteDeviceManager.getInstance();
        boolean deviceConnected = remoteDeviceManager.isRemoteDeviceConnected();
        Platform remoteDevicePlatform = deviceConnected ? remoteDeviceManager.getRemoteDevicePlatform() : null;
        if (!deviceConnected || remoteDevicePlatform == null) {
            return DEFAULT_VOICE_ASSIST_TIMEOUT;
        }
        switch (remoteDevicePlatform) {
            case PLATFORM_iOS:
                return SIRI_TIMEOUT;
            default:
                return DEFAULT_VOICE_ASSIST_TIMEOUT;
        }
    }

    public boolean isAlive() {
        return false;
    }

    public boolean isPurgeable() {
        return false;
    }

    public boolean canAddToStackIfCurrentExists() {
        return false;
    }

    public int getColor() {
        return 0;
    }

    public void onNotificationEvent(Mode mode) {
    }

    public void onExpandedNotificationEvent(Mode mode) {
    }

    public void onExpandedNotificationSwitched() {
    }

    public AnimatorSet getViewSwitchAnimation(boolean viewIn) {
        return null;
    }

    public boolean expandNotification() {
        return false;
    }

    public boolean supportScroll() {
        return false;
    }

    public boolean onKey(CustomKeyEvent event) {
        switch (event) {
            case LONG_PRESS:
                if (!this.waitingAfterTrigger) {
                    sendSiriKeyEvents();
                }
                return true;
            default:
                return false;
        }
    }

    private void sendSiriKeyEvents() {
        sLogger.i("sending siri key events");
        bus.post(new RemoteEvent(new MediaRemoteKeyEvent(MediaRemoteKey.MEDIA_REMOTE_KEY_SIRI, KeyEvent.KEY_DOWN)));
        bus.post(new RemoteEvent(new MediaRemoteKeyEvent(MediaRemoteKey.MEDIA_REMOTE_KEY_SIRI, KeyEvent.KEY_UP)));
    }

    @Subscribe
    public void onVoiceAssistResponse(VoiceAssistResponse response) {
        if (this.controller != null && this.layout != null) {
            if (this.mSiriTimeoutHandler != null) {
                this.mSiriTimeoutHandler.removeCallbacks(this.mSiriTimeoutRunnable);
            }
            RemoteDeviceManager remoteDeviceManager = RemoteDeviceManager.getInstance();
            boolean deviceConnected = remoteDeviceManager.isRemoteDeviceConnected();
            Platform remoteDevicePlatform = deviceConnected ? remoteDeviceManager.getRemoteDevicePlatform() : null;
            String title = "";
            String subTitle = "";
            boolean startFluctuating = false;
            int icon = 0;
            if (deviceConnected && remoteDevicePlatform != null) {
                sLogger.i("received response " + response.state);
                switch (response.state) {
                    case VOICE_ASSIST_AVAILABLE:
                        if (remoteDevicePlatform == Platform.PLATFORM_iOS) {
                            title = siri;
                            subTitle = siriIsStarting;
                            icon = R.drawable.icon_siri;
                            this.waitingAfterTrigger = true;
                            sendSiriKeyEvents();
                            if (this.mSiriTimeoutHandler == null) {
                                this.mSiriTimeoutHandler = new Handler();
                            }
                            this.mSiriTimeoutRunnable = new Runnable() {
                                public void run() {
                                    if (VoiceAssistNotification.this.layout != null) {
                                        VoiceAssistNotification.sLogger.d("Siri request did not get any response and timed out");
                                        VoiceAssistNotification.this.fluctuator.setVisibility(4);
                                        VoiceAssistNotification.this.fluctuator.stop();
                                        VoiceAssistNotification.this.subStatus.setText(VoiceAssistNotification.voice_assist_disabled);
                                        VoiceAssistNotification.this.image.setImageResource(R.drawable.icon_siri);
                                    }
                                }
                            };
                            this.mSiriTimeoutHandler.postDelayed(this.mSiriTimeoutRunnable, 5000);
                            break;
                        }
                        break;
                    case VOICE_ASSIST_NOT_AVAILABLE:
                        if (remoteDevicePlatform == Platform.PLATFORM_Android) {
                            title = google_now;
                            subTitle = voice_assist_disabled;
                            icon = R.drawable.icon_googlenow_off;
                        } else {
                            title = siri;
                            subTitle = voice_assist_disabled;
                            icon = R.drawable.icon_siri;
                        }
                        this.fluctuator.setVisibility(4);
                        this.fluctuator.stop();
                        this.waitingAfterTrigger = false;
                        break;
                    case VOICE_ASSIST_STOPPED:
                        this.fluctuator.setVisibility(4);
                        this.fluctuator.stop();
                        dismissNotification();
                        this.waitingAfterTrigger = false;
                        return;
                    case VOICE_ASSIST_LISTENING:
                        if (remoteDevicePlatform != Platform.PLATFORM_Android) {
                            title = siri;
                            subTitle = siriIsActive;
                            icon = R.drawable.icon_siri;
                            startFluctuating = true;
                            if (!this.waitingAfterTrigger) {
                                sendSiriKeyEvents();
                                break;
                            } else {
                                this.waitingAfterTrigger = false;
                                break;
                            }
                        }
                        title = google_now;
                        icon = R.drawable.icon_googlenow_on;
                        startFluctuating = true;
                        break;
                }
            }
            title = voice_assist_na;
            icon = R.drawable.icon_voice;
            this.fluctuator.stop();
            this.fluctuator.setVisibility(4);
            this.status.setText(title);
            this.subStatus.setText(subTitle);
            this.image.setImageResource(icon);
            if (startFluctuating) {
                this.fluctuator.setVisibility(0);
                this.fluctuator.start();
                return;
            }
            this.fluctuator.setVisibility(4);
            this.fluctuator.stop();
        }
    }

    private void dismissNotification() {
        NotificationManager.getInstance().removeNotification(NotificationId.VOICE_ASSIST_NOTIFICATION_ID);
    }

    public void onClick() {
    }

    public void onTrackHand(float normalized) {
    }

    public boolean onGesture(GestureEvent event) {
        return false;
    }

    public IInputHandler nextHandler() {
        return null;
    }
}
