package com.navdy.hud.app.framework.contacts;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.TextUtils;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.event.DriverProfileChanged;
import com.navdy.hud.app.event.RemoteEvent;
import com.navdy.hud.app.framework.DriverProfileHelper;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.profile.DriverProfile;
import com.navdy.hud.app.util.GenericUtil;
import com.navdy.hud.app.util.picasso.PicassoUtil;
import com.navdy.service.library.events.RequestStatus;
import com.navdy.service.library.events.connection.ConnectionStateChange;
import com.navdy.service.library.events.photo.PhotoRequest;
import com.navdy.service.library.events.photo.PhotoResponse;
import com.navdy.service.library.events.photo.PhotoType;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.task.TaskManager;
import com.navdy.service.library.util.IOUtils;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;
import java.io.File;
import java.io.FileOutputStream;
import java.util.Comparator;
import java.util.HashSet;
import java.util.concurrent.PriorityBlockingQueue;

public class PhoneImageDownloader {
    private static final String MD5_EXTENSION = ".md5";
    private static final boolean VERBOSE = false;
    private static final PhoneImageDownloader sInstance = new PhoneImageDownloader();
    private static final Logger sLogger = new Logger(PhoneImageDownloader.class);
    private Bus bus = RemoteDeviceManager.getInstance().getBus();
    private HashSet<String> contactPhotoChecked = new HashSet();
    private PriorityBlockingQueue<Info> contactQueue = new PriorityBlockingQueue(10, this.priorityComparator);
    private Context context = HudApplication.getAppContext();
    private volatile String currentDriverProfileId;
    private boolean downloading;
    private Object lockObj = new Object();
    private Comparator<Info> priorityComparator = new Comparator<Info>() {
        public int compare(Info lhs, Info rhs) {
            if (!(lhs instanceof Info) || !(rhs instanceof Info)) {
                return 0;
            }
            return lhs.priority - rhs.priority;
        }
    };

    private static class Info {
        String displayName;
        String fileName;
        PhotoType photoType;
        int priority;
        String sourceIdentifier;

        Info(String fileName, Priority priority, PhotoType photoType, String sourceIdentifier, String displayName) {
            this.fileName = fileName;
            this.priority = priority.getValue();
            this.photoType = photoType;
            this.sourceIdentifier = sourceIdentifier;
            this.displayName = displayName;
        }
    }

    public static class PhotoDownloadStatus {
        public boolean alreadyDownloaded;
        public PhotoType photoType;
        public String sourceIdentifier;
        public boolean success;

        PhotoDownloadStatus(String sourceIdentifier, PhotoType photoType, boolean success, boolean alreadyDownloaded) {
            this.sourceIdentifier = sourceIdentifier;
            this.photoType = photoType;
            this.success = success;
            this.alreadyDownloaded = alreadyDownloaded;
        }
    }

    public enum Priority {
        NORMAL(1),
        HIGH(2);
        
        private final int val;

        private Priority(int val) {
            this.val = val;
        }

        public int getValue() {
            return this.val;
        }
    }

    public static final PhoneImageDownloader getInstance() {
        return sInstance;
    }

    private PhoneImageDownloader() {
        this.bus.register(this);
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void submitDownload(String id, Priority priority, PhotoType photoType, String displayName) {
        if (TextUtils.isEmpty(id)) {
            sLogger.w("invalid id passed");
            return;
        }
        String filename = normalizedFilename(id, photoType);
        synchronized (this.lockObj) {
            if (photoType == PhotoType.PHOTO_CONTACT && this.contactPhotoChecked.contains(filename)) {
                sLogger.v("contact photo request has already been processed name[" + filename + "]");
                return;
            }
            sLogger.v("submitting photo request name[" + filename + "] type [" + photoType + "] displayName[" + displayName + "]");
            this.contactQueue.add(new Info(filename, priority, photoType, id, displayName));
            if (!this.downloading) {
                this.downloading = true;
                invokeDownload();
            }
        }
    }

    private void invokeDownload() {
        this.currentDriverProfileId = null;
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                PhoneImageDownloader.this.download();
            }
        }, 7);
    }

    public String hashFor(String id, PhotoType photoType) {
        String hash = null;
        String fileName = normalizedFilename(id, photoType);
        File imageDir = getPhotoPath(DriverProfileHelper.getInstance().getCurrentProfile(), photoType);
        File path = new File(imageDir, fileName);
        if (!path.exists() || path.isDirectory()) {
            return hash;
        }
        File md5 = new File(imageDir, fileName + MD5_EXTENSION);
        if (md5.exists()) {
            try {
                return IOUtils.convertFileToString(md5.getAbsolutePath());
            } catch (Throwable t) {
                sLogger.e("error reading md5[" + fileName + "]", t);
                IOUtils.deleteFile(this.context, path.getAbsolutePath());
                IOUtils.deleteFile(this.context, md5.getAbsolutePath());
                return hash;
            }
        }
        sLogger.w("md5 does not exist, delete file, " + fileName);
        IOUtils.deleteFile(this.context, path.getAbsolutePath());
        return hash;
    }

    private void download() {
        try {
            synchronized (this.lockObj) {
                if (this.contactQueue.size() == 0) {
                    sLogger.v("no request pending");
                    this.downloading = false;
                    return;
                }
                Info info = (Info) this.contactQueue.remove();
                if (info.priority == Priority.NORMAL.getValue()) {
                    GenericUtil.sleep(500);
                }
                this.currentDriverProfileId = DriverProfileHelper.getInstance().getCurrentProfile().getProfileName();
                String hash = hashFor(info.sourceIdentifier, info.photoType);
                if (hash == null) {
                    sLogger.v("sending fresh download request file [" + info.fileName + "] id[" + info.sourceIdentifier + "]");
                } else {
                    sLogger.v("checking for update file [" + info.fileName + "] id[" + info.sourceIdentifier + "]");
                }
                this.bus.post(new RemoteEvent(new PhotoRequest(info.sourceIdentifier, hash, info.photoType, info.displayName)));
            }
        } catch (Throwable t) {
            sLogger.e(t);
            invokeDownload();
        }
    }

    private void purgeQueue() {
        sLogger.i("purging queue");
        this.currentDriverProfileId = null;
        synchronized (this.lockObj) {
            this.contactQueue.clear();
            this.downloading = false;
        }
    }

    @Subscribe
    public void onConnectionStatusChange(ConnectionStateChange event) {
        switch (event.state) {
            case CONNECTION_DISCONNECTED:
                purgeQueue();
                return;
            default:
                return;
        }
    }

    @Subscribe
    public void onDriverProfileChanged(DriverProfileChanged event) {
        purgeQueue();
    }

    @Subscribe
    public void onPhotoResponse(PhotoResponse event) {
        sLogger.v("got photoResponse:" + event.status + " type[" + event.photoType + "] id[" + event.identifier + "]");
        if (this.currentDriverProfileId == null) {
            sLogger.w("currentDriverProfile is null");
            invokeDownload();
            return;
        }
        String filename = normalizedFilename(event.identifier, event.photoType);
        if (event.status == RequestStatus.REQUEST_SUCCESS) {
            this.contactPhotoChecked.add(filename);
            storePhoto(event);
            return;
        }
        if (event.status == RequestStatus.REQUEST_NOT_AVAILABLE) {
            this.contactPhotoChecked.add(filename);
            removeImage(event.identifier, event.photoType);
            sLogger.v("photo not available type[" + event.photoType + "] id[" + event.identifier + "]");
        } else {
            sLogger.v("photo not available type[" + event.photoType + "] id[" + event.identifier + "] error[" + event.status + "]");
        }
        invokeDownload();
    }

    private void storePhoto(final PhotoResponse event) {
        if (event.photo == null) {
            sLogger.v("photo is upto-date type[" + event.photoType + "] id[" + event.identifier + "]");
            this.bus.post(new PhotoDownloadStatus(event.identifier, event.photoType, true, true));
            invokeDownload();
            return;
        }
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                Throwable t;
                Throwable th;
                PhoneImageDownloader.sLogger.v("storing photo");
                FileOutputStream fos = null;
                FileOutputStream fos_hash = null;
                try {
                    byte[] byteArray = event.photo.toByteArray();
                    Bitmap image = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
                    String filename = PhoneImageDownloader.this.normalizedFilename(event.identifier, event.photoType);
                    if (image != null) {
                        DriverProfile driverProfile = DriverProfileHelper.getInstance().getCurrentProfile();
                        if (TextUtils.equals(driverProfile.getProfileName(), PhoneImageDownloader.this.currentDriverProfileId)) {
                            File f = new File(PhoneImageDownloader.this.getPhotoPath(driverProfile, event.photoType), filename);
                            FileOutputStream fos2 = new FileOutputStream(f);
                            try {
                                fos2.write(byteArray);
                                String hash = IOUtils.hashForBytes(byteArray);
                                FileOutputStream fos_hash2 = new FileOutputStream(new File(PhoneImageDownloader.this.getPhotoPath(driverProfile, event.photoType), filename + PhoneImageDownloader.MD5_EXTENSION));
                                try {
                                    fos_hash2.write(hash.getBytes());
                                    PhoneImageDownloader.sLogger.v("photo saved[" + event.photoType + "] id[" + event.identifier + "]");
                                    PicassoUtil.getInstance().invalidate(f);
                                    PhoneImageDownloader.this.bus.post(new PhotoDownloadStatus(event.identifier, event.photoType, true, false));
                                    fos_hash = fos_hash2;
                                    fos = fos2;
                                } catch (Throwable th2) {
                                    th = th2;
                                    fos_hash = fos_hash2;
                                    fos = fos2;
                                    IOUtils.fileSync(fos);
                                    IOUtils.closeStream(fos);
                                    IOUtils.fileSync(fos_hash);
                                    IOUtils.closeStream(fos_hash);
                                    PhoneImageDownloader.this.invokeDownload();
                                    throw th;
                                }
                            } catch (Throwable th3) {
                                th = th3;
                                fos = fos2;
                                IOUtils.fileSync(fos);
                                IOUtils.closeStream(fos);
                                IOUtils.fileSync(fos_hash);
                                IOUtils.closeStream(fos_hash);
                                PhoneImageDownloader.this.invokeDownload();
                                throw th;
                            }
                        }
                        PhoneImageDownloader.sLogger.w("photo cannot save [" + event.photoType + "] id[" + event.identifier + "] profile changed from [" + PhoneImageDownloader.this.currentDriverProfileId + "] to [" + driverProfile.getProfileName() + "]");
                    } else {
                        PhoneImageDownloader.this.bus.post(new PhotoDownloadStatus(event.identifier, event.photoType, false, false));
                    }
                    IOUtils.fileSync(fos);
                    IOUtils.closeStream(fos);
                    IOUtils.fileSync(fos_hash);
                    IOUtils.closeStream(fos_hash);
                    PhoneImageDownloader.this.invokeDownload();
                } catch (Throwable th4) {
                    t = th4;
                }
            }
        }, 7);
    }

    private File getPhotoPath(DriverProfile profile, PhotoType photoType) {
        switch (photoType) {
            case PHOTO_ALBUM_ART:
                return profile.getMusicImageDir();
            case PHOTO_CONTACT:
                return profile.getContactsImageDir();
            case PHOTO_DRIVER_PROFILE:
                return profile.getPreferencesDirectory();
            default:
                return null;
        }
    }

    private String normalizedFilename(String id, PhotoType type) {
        if (type == PhotoType.PHOTO_DRIVER_PROFILE) {
            return DriverProfile.DRIVER_PROFILE_IMAGE;
        }
        return GenericUtil.normalizeToFilename(id);
    }

    public File getImagePath(String id, PhotoType type) {
        return getImagePath(id, "", type);
    }

    public File getImagePath(String id, String suffix, PhotoType type) {
        String filename = normalizedFilename(id, type);
        File f = getPhotoPath(DriverProfileHelper.getInstance().getCurrentProfile(), type);
        if (f == null) {
            return null;
        }
        return new File(f, filename + suffix);
    }

    public void clearPhotoCheckEntry(String id, PhotoType type) {
        String filename = normalizedFilename(id, type);
        synchronized (this.lockObj) {
            if (this.contactPhotoChecked.remove(filename)) {
                sLogger.v("photo check entry removed [" + filename + "]");
            }
        }
    }

    public void clearAllPhotoCheckEntries() {
        synchronized (this.lockObj) {
            this.contactPhotoChecked.clear();
            sLogger.v("cleared all photo check entries");
        }
    }

    private void removeImage(final String id, final PhotoType photoType) {
        if (id == null || photoType == null) {
            sLogger.i("null param");
            return;
        }
        final File file = getImagePath(id, photoType);
        PicassoUtil.getInstance().invalidate(file);
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                if (IOUtils.deleteFile(PhoneImageDownloader.this.context, file.getAbsolutePath())) {
                    PhoneImageDownloader.sLogger.v("removed [" + id + "]");
                }
                PhoneImageDownloader.this.bus.post(new PhotoDownloadStatus(id, photoType, false, false));
            }
        }, 7);
    }
}
