package com.navdy.hud.app.framework.message;

import android.text.TextUtils;
import com.navdy.hud.app.framework.contacts.ContactUtil;
import com.navdy.hud.app.framework.contacts.NumberType;
import com.navdy.hud.app.framework.glance.GlanceHelper;
import com.navdy.hud.app.framework.recentcall.RecentCall;
import com.navdy.hud.app.framework.recentcall.RecentCall.CallType;
import com.navdy.hud.app.framework.recentcall.RecentCall.Category;
import com.navdy.hud.app.framework.recentcall.RecentCallManager;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.service.library.events.glances.GlanceEvent.Builder;
import com.navdy.service.library.events.glances.GlanceEvent.GlanceActions;
import com.navdy.service.library.events.glances.GlanceEvent.GlanceType;
import com.navdy.service.library.events.glances.KeyValue;
import com.navdy.service.library.events.glances.MessageConstants;
import com.navdy.service.library.events.notification.NotificationCategory;
import com.navdy.service.library.events.notification.NotificationEvent;
import com.navdy.service.library.log.Logger;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MessageManager {
    private static final String EMPTY = "";
    private static final MessageManager sInstance = new MessageManager();
    public static final Logger sLogger = new Logger(MessageManager.class);
    private Bus bus = RemoteDeviceManager.getInstance().getBus();

    public static MessageManager getInstance() {
        return sInstance;
    }

    private MessageManager() {
        this.bus.register(this);
    }

    @Subscribe
    public void onMessage(NotificationEvent event) {
        sLogger.v("old notification --> convert to glance");
        if (event.sourceIdentifier == null) {
            sLogger.w("Ignoring notification with null sourceIdentifier" + event);
            return;
        }
        RecentCall call = null;
        if (event.category == NotificationCategory.CATEGORY_SOCIAL) {
            call = new RecentCall(null, Category.MESSAGE, event.sourceIdentifier, NumberType.OTHER, new Date(), CallType.INCOMING, -1, 0);
            RecentCallManager.getInstance().handleNewCall(call);
        }
        String from = TextUtils.isEmpty(event.title) ? event.sourceIdentifier : event.title;
        String message = event.message;
        String number = null;
        boolean cannotReplyBack = Boolean.TRUE.equals(event.cannotReplyBack);
        boolean validNumber = false;
        if (ContactUtil.isValidNumber(event.sourceIdentifier)) {
            number = event.sourceIdentifier;
            validNumber = true;
        } else if (call != null && ContactUtil.isValidNumber(call.number)) {
            number = call.number;
            validNumber = true;
        }
        if (!validNumber) {
            cannotReplyBack = true;
        }
        String id = GlanceHelper.getId();
        List<KeyValue> data = new ArrayList();
        List<GlanceActions> action = null;
        data.add(new KeyValue(MessageConstants.MESSAGE_FROM.name(), from));
        if (number != null) {
            data.add(new KeyValue(MessageConstants.MESSAGE_FROM_NUMBER.name(), number));
            if (!cannotReplyBack) {
                action = new ArrayList(1);
                action.add(GlanceActions.REPLY);
                data.add(new KeyValue(MessageConstants.MESSAGE_IS_SMS.name(), from));
            }
        }
        data.add(new KeyValue(MessageConstants.MESSAGE_BODY.name(), message));
        Builder builder = new Builder().glanceType(GlanceType.GLANCE_TYPE_MESSAGE).provider(GlanceHelper.SMS_PACKAGE).id(id).postTime(Long.valueOf(System.currentTimeMillis())).glanceData(data);
        if (action != null) {
            builder.actions(action);
        }
        this.bus.post(builder.build());
    }
}
