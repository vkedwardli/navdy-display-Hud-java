package com.navdy.hud.app.framework.voice;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.R;
import com.navdy.hud.app.analytics.AnalyticsSupport;
import com.navdy.hud.app.event.DriverProfileChanged;
import com.navdy.hud.app.event.RemoteEvent;
import com.navdy.hud.app.framework.destinations.Destination.Builder;
import com.navdy.hud.app.framework.destinations.DestinationsManager;
import com.navdy.hud.app.framework.notifications.NotificationId;
import com.navdy.hud.app.framework.notifications.NotificationManager;
import com.navdy.hud.app.manager.MusicManager;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.maps.util.DestinationUtil;
import com.navdy.hud.app.ui.component.destination.DestinationParcelable;
import com.navdy.hud.app.ui.component.destination.DestinationParcelable.DestinationType;
import com.navdy.hud.app.ui.component.destination.DestinationPickerScreen;
import com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.DestinationPickerState;
import com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.PlaceTypeResourceHolder;
import com.navdy.hud.app.ui.component.destination.IDestinationPicker;
import com.navdy.hud.app.util.FeatureUtil;
import com.navdy.hud.app.util.RemoteCapabilitiesUtil;
import com.navdy.service.library.events.DeviceInfo.Platform;
import com.navdy.service.library.events.audio.VoiceSearchRequest;
import com.navdy.service.library.events.audio.VoiceSearchResponse;
import com.navdy.service.library.events.audio.VoiceSearchResponse.VoiceSearchState;
import com.navdy.service.library.events.destination.Destination;
import com.navdy.service.library.events.places.PlaceType;
import com.navdy.service.library.events.ui.Screen;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;
import java.util.List;

public class VoiceSearchHandler {
    private static final String searchAgainSubTitle = HudApplication.getAppContext().getResources().getString(R.string.retry_search);
    private Bus bus;
    private Context context;
    private Handler handler = new Handler(Looper.getMainLooper());
    private VoiceSearchResponse response;
    private boolean showVoiceSearchTipsToUser = true;
    private VoiceSearchUserInterface voiceSearchUserInterface;

    public interface VoiceSearchUserInterface {
        void close();

        void onVoiceSearchResponse(VoiceSearchResponse voiceSearchResponse);
    }

    public VoiceSearchHandler(Context context, Bus bus, FeatureUtil featureUtil) {
        this.bus = bus;
        this.context = context;
        this.bus.register(this);
    }

    public void setVoiceSearchUserInterface(VoiceSearchUserInterface voiceSearchUserInterface) {
        this.voiceSearchUserInterface = voiceSearchUserInterface;
    }

    @Subscribe
    public void onDriverProfileChanged(DriverProfileChanged driverProfileChanged) {
        this.showVoiceSearchTipsToUser = true;
    }

    public void showedVoiceSearchTipsToUser() {
        this.showVoiceSearchTipsToUser = false;
    }

    public boolean shouldShowVoiceSearchTipsToUser() {
        return this.showVoiceSearchTipsToUser;
    }

    @Subscribe
    public void onVoiceSearchResponse(VoiceSearchResponse voiceSearchResponse) {
        if (this.voiceSearchUserInterface != null) {
            this.response = voiceSearchResponse;
            this.voiceSearchUserInterface.onVoiceSearchResponse(voiceSearchResponse);
        }
    }

    public void startVoiceSearch() {
        Platform remotePlatform = RemoteDeviceManager.getInstance().getRemoteDevicePlatform();
        if (remotePlatform != null) {
            if (remotePlatform == Platform.PLATFORM_iOS && RemoteCapabilitiesUtil.supportsVoiceSearchNewIOSPauseBehaviors()) {
                MusicManager musicManager = RemoteDeviceManager.getInstance().getMusicManager();
                if (musicManager != null) {
                    musicManager.softPause();
                }
                this.handler.postDelayed(new Runnable() {
                    public void run() {
                        VoiceSearchHandler.this.bus.post(new RemoteEvent(new VoiceSearchRequest(Boolean.valueOf(false))));
                    }
                }, 250);
                return;
            }
            this.bus.post(new RemoteEvent(new VoiceSearchRequest(Boolean.valueOf(false))));
        }
    }

    public void stopVoiceSearch() {
        if (this.response != null && this.response.state != VoiceSearchState.VOICE_SEARCH_SUCCESS) {
            this.bus.post(new RemoteEvent(new VoiceSearchRequest(Boolean.valueOf(true))));
            MusicManager musicManager = RemoteDeviceManager.getInstance().getMusicManager();
            if (musicManager != null) {
                musicManager.acceptResumes();
            }
        }
    }

    public void showResults() {
        if (this.response != null && this.response.results != null) {
            String title;
            List<Destination> returnedDestinations = this.response.results;
            Bundle args = new Bundle();
            args.putBoolean(DestinationPickerScreen.PICKER_SHOW_DESTINATION_MAP, true);
            args.putInt(DestinationPickerScreen.PICKER_DESTINATION_ICON, -1);
            args.putInt(DestinationPickerScreen.PICKER_INITIAL_SELECTION, 1);
            int defaultColor = ContextCompat.getColor(this.context, R.color.route_sel);
            int deselectedColor = ContextCompat.getColor(this.context, R.color.icon_bk_color_unselected);
            String subTitle = null;
            if (TextUtils.isEmpty(this.response.recognizedWords)) {
                title = searchAgainSubTitle;
            } else {
                title = "\"" + this.response.recognizedWords + "\"";
                subTitle = searchAgainSubTitle;
            }
            DestinationParcelable searchAgain = new DestinationParcelable(R.id.search_again, title, subTitle, false, null, true, null, 0.0d, 0.0d, 0.0d, 0.0d, R.drawable.icon_mm_voice_search_2, 0, defaultColor, deselectedColor, DestinationType.NONE, null);
            List<DestinationParcelable> destinations = DestinationUtil.convert(this.context, returnedDestinations, defaultColor, deselectedColor, true);
            destinations.add(0, searchAgain);
            destinations.add(new DestinationParcelable(R.id.retry, searchAgainSubTitle, null, false, null, false, null, 0.0d, 0.0d, 0.0d, 0.0d, R.drawable.icon_mm_voice_search_2, 0, defaultColor, deselectedColor, DestinationType.NONE, PlaceType.PLACE_TYPE_UNKNOWN));
            DestinationParcelable[] destinationParcelables = new DestinationParcelable[destinations.size()];
            destinations.toArray(destinationParcelables);
            args.putParcelableArray(DestinationPickerScreen.PICKER_DESTINATIONS, destinationParcelables);
            NotificationManager.getInstance().removeNotification(NotificationId.VOICE_SEARCH_NOTIFICATION_ID, true, Screen.SCREEN_DESTINATION_PICKER, args, new IDestinationPicker() {
                boolean retrySelected = false;

                public boolean onItemClicked(int id, int pos, DestinationPickerState state) {
                    switch (id) {
                        case R.id.retry:
                        case R.id.search_again:
                            this.retrySelected = true;
                            if (id == R.id.retry) {
                                AnalyticsSupport.recordVoiceSearchListItemSelection(-1);
                                return true;
                            }
                            AnalyticsSupport.recordVoiceSearchListItemSelection(-2);
                            return true;
                        default:
                            AnalyticsSupport.recordVoiceSearchListItemSelection(pos);
                            return false;
                    }
                }

                public boolean onItemSelected(int id, int pos, DestinationPickerState state) {
                    return false;
                }

                public void onDestinationPickerClosed() {
                    if (this.retrySelected) {
                        NotificationManager notificationManager = NotificationManager.getInstance();
                        VoiceSearchNotification voiceSearchNotification = (VoiceSearchNotification) notificationManager.getNotification(NotificationId.VOICE_SEARCH_NOTIFICATION_ID);
                        if (voiceSearchNotification == null) {
                            voiceSearchNotification = new VoiceSearchNotification();
                        }
                        notificationManager.addNotification(voiceSearchNotification);
                    }
                }
            });
        }
    }

    public void go() {
        if (this.response != null && this.response.state == VoiceSearchState.VOICE_SEARCH_SUCCESS && this.response.results != null && this.response.results.size() > 0) {
            Destination destination = (Destination) this.response.results.get(0);
            com.navdy.hud.app.framework.destinations.Destination d = DestinationsManager.getInstance().transformToInternalDestination(destination);
            int iconRes = 0;
            PlaceTypeResourceHolder resourceHolder = DestinationPickerScreen.getPlaceTypeHolder(destination.place_type);
            if (resourceHolder != null) {
                iconRes = resourceHolder.iconRes;
            }
            if (d.destinationIcon == 0 && iconRes != 0) {
                d = new Builder(d).destinationIcon(iconRes).destinationIconBkColor(ContextCompat.getColor(this.context, R.color.icon_bk_color_unselected)).build();
            }
            DestinationsManager.getInstance().requestNavigationWithNavLookup(d);
        }
    }
}
