package com.navdy.hud.app.framework.glympse;

import android.widget.ImageView;
import android.widget.TextView;
import butterknife.ButterKnife.Finder;
import com.navdy.hud.app.R;
import com.navdy.hud.app.ui.component.ChoiceLayout2;
import com.navdy.hud.app.ui.component.image.IconColorImageView;

public class GlympseNotification$$ViewInjector {
    public static void inject(Finder finder, GlympseNotification target, Object source) {
        target.title = (TextView) finder.findRequiredView(source, R.id.title, "field 'title'");
        target.subtitle = (TextView) finder.findRequiredView(source, R.id.subtitle, "field 'subtitle'");
        target.choiceLayout = (ChoiceLayout2) finder.findRequiredView(source, R.id.choice_layout, "field 'choiceLayout'");
        target.notificationIcon = (IconColorImageView) finder.findRequiredView(source, R.id.notification_icon, "field 'notificationIcon'");
        target.notificationUserImage = (ImageView) finder.findRequiredView(source, R.id.notification_user_image, "field 'notificationUserImage'");
        target.badge = (ImageView) finder.findRequiredView(source, R.id.badge, "field 'badge'");
        target.badgeIcon = (IconColorImageView) finder.findRequiredView(source, R.id.badge_icon, "field 'badgeIcon'");
    }

    public static void reset(GlympseNotification target) {
        target.title = null;
        target.subtitle = null;
        target.choiceLayout = null;
        target.notificationIcon = null;
        target.notificationUserImage = null;
        target.badge = null;
        target.badgeIcon = null;
    }
}
