package com.navdy.hud.app.framework.destinations;

import android.content.res.Resources;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.R;
import com.navdy.hud.app.framework.contacts.Contact;
import com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder;
import com.navdy.hud.app.maps.here.HereNavigationManager;
import com.navdy.service.library.events.places.PlaceType;
import java.util.List;

public class Destination {
    public final List<Contact> contacts;
    public final int destinationIcon;
    public final int destinationIconBkColor;
    public final String destinationPlaceId;
    public final String destinationSubtitle;
    public final String destinationTitle;
    public final DestinationType destinationType;
    public final double displayPositionLatitude;
    public final double displayPositionLongitude;
    public final String distanceStr;
    public final FavoriteDestinationType favoriteDestinationType;
    public final String fullAddress;
    public final String identifier;
    public final String initials;
    public boolean isInitialNumber;
    public final double navigationPositionLatitude;
    public final double navigationPositionLongitude;
    public boolean needShortTitleFont;
    public final List<Contact> phoneNumbers;
    public final PlaceCategory placeCategory;
    public final PlaceType placeType;
    public final String recentTimeLabel;
    public final int recentTimeLabelColor;
    public final boolean recommendation;

    public static class Builder {
        private List<Contact> contacts;
        private int destinationIcon;
        private int destinationIconBkColor = 0;
        private String destinationPlaceId;
        private String destinationSubtitle;
        private String destinationTitle;
        private DestinationType destinationType;
        private double displayPositionLatitude;
        private double displayPositionLongitude;
        private String distanceStr;
        private FavoriteDestinationType favoriteDestinationType;
        private String fullAddress;
        private String identifier;
        private String initials;
        private double navigationPositionLatitude;
        private double navigationPositionLongitude;
        private List<Contact> phoneNumbers;
        private PlaceCategory placeCategory;
        private PlaceType placeType;
        private String recentTimeLabel;
        private int recentTimeLabelColor;
        private boolean recommendation;

        public Builder(Destination d) {
            this.navigationPositionLatitude = d.navigationPositionLatitude;
            this.navigationPositionLongitude = d.navigationPositionLongitude;
            this.displayPositionLatitude = d.displayPositionLatitude;
            this.displayPositionLongitude = d.displayPositionLongitude;
            this.favoriteDestinationType = d.favoriteDestinationType;
            this.fullAddress = d.fullAddress;
            this.destinationTitle = d.destinationTitle;
            this.destinationSubtitle = d.destinationSubtitle;
            this.identifier = d.identifier;
            this.destinationType = d.destinationType;
            this.initials = d.initials;
            this.placeCategory = d.placeCategory;
            this.recentTimeLabel = d.recentTimeLabel;
            this.recentTimeLabelColor = d.recentTimeLabelColor;
            this.recommendation = d.recommendation;
            this.destinationIcon = d.destinationIcon;
            this.destinationIconBkColor = d.destinationIconBkColor;
            this.destinationPlaceId = d.destinationPlaceId;
            this.distanceStr = d.distanceStr;
            this.phoneNumbers = d.phoneNumbers;
            this.contacts = d.contacts;
        }

        public Builder navigationPositionLatitude(double navigationPositionLatitude) {
            this.navigationPositionLatitude = navigationPositionLatitude;
            return this;
        }

        public Builder navigationPositionLongitude(double navigationPositionLongitude) {
            this.navigationPositionLongitude = navigationPositionLongitude;
            return this;
        }

        public Builder displayPositionLatitude(double displayPositionLatitude) {
            this.displayPositionLatitude = displayPositionLatitude;
            return this;
        }

        public Builder displayPositionLongitude(double displayPositionLongitude) {
            this.displayPositionLongitude = displayPositionLongitude;
            return this;
        }

        public Builder fullAddress(String fullAddress) {
            this.fullAddress = fullAddress;
            return this;
        }

        public Builder destinationTitle(String destinationTitle) {
            this.destinationTitle = destinationTitle;
            return this;
        }

        public Builder destinationSubtitle(String destinationSubtitle) {
            this.destinationSubtitle = destinationSubtitle;
            return this;
        }

        public Builder identifier(String identifier) {
            this.identifier = identifier;
            return this;
        }

        public Builder destinationType(DestinationType destinationType) {
            this.destinationType = destinationType;
            return this;
        }

        public Builder favoriteDestinationType(FavoriteDestinationType favoriteDestinationType) {
            this.favoriteDestinationType = favoriteDestinationType;
            return this;
        }

        public Builder initials(String initials) {
            this.initials = initials;
            return this;
        }

        public Builder placeCategory(PlaceCategory placeCategory) {
            this.placeCategory = placeCategory;
            return this;
        }

        public Builder recentTimeLabel(String recentTimeLabel) {
            this.recentTimeLabel = recentTimeLabel;
            return this;
        }

        public Builder recentTimeLabelColor(int recentTimeLabelColor) {
            this.recentTimeLabelColor = recentTimeLabelColor;
            return this;
        }

        public Builder recommendation(boolean recommendation) {
            this.recommendation = recommendation;
            return this;
        }

        public Builder destinationIcon(int destinationIcon) {
            this.destinationIcon = destinationIcon;
            return this;
        }

        public Builder destinationIconBkColor(int destinationIconBkColor) {
            this.destinationIconBkColor = destinationIconBkColor;
            return this;
        }

        public Builder destinationPlaceId(String placeId) {
            this.destinationPlaceId = placeId;
            return this;
        }

        public Builder placeType(PlaceType placeType) {
            this.placeType = placeType;
            return this;
        }

        public Builder distanceStr(String distanceStr) {
            this.distanceStr = distanceStr;
            return this;
        }

        public Builder phoneNumbers(List<Contact> phoneNumbers) {
            this.phoneNumbers = phoneNumbers;
            return this;
        }

        public Builder contacts(List<Contact> contacts) {
            this.contacts = contacts;
            return this;
        }

        public Destination build() {
            return new Destination();
        }
    }

    public enum DestinationType {
        DEFAULT,
        FIND_GAS
    }

    public enum FavoriteDestinationType {
        FAVORITE_NONE(0),
        FAVORITE_HOME(1),
        FAVORITE_WORK(2),
        FAVORITE_CONTACT(3),
        FAVORITE_CALENDAR(4),
        FAVORITE_CUSTOM(10);
        
        public final int value;

        private FavoriteDestinationType(int value) {
            this.value = value;
        }

        public static FavoriteDestinationType buildFromValue(int n) {
            for (FavoriteDestinationType type : values()) {
                if (n == type.value) {
                    return type;
                }
            }
            return FAVORITE_NONE;
        }
    }

    public enum PlaceCategory {
        SUGGESTED,
        RECENT,
        SUGGESTED_RECENT
    }

    private Destination(Builder builder) {
        this(builder.navigationPositionLatitude, builder.navigationPositionLongitude, builder.displayPositionLatitude, builder.displayPositionLongitude, builder.fullAddress, builder.destinationTitle, builder.destinationSubtitle, builder.identifier, builder.favoriteDestinationType, builder.destinationType, builder.initials, builder.placeCategory, builder.recentTimeLabel, builder.recentTimeLabelColor, builder.recommendation, builder.destinationIcon, builder.destinationIconBkColor, builder.destinationPlaceId, builder.placeType, builder.distanceStr, builder.phoneNumbers, builder.contacts);
    }

    public Destination(double navigationPositionLatitude, double navigationPositionLongitude, double displayPositionLatitude, double displayPositionLongitude, String fullAddress, String destinationTitle, String destinationSubtitle, String identifier, FavoriteDestinationType favoriteDestinationType, DestinationType destinationType, String initials, PlaceCategory placeCategory, String recentTimeLabel, int recentTimeLabelColor, boolean recommendation, int destinationIcon, int destinationIconBkColor, String destinationPlaceId, PlaceType placeType, String distanceStr, List<Contact> phoneNumbers, List<Contact> contacts) {
        this.navigationPositionLatitude = navigationPositionLatitude;
        this.navigationPositionLongitude = navigationPositionLongitude;
        this.displayPositionLatitude = displayPositionLatitude;
        this.displayPositionLongitude = displayPositionLongitude;
        this.fullAddress = fullAddress;
        this.destinationTitle = destinationTitle;
        this.destinationSubtitle = destinationSubtitle;
        this.identifier = identifier;
        this.favoriteDestinationType = favoriteDestinationType;
        this.destinationType = destinationType;
        this.initials = initials;
        this.placeCategory = placeCategory;
        this.recentTimeLabel = recentTimeLabel;
        this.recentTimeLabelColor = recentTimeLabelColor;
        this.recommendation = recommendation;
        this.destinationIcon = destinationIcon;
        this.destinationIconBkColor = destinationIconBkColor;
        this.destinationPlaceId = destinationPlaceId;
        this.placeType = placeType;
        this.distanceStr = distanceStr;
        this.phoneNumbers = phoneNumbers;
        this.contacts = contacts;
        try {
            Integer.parseInt(initials);
            this.isInitialNumber = true;
        } catch (Throwable th) {
            this.isInitialNumber = false;
        }
    }

    public static Destination getGasDestination() {
        String title;
        String subtitle;
        Resources resources = HudApplication.getAppContext().getResources();
        if (HereNavigationManager.getInstance().isNavigationModeOn()) {
            title = resources.getString(R.string.suggested_dest_find_gas_on_route_title);
            subtitle = resources.getString(R.string.suggested_dest_find_gas_on_route_desc);
        } else {
            title = resources.getString(R.string.suggested_dest_find_gas_title);
            subtitle = resources.getString(R.string.suggested_dest_find_gas_desc);
        }
        return new Builder().destinationTitle(title).destinationSubtitle(subtitle).favoriteDestinationType(FavoriteDestinationType.FAVORITE_NONE).destinationType(DestinationType.FIND_GAS).build();
    }

    public String toString() {
        return "id[" + this.identifier + "] placeid[" + this.destinationPlaceId + "] nav_pos[" + this.navigationPositionLatitude + HereManeuverDisplayBuilder.COMMA + this.navigationPositionLongitude + "]" + " display_pos[" + this.displayPositionLatitude + HereManeuverDisplayBuilder.COMMA + this.displayPositionLongitude + "] title[" + this.destinationTitle + "] subTitle[" + this.destinationSubtitle + "] address[" + this.fullAddress + "] place_type[" + this.placeType + "]";
    }
}
