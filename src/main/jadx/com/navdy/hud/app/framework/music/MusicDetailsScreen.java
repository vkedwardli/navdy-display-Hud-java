package com.navdy.hud.app.framework.music;

import android.animation.Animator;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.KeyEvent;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.R;
import com.navdy.hud.app.analytics.AnalyticsSupport;
import com.navdy.hud.app.manager.InputManager;
import com.navdy.hud.app.manager.InputManager.CustomKeyEvent;
import com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder;
import com.navdy.hud.app.screen.BaseScreen;
import com.navdy.hud.app.ui.component.vlist.VerticalList.ItemSelectionState;
import com.navdy.hud.app.ui.component.vlist.VerticalList.Model;
import com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder;
import com.navdy.hud.app.ui.component.vlist.viewholder.IconOptionsViewHolder;
import com.navdy.hud.app.ui.component.vlist.viewholder.TitleSubtitleViewHolder;
import com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder;
import com.navdy.hud.app.ui.framework.BasePresenter;
import com.navdy.hud.app.ui.framework.DefaultAnimationListener;
import com.navdy.service.library.events.connection.ConnectionStateChange;
import com.navdy.service.library.events.ui.Screen;
import com.navdy.service.library.events.ui.ShowScreen.Builder;
import com.navdy.service.library.log.Logger;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;
import flow.Flow.Direction;
import flow.Layout;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import javax.inject.Singleton;

@Layout(R.layout.screen_music_details)
public class MusicDetailsScreen extends BaseScreen {
    private static final Logger sLogger = new Logger(MusicDetailsScreen.class);

    @dagger.Module(addsTo = com.navdy.hud.app.ui.activity.Main.Module.class, injects = {MusicDetailsView.class})
    public class Module {
    }

    @Singleton
    public static class Presenter extends BasePresenter<MusicDetailsView> {
        private static final int MUSIC_CONTROL_TRAY_POS = 0;
        private static final int music_selected_color;
        private static final int music_unselected_color;
        private static final String[] subTitles = new String[]{"Churches Your Vibe, Your Tribe Vol. 1", "Old Classics, Vol 2", "woof woof"};
        private static final Model thumbsDown = IconBkColorViewHolder.buildModel(R.id.music_thumbsdown, R.drawable.icon_thumbs_dn, music_selected_color, music_unselected_color, music_selected_color, "Dislike", null);
        private static final Model thumbsUp = IconBkColorViewHolder.buildModel(R.id.music_thumbsup, R.drawable.icon_thumbs_up, music_selected_color, music_unselected_color, music_selected_color, "Like", null);
        private static final String[] titles = new String[]{"The Mother We Both Share", "Tomorrow, Tomorrow, I love you", "Who Let the Dogs out"};
        private boolean animateIn;
        @Inject
        Bus bus;
        private boolean closed;
        private int currentCounter;
        private int currentTitleSelection;
        private List<Model> data;
        private boolean handledSelection;
        private boolean keyPressed;
        private boolean registered;
        private int thumbPos;

        static {
            Resources resources = HudApplication.getAppContext().getResources();
            music_selected_color = resources.getColor(R.color.music_details_sel);
            music_unselected_color = resources.getColor(R.color.icon_bk_color_unselected);
        }

        public void onLoad(Bundle savedInstanceState) {
            this.currentCounter = 1;
            this.currentTitleSelection = 0;
            this.animateIn = false;
            this.keyPressed = false;
            reset(null);
            this.bus.register(this);
            this.registered = true;
            updateView();
            super.onLoad(savedInstanceState);
        }

        public void onUnload() {
            if (this.registered) {
                this.registered = false;
                this.bus.unregister(this);
            }
            reset(null);
            super.onUnload();
        }

        void reset(MusicDetailsView view) {
            this.closed = false;
            this.handledSelection = false;
            if (view != null) {
                view.vmenuComponent.unlock();
            }
        }

        void resetSelectedItem() {
            MusicDetailsScreen.sLogger.v("resetSelectedItem");
            this.handledSelection = false;
            if (!this.animateIn) {
                this.animateIn = true;
                MusicDetailsView view = (MusicDetailsView) getView();
                if (view != null) {
                    view.vmenuComponent.animateIn(null);
                }
            }
        }

        protected void updateView() {
            MusicDetailsView view = (MusicDetailsView) getView();
            if (view != null) {
                init(view);
            }
        }

        void init(MusicDetailsView view) {
            view.vmenuComponent.setOverrideDefaultKeyEvents(CustomKeyEvent.LONG_PRESS, true);
            List<Model> list = new ArrayList();
            list.add(TitleSubtitleViewHolder.buildModel(titles[this.currentTitleSelection], subTitles[this.currentTitleSelection]));
            this.currentTitleSelection++;
            int[] iconSelectedColor = new int[]{music_selected_color, music_selected_color, music_selected_color};
            list.add(IconOptionsViewHolder.buildModel(R.id.music_controls, new int[]{R.drawable.icon_prev_music, R.drawable.icon_play_music, R.drawable.icon_next_music}, new int[]{R.id.music_rewind, R.id.music_play, R.id.music_forward}, iconSelectedColor, new int[]{music_unselected_color, music_unselected_color, music_unselected_color}, iconSelectedColor, 1, false));
            int i = 2 + 1;
            list.add(IconBkColorViewHolder.buildModel(2, R.drawable.icon_playlist, music_selected_color, music_unselected_color, music_selected_color, "Playlist/Album name", null));
            int i2 = i + 1;
            list.add(IconBkColorViewHolder.buildModel(i, R.drawable.icon_heart, music_selected_color, music_unselected_color, music_selected_color, "Love", null));
            this.thumbPos = list.size();
            list.add(thumbsUp);
            i = i2 + 1;
            list.add(IconBkColorViewHolder.buildModel(i2, R.drawable.icon_shuffle, music_selected_color, music_unselected_color, music_selected_color, "Shuffle", null));
            i2 = i + 1;
            list.add(IconBkColorViewHolder.buildModel(i, R.drawable.icon_stations, music_selected_color, music_unselected_color, music_selected_color, "Stations", null));
            i = i2 + 1;
            list.add(IconBkColorViewHolder.buildModel(i2, R.drawable.icon_bookmark, music_selected_color, music_unselected_color, music_selected_color, "Bookmark", null));
            this.data = list;
            view.vmenuComponent.updateView(this.data, 0, false);
        }

        void close() {
            if (this.closed) {
                MusicDetailsScreen.sLogger.v("already closed");
                return;
            }
            this.closed = true;
            MusicDetailsScreen.sLogger.v("close");
            AnalyticsSupport.recordMenuSelection("exit");
            MusicDetailsView view = (MusicDetailsView) getView();
            if (view != null) {
                view.vmenuComponent.animateOut(new DefaultAnimationListener() {
                    public void onAnimationEnd(Animator animation) {
                        MusicDetailsScreen.sLogger.v("post back");
                        Presenter.this.bus.post(new Builder().screen(Screen.SCREEN_BACK).build());
                    }
                });
            }
        }

        boolean selectItem(ItemSelectionState selection) {
            MusicDetailsScreen.sLogger.v("select id:" + selection.id + HereManeuverDisplayBuilder.COMMA + selection.pos + " subid=" + selection.subId);
            if (this.handledSelection) {
                MusicDetailsScreen.sLogger.v("already handled [" + selection.id + "], " + selection.pos);
                return true;
            }
            MusicDetailsView view = (MusicDetailsView) getView();
            if (view != null) {
                switch (selection.id) {
                    case R.id.music_controls:
                        switch (selection.subId) {
                            case R.id.music_forward:
                                reset(view);
                                break;
                            case R.id.music_pause:
                                reset(view);
                                break;
                            case R.id.music_play:
                                reset(view);
                                if (this.currentTitleSelection == titles.length) {
                                    this.currentTitleSelection = 0;
                                }
                                Model titleModel = TitleSubtitleViewHolder.buildModel(titles[this.currentTitleSelection], subTitles[this.currentTitleSelection]);
                                this.currentTitleSelection++;
                                view.vmenuComponent.verticalList.refreshData(selection.pos, titleModel);
                                break;
                            case R.id.music_rewind:
                                reset(view);
                                break;
                        }
                        break;
                    case R.id.music_thumbsdown:
                        MusicDetailsScreen.sLogger.v("thumbsup");
                        reset(view);
                        view.vmenuComponent.verticalList.refreshData(this.thumbPos, thumbsUp);
                        break;
                    case R.id.music_thumbsup:
                        MusicDetailsScreen.sLogger.v("thumbsdown");
                        reset(view);
                        view.vmenuComponent.verticalList.refreshData(this.thumbPos, thumbsDown);
                        break;
                    default:
                        close();
                        break;
                }
            }
            return this.handledSelection;
        }

        boolean isClosed() {
            return this.closed;
        }

        boolean isItemClickable(ItemSelectionState selection) {
            return true;
        }

        @Subscribe
        public void onConnectionStateChange(ConnectionStateChange event) {
            switch (event.state) {
                case CONNECTION_DISCONNECTED:
                    MusicDetailsScreen.sLogger.v("disconnected");
                    close();
                    return;
                default:
                    return;
            }
        }

        @Subscribe
        public void onKeyEvent(KeyEvent event) {
            if (InputManager.isCenterKey(event.getKeyCode())) {
                MusicDetailsView view = (MusicDetailsView) getView();
                if (view != null && !view.vmenuComponent.isCloseMenuVisible() && view.vmenuComponent.verticalList.getCurrentPosition() == 0) {
                    VerticalViewHolder vh = view.vmenuComponent.verticalList.getCurrentViewHolder();
                    if (vh != null && (vh instanceof IconOptionsViewHolder)) {
                        IconOptionsViewHolder iconOptionsViewHolder = (IconOptionsViewHolder) vh;
                        int selection = iconOptionsViewHolder.getCurrentSelection();
                        if (selection != 1) {
                            boolean fwd;
                            if (selection == 2) {
                                fwd = true;
                            } else {
                                fwd = false;
                            }
                            if (event.getAction() == 0) {
                                if (!this.keyPressed) {
                                    this.keyPressed = true;
                                    iconOptionsViewHolder.selectionDown();
                                }
                                String text = String.valueOf(this.currentCounter);
                                if (fwd) {
                                    this.currentCounter++;
                                } else if (this.currentCounter > 1) {
                                    this.currentCounter--;
                                }
                                view.counter.setText(text);
                            } else if (event.getAction() == 1 && this.keyPressed) {
                                this.keyPressed = false;
                                iconOptionsViewHolder.selectionUp();
                                reset(view);
                            }
                        }
                    }
                }
            }
        }
    }

    public String getMortarScopeName() {
        return getClass().getName();
    }

    public Object getDaggerModule() {
        return new Module();
    }

    public Screen getScreen() {
        return Screen.SCREEN_MUSIC_DETAILS;
    }

    public int getAnimationIn(Direction direction) {
        return -1;
    }

    public int getAnimationOut(Direction direction) {
        return -1;
    }
}
