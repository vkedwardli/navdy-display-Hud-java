package com.navdy.hud.app.screen;

import com.navdy.hud.app.screen.DialManagerScreen.Module;
import dagger.internal.ModuleAdapter;

public final class DialManagerScreen$Module$$ModuleAdapter extends ModuleAdapter<Module> {
    private static final Class<?>[] INCLUDES = new Class[0];
    private static final String[] INJECTS = new String[]{"members/com.navdy.hud.app.view.DialManagerView"};
    private static final Class<?>[] STATIC_INJECTIONS = new Class[0];

    public DialManagerScreen$Module$$ModuleAdapter() {
        super(Module.class, INJECTS, STATIC_INJECTIONS, false, INCLUDES, true, false);
    }
}
