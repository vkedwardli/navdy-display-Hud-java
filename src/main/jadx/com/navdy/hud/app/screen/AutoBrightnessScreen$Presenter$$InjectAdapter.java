package com.navdy.hud.app.screen;

import android.content.SharedPreferences;
import com.navdy.hud.app.screen.AutoBrightnessScreen.Presenter;
import com.navdy.hud.app.ui.framework.BasePresenter;
import com.squareup.otto.Bus;
import dagger.MembersInjector;
import dagger.internal.Binding;
import dagger.internal.Linker;
import java.util.Set;
import javax.inject.Provider;

public final class AutoBrightnessScreen$Presenter$$InjectAdapter extends Binding<Presenter> implements Provider<Presenter>, MembersInjector<Presenter> {
    private Binding<Bus> bus;
    private Binding<SharedPreferences> sharedPreferences;
    private Binding<BasePresenter> supertype;

    public AutoBrightnessScreen$Presenter$$InjectAdapter() {
        super("com.navdy.hud.app.screen.AutoBrightnessScreen$Presenter", "members/com.navdy.hud.app.screen.AutoBrightnessScreen$Presenter", true, Presenter.class);
    }

    public void attach(Linker linker) {
        this.bus = linker.requestBinding("com.squareup.otto.Bus", Presenter.class, getClass().getClassLoader());
        this.sharedPreferences = linker.requestBinding("android.content.SharedPreferences", Presenter.class, getClass().getClassLoader());
        Linker linker2 = linker;
        this.supertype = linker2.requestBinding("members/com.navdy.hud.app.ui.framework.BasePresenter", Presenter.class, getClass().getClassLoader(), false, true);
    }

    public void getDependencies(Set<Binding<?>> set, Set<Binding<?>> injectMembersBindings) {
        injectMembersBindings.add(this.bus);
        injectMembersBindings.add(this.sharedPreferences);
        injectMembersBindings.add(this.supertype);
    }

    public Presenter get() {
        Presenter result = new Presenter();
        injectMembers(result);
        return result;
    }

    public void injectMembers(Presenter object) {
        object.bus = (Bus) this.bus.get();
        object.sharedPreferences = (SharedPreferences) this.sharedPreferences.get();
        this.supertype.injectMembers(object);
    }
}
