package com.navdy.hud.app.screen;

import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Resources;
import android.os.Bundle;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.R;
import com.navdy.hud.app.manager.InputManager.CustomKeyEvent;
import com.navdy.hud.app.settings.HUDSettings;
import com.navdy.hud.app.ui.component.ChoiceLayout.Choice;
import com.navdy.hud.app.ui.component.ChoiceLayout.IListener;
import com.navdy.hud.app.ui.framework.BasePresenter;
import com.navdy.hud.app.util.os.SystemProperties;
import com.navdy.hud.app.view.AutoBrightnessView;
import com.navdy.service.library.events.ui.Screen;
import com.navdy.service.library.events.ui.ShowScreen.Builder;
import com.navdy.service.library.log.Logger;
import com.squareup.otto.Bus;
import flow.Layout;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import javax.inject.Singleton;

@Layout(R.layout.screen_auto_brightness)
public class AutoBrightnessScreen extends BaseScreen {
    private static final Logger sLogger = new Logger(AutoBrightnessScreen.class);

    @dagger.Module(addsTo = com.navdy.hud.app.ui.activity.Main.Module.class, injects = {AutoBrightnessView.class})
    public class Module {
    }

    @Singleton
    public static class Presenter extends BasePresenter<AutoBrightnessView> {
        private static final int POS_ACTION = 0;
        private IListener autoBrightnessOffListener = new IListener() {
            public void executeItem(int pos, int id) {
                if (pos == 0) {
                    Presenter.this.setAutoBrightness(true);
                }
                Presenter.this.bus.post(new Builder().screen(Screen.SCREEN_BACK).build());
            }

            public void itemSelected(int pos, int id) {
            }
        };
        private IListener autoBrightnessOnListener = new IListener() {
            public void executeItem(int pos, int id) {
                if (pos == 0) {
                    Presenter.this.setAutoBrightness(false);
                }
                Presenter.this.bus.post(new Builder().screen(Screen.SCREEN_BACK).build());
            }

            public void itemSelected(int pos, int id) {
            }
        };
        @Inject
        Bus bus;
        private Resources resources;
        @Inject
        SharedPreferences sharedPreferences;
        private AutoBrightnessView view;

        private void setAutoBrightness(boolean enabled) {
            Editor editor = this.sharedPreferences.edit();
            editor.putString(HUDSettings.AUTO_BRIGHTNESS, enabled ? "true" : "false");
            editor.apply();
            SystemProperties.set("persist.sys.autobrightness", enabled ? "enabled" : "disabled");
        }

        public void onLoad(Bundle savedInstanceState) {
            this.resources = HudApplication.getAppContext().getResources();
            this.bus.register(this);
            updateView();
            super.onLoad(savedInstanceState);
        }

        protected void updateView() {
            AutoBrightnessView view = (AutoBrightnessView) getView();
            if (view != null) {
                init(view);
            }
        }

        private void init(AutoBrightnessView view) {
            List<Choice> autoBrightnessOnChoices;
            if ("true".equals(this.sharedPreferences.getString(HUDSettings.AUTO_BRIGHTNESS, ""))) {
                autoBrightnessOnChoices = getChoicesOn();
                view.setTitle(this.resources.getString(R.string.auto_brightness_on_title));
                view.setStatusLabel(this.resources.getString(R.string.auto_brightness_on_status_label));
                view.setIcon(this.resources.getDrawable(R.drawable.icon_mm_brightness_copy));
                view.setChoices(autoBrightnessOnChoices, this.autoBrightnessOnListener);
            } else {
                autoBrightnessOnChoices = getChoicesOff();
                view.setTitle(this.resources.getString(R.string.auto_brightness_off_title));
                view.setStatusLabel(this.resources.getString(R.string.auto_brightness_off_status_label));
                view.setIcon(this.resources.getDrawable(R.drawable.icon_mm_brightness_disabled));
                view.setChoices(autoBrightnessOnChoices, this.autoBrightnessOffListener);
            }
            this.view = view;
        }

        private List<Choice> getChoicesOn() {
            List<Choice> choices = new ArrayList();
            String choiceDisable = this.resources.getString(R.string.auto_brightness_choice_disable);
            String choiceCancel = this.resources.getString(R.string.auto_brightness_choice_cancel);
            choices.add(new Choice(choiceDisable, 0));
            choices.add(new Choice(choiceCancel, 0));
            return choices;
        }

        private List<Choice> getChoicesOff() {
            List<Choice> choices = new ArrayList();
            String choiceEnable = this.resources.getString(R.string.auto_brightness_choice_enable);
            String choiceCancel = this.resources.getString(R.string.auto_brightness_choice_cancel);
            choices.add(new Choice(choiceEnable, 0));
            choices.add(new Choice(choiceCancel, 0));
            return choices;
        }

        public boolean handleKey(CustomKeyEvent event) {
            switch (event) {
                case LEFT:
                    this.view.moveSelectionLeft();
                    break;
                case RIGHT:
                    this.view.moveSelectionRight();
                    break;
                case SELECT:
                    this.view.executeSelectedItem();
                    break;
            }
            return true;
        }
    }

    public Screen getScreen() {
        return Screen.SCREEN_AUTO_BRIGHTNESS;
    }

    public String getMortarScopeName() {
        return getClass().getName();
    }

    public Object getDaggerModule() {
        return new Module();
    }
}
