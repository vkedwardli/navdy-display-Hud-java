package com.navdy.hud.app.bluetooth.vcard.exception;

public class VCardAgentNotSupportedException extends VCardNotSupportedException {
    public VCardAgentNotSupportedException(String message) {
        super(message);
    }
}
