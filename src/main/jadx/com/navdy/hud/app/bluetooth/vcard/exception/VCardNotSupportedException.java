package com.navdy.hud.app.bluetooth.vcard.exception;

public class VCardNotSupportedException extends VCardException {
    public VCardNotSupportedException(String message) {
        super(message);
    }
}
