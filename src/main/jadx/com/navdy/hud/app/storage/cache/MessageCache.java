package com.navdy.hud.app.storage.cache;

import com.navdy.hud.app.debug.DebugReceiver;
import com.navdy.service.library.events.Ext_NavdyEvent;
import com.navdy.service.library.events.MessageStore;
import com.squareup.wire.Message;
import com.squareup.wire.Wire;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Metadata(bv = {1, 0, 1}, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0012\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\b\u0018\u0000*\b\b\u0000\u0010\u0001*\u00020\u00022\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u0002H\u00010\u0003B'\u0012\u0012\u0010\u0005\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00060\u0003\u0012\f\u0010\u0007\u001a\b\u0012\u0004\u0012\u00028\u00000\b\u00a2\u0006\u0002\u0010\tJ\b\u0010\u0010\u001a\u00020\u0011H\u0016J\u0010\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u0004H\u0016J\u0017\u0010\u0015\u001a\u0004\u0018\u00018\u00002\u0006\u0010\u0014\u001a\u00020\u0004H\u0016\u00a2\u0006\u0002\u0010\u0016J\u001d\u0010\u0017\u001a\u00020\u00112\u0006\u0010\u0014\u001a\u00020\u00042\u0006\u0010\u0018\u001a\u00028\u0000H\u0016\u00a2\u0006\u0002\u0010\u0019J\u0010\u0010\u001a\u001a\u00020\u00112\u0006\u0010\u0014\u001a\u00020\u0004H\u0016R\u001d\u0010\u0005\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00060\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u0017\u0010\u0007\u001a\b\u0012\u0004\u0012\u00028\u00000\b\u00a2\u0006\b\n\u0000\u001a\u0004\b\f\u0010\rR\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001b"}, d2 = {"Lcom/navdy/hud/app/storage/cache/MessageCache;", "T", "Lcom/squareup/wire/Message;", "Lcom/navdy/hud/app/storage/cache/Cache;", "", "dataCache", "", "type", "Ljava/lang/Class;", "(Lcom/navdy/hud/app/storage/cache/Cache;Ljava/lang/Class;)V", "getDataCache", "()Lcom/navdy/hud/app/storage/cache/Cache;", "getType", "()Ljava/lang/Class;", "wire", "Lcom/squareup/wire/Wire;", "clear", "", "contains", "", "key", "get", "(Ljava/lang/String;)Lcom/squareup/wire/Message;", "put", "message", "(Ljava/lang/String;Lcom/squareup/wire/Message;)V", "remove", "app_hudRelease"}, k = 1, mv = {1, 1, 6})
/* compiled from: MessageCache.kt */
public final class MessageCache<T extends Message> implements Cache<String, T> {
    @NotNull
    private final Cache<String, byte[]> dataCache;
    @NotNull
    private final Class<T> type;
    private final Wire wire = new Wire(Ext_NavdyEvent.class);

    public MessageCache(@NotNull Cache<String, byte[]> dataCache, @NotNull Class<T> type) {
        Intrinsics.checkParameterIsNotNull(dataCache, "dataCache");
        Intrinsics.checkParameterIsNotNull(type, "type");
        this.dataCache = dataCache;
        this.type = type;
    }

    @NotNull
    public final Cache<String, byte[]> getDataCache() {
        return this.dataCache;
    }

    @NotNull
    public final Class<T> getType() {
        return this.type;
    }

    @Nullable
    public T get(@NotNull String key) {
        Intrinsics.checkParameterIsNotNull(key, "key");
        byte[] messageData = (byte[]) this.dataCache.get(key);
        Message result = null;
        if (messageData != null) {
            return MessageStore.removeNulls(this.wire.parseFrom(messageData, this.type));
        }
        return null;
    }

    public void put(@NotNull String key, @NotNull T message) {
        Intrinsics.checkParameterIsNotNull(key, "key");
        Intrinsics.checkParameterIsNotNull(message, DebugReceiver.EXTRA_NOTIFICATION_EVENT_MESSAGE);
        byte[] bytes = message.toByteArray();
        Cache cache = this.dataCache;
        Intrinsics.checkExpressionValueIsNotNull(bytes, "bytes");
        cache.put(key, bytes);
    }

    public void remove(@NotNull String key) {
        Intrinsics.checkParameterIsNotNull(key, "key");
        this.dataCache.remove(key);
    }

    public boolean contains(@NotNull String key) {
        Intrinsics.checkParameterIsNotNull(key, "key");
        return this.dataCache.contains(key);
    }

    public void clear() {
        this.dataCache.clear();
    }
}
