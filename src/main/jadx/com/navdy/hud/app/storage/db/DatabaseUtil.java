package com.navdy.hud.app.storage.db;

import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;
import com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.util.MusicDataUtils;

public class DatabaseUtil {
    public static final String ROW_ID = "rowid";

    public static class DatabaseNotAvailable extends RuntimeException {
    }

    public static void dropTable(SQLiteDatabase db, String tableName, Logger logger) {
        try {
            if (!TextUtils.isEmpty(tableName)) {
                db.execSQL("DROP TABLE IF EXISTS " + tableName);
                logger.v("dropped table[" + tableName + "]");
            }
        } catch (Throwable t) {
            logger.e(t);
        }
    }

    public static void createIndex(SQLiteDatabase db, String tableName, String key, Logger logger) {
        String indexName = tableName + MusicDataUtils.ALTERNATE_SEPARATOR + key;
        db.execSQL("CREATE INDEX IF NOT EXISTS " + indexName + " ON " + tableName + HereManeuverDisplayBuilder.OPEN_BRACKET + key + ");");
        logger.v("createdIndex:" + indexName);
    }
}
