package com.navdy.hud.app.storage.db.helper;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.text.TextUtils;
import com.navdy.hud.app.framework.DriverProfileHelper;
import com.navdy.hud.app.framework.contacts.ContactImageHelper;
import com.navdy.hud.app.framework.contacts.NumberType;
import com.navdy.hud.app.framework.contacts.PhoneImageDownloader;
import com.navdy.hud.app.framework.contacts.PhoneImageDownloader.Priority;
import com.navdy.hud.app.framework.recentcall.RecentCall;
import com.navdy.hud.app.framework.recentcall.RecentCall.CallType;
import com.navdy.hud.app.framework.recentcall.RecentCall.Category;
import com.navdy.hud.app.framework.recentcall.RecentCallManager;
import com.navdy.hud.app.storage.db.DatabaseUtil.DatabaseNotAvailable;
import com.navdy.hud.app.storage.db.HudDatabase;
import com.navdy.hud.app.storage.db.table.RecentCallsTable;
import com.navdy.hud.app.util.GenericUtil;
import com.navdy.service.library.events.photo.PhotoType;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.util.IOUtils;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

public class RecentCallPersistenceHelper {
    private static final String BULK_INSERT_SQL = "INSERT INTO recent_calls(device_id,category,name,number,number_type,call_time,call_type,def_image,number_numeric) VALUES (?,?,?,?,?,?,?,?,?)";
    private static final int CALL_TIME_ORDINAL = 5;
    private static final int CALL_TYPE_ORDINAL = 4;
    private static final int CATEGORY_ORDINAL = 0;
    private static final int DEFAULT_IMAGE_INDEX_ORDINAL = 6;
    private static final List<RecentCall> EMPTY_LIST = new LinkedList();
    private static final int NAME_ORDINAL = 1;
    private static final int NUMBER_NUMERIC_ORDINAL = 7;
    private static final int NUMBER_ORDINAL = 2;
    private static final int NUMBER_TYPE_ORDINAL = 3;
    private static final String[] RECENT_CALL_ARGS = new String[1];
    private static final String RECENT_CALL_ORDER_BY = "call_time DESC";
    private static final String[] RECENT_CALL_PROJECTION = new String[]{RecentCallsTable.CATEGORY, "name", "number", "number_type", RecentCallsTable.CALL_TYPE, RecentCallsTable.CALL_TIME, "def_image", "number_numeric"};
    private static final String RECENT_CALL_WHERE = "device_id=?";
    private static final Object lockObj = new Object();
    private static final Logger sLogger = new Logger(RecentCallPersistenceHelper.class);
    private static final Comparator<RecentCall> sReverseComparator = new Comparator<RecentCall>() {
        public int compare(RecentCall lhs, RecentCall rhs) {
            return rhs.compareTo(lhs);
        }
    };

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static List<RecentCall> getRecentsCalls(String driverId) {
        GenericUtil.checkNotOnMainThread();
        if (DriverProfileHelper.getInstance().getCurrentProfile().isDefaultProfile()) {
            return EMPTY_LIST;
        }
        synchronized (lockObj) {
            SQLiteDatabase database = HudDatabase.getInstance().getWritableDatabase();
            if (database == null) {
                throw new DatabaseNotAvailable();
            }
            List<RecentCall> recentCalls;
            RECENT_CALL_ARGS[0] = driverId;
            Cursor cursor = database.query(RecentCallsTable.TABLE_NAME, RECENT_CALL_PROJECTION, RECENT_CALL_WHERE, RECENT_CALL_ARGS, null, null, RECENT_CALL_ORDER_BY);
            if (cursor != null) {
                try {
                    if (cursor.moveToFirst()) {
                        ContactImageHelper contactImageHelper = ContactImageHelper.getInstance();
                        recentCalls = new ArrayList();
                        while (true) {
                            int category = cursor.getInt(0);
                            String name = cursor.getString(1);
                            String number = cursor.getString(2);
                            int numberType = cursor.getInt(3);
                            int callType = cursor.getInt(4);
                            long callTime = cursor.getLong(5);
                            int imageIndex = cursor.getInt(6);
                            long numericNumber = cursor.getLong(7);
                            List<RecentCall> list = recentCalls;
                            list.add(new RecentCall(name, Category.buildFromValue(category), number, NumberType.buildFromValue(numberType), new Date(1000 * callTime), CallType.buildFromValue(callType), imageIndex, numericNumber));
                            if (!cursor.moveToNext()) {
                                break;
                            }
                        }
                    }
                } finally {
                    IOUtils.closeStream(cursor);
                }
            }
            recentCalls = EMPTY_LIST;
            IOUtils.closeStream(cursor);
            return recentCalls;
        }
    }

    public static void storeRecentCalls(String driverId, List<RecentCall> newCalls, boolean fromPbap) {
        GenericUtil.checkNotOnMainThread();
        RecentCallManager recentCallManager = RecentCallManager.getInstance();
        List<RecentCall> mergedCalls = mergeRecentCalls(recentCallManager.getRecentCalls(), newCalls, fromPbap);
        deleteRecentCalls(driverId);
        if (mergedCalls.size() == 0) {
            recentCallManager.setRecentCalls(null);
            return;
        }
        if (fromPbap) {
            PhoneImageDownloader.getInstance().clearAllPhotoCheckEntries();
        }
        synchronized (lockObj) {
            SQLiteDatabase database = HudDatabase.getInstance().getWritableDatabase();
            if (database == null) {
                throw new DatabaseNotAvailable();
            }
            SQLiteStatement sqLiteStatement = database.compileStatement(BULK_INSERT_SQL);
            int rows = 0;
            try {
                ContactImageHelper contactImageHelper = ContactImageHelper.getInstance();
                database.beginTransaction();
                for (RecentCall recentCall : mergedCalls) {
                    sqLiteStatement.clearBindings();
                    sqLiteStatement.bindString(1, driverId);
                    sqLiteStatement.bindLong(2, (long) recentCall.category.getValue());
                    if (TextUtils.isEmpty(recentCall.name)) {
                        sqLiteStatement.bindNull(3);
                    } else {
                        sqLiteStatement.bindString(3, recentCall.name);
                    }
                    sqLiteStatement.bindString(4, recentCall.number);
                    sqLiteStatement.bindLong(5, (long) recentCall.numberType.getValue());
                    sqLiteStatement.bindLong(6, recentCall.callTime.getTime() / 1000);
                    sqLiteStatement.bindLong(7, (long) recentCall.callType.getValue());
                    recentCall.defaultImageIndex = contactImageHelper.getContactImageIndex(recentCall.number);
                    sqLiteStatement.bindLong(8, (long) recentCall.defaultImageIndex);
                    sqLiteStatement.bindLong(9, recentCall.numericNumber);
                    sqLiteStatement.execute();
                    rows++;
                    if (fromPbap) {
                        PhoneImageDownloader.getInstance().submitDownload(recentCall.number, Priority.NORMAL, PhotoType.PHOTO_CONTACT, recentCall.name);
                    }
                }
                database.setTransactionSuccessful();
                RecentCallManager.getInstance().setRecentCalls(mergedCalls);
                sLogger.v("recent-calls rows added:" + rows);
            } finally {
                database.endTransaction();
            }
        }
    }

    private static void deleteRecentCalls(String driverId) {
        try {
            synchronized (lockObj) {
                SQLiteDatabase database = HudDatabase.getInstance().getWritableDatabase();
                if (database == null) {
                    throw new DatabaseNotAvailable();
                }
                RECENT_CALL_ARGS[0] = driverId;
                sLogger.v("recent-calls rows deleted:" + database.delete(RecentCallsTable.TABLE_NAME, RECENT_CALL_WHERE, RECENT_CALL_ARGS));
            }
        } catch (Throwable t) {
            sLogger.e(t);
        }
    }

    private static List<RecentCall> mergeRecentCalls(List<RecentCall> existingCalls, List<RecentCall> newCalls, boolean fromPbap) {
        RecentCall existingCall;
        HashMap<Long, RecentCall> map = new HashMap(52);
        if (existingCalls != null) {
            for (RecentCall existingCall2 : existingCalls) {
                if (!fromPbap || existingCall2.category != Category.PHONE_CALL) {
                    map.put(Long.valueOf(existingCall2.numericNumber), existingCall2);
                }
            }
        }
        if (newCalls != null) {
            for (RecentCall newCall : newCalls) {
                existingCall2 = (RecentCall) map.get(Long.valueOf(newCall.numericNumber));
                if (existingCall2 == null) {
                    map.put(Long.valueOf(newCall.numericNumber), newCall);
                } else if (fromPbap) {
                    map.put(Long.valueOf(newCall.numericNumber), newCall);
                } else if (newCall.callTime.getTime() >= existingCall2.callTime.getTime()) {
                    if (newCall.category == Category.MESSAGE && TextUtils.isEmpty(newCall.name)) {
                        newCall.name = existingCall2.name;
                    }
                    map.put(Long.valueOf(newCall.numericNumber), newCall);
                }
            }
        }
        List<RecentCall> mergedList = new ArrayList(map.values());
        Collections.sort(mergedList, sReverseComparator);
        if (mergedList.size() <= 30) {
            return mergedList;
        }
        List<RecentCall> truncatedList = new ArrayList(30);
        for (int i = 0; i < 30; i++) {
            truncatedList.add(mergedList.get(i));
        }
        return truncatedList;
    }
}
