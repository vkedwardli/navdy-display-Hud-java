package com.navdy.hud.app.debug;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.device.gps.GpsConstants;
import com.navdy.hud.app.device.gps.GpsManager;
import com.navdy.hud.app.framework.glance.GlanceConstants;
import com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder;
import com.navdy.hud.app.service.HudConnectionService;
import com.navdy.hud.app.storage.PathManager;
import com.navdy.hud.app.util.GenericUtil;
import com.navdy.service.library.events.RequestStatus;
import com.navdy.service.library.events.debug.DriveRecordingsResponse;
import com.navdy.service.library.events.debug.StartDrivePlaybackResponse;
import com.navdy.service.library.events.debug.StartDriveRecordingResponse;
import com.navdy.service.library.events.debug.StopDriveRecordingResponse;
import com.navdy.service.library.events.location.Coordinate;
import com.navdy.service.library.events.location.Coordinate.Builder;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.task.TaskManager;
import com.navdy.service.library.util.IOUtils;
import com.navdy.service.library.util.MusicDataUtils;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

public class RouteRecorder {
    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd'_'HH_mm_ss.SSS", Locale.US);
    public static final String SECONDARY_LOCATION_TAG = "[secondary] ";
    private static final RouteRecorder sInstance = new RouteRecorder();
    public static final Logger sLogger = new Logger(RouteRecorder.class);
    private HudConnectionService connectionService;
    private final Context context = HudApplication.getAppContext();
    private volatile int currentInjectionIndex = 0;
    private List<Coordinate> currentLocations;
    private BufferedWriter driveLogWriter;
    private final GpsManager gpsManager = GpsManager.getInstance();
    private final Handler handler = new Handler(Looper.getMainLooper());
    private final Runnable injectFakeLocationRunnable = new Runnable() {
        public void run() {
            if (RouteRecorder.this.currentLocations != null && RouteRecorder.this.currentLocations.size() > RouteRecorder.this.currentInjectionIndex) {
                RouteRecorder.this.gpsManager.feedLocation(new Builder((Coordinate) RouteRecorder.this.currentLocations.get(RouteRecorder.this.currentInjectionIndex)).timestamp(Long.valueOf(System.currentTimeMillis())).build());
                if (RouteRecorder.this.currentLocations.size() > RouteRecorder.this.currentInjectionIndex + 1) {
                    long timeDiff = ((Coordinate) RouteRecorder.this.currentLocations.get(RouteRecorder.this.currentInjectionIndex + 1)).timestamp.longValue() - ((Coordinate) RouteRecorder.this.currentLocations.get(RouteRecorder.this.currentInjectionIndex)).timestamp.longValue();
                    RouteRecorder.this.currentInjectionIndex = RouteRecorder.this.currentInjectionIndex + 1;
                    RouteRecorder.this.handler.postDelayed(this, timeDiff);
                } else if (RouteRecorder.this.isLooping) {
                    RouteRecorder.this.currentInjectionIndex = 0;
                    RouteRecorder.this.handler.post(RouteRecorder.this.injectFakeLocationRunnable);
                } else {
                    RouteRecorder.this.stopPlayback();
                }
            } else if (!RouteRecorder.this.isLooping || RouteRecorder.this.currentLocations == null) {
                RouteRecorder.this.stopPlayback();
            } else {
                RouteRecorder.this.currentInjectionIndex = 0;
                RouteRecorder.this.handler.post(RouteRecorder.this.injectFakeLocationRunnable);
            }
        }
    };
    private volatile boolean isLooping;
    private volatile boolean isRecording = false;
    boolean lastReportedDST = false;
    private TimeZone lastReportedTimeZone = null;
    private final LocationListener locationListener = new LocationListener() {
        public void onLocationChanged(Location location) {
            RouteRecorder.this.persistLocationToFile(location, false);
        }

        public void onStatusChanged(String provider, int status, Bundle extras) {
        }

        public void onProviderEnabled(String provider) {
        }

        public void onProviderDisabled(String provider) {
        }
    };
    private final LocationManager locationManager = ((LocationManager) this.context.getSystemService("location"));
    private final PathManager pathManager = PathManager.getInstance();
    private State playbackState = State.STOPPED;
    private volatile boolean prepared;
    private volatile long preparedFileLastModifiedTime;
    private volatile String preparedFileName;
    private String recordingLabel;
    private Runnable startLocationUpdates = new Runnable() {
        public void run() {
            if (RouteRecorder.this.locationManager.isProviderEnabled(GpsConstants.NAVDY_GPS_PROVIDER)) {
                RouteRecorder.this.locationManager.requestLocationUpdates(GpsConstants.NAVDY_GPS_PROVIDER, 0, 0.0f, RouteRecorder.this.locationListener);
            }
            if (RouteRecorder.this.locationManager.isProviderEnabled("network")) {
                RouteRecorder.this.locationManager.requestLocationUpdates("network", 0, 0.0f, RouteRecorder.this.locationListener);
            }
        }
    };

    public static RouteRecorder getInstance() {
        return sInstance;
    }

    private RouteRecorder() {
    }

    public String startRecording(String label, boolean local) {
        if (this.isRecording || isPlaying()) {
            sLogger.v("already busy");
            if (!local) {
                this.connectionService.sendMessage(new StartDriveRecordingResponse(RequestStatus.REQUEST_INVALID_STATE));
            }
            return null;
        }
        sLogger.d("Starting the drive recording");
        this.isRecording = true;
        this.recordingLabel = label;
        String fileName = GenericUtil.normalizeToFilename(label) + MusicDataUtils.ALTERNATE_SEPARATOR + DATE_FORMAT.format(new Date()) + ".log";
        TaskManager.getInstance().execute(createDriveRecord(fileName, local), 9);
        return fileName;
    }

    public void stopRecording(final boolean local) {
        if (this.isRecording) {
            sLogger.d("Stopping the drive recording");
            this.handler.post(new Runnable() {
                public void run() {
                    RouteRecorder.this.locationManager.removeUpdates(RouteRecorder.this.locationListener);
                    TaskManager.getInstance().execute(new Runnable() {
                        public void run() {
                            RouteRecorder.this.bStopRecording(local);
                        }
                    }, 9);
                }
            });
            return;
        }
        sLogger.v("already stopped, no-op");
    }

    private void bStopRecording(boolean local) {
        IOUtils.closeStream(this.driveLogWriter);
        this.isRecording = false;
        this.recordingLabel = null;
        if (!local) {
            this.connectionService.sendMessage(new StopDriveRecordingResponse(RequestStatus.REQUEST_SUCCESS));
        }
        sLogger.v("recording stopped");
    }

    public void requestRecordings() {
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                List fileNames = new ArrayList();
                File[] subFiles = new File(RouteRecorder.this.pathManager.getMapsPartitionPath() + File.separator + "drive_logs").listFiles();
                if (subFiles != null) {
                    for (File file : subFiles) {
                        if (!file.getName().endsWith(".obd")) {
                            fileNames.add(file.getName());
                        }
                    }
                }
                RouteRecorder.this.connectionService.sendMessage(new DriveRecordingsResponse(fileNames));
            }
        }, 1);
    }

    public void startPlayback(String fileName, boolean secondary, boolean local) {
        startPlayback(fileName, secondary, false, local);
    }

    public void prepare(final String fileName, final boolean secondary) {
        if (isPlaying() || isPaused()) {
            sLogger.e("Playback is not stopped , current state " + this.playbackState.name());
        } else {
            TaskManager.getInstance().execute(new Runnable() {
                public void run() {
                    if (RouteRecorder.this.bPrepare(fileName, secondary)) {
                        RouteRecorder.sLogger.e("Prepare succeeded ");
                    } else {
                        RouteRecorder.sLogger.e("Prepare failed ");
                    }
                }
            }, 9);
        }
    }

    public synchronized boolean bPrepare(String fileName, boolean secondary) {
        boolean z;
        Exception e;
        Throwable th;
        File driveLogFile = new File(DriveRecorder.getDriveLogsDir(fileName), fileName);
        long modifiedTime = driveLogFile.lastModified();
        if (TextUtils.equals(fileName, this.preparedFileName) && this.preparedFileLastModifiedTime == modifiedTime) {
            sLogger.d("Already prepared, ready for playback");
            z = true;
        } else {
            sLogger.d("Preparing for playback from file : " + fileName);
            this.preparedFileName = fileName;
            this.preparedFileLastModifiedTime = modifiedTime;
            BufferedReader reader = null;
            try {
                BufferedReader reader2 = new BufferedReader(new InputStreamReader(new FileInputStream(driveLogFile), "utf-8"));
                try {
                    this.currentLocations = new ArrayList();
                    while (true) {
                        String line = reader2.readLine();
                        if (line == null) {
                            z = true;
                            IOUtils.closeStream(reader2);
                            break;
                        } else if (!line.startsWith("#")) {
                            int index = line.indexOf(SECONDARY_LOCATION_TAG);
                            boolean hasSecondaryTag = index >= 0;
                            if (hasSecondaryTag) {
                                line = line.substring(SECONDARY_LOCATION_TAG.length() + index);
                            }
                            if (secondary) {
                                if (!hasSecondaryTag) {
                                }
                            } else if (hasSecondaryTag) {
                                continue;
                            }
                            String[] lineSplit = line.split(HereManeuverDisplayBuilder.COMMA);
                            if (lineSplit.length == 8) {
                                this.currentLocations.add(new Builder().latitude(Double.valueOf(Double.parseDouble(lineSplit[0]))).longitude(Double.valueOf(Double.parseDouble(lineSplit[1]))).bearing(Float.valueOf(Float.parseFloat(lineSplit[2]))).speed(Float.valueOf(Float.parseFloat(lineSplit[3]))).accuracy(Float.valueOf(Float.parseFloat(lineSplit[4]))).altitude(Double.valueOf(Double.parseDouble(lineSplit[5]))).timestamp(Long.valueOf(Long.parseLong(lineSplit[6]))).build());
                            } else if (lineSplit.length == 6) {
                                this.currentLocations.add(new Builder().latitude(Double.valueOf(Double.parseDouble(lineSplit[0]))).longitude(Double.valueOf(Double.parseDouble(lineSplit[1]))).bearing(Float.valueOf(Float.parseFloat(lineSplit[2]))).speed(Float.valueOf(Float.parseFloat(lineSplit[3]))).accuracy(Float.valueOf(1.0f)).altitude(Double.valueOf(0.0d)).timestamp(Long.valueOf(Long.parseLong(lineSplit[4]))).build());
                            }
                        }
                    }
                } catch (Exception e2) {
                    e = e2;
                    reader = reader2;
                } catch (Throwable th2) {
                    th = th2;
                    reader = reader2;
                }
            } catch (Exception e3) {
                e = e3;
                try {
                    sLogger.e("Error parsing the file, prepare failed", e);
                    z = false;
                    IOUtils.closeStream(reader);
                    return z;
                } catch (Throwable th3) {
                    th = th3;
                    IOUtils.closeStream(reader);
                    throw th;
                }
            }
        }
        return z;
    }

    public void startPlayback(final String fileName, final boolean secondary, boolean loop, final boolean local) {
        if (isPlaying() || this.isRecording) {
            sLogger.v("already busy, no-op");
            if (!local) {
                this.connectionService.sendMessage(new StartDrivePlaybackResponse(RequestStatus.REQUEST_INVALID_STATE));
                return;
            }
            return;
        }
        this.playbackState = State.PLAYING;
        this.isLooping = loop;
        this.gpsManager.stopUbloxReporting();
        this.connectionService.setSimulatingGpsCoordinates(true);
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                try {
                    if (RouteRecorder.this.bPrepare(fileName, secondary)) {
                        RouteRecorder.sLogger.d("Prepare succeeded");
                        if (!local) {
                            RouteRecorder.this.connectionService.sendMessage(new StartDrivePlaybackResponse(RequestStatus.REQUEST_SUCCESS));
                        }
                        RouteRecorder.sLogger.d("Starting playback");
                        RouteRecorder.this.currentInjectionIndex = 0;
                        RouteRecorder.this.handler.post(RouteRecorder.this.injectFakeLocationRunnable);
                        return;
                    }
                    if (!local) {
                        RouteRecorder.this.connectionService.sendMessage(new StartDrivePlaybackResponse(RequestStatus.REQUEST_SERVICE_ERROR));
                    }
                    RouteRecorder.this.stopPlayback();
                } catch (Throwable t) {
                    if (!local) {
                        RouteRecorder.this.connectionService.sendMessage(new StartDrivePlaybackResponse(RequestStatus.REQUEST_SERVICE_ERROR));
                    }
                    RouteRecorder.this.stopPlayback();
                    RouteRecorder.sLogger.e(t);
                }
            }
        }, 1);
    }

    public void pausePlayback() {
        if (isPlaying()) {
            this.handler.removeCallbacks(this.injectFakeLocationRunnable);
            this.playbackState = State.PAUSED;
            this.gpsManager.startUbloxReporting();
            this.connectionService.setSimulatingGpsCoordinates(false);
            return;
        }
        sLogger.v("already stopped, no-op");
    }

    public void resumePlayback() {
        if (isPaused()) {
            this.gpsManager.stopUbloxReporting();
            this.playbackState = State.PLAYING;
            this.connectionService.setSimulatingGpsCoordinates(true);
            this.handler.post(this.injectFakeLocationRunnable);
        }
    }

    public void stopPlayback() {
        if (isStopped()) {
            sLogger.v("already stopped, no-op");
            return;
        }
        this.handler.removeCallbacks(this.injectFakeLocationRunnable);
        this.currentInjectionIndex = 0;
        this.playbackState = State.STOPPED;
        this.gpsManager.startUbloxReporting();
        this.isLooping = false;
        this.connectionService.setSimulatingGpsCoordinates(false);
    }

    public synchronized void release() {
        if (this.currentLocations != null) {
            this.currentLocations.clear();
            this.currentLocations = null;
        }
        this.preparedFileName = null;
        this.preparedFileLastModifiedTime = -1;
    }

    public boolean restartPlayback() {
        if (!isPlaying() && !isPaused()) {
            return false;
        }
        if (isPaused()) {
            this.gpsManager.stopUbloxReporting();
            this.connectionService.setSimulatingGpsCoordinates(false);
        }
        this.playbackState = State.PLAYING;
        this.currentInjectionIndex = 0;
        this.handler.removeCallbacks(this.injectFakeLocationRunnable);
        this.handler.post(this.injectFakeLocationRunnable);
        return true;
    }

    public void setConnectionService(HudConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    private Runnable createDriveRecord(final String fileName, final boolean local) {
        return new Runnable() {
            public void run() {
                File driveLogsDir = DriveRecorder.getDriveLogsDir(fileName);
                if (!driveLogsDir.exists()) {
                    driveLogsDir.mkdirs();
                }
                File driveLogFile = new File(driveLogsDir, fileName);
                try {
                    if (driveLogFile.createNewFile()) {
                        RouteRecorder.this.driveLogWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(driveLogFile), "utf-8"));
                        RouteRecorder.sLogger.v("writing drive log: " + fileName);
                        RouteRecorder.this.handler.post(RouteRecorder.this.startLocationUpdates);
                        if (!local) {
                            RouteRecorder.this.connectionService.sendMessage(new StartDriveRecordingResponse(RequestStatus.REQUEST_SUCCESS));
                        }
                    }
                } catch (Throwable e) {
                    RouteRecorder.sLogger.e(e);
                }
            }
        };
    }

    private Runnable writeLocation(final String locationString) {
        return new Runnable() {
            public void run() {
                if (RouteRecorder.this.isRecording) {
                    try {
                        RouteRecorder.this.driveLogWriter.write(locationString + GlanceConstants.NEWLINE);
                        RouteRecorder.this.driveLogWriter.flush();
                    } catch (Throwable e) {
                        RouteRecorder.sLogger.e(e);
                    }
                }
            }
        };
    }

    private Runnable writeMarker(final String marker) {
        return new Runnable() {
            public void run() {
                if (RouteRecorder.this.isRecording) {
                    try {
                        RouteRecorder.this.driveLogWriter.write(marker + GlanceConstants.NEWLINE);
                    } catch (Throwable e) {
                        RouteRecorder.sLogger.e(e);
                    }
                }
            }
        };
    }

    private void persistLocationToFile(Location location, boolean secondary) {
        if (this.isRecording) {
            String provider = location.getProvider();
            if (GpsConstants.NAVDY_GPS_PROVIDER.equals(provider)) {
                provider = "gps";
            }
            String zoneString = "";
            TimeZone tz = TimeZone.getDefault();
            boolean inDST = tz.inDaylightTime(new Date());
            if (!(tz.equals(this.lastReportedTimeZone) && inDST == this.lastReportedDST)) {
                String str = "#zone=\"%s\", dst=%s\n";
                Object[] objArr = new Object[2];
                objArr[0] = tz.getID();
                objArr[1] = inDST ? "T" : "F";
                zoneString = String.format(str, objArr);
                this.lastReportedTimeZone = tz;
                this.lastReportedDST = inDST;
                sLogger.v(String.format("setting zoneinfo:  %s dst=%b", new Object[]{tz.getID(), Boolean.valueOf(inDST)}));
            }
            TaskManager.getInstance().execute(writeLocation(zoneString + ((secondary ? SECONDARY_LOCATION_TAG : "") + location.getLatitude() + HereManeuverDisplayBuilder.COMMA + location.getLongitude() + HereManeuverDisplayBuilder.COMMA + location.getBearing() + HereManeuverDisplayBuilder.COMMA + location.getSpeed() + HereManeuverDisplayBuilder.COMMA + location.getAccuracy() + HereManeuverDisplayBuilder.COMMA + location.getAltitude() + HereManeuverDisplayBuilder.COMMA + location.getTime() + HereManeuverDisplayBuilder.COMMA + provider)), 9);
        }
    }

    public void injectLocation(Location location, boolean secondary) {
        persistLocationToFile(location, secondary);
    }

    public void injectMarker(String marker) {
        if (this.isRecording) {
            TaskManager.getInstance().execute(writeMarker(marker), 9);
        }
    }

    public boolean isRecording() {
        return this.isRecording;
    }

    public String getLabel() {
        return this.recordingLabel;
    }

    public boolean isPlaying() {
        return this.playbackState == State.PLAYING;
    }

    public boolean isPaused() {
        return this.playbackState == State.PAUSED;
    }

    public boolean isStopped() {
        return this.playbackState == State.STOPPED;
    }
}
