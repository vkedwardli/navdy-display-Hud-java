package com.navdy.hud.app.debug;

import com.navdy.service.library.log.Logger;
import java.util.HashMap;
import java.util.Map.Entry;

public class BusLeakDetector {
    private static final String EMPTY = "";
    private static final Logger sLogger = new Logger(BusLeakDetector.class);
    private static final BusLeakDetector singleton = new BusLeakDetector();
    private HashMap<Class, Integer> registrationMap = new HashMap(100);

    public BusLeakDetector() {
        sLogger.v("::ctor::");
    }

    public static BusLeakDetector getInstance() {
        return singleton;
    }

    public synchronized void addReference(Class clz) {
        Integer count = (Integer) this.registrationMap.get(clz);
        if (count == null) {
            this.registrationMap.put(clz, Integer.valueOf(1));
        } else {
            this.registrationMap.put(clz, Integer.valueOf(count.intValue() + 1));
        }
    }

    public synchronized void removeReference(Class clz) {
        Integer count = (Integer) this.registrationMap.get(clz);
        if (count != null) {
            count = Integer.valueOf(count.intValue() - 1);
            if (count.intValue() == 0) {
                this.registrationMap.remove(clz);
            } else {
                this.registrationMap.put(clz, count);
            }
        }
    }

    public synchronized void printReferences() {
        sLogger.v("::printReferences: [" + this.registrationMap.size() + "]");
        for (Entry<Class, Integer> entry : this.registrationMap.entrySet()) {
            String suffix;
            int count = ((Integer) entry.getValue()).intValue();
            switch (count) {
                case 1:
                    suffix = "";
                    break;
                case 2:
                    suffix = " **";
                    break;
                case 3:
                    suffix = " ****";
                    break;
                case 4:
                    suffix = " ********";
                    break;
                default:
                    suffix = " ****************";
                    break;
            }
            sLogger.v(((Class) entry.getKey()).getName() + " = " + count + suffix);
        }
    }
}
