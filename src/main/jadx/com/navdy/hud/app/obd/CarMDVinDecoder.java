package com.navdy.hud.app.obd;

import android.text.TextUtils;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.framework.network.NetworkStateManager;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.storage.PathManager;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.network.http.IHttpManager;
import com.navdy.service.library.task.TaskManager;
import com.navdy.service.library.util.CredentialUtil;
import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import javax.inject.Inject;
import mortar.Mortar;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.HttpUrl.Builder;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okhttp3.internal.DiskLruCache;
import okhttp3.internal.DiskLruCache.Editor;
import okhttp3.internal.DiskLruCache.Snapshot;
import okhttp3.internal.io.FileSystem;
import okio.BufferedSink;
import okio.BufferedSource;
import okio.Okio;
import org.json.JSONException;
import org.json.JSONObject;

public class CarMDVinDecoder {
    public static final int APP_VERSION = 1;
    private static final HttpUrl CAR_MD_API_URL = HttpUrl.parse("https://api2.carmd.com/v2.0/decode");
    private static final String DATA = "data";
    public static final long MAX_SIZE = 5000;
    private static final String META_CAR_MD_AUTH = "CAR_MD_AUTH";
    private static final String META_CAR_MD_TOKEN = "CAR_MD_TOKEN";
    public static final String VIN = "vin";
    private static final Logger sLogger = new Logger(CarMDVinDecoder.class);
    private final String CAR_MD_AUTH;
    private final String CAR_MD_TOKEN;
    private volatile boolean mDecoding = false;
    private ObdDeviceConfigurationManager mDeviceConfigurationManager;
    private DiskLruCache mDiskLruCache;
    private OkHttpClient mHttpClient;
    @Inject
    IHttpManager mHttpManager;

    public CarMDVinDecoder(ObdDeviceConfigurationManager obdDeviceConfigurationManager) {
        Mortar.inject(HudApplication.getAppContext(), this);
        this.mDiskLruCache = DiskLruCache.create(FileSystem.SYSTEM, new File(PathManager.getInstance().getCarMdResponseDiskCacheFolder()), 1, 1, 5000);
        this.mDeviceConfigurationManager = obdDeviceConfigurationManager;
        this.mHttpClient = this.mHttpManager.getClientCopy().readTimeout(120, TimeUnit.SECONDS).connectTimeout(120, TimeUnit.SECONDS).build();
        this.CAR_MD_AUTH = CredentialUtil.getCredentials(HudApplication.getAppContext(), META_CAR_MD_AUTH);
        this.CAR_MD_TOKEN = CredentialUtil.getCredentials(HudApplication.getAppContext(), META_CAR_MD_TOKEN);
    }

    public synchronized void decodeVin(final String vinNumber) {
        if (!this.mDecoding) {
            this.mDecoding = true;
            TaskManager.getInstance().execute(new Runnable() {
                public void run() {
                    try {
                        String data = CarMDVinDecoder.this.getFromCache(vinNumber);
                        if (!TextUtils.isEmpty(data)) {
                            CarMDVinDecoder.sLogger.d("Entry found in the cache for the vinNumber :" + vinNumber + ", Data :" + data);
                            CarDetails details = CarDetails.fromJson(data);
                            if (details != null) {
                                CarMDVinDecoder.this.mDeviceConfigurationManager.setDecodedCarDetails(details);
                                CarMDVinDecoder.this.mDecoding = false;
                                return;
                            }
                            CarMDVinDecoder.sLogger.d("Error parsing the CarMD response to CarDetails");
                        }
                        if (CarMDVinDecoder.this.isConnected()) {
                            Builder builder = CarMDVinDecoder.CAR_MD_API_URL.newBuilder();
                            builder.addQueryParameter("vin", vinNumber);
                            Request.Builder requestBuilder = new Request.Builder().url(builder.build());
                            if (TextUtils.isEmpty(CarMDVinDecoder.this.CAR_MD_AUTH)) {
                                CarMDVinDecoder.sLogger.e("Missing car md auth credential !");
                                CarMDVinDecoder.this.mDecoding = false;
                                return;
                            }
                            requestBuilder.addHeader("authorization", "Basic " + CarMDVinDecoder.this.CAR_MD_AUTH);
                            if (TextUtils.isEmpty(CarMDVinDecoder.this.CAR_MD_TOKEN)) {
                                CarMDVinDecoder.sLogger.e("Missing car md token credential !");
                                CarMDVinDecoder.this.mDecoding = false;
                                return;
                            }
                            requestBuilder.addHeader("partner-token", CarMDVinDecoder.this.CAR_MD_TOKEN);
                            CarMDVinDecoder.this.mHttpClient.newCall(requestBuilder.build()).enqueue(new Callback() {
                                public void onFailure(Call call, IOException e) {
                                    CarMDVinDecoder.sLogger.e("onFailure : Decoding failed ", e);
                                    CarMDVinDecoder.this.mDecoding = false;
                                }

                                public void onResponse(Call call, Response response) throws IOException {
                                    CarMDVinDecoder.sLogger.d("onResponse : Decoding response received");
                                    CarMDVinDecoder.this.parseResponse(response.body());
                                    CarMDVinDecoder.this.mDecoding = false;
                                }
                            });
                            return;
                        }
                        CarMDVinDecoder.this.mDecoding = false;
                    } catch (Exception e) {
                        CarMDVinDecoder.sLogger.e("Exception while fetching the vin from CarMD");
                        CarMDVinDecoder.this.mDecoding = false;
                    }
                }
            }, 1);
        }
    }

    private void parseResponse(ResponseBody responseBody) {
        if (responseBody != null) {
            try {
                JSONObject dataObject = new JSONObject(responseBody.string()).getJSONObject(DATA);
                if (dataObject != null) {
                    String dataString = dataObject.toString();
                    CarDetails details = CarDetails.fromJson(dataString);
                    if (details != null) {
                        putToCache(details.vin, dataString);
                        this.mDeviceConfigurationManager.setDecodedCarDetails(details);
                        return;
                    }
                    sLogger.e("Error parsing the CarMD response to car details");
                }
            } catch (JSONException e) {
                sLogger.e("Error parsing the response body ", e);
            } catch (IOException e2) {
                sLogger.e("IO Error parsing the response body ", e2);
            }
        }
    }

    private String getFromCache(String vinNumber) {
        try {
            Snapshot snapShot = this.mDiskLruCache.get(vinNumber.toLowerCase());
            if (snapShot == null) {
                return null;
            }
            BufferedSource bufferedSource = Okio.buffer(snapShot.getSource(0));
            String dataRead = bufferedSource.readUtf8();
            bufferedSource.close();
            return dataRead;
        } catch (IOException e) {
            sLogger.d("Exception while retrieving response from cache", e);
            return null;
        }
    }

    private void putToCache(String vinNumber, String dataString) {
        try {
            Editor editor = this.mDiskLruCache.edit(vinNumber.toLowerCase());
            BufferedSink bufferedSink = Okio.buffer(editor.newSink(0));
            bufferedSink.writeUtf8(dataString);
            bufferedSink.close();
            editor.commit();
        } catch (IOException e) {
            sLogger.e("Exception while putting into cache ", e);
        }
    }

    private boolean isConnected() {
        return RemoteDeviceManager.getInstance().isRemoteDeviceConnected() && NetworkStateManager.isConnectedToNetwork(HudApplication.getAppContext());
    }
}
