package com.navdy.hud.app.service.pandora.messages;

import com.navdy.hud.app.service.pandora.exceptions.MessageWrongWayException;
import com.navdy.hud.app.service.pandora.exceptions.StringOverflowException;
import com.navdy.hud.app.service.pandora.exceptions.UnexpectedEndOfStringException;
import java.io.IOException;

public class GetTrackInfoExtended extends BaseOutgoingEmptyMessage {
    public static final GetTrackInfoExtended INSTANCE = new GetTrackInfoExtended();

    public /* bridge */ /* synthetic */ byte[] buildPayload() throws IOException, StringOverflowException, UnexpectedEndOfStringException, MessageWrongWayException {
        return super.buildPayload();
    }

    private GetTrackInfoExtended() {
    }

    protected byte getMessageType() {
        return BaseMessage.PNDR_GET_TRACK_INFO_EXTENDED;
    }

    public String toString() {
        return "Getting extended track's info";
    }
}
