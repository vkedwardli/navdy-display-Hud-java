package com.navdy.hud.app.service.pandora;

import java.io.ByteArrayOutputStream;
import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

abstract class FrameMessage {
    protected static final byte[] ACK_PAYLOAD = new byte[0];
    protected static final int ADDITIONAL_BYTES_LENGTH = 6;
    protected static final int CRC_BYTES_LENGTH = 2;
    protected static final int FRAME_TYPE_POSITION = 1;
    protected static final byte PNDR_FRAME_END = (byte) 124;
    private static final byte PNDR_FRAME_ESCAPE = (byte) 125;
    private static final Map<Byte, Byte> PNDR_FRAME_ESCAPE_MAPPING = new HashMap<Byte, Byte>() {
        {
            put(Byte.valueOf(FrameMessage.PNDR_FRAME_START), new Byte((byte) 94));
            put(Byte.valueOf(FrameMessage.PNDR_FRAME_END), new Byte((byte) 92));
            put(Byte.valueOf(FrameMessage.PNDR_FRAME_ESCAPE), new Byte((byte) 93));
        }
    };
    protected static final byte PNDR_FRAME_SEQUENCE_0 = (byte) 0;
    protected static final byte PNDR_FRAME_SEQUENCE_1 = (byte) 1;
    protected static final byte PNDR_FRAME_START = (byte) 126;
    protected static final byte PNDR_FRAME_TYPE_ACK = (byte) 1;
    protected static final byte PNDR_FRAME_TYPE_DATA = (byte) 0;
    private static final Map<Byte, Byte> PNDR_FRAME_UNESCAPE_MAPPING = new HashMap(PNDR_FRAME_ESCAPE_MAPPING.size());
    protected boolean isSequence0;
    protected byte[] payload;

    public abstract byte[] buildFrame();

    static {
        for (Entry<Byte, Byte> entry : PNDR_FRAME_ESCAPE_MAPPING.entrySet()) {
            PNDR_FRAME_UNESCAPE_MAPPING.put(entry.getValue(), entry.getKey());
        }
    }

    protected FrameMessage(boolean isSequence0, byte[] payload) {
        this.isSequence0 = isSequence0;
        this.payload = payload;
    }

    protected static byte[] escapeBytes(ByteBuffer in) {
        ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
        for (int i = 0; i < in.limit(); i++) {
            byte b = in.get(i);
            Byte escapedB = (Byte) PNDR_FRAME_ESCAPE_MAPPING.get(Byte.valueOf(b));
            if (escapedB != null) {
                byteStream.write(125);
                byteStream.write(escapedB.byteValue());
            } else {
                byteStream.write(b);
            }
        }
        return byteStream.toByteArray();
    }

    protected static ByteBuffer unescapeBytes(byte[] in) throws IllegalArgumentException {
        ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
        int length = in.length;
        int i = 0;
        while (i < length) {
            byte b = in[i];
            if (b == PNDR_FRAME_ESCAPE) {
                i++;
                try {
                    b = ((Byte) PNDR_FRAME_UNESCAPE_MAPPING.get(Byte.valueOf(in[i]))).byteValue();
                } catch (ArrayIndexOutOfBoundsException e) {
                    throw new IllegalArgumentException("Escape byte not followed by a byte");
                } catch (NullPointerException e2) {
                    throw new IllegalArgumentException("Escape byte not followed by a proper byte");
                }
            }
            byteStream.write(b);
            i++;
        }
        return ByteBuffer.wrap(byteStream.toByteArray());
    }

    protected static byte[] buildFrameFromCRCPart(ByteBuffer bufferToCrc) {
        ByteBuffer crc = CRC16CCITT.calculate(bufferToCrc);
        byte[] escapedDataBytes = escapeBytes(bufferToCrc);
        byte[] escapedCRCBytes = escapeBytes(crc);
        return ByteBuffer.allocate((escapedDataBytes.length + escapedCRCBytes.length) + 2).put(PNDR_FRAME_START).put(escapedDataBytes).put(escapedCRCBytes).put(PNDR_FRAME_END).array();
    }

    public static FrameMessage parseFrame(byte[] frame) throws IllegalArgumentException {
        byte type = frame[1];
        if (type == (byte) 1) {
            return AckFrameMessage.parseAckFrame(frame);
        }
        if (type == (byte) 0) {
            return DataFrameMessage.parseDataFrame(frame);
        }
        throw new IllegalArgumentException("Unknown message frame type: " + String.format("%02X", new Object[]{Byte.valueOf(type)}));
    }
}
