package com.navdy.hud.app.service.pandora.messages;

import com.navdy.hud.app.service.pandora.exceptions.CorruptedPayloadException;
import com.navdy.hud.app.service.pandora.exceptions.MessageWrongWayException;
import com.navdy.hud.app.service.pandora.exceptions.StringOverflowException;
import com.navdy.hud.app.service.pandora.exceptions.UnsupportedMessageReceivedException;
import com.navdy.hud.app.service.pandora.exceptions.UnterminatedStringException;
import com.navdy.service.library.util.IOUtils;
import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.nio.ByteBuffer;

public abstract class BaseIncomingMessage extends BaseMessage {
    protected static boolean getBoolean(ByteBuffer buffer) {
        return buffer.get() != (byte) 0;
    }

    protected static String getFixedLengthASCIIString(ByteBuffer buffer, int fixedLength) {
        Closeable byteArrayOS = null;
        for (int i = 0; i < fixedLength; i++) {
            byte b = buffer.get();
            if (b == (byte) 0 && byteArrayOS != null) {
                break;
            }
            if (b != (byte) 0) {
                if (byteArrayOS == null) {
                    byteArrayOS = new ByteArrayOutputStream();
                }
                byteArrayOS.write(b);
            }
        }
        String result = "";
        if (byteArrayOS == null) {
            return result;
        }
        result = new String(byteArrayOS.toByteArray(), FIXED_LENGTH_STRING_ENCODING);
        IOUtils.closeStream(byteArrayOS);
        return result;
    }

    protected static String getString(ByteBuffer buffer) throws UnterminatedStringException, StringOverflowException {
        ByteArrayOutputStream byteArrayOS = new ByteArrayOutputStream();
        int currentLength = 0;
        byte b = buffer.get();
        while (b != (byte) 0) {
            if (b < (byte) 0) {
                throw new UnterminatedStringException();
            }
            byteArrayOS.write(b);
            currentLength++;
            if (currentLength > 247) {
                throw new StringOverflowException();
            }
            b = buffer.get();
        }
        return new String(byteArrayOS.toByteArray(), STRING_ENCODING);
    }

    protected static BaseIncomingMessage innerBuildFromPayload(byte[] payload) throws MessageWrongWayException, CorruptedPayloadException {
        throw new MessageWrongWayException();
    }

    public static BaseIncomingMessage buildFromPayload(byte[] payload) throws MessageWrongWayException, CorruptedPayloadException, UnsupportedMessageReceivedException {
        switch (payload[0]) {
            case (byte) -127:
                return UpdateStatus.innerBuildFromPayload(payload);
            case (byte) -112:
                return UpdateTrack.innerBuildFromPayload(payload);
            case (byte) -107:
                return ReturnTrackAlbumArtSegment.innerBuildFromPayload(payload);
            case (byte) -106:
                return UpdateTrackAlbumArt.innerBuildFromPayload(payload);
            case (byte) -105:
                return UpdateTrackElapsed.innerBuildFromPayload(payload);
            case (byte) -99:
                return ReturnTrackInfoExtended.innerBuildFromPayload(payload);
            case (byte) -98:
                return UpdateTrackCompleted.innerBuildFromPayload(payload);
            case (byte) -70:
                return UpdateStationActive.innerBuildFromPayload(payload);
            default:
                throw new UnsupportedMessageReceivedException(payload[0]);
        }
    }
}
