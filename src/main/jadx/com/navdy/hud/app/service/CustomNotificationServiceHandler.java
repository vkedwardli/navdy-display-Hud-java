package com.navdy.hud.app.service;

import android.os.SystemClock;
import com.amazonaws.auth.STSAssumeRoleSessionCredentialsProvider;
import com.amazonaws.services.s3.internal.Constants;
import com.here.android.mpa.common.GeoCoordinate;
import com.here.android.mpa.search.Location;
import com.here.android.mpa.search.Place;
import com.navdy.hud.app.device.dial.DialNotification;
import com.navdy.hud.app.framework.connection.ConnectionNotification;
import com.navdy.hud.app.framework.fuel.FuelRoutingManager;
import com.navdy.hud.app.framework.fuel.FuelRoutingManager.ClearTestObdLowFuelLevel;
import com.navdy.hud.app.framework.fuel.FuelRoutingManager.FuelAddedTestEvent;
import com.navdy.hud.app.framework.fuel.FuelRoutingManager.OnNearestGasStationCallback;
import com.navdy.hud.app.framework.fuel.FuelRoutingManager.OnRouteToGasStationCallback;
import com.navdy.hud.app.framework.fuel.FuelRoutingManager.TestObdLowFuelLevel;
import com.navdy.hud.app.framework.glance.GlanceConstants;
import com.navdy.hud.app.framework.glance.GlanceHelper;
import com.navdy.hud.app.framework.notifications.NotificationManager;
import com.navdy.hud.app.framework.toast.ToastManager;
import com.navdy.hud.app.maps.MapEvents.LiveTrafficDismissEvent;
import com.navdy.hud.app.maps.MapEvents.LiveTrafficEvent;
import com.navdy.hud.app.maps.MapEvents.LiveTrafficEvent.Severity;
import com.navdy.hud.app.maps.MapEvents.LiveTrafficEvent.Type;
import com.navdy.hud.app.maps.MapEvents.TrafficDelayDismissEvent;
import com.navdy.hud.app.maps.MapEvents.TrafficDelayEvent;
import com.navdy.hud.app.maps.MapEvents.TrafficJamDismissEvent;
import com.navdy.hud.app.maps.MapEvents.TrafficJamProgressEvent;
import com.navdy.hud.app.maps.MapEvents.TrafficRerouteDismissEvent;
import com.navdy.hud.app.maps.MapEvents.TrafficRerouteEvent;
import com.navdy.hud.app.maps.here.HereMapsManager;
import com.navdy.hud.app.maps.here.HerePlacesManager;
import com.navdy.hud.app.maps.here.HerePlacesManager.Error;
import com.navdy.hud.app.maps.here.HerePlacesManager.OnCategoriesSearchListener;
import com.navdy.hud.app.profile.NotificationSettings;
import com.navdy.service.library.events.callcontrol.PhoneBatteryStatus;
import com.navdy.service.library.events.callcontrol.PhoneBatteryStatus.BatteryStatus;
import com.navdy.service.library.events.callcontrol.PhoneEvent;
import com.navdy.service.library.events.callcontrol.PhoneStatus;
import com.navdy.service.library.events.glances.CalendarConstants;
import com.navdy.service.library.events.glances.FuelConstants;
import com.navdy.service.library.events.glances.GenericConstants;
import com.navdy.service.library.events.glances.GlanceEvent.Builder;
import com.navdy.service.library.events.glances.GlanceEvent.GlanceActions;
import com.navdy.service.library.events.glances.GlanceEvent.GlanceType;
import com.navdy.service.library.events.glances.GlanceIconConstants;
import com.navdy.service.library.events.glances.KeyValue;
import com.navdy.service.library.events.glances.MessageConstants;
import com.navdy.service.library.events.navigation.NavigationRouteResult;
import com.navdy.service.library.events.notification.ShowCustomNotification;
import com.navdy.service.library.events.ui.Screen;
import com.navdy.service.library.events.ui.ShowScreen;
import com.navdy.service.library.log.Logger;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

public class CustomNotificationServiceHandler {
    private static final Logger sLogger = new Logger(CustomNotificationServiceHandler.class);
    Bus bus;

    public CustomNotificationServiceHandler(Bus bus) {
        this.bus = bus;
        bus.register(this);
    }

    public void start() {
    }

    @Subscribe
    public void onShowCustomNotification(ShowCustomNotification event) {
        sLogger.v("onShowCustomNotification: " + event.id);
        NotificationManager notificationManager = NotificationManager.getInstance();
        String str = event.id;
        Object obj = -1;
        switch (str.hashCode()) {
            case -2039247981:
                if (str.equals("DIAL_BATTERY_LOW")) {
                    obj = 14;
                    break;
                }
                break;
            case -1827661852:
                if (str.equals("DIAL_FORGOTTEN_MULTIPLE")) {
                    obj = 13;
                    break;
                }
                break;
            case -1814512122:
                if (str.equals("CLEAR_OBD_LOW_FUEL_LEVEL")) {
                    obj = 41;
                    break;
                }
                break;
            case -1802230586:
                if (str.equals("CLEAR_TRAFFIC_INCIDENT")) {
                    obj = 26;
                    break;
                }
                break;
            case -1735435934:
                if (str.equals("CLEAR_TRAFFIC_REROUTE")) {
                    obj = 22;
                    break;
                }
                break;
            case -1614526796:
                if (str.equals("OBD_LOW_FUEL_LEVEL")) {
                    obj = 40;
                    break;
                }
                break;
            case -1527103628:
                if (str.equals("TRAFFIC_REROUTE")) {
                    obj = 21;
                    break;
                }
                break;
            case -1481455415:
                if (str.equals("CLEAR_ALL_TOAST_AND_CURRENT")) {
                    obj = 34;
                    break;
                }
                break;
            case -1472431602:
                if (str.equals("LOW_FUEL_LEVEL_NOCHECK")) {
                    obj = 38;
                    break;
                }
                break;
            case -1338282068:
                if (str.equals("DIAL_BATTERY_VERY_LOW")) {
                    obj = 16;
                    break;
                }
                break;
            case -1284131530:
                if (str.equals("GOOGLE_CALENDAR_1")) {
                    obj = 35;
                    break;
                }
                break;
            case -1251325556:
                if (str.equals("PHONE_DISCONNECTED")) {
                    obj = 29;
                    break;
                }
                break;
            case -1221193815:
                if (str.equals("GENERIC_1")) {
                    obj = null;
                    break;
                }
                break;
            case -1221193814:
                if (str.equals("GENERIC_2")) {
                    obj = 1;
                    break;
                }
                break;
            case -1163820707:
                if (str.equals("INCOMING_PHONE_CALL_323")) {
                    obj = 2;
                    break;
                }
                break;
            case -930941457:
                if (str.equals("CLEAR_TRAFFIC_DELAY")) {
                    obj = 28;
                    break;
                }
                break;
            case -767590304:
                if (str.equals("FIND_ROUTE_TO_CLOSEST_GAS_STATION")) {
                    obj = 44;
                    break;
                }
                break;
            case -638027464:
                if (str.equals("PHONE_CONNECTED")) {
                    obj = 30;
                    break;
                }
                break;
            case -406264442:
                if (str.equals("LOW_FUEL_LEVEL")) {
                    obj = 37;
                    break;
                }
                break;
            case -342876771:
                if (str.equals("DIAL_BATTERY_OK")) {
                    obj = 17;
                    break;
                }
                break;
            case -63686527:
                if (str.equals("TRAFFIC_DELAY")) {
                    obj = 27;
                    break;
                }
                break;
            case -11797496:
                if (str.equals("FIND_GAS_STATION")) {
                    obj = 43;
                    break;
                }
                break;
            case -5849545:
                if (str.equals("CLEAR_ALL_TOAST")) {
                    obj = 33;
                    break;
                }
                break;
            case 43099391:
                if (str.equals("PHONE_BATTERY_OK")) {
                    obj = 6;
                    break;
                }
                break;
            case 60514076:
                if (str.equals("DIAL_FORGOTTEN_SINGLE")) {
                    obj = 12;
                    break;
                }
                break;
            case 73725445:
                if (str.equals("MUSIC")) {
                    obj = 18;
                    break;
                }
                break;
            case 83953026:
                if (str.equals("CLEAR_TRAFFIC_JAM")) {
                    obj = 24;
                    break;
                }
                break;
            case 204001803:
                if (str.equals("PHONE_BATTERY_EXTREMELY_LOW")) {
                    obj = 5;
                    break;
                }
                break;
            case 333419062:
                if (str.equals("VOICE_ASSIST")) {
                    obj = 19;
                    break;
                }
                break;
            case 361103604:
                if (str.equals("TRAFFIC_INCIDENT")) {
                    obj = 25;
                    break;
                }
                break;
            case 518374548:
                if (str.equals("TRAFFIC_JAM")) {
                    obj = 23;
                    break;
                }
                break;
            case 531351226:
                if (str.equals("FUEL_ADDED_TEST")) {
                    obj = 42;
                    break;
                }
                break;
            case 634551002:
                if (str.equals("TEXT_NOTIFICAION_WITH_NO_REPLY_510")) {
                    obj = 8;
                    break;
                }
                break;
            case 993806900:
                if (str.equals("LOW_FUEL_LEVEL_CLEAR")) {
                    obj = 39;
                    break;
                }
                break;
            case 998768680:
                if (str.equals("END_PHONE_CALL_323")) {
                    obj = 3;
                    break;
                }
                break;
            case 1033071277:
                if (str.equals("DIAL_BATTERY_EXTREMELY_LOW")) {
                    obj = 15;
                    break;
                }
                break;
            case 1133254737:
                if (str.equals("BRIGHTNESS")) {
                    obj = 20;
                    break;
                }
                break;
            case 1225163157:
                if (str.equals("APPLE_CALENDAR_1")) {
                    obj = 36;
                    break;
                }
                break;
            case 1314336394:
                if (str.equals("PHONE_APP_DISCONNECTED")) {
                    obj = 31;
                    break;
                }
                break;
            case 1336078449:
                if (str.equals("PHONE_BATTERY_LOW")) {
                    obj = 4;
                    break;
                }
                break;
            case 1427731674:
                if (str.equals("DIAL_CONNECTED")) {
                    obj = 10;
                    break;
                }
                break;
            case 1456922895:
                if (str.equals("CLEAR_CURRENT_TOAST")) {
                    obj = 32;
                    break;
                }
                break;
            case 1483755310:
                if (str.equals("TEXT_NOTIFICAION_WITH_REPLY_408")) {
                    obj = 7;
                    break;
                }
                break;
            case 1483760395:
                if (str.equals("TEXT_NOTIFICAION_WITH_REPLY_999")) {
                    obj = 9;
                    break;
                }
                break;
            case 1487737514:
                if (str.equals("DIAL_DISCONNECTED")) {
                    obj = 11;
                    break;
                }
                break;
        }
        List<GlanceActions> actionsList;
        List<KeyValue> data;
        long meetingTime;
        final long elapsedRealtime;
        switch (obj) {
            case null:
                List<KeyValue> data_1 = new ArrayList();
                data_1.add(new KeyValue(GenericConstants.GENERIC_TITLE.name(), "John Doe"));
                data_1.add(new KeyValue(GenericConstants.GENERIC_MESSAGE.name(), "Message Test"));
                data_1.add(new KeyValue(GenericConstants.GENERIC_MAIN_ICON.name(), GlanceIconConstants.GLANCE_ICON_NAVDY_MAIN.name()));
                data_1.add(new KeyValue(GenericConstants.GENERIC_SIDE_ICON.name(), GlanceIconConstants.GLANCE_ICON_MESSAGE_SIDE_BLUE.name()));
                this.bus.post(new Builder().glanceType(GlanceType.GLANCE_TYPE_GENERIC).id(UUID.randomUUID().toString()).postTime(Long.valueOf(System.currentTimeMillis())).provider("blah").glanceData(data_1).build());
                return;
            case 1:
                List<KeyValue> data_2 = new ArrayList();
                data_2.add(new KeyValue(GenericConstants.GENERIC_TITLE.name(), "Jone Doe"));
                data_2.add(new KeyValue(GenericConstants.GENERIC_MESSAGE.name(), "Message Test"));
                this.bus.post(new Builder().glanceType(GlanceType.GLANCE_TYPE_GENERIC).id(UUID.randomUUID().toString()).postTime(Long.valueOf(System.currentTimeMillis())).provider("blah").glanceData(data_2).build());
                return;
            case 2:
                this.bus.post(new PhoneEvent.Builder().number("323-222-1111").status(PhoneStatus.PHONE_RINGING).contact_name("Al Jazeera Al Bin Al Salaad").build());
                return;
            case 3:
                this.bus.post(new PhoneEvent.Builder().number("323-222-1111").status(PhoneStatus.PHONE_IDLE).contact_name("Al Jazeera").build());
                return;
            case 4:
                this.bus.post(new PhoneBatteryStatus(BatteryStatus.BATTERY_LOW, Integer.valueOf(12), Boolean.valueOf(false)));
                return;
            case 5:
                this.bus.post(new PhoneBatteryStatus(BatteryStatus.BATTERY_EXTREMELY_LOW, Integer.valueOf(4), Boolean.valueOf(false)));
                return;
            case 6:
                this.bus.post(new PhoneBatteryStatus(BatteryStatus.BATTERY_OK, Integer.valueOf(36), Boolean.valueOf(false)));
                return;
            case 7:
                actionsList = new ArrayList(1);
                actionsList.add(GlanceActions.REPLY);
                data = new ArrayList();
                data.add(new KeyValue(MessageConstants.MESSAGE_FROM.name(), "John Doe"));
                data.add(new KeyValue(MessageConstants.MESSAGE_FROM_NUMBER.name(), "4081111111"));
                data.add(new KeyValue(MessageConstants.MESSAGE_BODY.name(), "Hi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing"));
                data.add(new KeyValue(MessageConstants.MESSAGE_IS_SMS.name(), ""));
                this.bus.post(new Builder().glanceType(GlanceType.GLANCE_TYPE_MESSAGE).id(UUID.randomUUID().toString()).postTime(Long.valueOf(System.currentTimeMillis())).provider(GlanceHelper.SMS_PACKAGE).actions(actionsList).glanceData(data).build());
                return;
            case 8:
                data = new ArrayList();
                data.add(new KeyValue(MessageConstants.MESSAGE_FROM.name(), "Santa Singh"));
                data.add(new KeyValue(MessageConstants.MESSAGE_FROM_NUMBER.name(), "999-999-0999"));
                data.add(new KeyValue(MessageConstants.MESSAGE_BODY.name(), "Hi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doinghow are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doinghow are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing\nHi how are u doing"));
                data.add(new KeyValue(MessageConstants.MESSAGE_IS_SMS.name(), ""));
                this.bus.post(new Builder().glanceType(GlanceType.GLANCE_TYPE_MESSAGE).id(UUID.randomUUID().toString()).postTime(Long.valueOf(System.currentTimeMillis())).provider(GlanceHelper.SMS_PACKAGE).glanceData(data).build());
                return;
            case 9:
                actionsList = new ArrayList(1);
                actionsList.add(GlanceActions.REPLY);
                data = new ArrayList();
                data.add(new KeyValue(MessageConstants.MESSAGE_FROM.name(), "Barack Obama"));
                data.add(new KeyValue(MessageConstants.MESSAGE_FROM_NUMBER.name(), "9999999999"));
                data.add(new KeyValue(MessageConstants.MESSAGE_BODY.name(), "Hi how are u doing"));
                data.add(new KeyValue(MessageConstants.MESSAGE_IS_SMS.name(), ""));
                this.bus.post(new Builder().glanceType(GlanceType.GLANCE_TYPE_MESSAGE).id(UUID.randomUUID().toString()).postTime(Long.valueOf(System.currentTimeMillis())).provider(GlanceHelper.SMS_PACKAGE).actions(actionsList).glanceData(data).build());
                return;
            case 10:
                DialNotification.showConnectedToast();
                return;
            case 11:
                DialNotification.showDisconnectedToast("Navdy Dial (test)");
                return;
            case 12:
                DialNotification.showForgottenToast(false, "Navdy Dial (test)");
                return;
            case 13:
                DialNotification.showForgottenToast(true, "Navdy Dial (AAAA), Navdy Dial (BBBB)");
                return;
            case 14:
                DialNotification.showLowBatteryToast();
                return;
            case 15:
                DialNotification.showExtremelyLowBatteryToast();
                return;
            case 16:
                DialNotification.showVeryLowBatteryToast();
                return;
            case 17:
                DialNotification.dismissAllBatteryToasts();
                return;
            case 18:
                this.bus.post(new ShowScreen.Builder().screen(Screen.SCREEN_MUSIC).build());
                return;
            case 19:
                this.bus.post(new ShowScreen.Builder().screen(Screen.SCREEN_VOICE_CONTROL).build());
                return;
            case 20:
                this.bus.post(new ShowScreen.Builder().screen(Screen.SCREEN_BRIGHTNESS).build());
                return;
            case 21:
                this.bus.post(new TrafficRerouteEvent("Heaven", "Heaven, Hell", 1500, System.currentTimeMillis() + 300000, 10000, 0));
                return;
            case 22:
                this.bus.post(new TrafficRerouteDismissEvent());
                return;
            case 23:
                this.bus.post(new TrafficJamProgressEvent(STSAssumeRoleSessionCredentialsProvider.DEFAULT_DURATION_SECONDS));
                return;
            case 24:
                this.bus.post(new TrafficJamDismissEvent());
                return;
            case 25:
                this.bus.post(new LiveTrafficEvent(Type.INCIDENT, Severity.HIGH));
                return;
            case 26:
                this.bus.post(new LiveTrafficDismissEvent());
                return;
            case 27:
                this.bus.post(new TrafficDelayEvent(420));
                return;
            case 28:
                this.bus.post(new TrafficDelayDismissEvent());
                return;
            case 29:
                ConnectionNotification.showDisconnectedToast(false);
                return;
            case 30:
                ConnectionNotification.showConnectedToast();
                return;
            case 31:
                ConnectionNotification.showDisconnectedToast(true);
                return;
            case 32:
                ToastManager.getInstance().dismissCurrentToast();
                return;
            case 33:
                ToastManager.getInstance().clearAllPendingToast();
                return;
            case 34:
                ToastManager.getInstance().clearAllPendingToast();
                ToastManager.getInstance().dismissCurrentToast();
                return;
            case 35:
                meetingTime = System.currentTimeMillis() + TimeUnit.MINUTES.toMillis(10);
                data = new ArrayList();
                data.add(new KeyValue(CalendarConstants.CALENDAR_TITLE.name(), "Meeting with Obama"));
                data.add(new KeyValue(CalendarConstants.CALENDAR_TIME_STR.name(), "\u200e\u202a12:00 \u2013 12:10 PM\u202c\u200e"));
                data.add(new KeyValue(CalendarConstants.CALENDAR_LOCATION.name(), "575 7th Street, San Francisco, CA 94103"));
                this.bus.post(new Builder().glanceType(GlanceType.GLANCE_TYPE_CALENDAR).id(UUID.randomUUID().toString()).postTime(Long.valueOf(meetingTime)).provider("com.google.android.calendar").glanceData(data).build());
                return;
            case 36:
                meetingTime = System.currentTimeMillis();
                data = new ArrayList();
                data.add(new KeyValue(CalendarConstants.CALENDAR_TITLE.name(), "Huddle"));
                data.add(new KeyValue(CalendarConstants.CALENDAR_TIME.name(), String.valueOf(meetingTime)));
                data.add(new KeyValue(CalendarConstants.CALENDAR_TIME_STR.name(), "1:30 pm - 1:45 pm"));
                this.bus.post(new Builder().glanceType(GlanceType.GLANCE_TYPE_CALENDAR).id(UUID.randomUUID().toString()).postTime(Long.valueOf(meetingTime)).provider(NotificationSettings.APP_ID_CALENDAR).glanceData(data).build());
                return;
            case 37:
                if (GlanceHelper.isFuelNotificationEnabled()) {
                    data = new ArrayList();
                    data.add(new KeyValue(FuelConstants.FUEL_LEVEL.name(), "14"));
                    this.bus.post(new Builder().glanceType(GlanceType.GLANCE_TYPE_FUEL).id(UUID.randomUUID().toString()).postTime(Long.valueOf(System.currentTimeMillis())).provider(GlanceHelper.FUEL_PACKAGE).glanceData(data).build());
                    return;
                }
                return;
            case 38:
                data = new ArrayList();
                data.add(new KeyValue(FuelConstants.FUEL_LEVEL.name(), "14"));
                this.bus.post(new Builder().glanceType(GlanceType.GLANCE_TYPE_FUEL).id(UUID.randomUUID().toString()).postTime(Long.valueOf(System.currentTimeMillis())).provider(GlanceHelper.FUEL_PACKAGE).glanceData(data).build());
                return;
            case 39:
                NotificationManager.getInstance().removeNotification(FuelRoutingManager.LOW_FUEL_ID);
                return;
            case 40:
                this.bus.post(new TestObdLowFuelLevel());
                return;
            case 41:
                this.bus.post(new ClearTestObdLowFuelLevel());
                return;
            case 42:
                this.bus.post(new FuelAddedTestEvent());
                return;
            case 43:
                try {
                    if (HereMapsManager.getInstance().isInitialized()) {
                        elapsedRealtime = SystemClock.elapsedRealtime();
                        HerePlacesManager.handleCategoriesRequest(FuelRoutingManager.GAS_CATEGORY, 3, new OnCategoriesSearchListener() {
                            public void onCompleted(List<Place> places) {
                                try {
                                    CustomNotificationServiceHandler.sLogger.v("FIND_GAS_STATION: found gas station (" + places.size() + ") time to find [" + (SystemClock.elapsedRealtime() - elapsedRealtime) + "]");
                                    int counter = 1;
                                    GeoCoordinate currentPos = HereMapsManager.getInstance().getRouteStartPoint();
                                    if (currentPos == null) {
                                        currentPos = HereMapsManager.getInstance().getLocationFixManager().getLastGeoCoordinate();
                                    } else {
                                        CustomNotificationServiceHandler.sLogger.v("using debug start point:" + currentPos);
                                    }
                                    if (currentPos == null) {
                                        CustomNotificationServiceHandler.sLogger.e("FIND_GAS_STATION no current position");
                                        return;
                                    }
                                    for (Place place : places) {
                                        Location location = place.getLocation();
                                        GeoCoordinate gasNavGeo = HerePlacesManager.getPlaceEntry(place);
                                        String address = location.getAddress().toString().replace("<br/>", ", ").replace(GlanceConstants.NEWLINE, "");
                                        CustomNotificationServiceHandler.sLogger.v("FIND_GAS_STATION [" + counter + "]" + " name [" + place.getName() + "]" + " address [" + address + "]" + " distance [" + ((int) currentPos.distanceTo(gasNavGeo)) + "] meters" + " displayPos [" + location.getCoordinate() + "]" + " navPos [" + gasNavGeo + "]");
                                        counter++;
                                    }
                                } catch (Throwable t) {
                                    CustomNotificationServiceHandler.sLogger.e("FIND_GAS_STATION", t);
                                }
                            }

                            public void onError(Error error) {
                                CustomNotificationServiceHandler.sLogger.v("FIND_GAS_STATION: could not find gas station:" + error + " time to error [" + (SystemClock.elapsedRealtime() - elapsedRealtime) + "]");
                            }
                        });
                        return;
                    }
                    sLogger.v("FIND_GAS_STATION: here maps not initialized");
                    return;
                } catch (Throwable t) {
                    sLogger.e("FIND_GAS_STATION", t);
                    return;
                }
            case 44:
                try {
                    if (HereMapsManager.getInstance().isInitialized()) {
                        elapsedRealtime = SystemClock.elapsedRealtime();
                        FuelRoutingManager.getInstance().findNearestGasStation(new OnNearestGasStationCallback() {
                            public void onComplete(NavigationRouteResult result) {
                                CustomNotificationServiceHandler.sLogger.v("FIND_ROUTE_TO_CLOSEST_GAS_STATION: success: time  [" + (SystemClock.elapsedRealtime() - elapsedRealtime) + "]");
                                CustomNotificationServiceHandler.sLogger.v("FIND_ROUTE_TO_CLOSEST_GAS_STATION route id[" + result.routeId + "] label[" + result.label + "] via[" + result.via + "] address[" + (result.address != null ? result.address.replace(GlanceConstants.NEWLINE, "") : Constants.NULL_VERSION_ID) + "] length[" + result.length + "] duration[" + result.duration_traffic + "] freeFlowDuration[" + result.duration + "]");
                            }

                            public void onError(OnRouteToGasStationCallback.Error error) {
                                CustomNotificationServiceHandler.sLogger.v("FIND_ROUTE_TO_CLOSEST_GAS_STATION: failed:" + error + " time to error [" + (SystemClock.elapsedRealtime() - elapsedRealtime) + "]");
                            }
                        });
                        return;
                    }
                    sLogger.v("FIND_ROUTE_TO_CLOSEST_GAS_STATION: here maps not initialized");
                    return;
                } catch (Throwable t2) {
                    sLogger.e("FIND_ROUTE_TO_CLOSEST_GAS_STATION", t2);
                    return;
                }
            default:
                return;
        }
    }
}
