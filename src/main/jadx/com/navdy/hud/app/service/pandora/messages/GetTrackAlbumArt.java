package com.navdy.hud.app.service.pandora.messages;

import com.navdy.hud.app.service.pandora.exceptions.MessageWrongWayException;
import com.navdy.hud.app.service.pandora.exceptions.StringOverflowException;
import com.navdy.hud.app.service.pandora.exceptions.UnexpectedEndOfStringException;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class GetTrackAlbumArt extends BaseOutgoingConstantMessage {
    public static final GetTrackAlbumArt INSTANCE = new GetTrackAlbumArt();

    public /* bridge */ /* synthetic */ byte[] buildPayload() throws IOException, StringOverflowException, UnexpectedEndOfStringException, MessageWrongWayException {
        return super.buildPayload();
    }

    private GetTrackAlbumArt() {
    }

    protected ByteArrayOutputStream putThis(ByteArrayOutputStream os) throws IOException, StringOverflowException, UnexpectedEndOfStringException, MessageWrongWayException {
        BaseOutgoingMessage.putByte(os, BaseMessage.PNDR_GET_TRACK_ALBUM_ART);
        BaseOutgoingMessage.putInt(os, BaseMessage.MAX_ARTWORK_PAYLOAD_SIZE);
        return os;
    }

    public String toString() {
        return "Requesting album artwork";
    }
}
