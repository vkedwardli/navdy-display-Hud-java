package com.navdy.hud.app.service.pandora.messages;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

public abstract class BaseMessage {
    public static final String ACCESSORY_ID = "CFDA92FC";
    public static final short API_VERSION = (short) 3;
    protected static final Charset FIXED_LENGTH_STRING_ENCODING = StandardCharsets.US_ASCII;
    protected static final int INT_BYTES_SIZE = 4;
    public static final int MAX_ARTWORK_PAYLOAD_SIZE = 9025;
    protected static final int MAX_STRING_BYTE_LENGTH = 247;
    private static final short PNDR_API_VERSION_1 = (short) 1;
    private static final short PNDR_API_VERSION_3 = (short) 3;
    public static final byte PNDR_EVENT_TRACK_PLAY = (byte) 48;
    protected static final byte PNDR_FALSE = (byte) 0;
    public static final byte PNDR_GET_TRACK_ALBUM_ART = (byte) 20;
    public static final byte PNDR_GET_TRACK_INFO_EXTENDED = (byte) 22;
    private static final byte PNDR_IMAGE_JPEG = (byte) 1;
    private static final byte PNDR_IMAGE_NONE = (byte) 0;
    private static final byte PNDR_IMAGE_PNG = (byte) 2;
    private static final byte PNDR_IMAGE_RGB565 = (byte) 3;
    public static final byte PNDR_RETURN_TRACK_ALBUM_ART_SEGMENT = (byte) -107;
    public static final byte PNDR_RETURN_TRACK_INFO_EXTENDED = (byte) -99;
    private static final byte PNDR_SESSION_FLAG_PAUSE_ON_START = (byte) 2;
    public static final byte PNDR_SESSION_START = (byte) 0;
    public static final byte PNDR_SESSION_TERMINATE = (byte) 5;
    public static final byte PNDR_SET_TRACK_ELAPSED_POLLING = (byte) 21;
    public static final byte PNDR_STATUS_INCOMPATIBLE_API_VERSION = (byte) 3;
    public static final byte PNDR_STATUS_INSUFFICIENT_CONNECTIVITY = (byte) 7;
    public static final byte PNDR_STATUS_INVALID_LOGIN = (byte) 9;
    public static final byte PNDR_STATUS_LICENSING_RESTRICTIONS = (byte) 8;
    public static final byte PNDR_STATUS_NO_STATIONS = (byte) 5;
    public static final byte PNDR_STATUS_NO_STATION_ACTIVE = (byte) 6;
    public static final byte PNDR_STATUS_PAUSED = (byte) 2;
    public static final byte PNDR_STATUS_PLAYING = (byte) 1;
    public static final byte PNDR_STATUS_UNKNOWN_ERROR = (byte) 4;
    public static final byte PNDR_TRACK_FLAG_ALLOW_SKIP = (byte) 2;
    public static final int PNDR_TRACK_NONE = 0;
    protected static final byte PNDR_TRUE = (byte) 1;
    public static final byte PNDR_UPDATE_STATION_ACTIVE = (byte) -70;
    public static final byte PNDR_UPDATE_STATUS = (byte) -127;
    public static final byte PNDR_UPDATE_TRACK = (byte) -112;
    public static final byte PNDR_UPDATE_TRACK_ALBUM_ART = (byte) -106;
    public static final byte PNDR_UPDATE_TRACK_COMPLETED = (byte) -98;
    public static final byte PNDR_UPDATE_TRACK_ELAPSED = (byte) -105;
    public static final byte REQUIRED_ARTWORK_IMAGE_TYPE = (byte) 1;
    public static final short REQUIRED_ARTWORK_SIZE = (short) 100;
    public static final short REQUIRED_STATION_ARTWORK_SIZE = (short) 0;
    public static final byte SESSION_START_FLAGS = (byte) 2;
    protected static final int SHORT_BYTES_SIZE = 2;
    protected static final byte STRING_BORDER_BYTE = (byte) 0;
    protected static final Charset STRING_ENCODING = StandardCharsets.UTF_8;

    public abstract String toString();
}
