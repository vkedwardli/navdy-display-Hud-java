package com.navdy.hud.app.service.pandora;

import java.nio.ByteBuffer;
import java.util.Arrays;

class CRC16CCITT {
    private static final int CRC_INITIAL_VALUE = 65535;

    CRC16CCITT() {
    }

    public static ByteBuffer calculate(ByteBuffer buffer) {
        int crc = 65535;
        for (int i = 0; i < buffer.limit(); i++) {
            crc = (((crc >>> 8) | (crc << 8)) & 65535) ^ (buffer.get(i) & 255);
            crc ^= (crc & 255) >> 4;
            crc ^= (crc << 12) & 65535;
            crc ^= ((crc & 255) << 5) & 65535;
        }
        return ByteBuffer.allocate(2).putShort((short) crc);
    }

    public static boolean checkCRC(byte[] data, byte[] crc) {
        return Arrays.equals(calculate(ByteBuffer.wrap(data)).array(), crc);
    }
}
