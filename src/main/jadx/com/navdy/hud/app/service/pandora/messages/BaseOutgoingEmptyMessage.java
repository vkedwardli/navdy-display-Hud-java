package com.navdy.hud.app.service.pandora.messages;

import com.navdy.hud.app.service.pandora.exceptions.StringOverflowException;
import com.navdy.hud.app.service.pandora.exceptions.UnexpectedEndOfStringException;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

abstract class BaseOutgoingEmptyMessage extends BaseOutgoingConstantMessage {
    protected abstract byte getMessageType();

    BaseOutgoingEmptyMessage() {
    }

    protected ByteArrayOutputStream putThis(ByteArrayOutputStream os) throws IOException, StringOverflowException, UnexpectedEndOfStringException {
        BaseOutgoingMessage.putByte(os, getMessageType());
        return os;
    }
}
