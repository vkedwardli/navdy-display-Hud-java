package com.navdy.hud.app.config;

import android.content.SharedPreferences;
import com.navdy.service.library.log.Logger;
import com.squareup.otto.Bus;

public class SettingsManager implements IObserver {
    private static final Logger sLogger = new Logger(SettingsManager.class);
    private final Bus bus;
    private final SharedPreferences preferences;

    public static class SettingsChanged {
        public final String path;
        public final Setting setting;

        public SettingsChanged(String path, Setting setting) {
            this.path = path;
            this.setting = setting;
        }
    }

    public SettingsManager(Bus bus, SharedPreferences preferences) {
        this.bus = bus;
        this.preferences = preferences;
    }

    public void addSetting(Setting setting) {
        sLogger.i("Adding setting " + setting);
        setting.setObserver(this);
    }

    public void onChanged(Setting setting) {
        sLogger.i("Setting changed:" + setting);
        this.bus.post(new SettingsChanged(setting.getPath(), setting));
    }
}
