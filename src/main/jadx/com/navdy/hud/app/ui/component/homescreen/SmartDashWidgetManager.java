package com.navdy.hud.app.ui.component.homescreen;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.framework.DriverProfileHelper;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.obd.ObdManager;
import com.navdy.hud.app.view.CompassPresenter;
import com.navdy.hud.app.view.DashboardWidgetPresenter;
import com.navdy.hud.app.view.DriveScoreGaugePresenter;
import com.navdy.hud.app.view.EngineTemperaturePresenter;
import com.navdy.hud.app.view.FuelGaugePresenter2;
import com.navdy.hud.app.view.MPGGaugePresenter;
import com.navdy.hud.app.view.SpeedLimitSignPresenter;
import com.navdy.service.library.log.Logger;
import com.squareup.otto.Bus;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

public class SmartDashWidgetManager {
    public static final String ANALOG_CLOCK_WIDGET_ID = "ANALOG_CLOCK_WIDGET";
    public static final String CALENDAR_WIDGET_ID = "CALENDAR_WIDGET";
    public static final String COMPASS_WIDGET_ID = "COMPASS_WIDGET";
    public static final String DIGITAL_CLOCK_2_WIDGET_ID = "DIGITAL_CLOCK_2_WIDGET";
    public static final String DIGITAL_CLOCK_WIDGET_ID = "DIGITAL_CLOCK_WIDGET";
    public static final String DRIVE_SCORE_GAUGE_ID = "DRIVE_SCORE_GAUGE_ID";
    public static final String EMPTY_WIDGET_ID = "EMPTY_WIDGET";
    public static final String ENGINE_TEMPERATURE_GAUGE_ID = "ENGINE_TEMPERATURE_GAUGE_ID";
    public static final String ETA_GAUGE_ID = "ETA_GAUGE_ID";
    public static final String FUEL_GAUGE_ID = "FUEL_GAUGE_ID";
    private static List<String> GAUGES = new ArrayList();
    private static HashSet<String> GAUGE_NAME_LOOKUP = new HashSet();
    public static final String GFORCE_WIDGET_ID = "GFORCE_WIDGET";
    public static final String MPG_AVG_WIDGET_ID = "MPG_AVG_WIDGET";
    public static final String MPG_GRAPH_WIDGET_ID = "MPG_GRAPH_WIDGET";
    public static final String MUSIC_WIDGET_ID = "MUSIC_WIDGET";
    public static final String PREFERENCE_GAUGE_ENABLED = "PREF_GAUGE_ENABLED_";
    public static final String SPEED_LIMIT_SIGN_GAUGE_ID = "SPEED_LIMIT_SIGN_GAUGE_ID";
    public static final String TRAFFIC_INCIDENT_GAUGE_ID = "TRAFFIC_INCIDENT_GAUGE_ID";
    public static final String WEATHER_WIDGET_ID = "WEATHER_GRAPH_WIDGET";
    private static final Logger sLogger = new Logger(SmartDashWidgetManager.class);
    private Bus bus;
    private SharedPreferences currentUserPreferences;
    private IWidgetFilter filter;
    private SharedPreferences globalPreferences;
    private LifecycleEvent lastLifeCycleEvent = null;
    private Context mContext;
    private List<String> mGaugeIds;
    private boolean mLoaded = false;
    private HashMap<String, Integer> mWidgetIndexMap;
    private HashMap<Integer, SmartDashWidgetCache> mWidgetPresentersCache = new HashMap();

    public interface IWidgetFilter {
        boolean filter(String str);
    }

    public enum LifecycleEvent {
        PAUSE,
        RESUME
    }

    public enum Reload {
        RELOAD_CACHE,
        RELOADED
    }

    public class SmartDashWidgetCache {
        private Context mContext;
        private List<String> mGaugeIds = new ArrayList();
        private HashMap<String, Integer> mWidgetIndexMap = new HashMap();
        private HashMap<String, DashboardWidgetPresenter> mWidgetPresentersMap = new HashMap();

        public SmartDashWidgetCache(Context context) {
            this.mContext = context;
        }

        public void add(String identifier) {
            if (!this.mWidgetIndexMap.containsKey(identifier)) {
                this.mGaugeIds.add(identifier);
                this.mWidgetIndexMap.put(identifier, Integer.valueOf(this.mGaugeIds.size() - 1));
                this.mWidgetPresentersMap.put(identifier, DashWidgetPresenterFactory.createDashWidgetPresenter(this.mContext, identifier));
                initializeWidget(identifier);
            }
        }

        public int getIndexForWidget(String identifier) {
            if (this.mWidgetIndexMap.containsKey(identifier)) {
                return ((Integer) this.mWidgetIndexMap.get(identifier)).intValue();
            }
            return -1;
        }

        public DashboardWidgetPresenter getWidgetPresenter(int index) {
            if (index < 0 || index >= this.mGaugeIds.size()) {
                return null;
            }
            return (DashboardWidgetPresenter) this.mWidgetPresentersMap.get((String) this.mGaugeIds.get(index));
        }

        public DashboardWidgetPresenter getWidgetPresenter(String identifier) {
            return getWidgetPresenter(getIndexForWidget(identifier));
        }

        public int getWidgetsCount() {
            return this.mGaugeIds.size();
        }

        private void initializeWidget(String identifier) {
            Object obj = -1;
            switch (identifier.hashCode()) {
                case -131933527:
                    if (identifier.equals(SmartDashWidgetManager.ENGINE_TEMPERATURE_GAUGE_ID)) {
                        obj = 2;
                        break;
                    }
                    break;
                case 460083427:
                    if (identifier.equals(SmartDashWidgetManager.DRIVE_SCORE_GAUGE_ID)) {
                        obj = 1;
                        break;
                    }
                    break;
                case 1168400042:
                    if (identifier.equals(SmartDashWidgetManager.FUEL_GAUGE_ID)) {
                        obj = null;
                        break;
                    }
                    break;
            }
            switch (obj) {
                case null:
                    FuelGaugePresenter2 fuelGaugePresenter2 = (FuelGaugePresenter2) getWidgetPresenter(identifier);
                    int fuelLevel = ObdManager.getInstance().getFuelLevel();
                    if (fuelGaugePresenter2 != null) {
                        fuelGaugePresenter2.setFuelLevel(fuelLevel);
                        return;
                    }
                    return;
                case 1:
                    DriveScoreGaugePresenter driveScoreGaugePresenter = (DriveScoreGaugePresenter) getWidgetPresenter(identifier);
                    if (driveScoreGaugePresenter != null) {
                        driveScoreGaugePresenter.setDriveScoreUpdated(RemoteDeviceManager.getInstance().getTelemetryDataManager().getDriveScoreUpdatedEvent());
                        return;
                    }
                    return;
                case 2:
                    EngineTemperaturePresenter engineTemperaturePresenter = (EngineTemperaturePresenter) getWidgetPresenter(identifier);
                    double engineTemperature = ObdManager.getInstance().getPidValue(5);
                    if (engineTemperaturePresenter != null) {
                        if (engineTemperature == -1.0d) {
                            engineTemperature = EngineTemperaturePresenter.Companion.getTEMPERATURE_GAUGE_MID_POINT_CELSIUS();
                        }
                        engineTemperaturePresenter.setEngineTemperature(engineTemperature);
                        return;
                    }
                    return;
                default:
                    return;
            }
        }

        public void updateWidget(String identifier, Object data) {
            if (!TextUtils.isEmpty(identifier)) {
                Object obj = -1;
                switch (identifier.hashCode()) {
                    case -1874661839:
                        if (identifier.equals(SmartDashWidgetManager.COMPASS_WIDGET_ID)) {
                            obj = 1;
                            break;
                        }
                        break;
                    case -1158963060:
                        if (identifier.equals(SmartDashWidgetManager.MPG_AVG_WIDGET_ID)) {
                            obj = 2;
                            break;
                        }
                        break;
                    case -131933527:
                        if (identifier.equals(SmartDashWidgetManager.ENGINE_TEMPERATURE_GAUGE_ID)) {
                            obj = 4;
                            break;
                        }
                        break;
                    case 1119776967:
                        if (identifier.equals(SmartDashWidgetManager.SPEED_LIMIT_SIGN_GAUGE_ID)) {
                            obj = 3;
                            break;
                        }
                        break;
                    case 1168400042:
                        if (identifier.equals(SmartDashWidgetManager.FUEL_GAUGE_ID)) {
                            obj = null;
                            break;
                        }
                        break;
                }
                switch (obj) {
                    case null:
                        FuelGaugePresenter2 fuelGaugePresenter2 = (FuelGaugePresenter2) getWidgetPresenter(identifier);
                        if (fuelGaugePresenter2 != null && (data instanceof Double)) {
                            fuelGaugePresenter2.setFuelLevel(((Number) data).intValue());
                            return;
                        }
                        return;
                    case 1:
                        CompassPresenter compassPresenter = (CompassPresenter) getWidgetPresenter(identifier);
                        if (compassPresenter != null) {
                            compassPresenter.setHeadingAngle(((Number) data).doubleValue());
                            return;
                        }
                        return;
                    case 2:
                        MPGGaugePresenter mpgGaugePresenter2 = (MPGGaugePresenter) getWidgetPresenter(identifier);
                        if (mpgGaugePresenter2 != null) {
                            mpgGaugePresenter2.setCurrentMPG(((Number) data).doubleValue());
                            return;
                        }
                        return;
                    case 3:
                        SpeedLimitSignPresenter speedLimitSignPresenter = (SpeedLimitSignPresenter) getWidgetPresenter(identifier);
                        if (speedLimitSignPresenter != null) {
                            speedLimitSignPresenter.setSpeedLimit(((Number) data).intValue());
                            return;
                        }
                        return;
                    case 4:
                        EngineTemperaturePresenter engineTemperaturePresenter = (EngineTemperaturePresenter) getWidgetPresenter(identifier);
                        if (engineTemperaturePresenter != null) {
                            engineTemperaturePresenter.setEngineTemperature((double) ((Number) data).intValue());
                            return;
                        }
                        return;
                    default:
                        return;
                }
            }
        }

        public HashMap<String, DashboardWidgetPresenter> getWidgetPresentersMap() {
            return this.mWidgetPresentersMap;
        }

        public void clear() {
            this.mGaugeIds.clear();
            this.mWidgetPresentersMap.clear();
            this.mWidgetIndexMap.clear();
        }
    }

    public static class UserPreferenceChanged {
    }

    static {
        GAUGES.add(CALENDAR_WIDGET_ID);
        GAUGES.add(COMPASS_WIDGET_ID);
        GAUGES.add(ANALOG_CLOCK_WIDGET_ID);
        GAUGES.add(DIGITAL_CLOCK_WIDGET_ID);
        GAUGES.add(DIGITAL_CLOCK_2_WIDGET_ID);
        GAUGES.add(DRIVE_SCORE_GAUGE_ID);
        GAUGES.add(ENGINE_TEMPERATURE_GAUGE_ID);
        GAUGES.add(FUEL_GAUGE_ID);
        GAUGES.add(GFORCE_WIDGET_ID);
        GAUGES.add(MUSIC_WIDGET_ID);
        GAUGES.add(MPG_AVG_WIDGET_ID);
        GAUGES.add(SPEED_LIMIT_SIGN_GAUGE_ID);
        GAUGES.add(TRAFFIC_INCIDENT_GAUGE_ID);
        GAUGES.add(ETA_GAUGE_ID);
        GAUGES.add(EMPTY_WIDGET_ID);
        for (String name : GAUGES) {
            GAUGE_NAME_LOOKUP.add(name);
        }
    }

    public boolean isGaugeOn(String gaugeIdentifier) {
        if (this.currentUserPreferences == null) {
            sLogger.d("isGaugeOn : Current Preferences is not set, using the global preferences");
            this.currentUserPreferences = this.globalPreferences;
        }
        return this.currentUserPreferences.getBoolean(PREFERENCE_GAUGE_ENABLED + gaugeIdentifier, true);
    }

    public void setGaugeOn(String identifier, boolean on) {
        if (this.currentUserPreferences == null) {
            sLogger.d("setGaugeOn : Current Preferences is not set, using the global preferences");
            this.currentUserPreferences = this.globalPreferences;
        }
        this.currentUserPreferences.edit().putBoolean(PREFERENCE_GAUGE_ENABLED + identifier, on).apply();
    }

    public static boolean isValidGaugeId(String gaugeName) {
        return !TextUtils.isEmpty(gaugeName) && GAUGE_NAME_LOOKUP.contains(gaugeName);
    }

    public SmartDashWidgetManager(SharedPreferences globalPreferences, Context context) {
        this.globalPreferences = globalPreferences;
        this.mContext = context;
        this.mGaugeIds = new ArrayList();
        this.mWidgetIndexMap = new HashMap();
        this.bus = new Bus();
    }

    public void reLoadAvailableWidgets(boolean loadAll) {
        sLogger.v("reLoadAvailableWidgets");
        this.mGaugeIds.clear();
        this.mWidgetIndexMap.clear();
        boolean isDefaultProfile = DriverProfileHelper.getInstance().getCurrentProfile().isDefaultProfile();
        sLogger.d("Default profile ? " + isDefaultProfile);
        this.currentUserPreferences = isDefaultProfile ? this.globalPreferences : DriverProfileHelper.getInstance().getDriverProfileManager().getLocalPreferencesForCurrentDriverProfile(HudApplication.getAppContext());
        int i = 0;
        for (String identifier : GAUGES) {
            if ((!loadAll && !isGaugeOn(identifier)) || this.filter == null || this.filter.filter(identifier)) {
                sLogger.d("Not adding Gauge, as its filtered out , ID : " + identifier);
            } else {
                this.mGaugeIds.add(identifier);
                sLogger.d("adding Gauge : " + identifier + ", at : " + i);
                int i2 = i + 1;
                this.mWidgetIndexMap.put(identifier, Integer.valueOf(i));
                i = i2;
            }
        }
        if (this.mGaugeIds.size() == 0) {
            this.mGaugeIds.add(EMPTY_WIDGET_ID);
            this.mWidgetIndexMap.put(EMPTY_WIDGET_ID, Integer.valueOf(0));
        }
        if (this.mLoaded) {
            this.bus.post(Reload.RELOAD_CACHE);
            this.bus.post(Reload.RELOADED);
            return;
        }
        this.mLoaded = true;
    }

    public int getIndexForWidgetIdentifier(String identifier) {
        if (this.mWidgetIndexMap.containsKey(identifier)) {
            return ((Integer) this.mWidgetIndexMap.get(identifier)).intValue();
        }
        return -1;
    }

    public String getWidgetIdentifierForIndex(int index) {
        if (index < 0 || index >= this.mGaugeIds.size()) {
            return null;
        }
        return (String) this.mGaugeIds.get(index);
    }

    public void registerForChanges(Object object) {
        this.bus.register(object);
    }

    public SmartDashWidgetCache buildSmartDashWidgetCache(int clientIdentifier) {
        SmartDashWidgetCache cache;
        if (this.mWidgetPresentersCache.containsKey(Integer.valueOf(clientIdentifier))) {
            cache = (SmartDashWidgetCache) this.mWidgetPresentersCache.get(Integer.valueOf(clientIdentifier));
            cache.clear();
            sLogger.v("widget::: cache cleared for " + clientIdentifier);
        } else {
            cache = new SmartDashWidgetCache(this.mContext);
            this.mWidgetPresentersCache.put(Integer.valueOf(clientIdentifier), cache);
            sLogger.v("widget::: cache created for " + clientIdentifier);
        }
        for (String gaugeId : this.mGaugeIds) {
            cache.add(gaugeId);
        }
        if (this.lastLifeCycleEvent != null) {
            sendLifecycleEvent(this.lastLifeCycleEvent, cache.getWidgetPresentersMap().values().iterator());
        }
        return cache;
    }

    public void updateWidget(String identifier, Object data) {
        Collection<SmartDashWidgetCache> cacheCollection = this.mWidgetPresentersCache.values();
        if (cacheCollection != null) {
            for (SmartDashWidgetCache cache : cacheCollection) {
                if (cache != null) {
                    cache.updateWidget(identifier, data);
                }
            }
        }
    }

    public void onPause() {
        this.lastLifeCycleEvent = LifecycleEvent.PAUSE;
        sendLifecycleEvent(LifecycleEvent.PAUSE);
    }

    public void onResume() {
        this.lastLifeCycleEvent = LifecycleEvent.RESUME;
        sendLifecycleEvent(LifecycleEvent.RESUME);
    }

    private void sendLifecycleEvent(LifecycleEvent event) {
        for (SmartDashWidgetCache cache : this.mWidgetPresentersCache.values()) {
            sendLifecycleEvent(event, cache.getWidgetPresentersMap().values().iterator());
        }
    }

    private void sendLifecycleEvent(LifecycleEvent event, Iterator<DashboardWidgetPresenter> iterator) {
        while (iterator.hasNext()) {
            switch (event) {
                case PAUSE:
                    ((DashboardWidgetPresenter) iterator.next()).onPause();
                    break;
                case RESUME:
                    ((DashboardWidgetPresenter) iterator.next()).onResume();
                    break;
                default:
                    break;
            }
        }
    }

    public void setFilter(IWidgetFilter filter) {
        this.filter = filter;
    }
}
