package com.navdy.hud.app.ui.component.carousel;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.animation.AnimatorSet;
import android.animation.AnimatorSet.Builder;
import android.animation.ObjectAnimator;
import android.os.SystemClock;
import android.view.View;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.R;
import com.navdy.hud.app.ui.component.carousel.AnimationStrategy.Direction;
import com.navdy.hud.app.ui.component.carousel.Carousel.Model;
import com.navdy.hud.app.ui.component.carousel.Carousel.ViewType;
import com.navdy.hud.app.ui.framework.DefaultAnimationListener;
import com.navdy.service.library.log.Logger;

public class FastScrollAnimator implements AnimationStrategy {
    public static final int ANIMATION_HERO = 50;
    public static final int ANIMATION_HERO_SINGLE = 133;
    public static final int ANIMATION_HERO_TEXT = 165;
    public static final int ANIMATION_HERO_TRANSLATE_X = ((int) HudApplication.getAppContext().getResources().getDimension(R.dimen.carousel_fastscroll_hero_translate_x));
    private static final Logger sLogger = new Logger(FastScrollAnimator.class);
    private CarouselLayout carouselLayout;
    private boolean endPending;
    private View hiddenView;
    private long lastScrollAnimationFinishTime;

    FastScrollAnimator(CarouselLayout carouselLayout) {
        this.carouselLayout = carouselLayout;
        this.hiddenView = carouselLayout.buildView(0, 0, ViewType.MIDDLE_LEFT, false);
    }

    public AnimatorSet createMiddleLeftViewAnimation(CarouselLayout carousel, Direction direction) {
        return null;
    }

    public AnimatorSet createMiddleRightViewAnimation(CarouselLayout carousel, Direction direction) {
        return null;
    }

    public AnimatorSet createSideViewToMiddleAnimation(CarouselLayout carousel, Direction direction) {
        return null;
    }

    public Animator createViewOutAnimation(CarouselLayout carousel, Direction direction) {
        return null;
    }

    public AnimatorSet createHiddenViewAnimation(CarouselLayout carousel, Direction direction) {
        return null;
    }

    public AnimatorSet createNewMiddleRightViewAnimation(CarouselLayout carousel, Direction direction) {
        return null;
    }

    public AnimatorSet buildLayoutAnimation(AnimatorListener listener, CarouselLayout carousel, int currentPos, int newPos) {
        int translateX;
        int hiddenX;
        AnimatorSet animatorSet = new AnimatorSet();
        if ((newPos > currentPos ? Direction.LEFT : Direction.RIGHT) == Direction.LEFT) {
            translateX = (int) (carousel.middleLeftView.getX() - ((float) ANIMATION_HERO_TRANSLATE_X));
            hiddenX = (int) (carousel.middleLeftView.getX() + ((float) ANIMATION_HERO_TRANSLATE_X));
        } else {
            translateX = (int) (carousel.middleLeftView.getX() + ((float) ANIMATION_HERO_TRANSLATE_X));
            hiddenX = (int) (carousel.middleLeftView.getX() - ((float) ANIMATION_HERO_TRANSLATE_X));
        }
        this.hiddenView.setX((float) hiddenX);
        this.hiddenView.setAlpha(0.0f);
        Builder builder = animatorSet.play(ObjectAnimator.ofFloat(carousel.middleLeftView, View.X, new float[]{(float) translateX}));
        builder.with(ObjectAnimator.ofFloat(carousel.middleLeftView, View.ALPHA, new float[]{0.0f}));
        builder.with(ObjectAnimator.ofFloat(this.hiddenView, View.X, new float[]{this.carouselLayout.middleLeftView.getX()}));
        builder.with(ObjectAnimator.ofFloat(this.hiddenView, View.ALPHA, new float[]{1.0f}));
        this.carouselLayout.carouselAdapter.getView(newPos, this.hiddenView, ViewType.MIDDLE_LEFT, this.carouselLayout.imageLytResourceId, this.carouselLayout.mainImageSize);
        if (this.carouselLayout.carouselIndicator != null) {
            Animator indicatorAnimator = this.carouselLayout.carouselIndicator.getItemMoveAnimator(newPos, -1);
            if (indicatorAnimator != null) {
                builder.with(indicatorAnimator);
            }
        }
        animatorSet.setInterpolator(this.carouselLayout.interpolator);
        final AnimatorListener animatorListener = listener;
        final int i = newPos;
        animatorSet.addListener(new DefaultAnimationListener() {
            public void onAnimationStart(Animator animation) {
                FastScrollAnimator.this.endPending = true;
                if (animatorListener != null) {
                    animatorListener.onAnimationStart(animation);
                }
                FastScrollAnimator.this.hiddenView.setVisibility(0);
                FastScrollAnimator.this.carouselLayout.leftView.setAlpha(0.0f);
                FastScrollAnimator.this.carouselLayout.rightView.setAlpha(0.0f);
                FastScrollAnimator.this.carouselLayout.middleRightView.setAlpha(0.0f);
                if (!FastScrollAnimator.this.carouselLayout.isAnimationPending()) {
                    FastScrollAnimator.this.carouselLayout.carouselAdapter.getView(i - 1, FastScrollAnimator.this.carouselLayout.leftView, ViewType.SIDE, FastScrollAnimator.this.carouselLayout.imageLytResourceId, FastScrollAnimator.this.carouselLayout.sideImageSize);
                    FastScrollAnimator.this.carouselLayout.carouselAdapter.getView(i + 1, FastScrollAnimator.this.carouselLayout.rightView, ViewType.SIDE, FastScrollAnimator.this.carouselLayout.imageLytResourceId, FastScrollAnimator.this.carouselLayout.sideImageSize);
                    FastScrollAnimator.this.carouselLayout.carouselAdapter.getView(i, FastScrollAnimator.this.carouselLayout.middleRightView, ViewType.MIDDLE_RIGHT, FastScrollAnimator.this.carouselLayout.imageLytResourceId, FastScrollAnimator.this.carouselLayout.sideImageSize);
                }
            }

            public void onAnimationEnd(Animator animation) {
                FastScrollAnimator.this.lastScrollAnimationFinishTime = SystemClock.elapsedRealtime();
                if (FastScrollAnimator.this.carouselLayout.isAnimationPending()) {
                    FastScrollAnimator.this.carouselLayout.currentItem = i;
                    View newMiddleLeft = FastScrollAnimator.this.hiddenView;
                    FastScrollAnimator.this.hiddenView = FastScrollAnimator.this.carouselLayout.middleLeftView;
                    FastScrollAnimator.this.hiddenView.setVisibility(4);
                    FastScrollAnimator.this.hiddenView.setAlpha(0.0f);
                    FastScrollAnimator.this.carouselLayout.middleLeftView = newMiddleLeft;
                    if (animatorListener != null) {
                        animatorListener.onAnimationEnd(animation);
                    }
                    FastScrollAnimator.this.carouselLayout.selectedItemView = FastScrollAnimator.this.carouselLayout.middleLeftView;
                    if (FastScrollAnimator.this.carouselLayout.carouselIndicator != null) {
                        FastScrollAnimator.this.carouselLayout.carouselIndicator.setCurrentItem(FastScrollAnimator.this.carouselLayout.currentItem);
                    }
                    if (FastScrollAnimator.this.carouselLayout.itemChangeListener != null) {
                        FastScrollAnimator.this.carouselLayout.itemChangeListener.onCurrentItemChanged(FastScrollAnimator.this.carouselLayout.currentItem, ((Model) FastScrollAnimator.this.carouselLayout.model.get(FastScrollAnimator.this.carouselLayout.currentItem)).id);
                    }
                    FastScrollAnimator.this.carouselLayout.runQueuedOperation();
                    return;
                }
                FastScrollAnimator.this.endAnimation(animatorListener, i, false);
            }
        });
        int duration = 50;
        if (!this.carouselLayout.isAnimationPending() && SystemClock.elapsedRealtime() - this.lastScrollAnimationFinishTime > 50) {
            duration = 133;
        }
        animatorSet.setDuration((long) duration);
        if (sLogger.isLoggable(3)) {
            sLogger.v("dur=" + duration);
        }
        return animatorSet;
    }

    void endAnimation(final AnimatorListener listener, final int newPos, final boolean skipEnd) {
        AnimatorSet set = new AnimatorSet();
        ObjectAnimator alpha1 = ObjectAnimator.ofFloat(this.carouselLayout.leftView, View.ALPHA, new float[]{1.0f});
        ObjectAnimator alpha2 = ObjectAnimator.ofFloat(this.carouselLayout.rightView, View.ALPHA, new float[]{1.0f});
        ObjectAnimator alpha3 = ObjectAnimator.ofFloat(this.carouselLayout.middleRightView, View.ALPHA, new float[]{1.0f});
        set.setDuration(165);
        set.playTogether(new Animator[]{alpha1, alpha2, alpha3});
        set.addListener(new DefaultAnimationListener() {
            public void onAnimationEnd(Animator animation) {
                FastScrollAnimator.this.endPending = false;
                if (!skipEnd) {
                    FastScrollAnimator.this.carouselLayout.currentItem = newPos;
                    View newMiddleLeft = FastScrollAnimator.this.hiddenView;
                    FastScrollAnimator.this.hiddenView = FastScrollAnimator.this.carouselLayout.middleLeftView;
                    FastScrollAnimator.this.hiddenView.setVisibility(4);
                    FastScrollAnimator.this.hiddenView.setAlpha(0.0f);
                    FastScrollAnimator.this.carouselLayout.middleLeftView = newMiddleLeft;
                    if (listener != null) {
                        listener.onAnimationEnd(animation);
                    }
                    FastScrollAnimator.this.carouselLayout.selectedItemView = FastScrollAnimator.this.carouselLayout.middleLeftView;
                    if (FastScrollAnimator.this.carouselLayout.carouselIndicator != null) {
                        FastScrollAnimator.this.carouselLayout.carouselIndicator.setCurrentItem(FastScrollAnimator.this.carouselLayout.currentItem);
                    }
                    if (FastScrollAnimator.this.carouselLayout.itemChangeListener != null) {
                        FastScrollAnimator.this.carouselLayout.itemChangeListener.onCurrentItemChanged(FastScrollAnimator.this.carouselLayout.currentItem, ((Model) FastScrollAnimator.this.carouselLayout.model.get(FastScrollAnimator.this.carouselLayout.currentItem)).id);
                    }
                }
                FastScrollAnimator.this.carouselLayout.runQueuedOperation();
            }
        });
        set.start();
    }

    void endAnimation() {
        if (this.endPending) {
            this.endPending = false;
            this.carouselLayout.carouselAdapter.getView(this.carouselLayout.currentItem - 1, this.carouselLayout.leftView, ViewType.SIDE, this.carouselLayout.imageLytResourceId, this.carouselLayout.sideImageSize);
            this.carouselLayout.carouselAdapter.getView(this.carouselLayout.currentItem + 1, this.carouselLayout.rightView, ViewType.SIDE, this.carouselLayout.imageLytResourceId, this.carouselLayout.sideImageSize);
            this.carouselLayout.carouselAdapter.getView(this.carouselLayout.currentItem, this.carouselLayout.middleRightView, ViewType.MIDDLE_RIGHT, this.carouselLayout.imageLytResourceId, this.carouselLayout.sideImageSize);
            endAnimation(null, this.carouselLayout.currentItem, true);
        }
    }

    boolean isEndPending() {
        return this.endPending;
    }
}
