package com.navdy.hud.app.ui.component.destination;

import com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.DestinationPickerState;

public interface IDestinationPicker {
    void onDestinationPickerClosed();

    boolean onItemClicked(int i, int i2, DestinationPickerState destinationPickerState);

    boolean onItemSelected(int i, int i2, DestinationPickerState destinationPickerState);
}
