package com.navdy.hud.app.ui.component.mainmenu;

import com.navdy.hud.app.ui.component.vlist.VerticalList.ItemSelectionState;
import kotlin.Metadata;

@Metadata(bv = {1, 0, 1}, d1 = {"\u0000\b\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\b\u0002"}, d2 = {"<anonymous>", "", "run"}, k = 3, mv = {1, 1, 6})
/* compiled from: MessagePickerMenu.kt */
final class MessagePickerMenu$selectItem$1 implements Runnable {
    final /* synthetic */ ItemSelectionState $selection;
    final /* synthetic */ MessagePickerMenu this$0;

    @Metadata(bv = {1, 0, 1}, d1 = {"\u0000\b\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\b\u0002"}, d2 = {"<anonymous>", "", "run"}, k = 3, mv = {1, 1, 6})
    /* compiled from: MessagePickerMenu.kt */
    /* renamed from: com.navdy.hud.app.ui.component.mainmenu.MessagePickerMenu$selectItem$1$1 */
    static final class AnonymousClass1 implements Runnable {
        final /* synthetic */ MessagePickerMenu$selectItem$1 this$0;

        AnonymousClass1(MessagePickerMenu$selectItem$1 messagePickerMenu$selectItem$1) {
            this.this$0 = messagePickerMenu$selectItem$1;
        }

        public final void run() {
            this.this$0.this$0.sendMessage((String) this.this$0.this$0.messages.get(this.this$0.$selection.id - MessagePickerMenu.Companion.getBaseMessageId()));
        }
    }

    MessagePickerMenu$selectItem$1(MessagePickerMenu messagePickerMenu, ItemSelectionState itemSelectionState) {
        this.this$0 = messagePickerMenu;
        this.$selection = itemSelectionState;
    }

    public final void run() {
        this.this$0.presenter.close(new AnonymousClass1(this));
    }
}
