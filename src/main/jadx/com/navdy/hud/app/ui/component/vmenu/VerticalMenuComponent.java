package com.navdy.hud.app.ui.component.vmenu;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.animation.AnimatorSet;
import android.animation.AnimatorSet.Builder;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;
import android.os.Handler;
import android.os.SystemClock;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.ViewPropertyAnimator;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.ButterKnife;
import butterknife.InjectView;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.R;
import com.navdy.hud.app.manager.InputManager.CustomKeyEvent;
import com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder;
import com.navdy.hud.app.ui.component.HaloView;
import com.navdy.hud.app.ui.component.UISettings;
import com.navdy.hud.app.ui.component.carousel.CarouselIndicator;
import com.navdy.hud.app.ui.component.image.IconColorImageView;
import com.navdy.hud.app.ui.component.image.IconColorImageView.IconShape;
import com.navdy.hud.app.ui.component.image.InitialsImageView;
import com.navdy.hud.app.ui.component.image.InitialsImageView.Style;
import com.navdy.hud.app.ui.component.vlist.VerticalList;
import com.navdy.hud.app.ui.component.vlist.VerticalList.ContainerCallback;
import com.navdy.hud.app.ui.component.vlist.VerticalList.Direction;
import com.navdy.hud.app.ui.component.vlist.VerticalList.ItemSelectionState;
import com.navdy.hud.app.ui.component.vlist.VerticalList.Model;
import com.navdy.hud.app.ui.component.vlist.VerticalList.ModelState;
import com.navdy.hud.app.ui.component.vlist.VerticalList.ModelType;
import com.navdy.hud.app.ui.component.vlist.VerticalRecyclerView;
import com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder;
import com.navdy.hud.app.ui.framework.DefaultAnimationListener;
import com.navdy.hud.app.view.ToolTipView;
import com.navdy.service.library.events.input.GestureEvent;
import com.navdy.service.library.log.Logger;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class VerticalMenuComponent {
    public static final int ANIMATION_IN_OUT_DURATION = 150;
    public static final int CLICK_ANIMATION_DURATION = 50;
    public static final int CLOSE_ANIMATION_DURATION = 150;
    private static final int FAST_SCROLL_APPEARANCE_ANIM_TIME = 50;
    private static final int FAST_SCROLL_CLOSE_MENU_WAIT_PERIOD = 150;
    private static final int FAST_SCROLL_EXIT_DELAY = 800;
    private static final int FAST_SCROLL_TRIGGER_CLICKS = 10;
    private static final int FAST_SCROLL_TRIGGER_TIME = ((int) TimeUnit.SECONDS.toMillis(2));
    private static final int INDICATOR_FADE_IDLE_DURATION = 2000;
    private static final int INDICATOR_FADE_IN_DURATION = 500;
    private static final int INDICATOR_FADE_OUT_DURATION = 500;
    private static final float MAIN_TO_SELECTED_ICON_SCALE = 1.0f;
    public static final int MIN_FAST_SCROLL_ITEM = 40;
    public static final int MIN_INDEX_ENTRY_COUNT = 2;
    private static final int NO_ANIMATION = -1;
    private static final float SELECTED_TO_MAIN_ICON_SCALE = 1.2f;
    public static final int animateTranslateY;
    private static final int closeContainerHeight;
    private static final int closeContainerScrollY;
    private static final int closeHaloColor;
    private static final int indicatorY;
    private static final int[] location = new int[2];
    private static final Logger sLogger = new Logger("VerticalMenuC");
    private Interpolator accelerateInterpolator = new AccelerateInterpolator();
    private ImageView animImageView;
    private TextView animSubTitle;
    private TextView animSubTitle2;
    private TextView animTitle;
    private Callback callback;
    private int clickCount;
    private long clickTime;
    @InjectView(R.id.closeContainer)
    public ViewGroup closeContainer;
    @InjectView(R.id.closeContainerScrim)
    public View closeContainerScrim;
    @InjectView(R.id.closeHalo)
    public HaloView closeHalo;
    @InjectView(R.id.closeIconContainer)
    public ViewGroup closeIconContainer;
    private Runnable closeIconEndAction = new Runnable() {
        public void run() {
            VerticalMenuComponent.this.callback.close();
        }
    };
    private boolean closeMenuAnimationActive;
    private DefaultAnimationListener closeMenuHideListener = new DefaultAnimationListener() {
        public void onAnimationEnd(Animator animation) {
            VerticalMenuComponent.sLogger.v("closeMenuHideListener");
            VerticalMenuComponent.this.closeMenuAnimationActive = false;
            VerticalMenuComponent.this.closeContainer.setVisibility(8);
            VerticalMenuComponent.this.closeContainerScrim.setVisibility(8);
        }
    };
    private DefaultAnimationListener closeMenuShowListener = new DefaultAnimationListener() {
        public void onAnimationStart(Animator animation) {
            VerticalMenuComponent.sLogger.v("closeMenuShowListener");
            VerticalMenuComponent.this.closeContainer.setVisibility(0);
            VerticalMenuComponent.this.closeContainerScrim.setVisibility(0);
            VerticalMenuComponent.this.closeContainerScrim.setAlpha(0.0f);
        }

        public void onAnimationEnd(Animator animation) {
            VerticalMenuComponent.this.closeMenuAnimationActive = false;
        }
    };
    private ContainerCallback containerCallback = new ContainerCallback() {
        public boolean isCloseMenuVisible() {
            return VerticalMenuComponent.this.isCloseMenuVisible();
        }

        public boolean isFastScrolling() {
            return VerticalMenuComponent.this.isFastScrollVisible();
        }

        public void showToolTips() {
            if (VerticalMenuComponent.this.callback != null) {
                VerticalMenuComponent.this.callback.showToolTip();
            }
        }

        public void hideToolTips() {
            VerticalMenuComponent.this.hideToolTip();
        }
    };
    private Interpolator decelerateInterpolator = new DecelerateInterpolator();
    private Runnable fadeOutRunnable = new Runnable() {
        public void run() {
            VerticalMenuComponent.sLogger.v("fadeOutRunnable");
            VerticalMenuComponent.this.fadeoutIndicator(500);
        }
    };
    private final int fastScrollCloseWaitPeriod;
    @InjectView(R.id.fastScrollView)
    public ViewGroup fastScrollContainer;
    private int fastScrollCurrentItem;
    private DefaultAnimationListener fastScrollIn = new DefaultAnimationListener() {
        public void onAnimationEnd(Animator animation) {
            VerticalMenuComponent.this.handler.removeCallbacks(VerticalMenuComponent.this.fastScrollTimeout);
            VerticalMenuComponent.this.handler.postDelayed(VerticalMenuComponent.this.fastScrollTimeout, 800);
        }
    };
    private VerticalFastScrollIndex fastScrollIndex;
    private int fastScrollOffset;
    private DefaultAnimationListener fastScrollOut = new DefaultAnimationListener() {
        public void onAnimationEnd(Animator animation) {
            VerticalMenuComponent.this.stoppingFastScrolling = false;
            VerticalMenuComponent.this.fastScrollContainer.setVisibility(8);
            VerticalMenuComponent.this.startFadeOut();
        }
    };
    @InjectView(R.id.fastScrollText)
    public TextView fastScrollText;
    private Runnable fastScrollTimeout = new Runnable() {
        public void run() {
            VerticalMenuComponent.this.stopFastScrolling();
        }
    };
    private Handler handler = new Handler();
    @InjectView(R.id.indicator)
    public CarouselIndicator indicator;
    private long lastUpEvent;
    @InjectView(R.id.leftContainer)
    public ViewGroup leftContainer;
    private Interpolator linearInterpolator = new LinearInterpolator();
    private boolean listLoaded;
    private HashSet<CustomKeyEvent> overrideDefaultKeyEvents = new HashSet();
    private ViewGroup parentContainer;
    @InjectView(R.id.recyclerView)
    public VerticalRecyclerView recyclerView;
    @InjectView(R.id.rightContainer)
    public ViewGroup rightContainer;
    private Direction scrollDirection;
    @InjectView(R.id.selectedCustomView)
    public FrameLayout selectedCustomView;
    @InjectView(R.id.selectedIconColorImage)
    public IconColorImageView selectedIconColorImage;
    @InjectView(R.id.selectedIconImage)
    public InitialsImageView selectedIconImage;
    @InjectView(R.id.selectedImage)
    public ViewGroup selectedImage;
    @InjectView(R.id.selectedText)
    public TextView selectedText;
    private boolean stoppingFastScrolling;
    private HashMap<Integer, Bitmap> sublistAnimationCache = new HashMap();
    @InjectView(R.id.tooltip)
    public ToolTipView toolTip;
    public VerticalList verticalList;
    private com.navdy.hud.app.ui.component.vlist.VerticalList.Callback vlistCallback = new com.navdy.hud.app.ui.component.vlist.VerticalList.Callback() {
        public void select(ItemSelectionState selection) {
            VerticalMenuComponent.this.callback.select(selection);
        }

        public void onLoad() {
            VerticalMenuComponent.this.listLoaded = true;
            VerticalMenuComponent.this.callback.onLoad();
        }

        public void onBindToView(Model model, View view, int pos, ModelState state) {
            VerticalMenuComponent.this.callback.onBindToView(model, view, pos, state);
        }

        public void onItemSelected(ItemSelectionState selection) {
            VerticalMenuComponent.this.callback.onItemSelected(selection);
        }

        public void onScrollIdle() {
            VerticalMenuComponent.this.callback.onScrollIdle();
        }
    };

    public interface Callback {
        void close();

        boolean isClosed();

        boolean isItemClickable(ItemSelectionState itemSelectionState);

        void onBindToView(Model model, View view, int i, ModelState modelState);

        void onFastScrollEnd();

        void onFastScrollStart();

        void onItemSelected(ItemSelectionState itemSelectionState);

        void onLoad();

        void onScrollIdle();

        void select(ItemSelectionState itemSelectionState);

        void showToolTip();
    }

    static {
        Resources resources = HudApplication.getAppContext().getResources();
        closeContainerHeight = resources.getDimensionPixelSize(R.dimen.vmenu_close_height);
        closeContainerScrollY = resources.getDimensionPixelSize(R.dimen.vmenu_close_scroll_y);
        indicatorY = resources.getDimensionPixelSize(R.dimen.vmenu_indicator_y);
        animateTranslateY = resources.getDimensionPixelSize(R.dimen.vmenu_anim_translate_y);
        closeHaloColor = resources.getColor(R.color.close_halo);
    }

    public VerticalMenuComponent(ViewGroup root, Callback callback, boolean allowTwoLineTitles) {
        if (root == null || callback == null) {
            throw new IllegalArgumentException();
        }
        this.fastScrollCloseWaitPeriod = UISettings.isVerticalListNoCloseTimeout() ? 0 : 150;
        this.callback = callback;
        ButterKnife.inject( this, (View) root);
        this.parentContainer = root;
        this.closeContainer.setY((float) (-closeContainerHeight));
        this.indicator.setY((float) indicatorY);
        fadeoutIndicator(-1);
        this.verticalList = new VerticalList(this.recyclerView, this.indicator, this.vlistCallback, this.containerCallback, allowTwoLineTitles);
        this.closeHalo.setStrokeColor(closeHaloColor);
        this.toolTip.setParams(VerticalViewHolder.mainIconSize, VerticalViewHolder.iconMargin, 4);
        Context context = root.getContext();
        this.animImageView = new ImageView(context);
        this.animImageView.setVisibility(4);
        this.parentContainer.addView(this.animImageView, new MarginLayoutParams(VerticalViewHolder.selectedIconSize, VerticalViewHolder.selectedIconSize));
        this.animTitle = new TextView(context);
        this.animTitle.setTextAppearance(context, R.style.vlist_title);
        this.animTitle.setVisibility(4);
        this.parentContainer.addView(this.animTitle, new MarginLayoutParams(-2, -2));
        this.animSubTitle = new TextView(context);
        this.animSubTitle.setTextAppearance(context, R.style.vlist_subtitle);
        this.animSubTitle.setVisibility(4);
        this.parentContainer.addView(this.animSubTitle, new MarginLayoutParams(-2, -2));
        this.animSubTitle2 = new TextView(context);
        this.animSubTitle2.setTextAppearance(context, R.style.vlist_subtitle);
        this.animSubTitle2.setVisibility(4);
        this.parentContainer.addView(this.animSubTitle2, new MarginLayoutParams(-2, -2));
    }

    public void setLeftContainerWidth(int width) {
        sLogger.v("leftContainer width=" + width);
        ((MarginLayoutParams) this.leftContainer.getLayoutParams()).width = width;
    }

    public void setRightContainerWidth(int width) {
        sLogger.v("rightContainer width=" + width);
        ((MarginLayoutParams) this.rightContainer.getLayoutParams()).width = width;
    }

    public void updateView(List<Model> list, int initialSelection, boolean firstEntryBlank) {
        updateView(list, initialSelection, firstEntryBlank, null);
    }

    public void updateView(List<Model> list, int initialSelection, boolean firstEntryBlank, VerticalFastScrollIndex scrollIndex) {
        updateView(list, initialSelection, firstEntryBlank, false, scrollIndex);
    }

    public void updateView(List<Model> list, int initialSelection, boolean firstEntryBlank, boolean hasScrollModel, VerticalFastScrollIndex scrollIndex) {
        this.listLoaded = false;
        this.fastScrollIndex = null;
        if (scrollIndex != null) {
            int n = list.size();
            if (n < 40 || scrollIndex.getEntryCount() < 2) {
                sLogger.i("fast scroll threshold not met:" + n + HereManeuverDisplayBuilder.COMMA + scrollIndex.getEntryCount());
            } else {
                this.fastScrollIndex = scrollIndex;
                sLogger.i("fast scroll available:" + n);
            }
        }
        if (hasScrollModel) {
            this.verticalList.updateViewWithScrollableContent(list, initialSelection, firstEntryBlank);
        } else {
            this.verticalList.updateView(list, initialSelection, firstEntryBlank);
        }
    }

    private void showCloseMenu() {
        if (!isCloseMenuVisible()) {
            sLogger.v("showCloseMenu");
            AnimatorSet set = new AnimatorSet();
            set.setDuration(150);
            set.setInterpolator(this.linearInterpolator);
            set.addListener(this.closeMenuShowListener);
            r0 = new Animator[5];
            r0[0] = ObjectAnimator.ofFloat(this.closeContainer, View.Y, new float[]{0.0f});
            r0[1] = ObjectAnimator.ofFloat(this.closeContainerScrim, View.ALPHA, new float[]{1.0f});
            r0[2] = ObjectAnimator.ofFloat(this.leftContainer, View.Y, new float[]{(float) closeContainerScrollY});
            r0[3] = ObjectAnimator.ofFloat(this.rightContainer, View.Y, new float[]{(float) closeContainerScrollY});
            r0[4] = ObjectAnimator.ofFloat(this.indicator, View.Y, new float[]{(float) (indicatorY + closeContainerScrollY)});
            set.playTogether(r0);
            this.closeMenuAnimationActive = true;
            set.start();
            this.verticalList.animate(this.verticalList.getRawPosition(), false, 150, true, true);
            fadeoutIndicator(150);
        }
    }

    private void hideCloseMenu() {
        if (isCloseMenuVisible()) {
            sLogger.v("hideCloseMenu");
            AnimatorSet set = new AnimatorSet();
            set.setDuration(150);
            set.setInterpolator(this.linearInterpolator);
            set.addListener(this.closeMenuHideListener);
            r0 = new Animator[5];
            r0[0] = ObjectAnimator.ofFloat(this.closeContainer, View.Y, new float[]{(float) (-closeContainerHeight)});
            r0[1] = ObjectAnimator.ofFloat(this.closeContainerScrim, View.ALPHA, new float[]{0.0f});
            r0[2] = ObjectAnimator.ofFloat(this.leftContainer, View.Y, new float[]{0.0f});
            r0[3] = ObjectAnimator.ofFloat(this.rightContainer, View.Y, new float[]{0.0f});
            r0[4] = ObjectAnimator.ofFloat(this.indicator, View.Y, new float[]{(float) indicatorY});
            set.playTogether(r0);
            this.closeMenuAnimationActive = true;
            set.start();
            this.verticalList.animate(this.verticalList.getRawPosition(), true, 150, false, true);
        }
    }

    public boolean isCloseMenuVisible() {
        return this.closeContainer.getVisibility() == 0;
    }

    public void animateIn(AnimatorListener listener) {
        sLogger.v("animateIn");
        AnimatorSet set = new AnimatorSet();
        set.setDuration(150);
        set.setInterpolator(this.decelerateInterpolator);
        if (listener != null) {
            set.addListener(listener);
        }
        PropertyValuesHolder p1 = PropertyValuesHolder.ofFloat(View.TRANSLATION_Y, new float[]{(float) animateTranslateY, 0.0f});
        PropertyValuesHolder p2 = PropertyValuesHolder.ofFloat(View.ALPHA, new float[]{1.0f});
        PropertyValuesHolder p3 = PropertyValuesHolder.ofFloat(View.TRANSLATION_Y, new float[]{(float) animateTranslateY, 0.0f});
        PropertyValuesHolder p4 = PropertyValuesHolder.ofFloat(View.ALPHA, new float[]{1.0f});
        Animator[] animatorArr = new Animator[2];
        animatorArr[0] = ObjectAnimator.ofPropertyValuesHolder(this.leftContainer, new PropertyValuesHolder[]{p1, p2});
        animatorArr[1] = ObjectAnimator.ofPropertyValuesHolder(this.rightContainer, new PropertyValuesHolder[]{p3, p4});
        set.playTogether(animatorArr);
        set.start();
    }

    public void animateOut(AnimatorListener listener) {
        sLogger.v("animateOut");
        this.indicator.setVisibility(4);
        AnimatorSet set = new AnimatorSet();
        set.setDuration(150);
        set.setInterpolator(this.accelerateInterpolator);
        if (listener != null) {
            set.addListener(listener);
        }
        PropertyValuesHolder p1 = PropertyValuesHolder.ofFloat(View.Y, new float[]{this.leftContainer.getY() + ((float) animateTranslateY)});
        PropertyValuesHolder p2 = PropertyValuesHolder.ofFloat(View.ALPHA, new float[]{0.0f});
        PropertyValuesHolder p3 = PropertyValuesHolder.ofFloat(View.Y, new float[]{this.rightContainer.getY() + ((float) animateTranslateY)});
        PropertyValuesHolder p4 = PropertyValuesHolder.ofFloat(View.ALPHA, new float[]{0.0f});
        PropertyValuesHolder p5 = PropertyValuesHolder.ofFloat(View.TRANSLATION_Y, new float[]{(float) animateTranslateY});
        PropertyValuesHolder p6 = PropertyValuesHolder.ofFloat(View.ALPHA, new float[]{0.0f});
        r7 = new Animator[3];
        r7[0] = ObjectAnimator.ofPropertyValuesHolder(this.leftContainer, new PropertyValuesHolder[]{p1, p2});
        r7[1] = ObjectAnimator.ofPropertyValuesHolder(this.rightContainer, new PropertyValuesHolder[]{p3, p4});
        r7[2] = ObjectAnimator.ofPropertyValuesHolder(this.closeContainer, new PropertyValuesHolder[]{p5, p6});
        set.playTogether(r7);
        set.start();
    }

    public boolean handleGesture(GestureEvent event) {
        if (!this.listLoaded) {
            sLogger.v("list not loaded yet");
            return false;
        } else if (this.closeMenuAnimationActive) {
            sLogger.v("close menu animation is active, no-op");
            return false;
        } else {
            switch (event.gesture) {
                case GESTURE_SWIPE_RIGHT:
                    this.callback.close();
                    return true;
                default:
                    return false;
            }
        }
    }

    public boolean handleKey(CustomKeyEvent event) {
        if (!this.listLoaded) {
            sLogger.v("list not loaded yet");
            return false;
        } else if (this.closeMenuAnimationActive) {
            sLogger.v("close menu animation is active, no-op");
            return false;
        } else {
            long now = SystemClock.elapsedRealtime();
            switch (event) {
                case LEFT:
                    if (this.verticalList.getCurrentPosition() == 0) {
                        if (this.verticalList.up().keyHandled) {
                            this.lastUpEvent = now;
                            return true;
                        } else if (SystemClock.elapsedRealtime() - this.lastUpEvent < ((long) this.fastScrollCloseWaitPeriod)) {
                            sLogger.v("user scrolled fast");
                            this.lastUpEvent = now;
                            return true;
                        } else {
                            this.lastUpEvent = now;
                            showCloseMenu();
                        }
                    } else if (isFastScrollAvailable() && checkFastScroll(Direction.UP, -1, now)) {
                        return true;
                    } else {
                        this.lastUpEvent = now;
                        if (this.verticalList.up().listMoved) {
                            fadeinIndicator(500);
                            startFadeOut();
                        }
                    }
                    return true;
                case RIGHT:
                    if (isCloseMenuVisible()) {
                        hideCloseMenu();
                    } else if (isFastScrollAvailable() && checkFastScroll(Direction.DOWN, 1, now)) {
                        return true;
                    } else {
                        hideToolTip();
                        if (this.verticalList.down().listMoved) {
                            fadeinIndicator(500);
                            startFadeOut();
                        }
                    }
                    return true;
                case SELECT:
                    if (!isCloseMenuVisible() || this.callback.isClosed()) {
                        int pos = this.verticalList.getCurrentPosition();
                        Model current = this.verticalList.getCurrentModel();
                        if (current == null) {
                            return false;
                        }
                        switch (current.id) {
                            case R.id.vlist_content_loading:
                            case R.id.vlist_loading:
                                return false;
                            default:
                                if (isFastScrollAvailable() && isFastScrollVisible()) {
                                    selectFastScroll();
                                    return true;
                                }
                                ItemSelectionState itemSelectionState = this.verticalList.getItemSelectionState();
                                itemSelectionState.set(current, current.id, pos, -1, -1);
                                if (this.callback.isItemClickable(itemSelectionState)) {
                                    this.verticalList.select();
                                    break;
                                }
                                return false;
                        }
                    }
                    this.closeHalo.setVisibility(4);
                    VerticalAnimationUtils.performClick(this.closeIconContainer, 50, this.closeIconEndAction);
                    return true;
                default:
                    if (this.overrideDefaultKeyEvents.contains(event)) {
                        return true;
                    }
                    return false;
            }
        }
    }

    public void fadeinIndicator(int duration) {
        sLogger.v("fadeinIndicator:" + duration);
        if (duration == -1) {
            this.indicator.setAlpha(1.0f);
        } else if (this.indicator.getAlpha() != 1.0f) {
            this.indicator.animate().alpha(1.0f).setDuration((long) duration).start();
        }
    }

    public void fadeoutIndicator(int duration) {
        if (duration == -1) {
            this.indicator.setAlpha(0.0f);
        } else if (this.indicator.getAlpha() != 0.0f) {
            this.indicator.animate().alpha(0.0f).setDuration((long) duration).start();
        }
    }

    private void startFadeOut() {
        this.handler.removeCallbacks(this.fadeOutRunnable);
        this.handler.postDelayed(this.fadeOutRunnable, 2000);
    }

    public void setSelectedIconColorImage(int resourceId, int bkColor, Shader gradient, float scaleF) {
        setSelectedIconColorImage(resourceId, bkColor, gradient, scaleF, IconShape.CIRCLE);
    }

    public void setSelectedIconColorImage(int resourceId, int bkColor, Shader gradient, float scaleF, IconShape iconShape) {
        this.selectedIconColorImage.setIcon(resourceId, bkColor, gradient, scaleF);
        this.selectedIconColorImage.setVisibility(0);
        this.selectedIconColorImage.setIconShape(iconShape);
        this.selectedIconImage.setVisibility(8);
        this.selectedCustomView.setVisibility(8);
    }

    public void setSelectedIconImage(int resourceId, String initials, Style style) {
        this.selectedIconImage.setImage(resourceId, initials, style);
        this.selectedIconImage.setVisibility(0);
        this.selectedIconColorImage.setVisibility(8);
        this.selectedCustomView.setVisibility(8);
    }

    public void setSelectedIconImage(Bitmap bitmap) {
        this.selectedIconImage.setInitials(null, Style.DEFAULT);
        this.selectedIconImage.setImageBitmap(bitmap);
        this.selectedIconImage.setVisibility(0);
        this.selectedIconColorImage.setVisibility(8);
        this.selectedCustomView.setVisibility(8);
    }

    public void showSelectedCustomView() {
        this.selectedCustomView.setVisibility(0);
        this.selectedIconImage.setVisibility(8);
        this.selectedIconColorImage.setVisibility(8);
    }

    public ViewGroup getSelectedCustomImage() {
        return this.selectedCustomView;
    }

    public void performSelectionAnimation(Runnable endAction) {
        performSelectionAnimation(endAction, 0);
    }

    public void performSelectionAnimation(final Runnable endAction, final int startDelay) {
        sLogger.v("performSelectionAnimation");
        AnimatorSet set = new AnimatorSet();
        Builder builder = set.play(ObjectAnimator.ofFloat(this.leftContainer, View.ALPHA, new float[]{0.0f}));
        builder.with(ObjectAnimator.ofFloat(this.indicator, View.ALPHA, new float[]{0.0f}));
        this.verticalList.addCurrentHighlightAnimation(builder);
        set.setDuration(50);
        set.addListener(new DefaultAnimationListener() {
            public void onAnimationEnd(Animator animation) {
                ViewPropertyAnimator vp = VerticalMenuComponent.this.rightContainer.animate().alpha(0.0f).setDuration(150).withEndAction(endAction);
                if (startDelay > 0) {
                    vp.setStartDelay((long) startDelay);
                }
                vp.start();
            }
        });
        set.start();
    }

    public void performBackAnimation(Runnable startAction, Runnable endAction, Model model, int xOffset) {
        ImageView from;
        sLogger.v("performBackAnimation");
        float adjustment = (((float) VerticalViewHolder.selectedIconSize) * 0.20000005f) / 2.0f;
        this.animImageView.setScaleX(SELECTED_TO_MAIN_ICON_SCALE);
        this.animImageView.setScaleY(SELECTED_TO_MAIN_ICON_SCALE);
        this.animImageView.setX(this.selectedImage.getX() + adjustment);
        this.animImageView.setY(this.selectedImage.getY() + adjustment);
        if (this.selectedIconColorImage.getVisibility() == 0) {
            from = this.selectedIconColorImage;
        } else {
            from = this.selectedIconImage;
        }
        Bitmap cacheBitmap = getFromCache(model);
        if (cacheBitmap == null) {
            VerticalAnimationUtils.copyImage(from, this.animImageView);
            addToCache(this.animImageView, model);
        } else {
            this.animImageView.setImageBitmap(cacheBitmap);
        }
        this.animImageView.setVisibility(0);
        this.selectedImage.setAlpha(0.0f);
        PropertyValuesHolder p1 = PropertyValuesHolder.ofFloat(View.SCALE_X, new float[]{1.0f});
        PropertyValuesHolder p2 = PropertyValuesHolder.ofFloat(View.SCALE_Y, new float[]{1.0f});
        PropertyValuesHolder p3 = PropertyValuesHolder.ofFloat(View.X, new float[]{(float) (VerticalViewHolder.selectedImageX + xOffset)});
        PropertyValuesHolder p4 = PropertyValuesHolder.ofFloat(View.Y, new float[]{(float) VerticalViewHolder.selectedImageY});
        ObjectAnimator animatorImage = ObjectAnimator.ofPropertyValuesHolder(this.animImageView, new PropertyValuesHolder[]{p1, p2, p3, p4});
        animatorImage.setDuration((long) 150);
        final Runnable runnable = endAction;
        animatorImage.addListener(new DefaultAnimationListener() {
            public void onAnimationEnd(Animator animation) {
                AnimatorSet set = new AnimatorSet();
                Builder builder = set.play(ObjectAnimator.ofFloat(VerticalMenuComponent.this.rightContainer, View.ALPHA, new float[]{1.0f}));
                builder.with(ObjectAnimator.ofFloat(VerticalMenuComponent.this.selectedImage, View.ALPHA, new float[]{1.0f}));
                builder.with(ObjectAnimator.ofFloat(VerticalMenuComponent.this.selectedText, View.ALPHA, new float[]{1.0f}));
                set.setDuration(100);
                set.addListener(new DefaultAnimationListener() {
                    public void onAnimationEnd(Animator animation) {
                        VerticalMenuComponent.this.animImageView.setVisibility(4);
                        VerticalMenuComponent.this.animTitle.setVisibility(4);
                        VerticalMenuComponent.this.animSubTitle.setVisibility(4);
                        VerticalMenuComponent.this.animSubTitle2.setVisibility(4);
                        if (runnable != null) {
                            runnable.run();
                        }
                    }
                });
                set.start();
            }
        });
        animatorImage.start();
        this.animTitle.setX((float) (VerticalViewHolder.selectedTextX - animateTranslateY));
        if (model.fontInfo == null) {
            VerticalList.setFontSize(model, this.verticalList.allowsTwoLineTitles());
        }
        this.animTitle.setTextSize(model.fontInfo.titleFontSize);
        this.animTitle.setAlpha(0.0f);
        this.animTitle.setVisibility(0);
        boolean hasSubtitle = false;
        if (model == null || model.subTitle == null) {
            this.animSubTitle.setText("");
        } else {
            this.animSubTitle.setX((float) (VerticalViewHolder.selectedTextX - animateTranslateY));
            this.animSubTitle.setText(model.subTitle);
            this.animSubTitle.setTextSize(model.fontInfo.subTitleFontSize);
            this.animSubTitle.setAlpha(0.0f);
            this.animSubTitle.setVisibility(0);
            hasSubtitle = true;
        }
        boolean hasSubtitle2 = false;
        if (model == null || model.subTitle2 == null) {
            this.animSubTitle2.setText("");
        } else {
            this.animSubTitle2.setX((float) (VerticalViewHolder.selectedTextX - animateTranslateY));
            this.animSubTitle2.setText(model.subTitle2);
            this.animSubTitle2.setTextSize(model.fontInfo.subTitle2FontSize);
            this.animSubTitle2.setAlpha(0.0f);
            this.animSubTitle2.setVisibility(0);
            hasSubtitle2 = true;
        }
        AnimatorSet textSet = new AnimatorSet();
        p1 = PropertyValuesHolder.ofFloat(View.ALPHA, new float[]{1.0f});
        p2 = PropertyValuesHolder.ofFloat(View.X, new float[]{this.animTitle.getX() + ((float) animateTranslateY)});
        Builder textBuilder = textSet.play(ObjectAnimator.ofPropertyValuesHolder(this.animTitle, new PropertyValuesHolder[]{p1, p2}));
        if (hasSubtitle) {
            p1 = PropertyValuesHolder.ofFloat(View.ALPHA, new float[]{1.0f});
            p2 = PropertyValuesHolder.ofFloat(View.X, new float[]{this.animSubTitle.getX() + ((float) animateTranslateY)});
            textBuilder.with(ObjectAnimator.ofPropertyValuesHolder(this.animSubTitle, new PropertyValuesHolder[]{p1, p2}));
        }
        if (hasSubtitle2) {
            p1 = PropertyValuesHolder.ofFloat(View.ALPHA, new float[]{1.0f});
            p2 = PropertyValuesHolder.ofFloat(View.X, new float[]{this.animSubTitle2.getX() + ((float) animateTranslateY)});
            textBuilder.with(ObjectAnimator.ofPropertyValuesHolder(this.animSubTitle2, new PropertyValuesHolder[]{p1, p2}));
        }
        textSet.setStartDelay((long) ((int) (0.6666667f * ((float) 150))));
        textSet.setDuration((long) 50);
        textSet.start();
        ObjectAnimator mainTitle = ObjectAnimator.ofFloat(this.selectedText, View.ALPHA, new float[]{0.0f});
        mainTitle.setDuration((long) 50);
        mainTitle.start();
        this.rightContainer.setPivotX(0.0f);
        this.rightContainer.setPivotY((float) (this.recyclerView.getMeasuredHeight() / 2));
        AnimatorSet rightAnimator = new AnimatorSet();
        p1 = PropertyValuesHolder.ofFloat(View.SCALE_X, new float[]{0.5f});
        p2 = PropertyValuesHolder.ofFloat(View.SCALE_Y, new float[]{0.5f});
        p3 = PropertyValuesHolder.ofFloat(View.ALPHA, new float[]{0.0f});
        rightAnimator.setDuration((long) ((int) (0.6666667f * ((float) 150))));
        rightAnimator.play(ObjectAnimator.ofPropertyValuesHolder(this.rightContainer, new PropertyValuesHolder[]{p1, p2, p3}));
        runnable = startAction;
        rightAnimator.addListener(new DefaultAnimationListener() {
            public void onAnimationEnd(Animator animation) {
                VerticalMenuComponent.this.rightContainer.setAlpha(0.0f);
                VerticalMenuComponent.this.rightContainer.setScaleX(1.0f);
                VerticalMenuComponent.this.rightContainer.setScaleY(1.0f);
                if (runnable != null) {
                    runnable.run();
                }
            }
        });
        rightAnimator.start();
    }

    public void performEnterAnimation(Runnable startAction, Runnable endAction, Model model) {
        sLogger.v("performEnterAnimation");
        this.selectedImage.getLocationOnScreen(location);
        this.selectedImage.setAlpha(0.0f);
        this.selectedText.setAlpha(0.0f);
        this.indicator.setAlpha(0.0f);
        this.animImageView.setScaleX(1.0f);
        this.animImageView.setScaleY(1.0f);
        this.animImageView.setImageBitmap(null);
        Bitmap cacheBitmap = null;
        if (model.type != ModelType.ICON_OPTIONS) {
            cacheBitmap = getFromCache(model);
        }
        this.verticalList.copyAndPosition(this.animImageView, this.animTitle, this.animSubTitle, this.animSubTitle2, cacheBitmap == null);
        if (cacheBitmap != null) {
            this.animImageView.setImageBitmap(cacheBitmap);
        } else {
            addToCache(this.animImageView, model);
        }
        this.animImageView.setVisibility(0);
        this.animTitle.setAlpha(1.0f);
        this.animTitle.setVisibility(0);
        if (this.animSubTitle.getText().length() > 0) {
            this.animSubTitle.setAlpha(1.0f);
            this.animSubTitle.setVisibility(0);
        }
        this.rightContainer.setAlpha(0.0f);
        PropertyValuesHolder p1 = PropertyValuesHolder.ofFloat(View.SCALE_X, new float[]{SELECTED_TO_MAIN_ICON_SCALE});
        PropertyValuesHolder p2 = PropertyValuesHolder.ofFloat(View.SCALE_Y, new float[]{SELECTED_TO_MAIN_ICON_SCALE});
        float adjustment = (((float) VerticalViewHolder.selectedIconSize) * -0.20000005f) / 2.0f;
        PropertyValuesHolder p3 = PropertyValuesHolder.ofFloat(View.X, new float[]{((float) location[0]) - adjustment});
        PropertyValuesHolder p4 = PropertyValuesHolder.ofFloat(View.Y, new float[]{((float) (location[1] - VerticalViewHolder.rootTopOffset)) - adjustment});
        ObjectAnimator animatorImage = ObjectAnimator.ofPropertyValuesHolder(this.animImageView, new PropertyValuesHolder[]{p1, p2, p3, p4});
        animatorImage.setDuration((long) 150);
        final Runnable runnable = startAction;
        final Runnable runnable2 = endAction;
        animatorImage.addListener(new DefaultAnimationListener() {
            public void onAnimationStart(Animator animation) {
                if (runnable != null) {
                    runnable.run();
                }
                VerticalMenuComponent.this.rightContainer.setPivotX(0.0f);
                VerticalMenuComponent.this.rightContainer.setPivotY((float) (VerticalMenuComponent.this.recyclerView.getMeasuredHeight() / 2));
                VerticalMenuComponent.this.rightContainer.setScaleX(0.5f);
                VerticalMenuComponent.this.rightContainer.setScaleY(0.5f);
            }

            public void onAnimationEnd(Animator animation) {
                VerticalMenuComponent.this.selectedImage.setAlpha(1.0f);
                VerticalMenuComponent.this.selectedText.setAlpha(1.0f);
                VerticalMenuComponent.this.animImageView.setVisibility(4);
                VerticalMenuComponent.this.animTitle.setVisibility(4);
                VerticalMenuComponent.this.animSubTitle.setVisibility(4);
                if (runnable2 != null) {
                    runnable2.run();
                }
            }
        });
        animatorImage.start();
        this.animTitle.getLocationOnScreen(location);
        AnimatorSet textSet = new AnimatorSet();
        p1 = PropertyValuesHolder.ofFloat(View.ALPHA, new float[]{0.0f});
        p2 = PropertyValuesHolder.ofFloat(View.X, new float[]{this.animTitle.getX() - ((float) animateTranslateY)});
        Builder textBuilder = textSet.play(ObjectAnimator.ofPropertyValuesHolder(this.animTitle, new PropertyValuesHolder[]{p1, p2}));
        if (this.animSubTitle.getText().length() > 0) {
            p1 = PropertyValuesHolder.ofFloat(View.ALPHA, new float[]{0.0f});
            p2 = PropertyValuesHolder.ofFloat(View.X, new float[]{this.animSubTitle.getX() - ((float) animateTranslateY)});
            textBuilder.with(ObjectAnimator.ofPropertyValuesHolder(this.animSubTitle, new PropertyValuesHolder[]{p1, p2}));
        }
        textSet.setDuration((long) 50);
        textSet.start();
        ObjectAnimator mainTitle = ObjectAnimator.ofFloat(this.selectedText, View.ALPHA, new float[]{1.0f});
        mainTitle.setDuration((long) 50);
        mainTitle.setStartDelay((long) ((int) (0.6666667f * ((float) 150))));
        mainTitle.start();
        AnimatorSet rightAnimator = new AnimatorSet();
        p1 = PropertyValuesHolder.ofFloat(View.SCALE_X, new float[]{1.0f});
        p2 = PropertyValuesHolder.ofFloat(View.SCALE_Y, new float[]{1.0f});
        p3 = PropertyValuesHolder.ofFloat(View.ALPHA, new float[]{1.0f});
        rightAnimator.setStartDelay((long) 50);
        rightAnimator.setDuration((long) ((int) (0.6666667f * ((float) 150))));
        rightAnimator.play(ObjectAnimator.ofPropertyValuesHolder(this.rightContainer, new PropertyValuesHolder[]{p1, p2, p3}));
        rightAnimator.start();
    }

    public void unlock() {
        this.verticalList.unlock();
    }

    public void unlock(boolean startFluctuator) {
        this.verticalList.unlock(startFluctuator);
    }

    private void addToCache(ImageView imageView, Model model) {
        if (model != null) {
            BitmapDrawable drawable = (BitmapDrawable) imageView.getDrawable();
            if (drawable != null) {
                Bitmap bitmap = drawable.getBitmap();
                if (bitmap != null) {
                    this.sublistAnimationCache.put(Integer.valueOf(System.identityHashCode(model)), bitmap);
                }
            }
        }
    }

    private Bitmap getFromCache(Model model) {
        if (model == null) {
            return null;
        }
        return (Bitmap) this.sublistAnimationCache.get(Integer.valueOf(System.identityHashCode(model)));
    }

    public void clear() {
        this.sublistAnimationCache.clear();
        this.verticalList.unlock(false);
        this.verticalList.clearAllAnimations();
    }

    public void setOverrideDefaultKeyEvents(CustomKeyEvent event, boolean override) {
        if (override) {
            this.overrideDefaultKeyEvents.add(event);
        } else {
            this.overrideDefaultKeyEvents.remove(event);
        }
    }

    private boolean isFastScrollAvailable() {
        return this.fastScrollIndex != null;
    }

    private boolean isFastScrollVisible() {
        return this.fastScrollContainer.getVisibility() == 0;
    }

    private void startFastScrolling() {
        if (isFastScrollAvailable()) {
            this.verticalList.lock();
            String title = this.fastScrollIndex.getTitle(this.fastScrollCurrentItem);
            sLogger.v("startFastScrolling:" + title);
            this.fastScrollContainer.setAlpha(0.0f);
            this.fastScrollContainer.setVisibility(0);
            this.fastScrollText.setText(title);
            this.stoppingFastScrolling = false;
            this.callback.onFastScrollStart();
            this.fastScrollContainer.animate().alpha(1.0f).setDuration(50).setListener(this.fastScrollIn).start();
        }
    }

    private void stopFastScrolling() {
        if (isFastScrollAvailable()) {
            String title = String.valueOf(this.fastScrollText.getText());
            int pos = this.fastScrollIndex.getPosition(title);
            if (this.verticalList.isFirstEntryBlank()) {
                pos++;
            }
            if (this.fastScrollIndex != null) {
                pos += this.fastScrollIndex.getOffset();
            }
            sLogger.v("stopFastScrolling:" + pos + " , " + title);
            this.callback.onFastScrollEnd();
            this.stoppingFastScrolling = true;
            this.verticalList.unlock();
            this.verticalList.scrollToPosition(pos);
            this.fastScrollContainer.animate().alpha(0.0f).setDuration(50).setListener(this.fastScrollOut).start();
        }
    }

    private void changeScrollIndex(int n) {
        this.handler.removeCallbacks(this.fastScrollTimeout);
        int newIndex = this.fastScrollCurrentItem + n;
        if (newIndex < 0 || newIndex >= this.fastScrollIndex.length) {
            int pos = this.fastScrollIndex.getPosition(this.fastScrollIndex.getTitle(this.fastScrollCurrentItem));
            if (pos != this.indicator.getCurrentItem()) {
                this.indicator.setCurrentItem(pos);
            }
        } else {
            String title = this.fastScrollIndex.getTitle(newIndex);
            this.fastScrollText.setText(title);
            this.fastScrollCurrentItem = newIndex;
            this.indicator.setCurrentItem(this.fastScrollIndex.getPosition(title));
        }
        this.handler.postDelayed(this.fastScrollTimeout, 800);
    }

    public VerticalFastScrollIndex getFastScrollIndex() {
        return this.fastScrollIndex;
    }

    private boolean checkFastScroll(Direction direction, int changeIndex, long now) {
        if (this.stoppingFastScrolling) {
            return true;
        }
        if (isFastScrollVisible()) {
            changeScrollIndex(changeIndex);
            return true;
        }
        if (this.scrollDirection != direction) {
            this.scrollDirection = direction;
            this.clickCount = 0;
            this.clickTime = now;
        } else if (now - this.clickTime > ((long) FAST_SCROLL_TRIGGER_TIME)) {
            this.clickTime = now;
            this.clickCount = 1;
        } else {
            this.clickCount++;
            if (this.clickCount >= 10) {
                this.fastScrollCurrentItem = this.fastScrollIndex.getIndexForPosition(this.verticalList.getCurrentPosition());
                if (this.scrollDirection == Direction.DOWN && this.fastScrollCurrentItem == this.fastScrollIndex.getEntryCount() - 1) {
                    sLogger.v("checkFastScroll: not allowed,last element");
                    this.clickCount = 0;
                    this.clickTime = now;
                    return false;
                }
                startFastScrolling();
                fadeinIndicator(-1);
                this.handler.removeCallbacks(this.fadeOutRunnable);
                return true;
            }
        }
        return false;
    }

    private void selectFastScroll() {
        sLogger.v("selectFastScroll");
        this.handler.removeCallbacks(this.fastScrollTimeout);
        this.fastScrollTimeout.run();
    }

    public void showToolTip(int posIndex, String text) {
        this.toolTip.show(posIndex, text);
    }

    public void hideToolTip() {
        this.toolTip.hide();
    }
}
