package com.navdy.hud.app.ui.component.destination;

import com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.Presenter;
import dagger.MembersInjector;
import dagger.internal.Binding;
import dagger.internal.Linker;
import java.util.Set;

public final class DestinationPickerView$$InjectAdapter extends Binding<DestinationPickerView> implements MembersInjector<DestinationPickerView> {
    private Binding<Presenter> presenter;

    public DestinationPickerView$$InjectAdapter() {
        super(null, "members/com.navdy.hud.app.ui.component.destination.DestinationPickerView", false, DestinationPickerView.class);
    }

    public void attach(Linker linker) {
        this.presenter = linker.requestBinding("com.navdy.hud.app.ui.component.destination.DestinationPickerScreen$Presenter", DestinationPickerView.class, getClass().getClassLoader());
    }

    public void getDependencies(Set<Binding<?>> set, Set<Binding<?>> injectMembersBindings) {
        injectMembersBindings.add(this.presenter);
    }

    public void injectMembers(DestinationPickerView object) {
        object.presenter = (Presenter) this.presenter.get();
    }
}
