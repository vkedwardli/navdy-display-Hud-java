package com.navdy.hud.app.ui.component.vlist.viewholder;

import android.os.Handler;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ImageView;
import android.widget.TextView;
import com.navdy.hud.app.R;
import com.navdy.hud.app.ui.component.vlist.VerticalList;
import com.navdy.hud.app.ui.component.vlist.VerticalList.Model;
import com.navdy.hud.app.ui.component.vlist.VerticalList.ModelState;
import com.navdy.hud.app.ui.component.vlist.VerticalList.ModelType;
import com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.AnimationType;
import com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.State;
import com.navdy.service.library.log.Logger;

public class ScrollableViewHolder extends VerticalViewHolder {
    private static final Logger sLogger = new Logger(ScrollableViewHolder.class);

    public static Model buildModel(int layoutId) {
        Model model = new Model();
        model.type = ModelType.SCROLL_CONTENT;
        model.id = R.id.vlist_scroll_item;
        model.scrollItemLayoutId = layoutId;
        return model;
    }

    public static ScrollableViewHolder buildViewHolder(ViewGroup parent, VerticalList vlist, Handler handler) {
        return new ScrollableViewHolder((ViewGroup) LayoutInflater.from(parent.getContext()).inflate(R.layout.vlist_scroll_item, parent, false), vlist, handler);
    }

    public ScrollableViewHolder(ViewGroup layout, VerticalList vlist, Handler handler) {
        super(layout, vlist, handler);
    }

    public ModelType getModelType() {
        return ModelType.SCROLL_CONTENT;
    }

    public void setItemState(State state, AnimationType animation, int duration, boolean startFluctuator) {
    }

    public void preBind(Model model, ModelState modelState) {
        if (this.layout.getChildCount() == 0) {
            this.layout.addView((ViewGroup) LayoutInflater.from(this.layout.getContext()).inflate(model.scrollItemLayoutId, this.layout, false), new LayoutParams(-1, -2));
        }
    }

    public void bind(Model model, ModelState modelState) {
    }

    public void clearAnimation() {
    }

    public void select(Model model, int pos, int duration) {
    }

    public void copyAndPosition(ImageView imageC, TextView titleC, TextView subTitleC, TextView subTitle2C, boolean setImage) {
    }
}
