package com.navdy.hud.app.ui.component.homescreen;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.ButterKnife;
import butterknife.InjectView;
import com.navdy.hud.app.util.os.SystemProperties;
import com.navdy.hud.app.view.MainView.CustomAnimationMode;
import com.navdy.service.library.log.Logger;

public class TbtShrunkDistanceView extends RelativeLayout {
    private static final String TBT_SHRUNK_MODE = "persist.sys.tbtdistanceshrunk";
    private static final String TBT_SHRUNK_MODE_DISTANCE_NUMBER = "distance_number";
    private static final String TBT_SHRUNK_MODE_PROGRESS_BAR = "progress_bar";
    private Logger logger;
    private final Mode mode;
    @InjectView(R.id.now_shrunk_icon)
    View nowShrunkIcon;
    @InjectView(R.id.progress_bar_shrunk)
    ProgressBar progressBarShrunk;
    @InjectView(R.id.tbtShrunkDistance)
    TextView tbtShrunkDistance;

    private enum Mode {
        PROGRESS_BAR,
        DISTANCE_NUMBER
    }

    public TbtShrunkDistanceView(Context context) {
        this(context, null);
    }

    public TbtShrunkDistanceView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public TbtShrunkDistanceView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        if (SystemProperties.get(TBT_SHRUNK_MODE, TBT_SHRUNK_MODE_PROGRESS_BAR).equals(TBT_SHRUNK_MODE_DISTANCE_NUMBER)) {
            this.mode = Mode.DISTANCE_NUMBER;
        } else {
            this.mode = Mode.PROGRESS_BAR;
        }
    }

    protected void onFinishInflate() {
        this.logger = HomeScreenView.sLogger;
        super.onFinishInflate();
        ButterKnife.inject((View) this);
        setY((float) HomeScreenResourceValues.activeRoadShrunkDistanceY);
        switch (this.mode) {
            case DISTANCE_NUMBER:
                this.progressBarShrunk.setVisibility(8);
                return;
            default:
                this.tbtShrunkDistance.setVisibility(8);
                return;
        }
    }

    public void setView(CustomAnimationMode mode) {
        switch (mode) {
            case EXPAND:
                setX((float) HomeScreenResourceValues.activeRoadShrunkDistanceX);
                return;
            case SHRINK_LEFT:
                setX((float) HomeScreenResourceValues.activeRoadShrunkDistanceShrinkLeftX);
                return;
            default:
                return;
        }
    }

    public void setDistanceText(String text) {
        this.tbtShrunkDistance.setText(text);
    }

    public void showNowIcon() {
        HomeScreenUtils.getFadeInAndScaleUpAnimator(this.nowShrunkIcon).setDuration(250).start();
        if (this.mode == Mode.DISTANCE_NUMBER) {
            this.tbtShrunkDistance.setVisibility(4);
        }
    }

    public void hideNowIcon() {
        HomeScreenUtils.getFadeOutAndScaleDownAnimator(this.nowShrunkIcon).setDuration(250).start();
        if (this.mode == Mode.DISTANCE_NUMBER) {
            this.tbtShrunkDistance.setVisibility(0);
        }
    }

    public void initProgressBar() {
        if (this.mode == Mode.PROGRESS_BAR) {
            setRegularBackground();
            HomeScreenUtils.setWidth(this.progressBarShrunk, HomeScreenResourceValues.activeRoadShrunkProgressBarWidth);
            animateProgressBar(0.0d);
        }
    }

    public void hideProgressBar() {
        if (this.mode == Mode.PROGRESS_BAR) {
            HomeScreenUtils.setWidth(this.progressBarShrunk, 0);
            setTransparentBackground();
        }
    }

    public void animateProgressBar(double progress) {
        if (this.mode == Mode.PROGRESS_BAR) {
            HomeScreenUtils.getProgressBarAnimator(this.progressBarShrunk, (int) (100.0d * progress)).setDuration(250).start();
        }
    }

    private void setRegularBackground() {
        setBackgroundResource(17170444);
    }

    private void setTransparentBackground() {
        setBackgroundResource(17170445);
    }
}
