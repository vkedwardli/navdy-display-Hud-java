package com.navdy.hud.app.ui.component.mainmenu;

import android.content.res.Resources;
import android.view.View;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.R;
import com.navdy.hud.app.analytics.AnalyticsSupport;
import com.navdy.hud.app.debug.DriveRecorder;
import com.navdy.hud.app.debug.DriveRecorder.Action;
import com.navdy.hud.app.device.gps.GpsUtils;
import com.navdy.hud.app.framework.DriverProfileHelper;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.maps.here.HereMapsManager;
import com.navdy.hud.app.profile.DriverProfileManager;
import com.navdy.hud.app.ui.component.homescreen.SmartDashView;
import com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu.GaugeType;
import com.navdy.hud.app.ui.component.mainmenu.IMenu.Menu;
import com.navdy.hud.app.ui.component.mainmenu.IMenu.MenuLevel;
import com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.Presenter;
import com.navdy.hud.app.ui.component.vlist.VerticalList.ItemSelectionState;
import com.navdy.hud.app.ui.component.vlist.VerticalList.Model;
import com.navdy.hud.app.ui.component.vlist.VerticalList.ModelState;
import com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder;
import com.navdy.hud.app.ui.component.vlist.viewholder.SwitchViewHolder;
import com.navdy.hud.app.ui.component.vmenu.VerticalFastScrollIndex;
import com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent;
import com.navdy.hud.app.ui.framework.UIStateManager;
import com.navdy.hud.app.util.DeviceUtil;
import com.navdy.service.library.events.preferences.LocalPreferences;
import com.navdy.service.library.events.preferences.LocalPreferences.Builder;
import com.navdy.service.library.log.Logger;
import com.squareup.otto.Bus;
import java.util.ArrayList;
import java.util.List;

public class MainOptionsMenu implements IMenu {
    private static final Model autoZoom;
    private static final Model back;
    private static final int dashColor = resources.getColor(R.color.mm_dash_options);
    private static final String dashTitle = resources.getString(R.string.carousel_menu_smartdash_options);
    private static final Model manualZoom;
    private static final int mapColor = resources.getColor(R.color.mm_map_options);
    private static final String mapTitle = resources.getString(R.string.carousel_menu_map_options);
    private static final Model pauseDemo;
    private static final Model playDemo;
    private static final Model rawGps;
    private static final Resources resources = HudApplication.getAppContext().getResources();
    private static final Model restartDemo;
    private static final Logger sLogger = new Logger(MainOptionsMenu.class);
    private static final Model scrollLeftGauge;
    private static final Model scrollRightGauge;
    private static final Model selectCenterGauge;
    private static final Model sideGauges;
    private int backSelection;
    private Bus bus;
    private List<Model> cachedList;
    private DashGaugeConfiguratorMenu dashGaugeConfiguratorMenu;
    private DriveRecorder driveRecorder;
    private DriverProfileManager driverProfileManager = DriverProfileHelper.getInstance().getDriverProfileManager();
    private Mode mode;
    private IMenu parent;
    private Presenter presenter;
    private UIStateManager uiStateManager;
    private VerticalMenuComponent vscrollComponent;

    enum Mode {
        MAP,
        DASH
    }

    static {
        int backColor = resources.getColor(R.color.mm_back);
        back = IconBkColorViewHolder.buildModel(R.id.menu_back, R.drawable.icon_mm_back, backColor, MainMenu.bkColorUnselected, backColor, resources.getString(R.string.back), null);
        String title = resources.getString(R.string.manual_zoom);
        int fluctuatorColor = resources.getColor(R.color.mm_options_manual_zoom);
        manualZoom = IconBkColorViewHolder.buildModel(R.id.main_menu_options_manual_zoom, R.drawable.icon_options_map_zoom_manual_2, fluctuatorColor, MainMenu.bkColorUnselected, fluctuatorColor, title, null);
        title = resources.getString(R.string.show_raw_gps);
        fluctuatorColor = resources.getColor(R.color.mm_options_manual_zoom);
        boolean isOn = GpsUtils.SHOW_RAW_GPS.isEnabled();
        rawGps = SwitchViewHolder.Companion.buildModel(R.id.main_menu_options_raw_gps, fluctuatorColor, title, resources.getString(isOn ? R.string.si_enabled : R.string.si_disabled), isOn, true);
        title = resources.getString(R.string.auto_zoom);
        fluctuatorColor = resources.getColor(R.color.mm_options_auto_zoom);
        autoZoom = IconBkColorViewHolder.buildModel(R.id.main_menu_options_auto_zoom, R.drawable.icon_options_map_zoom_auto_2, fluctuatorColor, MainMenu.bkColorUnselected, fluctuatorColor, title, null);
        title = resources.getString(R.string.play_demo);
        fluctuatorColor = resources.getColor(R.color.mm_options_demo);
        playDemo = IconBkColorViewHolder.buildModel(R.id.main_menu_options_play_demo, R.drawable.icon_mm_demo_play_2, fluctuatorColor, MainMenu.bkColorUnselected, fluctuatorColor, title, null);
        title = resources.getString(R.string.pause_demo);
        fluctuatorColor = resources.getColor(R.color.mm_options_demo);
        pauseDemo = IconBkColorViewHolder.buildModel(R.id.main_menu_options_pause_demo, R.drawable.icon_mm_demo_pause_2, fluctuatorColor, MainMenu.bkColorUnselected, fluctuatorColor, title, null);
        title = resources.getString(R.string.restart_demo);
        fluctuatorColor = resources.getColor(R.color.mm_options_demo);
        restartDemo = IconBkColorViewHolder.buildModel(R.id.main_menu_options_restart_demo, R.drawable.icon_mm_demo_restart_2, fluctuatorColor, MainMenu.bkColorUnselected, fluctuatorColor, title, null);
        title = resources.getString(R.string.scroll_left_gauge);
        fluctuatorColor = resources.getColor(R.color.mm_options_scroll_gauge);
        scrollLeftGauge = IconBkColorViewHolder.buildModel(R.id.main_menu_options_scroll_left, R.drawable.icon_options_dash_scroll_left_2, fluctuatorColor, MainMenu.bkColorUnselected, fluctuatorColor, title, null);
        title = resources.getString(R.string.scroll_right_gauge);
        fluctuatorColor = resources.getColor(R.color.mm_options_scroll_gauge);
        scrollRightGauge = IconBkColorViewHolder.buildModel(R.id.main_menu_options_scroll_right, R.drawable.icon_options_dash_scroll_right_2, fluctuatorColor, MainMenu.bkColorUnselected, fluctuatorColor, title, null);
        title = resources.getString(R.string.carousel_menu_smartdash_select_center_gauge);
        fluctuatorColor = resources.getColor(R.color.mm_options_scroll_gauge);
        selectCenterGauge = IconBkColorViewHolder.buildModel(R.id.main_menu_options_select_center_gauge, R.drawable.icon_center_gauge, fluctuatorColor, MainMenu.bkColorUnselected, fluctuatorColor, title, null);
        title = resources.getString(R.string.carousel_menu_smartdash_side_gauges);
        fluctuatorColor = resources.getColor(R.color.mm_options_scroll_gauge);
        sideGauges = IconBkColorViewHolder.buildModel(R.id.main_menu_options_side_gauges, R.drawable.icon_side_gauges, fluctuatorColor, MainMenu.bkColorUnselected, fluctuatorColor, title, null);
    }

    MainOptionsMenu(Bus bus, VerticalMenuComponent vscrollComponent, Presenter presenter, IMenu parent) {
        this.bus = bus;
        this.vscrollComponent = vscrollComponent;
        this.presenter = presenter;
        this.parent = parent;
        RemoteDeviceManager remoteDeviceManager = RemoteDeviceManager.getInstance();
        this.driveRecorder = remoteDeviceManager.getDriveRecorder();
        this.uiStateManager = remoteDeviceManager.getUiStateManager();
        this.dashGaugeConfiguratorMenu = new DashGaugeConfiguratorMenu(bus, remoteDeviceManager.getSharedPreferences(), this.vscrollComponent, this.presenter, this);
    }

    void setMode(Mode mode) {
        this.mode = mode;
    }

    public List<Model> getItems() {
        List<Model> list = new ArrayList();
        list.add(back);
        switch (this.mode) {
            case MAP:
                if (HereMapsManager.getInstance().isInitialized()) {
                    LocalPreferences localPreferences = this.driverProfileManager.getLocalPreferences();
                    if (localPreferences.manualZoom == null || !localPreferences.manualZoom.booleanValue()) {
                        list.add(manualZoom);
                    } else {
                        list.add(autoZoom);
                    }
                    if (!DeviceUtil.isUserBuild()) {
                        list.add(rawGps);
                    }
                    boolean isDemoAvailable = this.driveRecorder.isDemoAvailable();
                    boolean isPlaying = this.driveRecorder.isDemoPlaying();
                    if (isDemoAvailable) {
                        if (!isPlaying) {
                            list.add(playDemo);
                            break;
                        }
                        list.add(pauseDemo);
                        list.add(restartDemo);
                        break;
                    }
                }
                break;
            case DASH:
                SmartDashView smartDashView = this.uiStateManager.getSmartDashView();
                if (smartDashView != null) {
                    switch (smartDashView.getCurrentScrollableSideOption()) {
                        case 0:
                            list.add(scrollLeftGauge);
                            break;
                        case 1:
                            list.add(scrollRightGauge);
                            break;
                    }
                }
                list.add(selectCenterGauge);
                list.add(sideGauges);
                break;
        }
        this.cachedList = list;
        return list;
    }

    public int getInitialSelection() {
        return 1;
    }

    public VerticalFastScrollIndex getScrollIndex() {
        return null;
    }

    public void setBackSelectionPos(int n) {
        this.backSelection = n;
    }

    public void setBackSelectionId(int id) {
    }

    public void setSelectedIcon() {
        int icon = 0;
        int color = 0;
        String title = null;
        switch (this.mode) {
            case MAP:
                icon = R.drawable.icon_main_menu_map_options;
                color = mapColor;
                title = mapTitle;
                break;
            case DASH:
                icon = R.drawable.icon_main_menu_dash_options;
                color = dashColor;
                title = dashTitle;
                break;
        }
        this.vscrollComponent.setSelectedIconColorImage(icon, color, null, 1.0f);
        this.vscrollComponent.selectedText.setText(title);
    }

    public boolean selectItem(ItemSelectionState selection) {
        boolean enabled = true;
        sLogger.v("select id:" + selection.id + " pos:" + selection.pos);
        switch (selection.id) {
            case R.id.main_menu_options_auto_zoom:
                this.presenter.performSelectionAnimation(new Runnable() {
                    public void run() {
                        MainOptionsMenu.this.presenter.close();
                        LocalPreferences localPreferences = MainOptionsMenu.this.driverProfileManager.getLocalPreferences();
                        AnalyticsSupport.recordOptionSelection("Auto_Zoom");
                        MainOptionsMenu.this.driverProfileManager.updateLocalPreferences(new Builder(localPreferences).manualZoom(Boolean.valueOf(false)).manualZoomLevel(Float.valueOf(-1.0f)).build());
                    }
                });
                break;
            case R.id.main_menu_options_manual_zoom:
                this.presenter.performSelectionAnimation(new Runnable() {
                    public void run() {
                        MainOptionsMenu.this.presenter.close();
                        LocalPreferences localPreferences = MainOptionsMenu.this.driverProfileManager.getLocalPreferences();
                        AnalyticsSupport.recordOptionSelection("Manual_Zoom");
                        MainOptionsMenu.this.driverProfileManager.updateLocalPreferences(new Builder(localPreferences).manualZoom(Boolean.valueOf(true)).manualZoomLevel(Float.valueOf(-1.0f)).build());
                    }
                });
                break;
            case R.id.main_menu_options_pause_demo:
                this.presenter.performSelectionAnimation(new Runnable() {
                    public void run() {
                        MainOptionsMenu.this.presenter.close();
                        MainOptionsMenu.this.driveRecorder.performDemoPlaybackAction(Action.PAUSE, true);
                    }
                });
                break;
            case R.id.main_menu_options_play_demo:
                this.presenter.performSelectionAnimation(new Runnable() {
                    public void run() {
                        MainOptionsMenu.this.presenter.close();
                        if (MainOptionsMenu.this.driveRecorder.isDemoPaused()) {
                            MainOptionsMenu.this.driveRecorder.performDemoPlaybackAction(Action.RESUME, true);
                        } else {
                            MainOptionsMenu.this.driveRecorder.performDemoPlaybackAction(Action.PLAY, true);
                        }
                    }
                });
                break;
            case R.id.main_menu_options_raw_gps:
                sLogger.i("Toggling raw gps");
                Model model = selection.model;
                if (model.isOn) {
                    enabled = false;
                }
                model.isOn = enabled;
                model.subTitle = resources.getString(enabled ? R.string.si_enabled : R.string.si_disabled);
                GpsUtils.SHOW_RAW_GPS.setEnabled(enabled);
                this.presenter.refreshDataforPos(selection.pos);
                return false;
            case R.id.main_menu_options_restart_demo:
                this.presenter.performSelectionAnimation(new Runnable() {
                    public void run() {
                        MainOptionsMenu.this.presenter.close();
                        MainOptionsMenu.this.driveRecorder.performDemoPlaybackAction(Action.RESTART, true);
                    }
                });
                break;
            case R.id.main_menu_options_scroll_left:
            case R.id.main_menu_options_scroll_right:
                this.presenter.performSelectionAnimation(new Runnable() {
                    public void run() {
                        MainOptionsMenu.this.presenter.close();
                        SmartDashView smartDashView = MainOptionsMenu.this.uiStateManager.getSmartDashView();
                        if (smartDashView != null) {
                            smartDashView.onScrollableSideOptionSelected();
                        }
                    }
                });
                break;
            case R.id.main_menu_options_select_center_gauge:
                this.dashGaugeConfiguratorMenu.setGaugeType(GaugeType.CENTER);
                this.presenter.loadMenu(this.dashGaugeConfiguratorMenu, MenuLevel.SUB_LEVEL, selection.pos, 0);
                break;
            case R.id.main_menu_options_side_gauges:
                this.dashGaugeConfiguratorMenu.setGaugeType(GaugeType.SIDE);
                this.presenter.loadMenu(this.dashGaugeConfiguratorMenu, MenuLevel.SUB_LEVEL, selection.pos, 0);
                break;
            case R.id.menu_back:
                sLogger.v("back");
                AnalyticsSupport.recordMenuSelection("back");
                this.presenter.loadMenu(this.parent, MenuLevel.BACK_TO_PARENT, this.backSelection, 0);
                break;
        }
        return true;
    }

    public Menu getType() {
        return Menu.MAIN_OPTIONS;
    }

    public boolean isItemClickable(int id, int pos) {
        return true;
    }

    public Model getModelfromPos(int pos) {
        if (this.cachedList == null || this.cachedList.size() <= pos) {
            return null;
        }
        return (Model) this.cachedList.get(pos);
    }

    public boolean isBindCallsEnabled() {
        return false;
    }

    public void onBindToView(Model model, View view, int pos, ModelState state) {
    }

    public IMenu getChildMenu(IMenu parent, String args, String path) {
        return null;
    }

    public void onUnload(MenuLevel level) {
    }

    public void onItemSelected(ItemSelectionState selection) {
    }

    public void onScrollIdle() {
    }

    public void onFastScrollStart() {
    }

    public void onFastScrollEnd() {
    }

    public void showToolTip() {
    }

    public boolean isFirstItemEmpty() {
        return true;
    }
}
