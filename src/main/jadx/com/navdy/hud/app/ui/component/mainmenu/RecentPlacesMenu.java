package com.navdy.hud.app.ui.component.mainmenu;

import android.content.res.Resources;
import android.view.View;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.R;
import com.navdy.hud.app.analytics.AnalyticsSupport;
import com.navdy.hud.app.framework.destinations.Destination;
import com.navdy.hud.app.framework.destinations.DestinationsManager;
import com.navdy.hud.app.ui.component.mainmenu.IMenu.Menu;
import com.navdy.hud.app.ui.component.mainmenu.IMenu.MenuLevel;
import com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.Presenter;
import com.navdy.hud.app.ui.component.vlist.VerticalList.ItemSelectionState;
import com.navdy.hud.app.ui.component.vlist.VerticalList.Model;
import com.navdy.hud.app.ui.component.vlist.VerticalList.ModelState;
import com.navdy.hud.app.ui.component.vlist.VerticalModelCache;
import com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder;
import com.navdy.hud.app.ui.component.vmenu.VerticalFastScrollIndex;
import com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent;
import com.navdy.service.library.log.Logger;
import com.squareup.otto.Bus;
import java.util.ArrayList;
import java.util.List;

public class RecentPlacesMenu implements IMenu {
    private static final Model back;
    private static final String places = resources.getString(R.string.carousel_menu_recent_place);
    private static final int placesColor = resources.getColor(R.color.mm_recent_places);
    private static final Resources resources = HudApplication.getAppContext().getResources();
    private static final Logger sLogger = new Logger(RecentPlacesMenu.class);
    private int backSelection;
    private Bus bus;
    private List<Model> cachedList;
    private IMenu parent;
    private Presenter presenter;
    private List<Model> returnToCacheList;
    private VerticalMenuComponent vscrollComponent;

    static {
        String title = resources.getString(R.string.back);
        int fluctuatorColor = resources.getColor(R.color.mm_back);
        back = IconBkColorViewHolder.buildModel(R.id.menu_back, R.drawable.icon_mm_back, fluctuatorColor, MainMenu.bkColorUnselected, fluctuatorColor, title, null);
    }

    public RecentPlacesMenu(Bus bus, VerticalMenuComponent vscrollComponent, Presenter presenter, IMenu parent) {
        this.bus = bus;
        this.vscrollComponent = vscrollComponent;
        this.presenter = presenter;
        this.parent = parent;
    }

    public List<Model> getItems() {
        if (this.cachedList != null) {
            return this.cachedList;
        }
        List<Model> list = new ArrayList();
        this.returnToCacheList = new ArrayList();
        list.add(back);
        List<Destination> recentDestinations = DestinationsManager.getInstance().getRecentDestinations();
        if (recentDestinations == null || recentDestinations.size() <= 0) {
            sLogger.v("no recent destinations");
        } else {
            sLogger.v("recent destinations:" + recentDestinations.size());
            int counter = 0;
            for (Destination destination : recentDestinations) {
                int counter2 = counter + 1;
                Model model = PlacesMenu.buildModel(destination, counter);
                model.state = destination;
                list.add(model);
                this.returnToCacheList.add(model);
                counter = counter2;
            }
        }
        this.cachedList = list;
        return list;
    }

    public int getInitialSelection() {
        if (this.cachedList == null || this.cachedList.size() <= 1) {
            return 0;
        }
        return 1;
    }

    public VerticalFastScrollIndex getScrollIndex() {
        return null;
    }

    public void setBackSelectionPos(int n) {
        this.backSelection = n;
    }

    public void setBackSelectionId(int id) {
    }

    public void setSelectedIcon() {
        this.vscrollComponent.setSelectedIconColorImage(R.drawable.icon_place_recent_2, placesColor, null, 1.0f);
        this.vscrollComponent.selectedText.setText(places);
    }

    public Model getModelfromPos(int pos) {
        if (this.cachedList == null || this.cachedList.size() <= pos) {
            return null;
        }
        return (Model) this.cachedList.get(pos);
    }

    public boolean isBindCallsEnabled() {
        return false;
    }

    public void onBindToView(Model model, View view, int pos, ModelState state) {
    }

    public IMenu getChildMenu(IMenu parent, String args, String path) {
        return null;
    }

    public void onUnload(MenuLevel level) {
        switch (level) {
            case CLOSE:
                if (this.returnToCacheList != null) {
                    sLogger.v("rpm:unload add to cache");
                    VerticalModelCache.addToCache(this.returnToCacheList);
                    this.returnToCacheList = null;
                    return;
                }
                return;
            default:
                return;
        }
    }

    public void onItemSelected(ItemSelectionState selection) {
    }

    public void onScrollIdle() {
    }

    public void onFastScrollStart() {
    }

    public void onFastScrollEnd() {
    }

    public void showToolTip() {
    }

    public boolean isFirstItemEmpty() {
        return true;
    }

    public boolean selectItem(ItemSelectionState selection) {
        sLogger.v("select id:" + selection.id + " pos:" + selection.pos);
        switch (selection.id) {
            case R.id.menu_back:
                sLogger.v("back");
                AnalyticsSupport.recordMenuSelection("back");
                this.presenter.loadMenu(this.parent, MenuLevel.BACK_TO_PARENT, this.backSelection, 0);
                break;
            default:
                ((PlacesMenu) this.parent).launchDestination((Model) this.cachedList.get(selection.pos));
                break;
        }
        return true;
    }

    public Menu getType() {
        return Menu.RECENT_PLACES;
    }

    public boolean isItemClickable(int id, int pos) {
        return true;
    }
}
