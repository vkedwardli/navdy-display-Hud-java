package com.navdy.hud.app.ui.component.homescreen;

import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import butterknife.ButterKnife.Finder;
import com.navdy.hud.app.R;
import com.navdy.hud.app.ui.component.tbt.TbtViewContainer;
import com.navdy.hud.app.view.DashboardWidgetView;
import com.navdy.hud.app.view.SpeedLimitSignView;

public class HomeScreenView$$ViewInjector {
    public static void inject(Finder finder, HomeScreenView target, Object source) {
        target.mapContainer = (NavigationView) finder.findRequiredView(source, R.id.map_container, "field 'mapContainer'");
        target.smartDashContainer = (BaseSmartDashView) finder.findRequiredView(source, R.id.smartDashContainer, "field 'smartDashContainer'");
        target.openMapRoadInfoContainer = (OpenRoadView) finder.findRequiredView(source, R.id.openMapRoadInfoContainer, "field 'openMapRoadInfoContainer'");
        target.tbtView = (TbtViewContainer) finder.findRequiredView(source, R.id.activeMapRoadInfoContainer, "field 'tbtView'");
        target.recalcRouteContainer = (RecalculatingView) finder.findRequiredView(source, R.id.recalcRouteContainer, "field 'recalcRouteContainer'");
        target.timeContainer = (TimeView) finder.findRequiredView(source, R.id.time, "field 'timeContainer'");
        target.activeEtaContainer = (EtaView) finder.findRequiredView(source, R.id.activeEtaContainer, "field 'activeEtaContainer'");
        target.navigationViewsContainer = (RelativeLayout) finder.findRequiredView(source, R.id.navigation_views_container, "field 'navigationViewsContainer'");
        target.laneGuidanceView = (LaneGuidanceView) finder.findRequiredView(source, R.id.laneGuidance, "field 'laneGuidanceView'");
        target.laneGuidanceIconIndicator = (ImageView) finder.findRequiredView(source, R.id.lane_guidance_map_icon_indicator, "field 'laneGuidanceIconIndicator'");
        target.mainscreenRightSection = (HomeScreenRightSectionView) finder.findRequiredView(source, R.id.mainscreenRightSection, "field 'mainscreenRightSection'");
        target.mapViewSpeedContainer = (ViewGroup) finder.findRequiredView(source, R.id.map_view_speed_container, "field 'mapViewSpeedContainer'");
        target.speedView = (SpeedView) finder.findRequiredView(source, R.id.speedContainer, "field 'speedView'");
        target.speedLimitSignView = (SpeedLimitSignView) finder.findRequiredView(source, R.id.home_screen_speed_limit_sign, "field 'speedLimitSignView'");
        target.dashboardWidgetView = (DashboardWidgetView) finder.findRequiredView(source, R.id.drive_score_events, "field 'dashboardWidgetView'");
        target.mapMask = (ImageView) finder.findRequiredView(source, R.id.map_mask, "field 'mapMask'");
    }

    public static void reset(HomeScreenView target) {
        target.mapContainer = null;
        target.smartDashContainer = null;
        target.openMapRoadInfoContainer = null;
        target.tbtView = null;
        target.recalcRouteContainer = null;
        target.timeContainer = null;
        target.activeEtaContainer = null;
        target.navigationViewsContainer = null;
        target.laneGuidanceView = null;
        target.laneGuidanceIconIndicator = null;
        target.mainscreenRightSection = null;
        target.mapViewSpeedContainer = null;
        target.speedView = null;
        target.speedLimitSignView = null;
        target.dashboardWidgetView = null;
        target.mapMask = null;
    }
}
