package com.navdy.hud.app.ui.component.carousel;

import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.navdy.hud.app.R;
import com.navdy.hud.app.ui.component.carousel.Carousel.Model;
import com.navdy.hud.app.ui.component.carousel.Carousel.ViewType;
import com.navdy.hud.app.ui.component.image.ColorImageView;
import com.navdy.hud.app.ui.component.image.CrossFadeImageView;
import com.navdy.hud.app.ui.component.image.CrossFadeImageView.Mode;
import com.navdy.hud.app.ui.component.image.InitialsImageView;
import com.navdy.hud.app.ui.component.image.InitialsImageView.Style;
import com.navdy.service.library.log.Logger;

public class CarouselAdapter {
    private static final Logger sLogger = new Logger(CarouselAdapter.class);
    CarouselLayout carousel;

    public CarouselAdapter(CarouselLayout carousel) {
        this.carousel = carousel;
    }

    public View getView(int position, View convertView, ViewType type, int layoutId, int size) {
        View view;
        if (convertView != null) {
            convertView.setTag(R.id.item_id, null);
        }
        Model item = null;
        if (position >= 0 && position < this.carousel.model.size()) {
            item = (Model) this.carousel.model.get(position);
        }
        if (convertView == null) {
            sLogger.v("create " + type + " view");
            view = this.carousel.inflater.inflate(layoutId, null);
            if (view instanceof CrossFadeImageView) {
                if (type == ViewType.SIDE) {
                    ((CrossFadeImageView) view).inject(Mode.SMALL);
                } else if (type == ViewType.MIDDLE_LEFT) {
                    ((CrossFadeImageView) view).inject(Mode.BIG);
                }
            }
        } else {
            view = convertView;
            if (view instanceof CrossFadeImageView) {
                if (type == ViewType.SIDE) {
                    ((CrossFadeImageView) view).setMode(Mode.SMALL);
                } else if (type == ViewType.MIDDLE_LEFT) {
                    ((CrossFadeImageView) view).setMode(Mode.BIG);
                }
            }
        }
        if (item != null) {
            view.setTag(R.id.item_id, Integer.valueOf(item.id));
            view.setVisibility(0);
            processView(type, item, view, position, size);
        } else {
            view.setVisibility(4);
            view.setTag(R.id.item_id, null);
        }
        return view;
    }

    private void processView(ViewType viewType, Model item, View view, int position, int size) {
        if (viewType == ViewType.MIDDLE_RIGHT) {
            processInfoView(item, view, position);
        } else {
            processImageView(viewType, item, view, position, size);
        }
    }

    private void processImageView(ViewType viewType, Model item, View imageView, int position, int size) {
        if (imageView instanceof CrossFadeImageView) {
            CrossFadeImageView crossFadeImageView = (CrossFadeImageView) imageView;
            InitialsImageView big = (InitialsImageView) crossFadeImageView.getBig();
            View small = crossFadeImageView.getSmall();
            big.setImage(item.largeImageRes, null, Style.DEFAULT);
            if (this.carousel.imageLytResourceId == R.layout.crossfade_image_lyt) {
                ((InitialsImageView) small).setImage(item.smallImageRes, null, Style.DEFAULT);
            } else {
                ((ColorImageView) small).setColor(item.smallImageColor);
            }
        } else if (imageView instanceof InitialsImageView) {
            InitialsImageView initialsImageView = (InitialsImageView) imageView;
            if (viewType == ViewType.MIDDLE_LEFT) {
                initialsImageView.setImage(item.largeImageRes, null, Style.DEFAULT);
            } else {
                initialsImageView.setImage(item.smallImageRes, null, Style.DEFAULT);
            }
        }
        if (this.carousel.viewProcessor != null) {
            try {
                if (viewType == ViewType.MIDDLE_LEFT) {
                    this.carousel.viewProcessor.processLargeImageView(item, imageView, position, size, size);
                } else {
                    this.carousel.viewProcessor.processSmallImageView(item, imageView, position, size, size);
                }
            } catch (Throwable t) {
                sLogger.e(t);
            }
        }
    }

    private void processInfoView(Model item, View view, int position) {
        if (item.infoMap != null) {
            ViewGroup viewGroup = (ViewGroup) view;
            int childCount = viewGroup.getChildCount();
            for (int i = 0; i < childCount; i++) {
                View child = viewGroup.getChildAt(i);
                if (child instanceof TextView) {
                    String text = (String) item.infoMap.get(Integer.valueOf(child.getId()));
                    if (text != null) {
                        ((TextView) child).setText(text);
                        child.setVisibility(0);
                    } else {
                        child.setVisibility(8);
                    }
                }
            }
        }
        if (this.carousel.viewProcessor != null) {
            try {
                this.carousel.viewProcessor.processInfoView(item, view, position);
            } catch (Throwable t) {
                sLogger.e(t);
            }
        }
    }
}
