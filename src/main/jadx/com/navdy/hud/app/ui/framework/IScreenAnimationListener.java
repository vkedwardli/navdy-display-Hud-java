package com.navdy.hud.app.ui.framework;

import com.navdy.hud.app.screen.BaseScreen;

public interface IScreenAnimationListener {
    void onStart(BaseScreen baseScreen, BaseScreen baseScreen2);

    void onStop(BaseScreen baseScreen, BaseScreen baseScreen2);
}
