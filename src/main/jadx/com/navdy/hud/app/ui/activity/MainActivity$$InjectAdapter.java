package com.navdy.hud.app.ui.activity;

import com.navdy.hud.app.device.PowerManager;
import com.navdy.hud.app.manager.PairingManager;
import com.navdy.hud.app.ui.activity.Main.Presenter;
import dagger.MembersInjector;
import dagger.internal.Binding;
import dagger.internal.Linker;
import java.util.Set;
import javax.inject.Provider;

public final class MainActivity$$InjectAdapter extends Binding<MainActivity> implements Provider<MainActivity>, MembersInjector<MainActivity> {
    private Binding<Presenter> mainPresenter;
    private Binding<PairingManager> pairingManager;
    private Binding<PowerManager> powerManager;
    private Binding<HudBaseActivity> supertype;

    public MainActivity$$InjectAdapter() {
        super("com.navdy.hud.app.ui.activity.MainActivity", "members/com.navdy.hud.app.ui.activity.MainActivity", false, MainActivity.class);
    }

    public void attach(Linker linker) {
        this.mainPresenter = linker.requestBinding("com.navdy.hud.app.ui.activity.Main$Presenter", MainActivity.class, getClass().getClassLoader());
        this.pairingManager = linker.requestBinding("com.navdy.hud.app.manager.PairingManager", MainActivity.class, getClass().getClassLoader());
        this.powerManager = linker.requestBinding("com.navdy.hud.app.device.PowerManager", MainActivity.class, getClass().getClassLoader());
        Linker linker2 = linker;
        this.supertype = linker2.requestBinding("members/com.navdy.hud.app.ui.activity.HudBaseActivity", MainActivity.class, getClass().getClassLoader(), false, true);
    }

    public void getDependencies(Set<Binding<?>> set, Set<Binding<?>> injectMembersBindings) {
        injectMembersBindings.add(this.mainPresenter);
        injectMembersBindings.add(this.pairingManager);
        injectMembersBindings.add(this.powerManager);
        injectMembersBindings.add(this.supertype);
    }

    public MainActivity get() {
        MainActivity result = new MainActivity();
        injectMembers(result);
        return result;
    }

    public void injectMembers(MainActivity object) {
        object.mainPresenter = (Presenter) this.mainPresenter.get();
        object.pairingManager = (PairingManager) this.pairingManager.get();
        object.powerManager = (PowerManager) this.powerManager.get();
        this.supertype.injectMembers(object);
    }
}
