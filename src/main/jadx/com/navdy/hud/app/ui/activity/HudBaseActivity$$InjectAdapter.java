package com.navdy.hud.app.ui.activity;

import com.navdy.hud.app.common.BaseActivity;
import com.navdy.hud.app.manager.InputManager;
import dagger.MembersInjector;
import dagger.internal.Binding;
import dagger.internal.Linker;
import java.util.Set;

public final class HudBaseActivity$$InjectAdapter extends Binding<HudBaseActivity> implements MembersInjector<HudBaseActivity> {
    private Binding<InputManager> inputManager;
    private Binding<BaseActivity> supertype;

    public HudBaseActivity$$InjectAdapter() {
        super(null, "members/com.navdy.hud.app.ui.activity.HudBaseActivity", false, HudBaseActivity.class);
    }

    public void attach(Linker linker) {
        this.inputManager = linker.requestBinding("com.navdy.hud.app.manager.InputManager", HudBaseActivity.class, getClass().getClassLoader());
        Linker linker2 = linker;
        this.supertype = linker2.requestBinding("members/com.navdy.hud.app.common.BaseActivity", HudBaseActivity.class, getClass().getClassLoader(), false, true);
    }

    public void getDependencies(Set<Binding<?>> set, Set<Binding<?>> injectMembersBindings) {
        injectMembersBindings.add(this.inputManager);
        injectMembersBindings.add(this.supertype);
    }

    public void injectMembers(HudBaseActivity object) {
        object.inputManager = (InputManager) this.inputManager.get();
        this.supertype.injectMembers(object);
    }
}
