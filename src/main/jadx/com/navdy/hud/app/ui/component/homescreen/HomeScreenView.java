package com.navdy.hud.app.ui.component.homescreen;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.AnimatorSet.Builder;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import butterknife.ButterKnife;
import butterknife.InjectView;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.R;
import com.navdy.hud.app.event.DriverProfileChanged;
import com.navdy.hud.app.framework.DriverProfileHelper;
import com.navdy.hud.app.framework.notifications.NotificationManager;
import com.navdy.hud.app.gesture.GestureDetector.GestureListener;
import com.navdy.hud.app.manager.InputManager;
import com.navdy.hud.app.manager.InputManager.CustomKeyEvent;
import com.navdy.hud.app.manager.InputManager.IInputHandler;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.manager.SpeedManager;
import com.navdy.hud.app.manager.SpeedManager.SpeedDataExpired;
import com.navdy.hud.app.manager.SpeedManager.SpeedUnit;
import com.navdy.hud.app.manager.SpeedManager.SpeedUnitChanged;
import com.navdy.hud.app.maps.MapEvents.ArrivalEvent;
import com.navdy.hud.app.maps.MapEvents.GPSSpeedEvent;
import com.navdy.hud.app.maps.MapEvents.ManeuverDisplay;
import com.navdy.hud.app.maps.MapEvents.NavigationModeChange;
import com.navdy.hud.app.maps.NavigationMode;
import com.navdy.hud.app.maps.here.HereNavigationManager;
import com.navdy.hud.app.obd.ObdManager;
import com.navdy.hud.app.obd.ObdManager.ObdPidChangeEvent;
import com.navdy.hud.app.screen.BaseScreen;
import com.navdy.hud.app.ui.activity.Main;
import com.navdy.hud.app.ui.component.homescreen.HomeScreen.DisplayMode;
import com.navdy.hud.app.ui.component.homescreen.HomeScreen.Presenter;
import com.navdy.hud.app.ui.component.tbt.TbtViewContainer;
import com.navdy.hud.app.ui.framework.DefaultAnimationListener;
import com.navdy.hud.app.ui.framework.IScreenAnimationListener;
import com.navdy.hud.app.ui.framework.UIStateManager;
import com.navdy.hud.app.util.DeviceUtil;
import com.navdy.hud.app.util.os.SystemProperties;
import com.navdy.hud.app.view.DashboardWidgetPresenter;
import com.navdy.hud.app.view.DashboardWidgetView;
import com.navdy.hud.app.view.DriveScoreGaugePresenter;
import com.navdy.hud.app.view.ICustomAnimator;
import com.navdy.hud.app.view.MainView.CustomAnimationMode;
import com.navdy.hud.app.view.SpeedLimitSignView;
import com.navdy.service.library.events.input.Gesture;
import com.navdy.service.library.events.input.GestureEvent;
import com.navdy.service.library.events.ui.Screen;
import com.navdy.service.library.log.Logger;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;
import java.util.concurrent.TimeUnit;
import javax.inject.Inject;
import mortar.Mortar;

public class HomeScreenView extends RelativeLayout implements ICustomAnimator, IInputHandler, GestureListener {
    private static final int MIN_SPEED_LIMIT_VISIBLE_DURATION = ((int) TimeUnit.SECONDS.toMillis(3));
    static final String SYSPROP_ALWAYS_SHOW_SPEED_LIMIT_SIGN = "persist.sys.speedlimit.always";
    static final Logger sLogger = new Logger(HomeScreenView.class);
    @InjectView(R.id.activeEtaContainer)
    EtaView activeEtaContainer;
    private boolean alwaysShowSpeedLimitSign;
    @Inject
    Bus bus;
    private NavigationMode currentNavigationMode;
    @InjectView(R.id.drive_score_events)
    DashboardWidgetView dashboardWidgetView;
    DriveScoreGaugePresenter driveScoreGaugePresenter;
    private SharedPreferences driverPreferences;
    @Inject
    SharedPreferences globalPreferences;
    private Handler handler;
    private Runnable hideSpeedLimitRunnable;
    private boolean isRecalculating;
    private boolean isTopInit;
    private boolean isTopViewAnimationOut;
    private boolean isTopViewAnimationRunning;
    @InjectView(R.id.lane_guidance_map_icon_indicator)
    ImageView laneGuidanceIconIndicator;
    @InjectView(R.id.laneGuidance)
    LaneGuidanceView laneGuidanceView;
    private long lastSpeedLimitShown;
    @InjectView(R.id.mainscreenRightSection)
    HomeScreenRightSectionView mainscreenRightSection;
    @InjectView(R.id.map_container)
    NavigationView mapContainer;
    @InjectView(R.id.map_mask)
    ImageView mapMask;
    @InjectView(R.id.map_view_speed_container)
    ViewGroup mapViewSpeedContainer;
    private DisplayMode mode;
    @InjectView(R.id.navigation_views_container)
    protected RelativeLayout navigationViewsContainer;
    private NotificationManager notificationManager;
    @InjectView(R.id.openMapRoadInfoContainer)
    OpenRoadView openMapRoadInfoContainer;
    private boolean paused;
    @Inject
    Presenter presenter;
    @InjectView(R.id.recalcRouteContainer)
    RecalculatingView recalcRouteContainer;
    private AnimatorSet rightScreenAnimator;
    private IScreenAnimationListener screenAnimationListener;
    private boolean showCollapsedNotif;
    @InjectView(R.id.smartDashContainer)
    BaseSmartDashView smartDashContainer;
    private int speedLimit;
    @InjectView(R.id.home_screen_speed_limit_sign)
    SpeedLimitSignView speedLimitSignView;
    private SpeedManager speedManager;
    @InjectView(R.id.speedContainer)
    SpeedView speedView;
    @InjectView(R.id.activeMapRoadInfoContainer)
    TbtViewContainer tbtView;
    @InjectView(R.id.time)
    TimeView timeContainer;
    private Runnable topViewAnimationRunnable;
    private AnimatorSet topViewAnimator;
    private UIStateManager uiStateManager;

    public HomeScreenView(Context context) {
        this(context, null);
    }

    public HomeScreenView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public HomeScreenView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.isTopInit = true;
        this.speedManager = SpeedManager.getInstance();
        this.speedLimit = 0;
        this.alwaysShowSpeedLimitSign = false;
        this.topViewAnimationRunnable = new Runnable() {
            public void run() {
                HomeScreenView.sLogger.v("out animation");
                HomeScreenView.this.animateTopViewsOut();
            }
        };
        this.handler = new Handler();
        this.screenAnimationListener = new IScreenAnimationListener() {
            public void onStart(BaseScreen in, BaseScreen out) {
            }

            public void onStop(BaseScreen in, BaseScreen out) {
                if (HomeScreenView.this.showCollapsedNotif && in != null && in.getScreen() == Screen.SCREEN_HYBRID_MAP) {
                    HomeScreenView.this.showCollapsedNotif = false;
                    HomeScreenView.sLogger.v("expanding collapse notifi");
                    NotificationManager notificationManager = NotificationManager.getInstance();
                    if (notificationManager.makeNotificationCurrent(false)) {
                        notificationManager.showNotification();
                    }
                }
            }
        };
        this.mode = DisplayMode.MAP;
        this.hideSpeedLimitRunnable = new Runnable() {
            public void run() {
                HomeScreenView.this.hideSpeedLimit();
            }
        };
        this.uiStateManager = RemoteDeviceManager.getInstance().getUiStateManager();
        this.notificationManager = NotificationManager.getInstance();
        this.alwaysShowSpeedLimitSign = SystemProperties.getBoolean(SYSPROP_ALWAYS_SHOW_SPEED_LIMIT_SIGN, false);
        if (!isInEditMode()) {
            this.driveScoreGaugePresenter = new DriveScoreGaugePresenter(context, R.layout.drive_score_map_layout, false);
            Mortar.inject(context, this);
        }
    }

    public static boolean showDriveScoreEventsOnMap() {
        return !DeviceUtil.isUserBuild();
    }

    protected void onFinishInflate() {
        super.onFinishInflate();
        LayoutInflater.from(getContext()).inflate(R.layout.screen_home_smartdash, (ViewGroup) findViewById(R.id.smart_dash_place_holder), true);
        ButterKnife.inject((View) this);
        updateDriverPrefs();
        setDisplayMode(false);
        this.mapMask.setImageResource(showDriveScoreEventsOnMap() ? R.drawable.map_mask : R.drawable.navigation_map_mask);
        this.mapContainer.init(this);
        this.smartDashContainer.init(this);
        this.recalcRouteContainer.init(this);
        this.tbtView.init(this);
        this.openMapRoadInfoContainer.init(this);
        this.activeEtaContainer.init(this);
        this.laneGuidanceView.init(this);
        this.mainscreenRightSection.setX((float) HomeScreenResourceValues.mainScreenRightSectionX);
        setViews(this.uiStateManager.getCustomAnimateMode());
        sLogger.v("setting navigation mode to MAP");
        setMode(NavigationMode.MAP);
        if (this.presenter != null) {
            this.presenter.takeView(this);
        }
        if (showDriveScoreEventsOnMap()) {
            Bundle args = new Bundle();
            args.putInt(DashboardWidgetPresenter.EXTRA_GRAVITY, 0);
            args.putBoolean(DashboardWidgetPresenter.EXTRA_IS_ACTIVE, true);
            this.driveScoreGaugePresenter.setView(this.dashboardWidgetView, args);
            this.driveScoreGaugePresenter.setWidgetVisibleToUser(true);
        }
        this.uiStateManager.addScreenAnimationListener(this.screenAnimationListener);
        this.bus.register(this);
    }

    private void setViews(CustomAnimationMode mode) {
        this.activeEtaContainer.setView(mode);
        this.openMapRoadInfoContainer.setView(mode);
        CustomAnimationMode mainScreenMode = mode;
        if (mode == CustomAnimationMode.EXPAND && isModeVisible()) {
            mainScreenMode = CustomAnimationMode.SHRINK_MODE;
        }
        this.mapContainer.setView(mainScreenMode);
        this.smartDashContainer.setView(mainScreenMode);
        setViewSpeed(mode);
        this.tbtView.setView(mode);
        this.recalcRouteContainer.setView(mode);
        this.timeContainer.setView(mode);
        this.laneGuidanceView.setView(mode);
    }

    public boolean isNavigationActive() {
        return this.currentNavigationMode == NavigationMode.MAP_ON_ROUTE || this.currentNavigationMode == NavigationMode.TBT_ON_ROUTE;
    }

    public DisplayMode getDisplayMode() {
        return this.mode;
    }

    public void setDisplayMode(DisplayMode mode) {
        if (this.mode != mode) {
            this.mode = mode;
            setDisplayMode(true);
        }
    }

    private void setDisplayMode(boolean resetTopViews) {
        sLogger.v("setDisplayMode:" + this.mode);
        switch (this.mode) {
            case MAP:
                sLogger.v("dash invisible");
                setViewsBackground(-16777216);
                this.mapContainer.onResume();
                this.laneGuidanceView.onResume();
                ObdManager.getInstance().enableInstantaneousMode(false);
                this.smartDashContainer.setVisibility(4);
                this.smartDashContainer.onPause();
                if (showDriveScoreEventsOnMap()) {
                    this.dashboardWidgetView.setVisibility(0);
                    this.driveScoreGaugePresenter.onResume();
                }
                if (isNavigationActive()) {
                    this.timeContainer.setVisibility(8);
                    this.activeEtaContainer.setVisibility(0);
                } else {
                    this.timeContainer.setVisibility(0);
                    this.activeEtaContainer.setVisibility(8);
                }
                if (this.uiStateManager.isMainUIShrunk()) {
                    this.mapViewSpeedContainer.setVisibility(8);
                } else {
                    this.mapViewSpeedContainer.setVisibility(0);
                }
                this.navigationViewsContainer.setVisibility(0);
                this.laneGuidanceView.showLastEvent();
                resumeNavigationViewContainer();
                break;
            case SMART_DASH:
                sLogger.v("dash visible");
                setViewsBackground(0);
                if (showDriveScoreEventsOnMap()) {
                    this.dashboardWidgetView.setVisibility(8);
                    this.driveScoreGaugePresenter.onPause();
                }
                ObdManager.getInstance().enableInstantaneousMode(true);
                this.smartDashContainer.onResume();
                this.smartDashContainer.setVisibility(0);
                this.mapViewSpeedContainer.setVisibility(8);
                this.activeEtaContainer.setVisibility(8);
                this.timeContainer.setVisibility(8);
                this.laneGuidanceView.setVisibility(8);
                this.laneGuidanceIconIndicator.setVisibility(8);
                this.mapContainer.onPause();
                this.laneGuidanceView.onPause();
                this.navigationViewsContainer.setVisibility(0);
                resumeNavigationViewContainer();
                break;
        }
        updateRoadInfoView();
        if (resetTopViews) {
            resetTopViewsAnimator();
        }
    }

    @Subscribe
    public void onNavigationModeChange(NavigationModeChange navigationModeChange) {
        setMode(HereNavigationManager.getInstance().getCurrentNavigationMode());
    }

    @Subscribe
    public void onMapEvent(ManeuverDisplay event) {
        this.speedLimit = Math.round((float) SpeedManager.convert((double) event.currentSpeedLimit, SpeedUnit.METERS_PER_SECOND, SpeedManager.getInstance().getSpeedUnit()));
        updateSpeedLimitSign();
    }

    @Subscribe
    public void onSpeedUnitChanged(SpeedUnitChanged speedUnitChanged) {
        updateSpeedLimitSign();
    }

    @Subscribe
    public void ObdPidChangeEvent(ObdPidChangeEvent event) {
        switch (event.pid.getId()) {
            case 13:
                updateSpeedLimitSign();
                return;
            default:
                return;
        }
    }

    @Subscribe
    public void GPSSpeedChangeEvent(GPSSpeedEvent event) {
        updateSpeedLimitSign();
    }

    @Subscribe
    public void onSpeedDataExpired(SpeedDataExpired speedDataExpired) {
        updateSpeedLimitSign();
    }

    private void updateSpeedLimitSign() {
        if (this.speedLimit <= 0) {
            hideSpeedLimit();
        } else if (this.speedManager.getCurrentSpeed() > this.speedLimit || this.alwaysShowSpeedLimitSign) {
            this.handler.removeCallbacks(this.hideSpeedLimitRunnable);
            this.speedLimitSignView.setSpeedLimitUnit(this.speedManager.getSpeedUnit());
            this.speedLimitSignView.setSpeedLimit(this.speedLimit);
            this.speedLimitSignView.setVisibility(0);
            this.lastSpeedLimitShown = SystemClock.elapsedRealtime();
        } else {
            hideSpeedLimit();
        }
    }

    private void hideSpeedLimit() {
        this.handler.removeCallbacks(this.hideSpeedLimitRunnable);
        long diff = SystemClock.elapsedRealtime() - this.lastSpeedLimitShown;
        if (diff > ((long) MIN_SPEED_LIMIT_VISIBLE_DURATION)) {
            this.speedLimitSignView.setVisibility(8);
            this.speedLimitSignView.setSpeedLimit(0);
            this.lastSpeedLimitShown = 0;
            return;
        }
        this.handler.postDelayed(this.hideSpeedLimitRunnable, diff);
    }

    @Subscribe
    public void onDriverProfileChanged(DriverProfileChanged event) {
        updateDriverPrefs();
    }

    @Subscribe
    public void onArrivalEvent(ArrivalEvent event) {
        this.activeEtaContainer.setVisibility(8);
        if (getDisplayMode() == DisplayMode.MAP) {
            this.timeContainer.setVisibility(0);
        }
        resetTopViewsAnimator();
    }

    public TbtViewContainer getTbtView() {
        return this.tbtView;
    }

    public NavigationView getNavigationView() {
        return this.mapContainer;
    }

    public BaseSmartDashView getSmartDashView() {
        return this.smartDashContainer;
    }

    public RecalculatingView getRecalculatingView() {
        return this.recalcRouteContainer;
    }

    public EtaView getEtaView() {
        return this.activeEtaContainer;
    }

    public OpenRoadView getOpenRoadView() {
        return this.openMapRoadInfoContainer;
    }

    public SharedPreferences getDriverPreferences() {
        return this.driverPreferences;
    }

    void updateDriverPrefs() {
        this.driverPreferences = DriverProfileHelper.getInstance().getDriverProfileManager().getLocalPreferencesForCurrentDriverProfile(HudApplication.getAppContext());
    }

    public void setMode(NavigationMode navigationMode) {
        if (this.currentNavigationMode != navigationMode || this.mapContainer.isOverviewMapMode()) {
            sLogger.v("navigation mode changed to " + navigationMode);
            this.currentNavigationMode = navigationMode;
            updateLayoutForMode(navigationMode);
            this.mapContainer.layoutMap();
            this.bus.post(navigationMode);
        }
    }

    private void updateLayoutForMode(NavigationMode navigationMode) {
        int time;
        this.handler.removeCallbacks(this.topViewAnimationRunnable);
        this.isRecalculating = false;
        boolean navigationActive = isNavigationActive();
        this.mapContainer.updateLayoutForMode(navigationMode);
        this.smartDashContainer.updateLayoutForMode(navigationMode, false);
        this.mainscreenRightSection.updateLayoutForMode(navigationMode, this);
        if (navigationActive) {
            if (this.mode != DisplayMode.MAP) {
                this.mapViewSpeedContainer.setVisibility(8);
            } else if (this.uiStateManager.isMainUIShrunk()) {
                this.mapViewSpeedContainer.setVisibility(8);
            } else {
                this.mapViewSpeedContainer.setVisibility(0);
            }
            this.timeContainer.setVisibility(8);
            this.tbtView.clearState();
            this.tbtView.setVisibility(0);
            this.activeEtaContainer.clearState();
            if (this.mode == DisplayMode.MAP) {
                this.activeEtaContainer.setVisibility(0);
                this.timeContainer.setVisibility(8);
            }
            this.mapContainer.clearState();
            this.speedView.clearState();
        } else {
            this.recalcRouteContainer.hideRecalculating();
            this.activeEtaContainer.clearState();
            this.activeEtaContainer.setVisibility(8);
            this.tbtView.clearState();
            this.tbtView.setVisibility(8);
            if (this.mode == DisplayMode.MAP) {
                this.timeContainer.setVisibility(0);
                if (this.uiStateManager.isMainUIShrunk()) {
                    this.mapViewSpeedContainer.setVisibility(8);
                } else {
                    this.mapViewSpeedContainer.setVisibility(0);
                }
            } else {
                this.mapViewSpeedContainer.setVisibility(8);
            }
            this.mapContainer.clearState();
            this.speedView.clearState();
        }
        updateRoadInfoView();
        resetTopViewsAnimator();
        if (this.isTopInit) {
            time = 30000;
            this.isTopInit = false;
        } else {
            time = 10000;
        }
        this.handler.postDelayed(this.topViewAnimationRunnable, (long) time);
    }

    private void updateRoadInfoView() {
        if (isNavigationActive()) {
            this.openMapRoadInfoContainer.setVisibility(8);
            return;
        }
        this.openMapRoadInfoContainer.setRoad();
        this.openMapRoadInfoContainer.setVisibility(0);
    }

    public void onClick() {
    }

    public void onTrackHand(float normalized) {
    }

    public boolean onGesture(GestureEvent event) {
        if (event.gesture == Gesture.GESTURE_SWIPE_RIGHT && isModeVisible() && hasModeView() && (this.rightScreenAnimator == null || !this.rightScreenAnimator.isRunning())) {
            sLogger.v("swipe_right animateOutModeView");
            animateOutModeView(true);
            return true;
        }
        switch (this.mode) {
            case MAP:
                return this.mapContainer.onGesture(event);
            case SMART_DASH:
                return this.smartDashContainer.onGesture(event);
            default:
                return false;
        }
    }

    public boolean onKey(CustomKeyEvent event) {
        if (shouldAnimateTopViews()) {
            switch (event) {
                case LEFT:
                    animateTopViewsIn();
                    break;
                case RIGHT:
                    animateTopViewsOut();
                    break;
            }
        }
        switch (this.mode) {
            case MAP:
                return this.mapContainer.onKey(event);
            case SMART_DASH:
                return this.smartDashContainer.onKey(event);
            default:
                return false;
        }
    }

    public IInputHandler nextHandler() {
        return InputManager.nextContainingHandler(this);
    }

    public Animator getCustomAnimator(CustomAnimationMode customAnimationMode) {
        switch (customAnimationMode) {
            case SHRINK_LEFT:
                return getShrinkLeftAnimator();
            case EXPAND:
                return getExpandAnimator();
            default:
                return null;
        }
    }

    private Animator getShrinkLeftAnimator() {
        boolean isNavigating = isNavigationActive();
        AnimatorSet shrinkAnimatorSet = new AnimatorSet();
        Builder builder = shrinkAnimatorSet.play(ValueAnimator.ofFloat(new float[]{1.0f, 100.0f}));
        this.mapContainer.getCustomAnimator(CustomAnimationMode.SHRINK_LEFT, builder);
        AnimatorSet speed = new AnimatorSet();
        speed.playTogether(new Animator[]{ObjectAnimator.ofFloat(this.mapViewSpeedContainer, View.ALPHA, new float[]{1.0f, 0.0f})});
        speed.addListener(new DefaultAnimationListener() {
            public void onAnimationEnd(Animator animation) {
                HomeScreenView.this.mapViewSpeedContainer.setVisibility(4);
                HomeScreenView.this.mapViewSpeedContainer.setAlpha(1.0f);
            }
        });
        builder.with(speed);
        this.smartDashContainer.getCustomAnimator(CustomAnimationMode.SHRINK_LEFT, builder);
        if (isNavigating) {
            this.openMapRoadInfoContainer.setView(CustomAnimationMode.SHRINK_LEFT);
            if (this.timeContainer.getVisibility() == 0) {
                builder.with(HomeScreenUtils.getXPositionAnimator(this.timeContainer, (float) HomeScreenResourceValues.timeShrinkLeftX));
                this.activeEtaContainer.setView(CustomAnimationMode.SHRINK_LEFT);
            } else {
                this.timeContainer.setView(CustomAnimationMode.SHRINK_LEFT);
                if (this.mode == DisplayMode.MAP) {
                    builder.with(HomeScreenUtils.getXPositionAnimator(this.activeEtaContainer, (float) HomeScreenResourceValues.etaShrinkLeftX));
                } else {
                    this.activeEtaContainer.setView(CustomAnimationMode.SHRINK_LEFT);
                }
            }
            this.tbtView.getCustomAnimator(CustomAnimationMode.SHRINK_LEFT, builder);
            this.laneGuidanceView.getCustomAnimator(CustomAnimationMode.SHRINK_LEFT, builder);
        } else {
            this.tbtView.setView(CustomAnimationMode.SHRINK_LEFT);
            this.activeEtaContainer.setView(CustomAnimationMode.SHRINK_LEFT);
            this.openMapRoadInfoContainer.getCustomAnimator(CustomAnimationMode.SHRINK_LEFT, builder);
            builder.with(HomeScreenUtils.getXPositionAnimator(this.timeContainer, (float) HomeScreenResourceValues.timeShrinkLeftX));
        }
        if (this.isRecalculating) {
            builder.with(HomeScreenUtils.getXPositionAnimator(this.recalcRouteContainer, (float) HomeScreenResourceValues.recalcShrinkLeftX));
        } else {
            this.recalcRouteContainer.setView(CustomAnimationMode.SHRINK_LEFT);
        }
        shrinkAnimatorSet.addListener(new DefaultAnimationListener() {
            public void onAnimationEnd(Animator animation) {
                if (!HomeScreenView.this.isNavigationActive()) {
                    return;
                }
                if (HereNavigationManager.getInstance().hasArrived()) {
                    if (!HomeScreenView.this.isTopViewAnimationOut) {
                        if (HomeScreenView.this.getDisplayMode() == DisplayMode.MAP) {
                            HomeScreenView.this.timeContainer.setVisibility(0);
                        }
                        HomeScreenView.this.timeContainer.setAlpha(1.0f);
                    }
                    HomeScreenView.this.activeEtaContainer.setVisibility(8);
                } else if (HomeScreenView.this.mode == DisplayMode.MAP) {
                    HomeScreenView.this.activeEtaContainer.setVisibility(0);
                    HomeScreenView.this.activeEtaContainer.setAlpha(1.0f);
                    HomeScreenView.this.timeContainer.setVisibility(8);
                }
            }
        });
        animateTopViewsOut();
        return shrinkAnimatorSet;
    }

    private Animator getExpandAnimator() {
        boolean isNavigating = isNavigationActive();
        AnimatorSet expandAnimatorSet = new AnimatorSet();
        Builder builder = expandAnimatorSet.play(ValueAnimator.ofFloat(new float[]{1.0f, 100.0f}));
        if (!this.isTopViewAnimationOut) {
            if (this.timeContainer.getAlpha() == 0.0f) {
                builder.with(ObjectAnimator.ofFloat(this.timeContainer, ALPHA, new float[]{0.0f, 1.0f}));
            }
            if (this.timeContainer.getX() != ((float) HomeScreenResourceValues.timeX)) {
                builder.with(HomeScreenUtils.getXPositionAnimator(this.timeContainer, (float) HomeScreenResourceValues.timeX));
            }
        }
        if (this.mode == DisplayMode.MAP) {
            if (this.activeEtaContainer.getX() != ((float) HomeScreenResourceValues.etaX)) {
                builder.with(HomeScreenUtils.getXPositionAnimator(this.activeEtaContainer, (float) HomeScreenResourceValues.etaX));
            }
            if (this.activeEtaContainer.getAlpha() == 0.0f) {
                builder.with(ObjectAnimator.ofFloat(this.activeEtaContainer, ALPHA, new float[]{0.0f, 1.0f}));
            }
        }
        this.mapContainer.getCustomAnimator(CustomAnimationMode.EXPAND, builder);
        if (this.mode == DisplayMode.MAP) {
            this.mapViewSpeedContainer.setVisibility(0);
        }
        this.smartDashContainer.getCustomAnimator(CustomAnimationMode.EXPAND, builder);
        if (isNavigating) {
            this.openMapRoadInfoContainer.setView(CustomAnimationMode.EXPAND);
            this.tbtView.getCustomAnimator(CustomAnimationMode.EXPAND, builder);
            this.laneGuidanceView.getCustomAnimator(CustomAnimationMode.EXPAND, builder);
        } else {
            this.tbtView.setView(CustomAnimationMode.EXPAND);
            this.openMapRoadInfoContainer.getCustomAnimator(CustomAnimationMode.EXPAND, builder);
        }
        if (this.isRecalculating) {
            builder.with(HomeScreenUtils.getXPositionAnimator(this.recalcRouteContainer, (float) HomeScreenResourceValues.recalcX));
        } else {
            this.recalcRouteContainer.setView(CustomAnimationMode.EXPAND);
        }
        expandAnimatorSet.addListener(new DefaultAnimationListener() {
            public void onAnimationStart(Animator animation) {
                boolean isNavigationActive = HomeScreenView.this.isNavigationActive();
                if (isNavigationActive && !HomeScreenView.this.isRecalculating) {
                    if (HereNavigationManager.getInstance().hasArrived()) {
                        if (HomeScreenView.this.getDisplayMode() == DisplayMode.MAP) {
                            HomeScreenView.this.timeContainer.setVisibility(0);
                            HomeScreenView.this.activeEtaContainer.setVisibility(8);
                        }
                    } else if (HomeScreenView.this.getDisplayMode() == DisplayMode.MAP) {
                        HomeScreenView.this.timeContainer.setVisibility(8);
                        HomeScreenView.this.activeEtaContainer.setVisibility(0);
                    }
                }
                if (!isNavigationActive && HomeScreenView.this.getDisplayMode() == DisplayMode.MAP) {
                    HomeScreenView.this.timeContainer.setVisibility(0);
                    HomeScreenView.this.activeEtaContainer.setVisibility(8);
                }
            }

            public void onAnimationEnd(Animator animation) {
            }
        });
        animateTopViewsIn();
        return expandAnimatorSet;
    }

    public boolean isRecalculating() {
        return this.isRecalculating;
    }

    public void setRecalculating(boolean val) {
        this.isRecalculating = val;
    }

    public boolean isShowCollapsedNotification() {
        return this.showCollapsedNotif;
    }

    public void setShowCollapsedNotification(boolean val) {
        this.showCollapsedNotif = val;
    }

    void animateTopViewsOut() {
        this.handler.removeCallbacks(this.topViewAnimationRunnable);
        if (!this.isTopViewAnimationRunning && !this.isTopViewAnimationOut) {
            Builder builder;
            this.topViewAnimator = new AnimatorSet();
            if (this.timeContainer.getVisibility() == 0) {
                builder = this.topViewAnimator.play(HomeScreenUtils.getXPositionAnimator(this.timeContainer, (float) HomeScreenResourceValues.topViewLeftAnimationOut));
                builder.with(ObjectAnimator.ofFloat(this.timeContainer, View.ALPHA, new float[]{1.0f, 0.0f}));
            } else {
                builder = this.topViewAnimator.play(ValueAnimator.ofInt(new int[]{0, 0}));
            }
            switch (this.mode) {
                case MAP:
                    this.mapContainer.getTopAnimator(builder, true);
                    this.speedView.getTopAnimator(builder, true);
                    break;
                case SMART_DASH:
                    this.smartDashContainer.getTopAnimator(builder, true);
                    break;
            }
            this.isTopViewAnimationRunning = true;
            this.topViewAnimator.addListener(new DefaultAnimationListener() {
                public void onAnimationEnd(Animator animation) {
                    HomeScreenView.this.isTopViewAnimationRunning = false;
                    HomeScreenView.this.isTopViewAnimationOut = true;
                }
            });
            this.topViewAnimator.start();
            sLogger.v("started out animation");
        }
    }

    boolean isTopViewAnimationOut() {
        return this.isTopViewAnimationOut;
    }

    void animateTopViewsIn() {
        if (!this.isTopViewAnimationRunning && shouldAnimateTopViews() && this.isTopViewAnimationOut) {
            Builder builder;
            this.topViewAnimator = new AnimatorSet();
            if (this.timeContainer.getVisibility() == 0) {
                builder = this.topViewAnimator.play(HomeScreenUtils.getXPositionAnimator(this.timeContainer, (float) HomeScreenResourceValues.topViewLeftAnimationIn));
                builder.with(ObjectAnimator.ofFloat(this.timeContainer, View.ALPHA, new float[]{0.0f, 1.0f}));
            } else {
                builder = this.topViewAnimator.play(ValueAnimator.ofInt(new int[]{0, 0}));
            }
            switch (this.mode) {
                case MAP:
                    this.mapContainer.getTopAnimator(builder, false);
                    this.speedView.getTopAnimator(builder, false);
                    break;
                case SMART_DASH:
                    this.smartDashContainer.getTopAnimator(builder, false);
                    break;
            }
            this.isTopViewAnimationRunning = true;
            this.topViewAnimator.addListener(new DefaultAnimationListener() {
                public void onAnimationEnd(Animator animation) {
                    HomeScreenView.this.isTopViewAnimationRunning = false;
                    HomeScreenView.this.isTopViewAnimationOut = false;
                    HomeScreenView.this.handler.removeCallbacks(HomeScreenView.this.topViewAnimationRunnable);
                    HomeScreenView.this.handler.postDelayed(HomeScreenView.this.topViewAnimationRunnable, 10000);
                }
            });
            this.topViewAnimator.start();
            sLogger.v("started in animation");
        }
    }

    private void resetTopViewsAnimator() {
        sLogger.v("resetTopViewsAnimator");
        if (this.topViewAnimator != null && this.topViewAnimator.isRunning()) {
            this.topViewAnimator.removeAllListeners();
            this.topViewAnimator.cancel();
        }
        this.mapContainer.resetTopViewsAnimator();
        this.speedView.resetTopViewsAnimator();
        this.smartDashContainer.resetTopViewsAnimator();
        if (shouldAnimateTopViews()) {
            this.timeContainer.setView(CustomAnimationMode.EXPAND);
            this.timeContainer.setAlpha(1.0f);
            this.activeEtaContainer.setView(CustomAnimationMode.EXPAND);
            this.activeEtaContainer.setAlpha(1.0f);
            this.isTopViewAnimationOut = false;
            this.isTopViewAnimationRunning = false;
            this.handler.removeCallbacks(this.topViewAnimationRunnable);
            this.handler.postDelayed(this.topViewAnimationRunnable, 10000);
            return;
        }
        this.handler.removeCallbacks(this.topViewAnimationRunnable);
        animateTopViewsOut();
    }

    public boolean shouldAnimateTopViews() {
        return this.mode != DisplayMode.SMART_DASH || this.smartDashContainer.shouldShowTopViews();
    }

    public ImageView getLaneGuidanceIconIndicator() {
        return this.laneGuidanceIconIndicator;
    }

    public void onPause() {
        if (!this.paused) {
            this.paused = true;
            this.smartDashContainer.onPause();
            this.mapContainer.onPause();
            pauseNavigationViewContainer();
            sLogger.v("::onPause");
        }
    }

    public void onResume() {
        if (this.paused) {
            this.paused = false;
            switch (this.mode) {
                case MAP:
                    this.mapContainer.onResume();
                    this.laneGuidanceView.onResume();
                    this.smartDashContainer.onPause();
                    break;
                case SMART_DASH:
                    this.mapContainer.onPause();
                    this.laneGuidanceView.onPause();
                    this.smartDashContainer.onResume();
                    break;
            }
            resumeNavigationViewContainer();
            sLogger.v("::onResume");
        }
    }

    private void pauseNavigationViewContainer() {
        sLogger.v("::onPause:pauseNavigationViewContainer");
        this.tbtView.onPause();
        this.recalcRouteContainer.onPause();
    }

    private void resumeNavigationViewContainer() {
        sLogger.v("::onResume:resumeNavigationViewContainer");
        this.tbtView.onResume();
        this.recalcRouteContainer.onResume();
    }

    private void setViewsBackground(int color) {
        sLogger.v("setViewsBackground:" + color);
        this.tbtView.setBackgroundColor(color);
        this.recalcRouteContainer.setBackgroundColor(color);
    }

    public void injectRightSection(View view) {
        sLogger.v("injectRightSection");
        this.mainscreenRightSection.injectRightSection(view);
        clearRightSectionAnimation();
        Main mainScreen = this.uiStateManager.getRootScreen();
        if (this.mainscreenRightSection.isViewVisible()) {
            sLogger.v("injectRightSection: right section view already visible");
        } else if (mainScreen.isNotificationExpanding() || mainScreen.isNotificationViewShowing()) {
            sLogger.v("injectRightSection: cannot animate");
        } else {
            animateInModeView();
        }
    }

    public void ejectRightSection() {
        sLogger.v("ejectRightSection");
        clearRightSectionAnimation();
        if (this.mainscreenRightSection.isViewVisible()) {
            Main mainScreen = this.uiStateManager.getRootScreen();
            if (mainScreen.isNotificationExpanding() || mainScreen.isNotificationViewShowing()) {
                sLogger.v("ejectRightSection: cannot animate");
                this.mainscreenRightSection.ejectRightSection();
                return;
            }
            animateOutModeView(true);
            return;
        }
        this.mainscreenRightSection.ejectRightSection();
    }

    private void clearRightSectionAnimation() {
        if (this.rightScreenAnimator != null && this.rightScreenAnimator.isRunning()) {
            this.rightScreenAnimator.removeAllListeners();
            this.rightScreenAnimator.cancel();
            sLogger.v("stopped rightscreen animation");
        }
        this.rightScreenAnimator = null;
    }

    public boolean hasModeView() {
        return this.mainscreenRightSection.hasView();
    }

    public boolean isModeVisible() {
        return this.mainscreenRightSection.isViewVisible();
    }

    public void animateInModeView() {
        if (hasModeView() && !isModeVisible()) {
            sLogger.v("mode-view animateInModeView");
            this.rightScreenAnimator = new AnimatorSet();
            Builder builder = this.rightScreenAnimator.play(ValueAnimator.ofFloat(new float[]{0.0f, 0.0f}));
            this.mapContainer.getCustomAnimator(CustomAnimationMode.SHRINK_MODE, builder);
            this.smartDashContainer.getCustomAnimator(CustomAnimationMode.SHRINK_MODE, builder);
            this.mainscreenRightSection.getCustomAnimator(CustomAnimationMode.SHRINK_MODE, builder);
            this.rightScreenAnimator.start();
        }
    }

    public void animateOutModeView(boolean removeView) {
        if (isModeVisible()) {
            sLogger.v("mode-view animateOutModeView:" + removeView);
            this.rightScreenAnimator = new AnimatorSet();
            Builder builder = this.rightScreenAnimator.play(ValueAnimator.ofFloat(new float[]{0.0f, 0.0f}));
            this.mapContainer.getCustomAnimator(CustomAnimationMode.EXPAND, builder);
            this.smartDashContainer.getCustomAnimator(CustomAnimationMode.EXPAND, builder);
            this.mainscreenRightSection.getCustomAnimator(CustomAnimationMode.EXPAND, builder);
            if (removeView) {
                this.rightScreenAnimator.addListener(new DefaultAnimationListener() {
                    public void onAnimationEnd(Animator animation) {
                        HomeScreenView.this.mainscreenRightSection.ejectRightSection();
                    }
                });
            }
            this.rightScreenAnimator.start();
        }
    }

    public void resetModeView() {
        if (isModeVisible()) {
            sLogger.v("mode-view resetModeView");
            this.mapContainer.setView(CustomAnimationMode.EXPAND);
            this.smartDashContainer.setView(CustomAnimationMode.EXPAND);
            this.mainscreenRightSection.setView(CustomAnimationMode.EXPAND);
        }
    }

    private void setViewSpeed(CustomAnimationMode mode) {
        switch (mode) {
            case SHRINK_LEFT:
                this.mapViewSpeedContainer.setX((float) HomeScreenResourceValues.speedShrinkLeftX);
                return;
            case EXPAND:
                this.mapViewSpeedContainer.setX((float) HomeScreenResourceValues.speedX);
                return;
            default:
                return;
        }
    }
}
