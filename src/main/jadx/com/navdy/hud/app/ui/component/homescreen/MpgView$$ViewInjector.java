package com.navdy.hud.app.ui.component.homescreen;

import android.widget.TextView;
import butterknife.ButterKnife.Finder;
import com.navdy.hud.app.R;

public class MpgView$$ViewInjector {
    public static void inject(Finder finder, MpgView target, Object source) {
        target.mpgTextView = (TextView) finder.findRequiredView(source, R.id.txt_mpg, "field 'mpgTextView'");
        target.mpgLabelTextView = (TextView) finder.findRequiredView(source, R.id.txt_mpg_label, "field 'mpgLabelTextView'");
    }

    public static void reset(MpgView target) {
        target.mpgTextView = null;
        target.mpgLabelTextView = null;
    }
}
