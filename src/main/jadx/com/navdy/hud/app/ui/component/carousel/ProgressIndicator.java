package com.navdy.hud.app.ui.component.carousel;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;
import com.navdy.hud.app.R;
import com.navdy.hud.app.ui.component.carousel.CarouselIndicator.Orientation;
import com.navdy.hud.app.ui.framework.DefaultAnimationListener;

public class ProgressIndicator extends View implements IProgressIndicator {
    private boolean animating;
    private int animatingColor;
    private float animatingPos;
    private int backgroundColor;
    private int barParentSize = -1;
    private int barSize = -1;
    private int blackColor;
    private int currentItem = -1;
    private int currentItemColor = -1;
    private int currentItemPaddingRadius;
    private int defaultColor;
    private boolean fullBackground;
    private int greyColor;
    private int itemCount;
    private int itemPadding;
    private int itemRadius;
    private Orientation orientation;
    private Paint paint;
    private int roundRadius;
    private int viewPadding;

    public ProgressIndicator(Context context) {
        super(context);
        init(context);
    }

    public ProgressIndicator(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        if (this.barSize != -1) {
            int width = getMeasuredWidth();
            int height = getMeasuredHeight();
            if (this.orientation == Orientation.VERTICAL) {
                width = this.barParentSize;
            } else {
                height = this.barParentSize;
            }
            setMeasuredDimension(width, height);
        }
    }

    private void init(Context context) {
        Resources resources = getResources();
        this.blackColor = resources.getColor(17170444);
        this.greyColor = resources.getColor(R.color.grey_4a);
        this.paint = new Paint();
        this.paint.setStrokeWidth(0.0f);
        this.paint.setAntiAlias(true);
    }

    public void setProperties(int roundRadius, int itemRadius, int itemPadding, int currentItemPaddingRadius, int defaultColor, int backgroundColor, boolean fullBackground, int viewPadding, int barSize, int barParentSize) {
        this.roundRadius = roundRadius;
        this.itemRadius = itemRadius;
        this.itemPadding = itemPadding;
        this.viewPadding = viewPadding;
        this.currentItemPaddingRadius = currentItemPaddingRadius;
        this.defaultColor = defaultColor;
        this.barSize = barSize;
        this.barParentSize = barParentSize;
        if (backgroundColor != -1) {
            this.backgroundColor = backgroundColor;
        } else {
            this.backgroundColor = this.greyColor;
        }
        this.fullBackground = fullBackground;
        if (this.fullBackground && viewPadding > 0) {
            this.viewPadding += itemRadius;
        }
    }

    public void setOrientation(Orientation orientation) {
        this.orientation = orientation;
    }

    public void setItemCount(int n) {
        if (n <= 0) {
            throw new IllegalArgumentException();
        }
        this.currentItem = -1;
        this.itemCount = n;
    }

    public int getCurrentItem() {
        return this.currentItem;
    }

    public void setCurrentItem(int n) {
        setCurrentItem(n, -1);
    }

    public void setCurrentItem(int n, int color) {
        if (n >= 0 && n <= this.itemCount - 1) {
            this.currentItem = n;
            this.currentItemColor = color;
            this.animating = false;
            this.animatingPos = -1.0f;
            this.animatingColor = -1;
            invalidate();
        }
    }

    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        float width = (float) getWidth();
        float height = (float) getHeight();
        int left = 0;
        int top = 0;
        float barWidth = width;
        float barHeight = height;
        if (this.barSize != -1) {
            if (this.orientation == Orientation.VERTICAL) {
                left = ((int) (width - ((float) this.barSize))) / 2;
                barWidth = (float) this.barSize;
            } else {
                top = ((int) (width - ((float) this.barSize))) / 2;
                barHeight = (float) this.barSize;
            }
        }
        this.paint.setColor(this.backgroundColor);
        if (this.itemCount == 0) {
            canvas.drawRoundRect(new RectF((float) left, (float) top, ((float) left) + barWidth, ((float) top) + barHeight), (float) this.roundRadius, (float) this.roundRadius, this.paint);
            return;
        }
        if (this.itemCount == 1) {
            canvas.drawRoundRect(new RectF((float) left, (float) top, ((float) left) + barWidth, ((float) top) + barHeight), (float) this.roundRadius, (float) this.roundRadius, this.paint);
        } else if (this.fullBackground) {
            canvas.drawRoundRect(new RectF((float) left, (float) top, ((float) left) + barWidth, ((float) top) + barHeight), (float) this.roundRadius, (float) this.roundRadius, this.paint);
        } else {
            float x;
            float y;
            RectF rectF;
            if (this.orientation == Orientation.HORIZONTAL) {
                x = (float) this.itemRadius;
                y = height / 2.0f;
                rectF = new RectF((float) ((this.itemRadius * 2) + this.itemPadding), 0.0f, width, height);
            } else {
                x = width / 2.0f;
                y = (float) this.itemRadius;
                rectF = new RectF(0.0f, (float) ((this.itemRadius * 2) + this.itemPadding), width, height);
            }
            canvas.drawCircle(x, y, (float) this.itemRadius, this.paint);
            canvas.drawRoundRect(rectF, (float) this.roundRadius, (float) this.roundRadius, this.paint);
        }
        int padding = this.currentItemPaddingRadius + this.itemRadius;
        if (this.animating) {
            drawItem(canvas, width, height, this.animatingPos, padding, this.animatingColor);
        } else if (this.currentItem != -1) {
            float f;
            if (this.orientation == Orientation.HORIZONTAL) {
                f = width;
            } else {
                f = height;
            }
            float targetPos = getItemTargetPos(f, this.currentItem);
            if (this.fullBackground && this.viewPadding > 0) {
                if (this.currentItem == 0) {
                    targetPos = (float) this.viewPadding;
                } else if (this.currentItem == this.itemCount - 1) {
                    targetPos = (float) (getHeight() - this.viewPadding);
                } else {
                    int lastItem = getHeight() - this.viewPadding;
                    int newPos = ((int) targetPos) + this.viewPadding;
                    targetPos = newPos <= lastItem ? (float) newPos : (float) lastItem;
                }
            }
            int color = this.defaultColor;
            if (this.currentItemColor != -1) {
                color = this.currentItemColor;
            }
            drawItem(canvas, width, height, targetPos, padding, color);
        }
    }

    private float getItemTargetPos(float dimension, int itemPos) {
        if (this.fullBackground && this.viewPadding > 0) {
            dimension -= (float) (this.viewPadding * 2);
        }
        if (this.itemCount <= 0) {
            return 0.0f;
        }
        if (this.itemCount == 1) {
            return dimension - ((float) (this.itemRadius + this.currentItemPaddingRadius));
        }
        float targetPos;
        float minPos = (float) (this.itemRadius + this.currentItemPaddingRadius);
        float maxPos = dimension - ((float) (this.itemRadius + this.currentItemPaddingRadius));
        if (itemPos == 0) {
            targetPos = (float) (this.itemRadius + this.currentItemPaddingRadius);
        } else if (itemPos == this.itemCount - 1) {
            targetPos = dimension - ((float) (this.itemRadius + this.currentItemPaddingRadius));
        } else {
            targetPos = ((float) itemPos) * (dimension / ((float) (this.itemCount - 1)));
        }
        if (targetPos < minPos) {
            return minPos;
        }
        if (targetPos > maxPos) {
            return maxPos;
        }
        return targetPos;
    }

    public AnimatorSet getItemMoveAnimator(int toPos, int color) {
        float f;
        float width = (float) getWidth();
        float height = (float) getHeight();
        if (this.orientation == Orientation.HORIZONTAL) {
            f = width;
        } else {
            f = height;
        }
        float currentPos = getItemTargetPos(f, this.currentItem);
        if (this.orientation == Orientation.HORIZONTAL) {
            f = width;
        } else {
            f = height;
        }
        float targetPos = getItemTargetPos(f, toPos);
        if (this.fullBackground) {
            if (this.orientation != Orientation.HORIZONTAL) {
                width = height;
            }
            int lastItem = (int) getItemTargetPos(width, this.itemCount - 1);
            int newPos = ((int) targetPos) + this.viewPadding;
            if (newPos < lastItem - this.viewPadding) {
                targetPos = (float) newPos;
            } else {
                targetPos = (float) (lastItem - this.viewPadding);
            }
        }
        ValueAnimator animator = ValueAnimator.ofFloat(new float[]{currentPos, targetPos});
        animator.addUpdateListener(new AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator animation) {
                ProgressIndicator.this.animatingPos = ((Float) animation.getAnimatedValue()).floatValue();
                ProgressIndicator.this.invalidate();
            }
        });
        animator.addListener(new DefaultAnimationListener() {
            public void onAnimationEnd(Animator animation) {
            }
        });
        AnimatorSet set = new AnimatorSet();
        set.play(animator);
        this.animatingPos = currentPos;
        this.animatingColor = color;
        this.animating = true;
        return set;
    }

    public RectF getItemPos(int n) {
        if (n < 0 || n >= this.itemCount) {
            return null;
        }
        float x;
        float y;
        float width = (float) getWidth();
        float height = (float) getHeight();
        if (this.orientation != Orientation.HORIZONTAL) {
            width = height;
        }
        float targetPos = getItemTargetPos(width, this.currentItem);
        if (this.orientation == Orientation.HORIZONTAL) {
            x = targetPos;
            y = 0.0f;
        } else {
            x = 0.0f;
            y = targetPos;
        }
        return new RectF(x, y, 0.0f, 0.0f);
    }

    private void drawItem(Canvas canvas, float width, float height, float targetPos, int padding, int color) {
        this.paint.setColor(this.blackColor);
        if (this.orientation == Orientation.HORIZONTAL) {
            canvas.drawCircle(targetPos, height / 2.0f, (float) padding, this.paint);
        } else {
            canvas.drawCircle(width / 2.0f, targetPos, (float) padding, this.paint);
        }
        this.paint.setColor(color);
        if (this.orientation == Orientation.HORIZONTAL) {
            canvas.drawCircle(targetPos, height / 2.0f, (float) this.itemRadius, this.paint);
        } else {
            canvas.drawCircle(width / 2.0f, targetPos, (float) this.itemRadius, this.paint);
        }
    }

    public void setBackgroundColor(int color) {
        this.backgroundColor = color;
    }
}
