package com.navdy.hud.app.util;

import android.text.TextUtils;
import com.glympse.android.lib.HttpJob;
import com.glympse.android.lib.StaticConfig;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.PhoneNumberUtil.PhoneNumberFormat;
import com.google.i18n.phonenumbers.Phonenumber.PhoneNumber;
import com.here.posclient.analytics.TrackerEvent;
import com.navdy.hud.app.bluetooth.obex.ResponseCodes;
import com.navdy.hud.app.framework.DriverProfileHelper;
import com.navdy.hud.app.ui.component.vlist.VerticalList;
import com.navdy.obd.Pids;
import com.navdy.service.library.log.Logger;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class PhoneUtil {
    private static final Map<String, Integer> sISOCountryCodeMap = new HashMap(310);
    private static final Logger sLogger = new Logger(PhoneUtil.class);
    private static final PhoneNumberUtil sPhoneNumberUtil = PhoneNumberUtil.getInstance();

    static {
        sISOCountryCodeMap.put("US", Integer.valueOf(1));
        sISOCountryCodeMap.put("RU", Integer.valueOf(7));
        sISOCountryCodeMap.put("KZ", Integer.valueOf(7));
        sISOCountryCodeMap.put("EG", Integer.valueOf(20));
        sISOCountryCodeMap.put("ZA", Integer.valueOf(27));
        sISOCountryCodeMap.put("GR", Integer.valueOf(30));
        sISOCountryCodeMap.put("NL", Integer.valueOf(31));
        sISOCountryCodeMap.put("BE", Integer.valueOf(32));
        sISOCountryCodeMap.put("FR", Integer.valueOf(33));
        sISOCountryCodeMap.put("ES", Integer.valueOf(34));
        sISOCountryCodeMap.put("HU", Integer.valueOf(36));
        sISOCountryCodeMap.put("IT", Integer.valueOf(39));
        sISOCountryCodeMap.put("VA", Integer.valueOf(39));
        sISOCountryCodeMap.put("RO", Integer.valueOf(40));
        sISOCountryCodeMap.put("CH", Integer.valueOf(41));
        sISOCountryCodeMap.put("AT", Integer.valueOf(43));
        sISOCountryCodeMap.put("GB", Integer.valueOf(44));
        sISOCountryCodeMap.put("GG", Integer.valueOf(44));
        sISOCountryCodeMap.put("IM", Integer.valueOf(44));
        sISOCountryCodeMap.put("JE", Integer.valueOf(44));
        sISOCountryCodeMap.put("DK", Integer.valueOf(45));
        sISOCountryCodeMap.put("SE", Integer.valueOf(46));
        sISOCountryCodeMap.put("NO", Integer.valueOf(47));
        sISOCountryCodeMap.put("SJ", Integer.valueOf(47));
        sISOCountryCodeMap.put("PL", Integer.valueOf(48));
        sISOCountryCodeMap.put("DE", Integer.valueOf(49));
        sISOCountryCodeMap.put("PE", Integer.valueOf(51));
        sISOCountryCodeMap.put("MX", Integer.valueOf(52));
        sISOCountryCodeMap.put("CU", Integer.valueOf(53));
        sISOCountryCodeMap.put("AR", Integer.valueOf(54));
        sISOCountryCodeMap.put("BR", Integer.valueOf(55));
        sISOCountryCodeMap.put("CL", Integer.valueOf(56));
        sISOCountryCodeMap.put("CO", Integer.valueOf(57));
        sISOCountryCodeMap.put("VE", Integer.valueOf(58));
        sISOCountryCodeMap.put("MY", Integer.valueOf(60));
        sISOCountryCodeMap.put("AU", Integer.valueOf(61));
        sISOCountryCodeMap.put("CC", Integer.valueOf(61));
        sISOCountryCodeMap.put("CX", Integer.valueOf(61));
        sISOCountryCodeMap.put("ID", Integer.valueOf(62));
        sISOCountryCodeMap.put("PH", Integer.valueOf(63));
        sISOCountryCodeMap.put("NZ", Integer.valueOf(64));
        sISOCountryCodeMap.put("SG", Integer.valueOf(65));
        sISOCountryCodeMap.put("TH", Integer.valueOf(66));
        sISOCountryCodeMap.put("JP", Integer.valueOf(81));
        sISOCountryCodeMap.put("KR", Integer.valueOf(82));
        sISOCountryCodeMap.put("VN", Integer.valueOf(84));
        sISOCountryCodeMap.put("CN", Integer.valueOf(86));
        sISOCountryCodeMap.put("TR", Integer.valueOf(90));
        sISOCountryCodeMap.put("IN", Integer.valueOf(91));
        sISOCountryCodeMap.put("IN", Integer.valueOf(91));
        sISOCountryCodeMap.put("IN", Integer.valueOf(91));
        sISOCountryCodeMap.put("IN", Integer.valueOf(91));
        sISOCountryCodeMap.put("PK", Integer.valueOf(92));
        sISOCountryCodeMap.put("AF", Integer.valueOf(93));
        sISOCountryCodeMap.put("LK", Integer.valueOf(94));
        sISOCountryCodeMap.put("MM", Integer.valueOf(95));
        sISOCountryCodeMap.put("IR", Integer.valueOf(98));
        sISOCountryCodeMap.put("SS", Integer.valueOf(211));
        sISOCountryCodeMap.put("MA", Integer.valueOf(212));
        sISOCountryCodeMap.put("EH", Integer.valueOf(212));
        sISOCountryCodeMap.put("DZ", Integer.valueOf(213));
        sISOCountryCodeMap.put("TN", Integer.valueOf(216));
        sISOCountryCodeMap.put("LY", Integer.valueOf(218));
        sISOCountryCodeMap.put("GM", Integer.valueOf(220));
        sISOCountryCodeMap.put("SN", Integer.valueOf(TrackerEvent.RadioMapManualOutdoor));
        sISOCountryCodeMap.put("MR", Integer.valueOf(TrackerEvent.RadioMapManualCommonIndoor));
        sISOCountryCodeMap.put("ML", Integer.valueOf(TrackerEvent.RadioMapManualPrivateIndoor));
        sISOCountryCodeMap.put("GN", Integer.valueOf(ResponseCodes.OBEX_DATABASE_FULL));
        sISOCountryCodeMap.put("CI", Integer.valueOf(ResponseCodes.OBEX_DATABASE_LOCKED));
        sISOCountryCodeMap.put("BF", Integer.valueOf(226));
        sISOCountryCodeMap.put("NE", Integer.valueOf(227));
        sISOCountryCodeMap.put("TG", Integer.valueOf(228));
        sISOCountryCodeMap.put("BJ", Integer.valueOf(229));
        sISOCountryCodeMap.put("MU", Integer.valueOf(VerticalList.ITEM_MOVE_ANIMATION_DURATION));
        sISOCountryCodeMap.put("LR", Integer.valueOf(231));
        sISOCountryCodeMap.put("SL", Integer.valueOf(232));
        sISOCountryCodeMap.put("GH", Integer.valueOf(233));
        sISOCountryCodeMap.put("NG", Integer.valueOf(234));
        sISOCountryCodeMap.put("TD", Integer.valueOf(235));
        sISOCountryCodeMap.put("CF", Integer.valueOf(236));
        sISOCountryCodeMap.put("CM", Integer.valueOf(237));
        sISOCountryCodeMap.put("CV", Integer.valueOf(238));
        sISOCountryCodeMap.put("ST", Integer.valueOf(239));
        sISOCountryCodeMap.put("ST", Integer.valueOf(239));
        sISOCountryCodeMap.put("GQ", Integer.valueOf(StaticConfig.PLACE_SEARCH_DISTANCE_FILTER));
        sISOCountryCodeMap.put("GA", Integer.valueOf(241));
        sISOCountryCodeMap.put("CG", Integer.valueOf(242));
        sISOCountryCodeMap.put("CD", Integer.valueOf(243));
        sISOCountryCodeMap.put("AO", Integer.valueOf(244));
        sISOCountryCodeMap.put("GW", Integer.valueOf(245));
        sISOCountryCodeMap.put("IO", Integer.valueOf(246));
        sISOCountryCodeMap.put("AC", Integer.valueOf(247));
        sISOCountryCodeMap.put("SC", Integer.valueOf(248));
        sISOCountryCodeMap.put("SD", Integer.valueOf(249));
        sISOCountryCodeMap.put("RW", Integer.valueOf(250));
        sISOCountryCodeMap.put("ET", Integer.valueOf(251));
        sISOCountryCodeMap.put("SO", Integer.valueOf(252));
        sISOCountryCodeMap.put("DJ", Integer.valueOf(253));
        sISOCountryCodeMap.put("KE", Integer.valueOf(254));
        sISOCountryCodeMap.put("TZ", Integer.valueOf(255));
        sISOCountryCodeMap.put("UG", Integer.valueOf(256));
        sISOCountryCodeMap.put("BI", Integer.valueOf(257));
        sISOCountryCodeMap.put("MZ", Integer.valueOf(Pids.ENGINE_OIL_PRESSURE));
        sISOCountryCodeMap.put("ZM", Integer.valueOf(Pids.TOTAL_VEHICLE_DISTANCE));
        sISOCountryCodeMap.put("MG", Integer.valueOf(261));
        sISOCountryCodeMap.put("KE", Integer.valueOf(254));
        sISOCountryCodeMap.put("YT", Integer.valueOf(262));
        sISOCountryCodeMap.put("KE", Integer.valueOf(262));
        sISOCountryCodeMap.put("ZW", Integer.valueOf(263));
        sISOCountryCodeMap.put("NA", Integer.valueOf(264));
        sISOCountryCodeMap.put("MW", Integer.valueOf(265));
        sISOCountryCodeMap.put("LS", Integer.valueOf(266));
        sISOCountryCodeMap.put("BW", Integer.valueOf(267));
        sISOCountryCodeMap.put("SZ", Integer.valueOf(268));
        sISOCountryCodeMap.put("KM", Integer.valueOf(269));
        sISOCountryCodeMap.put("SH", Integer.valueOf(290));
        sISOCountryCodeMap.put("TA", Integer.valueOf(290));
        sISOCountryCodeMap.put("ER", Integer.valueOf(291));
        sISOCountryCodeMap.put("AW", Integer.valueOf(297));
        sISOCountryCodeMap.put("FO", Integer.valueOf(298));
        sISOCountryCodeMap.put("GL", Integer.valueOf(299));
        sISOCountryCodeMap.put("GI", Integer.valueOf(350));
        sISOCountryCodeMap.put("PT", Integer.valueOf(351));
        sISOCountryCodeMap.put("LU", Integer.valueOf(352));
        sISOCountryCodeMap.put("IE", Integer.valueOf(353));
        sISOCountryCodeMap.put("IS", Integer.valueOf(354));
        sISOCountryCodeMap.put("AL", Integer.valueOf(355));
        sISOCountryCodeMap.put("MT", Integer.valueOf(356));
        sISOCountryCodeMap.put("CY", Integer.valueOf(357));
        sISOCountryCodeMap.put("FI", Integer.valueOf(358));
        sISOCountryCodeMap.put("AX", Integer.valueOf(358));
        sISOCountryCodeMap.put("BG", Integer.valueOf(359));
        sISOCountryCodeMap.put("LT", Integer.valueOf(370));
        sISOCountryCodeMap.put("LV", Integer.valueOf(371));
        sISOCountryCodeMap.put("EE", Integer.valueOf(372));
        sISOCountryCodeMap.put("MD", Integer.valueOf(373));
        sISOCountryCodeMap.put("AM", Integer.valueOf(374));
        sISOCountryCodeMap.put("BY", Integer.valueOf(375));
        sISOCountryCodeMap.put("AD", Integer.valueOf(376));
        sISOCountryCodeMap.put("MC", Integer.valueOf(377));
        sISOCountryCodeMap.put("SM", Integer.valueOf(378));
        sISOCountryCodeMap.put("UA", Integer.valueOf(380));
        sISOCountryCodeMap.put("RS", Integer.valueOf(381));
        sISOCountryCodeMap.put("ME", Integer.valueOf(382));
        sISOCountryCodeMap.put("HR", Integer.valueOf(385));
        sISOCountryCodeMap.put("SI", Integer.valueOf(386));
        sISOCountryCodeMap.put("BA", Integer.valueOf(387));
        sISOCountryCodeMap.put("MK", Integer.valueOf(389));
        sISOCountryCodeMap.put("CZ", Integer.valueOf(420));
        sISOCountryCodeMap.put("SK", Integer.valueOf(421));
        sISOCountryCodeMap.put("LI", Integer.valueOf(423));
        sISOCountryCodeMap.put("FK", Integer.valueOf(500));
        sISOCountryCodeMap.put("BZ", Integer.valueOf(501));
        sISOCountryCodeMap.put("GT", Integer.valueOf(502));
        sISOCountryCodeMap.put("SV", Integer.valueOf(503));
        sISOCountryCodeMap.put("HN", Integer.valueOf(504));
        sISOCountryCodeMap.put("NI", Integer.valueOf(505));
        sISOCountryCodeMap.put("CR", Integer.valueOf(506));
        sISOCountryCodeMap.put("PA", Integer.valueOf(507));
        sISOCountryCodeMap.put("PM", Integer.valueOf(508));
        sISOCountryCodeMap.put("HT", Integer.valueOf(509));
        sISOCountryCodeMap.put("GP", Integer.valueOf(590));
        sISOCountryCodeMap.put("BL", Integer.valueOf(590));
        sISOCountryCodeMap.put("MF", Integer.valueOf(590));
        sISOCountryCodeMap.put("BO", Integer.valueOf(591));
        sISOCountryCodeMap.put("GY", Integer.valueOf(592));
        sISOCountryCodeMap.put("EC", Integer.valueOf(593));
        sISOCountryCodeMap.put("GF", Integer.valueOf(594));
        sISOCountryCodeMap.put("PY", Integer.valueOf(595));
        sISOCountryCodeMap.put("MQ", Integer.valueOf(596));
        sISOCountryCodeMap.put("SR", Integer.valueOf(597));
        sISOCountryCodeMap.put("UY", Integer.valueOf(598));
        sISOCountryCodeMap.put("CW", Integer.valueOf(HttpJob.RESPONSE_CODE_MAX_RETRY_UPPER_BOUND));
        sISOCountryCodeMap.put("BQ", Integer.valueOf(HttpJob.RESPONSE_CODE_MAX_RETRY_UPPER_BOUND));
        sISOCountryCodeMap.put("TL", Integer.valueOf(670));
        sISOCountryCodeMap.put("NF", Integer.valueOf(672));
        sISOCountryCodeMap.put("BN", Integer.valueOf(673));
        sISOCountryCodeMap.put("NR", Integer.valueOf(674));
        sISOCountryCodeMap.put("PG", Integer.valueOf(675));
        sISOCountryCodeMap.put("TO", Integer.valueOf(676));
        sISOCountryCodeMap.put("SB", Integer.valueOf(677));
        sISOCountryCodeMap.put("VU", Integer.valueOf(678));
        sISOCountryCodeMap.put("FJ", Integer.valueOf(679));
        sISOCountryCodeMap.put("PW", Integer.valueOf(680));
        sISOCountryCodeMap.put("WF", Integer.valueOf(681));
        sISOCountryCodeMap.put("CK", Integer.valueOf(682));
        sISOCountryCodeMap.put("NU", Integer.valueOf(683));
        sISOCountryCodeMap.put("WS", Integer.valueOf(685));
        sISOCountryCodeMap.put("KI", Integer.valueOf(686));
        sISOCountryCodeMap.put("NC", Integer.valueOf(687));
        sISOCountryCodeMap.put("TV", Integer.valueOf(688));
        sISOCountryCodeMap.put("PF", Integer.valueOf(689));
        sISOCountryCodeMap.put("TK", Integer.valueOf(690));
        sISOCountryCodeMap.put("FM", Integer.valueOf(691));
        sISOCountryCodeMap.put("MH", Integer.valueOf(692));
        sISOCountryCodeMap.put("KP", Integer.valueOf(850));
        sISOCountryCodeMap.put("HK", Integer.valueOf(852));
        sISOCountryCodeMap.put("MO", Integer.valueOf(853));
        sISOCountryCodeMap.put("KH", Integer.valueOf(855));
        sISOCountryCodeMap.put("LA", Integer.valueOf(856));
        sISOCountryCodeMap.put("BD", Integer.valueOf(880));
        sISOCountryCodeMap.put("TW", Integer.valueOf(886));
        sISOCountryCodeMap.put("MV", Integer.valueOf(960));
        sISOCountryCodeMap.put("LB", Integer.valueOf(961));
        sISOCountryCodeMap.put("JO", Integer.valueOf(962));
        sISOCountryCodeMap.put("SY", Integer.valueOf(963));
        sISOCountryCodeMap.put("IQ", Integer.valueOf(964));
        sISOCountryCodeMap.put("KW", Integer.valueOf(965));
        sISOCountryCodeMap.put("SA", Integer.valueOf(966));
        sISOCountryCodeMap.put("YE", Integer.valueOf(967));
        sISOCountryCodeMap.put("OM", Integer.valueOf(968));
        sISOCountryCodeMap.put("PS", Integer.valueOf(970));
        sISOCountryCodeMap.put("AE", Integer.valueOf(971));
        sISOCountryCodeMap.put("IL", Integer.valueOf(972));
        sISOCountryCodeMap.put("BH", Integer.valueOf(973));
        sISOCountryCodeMap.put("QA", Integer.valueOf(974));
        sISOCountryCodeMap.put("BT", Integer.valueOf(975));
        sISOCountryCodeMap.put("MN", Integer.valueOf(976));
        sISOCountryCodeMap.put("NP", Integer.valueOf(977));
        sISOCountryCodeMap.put("TJ", Integer.valueOf(992));
        sISOCountryCodeMap.put("TM", Integer.valueOf(993));
        sISOCountryCodeMap.put("AZ", Integer.valueOf(994));
        sISOCountryCodeMap.put("GE", Integer.valueOf(995));
        sISOCountryCodeMap.put("KG", Integer.valueOf(996));
        sISOCountryCodeMap.put("UZ", Integer.valueOf(998));
    }

    public static synchronized String formatPhoneNumber(String phoneNumber) {
        synchronized (PhoneUtil.class) {
            try {
                if (TextUtils.isEmpty(phoneNumber)) {
                    phoneNumber = "";
                } else {
                    Locale currentLocale = DriverProfileHelper.getInstance().getCurrentLocale();
                    int countryPhoneCode = getCountryPhoneCode(currentLocale.getCountry());
                    PhoneNumber number = sPhoneNumberUtil.parse(phoneNumber, currentLocale.getCountry());
                    boolean local = true;
                    boolean countryCodeMatch = false;
                    if (countryPhoneCode > 0 && number.hasCountryCode()) {
                        int cc = number.getCountryCode();
                        if (cc == countryPhoneCode) {
                            countryCodeMatch = true;
                        } else if (phoneNumber.startsWith(String.valueOf(cc)) || phoneNumber.startsWith("+")) {
                            local = false;
                        }
                    }
                    if (local && !countryCodeMatch && phoneNumber.startsWith("+")) {
                        local = false;
                    }
                    if (local) {
                        phoneNumber = sPhoneNumberUtil.format(number, PhoneNumberFormat.NATIONAL);
                    } else {
                        phoneNumber = sPhoneNumberUtil.format(number, PhoneNumberFormat.INTERNATIONAL);
                    }
                }
            } catch (Throwable t) {
                if (sLogger.isLoggable(2)) {
                    sLogger.e(t);
                }
            }
        }
        return phoneNumber;
    }

    private static int getCountryPhoneCode(String isoCountryCode) {
        if (TextUtils.isEmpty(isoCountryCode)) {
            return -1;
        }
        Integer code = (Integer) sISOCountryCodeMap.get(isoCountryCode);
        if (code != null) {
            return code.intValue();
        }
        return -1;
    }

    public static String normalizeNumber(String number) {
        return TextUtils.isEmpty(number) ? number : number.replaceAll("[^0-9]+", "");
    }

    public static String convertToE164Format(String number) {
        try {
            return sPhoneNumberUtil.format(sPhoneNumberUtil.parse(number, DriverProfileHelper.getInstance().getCurrentLocale().getCountry()), PhoneNumberFormat.E164);
        } catch (Throwable t) {
            sLogger.e(t);
            return number;
        }
    }
}
