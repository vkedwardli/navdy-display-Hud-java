package com.navdy.hud.app.util;

import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.service.library.events.Capabilities;
import com.navdy.service.library.events.DeviceInfo;
import com.navdy.service.library.events.LegacyCapability;

public class RemoteCapabilitiesUtil {
    public static boolean supportsVoiceSearch() {
        Capabilities capabilities = getRemoteCapabilities();
        if (capabilities != null) {
            return Boolean.TRUE.equals(capabilities.voiceSearch);
        }
        return false;
    }

    public static boolean supportsPlaceSearch() {
        Capabilities capabilities = getRemoteCapabilities();
        if (capabilities != null) {
            return Boolean.TRUE.equals(capabilities.placeTypeSearch);
        }
        return RemoteDeviceManager.getInstance().doesRemoteDeviceHasCapability(LegacyCapability.CAPABILITY_PLACE_TYPE_SEARCH);
    }

    private static Capabilities getRemoteCapabilities() {
        DeviceInfo deviceInfo = RemoteDeviceManager.getInstance().getRemoteDeviceInfo();
        return deviceInfo != null ? deviceInfo.capabilities : null;
    }

    public static boolean supportsNavigationCoordinateLookup() {
        Capabilities capabilities = getRemoteCapabilities();
        return capabilities != null && Boolean.TRUE.equals(capabilities.navCoordsLookup);
    }

    public static boolean supportsVoiceSearchNewIOSPauseBehaviors() {
        Capabilities capabilities = getRemoteCapabilities();
        return capabilities != null && Boolean.TRUE.equals(capabilities.voiceSearchNewIOSPauseBehaviors);
    }
}
