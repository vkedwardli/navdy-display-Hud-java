package com.navdy.hud.app.util;

import android.hardware.Camera;
import android.os.Build;
import com.navdy.hud.app.storage.PathManager;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.util.IOUtils;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

public class DeviceUtil {
    public static final String BUILD_TYPE_USER = "user";
    private static final HereSdkVersion CURRENT_HERE_SDK = HereSdkVersion.SDK;
    public static final String TELEMETRY_FILE_NAME = "telemetry";
    private static Boolean hasCamera;
    private static boolean hudDevice = true;
    private static final Logger sLogger = new Logger(DeviceUtil.class);

    private enum HereSdkVersion {
        SDK("3.3.1", "00a584764c918d799f376cc41e46674ba558f8b4");
        
        private String folderName;
        private String version;

        private HereSdkVersion(String version, String timestamp) {
            this.version = version;
            this.folderName = timestamp;
        }
    }

    static {
        if (Build.MODEL.equalsIgnoreCase("NAVDY_HUD-MX6DL") || Build.MODEL.equalsIgnoreCase("Display")) {
            sLogger.i("Running on Navdy Device:" + Build.MODEL);
            return;
        }
        sLogger.i("Not running on Navdy Device:" + Build.MODEL);
    }

    public static boolean isNavdyDevice() {
        return hudDevice;
    }

    public static boolean supportsCamera() {
        if (hasCamera == null) {
            sLogger.v("Found " + Camera.getNumberOfCameras() + " cameras");
            Camera camera = null;
            try {
                camera = Camera.open(0);
                hasCamera = Boolean.valueOf(true);
                if (camera != null) {
                    camera.release();
                }
            } catch (Exception e) {
                sLogger.e("Failed to open camera", e);
                hasCamera = Boolean.valueOf(false);
                if (camera != null) {
                    camera.release();
                }
            } catch (Throwable th) {
                if (camera != null) {
                    camera.release();
                }
            }
        }
        return hasCamera.booleanValue();
    }

    public static void takeDeviceScreenShot(String absolutePath) {
        try {
            Runtime.getRuntime().exec("screencap -p " + absolutePath).waitFor();
        } catch (IOException e) {
            sLogger.e("Error while taking screen shot", e);
        } catch (InterruptedException e2) {
            sLogger.e("Error while taking screen shot", e2);
        }
    }

    public static boolean isUserBuild() {
        return "user".equals(Build.TYPE);
    }

    public static void copyHEREMapsDataInfo(String destination) {
        File dir = new File(PathManager.getInstance().getHereMapsDataDirectory());
        if (dir.exists()) {
            File metaData = new File(dir, PathManager.HERE_MAP_META_JSON_FILE);
            if (metaData.exists()) {
                try {
                    IOUtils.copyFile(metaData.getAbsolutePath(), destination + File.separator + "HERE_meta.json");
                    return;
                } catch (IOException e) {
                    sLogger.e("Error copying the HERE maps data meta json file");
                    return;
                }
            }
            sLogger.d("Meta json file not found");
            return;
        }
        sLogger.d("Here maps data directory on the maps partition does not exists");
    }

    public static String getHEREMapsDataInfo() {
        Exception e;
        Throwable th;
        String metaDataContent = null;
        File dir = new File(PathManager.getInstance().getHereMapsDataDirectory());
        if (dir.exists()) {
            File metaData = new File(dir, PathManager.HERE_MAP_META_JSON_FILE);
            if (metaData.exists()) {
                FileInputStream fis = null;
                try {
                    FileInputStream fis2 = new FileInputStream(metaData);
                    try {
                        metaDataContent = IOUtils.convertInputStreamToString(fis2, "UTF-8");
                        if (fis2 != null) {
                            try {
                                fis2.close();
                            } catch (IOException e2) {
                                sLogger.e("Error closing the FileInputStream", e2);
                            }
                        }
                    } catch (Exception e3) {
                        e = e3;
                        fis = fis2;
                    } catch (Throwable th2) {
                        th = th2;
                        fis = fis2;
                        if (fis != null) {
                            try {
                                fis.close();
                            } catch (IOException e22) {
                                sLogger.e("Error closing the FileInputStream", e22);
                            }
                        }
                        throw th;
                    }
                } catch (Exception e4) {
                    e = e4;
                    try {
                        sLogger.e("Error reading the Meta data,", e);
                        if (fis != null) {
                            try {
                                fis.close();
                            } catch (IOException e222) {
                                sLogger.e("Error closing the FileInputStream", e222);
                            }
                        }
                        return metaDataContent;
                    } catch (Throwable th3) {
                        th = th3;
                        if (fis != null) {
                            fis.close();
                        }
                        throw th;
                    }
                }
            }
            sLogger.d("Meta json file not found");
        } else {
            sLogger.d("Here maps data directory on the maps partition does not exists");
        }
        return metaDataContent;
    }

    public static String getCurrentHereSdkVersion() {
        return CURRENT_HERE_SDK.version;
    }

    public static String getCurrentHereSdkTimestamp() {
        return CURRENT_HERE_SDK.folderName;
    }
}
