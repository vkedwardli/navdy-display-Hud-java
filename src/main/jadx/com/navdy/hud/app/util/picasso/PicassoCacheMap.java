package com.navdy.hud.app.util.picasso;

import java.util.LinkedHashMap;

public class PicassoCacheMap<K, V> extends LinkedHashMap<K, V> {
    private PicassoItemCacheListener listener;

    PicassoCacheMap(int initialCapacity, float loadFactor) {
        super(initialCapacity, loadFactor, true);
    }

    void setListener(PicassoItemCacheListener listener) {
        this.listener = listener;
    }

    public V remove(Object key) {
        this.listener.itemRemoved(key);
        return super.remove(key);
    }

    public V put(K key, V value) {
        V ret = super.put(key, value);
        this.listener.itemAdded(key);
        return ret;
    }
}
