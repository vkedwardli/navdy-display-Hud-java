package com.navdy.hud.app.util.os;

import android.os.Handler;
import android.os.Looper;
import android.os.Process;
import android.text.TextUtils;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.task.TaskManager;
import com.navdy.service.library.util.SystemUtils;
import com.navdy.service.library.util.SystemUtils.CpuInfo;
import com.navdy.service.library.util.SystemUtils.ProcessCpuInfo;
import com.squareup.otto.Bus;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.TimeUnit;

public class CpuProfiler {
    private static final CpuUsage HIGH_CPU = new CpuUsage(Usage.HIGH);
    private static final String[] HIGH_CPU_THREAD_NAME_PATTERN = new String[]{"NAVDY-HOGGER-", "Thread-"};
    private static final int HIGH_CPU_USAGE_THRESHOLD = 85;
    private static final int INITIAL_PERIODIC_INTERVAL = ((int) TimeUnit.SECONDS.toMillis(45));
    private static final int LOW_NICE_PRIORITY = 19;
    private static final String NAVDY_PROCESS_NAME = "com.navdy.hud.app";
    private static final CpuUsage NORMAL_CPU = new CpuUsage(Usage.NORMAL);
    private static final int NORMAL_CPU_USAGE_THRESHOLD = 70;
    private static final int PERIODIC_INTERVAL = ((int) TimeUnit.SECONDS.toMillis(10));
    private static final int PROCESS_CPU_USAGE_THRESHOLD = 20;
    private static final Logger sLogger = new Logger(CpuProfiler.class);
    private static final CpuProfiler singleton = new CpuProfiler();
    private Bus bus = RemoteDeviceManager.getInstance().getBus();
    private Handler handler = new Handler(Looper.getMainLooper());
    private ProcessCpuInfo highCpuThreadInfo;
    private int highCpuThreadOrigPrio;
    private boolean highCpuUsage;
    private Runnable periodicRunnable = new Runnable() {
        public void run() {
            TaskManager.getInstance().execute(CpuProfiler.this.periodicRunnableBk, 1);
        }
    };
    private Runnable periodicRunnableBk = new Runnable() {
        /* JADX WARNING: inconsistent code. */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void run() {
            try {
                if (CpuProfiler.this.running) {
                    CpuInfo cpuInfo = SystemUtils.getCpuUsage();
                    int user = cpuInfo.getCpuUser();
                    int system = cpuInfo.getCpuSystem();
                    int total = user + system;
                    if (CpuProfiler.this.highCpuUsage) {
                        if (total < 70) {
                            CpuProfiler.this.highCpuUsage = false;
                            CpuProfiler.this.bus.post(CpuProfiler.NORMAL_CPU);
                            CpuProfiler.this.revertCorrectiveAction(cpuInfo);
                            CpuProfiler.this.printCpuInfo("NOW NORMAL", total, user, system, cpuInfo.getList());
                        } else {
                            CpuProfiler.this.printCpuInfo("STILL HIGH", total, user, system, cpuInfo.getList());
                            if (CpuProfiler.this.highCpuThreadInfo == null) {
                                CpuProfiler.this.takeCorrectiveAction(cpuInfo);
                            }
                        }
                    } else if (total > CpuProfiler.HIGH_CPU_USAGE_THRESHOLD) {
                        CpuProfiler.this.highCpuUsage = true;
                        CpuProfiler.this.bus.post(CpuProfiler.HIGH_CPU);
                        CpuProfiler.this.takeCorrectiveAction(cpuInfo);
                        CpuProfiler.this.printCpuInfo("HIGH", total, user, system, cpuInfo.getList());
                    } else if (CpuProfiler.sLogger.isLoggable(2)) {
                        CpuProfiler.this.printCpuInfo("NORMAL", total, user, system, cpuInfo.getList());
                    }
                    if (CpuProfiler.this.running) {
                        CpuProfiler.this.handler.postDelayed(CpuProfiler.this.periodicRunnable, (long) CpuProfiler.PERIODIC_INTERVAL);
                    }
                } else if (CpuProfiler.this.running) {
                    CpuProfiler.this.handler.postDelayed(CpuProfiler.this.periodicRunnable, (long) CpuProfiler.PERIODIC_INTERVAL);
                }
            } catch (Throwable th) {
                if (CpuProfiler.this.running) {
                    CpuProfiler.this.handler.postDelayed(CpuProfiler.this.periodicRunnable, (long) CpuProfiler.PERIODIC_INTERVAL);
                }
            }
        }
    };
    private boolean running;

    public static class CpuUsage {
        public Usage usage;

        CpuUsage(Usage usage) {
            this.usage = usage;
        }
    }

    public enum Usage {
        HIGH,
        NORMAL
    }

    public static CpuProfiler getInstance() {
        return singleton;
    }

    private CpuProfiler() {
    }

    public synchronized void start() {
        if (this.running) {
            sLogger.v("already running");
        } else {
            this.handler.removeCallbacks(this.periodicRunnable);
            this.handler.postDelayed(this.periodicRunnable, (long) INITIAL_PERIODIC_INTERVAL);
            this.running = true;
            sLogger.v("running");
        }
    }

    public synchronized void stop() {
        if (this.running) {
            this.handler.removeCallbacks(this.periodicRunnable);
            this.running = false;
            sLogger.v("stopped");
        } else {
            sLogger.v("not running");
        }
    }

    private void printCpuInfo(String tag, int total, int user, int system, ArrayList<ProcessCpuInfo> list) {
        sLogger.v("[CPU] " + tag + " cpu=" + total + "% user=" + user + "% system=" + system + "%");
        Iterator it = list.iterator();
        while (it.hasNext()) {
            ProcessCpuInfo info = (ProcessCpuInfo) it.next();
            if (!"top".equals(info.getProcessName())) {
                sLogger.v("[CPU] " + tag + " " + info.getCpu() + "% process=" + info.getProcessName() + " thread=" + info.getThreadName() + " tid=" + info.getTid() + " pid=" + info.getPid() + "");
            }
        }
    }

    private void takeCorrectiveAction(CpuInfo cpuInfo) {
        try {
            sLogger.i("takeCorrectiveAction");
            ProcessCpuInfo highCpuThread = getKnownHighCpuThread(cpuInfo);
            if (highCpuThread == null) {
                sLogger.i("takeCorrectiveAction high cpu thread not found");
            } else if (highCpuThread.getTid() != 0) {
                int prio = Process.getThreadPriority(highCpuThread.getTid());
                sLogger.v("takeCorrectiveAction: high cpu thread, priority[" + prio + "]");
                if (this.highCpuThreadInfo == null) {
                    this.highCpuThreadInfo = highCpuThread;
                    this.highCpuThreadOrigPrio = prio;
                    Process.setThreadPriority(this.highCpuThreadInfo.getTid(), 19);
                    sLogger.i("takeCorrectiveAction high cpu thread, priority changed from " + prio + " to " + Process.getThreadPriority(highCpuThread.getTid()));
                    return;
                }
                sLogger.i("takeCorrectiveAction high cpu thread, priority already reduced");
            } else {
                sLogger.i("takeCorrectiveAction high cpu thread tid is not valid");
            }
        } catch (Throwable t) {
            sLogger.e(t);
        }
    }

    private void revertCorrectiveAction(CpuInfo cpuInfo) {
        try {
            sLogger.i("revertCorrectiveAction");
            if (this.highCpuThreadInfo != null) {
                Process.setThreadPriority(this.highCpuThreadInfo.getTid(), this.highCpuThreadOrigPrio);
                sLogger.i("revertCorrectiveAction high cpu thread priority reduced from 19 to " + this.highCpuThreadOrigPrio);
                this.highCpuThreadInfo = null;
                this.highCpuThreadOrigPrio = 0;
                return;
            }
            sLogger.i("revertCorrectiveAction high cpu thread was not detected previously");
        } catch (Throwable t) {
            sLogger.e(t);
        }
    }

    private ProcessCpuInfo getKnownHighCpuThread(CpuInfo cpuInfo) {
        Iterator it = cpuInfo.getList().iterator();
        while (it.hasNext()) {
            ProcessCpuInfo info = (ProcessCpuInfo) it.next();
            if (TextUtils.equals(info.getProcessName(), "com.navdy.hud.app")) {
                String threadName = info.getThreadName();
                if (threadName != null) {
                    for (String startsWith : HIGH_CPU_THREAD_NAME_PATTERN) {
                        if (threadName.startsWith(startsWith)) {
                            if (info.getCpu() >= 20) {
                                sLogger.v("high cpu thread found " + info.getThreadName() + " " + info.getCpu() + "%");
                                return info;
                            }
                            sLogger.v("Thread found but cpu usage < threshold" + info.getThreadName() + " " + info.getCpu() + "%");
                        }
                    }
                    continue;
                } else {
                    continue;
                }
            }
        }
        return null;
    }
}
