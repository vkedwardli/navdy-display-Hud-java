package com.navdy.hud.app.util.os;

import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.system.ErrnoException;
import android.system.Os;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.task.TaskManager;
import com.navdy.service.library.util.IOUtils;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.util.regex.Pattern;
import org.json.JSONException;
import org.json.JSONObject;

public class PropsFileUpdater {
    private static final String CHAR_SET = "UTF-8";
    private static final String COMMAND_GETPROP = "getprop";
    private static final int PROPS_FILE_INTENT = 2;
    private static final String PROPS_FILE_NAME = "system_info.json";
    private static final String PROPS_FILE_PATH_PROP_NAME = "ro.maps_partition";
    private static final Pattern PROPS_KEY_VALUE_SEPARATOR = Pattern.compile("\\]: \\[");
    private static final int PROPS_READOUT_DELAY = 15000;
    private static final String PROPS_TEMP_FILE_SUFFIX = "~";
    private static final Logger sLogger = new Logger(PropsFileUpdater.class);

    public static void run() {
        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {

            class UpdatePropsFileRunnable implements Runnable {
                UpdatePropsFileRunnable() {
                }

                public void run() {
                    try {
                        PropsFileUpdater.updatePropsFile(PropsFileUpdater.readProps());
                    } catch (Throwable t) {
                        PropsFileUpdater.sLogger.e("Error while updating props file", t);
                    }
                }
            }

            public void run() {
                TaskManager.getInstance().execute(new UpdatePropsFileRunnable(), 1);
            }
        }, 15000);
    }

    private static void updatePropsFile(@Nullable String props) {
        if (props == null) {
            sLogger.w("Cannot update file with empty props string");
            return;
        }
        try {
            JSONObject json = propsToJSON(props);
            updatePropsFile(json.getString(PROPS_FILE_PATH_PROP_NAME), json.toString(2));
        } catch (JSONException e) {
            sLogger.e("Cannot build JSON string with props or read path for output file", e);
        }
    }

    private static void updatePropsFile(@NonNull String destinationFileFullPath, @NonNull String json) {
        String propsFileFullPath = new File(destinationFileFullPath, PROPS_FILE_NAME).getAbsolutePath();
        String propsTempFileFullPath = propsFileFullPath + PROPS_TEMP_FILE_SUFFIX;
        try {
            IOUtils.copyFile(propsTempFileFullPath, new ByteArrayInputStream(json.getBytes()));
            try {
                Os.rename(propsTempFileFullPath, propsFileFullPath);
                sLogger.i("Props file updated successfully");
            } catch (ErrnoException e) {
                sLogger.e("Cannot overwrite props file with the new one", e);
            }
        } catch (IOException e2) {
            sLogger.e("Exception while writing into file: " + propsTempFileFullPath, e2);
        }
    }

    @NonNull
    private static JSONObject propsToJSON(@NonNull String props) {
        String[] propLines = props.split(System.getProperty("line.separator"));
        JSONObject jsonObject = new JSONObject();
        for (String propLine : propLines) {
            String[] keyValuePair = PROPS_KEY_VALUE_SEPARATOR.split(propLine.trim().substring(1, propLine.length() - 1), 2);
            try {
                jsonObject.put(keyValuePair[0], keyValuePair[1]);
            } catch (JSONException e) {
                sLogger.e("Cannot create json for prop: " + propLine + " - skipping", e);
            }
        }
        return jsonObject;
    }

    @Nullable
    public static String readProps() {
        Process readerProcess = null;
        String props = null;
        try {
            readerProcess = new ProcessBuilder(new String[0]).command(new String[]{COMMAND_GETPROP}).start();
            readerProcess.waitFor();
            sLogger.d("Reading props process ended with exit value: " + readerProcess.exitValue());
            props = IOUtils.convertInputStreamToString(readerProcess.getInputStream(), "UTF-8");
            if (readerProcess != null) {
                readerProcess.destroy();
            }
        } catch (Throwable th) {
            if (readerProcess != null) {
                readerProcess.destroy();
            }
        }
        return props;
    }
}
