package com.navdy.hud.app.util;

import com.navdy.hud.app.debug.DriveRecorder;
import com.navdy.hud.app.gesture.GestureServiceConnector;
import com.navdy.service.library.network.http.IHttpManager;
import com.squareup.otto.Bus;
import dagger.MembersInjector;
import dagger.internal.Binding;
import dagger.internal.Linker;
import java.util.Set;
import javax.inject.Provider;

public final class ReportIssueService$$InjectAdapter extends Binding<ReportIssueService> implements Provider<ReportIssueService>, MembersInjector<ReportIssueService> {
    private Binding<Bus> bus;
    private Binding<DriveRecorder> driveRecorder;
    private Binding<GestureServiceConnector> gestureService;
    private Binding<IHttpManager> mHttpManager;

    public ReportIssueService$$InjectAdapter() {
        super("com.navdy.hud.app.util.ReportIssueService", "members/com.navdy.hud.app.util.ReportIssueService", false, ReportIssueService.class);
    }

    public void attach(Linker linker) {
        this.mHttpManager = linker.requestBinding("com.navdy.service.library.network.http.IHttpManager", ReportIssueService.class, getClass().getClassLoader());
        this.gestureService = linker.requestBinding("com.navdy.hud.app.gesture.GestureServiceConnector", ReportIssueService.class, getClass().getClassLoader());
        this.bus = linker.requestBinding("com.squareup.otto.Bus", ReportIssueService.class, getClass().getClassLoader());
        this.driveRecorder = linker.requestBinding("com.navdy.hud.app.debug.DriveRecorder", ReportIssueService.class, getClass().getClassLoader());
    }

    public void getDependencies(Set<Binding<?>> set, Set<Binding<?>> injectMembersBindings) {
        injectMembersBindings.add(this.mHttpManager);
        injectMembersBindings.add(this.gestureService);
        injectMembersBindings.add(this.bus);
        injectMembersBindings.add(this.driveRecorder);
    }

    public ReportIssueService get() {
        ReportIssueService result = new ReportIssueService();
        injectMembers(result);
        return result;
    }

    public void injectMembers(ReportIssueService object) {
        object.mHttpManager = (IHttpManager) this.mHttpManager.get();
        object.gestureService = (GestureServiceConnector) this.gestureService.get();
        object.bus = (Bus) this.bus.get();
        object.driveRecorder = (DriveRecorder) this.driveRecorder.get();
    }
}
