package com.navdy.hud.app.util;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import com.navdy.hud.app.framework.toast.ToastPresenter;
import com.navdy.hud.app.storage.cache.DiskLruCache;
import com.navdy.hud.app.storage.db.HudDatabase;
import com.navdy.hud.app.storage.db.table.MusicArtworkCacheTable;
import com.navdy.hud.app.util.picasso.PicassoUtil;
import com.navdy.service.library.events.audio.MusicCollectionInfo;
import com.navdy.service.library.events.audio.MusicCollectionType;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.task.TaskManager;
import com.navdy.service.library.util.IOUtils;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MusicArtworkCache {
    private static final String SEPARATOR = "_";
    private static Map<MusicCollectionType, String> collectionTypeMap = new HashMap();
    private static final Logger sLogger = new Logger(MusicArtworkCache.class);
    private final Object dbLock = new Object();

    public interface Callback {
        void onHit(@NonNull byte[] bArr);

        void onMiss();
    }

    private class CacheEntryHelper {
        String album;
        String author;
        String fileName;
        String name;
        @NonNull
        String type;

        CacheEntryHelper(String author, String album, String name) {
            this.type = MusicArtworkCacheTable.TYPE_MUSIC;
            this.author = author;
            this.album = album;
            this.name = name;
        }

        CacheEntryHelper(@NonNull MusicCollectionInfo collectionInfo) {
            this.type = (String) MusicArtworkCache.collectionTypeMap.get(collectionInfo.collectionType);
            switch (collectionInfo.collectionType) {
                case COLLECTION_TYPE_UNKNOWN:
                    MusicArtworkCache.sLogger.e("Unknown type");
                    return;
                case COLLECTION_TYPE_PLAYLISTS:
                    this.name = collectionInfo.name;
                    return;
                case COLLECTION_TYPE_ARTISTS:
                    this.author = collectionInfo.name;
                    return;
                case COLLECTION_TYPE_ALBUMS:
                    this.album = collectionInfo.name;
                    this.author = collectionInfo.subtitle;
                    return;
                case COLLECTION_TYPE_PODCASTS:
                    this.album = collectionInfo.name;
                    return;
                case COLLECTION_TYPE_AUDIOBOOKS:
                    return;
                default:
                    this.name = collectionInfo.name;
                    return;
            }
        }

        private String getFileName() {
            if (this.fileName == null) {
                StringBuilder builder = new StringBuilder(this.type);
                if (!TextUtils.isEmpty(this.author)) {
                    builder.append("_");
                    builder.append(this.author);
                }
                if (!TextUtils.isEmpty(this.album)) {
                    builder.append("_");
                    builder.append(this.album);
                }
                if (!TextUtils.isEmpty(this.name)) {
                    builder.append("_");
                    builder.append(this.name);
                }
                this.fileName = formatIdentifier(builder);
            }
            return this.fileName;
        }

        private String formatIdentifier(CharSequence unformatted) {
            Matcher matcher = Pattern.compile("[^A-Za-z0-9_]").matcher(unformatted);
            if (TextUtils.isEmpty(unformatted)) {
                return null;
            }
            return matcher.replaceAll("_");
        }

        public String toString() {
            return this.type + ", " + this.author + " - " + this.album + " - " + this.name;
        }
    }

    static {
        collectionTypeMap.put(MusicCollectionType.COLLECTION_TYPE_PLAYLISTS, MusicArtworkCacheTable.TYPE_PLAYLIST);
        collectionTypeMap.put(MusicCollectionType.COLLECTION_TYPE_ARTISTS, MusicArtworkCacheTable.TYPE_MUSIC);
        collectionTypeMap.put(MusicCollectionType.COLLECTION_TYPE_ALBUMS, MusicArtworkCacheTable.TYPE_MUSIC);
        collectionTypeMap.put(MusicCollectionType.COLLECTION_TYPE_PODCASTS, MusicArtworkCacheTable.TYPE_PODCAST);
        collectionTypeMap.put(MusicCollectionType.COLLECTION_TYPE_AUDIOBOOKS, MusicArtworkCacheTable.TYPE_AUDIOBOOK);
    }

    public void putArtwork(String author, String album, String name, byte[] artwork) {
        putArtworkInternal(new CacheEntryHelper(author, album, name), artwork);
    }

    public void putArtwork(@NonNull MusicCollectionInfo collectionInfo, byte[] artwork) {
        putArtworkInternal(new CacheEntryHelper(collectionInfo), artwork);
    }

    private void putArtworkInternal(final CacheEntryHelper cacheEntryHelper, final byte[] data) {
        if (cacheEntryHelper.getFileName() == null) {
            sLogger.e("Couldn't generate a filename for: " + cacheEntryHelper);
        } else {
            TaskManager.getInstance().execute(new Runnable() {
                public void run() {
                    DiskLruCache diskLruCache = PicassoUtil.getDiskLruCache();
                    if (diskLruCache == null) {
                        MusicArtworkCache.sLogger.e("No disk cache");
                        return;
                    }
                    diskLruCache.put(cacheEntryHelper.getFileName(), data);
                    MusicArtworkCache.this.createDbEntry(cacheEntryHelper);
                }
            }, 22);
        }
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void createDbEntry(CacheEntryHelper cacheEntryHelper) {
        sLogger.d("createDbEntry: " + cacheEntryHelper);
        if (cacheEntryHelper.author == null && cacheEntryHelper.album == null && cacheEntryHelper.name == null) {
            sLogger.w("Not cacheable");
            return;
        }
        ContentValues values = new ContentValues();
        values.put("type", cacheEntryHelper.type);
        if (cacheEntryHelper.author != null) {
            values.put(MusicArtworkCacheTable.AUTHOR, cacheEntryHelper.author);
        }
        if (cacheEntryHelper.album != null) {
            values.put(MusicArtworkCacheTable.ALBUM, cacheEntryHelper.album);
        }
        if (cacheEntryHelper.author != null) {
            values.put("name", cacheEntryHelper.name);
        }
        values.put(MusicArtworkCacheTable.FILE_NAME, cacheEntryHelper.getFileName());
        synchronized (this.dbLock) {
            SQLiteDatabase db = HudDatabase.getInstance().getWritableDatabase();
            if (db == null) {
                sLogger.e("Couldn't get db");
                return;
            }
            if (db.insertWithOnConflict(MusicArtworkCacheTable.TABLE_NAME, null, values, 4) == -1) {
                db.update(MusicArtworkCacheTable.TABLE_NAME, values, "rowid = ?", new String[]{String.valueOf(db.insertWithOnConflict(MusicArtworkCacheTable.TABLE_NAME, null, values, 4))});
            }
        }
    }

    public void getArtwork(@NonNull String artist, String album, String name, Callback callback) {
        getArtworkInternal(new CacheEntryHelper(artist, album, name), callback);
    }

    public void getArtwork(@NonNull MusicCollectionInfo musicCollectionInfo, @NonNull Callback callback) {
        getArtworkInternal(new CacheEntryHelper(musicCollectionInfo), callback);
    }

    private void getArtworkInternal(@NonNull final CacheEntryHelper cacheEntryHelper, @NonNull final Callback callback) {
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                String fileName = MusicArtworkCache.this.getFileNameFromDb(cacheEntryHelper);
                if (fileName == null) {
                    MusicArtworkCache.sLogger.i("No entry for " + cacheEntryHelper);
                    callback.onMiss();
                    return;
                }
                DiskLruCache diskLruCache = PicassoUtil.getDiskLruCache();
                if (diskLruCache == null) {
                    MusicArtworkCache.sLogger.e("No disk cache");
                    callback.onMiss();
                    return;
                }
                byte[] data = diskLruCache.get(fileName);
                if (data != null) {
                    callback.onHit(data);
                } else {
                    callback.onMiss();
                }
            }
        }, 22);
    }

    private String getFileNameFromDb(@NonNull CacheEntryHelper cacheEntryHelper) {
        sLogger.d("getFileNameFromDb: " + cacheEntryHelper);
        Cursor cursor = null;
        try {
            String[] columns = new String[]{MusicArtworkCacheTable.FILE_NAME};
            ArrayList<String> args = new ArrayList();
            StringBuilder selection = new StringBuilder("type = ?");
            args.add(cacheEntryHelper.type);
            if (cacheEntryHelper.author != null) {
                selection.append(" AND author = ?");
                args.add(cacheEntryHelper.author);
            }
            if (cacheEntryHelper.album != null) {
                selection.append(" AND album = ?");
                args.add(cacheEntryHelper.album);
            }
            if (!TextUtils.equals(cacheEntryHelper.type, MusicArtworkCacheTable.TYPE_MUSIC) || (cacheEntryHelper.name != null && cacheEntryHelper.album == null)) {
                selection.append(" AND name = ?");
                args.add(cacheEntryHelper.name != null ? cacheEntryHelper.name : "");
            }
            synchronized (this.dbLock) {
                SQLiteDatabase db = HudDatabase.getInstance().getWritableDatabase();
                if (db == null) {
                    sLogger.e("Couldn't get db");
                    IOUtils.closeObject(null);
                    return null;
                }
                cursor = db.query(MusicArtworkCacheTable.TABLE_NAME, columns, selection.toString(), (String[]) args.toArray(new String[args.size()]), null, null, null, ToastPresenter.EXTRA_MAIN_TITLE);
                if (cursor == null || !cursor.moveToFirst()) {
                    IOUtils.closeObject(cursor);
                    return null;
                }
                String string = cursor.getString(cursor.getColumnIndex(MusicArtworkCacheTable.FILE_NAME));
                IOUtils.closeObject(cursor);
                return string;
            }
        } catch (Throwable th) {
            IOUtils.closeObject(cursor);
        }
    }
}
