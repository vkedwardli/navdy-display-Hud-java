package com.navdy.hud.app.util.picasso;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.storage.PathManager;
import com.navdy.hud.app.storage.cache.DiskLruCache;
import com.navdy.service.library.log.Logger;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Picasso.Builder;
import com.squareup.picasso.Picasso.LoadedFrom;
import com.squareup.picasso.Request;
import com.squareup.picasso.RequestHandler;
import com.squareup.picasso.RequestHandler.Result;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;

public class PicassoUtil {
    private static final String DISK_CACHE_SCHEME = "diskcache://";
    private static final String DISK_CACHE_SCHEME_NAME = "diskcache";
    private static final int DISK_CACHE_SIZE = 10485760;
    public static final int IMAGE_MEMORY_CACHE = 8388608;
    private static DiskLruCache diskLruCache;
    private static volatile boolean initialized;
    private static Object lockObj = new Object();
    private static PicassoLruCache lruCache;
    private static Picasso picasso;
    private static final Logger sLogger = new Logger(PicassoUtil.class);

    public static void initPicasso(Context context) {
        if (!initialized) {
            synchronized (lockObj) {
                if (!initialized) {
                    String imageDiskCacheFolder;
                    try {
                        lruCache = new PicassoLruCache(8388608);
                        lruCache.init();
                        imageDiskCacheFolder = PathManager.getInstance().getImageDiskCacheFolder();
                        diskLruCache = new DiskLruCache("imagecache", imageDiskCacheFolder, DISK_CACHE_SIZE);
                    } catch (Throwable t2) {
                        sLogger.e("Error re-creating disk cache: " + t2);
                    }
                    picasso = new Builder(context).memoryCache(lruCache).addRequestHandler(new RequestHandler() {
                        public boolean canHandleRequest(Request data) {
                            return PicassoUtil.DISK_CACHE_SCHEME_NAME.equals(data.uri.getScheme());
                        }

                        public Result load(Request request, int networkPolicy) throws IOException {
                            String resourceName = request.uri.getHost();
                            PicassoUtil.sLogger.v("load:" + resourceName);
                            if (PicassoUtil.diskLruCache == null) {
                                throw new IOException();
                            }
                            byte[] data = PicassoUtil.diskLruCache.get(resourceName);
                            if (data != null) {
                                return new Result(new ByteArrayInputStream(data), LoadedFrom.DISK);
                            }
                            throw new IOException();
                        }
                    }).build();
                    Picasso.setSingletonInstance(picasso);
                    initialized = true;
                }
            }
        }
    }

    public static void clearCache() {
        if (lruCache != null) {
            lruCache.clear();
        }
    }

    public static Bitmap getBitmapfromCache(File file) {
        if (lruCache != null) {
            return lruCache.getBitmap("file://" + file.getAbsolutePath());
        }
        return null;
    }

    public static Bitmap getBitmapfromCache(String key) {
        if (lruCache != null) {
            return lruCache.getBitmap(key);
        }
        return null;
    }

    public static void setBitmapInCache(String key, Bitmap bitmap) {
        if (lruCache != null) {
            lruCache.setBitmap(key, bitmap);
        }
    }

    public static Picasso getInstance() {
        if (!initialized) {
            initPicasso(HudApplication.getAppContext());
        }
        return picasso;
    }

    public static boolean isImageAvailableInCache(File file) {
        if (getBitmapfromCache(file) != null) {
            return true;
        }
        return false;
    }

    public static DiskLruCache getDiskLruCache() {
        return diskLruCache;
    }

    public static Uri getDiskcacheUri(String name) {
        return Uri.parse(DISK_CACHE_SCHEME + name);
    }
}
