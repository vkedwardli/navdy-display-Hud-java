package com.navdy.hud.app.profile;

import com.navdy.hud.app.common.TimeHelper;
import com.navdy.hud.app.storage.PathManager;
import com.squareup.otto.Bus;
import dagger.internal.Binding;
import dagger.internal.Linker;
import java.util.Set;
import javax.inject.Provider;

public final class DriverProfileManager$$InjectAdapter extends Binding<DriverProfileManager> implements Provider<DriverProfileManager> {
    private Binding<Bus> bus;
    private Binding<PathManager> pathManager;
    private Binding<TimeHelper> timeHelper;

    public DriverProfileManager$$InjectAdapter() {
        super("com.navdy.hud.app.profile.DriverProfileManager", "members/com.navdy.hud.app.profile.DriverProfileManager", false, DriverProfileManager.class);
    }

    public void attach(Linker linker) {
        this.bus = linker.requestBinding("com.squareup.otto.Bus", DriverProfileManager.class, getClass().getClassLoader());
        this.pathManager = linker.requestBinding("com.navdy.hud.app.storage.PathManager", DriverProfileManager.class, getClass().getClassLoader());
        this.timeHelper = linker.requestBinding("com.navdy.hud.app.common.TimeHelper", DriverProfileManager.class, getClass().getClassLoader());
    }

    public void getDependencies(Set<Binding<?>> getBindings, Set<Binding<?>> set) {
        getBindings.add(this.bus);
        getBindings.add(this.pathManager);
        getBindings.add(this.timeHelper);
    }

    public DriverProfileManager get() {
        return new DriverProfileManager((Bus) this.bus.get(), (PathManager) this.pathManager.get(), (TimeHelper) this.timeHelper.get());
    }
}
