package com.navdy.hud.app.analytics;

import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.Lambda;

@Metadata(bv = {1, 0, 1}, d1 = {"\u0000\b\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\b\u0002"}, d2 = {"<anonymous>", "", "invoke"}, k = 3, mv = {1, 1, 6})
/* compiled from: TelemetryDataManager.kt */
final class TelemetryDataManager$reportRunnable$1 extends Lambda implements Function0<Unit> {
    final /* synthetic */ TelemetryDataManager this$0;

    TelemetryDataManager$reportRunnable$1(TelemetryDataManager telemetryDataManager) {
        super(0);
        this.this$0 = telemetryDataManager;
    }

    public final void invoke() {
        this.this$0.reportTelemetryData();
    }
}
