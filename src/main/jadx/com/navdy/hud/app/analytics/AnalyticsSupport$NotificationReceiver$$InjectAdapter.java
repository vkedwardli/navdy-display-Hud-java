package com.navdy.hud.app.analytics;

import android.content.SharedPreferences;
import com.navdy.hud.app.analytics.AnalyticsSupport.NotificationReceiver;
import com.navdy.hud.app.device.PowerManager;
import dagger.MembersInjector;
import dagger.internal.Binding;
import dagger.internal.Linker;
import java.util.Set;
import javax.inject.Provider;

public final class AnalyticsSupport$NotificationReceiver$$InjectAdapter extends Binding<NotificationReceiver> implements Provider<NotificationReceiver>, MembersInjector<NotificationReceiver> {
    private Binding<SharedPreferences> appPreferences;
    private Binding<PowerManager> powerManager;

    public AnalyticsSupport$NotificationReceiver$$InjectAdapter() {
        super("com.navdy.hud.app.analytics.AnalyticsSupport$NotificationReceiver", "members/com.navdy.hud.app.analytics.AnalyticsSupport$NotificationReceiver", false, NotificationReceiver.class);
    }

    public void attach(Linker linker) {
        this.powerManager = linker.requestBinding("com.navdy.hud.app.device.PowerManager", NotificationReceiver.class, getClass().getClassLoader());
        this.appPreferences = linker.requestBinding("android.content.SharedPreferences", NotificationReceiver.class, getClass().getClassLoader());
    }

    public void getDependencies(Set<Binding<?>> set, Set<Binding<?>> injectMembersBindings) {
        injectMembersBindings.add(this.powerManager);
        injectMembersBindings.add(this.appPreferences);
    }

    public NotificationReceiver get() {
        NotificationReceiver result = new NotificationReceiver();
        injectMembers(result);
        return result;
    }

    public void injectMembers(NotificationReceiver object) {
        object.powerManager = (PowerManager) this.powerManager.get();
        object.appPreferences = (SharedPreferences) this.appPreferences.get();
    }
}
