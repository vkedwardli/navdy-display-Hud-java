package com.navdy.hud.app.analytics;

import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = {1, 0, 1}, k = 3, mv = {1, 1, 6})
/* compiled from: TelemetryDataManager.kt */
final class TelemetryDataManagerKt$sam$Runnable$cc6381d8 implements Runnable {
    private final /* synthetic */ Function0 function;

    TelemetryDataManagerKt$sam$Runnable$cc6381d8(Function0 function0) {
        this.function = function0;
    }

    public final /* synthetic */ void run() {
        Intrinsics.checkExpressionValueIsNotNull(this.function.invoke(), "invoke(...)");
    }
}
