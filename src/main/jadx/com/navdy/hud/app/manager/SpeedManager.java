package com.navdy.hud.app.manager;

import android.os.Handler;
import android.os.Looper;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.analytics.RawSpeed;
import com.navdy.hud.app.event.DriverProfileChanged;
import com.navdy.hud.app.event.DriverProfileUpdated;
import com.navdy.hud.app.event.DriverProfileUpdated.State;
import com.navdy.hud.app.profile.DriverProfileManager;
import com.navdy.service.library.events.preferences.DriverProfilePreferences.UnitSystem;
import com.navdy.service.library.log.Logger;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;
import javax.inject.Inject;
import mortar.Mortar;

public class SpeedManager {
    private static final double EPSILON = 1.0E-5d;
    private static final long GPS_SPEED_EXPIRY_INTERVAL = 5000;
    private static final double MAX_VALID_METERS_PER_SEC = 89.41333333333333d;
    private static final double METERS_PER_KILOMETER = 1000.0d;
    private static final double METERS_PER_MILE = 1609.44d;
    private static final int SECONDS_PER_HOUR = 3600;
    public static final int SPEED_NOT_AVAILABLE = -1;
    private static final Logger sLogger = new Logger(SpeedManager.class);
    private static final SpeedManager singleton = new SpeedManager();
    @Inject
    Bus bus;
    @Inject
    DriverProfileManager driverProfileManager;
    private volatile int gpsSpeed;
    private Runnable gpsSpeedExpiryRunnable;
    private Handler handler;
    private volatile int obdSpeed = -1;
    private volatile float rawGpsSpeed = -1.0f;
    private volatile long rawGpsSpeedTimeStamp = 0;
    private volatile int rawObdSpeed = -1;
    private volatile long rawObdSpeedTimeStamp = 0;
    private SpeedUnit speedUnit = SpeedUnit.MILES_PER_HOUR;

    public static class SpeedDataExpired {
        public SpeedDataSource source = SpeedDataSource.GPS;
    }

    public enum SpeedDataSource {
        GPS,
        OBD
    }

    public enum SpeedUnit {
        MILES_PER_HOUR,
        KILOMETERS_PER_HOUR,
        METERS_PER_SECOND
    }

    public static class SpeedUnitChanged {
    }

    public static SpeedManager getInstance() {
        return singleton;
    }

    @Inject
    public SpeedManager() {
        Mortar.inject(HudApplication.getAppContext(), this);
        this.handler = new Handler(Looper.getMainLooper());
        this.bus.register(this);
        setUnit();
    }

    public SpeedUnit getSpeedUnit() {
        return this.speedUnit;
    }

    public void setSpeedUnit(SpeedUnit speedUnit) {
        if (speedUnit != this.speedUnit) {
            this.gpsSpeed = convert((double) this.gpsSpeed, this.speedUnit, speedUnit);
            this.obdSpeed = convert((double) this.obdSpeed, this.speedUnit, speedUnit);
            this.speedUnit = speedUnit;
        }
    }

    public synchronized int getGpsSpeed() {
        return this.gpsSpeed;
    }

    public synchronized boolean setGpsSpeed(float metersPerSec, long rawGpsSpeedTimeStamp) {
        if (this.gpsSpeedExpiryRunnable == null) {
            this.gpsSpeedExpiryRunnable = new Runnable() {
                public void run() {
                    SpeedManager.sLogger.d("Gps RawSpeed data expired, resetting to zero");
                    SpeedManager.this.gpsSpeed = -1;
                    HudApplication.getApplication().getBus().post(new SpeedDataExpired());
                }
            };
        }
        this.handler.removeCallbacks(this.gpsSpeedExpiryRunnable);
        this.handler.postDelayed(this.gpsSpeedExpiryRunnable, 5000);
        return updateSpeed(metersPerSec, rawGpsSpeedTimeStamp);
    }

    private static boolean almostEqual(float a, float b) {
        return ((double) Math.abs(a - b)) < EPSILON;
    }

    private synchronized boolean updateSpeed(float metersPerSec, long timeStamp) {
        boolean z = false;
        synchronized (this) {
            if (almostEqual(this.rawGpsSpeed, metersPerSec)) {
                this.rawGpsSpeedTimeStamp = timeStamp;
            } else if (metersPerSec >= 0.0f && ((double) metersPerSec) <= MAX_VALID_METERS_PER_SEC) {
                this.rawGpsSpeedTimeStamp = timeStamp;
                this.rawGpsSpeed = metersPerSec;
                this.gpsSpeed = convert((double) metersPerSec, SpeedUnit.METERS_PER_SECOND, this.speedUnit);
                z = true;
            }
        }
        return z;
    }

    public int getObdSpeed() {
        return this.obdSpeed;
    }

    public int getRawObdSpeed() {
        return this.rawObdSpeed;
    }

    public synchronized void setObdSpeed(int kph, long timeStamp) {
        if (kph < -1 || kph > 256) {
            sLogger.w("Invalid OBD speed ignored:" + kph);
        } else {
            this.rawObdSpeed = kph;
            this.rawObdSpeedTimeStamp = timeStamp;
            this.obdSpeed = convert((double) kph, SpeedUnit.KILOMETERS_PER_HOUR, this.speedUnit);
        }
    }

    public static int convert(double source, SpeedUnit sourceUnit, SpeedUnit desiredUnit) {
        return Math.round(convertWithPrecision(source, sourceUnit, desiredUnit));
    }

    public static float convertWithPrecision(double source, SpeedUnit sourceUnit, SpeedUnit desiredUnit) {
        if (source == 0.0d || source == -1.0d) {
            return (float) source;
        }
        if (sourceUnit == desiredUnit) {
            return (float) source;
        }
        double metersPerSecond;
        switch (sourceUnit) {
            case MILES_PER_HOUR:
                metersPerSecond = (source * METERS_PER_MILE) / 3600.0d;
                break;
            case KILOMETERS_PER_HOUR:
                metersPerSecond = (source * METERS_PER_KILOMETER) / 3600.0d;
                break;
            case METERS_PER_SECOND:
                metersPerSecond = source;
                break;
            default:
                metersPerSecond = 0.0d;
                break;
        }
        switch (desiredUnit) {
            case MILES_PER_HOUR:
                return (float) ((metersPerSecond * 3600.0d) / METERS_PER_MILE);
            case KILOMETERS_PER_HOUR:
                return (float) ((metersPerSecond * 3600.0d) / METERS_PER_KILOMETER);
            case METERS_PER_SECOND:
                return (float) metersPerSecond;
            default:
                return 0.0f;
        }
    }

    public synchronized int getCurrentSpeed() {
        int obdSpeed;
        obdSpeed = getObdSpeed();
        if (obdSpeed == -1) {
            obdSpeed = getGpsSpeed();
        }
        return obdSpeed;
    }

    public synchronized RawSpeed getCurrentSpeedInMetersPerSecond() {
        RawSpeed rawSpeed;
        if (this.rawObdSpeed != -1) {
            rawSpeed = new RawSpeed(convertWithPrecision((double) this.rawObdSpeed, SpeedUnit.KILOMETERS_PER_HOUR, SpeedUnit.METERS_PER_SECOND), this.rawObdSpeedTimeStamp);
        } else {
            rawSpeed = new RawSpeed(this.rawGpsSpeed, this.rawGpsSpeedTimeStamp);
        }
        return rawSpeed;
    }

    public synchronized float getGpsSpeedInMetersPerSecond() {
        return this.rawGpsSpeed;
    }

    public static int convert(double metersPerSec, SpeedUnit unit) {
        return convert(metersPerSec, SpeedUnit.METERS_PER_SECOND, unit);
    }

    @Subscribe
    public void onDriverProfileUpdated(DriverProfileUpdated event) {
        if (event.state == State.UPDATED) {
            setUnit();
        }
    }

    @Subscribe
    public void onDriverProfileChanged(DriverProfileChanged event) {
        setUnit();
    }

    private void setUnit() {
        SpeedUnit speedUnit = this.driverProfileManager.getCurrentProfile().getUnitSystem() == UnitSystem.UNIT_SYSTEM_METRIC ? SpeedUnit.KILOMETERS_PER_HOUR : SpeedUnit.MILES_PER_HOUR;
        if (this.speedUnit != speedUnit) {
            sLogger.i("ondriverprofileupdate speed unit has changed to " + speedUnit.name());
            setSpeedUnit(speedUnit);
            this.bus.post(new SpeedUnitChanged());
            return;
        }
        sLogger.v("ondriverprofileupdated speed unit is still " + speedUnit.name());
    }
}
