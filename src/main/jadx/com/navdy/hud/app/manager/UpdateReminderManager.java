package com.navdy.hud.app.manager;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import com.navdy.hud.app.common.TimeHelper.DateTimeAvailableEvent;
import com.navdy.hud.app.device.dial.DialManager;
import com.navdy.hud.app.device.dial.DialManager.DialUpdateStatus;
import com.navdy.hud.app.event.ShowScreenWithArgs;
import com.navdy.hud.app.screen.DialUpdateProgressScreen;
import com.navdy.hud.app.util.OTAUpdateService;
import com.navdy.hud.app.util.OTAUpdateService.UpdateVerified;
import com.navdy.service.library.events.ui.Screen;
import com.navdy.service.library.log.Logger;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;
import java.util.concurrent.TimeUnit;
import javax.inject.Inject;
import mortar.Mortar;

public class UpdateReminderManager {
    private static long DIAL_CONNECT_CHECK_INTERVAL = TimeUnit.MINUTES.toMillis(15);
    public static final String DIAL_REMINDER_TIME = "dial_reminder_time";
    public static final String EXTRA_UPDATE_REMINDER = "UPDATE_REMINDER";
    private static long INITIAL_REMINDER_INTERVAL = TimeUnit.DAYS.toMillis(10);
    public static final String OTA_REMINDER_TIME = "ota_reminder_time";
    private static long REMINDER_INTERVAL = TimeUnit.DAYS.toMillis(1);
    private static final Logger sLogger = new Logger(UpdateReminderManager.class);
    @Inject
    Bus bus;
    private boolean dialUpdateAvailable = false;
    private Handler handler = new Handler(Looper.getMainLooper());
    private ReminderRunnable reminderRunnable = null;
    @Inject
    SharedPreferences sharedPreferences;
    private boolean timeAvailable = false;

    private class ReminderRunnable implements Runnable {
        boolean isDialUpdate;

        ReminderRunnable(boolean isDial) {
            this.isDialUpdate = isDial;
        }

        public void run() {
            Bundle args = new Bundle();
            if (this.isDialUpdate) {
                DialManager dialManager = DialManager.getInstance();
                if (!dialManager.isDialConnected()) {
                    UpdateReminderManager.this.sharedPreferences.edit().putLong(UpdateReminderManager.DIAL_REMINDER_TIME, System.currentTimeMillis() + UpdateReminderManager.DIAL_CONNECT_CHECK_INTERVAL).apply();
                } else if (dialManager.getDialFirmwareUpdater().getVersions().isUpdateAvailable()) {
                    args.putBoolean(UpdateReminderManager.EXTRA_UPDATE_REMINDER, true);
                    if (!DialUpdateProgressScreen.updateStarted) {
                        UpdateReminderManager.this.bus.post(new ShowScreenWithArgs(Screen.SCREEN_DIAL_UPDATE_CONFIRMATION, args, false));
                    }
                    UpdateReminderManager.this.sharedPreferences.edit().putLong(UpdateReminderManager.DIAL_REMINDER_TIME, System.currentTimeMillis() + UpdateReminderManager.REMINDER_INTERVAL).apply();
                } else {
                    UpdateReminderManager.this.sharedPreferences.edit().remove(UpdateReminderManager.DIAL_REMINDER_TIME).apply();
                }
            } else if (UpdateReminderManager.this.isOTAUpdateRequired()) {
                args.putBoolean(UpdateReminderManager.EXTRA_UPDATE_REMINDER, true);
                UpdateReminderManager.this.bus.post(new ShowScreenWithArgs(Screen.SCREEN_OTA_CONFIRMATION, args, false));
                UpdateReminderManager.this.sharedPreferences.edit().putLong(UpdateReminderManager.OTA_REMINDER_TIME, System.currentTimeMillis() + UpdateReminderManager.REMINDER_INTERVAL).apply();
            } else {
                UpdateReminderManager.this.sharedPreferences.edit().remove(UpdateReminderManager.OTA_REMINDER_TIME).apply();
            }
            UpdateReminderManager.this.scheduleReminder();
        }
    }

    public UpdateReminderManager(Context context) {
        Mortar.inject(context, this);
        this.bus.register(this);
    }

    private boolean isOTAUpdateRequired() {
        return OTAUpdateService.isUpdateAvailable();
    }

    private void scheduleReminder() {
        if (this.timeAvailable) {
            clearReminder();
            long ota_time = this.sharedPreferences.getLong(OTA_REMINDER_TIME, 0);
            long dial_time = 0;
            if (this.dialUpdateAvailable) {
                dial_time = this.sharedPreferences.getLong(DIAL_REMINDER_TIME, 0);
            }
            long time = 0;
            boolean dialUpdate = false;
            if (ota_time != 0 && (dial_time == 0 || ota_time <= dial_time)) {
                time = ota_time;
            } else if (dial_time != 0) {
                time = dial_time;
                dialUpdate = true;
            }
            long curTime = System.currentTimeMillis();
            if (time != 0) {
                long delta = time - curTime;
                if (dialUpdate && delta < TimeUnit.SECONDS.toMillis(1)) {
                    delta = TimeUnit.SECONDS.toMillis(1);
                } else if (delta < 0) {
                    delta = 0;
                }
                Logger logger = sLogger;
                String str = "new scheduling %s reminder for +%d seconds.";
                Object[] objArr = new Object[2];
                objArr[0] = dialUpdate ? "dial" : "OTA";
                objArr[1] = Long.valueOf(TimeUnit.MILLISECONDS.toSeconds(delta));
                logger.v(String.format(str, objArr));
                this.reminderRunnable = new ReminderRunnable(dialUpdate);
                this.handler.postDelayed(this.reminderRunnable, delta);
            }
        }
    }

    private void clearReminder() {
        if (this.reminderRunnable != null) {
            this.handler.removeCallbacks(this.reminderRunnable);
            this.reminderRunnable = null;
        }
    }

    @Subscribe
    public void handleDialUpdate(DialUpdateStatus event) {
        long time = this.sharedPreferences.getLong(DIAL_REMINDER_TIME, 0);
        if (event.available) {
            long curTime = System.currentTimeMillis();
            if (time == 0) {
                this.sharedPreferences.edit().putLong(DIAL_REMINDER_TIME, curTime + INITIAL_REMINDER_INTERVAL).apply();
                sLogger.v(String.format("handleDialUpdate(): setting dial update reminder time to +%d seconds", new Object[]{Long.valueOf(TimeUnit.MILLISECONDS.toSeconds(INITIAL_REMINDER_INTERVAL))}));
            } else {
                sLogger.v(String.format("handleDialUpdate(): dial update reminder time already set to +%d seconds", new Object[]{Long.valueOf(TimeUnit.MILLISECONDS.toSeconds(time - curTime))}));
            }
            this.dialUpdateAvailable = true;
            scheduleReminder();
        } else if (time != 0) {
            this.sharedPreferences.edit().remove(DIAL_REMINDER_TIME).apply();
            sLogger.v("handleDialUpdate(): clearing dial reminder");
        }
    }

    @Subscribe
    public void onDateTimeAvailable(DateTimeAvailableEvent event) {
        this.timeAvailable = true;
        long reminderTime = this.sharedPreferences.getLong(OTA_REMINDER_TIME, 0);
        if (isOTAUpdateRequired()) {
            long curTime = System.currentTimeMillis();
            if (reminderTime == 0) {
                this.sharedPreferences.edit().putLong(OTA_REMINDER_TIME, INITIAL_REMINDER_INTERVAL + curTime).apply();
                sLogger.v(String.format("onDateTimeAvailable(): setting OTA reminder time to +%d seconds ", new Object[]{Long.valueOf(TimeUnit.MILLISECONDS.toSeconds(INITIAL_REMINDER_INTERVAL))}));
            } else {
                sLogger.v(String.format("onDateTimeAvailable() OTA reminder time already set to +%d seconds", new Object[]{Long.valueOf(TimeUnit.MILLISECONDS.toSeconds(reminderTime - curTime))}));
            }
        } else if (reminderTime != 0) {
            this.sharedPreferences.edit().remove(OTA_REMINDER_TIME).apply();
            sLogger.v("onDateTimeAvailable(): clearing ota reminder");
        }
        scheduleReminder();
    }

    @Subscribe
    public void handleOTAUpdate(UpdateVerified event) {
        long reminderTime = this.sharedPreferences.getLong(OTA_REMINDER_TIME, 0);
        long curTime = System.currentTimeMillis();
        if (reminderTime == 0) {
            this.sharedPreferences.edit().putLong(OTA_REMINDER_TIME, INITIAL_REMINDER_INTERVAL + curTime).apply();
            sLogger.v(String.format("handleOTAUpdate() setting OTA reminder for +%d seconds ", new Object[]{Long.valueOf(TimeUnit.MILLISECONDS.toSeconds(INITIAL_REMINDER_INTERVAL))}));
        } else {
            sLogger.v(String.format("handleOTAUpdate() OTA reminder already set for +%d seconds", new Object[]{Long.valueOf(TimeUnit.MILLISECONDS.toSeconds(reminderTime - curTime))}));
        }
        scheduleReminder();
    }
}
