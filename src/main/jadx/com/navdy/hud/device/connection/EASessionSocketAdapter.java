package com.navdy.hud.device.connection;

import com.navdy.hud.mfi.EASession;
import com.navdy.service.library.device.NavdyDeviceId;
import com.navdy.service.library.device.NavdyDeviceId.Type;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.network.SocketAdapter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class EASessionSocketAdapter implements SocketAdapter {
    EASession eaSession;
    private Logger sLogger = new Logger(EASessionSocketAdapter.class);

    public EASessionSocketAdapter(EASession eaSession) {
        this.eaSession = eaSession;
    }

    public void connect() throws IOException {
        if (isConnected()) {
            this.sLogger.d("Connecting to existing eaSession");
            return;
        }
        throw new IOException("Can't create outbound EASession connections");
    }

    public void close() throws IOException {
        this.sLogger.d("closing connection");
        if (this.eaSession != null) {
            this.eaSession.close();
            this.eaSession = null;
        }
    }

    public InputStream getInputStream() throws IOException {
        if (this.eaSession != null) {
            return this.eaSession.getInputStream();
        }
        return null;
    }

    public OutputStream getOutputStream() throws IOException {
        if (this.eaSession != null) {
            return this.eaSession.getOutputStream();
        }
        return null;
    }

    public boolean isConnected() {
        return this.eaSession != null;
    }

    public NavdyDeviceId getRemoteDevice() {
        if (isConnected()) {
            return new NavdyDeviceId(Type.EA, this.eaSession.getMacAddress(), this.eaSession.getName());
        }
        return null;
    }
}
