package com.navdy.hud.mfi;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class ByteArrayBuilder {
    ByteArrayOutputStream bos;
    DataOutputStream dos;

    public ByteArrayBuilder() {
        this.bos = new ByteArrayOutputStream();
        this.dos = new DataOutputStream(this.bos);
    }

    public ByteArrayBuilder(int initialSize) {
        this.bos = new ByteArrayOutputStream(initialSize);
        this.dos = new DataOutputStream(this.bos);
    }

    public ByteArrayBuilder addInt16(int val) throws IOException {
        this.dos.writeShort(val);
        return this;
    }

    public ByteArrayBuilder addInt8(int val) throws IOException {
        this.dos.writeByte(val);
        return this;
    }

    public ByteArrayBuilder addInt32(int val) throws IOException {
        this.dos.writeInt(val);
        return this;
    }

    public ByteArrayBuilder addInt64(long val) throws IOException {
        this.dos.writeLong(val);
        return this;
    }

    public ByteArrayBuilder addUTF8(String s) throws IOException {
        this.dos.write(s.getBytes("UTF-8"));
        return this;
    }

    public ByteArrayBuilder addBlob(byte[] b) throws IOException {
        this.dos.write(b);
        return this;
    }

    public ByteArrayBuilder addBlob(byte[] b, int startOffset, int length) throws IOException {
        this.dos.write(b, startOffset, length);
        return this;
    }

    public byte[] build() {
        return this.bos.toByteArray();
    }

    public int size() {
        return this.bos.size();
    }
}
