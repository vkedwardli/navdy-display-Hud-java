package com.navdy.hud.mfi;

public interface IAPCommunicationsUpdateListener {
    void onCallStateUpdate(CallStateUpdate callStateUpdate);

    void onCommunicationUpdate(CommunicationUpdate communicationUpdate);
}
