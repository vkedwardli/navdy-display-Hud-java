package com.navdy.hud.mfi;

public interface EASessionPacketReceiver {
    void queue(EASessionPacket eASessionPacket);
}
