package com.navdy.hud.mfi;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import com.navdy.hud.app.util.CrashReportService;
import java.io.IOException;

public class AuthCoprocessor {
    private static final int ACCESSORY_CERTIFICATE_DATA_REGISTER = 49;
    private static final int ACCESSORY_CERTIFICATE_LENGTH_REGISTER = 48;
    private static final int ACCESSORY_SIGNATURE_GENERATION = 1;
    private static final int AUTH_ERROR_FLAG = 128;
    private static final int BUS = 0;
    private static final int CHALLENGE_DATA_REGISTER = 33;
    private static final int CHALLENGE_LENGTH_REGISTER = 32;
    private static final int DEVICE_ADDRESS = 17;
    private static final int DEVICE_CERTIFICATE_DATA_REGISTER = 81;
    private static final int DEVICE_CERTIFICATE_LENGTH_REGISTER = 80;
    private static final int DEVICE_CERTIFICATE_VALIDATION = 4;
    private static final int DEVICE_CHALLENGE_GENERATION = 2;
    private static final int DEVICE_SIGNATURE_VALIDATION = 3;
    private static final int ERROR_CODE_REGISTER = 5;
    private static final int PAGE_SIZE = 128;
    private static final int SIGNATURE_DATA_REGISTER = 18;
    private static final int SIGNATURE_LENGTH_REGISTER = 17;
    private static final int STATUS_CONTROL_REGISTER = 16;
    private static final String TAG = "AuthCoprocessor";
    private Context context;
    private boolean crashReportSent = false;
    private I2C i2c = new I2C(0);
    private IAPListener iapListener;

    public AuthCoprocessor(IAPListener listener, Context context) {
        if (listener == null) {
            this.iapListener = new IAPListener() {
                public void onCoprocessorStatusCheckFailed(String desc, String status, String errorCode) {
                }

                public void onDeviceAuthenticationSuccess(int retries) {
                }
            };
        } else {
            this.iapListener = listener;
        }
        this.context = context;
    }

    public void open() throws IOException {
        this.i2c.open();
    }

    public void close() {
        this.i2c.close();
    }

    private void sendCrashReport() {
        if (this.crashReportSent) {
            Log.e(TAG, "skipping duplicate crash report");
        } else if (this.context == null) {
            Log.e(TAG, "cannot sent crash report, null context");
        } else {
            try {
                Intent intent = new Intent(this.context, Class.forName("com.navdy.hud.app.util.CrashReportService"));
                intent.setAction("DumpCrashReport");
                intent.putExtra(CrashReportService.EXTRA_CRASH_TYPE, "MFI_ERROR");
                this.context.startService(intent);
                this.crashReportSent = true;
            } catch (Exception e) {
                Log.e(TAG, "error in lookup of CrashReportService class", e);
            }
        }
    }

    private void checkStatus(String desc) throws IOException {
        int status = this.i2c.readByte(17, 16);
        int err = this.i2c.readByte(17, 5);
        if ((status & 128) != 0) {
            String statusHex = Utils.bytesToHex(new byte[]{(byte) status});
            String errorHex = Utils.bytesToHex(new byte[]{(byte) err});
            Log.e(TAG, "auth CP status: " + statusHex);
            Log.e(TAG, "auth CP error code: " + errorHex);
            sendCrashReport();
            try {
                this.iapListener.onCoprocessorStatusCheckFailed(desc, statusHex, errorHex);
            } catch (Throwable e) {
                Log.e(TAG, "logging to listener failed", e);
            }
            throw new RuntimeException("failed to " + desc);
        }
    }

    private void writePages(int lengthRegister, int dataRegister, byte[] data, String desc) throws IOException {
        this.i2c.writeShort(17, lengthRegister, data.length);
        checkStatus("write " + desc + " length");
        int bytesToWrite = data.length;
        int offset = 0;
        byte[] buf = new byte[128];
        while (bytesToWrite > 0) {
            int len = Math.min(bytesToWrite, 128);
            System.arraycopy(data, offset, buf, 0, len);
            this.i2c.write(17, dataRegister, buf, 0, buf.length);
            checkStatus("write " + desc);
            dataRegister++;
            offset += len;
            bytesToWrite -= len;
        }
    }

    public byte[] readAccessoryCertificate() throws IOException {
        byte[] cert = new byte[this.i2c.readShort(17, 48)];
        this.i2c.read(17, 49, cert);
        checkStatus("read accessory certificate");
        return cert;
    }

    public byte[] createAccessorySignature(byte[] challenge) {
        try {
            if (challenge.length != 20) {
                throw new RuntimeException("wrong challenge size");
            }
            writePages(32, 33, challenge, "accessory signature");
            this.i2c.writeShort(17, 17, 128);
            this.i2c.writeByte(17, 16, 1);
            checkStatus("generate accessory signature");
            byte[] signature = new byte[this.i2c.readShort(17, 17)];
            this.i2c.read(17, 18, signature);
            checkStatus("read accessory signature");
            return signature;
        } catch (Exception e) {
            Log.e(TAG, "", e);
            return null;
        }
    }

    public byte[] validateDeviceCertificateAndGenerateDeviceChallenge(byte[] certificate) {
        try {
            writePages(80, DEVICE_CERTIFICATE_DATA_REGISTER, certificate, "device certificate");
            this.i2c.writeByte(17, 16, 4);
            checkStatus("validate device certificate");
            this.i2c.writeByte(17, 16, 2);
            checkStatus("generate device challenge");
            byte[] challenge = new byte[this.i2c.readShort(17, 32)];
            this.i2c.read(17, 33, challenge);
            checkStatus("read device challenge");
            return challenge;
        } catch (Exception e) {
            Log.e(TAG, "", e);
            return null;
        }
    }

    public boolean validateDeviceSignature(byte[] signature) {
        try {
            writePages(17, 18, signature, "device signature");
            this.i2c.writeByte(17, 16, 3);
            checkStatus("validate device signature");
            return true;
        } catch (Exception e) {
            Log.e(TAG, "", e);
            return false;
        }
    }
}
