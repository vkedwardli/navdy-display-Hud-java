package com.navdy.hud.mfi;

import java.io.IOException;

public class I2C {
    public static final int MAX_BLOCK_SIZE = 32;
    private int mBus;
    private int mFD = -1;

    private static native void classInitNative();

    private native void native_close();

    private native int native_open(int i);

    private native int native_read(int i, int i2, byte[] bArr, int i3, int i4);

    private native int native_write(int i, int i2, byte[] bArr, int i3, int i4);

    static {
        System.loadLibrary("iap2");
        classInitNative();
    }

    public I2C(int mBus) {
        this.mBus = mBus;
    }

    public void open() throws IOException {
        native_open(this.mBus);
        if (this.mFD < 0) {
            throw new IOException("Failed to open i2c device");
        }
    }

    public void close() {
        if (this.mFD >= 0) {
            native_close();
        }
    }

    public int read(int deviceAddress, int registerAddress, byte[] bytes, int offset, int len) {
        return native_read(deviceAddress, registerAddress, bytes, offset, len);
    }

    public int read(int deviceAddress, int registerAddress, byte[] bytes) {
        return read(deviceAddress, registerAddress, bytes, 0, bytes.length);
    }

    public int writeShort(int deviceAddress, int registerAddress, int shortVal) {
        byte[] bytes = new byte[]{(byte) ((shortVal >> 8) & 255), (byte) (shortVal & 255)};
        return write(deviceAddress, registerAddress, bytes, bytes.length);
    }

    public int writeByte(int deviceAddress, int registerAddress, int byteVal) {
        byte[] bytes = new byte[]{(byte) byteVal};
        return write(deviceAddress, registerAddress, bytes, bytes.length);
    }

    public int write(int deviceAddress, int registerAddress, byte[] bytes, int offset, int len) {
        return native_write(deviceAddress, registerAddress, bytes, 0, len);
    }

    public int write(int deviceAddress, int registerAddress, byte[] bytes, int len) {
        return native_write(deviceAddress, registerAddress, bytes, 0, len);
    }

    public int readShort(int deviceAddress, int registerAddress) throws IOException {
        byte[] bytes = new byte[2];
        if (read(deviceAddress, registerAddress, bytes) == 0) {
            return ((bytes[0] & 255) << 8) | (bytes[1] & 255);
        }
        throw new IOException("Unable to read short");
    }

    public int readByte(int deviceAddress, int registerAddress) throws IOException {
        byte[] bytes = new byte[1];
        if (read(deviceAddress, registerAddress, bytes) == 0) {
            return bytes[0] & 255;
        }
        throw new IOException("Unable to read byte");
    }
}
