package com.navdy.service.library.device.discovery;

class MDNSDeviceBroadcaster$1 implements android.net.nsd.NsdManager.RegistrationListener {
    final com.navdy.service.library.device.discovery.MDNSDeviceBroadcaster this$0;
    
    MDNSDeviceBroadcaster$1(com.navdy.service.library.device.discovery.MDNSDeviceBroadcaster a) {
        super();
        this.this$0 = a;
    }
    
    public void onRegistrationFailed(android.net.nsd.NsdServiceInfo a, int i) {
        com.navdy.service.library.device.discovery.MDNSDeviceBroadcaster.sLogger.d(new StringBuilder().append("SERVICE REGISTRATION FAILED").append(this.this$0.mServiceInfo.getServiceType()).toString());
    }
    
    public void onServiceRegistered(android.net.nsd.NsdServiceInfo a) {
        com.navdy.service.library.device.discovery.MDNSDeviceBroadcaster.sLogger.d(new StringBuilder().append("SERVICE REGISTERED: ").append(this.this$0.mServiceInfo.getServiceType()).toString());
    }
    
    public void onServiceUnregistered(android.net.nsd.NsdServiceInfo a) {
        com.navdy.service.library.device.discovery.MDNSDeviceBroadcaster.sLogger.d(new StringBuilder().append("SERVICE UNREGISTERED").append(this.this$0.mServiceInfo.getServiceType()).toString());
    }
    
    public void onUnregistrationFailed(android.net.nsd.NsdServiceInfo a, int i) {
        com.navdy.service.library.device.discovery.MDNSDeviceBroadcaster.sLogger.d(new StringBuilder().append("SERVICE UNREGISTRATION FAILED: ").append(this.this$0.mServiceInfo.getServiceType()).toString());
    }
}
