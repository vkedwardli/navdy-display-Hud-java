package com.navdy.service.library.device;

class RemoteDevice$7 {
    final static int[] $SwitchMap$com$navdy$service$library$device$RemoteDevice$LinkStatus;
    final static int[] $SwitchMap$com$navdy$service$library$device$connection$ConnectionType;
    
    static {
        $SwitchMap$com$navdy$service$library$device$connection$ConnectionType = new int[com.navdy.service.library.device.connection.ConnectionType.values().length];
        int[] a = $SwitchMap$com$navdy$service$library$device$connection$ConnectionType;
        com.navdy.service.library.device.connection.ConnectionType a0 = com.navdy.service.library.device.connection.ConnectionType.BT_PROTOBUF;
        try {
            a[a0.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException) {
        }
        try {
            $SwitchMap$com$navdy$service$library$device$connection$ConnectionType[com.navdy.service.library.device.connection.ConnectionType.TCP_PROTOBUF.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException0) {
        }
        try {
            $SwitchMap$com$navdy$service$library$device$connection$ConnectionType[com.navdy.service.library.device.connection.ConnectionType.EA_PROTOBUF.ordinal()] = 3;
        } catch(NoSuchFieldError ignoredException1) {
        }
        try {
            $SwitchMap$com$navdy$service$library$device$connection$ConnectionType[com.navdy.service.library.device.connection.ConnectionType.BT_IAP2_LINK.ordinal()] = 4;
        } catch(NoSuchFieldError ignoredException2) {
        }
        $SwitchMap$com$navdy$service$library$device$RemoteDevice$LinkStatus = new int[com.navdy.service.library.device.RemoteDevice$LinkStatus.values().length];
        int[] a1 = $SwitchMap$com$navdy$service$library$device$RemoteDevice$LinkStatus;
        com.navdy.service.library.device.RemoteDevice$LinkStatus a2 = com.navdy.service.library.device.RemoteDevice$LinkStatus.CONNECTED;
        try {
            a1[a2.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException3) {
        }
        try {
            $SwitchMap$com$navdy$service$library$device$RemoteDevice$LinkStatus[com.navdy.service.library.device.RemoteDevice$LinkStatus.DISCONNECTED.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException4) {
        }
        try {
            $SwitchMap$com$navdy$service$library$device$RemoteDevice$LinkStatus[com.navdy.service.library.device.RemoteDevice$LinkStatus.LINK_ESTABLISHED.ordinal()] = 3;
        } catch(NoSuchFieldError ignoredException5) {
        }
    }
}
