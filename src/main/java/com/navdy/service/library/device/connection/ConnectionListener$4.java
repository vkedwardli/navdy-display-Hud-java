package com.navdy.service.library.device.connection;

class ConnectionListener$4 implements com.navdy.service.library.device.connection.ConnectionListener$EventDispatcher {
    final com.navdy.service.library.device.connection.ConnectionListener this$0;
    final com.navdy.service.library.device.connection.Connection val$connection;
    
    ConnectionListener$4(com.navdy.service.library.device.connection.ConnectionListener a, com.navdy.service.library.device.connection.Connection a0) {
        super();
        this.this$0 = a;
        this.val$connection = a0;
    }
    
    public void dispatchEvent(com.navdy.service.library.device.connection.ConnectionListener a, com.navdy.service.library.device.connection.ConnectionListener$Listener a0) {
        a0.onConnected(a, this.val$connection);
    }
    
    public void dispatchEvent(com.navdy.service.library.util.Listenable a, com.navdy.service.library.util.Listenable$Listener a0) {
        this.dispatchEvent((com.navdy.service.library.device.connection.ConnectionListener)a, (com.navdy.service.library.device.connection.ConnectionListener$Listener)a0);
    }
}
