package com.navdy.service.library.device.discovery;

public class MDNSRemoteDeviceScanner extends com.navdy.service.library.device.discovery.RemoteDeviceScanner {
    final public static com.navdy.service.library.log.Logger sLogger;
    protected java.util.Timer discoveryTimer;
    protected android.net.nsd.NsdManager.DiscoveryListener mDiscoveryListener;
    protected boolean mDiscoveryRunning;
    protected android.os.Handler mHandler;
    protected android.net.nsd.NsdManager mNsdManager;
    protected java.util.ArrayList mResolveListeners;
    protected android.net.nsd.NsdServiceInfo mResolvingService;
    final protected String mServiceType;
    protected java.util.Queue mUnresolvedServices;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.service.library.device.discovery.MDNSRemoteDeviceScanner.class);
    }
    
    public MDNSRemoteDeviceScanner(android.content.Context a, String s) {
        super(a);
        this.mServiceType = s;
        this.mResolveListeners = new java.util.ArrayList();
        this.mUnresolvedServices = (java.util.Queue)new java.util.LinkedList();
        this.mHandler = new android.os.Handler(android.os.Looper.getMainLooper());
    }
    
    public android.net.nsd.NsdManager.ResolveListener getNewResolveListener() {
        return (android.net.nsd.NsdManager.ResolveListener)new com.navdy.service.library.device.discovery.MDNSRemoteDeviceScanner$3(this);
    }
    
    public void initNsd() {
        this.initializeDiscoveryListener();
    }
    
    public void initializeDiscoveryListener() {
        if (this.mNsdManager == null) {
            this.mNsdManager = (android.net.nsd.NsdManager)this.mContext.getSystemService("servicediscovery");
        }
        if (this.mDiscoveryListener == null) {
            this.mDiscoveryListener = (android.net.nsd.NsdManager.DiscoveryListener)new com.navdy.service.library.device.discovery.MDNSRemoteDeviceScanner$2(this);
        }
    }
    
    protected void resolveComplete() {
        this.mResolvingService = null;
        if (this.mUnresolvedServices.size() > 0) {
            this.resolveService((android.net.nsd.NsdServiceInfo)this.mUnresolvedServices.remove());
        }
    }
    
    protected void resolveService(android.net.nsd.NsdServiceInfo a) {
        if (this.mResolvingService != null) {
            this.mUnresolvedServices.add(a);
        } else {
            this.mResolvingService = a;
            android.net.nsd.NsdManager.ResolveListener a0 = this.getNewResolveListener();
            this.mResolveListeners.add(a0);
            this.mNsdManager.resolveService(a, a0);
        }
    }
    
    public boolean startScan() {
        boolean b = false;
        sLogger.e(new StringBuilder().append("Starting scan: ").append(this.mServiceType).toString());
        this.initNsd();
        label2: if (this.mNsdManager != null) {
            java.util.Timer a = this.discoveryTimer;
            label0: {
                label1: {
                    if (a != null) {
                        break label1;
                    }
                    if (!this.mDiscoveryRunning) {
                        break label0;
                    }
                }
                sLogger.e(new StringBuilder().append("Can't start: already started ").append(this.mServiceType).toString());
                b = true;
                break label2;
            }
            this.discoveryTimer = new java.util.Timer();
            this.discoveryTimer.schedule((java.util.TimerTask)new com.navdy.service.library.device.discovery.MDNSRemoteDeviceScanner$1(this), 2000L);
            b = true;
        } else {
            sLogger.e(new StringBuilder().append("Can't scan: no NsdManager: ").append(this.mServiceType).toString());
            b = false;
        }
        return b;
    }
    
    public boolean stopScan() {
        boolean b = false;
        sLogger.e(new StringBuilder().append("Stopping scan: ").append(this.mServiceType).toString());
        if (this.mNsdManager != null) {
            if (this.discoveryTimer == null) {
                if (this.mDiscoveryListener != null) {
                    try {
                        this.mNsdManager.stopServiceDiscovery(this.mDiscoveryListener);
                    } catch(Exception a) {
                        if (this.mDiscoveryRunning) {
                            sLogger.e("Problem stopping nsd service discovery", (Throwable)a);
                        }
                    }
                    this.mDiscoveryListener = null;
                    this.mDiscoveryRunning = false;
                }
                b = true;
            } else {
                this.discoveryTimer.cancel();
                this.discoveryTimer = null;
                b = true;
            }
        } else {
            sLogger.e(new StringBuilder().append("Can't stop: already stopped: ").append(this.mServiceType).toString());
            b = false;
        }
        return b;
    }
}
