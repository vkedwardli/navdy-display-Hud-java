package com.navdy.service.library.device.connection;

abstract public interface Connection$Listener extends com.navdy.service.library.util.Listenable$Listener {
    abstract public void onConnected(com.navdy.service.library.device.connection.Connection arg);
    
    
    abstract public void onConnectionFailed(com.navdy.service.library.device.connection.Connection arg, com.navdy.service.library.device.connection.Connection$ConnectionFailureCause arg0);
    
    
    abstract public void onDisconnected(com.navdy.service.library.device.connection.Connection arg, com.navdy.service.library.device.connection.Connection$DisconnectCause arg0);
}
