package com.navdy.service.library.device.connection;

class Connection$5 {
    final static int[] $SwitchMap$com$navdy$service$library$device$connection$ConnectionType;
    
    static {
        $SwitchMap$com$navdy$service$library$device$connection$ConnectionType = new int[com.navdy.service.library.device.connection.ConnectionType.values().length];
        int[] a = $SwitchMap$com$navdy$service$library$device$connection$ConnectionType;
        com.navdy.service.library.device.connection.ConnectionType a0 = com.navdy.service.library.device.connection.ConnectionType.BT_PROTOBUF;
        try {
            a[a0.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException) {
        }
        try {
            $SwitchMap$com$navdy$service$library$device$connection$ConnectionType[com.navdy.service.library.device.connection.ConnectionType.BT_TUNNEL.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException0) {
        }
        try {
            $SwitchMap$com$navdy$service$library$device$connection$ConnectionType[com.navdy.service.library.device.connection.ConnectionType.EA_PROTOBUF.ordinal()] = 3;
        } catch(NoSuchFieldError ignoredException1) {
        }
        try {
            $SwitchMap$com$navdy$service$library$device$connection$ConnectionType[com.navdy.service.library.device.connection.ConnectionType.BT_IAP2_LINK.ordinal()] = 4;
        } catch(NoSuchFieldError ignoredException2) {
        }
        try {
            $SwitchMap$com$navdy$service$library$device$connection$ConnectionType[com.navdy.service.library.device.connection.ConnectionType.TCP_PROTOBUF.ordinal()] = 5;
        } catch(NoSuchFieldError ignoredException3) {
        }
    }
}
