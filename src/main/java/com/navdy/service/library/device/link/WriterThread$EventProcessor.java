package com.navdy.service.library.device.link;

abstract public interface WriterThread$EventProcessor {
    abstract public byte[] processEvent(byte[] arg);
}
