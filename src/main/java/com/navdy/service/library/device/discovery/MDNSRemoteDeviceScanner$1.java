package com.navdy.service.library.device.discovery;

class MDNSRemoteDeviceScanner$1 extends java.util.TimerTask {
    final com.navdy.service.library.device.discovery.MDNSRemoteDeviceScanner this$0;
    
    MDNSRemoteDeviceScanner$1(com.navdy.service.library.device.discovery.MDNSRemoteDeviceScanner a) {
        super();
        this.this$0 = a;
    }
    
    public void run() {
        com.navdy.service.library.device.discovery.MDNSRemoteDeviceScanner.sLogger.e(new StringBuilder().append("Scan timer fired: ").append(this.this$0.mServiceType).toString());
        this.this$0.mNsdManager.discoverServices(this.this$0.mServiceType, 1, this.this$0.mDiscoveryListener);
        com.navdy.service.library.device.discovery.MDNSRemoteDeviceScanner.sLogger.e(new StringBuilder().append("Scan started: ").append(this.this$0.mServiceType).toString());
    }
}
