package com.navdy.service.library.util;

public class SystemUtils {
    final private static String COMMA = ",";
    final private static String PERCENTAGE = "%";
    final private static String SPACE = " ";
    final private static com.navdy.service.library.log.Logger sLogger;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.service.library.util.SystemUtils.class);
    }
    
    public SystemUtils() {
    }
    
    private static int extractNumberFromPercent(String s) {
        int i = 0;
        label0: {
            Throwable a = null;
            if (s == null) {
                i = 0;
                break label0;
            } else {
                try {
                    String s0 = s.trim();
                    int i0 = s0.indexOf("%");
                    if (i0 != -1) {
                        s0 = s0.substring(0, i0).trim();
                    }
                    i = Integer.parseInt(s0);
                    break label0;
                } catch(Throwable a0) {
                    a = a0;
                }
            }
            sLogger.e(a);
            i = 0;
        }
        return i;
    }
    
    public static com.navdy.service.library.util.SystemUtils$CpuInfo getCpuUsage() {
        com.navdy.service.library.util.SystemUtils$CpuInfo a = null;
        label1: {
            java.io.InputStream a0 = null;
            label0: {
                try {
                    a0 = null;
                    Process a1 = Runtime.getRuntime().exec("top -m 5 -t -n 1");
                    a1.waitFor();
                    a0 = a1.getInputStream();
                    java.io.BufferedReader a2 = new java.io.BufferedReader((java.io.Reader)new java.io.StringReader(com.navdy.service.library.util.IOUtils.convertInputStreamToString(a0, "UTF-8")));
                    java.util.ArrayList a3 = new java.util.ArrayList();
                    String s = null;
                    int i = 1;
                    String s0 = null;
                    while(true) {
                        String s1 = a2.readLine();
                        if (s1 == null) {
                            break;
                        }
                        if (s1.trim().length() != 0) {
                            if (i != 1) {
                                if (i >= 4) {
                                    java.util.StringTokenizer a4 = new java.util.StringTokenizer(s1, " ");
                                    String s2 = null;
                                    String s3 = null;
                                    int i0 = 1;
                                    String s4 = null;
                                    int i1 = 0;
                                    int i2 = 0;
                                    while(a4.hasMoreElements()) {
                                        String s5 = (String)a4.nextElement();
                                        switch(i0) {
                                            case 11: {
                                                s4 = s5;
                                                break;
                                            }
                                            case 10: {
                                                s2 = s5;
                                                break;
                                            }
                                            case 4: {
                                                s3 = s5;
                                                break;
                                            }
                                            case 2: {
                                                try {
                                                    i2 = Integer.parseInt(s5);
                                                    break;
                                                } catch(Throwable ignoredException) {
                                                    break;
                                                }
                                            }
                                            case 1: {
                                                try {
                                                    i1 = Integer.parseInt(s5);
                                                    break;
                                                } catch(Throwable ignoredException0) {
                                                    break;
                                                }
                                            }
                                            default: {
                                                break;
                                            }
                                            case 3: case 5: case 6: case 7: case 8: case 9: {
                                            }
                                        }
                                        i0 = i0 + 1;
                                    }
                                    a3.add(new com.navdy.service.library.util.SystemUtils$ProcessCpuInfo(i1, i2, s4, s2, com.navdy.service.library.util.SystemUtils.extractNumberFromPercent(s3)));
                                }
                            } else {
                                java.util.StringTokenizer a5 = new java.util.StringTokenizer(s1, ",");
                                while(a5.hasMoreElements()) {
                                    String s6 = ((String)a5.nextElement()).trim();
                                    int i3 = s6.indexOf(" ");
                                    {
                                        if (i3 < 0) {
                                            continue;
                                        }
                                        if (s == null) {
                                            s = s6.substring(i3 + 1).trim();
                                            continue;
                                        }
                                        s0 = s6.substring(i3 + 1).trim();
                                        break;
                                    }
                                }
                            }
                            i = i + 1;
                        }
                    }
                    a = new com.navdy.service.library.util.SystemUtils$CpuInfo(com.navdy.service.library.util.SystemUtils.extractNumberFromPercent(s), com.navdy.service.library.util.SystemUtils.extractNumberFromPercent(s0), a3);
                    break label0;
                } catch(Throwable ignoredException1) {
                }
                com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a0);
                a = null;
                break label1;
            }
            com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a0);
        }
        return a;
    }
    
    public static int getNativeProcessId(String s) {
        int i = 0;
        label4: {
            java.io.InputStream a = null;
            label2: {
                label3: {
                    label0: {
                        label1: try {
                            String s0 = null;
                            a = null;
                            Process a0 = Runtime.getRuntime().exec("ps");
                            a0.waitFor();
                            a = a0.getInputStream();
                            java.io.BufferedReader a1 = new java.io.BufferedReader((java.io.Reader)new java.io.StringReader(com.navdy.service.library.util.IOUtils.convertInputStreamToString(a, "UTF-8")));
                            while(true) {
                                s0 = a1.readLine();
                                if (s0 == null) {
                                    break label1;
                                }
                                if (s0.contains((CharSequence)s)) {
                                    break;
                                }
                            }
                            java.util.StringTokenizer a2 = new java.util.StringTokenizer(s0);
                            a2.nextElement();
                            i = Integer.parseInt(((String)a2.nextElement()).trim());
                            break label2;
                        } catch(Throwable ignoredException) {
                            break label0;
                        }
                        com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a);
                        break label3;
                    }
                    com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a);
                }
                i = -1;
                break label4;
            }
            com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a);
        }
        return i;
    }
    
    public static int getProcessId(android.content.Context a, String s) {
        Object a0 = ((android.app.ActivityManager)a.getSystemService("activity")).getRunningAppProcesses().iterator();
        while(true) {
            int i = 0;
            if (((java.util.Iterator)a0).hasNext()) {
                android.app.ActivityManager.RunningAppProcessInfo a1 = (android.app.ActivityManager.RunningAppProcessInfo)((java.util.Iterator)a0).next();
                if (!android.text.TextUtils.equals((CharSequence)a1.processName, (CharSequence)s)) {
                    continue;
                }
                i = a1.pid;
            } else {
                i = -1;
            }
            return i;
        }
    }
    
    public static String getProcessName(android.content.Context a, int i) {
        String s = null;
        java.util.List a0 = ((android.app.ActivityManager)a.getSystemService("activity")).getRunningAppProcesses();
        label2: {
            android.app.ActivityManager.RunningAppProcessInfo a1 = null;
            label0: {
                label1: {
                    if (a0 == null) {
                        break label1;
                    }
                    Object a2 = a0.iterator();
                    while(((java.util.Iterator)a2).hasNext()) {
                        a1 = (android.app.ActivityManager.RunningAppProcessInfo)((java.util.Iterator)a2).next();
                        if (a1 == null) {
                            continue;
                        }
                        if (a1.pid == i) {
                            break label0;
                        }
                    }
                }
                s = "";
                break label2;
            }
            s = a1.processName;
        }
        return s;
    }
    
    public static boolean isConnectedToCellNetwork(android.content.Context a) {
        android.net.NetworkInfo a0 = ((android.net.ConnectivityManager)a.getSystemService("connectivity")).getNetworkInfo(0);
        return a0 != null && a0.isConnected();
    }
    
    public static boolean isConnectedToNetwork(android.content.Context a) {
        boolean b = false;
        android.net.NetworkInfo a0 = ((android.net.ConnectivityManager)a.getSystemService("connectivity")).getActiveNetworkInfo();
        label2: {
            label0: {
                label1: {
                    if (a0 == null) {
                        break label1;
                    }
                    if (a0.isConnectedOrConnecting()) {
                        break label0;
                    }
                }
                b = false;
                break label2;
            }
            b = true;
        }
        return b;
    }
    
    public static boolean isConnectedToWifi(android.content.Context a) {
        boolean b = false;
        android.net.NetworkInfo a0 = ((android.net.ConnectivityManager)a.getSystemService("connectivity")).getNetworkInfo(1);
        label0: {
            label1: {
                if (a0 == null) {
                    break label1;
                }
                if (a0.isConnected()) {
                    b = true;
                    break label0;
                }
            }
            b = false;
        }
        return b;
    }
}
