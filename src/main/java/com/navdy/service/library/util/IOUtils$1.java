package com.navdy.service.library.util;

final class IOUtils$1 implements com.navdy.service.library.util.IOUtils$OnFileTraversal {
    final byte[] val$buffer;
    final java.security.MessageDigest val$mDigest;
    
    IOUtils$1(byte[] a, java.security.MessageDigest a0) {
        super();
        this.val$buffer = a;
        this.val$mDigest = a0;
    }
    
    public void onFileTraversal(java.io.File a) {
        label3: {
            java.io.FileInputStream a0 = null;
            Throwable a1 = null;
            label1: {
                long j = 0L;
                long j0 = 0L;
                label2: {
                    try {
                        j = a.length();
                        a0 = new java.io.FileInputStream(a);
                        j0 = 0L;
                        break label2;
                    } catch(Throwable a2) {
                        a1 = a2;
                    }
                    a0 = null;
                    break label1;
                }
                label0: {
                    try {
                        while(true) {
                            int i = a0.read(this.val$buffer, 0, (int)Math.min(j - j0, 1048576L));
                            this.val$mDigest.update(this.val$buffer, 0, i);
                            j0 = j0 + (long)i;
                            if (j0 >= j) {
                                break label0;
                            }
                            if (i == -1) {
                                break label0;
                            }
                        }
                    } catch(Throwable a3) {
                        a1 = a3;
                    }
                    break label1;
                }
                com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a0);
                break label3;
            }
            try {
                com.navdy.service.library.util.IOUtils.access$000().e("Exception while calculating md5 checksum for a path ", a1);
            } catch(Throwable a4) {
                com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a0);
                throw a4;
            }
            com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a0);
        }
    }
}
