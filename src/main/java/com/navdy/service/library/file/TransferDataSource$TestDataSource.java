package com.navdy.service.library.file;

class TransferDataSource$TestDataSource extends com.navdy.service.library.file.TransferDataSource {
    final static int DATA_BUFFER_LEN = 4096;
    byte[] mDataBuffer;
    long mLength;
    
    TransferDataSource$TestDataSource(long j) {
        this.mLength = j;
        this.mDataBuffer = new byte[4096];
        new java.util.Random().nextBytes(this.mDataBuffer);
    }
    
    public String checkSum() {
        return this.checkSum(this.mLength);
    }
    
    public String checkSum(long j) {
        String s = null;
        if (j > this.mLength) {
            j = this.mLength;
        }
        try {
            java.security.MessageDigest a = java.security.MessageDigest.getInstance("MD5");
            long j0 = 0L;
            while(j0 < j) {
                long j1 = j - j0;
                int i = (j1 >= 4096L) ? 4096 : (int)j1;
                a.update(this.mDataBuffer, 0, i);
                j0 = j0 + (long)i;
            }
            s = com.navdy.service.library.util.IOUtils.bytesToHexString(a.digest());
        } catch(java.security.NoSuchAlgorithmException ignoredException) {
            com.navdy.service.library.file.TransferDataSource.access$000().e("unable to get MD5 algorithm");
            s = null;
        }
        return s;
    }
    
    public java.io.File getFile() {
        return null;
    }
    
    public String getName() {
        return "<TESTDATA>";
    }
    
    public long length() {
        return this.mLength;
    }
    
    public int read(byte[] a) {
        int i = (int)(this.mLength % 4096L);
        long j = this.mLength - this.mCurOffset;
        int i0 = (j < (long)a.length) ? (int)j : a.length;
        int i1 = 0;
        while(i1 < i0) {
            int i2 = this.mDataBuffer[i];
            int i3 = (byte)i2;
            a[i1] = (byte)i3;
            i = (i + 1) % 4096;
            i1 = i1 + 1;
        }
        return i0;
    }
}
