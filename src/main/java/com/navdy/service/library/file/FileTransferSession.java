package com.navdy.service.library.file;

import com.navdy.service.library.events.file.FileTransferError;
import com.navdy.service.library.util.IOUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class FileTransferSession {
    final public static int CHUNK_SIZE = 131072;
    final public static int PULL_CHUNK_SIZE = 16384;
    final private static com.navdy.service.library.log.Logger sLogger;
    private long mAlreadyTransferred;
    java.security.MessageDigest mDataCheckSum;
    String mDestinationFolder;
    int mExpectedChunk;
    java.io.File mFile;
    String mFileName;
    private java.io.FileInputStream mFileReader;
    long mFileSize;
    private com.navdy.service.library.events.file.FileType mFileType;
    private boolean mIsTestTransfer;
    private long mLastActivity;
    private boolean mPullRequest;
    private byte[] mReadBuffer;
    long mTotalBytesTransferred;
    int mTransferId;
    private boolean mWaitForAcknowledgements;
    long negotiatedOffset;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.service.library.file.FileTransferSession.class);
    }
    
    public FileTransferSession(int i) {
        this.mTransferId = -1;
        this.negotiatedOffset = -1L;
        this.mExpectedChunk = 0;
        this.mLastActivity = 0L;
        this.mPullRequest = false;
        this.mIsTestTransfer = false;
        this.mTransferId = i;
    }
    
    private com.navdy.service.library.events.file.FileTransferStatus verifyTestDataChunk(android.content.Context a, com.navdy.service.library.events.file.FileTransferData a0, com.navdy.service.library.events.file.FileTransferStatus$Builder a1) {
        com.navdy.service.library.events.file.FileTransferStatus a2 = null;
        label3: synchronized(this) {
            if (a0.chunkIndex.intValue() == this.mExpectedChunk) {
                if (a0.dataBytes != null && a0.dataBytes.size() > 0) {
                    this.mTotalBytesTransferred = this.mTotalBytesTransferred + (long)a0.dataBytes.size();
                    this.mDataCheckSum.update(a0.dataBytes.toByteArray());
                }
                a1.totalBytesTransferred(Long.valueOf(this.mTotalBytesTransferred));
                this.mExpectedChunk = this.mExpectedChunk + 1;
                boolean b = a0.lastChunk.booleanValue();
                label1: {
                    String s = null;
                    label0: {
                        label2: {
                            if (!b) {
                                break label2;
                            }
                            if (this.mTotalBytesTransferred != this.mFileSize) {
                                break label1;
                            }
                            s = com.navdy.service.library.util.IOUtils.bytesToHexString(this.mDataCheckSum.digest());
                            if (!s.equals(a0.fileCheckSum)) {
                                break label0;
                            }
                            sLogger.d(new StringBuilder().append("Received last chunk, Transfer complete, final data size ").append(this.mFileSize).toString());
                            a1.transferComplete(Boolean.valueOf(true));
                        }
                        a2 = a1.success(Boolean.valueOf(true)).build();
                        break label3;
                    }
                    com.navdy.service.library.log.Logger a3 = sLogger;
                    Object[] a4 = new Object[2];
                    a4[0] = a0.fileCheckSum;
                    a4[1] = s;
                    a3.e(String.format("Received last chunk but the checksum did not match. expected = \"%s\", actual = \"%s\"", a4));
                    a2 = a1.error(com.navdy.service.library.events.file.FileTransferError.FILE_TRANSFER_CHECKSUM_ERROR).build();
                    break label3;
                }
                sLogger.e("Not the last chunk, data missing. Did not receive full file");
                a2 = a1.error(com.navdy.service.library.events.file.FileTransferError.FILE_TRANSFER_ILLEGAL_CHUNK).build();
            } else {
                sLogger.e(new StringBuilder().append("Illegal chunk,  Expected :").append(this.mExpectedChunk).append(", received:").append(a0.chunkIndex).toString());
                a2 = a1.error(com.navdy.service.library.events.file.FileTransferError.FILE_TRANSFER_ILLEGAL_CHUNK).build();
            }
        }
        /*monexit(this)*/;
        return a2;
    }

    public synchronized com.navdy.service.library.events.file.FileTransferStatus appendChunk(android.content.Context context, com.navdy.service.library.events.file.FileTransferData data) {
        com.navdy.service.library.events.file.FileTransferStatus verifyTestDataChunk = null;
        IOException e;
        Throwable th;
        this.mLastActivity = System.currentTimeMillis();
        FileOutputStream outputStream = null;
        com.navdy.service.library.events.file.FileTransferStatus$Builder responseBuilder = new com.navdy.service.library.events.file.FileTransferStatus$Builder().transferId(Integer.valueOf(this.mTransferId)).totalBytesTransferred(Long.valueOf(0)).transferComplete(Boolean.valueOf(false)).success(Boolean.valueOf(false)).chunkIndex(data.chunkIndex);
        if (this.mIsTestTransfer) {
            verifyTestDataChunk = verifyTestDataChunk(context, data, responseBuilder);
        } else {
            File file = new File(this.mDestinationFolder, this.mFileName);
            if (!file.exists()) {
                sLogger.e("Session not initiated ");
                verifyTestDataChunk = responseBuilder.error(FileTransferError.FILE_TRANSFER_NOT_INITIATED).build();
            } else if (data.chunkIndex.intValue() != this.mExpectedChunk) {
                sLogger.e("Illegal chunk,  Expected :" + this.mExpectedChunk + ", received:" + data.chunkIndex);
                verifyTestDataChunk = responseBuilder.error(FileTransferError.FILE_TRANSFER_ILLEGAL_CHUNK).build();
            } else {
                try {
                    sLogger.v(String.format("Starting to write chunk %d into file %s from folder %s", new Object[]{data.chunkIndex, file.getName(), file.getAbsolutePath()}));
                    if (data.dataBytes != null && data.dataBytes.size() > 0) {
                        FileOutputStream outputStream2 = new FileOutputStream(file, true);
                        try {
                            outputStream2.write(data.dataBytes.toByteArray());
                            outputStream = outputStream2;
                        } catch (IOException e2) {
                            e = e2;
                            outputStream = outputStream2;
                            sLogger.e("Error writing the chunk to the file", e);
                            verifyTestDataChunk = responseBuilder.error(FileTransferError.FILE_TRANSFER_IO_ERROR).build();
                            IOUtils.fileSync(outputStream);
                            IOUtils.closeStream(outputStream);
                            return verifyTestDataChunk;
                        } catch (Throwable th3) {
                            outputStream = outputStream2;
                            IOUtils.fileSync(outputStream);
                            IOUtils.closeStream(outputStream);
                            throw th3;
                        }
                    }
                    responseBuilder.totalBytesTransferred(Long.valueOf(file.length()));
                    this.mExpectedChunk++;
                    if (data.lastChunk.booleanValue()) {
                        if (file.length() != this.mFileSize) {
                            sLogger.e("Not the last chunk, data missing. Did not receive full file");
                            verifyTestDataChunk = responseBuilder.error(FileTransferError.FILE_TRANSFER_ILLEGAL_CHUNK).build();
                            IOUtils.fileSync(outputStream);
                            IOUtils.closeStream(outputStream);
                        } else {
                            String finalChecksumForTheFile = IOUtils.hashForFile(file);
                            if (finalChecksumForTheFile.equals(data.fileCheckSum)) {
                                sLogger.d("Written last chunk to the file , Transfer complete, final file size " + file.length());
                                responseBuilder.transferComplete(Boolean.valueOf(true));
                            } else {
                                sLogger.e(String.format("Received last chunk but the checksum did not match. expected = \"%s\", actual = \"%s\"", new Object[]{data.fileCheckSum, finalChecksumForTheFile}));
                                verifyTestDataChunk = responseBuilder.error(FileTransferError.FILE_TRANSFER_CHECKSUM_ERROR).build();
                                IOUtils.fileSync(outputStream);
                                IOUtils.closeStream(outputStream);
                            }
                        }
                    }
                    IOUtils.fileSync(outputStream);
                    IOUtils.closeStream(outputStream);
                    verifyTestDataChunk = responseBuilder.success(Boolean.valueOf(true)).build();
                } catch (IOException e3) {
                    e = e3;
                }
            }
        }
        return verifyTestDataChunk;
    }

//    public com.navdy.service.library.events.file.FileTransferStatus appendChunk(android.content.Context a, com.navdy.service.library.events.file.FileTransferData a0) {
//        com.navdy.service.library.events.file.FileTransferStatus a1 = null;
//        label0: synchronized(this) {
//            java.io.FileOutputStream a2 = null;
//            this.mLastActivity = System.currentTimeMillis();
//            com.navdy.service.library.events.file.FileTransferStatus$Builder a3 = new com.navdy.service.library.events.file.FileTransferStatus$Builder().transferId(Integer.valueOf(this.mTransferId)).totalBytesTransferred(Long.valueOf(0L)).transferComplete(Boolean.valueOf(false)).success(Boolean.valueOf(false)).chunkIndex(a0.chunkIndex);
//            boolean b = this.mIsTestTransfer;
//            label10: {
//                java.io.FileOutputStream a4 = null;
//                label1: {
//                    java.io.FileOutputStream a6 = null;
//                    label7: {
//                        if (b) {
//                            a1 = this.verifyTestDataChunk(a, a0, a3);
//                            break label0;
//                        } else {
//                            java.io.File a7 = new java.io.File(this.mDestinationFolder, this.mFileName);
//                            if (a7.exists()) {
//                                if (a0.chunkIndex.intValue() == this.mExpectedChunk) {
//                                    label5: {
//                                        label3: {
//                                            label6: {
//                                                java.io.IOException a8 = null;
//                                                label9: try {
//                                                    com.navdy.service.library.log.Logger a9 = sLogger;
//                                                    a4 = null;
//                                                    a2 = null;
//                                                    Object[] a10 = new Object[3];
//                                                    a10[0] = a0.chunkIndex;
//                                                    a10[1] = a7.getName();
//                                                    a10[2] = a7.getAbsolutePath();
//                                                    a9.v(String.format("Starting to write chunk %d into file %s from folder %s", a10));
//                                                    okio.ByteString a11 = a0.dataBytes;
//                                                    a6 = null;
//                                                    label8: {
//                                                        if (a11 == null) {
//                                                            break label8;
//                                                        }
//                                                        a4 = null;
//                                                        a2 = null;
//                                                        int i = a0.dataBytes.size();
//                                                        a6 = null;
//                                                        if (i <= 0) {
//                                                            break label8;
//                                                        }
//                                                        a4 = null;
//                                                        a2 = null;
//                                                        a6 = new java.io.FileOutputStream(a7, true);
//                                                        try {
//                                                            a6.write(a0.dataBytes.toByteArray());
//                                                            break label8;
//                                                        } catch(java.io.IOException a12) {
//                                                            a8 = a12;
//                                                        }
//                                                        a2 = a6;
//                                                        break label9;
//                                                    }
//                                                    a4 = a6;
//                                                    a2 = a6;
//                                                    a3.totalBytesTransferred(Long.valueOf(a7.length()));
//                                                    this.mExpectedChunk = this.mExpectedChunk + 1;
//                                                    if (!a0.lastChunk.booleanValue()) {
//                                                        break label6;
//                                                    }
//                                                    a4 = a6;
//                                                    a2 = a6;
//                                                    long j = a7.length();
//                                                    long j0 = this.mFileSize;
//                                                    int i0 = (j < j0) ? -1 : (j == j0) ? 0 : 1;
//                                                    label4: {
//                                                        if (i0 == 0) {
//                                                            break label4;
//                                                        }
//                                                        com.navdy.service.library.log.Logger a14 = sLogger;
//                                                        a4 = a6;
//                                                        a2 = a6;
//                                                        a14.e("Not the last chunk, data missing. Did not receive full file");
//                                                        a1 = a3.error(com.navdy.service.library.events.file.FileTransferError.FILE_TRANSFER_ILLEGAL_CHUNK).build();
//                                                        break label5;
//                                                    }
//                                                    a4 = a6;
//                                                    a2 = a6;
//                                                    String s = com.navdy.service.library.util.IOUtils.hashForFile(a7);
//                                                    boolean b0 = s.equals(a0.fileCheckSum);
//                                                    label2: {
//                                                        if (b0) {
//                                                            break label2;
//                                                        }
//                                                        com.navdy.service.library.log.Logger a15 = sLogger;
//                                                        a4 = a6;
//                                                        a2 = a6;
//                                                        Object[] a16 = new Object[2];
//                                                        a16[0] = a0.fileCheckSum;
//                                                        a16[1] = s;
//                                                        a15.e(String.format("Received last chunk but the checksum did not match. expected = \"%s\", actual = \"%s\"", a16));
//                                                        a1 = a3.error(com.navdy.service.library.events.file.FileTransferError.FILE_TRANSFER_CHECKSUM_ERROR).build();
//                                                        break label3;
//                                                    }
//                                                    com.navdy.service.library.log.Logger a17 = sLogger;
//                                                    a4 = a6;
//                                                    a2 = a6;
//                                                    a17.d(new StringBuilder().append("Written last chunk to the file , Transfer complete, final file size ").append(a7.length()).toString());
//                                                    a3.transferComplete(Boolean.valueOf(true));
//                                                    break label6;
//                                                } catch(java.io.IOException a18) {
//                                                    a8 = a18;
//                                                }
//                                                com.navdy.service.library.log.Logger a19 = sLogger;
//                                                a4 = a2;
//                                                a19.e("Error writing the chunk to the file", (Throwable)a8);
//                                                a1 = a3.error(com.navdy.service.library.events.file.FileTransferError.FILE_TRANSFER_IO_ERROR).build();
//                                                break label10;
//                                            }
//                                            com.navdy.service.library.util.IOUtils.fileSync(a6);
//                                            com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a6);
//                                            a1 = a3.success(Boolean.valueOf(true)).build();
//                                            break label0;
//                                        }
//                                        com.navdy.service.library.util.IOUtils.fileSync(a6);
//                                        com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a6);
//                                        break label0;
//                                    }
//                                    com.navdy.service.library.util.IOUtils.fileSync(a6);
//                                    com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a6);
//                                    break label0;
//                                } else {
//                                    sLogger.e(new StringBuilder().append("Illegal chunk,  Expected :").append(this.mExpectedChunk).append(", received:").append(a0.chunkIndex).toString());
//                                    a1 = a3.error(com.navdy.service.library.events.file.FileTransferError.FILE_TRANSFER_ILLEGAL_CHUNK).build();
//                                    break label0;
//                                }
//                            } else {
//                                sLogger.e("Session not initiated ");
//                                a1 = a3.error(com.navdy.service.library.events.file.FileTransferError.FILE_TRANSFER_NOT_INITIATED).build();
//                                break label0;
//                            }
//                        }
//                    }
//                    a4 = a6;
//                }
//                com.navdy.service.library.util.IOUtils.fileSync(a4);
//                com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a4);
//            }
//            com.navdy.service.library.util.IOUtils.fileSync(a2);
//            com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a2);
//        }
//        /*monexit(this)*/;
//        return a1;
//    }
    
    public void endSession(android.content.Context a, boolean b) {
        synchronized(this) {
            com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)this.mFileReader);
            if (this.mPullRequest && b) {
                com.navdy.service.library.util.IOUtils.deleteFile(a, this.mFile.getAbsolutePath());
            }
            this.mReadBuffer = null;
        }
        /*monexit(this)*/;
    }
    
    public com.navdy.service.library.events.file.FileType getFileType() {
        return this.mFileType;
    }
    
    public long getLastActivity() {
        long j = 0L;
        synchronized(this) {
            j = this.mLastActivity;
        }
        /*monexit(this)*/;
        return j;
    }
    
    public com.navdy.service.library.events.file.FileTransferData getNextChunk() {
        com.navdy.service.library.events.file.FileTransferData a = null;
        label0: synchronized(this) {
            try {
                int i = 0;
                boolean b = false;
                int i0 = this.mExpectedChunk;
                this.mExpectedChunk = i0 + 1;
                long j = this.mFileSize - this.mAlreadyTransferred;
                if (j < 0L) {
                    throw new java.io.EOFException("End of file, no more chunk");
                }
                if (j >= 16384L) {
                    i = 16384;
                    b = false;
                } else {
                    i = (int)j;
                    b = true;
                }
                byte[] a1 = this.mReadBuffer;
                label1: {
                    label2: {
                        if (a1 == null) {
                            break label2;
                        }
                        if (this.mReadBuffer.length == i) {
                            break label1;
                        }
                    }
                    this.mReadBuffer = new byte[i];
                }
                int i1 = this.mFileReader.read(this.mReadBuffer);
                this.mAlreadyTransferred = this.mAlreadyTransferred + (long)i1;
                a = new com.navdy.service.library.events.file.FileTransferData$Builder().transferId(Integer.valueOf(this.mTransferId)).chunkIndex(Integer.valueOf(i0)).dataBytes(okio.ByteString.of(this.mReadBuffer)).lastChunk(Boolean.valueOf(b)).build();
                break label0;
            } catch(Throwable a2) {
                sLogger.e("Exception while reading the next chunk ", a2);
                com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)this.mFileReader);
                a = null;
                break label0;
            }
        }
        /*monexit(this)*/;
        return a;
    }
    
    public boolean handleFileTransferStatus(com.navdy.service.library.events.file.FileTransferStatus a) {
        boolean b = false;
        label2: synchronized(this) {
            label0: {
                label1: {
                    int i = 0;
                    if (a == null) {
                        break label1;
                    }
                    try {
                        if (!a.success.booleanValue()) {
                            break label1;
                        }
                        if (a.chunkIndex.intValue() != this.mExpectedChunk - 1) {
                            break label1;
                        }
                        long j = a.totalBytesTransferred.longValue();
                        long j0 = this.mAlreadyTransferred;
                        i = (j < j0) ? -1 : (j == j0) ? 0 : 1;
                    } catch(Throwable a0) {
                        /*monexit(this)*/;
                        throw a0;
                    }
                    if (i == 0) {
                        break label0;
                    }
                }
                b = false;
                break label2;
            }
            b = true;
        }
        /*monexit(this)*/;
        return b;
    }
    
    public com.navdy.service.library.events.file.FileTransferResponse initFileTransfer(android.content.Context a, com.navdy.service.library.events.file.FileType a0, String s, String s0, long j, long j0, String s1, boolean b) {
        com.navdy.service.library.events.file.FileTransferResponse a1 = null;
        label0: synchronized(this) {
            synchronized(this) {
                this.mLastActivity = System.currentTimeMillis();
                /*monexit(this)*/;
            }
            this.mDestinationFolder = s;
            this.mFileName = s0;
            this.mFileSize = j;
            this.mFileType = a0;
            synchronized(this) {
                this.mExpectedChunk = 0;
                /*monexit(this)*/;
            }
            java.io.File a10 = new java.io.File(s);
            com.navdy.service.library.events.file.FileTransferResponse$Builder a11 = new com.navdy.service.library.events.file.FileTransferResponse$Builder();
            a11.fileType(a0).destinationFileName(s0);
            a11.transferId(Integer.valueOf(this.mTransferId));
            try {
                long j1 = 0L;
                java.io.File a12 = new java.io.File(a10, s0);
                boolean b0 = a12.exists();
                label4: {
                    java.io.FileOutputStream a13 = null;
                    Throwable a14 = null;
                    java.nio.channels.FileChannel a15 = null;
                    label6: {
                        if (b0) {
                            break label6;
                        }
                        sLogger.d("File does not exists, creating new file");
                        if (a12.createNewFile()) {
                            j1 = 0L;
                            break label4;
                        }
                        sLogger.d("Permission denied");
                        a1 = a11.error(com.navdy.service.library.events.file.FileTransferError.FILE_TRANSFER_PERMISSION_DENIED).success(Boolean.valueOf(false)).build();
                        break label0;
                    }
                    boolean b1 = a12.canWrite();
                    label5: {
                        if (b1) {
                            break label5;
                        }
                        a1 = a11.error(com.navdy.service.library.events.file.FileTransferError.FILE_TRANSFER_PERMISSION_DENIED).success(Boolean.valueOf(false)).build();
                        break label0;
                    }
                    long j2 = a12.length();
                    sLogger.d(new StringBuilder().append("Requested offset :").append(j0).append(" , Already existing file size:").append(j2).append(", override :").append(b).toString());
                    if (j0 > j2) {
                        j1 = j2;
                    } else {
                        j1 = b ? j0 : j2;
                        if (j1 > 0L && !com.navdy.service.library.util.IOUtils.hashForFile(a12, j0).equals(s1)) {
                            sLogger.e("Checksum mismatch, local file does not match remote file");
                            j1 = 0L;
                        }
                    }
                    sLogger.d(new StringBuilder().append("New proposed offset :").append(j1).toString());
                    long j3 = j2 - j1;
                    if (((j3 < 0L) ? -1 : (j3 == 0L) ? 0 : 1) <= 0) {
                        break label4;
                    }
                    label2: {
                        label3: {
                            try {
                                a13 = new java.io.FileOutputStream(a12, true);
                            } catch(Throwable a16) {
                                a14 = a16;
                                break label3;
                            }
                            try {
                                a15 = null;
                                a15 = a13.getChannel();
                                a15.truncate(j1);
                            } catch(Throwable a17) {
                                a14 = a17;
                                break label2;
                            }
                            com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a13);
                            com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a15);
                            sLogger.e(new StringBuilder().append("Truncated the file , previous size :").append(j2).append(", new size :").append(a12.length()).toString());
                            break label4;
                        }
                        a13 = null;
                        a15 = null;
                    }
                    try {
                        sLogger.e(new StringBuilder().append("Exception truncating the file to offset ").append(j1).toString(), a14);
                        a1 = a11.error(com.navdy.service.library.events.file.FileTransferError.FILE_TRANSFER_IO_ERROR).success(Boolean.valueOf(false)).build();
                    } catch(Throwable a18) {
                        com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a13);
                        com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a15);
                        throw a18;
                    }
                    com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a13);
                    com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a15);
                    break label0;
                }
                this.negotiatedOffset = j1;
                sLogger.d(new StringBuilder().append("Final offset proposed ").append(this.negotiatedOffset).toString());
                long j4 = j - this.negotiatedOffset;
                long j5 = com.navdy.service.library.util.IOUtils.getFreeSpace(s);
                int i = (j5 < j4) ? -1 : (j5 == j4) ? 0 : 1;
                label1: {
                    if (i > 0) {
                        break label1;
                    }
                    sLogger.d(new StringBuilder().append("Not enough space to write the file, space required :").append(j4).toString());
                    a1 = a11.error(com.navdy.service.library.events.file.FileTransferError.FILE_TRANSFER_IO_ERROR).success(Boolean.valueOf(false)).build();
                    break label0;
                }
                if (this.negotiatedOffset != j0) {
                    long j6 = this.negotiatedOffset;
                    int i0 = (j6 < 0L) ? -1 : (j6 == 0L) ? 0 : 1;
                    String s2 = null;
                    s1 = (i0 <= 0) ? s2 : com.navdy.service.library.util.IOUtils.hashForFile(a12, this.negotiatedOffset);
                }
            } catch(Exception a19) {
                sLogger.e("Error processing FileTransferRequest ", (Throwable)a19);
                a1 = a11.error(com.navdy.service.library.events.file.FileTransferError.FILE_TRANSFER_IO_ERROR).success(Boolean.valueOf(false)).build();
                break label0;
            }
            a11.maxChunkSize(Integer.valueOf(131072)).offset(Long.valueOf(this.negotiatedOffset));
            sLogger.d("FileTransferRequest successful");
            a11.success(Boolean.valueOf(true));
            if (s1 != null) {
                a11.checksum(s1);
            }
            a1 = a11.build();
        }
        /*monexit(this)*/;
        return a1;
    }
    
    public com.navdy.service.library.events.file.FileTransferResponse initPull(android.content.Context a, com.navdy.service.library.events.file.FileType a0, String s, String s0, boolean b) {
        com.navdy.service.library.events.file.FileTransferResponse a1 = null;
        label1: synchronized(this) {
            com.navdy.service.library.events.file.FileTransferResponse$Builder a2 = new com.navdy.service.library.events.file.FileTransferResponse$Builder();
            this.mWaitForAcknowledgements = b;
            this.mFileType = a0;
            a2.supportsAcks(Boolean.valueOf(b));
            a2.destinationFileName(s0).fileType(a0);
            try {
                synchronized(this) {
                    this.mLastActivity = System.currentTimeMillis();
                    /*monexit(this)*/;
                }
                this.mDestinationFolder = s;
                this.mFileName = s0;
                this.mFile = new java.io.File(s, this.mFileName);
                this.mPullRequest = true;
                boolean b0 = this.mFile.exists();
                label0: {
                    if (!b0) {
                        break label0;
                    }
                    if (!this.mFile.canRead()) {
                        break label0;
                    }
                    if (this.mFile.length() <= 0L) {
                        break label0;
                    }
                    this.mFileSize = this.mFile.length();
                    this.mFileReader = new java.io.FileInputStream(this.mFile);
                    a1 = a2.success(Boolean.valueOf(true)).maxChunkSize(Integer.valueOf(16384)).transferId(Integer.valueOf(this.mTransferId)).offset(Long.valueOf(this.mFileSize)).build();
                    break label1;
                }
                a1 = a2.success(Boolean.valueOf(false)).error(com.navdy.service.library.events.file.FileTransferError.FILE_TRANSFER_IO_ERROR).build();
            } catch(Throwable ignoredException) {
                a1 = a2.success(Boolean.valueOf(false)).error(com.navdy.service.library.events.file.FileTransferError.FILE_TRANSFER_IO_ERROR).build();
            }
        }
        /*monexit(this)*/;
        return a1;
    }
    
    public com.navdy.service.library.events.file.FileTransferResponse initTestData(android.content.Context a, com.navdy.service.library.events.file.FileType a0, String s, long j) {
        com.navdy.service.library.events.file.FileTransferResponse a1 = null;
        label1: synchronized(this) {
            this.mLastActivity = System.currentTimeMillis();
            this.mDestinationFolder = "";
            this.mFileName = s;
            this.mFileSize = j;
            this.mFileType = a0;
            this.mExpectedChunk = 0;
            this.mTotalBytesTransferred = 0L;
            this.negotiatedOffset = 0L;
            this.mIsTestTransfer = true;
            com.navdy.service.library.events.file.FileTransferResponse$Builder a2 = new com.navdy.service.library.events.file.FileTransferResponse$Builder();
            a2.fileType(a0).destinationFileName(s);
            a2.maxChunkSize(Integer.valueOf(131072)).offset(Long.valueOf(this.negotiatedOffset));
            a2.transferId(Integer.valueOf(this.mTransferId));
            label0: {
                com.navdy.service.library.log.Logger a3 = null;
                try {
                    this.mDataCheckSum = java.security.MessageDigest.getInstance("MD5");
                    a3 = sLogger;
                } catch(java.security.NoSuchAlgorithmException ignoredException) {
                    break label0;
                }
                a3.d("FileTransferRequest successful");
                a2.success(Boolean.valueOf(true));
                a1 = a2.build();
                break label1;
            }
            sLogger.e("unable to create MD5 message digest");
            a1 = a2.error(com.navdy.service.library.events.file.FileTransferError.FILE_TRANSFER_CHECKSUM_ERROR).success(Boolean.valueOf(false)).build();
        }
        /*monexit(this)*/;
        return a1;
    }
    
    public boolean isFlowControlEnabled() {
        return this.mWaitForAcknowledgements;
    }
}
