package com.navdy.service.library.file;

abstract public interface IFileTransferManager {
    abstract public com.navdy.service.library.events.file.FileType getFileType(int arg);
    
    
    abstract public com.navdy.service.library.events.file.FileTransferData getNextChunk(int arg);
    
    
    abstract public com.navdy.service.library.events.file.FileTransferStatus handleFileTransferData(com.navdy.service.library.events.file.FileTransferData arg);
    
    
    abstract public com.navdy.service.library.events.file.FileTransferResponse handleFileTransferRequest(com.navdy.service.library.events.file.FileTransferRequest arg);
    
    
    abstract public boolean handleFileTransferStatus(com.navdy.service.library.events.file.FileTransferStatus arg);
    
    
    abstract public void stop();
}
