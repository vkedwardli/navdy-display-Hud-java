package com.navdy.service.library.log;

abstract public interface LogAppender {
    abstract public void close();
    
    
    abstract public void d(String arg, String arg0);
    
    
    abstract public void d(String arg, String arg0, Throwable arg1);
    
    
    abstract public void e(String arg, String arg0);
    
    
    abstract public void e(String arg, String arg0, Throwable arg1);
    
    
    abstract public void flush();
    
    
    abstract public void i(String arg, String arg0);
    
    
    abstract public void i(String arg, String arg0, Throwable arg1);
    
    
    abstract public void v(String arg, String arg0);
    
    
    abstract public void v(String arg, String arg0, Throwable arg1);
    
    
    abstract public void w(String arg, String arg0);
    
    
    abstract public void w(String arg, String arg0, Throwable arg1);
}
