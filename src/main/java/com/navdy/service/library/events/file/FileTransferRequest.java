package com.navdy.service.library.events.file;

final public class FileTransferRequest extends com.squareup.wire.Message {
    final public static String DEFAULT_DESTINATIONFILENAME = "";
    final public static String DEFAULT_FILEDATACHECKSUM = "";
    final public static Long DEFAULT_FILESIZE;
    final public static com.navdy.service.library.events.file.FileType DEFAULT_FILETYPE;
    final public static Long DEFAULT_OFFSET;
    final public static Boolean DEFAULT_OVERRIDE;
    final public static Boolean DEFAULT_SUPPORTSACKS;
    final private static long serialVersionUID = 0L;
    final public String destinationFileName;
    final public String fileDataChecksum;
    final public Long fileSize;
    final public com.navdy.service.library.events.file.FileType fileType;
    final public Long offset;
    final public Boolean override;
    final public Boolean supportsAcks;
    
    static {
        DEFAULT_FILETYPE = com.navdy.service.library.events.file.FileType.FILE_TYPE_OTA;
        DEFAULT_FILESIZE = Long.valueOf(0L);
        DEFAULT_OFFSET = Long.valueOf(0L);
        DEFAULT_OVERRIDE = Boolean.valueOf(false);
        DEFAULT_SUPPORTSACKS = Boolean.valueOf(false);
    }
    
    private FileTransferRequest(com.navdy.service.library.events.file.FileTransferRequest$Builder a) {
        this(a.fileType, a.destinationFileName, a.fileSize, a.offset, a.fileDataChecksum, a.override, a.supportsAcks);
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    FileTransferRequest(com.navdy.service.library.events.file.FileTransferRequest$Builder a, com.navdy.service.library.events.file.FileTransferRequest$1 a0) {
        this(a);
    }
    
    public FileTransferRequest(com.navdy.service.library.events.file.FileType a, String s, Long a0, Long a1, String s0, Boolean a2, Boolean a3) {
        this.fileType = a;
        this.destinationFileName = s;
        this.fileSize = a0;
        this.offset = a1;
        this.fileDataChecksum = s0;
        this.override = a2;
        this.supportsAcks = a3;
    }
    
    public boolean equals(Object a) {
        boolean b = false;
        label0: if (a != this) {
            if (a instanceof com.navdy.service.library.events.file.FileTransferRequest) {
                com.navdy.service.library.events.file.FileTransferRequest a0 = (com.navdy.service.library.events.file.FileTransferRequest)a;
                boolean b0 = this.equals(this.fileType, a0.fileType);
                label1: {
                    if (!b0) {
                        break label1;
                    }
                    if (!this.equals(this.destinationFileName, a0.destinationFileName)) {
                        break label1;
                    }
                    if (!this.equals(this.fileSize, a0.fileSize)) {
                        break label1;
                    }
                    if (!this.equals(this.offset, a0.offset)) {
                        break label1;
                    }
                    if (!this.equals(this.fileDataChecksum, a0.fileDataChecksum)) {
                        break label1;
                    }
                    if (!this.equals(this.override, a0.override)) {
                        break label1;
                    }
                    if (this.equals(this.supportsAcks, a0.supportsAcks)) {
                        b = true;
                        break label0;
                    }
                }
                b = false;
            } else {
                b = false;
            }
        } else {
            b = true;
        }
        return b;
    }
    
    public int hashCode() {
        int i = this.hashCode;
        if (i == 0) {
            i = (((((((this.fileType == null) ? 0 : this.fileType.hashCode()) * 37 + ((this.destinationFileName == null) ? 0 : this.destinationFileName.hashCode())) * 37 + ((this.fileSize == null) ? 0 : this.fileSize.hashCode())) * 37 + ((this.offset == null) ? 0 : this.offset.hashCode())) * 37 + ((this.fileDataChecksum == null) ? 0 : this.fileDataChecksum.hashCode())) * 37 + ((this.override == null) ? 0 : this.override.hashCode())) * 37 + ((this.supportsAcks == null) ? 0 : this.supportsAcks.hashCode());
            this.hashCode = i;
        }
        return i;
    }
}
