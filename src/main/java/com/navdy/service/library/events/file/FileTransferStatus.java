package com.navdy.service.library.events.file;

final public class FileTransferStatus extends com.squareup.wire.Message {
    final public static Integer DEFAULT_CHUNKINDEX;
    final public static com.navdy.service.library.events.file.FileTransferError DEFAULT_ERROR;
    final public static Boolean DEFAULT_SUCCESS;
    final public static Long DEFAULT_TOTALBYTESTRANSFERRED;
    final public static Boolean DEFAULT_TRANSFERCOMPLETE;
    final public static Integer DEFAULT_TRANSFERID;
    final private static long serialVersionUID = 0L;
    final public Integer chunkIndex;
    final public com.navdy.service.library.events.file.FileTransferError error;
    final public Boolean success;
    final public Long totalBytesTransferred;
    final public Boolean transferComplete;
    final public Integer transferId;
    
    static {
        DEFAULT_TRANSFERID = Integer.valueOf(0);
        DEFAULT_SUCCESS = Boolean.valueOf(false);
        DEFAULT_TOTALBYTESTRANSFERRED = Long.valueOf(0L);
        DEFAULT_ERROR = com.navdy.service.library.events.file.FileTransferError.FILE_TRANSFER_NO_ERROR;
        DEFAULT_CHUNKINDEX = Integer.valueOf(0);
        DEFAULT_TRANSFERCOMPLETE = Boolean.valueOf(false);
    }
    
    private FileTransferStatus(com.navdy.service.library.events.file.FileTransferStatus$Builder a) {
        this(a.transferId, a.success, a.totalBytesTransferred, a.error, a.chunkIndex, a.transferComplete);
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    FileTransferStatus(com.navdy.service.library.events.file.FileTransferStatus$Builder a, com.navdy.service.library.events.file.FileTransferStatus$1 a0) {
        this(a);
    }
    
    public FileTransferStatus(Integer a, Boolean a0, Long a1, com.navdy.service.library.events.file.FileTransferError a2, Integer a3, Boolean a4) {
        this.transferId = a;
        this.success = a0;
        this.totalBytesTransferred = a1;
        this.error = a2;
        this.chunkIndex = a3;
        this.transferComplete = a4;
    }
    
    public boolean equals(Object a) {
        boolean b = false;
        label0: if (a != this) {
            if (a instanceof com.navdy.service.library.events.file.FileTransferStatus) {
                com.navdy.service.library.events.file.FileTransferStatus a0 = (com.navdy.service.library.events.file.FileTransferStatus)a;
                boolean b0 = this.equals(this.transferId, a0.transferId);
                label1: {
                    if (!b0) {
                        break label1;
                    }
                    if (!this.equals(this.success, a0.success)) {
                        break label1;
                    }
                    if (!this.equals(this.totalBytesTransferred, a0.totalBytesTransferred)) {
                        break label1;
                    }
                    if (!this.equals(this.error, a0.error)) {
                        break label1;
                    }
                    if (!this.equals(this.chunkIndex, a0.chunkIndex)) {
                        break label1;
                    }
                    if (this.equals(this.transferComplete, a0.transferComplete)) {
                        b = true;
                        break label0;
                    }
                }
                b = false;
            } else {
                b = false;
            }
        } else {
            b = true;
        }
        return b;
    }
    
    public int hashCode() {
        int i = this.hashCode;
        if (i == 0) {
            i = ((((((this.transferId == null) ? 0 : this.transferId.hashCode()) * 37 + ((this.success == null) ? 0 : this.success.hashCode())) * 37 + ((this.totalBytesTransferred == null) ? 0 : this.totalBytesTransferred.hashCode())) * 37 + ((this.error == null) ? 0 : this.error.hashCode())) * 37 + ((this.chunkIndex == null) ? 0 : this.chunkIndex.hashCode())) * 37 + ((this.transferComplete == null) ? 0 : this.transferComplete.hashCode());
            this.hashCode = i;
        }
        return i;
    }
}
