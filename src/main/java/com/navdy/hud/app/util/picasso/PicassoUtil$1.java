package com.navdy.hud.app.util.picasso;

import java.io.IOException;

final class PicassoUtil$1 extends com.squareup.picasso.RequestHandler {
    PicassoUtil$1() {
    }
    
    public boolean canHandleRequest(com.squareup.picasso.Request a) {
        return "diskcache".equals(a.uri.getScheme());
    }
    
    public com.squareup.picasso.RequestHandler.Result load(com.squareup.picasso.Request a, int i) throws IOException {
        String s = a.uri.getHost();
        com.navdy.hud.app.util.picasso.PicassoUtil.access$000().v("load:" + s);
        if (com.navdy.hud.app.util.picasso.PicassoUtil.access$100() == null) {
            throw new java.io.IOException();
        }
        byte[] a0 = com.navdy.hud.app.util.picasso.PicassoUtil.access$100().get(s);
        if (a0 == null) {
            throw new java.io.IOException();
        }
        return new com.squareup.picasso.RequestHandler.Result(new java.io.ByteArrayInputStream(a0), com.squareup.picasso.Picasso.LoadedFrom.DISK);
    }
}
