package com.navdy.hud.app.util;

import android.content.BroadcastReceiver;
import mortar.Mortar;
import android.os.IBinder;

import java.io.InputStream;
import java.security.GeneralSecurityException;
import android.os.RecoverySystem;
import com.navdy.hud.app.device.dial.DialManager;
import com.navdy.hud.app.framework.glance.GlanceConstants;
import com.navdy.hud.app.framework.toast.ToastPresenter;
import com.navdy.hud.app.obd.ObdDeviceConfigurationManager;
import android.content.Intent;
import java.lang.reflect.InvocationTargetException;
import android.os.RecoverySystem;
import com.navdy.hud.app.event.Shutdown;

import java.util.Enumeration;
import java.util.Properties;
import java.util.regex.Matcher;
import com.navdy.hud.app.HudApplication;
import com.navdy.service.library.events.file.FileType;
import com.navdy.hud.app.storage.PathManager;
import com.navdy.service.library.task.TaskManager;
import android.os.Build;
import com.navdy.hud.app.analytics.AnalyticsSupport;
import android.text.TextUtils;
import com.navdy.service.library.util.IOUtils;
import java.io.IOException;
import java.io.File;
import android.content.Context;
import android.content.SharedPreferences;
import javax.inject.Inject;
import com.squareup.otto.Bus;
import com.navdy.service.library.log.Logger;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import android.app.Service;

import com.navdy.hud.app.BuildConfig;


public class OTAUpdateService extends Service
{
    public static final int CAPTURE_GROUP_FROM_VERSION = 2;
    public static final int CAPTURE_GROUP_TARGET_VERSION = 3;
    public static final String COMMAND_CHECK_UPDATE_ON_REBOOT = "CHECK_UPDATE";
    public static final String COMMAND_DO_NOT_PROMPT = "DO_NOT_PROMPT";
    public static final String COMMAND_INSTALL_UPDATE = "INSTALL_UPDATE";
    public static final String COMMAND_INSTALL_UPDATE_REBOOT_QUIET = "INSTALL_UPDATE_REBOOT_QUIET";
    public static final String COMMAND_INSTALL_UPDATE_SHUTDOWN = "INSTALL_UPDATE_SHUTDOWN";
    public static final String COMMAND_VERIFY_UPDATE = "VERIFY_UPDATE";
    public static final String DO_NOT_PROMPT = "do_not_prompt";
    public static final String EXTRA_COMMAND = "COMMAND";
    private static final String FAILED_OTA_FILE = "failed_ota_file";
    private static final long MAX_FILE_SIZE = 2147483648L;
    public static final int MAX_RETRY_COUNT = 3;
    private static final int MAX_ZIP_ENTRIES = 1000;
    public static final String RECOVERY_LAST_INSTALL_FILE_PATH = "/cache/recovery/last_install";
    private static final String RECOVERY_LAST_LOG_FILE_PATH = "/cache/recovery/last_log";
    public static final String RETRY_COUNT = "retry_count";
    public static final String UPDATE_FILE = "last_update_file_received";
    private static final String UPDATE_FROM_VERSION = "update_from_version";
    public static final String VERIFIED = "verified";
    public static final String VERSION_PREFIX = "1.0.";
    private static final Pattern sFileNamePattern;
    private static final Logger sLogger;
    private static String swVersion;
    @Inject
    Bus bus;
    private boolean installing;
    @Inject
    SharedPreferences sharedPreferences;
    
    static {
        sLogger = new Logger(OTAUpdateService.class);
        sFileNamePattern = Pattern.compile("^navdy_ota_(([0-9]+)_to_)?([0-9]+)\\.zip");
    }

    private boolean checkIfIncrementalUpdatesFailed(File currentUpdate) {
        File lastInstallFile = new File(RECOVERY_LAST_INSTALL_FILE_PATH);
        if (!lastInstallFile.exists()) {
            return true;
        }
        try {
            String lastInstallInfo = IOUtils.convertFileToString(lastInstallFile.getAbsolutePath());
            if (TextUtils.isEmpty(lastInstallInfo)) {
                return true;
            }
            String[] lines = lastInstallInfo.split(GlanceConstants.NEWLINE);
            if (lines.length < 2) {
                return true;
            }
            String fileName = lines[0];
            sLogger.d("Last installed update was " + fileName);
            String updateBaseName = new File(fileName).getName();
            boolean lastInstallSucceeded = Integer.parseInt(lines[1]) != 0;
            if (!lastInstallSucceeded) {
                reportOTAFailure(updateBaseName);
            }
            sLogger.d("Last install succeeded: " + (lastInstallSucceeded ? ToastPresenter.EXTRA_MAIN_TITLE : "0"));
            int fromVersion = extractFromVersion(updateBaseName);
            boolean isIncremental = fromVersion != -1;
            String priorVersion = this.sharedPreferences.getString(UPDATE_FROM_VERSION, "");
            if (!TextUtils.isEmpty(priorVersion)) {
                AnalyticsSupport.recordOTAInstallResult(isIncremental, priorVersion, lastInstallSucceeded);
                this.sharedPreferences.edit().remove(UPDATE_FROM_VERSION).apply();
            }
            if (!isIncremental) {
                return true;
            }
            sLogger.d("Last install was an incremental update");
            int incrementalVersion = 0;
            try {
                incrementalVersion = Integer.parseInt(Build.VERSION.INCREMENTAL);
            } catch (NumberFormatException e) {
                sLogger.e("Cannot parse the incremental version of the build :" + Build.VERSION.INCREMENTAL);
            }
            if (incrementalVersion != fromVersion) {
                return true;
            }
            sLogger.d("The last installed incremental from version is same as current version :" + incrementalVersion);
            if (lastInstallSucceeded) {
                return true;
            }
            sLogger.e("Last incremental update has failed");
            this.bus.post(new IncrementalOTAFailureDetected());
            if (currentUpdate == null || !currentUpdate.getAbsolutePath().equals(fileName)) {
                return true;
            }
            sLogger.d("Current update is the same as the one failed during last install");
            return false;
        } catch (Throwable e2) {
            sLogger.e("Error reading the file :" + lastInstallFile.getAbsolutePath(), e2);
            return true;
        }
    }

    private static File checkUpdateFile(Context context, SharedPreferences sharedPreferences) {
        sLogger.d("Checking for update file");
        swVersion = null;
        String fileName = sharedPreferences.getString(UPDATE_FILE, null);
        if (fileName == null) {
            sLogger.e("No update available");
            return null;
        }
        File file = new File(fileName);
        if (file.isFile() && file.exists() && file.canRead()) {
            int incrementalVersion = 0;
            try {
                incrementalVersion = Integer.parseInt(Build.VERSION.INCREMENTAL);
            } catch (NumberFormatException e) {
                sLogger.e("Cannot parse the incremental version of the build :" + Build.VERSION.INCREMENTAL);
            }
            sLogger.d("Incremental version of the build on the device:" + incrementalVersion);
            int updateVersion = extractIncrementalVersion(file.getName());
            int fromVersion = extractFromVersion(file.getName());
            sLogger.d("Update from version " + fromVersion);
            if (fromVersion != -1 && incrementalVersion != 0 && fromVersion != incrementalVersion) {
                sLogger.e("Update from version mismatch " + fromVersion + " " + incrementalVersion);
                clearUpdate(file, context, sharedPreferences);
                return null;
            } else if (updateVersion <= incrementalVersion) {
                sLogger.e("Already up to date :" + incrementalVersion);
                clearUpdate(file, context, sharedPreferences);
                return null;
            } else if (sharedPreferences.getInt(RETRY_COUNT, 0) < 3) {
                return file;
            } else {
                sLogger.d("Maximum retries. Deleting the update file.");
                clearUpdate(file, context, sharedPreferences);
                return null;
            }
        }
        sLogger.e("Invalid update file");
        return null;
    }

    private void cleanupOldFiles() {
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                int incrementalVersion = -1;
                try {
                    incrementalVersion = Integer.parseInt(Build.VERSION.INCREMENTAL);
                } catch (NumberFormatException e) {
                    OTAUpdateService.sLogger.e("Cannot parse the incremental version of the build :" + Build.VERSION.INCREMENTAL);
                }
                if (incrementalVersion == -1) {
                    String fileName = OTAUpdateService.this.sharedPreferences.getString(OTAUpdateService.UPDATE_FILE, null);
                    if (!TextUtils.isEmpty(fileName)) {
                        incrementalVersion = OTAUpdateService.extractIncrementalVersion(fileName);
                    }
                }
                if (incrementalVersion > 0) {
                    File otaUpdatesFileDirectory = new File(PathManager.getInstance().getDirectoryForFileType(FileType.FILE_TYPE_OTA));
                    if (otaUpdatesFileDirectory.exists()) {
                        File[] children = otaUpdatesFileDirectory.listFiles();
                        if (children != null) {
                            for (File file : children) {
                                if (file.isFile() && OTAUpdateService.isOtaFile(file.getName()) && OTAUpdateService.extractIncrementalVersion(file.getName()) <= incrementalVersion) {
                                    OTAUpdateService.sLogger.e("Clearing the old update file :" + file.getName());
                                    IOUtils.deleteFile(HudApplication.getAppContext(), file.getAbsolutePath());
                                }
                            }
                        }
                    }
                }
            }
        }, 1);
    }
    
    public static void clearUpdate(final File file, final Context context, final SharedPreferences sharedPreferences) {
        if (file != null) {
            OTAUpdateService.sLogger.d("Clearing the update");
            IOUtils.deleteFile(context, file.getAbsolutePath());
        }
        sharedPreferences.edit().remove("last_update_file_received").remove("retry_count").remove("do_not_prompt").remove("verified").apply();
    }
    
    private static int extractFromVersion(String group) {
        final int n = -1;
        final Matcher matcher = OTAUpdateService.sFileNamePattern.matcher(group);
        int int1 = n;
        if (matcher.find()) {
            int1 = n;
            if (matcher.groupCount() > 0) {
                group = matcher.group(2);
                if (group == null) {
                    int1 = n;
                }
                else {
                    try {
                        int1 = Integer.parseInt(group);
                    }
                    catch (NumberFormatException ex) {
                        OTAUpdateService.sLogger.e("Cannot parse the file to retrieve the from version" + ex);
                        ex.printStackTrace();
                        int1 = n;
                    }
                }
            }
        }
        return int1;
    }
    
    private static int extractIncrementalVersion(String group) {
        final Matcher matcher = OTAUpdateService.sFileNamePattern.matcher(group);
        if (!matcher.find() || matcher.groupCount() <= 0) {
            return -1;
        }
        group = matcher.group(3);
        try {
            return Integer.parseInt(group);
        }
        catch (NumberFormatException ex) {
            OTAUpdateService.sLogger.e("Cannot parse the file to retrieve the target version" + ex);
            ex.printStackTrace();
        }
        return -1;
    }
    
    public static String getCurrentVersion() {
        return shortVersion(BuildConfig.VERSION_NAME);
    }
    
    public static String getIncrementalUpdateVersion(final SharedPreferences sharedPreferences) {
        final String s = null;
        final String string = sharedPreferences.getString("last_update_file_received", null);
        String value;
        if (string == null) {
            value = s;
        }
        else {
            final int incrementalVersion = extractIncrementalVersion(new File(string).getName());
            value = s;
            if (incrementalVersion > 0) {
                value = String.valueOf(incrementalVersion);
            }
        }
        return value;
    }
    
    public static String getSWUpdateVersion() {
        return OTAUpdateService.swVersion;
    }
    
    private static String getUpdateVersion(SharedPreferences sharedPreferences) throws Throwable {
        IOException e;
        Throwable th;
        String fileName = sharedPreferences.getString(UPDATE_FILE, null);
        if (fileName == null) {
            return null;
        }
        ZipFile zipFile = null;
        InputStream inputStream = null;
        String versionName = null;
        try {
            ZipFile zipFile2 = new ZipFile(new File(fileName));
            try {
                ZipEntry entry = zipFile2.getEntry("META-INF/com/android/metadata");
                Properties properties = new Properties();
                versionName = shortVersion(properties.getProperty("version-name"));
                inputStream = zipFile2.getInputStream(entry);
                properties.load(inputStream);
                IOUtils.closeObject(inputStream);
                IOUtils.closeObject(zipFile2);
                zipFile = zipFile2;
            } catch (IOException e2) {
                e = e2;
                zipFile = zipFile2;
                try {
                    sLogger.w("Error extracting version-name from ota metadata ", e);
                    IOUtils.closeObject(inputStream);
                    IOUtils.closeObject(zipFile);
                    if (TextUtils.isEmpty(versionName)) {
                        return "1.0." + getIncrementalUpdateVersion(sharedPreferences);
                    }
                    return versionName;
                } catch (Throwable th2) {
                    th = th2;
                    IOUtils.closeObject(inputStream);
                    IOUtils.closeObject(zipFile);
                    throw th;
                }
            } catch (Throwable th3) {
                th = th3;
                zipFile = zipFile2;
                IOUtils.closeObject(inputStream);
                IOUtils.closeObject(zipFile);
                throw th;
            }
        } catch (IOException e3) {
            e = e3;
            sLogger.w("Error extracting version-name from ota metadata ", e);
            IOUtils.closeObject(inputStream);
            IOUtils.closeObject(zipFile);
            if (TextUtils.isEmpty(versionName)) {
                return versionName;
            }
            return "1.0." + getIncrementalUpdateVersion(sharedPreferences);
        }
        if (TextUtils.isEmpty(versionName)) {
            return "1.0." + getIncrementalUpdateVersion(sharedPreferences);
        }
        return versionName;
    }

    private void installPackage(Context context, File file, String postUpdateCommand) throws IOException {
        AnalyticsSupport.recordShutdown(Shutdown.Reason.OTA, false);
        try {
            RecoverySystem.class.getMethod("installPackage", Context.class, File.class, String.class).invoke(null, context, file, postUpdateCommand);
        } catch (InvocationTargetException ie) {
            Throwable e = ie.getCause();
            if (e instanceof IOException) {
                throw ((IOException) e);
            }
            sLogger.e("unexpected exception from RecoverySystem.installPackage()" + e);
        } catch (Exception e2) {
            sLogger.e("error invoking Recovery.installPackage(): " + e2);
            RecoverySystem.installPackage(context, file);
        }
    }
    
    public static boolean isOTAUpdateVerified(final SharedPreferences sharedPreferences) {
        return sharedPreferences.getBoolean("verified", false);
    }
    
    public static boolean isOtaFile(final String s) {
        final Matcher matcher = OTAUpdateService.sFileNamePattern.matcher(s);
        return matcher.find();
    }
    
    public static boolean isUpdateAvailable() {
        return !TextUtils.isEmpty(OTAUpdateService.swVersion);
    }

        private void reportOTAFailure(String updateFileName) {
            if (updateFileName.equals(this.sharedPreferences.getString(FAILED_OTA_FILE, ""))) {
                sLogger.v("update failure already reported for " + updateFileName);
                return;
            }
            String last_install;
            String last_log;
            try {
                last_install = "\n========== /cache/recovery/last_install\n" + IOUtils.convertFileToString(RECOVERY_LAST_INSTALL_FILE_PATH);
            } catch (IOException e) {
                last_install = "\n*** failed to read /cache/recovery/last_install";
                sLogger.e("exception reading /cache/recovery/last_install", e);
            }
            try {
                last_log = "\n========== /cache/recovery/last_log\n" + IOUtils.convertFileToString(RECOVERY_LAST_LOG_FILE_PATH);
            } catch (IOException e2) {
                last_log = "\n*** failed to read /cache/recovery/last_log";
                sLogger.e("exception reading /cache/recovery/last_log", e2);
            }
            final OTAFailedException exception = new OTAFailedException(last_install, last_log);
            TaskManager.getInstance().execute(new Runnable() {
                public void run() {
                    CrashReporter.getInstance().reportOTAFailure(exception);
                }
            }, 1);
            this.sharedPreferences.edit().putString(FAILED_OTA_FILE, updateFileName).apply();
        }
    
    public static String shortVersion(String s) {
        if (s != null) {
            s = s.split("-")[0];
        }
        else {
            s = null;
        }
        return s;
    }
    
    public static void startServiceToVerifyUpdate() {
        final Context appContext = HudApplication.getAppContext();
        final Intent intent = new Intent(appContext, OTAUpdateService.class);
        intent.putExtra("COMMAND", "VERIFY_UPDATE");
        appContext.startService(intent);
    }
    
    private void update(final String s) {
        if (!this.installing) {
            this.installing = true;
            this.bus.post(new InstallingUpdate());
            TaskManager.getInstance().execute(new Runnable() {
                @Override
                public void run() {
                    try {
                        OTAUpdateService.sLogger.d("Applying the update");
                        final File access$100 = checkUpdateFile(OTAUpdateService.this, OTAUpdateService.this.sharedPreferences);
                        Label_0224: {
                            if (access$100 == null) {
                                break Label_0224;
                            }
                            try {
                                final int int1 = OTAUpdateService.this.sharedPreferences.getInt("retry_count", 0);
                                OTAUpdateService.this.sharedPreferences.edit().putInt("retry_count", int1 + 1).putString("update_from_version", Build.VERSION.INCREMENTAL).apply();
                                OTAUpdateService.sLogger.d("Trying to the install the update, retry count :" + (int1 + 1));
                                try {
                                    AnalyticsSupport.recordInstallOTAUpdate(OTAUpdateService.this.sharedPreferences);
                                    CrashReporter.getInstance().stopCrashReporting(true);
                                    OTAUpdateService.sLogger.i("NAVDY_NOT_A_CRASH");
                                    ObdDeviceConfigurationManager.turnOffOBDSleep();
                                    DialManager.getInstance().requestDialReboot(false);
                                    Thread.sleep(2000L);
                                    OTAUpdateService.this.installPackage(OTAUpdateService.this, access$100, s);
                                    return;
                                }
                                catch (Throwable t) {
                                    OTAUpdateService.sLogger.e("Exception while reporting the update", t);
                                }
                            }
                            catch (Throwable th2) {
                                OTAUpdateService.sLogger.e("Exception while running update OTA package", th2);
                                CrashReporter.getInstance().reportNonFatalException(th2);
                                return;
                            }
                        }
                        OTAUpdateService.clearUpdate(access$100, OTAUpdateService.this, OTAUpdateService.this.sharedPreferences);
                    }
                    catch (Exception access$100) {
                        OTAUpdateService.sLogger.e("Failed to install OTA package", access$100);
                        OTAUpdateService.this.installing = false;
                    }
                    finally {
                        OTAUpdateService.this.installing = false;
                    }
                }
            }, 1);
        }
    }

    public static boolean verifyOTAZipFile(File file, int maxEntries, long maxSize) throws Throwable {
        IOException e;
        Throwable th;
        ZipFile zipFile = null;
        try {
            File destinationDirectory = file.getParentFile();
            String destinationCanonicalPath = destinationDirectory.getCanonicalPath();
            ZipFile zipFile2 = new ZipFile(file);
            try {
                Enumeration entries = zipFile2.entries();
                int entryCount = 0;
                long uncompressedSize = 0;
                while (entries.hasMoreElements()) {
                    ZipEntry entry = (ZipEntry) entries.nextElement();
                    entryCount++;
                    if (!entry.isDirectory()) {
                        long size = entry.getSize();
                        if (size > 0) {
                            uncompressedSize += size;
                        }
                    }
                    if (entryCount > maxEntries) {
                        sLogger.d("OTA zip file failed verification : Too many entries " + entryCount);
                        IOUtils.closeObject(zipFile2);
                        zipFile = zipFile2;
                        return false;
                    } else if (uncompressedSize > maxSize) {
                        sLogger.d("OTA zip file failed verification : Exceeded max uncompressed size " + maxSize);
                        IOUtils.closeObject(zipFile2);
                        zipFile = zipFile2;
                        return false;
                    } else {
                        String zipEntryCanonicalPath = new File(destinationDirectory, entry.getName()).getCanonicalPath();
                        if (!TextUtils.isEmpty(zipEntryCanonicalPath)) {
                            if (!zipEntryCanonicalPath.startsWith(destinationCanonicalPath)) {
                            }
                        }
                        sLogger.d("OTA zip failed verification : Zip entry tried to write outside destination " + entry.getName() + " , Canonical path " + zipEntryCanonicalPath);
                        IOUtils.closeObject(zipFile2);
                        zipFile = zipFile2;
                        return false;
                    }
                }
                IOUtils.closeObject(zipFile2);
                zipFile = zipFile2;
                return true;
            } catch (IOException e2) {
                e = e2;
                zipFile = zipFile2;
            } catch (Throwable th2) {
                th = th2;
                zipFile = zipFile2;
            }
        } catch (IOException e3) {
            e = e3;
            try {
                sLogger.e("Exception while verifying the OTA package " + e);
                e.printStackTrace();
                IOUtils.closeObject(zipFile);
                return false;
            } catch (Throwable th3) {
                th = th3;
                IOUtils.closeObject(zipFile);
                throw th;
            }
        }
        return false;
    }

    public boolean bVerifyUpdate(File file, SharedPreferences preferences) {
        Exception e;
        try {
            RecoverySystem.verifyPackage(file, new RecoverySystem.ProgressListener() {
                public void onProgress(int progress) {
                    OTAUpdateService.sLogger.d(String.format("Verify progress: %d%% completed", progress));
                }
            }, null);
            verifyOTAZipFile(file, 1000, MAX_FILE_SIZE);
            return true;
        } catch (GeneralSecurityException e3) {
            e = e3;
            sLogger.e("Exception while verifying the update package " + e);
            return false;
        } catch (Throwable e2) {
            sLogger.e("Exception while verifying the update package " + e2);
            return false;
        }
    }

    public void checkForUpdate() {
        TaskManager.getInstance().execute(new Runnable() {
            @Override
            public void run() {
                OTAUpdateService.sLogger.d("Checking the update");
                final File access$100 = checkUpdateFile(OTAUpdateService.this, OTAUpdateService.this.sharedPreferences);
                if (access$100 == null) {
                    OTAUpdateService.sLogger.e("No update file found");
                    OTAUpdateService.this.checkIfIncrementalUpdatesFailed(null);
                    OTAUpdateService.clearUpdate(access$100, OTAUpdateService.this, OTAUpdateService.this.sharedPreferences);
                }
                else {
                    final boolean access$101 = OTAUpdateService.this.checkIfIncrementalUpdatesFailed(access$100);
                    OTAUpdateService.sLogger.d("Current update valid ? :" + access$101);
                    if (access$101) {
                        if (!OTAUpdateService.isOTAUpdateVerified(OTAUpdateService.this.sharedPreferences)) {
                            if (!OTAUpdateService.this.bVerifyUpdate(access$100, OTAUpdateService.this.sharedPreferences)) {
                                OTAUpdateService.clearUpdate(access$100, OTAUpdateService.this, OTAUpdateService.this.sharedPreferences);
                                return;
                            }
                            OTAUpdateService.this.sharedPreferences.edit().putBoolean("verified", true).apply();
                            OTAUpdateService.this.bus.post(new UpdateVerified());
                        }
                        try {
                            OTAUpdateService.swVersion = getUpdateVersion(OTAUpdateService.this.sharedPreferences);
                        } catch (Throwable throwable) {

                            sLogger.e("Exception while getting update version " + throwable);
                        }
                        OTAUpdateService.sLogger.v("verified swVersion:" + OTAUpdateService.swVersion);
                    }
                    else {
                        OTAUpdateService.clearUpdate(access$100, OTAUpdateService.this, OTAUpdateService.this.sharedPreferences);
                    }
                }
            }
        }, 1);
    }
    
    public IBinder onBind(final Intent intent) {
        return null;
    }
    
    public void onCreate() {
        super.onCreate();
        Mortar.inject(HudApplication.getAppContext(), this);
    }
    
    public int onStartCommand(final Intent intent, int n, final int n2) {
        super.onStartCommand(intent, n, n2);
        final String stringExtra = intent.getStringExtra("COMMAND");
        OTAUpdateService.sLogger.d("OTA Update service started command[" + stringExtra + "]");
        if (intent.hasExtra("COMMAND")) {
            n = -1;
            switch (stringExtra.hashCode()) {
                case -1672139796:
                    if (stringExtra.equals("INSTALL_UPDATE_REBOOT_QUIET")) {
                        n = 0;
                        break;
                    }
                    break;
                case 227671624:
                    if (stringExtra.equals("INSTALL_UPDATE_SHUTDOWN")) {
                        n = 1;
                        break;
                    }
                    break;
                case -2036275187:
                    if (stringExtra.equals("INSTALL_UPDATE")) {
                        n = 2;
                        break;
                    }
                    break;
                case 1870386340:
                    if (stringExtra.equals("DO_NOT_PROMPT")) {
                        n = 3;
                        break;
                    }
                    break;
                case 1813668687:
                    if (stringExtra.equals("VERIFY_UPDATE")) {
                        n = 4;
                        break;
                    }
                    break;
            }
            switch (n) {
                default:
                    OTAUpdateService.sLogger.e("onStartCommand() invalid command: " + stringExtra);
                    break;
                case Service.START_STICKY_COMPATIBILITY:
                    this.update("--reboot_quiet_after");
                    break;
                case Service.START_STICKY:
                    this.update("--shutdown_after");
                    break;
                case Service.START_FLAG_RETRY:
                    this.update(null);
                    break;
                case Service.START_REDELIVER_INTENT:
                    this.sharedPreferences.edit().putBoolean("do_not_prompt", true).apply();
                    break;
                case 4:
                    this.checkForUpdate();
                    this.cleanupOldFiles();
                    break;
            }
        }
        return Service.START_NOT_STICKY;
    }
    
    public static class IncrementalOTAFailureDetected
    {
    }
    
    public static class InstallingUpdate
    {
    }
    
    public static class OTADownloadIntentsReceiver extends BroadcastReceiver
    {
        private SharedPreferences sharedPreferences;
        
        public OTADownloadIntentsReceiver(final SharedPreferences sharedPreferences) {
            this.sharedPreferences = sharedPreferences;
        }
        
        public void onReceive(final Context context, final Intent intent) {
            TaskManager.getInstance().execute(new Runnable() {
                final /* synthetic */ String val$path = intent.getStringExtra("path");
                
                @Override
                public void run() {
                    final File file = new File(this.val$path);
                    if (OTAUpdateService.isOtaFile(file.getName())) {
                        final String string = OTADownloadIntentsReceiver.this.sharedPreferences.getString("last_update_file_received", null);
                        final String absolutePath = file.getAbsolutePath();
                        OTADownloadIntentsReceiver.this.sharedPreferences.edit().putString("last_update_file_received", absolutePath).remove("do_not_prompt").remove("retry_count").remove("verified").apply();
                        if (!absolutePath.equals(string)) {
                            if (string != null) {
                                IOUtils.deleteFile(context, string);
                                OTAUpdateService.sLogger.d("Delete the old update file " + string + ", new update :" + absolutePath);
                            }
                            AnalyticsSupport.recordDownloadOTAUpdate(OTADownloadIntentsReceiver.this.sharedPreferences);
                        }
                        OTAUpdateService.startServiceToVerifyUpdate();
                    }
                }
            }, 1);
        }
    }
    
    public static class OTAFailedException extends Exception
    {
        public String last_install;
        public String last_log;
        
        OTAFailedException(final String last_install, final String last_log) {
            super("OTA installation failed");
            this.last_install = last_install;
            this.last_log = last_log;
        }
    }
    
    public static class UpdateVerified
    {
    }
}
