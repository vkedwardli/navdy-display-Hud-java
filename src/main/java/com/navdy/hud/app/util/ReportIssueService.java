package com.navdy.hud.app.util;

import com.navdy.hud.app.R;
import com.navdy.hud.app.device.gps.GpsDeadReckoningManager;
import com.navdy.hud.app.device.light.HUDLightUtils;
import com.navdy.hud.app.device.light.LightManager;
import com.navdy.hud.app.framework.glance.GlanceConstants;
import com.navdy.hud.app.framework.network.NetworkBandwidthController$Component;
import com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder;
import com.navdy.service.library.network.http.HttpUtils;
import com.navdy.service.library.util.CredentialUtil;
import mortar.Mortar;
import java.io.Closeable;
import java.io.FileWriter;
import java.io.Reader;
import java.io.BufferedReader;
import java.io.FileReader;
import com.navdy.hud.app.framework.network.NetworkStateManager;
import com.navdy.hud.app.framework.network.NetworkBandwidthController;
import com.navdy.hud.app.profile.DriverProfile;
import com.navdy.hud.app.ui.framework.UIStateManager;
import com.navdy.hud.app.service.ConnectionHandler;
import org.json.JSONException;
import com.navdy.hud.app.profile.HudLocale;
import com.navdy.service.library.events.DeviceInfo;
import com.navdy.hud.app.ui.component.homescreen.HomeScreen;
import org.json.JSONObject;
import com.navdy.hud.app.event.ShowScreenWithArgs;
import com.navdy.service.library.events.ui.Screen;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2;
import android.os.Bundle;
import android.app.PendingIntent;
import android.os.SystemClock;
import android.app.AlarmManager;
import com.navdy.hud.app.storage.PathManager;
import java.util.Arrays;
import java.io.Serializable;
import android.content.Intent;
import com.here.android.mpa.common.GeoCoordinate;
import com.here.android.mpa.routing.RouteTta;
import com.navdy.hud.app.maps.here.HereNavController;
import com.here.android.mpa.common.RoadElement;
import com.here.android.mpa.common.GeoPosition;
import android.os.Build;
import android.os.Build;
import com.here.android.mpa.routing.Maneuver;
import com.navdy.service.library.events.navigation.NavigationRouteRequest;
import com.navdy.service.library.events.preferences.NavigationPreferences;

import java.util.Collections;
import java.util.Date;
import com.navdy.hud.app.analytics.NavigationQualityTracker;
import com.here.android.mpa.routing.Route;
import com.navdy.hud.app.maps.here.HereNavigationManager;
import com.navdy.hud.app.framework.DriverProfileHelper;
import com.navdy.hud.app.maps.here.HereMapUtil;
import com.navdy.hud.app.maps.here.HereMapsManager;
import android.text.TextUtils;
import com.google.gson.JsonParser;
import com.google.gson.GsonBuilder;
import android.content.Context;
import com.navdy.service.library.device.NavdyDeviceId;

import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.io.IOException;
import java.util.ArrayList;
import com.navdy.service.library.util.IOUtils;
import com.navdy.hud.app.HudApplication;

import java.util.Objects;
import java.util.concurrent.TimeUnit;
import java.util.Comparator;
import java.util.Locale;
import com.navdy.service.library.network.http.services.JiraClient;
import com.navdy.service.library.network.http.IHttpManager;
import com.navdy.hud.app.gesture.GestureServiceConnector;
import com.navdy.hud.app.debug.DriveRecorder;
import javax.inject.Inject;

import com.navdy.service.library.util.LogUtils;
import com.navdy.service.library.util.MusicDataUtils;
import com.squareup.otto.Bus;
import com.navdy.service.library.log.Logger;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.PriorityBlockingQueue;
import java.io.File;
import java.text.SimpleDateFormat;
import android.app.IntentService;

import com.navdy.hud.app.BuildConfig;


public class ReportIssueService extends IntentService
{
    private static final String ACTION_DUMP = "Dump";
    private static final String ACTION_SNAPSHOT = "Snapshot";
    private static final String ACTION_SUBMIT_JIRA = "Jira";
    private static final String ACTION_SYNC = "Sync";
    public static final String ASSIGNEE = "assignee";
    public static final String ASSIGNEE_EMAIL = "assigneeEmail";
    public static final String ATTACHMENT = "attachment";
    private static final SimpleDateFormat DATE_FORMAT;
    public static final String ENVIRONMENT = "Environment";
    public static final String EXTRA_ISSUE_TYPE = "EXTRA_ISSUE_TYPE";
    public static final String EXTRA_SNAPSHOT_TITLE = "EXTRA_SNAPSHOT_TITLE";
    private static final String HUD_ISSUE_TYPE = "Issue";
    private static final String HUD_PROJECT = "HUD";
    private static final String ISSUE_TYPE = "Task";
    private static final String JIRA_CREDENTIALS_META = "JIRA_CREDENTIALS";
    private static final int MAX_FILES_OUT_STANDING = 10;
    private static final String NAME = "REPORT_ISSUE";
    private static final String PROJECT_NAME = "NP";
    public static final long RETRY_INTERVAL;
    private static final SimpleDateFormat SNAPSHOT_DATE_FORMAT;
    private static final SimpleDateFormat SNAPSHOT_JIRA_TICKET_DATE_FORMAT;
    public static final String SUMMARY = "Summary";
    public static final int SYNC_REQ = 128;
    public static final String TICKET_ID = "ticketId";
    private static File lastSnapShotFile;
    private static boolean mIsInitialized;
    private static PriorityBlockingQueue<File> mIssuesToSync;
    private static long mLastIssueSubmittedAt;
    private static AtomicBoolean mSyncing;
    private static String navigationIssuesFolder;
    private static Logger sLogger;
    @Inject
    Bus bus;
    @Inject
    DriveRecorder driveRecorder;
    @Inject
    GestureServiceConnector gestureService;
    @Inject
    IHttpManager mHttpManager;
    JiraClient mJiraClient;
    
    static {
        DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd'_'HH:mm:ss.SSS", Locale.US);
        ReportIssueService.mIssuesToSync = new PriorityBlockingQueue<>(5, new FilesModifiedTimeComparator());
        RETRY_INTERVAL = TimeUnit.MINUTES.toMillis(3L);
        ReportIssueService.mLastIssueSubmittedAt = 0L;
        ReportIssueService.mIsInitialized = false;
        ReportIssueService.mSyncing = new AtomicBoolean(false);
        ReportIssueService.sLogger = new Logger(ReportIssueService.class);
        SNAPSHOT_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss", Locale.US);
        SNAPSHOT_JIRA_TICKET_DATE_FORMAT = new SimpleDateFormat("MMM,dd hh:mm a", Locale.US);
    }
    
    public ReportIssueService() {
        super("REPORT_ISSUE");
    }

    private void attachFilesToTheJiraTicket(String ticket, File snapShotFile, JiraClient.ResultCallback callback) {
        if (snapShotFile.exists() && snapShotFile.getName().endsWith(".zip")) {
            File file = new File(snapShotFile.getParentFile(), "Staging");
            if (file.exists()) {
                if (file.isDirectory()) {
                    IOUtils.deleteDirectory(HudApplication.getAppContext(), file);
                } else {
                    IOUtils.deleteFile(HudApplication.getAppContext(), file.getAbsolutePath());
                }
            }
            boolean created = file.mkdir();
            try {
                IOUtils.deCompressZipToDirectory(HudApplication.getAppContext(), snapShotFile, file);
                File[] decompressedFiles = file.listFiles();
                ArrayList<File> logFiles = new ArrayList<>();
                ArrayList<File> pngFiles = new ArrayList<>();
                for (File file2 : decompressedFiles) {
                    if (file2.getName().endsWith(".png")) {
                        pngFiles.add(file2);
                    } else {
                        logFiles.add(file2);
                    }
                }
                File[] logFilesArray = logFiles.toArray(new File[logFiles.size()]);
                File logFilesZip = new File(file, "logs.zip");
                IOUtils.compressFilesToZip(HudApplication.getAppContext(), logFilesArray, logFilesZip.getAbsolutePath());
                ArrayList<JiraClient.Attachment> attachments = new ArrayList<>();
                JiraClient.Attachment logFilesZipAttachment = new JiraClient.Attachment();
                logFilesZipAttachment.filePath = logFilesZip.getAbsolutePath();
                logFilesZipAttachment.mimeType = HttpUtils.MIME_TYPE_ZIP;
                attachments.add(logFilesZipAttachment);
            for (File pngFile : pngFiles) {
                JiraClient.Attachment pngFileAttachment = new JiraClient.Attachment();
                pngFileAttachment.filePath = pngFile.getAbsolutePath();
                pngFileAttachment.mimeType = HttpUtils.MIME_TYPE_IMAGE_PNG;
                attachments.add(pngFileAttachment);
            }
                this.mJiraClient.attachFilesToTicket(ticket, attachments, callback);
            } catch (IOException e) {
                sLogger.e("Error decompressing the zip file to get the attachments", e);
            }
        }
    }

    private String buildDescription() {
        String userName;
        String userEmail;
        NavdyDeviceId displayDeviceId = NavdyDeviceId.getThisDevice(this);
        StringBuilder builder = new StringBuilder();
        builder.append("\n#############\n");
        builder.append("HERE metadata: ");
        builder.append("\n#############\n\n");
        String prettyJsonString = "";
        String hereSdkVersion = DeviceUtil.getCurrentHereSdkVersion();
        String metaData = DeviceUtil.getHEREMapsDataInfo();
        if (metaData != null) {
            try {
                prettyJsonString = new GsonBuilder().setPrettyPrinting().create().toJson(new JsonParser().parse(metaData));
            } catch (Throwable t) {
                sLogger.e(t);
            }
        }
        if (!TextUtils.isEmpty(hereSdkVersion)) {
            builder.append("HERE mSDK for Android v").append(hereSdkVersion).append(GlanceConstants.NEWLINE);
        }
        if (!TextUtils.isEmpty(prettyJsonString)) {
            builder.append("HERE Map Data:\n").append(metaData).append(GlanceConstants.NEWLINE);
        }
        builder.append("\n#############\n");
        builder.append("Navigation details:");
        builder.append("\n#############\n\n");
        GeoPosition position = HereMapsManager.getInstance().getLastGeoPosition();
        if (position == null || position.getCoordinate() == null) {
            builder.append("Current Location: N/A\n");
        } else {
            builder.append("Current Location: ").append(position.getCoordinate().getLatitude()).append(HereManeuverDisplayBuilder.COMMA).append(position.getCoordinate().getLongitude()).append(GlanceConstants.NEWLINE);
        }
        builder.append("Current Road: ").append(HereMapUtil.getCurrentRoadName()).append(GlanceConstants.NEWLINE);
        RoadElement roadElement = HereMapUtil.getCurrentRoadElement();
        if (roadElement != null) {
            builder.append("Current road element ID: ").append(roadElement.getIdentifier()).append(GlanceConstants.NEWLINE);
        }
        DriverProfileHelper driverProfileHelper = DriverProfileHelper.getInstance();
        DriverProfile currentProfile = driverProfileHelper.getCurrentProfile();
        if (HereMapsManager.getInstance().isNavigationModeOn()) {
            HereNavigationManager hereNavigationManager = HereNavigationManager.getInstance();
            Route route = hereNavigationManager.getCurrentRoute();
            HereNavController hereNavController = hereNavigationManager.getNavController();
            if (route != null) {
                RouteTta routeTta = hereNavController.getTta(Route.TrafficPenaltyMode.OPTIMAL, true);
                GeoCoordinate startingPoint = hereNavigationManager.getStartLocation();
                if (startingPoint != null) {
                    builder.append("Route Starting point: ").append(startingPoint.getLatitude()).append(HereManeuverDisplayBuilder.COMMA).append(startingPoint.getLongitude()).append(GlanceConstants.NEWLINE);
                } else {
                    builder.append("Route Starting point: N/A\n");
                }
                GeoCoordinate destination = route.getDestination();
                if (destination != null) {
                    builder.append("Route Ending point: ").append(destination.getLatitude()).append(HereManeuverDisplayBuilder.COMMA).append(destination.getLongitude()).append(GlanceConstants.NEWLINE);
                } else {
                    builder.append("Route Ending point: N/A\n");
                }
                builder.append("Traffic Used: ").append(hereNavigationManager.isTrafficConsidered(hereNavigationManager.getCurrentRouteId())).append(GlanceConstants.NEWLINE);
                long tripStartedTimestamp = NavigationQualityTracker.getInstance().getTripStartUtc();
                if (tripStartedTimestamp > 0) {
                    builder.append("Trip started: ").append(new SimpleDateFormat("MMMM dd HH:mm:ss zzzz yyyy Z", Locale.US).format(new Date(tripStartedTimestamp))).append(GlanceConstants.NEWLINE);
                }
                NavigationQualityTracker navigationQualityTracker = NavigationQualityTracker.getInstance();
                int durationSoFar = navigationQualityTracker.getActualDurationSoFar();
                int initialDuration = navigationQualityTracker.getExpectedDuration();
                int initialDistance = navigationQualityTracker.getExpectedDistance();
                long destinationDistance = hereNavController.getDestinationDistance();
                if (initialDuration > 0) {
                    builder.append("Expected initial ETA according to HERE: ").append(initialDuration).append(" seconds\n");
                }
                if (initialDistance > 0) {
                    builder.append("Expected initial distance according to HERE: ").append(initialDistance).append(" meters\n");
                }
                if (routeTta != null) {
                    builder.append("Time To Arrival remaining according to HERE: ").append(routeTta.getDuration()).append(" seconds\n");
                }
                if (destinationDistance > 0) {
                    builder.append("Distance remaining according to HERE: ").append(destinationDistance).append(" meters\n");
                }
                if (durationSoFar > 0) {
                    builder.append("Duration of the trip so far: ").append(durationSoFar).append(" seconds\n");
                }
            }
            String destinationLabel = hereNavigationManager.getDestinationLabel();
            builder.append("Destination: ").append(destinationLabel).append(", ").append(hereNavigationManager.getDestinationStreetAddress()).append(GlanceConstants.NEWLINE);
            builder.append("\n#############\n");
            builder.append("User route preferences:");
            builder.append("\n#############\n\n");
            NavigationPreferences userNavPrefs = currentProfile.getNavigationPreferences();
            builder.append("Route calculation: ").append(userNavPrefs.routingType == NavigationPreferences.RoutingType.ROUTING_FASTEST ? "fastest" : "shortest").append(GlanceConstants.NEWLINE);
            builder.append("Highways: ").append(userNavPrefs.allowHighways).append(GlanceConstants.NEWLINE);
            builder.append("Toll roads: ").append(userNavPrefs.allowTollRoads).append(GlanceConstants.NEWLINE);
            builder.append("Ferries: ").append(userNavPrefs.allowFerries).append(GlanceConstants.NEWLINE);
            builder.append("Tunnels: ").append(userNavPrefs.allowTunnels).append(GlanceConstants.NEWLINE);
            builder.append("Unpaved Roads: ").append(userNavPrefs.allowUnpavedRoads).append(GlanceConstants.NEWLINE);
            builder.append("Auto Trains: ").append(userNavPrefs.allowAutoTrains).append(GlanceConstants.NEWLINE);
            builder.append("\n#############\n");
            builder.append("Maneuvers (entire route):");
            builder.append("\n#############\n\n");
            HereMapUtil.printRouteDetails(route, destinationLabel, null, builder);
            builder.append("\n#############\n");
            builder.append("Waypoints (entire route):");
            builder.append("\n#############\n\n");
            int idx = 0;
            assert route != null;
            for (Maneuver maneuver : route.getManeuvers()) {
                GeoCoordinate coordinate = maneuver.getCoordinate();
                if (coordinate != null) {
                    builder.append("waypoint").append(idx).append(": ").append(coordinate.getLatitude()).append(HereManeuverDisplayBuilder.COMMA).append(coordinate.getLongitude()).append(GlanceConstants.NEWLINE);
                    idx++;
                }
            }
            builder.append("\n#############\n");
            builder.append("Maneuvers (remaining route):");
            builder.append("\n#############\n\n");
            HereMapUtil.printRouteDetails(route, destinationLabel, null, builder, hereNavigationManager.getNextManeuver());
        }
        builder.append("\n#############\n");
        builder.append("Navdy info:");
        builder.append("\n#############\n\n");
        builder.append("Device ID: ").append(displayDeviceId.toString()).append(GlanceConstants.NEWLINE);
        builder.append("HUD app version:").append(BuildConfig.VERSION_NAME).append(GlanceConstants.NEWLINE);
        builder.append("Serial number: ").append(Build.SERIAL).append(GlanceConstants.NEWLINE);
        builder.append("Model: ").append(Build.MODEL).append(GlanceConstants.NEWLINE);
        builder.append("API Level: ").append(Build.VERSION.SDK_INT).append(GlanceConstants.NEWLINE);
        builder.append("Build type: ").append(Build.TYPE).append(GlanceConstants.NEWLINE);
        if (currentProfile.isDefaultProfile()) {
            userName = driverProfileHelper.getDriverProfileManager().getLastUserName();
            userEmail = driverProfileHelper.getDriverProfileManager().getLastUserEmail();
        } else {
            userName = currentProfile.getDriverName();
            userEmail = currentProfile.getDriverEmail();
        }
        if (!(TextUtils.isEmpty(userName) || TextUtils.isEmpty(userEmail))) {
            builder.append("Username: ").append(userName).append(GlanceConstants.NEWLINE);
            builder.append("Email: ").append(userEmail).append(GlanceConstants.NEWLINE);
        }
        return builder.toString();
    }
    
    public static boolean canReportIssue() {
        return ReportIssueService.mIssuesToSync.size() <= 10;
    }
    
    public static void dispatchReportNewIssue(final IssueType issueType) {
        final Intent intent = new Intent(HudApplication.getAppContext(), ReportIssueService.class);
        intent.setAction("Dump");
        intent.putExtra("EXTRA_ISSUE_TYPE", issueType);
        HudApplication.getAppContext().startService(intent);
    }
    
    public static void dispatchSync() {
        final Intent intent = new Intent(HudApplication.getAppContext(), ReportIssueService.class);
        intent.setAction("Sync");
        HudApplication.getAppContext().startService(intent);
    }
    
    private void dumpIssue(final IssueType issueType) throws Throwable {
        if (HereMapsManager.getInstance().isNavigationModeOn()) {
            String sb = String.valueOf(issueType.ordinal()) + "\n" +
                    this.buildDescription();
            this.saveJiraTicketDescriptionToFile(sb, false);
            ReportIssueService.mLastIssueSubmittedAt = System.currentTimeMillis();
        }
    }

    private void dumpSnapshot() throws Throwable {
        sLogger.d("Creating the snapshot of the HUD for support ticket");
        HUDLightUtils.showSnapshotCollection(HudApplication.getAppContext(), LightManager.getInstance(), false);
        String folder = PathManager.getInstance().getNonFatalCrashReportDir();
        DeviceUtil.takeDeviceScreenShot(folder + File.separator + "screen.png");
        List<File> snapshotFiles = new ArrayList<>();
        if (Build.TYPE.equals("eng")) {
            this.gestureService.dumpRecording();
        }
        this.driveRecorder.flushRecordings();
        this.gestureService.takeSnapShot(GestureServiceConnector.SNAPSHOT_PATH);
        File deviceInfoTextFile = new File(folder + File.separator + "deviceInfo.txt");
        if (deviceInfoTextFile.exists()) {
            IOUtils.deleteFile(this, deviceInfoTextFile.getAbsolutePath());
        }
        FileWriter fileWriter = new FileWriter(deviceInfoTextFile);
        ConnectionHandler connectionHandler = RemoteDeviceManager.getInstance().getConnectionHandler();
        if (connectionHandler != null) {
            DeviceInfo lastConnectedDeviceInfo = connectionHandler.getLastConnectedDeviceInfo();
            if (lastConnectedDeviceInfo != null) {
                fileWriter.write(CrashReportService.printDeviceInfo(lastConnectedDeviceInfo));
            }
        }
        fileWriter.flush();
        try {
            fileWriter.close();
        } catch (IOException e) {
            sLogger.e("Error closing the file writer for log file", e);
        } catch (Throwable t) {
            try {
                sLogger.e("Error creating a snapshot ", t);
                return;
            } finally {
                HUDLightUtils.showSnapshotCollection(HudApplication.getAppContext(), LightManager.getInstance(), true);
            }
        }
                // Grab logcat
        String cmd = "logcat -d -f"+folder+"/logcat";
        Runtime.getRuntime().exec(cmd);


        File file = new File(folder + File.separator + "temp");
        if (file.exists()) {
            IOUtils.deleteDirectory(this, file);
        }
        if (file.mkdirs()) {
            String stagingPath = file.getAbsolutePath();
            LogUtils.copySnapshotSystemLogs(stagingPath);
            GpsDeadReckoningManager.getInstance().dumpGpsInfo(stagingPath);
            PathManager.getInstance().collectEnvironmentInfo(stagingPath);
            File[] logFiles = file.listFiles();
            file = new File(folder + File.separator + "route.log");
            if (file.exists()) {
                IOUtils.deleteFile(this, file.getAbsolutePath());
            }
            String routeDescription = buildDescription();
            fileWriter = new FileWriter(file);
            fileWriter.write(routeDescription);
            fileWriter.flush();
            try {
                fileWriter.close();
            } catch (IOException e2) {
                sLogger.e("Error closing the file writer for route log file", e2);
            }
            snapshotFiles.add(new File(GestureServiceConnector.SNAPSHOT_PATH));
            snapshotFiles.add(new File(folder + File.separator + "screen.png"));
            snapshotFiles.add(file);
            snapshotFiles.add(deviceInfoTextFile);
            if (logFiles != null) {
                Collections.addAll(snapshotFiles, logFiles);
            }
            HashSet<File> driveLogFilesSet = new HashSet<>();
            List<File> latestFiles = getLatestDriveLogFiles(1);
            if (latestFiles != null && latestFiles.size() > 0) {
                snapshotFiles.addAll(latestFiles);
                driveLogFilesSet.addAll(latestFiles);
            }
            file = new File(folder + File.separator + ("snapshot-" + Build.SERIAL + "-" + Build.VERSION.INCREMENTAL + MusicDataUtils.ALTERNATE_SEPARATOR + SNAPSHOT_DATE_FORMAT.format(new Date(System.currentTimeMillis())) + ".zip"));
            if (file.exists()) {
                IOUtils.deleteFile(HudApplication.getAppContext(), file.getAbsolutePath());
            }
            File[] files = new File[snapshotFiles.size()];
            snapshotFiles.toArray(files);
            if (file.createNewFile()) {
                IOUtils.compressFilesToZip(this, files, file.getAbsolutePath());
                for (File file2 : snapshotFiles) {
                    if (!driveLogFilesSet.contains(file2)) {
                        IOUtils.deleteFile(HudApplication.getAppContext(), file2.getAbsolutePath());
                    }
                }
                IOUtils.deleteDirectory(this, file);
                lastSnapShotFile = file;
                sLogger.d("Snapshot File created " + lastSnapShotFile);
                CrashReportService.addSnapshotAsync(file.getAbsolutePath());
                HUDLightUtils.showSnapshotCollection(HudApplication.getAppContext(), LightManager.getInstance(), true);
                return;
            }
            sLogger.e("snapshot file could not be created");
            HUDLightUtils.showSnapshotCollection(HudApplication.getAppContext(), LightManager.getInstance(), true);
            return;
        }
        sLogger.e("could not create tempDirectory");
        HUDLightUtils.showSnapshotCollection(HudApplication.getAppContext(), LightManager.getInstance(), true);
    }
    
    public static void dumpSnapshotAsync() {
        final Intent intent = new Intent(HudApplication.getAppContext(), ReportIssueService.class);
        intent.setAction("Snapshot");
        HudApplication.getAppContext().startService(intent);
    }
    
    public static IssueType getIssueTypeForId(final int n) {
        final IssueType[] values = IssueType.values();
        IssueType issueType;
        if (n >= 0 && n < values.length) {
            issueType = values[n];
        }
        else {
            issueType = null;
        }
        return issueType;
    }
    
    public static List<File> getLatestDriveLogFiles(final int n) {
        final List<File> list = null;
        List<File> list2;
        if (n <= 0) {
            list2 = list;
        }
        else {
            final File[] listFiles = DriveRecorder.getDriveLogsDir("").listFiles();
            list2 = list;
            if (listFiles != null) {
                list2 = list;
                if (listFiles.length != 0) {
                    final ArrayList<File> list3 = new ArrayList<>();
                    Arrays.sort(listFiles, new Comparator<File>() {
                        @Override
                        public int compare(final File file, final File file2) {
                            return Long.compare(file.lastModified(), file2.lastModified());
                        }
                    });
                    int n2 = listFiles.length - 1;
                    while (true) {
                        list2 = list3;
                        if (n2 < 0) {
                            break;
                        }
                        list2 = list3;
                        if (list3.size() >= n * 4) {
                            break;
                        }
                        list3.add(listFiles[n2]);
                        --n2;
                    }
                }
            }
        }
        return list2;
    }
    
    private String getSummary(final IssueType issueType) {
        String s;
        if (issueType.getMessageStringResource() != 0) {
            s = "Navigation issue: [" + issueType.getIssueTypeCode() + "] " + this.getString(issueType.getTitleStringResource()) + ": " + this.getString(issueType.getMessageStringResource());
        }
        else {
            s = "Navigation issue: [" + issueType.getIssueTypeCode() + "] " + this.getString(issueType.getTitleStringResource());
        }
        return s;
    }
    
    public static void initialize() {
        final int n = 0;
        if (!ReportIssueService.mIsInitialized) {
            ReportIssueService.sLogger.d("Initializing the service statically");
            ReportIssueService.navigationIssuesFolder = PathManager.getInstance().getNavigationIssuesDir();
            final File[] listFiles = new File(ReportIssueService.navigationIssuesFolder).listFiles();
            final Logger sLogger = ReportIssueService.sLogger;
            final StringBuilder append = new StringBuilder().append("Number of Files waiting for sync :");
            int length;
            if (listFiles != null) {
                length = listFiles.length;
            }
            else {
                length = 0;
            }
            sLogger.d(append.append(length).toString());
            if (listFiles != null) {
                for (int length2 = listFiles.length, i = n; i < length2; ++i) {
                    final File file = listFiles[i];
                    ReportIssueService.sLogger.d("File " + file.getName());
                    ReportIssueService.mIssuesToSync.add(file);
                    if (ReportIssueService.mIssuesToSync.size() == 10) {
                        final File file2 = ReportIssueService.mIssuesToSync.poll();
                        ReportIssueService.sLogger.d("Deleting the old file " + file2.getName());
                        IOUtils.deleteFile(HudApplication.getAppContext(), file2.getAbsolutePath());
                    }
                }
            }
            ReportIssueService.mIsInitialized = true;
        }
    }
    
    private void onSyncComplete(final File file) {
        if (file != null) {
            ReportIssueService.sLogger.d("Removed the file from list :" + ReportIssueService.mIssuesToSync.remove(file));
            IOUtils.deleteFile(this, file.getAbsolutePath());
            dispatchSync();
        }
        ReportIssueService.mSyncing.set(false);
    }

    private void saveJiraTicketDescriptionToFile(String data, boolean isJson) throws Throwable {
        IOException e;
        Throwable th;
        File file = new File(navigationIssuesFolder + File.separator + DATE_FORMAT.format(new Date(System.currentTimeMillis())) + (isJson ? ".json" : ""));
        if (file.exists()) {
            IOUtils.deleteFile(this, file.getAbsolutePath());
        }
        FileWriter writer = null;
        try {
            if (file.createNewFile()) {
                FileWriter fileWriter = new FileWriter(file);
                try {
                    fileWriter.write(data);
                    fileWriter.close();
                    mIssuesToSync.add(file);
                    writer = fileWriter;
                    dispatchSync();
                } catch (IOException e2) {
                    e = e2;
                    writer = fileWriter;
                    try {
                        sLogger.e("Error while dumping the issue " + e.getMessage(), e);
                        IOUtils.closeStream(writer);
                    } catch (Throwable th2) {
                        th = th2;
                        IOUtils.closeStream(writer);
                        throw th;
                    }
                } catch (Throwable th3) {
                    th = th3;
                    writer = fileWriter;
                    IOUtils.closeStream(writer);
                    throw th;
                }
            }
            IOUtils.closeStream(writer);
        } catch (IOException e3) {
            e = e3;
            sLogger.e("Error while dumping the issue " + e.getMessage(), e);
            IOUtils.closeStream(writer);
        }
    }
    
    public static void scheduleSync() {
        final Intent intent = new Intent(HudApplication.getAppContext(), ReportIssueService.class);
        intent.setAction("Sync");
       ((AlarmManager) Objects.requireNonNull(HudApplication.getAppContext().getSystemService(Context.ALARM_SERVICE))).set(AlarmManager.ELAPSED_REALTIME, SystemClock.elapsedRealtime() + ReportIssueService.RETRY_INTERVAL, PendingIntent.getService(HudApplication.getAppContext(), 128, intent, Intent.FILL_IN_DATA));


       // ((AlarmManager) Objects.requireNonNull(HudApplication.getAppContext().getSystemService(Context.ALARM_SERVICE))).set(AlarmManager.ELAPSED_REALTIME, SystemClock.elapsedRealtime() + ReportIssueService.RETRY_INTERVAL, PendingIntent.getService(HudApplication.getAppContext(), 128, intent, Intent.FILL_IN_DATA));


    }
    
    public static void showSnapshotMenu() {
        final Bundle bundle = new Bundle();
        bundle.putInt("MENU_MODE", MainMenuScreen2.MenuMode.SNAPSHOT_TITLE_PICKER.ordinal());
        RemoteDeviceManager.getInstance().getBus().post(new ShowScreenWithArgs(Screen.SCREEN_MAIN_MENU, bundle, null, false));
    }
    
    private void submitJiraTicket(String s) {
        ReportIssueService.sLogger.d("Submitting jira ticket with title " + s);
        try {
            final Date date = new Date(System.currentTimeMillis());
            final ConnectionHandler connectionHandler = RemoteDeviceManager.getInstance().getConnectionHandler();
            final JSONObject jsonObject = new JSONObject();
            jsonObject.put("attachment", ReportIssueService.lastSnapShotFile.getAbsolutePath());
            final StringBuilder sb = new StringBuilder();
            final UIStateManager uiStateManager = RemoteDeviceManager.getInstance().getUiStateManager();
            final String format = ReportIssueService.SNAPSHOT_JIRA_TICKET_DATE_FORMAT.format(date);
            final DriverProfile currentProfile = DriverProfileHelper.getInstance().getCurrentProfile();
            final StringBuilder append = new StringBuilder().append("[Snapshot ").append(s).append(" ").append(format).append("] ");
            if (HomeScreen.DisplayMode.MAP.equals(uiStateManager.getHomescreenView().getDisplayMode())) {
                s = "Map";
            }
            else {
                s = "Dash";
            }
            sb.append(append.append(s).toString());
            s = HereMapUtil.getCurrentRoadName();
            if (!TextUtils.isEmpty(s)) {
                sb.append(" ").append(s);
            }
            if (!currentProfile.isDefaultProfile()) {
                sb.append(" , From ").append(currentProfile.getDriverEmail());
            }
            jsonObject.put("Summary", sb.toString());
            if (!currentProfile.isDefaultProfile()) {
                s = currentProfile.getDriverName();
                if (!TextUtils.isEmpty(s)) {
                    final String trim = s.split(" ")[0].trim();
                    ReportIssueService.sLogger.d("Assignee name for the snapshot " + trim);
                    jsonObject.put("assignee", trim);
                }
                s = currentProfile.getDriverEmail();
                if (!TextUtils.isEmpty(s)) {
                    jsonObject.put("assigneeEmail", s);
                }
            }
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("HUD ").append(Build.VERSION.INCREMENTAL).append("\n");
            if (connectionHandler != null) {
                final DeviceInfo lastConnectedDeviceInfo = connectionHandler.getLastConnectedDeviceInfo();
                if (lastConnectedDeviceInfo != null) {
                    if (DeviceInfo.Platform.PLATFORM_iOS.equals(lastConnectedDeviceInfo.platform)) {
                        s = "iPhone";
                    }
                    else {
                        s = "Android";
                    }
                    sb2.append(s);
                    sb2.append(" V").append(lastConnectedDeviceInfo.clientVersion).append("\n");
                }
            }
            sb2.append("Language : ").append(HudLocale.getCurrentLocale(HudApplication.getAppContext()).getLanguage());
            jsonObject.put("Environment", sb2.toString());
            this.saveJiraTicketDescriptionToFile(jsonObject.toString(), true);
        }
        catch (JSONException ex) {
            ex.printStackTrace();
            sLogger.e("JSON Error while submitting the issue " + ex.getMessage(), ex);

        } catch (Throwable throwable) {
            throwable.printStackTrace();
            sLogger.e("Error while submitting the issue " + throwable.getMessage(), throwable);
        }
    }
    
    public static void submitJiraTicketAsync(final String s) {
        final Intent intent = new Intent(HudApplication.getAppContext(), ReportIssueService.class);
        intent.setAction("Jira");
        intent.putExtra("EXTRA_SNAPSHOT_TITLE", s);
        HudApplication.getAppContext().startService(intent);
    }

    private void sync() {
        if (NetworkBandwidthController.getInstance().isNetworkAccessAllowed(NetworkBandwidthController$Component.JIRA)) {
            boolean deviceConnected = RemoteDeviceManager.getInstance().isRemoteDeviceConnected();
            boolean internetConnected = NetworkStateManager.isConnectedToNetwork(this);
            sLogger.d("Trying to sync , already syncing:" + mSyncing.get() + ", Navigation mode on:" + HereMapsManager.getInstance().isNavigationModeOn() + ", Device connected:" + deviceConnected + ", Internet connected :" + internetConnected);
            if (!deviceConnected || !internetConnected) {
                sLogger.d("Already sync is running, scheduling for a later time");
                scheduleSync();
                return;
            } else if (mSyncing.compareAndSet(false, true)) {
                final File fileToSync = mIssuesToSync.peek();
                String summary;
                if (fileToSync == null || !fileToSync.exists()) {
                    sLogger.d("Ticket file does not exist anymore");
                    onSyncComplete(fileToSync);
                    return;
                } else if (fileToSync.getName().endsWith(".json")) {
                    try {
                        sLogger.d("Submitting ticket from JSON file");
                        final JSONObject jsonObject = new JSONObject(IOUtils.convertFileToString(fileToSync.getAbsolutePath()));
                        summary = jsonObject.getString(SUMMARY);
                        String environment = jsonObject.has(ENVIRONMENT) ? jsonObject.getString(ENVIRONMENT) : "";
                        String assigneedNameTemp = jsonObject.has("assignee") ? jsonObject.getString("assignee") : "";
                        String assigneeEmailTemp = jsonObject.has(ASSIGNEE_EMAIL) ? jsonObject.getString(ASSIGNEE_EMAIL) : "";
                        if (TextUtils.isEmpty(assigneedNameTemp)) {
                            DriverProfile profile = DriverProfileHelper.getInstance().getCurrentProfile();
                            String driverName = profile.getDriverName();
                            if (!TextUtils.isEmpty(driverName)) {
                                assigneedNameTemp = driverName.trim().split(" ")[0];
                            }
                            assigneeEmailTemp = profile.getDriverEmail();
                        }
                        final String assigneeName = assigneedNameTemp;
                        final String assigneeEmail = assigneeEmailTemp;
                        sLogger.d("Assignee name " + assigneeName);
                        String attachment = jsonObject.has(ATTACHMENT) ? jsonObject.getString(ATTACHMENT) : "";
                        sLogger.d("Attachment " + attachment);
                        final File attachmentFile = new File(attachment);
                        if (attachmentFile.exists()) {
                            String ticketId = jsonObject.has(TICKET_ID) ? jsonObject.getString(TICKET_ID) : "";
                            if (TextUtils.isEmpty(ticketId)) {
                                sLogger.d("Submitting a new ticket from JSON file");
                                this.mJiraClient.submitTicket(HUD_PROJECT, ISSUE_TYPE, summary, "Snapshot, fill in the details", environment, new JiraClient.ResultCallback() {
                                    public void onSuccess(Object object) {
                                        boolean attachFiles = false;
                                        IOException e;
                                        Throwable th;
                                        String ticketId = null;
                                        if (object instanceof String) {
                                            ticketId = (String) object;
                                            ReportIssueService.sLogger.d("Issue reported " + ticketId);
                                        }
                                        try {
                                            jsonObject.put(ReportIssueService.TICKET_ID, ticketId);
                                            String data = jsonObject.toString();
                                            FileWriter writer = null;
                                            try {
                                                FileWriter writer2 = new FileWriter(fileToSync);
                                                try {
                                                    writer2.write(data);
                                                    writer2.close();
                                                    IOUtils.closeStream(writer2);
                                                    writer = writer2;
                                                } catch (IOException e2) {
                                                    e = e2;
                                                    writer = writer2;
                                                    try {
                                                        ReportIssueService.sLogger.e("Error while dumping the issue " + e.getMessage(), e);
                                                        IOUtils.closeStream(writer);
                                                        attachFiles = true;
                                                    } catch (Throwable th2) {
                                                        IOUtils.closeStream(writer);
                                                        throw th2;
                                                    }
                                                } catch (Throwable th3) {
                                                    writer = writer2;
                                                    IOUtils.closeStream(writer);
                                                    throw th3;
                                                }
                                            } catch (IOException e3) {
                                                e = e3;
                                                ReportIssueService.sLogger.e("Error while dumping the issue " + e.getMessage(), e);
                                                IOUtils.closeStream(writer);
                                                attachFiles = true;
                                            }
                                        } catch (JSONException e4) {
                                            ReportIssueService.sLogger.e("JSONException while writing the meta data to the file");
                                        }
                                        if (attachFiles) {
                                            ReportIssueService.sLogger.d("Attaching files to the submitted ticket");
                                            ReportIssueService.this.attachFilesToTheJiraTicket(ticketId, attachmentFile, new JiraClient.ResultCallback() {
                                                public void onSuccess(Object object) {
                                                    ReportIssueService.sLogger.d("Files successfully attached, Assigning the ticket ot " + assigneeName + ", Email : " + assigneeEmail);
                                                    ReportIssueService.this.mJiraClient.assignTicketForName((String) object, assigneeName, assigneeEmail, new JiraClient.ResultCallback() {
                                                        public void onSuccess(Object object) {
                                                            ReportIssueService.sLogger.d("Assigned the ticket successfully to " + object);
                                                            ReportIssueService.this.onSyncComplete(fileToSync);
                                                        }

                                                        public void onError(Throwable t) {
                                                            ReportIssueService.sLogger.e("Error Assigning the ticket", t);
                                                            ReportIssueService.this.onSyncComplete(fileToSync);
                                                        }
                                                    });
                                                }

                                                public void onError(Throwable t) {
                                                    ReportIssueService.this.syncLater();
                                                }
                                            });
                                        }
                                    }

                                    public void onError(Throwable t) {
                                        ReportIssueService.sLogger.e("Error during sync " + t);
                                        ReportIssueService.this.syncLater();
                                    }
                                });
                                return;
                            }
                            sLogger.d("Ticket already exists " + ticketId + " Attaching the files to the ticket");
                            attachFilesToTheJiraTicket(ticketId, attachmentFile, new JiraClient.ResultCallback() {
                                public void onSuccess(Object object) {
                                    ReportIssueService.sLogger.d("Files attached successfully");
                                    ReportIssueService.this.onSyncComplete(fileToSync);
                                }

                                public void onError(Throwable t) {
                                    ReportIssueService.sLogger.e("onError, error uploading the files ", t);
                                    ReportIssueService.this.syncLater();
                                }
                            });
                            return;
                        }
                        sLogger.e("Snapshot file has been deleted, not creating a ticket ");
                        onSyncComplete(fileToSync);
                        return;
                    } catch (Throwable e) {
                        sLogger.e("Not able to read json file ", e);
                        syncLater();
                        return;
                    }
                } else {
                    FileReader reader = null;
                    try {
                        Reader fileReader = new FileReader(fileToSync);
                        try {
                            BufferedReader bufferedReader = new BufferedReader(fileReader);
                            IssueType issueType = getIssueTypeForId(Integer.parseInt(bufferedReader.readLine().trim()));
                            if (issueType == null) {
                                throw new IOException("Bad file format");
                            }
                            summary = getSummary(issueType);
                            StringBuilder builder = new StringBuilder();
                            while (true) {
                                String line = bufferedReader.readLine();
                                if (line != null) {
                                    builder.append(line).append(GlanceConstants.NEWLINE);
                                } else {
                                    IOUtils.closeStream(fileReader);
                                    this.mJiraClient.submitTicket(PROJECT_NAME, ISSUE_TYPE, summary, builder.toString(), new JiraClient.ResultCallback() {
                                        public void onSuccess(Object object) {
                                            if (object instanceof String) {
                                                ReportIssueService.sLogger.d("Issue reported " + object);
                                            }
                                            ReportIssueService.sLogger.d("Sync succeeded");
                                            ReportIssueService.this.onSyncComplete(fileToSync);
                                        }

                                        public void onError(Throwable t) {
                                            ReportIssueService.sLogger.e("Error during sync " + t);
                                            ReportIssueService.this.syncLater();
                                        }
                                    });
                                    return;
                                }
                            }
                        } catch (Exception e2) {
                            IOUtils.closeStream(fileReader);
                            onSyncComplete(fileToSync);
                            return;
                        }
                    } catch (Exception e3) {
                        IOUtils.closeStream(reader);
                        onSyncComplete(fileToSync);
                        return;
                    }
                }
            } else {
                return;
            }
        }
        scheduleSync();
    }
    private void syncLater() {
        ReportIssueService.mSyncing.set(false);
        scheduleSync();
    }
    
    public void onCreate() {
        super.onCreate();
        Mortar.inject(HudApplication.getAppContext(), this);
        ReportIssueService.sLogger.d("ReportIssue service started");
        (this.mJiraClient = new JiraClient(this.mHttpManager.getClientCopy().readTimeout(120L, TimeUnit.SECONDS).connectTimeout(120L, TimeUnit.SECONDS).build())).setEncodedCredentials(CredentialUtil.getCredentials(this, "JIRA_CREDENTIALS"));
    }


    protected void onHandleIntent(Intent intent) {
        try {
            sLogger.d("onHandleIntent " + intent.toString());
            if (ACTION_DUMP.equals(intent.getAction())) {
                IssueType issueType = (IssueType) intent.getSerializableExtra(EXTRA_ISSUE_TYPE);
                sLogger.d("Dumping an issue " + issueType);
                if (issueType != null) {
                    dumpIssue(issueType);
                }
            } else if (ACTION_SNAPSHOT.equals(intent.getAction())) {
                dumpSnapshot();
            } else if (ACTION_SUBMIT_JIRA.equals(intent.getAction())) {
                String title = intent.getStringExtra(EXTRA_SNAPSHOT_TITLE);
                if (TextUtils.isEmpty(title)) {
                    title = HUD_PROJECT;
                }
                submitJiraTicket(title);
            } else {
                sLogger.d("Handling Sync request");
                sync();
            }
        } catch (Throwable t) {
            sLogger.e("Exception while handling intent ", t);
        }
    }
    
    static class FilesModifiedTimeComparator implements Comparator<File>
    {
        @Override
        public int compare(final File file, final File file2) {
            return (int)(file.lastModified() - file2.lastModified());
        }
    }
    
    public enum IssueType
    {
        INEFFICIENT_ROUTE_ETA_TRAFFIC(R.string.eta_inaccurate_traffic, 0, 107), 
        INEFFICIENT_ROUTE_SELECTED(R.string.route_inefficient, 0, 106), 
        OTHER(R.string.other_navigation_issue, 0, 105), 
        ROAD_CLOSED(R.string.road_closed, R.string.road_closed_message, 103), 
        ROAD_CLOSED_PERMANENT(R.string.permanent_closure, 0, 108), 
        ROAD_NAME(R.string.road_name, 0, 104), 
        WRONG_DIRECTION(R.string.wrong_direction, 0, 102);
        
        private int issueTypeCode;
        private int messageStringResource;
        private int titleStringResource;
        
        IssueType(final int titleStringResource, final int messageStringResource, final int issueTypeCode) {
            this.titleStringResource = titleStringResource;
            this.messageStringResource = messageStringResource;
            this.issueTypeCode = issueTypeCode;
        }
        
        public int getIssueTypeCode() {
            return this.issueTypeCode;
        }
        
        public int getMessageStringResource() {
            return this.messageStringResource;
        }
        
        public int getTitleStringResource() {
            return this.titleStringResource;
        }
    }
}
