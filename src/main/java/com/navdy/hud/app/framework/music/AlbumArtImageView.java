package com.navdy.hud.app.framework.music;

import com.navdy.hud.app.R;

public class AlbumArtImageView extends android.support.v7.widget.AppCompatImageView {
    final private static int ARTWORK_TRANSITION_DURATION = 500;
    private static com.navdy.service.library.log.Logger logger;
    final private Object artworkLock;
    private android.graphics.drawable.BitmapDrawable defaultArtwork;
    private android.os.Handler handler;
    private java.util.concurrent.atomic.AtomicBoolean isAnimatingArtwork;
    private boolean mask;
    private android.graphics.Bitmap nextArtwork;
    private String nextArtworkHash;
    private android.graphics.Paint paint;
    private android.graphics.RadialGradient radialGradient;

    static {
        logger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.framework.music.AlbumArtImageView.class);
    }

    public AlbumArtImageView(android.content.Context a) {
        this(a, null);
    }

    public AlbumArtImageView(android.content.Context a, android.util.AttributeSet a0) {
        this(a, a0, 0);
    }

    public AlbumArtImageView(android.content.Context a, android.util.AttributeSet a0, int i) {
        super(a, a0, i);
        this.isAnimatingArtwork = new java.util.concurrent.atomic.AtomicBoolean(false);
        this.artworkLock = new Object();
        this.handler = new android.os.Handler(android.os.Looper.getMainLooper());
        this.init(a0);
    }

    static Object access$000(com.navdy.hud.app.framework.music.AlbumArtImageView a) {
        return a.artworkLock;
    }

    static String access$100(com.navdy.hud.app.framework.music.AlbumArtImageView a) {
        return a.nextArtworkHash;
    }

    static java.util.concurrent.atomic.AtomicBoolean access$200(com.navdy.hud.app.framework.music.AlbumArtImageView a) {
        return a.isAnimatingArtwork;
    }

    static void access$300(com.navdy.hud.app.framework.music.AlbumArtImageView a) {
        a.animateArtwork();
    }

    private android.graphics.Bitmap addMask(android.graphics.Bitmap a) {
        android.graphics.Bitmap a0;
        if (a != null) {
            int i = a.getWidth();
            int i0 = a.getHeight();
            a0 = android.graphics.Bitmap.createBitmap(i, i0, android.graphics.Bitmap.Config.ARGB_8888);
            android.graphics.Canvas a1 = new android.graphics.Canvas(a0);
            a1.drawBitmap(a, 0.0f, 0.0f, null);
            this.paint.setShader(this.radialGradient);
            this.paint.setXfermode(new android.graphics.PorterDuffXfermode(android.graphics.PorterDuff.Mode.MULTIPLY));
            a1.drawRect(0.0f, 0.0f, (float)i, (float)i0, this.paint);
        } else {
            a0 = null;
        }
        return a0;
    }

    private void animateArtwork() {
        String _nextArtworkHash;
        android.graphics.Bitmap _nextArtwork;
        synchronized(this.artworkLock) {
            _nextArtworkHash = this.nextArtworkHash;
            _nextArtwork = this.nextArtwork;
            /*monexit(a0)*/
        }
        android.graphics.drawable.Drawable[] a5 = new android.graphics.drawable.Drawable[2];
//        android.graphics.drawable.Drawable a6 = this.getDrawable();

        this.getDrawable();


//      EDIT: Always fade from black
//        if (a6 != null) {
//            a5[0] = ((android.graphics.drawable.LayerDrawable)a6).getDrawable(0);
//        } else {
        a5[0] = new android.graphics.drawable.ColorDrawable(0);
//        }
        if (_nextArtwork == null) {
            logger.i("Bitmap image is null");
            a5[1] = this.defaultArtwork;
        } else {
            a5[1] = new android.graphics.drawable.BitmapDrawable(this.getContext().getResources(), _nextArtwork);
        }
        android.graphics.drawable.TransitionDrawable a7 = new android.graphics.drawable.TransitionDrawable(a5);
        this.setImageDrawable(a7);
        a7.setCrossFadeEnabled(true);
        a7.startTransition(ARTWORK_TRANSITION_DURATION);
        this.handler.postDelayed(new AlbumArtImageView$1(this, a5, _nextArtworkHash), 500L);
    }

    private void drawImmediately() {
        this.isAnimatingArtwork.set(false);
        this.handler.removeCallbacksAndMessages(null);
        android.graphics.drawable.Drawable[] a = new android.graphics.drawable.Drawable[1];
        if (this.nextArtwork == null) {
            logger.i("Bitmap image is null");
            a[0] = this.defaultArtwork;
        } else {
            a[0] = new android.graphics.drawable.BitmapDrawable(this.getContext().getResources(), this.nextArtwork);
        }
        this.setImageDrawable(new android.graphics.drawable.LayerDrawable(a));
    }

    public void clearImmediately() {
        this.isAnimatingArtwork.set(false);
        this.handler.removeCallbacksAndMessages(null);
        android.graphics.drawable.Drawable[] a = new android.graphics.drawable.Drawable[1];
        a[0] = new android.graphics.drawable.ColorDrawable(0);
        this.setImageDrawable(new android.graphics.drawable.LayerDrawable(a));
    }

    private void init(android.util.AttributeSet a) {
        int i;
        if (a == null) {
            i = R.drawable.icon_mm_music_control_blank;
        } else {
            android.content.res.TypedArray a0 = this.getContext().obtainStyledAttributes(a, com.navdy.hud.app.R.styleable.AlbumArtImageView);
            this.mask = a0.getBoolean(0, false);
            i = a0.getResourceId(R.styleable.AlbumArtImageView_defaultArtwork, R.drawable.icon_mm_music_control_blank);
            a0.recycle();
        }
        android.graphics.drawable.BitmapDrawable a1 = (android.graphics.drawable.BitmapDrawable)this.getResources().getDrawable(i);
        if (this.mask) {
            this.paint = new android.graphics.Paint();
            int[] a2 = this.getResources().getIntArray(R.array.smart_dash_music_gauge_gradient);
            android.content.Context a3 = this.getContext();
            int[] a4 = new int[1];
            a4[0] = 16842996;
            android.content.res.TypedArray a5 = a3.obtainStyledAttributes(a, a4);
            float f = (float)(a5.getDimensionPixelSize(0, 130) / 2);
            a5.recycle();
            this.radialGradient = new android.graphics.RadialGradient(f, f, f, a2, null, android.graphics.Shader.TileMode.CLAMP);
            this.defaultArtwork = new android.graphics.drawable.BitmapDrawable(this.getResources(), this.addMask(a1.getBitmap()));
        } else {
            this.defaultArtwork = a1;
        }
        android.graphics.drawable.Drawable[] a6 = new android.graphics.drawable.Drawable[1];
        a6[0] = this.defaultArtwork;
        this.setImageDrawable(new android.graphics.drawable.LayerDrawable(a6));
    }

    public void setArtworkBitmap(android.graphics.Bitmap a, boolean animate) {
        String hashForBitmap = com.navdy.service.library.util.IOUtils.hashForBitmap(a);
        boolean isSame = android.text.TextUtils.equals(this.nextArtworkHash, hashForBitmap);
        if (isSame) {
            logger.d("Already set this artwork, ignoring");
        } else {
            synchronized (this) {
                this.nextArtworkHash = hashForBitmap;
                if (this.mask) {
                    a = this.addMask(a);
                }
                this.nextArtwork = a;
                /*monexit(this)*/

            }
            if (animate) {
                if (this.isAnimatingArtwork.compareAndSet(false, true)) {
                    this.animateArtwork();
                } else {
                    logger.d("Already animating");
                }
            } else {
                this.drawImmediately();
            }
        }
        //        while(true) {
//            try {
//                /*monexit(this)*/;
//            } catch(IllegalMonitorStateException | NullPointerException a2) {
//                Throwable a3 = a2;
//                a0 = a3;
//                continue;
//            }
//            throw a0;
//        }
    }
}
