package com.navdy.hud.app.framework.network;

class NetworkBandwidthController$1 implements Runnable {
    final com.navdy.hud.app.framework.network.NetworkBandwidthController this$0;

    final private static String DATA = "data";
    final private static String EVENT = "event";
    final private static String EVENT_DNS_INFO = "dnslookup";
    final private static String EVENT_NETSTAT = "netstat";
    final private static String FD = "fd";
    final private static String FROM = "from";
    final private static String GOOGLE_DNS_IP = "8.8.4.4";
    final private static String HERE_ROUTE_END_POINT_PATTERN = "route.hybrid.api.here.com";
    final private static String HOST = "host";
    final private static String IP = "ip";
    final private static int NETWORK_STAT_INFO_PORT = 23655;
    final private static String RX = "rx";
    final private static String TO = "to";
    final private static String TX = "tx";

    NetworkBandwidthController$1(com.navdy.hud.app.framework.network.NetworkBandwidthController a) {
        super();
        this.this$0 = a;
    }
    
    public void run() {
        java.net.DatagramSocket a;
        Throwable a0 = null;
        label2: {
            label1: {
                try {
                    com.navdy.hud.app.framework.network.NetworkBandwidthController.access$000().v("networkStat thread enter");
                    a = new java.net.DatagramSocket(NETWORK_STAT_INFO_PORT, java.net.InetAddress.getByName("127.0.0.1"));
                    break label1;
                } catch(Throwable a1) {
                    a0 = a1;
                }
                a = null;
                break label2;
            }
            try {
                a.setReceiveBufferSize(16384);
                byte[] a2 = new byte[2048];
                java.net.DatagramPacket a3 = new java.net.DatagramPacket(a2, a2.length);
//                label0: while(true) {
                    try {
                        int i;
                        com.navdy.hud.app.framework.network.NetworkBandwidthController$ComponentInfo a4;
                        a.receive(a3);
                        org.json.JSONObject a6 = new org.json.JSONObject(new String(a2, 0, a3.getLength()));
                        String s = a6.getString(EVENT);
                        switch(s.hashCode()) {
                            case 1843370353: {
                                i = (s.equals(EVENT_NETSTAT)) ? 1 : -1;
                                break;
                            }
                            case 260369379: {
                                i = (s.equals(EVENT_DNS_INFO)) ? 0 : -1;
                                break;
                            }
                            default: {
                                i = -1;
                            }
                        }
                        switch(i) {
                            case 1: {
                                org.json.JSONArray a7 = a6.getJSONArray(DATA);
                                if (a7.length() == 0) {
                                   break;
                                }
                                org.json.JSONObject a8 = a7.getJSONObject(0);
                                String s0 = a8.getString(FROM);
                                String s1 = a8.getString(TO);
                                int i0 = a8.getInt(TX);
                                int i1 = a8.getInt(RX);
                                int i2 = a8.getInt(FD);
                                if (com.navdy.hud.app.framework.network.NetworkBandwidthController.access$000().isLoggable(2)) {
                                    com.navdy.hud.app.framework.network.NetworkBandwidthController.access$000().v("[" + i2 + "] tx[" + i0 + "] rx[" + i1 + "] from[" + s0 + "] to[" + s1 + "]");
                                }
                                if (!android.text.TextUtils.isEmpty(s1)) {
                                    s0 = s1;
                                }
                                if (GOOGLE_DNS_IP.equals(s0)) {
                                    break;
                                }
                                com.navdy.hud.app.framework.network.NetworkBandwidthController.access$200(this.this$0).addStat(s0, i0, i1, i2);
                                if (i0 <= 0 && i1 <= 0) {
                                    break;
                                }
                                String s2 = com.navdy.hud.app.framework.network.NetworkBandwidthController.access$100(this.this$0).getHostnamefromIP(s0);
                                if (s2 == null) {
                                    break;
                                }
                                com.navdy.hud.app.framework.network.NetworkBandwidthController$Component a9 = (com.navdy.hud.app.framework.network.NetworkBandwidthController$Component)com.navdy.hud.app.framework.network.NetworkBandwidthController.access$300(this.this$0).get(s2);
                                if (a9 == null && s2.contains(HERE_ROUTE_END_POINT_PATTERN)) {
                                    a9 = com.navdy.hud.app.framework.network.NetworkBandwidthController$Component.HERE_ROUTE;
                                }
                                if (a9 == null) {
                                    break;
                                }
                                if (!com.navdy.hud.app.framework.network.NetworkBandwidthController.access$400(this.this$0) && a9 == com.navdy.hud.app.framework.network.NetworkBandwidthController$Component.HERE_TRAFFIC && i1 > 0) {
                                    com.navdy.hud.app.framework.network.NetworkBandwidthController.access$000().i("traffic data downloaded once");
                                    com.navdy.hud.app.framework.network.NetworkBandwidthController.access$402(this.this$0);
                                }
                                a4 = (com.navdy.hud.app.framework.network.NetworkBandwidthController$ComponentInfo)com.navdy.hud.app.framework.network.NetworkBandwidthController.access$500(this.this$0).get(a9);
                                synchronized((com.navdy.hud.app.framework.network.NetworkBandwidthController$ComponentInfo)com.navdy.hud.app.framework.network.NetworkBandwidthController.access$500(this.this$0).get(a9)) {
                                    a4.lastActivity = android.os.SystemClock.elapsedRealtime();
                                    /*monexit(a4)*/
                                    break;
                                }
                            }
                            case 0: {
                                String s3 = a6.getString(HOST);
                                if ("localhost".equals(s3)) {
                                    break;
                                }
                                org.json.JSONArray a11 = a6.getJSONArray(IP);
                                int i3 = a11.length();
                                if (i3 == 0) {
                                    break;
                                }
                                int i4 = 0;
                                while(i4 < i3) {
                                    String s4 = a11.getString(i4);
                                    com.navdy.hud.app.framework.network.NetworkBandwidthController.access$100(this.this$0).addEntry(s4, s3);
                                    if (com.navdy.hud.app.framework.network.NetworkBandwidthController.access$000().isLoggable(2)) {
                                        com.navdy.hud.app.framework.network.NetworkBandwidthController.access$000().v("dnslookup " + s3 + " : " + s4);
                                    }
                                    i4 = i4 + 1;
                                }
                                break;
                            }
                            default: {
                                com.navdy.hud.app.framework.network.NetworkBandwidthController.access$000().v("invalid command:" + s);
                            }
                        }
//                        while(true) {
//                            try {
//                                /*monexit(a4)*/;
//                            } catch(IllegalMonitorStateException | NullPointerException a12) {
//                                Throwable a13 = a12;
//                                a5 = a13;
//                                continue;
//                            }
//                            throw a5;
//                        }
                    } catch(Throwable a14) {
                        com.navdy.hud.app.framework.network.NetworkBandwidthController.access$000().e("[networkStat]", a14);
                    }
//                }
            } catch(Throwable a15) {
                a0 = a15;
            }
        }
        com.navdy.hud.app.framework.network.NetworkBandwidthController.access$000().e(a0);
        if (a != null) {
            com.navdy.service.library.util.IOUtils.closeStream(a);
        }
        com.navdy.hud.app.framework.network.NetworkBandwidthController.access$000().v("networkStat thread exit");
    }
}
