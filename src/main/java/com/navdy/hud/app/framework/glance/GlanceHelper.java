package com.navdy.hud.app.framework.glance;

import java.io.ObjectInputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;

import com.navdy.hud.app.R;
import com.navdy.hud.app.framework.toast.IToastCallback;
import com.navdy.hud.app.framework.toast.ToastManager;
import android.os.Bundle;
import com.navdy.hud.app.framework.DriverProfileHelper;
import android.view.View;
import android.content.Context;
import android.text.StaticLayout;
import android.text.Layout;
import android.widget.TextView;

import com.navdy.hud.app.framework.toast.ToastManager$ToastParams;
import com.navdy.service.library.events.glances.FuelConstants;
import com.navdy.service.library.events.glances.CalendarConstants;
import android.text.TextUtils;
import com.navdy.service.library.events.glances.GenericConstants;
import com.navdy.service.library.events.glances.SocialConstants;
import com.navdy.service.library.events.glances.EmailConstants;
import com.navdy.service.library.events.glances.MessageConstants;
import java.util.Date;
import com.navdy.hud.app.common.TimeHelper;
import java.util.Iterator;
import com.navdy.hud.app.framework.contacts.ContactUtil;
import com.navdy.service.library.events.glances.KeyValue;
import java.util.Map;
import com.navdy.service.library.events.glances.GlanceEvent;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.HudApplication;
import com.navdy.service.library.log.Logger;
import android.content.res.Resources;
import com.navdy.hud.app.gesture.GestureServiceConnector;
import java.util.concurrent.atomic.AtomicLong;
import java.util.HashMap;

public class GlanceHelper
{
    private static final HashMap<String, GlanceApp> APP_ID_MAP;
    public static final String DELETE_ALL_GLANCES_TOAST_ID = "glance-deleted";
    public static final String FUEL_PACKAGE = "com.navdy.fuel";
    private static final AtomicLong ID;
    public static final String IOS_PHONE_PACKAGE = "com.apple.mobilephone";
    public static final String MICROSOFT_OUTLOOK = "com.microsoft.Office.Outlook";
    public static final String MUSIC_PACKAGE = "com.navdy.music";
    public static final String PHONE_PACKAGE = "com.navdy.phone";
    public static final String SMS_PACKAGE = "com.navdy.sms";
    public static final String TRAFFIC_PACKAGE = "com.navdy.traffic";
    public static final String VOICE_SEARCH_PACKAGE = "com.navdy.voice.search";
    private static final StringBuilder builder;
    private static final GestureServiceConnector gestureServiceConnector;
    private static final Resources resources;
    private static final Logger sLogger;
    
    static {
        sLogger = new Logger(GlanceHelper.class);
        ID = new AtomicLong(1L);
        builder = new StringBuilder();
        (APP_ID_MAP = new HashMap<String, GlanceApp>()).put("com.navdy.fuel", GlanceApp.FUEL);
        GlanceHelper.APP_ID_MAP.put("com.google.android.calendar", GlanceApp.GOOGLE_CALENDAR);
        GlanceHelper.APP_ID_MAP.put("com.google.android.gm", GlanceApp.GOOGLE_MAIL);
        GlanceHelper.APP_ID_MAP.put("com.google.android.talk", GlanceApp.GOOGLE_HANGOUT);
        GlanceHelper.APP_ID_MAP.put("com.Slack", GlanceApp.SLACK);
        GlanceHelper.APP_ID_MAP.put("com.whatsapp", GlanceApp.WHATS_APP);
        GlanceHelper.APP_ID_MAP.put("com.facebook.orca", GlanceApp.FACEBOOK_MESSENGER);
        GlanceHelper.APP_ID_MAP.put("com.facebook.katana", GlanceApp.FACEBOOK);
        GlanceHelper.APP_ID_MAP.put("com.twitter.android", GlanceApp.TWITTER);
        GlanceHelper.APP_ID_MAP.put("com.navdy.sms", GlanceApp.SMS);
        GlanceHelper.APP_ID_MAP.put("com.google.android.apps.inbox", GlanceApp.GOOGLE_INBOX);
        GlanceHelper.APP_ID_MAP.put("com.google.calendar", GlanceApp.GOOGLE_CALENDAR);
        GlanceHelper.APP_ID_MAP.put("com.google.Gmail", GlanceApp.GOOGLE_MAIL);
        GlanceHelper.APP_ID_MAP.put("com.google.hangouts", GlanceApp.GOOGLE_HANGOUT);
        GlanceHelper.APP_ID_MAP.put("com.tinyspeck.chatlyio", GlanceApp.SLACK);
        GlanceHelper.APP_ID_MAP.put("net.whatsapp.WhatsApp", GlanceApp.WHATS_APP);
        GlanceHelper.APP_ID_MAP.put("com.facebook.Messenger", GlanceApp.FACEBOOK_MESSENGER);
        GlanceHelper.APP_ID_MAP.put("com.facebook.Facebook", GlanceApp.FACEBOOK);
        GlanceHelper.APP_ID_MAP.put("com.atebits.Tweetie2", GlanceApp.TWITTER);
        GlanceHelper.APP_ID_MAP.put("com.apple.MobileSMS", GlanceApp.IMESSAGE);
        GlanceHelper.APP_ID_MAP.put("com.apple.mobilemail", GlanceApp.APPLE_MAIL);
        GlanceHelper.APP_ID_MAP.put("com.apple.mobilecal", GlanceApp.APPLE_CALENDAR);
        GlanceHelper.APP_ID_MAP.put("com.microsoft.Office.Outlook", GlanceApp.GENERIC_MAIL);
        resources = HudApplication.getAppContext().getResources();
        gestureServiceConnector = RemoteDeviceManager.getInstance().getGestureServiceConnector();
    }
    
    public static Map<String, String> buildDataMap(final GlanceEvent glanceEvent) {
        final HashMap<String, String> hashMap = new HashMap<String, String>();
        if (glanceEvent.glanceData != null) {
            for (final KeyValue keyValue : glanceEvent.glanceData) {
                hashMap.put(keyValue.key, ContactUtil.sanitizeString(keyValue.value));
            }
        }
        return hashMap;
    }
    
    public static String getCalendarTime(final long n, final StringBuilder sb, final StringBuilder sb2, final TimeHelper timeHelper) {
        final long n2 = Math.round((n - System.currentTimeMillis()) / 60000.0f);
        sb.setLength(0);
        sb2.setLength(0);
        String s;
        if (n2 >= 60L || n2 < -5L) {
            s = timeHelper.formatTime12Hour(new Date(n), sb2, true);
        }
        else if (n2 <= 0L) {
            s = GlanceConstants.nowStr;
        }
        else {
            final String value = String.valueOf(n2);
            sb.append(GlanceConstants.minute);
            s = value;
        }
        return s;
    }
    
    public static String getFrom(final GlanceApp glanceApp, final Map<String, String> map) {
        final String s = null;
        String s2 = null;
        if (map == null) {
            s2 = null;
        }
        else {
            switch (glanceApp) {
                default:
                    s2 = s;
                    break;
                case GOOGLE_HANGOUT:
                case IMESSAGE:
                case SMS:
                    s2 = map.get(MessageConstants.MESSAGE_FROM.name());
                    break;
            }
        }
        return s2;
    }
    
    public static GestureServiceConnector getGestureService() {
        return GlanceHelper.gestureServiceConnector;
    }

    public static synchronized String getGlanceMessage(GlanceApp glanceApp, Map<String, String> dataMap) {
        String message;
        synchronized (GlanceHelper.class) {
            String body;
            switch (glanceApp) {
                case GOOGLE_HANGOUT:
                case SLACK:
                case WHATS_APP:
                case FACEBOOK_MESSENGER:
                case GENERIC_MESSAGE:
                case IMESSAGE:
                case SMS:
                    body = (String) dataMap.get(MessageConstants.MESSAGE_BODY.name());
                    if (body != null) {
                        builder.append(body);
                        break;
                    }
                    break;
                case FACEBOOK:
                case TWITTER:
                case GENERIC_SOCIAL:
                    body = (String) dataMap.get(SocialConstants.SOCIAL_MESSAGE.name());
                    if (body != null) {
                        builder.append(body);
                        break;
                    }
                    break;
                case GOOGLE_MAIL:
                case APPLE_MAIL:
                case GENERIC_MAIL:
                case GOOGLE_INBOX:
                    body = (String) dataMap.get(EmailConstants.EMAIL_BODY.name());
                    if (body != null) {
                        builder.append(body);
                        break;
                    }
                    break;
                case GENERIC:
                    body = (String) dataMap.get(GenericConstants.GENERIC_MESSAGE.name());
                    if (body != null) {
                        builder.append(body);
                        break;
                    }
                    break;
            }
            message = builder.toString();
            builder.setLength(0);
        }
        return message;
    }

    public static GlanceApp getGlancesApp(final GlanceEvent glanceEvent) {
        GlanceApp glanceApp;
        if (glanceEvent == null) {
            glanceApp = null;
        }
        else if ((glanceApp = GlanceHelper.APP_ID_MAP.get(glanceEvent.provider)) == null) {
            switch (glanceEvent.glanceType) {
                default:
                    glanceApp = null;
                    break;
                case GLANCE_TYPE_EMAIL:
                    glanceApp = GlanceApp.GENERIC_MAIL;
                    break;
                case GLANCE_TYPE_CALENDAR:
                    glanceApp = GlanceApp.GENERIC_CALENDAR;
                    break;
                case GLANCE_TYPE_MESSAGE:
                    glanceApp = GlanceApp.GENERIC_MESSAGE;
                    break;
                case GLANCE_TYPE_SOCIAL:
                    glanceApp = GlanceApp.GENERIC_SOCIAL;
                    break;
            }
        }
        return glanceApp;
    }
    
    public static GlanceApp getGlancesApp(final String s) {
        return GlanceHelper.APP_ID_MAP.get(s);
    }
    
    public static int getIcon(final String s) {
        final int n = -1;
        int n2 = 0;
        if (TextUtils.isEmpty((CharSequence)s)) {
            n2 = n;
        }
        else {
            int n3 = 0;
            Label_0046: {
                switch (s.hashCode()) {
                    case -9470533:
                        if (s.equals("GLANCE_ICON_NAVDY_MAIN")) {
                            n3 = 0;
                            break Label_0046;
                        }
                        break;
                    case -30680433:
                        if (s.equals("GLANCE_ICON_MESSAGE_SIDE_BLUE")) {
                            n3 = 1;
                            break Label_0046;
                        }
                        break;
                    default:
                        sLogger.i("ERROR: getIcon hashCode: " + Integer.toString(s.hashCode()) + ", s: " + s);
                }
                n3 = -1;
            }
            switch (n3) {
                default:
                    n2 = n;
                    break;
                case 0:
                    n2 = R.drawable.icon_user_navdy;
                    break;
                case 1:
                    n2 = R.drawable.icon_message_blue;
                    break;
            }
        }
        return n2;
    }
    
    public static String getId() {
        return String.valueOf(GlanceHelper.ID.getAndIncrement());
    }
    
    public static GlanceViewCache.ViewType getLargeViewType(final GlanceApp glanceApp) {
        GlanceViewCache.ViewType viewType = null;
        switch (glanceApp) {
            default:
                viewType = GlanceViewCache.ViewType.BIG_GLANCE_MESSAGE_SINGLE;
                break;
            case GOOGLE_CALENDAR:
            case APPLE_CALENDAR:
            case GENERIC_CALENDAR:
                viewType = GlanceViewCache.ViewType.BIG_CALENDAR;
                break;
            case FUEL:
                viewType = GlanceViewCache.ViewType.BIG_MULTI_TEXT;
                break;
        }
        return viewType;
    }
    
    public static String getNotificationId(final GlanceEvent.GlanceType glanceType, final String s) {
        String s2 = null;
        switch (glanceType) {
            default:
                s2 = "glance_gen_";
                break;
            case GLANCE_TYPE_CALENDAR:
                s2 = "glance_cal_";
                break;
            case GLANCE_TYPE_EMAIL:
                s2 = "glance_email_";
                break;
            case GLANCE_TYPE_MESSAGE:
                s2 = "glance_msg_";
                break;
            case GLANCE_TYPE_SOCIAL:
                s2 = "glance_soc_";
                break;
            case GLANCE_TYPE_FUEL:
                s2 = "glance_fuel_";
                break;
        }
        return s2 + s;
    }
    
    public static String getNotificationId(final GlanceEvent glanceEvent) {
        return getNotificationId(glanceEvent.glanceType, glanceEvent.id);
    }
    
    public static String getNumber(final GlanceApp glanceApp, final Map<String, String> map) {
        final String s = null;
        String s2 = null;
        if (map == null) {
            s2 = null;
        }
        else {
            switch (glanceApp) {
                default:
                    s2 = s;
                    break;
                case GOOGLE_HANGOUT:
                case IMESSAGE:
                case SMS:
                    s2 = map.get(MessageConstants.MESSAGE_FROM_NUMBER.name());
                    break;
            }
        }
        return s2;
    }
    
    public static GlanceViewCache.ViewType getSmallViewType(final GlanceApp glanceApp) {
        GlanceViewCache.ViewType viewType = null;
        switch (glanceApp) {
            default:
                viewType = GlanceViewCache.ViewType.SMALL_GLANCE_MESSAGE;
                break;
            case GOOGLE_CALENDAR:
            case APPLE_CALENDAR:
            case GENERIC_CALENDAR:
                viewType = GlanceViewCache.ViewType.SMALL_SIGN;
                break;
        }
        return viewType;
    }

    public static String getSubTitle(GlanceApp app, Map<String, String> data) {
        String subTitle = "";
        if (data == null) {
            return subTitle;
        }
        switch (app) {
            case SLACK:
                subTitle = (String) data.get(MessageConstants.MESSAGE_DOMAIN.name());
                if (subTitle != null) {
                    subTitle = resources.getString(R.string.slack_team, subTitle);
                    break;
                }
                break;
            case GOOGLE_CALENDAR:
            case APPLE_CALENDAR:
            case GENERIC_CALENDAR:
                subTitle = (String) data.get(CalendarConstants.CALENDAR_TIME_STR.name());
                break;
            case GOOGLE_MAIL:
            case APPLE_MAIL:
            case GENERIC_MAIL:
            case GOOGLE_INBOX:
                subTitle = (String) data.get(EmailConstants.EMAIL_SUBJECT.name());
                break;
            case FUEL:
                if (data.get(FuelConstants.NO_ROUTE.name()) == null) {
                    subTitle = GlanceConstants.fuelNotificationSubtitle;
                    break;
                }
                break;
            case GENERIC:
                subTitle = (String) data.get(GenericConstants.GENERIC_TITLE.name());
                break;
        }
        if (subTitle == null) {
            subTitle = "";
        }
        return subTitle;
    }
    
    public static String getTimeStr(final long n, final Date date, final TimeHelper timeHelper) {
        String s;
        if (n - date.getTime() <= 60000L) {
            s = GlanceConstants.nowStr;
        }
        else {
            s = timeHelper.formatTime(date, null);
        }
        return s;
    }
    
    public static String getTitle(final GlanceApp glanceApp, final Map<String, String> map) {
        String s;
        if (map == null) {
            s = "";
        }
        else {
            String s2 = null;
            switch (glanceApp) {
                default:
                    s2 = GlanceConstants.notification;
                    break;
                case GOOGLE_HANGOUT:
                    s2 = map.get(MessageConstants.MESSAGE_FROM.name());
                    break;
                case SLACK:
                case WHATS_APP:
                case FACEBOOK_MESSENGER:
                case GENERIC_MESSAGE:
                    s2 = map.get(MessageConstants.MESSAGE_FROM.name());
                    break;
                case FACEBOOK:
                    if (TextUtils.isEmpty((CharSequence)(s2 = map.get(SocialConstants.SOCIAL_FROM.name())))) {
                        s2 = GlanceConstants.facebook;
                        break;
                    }
                    break;
                case TWITTER:
                case GENERIC_SOCIAL:
                    s2 = map.get(SocialConstants.SOCIAL_FROM.name());
                    break;
                case IMESSAGE:
                    if ((s2 = map.get(MessageConstants.MESSAGE_FROM.name())) == null) {
                        s2 = map.get(MessageConstants.MESSAGE_FROM_NUMBER.name());
                        break;
                    }
                    break;
                case GOOGLE_CALENDAR:
                case APPLE_CALENDAR:
                case GENERIC_CALENDAR:
                    s2 = map.get(CalendarConstants.CALENDAR_TITLE.name());
                    break;
                case GOOGLE_MAIL:
                case APPLE_MAIL:
                case GENERIC_MAIL:
                case GOOGLE_INBOX:
                    if ((s2 = map.get(EmailConstants.EMAIL_FROM_NAME.name())) == null) {
                        s2 = map.get(EmailConstants.EMAIL_FROM_EMAIL.name());
                        break;
                    }
                    break;
                case SMS:
                    if ((s2 = map.get(MessageConstants.MESSAGE_FROM.name())) == null) {
                        s2 = map.get(MessageConstants.MESSAGE_FROM_NUMBER.name());
                        break;
                    }
                    break;
                case FUEL:
                    if (map.get(FuelConstants.FUEL_LEVEL.name()) != null) {
                        s2 = String.format("%s %s%%", GlanceConstants.fuelNotificationTitle, map.get(FuelConstants.FUEL_LEVEL.name()));
                        break;
                    }
                    s2 = GlanceHelper.resources.getString(R.string.gas_station);
                    break;
            }
            String s3;
            if ((s3 = s2) == null) {
                s3 = "";
            }
            s = s3;
        }
        return s;
    }

    public static synchronized String getTtsMessage(GlanceApp glanceApp, Map<String, String> dataMap, StringBuilder stringBuilder1, StringBuilder stringBuilder2, TimeHelper timeHelper) {
        String tts;
        synchronized (GlanceHelper.class) {
            String body;
            switch (glanceApp) {
                case GOOGLE_HANGOUT:
                case SLACK:
                case WHATS_APP:
                case FACEBOOK_MESSENGER:
                case GENERIC_MESSAGE:
                case IMESSAGE:
                case SMS:
                    body = (String) dataMap.get(MessageConstants.MESSAGE_BODY.name());
                    if (body != null) {
                        builder.append(body);
                        break;
                    }
                    break;
                case FACEBOOK:
                case TWITTER:
                case GENERIC_SOCIAL:
                    body = (String) dataMap.get(SocialConstants.SOCIAL_MESSAGE.name());
                    if (body != null) {
                        builder.append(body);
                        break;
                    }
                    break;
                case GOOGLE_CALENDAR:
                case APPLE_CALENDAR:
                case GENERIC_CALENDAR:
                    String location = (String) dataMap.get(CalendarConstants.CALENDAR_LOCATION.name());
                    String title = (String) dataMap.get(CalendarConstants.CALENDAR_TITLE.name());
                    long millis = 0;
                    String str = (String) dataMap.get(CalendarConstants.CALENDAR_TIME.name());
                    if (str != null) {
                        try {
                            millis = Long.parseLong(str);
                        } catch (Throwable t) {
                            sLogger.e(t);
                        }
                    }
                    String time = null;
                    if (millis > 0) {
                        String data = getCalendarTime(millis, stringBuilder1, stringBuilder2, timeHelper);
                        if (TextUtils.isEmpty(stringBuilder1.toString())) {
                            String pmMarker = stringBuilder2.toString();
                            if (TextUtils.isEmpty(pmMarker)) {
                                time = data + GlanceConstants.PERIOD;
                            } else {
                                String ampmMarker = TextUtils.equals(pmMarker, GlanceConstants.pmMarker) ? GlanceConstants.pm : GlanceConstants.am;
                                time = resources.getString(R.string.tts_calendar_time_at, new Object[]{data, ampmMarker});
                            }
                        } else {
                            try {
                                time = Long.parseLong(data) > 1 ? resources.getString(R.string.tts_calendar_time_mins, new Object[]{data}) : resources.getString(R.string.tts_calendar_time_min, new Object[]{data});
                            } catch (Throwable th) {
                                sLogger.e(th);
                            }
                        }
                    }
                    if (title != null) {
                        builder.append(resources.getString(R.string.tts_calendar_title, new Object[]{title}));
                    }
                    if (time != null) {
                        builder.append(" ");
                        builder.append(time);
                    }
                    if (location != null) {
                        builder.append(" ");
                        builder.append(resources.getString(R.string.tts_calendar_location, new Object[]{location}));
                        break;
                    }
                    break;
                case GOOGLE_MAIL:
                case APPLE_MAIL:
                case GENERIC_MAIL:
                case GOOGLE_INBOX:
                    String subject = (String) dataMap.get(EmailConstants.EMAIL_SUBJECT.name());
                    if (subject != null) {
                        builder.append(subject);
                    }
                    body = (String) dataMap.get(EmailConstants.EMAIL_BODY.name());
                    if (body != null) {
                        if (builder.length() > 0) {
                            builder.append(GlanceConstants.PERIOD);
                            builder.append(" ");
                        }
                        builder.append(body);
                        break;
                    }
                    break;
                case FUEL:
                    if (dataMap.containsKey(FuelConstants.FUEL_LEVEL.name())) {
                        builder.append(resources.getString(R.string.your_fuel_level_is_low_tts));
                    }
                    builder.append(resources.getString(R.string.i_can_add_trip_gas_station, dataMap.get(FuelConstants.GAS_STATION_NAME.name())));
                    builder.append(GlanceConstants.PERIOD);
                    break;
                case GENERIC:
                    body = (String) dataMap.get(GenericConstants.GENERIC_MESSAGE.name());
                    if (body != null) {
                        builder.append(body);
                        break;
                    }
                    break;
            }
            tts = builder.toString();
            builder.setLength(0);
        }
        return tts;
    }
    
    public static void initMessageAttributes() {
        final Context appContext = HudApplication.getAppContext();
        final TextView textView = new TextView(appContext);
        textView.setTextAppearance(appContext, R.style.glance_message_title);
        final StaticLayout staticLayout = new StaticLayout((CharSequence)"RockgOn", textView.getPaint(), GlanceConstants.messageWidthBound, Layout.Alignment.ALIGN_NORMAL, 1.0f, 0.0f, false);
        final int messageTitleHeight = staticLayout.getHeight() + (GlanceConstants.messageTitleMarginTop + GlanceConstants.messageTitleMarginBottom);
        GlanceHelper.sLogger.v("message-title-ht = " + messageTitleHeight + " layout=" + staticLayout.getHeight());
        GlanceConstants.setMessageTitleHeight(messageTitleHeight);
    }
    
    public static boolean isCalendarApp(final GlanceApp glanceApp) {
        boolean b = false;
        switch (glanceApp) {
            default:
                b = false;
                break;
            case GOOGLE_CALENDAR:
            case APPLE_CALENDAR:
            case GENERIC_CALENDAR:
                b = true;
                break;
        }
        return b;
    }
    
    public static boolean isDeleteAllView(final View view) {
        return view != null && view.getTag() == GlanceConstants.DELETE_ALL_VIEW_TAG;
    }
    
    public static boolean isFuelNotificationEnabled() {
        return isPackageEnabled("com.navdy.fuel");
    }
    
    public static boolean isMusicNotificationEnabled() {
        return isPackageEnabled("com.navdy.music");
    }
    
    public static boolean isPackageEnabled(final String s) {
        return DriverProfileHelper.getInstance().getCurrentProfile().getNotificationPreferences().enabled && Boolean.TRUE.equals(DriverProfileHelper.getInstance().getCurrentProfile().getNotificationSettings().enabled(s));
    }
    
    public static boolean isPhoneNotificationEnabled() {
        return isPackageEnabled("com.navdy.phone") || isPackageEnabled("com.apple.mobilephone");
    }
    
    public static boolean isPhotoCheckRequired(final GlanceApp glanceApp) {
        boolean b = false;
        switch (glanceApp) {
            default:
                b = false;
                break;
            case GOOGLE_HANGOUT:
            case IMESSAGE:
            case SMS:
                b = true;
                break;
        }
        return b;
    }
    
    public static boolean isTrafficNotificationEnabled() {
        return isPackageEnabled("com.navdy.traffic");
    }
    
    public static boolean needsScrollLayout(final String s) {
        boolean b = false;
        if (!TextUtils.isEmpty((CharSequence)s)) {
            final Context appContext = HudApplication.getAppContext();
            final TextView textView = new TextView(appContext);
            textView.setTextAppearance(appContext, R.style.glance_message_body);
            final float n = GlanceConstants.messageHeightBound / 2 - new StaticLayout((CharSequence)s, textView.getPaint(), GlanceConstants.messageWidthBound, Layout.Alignment.ALIGN_NORMAL, 1.0f, 0.0f, false).getHeight() / 2;
            final int messageTitleHeight = GlanceConstants.getMessageTitleHeight();
            b = (n <= messageTitleHeight);
            GlanceHelper.sLogger.v("message-tittle-calc left[" + n + "] titleHt [" + messageTitleHeight + "]");
        }
        return b;
    }
    
    public static void showGlancesDeletedToast() {
        final Bundle bundle = new Bundle();
        bundle.putInt("13", 1000);
        bundle.putInt("8", R.drawable.icon_qm_glances_grey);
        bundle.putString("4", GlanceHelper.resources.getString(R.string.glances_deleted));
        bundle.putInt("5", R.style.Glances_1);
        final ToastManager instance = ToastManager.getInstance();
        instance.dismissCurrentToast();
        instance.clearAllPendingToast();
        instance.addToast(new ToastManager$ToastParams("glance-deleted", bundle, null, true, true));
    }
    
    public static boolean usesMessageLayout(final GlanceApp glanceApp) {
        boolean b = false;
        switch (glanceApp) {
            default:
                b = false;
                break;
            case GOOGLE_HANGOUT:
            case SLACK:
            case WHATS_APP:
            case FACEBOOK_MESSENGER:
            case GENERIC_MESSAGE:
            case FACEBOOK:
            case TWITTER:
            case GENERIC_SOCIAL:
            case IMESSAGE:
            case GOOGLE_MAIL:
            case APPLE_MAIL:
            case GENERIC_MAIL:
            case GOOGLE_INBOX:
            case SMS:
            case GENERIC:
                b = true;
                break;
        }
        return b;
    }
}
