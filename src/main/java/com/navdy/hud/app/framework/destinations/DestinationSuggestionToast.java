package com.navdy.hud.app.framework.destinations;
import com.navdy.hud.app.R;

public class DestinationSuggestionToast {
    final public static String DESTINATION_SUGGESTION_TOAST_ID = "connection#toast";
    final private static int TAG_ACCEPT = 1;
    final private static int TAG_IGNORE = 2;
    final private static int TOAST_TIMEOUT = 15000;
    final private static String accept;
    final private static String ignore;
    final public static com.navdy.service.library.log.Logger sLogger;
    final private static java.util.ArrayList suggestionChoices;
    final private static String title;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.framework.destinations.DestinationSuggestionToast.class);
        suggestionChoices = new java.util.ArrayList(2);
        android.content.res.Resources a = com.navdy.hud.app.HudApplication.getAppContext().getResources();
        title = a.getString(R.string.suggested_trip);
        accept = a.getString(R.string.accept);
        ignore = a.getString(R.string.ignore);
        suggestionChoices.add(new com.navdy.hud.app.ui.component.ChoiceLayout.Choice(accept, 1));
        suggestionChoices.add(new com.navdy.hud.app.ui.component.ChoiceLayout.Choice(ignore, 2));
    }
    
    public DestinationSuggestionToast() {
    }
    
    public static void dismissSuggestionToast() {
        com.navdy.hud.app.framework.toast.ToastManager a = com.navdy.hud.app.framework.toast.ToastManager.getInstance();
        a.clearPendingToast("connection#toast");
        a.dismissCurrentToast("connection#toast");
    }
    
    public static void showSuggestion(com.navdy.service.library.events.places.SuggestedDestination a) {
        com.navdy.service.library.events.destination.Destination a0 = a.destination;
        android.os.Bundle a1 = new android.os.Bundle();
        a1.putInt("13", 15000);
        int i = (a.type == null) ? R.drawable.icon_places_favorite : (com.navdy.hud.app.framework.destinations.DestinationSuggestionToast$2.$SwitchMap$com$navdy$service$library$events$places$SuggestedDestination$SuggestionType[a.type.ordinal()] != 0) ? R.drawable.icon_place_calendar : R.drawable.icon_places_favorite;
        if (i == R.drawable.icon_places_favorite) {
            switch(com.navdy.hud.app.framework.destinations.DestinationSuggestionToast$2.$SwitchMap$com$navdy$service$library$events$destination$Destination$FavoriteType[a0.favorite_type.ordinal()]) {
                case 4: {
                    i = R.drawable.icon_mm_suggested_places;
                    break;
                }
                case 3: {
                    i = R.drawable.icon_places_favorite;
                    break;
                }
                case 2: {
                    i = R.drawable.icon_places_work;
                    break;
                }
                case 1: {
                    i = R.drawable.icon_places_home;
                    break;
                }
            }
        }
        a1.putInt("8", i);
        a1.putInt("1_1", R.style.ToastMainTitle);
        a1.putBoolean("19", true);
        a1.putString("1", title);
        a1.putParcelableArrayList("20", suggestionChoices);
        String s = (android.text.TextUtils.isEmpty((CharSequence)a0.destination_title)) ? (android.text.TextUtils.isEmpty((CharSequence)a0.destination_subtitle)) ? a0.full_address : a0.destination_subtitle : a0.destination_title;
        int i0 = ((Integer)com.squareup.wire.Wire.get(a.duration_traffic, Integer.valueOf(-1))).intValue();
        android.content.res.Resources a2 = com.navdy.hud.app.HudApplication.getAppContext().getResources();
        if (a2 != null) {
            String s0 = null;
            boolean b = false;
            if (i0 <= 0) {
                s0 = (android.text.TextUtils.isEmpty((CharSequence)a0.destination_subtitle)) ? a0.full_address : a0.destination_subtitle;
            } else {
                Object[] a3 = new Object[1];
                a3[0] = com.navdy.hud.app.maps.util.RouteUtils.formatEtaMinutes(a2, (int)java.util.concurrent.TimeUnit.SECONDS.toMinutes((long)i0));
                s0 = a2.getString(R.string.suggested_eta, a3);
            }
            a1.putString("4", s);
            a1.putInt("5", R.style.Toast_2);
            a1.putString("6", s0);
            a1.putInt("7", R.style.Toast_1);
            a1.putInt("16", a2.getDimensionPixelSize(R.dimen.expand_notif_width));
            com.navdy.hud.app.framework.destinations.DestinationSuggestionToast$1 a4 = new com.navdy.hud.app.framework.destinations.DestinationSuggestionToast$1(a0);
            com.navdy.hud.app.framework.toast.ToastManager a5 = com.navdy.hud.app.framework.toast.ToastManager.getInstance();
            if (android.text.TextUtils.equals((CharSequence)a5.getCurrentToastId(), (CharSequence)"incomingcall#toast")) {
                sLogger.v("phone toast active");
                b = false;
            } else {
                a5.dismissCurrentToast(a5.getCurrentToastId());
                a5.clearAllPendingToast();
                b = true;
            }
            a5.addToast(new com.navdy.hud.app.framework.toast.ToastManager$ToastParams("connection#toast", a1, (com.navdy.hud.app.framework.toast.IToastCallback)a4, true, b));
        }
    }
}
