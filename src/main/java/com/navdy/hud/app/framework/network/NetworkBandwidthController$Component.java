package com.navdy.hud.app.framework.network;


public enum NetworkBandwidthController$Component {
    LOCALYTICS(0),
    HERE_ROUTE(1),
    HERE_TRAFFIC(2),
    HERE_MAP_DOWNLOAD(3),
    HERE_REVERSE_GEO(4),
    HOCKEY(5),
    JIRA(6);

    private int value;
    NetworkBandwidthController$Component(int value) {
        this.value = value;
    }
    public int getValue() {
        return value;
    }
}

//final public class NetworkBandwidthController$Component extends Enum {
//    final private static com.navdy.hud.app.framework.network.NetworkBandwidthController$Component[] $VALUES;
//    final public static com.navdy.hud.app.framework.network.NetworkBandwidthController$Component HERE_MAP_DOWNLOAD;
//    final public static com.navdy.hud.app.framework.network.NetworkBandwidthController$Component HERE_REVERSE_GEO;
//    final public static com.navdy.hud.app.framework.network.NetworkBandwidthController$Component HERE_ROUTE;
//    final public static com.navdy.hud.app.framework.network.NetworkBandwidthController$Component HERE_TRAFFIC;
//    final public static com.navdy.hud.app.framework.network.NetworkBandwidthController$Component HOCKEY;
//    final public static com.navdy.hud.app.framework.network.NetworkBandwidthController$Component JIRA;
//    final public static com.navdy.hud.app.framework.network.NetworkBandwidthController$Component LOCALYTICS;
//    
//    static {
//        LOCALYTICS = new com.navdy.hud.app.framework.network.NetworkBandwidthController$Component("LOCALYTICS", 0);
//        HERE_ROUTE = new com.navdy.hud.app.framework.network.NetworkBandwidthController$Component("HERE_ROUTE", 1);
//        HERE_TRAFFIC = new com.navdy.hud.app.framework.network.NetworkBandwidthController$Component("HERE_TRAFFIC", 2);
//        HERE_MAP_DOWNLOAD = new com.navdy.hud.app.framework.network.NetworkBandwidthController$Component("HERE_MAP_DOWNLOAD", 3);
//        HERE_REVERSE_GEO = new com.navdy.hud.app.framework.network.NetworkBandwidthController$Component("HERE_REVERSE_GEO", 4);
//        HOCKEY = new com.navdy.hud.app.framework.network.NetworkBandwidthController$Component("HOCKEY", 5);
//        JIRA = new com.navdy.hud.app.framework.network.NetworkBandwidthController$Component("JIRA", 6);
//        com.navdy.hud.app.framework.network.NetworkBandwidthController$Component[] a = new com.navdy.hud.app.framework.network.NetworkBandwidthController$Component[7];
//        a[0] = LOCALYTICS;
//        a[1] = HERE_ROUTE;
//        a[2] = HERE_TRAFFIC;
//        a[3] = HERE_MAP_DOWNLOAD;
//        a[4] = HERE_REVERSE_GEO;
//        a[5] = HOCKEY;
//        a[6] = JIRA;
//        $VALUES = a;
//    }
//    
//    private NetworkBandwidthController$Component(String s, int i) {
//        super(s, i);
//    }
//    
//    public static com.navdy.hud.app.framework.network.NetworkBandwidthController$Component valueOf(String s) {
//        return (com.navdy.hud.app.framework.network.NetworkBandwidthController$Component)Enum.valueOf(com.navdy.hud.app.framework.network.NetworkBandwidthController$Component.class, s);
//    }
//    
//    public static com.navdy.hud.app.framework.network.NetworkBandwidthController$Component[] values() {
//        return $VALUES.clone();
//    }
//}
//