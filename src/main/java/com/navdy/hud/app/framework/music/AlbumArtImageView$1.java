package com.navdy.hud.app.framework.music;

class AlbumArtImageView$1 implements Runnable {
    private final com.navdy.hud.app.framework.music.AlbumArtImageView this$0;
    private final String val$artworkHash;
    private final android.graphics.drawable.Drawable[] val$drawables;

    AlbumArtImageView$1(com.navdy.hud.app.framework.music.AlbumArtImageView a, android.graphics.drawable.Drawable[] a0, String s) {
        super();
        this.this$0 = a;
        this.val$drawables = a0;
        this.val$artworkHash = s;
    }

    public void run() {
        boolean b;
        android.graphics.drawable.Drawable a = this.val$drawables[1];
        android.graphics.drawable.Drawable[] a1 = new android.graphics.drawable.Drawable[1];
        a1[0] = a;
        this$0.setImageDrawable(new android.graphics.drawable.LayerDrawable(a1));        synchronized(com.navdy.hud.app.framework.music.AlbumArtImageView.access$000(this.this$0)) {
            b = android.text.TextUtils.equals(this.val$artworkHash, AlbumArtImageView.access$100(this.this$0));
            /*monexit(a2)*/
        }
        if (b) {
            com.navdy.hud.app.framework.music.AlbumArtImageView.access$200(this.this$0).set(false);
        } else {
            com.navdy.hud.app.framework.music.AlbumArtImageView.access$300(this.this$0);
        }
    }
}
