package com.navdy.hud.app.framework;

import com.navdy.hud.app.R;
import com.navdy.hud.app.settings.HUDSettings;
import com.navdy.service.library.events.input.GestureEvent;
import com.navdy.hud.app.ui.framework.UIStateManager;
import android.net.Uri;
import com.navdy.hud.app.manager.InputManager;
import android.animation.AnimatorSet;
import android.view.LayoutInflater;
import com.navdy.hud.app.framework.notifications.NotificationType;
import android.view.View;
import android.content.Context;
import android.content.res.Resources;
import java.util.List;
import java.util.ArrayList;
import android.provider.Settings;
import android.provider.Settings;
import com.navdy.hud.app.HudApplication;
import android.os.SystemClock;
import com.navdy.hud.app.analytics.AnalyticsSupport;
import com.navdy.hud.app.framework.notifications.NotificationManager;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import android.os.Handler;
import android.widget.TextView;
import android.content.SharedPreferences;
import android.content.SharedPreferences;
import com.navdy.hud.app.view.Gauge;
import com.navdy.hud.app.framework.notifications.INotificationController;
import android.view.ViewGroup;
import com.navdy.hud.app.ui.component.ChoiceLayout2;
import com.navdy.service.library.log.Logger;
import android.content.SharedPreferences;
import com.navdy.hud.app.framework.notifications.INotification;
import android.database.ContentObserver;

public class AdaptiveBrightnessNotification extends ContentObserver implements INotification, SharedPreferences.OnSharedPreferenceChangeListener
{
    private static final int DEFAULT_BRIGHTNESS_INT;
    private static final long DETENT_TIMEOUT = 250L;
    private static final int INITIAL_CHANGING_STEP = 1;
    private static final int MAX_BRIGHTNESS = 255;
    private static final int MAX_STEP = 16;
    private static final int NOTIFICATION_TIMEOUT = 10000;
    private static final Logger sLogger;
    private final boolean AUTO_BRIGHTNESS_ADJ_ENABLED;
    private int changingStep;
    private ChoiceLayout2 choiceLayout;
    private ViewGroup container;
    private INotificationController controller;
    private int currentValue;
    private boolean isAutoBrightnessOn;
    private boolean isLastChangeIncreasing;
    private long lastChangeActionTime;
    private Gauge progressBar;
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor sharedPreferencesEditor;
    private TextView textIndicator;
    
    static {
        sLogger = new Logger(AdaptiveBrightnessNotification.class);
        DEFAULT_BRIGHTNESS_INT = Integer.parseInt("128");
    }
    
    public AdaptiveBrightnessNotification() {
        super(new Handler());
        this.changingStep = 1;
        this.lastChangeActionTime = -251L;
        this.isLastChangeIncreasing = false;
        this.AUTO_BRIGHTNESS_ADJ_ENABLED = true;
        this.sharedPreferences = RemoteDeviceManager.getInstance().getSharedPreferences();
        this.sharedPreferencesEditor = this.sharedPreferences.edit();
        this.isAutoBrightnessOn = this.getCurrentIsAutoBrightnessOn();
    }
    
    private void close() {
        NotificationManager.getInstance().removeNotification(this.getId());
        AnalyticsSupport.recordAdaptiveAutobrightnessAdjustmentChanged(this.getCurrentBrightness());
    }
    
    private void detentChangingStep(final boolean isLastChangeIncreasing) {
        final long elapsedRealtime = SystemClock.elapsedRealtime();
        if (isLastChangeIncreasing == this.isLastChangeIncreasing && this.lastChangeActionTime + 250L >= elapsedRealtime) {
            this.changingStep <<= 1;
            if (this.changingStep > 16) {
                this.changingStep = 16;
            }
        }
        else {
            this.changingStep = 1;
        }
        this.isLastChangeIncreasing = isLastChangeIncreasing;
        this.lastChangeActionTime = elapsedRealtime;
    }

    private int getCurrentBrightness() {
        if (this.isAutoBrightnessOn) {
            try {
                return Settings.System.getInt(HudApplication.getAppContext().getContentResolver(), "screen_brightness");
            } catch (Settings.SettingNotFoundException e) {
                sLogger.e("Settings not found exception ", e);
                return DEFAULT_BRIGHTNESS_INT;
            }
        }
        try {
            return Integer.parseInt(this.sharedPreferences.getString(HUDSettings.BRIGHTNESS, "128"));
        } catch (NumberFormatException e2) {
            sLogger.e("Cannot parse brightness from the shared preferences", e2);
            return DEFAULT_BRIGHTNESS_INT;
        }
    }
    
    private boolean getCurrentIsAutoBrightnessOn() {
        return "true".equals(this.sharedPreferences.getString("screen.auto_brightness", ""));
    }
    
    private void showFixedChoices(final ViewGroup viewGroup) {
        this.choiceLayout = (ChoiceLayout2)viewGroup.findViewById(R.id.choiceLayout);
        final ArrayList<ChoiceLayout2.Choice> list = new ArrayList<ChoiceLayout2.Choice>(3);
        final Resources resources = viewGroup.getResources();
        final String string = resources.getString(R.string.glances_ok);
        final int color = resources.getColor(R.color.glance_ok_blue);
        list.add(new ChoiceLayout2.Choice(2, R.drawable.icon_glances_ok, color, R.drawable.icon_glances_ok, -16777216, string, color));
        this.choiceLayout.setChoices(list, 1, (ChoiceLayout2.IListener)new ChoiceLayout2.IListener() {
            @Override
            public void executeItem(final ChoiceLayout2.Selection selection) {
                AdaptiveBrightnessNotification.this.sharedPreferencesEditor.putString("screen.auto_brightness_adj", String.valueOf(AdaptiveBrightnessNotification.this.currentValue));
                AdaptiveBrightnessNotification.this.sharedPreferencesEditor.apply();
                AdaptiveBrightnessNotification.this.close();
            }
            
            @Override
            public void itemSelected(final ChoiceLayout2.Selection selection) {
            }
        });
        this.choiceLayout.setVisibility(View.VISIBLE);
    }
    
    public static void showNotification() {
        NotificationManager.getInstance().addNotification(new AdaptiveBrightnessNotification());
    }
    
    private void updateBrightness(int n) {
        if (n != 0) {
            if ((n += this.currentValue) < 0) {
                n = 0;
            }
            int n2;
            if ((n2 = n) > 255) {
                n2 = 255;
            }
            this.sharedPreferencesEditor.putString("screen.brightness", String.valueOf(n2));
            this.sharedPreferencesEditor.apply();
        }
    }
    
    public boolean canAddToStackIfCurrentExists() {
        return false;
    }
    
    public boolean expandNotification() {
        return false;
    }
    
    public int getColor() {
        return 0;
    }
    
    public View getExpandedView(final Context context, final Object o) {
        return null;
    }
    
    public int getExpandedViewIndicatorColor() {
        return 0;
    }
    
    public String getId() {
        return "navdy#brightness#notif";
    }
    
    public int getTimeout() {
        return 10000;
    }
    
    public NotificationType getType() {
        return NotificationType.BRIGHTNESS;
    }
    
    public View getView(final Context context) {
        if (this.container == null) {
            this.container = (ViewGroup)LayoutInflater.from(context).inflate(R.layout.notification_adaptive_auto_brightness, (ViewGroup)null);
            this.textIndicator = (TextView)this.container.findViewById(R.id.subTitle);
            (this.progressBar = (Gauge)this.container.findViewById(R.id.circle_progress)).setAnimated(false);
            this.showFixedChoices(this.container);
        }
        return (View)this.container;
    }
    
    public AnimatorSet getViewSwitchAnimation(final boolean b) {
        return null;
    }
    
    public boolean isAlive() {
        return false;
    }
    
    public boolean isPurgeable() {
        return false;
    }
    
    public InputManager.IInputHandler nextHandler() {
        return null;
    }
    
    public void onChange(final boolean b, final Uri uri) {
    }
    
    public void onClick() {
        this.close();
    }
    
    public void onExpandedNotificationEvent(final UIStateManager.Mode mode) {
    }
    
    public void onExpandedNotificationSwitched() {
    }
    
    public boolean onGesture(final GestureEvent gestureEvent) {
        return false;
    }
    
    public boolean onKey(final InputManager.CustomKeyEvent customKeyEvent) {
        boolean b = false;
        if (this.controller != null) {
            if (customKeyEvent != InputManager.CustomKeyEvent.SELECT) {
                this.controller.resetTimeout();
            }
            switch (customKeyEvent) {
                case SELECT:
                    if (this.choiceLayout != null) {
                        this.choiceLayout.executeSelectedItem();
                    }
                    else {
                        this.close();
                    }
                    b = true;
                    break;
                case LEFT:
                    AdaptiveBrightnessNotification.sLogger.v("Decreasing brightness");
                    this.detentChangingStep(false);
                    this.updateBrightness(-this.changingStep);
                    b = true;
                    break;
                case RIGHT:
                    AdaptiveBrightnessNotification.sLogger.v("Increasing brightness");
                    this.detentChangingStep(true);
                    this.updateBrightness(this.changingStep);
                    b = true;
                    break;
            }
        }
        return b;
    }
    
    public void onNotificationEvent(final UIStateManager.Mode mode) {
    }
    
    public void onSharedPreferenceChanged(final SharedPreferences sharedPreferences, final String s) {
        if (s.equals("screen.brightness")) {
            this.updateState();
        }
    }
    
    public void onStart(final INotificationController controller) {
        this.currentValue = this.getCurrentBrightness();
        this.sharedPreferencesEditor.putString("screen.auto_brightness", "false");
        this.sharedPreferencesEditor.apply();
        this.sharedPreferencesEditor.putString("screen.brightness", String.valueOf(this.currentValue));
        this.sharedPreferencesEditor.apply();
        this.isAutoBrightnessOn = false;
        this.controller = controller;
        this.sharedPreferences.registerOnSharedPreferenceChangeListener((SharedPreferences.OnSharedPreferenceChangeListener)this);
        this.updateState();
    }
    
    public void onStop() {
        this.sharedPreferencesEditor.putString("screen.auto_brightness", "true");
        this.sharedPreferencesEditor.apply();
        this.isAutoBrightnessOn = false;
        this.sharedPreferences.unregisterOnSharedPreferenceChangeListener((SharedPreferences.OnSharedPreferenceChangeListener)this);
        this.controller = null;
    }
    
    public void onTrackHand(final float n) {
    }
    
    public void onUpdate() {
        if (this.controller != null) {
            this.controller.resetTimeout();
        }
        this.updateState();
    }
    
    public boolean supportScroll() {
        return false;
    }
    
    public void updateState() {
        if (this.controller == null) {
            AdaptiveBrightnessNotification.sLogger.v("brightness notif offscreen");
        }
        else {
            this.currentValue = this.getCurrentBrightness();
            this.progressBar.setValue(this.currentValue);
        }
    }
}
