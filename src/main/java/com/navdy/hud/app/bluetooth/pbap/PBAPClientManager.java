package com.navdy.hud.app.bluetooth.pbap;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.HandlerThread;
import android.os.Message;
import android.os.SystemClock;
import android.text.TextUtils;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.bluetooth.vcard.VCardEntry;
import com.navdy.hud.app.bluetooth.vcard.VCardEntry.PhoneData;
import com.navdy.hud.app.event.DriverProfileChanged;
import com.navdy.hud.app.framework.DriverProfileHelper;
import com.navdy.hud.app.framework.contacts.ContactUtil;
import com.navdy.hud.app.framework.contacts.FavoriteContactsManager;
import com.navdy.hud.app.framework.contacts.NumberType;
import com.navdy.hud.app.framework.recentcall.RecentCall;
import com.navdy.hud.app.framework.recentcall.RecentCall.CallType;
import com.navdy.hud.app.framework.recentcall.RecentCall.Category;
import com.navdy.hud.app.framework.recentcall.RecentCallManager;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.profile.DriverProfile;
import com.navdy.hud.app.service.ConnectionHandler;
import com.navdy.hud.app.storage.db.helper.RecentCallPersistenceHelper;
import com.navdy.hud.app.util.DeviceUtil;
import com.navdy.service.library.device.NavdyDeviceId;
import com.navdy.service.library.events.DeviceInfo;
import com.navdy.service.library.events.connection.ConnectionStateChange;
import com.navdy.service.library.log.Logger;
import com.squareup.otto.Subscribe;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

public class PBAPClientManager {
    private static final int CHECK_PBAP_CONNECTION_HANG_INTERVAL = 60000;
    private static final int CHECK_PBAP_PULL_PHONEBOOK_HANG_INTERVAL = 45000;
    private static final int INITIAL_CONNECTION_DELAY = 30000;
    public static final int MAX_RECENT_CALLS = 30;
    private static final int TYPE_HOME = 1;
    private static final int TYPE_MOBILE = 2;
    private static final int TYPE_WORK = 3;
    private static final int TYPE_WORK_MOBILE = 17;
    private static final Logger sLogger = new Logger(PBAPClientManager.class);
    private static final PBAPClientManager singleton = new PBAPClientManager();
    private BluetoothAdapter bluetoothAdapter;
    private Runnable checkPbapConnectionHang = new Runnable() {
        public void run() {
            try {
                PBAPClientManager.sLogger.v("pbap hang detected, reattempting...");
                PBAPClientManager.this.stopPbapClient();
                PBAPClientManager.this.handler.removeCallbacks(PBAPClientManager.this.syncRunnable);
                PBAPClientManager.this.handler.post(PBAPClientManager.this.syncRunnable);
            } catch (Throwable t) {
                PBAPClientManager.sLogger.e(t);
            }
        }
    };
    private Handler handler;
    private Callback handlerCallback = new Callback() {
        /* JADX WARNING: inconsistent code. */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public boolean handleMessage(Message msg) {
            try {
                switch (msg.what) {
                    case 1:
                        PBAPClientManager.sLogger.v("pbap client set phone book done");
                        break;
                    case 2:
                        PBAPClientManager.sLogger.v("pbap client pull phone book done");
                        PBAPClientManager.this.handler.removeCallbacks(PBAPClientManager.this.checkPbapConnectionHang);
                        PBAPClientManager.this.buildRecentCallList((ArrayList) msg.obj);
                        break;
                    case 3:
                        PBAPClientManager.sLogger.v("pbap client vcard listing done");
                        break;
                    case 4:
                        PBAPClientManager.sLogger.v("pbap client pull vcard entry done");
                        break;
                    case 5:
                        PBAPClientManager.sLogger.v("pbap client phone book size done:" + msg.arg1);
                        break;
                    case 6:
                        PBAPClientManager.sLogger.v("pbap client vcard listing size done:" + msg.arg1);
                        break;
                    case BluetoothPbapClient.EVENT_SET_PHONE_BOOK_ERROR /*101*/:
                        PBAPClientManager.sLogger.e("pbap client set phone book error");
                        break;
                    case 102:
                        PBAPClientManager.sLogger.e("pbap client pull phone book error");
                        break;
                    case BluetoothPbapClient.EVENT_PULL_VCARD_LISTING_ERROR /*103*/:
                        PBAPClientManager.sLogger.e("pbap client vcard listing error");
                        break;
                    case BluetoothPbapClient.EVENT_PULL_VCARD_ENTRY_ERROR /*104*/:
                        PBAPClientManager.sLogger.e("pbap client vcard entry error");
                        break;
                    case 105:
                        PBAPClientManager.sLogger.e("pbap client phone book size error");
                        break;
                    case BluetoothPbapClient.EVENT_PULL_VCARD_LISTING_SIZE_ERROR /*106*/:
                        PBAPClientManager.sLogger.e("pbap client vcard listing size error");
                        break;
                    case 201:
                        PBAPClientManager.sLogger.i("pbap client connected, pulling phonebook [telecom/cch.vcf]");
                        PBAPClientManager.this.handler.removeCallbacks(PBAPClientManager.this.checkPbapConnectionHang);
                        PBAPClientManager.this.pbapClient.pullPhoneBook(BluetoothPbapClient.CCH_PATH);
                        PBAPClientManager.this.handler.postDelayed(PBAPClientManager.this.checkPbapConnectionHang, CHECK_PBAP_PULL_PHONEBOOK_HANG_INTERVAL);
                        break;
                    case 202:
                        PBAPClientManager.sLogger.i("pbap client dis-connected");
                        break;
                    case 203:
                        PBAPClientManager.sLogger.w("pbap client auth-requested");
                        break;
                    case 204:
                        PBAPClientManager.sLogger.w("pbap client auth-timeout");
                        break;
                }
            } catch (Throwable t) {
                PBAPClientManager.sLogger.e(t);
            }
            return false;
        }
    };
    private HandlerThread handlerThread;
    private long lastSyncAttemptTime;
    private BluetoothPbapClient pbapClient;
    private RemoteDeviceManager remoteDeviceManager = RemoteDeviceManager.getInstance();
    private Runnable stopPbapClientRunnable = new Runnable() {
        public void run() {
            PBAPClientManager.this.stopPbapClient();
        }
    };
    private Runnable syncRunnable = new Runnable() {
        public void run() {
            PBAPClientManager.this.syncRecentCalls();
        }
    };

    public static PBAPClientManager getInstance() {
        return singleton;
    }

    public PBAPClientManager() {
        this.remoteDeviceManager.getBus().register(this);
        this.handlerThread = new HandlerThread("HudPBAPClientManager");
        this.handlerThread.start();
        this.handler = new Handler(this.handlerThread.getLooper(), this.handlerCallback);
        BluetoothManager bluetoothManager = (BluetoothManager) HudApplication.getAppContext().getSystemService(Context.BLUETOOTH_SERVICE);
        if (bluetoothManager != null) {
            this.bluetoothAdapter = bluetoothManager.getAdapter();
        }
        this.handler.removeCallbacks(this.syncRunnable);
        this.handler.postDelayed(this.syncRunnable, INITIAL_CONNECTION_DELAY);
    }

    @Subscribe
    public void onConnectionStatusChange(ConnectionStateChange event) {
        switch (event.state) {
            case CONNECTION_LINK_LOST:
                this.handler.post(this.stopPbapClientRunnable);
                RecentCallManager.getInstance().clearRecentCalls();
                FavoriteContactsManager.getInstance().clearFavoriteContacts();
            default:
        }
    }

    @Subscribe
    public void onDeviceSyncRequired(ConnectionHandler.DeviceSyncEvent event) {
        boolean fullSync = true;
        if (event.amount != 1) {
            fullSync = false;
        }
        if (fullSync) {
            this.handler.removeCallbacks(this.syncRunnable);
            sLogger.v("trigger PBAP download");
            this.handler.post(this.syncRunnable);
        }
        FavoriteContactsManager.getInstance().syncFavoriteContacts(fullSync);
    }

    @Subscribe
    public void onDriverProfileChanged(DriverProfileChanged event) {
        RecentCallManager recentCallManager = RecentCallManager.getInstance();
        recentCallManager.clearContactLookupMap();
        recentCallManager.buildRecentCalls();
        FavoriteContactsManager.getInstance().buildFavoriteContacts();
    }

    private void startPbapClient(String btAddress) {
        try {
            if (this.pbapClient == null) {
                sLogger.v("startPbapClient");
                this.pbapClient = new BluetoothPbapClient(this.bluetoothAdapter.getRemoteDevice(btAddress), this.handler);
                this.pbapClient.connect();
                sLogger.v("started pbap client");
            }
        } catch (Throwable t) {
            sLogger.e("could not start pbap client", t);
        }
    }

    private void stopPbapClient() {
        try {
            if (this.pbapClient != null) {
                sLogger.v("stopPbapClient");
                this.pbapClient.disconnect();
                sLogger.v("stopped pbap client");
                this.pbapClient = null;
                this.lastSyncAttemptTime = 0;
                this.handler.removeCallbacks(this.checkPbapConnectionHang);
            }
        } catch (Throwable t) {
            sLogger.e("error stopping pbap client", t);
        } finally {
            this.pbapClient = null;
            this.lastSyncAttemptTime = 0;
            this.handler.removeCallbacks(this.checkPbapConnectionHang);
        }
    }

    public void syncRecentCalls() {
        try {
            sLogger.v("syncRecentCalls");
            if (this.lastSyncAttemptTime > 0) {
                sLogger.v("syncRecentCalls in progress");
                return;
            }
            this.handler.removeCallbacks(this.checkPbapConnectionHang);
            DeviceInfo deviceInfo = this.remoteDeviceManager.getRemoteDeviceInfo();
            if (deviceInfo == null) {
                sLogger.v("no remote device, bailing out");
                return;
            }
            String btAddress = new NavdyDeviceId(deviceInfo.deviceId).getBluetoothAddress();
            if (btAddress == null) {
                sLogger.i("BT Address n/a deviceId:" + deviceInfo.deviceId);
                return;
            }
            stopPbapClient();
            this.lastSyncAttemptTime = SystemClock.elapsedRealtime();
            sLogger.v("syncRecentCalls-attempting to connect");
            startPbapClient(btAddress);
            this.handler.postDelayed(this.checkPbapConnectionHang, CHECK_PBAP_CONNECTION_HANG_INTERVAL);
        } catch (Throwable t) {
            sLogger.e("syncRecentCalls", t);
            stopPbapClient();
        }
    }

    private void buildRecentCallList(ArrayList<VCardEntry> list) {
        try {
            sLogger.v("recent calls vcard-list size[" + list.size() + "]");
            boolean verbose = !DeviceUtil.isUserBuild();
            HashMap<Long, RecentCall> callMap = new LinkedHashMap();
            PhoneNumberUtil phoneNumberUtil = PhoneNumberUtil.getInstance();
            String currentCountry = DriverProfileHelper.getInstance().getCurrentLocale().getCountry();
            for (VCardEntry entry : list) {
                if (verbose) {
                    sLogger.v("vcard entry:" + entry);
                }
                List<PhoneData> phoneData = entry.getPhoneList();
                String phoneNumber = null;
                int phoneNumberType = 0;
                if (phoneData != null && phoneData.size() > 0) {
                    for (PhoneData data : phoneData) {
                        String number = data.getNumber();
                        if (!TextUtils.isEmpty(number)) {
                            phoneNumber = number;
                            phoneNumberType = data.getType();
                            break;
                        }
                    }
                }
                if (phoneNumber == null) {
                    sLogger.v("no phone number in vcard");
                } else {
                    try {
                        long nPhoneNum = phoneNumberUtil.parse(phoneNumber, currentCountry).getNationalNumber();
                        RecentCall call = callMap.get(nPhoneNum);
                        if (call == null || TextUtils.isEmpty(call.name)) {
                            String displayName = entry.getDisplayName();
                            if (!ContactUtil.isDisplayNameValid(displayName, phoneNumber, nPhoneNum)) {
                                displayName = null;
                            }
                            int callType = entry.getCallType();
                            Date callTime = entry.getCallTime();
                            if (callTime == null) {
                                sLogger.v("ignoring spam number:" + phoneNumber);
                            } else {
                                RecentCall recentCall = new RecentCall(displayName, Category.PHONE_CALL, phoneNumber, getNumberType(phoneNumberType), callTime, getCallType(callType), -1, nPhoneNum);
                                if (verbose) {
                                    sLogger.v("adding recent call:" + recentCall);
                                }
                                callMap.put(nPhoneNum, recentCall);
                                if (callMap.size() == MAX_RECENT_CALLS) {
                                    sLogger.v("exceeded max size");
                                    break;
                                }
                            }
                        } else if (verbose) {
                            sLogger.v("Ignoring entry with duplicate phone number: " + nPhoneNum);
                        }
                    } catch (Throwable t) {
                        sLogger.e(t);
                    }
                }
            }
            sLogger.v("recent calls map size[" + callMap.size() + "]");
            DriverProfile driverProfile = DriverProfileHelper.getInstance().getCurrentProfile();
            List<RecentCall> recentCalls = new ArrayList<>(callMap.values());
            RecentCallPersistenceHelper.storeRecentCalls(driverProfile.getProfileName(), recentCalls, true);
        } catch (Throwable t2) {
            sLogger.e(t2);
        } finally {
            sLogger.v("buildRecentCallList: stopping client");
            stopPbapClient();
        }
    }

    private NumberType getNumberType(int type) {
        switch (type) {
            case 1:
                return NumberType.HOME;
            case 2:
                return NumberType.MOBILE;
            case 3:
                return NumberType.WORK;
            case 17:
                return NumberType.WORK_MOBILE;
            default:
                return NumberType.OTHER;
        }
    }

    private CallType getCallType(int type) {
        switch (type) {
            case 10:
                return CallType.INCOMING;
            case 11:
                return CallType.OUTGOING;
            case 12:
                return CallType.MISSED;
            default:
                return CallType.UNNKNOWN;
        }
    }

    private void printCallMap(Collection<RecentCall> calls) {
        for (RecentCall call : calls) {
            sLogger.v("[" + call.category + "] " + "[" + call.number + "]" + "[" + call.numberType + "]" + "[" + call.name + "]" + "[" + call.callTime + "]" + "[" + call.callType + "]");
        }
    }
}
