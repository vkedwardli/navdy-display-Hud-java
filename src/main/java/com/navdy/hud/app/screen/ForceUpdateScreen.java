package com.navdy.hud.app.screen;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.R;
import com.navdy.hud.app.event.Shutdown;
import com.navdy.hud.app.event.Shutdown.Reason;
import com.navdy.hud.app.framework.notifications.NotificationManager;
import com.navdy.hud.app.gesture.GestureServiceConnector;
import com.navdy.hud.app.ui.framework.BasePresenter;
import com.navdy.hud.app.ui.framework.UIStateManager;
import com.navdy.hud.app.util.OTAUpdateService;
import com.navdy.hud.app.view.ForceUpdateView;
import com.navdy.service.library.events.ui.Screen;
import com.navdy.service.library.events.ui.ShowScreen.Builder;
import com.navdy.service.library.log.Logger;
import com.squareup.otto.Bus;
import dagger.Provides;
import flow.Layout;
import javax.inject.Inject;

@Layout(R.layout.force_update_lyt)
public class ForceUpdateScreen extends BaseScreen {
    private static final Logger sLogger = new Logger(ForceUpdateScreen.class);

    @dagger.Module(addsTo = com.navdy.hud.app.ui.activity.Main.Module.class, injects = {ForceUpdateView.class})
    public class Module {
        @Provides
        ForceUpdateScreen provideScreen() {
            return ForceUpdateScreen.this;
        }
    }

    public static class Presenter extends BasePresenter<ForceUpdateView> {
        @Inject
        GestureServiceConnector gestureServiceConnector;
        @Inject
        Bus mBus;
        @Inject
        SharedPreferences mPreferences;
        @Inject
        ForceUpdateScreen mScreen;
        @Inject
        UIStateManager uiStateManager;

        public void onLoad(Bundle savedInstanceState) {
            super.onLoad(savedInstanceState);
            this.uiStateManager.enableSystemTray(false);
            this.uiStateManager.enableNotificationColor(false);
            NotificationManager.getInstance().enableNotifications(false);
        }

        protected void onUnload() {
            this.uiStateManager.enableSystemTray(true);
            this.uiStateManager.enableNotificationColor(true);
            NotificationManager.getInstance().enableNotifications(true);
            super.onUnload();
        }

        public void dismiss() {
            this.mBus.post(new Builder().screen(this.uiStateManager.getDefaultMainActiveScreen()).build());
        }

        public void install() {
            Intent intent = new Intent(HudApplication.getAppContext(), OTAUpdateService.class);
            intent.putExtra("COMMAND", OTAUpdateService.COMMAND_INSTALL_UPDATE);
            HudApplication.getAppContext().startService(intent);
        }

        public void shutDown() {
            this.mBus.post(new Shutdown(Reason.FORCED_UPDATE));
        }

        public boolean isSoftwareUpdatePending() {
            return OTAUpdateService.isUpdateAvailable();
        }

        public Bus getBus() {
            return this.mBus;
        }
    }

    public String getMortarScopeName() {
        return ForceUpdateScreen.class.getName();
    }

    public Object getDaggerModule() {
        return new Module();
    }

    public Screen getScreen() {
        return Screen.SCREEN_FORCE_UPDATE;
    }
}
