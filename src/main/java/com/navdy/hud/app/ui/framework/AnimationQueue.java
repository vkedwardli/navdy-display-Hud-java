package com.navdy.hud.app.ui.framework;

import android.animation.Animator.AnimatorListener;
import android.animation.Animator;
import java.util.concurrent.LinkedBlockingQueue;
import android.animation.AnimatorSet;
import java.util.Queue;

public class AnimationQueue
{
    private boolean animationRunning;
    private Queue<AnimatorSet> animatorQueue;
    private DefaultAnimationListener defaultAnimationListener;
    
    public AnimationQueue() {
        this.animatorQueue = new LinkedBlockingQueue<AnimatorSet>();
        this.defaultAnimationListener = new DefaultAnimationListener() {
            @Override
            public void onAnimationEnd(final Animator animator) {
                AnimationQueue.this.animationRunning = false;
                AnimationQueue.this.runQueuedAnimation();
            }
        };
    }
    
    private void runQueuedAnimation() {
        if (this.animatorQueue.size() != 0) {
            this.startAnimation(this.animatorQueue.remove());
        }
    }
    
    public boolean isAnimationRunning() {
        return this.animationRunning;
    }
    
    public void startAnimation(final AnimatorSet set) {
        if (!this.animationRunning) {
            this.animationRunning = true;
            set.addListener((Animator.AnimatorListener)this.defaultAnimationListener);
            set.start();
        }
        else {
            this.animatorQueue.add(set);
        }
    }
}
