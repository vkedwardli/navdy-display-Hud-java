package com.navdy.hud.app.ui.component.carousel;

import android.widget.TextView;
import android.view.ViewGroup;

import com.navdy.hud.app.R;
import com.navdy.hud.app.ui.component.image.ColorImageView;
import com.navdy.hud.app.ui.component.image.InitialsImageView;
import com.navdy.hud.app.ui.component.image.CrossFadeImageView;
import android.view.View;
import com.navdy.service.library.log.Logger;

import static android.view.View.GONE;
import static android.view.View.INVISIBLE;

public class CarouselAdapter
{
    private static final Logger sLogger;
    CarouselLayout carousel;
    
    static {
        sLogger = new Logger(CarouselAdapter.class);
    }
    
    public CarouselAdapter(final CarouselLayout carousel) {
        this.carousel = carousel;
    }

    private void processImageView(Carousel.ViewType viewType, Carousel.Model item, View imageView, int position, int size) {
        if (imageView instanceof CrossFadeImageView) {
            CrossFadeImageView crossFadeImageView = (CrossFadeImageView) imageView;
            InitialsImageView big = (InitialsImageView) crossFadeImageView.getBig();
            View small = crossFadeImageView.getSmall();
            big.setImage(item.largeImageRes, null, InitialsImageView.Style.DEFAULT);
            if (this.carousel.imageLytResourceId == R.layout.crossfade_image_lyt) {
                ((InitialsImageView) small).setImage(item.smallImageRes, null, InitialsImageView.Style.DEFAULT);
            } else {
                ((ColorImageView) small).setColor(item.smallImageColor);
            }
        } else if (imageView instanceof InitialsImageView) {
            InitialsImageView initialsImageView = (InitialsImageView) imageView;
            if (viewType == Carousel.ViewType.MIDDLE_LEFT) {
                initialsImageView.setImage(item.largeImageRes, null, InitialsImageView.Style.DEFAULT);
            } else {
                initialsImageView.setImage(item.smallImageRes, null, InitialsImageView.Style.DEFAULT);
            }
        }
        if (this.carousel.viewProcessor != null) {
            try {
                if (viewType == Carousel.ViewType.MIDDLE_LEFT) {
                    this.carousel.viewProcessor.processLargeImageView(item, imageView, position, size, size);
                } else {
                    this.carousel.viewProcessor.processSmallImageView(item, imageView, position, size, size);
                }
            } catch (Throwable t) {
                sLogger.e(t);
            }
        }
    }

    private void processInfoView(final Carousel.Model model, final View view, final int n) {
        if (model.infoMap != null) {
            final ViewGroup viewGroup = (ViewGroup)view;
            for (int childCount = viewGroup.getChildCount(), i = 0; i < childCount; ++i) {
                final View child = viewGroup.getChildAt(i);
                if (child instanceof TextView) {
                    final String text = model.infoMap.get(child.getId());
                    if (text != null) {
                        ((TextView)child).setText((CharSequence)text);
                        child.setVisibility(View.VISIBLE);
                    }
                    else {
                        child.setVisibility(GONE);
                    }
                }
            }
        }
        if (this.carousel.viewProcessor == null) {
            return;
        }
        try {
            this.carousel.viewProcessor.processInfoView(model, view, n);
        }
        catch (Throwable t) {
            CarouselAdapter.sLogger.e(t);
        }
    }
    
    private void processView(final Carousel.ViewType viewType, final Carousel.Model model, final View view, final int n, final int n2) {
        if (viewType == Carousel.ViewType.MIDDLE_RIGHT) {
            this.processInfoView(model, view, n);
        }
        else {
            this.processImageView(viewType, model, view, n, n2);
        }
    }
    
    public View getView(final int n, View inflate, final Carousel.ViewType viewType, final int n2, final int n3) {
        if (inflate != null) {
            inflate.setTag(R.id.item_id, null);
        }
        Carousel.Model model2;
        final Carousel.Model model = model2 = null;
        if (n >= 0) {
            model2 = model;
            if (n < this.carousel.model.size()) {
                model2 = this.carousel.model.get(n);
            }
        }
        if (inflate == null) {
            CarouselAdapter.sLogger.v("create " + viewType + " view");
            final Object o = inflate = this.carousel.inflater.inflate(n2, (ViewGroup)null);
            if (o instanceof CrossFadeImageView) {
                if (viewType == Carousel.ViewType.SIDE) {
                    ((CrossFadeImageView)o).inject(CrossFadeImageView.Mode.SMALL);
                    inflate = (View)o;
                }
                else {
                    inflate = (View)o;
                    if (viewType == Carousel.ViewType.MIDDLE_LEFT) {
                        ((CrossFadeImageView)o).inject(CrossFadeImageView.Mode.BIG);
                        inflate = (View)o;
                    }
                }
            }
        }
        else {
            final Object o2 = inflate = inflate;
            if (o2 instanceof CrossFadeImageView) {
                if (viewType == Carousel.ViewType.SIDE) {
                    ((CrossFadeImageView)o2).setMode(CrossFadeImageView.Mode.SMALL);
                    inflate = (View)o2;
                }
                else {
                    inflate = (View)o2;
                    if (viewType == Carousel.ViewType.MIDDLE_LEFT) {
                        ((CrossFadeImageView)o2).setMode(CrossFadeImageView.Mode.BIG);
                        inflate = (View)o2;
                    }
                }
            }
        }
        if (model2 != null) {
            inflate.setTag(R.id.item_id, model2.id);
            inflate.setVisibility(View.VISIBLE);
            this.processView(viewType, model2, inflate, n, n3);
        }
        else {
            inflate.setVisibility(INVISIBLE);
            inflate.setTag(R.id.item_id, null);
        }
        return inflate;
    }
}
