package com.navdy.hud.app.ui.component.tbt;

import com.navdy.hud.app.R;
import com.navdy.hud.app.maps.MapEvents;
import android.view.ViewGroup;
import android.view.ViewGroup;
import kotlin.TypeCastException;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.Animator;
import android.animation.AnimatorSet;

import com.navdy.hud.app.maps.MapEvents$ManeuverDisplay;
import com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder$ManeuverState;
import com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils;
import android.view.View;
import android.text.TextUtils;
import android.util.AttributeSet;
import org.jetbrains.annotations.NotNull;
import android.content.Context;
import kotlin.jvm.internal.Intrinsics;
import com.navdy.hud.app.HudApplication;
//import kotlin.jvm.internal.DefaultConstructorMarker;
import android.widget.ProgressBar;
import android.animation.ValueAnimator;
import android.widget.ImageView;
import com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder;
import android.widget.TextView;
import com.navdy.hud.app.view.MainView;
import java.util.HashMap;
import android.content.res.Resources;
import com.navdy.service.library.log.Logger;
import kotlin.Metadata;
import android.support.constraint.ConstraintLayout;

import net.hockeyapp.android.LoginActivity;

import static android.view.View.GONE;

@Metadata(bv = { 1, 0, 1 }, d1 = { "\u0000r\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\t\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u0000 ,2\u00020\u0001:\u0001,B\u000f\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004B\u0017\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006¢\u0006\u0002\u0010\u0007B\u001f\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\b\u001a\u00020\t¢\u0006\u0002\u0010\nJ\b\u0010\u001a\u001a\u00020\u001bH\u0002J\u0006\u0010\u001c\u001a\u00020\u001bJ\u001a\u0010\u001d\u001a\u00020\u001b2\u0006\u0010\u001e\u001a\u00020\f2\n\u0010\u001f\u001a\u00060 R\u00020!J\b\u0010\"\u001a\u00020\u001bH\u0014J$\u0010#\u001a\u00020\u001b2\b\u0010$\u001a\u0004\u0018\u00010\u00132\b\u0010%\u001a\u0004\u0018\u00010\u00112\u0006\u0010&\u001a\u00020'H\u0002J\u000e\u0010(\u001a\u00020\u001b2\u0006\u0010\u001e\u001a\u00020\fJ\u000e\u0010)\u001a\u00020\u001b2\u0006\u0010*\u001a\u00020+R\u0010\u0010\u000b\u001a\u0004\u0018\u00010\fX\u0082\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\r\u001a\u0004\u0018\u00010\u000eX\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\tX\u0082\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\u0010\u001a\u0004\u0018\u00010\u0011X\u0082\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\u0012\u001a\u0004\u0018\u00010\u0013X\u0082\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\u0014\u001a\u0004\u0018\u00010\u0015X\u0082\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\u0016\u001a\u0004\u0018\u00010\u0017X\u0082\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\u0018\u001a\u0004\u0018\u00010\u0019X\u0082\u000e¢\u0006\u0002\n\u0000¨\u0006-" }, d2 = { "Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;", "Landroid/support/constraint/ConstraintLayout;", "context", "Landroid/content/Context;", "(Landroid/content/Context;)V", "attrs", "Landroid/util/AttributeSet;", "(Landroid/content/Context;Landroid/util/AttributeSet;)V", "defStyleAttr", "", "(Landroid/content/Context;Landroid/util/AttributeSet;I)V", "currentMode", "Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;", "distanceView", "Landroid/widget/TextView;", "initialProgressBarDistance", "lastDistance", "", "lastManeuverState", "Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;", "nowView", "Landroid/widget/ImageView;", "progressAnimator", "Landroid/animation/ValueAnimator;", "progressView", "Landroid/widget/ProgressBar;", "cancelProgressAnimator", "", "clear", "getCustomAnimator", "mode", "mainBuilder", "Landroid/animation/AnimatorSet.Builder;", "Landroid/animation/AnimatorSet;", "onFinishInflate", "setDistance", "maneuverState", "text", "distanceInMeters", "", "setMode", "updateDisplay", "event", "Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;", "Companion", "app_hudRelease" }, k = 1, mv = { 1, 1, 6 })
public final class TbtDistanceView extends ConstraintLayout
{
    public static final Companion Companion;
    private static final int fullWidth;
    private static final Logger logger;
    private static final int mediumWidth;
    private static final int nowHeightFull;
    private static final int nowHeightMedium;
    private static final int nowWidthFull;
    private static final int nowWidthMedium;
    private static final int progressHeightFull;
    private static final int progressHeightMedium;
    private static final Resources resources;
    private static final float size18;
    private static final float size22;
    private HashMap _$_findViewCache;
    private MainView.CustomAnimationMode currentMode;
    private TextView distanceView;
    private int initialProgressBarDistance;
    private String lastDistance;
    private HereManeuverDisplayBuilder$ManeuverState lastManeuverState;
    private ImageView nowView;
    private ValueAnimator progressAnimator;
    private ProgressBar progressView;
    
    static {
        Companion = new Companion();
        logger = new Logger("TbtDistanceView");
        final Resources resources2 = HudApplication.getAppContext().getResources();
        Intrinsics.checkExpressionValueIsNotNull(resources2, "HudApplication.getAppContext().resources");
        resources = resources2;
        fullWidth = TbtDistanceView.Companion.getResources().getDimensionPixelSize(R.dimen.tbt_distance_full_w);
        mediumWidth = TbtDistanceView.Companion.getResources().getDimensionPixelSize(R.dimen.tbt_distance_medium_w);
        size22 = TbtDistanceView.Companion.getResources().getDimension(R.dimen.tbt_instruction_22);
        size18 = TbtDistanceView.Companion.getResources().getDimension(R.dimen.tbt_instruction_18);
        progressHeightFull = TbtDistanceView.Companion.getResources().getDimensionPixelSize(R.dimen.tbt_distance_progress_h_full);
        progressHeightMedium = TbtDistanceView.Companion.getResources().getDimensionPixelSize(R.dimen.tbt_distance_progress_h_medium);
        nowWidthFull = TbtDistanceView.Companion.getResources().getDimensionPixelSize(R.dimen.tbt_distance_now_w_full);
        nowHeightFull = TbtDistanceView.Companion.getResources().getDimensionPixelSize(R.dimen.tbt_distance_now_h_full);
        nowWidthMedium = TbtDistanceView.Companion.getResources().getDimensionPixelSize(R.dimen.tbt_distance_now_w_medium);
        nowHeightMedium = TbtDistanceView.Companion.getResources().getDimensionPixelSize(R.dimen.tbt_distance_now_h_medium);
    }
    
    public TbtDistanceView(@NotNull final Context context) {
        super(context);
        Intrinsics.checkParameterIsNotNull(context, "context");
    }
    
    public TbtDistanceView(@NotNull final Context context, @NotNull final AttributeSet set) {
        super(context, set);
        Intrinsics.checkParameterIsNotNull(context, "context");
        Intrinsics.checkParameterIsNotNull(set, "attrs");
    }
    
    public TbtDistanceView(@NotNull final Context context, @NotNull final AttributeSet set, final int n) {
        super(context, set, n);
        Intrinsics.checkParameterIsNotNull(context, "context");
        Intrinsics.checkParameterIsNotNull(set, "attrs");
    }
    
    public static  /* synthetic */ int access$getFullWidth$cp() {
        return TbtDistanceView.fullWidth;
    }
    
    @NotNull
    public static  /* synthetic */ Logger access$getLogger$cp() {
        return TbtDistanceView.logger;
    }
    
    public static  /* synthetic */ int access$getMediumWidth$cp() {
        return TbtDistanceView.mediumWidth;
    }
    
    public static  /* synthetic */ int access$getNowHeightFull$cp() {
        return TbtDistanceView.nowHeightFull;
    }
    
    public static  /* synthetic */ int access$getNowHeightMedium$cp() {
        return TbtDistanceView.nowHeightMedium;
    }
    
    public static  /* synthetic */ int access$getNowWidthFull$cp() {
        return TbtDistanceView.nowWidthFull;
    }
    
    public static /* synthetic */ int access$getNowWidthMedium$cp() {
        return TbtDistanceView.nowWidthMedium;
    }
    
    public static  /* synthetic */ int access$getProgressHeightFull$cp() {
        return TbtDistanceView.progressHeightFull;
    }
    
    public static /* synthetic */ int access$getProgressHeightMedium$cp() {
        return TbtDistanceView.progressHeightMedium;
    }
    
    @NotNull
    public static  /* synthetic */ Resources access$getResources$cp() {
        return TbtDistanceView.resources;
    }
    
    public static  /* synthetic */ float access$getSize18$cp() {
        return TbtDistanceView.size18;
    }
    
    public static  /* synthetic */ float access$getSize22$cp() {
        return TbtDistanceView.size22;
    }
    
    private void cancelProgressAnimator() {
        final ValueAnimator progressAnimator = this.progressAnimator;
        if (progressAnimator != null && progressAnimator.isRunning()) {
            final ValueAnimator progressAnimator2 = this.progressAnimator;
            if (progressAnimator2 != null) {
                progressAnimator2.removeAllListeners();
            }
            final ValueAnimator progressAnimator3 = this.progressAnimator;
            if (progressAnimator3 != null) {
                progressAnimator3.cancel();
            }
            this.progressAnimator = null;
        }
    }
    
    private void setDistance(final HereManeuverDisplayBuilder$ManeuverState maneuverState, final String lastDistance, final long n) {
        if (!TextUtils.equals(lastDistance, this.lastDistance) || !Intrinsics.areEqual(this.lastManeuverState, maneuverState)) {
            if (maneuverState == null) {
                this.cancelProgressAnimator();
                final ProgressBar progressView = this.progressView;
                if (progressView != null) {
                    progressView.setVisibility(GONE);
                }
                final TextView distanceView = this.distanceView;
                if (distanceView != null) {
                    distanceView.setText("");
                }
                final ImageView nowView = this.nowView;
                if (nowView != null) {
                    nowView.setAlpha(0.0f);
                }
                final ImageView nowView2 = this.nowView;
                if (nowView2 != null) {
                    nowView2.setScaleX(0.9f);
                }
                final ImageView nowView3 = this.nowView;
                if (nowView3 != null) {
                    nowView3.setScaleY(0.9f);
                }
                final ImageView nowView4 = this.nowView;
                if (nowView4 != null) {
                    nowView4.setVisibility(GONE);
                }
                this.setVisibility(GONE);
            }
            else {
                this.lastDistance = lastDistance;
                final TextView distanceView2 = this.distanceView;
                if (distanceView2 != null) {
                    String text;
                    if (lastDistance != null) {
                        text = lastDistance;
                    }
                    else {
                        text = "";
                    }
                    distanceView2.setText(text);
                }
                int n2;
                if ((!Intrinsics.areEqual(this.lastManeuverState, HereManeuverDisplayBuilder$ManeuverState.NOW)) && Intrinsics.areEqual(maneuverState, HereManeuverDisplayBuilder$ManeuverState.NOW)) {
                    n2 = 1;
                }
                else {
                    n2 = 0;
                }
                final boolean b = Intrinsics.areEqual(this.lastManeuverState, HereManeuverDisplayBuilder$ManeuverState.NOW) && (!Intrinsics.areEqual(maneuverState, HereManeuverDisplayBuilder$ManeuverState.NOW));
                if (n2 != 0) {
                    final ImageView nowView5 = this.nowView;
                    if (nowView5 != null) {
                        nowView5.setAlpha(0.0f);
                    }
                    final ImageView nowView6 = this.nowView;
                    if (nowView6 != null) {
                        nowView6.setVisibility(View.VISIBLE);
                    }
                    final AnimatorSet fadeInAndScaleUpAnimator = HomeScreenUtils.getFadeInAndScaleUpAnimator(this.nowView);
                    this.cancelProgressAnimator();
                    final ProgressBar progressView2 = this.progressView;
                    if (progressView2 != null) {
                        progressView2.setVisibility(GONE);
                    }
                    final TextView distanceView3 = this.distanceView;
                    if (distanceView3 != null) {
                        distanceView3.setAlpha(0.0f);
                    }
                    final AnimatorSet set = new AnimatorSet();
                    set.play(fadeInAndScaleUpAnimator);
                    set.setDuration(TbtViewContainer.Companion.getMANEUVER_PROGRESS_ANIMATION_DURATION()).start();
                }
                else if (b) {
                    final ImageView nowView7 = this.nowView;
                    if (nowView7 != null) {
                        nowView7.setVisibility(GONE);
                    }
                    final ImageView nowView8 = this.nowView;
                    if (nowView8 != null) {
                        nowView8.setAlpha(0.0f);
                    }
                    final ObjectAnimator alphaAnimator = HomeScreenUtils.getAlphaAnimator(this.distanceView, 1);
                    final AnimatorSet set2 = new AnimatorSet();
                    set2.play(alphaAnimator);
                    final AnimatorSet setDuration = set2.setDuration(TbtViewContainer.Companion.getMANEUVER_PROGRESS_ANIMATION_DURATION());
                    if (setDuration != null) {
                        setDuration.start();
                    }
                }
                else {
                    final ImageView nowView9 = this.nowView;
                    Integer value;
                    if (nowView9 != null) {
                        value = nowView9.getVisibility();
                    }
                    else {
                        value = null;
                    }
                    if (!Intrinsics.areEqual(value, 0)) {
                        final TextView distanceView4 = this.distanceView;
                        if (distanceView4 != null) {
                            distanceView4.setAlpha(1.0f);
                        }
                    }
                }
                final boolean b2 = (!Intrinsics.areEqual(this.lastManeuverState, HereManeuverDisplayBuilder$ManeuverState.SOON)) && Intrinsics.areEqual(maneuverState, HereManeuverDisplayBuilder$ManeuverState.SOON);
                int n3;
                if (Intrinsics.areEqual(this.lastManeuverState, HereManeuverDisplayBuilder$ManeuverState.SOON) && (!Intrinsics.areEqual(maneuverState, HereManeuverDisplayBuilder$ManeuverState.SOON))) {
                    n3 = 1;
                }
                else {
                    n3 = 0;
                }
                if (n3 != 0) {
                    final ProgressBar progressView3 = this.progressView;
                    if (progressView3 != null) {
                        progressView3.setVisibility(GONE);
                    }
                    this.cancelProgressAnimator();
                    this.initialProgressBarDistance = 0;
                }
                else if (b2) {
                    this.cancelProgressAnimator();
                    this.initialProgressBarDistance = (int)n;
                    final ProgressBar progressView4 = this.progressView;
                    if (progressView4 != null) {
                        progressView4.setProgress(0);
                    }
                    final ProgressBar progressView5 = this.progressView;
                    if (progressView5 != null) {
                        progressView5.setVisibility(View.VISIBLE);
                    }
                    final ProgressBar progressView6 = this.progressView;
                    if (progressView6 != null) {
                        progressView6.requestLayout();
                    }
                }
                else if (Intrinsics.areEqual(maneuverState, HereManeuverDisplayBuilder$ManeuverState.SOON)) {
                    HomeScreenUtils.getProgressBarAnimator(this.progressView, (int)(100 * (1 - n / this.initialProgressBarDistance))).setDuration(TbtViewContainer.Companion.getMANEUVER_PROGRESS_ANIMATION_DURATION()).start();
                }
                this.setVisibility(View.VISIBLE);
            }
        }
    }
    
    public void _$_clearFindViewByIdCache() {
        if (this._$_findViewCache != null) {
            this._$_findViewCache.clear();
        }
    }
    
    public View _$_findCachedViewById(final int n) {
        if (this._$_findViewCache == null) {
            this._$_findViewCache = new HashMap();
        }
        View viewById;
        if ((viewById = (View)this._$_findViewCache.get(n)) == null) {
            viewById = this.findViewById(n);
            this._$_findViewCache.put(n, viewById);
        }
        return viewById;
    }
    
    public final void clear() {
        this.lastDistance = null;
        final TextView distanceView = this.distanceView;
        if (distanceView != null) {
            distanceView.setText("");
        }
        this.cancelProgressAnimator();
        final ProgressBar progressView = this.progressView;
        if (progressView != null) {
            progressView.setVisibility(GONE);
        }
    }
    
    public final void getCustomAnimator(@NotNull final MainView.CustomAnimationMode customAnimationMode, @NotNull final AnimatorSet.Builder animatorSet$Builder) {
        Intrinsics.checkParameterIsNotNull(customAnimationMode, "mode");
        Intrinsics.checkParameterIsNotNull(animatorSet$Builder, "mainBuilder");
    }
    
    protected void onFinishInflate() {
        super.onFinishInflate();
        final View viewById = this.findViewById(R.id.distance);
        if (viewById == null) {
            throw new TypeCastException("null cannot be cast to non-null type android.widget.TextView");
        }
        this.distanceView = (TextView)viewById;
        final View viewById2 = this.findViewById(R.id.progress);
        if (viewById2 == null) {
            throw new TypeCastException("null cannot be cast to non-null type android.widget.ProgressBar");
        }
        this.progressView = (ProgressBar)viewById2;
        final View viewById3 = this.findViewById(R.id.now);
        if (viewById3 == null) {
            throw new TypeCastException("null cannot be cast to non-null type android.widget.ImageView");
        }
        this.nowView = (ImageView)viewById3;
    }

    public final void setMode(@NotNull MainView.CustomAnimationMode mode) {
        Intrinsics.checkParameterIsNotNull(mode, LoginActivity.EXTRA_MODE);
        if (!Intrinsics.areEqual(this.currentMode,  mode)) {
            int lytWidth = 0;
            float fontSize = 0.0f;
            int progressSize = 0;
            int nowW = 0;
            int nowH = 0;
            switch (mode) {
                case EXPAND:
                    lytWidth = Companion.getFullWidth();
                    fontSize = Companion.getSize22();
                    progressSize = Companion.getProgressHeightFull();
                    nowW = Companion.getNowWidthFull();
                    nowH = Companion.getNowHeightFull();
                    break;
                case SHRINK_LEFT:
                    lytWidth = Companion.getMediumWidth();
                    fontSize = Companion.getSize18();
                    progressSize = Companion.getProgressHeightMedium();
                    nowW = Companion.getNowWidthMedium();
                    nowH = Companion.getNowHeightMedium();
                    break;
            }
            MarginLayoutParams lytParams = (MarginLayoutParams)getLayoutParams();
            if (lytParams == null) {
                throw new TypeCastException("null cannot be cast to non-null type android.view.ViewGroup.MarginLayoutParams");
            }
            lytParams.width = lytWidth;
            TextView textView = this.distanceView;
            if (textView != null) {
                textView.setTextSize(fontSize);
            }
            ProgressBar progressBar = this.progressView;
            lytParams = progressBar != null ? (MarginLayoutParams)progressBar.getLayoutParams() : null;
            if (lytParams == null) {
                throw new TypeCastException("null cannot be cast to non-null type android.view.ViewGroup.MarginLayoutParams");
            }
            lytParams.height = progressSize;
            ImageView imageView = this.nowView;
            lytParams = imageView != null ? (MarginLayoutParams)imageView.getLayoutParams() : null;
            if (lytParams == null) {
                throw new TypeCastException("null cannot be cast to non-null type android.view.ViewGroup.MarginLayoutParams");
            }
            lytParams = lytParams;
            lytParams.width = nowW;
            lytParams.height = nowH;
            invalidate();
            requestLayout();
            this.currentMode = mode;
            Companion.getLogger().v("view width = " + lytParams.width);
        }
    }

    public final void updateDisplay(@NotNull final MapEvents$ManeuverDisplay maneuverDisplay) {
        Intrinsics.checkParameterIsNotNull(maneuverDisplay, "event");
        this.setDistance(maneuverDisplay.maneuverState, maneuverDisplay.distanceToPendingRoadText, maneuverDisplay.distanceInMeters);
        this.lastManeuverState = maneuverDisplay.maneuverState;
    }
    
    @Metadata(bv = { 1, 0, 1 }, d1 = { "\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0011\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0007\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0014\u0010\u0003\u001a\u00020\u0004X\u0082\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006R\u0014\u0010\u0007\u001a\u00020\bX\u0082\u0004¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0014\u0010\u000b\u001a\u00020\u0004X\u0082\u0004¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\u0006R\u0014\u0010\r\u001a\u00020\u0004X\u0082\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u0006R\u0014\u0010\u000f\u001a\u00020\u0004X\u0082\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u0006R\u0014\u0010\u0011\u001a\u00020\u0004X\u0082\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u0006R\u0014\u0010\u0013\u001a\u00020\u0004X\u0082\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010\u0006R\u0014\u0010\u0015\u001a\u00020\u0004X\u0082\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0016\u0010\u0006R\u0014\u0010\u0017\u001a\u00020\u0004X\u0082\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0018\u0010\u0006R\u0014\u0010\u0019\u001a\u00020\u001aX\u0082\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u001b\u0010\u001cR\u0014\u0010\u001d\u001a\u00020\u001eX\u0082\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u001f\u0010 R\u0014\u0010!\u001a\u00020\u001eX\u0082\u0004¢\u0006\b\n\u0000\u001a\u0004\b\"\u0010 ¨\u0006#" }, d2 = { "Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView$Companion;", "", "()V", "fullWidth", "", "getFullWidth", "()I", "logger", "Lcom/navdy/service/library/log/Logger;", "getLogger", "()Lcom/navdy/service/library/log/Logger;", "mediumWidth", "getMediumWidth", "nowHeightFull", "getNowHeightFull", "nowHeightMedium", "getNowHeightMedium", "nowWidthFull", "getNowWidthFull", "nowWidthMedium", "getNowWidthMedium", "progressHeightFull", "getProgressHeightFull", "progressHeightMedium", "getProgressHeightMedium", "resources", "Landroid/content/res/Resources;", "getResources", "()Landroid/content/res/Resources;", "size18", "", "getSize18", "()F", "size22", "getSize22", "app_hudRelease" }, k = 1, mv = { 1, 1, 6 })
    public static final class Companion
    {
        private  int getFullWidth() {
            return TbtDistanceView.access$getFullWidth$cp();
        }
        
        private  Logger getLogger() {
            return TbtDistanceView.access$getLogger$cp();
        }
        
        private int getMediumWidth() {
            return TbtDistanceView.access$getMediumWidth$cp();
        }
        
        private  int getNowHeightFull() {
            return TbtDistanceView.access$getNowHeightFull$cp();
        }
        
        private  int getNowHeightMedium() {
            return TbtDistanceView.access$getNowHeightMedium$cp();
        }
        
        private  int getNowWidthFull() {
            return TbtDistanceView.access$getNowWidthFull$cp();
        }
        
        private  int getNowWidthMedium() {
            return TbtDistanceView.access$getNowWidthMedium$cp();
        }
        
        private  int getProgressHeightFull() {
            return TbtDistanceView.access$getProgressHeightFull$cp();
        }
        
        private  int getProgressHeightMedium() {
            return TbtDistanceView.access$getProgressHeightMedium$cp();
        }
        
        private  Resources getResources() {
            return TbtDistanceView.access$getResources$cp();
        }
        
        private  float getSize18() {
            return TbtDistanceView.access$getSize18$cp();
        }
        
        private  float getSize22() {
            return TbtDistanceView.access$getSize22$cp();
        }
    }
}
