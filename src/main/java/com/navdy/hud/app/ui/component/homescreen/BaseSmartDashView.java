package com.navdy.hud.app.ui.component.homescreen;

import com.navdy.hud.app.maps.MapEvents$GPSSpeedEvent;
import com.navdy.hud.app.maps.MapEvents$ManeuverDisplay;
import com.navdy.hud.app.maps.NavigationMode;
import com.navdy.service.library.events.input.GestureEvent;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import android.view.View;
import butterknife.ButterKnife;
import com.navdy.hud.app.maps.here.HereMapUtil;
import com.navdy.hud.app.maps.MapEvents;
import com.navdy.hud.app.view.MainView;
import android.animation.AnimatorSet.Builder;
import android.view.ViewGroup;
import com.squareup.otto.Subscribe;
import com.navdy.hud.app.obd.ObdManager;
import com.navdy.hud.app.framework.voice.TTSUtils;
import android.util.AttributeSet;
import android.content.Context;
import com.navdy.hud.app.ui.framework.UIStateManager;
import com.navdy.hud.app.manager.SpeedManager;
import com.navdy.service.library.log.Logger;
import android.os.Handler;
import com.squareup.otto.Bus;
import com.navdy.hud.app.gesture.GestureDetector;
import com.navdy.hud.app.manager.InputManager;
import android.widget.FrameLayout;
import com.navdy.hud.app.manager.InputManager.IInputHandler;
import com.navdy.hud.app.gesture.GestureDetector.GestureListener;

public class BaseSmartDashView extends FrameLayout implements IInputHandler, GestureListener, IHomeScreenLifecycle
{
    protected Bus bus;
    private Handler handler;
    protected HomeScreenView homeScreenView;
    private int lastObdSpeed;
    private Logger logger;
    private SpeedManager speedManager;
    protected UIStateManager uiStateManager;
    
    public BaseSmartDashView(final Context context) {
        this(context, null);
    }
    
    public BaseSmartDashView(final Context context, final AttributeSet set) {
        this(context, set, 0);
    }
    
    public BaseSmartDashView(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
        this.lastObdSpeed = -1;
        this.handler = new Handler();
    }
    
    private void initViews() {
        if (this.uiStateManager != null) {
            if (this.uiStateManager.getCurrentDashboardType() == null) {
                this.logger.v("setting initial dashtype = " + SmartDashViewConstants.Type.Smart);
                this.uiStateManager.setCurrentDashboardType(SmartDashViewConstants.Type.Smart);
            }
            this.setDashboard(this.uiStateManager.getCurrentDashboardType());
        }
        if (TTSUtils.isDebugTTSEnabled()) {
            this.handler.postDelayed((Runnable)new Runnable() {
                @Override
                public void run() {
                    String s;
                    if (SpeedManager.getInstance().getObdSpeed() != -1) {
                        s = "Using OBD RawSpeed";
                    }
                    else {
                        s = "Using GPS RawSpeed";
                    }
                    TTSUtils.debugShowSpeedInput(s);
                }
            }, 60000L);
        }
    }
    
    private void updateSpeedLimitInternal(final float n) {
        this.updateSpeedLimit(Math.round(SpeedManager.convert(n, SpeedManager.SpeedUnit.METERS_PER_SECOND, SpeedManager.getInstance().getSpeedUnit())));
    }
    
    public void ObdPidChangeEvent(final ObdManager.ObdPidChangeEvent obdPidChangeEvent) {
        switch (obdPidChangeEvent.pid.getId()) {
            case 13:
                this.updateSpeed(false);
                break;
        }
    }
    
    @Subscribe
    public void ObdStateChangeEvent(final ObdManager.ObdConnectionStatusEvent obdConnectionStatusEvent) {
        this.updateSpeed(true);
    }
    
    protected void adjustDashHeight(final int height) {
        ((ViewGroup.MarginLayoutParams)this.getLayoutParams()).height = height;
        this.invalidate();
        this.requestLayout();
        if (height == HomeScreenResourceValues.openMapHeight && this.homeScreenView.getDisplayMode() == HomeScreen.DisplayMode.SMART_DASH) {
            this.animateToFullMode();
        }
    }
    
    protected void animateToFullMode() {
    }
    
    protected void animateToFullModeInternal(final Builder builder) {
    }
    
    public void getCustomAnimator(final MainView.CustomAnimationMode customAnimationMode, final Builder builder) {
    }
    
    public SmartDashViewConstants.Type getDashBoardType() {
        return this.uiStateManager.getCurrentDashboardType();
    }
    
    public void getTopAnimator(final Builder builder, final boolean b) {
    }
    
    public void init(final HomeScreenView homeScreenView) {
        this.homeScreenView = homeScreenView;
        this.initializeView();
        this.bus.register(new Object() {
            @Subscribe
            public void GPSSpeedChangeEvent(final MapEvents$GPSSpeedEvent gpsSpeedEvent) {
                BaseSmartDashView.this.updateSpeed(false);
            }
            
            @Subscribe
            public void ObdPidChangeEvent(final ObdManager.ObdPidChangeEvent obdPidChangeEvent) {
                BaseSmartDashView.this.ObdPidChangeEvent(obdPidChangeEvent);
            }
            
            @Subscribe
            public void onMapEvent(final MapEvents$ManeuverDisplay maneuverDisplay) {
                BaseSmartDashView.this.updateSpeedLimitInternal(maneuverDisplay.currentSpeedLimit);
            }
            
            @Subscribe
            public void onSpeedDataExpired(final SpeedManager.SpeedDataExpired speedDataExpired) {
                BaseSmartDashView.this.updateSpeed(true);
            }
            
            @Subscribe
            public void onSpeedUnitChanged(final SpeedManager.SpeedUnitChanged speedUnitChanged) {
                BaseSmartDashView.this.updateSpeed(true);
            }
        });
    }
    
    protected void initializeView() {
        this.updateSpeedLimitInternal(HereMapUtil.getCurrentSpeedLimit());
        this.updateSpeed(true);
    }
    
    public IInputHandler nextHandler() {
        return null;
    }
    
    public void onClick() {
    }
    
    protected void onFinishInflate() {
        this.logger = HomeScreenView.sLogger;
        super.onFinishInflate();
        if (!this.isInEditMode()) {
            ButterKnife.inject((View)this);
            this.speedManager = SpeedManager.getInstance();
            final RemoteDeviceManager instance = RemoteDeviceManager.getInstance();
            this.bus = instance.getBus();
            this.uiStateManager = instance.getUiStateManager();
            this.initViews();
        }
    }
    
    public boolean onGesture(final GestureEvent gestureEvent) {
        return false;
    }
    
    public boolean onKey(final InputManager.CustomKeyEvent customKeyEvent) {
        return false;
    }
    
    public void onPause() {
    }
    
    public void onResume() {
    }
    
    public void onTrackHand(final float n) {
    }
    
    public void resetTopViewsAnimator() {
        final MainView.CustomAnimationMode customAnimateMode = this.uiStateManager.getCustomAnimateMode();
        this.logger.v("mode-view:" + customAnimateMode);
        this.setView(customAnimateMode);
    }
    
    public void setDashboard(final SmartDashViewConstants.Type type) {
        switch (type) {
            case Smart:
                this.uiStateManager.setCurrentDashboardType(type);
                break;
            case Eco:
                this.uiStateManager.setCurrentDashboardType(type);
                break;
        }
    }
    
    public void setMiddleGaugeView(final MainView.CustomAnimationMode customAnimationMode) {
    }
    
    public void setView(final MainView.CustomAnimationMode middleGaugeView) {
        this.setMiddleGaugeView(middleGaugeView);
    }
    
    public boolean shouldShowTopViews() {
        return true;
    }
    
    public void updateLayoutForMode(final NavigationMode navigationMode, final boolean b) {
        final boolean navigationActive = this.homeScreenView.isNavigationActive();
        if (b) {
            int n;
            if (navigationActive) {
                n = HomeScreenResourceValues.activeMapHeight;
            }
            else {
                n = HomeScreenResourceValues.openMapHeight;
            }
            this.adjustDashHeight(n);
        }
    }
    
    protected void updateSpeed(final int n) {
    }
    
    protected void updateSpeed(final boolean b) {
        int currentSpeed;
        if ((currentSpeed = this.speedManager.getCurrentSpeed()) == -1) {
            currentSpeed = 0;
        }
        boolean b2 = false;
        if (b || currentSpeed != this.lastObdSpeed) {
            this.updateSpeed(this.lastObdSpeed = currentSpeed);
            b2 = true;
        }
        if (b2 && this.logger.isLoggable(2)) {
            this.logger.v("SPEED updated :" + currentSpeed);
        }
    }
    
    protected void updateSpeedLimit(final int n) {
    }
}
