package com.navdy.hud.app.ui.component.vlist;

import android.content.Context;
import android.graphics.PointF;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.LinearSmoothScroller;
import android.support.v7.widget.RecyclerView.Recycler;
import android.support.v7.widget.RecyclerView.SmoothScroller.Action;
import android.support.v7.widget.RecyclerView.State;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.animation.Interpolator;
import com.navdy.hud.app.ui.component.vlist.VerticalList.Callback;
import com.navdy.service.library.log.Logger;

public class VerticalLayoutManager extends LinearLayoutManager {
    private static final Logger sLogger = new Logger(VerticalLayoutManager.class);
    private Callback callback;
    private Context context;
    private boolean listLoaded;
    private VerticalRecyclerView recyclerView;
    private VerticalList vlist;

    private static class VerticalScroller extends LinearSmoothScroller {
        private Interpolator interpolator = VerticalList.getInterpolator();
        private VerticalLayoutManager layoutManager;
        private int position;
        private VerticalRecyclerView recyclerView;
        private int snapPreference;
        private VerticalList vlist;

        public VerticalScroller(Context context, VerticalList vlist, VerticalLayoutManager layoutManager, VerticalRecyclerView recyclerView, int position, int snapPreference) {
            super(context);
            this.vlist = vlist;
            this.layoutManager = layoutManager;
            this.recyclerView = recyclerView;
            this.position = position;
            this.snapPreference = snapPreference;
        }

        public PointF computeScrollVectorForPosition(int targetPosition) {
            return this.layoutManager.computeScrollVectorForPosition(targetPosition);
        }

        protected int getVerticalSnapPreference() {
            return this.snapPreference;
        }

        protected void onStart() {
            super.onStart();
        }

        protected void onStop() {
            super.onStop();
        }

        protected int calculateTimeForScrolling(int dx) {
            return super.calculateTimeForScrolling(dx);
        }

        protected int calculateTimeForDeceleration(int dx) {
            return this.vlist.animationDuration;
        }

        protected void onSeekTargetStep(int dx, int dy, State state, Action action) {
            super.onSeekTargetStep(dx, dy, state, action);
        }

        protected void updateActionForInterimTarget(Action action) {
            super.updateActionForInterimTarget(action);
        }

        protected void onTargetFound(View targetView, State state, Action action) {
            int dx = calculateDxToMakeVisible(targetView, getHorizontalSnapPreference());
            int dy = calculateDyToMakeVisible(targetView, getVerticalSnapPreference());
            int time = calculateTimeForDeceleration((int) Math.sqrt((double) ((dx * dx) + (dy * dy))));
            if (time > 0) {
                action.update(-dx, -dy, time, this.interpolator);
            }
            if (VerticalLayoutManager.sLogger.isLoggable(2)) {
                VerticalLayoutManager.sLogger.v("target found dy=" + action.getDy());
            }
            this.vlist.targetFound(action.getDy());
        }

        protected float calculateSpeedPerPixel(DisplayMetrics displayMetrics) {
            return super.calculateSpeedPerPixel(displayMetrics);
        }
    }

    public VerticalLayoutManager(Context context, VerticalList vlist, Callback callback, VerticalRecyclerView recyclerView) {
        super(context);
        this.context = context;
        this.vlist = vlist;
        this.callback = callback;
        this.recyclerView = recyclerView;
    }

    public void smoothScrollToPosition(int position, int snapPreference) {
        VerticalScroller verticalScroller = new VerticalScroller(this.context, this.vlist, this, this.recyclerView, position, snapPreference);
        verticalScroller.setTargetPosition(position);
        startSmoothScroll(verticalScroller);
    }

    public void onLayoutChildren(Recycler recycler, State state) {
        super.onLayoutChildren(recycler, state);
        if (!this.listLoaded && !state.isPreLayout()) {
            this.listLoaded = true;
            this.callback.onLoad();
        }
    }

    public void clear() {
        this.listLoaded = false;
    }
}
