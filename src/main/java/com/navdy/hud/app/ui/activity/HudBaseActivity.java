package com.navdy.hud.app.ui.activity;

import android.view.KeyEvent;
import android.view.Window;
import javax.inject.Inject;
import com.navdy.hud.app.manager.InputManager;
import com.navdy.hud.app.common.BaseActivity;

public abstract class HudBaseActivity extends BaseActivity
{
    @Inject
    InputManager inputManager;
    
    protected void makeImmersive(final boolean b) {
        final Window window = this.getWindow();
        int n = 4718592;
        if (b) {
            n = (0x480000 | 0x80);
        }
        window.addFlags(n);
        window.getDecorView().setSystemUiVisibility(5894);
        this.logger.v("setting immersive mode");
    }
    
    public boolean onKeyDown(final int n, final KeyEvent keyEvent) {
        boolean onKeyDown = false;
        this.logger.v("keydown:" + n);
        if (this.inputManager != null) {
            onKeyDown = this.inputManager.onKeyDown(n, keyEvent);
        }
        return onKeyDown || super.onKeyDown(n, keyEvent);
    }
    
    public boolean onKeyLongPress(final int n, final KeyEvent keyEvent) {
        boolean onKeyLongPress = false;
        if (this.inputManager != null) {
            onKeyLongPress = this.inputManager.onKeyLongPress(n, keyEvent);
        }
        return onKeyLongPress || super.onKeyLongPress(n, keyEvent);
    }
    
    public boolean onKeyUp(final int n, final KeyEvent keyEvent) {
        boolean onKeyUp = false;
        if (this.inputManager != null) {
            onKeyUp = this.inputManager.onKeyUp(n, keyEvent);
        }
        return onKeyUp || super.onKeyUp(n, keyEvent);
    }
}
