package com.navdy.hud.app.storage;

class PathManager$1 implements java.io.FileFilter {
    final com.navdy.hud.app.storage.PathManager this$0;
    
    PathManager$1(com.navdy.hud.app.storage.PathManager a) {
        super();
        this.this$0 = a;
    }
    
    public boolean accept(java.io.File a) {
        boolean b;
        boolean b0 = a.isDirectory();
        label2: {
            label0: {
                label1: {
                    if (!b0) {
                        break label1;
                    }
                    if (com.navdy.hud.app.storage.PathManager.access$000().matcher((CharSequence)a.getName()).matches()) {
                        break label0;
                    }
                }
                b = false;
                break label2;
            }
            b = true;
        }
        return b;
    }
}
