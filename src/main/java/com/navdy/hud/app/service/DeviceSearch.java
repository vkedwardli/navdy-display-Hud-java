package com.navdy.hud.app.service;

class DeviceSearch implements com.navdy.service.library.device.RemoteDevice$Listener {
    final private static int SEARCH_TIMEOUT = 15000;
    private java.util.Deque connectedDevices;
    private java.util.Deque connectingDevices;
    private com.navdy.hud.app.service.DeviceSearch$EventSink eventSink;
    final private android.os.Handler handler;
    final protected com.navdy.service.library.log.Logger logger;
    private java.util.Deque remoteDevices;
    private long startTime;
    private boolean started;
    
    public DeviceSearch(android.content.Context a, com.navdy.hud.app.service.DeviceSearch$EventSink a0, boolean b) {
        this.logger = new com.navdy.service.library.log.Logger((this).getClass());
        this.remoteDevices = (java.util.Deque)new java.util.ArrayDeque();
        this.connectingDevices = (java.util.Deque)new java.util.ArrayDeque();
        this.connectedDevices = (java.util.Deque)new java.util.ArrayDeque();
        this.started = false;
        this.startTime = android.os.SystemClock.elapsedRealtime();
        this.handler = new android.os.Handler();
        this.eventSink = a0;
        Object a1 = com.navdy.service.library.device.RemoteDeviceRegistry.getInstance(a).getPairedConnections();
        int i = 0;
        while(i < ((java.util.List)a1).size()) {
            com.navdy.service.library.device.RemoteDevice a2 = new com.navdy.service.library.device.RemoteDevice(a, (com.navdy.service.library.device.connection.ConnectionInfo)((java.util.List)a1).get(i), b);
            a2.addListener((com.navdy.service.library.util.Listenable$Listener)this);
            this.remoteDevices.add(a2);
            i = i + 1;
        }
    }
    
    static com.navdy.hud.app.service.DeviceSearch$EventSink access$000(com.navdy.hud.app.service.DeviceSearch a) {
        return a.eventSink;
    }
    
    private void closeAll(java.util.Deque a) {
        Object a0 = a.iterator();
        while(((java.util.Iterator)a0).hasNext()) {
            com.navdy.service.library.device.RemoteDevice a1 = (com.navdy.service.library.device.RemoteDevice)((java.util.Iterator)a0).next();
            a1.removeListener((com.navdy.service.library.util.Listenable$Listener)this);
            this.queueDisconnect(a1);
        }
    }
    
    private com.navdy.service.library.device.RemoteDevice findDevice(android.bluetooth.BluetoothDevice a, java.util.Deque a0) {
        return this.findDevice(a.getAddress(), a0);
    }
    
    private com.navdy.service.library.device.RemoteDevice findDevice(String s, java.util.Deque a) {
        Object a0 = a.iterator();
        while(true) {
            com.navdy.service.library.device.RemoteDevice a1 = null;
            if (((java.util.Iterator)a0).hasNext()) {
                a1 = (com.navdy.service.library.device.RemoteDevice)((java.util.Iterator)a0).next();
                if (!a1.getDeviceId().getBluetoothAddress().equals(s)) {
                    continue;
                }
            } else {
                a1 = null;
            }
            return a1;
        }
    }
    
    private void queueDisconnect(com.navdy.service.library.device.RemoteDevice a) {
        if (a != null) {
            this.handler.post((Runnable)new com.navdy.hud.app.service.DeviceSearch$1(this, a));
        }
    }
    
    private void sendEvent(com.squareup.wire.Message a) {
        if (this.eventSink != null) {
            this.handler.post((Runnable)new com.navdy.hud.app.service.DeviceSearch$2(this, a));
        }
    }
    
    public void close() {
        synchronized(this) {
            this.stop();
            this.closeAll(this.connectingDevices);
            this.closeAll(this.connectedDevices);
        }
        /*monexit(this)*/;
    }
    
    public boolean forgetDevice(android.bluetooth.BluetoothDevice a) {
        boolean b = false;
        synchronized(this) {
            this.logger.i(new StringBuilder().append("DeviceSearch forgetting:").append(a).toString());
            com.navdy.service.library.device.RemoteDevice a0 = this.findDevice(a, this.connectingDevices);
            if (a0 == null) {
                com.navdy.service.library.device.RemoteDevice a1 = this.findDevice(a, this.remoteDevices);
                if (a1 == null) {
                    b = false;
                } else {
                    this.logger.i("DeviceSearch removing device from remote devices");
                    this.remoteDevices.remove(a1);
                    b = true;
                }
            } else {
                this.logger.i("DeviceSearch removing device from connecting devices");
                this.connectingDevices.remove(a0);
                this.queueDisconnect(a0);
                b = true;
            }
        }
        /*monexit(this)*/;
        return b;
    }
    
    public void handleDisconnect(android.bluetooth.BluetoothDevice a) {
        synchronized(this) {
            this.queueDisconnect(this.findDevice(a, this.connectedDevices));
        }
        /*monexit(this)*/;
    }
    
    public boolean next() {
        boolean b = false;
        synchronized(this) {
            if (android.os.SystemClock.elapsedRealtime() - this.startTime <= 15000L) {
                if (this.remoteDevices.size() <= 0) {
                    if (this.connectingDevices.size() <= 0) {
                        this.stop();
                        b = false;
                    } else {
                        b = true;
                    }
                } else {
                    this.start();
                    com.navdy.service.library.device.RemoteDevice a = (com.navdy.service.library.device.RemoteDevice)this.remoteDevices.removeFirst();
                    if (a.connect()) {
                        this.connectingDevices.add(a);
                    } else {
                        this.remoteDevices.addLast(a);
                    }
                    b = true;
                }
            } else {
                this.stop();
                b = false;
            }
        }
        /*monexit(this)*/;
        return b;
    }
    
    public void onDeviceConnectFailure(com.navdy.service.library.device.RemoteDevice a, com.navdy.service.library.device.connection.Connection$ConnectionFailureCause a0) {
        synchronized(this) {
            this.logger.i(new StringBuilder().append("DeviceSearch connect failure:").append(a.getDeviceId()).toString());
            if (this.connectingDevices.remove(a)) {
                this.remoteDevices.addLast(a);
            }
        }
        /*monexit(this)*/;
    }
    
    public void onDeviceConnected(com.navdy.service.library.device.RemoteDevice a) {
        synchronized(this) {
            this.logger.i(new StringBuilder().append("DeviceSearch connected:").append(a.getDeviceId()).toString());
            this.connectingDevices.remove(a);
            this.connectedDevices.add(a);
            this.sendEvent((com.squareup.wire.Message)new com.navdy.service.library.events.connection.ConnectionStatus(com.navdy.service.library.events.connection.ConnectionStatus.Status.CONNECTION_FOUND, a.getDeviceId().toString()));
        }
        /*monexit(this)*/;
    }
    
    public void onDeviceConnecting(com.navdy.service.library.device.RemoteDevice a) {
    }
    
    public void onDeviceDisconnected(com.navdy.service.library.device.RemoteDevice a, com.navdy.service.library.device.connection.Connection$DisconnectCause a0) {
        synchronized(this) {
            this.logger.i(new StringBuilder().append("DeviceSearch disconnected:").append(a.getDeviceId()).toString());
            this.connectedDevices.remove(a);
            this.remoteDevices.addLast(a);
            this.sendEvent((com.squareup.wire.Message)new com.navdy.service.library.events.connection.ConnectionStatus(com.navdy.service.library.events.connection.ConnectionStatus.Status.CONNECTION_LOST, a.getDeviceId().toString()));
        }
        /*monexit(this)*/;
    }
    
    public void onNavdyEventReceived(com.navdy.service.library.device.RemoteDevice a, com.navdy.service.library.events.NavdyEvent a0) {
    }
    
    public void onNavdyEventReceived(com.navdy.service.library.device.RemoteDevice a, byte[] a0) {
    }
    
    public com.navdy.service.library.device.RemoteDevice select(com.navdy.service.library.device.NavdyDeviceId a) {
        com.navdy.service.library.device.RemoteDevice a0 = null;
        synchronized(this) {
            String s = a.getBluetoothAddress();
            a0 = this.findDevice(s, this.remoteDevices);
            com.navdy.service.library.device.RemoteDevice a1 = this.findDevice(s, this.connectingDevices);
            com.navdy.service.library.device.RemoteDevice a2 = this.findDevice(s, this.connectedDevices);
            if (a0 == null) {
                if (a1 == null) {
                    if (a2 == null) {
                        a0 = null;
                    } else {
                        this.connectedDevices.remove(a2);
                        a2.removeListener((com.navdy.service.library.util.Listenable$Listener)this);
                        a0 = a2;
                    }
                } else {
                    this.connectingDevices.remove(a1);
                    a1.removeListener((com.navdy.service.library.util.Listenable$Listener)this);
                    a0 = a1;
                }
            } else {
                this.remoteDevices.remove(a0);
                a0.removeListener((com.navdy.service.library.util.Listenable$Listener)this);
            }
        }
        /*monexit(this)*/;
        return a0;
    }
    
    public void start() {
        if (!this.started) {
            this.started = true;
            this.sendEvent((com.squareup.wire.Message)new com.navdy.service.library.events.connection.ConnectionStatus(com.navdy.service.library.events.connection.ConnectionStatus.Status.CONNECTION_SEARCH_STARTED, (String)null));
        }
    }
    
    public void stop() {
        if (this.started) {
            this.started = false;
            this.sendEvent((com.squareup.wire.Message)new com.navdy.service.library.events.connection.ConnectionStatus(com.navdy.service.library.events.connection.ConnectionStatus.Status.CONNECTION_SEARCH_FINISHED, (String)null));
        }
    }
}
