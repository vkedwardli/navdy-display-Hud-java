package com.navdy.hud.app.view;

import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.ButterKnife;
import butterknife.InjectView;
import com.navdy.hud.app.R;
import com.navdy.hud.app.analytics.AnalyticsSupport;
import com.navdy.hud.app.device.dial.DialFirmwareUpdater.Versions;
import com.navdy.hud.app.device.dial.DialManager;
import com.navdy.hud.app.event.Shutdown.Reason;
import com.navdy.hud.app.framework.notifications.NotificationManager;
import com.navdy.hud.app.framework.toast.ToastManager;
import com.navdy.hud.app.manager.InputManager.CustomKeyEvent;
import com.navdy.hud.app.manager.InputManager.IInputHandler;
import com.navdy.hud.app.screen.ShutDownScreen.Presenter;
import com.navdy.hud.app.ui.component.ChoiceLayout;
import com.navdy.hud.app.ui.component.ChoiceLayout.Choice;
import com.navdy.hud.app.ui.component.ChoiceLayout.IListener;
import com.navdy.hud.app.ui.component.ChoiceLayout.Mode;
import com.navdy.hud.app.util.ViewUtil;
import com.navdy.service.library.events.input.GestureEvent;
import com.navdy.service.library.log.Logger;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import mortar.Mortar;

public class ShutDownConfirmationView extends RelativeLayout implements IListener, IInputHandler {
    public static final int DIAL_OTA = 2;
    public static final int INACTIVITY = 0;
    private static final int INACTIVITY_SHUTDOWN_TIMEOUT = 60000;
    private static final int INSTALL_UPDATE_AND_SHUTDOWN_TIMEOUT = 30000;
    public static final int OTA = 1;
    private static final int SHUTDOWN_MESSAGE_MAX_WIDTH = 360;
    private static final int SHUTDOWN_RESET_TIMEOUT = 30000;
    private static final int SHUTDOWN_TIMEOUT = 5000;
    private static final int TAG_DO_NOT_SHUT_DOWN = 2;
    private static final int TAG_INSTALL_AND_SHUT_DOWN = 1;
    private static final int TAG_INSTALL_DIAL_UPDATE = 3;
    private static final int TAG_SHUT_DOWN = 0;
    private static final Logger sLogger = new Logger(ShutDownConfirmationView.class);
    private boolean gesturesEnabled;
    private Handler handler;
    private int initialState;
    @InjectView(R.id.choiceLayout)
    ChoiceLayout mChoiceLayout;
    @InjectView(R.id.image)
    ImageView mIcon;
    @InjectView(R.id.title3)
    TextView mInfoText;
    boolean mIsUpdateAvailable;
    @InjectView(R.id.leftSwipe)
    ImageView mLefttSwipe;
    @InjectView(R.id.title2)
    TextView mMainTitleText;
    @Inject
    Presenter mPresenter;
    @InjectView(R.id.rightSwipe)
    ImageView mRightSwipe;
    @InjectView(R.id.mainTitle)
    TextView mScreenTitleText;
    ValueAnimator mSecondsAnimator;
    @InjectView(R.id.title4)
    TextView mSummaryText;
    @InjectView(R.id.title1)
    TextView mTextView1;
    private Reason shutDownCause;
    private int swipeLeftAction;
    private int swipeRightAction;
    private Runnable timeout;
    String timerMessage;
    private String updateTargetVersion;

    public ShutDownConfirmationView(Context context) {
        this(context, null);
    }

    public ShutDownConfirmationView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ShutDownConfirmationView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.mIsUpdateAvailable = false;
        this.handler = new Handler();
        this.gesturesEnabled = false;
        this.swipeLeftAction = -1;
        this.swipeRightAction = -1;
        this.timerMessage = null;
        this.updateTargetVersion = "1.3.2884";
        this.shutDownCause = Reason.INACTIVITY;
        this.initialState = 0;
        if (!isInEditMode()) {
            Mortar.inject(context, this);
        }
        initFromAttributes(context, attrs);
    }

    private void initFromAttributes(Context context, AttributeSet attrs) {
        TypedArray a = context.getTheme().obtainStyledAttributes(attrs, R.styleable.ShutdownScreen, 0, 0);
        try {
            this.initialState = a.getInt(R.styleable.ShutdownScreen_shutdownState, 0);
        } finally {
            a.recycle();
        }
    }

    protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.inject((View) this);
        this.mTextView1.setVisibility(GONE);
        LinearLayout linearLayout = (LinearLayout) findViewById(R.id.infoContainer);
        LayoutParams params = (LayoutParams) linearLayout.getLayoutParams();
        ((MaxWidthLinearLayout) linearLayout).setMaxWidth(SHUTDOWN_MESSAGE_MAX_WIDTH);
        params.width = -2;
        linearLayout.setLayoutParams(params);
        String updateFromVersion = "1.2.2872";
        if (this.mPresenter != null) {
            this.mPresenter.takeView(this);
            ToastManager.getInstance().disableToasts(true);
            NotificationManager.getInstance().enableNotifications(false);
            if (this.mPresenter.isSoftwareUpdatePending()) {
                this.initialState = 1;
                this.updateTargetVersion = this.mPresenter.getUpdateVersion();
                updateFromVersion = this.mPresenter.getCurrentVersion();
            } else if (this.mPresenter.isDialFirmwareUpdatePending()) {
                this.initialState = 2;
                Versions versions = DialManager.getInstance().getDialFirmwareUpdater().getVersions();
                this.updateTargetVersion = "1.0." + versions.local.incrementalVersion;
                updateFromVersion = "1.0." + versions.dial.incrementalVersion;
            } else {
                this.shutDownCause = this.mPresenter.getShutDownCause();
                this.initialState = 0;
            }
        }
        this.gesturesEnabled = false;
        this.swipeLeftAction = -1;
        this.swipeRightAction = -1;
        List<Choice> list;
        if (this.initialState == 1) {
            this.mIsUpdateAvailable = true;
            this.mScreenTitleText.setVisibility(GONE);
            this.mMainTitleText.setText(R.string.update_available);
            this.timerMessage = getResources().getString(R.string.update_installation_will_begin_in, new Object[]{Integer.valueOf(30)});
            this.mInfoText.setVisibility(View.VISIBLE);
            this.mInfoText.setText(this.timerMessage);
            String updateSummary = String.format(getResources().getString(R.string.update_navdy_will_upgrade_from_to), new Object[]{this.updateTargetVersion, updateFromVersion});
            this.mSummaryText.setVisibility(View.VISIBLE);
            this.mSummaryText.setText(updateSummary);
            this.mIcon.setImageResource(R.drawable.icon_software_update);
            list = new ArrayList();
            list.add(new Choice(getContext().getString(R.string.install), 1));
            list.add(new Choice(getContext().getString(R.string.shutdown), 0));
            list.add(new Choice(getContext().getString(R.string.dont_shutdown), 2));
            this.mChoiceLayout.setChoices(Mode.LABEL, list, 0, this);
            this.swipeLeftAction = 1;
            this.swipeRightAction = 2;
            this.gesturesEnabled = true;
            this.mRightSwipe.setVisibility(View.VISIBLE);
            this.mLefttSwipe.setVisibility(View.VISIBLE);
            startCountDown();
        } else if (this.initialState == 2) {
            this.mIsUpdateAvailable = true;
            this.mScreenTitleText.setText("");
            this.mMainTitleText.setText(R.string.dial_update_available);
            this.timerMessage = getResources().getString(R.string.dial_update_installation_will_begin_in, new Object[]{Integer.valueOf(30)});
            this.mInfoText.setVisibility(View.VISIBLE);
            this.mInfoText.setText(this.timerMessage);
            this.mSummaryText.setText(String.format(getResources().getString(R.string.dial_update_will_upgrade_to_from), new Object[]{this.updateTargetVersion, updateFromVersion}));
            this.mSummaryText.setVisibility(View.VISIBLE);
            this.mIcon.setImageResource(R.drawable.icon_dial_update);
            list = new ArrayList();
            list.add(new Choice(getContext().getString(R.string.install), 3));
            list.add(new Choice(getContext().getString(R.string.shutdown), 0));
            list.add(new Choice(getContext().getString(R.string.dont_shutdown), 2));
            this.mChoiceLayout.setChoices(Mode.LABEL, list, 0, this);
            this.swipeLeftAction = 3;
            this.swipeRightAction = 2;
            this.gesturesEnabled = true;
            this.mRightSwipe.setVisibility(View.VISIBLE);
            this.mLefttSwipe.setVisibility(View.VISIBLE);
            startCountDown();
        } else {
            this.mIsUpdateAvailable = false;
            this.mScreenTitleText.setText(null);
            this.mMainTitleText.setText(R.string.shutting_down);
            this.mInfoText.setVisibility(View.VISIBLE);
            setShutdownCause(this.shutDownCause);
            this.mSummaryText.setText(getResources().getString(R.string.powering_off, new Object[]{Integer.valueOf(5)}));
            this.mSummaryText.setVisibility(View.VISIBLE);
            this.mIcon.setImageResource(R.drawable.icon_power);
            list = new ArrayList();
            list.add(new Choice(getContext().getString(R.string.shutdown), 0));
            list.add(new Choice(getContext().getString(R.string.dont_shutdown), 2));
            this.mChoiceLayout.setChoices(Mode.LABEL, list, 0, this);
            sLogger.v("shutdown cause is " + this.shutDownCause + " time is " + getTimeout(this.shutDownCause));
            this.mRightSwipe.setVisibility(GONE);
            this.mLefttSwipe.setVisibility(GONE);
            startCountDown();
        }
        ViewUtil.autosize(this.mMainTitleText, 2, (int) SHUTDOWN_MESSAGE_MAX_WIDTH, (int) R.array.title_sizes);
    }

    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        cancelTimeout();
        if (this.mPresenter != null) {
            this.mPresenter.dropView(this);
            ToastManager.getInstance().disableToasts(false);
            NotificationManager.getInstance().enableNotifications(true);
        }
    }

    public boolean onGesture(GestureEvent event) {
        if (this.gesturesEnabled && event.gesture != null) {
            switch (event.gesture) {
                case GESTURE_SWIPE_LEFT:
                    if (this.swipeLeftAction != -1) {
                        executeItem(0, this.swipeLeftAction);
                        return true;
                    }
                    break;
                case GESTURE_SWIPE_RIGHT:
                    if (this.swipeRightAction != -1) {
                        executeItem(0, this.swipeRightAction);
                        return true;
                    }
                    break;
            }
        }
        return false;
    }

    public boolean onKey(CustomKeyEvent event) {
        if (this.mChoiceLayout == null || this.mChoiceLayout.getVisibility() != 0) {
            return false;
        }
        switch (event) {
            case LEFT:
                startCountDown();
                this.mChoiceLayout.moveSelectionLeft();
                return true;
            case RIGHT:
                startCountDown();
                this.mChoiceLayout.moveSelectionRight();
                return true;
            case SELECT:
                cancelTimeout();
                this.mChoiceLayout.executeSelectedItem(true);
                return true;
            case POWER_BUTTON_DOUBLE_CLICK:
                sLogger.v("power btn dlb-click");
                cancelTimeout();
                this.mPresenter.finish();
                return true;
            case POWER_BUTTON_CLICK:
                sLogger.v("power btn click");
                cancelTimeout();
                this.mPresenter.finish();
                return true;
            default:
                return true;
        }
    }

    public IInputHandler nextHandler() {
        return null;
    }

    public void executeItem(int pos, int id) {
        performAction(id);
    }

    public void itemSelected(int pos, int id) {
    }

    private void performAction(int tag) {
        switch (tag) {
            case 0:
                recordUpdateSelection(false);
                this.mPresenter.shutDown();
                return;
            case 1:
                if (this.mPresenter.isSoftwareUpdatePending()) {
                    recordUpdateSelection(true);
                    this.mPresenter.installAndShutDown();
                    return;
                }
                return;
            case 2:
                recordUpdateSelection(false);
                this.mPresenter.finish();
                return;
            case 3:
                if (this.mPresenter.isDialFirmwareUpdatePending()) {
                    recordUpdateSelection(true);
                    this.mScreenTitleText.setText(R.string.dial_update_started);
                    this.mMainTitleText.setText("");
                    this.mInfoText.setVisibility(View.VISIBLE);
                    List<Choice> list = new ArrayList();
                    list.add(new Choice(getContext().getString(R.string.shutdown), 0));
                    this.mChoiceLayout.setChoices(Mode.LABEL, list, 0, this);
                    this.mPresenter.installDialUpdateAndShutDown();
                    return;
                }
                return;
            default:
                return;
        }
    }

    private void recordUpdateSelection(boolean accepted) {
        if (this.mIsUpdateAvailable) {
            AnalyticsSupport.recordUpdatePrompt(accepted, !this.mPresenter.isSoftwareUpdatePending(), this.updateTargetVersion);
        }
    }

    public void setShutdownCause(Reason cause) {
        sLogger.d("Update is " + (this.mIsUpdateAvailable ? "" : "not ") + "available, shut down cause:" + cause);
        if (this.mIsUpdateAvailable) {
            this.mInfoText.setVisibility(View.VISIBLE);
            return;
        }
        switch (cause) {
            case ENGINE_OFF:
            case ACCELERATE_SHUTDOWN:
            case INACTIVITY:
                this.mInfoText.setVisibility(View.VISIBLE);
                this.mInfoText.setText(getResources().getString(R.string.navdy_inactivity));
                return;
            default:
                this.mInfoText.setVisibility(GONE);
                return;
        }
    }

    private void startCountDown() {
        sLogger.v("startCountDown");
        if (this.timeout != null) {
            this.handler.removeCallbacks(this.timeout);
        }
        if (this.mSecondsAnimator != null) {
            this.mSecondsAnimator.removeAllUpdateListeners();
            this.mSecondsAnimator.removeAllListeners();
            this.mSecondsAnimator.cancel();
        }
        if (this.initialState == 1) {
            if (this.timeout == null) {
                this.timeout = new Runnable() {
                    public void run() {
                        ShutDownConfirmationView.this.performAction(1);
                    }
                };
            }
            this.mSecondsAnimator = ValueAnimator.ofInt(new int[]{30, 0});
            this.mSecondsAnimator.setDuration(30000);
            this.mSecondsAnimator.setInterpolator(new LinearInterpolator());
            this.mSecondsAnimator.addUpdateListener(new AnimatorUpdateListener() {
                public void onAnimationUpdate(ValueAnimator animation) {
                    int value = ((Integer) animation.getAnimatedValue()).intValue();
                    ShutDownConfirmationView.this.mInfoText.setText(ShutDownConfirmationView.this.getResources().getString(R.string.update_installation_will_begin_in, new Object[]{Integer.valueOf(value)}));
                }
            });
            this.handler.postDelayed(this.timeout, 30000);
            this.mSecondsAnimator.start();
        } else if (this.initialState == 2) {
            if (this.timeout == null) {
                this.timeout = new Runnable() {
                    public void run() {
                        ShutDownConfirmationView.this.performAction(3);
                    }
                };
            }
            this.mSecondsAnimator = ValueAnimator.ofInt(new int[]{30, 0});
            this.mSecondsAnimator.setDuration(30000);
            this.mSecondsAnimator.setInterpolator(new LinearInterpolator());
            this.mSecondsAnimator.addUpdateListener(new AnimatorUpdateListener() {
                public void onAnimationUpdate(ValueAnimator animation) {
                    int value = ((Integer) animation.getAnimatedValue()).intValue();
                    ShutDownConfirmationView.this.mInfoText.setText(ShutDownConfirmationView.this.getResources().getString(R.string.dial_update_installation_will_begin_in, new Object[]{Integer.valueOf(value)}));
                }
            });
            this.handler.postDelayed(this.timeout, 30000);
            this.mSecondsAnimator.start();
        } else {
            int timeVal = 30000;
            if (this.mSecondsAnimator == null) {
                timeVal = getTimeout(this.shutDownCause);
            }
            if (this.timeout == null) {
                this.timeout = new Runnable() {
                    public void run() {
                        ShutDownConfirmationView.this.performAction(0);
                    }
                };
            }
            this.mSecondsAnimator = ValueAnimator.ofInt(new int[]{timeVal / 1000, 0});
            this.mSecondsAnimator.setDuration((long) timeVal);
            this.mSecondsAnimator.setInterpolator(new LinearInterpolator());
            this.mSecondsAnimator.addUpdateListener(new AnimatorUpdateListener() {
                public void onAnimationUpdate(ValueAnimator animation) {
                    int value = ((Integer) animation.getAnimatedValue()).intValue();
                    ShutDownConfirmationView.this.mSummaryText.setText(ShutDownConfirmationView.this.getResources().getString(R.string.powering_off, new Object[]{Integer.valueOf(value)}));
                }
            });
            this.handler.postDelayed(this.timeout, (long) timeVal);
            this.mSecondsAnimator.start();
        }
    }

    private int getTimeout(Reason reason) {
        switch (reason) {
            case INACTIVITY:
                return 60000;
            default:
                return 5000;
        }
    }

    private void cancelTimeout() {
        sLogger.v("cancelTimeout");
        if (this.timeout != null) {
            this.handler.removeCallbacks(this.timeout);
        }
        if (this.mSecondsAnimator != null) {
            this.mSecondsAnimator.removeAllUpdateListeners();
            this.mSecondsAnimator.removeAllListeners();
            this.mSecondsAnimator.cancel();
        }
    }
}
