package com.navdy.hud.app.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import butterknife.ButterKnife;
import butterknife.InjectView;

import com.navdy.hud.app.R;
import com.navdy.hud.app.framework.toast.IToastCallback;
import com.navdy.hud.app.framework.toast.ToastManager;
import com.navdy.hud.app.framework.toast.ToastManager$ShowToast;
import com.navdy.hud.app.framework.toast.ToastPresenter;
import com.navdy.hud.app.framework.voice.TTSUtils;
import com.navdy.hud.app.manager.InputManager;
import com.navdy.hud.app.manager.InputManager.CustomKeyEvent;
import com.navdy.hud.app.manager.InputManager.IInputHandler;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.ui.activity.Main;
import com.navdy.hud.app.ui.component.ChoiceLayout.Choice;
import com.navdy.hud.app.ui.component.ConfirmationLayout;
import com.navdy.service.library.events.input.GestureEvent;
import com.navdy.service.library.events.ui.Screen;
import com.navdy.service.library.events.ui.ShowScreen.Builder;
import com.navdy.service.library.log.Logger;
import com.squareup.otto.Bus;
import java.util.List;

public class ToastView extends FrameLayout implements IInputHandler {
    private static final Logger sLogger = new Logger(ToastView.class);
    private Bus bus;
    public boolean gestureOn;
    InputManager inputManager;
    private Main mainScreen;
    @InjectView(R.id.mainView)
    ConfirmationLayout mainView;
    private volatile boolean sendShowEvent;

    public ToastView(Context context) {
        this(context, null);
    }

    public ToastView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ToastView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        RemoteDeviceManager remoteDeviceManager = RemoteDeviceManager.getInstance();
        this.inputManager = remoteDeviceManager.getInputManager();
        this.bus = remoteDeviceManager.getBus();
    }

    protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.inject((View) this);
    }

    public ConfirmationLayout getMainLayout() {
        return this.mainView;
    }

    public boolean onGesture(GestureEvent event) {
        if (this.gestureOn && event.gesture != null) {
            IToastCallback callback = ToastPresenter.getCurrentCallback();
            if (callback == null) {
                return false;
            }
            List<Choice> choice = this.mainView.choiceLayout.getChoices();
            if (choice == null || choice.size() == 0) {
                return false;
            }
            switch (event.gesture) {
                case GESTURE_SWIPE_LEFT:
                    callback.executeChoiceItem(0, ((Choice) choice.get(0)).id);
                    return true;
                case GESTURE_SWIPE_RIGHT:
                    if (choice.size() == 1) {
                        callback.executeChoiceItem(0, ((Choice) choice.get(0)).id);
                    } else {
                        callback.executeChoiceItem(1, ((Choice) choice.get(1)).id);
                    }
                    return true;
            }
        }
        return false;
    }

    public boolean onKey(CustomKeyEvent event) {
        IToastCallback cb = ToastPresenter.getCurrentCallback();
        if (cb != null && cb.onKey(event)) {
            return true;
        }
        switch (event) {
            case LEFT:
                this.mainView.choiceLayout.moveSelectionLeft();
                return true;
            case RIGHT:
                this.mainView.choiceLayout.moveSelectionRight();
                return true;
            case SELECT:
                if (!ToastPresenter.hasTimeout() || this.mainView.choiceLayout.getVisibility() == VISIBLE) {
                    this.mainView.choiceLayout.executeSelectedItem(true);
                    return true;
                }
                dismissToast();
                return true;
            case POWER_BUTTON_LONG_PRESS:
            case LONG_PRESS:
                return false;
            default:
                return true;
        }
    }

    public IInputHandler nextHandler() {
        return null;
    }

    public ConfirmationLayout getConfirmation() {
        return this.mainView;
    }

    public void dismissToast() {
        if (ToastManager.getInstance().isToastDisplayed()) {
            animateOut(false);
        }
    }

    public void animateIn(final String tts, final String screenName, final String id, boolean noStartDelay) {
        this.sendShowEvent = false;
        this.mainView.setTranslationY((float) ToastPresenter.ANIMATION_TRANSLATION);
        this.mainView.setAlpha(0.0f);
        this.mainView.animate().alpha(1.0f).translationY(0.0f).setDuration(noStartDelay ? 0 : 100).setStartDelay(250).withStartAction(new Runnable() {
            public void run() {
                ToastView.sLogger.v("toast:animationIn :" + screenName);
                ToastView.sLogger.v("[focus] toastView");
                ToastView.this.inputManager.setFocus(ToastView.this);
                ToastView.this.setVisibility(View.VISIBLE);
                ToastManager.getInstance().setToastDisplayFlag(true);
            }
        }).withEndAction(new Runnable() {
            public void run() {
                if (tts != null) {
                    TTSUtils.sendSpeechRequest(tts, id);
                }
                if (screenName != null) {
                    try {
                        ToastView.sLogger.v("in dismiss screen:" + screenName);
                        ToastView.this.bus.post(new Builder().screen(Screen.valueOf(screenName)).build());
                        ToastPresenter.clearScreenName();
                    } catch (Throwable t) {
                        ToastView.sLogger.e(t);
                    }
                }
                ToastView.this.bus.post(new ToastManager$ShowToast(ToastPresenter.getCurrentId()));
                ToastView.this.sendShowEvent = true;
            }
        }).start();
    }

    public void animateOut(boolean quickly) {
        int duration;
        if (quickly) {
            duration = 50;
        } else {
            duration = 100;
        }
        if (!this.sendShowEvent) {
            this.bus.post(new ToastManager$ShowToast(ToastPresenter.getCurrentId()));
            this.sendShowEvent = true;
        }
        this.mainView.animate().alpha(0.0f).translationY((float) ToastPresenter.ANIMATION_TRANSLATION).setDuration((long) duration).withStartAction(new Runnable() {
            public void run() {
                String screenName = ToastPresenter.getScreenName();
                if (screenName != null) {
                    try {
                        ToastView.sLogger.v("out dismiss screen:" + screenName);
                        ToastView.this.bus.post(new Builder().screen(Screen.valueOf(screenName)).build());
                        ToastPresenter.clearScreenName();
                    } catch (Throwable t) {
                        ToastView.sLogger.e(t);
                    }
                }
            }
        }).withEndAction(new Runnable() {
            public void run() {
                ToastView.sLogger.v("toast:animationOut");
                if (ToastView.this.mainScreen == null) {
                    ToastView.this.mainScreen = RemoteDeviceManager.getInstance().getUiStateManager().getRootScreen();
                }
                if (ToastView.this.mainScreen != null) {
                    ToastView.this.inputManager.setFocus(null);
                    ToastView.this.mainScreen.setInputFocus();
                }
                ToastView.this.setVisibility(GONE);
                ToastView.this.revertToOriginal();
                ToastPresenter.clear();
                ToastManager.getInstance().setToastDisplayFlag(false);
            }
        }).start();
    }

    public void revertToOriginal() {
        this.mainView.fluctuatorView.stop();
        this.gestureOn = false;
    }

    public ConfirmationLayout getView() {
        return this.mainView;
    }
}
