package com.navdy.hud.app.view;

import android.content.Context;
import android.content.res.Resources;
import android.text.Layout.Alignment;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.MarginLayoutParams;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.ButterKnife;
import butterknife.InjectView;
import com.navdy.hud.app.R;
import com.navdy.service.library.log.Logger;

public class ToolTipView extends LinearLayout {
    private static final Logger sLogger = new Logger(ToolTipView.class);
    private int iconWidth;
    private int margin;
    private int maxIcons;
    private int maxTextWidth;
    private int minTextWidth;
    private int padding;
    private int sideMargin;
    @InjectView(R.id.tooltip_container)
    ViewGroup toolTipContainer;
    @InjectView(R.id.tooltip_scrim)
    View toolTipScrim;
    @InjectView(R.id.tooltip_text)
    TextView toolTipTextView;
    @InjectView(R.id.tooltip_triangle)
    View toolTipTriangle;
    private int totalWidth;
    private int triangleWidth;
    private TextView widthCalcTextView;

    public ToolTipView(Context context) {
        this(context, null);
    }

    public ToolTipView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ToolTipView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        Context context = getContext();
        this.widthCalcTextView = new TextView(context);
        this.widthCalcTextView.setTextAppearance(context, R.style.tool_tip);
        Resources resources = context.getResources();
        this.maxTextWidth = resources.getDimensionPixelSize(R.dimen.tool_tip_max_text_width);
        this.minTextWidth = resources.getDimensionPixelSize(R.dimen.tool_tip_min_text_width);
        this.padding = resources.getDimensionPixelSize(R.dimen.tool_tip_padding);
        this.sideMargin = resources.getDimensionPixelSize(R.dimen.tool_tip_margin);
        this.triangleWidth = resources.getDimensionPixelSize(R.dimen.tool_tip_triangle_width);
        setOrientation(1);
        LayoutInflater.from(context).inflate(R.layout.tooltip_lyt, this, true);
        ButterKnife.inject((View) this);
    }

    public void setParams(int iconWidth, int margin, int maxIcons) {
        this.iconWidth = iconWidth;
        this.margin = margin;
        this.maxIcons = maxIcons;
        this.totalWidth = (iconWidth * maxIcons) + ((maxIcons - 1) * margin);
    }

    public void show(int posIndex, String text) {
        int textWidth;
        int x;
        TextPaint textPaint = this.widthCalcTextView.getPaint();
        boolean scrimOn = false;
        if (new StaticLayout(text, textPaint, this.maxTextWidth, Alignment.ALIGN_NORMAL, 1.0f, 0.0f, false).getLineCount() == 1) {
            int width = (int) textPaint.measureText(text);
            if (width < this.minTextWidth) {
                textWidth = this.minTextWidth;
            } else {
                textWidth = width;
            }
        } else {
            textWidth = this.maxTextWidth;
            scrimOn = true;
        }
        textWidth += this.padding * 2;
        ((MarginLayoutParams) getLayoutParams()).width = textWidth;
        int triangleMiddlePos = ((this.iconWidth * posIndex) + (this.margin * posIndex)) + (this.iconWidth / 2);
        int triangleStartPos = triangleMiddlePos - (this.triangleWidth / 2);
        switch (posIndex) {
            case 0:
            case 1:
                if (textWidth / 2 <= triangleMiddlePos - this.sideMargin) {
                    x = triangleMiddlePos - (textWidth / 2);
                    break;
                } else {
                    x = this.sideMargin;
                    break;
                }
            case 2:
            case 3:
                int distanceRight = (this.totalWidth - triangleMiddlePos) - this.sideMargin;
                if (textWidth / 2 <= distanceRight) {
                    x = triangleMiddlePos - (textWidth / 2);
                    break;
                } else {
                    x = triangleMiddlePos - (textWidth - distanceRight);
                    break;
                }
            default:
                x = 0;
                break;
        }
        ((MarginLayoutParams) this.toolTipTriangle.getLayoutParams()).leftMargin = triangleStartPos - x;
        this.toolTipTextView.setText(text);
        this.toolTipTextView.requestLayout();
        if (scrimOn) {
            this.toolTipScrim.setVisibility(View.VISIBLE);
        } else {
            this.toolTipScrim.setVisibility(GONE);
        }
        setX((float) x);
        requestLayout();
        setVisibility(View.VISIBLE);
    }

    public void hide() {
        setVisibility(View.GONE);
    }
}
