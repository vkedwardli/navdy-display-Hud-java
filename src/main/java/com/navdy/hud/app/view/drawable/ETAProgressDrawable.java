package com.navdy.hud.app.view.drawable;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Paint.Style;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import com.navdy.hud.app.R;

public class ETAProgressDrawable extends GaugeDrawable {
    private int backgroundColor;
    private int foreGroundColor;
    private Drawable progressDrawable;
    private int verticalMargin;

    public ETAProgressDrawable(Context context) {
        super(context, 0);
        Resources res = context.getResources();
        this.backgroundColor = res.getColor(R.color.cyan);
        this.foreGroundColor = res.getColor(R.color.grey_4a);
        this.progressDrawable = res.getDrawable(R.drawable.trip_progress_point_indicator);
        this.verticalMargin = res.getDimensionPixelSize(R.dimen.eta_progress_vertical_margin);
    }

    public void draw(Canvas canvas) {
        super.draw(canvas);
        Rect bounds = getBounds();
        float height = (float) bounds.height();
        float width = (float) bounds.width();
        Rect progressBounds = new Rect(bounds);
        progressBounds.inset(0, this.verticalMargin);
        this.mPaint.setColor(this.backgroundColor);
        this.mPaint.setStyle(Style.FILL);
        canvas.drawRect((float) progressBounds.left, (float) progressBounds.top, (float) progressBounds.right, (float) progressBounds.bottom, this.mPaint);
        int widthFraction = (int) (width * (this.mValue / (this.mMaxValue - this.mMinValue)));
        this.mPaint.setColor(this.foreGroundColor);
        canvas.drawRect((float) progressBounds.left, (float) progressBounds.top, (float) (progressBounds.left + widthFraction), (float) progressBounds.bottom, this.mPaint);
        float imageWidth = height;
        int imageLeft = (int) Math.min((float) (progressBounds.left + widthFraction), width - imageWidth);
        this.progressDrawable.setBounds(imageLeft, bounds.top, (int) (((float) imageLeft) + imageWidth), bounds.bottom);
        this.progressDrawable.draw(canvas);
    }
}
