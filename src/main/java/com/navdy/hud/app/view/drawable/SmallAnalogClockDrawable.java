package com.navdy.hud.app.view.drawable;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint.Cap;
import android.graphics.Paint.Style;
import android.graphics.Rect;
import android.graphics.RectF;
import com.navdy.hud.app.R;
import com.navdy.hud.app.util.DateUtil;

public class SmallAnalogClockDrawable extends CustomDrawable {
    private int frameColor;
    private int frameStrokeWidth;
    private int hour;
    private int hourHandColor;
    private int hourHandStrokeWidth;
    private int minute;
    private int minuteHandColor = -1;
    private int minuteHandStrokeWidth;
    private int seconds;

    public SmallAnalogClockDrawable(Context context) {
        this.frameColor = context.getResources().getColor(R.color.small_analog_clock_frame_color);
        this.frameStrokeWidth = context.getResources().getDimensionPixelSize(R.dimen.small_analog_clock_frame_stroke_width);
        this.hourHandColor = context.getResources().getColor(R.color.cyan);
        this.hourHandStrokeWidth = context.getResources().getDimensionPixelSize(R.dimen.small_analog_clock_hour_hand_width);
        this.minuteHandStrokeWidth = context.getResources().getDimensionPixelSize(R.dimen.small_analog_clock_minute_hand_width);
    }

    public void setTime(int hour, int minute, int seconds) {
        this.hour = hour;
        this.minute = minute;
        this.seconds = seconds;
    }

    public void draw(Canvas canvas) {
        super.draw(canvas);
        Rect bounds = getBounds();
        RectF boundsRectF = new RectF(bounds);
        this.mPaint.setAntiAlias(true);
        this.mPaint.setStyle(Style.STROKE);
        this.mPaint.setStrokeWidth((float) this.frameStrokeWidth);
        this.mPaint.setColor(this.frameColor);
        RectF frameBounds = new RectF(bounds);
        frameBounds.inset(((float) (this.frameStrokeWidth / 2)) + 0.5f, ((float) (this.frameStrokeWidth / 2)) + 0.5f);
        canvas.drawArc(frameBounds, 0.0f, 360.0f, true, this.mPaint);
        this.mPaint.setStyle(Style.STROKE);
        this.mPaint.setColor(this.hourHandColor);
        this.mPaint.setStrokeCap(Cap.ROUND);
        this.mPaint.setStrokeWidth((float) this.hourHandStrokeWidth);
        float hourAngle = DateUtil.getClockAngleForHour(this.hour, this.minute);
        float radius = (float) ((bounds.width() / 2) - 7);
        Canvas canvas2 = canvas;
        canvas2.drawLine((float) bounds.centerX(), (float) bounds.centerY(), (float) ((int) (((double) bounds.centerX()) + (((double) radius) * Math.cos(Math.toRadians((double) hourAngle))))), (float) ((int) (((double) bounds.centerY()) + (((double) radius) * Math.sin(Math.toRadians((double) hourAngle))))), this.mPaint);
        this.mPaint.setStyle(Style.STROKE);
        this.mPaint.setColor(this.minuteHandColor);
        this.mPaint.setStrokeCap(Cap.ROUND);
        this.mPaint.setStrokeWidth((float) this.minuteHandStrokeWidth);
        float minuteAngle = DateUtil.getClockAngleForMinutes(this.minute);
        radius = (float) ((bounds.width() / 2) - 6);
        canvas2 = canvas;
        canvas2.drawLine((float) bounds.centerX(), (float) bounds.centerY(), (float) ((int) (((double) bounds.centerX()) + (((double) radius) * Math.cos(Math.toRadians((double) minuteAngle))))), (float) ((int) (((double) bounds.centerY()) + (((double) radius) * Math.sin(Math.toRadians((double) minuteAngle))))), this.mPaint);
    }
}
