package com.navdy.hud.app.view;

import android.animation.Animator;
import android.animation.LayoutTransition;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.ImageSpan;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.VideoView;
import butterknife.ButterKnife;
import butterknife.InjectView;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.R;
import com.navdy.hud.app.device.dial.DialConstants;
import com.navdy.hud.app.device.dial.DialConstants.DialConnectionStatus;
import com.navdy.hud.app.device.dial.DialManager;
import com.navdy.hud.app.device.dial.DialManagerHelper;
import com.navdy.hud.app.device.dial.DialManagerHelper.IDialForgotten;
import com.navdy.hud.app.device.dial.DialNotification;
import com.navdy.hud.app.event.LocalSpeechRequest;
import com.navdy.hud.app.framework.notifications.NotificationManager;
import com.navdy.hud.app.framework.toast.ToastManager;
import com.navdy.hud.app.framework.toast.ToastManager$ShowToast;
import com.navdy.hud.app.framework.voice.TTSUtils;
import com.navdy.hud.app.manager.InputManager.CustomKeyEvent;
import com.navdy.hud.app.manager.InputManager.IInputHandler;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.screen.DialManagerScreen.Presenter;
import com.navdy.hud.app.ui.component.ChoiceLayout.Choice;
import com.navdy.hud.app.ui.component.ChoiceLayout.IListener;
import com.navdy.hud.app.ui.component.ChoiceLayout.Mode;
import com.navdy.hud.app.ui.component.ConfirmationLayout;
import com.navdy.hud.app.ui.component.image.InitialsImageView.Style;
import com.navdy.hud.app.ui.framework.DefaultAnimationListener;
import com.navdy.service.library.events.audio.SpeechRequest.Builder;
import com.navdy.service.library.events.audio.SpeechRequest.Category;
import com.navdy.service.library.events.input.GestureEvent;
import com.navdy.service.library.log.Logger;
import com.squareup.otto.Subscribe;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;
import javax.inject.Inject;
import mortar.Mortar;

public class DialManagerView extends RelativeLayout implements IInputHandler {
    private static final int CONNECTED_TIMEOUT = 30000;
    public static final int DIAL_CONNECTED = 5;
    public static final int DIAL_CONNECTING = 4;
    public static final int DIAL_CONNECTION_FAILED = 6;
    public static final int DIAL_PAIRED = 3;
    public static final int DIAL_PAIRING = 1;
    public static final int DIAL_SEARCHING = 2;
    private static final String EMPTY = "";
    private static final long POST_OTA_TIMEOUT = TimeUnit.SECONDS.toMillis(15);
    private static final int POS_DISMISS = 0;
    private static final int RESTART_INTERVAL = 2000;
    private static Uri firstLaunchVideoUri;
    private static Uri repairVideoUri;
    private static final Logger sLogger = new Logger(DialManagerView.class);
    private int batteryLevel;
    private int completeCount;
    private IListener connectedChoiceListener;
    private Runnable connectedTimeoutRunnable;
    @InjectView(R.id.connectedView)
    public ConfirmationLayout connectedView;
    private String dialName;
    private boolean firstTime;
    private boolean foundDial;
    private Handler handler;
    private int incrementalVersion;
    private int initialState;
    @Inject
    Presenter presenter;
    @InjectView(R.id.repair_text)
    public TextView rePairTextView;
    private boolean registered;
    private Runnable restartScanRunnable;
    @Inject
    SharedPreferences sharedPreferences;
    private ObjectAnimator spinner;
    private int state;
    @InjectView(R.id.videoContainer)
    public ViewGroup videoContainer;
    private Uri videoUri;
    @InjectView(R.id.videoView)
    public VideoView videoView;
    private boolean videoViewInitialized;

    static /* synthetic */ int access$204(DialManagerView x0) {
        int i = x0.completeCount + 1;
        x0.completeCount = i;
        return i;
    }

    public DialManagerView(Context context) {
        this(context, null);
    }

    public DialManagerView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public DialManagerView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.initialState = 0;
        this.state = 0;
        this.dialName = "Navdy Dial";
        this.handler = new Handler(Looper.getMainLooper());
        this.connectedTimeoutRunnable = new Runnable() {
            public void run() {
                DialManagerView.sLogger.v("connected timeout");
                DialManagerView.this.presenter.dismiss();
            }
        };
        this.connectedChoiceListener = new IListener() {
            public void executeItem(int pos, int id) {
                if (pos == 0) {
                    DialManagerView.this.presenter.dismiss();
                }
            }

            public void itemSelected(int pos, int id) {
            }
        };
        this.restartScanRunnable = new Runnable() {
            public void run() {
                DialManagerView.this.handler.removeCallbacks(this);
                DialManagerView.this.showDialPairing();
            }
        };
        if (isInEditMode()) {
            HudApplication.setContext(context);
        } else {
            Mortar.inject(context, this);
            if (firstLaunchVideoUri == null) {
                String packageName = HudApplication.getAppContext().getPackageName();
                firstLaunchVideoUri = Uri.parse("android.resource://" + packageName + "/raw/dial_pairing_flow");
                repairVideoUri = Uri.parse("android.resource://" + packageName + "/raw/dial_repairing_flow");
            }
        }
        initFromAttributes(context, attrs);
    }

    private void initFromAttributes(Context context, AttributeSet attrs) {
        TypedArray a = context.getTheme().obtainStyledAttributes(attrs, R.styleable.DialManagerScreen, 0, 0);
        try {
            this.initialState = a.getInt(R.styleable.DialManagerScreen_dialState, 1);
            this.batteryLevel = a.getInt(R.styleable.DialManagerScreen_batteryLevel, -1);
        } finally {
            a.recycle();
        }
    }

    protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.inject((View) this);
        ((MaxWidthLinearLayout) findViewById(R.id.infoContainer)).setMaxWidth(getContext().getResources().getDimensionPixelOffset(R.dimen.dial_message_max_width));
    }

    protected void onAttachedToWindow() {
        sLogger.v("onAttachToWindow");
        super.onAttachedToWindow();
        if (this.presenter != null) {
            ToastManager.getInstance().disableToasts(true);
            NotificationManager.getInstance().enableNotifications(false);
            this.presenter.takeView(this);
            this.presenter.getBus().register(this);
            this.registered = true;
            return;
        }
        setState(this.initialState);
    }

    protected void onDetachedFromWindow() {
        sLogger.v("onDetachToWindow");
        clearConnectedTimeout();
        if (this.videoContainer.getVisibility() == VISIBLE) {
            sLogger.v("stop dial scan");
            DialManager.getInstance().stopScan(false);
            stopVideo();
        }
        ToastManager.getInstance().disableToasts(false);
        NotificationManager.getInstance().enableNotifications(true);
        super.onDetachedFromWindow();
        if (this.presenter != null) {
            this.presenter.dropView(this);
            if (this.registered) {
                this.presenter.getBus().unregister(this);
            }
        }
        stopSpinner();
        if (!isInfoViewVisible()) {
            DialManagerHelper.sendLocalyticsEvent(this.presenter.getHandler(), this.firstTime, this.foundDial, 10000, true);
        }
    }

    public boolean onGesture(GestureEvent event) {
        return false;
    }

    public boolean onKey(CustomKeyEvent event) {
        switch (event) {
            case POWER_BUTTON_CLICK:
            case POWER_BUTTON_DOUBLE_CLICK:
                clearConnectedTimeout();
                this.presenter.dismiss();
                break;
            case LEFT:
                if (isInfoViewVisible()) {
                    resetConnectedTimeout();
                    this.connectedView.choiceLayout.moveSelectionLeft();
                    break;
                }
                break;
            case RIGHT:
                if (isInfoViewVisible()) {
                    resetConnectedTimeout();
                    this.connectedView.choiceLayout.moveSelectionRight();
                    break;
                }
                break;
            case SELECT:
                if (isInfoViewVisible()) {
                    clearConnectedTimeout();
                    this.connectedView.choiceLayout.executeSelectedItem(true);
                    break;
                }
                break;
            case POWER_BUTTON_LONG_PRESS:
                clearConnectedTimeout();
                sLogger.v("pwrbtn long press in connected mode");
                DialManager.getInstance().forgetAllDials(true, new IDialForgotten() {
                    public void onForgotten() {
                        DialManagerView.sLogger.v("all forgotten");
                        DialManagerView.this.presenter.getHandler().post(new Runnable() {
                            public void run() {
                                DialManagerView.this.presenter.updateView(null);
                            }
                        });
                    }
                });
                break;
        }
        return true;
    }

    public IInputHandler nextHandler() {
        return null;
    }

    private void setState(int state) {
        if (this.state != state) {
            sLogger.i("Switching to state:" + state);
        }
        this.state = state;
        switch (this.state) {
            case 1:
                showDialPairingState();
                return;
            case 2:
                showDialSearchingState();
                return;
            case 3:
                showPairedState();
                return;
            case 4:
                setConnectingUI();
                return;
            case 5:
                showDialConnectedState();
                return;
            case 6:
                showConnectionFailedState();
                return;
            default:
                return;
        }
    }

    public void showDialPairing() {
        setState(1);
    }

    public void showDialConnected() {
        this.dialName = DialNotification.getDialName();
        this.batteryLevel = DialManager.getInstance().getLastKnownBatteryLevel();
        this.incrementalVersion = DialManager.getInstance().getDialFirmwareUpdater().getVersions().dial.incrementalVersion;
        setState(5);
    }

    public void showDialSearching(String dialName) {
        this.dialName = dialName;
        setState(2);
    }

    private void showDialPairingState() {
        this.firstTime = DialManager.getInstance().getBondedDialCount() == 0;
        this.connectedView.setVisibility(GONE);
        resetConnectedView();
        if (!this.videoViewInitialized) {
            this.videoView.setOnPreparedListener(new OnPreparedListener() {
                public void onPrepared(MediaPlayer mp) {
                    mp.setLooping(true);
                    mp.start();
                    DialManagerView.sLogger.v("Duration= " + mp.getDuration());
                }
            });
            this.videoView.setOnCompletionListener(new OnCompletionListener() {
                public void onCompletion(MediaPlayer mp) {
                    DialManagerView.access$204(DialManagerView.this);
                    DialManagerView.sLogger.v("completed:" + DialManagerView.this.completeCount);
                }
            });
            this.videoViewInitialized = true;
        }
        this.videoContainer.setVisibility(View.VISIBLE);
        if (this.sharedPreferences == null || !this.sharedPreferences.getBoolean(DialConstants.VIDEO_SHOWN_PREF, false)) {
            setVideoURI(firstLaunchVideoUri);
            this.rePairTextView.setVisibility(GONE);
        } else {
            setVideoURI(repairVideoUri);
            this.rePairTextView.setText(getContext().getString(R.string.navdy_dial_repair_text));
            this.rePairTextView.setVisibility(View.VISIBLE);
        }
        sLogger.v("starting scanning");
        DialManager.getInstance().startScan();
    }

    private void setVideoURI(Uri uri) {
        if (uri != null) {
            try {
                if (!uri.equals(this.videoUri)) {
                    this.videoView.setVideoURI(uri);
                    this.videoUri = uri;
                }
            } catch (Throwable t) {
                sLogger.e(t);
            }
        }
        if (uri == null) {
            this.videoView.stopPlayback();
        }
        this.videoUri = uri;
    }

    private void resetConnectedView() {
        this.connectedView.title1.setVisibility(GONE);
        this.connectedView.title1.setTextAppearance(getContext(), R.style.title1);
        this.connectedView.title2.setTextAppearance(getContext(), R.style.title2);
        this.connectedView.title3.setVisibility(GONE);
        this.connectedView.title4.setVisibility(GONE);
        this.connectedView.sideImage.setVisibility(GONE);
        ((MarginLayoutParams) this.connectedView.choiceLayout.getLayoutParams()).bottomMargin = (int) getResources().getDimension(R.dimen.dial_pairing_status_margin_bottom);
        this.connectedView.choiceLayout.setChoices(Mode.LABEL, Collections.EMPTY_LIST, 0, this.connectedChoiceListener);
    }

    private void showDialConnectedState() {
        String fw;
        stopVideo();
        Context context = getContext();
        Resources resources = context.getResources();
        this.connectedView.screenImage.setImage(R.drawable.icon_dial_2, null, Style.DEFAULT);
        this.connectedView.sideImage.setImageResource(R.drawable.icon_sm_success);
        this.connectedView.sideImage.setVisibility(View.VISIBLE);
        this.connectedView.titleContainer.setVisibility(View.VISIBLE);
        this.connectedView.title1.setTextAppearance(context, R.style.title2);
        this.connectedView.title1.setText(resources.getString(R.string.dial_paired_text));
        this.connectedView.title2.setTextAppearance(context, R.style.title1);
        if (this.dialName != null) {
            this.connectedView.title2.setText(this.dialName);
        } else {
            this.connectedView.title2.setVisibility(GONE);
        }
        switch (DialManager.getBatteryLevelCategory(this.batteryLevel)) {
            case LOW_BATTERY:
                SpannableStringBuilder ss1 = new SpannableStringBuilder("  " + resources.getString(R.string.dial_battery_level_low, new Object[]{DialNotification.getDialBatteryLevel()}));
                ss1.setSpan(new ImageSpan(context, R.drawable.icon_dial_batterylow_2_copy, 1), 0, 1, 33);
                this.connectedView.title3.setText(ss1);
                break;
            case VERY_LOW_BATTERY:
                SpannableStringBuilder ss2 = new SpannableStringBuilder("  " + resources.getString(R.string.dial_battery_level_very_low, new Object[]{DialNotification.getDialBatteryLevel()}));
                ss2.setSpan(new ImageSpan(context, R.drawable.icon_dial_batterylow_copy, 1), 0, 1, 33);
                this.connectedView.title3.setText(ss2);
                break;
            case EXTREMELY_LOW_BATTERY:
                SpannableStringBuilder ss3 = new SpannableStringBuilder("  " + resources.getString(R.string.dial_battery_level_extremely_low, new Object[]{DialNotification.getDialBatteryLevel()}));
                ss3.setSpan(new ImageSpan(context, R.drawable.icon_dial_batterylow_copy, 1), 0, 1, 33);
                this.connectedView.title3.setText(ss3);
                break;
            default:
                this.connectedView.title3.setText(resources.getString(R.string.dial_battery_level_ok));
                break;
        }
        int incremental = this.incrementalVersion;
        if (incremental == -1) {
            fw = "";
        } else {
            fw = "1.0." + incremental;
        }
        this.connectedView.title4.setText(resources.getString(R.string.dial_firmware_rev, new Object[]{fw}));
        this.connectedView.title4.setVisibility(View.VISIBLE);
        ((MarginLayoutParams) this.connectedView.choiceLayout.getLayoutParams()).bottomMargin = (int) resources.getDimension(R.dimen.dial_connected_status_margin_bottom);
        List<Choice> choiceList = new ArrayList(2);
        choiceList.add(new Choice(resources.getString(R.string.dial_paired_choice_dismiss), 0));
        this.connectedView.choiceLayout.setChoices(Mode.LABEL, choiceList, 0, this.connectedChoiceListener);
        this.connectedView.setVisibility(View.VISIBLE);
        if (this.presenter != null) {
            Handler handler = this.presenter.getHandler();
            handler.removeCallbacks(this.connectedTimeoutRunnable);
            handler.postDelayed(this.connectedTimeoutRunnable, 30000);
        }
    }

    private void showDialSearchingState() {
        this.firstTime = false;
        resetConnectedView();
        setConnectingUI();
        sLogger.v("starting scanning");
        DialManager.getInstance().startScan();
        this.handler.postDelayed(this.restartScanRunnable, POST_OTA_TIMEOUT);
    }

    private void stopVideo() {
        if (this.videoView.isPlaying()) {
            setVideoURI(null);
            sLogger.v("stopped video");
        } else {
            sLogger.v("video not playing");
        }
        this.videoContainer.setVisibility(GONE);
    }

    @Subscribe
    public void onShowToast(ToastManager$ShowToast event) {
        sLogger.v("got toast:" + event.name);
        if (TextUtils.equals(event.name, DialNotification.DIAL_CONNECT_ID) && this.presenter != null) {
            this.presenter.dismiss();
        }
    }

    @Subscribe
    public void onDialConnectionStatus(DialConnectionStatus event) {
        if (!isInfoViewVisible()) {
            sLogger.v("onDialConnectionStatus:" + event.status + " name:" + event.dialName);
            if (this.foundDial) {
                sLogger.v("already found dial");
                return;
            }
            DialManager dialManager = DialManager.getInstance();
            this.handler.removeCallbacks(this.restartScanRunnable);
            stopSpinner();
            switch (event.status) {
                case CONNECTED:
                    sLogger.v("dial connected:" + event.dialName);
                    this.foundDial = true;
                    dialManager.stopScan(false);
                    ToastManager toastManager = ToastManager.getInstance();
                    toastManager.dismissCurrentToast();
                    toastManager.clearAllPendingToast();
                    toastManager.disableToasts(false);
                    sLogger.v("show connected state");
                    showPairingSuccessUI(event.dialName);
                    return;
                case CONNECTING:
                    sLogger.v("dial connnecting:" + event.dialName);
                    stopVideo();
                    setConnectingUI();
                    return;
                case DISCONNECTED:
                    sLogger.v("dial connection disconnected:" + event.dialName);
                    this.handler.post(this.restartScanRunnable);
                    return;
                case CONNECTION_FAILED:
                    sLogger.v("dial connection failed:" + event.dialName);
                    setState(6);
                    this.handler.postDelayed(this.restartScanRunnable, 2000);
                    return;
                case NO_DIAL_FOUND:
                    sLogger.v("no dials found");
                    showDialPairing();
                    return;
                default:
                    return;
            }
        }
    }

    private void resetConnectedTimeout() {
        Handler handler = this.presenter.getHandler();
        handler.removeCallbacks(this.connectedTimeoutRunnable);
        handler.postDelayed(this.connectedTimeoutRunnable, 30000);
    }

    private void clearConnectedTimeout() {
        this.presenter.getHandler().removeCallbacks(this.connectedTimeoutRunnable);
    }

    private boolean isInfoViewVisible() {
        return this.connectedView.getVisibility() == VISIBLE && this.connectedView.title4.getVisibility() == VISIBLE;
    }

    private void startSpinner() {
        if (this.spinner == null) {
            this.spinner = ObjectAnimator.ofFloat(this.connectedView.sideImage, View.ROTATION, new float[]{360.0f});
            this.spinner.setDuration(500);
            this.spinner.setInterpolator(new AccelerateDecelerateInterpolator());
            this.spinner.addListener(new DefaultAnimationListener() {
                public void onAnimationEnd(Animator animation) {
                    if (DialManagerView.this.spinner != null) {
                        DialManagerView.this.spinner.setStartDelay(33);
                        DialManagerView.this.spinner.start();
                    }
                }
            });
        }
        if (!this.spinner.isRunning()) {
            this.spinner.start();
        }
    }

    private void stopSpinner() {
        if (this.spinner != null) {
            this.spinner.removeAllListeners();
            this.spinner.cancel();
            this.connectedView.sideImage.setRotation(0.0f);
            this.spinner = null;
        }
    }

    private void setConnectingUI() {
        stopVideo();
        this.connectedView.screenImage.setImage(R.drawable.icon_dial_2, null, Style.DEFAULT);
        this.connectedView.screenImage.setVisibility(View.VISIBLE);
        this.connectedView.mainSection.setLayoutTransition(new LayoutTransition());
        this.connectedView.mainSection.removeView(this.connectedView.titleContainer);
        this.connectedView.sideImage.setImageResource(R.drawable.icon_sm_spinner);
        this.connectedView.sideImage.setVisibility(View.VISIBLE);
        startSpinner();
        this.connectedView.setVisibility(View.VISIBLE);
    }

    private void showConnectionFailedState() {
        stopVideo();
        this.connectedView.screenImage.setImage(R.drawable.icon_dial_2, null, Style.DEFAULT);
        this.connectedView.screenImage.setVisibility(View.VISIBLE);
        this.connectedView.sideImage.setImageResource(R.drawable.icon_connection_failed);
        this.connectedView.sideImage.setVisibility(View.VISIBLE);
        this.connectedView.setVisibility(View.VISIBLE);
    }

    private void showPairingSuccessUI(String dialName) {
        this.dialName = dialName;
        setState(3);
        RemoteDeviceManager.getInstance().getBus().post(new LocalSpeechRequest(new Builder().words(TTSUtils.TTS_DIAL_CONNECTED).language(Locale.getDefault().toLanguageTag()).category(Category.SPEECH_NOTIFICATION).build()));
        this.handler.postDelayed(new Runnable() {
            public void run() {
                if (DialManagerView.this.presenter == null || !DialManagerView.this.presenter.isActive()) {
                    DialManagerView.sLogger.v("showPairingSuccessUI:hide not active");
                } else {
                    DialManagerView.this.presenter.dismiss();
                }
            }
        }, 5000);
    }

    private void showPairedState() {
        sLogger.v("showPairingSuccessUI=" + this.dialName);
        Context context = getContext();
        stopVideo();
        Resources resources = context.getResources();
        this.connectedView.mainSection.removeView(this.connectedView.titleContainer);
        this.connectedView.mainSection.setLayoutTransition(new LayoutTransition());
        this.connectedView.screenImage.setImage(R.drawable.icon_dial_2, null, Style.DEFAULT);
        this.connectedView.screenImage.setVisibility(View.VISIBLE);
        this.connectedView.sideImage.setImageResource(R.drawable.icon_sm_success);
        this.connectedView.sideImage.setVisibility(View.VISIBLE);
        this.connectedView.title1.setVisibility(GONE);
        this.connectedView.title2.setTextAppearance(context, R.style.Glances_1_bold);
        this.connectedView.title2.setText(resources.getString(R.string.navdy_dial));
        this.dialName = DialNotification.getDialAddressPart(this.dialName);
        if (TextUtils.isEmpty(this.dialName)) {
            this.connectedView.title3.setVisibility(GONE);
        } else {
            this.connectedView.title3.setText(resources.getString(R.string.navdy_dial_name, new Object[]{this.dialName}));
            this.connectedView.title3.setVisibility(View.VISIBLE);
        }
        this.connectedView.titleContainer.setVisibility(View.VISIBLE);
        this.connectedView.mainSection.addView(this.connectedView.titleContainer);
        this.connectedView.setVisibility(View.VISIBLE);
    }
}
