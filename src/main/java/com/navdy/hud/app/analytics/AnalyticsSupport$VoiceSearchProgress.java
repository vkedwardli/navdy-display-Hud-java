package com.navdy.hud.app.analytics;


    public enum AnalyticsSupport$VoiceSearchProgress {
        PROGRESS_SEARCHING(0),
        PROGRESS_DESTINATION_FOUND(1),
        PROGRESS_LIST_DISPLAYED(2),
        PROGRESS_ROUTE_SELECTED(3);

        private int value;
        AnalyticsSupport$VoiceSearchProgress(int value) {
            this.value = value;
        }
        public int getValue() {
            return value;
        }
    }
    
//final class AnalyticsSupport$VoiceSearchProgress extends Enum {
//    final private static com.navdy.hud.app.analytics.AnalyticsSupport$VoiceSearchProgress[] $VALUES;
//    final public static com.navdy.hud.app.analytics.AnalyticsSupport$VoiceSearchProgress PROGRESS_DESTINATION_FOUND;
//    final public static com.navdy.hud.app.analytics.AnalyticsSupport$VoiceSearchProgress PROGRESS_LIST_DISPLAYED;
//    final public static com.navdy.hud.app.analytics.AnalyticsSupport$VoiceSearchProgress PROGRESS_ROUTE_SELECTED;
//    final public static com.navdy.hud.app.analytics.AnalyticsSupport$VoiceSearchProgress PROGRESS_SEARCHING;
//    
//    static {
//        PROGRESS_SEARCHING = new com.navdy.hud.app.analytics.AnalyticsSupport$VoiceSearchProgress("PROGRESS_SEARCHING", 0);
//        PROGRESS_DESTINATION_FOUND = new com.navdy.hud.app.analytics.AnalyticsSupport$VoiceSearchProgress("PROGRESS_DESTINATION_FOUND", 1);
//        PROGRESS_LIST_DISPLAYED = new com.navdy.hud.app.analytics.AnalyticsSupport$VoiceSearchProgress("PROGRESS_LIST_DISPLAYED", 2);
//        PROGRESS_ROUTE_SELECTED = new com.navdy.hud.app.analytics.AnalyticsSupport$VoiceSearchProgress("PROGRESS_ROUTE_SELECTED", 3);
//        com.navdy.hud.app.analytics.AnalyticsSupport$VoiceSearchProgress[] a = new com.navdy.hud.app.analytics.AnalyticsSupport$VoiceSearchProgress[4];
//        a[0] = PROGRESS_SEARCHING;
//        a[1] = PROGRESS_DESTINATION_FOUND;
//        a[2] = PROGRESS_LIST_DISPLAYED;
//        a[3] = PROGRESS_ROUTE_SELECTED;
//        $VALUES = a;
//    }
//    
//    private AnalyticsSupport$VoiceSearchProgress(String s, int i) {
//        super(s, i);
//    }
//    
//    public static com.navdy.hud.app.analytics.AnalyticsSupport$VoiceSearchProgress valueOf(String s) {
//        return (com.navdy.hud.app.analytics.AnalyticsSupport$VoiceSearchProgress)Enum.valueOf(com.navdy.hud.app.analytics.AnalyticsSupport$VoiceSearchProgress.class, s);
//    }
//    
//    public static com.navdy.hud.app.analytics.AnalyticsSupport$VoiceSearchProgress[] values() {
//        return $VALUES.clone();
//    }
//}
//