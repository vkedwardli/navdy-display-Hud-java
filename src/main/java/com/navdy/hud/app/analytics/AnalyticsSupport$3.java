package com.navdy.hud.app.analytics;

class AnalyticsSupport$3 {
    final static int[] $SwitchMap$com$navdy$service$library$events$audio$VoiceSearchResponse$VoiceSearchState;
    final static int[] $SwitchMap$com$navdy$service$library$events$preferences$DriverProfilePreferences$FeatureMode;
    final static int[] $SwitchMap$com$navdy$service$library$events$preferences$NavigationPreferences$RerouteForTraffic;
    
    static {
        $SwitchMap$com$navdy$service$library$events$audio$VoiceSearchResponse$VoiceSearchState = new int[com.navdy.service.library.events.audio.VoiceSearchResponse.VoiceSearchState.values().length];
        com.navdy.service.library.events.audio.VoiceSearchResponse.VoiceSearchState a0 = com.navdy.service.library.events.audio.VoiceSearchResponse.VoiceSearchState.VOICE_SEARCH_STARTING;
        try {
            $SwitchMap$com$navdy$service$library$events$audio$VoiceSearchResponse$VoiceSearchState[a0.ordinal()] = 1;
        } catch(NoSuchFieldError ignored) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$audio$VoiceSearchResponse$VoiceSearchState[com.navdy.service.library.events.audio.VoiceSearchResponse.VoiceSearchState.VOICE_SEARCH_LISTENING.ordinal()] = 2;
        } catch(NoSuchFieldError ignored) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$audio$VoiceSearchResponse$VoiceSearchState[com.navdy.service.library.events.audio.VoiceSearchResponse.VoiceSearchState.VOICE_SEARCH_SUCCESS.ordinal()] = 3;
        } catch(NoSuchFieldError ignored) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$audio$VoiceSearchResponse$VoiceSearchState[com.navdy.service.library.events.audio.VoiceSearchResponse.VoiceSearchState.VOICE_SEARCH_SEARCHING.ordinal()] = 4;
        } catch(NoSuchFieldError ignored) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$audio$VoiceSearchResponse$VoiceSearchState[com.navdy.service.library.events.audio.VoiceSearchResponse.VoiceSearchState.VOICE_SEARCH_ERROR.ordinal()] = 5;
        } catch(NoSuchFieldError ignored) {
        }
        $SwitchMap$com$navdy$service$library$events$preferences$NavigationPreferences$RerouteForTraffic = new int[com.navdy.service.library.events.preferences.NavigationPreferences.RerouteForTraffic.values().length];
        com.navdy.service.library.events.preferences.NavigationPreferences.RerouteForTraffic a2 = com.navdy.service.library.events.preferences.NavigationPreferences.RerouteForTraffic.REROUTE_AUTOMATIC;
        try {
            $SwitchMap$com$navdy$service$library$events$preferences$NavigationPreferences$RerouteForTraffic[a2.ordinal()] = 1;
        } catch(NoSuchFieldError ignored) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$preferences$NavigationPreferences$RerouteForTraffic[com.navdy.service.library.events.preferences.NavigationPreferences.RerouteForTraffic.REROUTE_CONFIRM.ordinal()] = 2;
        } catch(NoSuchFieldError ignored) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$preferences$NavigationPreferences$RerouteForTraffic[com.navdy.service.library.events.preferences.NavigationPreferences.RerouteForTraffic.REROUTE_NEVER.ordinal()] = 3;
        } catch(NoSuchFieldError ignored) {
        }
        $SwitchMap$com$navdy$service$library$events$preferences$DriverProfilePreferences$FeatureMode = new int[com.navdy.service.library.events.preferences.DriverProfilePreferences.FeatureMode.values().length];
        com.navdy.service.library.events.preferences.DriverProfilePreferences.FeatureMode a4 = com.navdy.service.library.events.preferences.DriverProfilePreferences.FeatureMode.FEATURE_MODE_BETA;
        try {
            $SwitchMap$com$navdy$service$library$events$preferences$DriverProfilePreferences$FeatureMode[a4.ordinal()] = 1;
        } catch(NoSuchFieldError ignored) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$preferences$DriverProfilePreferences$FeatureMode[com.navdy.service.library.events.preferences.DriverProfilePreferences.FeatureMode.FEATURE_MODE_RELEASE.ordinal()] = 2;
        } catch(NoSuchFieldError ignored) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$preferences$DriverProfilePreferences$FeatureMode[com.navdy.service.library.events.preferences.DriverProfilePreferences.FeatureMode.FEATURE_MODE_EXPERIMENTAL.ordinal()] = 3;
        } catch(NoSuchFieldError ignored) {
        }
    }
}
