package com.navdy.hud.app.device.gps;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.Process;
import android.os.SystemClock;
import android.os.UserHandle;

import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.debug.RouteRecorder;
import com.navdy.hud.app.device.PowerManager;
import com.navdy.hud.app.service.ConnectionServiceAnalyticsSupport;
import com.navdy.hud.app.service.HudConnectionService;
import com.navdy.service.library.events.location.Coordinate;
import com.navdy.service.library.events.location.TransmitLocation;
import com.navdy.service.library.log.Logger;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

public class GpsManager {
    final private static int ACCEPTABLE_THRESHOLD = 2;
    final private static String DISCARD_TIME = "DISCARD_TIME";
    final private static String GPS_SYSTEM_PROPERTY = "persist.sys.hud_gps";
    final public static long INACCURATE_GPS_REPORT_INTERVAL;
    final private static int INITIAL_INTERVAL = 15000;
    final private static int KEEP_PHONE_GPS_ON = -1;
    final private static int LOCATION_ACCURACY_THROW_AWAY_THRESHOLD = 30;
    final private static int LOCATION_TIME_THROW_AWAY_THRESHOLD = 2000;
    final public static double MAXIMUM_STOPPED_SPEED = 0.22353333333333333;
    final private static double METERS_PER_MILE = 1609.44;
    final public static long MINIMUM_DRIVE_TIME = 3000L;
    final public static double MINIMUM_DRIVING_SPEED = 2.2353333333333336;
    final static int MSG_NMEA = 1;
    final static int MSG_SATELLITE_STATUS = 2;
    final private static int SECONDS_PER_HOUR = 3600;
    final private static String SET_LOCATION_DISCARD_TIME_THRESHOLD = "SET_LOCATION_DISCARD_TIME_THRESHOLD";
    final private static String SET_UBLOX_ACCURACY = "SET_UBLOX_ACCURACY";
    final private static TransmitLocation START_SENDING_LOCATION;
    final private static String START_UBLOX = "START_REPORTING_UBLOX_LOCATION";
    final private static TransmitLocation STOP_SENDING_LOCATION;
    final private static String STOP_UBLOX = "STOP_REPORTING_UBLOX_LOCATION";
    final private static String TAG_GPS = "[Gps-i] ";
    final private static String TAG_GPS_LOC = "[Gps-loc] ";
    final private static String TAG_GPS_LOC_NAVDY = "[Gps-loc-navdy] ";
    final private static String TAG_GPS_NMEA = "[Gps-nmea] ";
    final private static String TAG_GPS_STATUS = "[Gps-stat] ";
    final private static String TAG_PHONE_LOC = "[Phone-loc] ";
    final private static String UBLOX_ACCURACY = "UBLOX_ACCURACY";
    final private static float UBLOX_MIN_ACCEPTABLE_THRESHOLD = 5f;
    final private static long WARM_RESET_INTERVAL;
    final private static Logger sLogger;
    final private static GpsManager singleton;
    private LocationSource activeLocationSource;
    private HudConnectionService connectionService;
    private boolean driving;
    private BroadcastReceiver eventReceiver;
    private volatile boolean extrapolationOn;
    private volatile long extrapolationStartTime;
    private boolean firstSwitchToUbloxFromPhone;
    private float gpsAccuracy;
    private long gpsLastEventTime;
    private long gpsManagerStartTime;
    private Handler handler;
    private long inaccurateGpsCount;
    private boolean isDebugTTSEnabled;
    private boolean keepPhoneGpsOn;
    private long lastInaccurateGpsTime;
    private Coordinate lastPhoneCoordinate;
    private long lastPhoneCoordinateTime;
    private Location lastUbloxCoordinate;
    private long lastUbloxCoordinateTime;
    private Runnable locationFixRunnable;
    private LocationManager locationManager;
    private LocationListener navdyGpsLocationListener;
    private Handler.Callback nmeaCallback;
    private Handler nmeaHandler;
    private android.os.HandlerThread nmeaHandlerThread;
    private GpsNmeaParser nmeaParser;
    private Runnable noLocationUpdatesRunnable;
    private Runnable noPhoneLocationFixRunnable;
    private ArrayList queue;
    private Runnable queueCallback;
    private volatile boolean startUbloxCalled;
    private boolean switchBetweenPhoneUbloxAllowed;
    private long transitionStartTime;
    private boolean transitioning;
    private LocationListener ubloxGpsLocationListener;
    private GpsStatus.NmeaListener ubloxGpsNmeaListener;
    private GpsStatus.Listener ubloxGpsStatusListener;
    private boolean warmResetCancelled;
    private Runnable warmResetRunnable;
    
    static {
        sLogger = new Logger(GpsManager.class);
        INACCURATE_GPS_REPORT_INTERVAL = TimeUnit.SECONDS.toMillis(30L);
        WARM_RESET_INTERVAL = TimeUnit.SECONDS.toMillis(160L);
        START_SENDING_LOCATION = new TransmitLocation(Boolean.valueOf(true));
        STOP_SENDING_LOCATION = new TransmitLocation(Boolean.valueOf(false));
        singleton = new GpsManager();
    }
    
    private GpsManager() {
        this.queue = new ArrayList();
        this.handler = new Handler(Looper.getMainLooper());
        final GpsManager a14 = this;
        this.ubloxGpsNmeaListener = new GpsStatus.NmeaListener() {
            final GpsManager this$0 = a14;

            public void onNmeaReceived(long j, String s) {
                try {
                    if (sLogger.isLoggable(2)) {
                        sLogger.v(new StringBuilder().append("[Gps-nmea] ").append(s).toString());
                    }
                    this.this$0.processNmea(s);
                } catch(Throwable a) {
                    sLogger.e(a);
                }
            }
        };
        final GpsManager a13 = this;
        this.ubloxGpsLocationListener = new LocationListener() {
            final GpsManager this$0 = a13;

            public void onLocationChanged(Location a) {
                label1: {
                    Throwable a0 = null;
                    label0: {
                        long j = 0L;
                        float f = 0.0f;
                        int i = 0;
                        Logger a1 = null;
                        StringBuilder a2 = null;
                        int i0 = 0;
                        try {
                            if (sLogger.isLoggable(2)) {
                                sLogger.d(new StringBuilder().append("[Gps-loc] ").append((a.isFromMockProvider()) ? "[Mock] " : "").append(a).toString());
                            }
                            if (!this.this$0.startUbloxCalled) {
                                break label1;
                            }
                            if (a.isFromMockProvider()) {
                                break label1;
                            }
                            if (this.this$0.activeLocationSource == LocationSource.UBLOX) {
                                break label1;
                            }
                            LocationSource a3 = this.this$0.activeLocationSource;
                            this.this$0.activeLocationSource = LocationSource.UBLOX;
                            this.this$0.handler.removeCallbacks(this.this$0.noPhoneLocationFixRunnable);
                            this.this$0.handler.removeCallbacks(this.this$0.locationFixRunnable);
                            this.this$0.handler.postDelayed(this.this$0.locationFixRunnable, 1000L);
                            this.this$0.stopSendingLocation();
                            int i1 = (int)(SystemClock.elapsedRealtime() - this.this$0.gpsLastEventTime) / 1000;
                            j = SystemClock.elapsedRealtime() - this.this$0.lastPhoneCoordinateTime;
                            f = (this.this$0.lastPhoneCoordinate == null) ? -1f : (j >= 3000L) ? -1f : this.this$0.lastPhoneCoordinate.accuracy.floatValue();
                            i = (int)((this.this$0.lastUbloxCoordinate == null) ? -1f : this.this$0.lastUbloxCoordinate.getAccuracy());
                            ConnectionServiceAnalyticsSupport.recordGpsAcquireLocation(String.valueOf(i1), String.valueOf(i));
                            long j1 = SystemClock.elapsedRealtime();
                            this.this$0.gpsLastEventTime = j1;
                            GpsManager a4 = this.this$0;
                            StringBuilder a5 = new StringBuilder().append("Phone =");
                            Object a6 = (f != -1f) ? Float.valueOf(f) : "n/a";
                            a4.markLocationFix("Switched to Ublox Gps", a5.append(a6).append(" ublox =").append(i).toString(), false, true);
                            a1 = sLogger;
                            a2 = new StringBuilder().append("[Gps-loc] [LocationProvider] switched from [").append(a3).append("] to [").append(LocationSource.UBLOX).append("] ").append("Phone =");
                            i0 = (f > -1f) ? 1 : (f == -1f) ? 0 : -1;
                        } catch(Throwable a7) {
                            a0 = a7;
                            break label0;
                        }
                        Object a8 = (i0 != 0) ? Float.valueOf(f) : "n/a";
                        try {
                            a1.v(a2.append(a8).append(" ublox =").append(i).append(" time=").append(j).toString());
                        } catch(Throwable a9) {
                            a0 = a9;
                            break label0;
                        }
                        break label1;
                    }
                    sLogger.e(a0);
                }
            }

            public void onProviderDisabled(String s) {
                sLogger.i("[Gps-stat] " + s + "[disabled]");
            }

            public void onProviderEnabled(String s) {
                sLogger.i(new StringBuilder().append("[Gps-stat] ").append(s).append("[enabled]").toString());
            }

            public void onStatusChanged(String s, int i, Bundle a) {
                if (PowerManager.isAwake()) {
                    sLogger.i(new StringBuilder().append("[Gps-stat] ").append(s).append(",").append(i).toString());
                }
            }
        };
        final GpsManager a12 = this;
        this.navdyGpsLocationListener = new LocationListener() {
            private float accuracyMax = 0.0f;
            private float accuracyMin = 0.0f;
            private int accuracySampleCount = 0;
            private long accuracySampleStartTime = SystemClock.elapsedRealtime();
            private float accuracySum = 0.0f;
            final GpsManager this$0 = a12;

            private void collectAccuracyStats(float f) {
                if (f != 0.0f) {
                    this.accuracySum = this.accuracySum + f;
                    this.accuracySampleCount = this.accuracySampleCount + 1;
                    float f0 = this.accuracyMin;
                    int i = (f0 > 0.0f) ? 1 : (f0 == 0.0f) ? 0 : -1;
                    label0: {
                        label1: {
                            if (i == 0) {
                                break label1;
                            }
                            if (!(f < this.accuracyMin)) {
                                break label0;
                            }
                        }
                        this.accuracyMin = f;
                    }
                    if (this.accuracyMax < f) {
                        this.accuracyMax = f;
                    }
                }
                if (this.this$0.lastUbloxCoordinateTime - this.accuracySampleStartTime > 300000L) {
                    if (this.accuracySampleCount > 0) {
                        ConnectionServiceAnalyticsSupport.recordGpsAccuracy(Integer.toString(Math.round(this.accuracyMin)), Integer.toString(Math.round(this.accuracyMax)), Integer.toString(Math.round(this.accuracySum / (float)this.accuracySampleCount)));
                        this.accuracySum = 0.0f;
                        this.accuracyMax = 0.0f;
                        this.accuracyMin = 0.0f;
                        this.accuracySampleCount = 0;
                    }
                    this.accuracySampleStartTime = this.this$0.lastUbloxCoordinateTime;
                }
            }

            public void onLocationChanged(Location a) {
                label0: try {
                    if (!this.this$0.warmResetCancelled) {
                        this.this$0.cancelUbloxResetRunnable();
                    }
                    if (!RouteRecorder.getInstance().isPlaying()) {
                        this.this$0.updateDrivingState(a);
                        this.this$0.lastUbloxCoordinate = a;
                        long j1 = SystemClock.elapsedRealtime();
                        this.this$0.lastUbloxCoordinateTime = j1;
                        this.collectAccuracyStats(a.getAccuracy());
                        HudConnectionService a0 = this.this$0.connectionService;
                        label3: {
                            label4: {
                                if (a0 == null) {
                                    break label4;
                                }
                                if (this.this$0.connectionService.isConnected()) {
                                    break label3;
                                }
                            }
                            if (this.this$0.activeLocationSource == LocationSource.UBLOX) {
                                break label0;
                            }
                            sLogger.v("not connected with phone, start uBloxReporting");
                            this.this$0.startUbloxReporting();
                            break label0;
                        }
                        LocationSource a1 = this.this$0.activeLocationSource;
                        LocationSource a2 = LocationSource.UBLOX;
                        label2: {
                            if (a1 == a2) {
                                break label2;
                            }
                            long j = SystemClock.elapsedRealtime() - this.this$0.lastPhoneCoordinateTime;
                            if (j <= 3000L) {
                                break label2;
                            }
                            sLogger.v(new StringBuilder().append("phone threshold exceeded= ").append(j).append(", start uBloxReporting").toString());
                            this.this$0.startUbloxReporting();
                            break label0;
                        }
                        boolean b = this.this$0.firstSwitchToUbloxFromPhone;
                        label1: {
                            if (!b) {
                                break label1;
                            }
                            if (!this.this$0.switchBetweenPhoneUbloxAllowed) {
                                break label0;
                            }
                        }
                        this.this$0.checkGpsToPhoneAccuracy(a);
                    }
                } catch(Throwable a3) {
                    sLogger.e(a3);
                }
            }

            public void onProviderDisabled(String s) {
            }

            public void onProviderEnabled(String s) {
            }

            public void onStatusChanged(String s, int i, Bundle a) {
            }
        };
        final GpsManager a11 = this;
        this.ubloxGpsStatusListener = new GpsStatus.Listener() {
            final GpsManager this$0 = a11;

            public void onGpsStatusChanged(int i) {
                switch(i) {
                    case 3: {
                        sLogger.i("[Gps-stat] gps got first fix");
                        break;
                    }
                    case 2: {
                        sLogger.w("[Gps-stat] gps stopped searching");
                        break;
                    }
                    case 1: {
                        sLogger.i("[Gps-stat] gps searching...");
                        break;
                    }
                }
            }
        };
        final GpsManager a10 = this;
        this.noPhoneLocationFixRunnable = new Runnable() {
            final GpsManager this$0 = a10;

            public void run() {
                label0: {
                    Throwable a = null;
                    label1: {
                        boolean b = false;
                        try {
                            if (this.this$0.activeLocationSource != LocationSource.UBLOX) {
                                this.this$0.updateDrivingState(null);
                                this.this$0.startSendingLocation();
                                b = true;
                            } else {
                                sLogger.i("[Gps-loc] noPhoneLocationFixRunnable: has location fix now");
                                b = false;
                            }
                        } catch(Throwable a0) {
                            a = a0;
                            break label1;
                        }
                        if (!b) {
                            break label0;
                        }
                        this.this$0.handler.postDelayed(this.this$0.noPhoneLocationFixRunnable, 5000L);
                        break label0;
                    }
                    try {
                        sLogger.e(a);
                    } catch(Throwable a1) {
                        this.this$0.handler.postDelayed(this.this$0.noPhoneLocationFixRunnable, 5000L);
                        throw a1;
                    }
                    this.this$0.handler.postDelayed(this.this$0.noPhoneLocationFixRunnable, 5000L);
                }
            }
        };
        final GpsManager a9 = this;
        this.noLocationUpdatesRunnable = new Runnable() {
            final GpsManager this$0 = a9;

            public void run() {
                this.this$0.updateDrivingState(null);
            }
        };
        final GpsManager a8 = this;
        this.warmResetRunnable = new Runnable() {
            final GpsManager this$0 = a8;

            public void run() {
                try {
                    sLogger.v(new StringBuilder().append("issuing warm reset to ublox, no location report in last ").append(WARM_RESET_INTERVAL).toString());
                    this.this$0.sendGpsEventBroadcast("GPS_WARM_RESET_UBLOX", null);
                } catch(Throwable a) {
                    sLogger.e(a);
                }
            }
        };
        final GpsManager a7 = this;
        this.locationFixRunnable = new Runnable() {
            final GpsManager this$0 = a7;

            public void run() {
                label0: {
                    boolean b = false;
                    Throwable a = null;
                    label1: {
                        boolean b0 = false;
                        label4: {
                            try {
                                b = true;
                                long j = SystemClock.elapsedRealtime() - this.this$0.lastUbloxCoordinateTime;
                                if (j < 3000L) {
                                    b0 = true;
                                    break label4;
                                }
                                b = true;
                                sLogger.i(new StringBuilder().append("[Gps-loc] locationFixRunnable: lost location fix[").append(j).append("]").toString());
                                boolean b1 = this.this$0.extrapolationOn;
                                label2: {
                                    label3: {
                                        if (!b1) {
                                            break label3;
                                        }
                                        b = true;
                                        long j0 = SystemClock.elapsedRealtime() - this.this$0.extrapolationStartTime;
                                        sLogger.i(new StringBuilder().append("[Gps-loc] locationFixRunnable: extrapolation on time:").append(j0).toString());
                                        if (j0 < 5000L) {
                                            break label2;
                                        }
                                        b = true;
                                        this.this$0.extrapolationOn = false;
                                        this.this$0.extrapolationStartTime = 0L;
                                        sLogger.i("[Gps-loc] locationFixRunnable: expired");
                                    }
                                    b = false;
                                    this.this$0.updateDrivingState(null);
                                    sLogger.e("[Gps-loc] lost gps fix");
                                    this.this$0.handler.removeCallbacks(this.this$0.locationFixRunnable);
                                    this.this$0.handler.removeCallbacks(this.this$0.noPhoneLocationFixRunnable);
                                    this.this$0.handler.postDelayed(this.this$0.noPhoneLocationFixRunnable, 5000L);
                                    this.this$0.startSendingLocation();
                                    ConnectionServiceAnalyticsSupport.recordGpsLostLocation(String.valueOf((SystemClock.elapsedRealtime() - this.this$0.gpsLastEventTime) / 1000L));
                                    long j1 = SystemClock.elapsedRealtime();
                                    this.this$0.gpsLastEventTime = j1;
                                    b0 = false;
                                    break label4;
                                }
                                b = true;
                                sLogger.i("[Gps-loc] locationFixRunnable: reset");
                            } catch(Throwable a0) {
                                a = a0;
                                break label1;
                            }
                            this.this$0.handler.postDelayed(this.this$0.locationFixRunnable, 1000L);
                            break label0;
                        }
                        if (!b0) {
                            break label0;
                        }
                        this.this$0.handler.postDelayed(this.this$0.locationFixRunnable, 1000L);
                        break label0;
                    }
                    try {
                        sLogger.e(a);
                    } catch(Throwable a1) {
                        if (b) {
                            this.this$0.handler.postDelayed(this.this$0.locationFixRunnable, 1000L);
                        }
                        throw a1;
                    }
                    if (b) {
                        this.this$0.handler.postDelayed(this.this$0.locationFixRunnable, 1000L);
                    }
                }
            }
        };
        final GpsManager a6 = this;
        this.nmeaCallback = new Handler.Callback() {
            final GpsManager this$0 = a6;

            public boolean handleMessage(Message a) {
                try {
                    switch(a.what) {
                        case 2: {
                            if (SystemClock.elapsedRealtime() - this.this$0.gpsManagerStartTime <= 15000L) {
                                break;
                            }
                            Bundle a0 = (Bundle)a.obj;
                            Bundle a1 = new Bundle();
                            a1.putBundle("satellite_data", a0);
                            this.this$0.sendGpsEventBroadcast("GPS_SATELLITE_STATUS", a1);
                            break;
                        }
                        case 1: {
                            this.this$0.nmeaParser.parseNmeaMessage((String)a.obj);
                            break;
                        }
                    }
                } catch(Throwable a2) {
                    sLogger.e(a2);
                }
                return true;
            }
        };
        final GpsManager a5 = this;
        this.queueCallback = new Runnable() {
            final GpsManager this$0 = a5;

            public void run() {
                try {
                    Context a = HudApplication.getAppContext();
                    UserHandle a0 = Process.myUserHandle();
                    int i = this.this$0.queue.size();
                    if (i > 0) {
                        int i0 = 0;
                        while(i0 < i) {
                            Intent a1 = (Intent) this.this$0.queue.get(i0);
                            a.sendBroadcastAsUser(a1, a0);
                            sLogger.v(new StringBuilder().append("sent-queue gps event user:").append(a1.getAction()).toString());
                            i0 = i0 + 1;
                        }
                        this.this$0.queue.clear();
                    }
                } catch(Throwable a2) {
                    sLogger.e(a2);
                }
            }
        };
        final GpsManager a4 = this;
        this.eventReceiver = new BroadcastReceiver() {
            final GpsManager this$0 = a4;

            public void onReceive(Context a, Intent a0) {
                try {
                    String s = a0.getAction();
                    if (s != null) {
                        int i = 0;
                        RouteRecorder a1 = RouteRecorder.getInstance();
                        int i0 = s.hashCode();
                        label0: {
                            switch(i0) {
                                case -47674184: {
                                    if (!s.equals("EXTRAPOLATION")) {
                                        break;
                                    }
                                    i = 2;
                                    break label0;
                                }
                                case -1788590639: {
                                    if (!s.equals("GPS_DR_STOPPED")) {
                                        break;
                                    }
                                    i = 1;
                                    break label0;
                                }
                                case -1801456507: {
                                    if (!s.equals("GPS_DR_STARTED")) {
                                        break;
                                    }
                                    i = 0;
                                    break label0;
                                }
                                default: {
                                    sLogger.i("ERROR: eventReceiver hashCode: " + Integer.toString(i0) + ", s: " + s);
                                }
                            }
                            i = -1;
                        }
                        switch(i) {
                            case 2: {
                                boolean b = a0.getBooleanExtra("EXTRAPOLATION_FLAG", false);
                                this.this$0.extrapolationOn = b;
                                if (this.this$0.extrapolationOn) {
                                    long j = SystemClock.elapsedRealtime();
                                    this.this$0.extrapolationStartTime = j;
                                } else {
                                    this.this$0.extrapolationStartTime = 0L;
                                }
                                sLogger.v(new StringBuilder().append("extrapolation is:").append(this.this$0.extrapolationOn).append(" time:").append(this.this$0.extrapolationStartTime).toString());
                                break;
                            }
                            case 1: {
                                if (!a1.isRecording()) {
                                    break;
                                }
                                a1.injectMarker("GPS_DR_STOPPED");
                                break;
                            }
                            case 0: {
                                if (!a1.isRecording()) {
                                    break;
                                }
                                String s0 = a0.getStringExtra("drtype");
                                if (s0 == null) {
                                    s0 = "";
                                }
                                a1.injectMarker(new StringBuilder().append("GPS_DR_STARTED ").append(s0).toString());
                                break;
                            }
                        }
                    }
                } catch(Throwable a2) {
                    sLogger.e(a2);
                }
            }
        };
        android.content.Context a = com.navdy.hud.app.HudApplication.getAppContext();
        try {
            int i = com.navdy.hud.app.util.os.SystemProperties.getInt("persist.sys.hud_gps", 0);
            if (i == -1) {
                this.keepPhoneGpsOn = true;
                this.switchBetweenPhoneUbloxAllowed = true;
                sLogger.v("switch between phone and ublox allowed");
            }
            sLogger.v(new StringBuilder().append("keepPhoneGpsOn[").append(this.keepPhoneGpsOn).append("] val[").append(i).append("]").toString());
            this.isDebugTTSEnabled = new java.io.File("/sdcard/debug_tts").exists();
            this.setUbloxAccuracyThreshold();
            this.setUbloxTimeDiscardThreshold();
        } catch(Throwable a0) {
            sLogger.e(a0);
        }
        this.locationManager = (LocationManager)a.getSystemService("location");
        boolean b = this.locationManager.isProviderEnabled("gps");
        android.location.LocationProvider a1 = null;
        if (b) {
            a1 = this.locationManager.getProvider("gps");
            if (a1 != null) {
                this.nmeaHandlerThread = new android.os.HandlerThread("navdynmea");
                this.nmeaHandlerThread.start();
                this.nmeaHandler = new Handler(this.nmeaHandlerThread.getLooper(), this.nmeaCallback);
                this.nmeaParser = new GpsNmeaParser(sLogger, this.nmeaHandler);
                this.locationManager.addGpsStatusListener(this.ubloxGpsStatusListener);
                this.locationManager.addNmeaListener(this.ubloxGpsNmeaListener);
            }
        }
        if (com.navdy.hud.app.util.DeviceUtil.isNavdyDevice()) {
            sLogger.i("[Gps-i] setting up ublox gps");
            if (a1 == null) {
                sLogger.e("[Gps-i] gps provider not found");
            } else {
                this.locationManager.requestLocationUpdates("gps", 0L, 0.0f, this.ubloxGpsLocationListener);
                try {
                    this.locationManager.requestLocationUpdates("NAVDY_GPS_PROVIDER", 0L, 0.0f, this.navdyGpsLocationListener);
                    sLogger.v("requestLocationUpdates successful for NAVDY_GPS_PROVIDER");
                } catch(Throwable a2) {
                    sLogger.e("requestLocationUpdates", a2);
                }
                sLogger.i("[Gps-i] ublox gps listeners installed");
            }
        } else {
            sLogger.i("[Gps-i] not a Hud device,not setting up ublox gps");
        }
        this.locationManager.addTestProvider("network", false, true, false, false, true, true, true, 0, 3);
        this.locationManager.setTestProviderEnabled("network", true);
        this.locationManager.setTestProviderStatus("network", 2, null, System.currentTimeMillis());
        sLogger.i("[Gps-i] added mock network provider");
        this.gpsManagerStartTime = android.os.SystemClock.elapsedRealtime();
        this.gpsLastEventTime = this.gpsManagerStartTime;
        com.navdy.hud.app.service.ConnectionServiceAnalyticsSupport.recordGpsAttemptAcquireLocation();
        if (com.navdy.hud.app.util.DeviceUtil.isNavdyDevice()) {
            this.setUbloxResetRunnable();
        }
        android.content.IntentFilter a3 = new android.content.IntentFilter();
        a3.addAction("GPS_DR_STARTED");
        a3.addAction("GPS_DR_STOPPED");
        a3.addAction("EXTRAPOLATION");
        a.registerReceiver(this.eventReceiver, a3);
    }

    private static Location androidLocationFromCoordinate(Coordinate a) {
        Location a0 = new Location("network");
        a0.setLatitude(a.latitude.doubleValue());
        a0.setLongitude(a.longitude.doubleValue());
        a0.setAccuracy(a.accuracy.floatValue());
        a0.setAltitude(a.altitude.doubleValue());
        a0.setBearing(a.bearing.floatValue());
        a0.setSpeed(a.speed.floatValue());
        a0.setTime(System.currentTimeMillis());
        a0.setElapsedRealtimeNanos(android.os.SystemClock.elapsedRealtimeNanos());
        return a0;
    }
    
    private void cancelUbloxResetRunnable() {
        this.handler.removeCallbacks(this.warmResetRunnable);
        this.warmResetCancelled = true;
    }
    
    private void checkGpsToPhoneAccuracy(Location a) {
        float f = a.getAccuracy();
        long j = android.os.SystemClock.elapsedRealtime();
        long j0 = this.lastPhoneCoordinateTime;
        if (sLogger.isLoggable(3)) {
            Logger a0 = sLogger;
            StringBuilder a1 = new StringBuilder().append("[Gps-loc-navdy] checkGpsToPhoneAccuracy: ublox[").append(a.getAccuracy()).append("] phone[");
            Object a2 = (this.lastPhoneCoordinate == null) ? "n/a" : this.lastPhoneCoordinate.accuracy;
            StringBuilder a3 = a1.append(a2).append("] lastPhoneTime [");
            Object a4 = (this.lastPhoneCoordinate == null) ? "n/a" : Long.valueOf(j - j0);
            a0.v(a3.append(a4).append("]").toString());
        }
        if (f != 0.0f) {
            if (this.activeLocationSource != LocationSource.UBLOX) {
                if (f + 2f < this.lastPhoneCoordinate.accuracy.floatValue()) {
                    sLogger.v(new StringBuilder().append("ublox accuracy [").append(f).append("] < phone accuracy[").append(this.lastPhoneCoordinate.accuracy).append("], start uBloxReporting, switch complete").toString());
                    this.firstSwitchToUbloxFromPhone = true;
                    this.markLocationFix("Switched to Ublox Gps", new StringBuilder().append("Phone =").append(this.lastPhoneCoordinate.accuracy).append(" ublox =").append(f).toString(), false, false);
                    this.startUbloxReporting();
                } else if (sLogger.isLoggable(3)) {
                    sLogger.v(new StringBuilder().append("ublox accuracy [").append(f).append("] > phone accuracy[").append(this.lastPhoneCoordinate.accuracy).append("] no action").toString());
                }
            } else if (this.lastPhoneCoordinate != null && android.os.SystemClock.elapsedRealtime() - this.lastPhoneCoordinateTime <= 3000L) {
                if (this.lastPhoneCoordinate.accuracy.floatValue() + 2f < f) {
                    if (sLogger.isLoggable(3)) {
                        sLogger.v(new StringBuilder().append("[Gps-loc-navdy]  ublox accuracy [").append(f).append("] > phone accuracy[").append(this.lastPhoneCoordinate.accuracy).append("]").toString());
                    }
                    if (this.startUbloxCalled) {
                        if (f <= 5f) {
                            sLogger.v(new StringBuilder().append("ublox accuracy [").append(f).append("] > phone accuracy[").append(this.lastPhoneCoordinate.accuracy).append("], but not above threshold").toString());
                            this.stopSendingLocation();
                        } else {
                            sLogger.v(new StringBuilder().append("ublox accuracy [").append(f).append("] > phone accuracy[").append(this.lastPhoneCoordinate.accuracy).append("], stop uBloxReporting").toString());
                            this.handler.removeCallbacks(this.locationFixRunnable);
                            this.handler.removeCallbacks(this.noPhoneLocationFixRunnable);
                            this.stopUbloxReporting();
                            sLogger.v(new StringBuilder().append("[Gps-loc] [LocationProvider] switched from [").append(this.activeLocationSource).append("] to [").append(LocationSource.PHONE).append("]").toString());
                            this.activeLocationSource = LocationSource.PHONE;
                            this.markLocationFix("Switched to Phone Gps", new StringBuilder().append("Phone =").append(this.lastPhoneCoordinate.accuracy).append(" ublox =").append(f).toString(), true, false);
                        }
                    }
                } else if (sLogger.isLoggable(3)) {
                    sLogger.v(new StringBuilder().append("ublox accuracy [").append(f).append("] < phone accuracy[").append(this.lastPhoneCoordinate.accuracy).append("] no action").toString());
                }
            }
        }
    }
    
    private void feedLocationToProvider(Location a, Coordinate a0) {
        try {
            if (sLogger.isLoggable(3)) {
                sLogger.d(new StringBuilder().append("[Phone-loc] ").append(a0).toString());
            }
            if (a == null) {
                a = GpsManager.androidLocationFromCoordinate(a0);
            }
            this.updateDrivingState(a);
            this.locationManager.setTestProviderLocation(a.getProvider(), a);
        } catch(Throwable a1) {
            sLogger.e("feedLocation", a1);
        }
    }
    
    public static GpsManager getInstance() {
        return singleton;
    }
    
    private void markLocationFix(String s, String s0, boolean b, boolean b0) {
        android.os.Bundle a = new android.os.Bundle();
        a.putString("title", s);
        a.putString("info", s0);
        a.putBoolean("phone", b);
        a.putBoolean("ublox", b0);
        this.sendGpsEventBroadcast("GPS_Switch", a);
    }
    
    private void processNmea(String s) {
        android.os.Message a = this.nmeaHandler.obtainMessage(1);
        a.obj = s;
        this.nmeaHandler.sendMessage(a);
    }
    
    private void sendGpsEventBroadcast(String s, android.os.Bundle a) {
        try {
            android.content.Intent a0 = new android.content.Intent(s);
            if (a != null) {
                a0.putExtras(a);
            }
            if (android.os.SystemClock.elapsedRealtime() - this.gpsManagerStartTime > 15000L) {
                android.content.Context a1 = com.navdy.hud.app.HudApplication.getAppContext();
                android.os.UserHandle a2 = android.os.Process.myUserHandle();
                if (this.queue.size() > 0) {
                    this.queueCallback.run();
                }
                a1.sendBroadcastAsUser(a0, a2);
            } else {
                this.queue.add(a0);
                this.handler.removeCallbacks(this.queueCallback);
                this.handler.postDelayed(this.queueCallback, 15000L);
            }
        } catch(Throwable a3) {
            sLogger.e(a3);
        }
    }
    
    private void sendSpeechRequest(com.navdy.service.library.events.audio.SpeechRequest a) {
        HudConnectionService a0 = this.connectionService;
        label0: {
            Throwable a1 = null;
            if (a0 == null) {
                break label0;
            }
            if (!this.connectionService.isConnected()) {
                break label0;
            }
            try {
                sLogger.i("[Gps-loc] send speech request");
                this.connectionService.sendMessage(a);
                break label0;
            } catch(Throwable a2) {
                a1 = a2;
            }
            sLogger.e(a1);
        }
    }
    
    private void setDriving(boolean b) {
        if (this.driving != b) {
            this.driving = b;
            this.sendGpsEventBroadcast(b ? "driving_started" : "driving_stopped", null);
        }
    }
    
    private void setUbloxAccuracyThreshold() {
        try {
            android.content.Context a = com.navdy.hud.app.HudApplication.getAppContext();
            android.os.UserHandle a0 = android.os.Process.myUserHandle();
            android.content.Intent a1 = new android.content.Intent("SET_UBLOX_ACCURACY");
            a1.putExtra("UBLOX_ACCURACY", 30);
            a.sendBroadcastAsUser(a1, a0);
            sLogger.v("setUbloxAccuracyThreshold:30");
        } catch(Throwable a2) {
            sLogger.e("setUbloxAccuracyThreshold", a2);
        }
    }
    
    private void setUbloxResetRunnable() {
        sLogger.v("setUbloxResetRunnable");
        this.handler.postDelayed(this.warmResetRunnable, WARM_RESET_INTERVAL);
    }
    
    private void setUbloxTimeDiscardThreshold() {
        try {
            android.content.Context a = com.navdy.hud.app.HudApplication.getAppContext();
            android.os.UserHandle a0 = android.os.Process.myUserHandle();
            android.content.Intent a1 = new android.content.Intent("SET_LOCATION_DISCARD_TIME_THRESHOLD");
            a1.putExtra("DISCARD_TIME", 2000);
            a.sendBroadcastAsUser(a1, a0);
            sLogger.v("setUbloxTimeDiscardThreshold:2000");
        } catch(Throwable a2) {
            sLogger.e("setUbloxTimeDiscardThreshold", a2);
        }
    }
    
    private void startSendingLocation() {
        HudConnectionService a = this.connectionService;
        label0: {
            Throwable a0 = null;
            if (a == null) {
                break label0;
            }
            if (!this.connectionService.isConnected()) {
                break label0;
            }
            try {
                sLogger.i("[Gps-loc] phone start sending location");
                this.connectionService.sendMessage(START_SENDING_LOCATION);
                break label0;
            } catch(Throwable a1) {
                a0 = a1;
            }
            sLogger.e(a0);
        }
    }
    
    private void stopSendingLocation() {
        boolean b = this.keepPhoneGpsOn;
        label1: {
            Throwable a = null;
            label0: if (b) {
                sLogger.v("stopSendingLocation: keeping phone gps on");
                break label1;
            } else if (this.isDebugTTSEnabled) {
                sLogger.v("stopSendingLocation: not stopping, debug_tts enabled");
                break label1;
            } else {
                if (this.connectionService == null) {
                    break label1;
                }
                if (!this.connectionService.isConnected()) {
                    break label1;
                }
                {
                    try {
                        sLogger.i("[Gps-loc] phone stop sending location");
                        this.connectionService.sendMessage(STOP_SENDING_LOCATION);
                    } catch(Throwable a0) {
                        a = a0;
                        break label0;
                    }
                    break label1;
                }
            }
            sLogger.e(a);
        }
    }
    
    private void updateDrivingState(Location a) {
        label2: if (this.driving) {
            label3: {
                if (a == null) {
                    break label3;
                }
                if ((double)a.getSpeed() < 0.22353333333333333) {
                    break label3;
                }
                this.transitioning = false;
                break label2;
            }
            if (this.transitioning) {
                if (android.os.SystemClock.elapsedRealtime() - this.transitionStartTime > 3000L) {
                    this.setDriving(false);
                    this.transitioning = false;
                }
            } else {
                this.transitioning = true;
                this.transitionStartTime = android.os.SystemClock.elapsedRealtime();
            }
        } else {
            label0: {
                label1: {
                    if (a == null) {
                        break label1;
                    }
                    if ((double)a.getSpeed() > 2.2353333333333336) {
                        break label0;
                    }
                }
                this.transitioning = false;
                break label2;
            }
            if (this.transitioning) {
                if (android.os.SystemClock.elapsedRealtime() - this.transitionStartTime > 3000L) {
                    this.setDriving(true);
                    this.transitioning = false;
                }
            } else {
                this.transitioning = true;
                this.transitionStartTime = android.os.SystemClock.elapsedRealtime();
            }
        }
        this.handler.removeCallbacks(this.noLocationUpdatesRunnable);
        if (this.driving) {
            this.handler.postDelayed(this.noLocationUpdatesRunnable, 3000L);
        }
    }
    
    public void feedLocation(Coordinate a) {
        boolean b = false;
        long j = this.lastPhoneCoordinateTime;
        int i = (j < 0L) ? -1 : (j == 0L) ? 0 : 1;
        label7: {
            label5: {
                label6: {
                    if (i > 0) {
                        break label6;
                    }
                    if (this.lastUbloxCoordinateTime <= 0L) {
                        break label5;
                    }
                }
                b = true;
                break label7;
            }
            b = false;
        }
        long j0 = System.currentTimeMillis();
        float f = a.accuracy.floatValue();
        int i0 = (f > 30f) ? 1 : (f == 30f) ? 0 : -1;
        label1: {
            label4: {
                if (i0 < 0) {
                    break label4;
                }
                if (!b) {
                    break label4;
                }
                this.inaccurateGpsCount = this.inaccurateGpsCount + 1L;
                this.gpsAccuracy = this.gpsAccuracy + a.accuracy.floatValue();
                long j1 = this.lastInaccurateGpsTime;
                int i1 = (j1 < 0L) ? -1 : (j1 == 0L) ? 0 : 1;
                label3: {
                    if (i1 == 0) {
                        break label3;
                    }
                    if (j0 - this.lastInaccurateGpsTime < INACCURATE_GPS_REPORT_INTERVAL) {
                        break label1;
                    }
                }
                float f0 = this.gpsAccuracy / (float)this.inaccurateGpsCount;
                sLogger.e(new StringBuilder().append("BAD gps accuracy (discarding) avg:").append(f0).append(", count=").append(this.inaccurateGpsCount).append(", last coord: ").append(a).toString());
                this.inaccurateGpsCount = 0L;
                this.gpsAccuracy = 0.0f;
                this.lastInaccurateGpsTime = j0;
                break label1;
            }
            long j2 = j0 - a.timestamp.longValue();
            int i2 = (j2 < 2000L) ? -1 : (j2 == 2000L) ? 0 : 1;
            label2: {
                if (i2 < 0) {
                    break label2;
                }
                if (!b) {
                    break label2;
                }
                sLogger.e(new StringBuilder().append("OLD location from phone(discard) time:").append(j2).append(", timestamp:").append(a.timestamp).append(" now:").append(j0).append(" lat:").append(a.latitude).append(" lng:").append(a.longitude).toString());
                break label1;
            }
            Coordinate a0 = this.lastPhoneCoordinate;
            label0: {
                if (a0 == null) {
                    break label0;
                }
                if (this.connectionService.getDevicePlatform() != com.navdy.service.library.events.DeviceInfo.Platform.PLATFORM_iOS) {
                    break label0;
                }
                if (this.lastPhoneCoordinate.latitude != a.latitude) {
                    break label0;
                }
                if (this.lastPhoneCoordinate.longitude != a.longitude) {
                    break label0;
                }
                if (this.lastPhoneCoordinate.accuracy != a.accuracy) {
                    break label0;
                }
                if (this.lastPhoneCoordinate.altitude != a.altitude) {
                    break label0;
                }
                if (this.lastPhoneCoordinate.bearing != a.bearing) {
                    break label0;
                }
                if (this.lastPhoneCoordinate.speed != a.speed) {
                    break label0;
                }
                sLogger.e(new StringBuilder().append("same location(discard) diff=").append(a.timestamp.longValue() - this.lastPhoneCoordinate.timestamp.longValue()).toString());
                break label1;
            }
            this.lastPhoneCoordinate = a;
            this.lastPhoneCoordinateTime = android.os.SystemClock.elapsedRealtime();
            this.handler.removeCallbacks(this.noPhoneLocationFixRunnable);
            com.navdy.hud.app.debug.RouteRecorder a1 = com.navdy.hud.app.debug.RouteRecorder.getInstance();
            boolean b0 = a1.isRecording();
            Location a2 = null;
            if (b0) {
                a2 = GpsManager.androidLocationFromCoordinate(a);
                a1.injectLocation(a2, true);
            }
            if (this.activeLocationSource != null) {
                if (this.activeLocationSource != LocationSource.PHONE) {
                    if (android.os.SystemClock.elapsedRealtime() - this.lastUbloxCoordinateTime > 3000L) {
                        sLogger.v(new StringBuilder().append("[Gps-loc] [LocationProvider] switched from [").append(this.activeLocationSource).append("] to [").append(LocationSource.PHONE).append("]").toString());
                        this.activeLocationSource = LocationSource.PHONE;
                        this.stopUbloxReporting();
                        this.feedLocationToProvider(a2, a);
                        this.markLocationFix("Switched to Phone Gps", new StringBuilder().append("Phone =").append(this.lastPhoneCoordinate.accuracy).append(" ublox =lost").toString(), true, false);
                    }
                } else {
                    this.feedLocationToProvider(a2, a);
                }
            } else {
                sLogger.v(new StringBuilder().append("[Gps-loc] [LocationProvider] switched from [").append(this.activeLocationSource).append("] to [").append(LocationSource.PHONE).append("]").toString());
                this.stopUbloxReporting();
                this.activeLocationSource = LocationSource.PHONE;
                this.feedLocationToProvider(a2, a);
                this.markLocationFix("Switched to Phone Gps", new StringBuilder().append("Phone =").append(this.lastPhoneCoordinate.accuracy).append(" ublox=n/a").toString(), true, false);
            }
        }
    }
    
    public void setConnectionService(HudConnectionService a) {
        this.connectionService = a;
    }
    
    public void shutdown() {
        if (this.locationManager != null) {
            this.locationManager.removeUpdates(this.ubloxGpsLocationListener);
            this.locationManager.removeUpdates(this.navdyGpsLocationListener);
            this.locationManager.removeGpsStatusListener(this.ubloxGpsStatusListener);
            this.locationManager.removeNmeaListener(this.ubloxGpsNmeaListener);
        }
    }
    
    public void startUbloxReporting() {
        try {
            android.content.Context a = com.navdy.hud.app.HudApplication.getAppContext();
            android.os.UserHandle a0 = android.os.Process.myUserHandle();
            a.sendBroadcastAsUser(new android.content.Intent(START_UBLOX), a0);
            this.startUbloxCalled = true;
            sLogger.v("started ublox reporting");
        } catch(Throwable a1) {
            sLogger.e("startUbloxReporting", a1);
        }
    }
    
    public void stopUbloxReporting() {
        try {
            this.startUbloxCalled = false;
            android.content.Context a = com.navdy.hud.app.HudApplication.getAppContext();
            android.os.UserHandle a0 = android.os.Process.myUserHandle();
            a.sendBroadcastAsUser(new android.content.Intent(STOP_UBLOX), a0);
            sLogger.v("stopped ublox reporting");
        } catch(Throwable a1) {
            sLogger.e("stopUbloxReporting", a1);
        }
    }

    public enum LocationSource {
        UBLOX(0),
        PHONE(1);

        private int value;
        LocationSource(int value) {
            this.value = value;
        }
        public int getValue() {
            return value;
        }
    }
}
