package com.navdy.hud.app.maps;

public class MapEvents$LiveTrafficEvent {
    final public com.navdy.hud.app.maps.MapEvents$LiveTrafficEvent$Severity severity;
    final public com.navdy.hud.app.maps.MapEvents$LiveTrafficEvent$Type type;
    
    public MapEvents$LiveTrafficEvent(com.navdy.hud.app.maps.MapEvents$LiveTrafficEvent$Type a, com.navdy.hud.app.maps.MapEvents$LiveTrafficEvent$Severity a0) {
        this.type = a;
        this.severity = a0;
    }
}
