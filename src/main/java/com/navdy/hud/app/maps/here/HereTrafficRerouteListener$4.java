package com.navdy.hud.app.maps.here;

class HereTrafficRerouteListener$4 implements Runnable {
    final com.navdy.hud.app.maps.here.HereTrafficRerouteListener this$0;
    final com.here.android.mpa.routing.Route val$fasterRoute;
    final boolean val$notifFromHereTraffic;
    
    HereTrafficRerouteListener$4(com.navdy.hud.app.maps.here.HereTrafficRerouteListener a, boolean b, com.here.android.mpa.routing.Route a0) {

        super();
        this.this$0 = a;
        this.val$notifFromHereTraffic = b;
        this.val$fasterRoute = a0;
    }
    
    public void run() {
        label1: try {
            String s = (this.val$notifFromHereTraffic) ? "[Here Traffic]" : "[Navdy Calc]";
            com.navdy.hud.app.maps.here.HereTrafficRerouteListener.access$100(this.this$0).v(new StringBuilder().append(com.navdy.hud.app.maps.here.HereTrafficRerouteListener.access$500(this.this$0)).append(s).append(" onTrafficRerouted").toString());
            this.this$0.dismissReroute();
            if (com.navdy.hud.app.maps.here.HereTrafficRerouteListener.access$300(this.this$0).isNavigationModeOn()) {
                if (com.navdy.hud.app.maps.here.HereTrafficRerouteListener.access$300(this.this$0).hasArrived()) {
                    com.navdy.hud.app.maps.here.HereTrafficRerouteListener.access$100(this.this$0).v("onTrafficRerouted, arrival mode on");
                } else if (com.navdy.hud.app.maps.here.HereTrafficRerouteListener.access$300(this.this$0).isOnGasRoute()) {
                    com.navdy.hud.app.maps.here.HereTrafficRerouteListener.access$100(this.this$0).v("onTrafficRerouted, on fas route");
                } else {
                    com.here.android.mpa.routing.Route a = com.navdy.hud.app.maps.here.HereTrafficRerouteListener.access$300(this.this$0).getCurrentRoute();
                    if (a != null) {
                        java.util.List a0 = a.getManeuvers();
                        if (this.val$notifFromHereTraffic) {
                            String s0 = com.navdy.hud.app.maps.here.HereMapUtil.getAllRoadNames(this.val$fasterRoute.getManeuvers(), 0);
                            String s1 = com.navdy.hud.app.maps.here.HereMapUtil.getAllRoadNames(a0, 0);
                            com.navdy.hud.app.maps.here.HereTrafficRerouteListener.access$100(this.this$0).v("[HereNotif-RoadNames-current]");
                            com.navdy.hud.app.maps.here.HereTrafficRerouteListener.access$100(this.this$0).v(s0);
                            com.navdy.hud.app.maps.here.HereTrafficRerouteListener.access$100(this.this$0).v("[HereNotif-RoadNames-faster]");
                            com.navdy.hud.app.maps.here.HereTrafficRerouteListener.access$100(this.this$0).v(s1);
                        }
                        com.here.android.mpa.routing.RouteTta a1 = this.val$fasterRoute.getTta(com.here.android.mpa.routing.Route.TrafficPenaltyMode.OPTIMAL, 268435455);
                        com.here.android.mpa.routing.RouteTta a2 = this.val$fasterRoute.getTta(com.here.android.mpa.routing.Route.TrafficPenaltyMode.DISABLED, 268435455);
                        label2: {
                            label3: {
                                if (a1 == null) {
                                    break label3;
                                }
                                if (a2 != null) {
                                    break label2;
                                }
                            }
                            com.navdy.hud.app.maps.here.HereTrafficRerouteListener.access$100(this.this$0).v(new StringBuilder().append(com.navdy.hud.app.maps.here.HereTrafficRerouteListener.access$500(this.this$0)).append("error retrieving navigation and TTA").toString());
                            break label1;
                        }
                        boolean b = this.val$notifFromHereTraffic;
                        label0: {
                            if (b) {
                                break label0;
                            }
                            long j = (long)a1.getDuration();
                            if (j != (long)a2.getDuration()) {
                                break label0;
                            }
                            com.navdy.hud.app.maps.here.HereTrafficRerouteListener.access$100(this.this$0).v(new StringBuilder().append(com.navdy.hud.app.maps.here.HereTrafficRerouteListener.access$500(this.this$0)).append("faster route durations is same=").append(j).toString());
                            break label1;
                        }
                        com.here.android.mpa.routing.RouteTta a3 = a.getTta(com.here.android.mpa.routing.Route.TrafficPenaltyMode.OPTIMAL, 268435455);
                        long j0 = (a3 == null) ? 0L : (long)a3.getDuration();
                        java.util.Date a4 = com.navdy.hud.app.maps.here.HereTrafficRerouteListener.access$000(this.this$0).getEta(true, com.here.android.mpa.routing.Route.TrafficPenaltyMode.OPTIMAL);
                        if (com.navdy.hud.app.maps.here.HereMapUtil.isValidEtaDate(a4)) {
                            long j1 = a4.getTime();
                            long j2 = System.currentTimeMillis();
                            long j3 = (j1 - j2) / 1000L;
                            long j4 = (long)a1.getDuration();
                            long j5 = j3 - j4;
                            com.navdy.hud.app.maps.here.HereTrafficRerouteListener.access$100(this.this$0).i(new StringBuilder().append("etaDate[").append(a4).append("] etaUtc[").append(j1).append("] currentTta[").append(j3).append("] now[").append(j2).append("] newTta[").append(j4).append("]").append("] diff[").append(j5).append("]").append("] oldTtaDuration[").append(j0).append("]").toString());
                            java.util.HashSet a5 = new java.util.HashSet();
                            java.util.ArrayList a6 = new java.util.ArrayList(1);
                            java.util.List a7 = com.navdy.hud.app.maps.here.HereMapUtil.getDifferentManeuver(this.val$fasterRoute, a, 2, (java.util.List)a6);
                            if (a7.size() != 0) {
                                StringBuilder a8 = new StringBuilder();
                                java.util.Iterator a9 = a7.iterator();
                                String s2 = null;
                                Object a10 = a0;
                                Object a11 = a7;
                                Object a12 = a9;
                                while(true) {
                                    boolean b0 = ((java.util.Iterator)a12).hasNext();
                                    String s3 = s2;
                                    if (!b0) {
                                        break;
                                    }
                                    s2 = com.navdy.hud.app.maps.here.HereMapUtil.getNextRoadName((com.here.android.mpa.routing.Maneuver)((java.util.Iterator)a12).next());
                                    if (s3 != null) {
                                        if (a5.contains(s2)) {
                                            s2 = s3;
                                        } else {
                                            a8.append(",");
                                            a8.append(" ");
                                            a8.append(s2);
                                            a5.add(s2);
                                            s2 = s3;
                                        }
                                    } else {
                                        a8.append(s2);
                                        a5.add(s2);
                                    }
                                }
                                String s4 = com.navdy.hud.app.maps.here.HereRouteViaGenerator.getViaString(this.val$fasterRoute);
                                if (!a5.contains(s4)) {
                                    a8.append(",");
                                    a8.append(" ");
                                    a8.append(s4);
                                }
                                com.navdy.hud.app.maps.here.HereRouteCache$RouteInfo a13 = com.navdy.hud.app.maps.here.HereRouteCache.getInstance().getRoute(com.navdy.hud.app.maps.here.HereTrafficRerouteListener.access$300(this.this$0).getCurrentRouteId());
                                String s5 = null;
                                if (a13 != null) {
                                    s5 = a13.routeResult.via;
                                }
                                String s6 = a8.toString();
                                long j6 = (long)this.val$fasterRoute.getLength() - com.navdy.hud.app.maps.here.HereTrafficRerouteListener.access$000(this.this$0).getDestinationDistance();
                                int i = this.this$0.getRerouteMinDuration();
                                com.navdy.hud.app.maps.here.HereTrafficRerouteListener.access$100(this.this$0).v(new StringBuilder().append(com.navdy.hud.app.maps.here.HereTrafficRerouteListener.access$500(this.this$0)).append(s).append(" new route via[").append(s2).append("] additionalVia[").append(s6).append("] is faster by[").append(j5).append("] seconds than current route via[").append(s5).append("] distanceDiff [").append(j6).append("] minDur[").append(i).append("]").toString());
                                if (j5 < (long)i) {
                                    com.navdy.hud.app.maps.here.HereTrafficRerouteListener.access$100(this.this$0).v(new StringBuilder().append("faster route is below threshold:").append(j5).toString());
                                    if (com.navdy.hud.app.maps.here.HereMapsManager.getInstance().isRecalcCurrentRouteForTrafficEnabled()) {
                                        if (com.navdy.hud.app.maps.here.HereTrafficRerouteListener.access$600(this.this$0, (java.util.List)a10, (java.util.List)a6, (java.util.List)a11)) {
                                            com.navdy.hud.app.maps.here.HereTrafficRerouteListener.access$700(this.this$0).reCalculateCurrentRoute(a, this.val$fasterRoute, this.val$notifFromHereTraffic, s2, j5, s6, j1, j6, s5, a1);
                                        } else {
                                            com.navdy.hud.app.maps.here.HereTrafficRerouteListener.access$100(this.this$0).v("faster route not near by, threshold not met");
                                        }
                                    } else {
                                        this.this$0.fasterRouteNotFound(this.val$notifFromHereTraffic, j5, s2, s5);
                                    }
                                } else {
                                    com.navdy.hud.app.maps.here.HereTrafficRerouteListener.access$100(this.this$0).v(new StringBuilder().append("faster route is above threshold:").append(j5).toString());
                                    if (com.navdy.hud.app.maps.here.HereTrafficRerouteListener.access$600(this.this$0, (java.util.List)a10, (java.util.List)a6, (java.util.List)a11)) {
                                        this.this$0.setFasterRoute(this.val$fasterRoute, this.val$notifFromHereTraffic, s2, j5, s6, j1, j6, 0L);
                                    } else {
                                        com.navdy.hud.app.maps.here.HereTrafficRerouteListener.access$100(this.this$0).v("faster route not near by, threshold not met");
                                    }
                                }
                            } else {
                                com.navdy.hud.app.maps.here.HereTrafficRerouteListener.access$100(this.this$0).i("could not find a difference in maneuvers");
                            }
                        } else {
                            com.navdy.hud.app.maps.here.HereTrafficRerouteListener.access$100(this.this$0).v(new StringBuilder().append(com.navdy.hud.app.maps.here.HereTrafficRerouteListener.access$500(this.this$0)).append("error retrieving eta for current").toString());
                        }
                    } else {
                        com.navdy.hud.app.maps.here.HereTrafficRerouteListener.access$100(this.this$0).v(new StringBuilder().append(com.navdy.hud.app.maps.here.HereTrafficRerouteListener.access$500(this.this$0)).append(" onTrafficRerouted, not current route").toString());
                    }
                }
            } else {
                com.navdy.hud.app.maps.here.HereTrafficRerouteListener.access$100(this.this$0).v(new StringBuilder().append(com.navdy.hud.app.maps.here.HereTrafficRerouteListener.access$500(this.this$0)).append(" onTrafficRerouted, not currently navigating").toString());
            }
        } catch(Throwable a14) {
            com.navdy.hud.app.maps.here.HereTrafficRerouteListener.access$100(this.this$0).e(a14);
            com.navdy.hud.app.util.CrashReporter.getInstance().reportNonFatalException(a14);
        }
    }
}
