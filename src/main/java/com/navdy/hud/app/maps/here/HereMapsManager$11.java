package com.navdy.hud.app.maps.here;
import static com.navdy.hud.app.maps.here.HereMapsManager.sLogger;

class HereMapsManager$11 extends android.content.BroadcastReceiver {
    final com.navdy.hud.app.maps.here.HereMapsManager this$0;
    
    HereMapsManager$11(com.navdy.hud.app.maps.here.HereMapsManager a) {

        super();
        this.this$0 = a;
    }
    
    public void onReceive(android.content.Context a, android.content.Intent a0) {
        if ("android.net.conn.CONNECTIVITY_CHANGE".equals(a0.getAction())) {
            sLogger.v("setEngineOnlineState n/w state change");
            this.this$0.setEngineOnlineState();
        }
    }
}
