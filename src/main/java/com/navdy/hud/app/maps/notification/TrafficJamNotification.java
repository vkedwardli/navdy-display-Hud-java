package com.navdy.hud.app.maps.notification;
import com.navdy.hud.app.R;

public class TrafficJamNotification extends com.navdy.hud.app.maps.notification.BaseTrafficNotification {
    final public static float REGULAR_SIZE = 64f;
    final public static float SMALL_SIZE = 46f;
    final private static int TAG_DISMISS = 1;
    private static String delay;
    private static String dismiss;
    private static java.util.List dismissChoices;
    private static String hr;
    private static String min;
    private static String trafficJam;
    private com.squareup.otto.Bus bus;
    private com.navdy.hud.app.ui.component.ChoiceLayout2.IListener choiceListener;
    private android.view.ViewGroup container;
    private com.navdy.hud.app.view.Gauge gauge;
    private int initialRemainingTime;
    private int notifColor;
    private android.widget.TextView title;
    private android.widget.TextView title1;
    private android.widget.TextView title2;
    private android.widget.TextView title3;
    private com.navdy.hud.app.maps.MapEvents$TrafficJamProgressEvent trafficJamEvent;
    
    static {
        dismissChoices = (java.util.List)new java.util.ArrayList(1);
    }
    
    public TrafficJamNotification(com.squareup.otto.Bus a, com.navdy.hud.app.maps.MapEvents$TrafficJamProgressEvent a0) {
        this.choiceListener = (com.navdy.hud.app.ui.component.ChoiceLayout2.IListener)new com.navdy.hud.app.maps.notification.TrafficJamNotification$1(this);
        if (trafficJam == null) {
            android.content.res.Resources a1 = com.navdy.hud.app.HudApplication.getAppContext().getResources();
            trafficJam = a1.getString(R.string.traffic_jam_title);
            delay = a1.getString(R.string.traffic_notification_delay);
            min = a1.getString(R.string.min);
            hr = a1.getString(R.string.hr);
            dismiss = a1.getString(R.string.traffic_notification_dismiss);
            int i = a1.getColor(R.color.glance_dismiss);
            dismissChoices.add(new com.navdy.hud.app.ui.component.ChoiceLayout2.Choice(1, R.drawable.icon_glances_dismiss, i, R.drawable.icon_glances_dismiss, -16777216, dismiss, i));
            this.notifColor = a1.getColor(R.color.traffic_bad);
        }
        this.bus = a;
        this.initialRemainingTime = a0.remainingTime;
        this.logger.v(new StringBuilder().append("TrafficJamNotification: initialRemainingTime=").append(a0.remainingTime).toString());
    }
    
    static com.navdy.hud.app.maps.MapEvents$TrafficJamProgressEvent access$002(com.navdy.hud.app.maps.notification.TrafficJamNotification a, com.navdy.hud.app.maps.MapEvents$TrafficJamProgressEvent a0) {
        a.trafficJamEvent = a0;
        return a0;
    }
    
    private void updateState() {
        if (this.trafficJamEvent != null) {
            this.title.setText((CharSequence)trafficJam);
            this.title1.setText((CharSequence)delay);
            if (this.trafficJamEvent.remainingTime < 3600) {
                this.title2.setTextSize(2, 64f);
                this.title3.setText((CharSequence)min);
            } else {
                this.title2.setTextSize(2, 46f);
                this.title3.setText((CharSequence)hr);
            }
            String s = com.navdy.hud.app.maps.util.MapUtils.formatTime(this.trafficJamEvent.remainingTime);
            this.title2.setText((CharSequence)s);
            this.gauge.setValue(this.trafficJamEvent.remainingTime);
            this.logger.v(new StringBuilder().append("TrafficJamNotification: trafficJamEvent.remainingTime=").append(this.trafficJamEvent.remainingTime).toString());
            this.choiceLayout.setChoices(dismissChoices, 0, this.choiceListener, 0.5f);
        }
    }
    
    public boolean canAddToStackIfCurrentExists() {
        return true;
    }
    
    public boolean expandNotification() {
        return false;
    }
    
    public int getColor() {
        return this.notifColor;
    }
    
    public android.view.View getExpandedView(android.content.Context a, Object a0) {
        return null;
    }
    
    public int getExpandedViewIndicatorColor() {
        return 0;
    }
    
    public String getId() {
        return "navdy#traffic#jam#notif";
    }
    
    public int getTimeout() {
        return 0;
    }
    
    public com.navdy.hud.app.framework.notifications.NotificationType getType() {
        return com.navdy.hud.app.framework.notifications.NotificationType.TRAFFIC_JAM;
    }
    
    public android.view.View getView(android.content.Context a) {
        if (this.container == null) {
            this.container = (android.view.ViewGroup)android.view.LayoutInflater.from(a).inflate(R.layout.notification_traffic_jam, (android.view.ViewGroup)null);
            this.title = (android.widget.TextView)this.container.findViewById(R.id.title);
            this.title1 = (android.widget.TextView)this.container.findViewById(R.id.title1);
            this.title2 = (android.widget.TextView)this.container.findViewById(R.id.title2);
            this.title3 = (android.widget.TextView)this.container.findViewById(R.id.title3);
            this.gauge = (com.navdy.hud.app.view.Gauge)this.container.findViewById(R.id.traffic_jam_progress);
            this.gauge.setMaxValue(this.initialRemainingTime);
            this.choiceLayout = (com.navdy.hud.app.ui.component.ChoiceLayout2)this.container.findViewById(R.id.choiceLayout);
        }
        return this.container;
    }
    
    public android.animation.AnimatorSet getViewSwitchAnimation(boolean b) {
        return null;
    }
    
    public boolean isAlive() {
        return true;
    }
    
    public boolean isPurgeable() {
        return false;
    }
    
    public void onExpandedNotificationEvent(com.navdy.hud.app.ui.framework.UIStateManager.Mode a) {
    }
    
    public void onExpandedNotificationSwitched() {
    }
    
    public void onNotificationEvent(com.navdy.hud.app.ui.framework.UIStateManager.Mode a) {
    }
    
    public void onStart(com.navdy.hud.app.framework.notifications.INotificationController a) {
        super.onStart(a);
        this.updateState();
    }
    
    public void onStop() {
        super.onStop();
    }
    
    public void onUpdate() {
        this.updateState();
    }
    
    public void setTrafficEvent(com.navdy.hud.app.maps.MapEvents$TrafficJamProgressEvent a) {
        this.trafficJamEvent = a;
    }
}
