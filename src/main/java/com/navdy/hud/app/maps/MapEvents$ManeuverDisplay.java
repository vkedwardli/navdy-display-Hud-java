package com.navdy.hud.app.maps;
import com.navdy.hud.app.R;

public class MapEvents$ManeuverDisplay {
    final static String NOT_AVAILABLE = "N/A";
    public String currentRoad;
    public float currentSpeedLimit;
    public int destinationIconId;
    public com.navdy.hud.app.maps.MapEvents$DestinationDirection direction;
    public float distance;
    public long distanceInMeters;
    public String distanceToPendingRoadText;
    public com.navdy.service.library.events.navigation.DistanceUnit distanceUnit;
    public boolean empty;
    public String eta;
    public String etaAmPm;
    public java.util.Date etaDate;
    public String maneuverId;
    public com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder$ManeuverState maneuverState;
    public com.navdy.service.library.events.navigation.NavigationTurn navigationTurn;
    public int nextTurnIconId;
    public String pendingRoad;
    public String pendingTurn;
    public float totalDistance;
    public float totalDistanceRemaining;
    public com.navdy.service.library.events.navigation.DistanceUnit totalDistanceRemainingUnit;
    public com.navdy.service.library.events.navigation.DistanceUnit totalDistanceUnit;
    public int turnIconId;
    public int turnIconNowId;
    public int turnIconSoonId;
    
    public MapEvents$ManeuverDisplay() {
        this.turnIconId = -1;
        this.nextTurnIconId = -1;
    }
    
    public boolean isArrived() {
        return this.turnIconId == R.drawable.icon_tbt_arrive;
    }
    
    public boolean isEmpty() {
        return this.empty;
    }
    
    public boolean isNavigating() {
        return this.turnIconId != -1;
    }
    
    public String toString() {
        String s = null;
        int i = this.turnIconId;
        label0: {
            Throwable a = null;
            if (i == -1) {
                s = "N/A";
                break label0;
            } else {
                try {
                    s = com.navdy.hud.app.HudApplication.getAppContext().getResources().getResourceName(this.turnIconId);
                    break label0;
                } catch(Throwable a0) {
                    a = a0;
                }
            }
            com.navdy.hud.app.maps.MapEvents.access$000().e(a);
            s = "N/A";
        }
        StringBuilder a1 = new StringBuilder().append("TurnIcon [").append(s).append("] ").append("TurnIcon [").append(this.turnIconId).append("] ").append("PendingTurn[").append(this.pendingTurn).append("] ").append("PendingRoad[").append(this.pendingRoad).append("] ").append("Distanceleft[").append(this.distanceToPendingRoadText).append("] ").append("DistanceleftInMeters[").append(this.distanceInMeters).append("] ").append("CurrentRoad[").append(this.currentRoad).append("] ").append("eta[").append(this.eta).append("] ").append("etaUtc[");
        return a1.append(this.etaDate).append("] ").append("currentSpeedLimit[").append(this.currentSpeedLimit).append("] ").append("earlyManeuver[").append((this.nextTurnIconId != -1) ? "available] " : "not_available] ").append("destinationDirection[").append(this.direction).append("] ").append("totalDistance [").append(this.totalDistance).append(" ").append(this.totalDistanceUnit).append("] ").append("totalDistanceRemain [").append(this.totalDistanceRemaining).append(" ").append(this.totalDistanceRemainingUnit).append("] ").append("empty[").append(this.empty).append("] ").append("navigating[").append(this.isNavigating()).append("] ").append("arrived [").append(this.isArrived()).append("] ").toString();
    }
}
