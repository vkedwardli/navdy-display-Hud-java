package com.navdy.hud.app.maps.here;

class HereLocationFixManager$8 implements Runnable {
    final com.navdy.hud.app.maps.here.HereLocationFixManager this$0;
    final com.here.android.mpa.common.GeoPosition val$geoPosition;
    final boolean val$isMapMatched;
    final com.here.android.mpa.common.PositioningManager.LocationMethod val$locationMethod;
    
    HereLocationFixManager$8(com.navdy.hud.app.maps.here.HereLocationFixManager a, com.here.android.mpa.common.GeoPosition a0, com.here.android.mpa.common.PositioningManager.LocationMethod a1, boolean b) {
        super();
        this.this$0 = a;
        this.val$geoPosition = a0;
        this.val$locationMethod = a1;
        this.val$isMapMatched = b;
    }
    
    public void run() {
        try {
            String s = null;
            com.here.android.mpa.common.GeoCoordinate a = this.val$geoPosition.getCoordinate();
            if (this.val$geoPosition instanceof com.here.android.mpa.common.MatchedGeoPosition) {
                com.here.android.mpa.common.MatchedGeoPosition a0 = (com.here.android.mpa.common.MatchedGeoPosition)this.val$geoPosition;
                s = new StringBuilder().append(",").append(a0.getMatchQuality()).append(",").append(a0.isExtrapolated()).append(",").append(a0.isOnStreet()).toString();
            } else {
                s = "";
            }
            String s0 = new StringBuilder().append(a.getLatitude()).append(",").append(a.getLongitude()).append(",").append(this.val$geoPosition.getHeading()).append(",").append(this.val$geoPosition.getSpeed()).append(",").append(this.val$geoPosition.getLatitudeAccuracy()).append(",").append(a.getAltitude()).append(",").append(this.val$geoPosition.getTimestamp().getTime()).append(",").append("here").append(",").append(this.val$locationMethod.name()).append(",").append(this.val$isMapMatched).append(s).append("\n").toString();
            com.navdy.hud.app.maps.here.HereLocationFixManager.access$1500(this.this$0).write(s0.getBytes());
        } catch(Throwable a1) {
            com.navdy.hud.app.maps.here.HereLocationFixManager.access$200().e(a1);
        }
    }
}
