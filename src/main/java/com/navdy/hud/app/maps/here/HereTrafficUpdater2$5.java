package com.navdy.hud.app.maps.here;

class HereTrafficUpdater2$5 {
    final static int[] $SwitchMap$com$here$android$mpa$guidance$TrafficUpdater$Error;
    final static int[] $SwitchMap$com$here$android$mpa$mapping$TrafficEvent$Severity;
    
    static {
        $SwitchMap$com$here$android$mpa$mapping$TrafficEvent$Severity = new int[com.here.android.mpa.mapping.TrafficEvent.Severity.values().length];
        int[] a = $SwitchMap$com$here$android$mpa$mapping$TrafficEvent$Severity;
        com.here.android.mpa.mapping.TrafficEvent.Severity a0 = com.here.android.mpa.mapping.TrafficEvent.Severity.BLOCKING;
        try {
            a[a0.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException) {
        }
        try {
            $SwitchMap$com$here$android$mpa$mapping$TrafficEvent$Severity[com.here.android.mpa.mapping.TrafficEvent.Severity.VERY_HIGH.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException0) {
        }
        try {
            $SwitchMap$com$here$android$mpa$mapping$TrafficEvent$Severity[com.here.android.mpa.mapping.TrafficEvent.Severity.HIGH.ordinal()] = 3;
        } catch(NoSuchFieldError ignoredException1) {
        }
        $SwitchMap$com$here$android$mpa$guidance$TrafficUpdater$Error = new int[com.here.android.mpa.guidance.TrafficUpdater.Error.values().length];
        int[] a1 = $SwitchMap$com$here$android$mpa$guidance$TrafficUpdater$Error;
        com.here.android.mpa.guidance.TrafficUpdater.Error a2 = com.here.android.mpa.guidance.TrafficUpdater.Error.NONE;
        try {
            a1[a2.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException2) {
        }
    }
}
