package com.navdy.hud.app.maps.here;
import static com.navdy.hud.app.maps.here.HereMapsManager.sLogger;


class HereMapsManager$3$2 implements Runnable {
    final com.navdy.hud.app.maps.here.HereMapsManager$3 this$1;
    private final com.here.android.mpa.common.PositioningManager.LocationMethod val$locationMethod;
    private final com.here.android.mpa.common.PositioningManager.LocationStatus val$locationStatus;
    
    HereMapsManager$3$2(com.navdy.hud.app.maps.here.HereMapsManager$3 a, com.here.android.mpa.common.PositioningManager.LocationMethod a0, com.here.android.mpa.common.PositioningManager.LocationStatus a1) {
        super();
        this.this$1 = a;
        this.val$locationMethod = a0;
        this.val$locationStatus = a1;
    }
    
    public void run() {
        try {
            if (sLogger.isLoggable(2)) {
                sLogger.d("onPositionFixChanged: method:" + this.val$locationMethod + " status: " + this.val$locationStatus);
            }
            if (this.val$locationMethod == com.here.android.mpa.common.PositioningManager.LocationMethod.GPS && com.navdy.hud.app.maps.here.HereMapUtil.isInTunnel(this.this$1.this$0.positioningManager.getRoadElement()) && !com.navdy.hud.app.maps.here.HereMapsManager.access$2800(this.this$1.this$0)) {
                sLogger.d("TUNNEL extrapolation on");
                com.navdy.hud.app.maps.here.HereMapsManager.access$2802(this.this$1.this$0, true);
                com.navdy.hud.app.maps.here.HereMapsManager.access$2900(this.this$1.this$0);
            }
        } catch(Throwable a) {
            HereMapsManager.sLogger.e(a);
        }
    }
}
