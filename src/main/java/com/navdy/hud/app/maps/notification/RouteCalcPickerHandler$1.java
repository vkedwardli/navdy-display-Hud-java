package com.navdy.hud.app.maps.notification;

class RouteCalcPickerHandler$1 implements Runnable {
    final com.navdy.hud.app.maps.notification.RouteCalcPickerHandler this$0;
    final com.navdy.service.library.events.navigation.NavigationRouteResult val$result;
    final com.navdy.hud.app.maps.here.HereRouteCache$RouteInfo val$routeInfo;
    
    RouteCalcPickerHandler$1(com.navdy.hud.app.maps.notification.RouteCalcPickerHandler a, com.navdy.service.library.events.navigation.NavigationRouteResult a0, com.navdy.hud.app.maps.here.HereRouteCache$RouteInfo a1) {

        super();
        this.this$0 = a;
        this.val$result = a0;
        this.val$routeInfo = a1;
    }
    
    public void run() {
        com.navdy.hud.app.maps.here.HereNavigationManager a = com.navdy.hud.app.maps.here.HereNavigationManager.getInstance();
        boolean b = a.isTrafficConsidered(this.val$result.routeId);
        a.setReroute(this.val$routeInfo.route, com.navdy.service.library.events.navigation.NavigationSessionRouteChange.RerouteReason.NAV_SESSION_ROUTE_PICKER, this.val$result.routeId, this.val$result.via, true, b);
        if (a.getNavigationSessionPreference().spokenTurnByTurn) {
            String s = com.navdy.hud.app.maps.here.HereRouteManager.buildChangeRouteTTS(this.val$result.routeId);
            com.navdy.hud.app.maps.notification.RouteCalcPickerHandler.access$000().v(new StringBuilder().append("tts[").append(s).append("]").toString());
            com.squareup.otto.Bus a0 = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getBus();
            a0.post(com.navdy.hud.app.maps.notification.RouteCalculationNotification.CANCEL_TBT_TTS);
            a0.post(com.navdy.hud.app.maps.notification.RouteCalculationNotification.CANCEL_CALC_TTS);
            com.navdy.hud.app.framework.voice.TTSUtils.sendSpeechRequest(s, com.navdy.service.library.events.audio.SpeechRequest.Category.SPEECH_TURN_BY_TURN, com.navdy.hud.app.maps.notification.RouteCalculationNotification.ROUTE_CALC_TTS_ID);
        }
        com.navdy.hud.app.maps.notification.RouteCalcPickerHandler.access$000().w(new StringBuilder().append("new route set: ").append(this.val$routeInfo.routeRequest).toString());
    }
}
