package com.navdy.hud.app.maps.here;

class HereTrafficUpdater$JamTrackEvent {
    final public int remainingTimeInJam;
    final public com.here.android.mpa.routing.Route route;
    final com.navdy.hud.app.maps.here.HereTrafficUpdater this$0;
    final public com.here.android.mpa.mapping.TrafficEvent trafficEvent;
    
    public HereTrafficUpdater$JamTrackEvent(com.navdy.hud.app.maps.here.HereTrafficUpdater a, com.here.android.mpa.routing.Route a0, com.here.android.mpa.mapping.TrafficEvent a1, int i) {

        super();
        this.this$0 = a;
        this.route = a0;
        this.trafficEvent = a1;
        this.remainingTimeInJam = i;
    }
}
