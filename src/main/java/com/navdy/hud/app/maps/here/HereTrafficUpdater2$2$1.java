package com.navdy.hud.app.maps.here;

class HereTrafficUpdater2$2$1 implements Runnable {
    final com.navdy.hud.app.maps.here.HereTrafficUpdater2$2 this$1;
    final java.util.List val$list;
    final com.here.android.mpa.routing.Route val$r;
    
    HereTrafficUpdater2$2$1(com.navdy.hud.app.maps.here.HereTrafficUpdater2$2 a, com.here.android.mpa.routing.Route a0, java.util.List a1) {

        super();
        this.this$1 = a;
        this.val$r = a0;
        this.val$list = a1;
    }
    
    public void run() {
        if (com.navdy.hud.app.maps.here.HereTrafficUpdater2.access$100(this.this$1.this$0) != null) {
            long j = com.navdy.hud.app.maps.here.HereNavigationManager.getInstance().getNavController().getDestinationDistance();
            com.navdy.hud.app.maps.here.HereTrafficUpdater2.access$900(this.this$1.this$0, this.val$r, this.val$list, j);
        } else {
            com.navdy.hud.app.maps.here.HereTrafficUpdater2.access$000().w("no current route, cannot process traffic events");
        }
    }
}
