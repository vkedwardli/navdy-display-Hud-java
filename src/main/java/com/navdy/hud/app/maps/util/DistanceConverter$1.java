package com.navdy.hud.app.maps.util;
import com.navdy.hud.app.manager.SpeedManager;


class DistanceConverter$1 {
    final static int[] $SwitchMap$com$navdy$hud$app$manager$SpeedManager$SpeedUnit;
    final static int[] $SwitchMap$com$navdy$service$library$events$navigation$DistanceUnit;
    
    static {
        $SwitchMap$com$navdy$hud$app$manager$SpeedManager$SpeedUnit = new int[com.navdy.hud.app.manager.SpeedManager.SpeedUnit.values().length];
        SpeedManager.SpeedUnit a0 = SpeedManager.SpeedUnit.MILES_PER_HOUR;
        try {
            $SwitchMap$com$navdy$hud$app$manager$SpeedManager$SpeedUnit[a0.ordinal()] = 0;
        } catch(NoSuchFieldError ignored) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$manager$SpeedManager$SpeedUnit[SpeedManager.SpeedUnit.KILOMETERS_PER_HOUR.ordinal()] = 1;
        } catch(NoSuchFieldError ignored) {
        }
        $SwitchMap$com$navdy$service$library$events$navigation$DistanceUnit = new int[com.navdy.service.library.events.navigation.DistanceUnit.values().length];
        com.navdy.service.library.events.navigation.DistanceUnit a2 = com.navdy.service.library.events.navigation.DistanceUnit.DISTANCE_METERS;
        try {
            $SwitchMap$com$navdy$service$library$events$navigation$DistanceUnit[a2.ordinal()] = 2;
        } catch(NoSuchFieldError ignored) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$navigation$DistanceUnit[com.navdy.service.library.events.navigation.DistanceUnit.DISTANCE_FEET.ordinal()] = 3;
        } catch(NoSuchFieldError ignored) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$navigation$DistanceUnit[com.navdy.service.library.events.navigation.DistanceUnit.DISTANCE_KMS.ordinal()] = 4;
        } catch(NoSuchFieldError ignored) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$navigation$DistanceUnit[com.navdy.service.library.events.navigation.DistanceUnit.DISTANCE_MILES.ordinal()] = 1;
        } catch(NoSuchFieldError ignored) {
        }
    }
}
