package com.navdy.hud.app.maps.notification;

class RouteCalculationEventHandler$3 {
    final static int[] $SwitchMap$com$here$android$mpa$routing$RouteOptions$Type;
    final static int[] $SwitchMap$com$navdy$hud$app$maps$MapEvents$RouteCalculationState;
    
    static {
        $SwitchMap$com$navdy$hud$app$maps$MapEvents$RouteCalculationState = new int[com.navdy.hud.app.maps.MapEvents$RouteCalculationState.values().length];
        int[] a = $SwitchMap$com$navdy$hud$app$maps$MapEvents$RouteCalculationState;
        com.navdy.hud.app.maps.MapEvents$RouteCalculationState a0 = com.navdy.hud.app.maps.MapEvents$RouteCalculationState.STARTED;
        try {
            a[a0.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$maps$MapEvents$RouteCalculationState[com.navdy.hud.app.maps.MapEvents$RouteCalculationState.FINDING_NAV_COORDINATES.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException0) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$maps$MapEvents$RouteCalculationState[com.navdy.hud.app.maps.MapEvents$RouteCalculationState.STOPPED.ordinal()] = 3;
        } catch(NoSuchFieldError ignoredException1) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$maps$MapEvents$RouteCalculationState[com.navdy.hud.app.maps.MapEvents$RouteCalculationState.IN_PROGRESS.ordinal()] = 4;
        } catch(NoSuchFieldError ignoredException2) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$maps$MapEvents$RouteCalculationState[com.navdy.hud.app.maps.MapEvents$RouteCalculationState.ABORT_NAVIGATION.ordinal()] = 5;
        } catch(NoSuchFieldError ignoredException3) {
        }
        $SwitchMap$com$here$android$mpa$routing$RouteOptions$Type = new int[com.here.android.mpa.routing.RouteOptions.Type.values().length];
        int[] a1 = $SwitchMap$com$here$android$mpa$routing$RouteOptions$Type;
        com.here.android.mpa.routing.RouteOptions.Type a2 = com.here.android.mpa.routing.RouteOptions.Type.SHORTEST;
        try {
            a1[a2.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException4) {
        }
        try {
            $SwitchMap$com$here$android$mpa$routing$RouteOptions$Type[com.here.android.mpa.routing.RouteOptions.Type.FASTEST.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException5) {
        }
    }
}
