package com.navdy.hud.app.maps.here;
import com.navdy.hud.app.R;

final class HereRouteManager$2 implements com.navdy.hud.app.maps.here.HerePlacesManager$GeoCodeCallback {
    final boolean val$factoringInTraffic;
    final com.navdy.service.library.events.navigation.NavigationRouteRequest val$request;
    final com.here.android.mpa.common.GeoCoordinate val$startPoint;
    final java.util.List val$waypoints;
    
    HereRouteManager$2(com.navdy.service.library.events.navigation.NavigationRouteRequest a, com.here.android.mpa.common.GeoCoordinate a0, java.util.List a1, boolean b) {

        super();
        this.val$request = a;
        this.val$startPoint = a0;
        this.val$waypoints = a1;
        this.val$factoringInTraffic = b;
    }
    
    public void result(com.here.android.mpa.search.ErrorCode a, java.util.List a0) {
        label0: try {
            Object a1 = null;
            Throwable a2 = null;
            com.here.android.mpa.search.ErrorCode a3 = com.here.android.mpa.search.ErrorCode.CANCELLED;
            label3: {
                Object a4 = null;
                Throwable a5 = null;
                label4: if (a != a3) {
                    boolean b = false;
                    synchronized(com.navdy.hud.app.maps.here.HereRouteManager.access$400()) {
                        b = android.text.TextUtils.equals((CharSequence)this.val$request.requestId, (CharSequence)com.navdy.hud.app.maps.here.HereRouteManager.access$800());
                        /*monexit(a4)*/;
                    }
                    if (b) {
                        com.here.android.mpa.search.ErrorCode a7 = null;
                        synchronized(com.navdy.hud.app.maps.here.HereRouteManager.access$400()) {
                            com.navdy.hud.app.maps.here.HereRouteManager.access$902((com.here.android.mpa.search.GeocodeRequest)null);
                            com.navdy.hud.app.maps.here.HereRouteManager.access$802((String)null);
                            com.navdy.hud.app.maps.here.HereRouteManager.access$1002((com.navdy.service.library.events.navigation.NavigationRouteRequest)null);
                            /*monexit(a1)*/;
                            a7 = com.here.android.mpa.search.ErrorCode.NONE;
                        }
                        label1: {
                            label2: {
                                if (a != a7) {
                                    break label2;
                                }
                                if (a0 == null) {
                                    break label2;
                                }
                                if (a0.size() > 0) {
                                    break label1;
                                }
                            }
                            com.navdy.hud.app.maps.here.HereRouteManager.access$100().v(new StringBuilder().append("could not geocode, Street Address placesSearch error:").append(a.name()).toString());
                            com.navdy.hud.app.maps.here.HereRouteManager.access$300(com.navdy.service.library.events.RequestStatus.REQUEST_SERVICE_ERROR, this.val$request, com.navdy.hud.app.maps.here.HereRouteManager.access$200().getString(R.string.geocoding_failed));
                            break label0;
                        }
                        com.navdy.hud.app.maps.here.HereRouteManager.access$100().v(new StringBuilder().append("Street Address results size=").append(a0.size()).toString());
                        com.here.android.mpa.common.GeoCoordinate a9 = ((com.here.android.mpa.search.Location)a0.get(0)).getCoordinate();
                        com.navdy.hud.app.maps.here.HereRouteManager.access$100().v(new StringBuilder().append("Street Address placesSearch success: new-end-geo:").append(a9).toString());
                        com.navdy.hud.app.maps.here.HereRouteManager.access$1100(this.val$request, this.val$startPoint, this.val$waypoints, a9, this.val$factoringInTraffic);
                        break label0;
                    } else {
                        com.navdy.hud.app.maps.here.HereRouteManager.access$100().v(new StringBuilder().append("geocode request is not valid active:").append(com.navdy.hud.app.maps.here.HereRouteManager.access$800()).append(" request:").append(this.val$request.requestId).toString());
                        com.navdy.service.library.events.navigation.NavigationRouteResponse a10 = new com.navdy.service.library.events.navigation.NavigationRouteResponse(com.navdy.service.library.events.RequestStatus.REQUEST_CANCELLED, (String)null, this.val$request.destination, (String)null, (java.util.List)null, Boolean.valueOf(false), this.val$request.requestId);
                        com.navdy.hud.app.maps.MapsEventHandler.getInstance().sendEventToClient((com.squareup.wire.Message)a10);
                        break label0;
                    }
                } else {
                    com.navdy.hud.app.maps.here.HereRouteManager.access$100().v(new StringBuilder().append("geocode request has been cancelled [").append(this.val$request.requestId).append("]").toString());
                    break label0;
                }

            }

        } catch(Throwable a15) {
            com.navdy.hud.app.maps.here.HereRouteManager.access$100().v("Street Address placesSearch exception", a15);
            com.navdy.hud.app.maps.here.HereRouteManager.access$300(com.navdy.service.library.events.RequestStatus.REQUEST_UNKNOWN_ERROR, this.val$request, com.navdy.hud.app.maps.here.HereRouteManager.access$200().getString(R.string.geocoding_failed));
        }
    }
}
