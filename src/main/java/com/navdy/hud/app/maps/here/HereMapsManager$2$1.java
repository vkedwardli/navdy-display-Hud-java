package com.navdy.hud.app.maps.here;

import android.content.res.Resources;
import android.os.Bundle;
import android.text.TextUtils;

import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.R;
import com.navdy.hud.app.framework.glance.GlanceHelper;
import com.navdy.hud.app.framework.toast.ToastManager;
import com.navdy.hud.app.framework.toast.ToastPresenter;
import com.navdy.hud.app.framework.voice.TTSUtils;
import com.navdy.service.library.events.callcontrol.PhoneBatteryStatus;

import static com.navdy.hud.app.framework.glance.GlanceApp.GENERIC;

import static com.navdy.hud.app.maps.here.HereMapsManager.sLogger;


class HereMapsManager$2$1 implements com.here.android.mpa.odml.MapLoader.Listener {
    final com.navdy.hud.app.maps.here.HereMapsManager$2 this$1;
    private Resources resources = HudApplication.getAppContext().getResources();
    
    HereMapsManager$2$1(com.navdy.hud.app.maps.here.HereMapsManager$2 a) {
        super();
        this.this$1 = a;
    }
    
    public void onCheckForUpdateComplete(boolean isAvailable, String currentVersion, String availableVersion, com.here.android.mpa.odml.MapLoader.ResultCode a) {
        sLogger.v("MapLoader: updateAvailable[" + isAvailable + "] currentMapVersion[" + currentVersion + "] newestMapVersion[" + availableVersion + "] resultCode[" + a + "]");
        this.this$1.this$0.mapsUpdateAvailable = isAvailable;
        this.this$1.this$0.offlineMapsVersion.setVersion(currentVersion);
        this.this$1.this$0.offlineMapsVersion.setUpdateAvailable(isAvailable);
    }
    
    public void onGetMapPackagesComplete(com.here.android.mpa.odml.MapPackage mapPackage, com.here.android.mpa.odml.MapLoader.ResultCode resultCode) {
        if (resultCode == com.here.android.mpa.odml.MapLoader.ResultCode.OPERATION_SUCCESSFUL) {
            sLogger.v("initMapLoader: get map package success");
            com.navdy.service.library.task.TaskManager.getInstance().execute(new HereMapsManager$2$1$1(this, mapPackage), 2);
        } else {
            sLogger.e("initMapLoader: get map package failed:" + resultCode.name());
        }
    }
    
    public void onInstallMapPackagesComplete(com.here.android.mpa.odml.MapPackage a, com.here.android.mpa.odml.MapLoader.ResultCode a0) {
        sLogger.v("MapLoader: onInstallMapPackagesComplete result[" + a0.name() + "] package[" + ((a == null) ? "null" : a.getTitle()) + "]");
    }
    
    public void onInstallationSize(long j, long j0) {
        sLogger.v("MapLoader: onInstallationSize disk[" + j + "] network[" + j0 + "]");
    }
    
    public void onPerformMapDataUpdateComplete(com.here.android.mpa.odml.MapPackage a, com.here.android.mpa.odml.MapLoader.ResultCode a0) {
        this.this$1.this$0.mapUpdateInProgress = false;
        HereMapsManager.sLogger.v("MapLoader: onPerformMapDataUpdateComplete result[" + a0.name() + "] package[" + ((a == null) ? "null" : a.getTitle()) + "]");
        this.this$1.this$0.showMapsUpdateToast(resources.getString(R.string.map_update_complete, a0.name()));
    }
    
    public void onProgress(int i) {
        HereMapsManager.sLogger.v("MapLoader: progress[" + i + "]");
        this.this$1.this$0.showMapsUpdateToast(resources.getString(R.string.map_update_progress, i));
    }
    
    public void onUninstallMapPackagesComplete(com.here.android.mpa.odml.MapPackage a, com.here.android.mpa.odml.MapLoader.ResultCode a0) {
    }
}
