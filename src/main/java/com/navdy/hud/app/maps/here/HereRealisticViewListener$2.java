package com.navdy.hud.app.maps.here;

class HereRealisticViewListener$2 implements Runnable {
    final com.navdy.hud.app.maps.here.HereRealisticViewListener this$0;
    
    HereRealisticViewListener$2(com.navdy.hud.app.maps.here.HereRealisticViewListener a) {

        super();
        this.this$0 = a;
    }
    
    public void run() {
        com.navdy.hud.app.maps.here.HereRealisticViewListener.access$000(this.this$0).setRealisticViewMode(com.here.android.mpa.guidance.NavigationManager.RealisticViewMode.OFF);
        com.navdy.hud.app.maps.here.HereRealisticViewListener.access$000(this.this$0).removeRealisticViewListener((com.here.android.mpa.guidance.NavigationManager.RealisticViewListener)this.this$0);
        com.navdy.hud.app.maps.here.HereRealisticViewListener.access$100().v("removed realistic view listener");
    }
}
