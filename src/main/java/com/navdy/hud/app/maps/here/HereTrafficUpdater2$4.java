package com.navdy.hud.app.maps.here;

class HereTrafficUpdater2$4 implements Runnable {
    final com.navdy.hud.app.maps.here.HereTrafficUpdater2 this$0;
    
    HereTrafficUpdater2$4(com.navdy.hud.app.maps.here.HereTrafficUpdater2 a) {

        super();
        this.this$0 = a;
    }
    
    public void run() {
        boolean b = false;
        boolean b0 = false;
        try {
            b = false;
            com.navdy.hud.app.maps.here.HereTrafficUpdater2.access$000().v("checkRunnable");
            if (com.navdy.hud.app.maps.here.HereTrafficUpdater2.access$200(this.this$0) == null) {
                b0 = false;
            } else {
                b = false;
                if (com.navdy.hud.app.maps.here.HereTrafficUpdater2.access$300(this.this$0) <= 0L) {
                    b0 = false;
                } else {
                    b = false;
                    long j = android.os.SystemClock.elapsedRealtime() - com.navdy.hud.app.maps.here.HereTrafficUpdater2.access$300(this.this$0);
                    com.navdy.hud.app.maps.here.HereTrafficUpdater2.access$000().v(new StringBuilder().append("checkRunnable:").append(j).toString());
                    if (j < (long)com.navdy.hud.app.maps.here.HereTrafficUpdater2.access$1100()) {
                        b = true;
                        com.navdy.hud.app.maps.here.HereTrafficUpdater2.access$000().v("checkRunnable: request not stale");
                        b0 = true;
                    } else {
                        b = false;
                        com.navdy.hud.app.maps.here.HereTrafficUpdater2.access$000().v("checkRunnable: request stale");
                        com.navdy.hud.app.maps.here.HereTrafficUpdater2.access$400(this.this$0, 5000);
                        b0 = false;
                    }
                }
            }
        } catch(Throwable a) {
            if (b) {
                com.navdy.hud.app.maps.here.HereTrafficUpdater2.access$1300(this.this$0).postDelayed((Runnable)this, (long)com.navdy.hud.app.maps.here.HereTrafficUpdater2.access$1200());
            }
            throw a;
        }
        if (b0) {
            com.navdy.hud.app.maps.here.HereTrafficUpdater2.access$1300(this.this$0).postDelayed((Runnable)this, (long)com.navdy.hud.app.maps.here.HereTrafficUpdater2.access$1200());
        }
    }
}
