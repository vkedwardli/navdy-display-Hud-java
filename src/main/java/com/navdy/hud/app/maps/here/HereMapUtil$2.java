package com.navdy.hud.app.maps.here;

class HereMapUtil$2 {
    final static int[] $SwitchMap$com$here$android$mpa$routing$Maneuver$Action;
    
    static {
        $SwitchMap$com$here$android$mpa$routing$Maneuver$Action = new int[com.here.android.mpa.routing.Maneuver.Action.values().length];
        int[] a = $SwitchMap$com$here$android$mpa$routing$Maneuver$Action;
        com.here.android.mpa.routing.Maneuver.Action a0 = com.here.android.mpa.routing.Maneuver.Action.CONTINUE_HIGHWAY;
        try {
            a[a0.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException) {
        }
        try {
            $SwitchMap$com$here$android$mpa$routing$Maneuver$Action[com.here.android.mpa.routing.Maneuver.Action.CHANGE_HIGHWAY.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException0) {
        }
        try {
            $SwitchMap$com$here$android$mpa$routing$Maneuver$Action[com.here.android.mpa.routing.Maneuver.Action.ENTER_HIGHWAY.ordinal()] = 3;
        } catch(NoSuchFieldError ignoredException1) {
        }
        try {
            $SwitchMap$com$here$android$mpa$routing$Maneuver$Action[com.here.android.mpa.routing.Maneuver.Action.ENTER_HIGHWAY_FROM_LEFT.ordinal()] = 4;
        } catch(NoSuchFieldError ignoredException2) {
        }
        try {
            $SwitchMap$com$here$android$mpa$routing$Maneuver$Action[com.here.android.mpa.routing.Maneuver.Action.ENTER_HIGHWAY_FROM_RIGHT.ordinal()] = 5;
        } catch(NoSuchFieldError ignoredException3) {
        }
    }
}
