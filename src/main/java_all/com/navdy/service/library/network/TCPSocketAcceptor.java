package com.navdy.service.library.network;

import com.navdy.service.library.device.NavdyDeviceId;
import com.navdy.service.library.device.connection.TCPConnectionInfo;
import com.navdy.service.library.device.connection.ConnectionInfo;
import com.navdy.service.library.device.connection.ConnectionType;
import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import com.navdy.service.library.log.Logger;

public class TCPSocketAcceptor implements SocketAcceptor
{
    private static final Logger sLogger;
    private final boolean local;
    private final int port;
    private ServerSocket serverSocket;
    
    static {
        sLogger = new Logger(TCPSocketAcceptor.class);
    }
    
    public TCPSocketAcceptor(final int n) {
        this(n, false);
    }
    
    public TCPSocketAcceptor(final int port, final boolean local) {
        this.local = local;
        this.port = port;
    }
    
    @Override
    public SocketAdapter accept() throws IOException {
        if (this.serverSocket == null) {
            if (this.local) {
                this.serverSocket = new ServerSocket(this.port, 10, InetAddress.getByAddress(new byte[] { 127, 0, 0, 1 }));
            }
            else {
                this.serverSocket = new ServerSocket(this.port, 10);
            }
            final Logger sLogger = TCPSocketAcceptor.sLogger;
            final StringBuilder append = new StringBuilder().append("Socket bound to ");
            String s;
            if (this.local) {
                s = "localhost";
            }
            else {
                s = "all interfaces";
            }
            sLogger.d(append.append(s).append(", port ").append(this.port).toString());
        }
        return new TCPSocketAdapter(this.serverSocket.accept());
    }
    
    @Override
    public void close() throws IOException {
        if (this.serverSocket != null) {
            this.serverSocket.close();
            this.serverSocket = null;
        }
    }
    
    @Override
    public ConnectionInfo getRemoteConnectionInfo(final SocketAdapter socketAdapter, final ConnectionType connectionType) {
        if (!(socketAdapter instanceof TCPSocketAdapter)) {
            return null;
        }
        final TCPSocketAdapter tcpSocketAdapter = (TCPSocketAdapter)socketAdapter;
        final NavdyDeviceId remoteDevice = socketAdapter.getRemoteDevice();
        if (remoteDevice == null) {
            return null;
        }
        return new TCPConnectionInfo(remoteDevice, tcpSocketAdapter.getRemoteAddress());
        tcpConnectionInfo = null;
        return tcpConnectionInfo;
    }
}
