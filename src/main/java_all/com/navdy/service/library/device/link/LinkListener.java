package com.navdy.service.library.device.link;

import com.navdy.service.library.device.connection.Connection;
import com.navdy.service.library.device.connection.ConnectionType;

public interface LinkListener
{
    void linkEstablished(final ConnectionType p0);
    
    void linkLost(final ConnectionType p0, final Connection.DisconnectCause p1);
    
    void onNavdyEventReceived(final byte[] p0);
    
    void onNetworkLinkReady();
}
