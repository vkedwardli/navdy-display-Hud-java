package com.navdy.service.library.device.connection;

import java.util.HashSet;
import java.util.Set;

public enum ConnectionType
{
    BT_IAP2_LINK, 
    BT_PROTOBUF, 
    BT_TUNNEL, 
    EA_PROTOBUF, 
    EA_TUNNEL, 
    HTTP("_http._tcp."), 
    TCP_PROTOBUF("_navdybus._tcp.");
    
    private final String mServiceType;
    
    private ConnectionType() {
        this.mServiceType = null;
    }
    
    private ConnectionType(final String mServiceType) {
        this.mServiceType = mServiceType;
    }
    
    public static ConnectionType fromServiceType(final String s) {
        for (final ConnectionType connectionType : values()) {
            if (s.equals(connectionType.getServiceType())) {
                return connectionType;
            }
        }
        return null;
    }
    
    public static Set<String> getServiceTypes() {
        final HashSet<String> set = new HashSet<String>(values().length);
        for (final ConnectionType connectionType : values()) {
            if (connectionType.getServiceType() != null) {
                set.add(connectionType.getServiceType());
            }
        }
        return set;
    }
    
    public String getServiceType() {
        return this.mServiceType;
    }
}
