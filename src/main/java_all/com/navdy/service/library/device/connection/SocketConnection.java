package com.navdy.service.library.device.connection;

import java.io.IOException;
import com.navdy.service.library.network.SocketAdapter;
import com.navdy.service.library.network.SocketFactory;

public class SocketConnection extends Connection
{
    private SocketFactory connector;
    protected boolean mBlockReconnect;
    protected ConnectThread mConnectThread;
    private SocketAdapter socket;
    
    public SocketConnection(final ConnectionInfo connectionInfo, final SocketAdapter existingSocketConnection) {
        super(connectionInfo);
        this.setExistingSocketConnection(existingSocketConnection);
    }
    
    public SocketConnection(final ConnectionInfo connectionInfo, final SocketFactory connector) {
        super(connectionInfo);
        this.connector = connector;
    }
    
    private void closeQuietly(final SocketAdapter socketAdapter) {
        if (socketAdapter == null) {
            return;
        }
        try {
            socketAdapter.close();
        }
        catch (Throwable t) {
            this.logger.e("[SOCK] Exception while closing", t);
        }
    }
    
    private void connectionFailed(final ConnectionFailureCause connectionFailureCause) {
        this.dispatchConnectionFailedEvent(connectionFailureCause);
    }
    
    @Override
    public boolean connect() {
        while (true) {
            boolean b = false;
            Label_0115: {
                synchronized (this) {
                    if (!this.mBlockReconnect) {
                        this.logger.d("connect to: " + this.mConnectionInfo);
                        if (this.mStatus == Status.DISCONNECTED) {
                            break Label_0115;
                        }
                        this.logger.d("can't connect: state: " + this.mStatus);
                    }
                    return b;
                }
            }
            this.mStatus = Status.CONNECTING;
            (this.mConnectThread = new ConnectThread()).start();
            b = true;
            return b;
        }
    }
    
    public void connected(final SocketAdapter socket) {
        synchronized (this) {
            this.logger.i("connected");
            if (this.mConnectThread != null) {
                this.mConnectThread.cancel();
                this.mConnectThread = null;
            }
            if (this.mStatus != Status.CONNECTING || socket == null) {
                this.logger.e("No longer connecting - disconnecting.");
                this.closeQuietly(socket);
                this.mStatus = Status.DISCONNECTED;
                this.dispatchDisconnectEvent(DisconnectCause.NORMAL);
            }
            else {
                this.socket = socket;
                this.mStatus = Status.CONNECTED;
                this.dispatchConnectEvent();
            }
        }
    }
    
    @Override
    public boolean disconnect() {
        while (true) {
            Label_0140: {
                synchronized (this) {
                    if (this.mStatus == Status.DISCONNECTED || this.mStatus == Status.DISCONNECTING) {
                        this.logger.d("can't disconnect: state: " + this.mStatus);
                    }
                    else {
                        this.logger.d("disconnect - connectThread:" + this.mConnectThread + " socket:" + this.socket);
                        this.mStatus = Status.DISCONNECTING;
                        if (this.mConnectThread == null) {
                            break Label_0140;
                        }
                        this.mConnectThread.cancel();
                        this.mConnectThread = null;
                    }
                    return true;
                }
            }
            if (this.socket != null) {
                while (true) {
                    try {
                        this.socket.close();
                        this.socket = null;
                        return true;
                    }
                    catch (IOException ex) {
                        this.logger.e("Exception closing socket" + ex.getMessage());
                        continue;
                    }
                    break;
                }
            }
            this.mStatus = Status.DISCONNECTED;
            this.dispatchDisconnectEvent(DisconnectCause.NORMAL);
            return true;
        }
    }
    
    @Override
    public SocketAdapter getSocket() {
        return this.socket;
    }
    
    protected void setExistingSocketConnection(final SocketAdapter socketAdapter) {
        synchronized (this) {
            this.mBlockReconnect = true;
            this.mStatus = Status.CONNECTING;
            this.connected(socketAdapter);
        }
    }
    
    private class ConnectThread extends Thread
    {
        private SocketAdapter mmSocket;
        
        public ConnectThread() {
            this.setName("ConnectThread");
        }
        
        public void cancel() {
            SocketConnection.this.logger.d("[SOCK]Cancelling connect, closing socket");
            SocketConnection.this.closeQuietly(this.mmSocket);
            this.mmSocket = null;
        }
        
        @Override
        public void run() {
            // 
            This method could not be decompiled.
            // 
            // Original Bytecode:
            // 
            //     1: getfield        com/navdy/service/library/device/connection/SocketConnection$ConnectThread.this$0:Lcom/navdy/service/library/device/connection/SocketConnection;
            //     4: getfield        com/navdy/service/library/device/connection/SocketConnection.logger:Lcom/navdy/service/library/log/Logger;
            //     7: ldc             "BEGIN mConnectThread"
            //     9: invokevirtual   com/navdy/service/library/log/Logger.i:(Ljava/lang/String;)V
            //    12: getstatic       com/navdy/service/library/device/connection/Connection$ConnectionFailureCause.UNKNOWN:Lcom/navdy/service/library/device/connection/Connection$ConnectionFailureCause;
            //    15: astore_1       
            //    16: aload_0        
            //    17: aload_0        
            //    18: getfield        com/navdy/service/library/device/connection/SocketConnection$ConnectThread.this$0:Lcom/navdy/service/library/device/connection/SocketConnection;
            //    21: invokestatic    com/navdy/service/library/device/connection/SocketConnection.access$000:(Lcom/navdy/service/library/device/connection/SocketConnection;)Lcom/navdy/service/library/network/SocketFactory;
            //    24: invokeinterface com/navdy/service/library/network/SocketFactory.build:()Lcom/navdy/service/library/network/SocketAdapter;
            //    29: putfield        com/navdy/service/library/device/connection/SocketConnection$ConnectThread.mmSocket:Lcom/navdy/service/library/network/SocketAdapter;
            //    32: aload_0        
            //    33: getfield        com/navdy/service/library/device/connection/SocketConnection$ConnectThread.mmSocket:Lcom/navdy/service/library/network/SocketAdapter;
            //    36: invokeinterface com/navdy/service/library/network/SocketAdapter.connect:()V
            //    41: aload_0        
            //    42: getfield        com/navdy/service/library/device/connection/SocketConnection$ConnectThread.this$0:Lcom/navdy/service/library/device/connection/SocketConnection;
            //    45: astore_1       
            //    46: aload_1        
            //    47: dup            
            //    48: astore_2       
            //    49: monitorenter   
            //    50: aload_0        
            //    51: getfield        com/navdy/service/library/device/connection/SocketConnection$ConnectThread.this$0:Lcom/navdy/service/library/device/connection/SocketConnection;
            //    54: aconst_null    
            //    55: putfield        com/navdy/service/library/device/connection/SocketConnection.mConnectThread:Lcom/navdy/service/library/device/connection/SocketConnection$ConnectThread;
            //    58: aload_2        
            //    59: monitorexit    
            //    60: aload_0        
            //    61: getfield        com/navdy/service/library/device/connection/SocketConnection$ConnectThread.this$0:Lcom/navdy/service/library/device/connection/SocketConnection;
            //    64: aload_0        
            //    65: getfield        com/navdy/service/library/device/connection/SocketConnection$ConnectThread.mmSocket:Lcom/navdy/service/library/network/SocketAdapter;
            //    68: invokevirtual   com/navdy/service/library/device/connection/SocketConnection.connected:(Lcom/navdy/service/library/network/SocketAdapter;)V
            //    71: return         
            //    72: astore_3       
            //    73: aload_0        
            //    74: getfield        com/navdy/service/library/device/connection/SocketConnection$ConnectThread.this$0:Lcom/navdy/service/library/device/connection/SocketConnection;
            //    77: getfield        com/navdy/service/library/device/connection/SocketConnection.logger:Lcom/navdy/service/library/log/Logger;
            //    80: new             Ljava/lang/StringBuilder;
            //    83: dup            
            //    84: invokespecial   java/lang/StringBuilder.<init>:()V
            //    87: ldc             "Unable to connect - "
            //    89: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //    92: aload_3        
            //    93: invokevirtual   java/lang/Exception.getMessage:()Ljava/lang/String;
            //    96: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //    99: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
            //   102: invokevirtual   com/navdy/service/library/log/Logger.e:(Ljava/lang/String;)V
            //   105: aload_1        
            //   106: astore          4
            //   108: aload_3        
            //   109: instanceof      Ljava/net/SocketException;
            //   112: ifeq            134
            //   115: aload_3        
            //   116: invokevirtual   java/lang/Exception.getMessage:()Ljava/lang/String;
            //   119: astore_3       
            //   120: aload_3        
            //   121: ldc             "ETIMEDOUT"
            //   123: invokevirtual   java/lang/String.contains:(Ljava/lang/CharSequence;)Z
            //   126: ifeq            252
            //   129: getstatic       com/navdy/service/library/device/connection/Connection$ConnectionFailureCause.CONNECTION_TIMED_OUT:Lcom/navdy/service/library/device/connection/Connection$ConnectionFailureCause;
            //   132: astore          4
            //   134: aload_0        
            //   135: getfield        com/navdy/service/library/device/connection/SocketConnection$ConnectThread.this$0:Lcom/navdy/service/library/device/connection/SocketConnection;
            //   138: astore_1       
            //   139: aload_1        
            //   140: dup            
            //   141: astore_2       
            //   142: monitorenter   
            //   143: aload_0        
            //   144: getfield        com/navdy/service/library/device/connection/SocketConnection$ConnectThread.this$0:Lcom/navdy/service/library/device/connection/SocketConnection;
            //   147: getfield        com/navdy/service/library/device/connection/SocketConnection.logger:Lcom/navdy/service/library/log/Logger;
            //   150: ldc             "Connect failed, closing socket"
            //   152: invokevirtual   com/navdy/service/library/log/Logger.d:(Ljava/lang/String;)V
            //   155: aload_0        
            //   156: getfield        com/navdy/service/library/device/connection/SocketConnection$ConnectThread.this$0:Lcom/navdy/service/library/device/connection/SocketConnection;
            //   159: aload_0        
            //   160: getfield        com/navdy/service/library/device/connection/SocketConnection$ConnectThread.mmSocket:Lcom/navdy/service/library/network/SocketAdapter;
            //   163: invokestatic    com/navdy/service/library/device/connection/SocketConnection.access$100:(Lcom/navdy/service/library/device/connection/SocketConnection;Lcom/navdy/service/library/network/SocketAdapter;)V
            //   166: aload_0        
            //   167: aconst_null    
            //   168: putfield        com/navdy/service/library/device/connection/SocketConnection$ConnectThread.mmSocket:Lcom/navdy/service/library/network/SocketAdapter;
            //   171: aload_0        
            //   172: getfield        com/navdy/service/library/device/connection/SocketConnection$ConnectThread.this$0:Lcom/navdy/service/library/device/connection/SocketConnection;
            //   175: getfield        com/navdy/service/library/device/connection/SocketConnection.mStatus:Lcom/navdy/service/library/device/connection/Connection$Status;
            //   178: astore_3       
            //   179: aload_0        
            //   180: getfield        com/navdy/service/library/device/connection/SocketConnection$ConnectThread.this$0:Lcom/navdy/service/library/device/connection/SocketConnection;
            //   183: getstatic       com/navdy/service/library/device/connection/Connection$Status.DISCONNECTED:Lcom/navdy/service/library/device/connection/Connection$Status;
            //   186: putfield        com/navdy/service/library/device/connection/SocketConnection.mStatus:Lcom/navdy/service/library/device/connection/Connection$Status;
            //   189: aload_2        
            //   190: monitorexit    
            //   191: getstatic       com/navdy/service/library/device/connection/SocketConnection$1.$SwitchMap$com$navdy$service$library$device$connection$Connection$Status:[I
            //   194: aload_3        
            //   195: invokevirtual   com/navdy/service/library/device/connection/Connection$Status.ordinal:()I
            //   198: iaload         
            //   199: tableswitch {
            //                2: 279
            //                3: 291
            //          default: 220
            //        }
            //   220: aload_0        
            //   221: getfield        com/navdy/service/library/device/connection/SocketConnection$ConnectThread.this$0:Lcom/navdy/service/library/device/connection/SocketConnection;
            //   224: getfield        com/navdy/service/library/device/connection/SocketConnection.logger:Lcom/navdy/service/library/log/Logger;
            //   227: new             Ljava/lang/StringBuilder;
            //   230: dup            
            //   231: invokespecial   java/lang/StringBuilder.<init>:()V
            //   234: ldc             "unexpected state during connection failure: "
            //   236: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //   239: aload_3        
            //   240: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/Object;)Ljava/lang/StringBuilder;
            //   243: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
            //   246: invokevirtual   com/navdy/service/library/log/Logger.w:(Ljava/lang/String;)V
            //   249: goto            71
            //   252: aload_1        
            //   253: astore          4
            //   255: aload_3        
            //   256: ldc             "ECONNREFUSED"
            //   258: invokevirtual   java/lang/String.contains:(Ljava/lang/CharSequence;)Z
            //   261: ifeq            134
            //   264: getstatic       com/navdy/service/library/device/connection/Connection$ConnectionFailureCause.CONNECTION_REFUSED:Lcom/navdy/service/library/device/connection/Connection$ConnectionFailureCause;
            //   267: astore          4
            //   269: goto            134
            //   272: astore          4
            //   274: aload_2        
            //   275: monitorexit    
            //   276: aload           4
            //   278: athrow         
            //   279: aload_0        
            //   280: getfield        com/navdy/service/library/device/connection/SocketConnection$ConnectThread.this$0:Lcom/navdy/service/library/device/connection/SocketConnection;
            //   283: aload           4
            //   285: invokestatic    com/navdy/service/library/device/connection/SocketConnection.access$200:(Lcom/navdy/service/library/device/connection/SocketConnection;Lcom/navdy/service/library/device/connection/Connection$ConnectionFailureCause;)V
            //   288: goto            71
            //   291: aload_0        
            //   292: getfield        com/navdy/service/library/device/connection/SocketConnection$ConnectThread.this$0:Lcom/navdy/service/library/device/connection/SocketConnection;
            //   295: getstatic       com/navdy/service/library/device/connection/Connection$DisconnectCause.NORMAL:Lcom/navdy/service/library/device/connection/Connection$DisconnectCause;
            //   298: invokevirtual   com/navdy/service/library/device/connection/SocketConnection.dispatchDisconnectEvent:(Lcom/navdy/service/library/device/connection/Connection$DisconnectCause;)V
            //   301: goto            71
            //   304: astore          4
            //   306: aload_2        
            //   307: monitorexit    
            //   308: aload           4
            //   310: athrow         
            //    Exceptions:
            //  Try           Handler
            //  Start  End    Start  End    Type                 
            //  -----  -----  -----  -----  ---------------------
            //  16     41     72     304    Ljava/lang/Exception;
            //  50     60     304    311    Any
            //  143    191    272    279    Any
            //  274    276    272    279    Any
            //  306    308    304    311    Any
            // 
            // The error that occurred was:
            // 
            // java.lang.IllegalStateException: Expression is linked from several locations: Label_0071:
            //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
            //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
            //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
            //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
            //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
            //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:556)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
            //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
            //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
            //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
            //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
            //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
            //     at java.lang.Thread.run(Unknown Source)
            // 
            throw new IllegalStateException("An error occurred while decompiling this method.");
        }
    }
}
