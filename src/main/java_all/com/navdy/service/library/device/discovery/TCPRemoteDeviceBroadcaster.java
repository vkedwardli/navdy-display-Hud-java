package com.navdy.service.library.device.discovery;

import com.navdy.service.library.device.connection.TCPConnectionInfo;
import com.navdy.service.library.device.NavdyDeviceId;
import android.content.Context;

public class TCPRemoteDeviceBroadcaster extends MDNSDeviceBroadcaster
{
    public TCPRemoteDeviceBroadcaster(final Context context) {
        super(context, TCPConnectionInfo.nsdServiceWithDeviceId(NavdyDeviceId.getThisDevice(context)));
    }
}
