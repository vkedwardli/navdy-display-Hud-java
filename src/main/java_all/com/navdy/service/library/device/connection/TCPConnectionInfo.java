package com.navdy.service.library.device.connection;

import com.navdy.service.library.device.NavdyDeviceId;
import android.net.nsd.NsdServiceInfo;
import com.google.gson.annotations.SerializedName;

public class TCPConnectionInfo extends ConnectionInfo
{
    public static final String FORMAT_SERVICE_NAME_WITH_DEVICE_ID = "Navdy %s";
    public static final int SERVICE_PORT = 21301;
    public static final String SERVICE_TYPE = "_navdybus._tcp.";
    @SerializedName("address")
    String mHostName;
    
    public TCPConnectionInfo(final NsdServiceInfo nsdServiceInfo) {
        super(nsdServiceInfo);
        final String hostAddress = nsdServiceInfo.getHost().getHostAddress();
        if (hostAddress == null) {
            throw new IllegalArgumentException("hostname not found in service record");
        }
        this.mHostName = hostAddress;
    }
    
    public TCPConnectionInfo(final NavdyDeviceId navdyDeviceId, final String mHostName) {
        super(navdyDeviceId, ConnectionType.TCP_PROTOBUF);
        if (mHostName == null) {
            throw new IllegalArgumentException("hostname required");
        }
        this.mHostName = mHostName;
    }
    
    public static NsdServiceInfo nsdServiceWithDeviceId(final NavdyDeviceId navdyDeviceId) {
        final NsdServiceInfo nsdServiceInfo = new NsdServiceInfo();
        nsdServiceInfo.setServiceName(String.format("Navdy %s", navdyDeviceId.toString()));
        nsdServiceInfo.setServiceType("_navdybus._tcp.");
        nsdServiceInfo.setPort(21301);
        return nsdServiceInfo;
    }
    
    @Override
    public boolean equals(final Object o) {
        final boolean b = false;
        boolean equals;
        if (this == o) {
            equals = true;
        }
        else {
            equals = b;
            if (o != null) {
                equals = b;
                if (this.getClass() == o.getClass()) {
                    equals = b;
                    if (super.equals(o)) {
                        equals = this.mHostName.equals(((TCPConnectionInfo)o).mHostName);
                    }
                }
            }
        }
        return equals;
    }
    
    @Override
    public ServiceAddress getAddress() {
        return new ServiceAddress(this.mHostName, String.valueOf(21301));
    }
    
    @Override
    public int hashCode() {
        return super.hashCode() * 31 + this.mHostName.hashCode();
    }
}
