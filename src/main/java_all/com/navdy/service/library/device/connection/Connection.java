package com.navdy.service.library.device.connection;

import com.navdy.service.library.network.SocketAdapter;
import com.navdy.service.library.network.TCPSocketFactory;
import com.navdy.service.library.network.SocketFactory;
import com.navdy.service.library.network.BTSocketFactory;
import android.content.Context;
import java.util.HashMap;
import com.navdy.service.library.log.Logger;
import java.util.Map;
import com.navdy.service.library.util.Listenable;

public abstract class Connection extends Listenable<Listener>
{
    private static Map<ConnectionType, ConnectionFactory> factoryMap;
    private static ConnectionFactory sDefaultFactory;
    protected Logger logger;
    protected ConnectionInfo mConnectionInfo;
    protected Status mStatus;
    
    static {
        Connection.factoryMap = new HashMap<ConnectionType, ConnectionFactory>();
        Connection.sDefaultFactory = (ConnectionFactory)new ConnectionFactory() {
            @Override
            public Connection build(final Context context, final ConnectionInfo connectionInfo) {
                SocketConnection socketConnection = null;
                switch (connectionInfo.getType()) {
                    default:
                        throw new IllegalArgumentException("Unknown connection class for type: " + connectionInfo.getType());
                    case BT_PROTOBUF:
                    case BT_TUNNEL:
                    case EA_PROTOBUF:
                    case BT_IAP2_LINK: {
                        ServiceAddress address;
                        final ServiceAddress serviceAddress = address = connectionInfo.getAddress();
                        if (serviceAddress.getService().equals(ConnectionService.ACCESSORY_IAP2.toString())) {
                            address = new ServiceAddress(serviceAddress.getAddress(), ConnectionService.DEVICE_IAP2.toString(), serviceAddress.getProtocol());
                        }
                        socketConnection = new SocketConnection(connectionInfo, new BTSocketFactory(address));
                        break;
                    }
                    case TCP_PROTOBUF:
                        socketConnection = new SocketConnection(connectionInfo, new TCPSocketFactory(connectionInfo.getAddress()));
                        break;
                }
                return socketConnection;
            }
        };
        registerConnectionType(ConnectionType.BT_PROTOBUF, Connection.sDefaultFactory);
        registerConnectionType(ConnectionType.BT_TUNNEL, Connection.sDefaultFactory);
        registerConnectionType(ConnectionType.TCP_PROTOBUF, Connection.sDefaultFactory);
        registerConnectionType(ConnectionType.BT_IAP2_LINK, Connection.sDefaultFactory);
        registerConnectionType(ConnectionType.EA_PROTOBUF, Connection.sDefaultFactory);
    }
    
    public Connection(final ConnectionInfo mConnectionInfo) {
        this.logger = new Logger(this.getClass());
        this.mConnectionInfo = mConnectionInfo;
        this.mStatus = Status.DISCONNECTED;
    }
    
    public static Connection instantiateFromConnectionInfo(final Context context, final ConnectionInfo connectionInfo) {
        final ConnectionFactory connectionFactory = Connection.factoryMap.get(connectionInfo.getType());
        if (connectionFactory != null) {
            return connectionFactory.build(context, connectionInfo);
        }
        throw new IllegalArgumentException("Unknown connection class for type: " + connectionInfo.getType());
    }
    
    public static void registerConnectionType(final ConnectionType connectionType, final ConnectionFactory connectionFactory) {
        Connection.factoryMap.put(connectionType, connectionFactory);
    }
    
    public abstract boolean connect();
    
    public abstract boolean disconnect();
    
    protected void dispatchConnectEvent() {
        this.dispatchToListeners((Listenable.EventDispatcher)new EventDispatcher() {
            public void dispatchEvent(final Connection connection, final Connection.Listener listener) {
                listener.onConnected(connection);
            }
        });
    }
    
    protected void dispatchConnectionFailedEvent(final ConnectionFailureCause connectionFailureCause) {
        this.dispatchToListeners((Listenable.EventDispatcher)new EventDispatcher() {
            public void dispatchEvent(final Connection connection, final Connection.Listener listener) {
                listener.onConnectionFailed(connection, connectionFailureCause);
            }
        });
    }
    
    protected void dispatchDisconnectEvent(final DisconnectCause disconnectCause) {
        this.dispatchToListeners((Listenable.EventDispatcher)new EventDispatcher() {
            public void dispatchEvent(final Connection connection, final Connection.Listener listener) {
                listener.onDisconnected(connection, disconnectCause);
            }
        });
    }
    
    public ConnectionInfo getConnectionInfo() {
        return this.mConnectionInfo;
    }
    
    public abstract SocketAdapter getSocket();
    
    public Status getStatus() {
        return this.mStatus;
    }
    
    public ConnectionType getType() {
        return this.mConnectionInfo.getType();
    }
    
    public interface ConnectionFactory
    {
        Connection build(final Context p0, final ConnectionInfo p1);
    }
    
    public enum ConnectionFailureCause
    {
        CONNECTION_REFUSED, 
        CONNECTION_TIMED_OUT, 
        UNKNOWN;
    }
    
    public enum DisconnectCause
    {
        ABORTED, 
        NORMAL, 
        UNKNOWN;
    }
    
    protected interface EventDispatcher extends Listenable.EventDispatcher<Connection, Connection.Listener>
    {
    }
    
    public interface Listener extends Listenable.Listener
    {
        void onConnected(final Connection p0);
        
        void onConnectionFailed(final Connection p0, final ConnectionFailureCause p1);
        
        void onDisconnected(final Connection p0, final DisconnectCause p1);
    }
    
    public enum Status
    {
        CONNECTED, 
        CONNECTING, 
        DISCONNECTED, 
        DISCONNECTING;
    }
}
