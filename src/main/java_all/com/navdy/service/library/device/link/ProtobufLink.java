package com.navdy.service.library.device.link;

import java.io.OutputStream;
import java.io.InputStream;
import java.io.Closeable;
import java.io.IOException;
import com.navdy.service.library.util.IOUtils;
import java.util.concurrent.LinkedBlockingDeque;
import com.navdy.service.library.network.SocketAdapter;
import com.navdy.service.library.device.connection.ConnectionInfo;

public class ProtobufLink implements Link
{
    private int bandwidthLevel;
    private ConnectionInfo connectionInfo;
    private ReaderThread readerThread;
    private WriterThread writerThread;
    
    public ProtobufLink(final ConnectionInfo connectionInfo) {
        this.bandwidthLevel = 1;
        this.connectionInfo = connectionInfo;
    }
    
    @Override
    public void close() {
        if (this.writerThread != null) {
            this.writerThread.cancel();
            this.writerThread = null;
        }
        if (this.readerThread != null) {
            this.readerThread.cancel();
            this.readerThread = null;
        }
    }
    
    @Override
    public int getBandWidthLevel() {
        return this.bandwidthLevel;
    }
    
    @Override
    public void setBandwidthLevel(final int bandwidthLevel) {
        this.bandwidthLevel = bandwidthLevel;
    }
    
    @Override
    public boolean start(final SocketAdapter socketAdapter, final LinkedBlockingDeque<EventRequest> linkedBlockingDeque, final LinkListener linkListener) {
        boolean b = true;
        if ((this.writerThread != null && !this.writerThread.isClosing()) || (this.readerThread != null && !this.readerThread.isClosing())) {
            throw new IllegalStateException("Must stop threads before calling startLink");
        }
        Closeable inputStream = null;
        try {
            final InputStream inputStream2 = (InputStream)(inputStream = socketAdapter.getInputStream());
            final OutputStream outputStream = socketAdapter.getOutputStream();
            inputStream = inputStream2;
            inputStream = inputStream2;
            final ReaderThread readerThread = new ReaderThread(this.connectionInfo.getType(), inputStream2, linkListener, true);
            inputStream = inputStream2;
            this.readerThread = readerThread;
            inputStream = inputStream2;
            inputStream = inputStream2;
            final WriterThread writerThread = new WriterThread(linkedBlockingDeque, outputStream, null);
            inputStream = inputStream2;
            this.writerThread = writerThread;
            this.readerThread.start();
            this.writerThread.start();
            return b;
        }
        catch (IOException ex) {
            IOUtils.closeStream(inputStream);
            this.readerThread = null;
            this.writerThread = null;
            b = false;
            return b;
        }
    }
}
