package com.navdy.service.library.device;

import com.navdy.service.library.network.SocketAdapter;
import com.navdy.service.library.device.link.LinkManager;
import android.os.SystemClock;
import com.navdy.service.library.device.connection.ConnectionType;
import android.support.annotation.Nullable;
import com.navdy.service.library.events.connection.NetworkLinkReady;
import com.navdy.service.library.events.connection.ConnectionStateChange;
import com.navdy.service.library.events.NavdyEvent;
import com.navdy.service.library.events.NavdyEventUtil;
import com.squareup.wire.Message;
import com.navdy.service.library.events.Ext_NavdyEvent;
import android.os.Looper;
import com.squareup.wire.Wire;
import java.util.concurrent.atomic.AtomicBoolean;
import com.navdy.service.library.device.link.Link;
import android.os.Handler;
import com.navdy.service.library.device.link.EventRequest;
import java.util.concurrent.LinkedBlockingDeque;
import com.navdy.service.library.events.DeviceInfo;
import android.content.Context;
import com.navdy.service.library.device.connection.ConnectionInfo;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.device.link.LinkListener;
import com.navdy.service.library.device.connection.Connection;
import com.navdy.service.library.util.Listenable;

public class RemoteDevice extends Listenable<Listener> implements Connection.Listener, LinkListener
{
    public static final int MAX_PACKET_SIZE = 524288;
    private static final Object lock;
    public static final LastSeenDeviceInfo sLastSeenDeviceInfo;
    public static final Logger sLogger;
    protected boolean convertToNavdyEvent;
    public long lastMessageSentTime;
    protected Connection mActiveConnection;
    protected ConnectionInfo mConnectionInfo;
    protected final Context mContext;
    protected NavdyDeviceId mDeviceId;
    protected DeviceInfo mDeviceInfo;
    protected final LinkedBlockingDeque<EventRequest> mEventQueue;
    protected final Handler mHandler;
    private Link mLink;
    private LinkStatus mLinkStatus;
    private volatile boolean mNetworkLinkReady;
    protected AtomicBoolean mNetworkReadyEventDispatched;
    private Wire mWire;
    
    static {
        sLogger = new Logger(RemoteDevice.class);
        lock = new Object();
        sLastSeenDeviceInfo = new LastSeenDeviceInfo();
    }
    
    public RemoteDevice(final Context mContext, final NavdyDeviceId mDeviceId, final boolean convertToNavdyEvent) {
        this.mLinkStatus = LinkStatus.DISCONNECTED;
        this.mNetworkLinkReady = false;
        this.mNetworkReadyEventDispatched = new AtomicBoolean(false);
        this.mContext = mContext;
        this.mDeviceId = mDeviceId;
        this.mHandler = new Handler(Looper.getMainLooper());
        this.mEventQueue = new LinkedBlockingDeque<EventRequest>();
        this.convertToNavdyEvent = convertToNavdyEvent;
        if (convertToNavdyEvent) {
            this.mWire = new Wire((Class<?>[])new Class[] { Ext_NavdyEvent.class });
        }
    }
    
    public RemoteDevice(final Context context, final ConnectionInfo mConnectionInfo, final boolean b) {
        this(context, mConnectionInfo.getDeviceId(), b);
        this.mConnectionInfo = mConnectionInfo;
    }
    
    private void dispatchLocalEvent(final Message message) {
        final NavdyEvent eventFromMessage = NavdyEventUtil.eventFromMessage(message);
        if (this.convertToNavdyEvent) {
            this.dispatchNavdyEvent(eventFromMessage);
        }
        else {
            this.dispatchNavdyEvent(eventFromMessage.toByteArray());
        }
    }
    
    private void dispatchStateChangeEvents(final LinkStatus linkStatus, final LinkStatus linkStatus2) {
        final String string = this.getDeviceId().toString();
        switch (linkStatus) {
            case CONNECTED:
                if (linkStatus2 == LinkStatus.DISCONNECTED) {
                    this.dispatchLocalEvent(new ConnectionStateChange(string, ConnectionStateChange.ConnectionState.CONNECTION_LINK_ESTABLISHED));
                }
                this.dispatchLocalEvent(new ConnectionStateChange(string, ConnectionStateChange.ConnectionState.CONNECTION_CONNECTED));
                if (this.mNetworkLinkReady && this.mNetworkReadyEventDispatched.compareAndSet(false, true)) {
                    this.dispatchLocalEvent(new NetworkLinkReady());
                    RemoteDevice.sLogger.d("dispatchStateChangeEvents: dispatching the Network Link Ready message");
                    break;
                }
                break;
            case DISCONNECTED:
                if (linkStatus2 == LinkStatus.CONNECTED) {
                    this.dispatchLocalEvent(new ConnectionStateChange(string, ConnectionStateChange.ConnectionState.CONNECTION_DISCONNECTED));
                }
                this.dispatchLocalEvent(new ConnectionStateChange(string, ConnectionStateChange.ConnectionState.CONNECTION_LINK_LOST));
                break;
            case LINK_ESTABLISHED:
                if (linkStatus2 == LinkStatus.CONNECTED) {
                    this.dispatchLocalEvent(new ConnectionStateChange(string, ConnectionStateChange.ConnectionState.CONNECTION_DISCONNECTED));
                    break;
                }
                this.dispatchLocalEvent(new ConnectionStateChange(string, ConnectionStateChange.ConnectionState.CONNECTION_LINK_ESTABLISHED));
                break;
        }
    }
    
    private void markDisconnected(final DisconnectCause disconnectCause) {
        this.removeActiveConnection();
        this.dispatchDisconnectEvent(disconnectCause);
    }
    
    private void setLinkStatus(final LinkStatus mLinkStatus) {
        if (this.mLinkStatus != mLinkStatus) {
            this.dispatchStateChangeEvents(this.mLinkStatus = mLinkStatus, this.mLinkStatus);
        }
    }
    
    public boolean connect() {
        boolean b = false;
        if (this.getConnectionStatus() == Status.DISCONNECTED) {
            if (this.mConnectionInfo == null) {
                RemoteDevice.sLogger.e("can't connect without a connectionInfo");
            }
            else if (this.setActiveConnection(Connection.instantiateFromConnectionInfo(this.mContext, this.mConnectionInfo))) {
                final Object lock = RemoteDevice.lock;
                synchronized (lock) {
                    int n;
                    if (this.mActiveConnection.connect() && this.getConnectionStatus() == Status.CONNECTING) {
                        n = 1;
                    }
                    else {
                        n = 0;
                    }
                    // monitorexit(lock)
                    if (n != 0) {
                        this.dispatchConnectingEvent();
                        RemoteDevice.sLogger.i("Starting connection");
                        b = true;
                        return b;
                    }
                }
                RemoteDevice.sLogger.e("Unable to connect");
                this.removeActiveConnection();
            }
            else {
                RemoteDevice.sLogger.e("unable to find connection of type: " + this.mConnectionInfo.getType());
            }
        }
        return b;
    }
    
    public boolean disconnect() {
        while (true) {
            synchronized (this) {
                boolean disconnect;
                if (this.getConnectionStatus() == Status.DISCONNECTED) {
                    RemoteDevice.sLogger.e("can't disconnect; already disconnected");
                    disconnect = true;
                }
                else {
                    final Object lock = RemoteDevice.lock;
                    synchronized (lock) {
                        final Connection mActiveConnection = this.mActiveConnection;
                        this.mActiveConnection = null;
                        // monitorexit(lock)
                        if (mActiveConnection != null) {
                            disconnect = mActiveConnection.disconnect();
                            return disconnect;
                        }
                        return false;
                    }
                }
                return disconnect;
            }
            return false;
        }
    }
    
    protected void dispatchConnectEvent() {
        this.dispatchToListeners((Listenable.EventDispatcher)new EventDispatcher() {
            public void dispatchEvent(final RemoteDevice remoteDevice, final RemoteDevice.Listener listener) {
                listener.onDeviceConnected(remoteDevice);
            }
        });
    }
    
    protected void dispatchConnectFailureEvent(final ConnectionFailureCause connectionFailureCause) {
        this.dispatchToListeners((Listenable.EventDispatcher)new EventDispatcher() {
            public void dispatchEvent(final RemoteDevice remoteDevice, final RemoteDevice.Listener listener) {
                listener.onDeviceConnectFailure(remoteDevice, connectionFailureCause);
            }
        });
    }
    
    protected void dispatchConnectingEvent() {
        this.dispatchToListeners((Listenable.EventDispatcher)new EventDispatcher() {
            public void dispatchEvent(final RemoteDevice remoteDevice, final RemoteDevice.Listener listener) {
                listener.onDeviceConnecting(remoteDevice);
            }
        });
    }
    
    protected void dispatchDisconnectEvent(final DisconnectCause disconnectCause) {
        this.dispatchToListeners((Listenable.EventDispatcher)new EventDispatcher() {
            public void dispatchEvent(final RemoteDevice remoteDevice, final RemoteDevice.Listener listener) {
                listener.onDeviceDisconnected(remoteDevice, disconnectCause);
            }
        });
    }
    
    protected void dispatchNavdyEvent(final NavdyEvent navdyEvent) {
        this.dispatchToListeners((Listenable.EventDispatcher)new EventDispatcher() {
            public void dispatchEvent(final RemoteDevice remoteDevice, final RemoteDevice.Listener listener) {
                listener.onNavdyEventReceived(remoteDevice, navdyEvent);
            }
        });
    }
    
    protected void dispatchNavdyEvent(final byte[] array) {
        this.dispatchToListeners((Listenable.EventDispatcher)new EventDispatcher() {
            public void dispatchEvent(final RemoteDevice remoteDevice, final RemoteDevice.Listener listener) {
                listener.onNavdyEventReceived(remoteDevice, array);
            }
        });
    }
    
    @Nullable
    public ConnectionInfo getActiveConnectionInfo() {
        final Object lock = RemoteDevice.lock;
        synchronized (lock) {
            ConnectionInfo connectionInfo;
            if (this.mActiveConnection != null) {
                connectionInfo = this.mActiveConnection.getConnectionInfo();
            }
            else {
                // monitorexit(lock)
                connectionInfo = null;
            }
            return connectionInfo;
        }
    }
    
    public ConnectionInfo getConnectionInfo() {
        return this.mConnectionInfo;
    }
    
    public Status getConnectionStatus() {
        final Object lock = RemoteDevice.lock;
        synchronized (lock) {
            Connection.Status status;
            if (this.mActiveConnection == null) {
                status = Status.DISCONNECTED;
            }
            else {
                status = this.mActiveConnection.getStatus();
            }
            // monitorexit(lock)
            return status;
        }
    }
    
    public NavdyDeviceId getDeviceId() {
        return this.mDeviceId;
    }
    
    public DeviceInfo getDeviceInfo() {
        return this.mDeviceInfo;
    }
    
    public int getLinkBandwidthLevel() {
        int bandWidthLevel;
        if (this.mLink != null && this.mLinkStatus == LinkStatus.CONNECTED) {
            bandWidthLevel = this.mLink.getBandWidthLevel();
        }
        else {
            bandWidthLevel = -1;
        }
        return bandWidthLevel;
    }
    
    public LinkStatus getLinkStatus() {
        return this.mLinkStatus;
    }
    
    public boolean isConnected() {
        return this.getConnectionStatus() == Status.CONNECTED;
    }
    
    public boolean isConnecting() {
        return this.getConnectionStatus() == Status.CONNECTING;
    }
    
    @Override
    public void linkEstablished(final ConnectionType connectionType) {
        while (true) {
            Label_0112: {
                synchronized (this) {
                    switch (connectionType) {
                        default:
                            RemoteDevice.sLogger.e("Unknown connection type: " + connectionType);
                            break;
                        case BT_PROTOBUF:
                        case TCP_PROTOBUF:
                        case EA_PROTOBUF:
                            this.setLinkStatus(LinkStatus.CONNECTED);
                            if (this.mLink != null) {
                                this.mLink.setBandwidthLevel(1);
                                break;
                            }
                            break;
                        case BT_IAP2_LINK:
                            break Label_0112;
                    }
                    return;
                }
            }
            this.setLinkStatus(LinkStatus.LINK_ESTABLISHED);
            if (this.mLink != null) {
                this.mLink.setBandwidthLevel(1);
            }
        }
    }
    
    @Override
    public void linkLost(final ConnectionType connectionType, DisconnectCause normal) {
        while (true) {
            Label_0144: {
                Label_0124: {
                    synchronized (this) {
                        if (this.getConnectionStatus() == Status.DISCONNECTED) {
                            normal = DisconnectCause.NORMAL;
                        }
                        this.mNetworkReadyEventDispatched.set(false);
                        this.mNetworkLinkReady = false;
                        switch (connectionType) {
                            default:
                                RemoteDevice.sLogger.e("Unknown connection type: " + connectionType);
                                break;
                            case BT_PROTOBUF:
                            case TCP_PROTOBUF:
                                this.setLinkStatus(LinkStatus.DISCONNECTED);
                                this.markDisconnected(normal);
                                break;
                            case EA_PROTOBUF:
                                break Label_0124;
                            case BT_IAP2_LINK:
                                break Label_0144;
                        }
                        return;
                    }
                }
                if (this.getLinkStatus() == LinkStatus.CONNECTED) {
                    this.setLinkStatus(LinkStatus.LINK_ESTABLISHED);
                    return;
                }
                return;
            }
            this.setLinkStatus(LinkStatus.DISCONNECTED);
            this.markDisconnected(normal);
        }
    }
    
    @Override
    public void onConnected(final Connection connection) {
        final Object lock = RemoteDevice.lock;
        synchronized (lock) {
            if (this.mActiveConnection == connection) {
                RemoteDevice.sLogger.i("Connected");
                this.dispatchConnectEvent();
            }
            else {
                RemoteDevice.sLogger.e("Received connect event for unknown connection");
            }
        }
    }
    
    @Override
    public void onConnectionFailed(final Connection connection, final ConnectionFailureCause connectionFailureCause) {
        final Object lock = RemoteDevice.lock;
        synchronized (lock) {
            if (this.mActiveConnection == connection) {
                this.removeActiveConnection();
            }
            // monitorexit(lock)
            RemoteDevice.sLogger.e("Connection failed: " + connectionFailureCause);
            this.dispatchConnectFailureEvent(connectionFailureCause);
        }
    }
    
    @Override
    public void onDisconnected(final Connection connection, final DisconnectCause disconnectCause) {
        while (true) {
            Object o = RemoteDevice.lock;
            while (true) {
                synchronized (o) {
                    if (this.mActiveConnection == connection) {
                        this.removeActiveConnection();
                    }
                    // monitorexit(o)
                    final Object connectionInfo = connection.getConnectionInfo();
                    o = RemoteDevice.sLogger;
                    final StringBuilder append = new StringBuilder().append("Disconnected ");
                    if (connectionInfo != null) {
                        ((Logger)o).i(append.append(connectionInfo).append(" - ").append(disconnectCause).toString());
                        this.dispatchDisconnectEvent(disconnectCause);
                        return;
                    }
                }
                final Object connectionInfo = "unknown";
                continue;
            }
        }
    }
    
    @Override
    public void onNavdyEventReceived(final byte[] p0) {
        // 
        This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: getfield        com/navdy/service/library/device/RemoteDevice.convertToNavdyEvent:Z
        //     4: ifeq            84
        //     7: aconst_null    
        //     8: astore_2       
        //     9: aload_0        
        //    10: getfield        com/navdy/service/library/device/RemoteDevice.mWire:Lcom/squareup/wire/Wire;
        //    13: aload_1        
        //    14: ldc             Lcom/navdy/service/library/events/NavdyEvent;.class
        //    16: invokevirtual   com/squareup/wire/Wire.parseFrom:([BLjava/lang/Class;)Lcom/squareup/wire/Message;
        //    19: checkcast       Lcom/navdy/service/library/events/NavdyEvent;
        //    22: astore_3       
        //    23: aload_3        
        //    24: astore_1       
        //    25: aload_1        
        //    26: ifnull          34
        //    29: aload_0        
        //    30: aload_1        
        //    31: invokevirtual   com/navdy/service/library/device/RemoteDevice.dispatchNavdyEvent:(Lcom/navdy/service/library/events/NavdyEvent;)V
        //    34: return         
        //    35: astore_3       
        //    36: iconst_m1      
        //    37: istore          4
        //    39: aload_1        
        //    40: invokestatic    com/navdy/service/library/events/WireUtil.getEventTypeIndex:([B)I
        //    43: istore          5
        //    45: getstatic       com/navdy/service/library/device/RemoteDevice.sLogger:Lcom/navdy/service/library/log/Logger;
        //    48: new             Ljava/lang/StringBuilder;
        //    51: dup            
        //    52: invokespecial   java/lang/StringBuilder.<init>:()V
        //    55: ldc_w           "Ignoring invalid navdy event["
        //    58: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    61: iload           5
        //    63: invokevirtual   java/lang/StringBuilder.append:(I)Ljava/lang/StringBuilder;
        //    66: ldc_w           "]"
        //    69: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    72: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //    75: aload_3        
        //    76: invokevirtual   com/navdy/service/library/log/Logger.e:(Ljava/lang/String;Ljava/lang/Throwable;)V
        //    79: aload_2        
        //    80: astore_1       
        //    81: goto            25
        //    84: aload_0        
        //    85: aload_1        
        //    86: invokevirtual   com/navdy/service/library/device/RemoteDevice.dispatchNavdyEvent:([B)V
        //    89: goto            34
        //    92: astore_1       
        //    93: iload           4
        //    95: istore          5
        //    97: goto            45
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  9      23     35     84     Ljava/lang/Throwable;
        //  39     45     92     100    Ljava/lang/Throwable;
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0045:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    @Override
    public void onNetworkLinkReady() {
        this.mNetworkLinkReady = true;
        if (this.getLinkStatus() == LinkStatus.CONNECTED) {
            if (this.mNetworkReadyEventDispatched.compareAndSet(false, true)) {
                this.dispatchLocalEvent(new NetworkLinkReady());
                RemoteDevice.sLogger.d("onNetworkLinkReady: dispatching the Network Link Ready message");
            }
        }
        else {
            RemoteDevice.sLogger.d("Network Link is ready, but state is not connected");
        }
    }
    
    public boolean postEvent(final NavdyEvent navdyEvent) {
        return this.postEvent(navdyEvent, null);
    }
    
    public boolean postEvent(final NavdyEvent navdyEvent, final PostEventHandler postEventHandler) {
        return this.postEvent(navdyEvent.toByteArray(), postEventHandler);
    }
    
    public boolean postEvent(final Message message) {
        return this.postEvent(NavdyEventUtil.eventFromMessage(message), null);
    }
    
    public boolean postEvent(final byte[] array) {
        return this.postEvent(array, null);
    }
    
    public boolean postEvent(final byte[] array, final PostEventHandler postEventHandler) {
        final boolean add = this.mEventQueue.add(new EventRequest(this.mHandler, array, postEventHandler));
        this.lastMessageSentTime = SystemClock.elapsedRealtime();
        if (RemoteDevice.sLogger.isLoggable(2)) {
            RemoteDevice.sLogger.v("NAVDY-PACKET Event queue size [" + this.mEventQueue.size() + "]");
        }
        return add;
    }
    
    public void removeActiveConnection() {
        final Object lock = RemoteDevice.lock;
        synchronized (lock) {
            if (this.mActiveConnection != null) {
                this.mActiveConnection.removeListener((Connection.Listener)this);
                this.mActiveConnection = null;
            }
        }
    }
    
    public boolean setActiveConnection(final Connection mActiveConnection) {
        final Object lock = RemoteDevice.lock;
        synchronized (lock) {
            this.mActiveConnection = mActiveConnection;
            if (this.mActiveConnection != null) {
                this.mActiveConnection.addListener((Connection.Listener)this);
                this.mConnectionInfo = this.mActiveConnection.getConnectionInfo();
            }
            if (this.isConnected()) {
                this.dispatchConnectEvent();
            }
            return this.mActiveConnection != null;
        }
    }
    
    public void setDeviceInfo(final DeviceInfo mDeviceInfo) {
        this.mDeviceInfo = mDeviceInfo;
        if (mDeviceInfo != null) {
            RemoteDevice.sLastSeenDeviceInfo.mDeviceInfo = mDeviceInfo;
        }
    }
    
    public void setLinkBandwidthLevel(final int bandwidthLevel) {
        if (this.mLink != null && this.mLinkStatus == LinkStatus.CONNECTED) {
            this.mLink.setBandwidthLevel(bandwidthLevel);
        }
    }
    
    public boolean startLink() {
        if (this.mLink != null) {
            throw new IllegalStateException("Link already started");
        }
        while (true) {
            RemoteDevice.sLogger.i("Starting link");
            this.mLink = LinkManager.build(this.getConnectionInfo());
            final SocketAdapter socketAdapter = null;
            // monitorenter(lock = RemoteDevice.lock)
            SocketAdapter socket = socketAdapter;
            try {
                if (this.mLink != null) {
                    socket = socketAdapter;
                    if (this.mActiveConnection != null) {
                        socket = this.mActiveConnection.getSocket();
                    }
                }
                // monitorexit(lock)
                if (socket != null) {
                    return this.mLink.start(socket, this.mEventQueue, this);
                }
            }
            finally {
            }
            // monitorexit(lock)
            return false;
        }
    }
    
    public boolean stopLink() {
        RemoteDevice.sLogger.i("Stopping link");
        if (this.mLink != null) {
            RemoteDevice.sLogger.i("Closing link");
            this.mNetworkReadyEventDispatched.set(false);
            this.mNetworkLinkReady = false;
            this.mLink.close();
            this.mLink = null;
            this.mEventQueue.clear();
        }
        return true;
    }
    
    protected interface EventDispatcher extends Listenable.EventDispatcher<RemoteDevice, RemoteDevice.Listener>
    {
    }
    
    public static class LastSeenDeviceInfo
    {
        private DeviceInfo mDeviceInfo;
        
        public DeviceInfo getDeviceInfo() {
            return this.mDeviceInfo;
        }
    }
    
    public enum LinkStatus
    {
        CONNECTED, 
        DISCONNECTED, 
        LINK_ESTABLISHED;
    }
    
    public interface Listener extends Listenable.Listener
    {
        void onDeviceConnectFailure(final RemoteDevice p0, final ConnectionFailureCause p1);
        
        void onDeviceConnected(final RemoteDevice p0);
        
        void onDeviceConnecting(final RemoteDevice p0);
        
        void onDeviceDisconnected(final RemoteDevice p0, final DisconnectCause p1);
        
        void onNavdyEventReceived(final RemoteDevice p0, final NavdyEvent p1);
        
        void onNavdyEventReceived(final RemoteDevice p0, final byte[] p1);
    }
    
    public interface PostEventHandler
    {
        void onComplete(final PostEventStatus p0);
    }
    
    public enum PostEventStatus
    {
        DISCONNECTED, 
        SEND_FAILED, 
        SUCCESS, 
        UNKNOWN_FAILURE;
    }
}
