package com.navdy.service.library.device.connection;

import java.util.UUID;
import com.navdy.service.library.device.NavdyDeviceId;
import com.google.gson.annotations.SerializedName;

public class BTConnectionInfo extends ConnectionInfo
{
    @SerializedName("address")
    ServiceAddress mAddress;
    
    public BTConnectionInfo(final NavdyDeviceId navdyDeviceId, final ServiceAddress mAddress, final ConnectionType connectionType) {
        super(navdyDeviceId, connectionType);
        this.mAddress = mAddress;
    }
    
    public BTConnectionInfo(final NavdyDeviceId navdyDeviceId, final UUID uuid, final ConnectionType connectionType) {
        super(navdyDeviceId, connectionType);
        if (navdyDeviceId.getBluetoothAddress() != null) {
            this.mAddress = new ServiceAddress(navdyDeviceId.getBluetoothAddress(), uuid.toString());
            return;
        }
        throw new IllegalArgumentException("BT address missing");
    }
    
    @Override
    public boolean equals(final Object o) {
        final boolean b = false;
        boolean equals;
        if (this == o) {
            equals = true;
        }
        else {
            equals = b;
            if (o != null) {
                equals = b;
                if (this.getClass() == o.getClass()) {
                    equals = b;
                    if (super.equals(o)) {
                        equals = this.mAddress.equals(((BTConnectionInfo)o).mAddress);
                    }
                }
            }
        }
        return equals;
    }
    
    @Override
    public ServiceAddress getAddress() {
        return this.mAddress;
    }
    
    @Override
    public int hashCode() {
        return super.hashCode() * 31 + this.mAddress.hashCode();
    }
}
