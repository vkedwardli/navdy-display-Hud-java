package com.navdy.service.library.util;

import java.io.OutputStream;
import android.graphics.Bitmap;
import java.io.ByteArrayOutputStream;
import android.content.res.Resources;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory;
import android.graphics.Paint;
import android.graphics.Canvas;
import android.graphics.Bitmap;
import android.graphics.Bitmap;
import android.graphics.Rect;

public class ScalingUtilities
{
    public static Rect calculateDstRect(final int n, final int n2, final int n3, final int n4, final ScalingLogic scalingLogic) {
        Rect rect;
        if (scalingLogic == ScalingLogic.FIT) {
            final float n5 = n / n2;
            if (n5 > n3 / n4) {
                rect = new Rect(0, 0, n3, (int)(n3 / n5));
            }
            else {
                rect = new Rect(0, 0, (int)(n4 * n5), n4);
            }
        }
        else {
            rect = new Rect(0, 0, n3, n4);
        }
        return rect;
    }
    
    public static int calculateSampleSize(int n, final int n2, final int n3, final int n4, final ScalingLogic scalingLogic) {
        if (scalingLogic == ScalingLogic.FIT) {
            if (n / n2 > n3 / n4) {
                n /= n3;
            }
            else {
                n = n2 / n4;
            }
        }
        else if (n / n2 > n3 / n4) {
            n = n2 / n4;
        }
        else {
            n /= n3;
        }
        return n;
    }
    
    public static Rect calculateSrcRect(int n, int n2, int n3, final int n4, final ScalingLogic scalingLogic) {
        Rect rect;
        if (scalingLogic == ScalingLogic.CROP) {
            final float n5 = n / n2;
            final float n6 = n3 / n4;
            if (n5 > n6) {
                n3 = (int)(n2 * n6);
                n = (n - n3) / 2;
                rect = new Rect(n, 0, n + n3, n2);
            }
            else {
                n3 = (int)(n / n6);
                n2 = (n2 - n3) / 2;
                rect = new Rect(0, n2, n, n2 + n3);
            }
        }
        else {
            rect = new Rect(0, 0, n, n2);
        }
        return rect;
    }
    
    @Deprecated
    public static Bitmap createScaledBitmap(Bitmap bitmap, final int n, final int n2, final ScalingLogic scalingLogic) {
        if (bitmap.getHeight() != n2 || bitmap.getWidth() != n) {
            final Rect calculateSrcRect = calculateSrcRect(bitmap.getWidth(), bitmap.getHeight(), n, n2, scalingLogic);
            final Rect calculateDstRect = calculateDstRect(bitmap.getWidth(), bitmap.getHeight(), n, n2, scalingLogic);
            final Bitmap bitmap2 = Bitmap.createBitmap(calculateDstRect.width(), calculateDstRect.height(), Bitmap$Config.ARGB_8888);
            new Canvas(bitmap2).drawBitmap(bitmap, calculateSrcRect, calculateDstRect, new Paint(2));
            bitmap = bitmap2;
        }
        return bitmap;
    }
    
    public static Bitmap createScaledBitmapAndRecycleOriginalIfScaled(Bitmap bitmap, final int n, final int n2, final ScalingLogic scalingLogic) {
        if (bitmap.getHeight() != n2 || bitmap.getWidth() != n) {
            final Rect calculateSrcRect = calculateSrcRect(bitmap.getWidth(), bitmap.getHeight(), n, n2, scalingLogic);
            final Rect calculateDstRect = calculateDstRect(bitmap.getWidth(), bitmap.getHeight(), n, n2, scalingLogic);
            final Bitmap bitmap2 = Bitmap.createBitmap(calculateDstRect.width(), calculateDstRect.height(), Bitmap$Config.ARGB_8888);
            new Canvas(bitmap2).drawBitmap(bitmap, calculateSrcRect, calculateDstRect, new Paint(2));
            bitmap.recycle();
            bitmap = bitmap2;
        }
        return bitmap;
    }
    
    public static Bitmap decodeByteArray(final byte[] array, final int n, final int n2, final ScalingLogic scalingLogic) {
        final BitmapFactory$Options bitmapFactory$Options = new BitmapFactory$Options();
        bitmapFactory$Options.inJustDecodeBounds = true;
        BitmapFactory.decodeByteArray(array, 0, array.length, bitmapFactory$Options);
        BitmapFactory$Options bitmapFactory$Options2;
        if (bitmapFactory$Options.outWidth == n && bitmapFactory$Options.outHeight == n2) {
            bitmapFactory$Options2 = null;
        }
        else {
            bitmapFactory$Options.inJustDecodeBounds = false;
            bitmapFactory$Options.inSampleSize = calculateSampleSize(bitmapFactory$Options.outWidth, bitmapFactory$Options.outHeight, n, n2, scalingLogic);
            bitmapFactory$Options2 = bitmapFactory$Options;
        }
        return BitmapFactory.decodeByteArray(array, 0, array.length, bitmapFactory$Options2);
    }
    
    public static Bitmap decodeResource(final Resources resources, final int n, final int n2, final int n3, final ScalingLogic scalingLogic) {
        final BitmapFactory$Options bitmapFactory$Options = new BitmapFactory$Options();
        bitmapFactory$Options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(resources, n, bitmapFactory$Options);
        bitmapFactory$Options.inJustDecodeBounds = false;
        bitmapFactory$Options.inSampleSize = calculateSampleSize(bitmapFactory$Options.outWidth, bitmapFactory$Options.outHeight, n2, n3, scalingLogic);
        return BitmapFactory.decodeResource(resources, n, bitmapFactory$Options);
    }
    
    public static byte[] encodeByteArray(final Bitmap bitmap) {
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap$CompressFormat.PNG, 100, (OutputStream)byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }
    
    public enum ScalingLogic
    {
        CROP, 
        FIT;
    }
}
