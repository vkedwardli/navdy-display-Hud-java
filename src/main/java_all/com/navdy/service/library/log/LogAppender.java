package com.navdy.service.library.log;

public interface LogAppender
{
    void close();
    
    void d(final String p0, final String p1);
    
    void d(final String p0, final String p1, final Throwable p2);
    
    void e(final String p0, final String p1);
    
    void e(final String p0, final String p1, final Throwable p2);
    
    void flush();
    
    void i(final String p0, final String p1);
    
    void i(final String p0, final String p1, final Throwable p2);
    
    void v(final String p0, final String p1);
    
    void v(final String p0, final String p1, final Throwable p2);
    
    void w(final String p0, final String p1);
    
    void w(final String p0, final String p1, final Throwable p2);
}
