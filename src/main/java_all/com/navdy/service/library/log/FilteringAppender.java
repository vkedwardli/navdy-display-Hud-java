package com.navdy.service.library.log;

public class FilteringAppender implements LogAppender
{
    private LogAppender baseAppender;
    private Filter filter;
    
    public FilteringAppender(final LogAppender baseAppender, final Filter filter) {
        this.baseAppender = baseAppender;
        this.filter = filter;
    }
    
    @Override
    public void close() {
        this.baseAppender.close();
    }
    
    @Override
    public void d(final String s, final String s2) {
        if (this.filter.matches(3, s, s2)) {
            this.baseAppender.d(s, s2);
        }
    }
    
    @Override
    public void d(final String s, final String s2, final Throwable t) {
        if (this.filter.matches(3, s, s2)) {
            this.baseAppender.d(s, s2, t);
        }
    }
    
    @Override
    public void e(final String s, final String s2) {
        if (this.filter.matches(6, s, s2)) {
            this.baseAppender.e(s, s2);
        }
    }
    
    @Override
    public void e(final String s, final String s2, final Throwable t) {
        if (this.filter.matches(6, s, s2)) {
            this.baseAppender.e(s, s2, t);
        }
    }
    
    @Override
    public void flush() {
        this.baseAppender.flush();
    }
    
    @Override
    public void i(final String s, final String s2) {
        if (this.filter.matches(4, s, s2)) {
            this.baseAppender.i(s, s2);
        }
    }
    
    @Override
    public void i(final String s, final String s2, final Throwable t) {
        if (this.filter.matches(4, s, s2)) {
            this.baseAppender.i(s, s2, t);
        }
    }
    
    @Override
    public void v(final String s, final String s2) {
        if (this.filter.matches(2, s, s2)) {
            this.baseAppender.v(s, s2);
        }
    }
    
    @Override
    public void v(final String s, final String s2, final Throwable t) {
        if (this.filter.matches(2, s, s2)) {
            this.baseAppender.v(s, s2, t);
        }
    }
    
    @Override
    public void w(final String s, final String s2) {
        if (this.filter.matches(5, s, s2)) {
            this.baseAppender.w(s, s2);
        }
    }
    
    @Override
    public void w(final String s, final String s2, final Throwable t) {
        if (this.filter.matches(5, s, s2)) {
            this.baseAppender.w(s, s2, t);
        }
    }
}
