package com.navdy.service.library.events.audio;

import com.squareup.wire.ProtoField;
import com.squareup.wire.Message;

public final class MusicCollectionFilter extends Message
{
    public static final String DEFAULT_FIELD = "";
    public static final String DEFAULT_VALUE = "";
    private static final long serialVersionUID = 0L;
    @ProtoField(tag = 1, type = Datatype.STRING)
    public final String field;
    @ProtoField(tag = 2, type = Datatype.STRING)
    public final String value;
    
    private MusicCollectionFilter(final Builder builder) {
        this(builder.field, builder.value);
        this.setBuilder((Message.Builder)builder);
    }
    
    public MusicCollectionFilter(final String field, final String value) {
        this.field = field;
        this.value = value;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o != this) {
            if (!(o instanceof MusicCollectionFilter)) {
                b = false;
            }
            else {
                final MusicCollectionFilter musicCollectionFilter = (MusicCollectionFilter)o;
                if (!this.equals(this.field, musicCollectionFilter.field) || !this.equals(this.value, musicCollectionFilter.value)) {
                    b = false;
                }
            }
        }
        return b;
    }
    
    @Override
    public int hashCode() {
        int hashCode = 0;
        int hashCode2;
        if ((hashCode2 = this.hashCode) == 0) {
            int hashCode3;
            if (this.field != null) {
                hashCode3 = this.field.hashCode();
            }
            else {
                hashCode3 = 0;
            }
            if (this.value != null) {
                hashCode = this.value.hashCode();
            }
            hashCode2 = hashCode3 * 37 + hashCode;
            this.hashCode = hashCode2;
        }
        return hashCode2;
    }
    
    public static final class Builder extends Message.Builder<MusicCollectionFilter>
    {
        public String field;
        public String value;
        
        public Builder() {
        }
        
        public Builder(final MusicCollectionFilter musicCollectionFilter) {
            super(musicCollectionFilter);
            if (musicCollectionFilter != null) {
                this.field = musicCollectionFilter.field;
                this.value = musicCollectionFilter.value;
            }
        }
        
        public MusicCollectionFilter build() {
            return new MusicCollectionFilter(this, null);
        }
        
        public Builder field(final String field) {
            this.field = field;
            return this;
        }
        
        public Builder value(final String value) {
            this.value = value;
            return this;
        }
    }
}
