package com.navdy.service.library.events.connection;

import com.squareup.wire.ProtoEnum;
import com.squareup.wire.ProtoField;
import com.squareup.wire.Message;

public final class ConnectionRequest extends Message
{
    public static final Action DEFAULT_ACTION;
    public static final String DEFAULT_REMOTEDEVICEID = "";
    private static final long serialVersionUID = 0L;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.ENUM)
    public final Action action;
    @ProtoField(tag = 2, type = Datatype.STRING)
    public final String remoteDeviceId;
    
    static {
        DEFAULT_ACTION = Action.CONNECTION_START_SEARCH;
    }
    
    public ConnectionRequest(final Action action, final String remoteDeviceId) {
        this.action = action;
        this.remoteDeviceId = remoteDeviceId;
    }
    
    private ConnectionRequest(final Builder builder) {
        this(builder.action, builder.remoteDeviceId);
        this.setBuilder((Message.Builder)builder);
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o != this) {
            if (!(o instanceof ConnectionRequest)) {
                b = false;
            }
            else {
                final ConnectionRequest connectionRequest = (ConnectionRequest)o;
                if (!this.equals(this.action, connectionRequest.action) || !this.equals(this.remoteDeviceId, connectionRequest.remoteDeviceId)) {
                    b = false;
                }
            }
        }
        return b;
    }
    
    @Override
    public int hashCode() {
        int hashCode = 0;
        int hashCode2;
        if ((hashCode2 = this.hashCode) == 0) {
            int hashCode3;
            if (this.action != null) {
                hashCode3 = this.action.hashCode();
            }
            else {
                hashCode3 = 0;
            }
            if (this.remoteDeviceId != null) {
                hashCode = this.remoteDeviceId.hashCode();
            }
            hashCode2 = hashCode3 * 37 + hashCode;
            this.hashCode = hashCode2;
        }
        return hashCode2;
    }
    
    public enum Action implements ProtoEnum
    {
        CONNECTION_SELECT(3), 
        CONNECTION_START_SEARCH(1), 
        CONNECTION_STOP_SEARCH(2);
        
        private final int value;
        
        private Action(final int value) {
            this.value = value;
        }
        
        @Override
        public int getValue() {
            return this.value;
        }
    }
    
    public static final class Builder extends Message.Builder<ConnectionRequest>
    {
        public Action action;
        public String remoteDeviceId;
        
        public Builder() {
        }
        
        public Builder(final ConnectionRequest connectionRequest) {
            super(connectionRequest);
            if (connectionRequest != null) {
                this.action = connectionRequest.action;
                this.remoteDeviceId = connectionRequest.remoteDeviceId;
            }
        }
        
        public Builder action(final Action action) {
            this.action = action;
            return this;
        }
        
        public ConnectionRequest build() {
            ((Message.Builder)this).checkRequiredFields();
            return new ConnectionRequest(this, null);
        }
        
        public Builder remoteDeviceId(final String remoteDeviceId) {
            this.remoteDeviceId = remoteDeviceId;
            return this;
        }
    }
}
