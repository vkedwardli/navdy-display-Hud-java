package com.navdy.service.library.events.debug;

import com.squareup.wire.ProtoField;
import com.squareup.wire.Message;

public final class StartDrivePlaybackEvent extends Message
{
    public static final String DEFAULT_LABEL = "";
    public static final Boolean DEFAULT_PLAYSECONDARYLOCATION;
    private static final long serialVersionUID = 0L;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.STRING)
    public final String label;
    @ProtoField(tag = 2, type = Datatype.BOOL)
    public final Boolean playSecondaryLocation;
    
    static {
        DEFAULT_PLAYSECONDARYLOCATION = false;
    }
    
    private StartDrivePlaybackEvent(final Builder builder) {
        this(builder.label, builder.playSecondaryLocation);
        this.setBuilder((Message.Builder)builder);
    }
    
    public StartDrivePlaybackEvent(final String label, final Boolean playSecondaryLocation) {
        this.label = label;
        this.playSecondaryLocation = playSecondaryLocation;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o != this) {
            if (!(o instanceof StartDrivePlaybackEvent)) {
                b = false;
            }
            else {
                final StartDrivePlaybackEvent startDrivePlaybackEvent = (StartDrivePlaybackEvent)o;
                if (!this.equals(this.label, startDrivePlaybackEvent.label) || !this.equals(this.playSecondaryLocation, startDrivePlaybackEvent.playSecondaryLocation)) {
                    b = false;
                }
            }
        }
        return b;
    }
    
    @Override
    public int hashCode() {
        int hashCode = 0;
        int hashCode2;
        if ((hashCode2 = this.hashCode) == 0) {
            int hashCode3;
            if (this.label != null) {
                hashCode3 = this.label.hashCode();
            }
            else {
                hashCode3 = 0;
            }
            if (this.playSecondaryLocation != null) {
                hashCode = this.playSecondaryLocation.hashCode();
            }
            hashCode2 = hashCode3 * 37 + hashCode;
            this.hashCode = hashCode2;
        }
        return hashCode2;
    }
    
    public static final class Builder extends Message.Builder<StartDrivePlaybackEvent>
    {
        public String label;
        public Boolean playSecondaryLocation;
        
        public Builder() {
        }
        
        public Builder(final StartDrivePlaybackEvent startDrivePlaybackEvent) {
            super(startDrivePlaybackEvent);
            if (startDrivePlaybackEvent != null) {
                this.label = startDrivePlaybackEvent.label;
                this.playSecondaryLocation = startDrivePlaybackEvent.playSecondaryLocation;
            }
        }
        
        public StartDrivePlaybackEvent build() {
            ((Message.Builder)this).checkRequiredFields();
            return new StartDrivePlaybackEvent(this, null);
        }
        
        public Builder label(final String label) {
            this.label = label;
            return this;
        }
        
        public Builder playSecondaryLocation(final Boolean playSecondaryLocation) {
            this.playSecondaryLocation = playSecondaryLocation;
            return this;
        }
    }
}
