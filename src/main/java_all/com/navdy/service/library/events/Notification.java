package com.navdy.service.library.events;

import com.squareup.wire.ProtoField;
import com.squareup.wire.Message;

public final class Notification extends Message
{
    public static final String DEFAULT_MESSAGE_DATA = "";
    public static final String DEFAULT_ORIGIN = "";
    public static final String DEFAULT_SENDER = "";
    public static final Integer DEFAULT_SMS_CLASS;
    public static final String DEFAULT_TARGET = "";
    public static final String DEFAULT_TYPE = "";
    private static final long serialVersionUID = 0L;
    @ProtoField(tag = 5, type = Datatype.STRING)
    public final String message_data;
    @ProtoField(tag = 2, type = Datatype.STRING)
    public final String origin;
    @ProtoField(tag = 3, type = Datatype.STRING)
    public final String sender;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.INT32)
    public final Integer sms_class;
    @ProtoField(tag = 4, type = Datatype.STRING)
    public final String target;
    @ProtoField(tag = 6, type = Datatype.STRING)
    public final String type;
    
    static {
        DEFAULT_SMS_CLASS = 0;
    }
    
    private Notification(final Builder builder) {
        this(builder.sms_class, builder.origin, builder.sender, builder.target, builder.message_data, builder.type);
        this.setBuilder((Message.Builder)builder);
    }
    
    public Notification(final Integer sms_class, final String origin, final String sender, final String target, final String message_data, final String type) {
        this.sms_class = sms_class;
        this.origin = origin;
        this.sender = sender;
        this.target = target;
        this.message_data = message_data;
        this.type = type;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o != this) {
            if (!(o instanceof Notification)) {
                b = false;
            }
            else {
                final Notification notification = (Notification)o;
                if (!this.equals(this.sms_class, notification.sms_class) || !this.equals(this.origin, notification.origin) || !this.equals(this.sender, notification.sender) || !this.equals(this.target, notification.target) || !this.equals(this.message_data, notification.message_data) || !this.equals(this.type, notification.type)) {
                    b = false;
                }
            }
        }
        return b;
    }
    
    @Override
    public int hashCode() {
        int hashCode = 0;
        int hashCode2;
        if ((hashCode2 = this.hashCode) == 0) {
            int hashCode3;
            if (this.sms_class != null) {
                hashCode3 = this.sms_class.hashCode();
            }
            else {
                hashCode3 = 0;
            }
            int hashCode4;
            if (this.origin != null) {
                hashCode4 = this.origin.hashCode();
            }
            else {
                hashCode4 = 0;
            }
            int hashCode5;
            if (this.sender != null) {
                hashCode5 = this.sender.hashCode();
            }
            else {
                hashCode5 = 0;
            }
            int hashCode6;
            if (this.target != null) {
                hashCode6 = this.target.hashCode();
            }
            else {
                hashCode6 = 0;
            }
            int hashCode7;
            if (this.message_data != null) {
                hashCode7 = this.message_data.hashCode();
            }
            else {
                hashCode7 = 0;
            }
            if (this.type != null) {
                hashCode = this.type.hashCode();
            }
            hashCode2 = ((((hashCode3 * 37 + hashCode4) * 37 + hashCode5) * 37 + hashCode6) * 37 + hashCode7) * 37 + hashCode;
            this.hashCode = hashCode2;
        }
        return hashCode2;
    }
    
    public static final class Builder extends Message.Builder<Notification>
    {
        public String message_data;
        public String origin;
        public String sender;
        public Integer sms_class;
        public String target;
        public String type;
        
        public Builder() {
        }
        
        public Builder(final Notification notification) {
            super(notification);
            if (notification != null) {
                this.sms_class = notification.sms_class;
                this.origin = notification.origin;
                this.sender = notification.sender;
                this.target = notification.target;
                this.message_data = notification.message_data;
                this.type = notification.type;
            }
        }
        
        public Notification build() {
            ((Message.Builder)this).checkRequiredFields();
            return new Notification(this, null);
        }
        
        public Builder message_data(final String message_data) {
            this.message_data = message_data;
            return this;
        }
        
        public Builder origin(final String origin) {
            this.origin = origin;
            return this;
        }
        
        public Builder sender(final String sender) {
            this.sender = sender;
            return this;
        }
        
        public Builder sms_class(final Integer sms_class) {
            this.sms_class = sms_class;
            return this;
        }
        
        public Builder target(final String target) {
            this.target = target;
            return this;
        }
        
        public Builder type(final String type) {
            this.type = type;
            return this;
        }
    }
}
