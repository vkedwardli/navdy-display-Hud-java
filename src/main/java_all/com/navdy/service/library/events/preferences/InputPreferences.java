package com.navdy.service.library.events.preferences;

import com.squareup.wire.ProtoEnum;
import com.squareup.wire.ProtoField;
import com.squareup.wire.Message;

public final class InputPreferences extends Message
{
    public static final DialSensitivity DEFAULT_DIAL_SENSITIVITY;
    public static final Long DEFAULT_SERIAL_NUMBER;
    public static final Boolean DEFAULT_USE_GESTURES;
    private static final long serialVersionUID = 0L;
    @ProtoField(tag = 3, type = Datatype.ENUM)
    public final DialSensitivity dial_sensitivity;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.INT64)
    public final Long serial_number;
    @ProtoField(tag = 2, type = Datatype.BOOL)
    public final Boolean use_gestures;
    
    static {
        DEFAULT_SERIAL_NUMBER = 0L;
        DEFAULT_USE_GESTURES = true;
        DEFAULT_DIAL_SENSITIVITY = DialSensitivity.DIAL_SENSITIVITY_STANDARD;
    }
    
    private InputPreferences(final Builder builder) {
        this(builder.serial_number, builder.use_gestures, builder.dial_sensitivity);
        this.setBuilder((Message.Builder)builder);
    }
    
    public InputPreferences(final Long serial_number, final Boolean use_gestures, final DialSensitivity dial_sensitivity) {
        this.serial_number = serial_number;
        this.use_gestures = use_gestures;
        this.dial_sensitivity = dial_sensitivity;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o != this) {
            if (!(o instanceof InputPreferences)) {
                b = false;
            }
            else {
                final InputPreferences inputPreferences = (InputPreferences)o;
                if (!this.equals(this.serial_number, inputPreferences.serial_number) || !this.equals(this.use_gestures, inputPreferences.use_gestures) || !this.equals(this.dial_sensitivity, inputPreferences.dial_sensitivity)) {
                    b = false;
                }
            }
        }
        return b;
    }
    
    @Override
    public int hashCode() {
        int hashCode = 0;
        int hashCode2;
        if ((hashCode2 = this.hashCode) == 0) {
            int hashCode3;
            if (this.serial_number != null) {
                hashCode3 = this.serial_number.hashCode();
            }
            else {
                hashCode3 = 0;
            }
            int hashCode4;
            if (this.use_gestures != null) {
                hashCode4 = this.use_gestures.hashCode();
            }
            else {
                hashCode4 = 0;
            }
            if (this.dial_sensitivity != null) {
                hashCode = this.dial_sensitivity.hashCode();
            }
            hashCode2 = (hashCode3 * 37 + hashCode4) * 37 + hashCode;
            this.hashCode = hashCode2;
        }
        return hashCode2;
    }
    
    public static final class Builder extends Message.Builder<InputPreferences>
    {
        public DialSensitivity dial_sensitivity;
        public Long serial_number;
        public Boolean use_gestures;
        
        public Builder() {
        }
        
        public Builder(final InputPreferences inputPreferences) {
            super(inputPreferences);
            if (inputPreferences != null) {
                this.serial_number = inputPreferences.serial_number;
                this.use_gestures = inputPreferences.use_gestures;
                this.dial_sensitivity = inputPreferences.dial_sensitivity;
            }
        }
        
        public InputPreferences build() {
            ((Message.Builder)this).checkRequiredFields();
            return new InputPreferences(this, null);
        }
        
        public Builder dial_sensitivity(final DialSensitivity dial_sensitivity) {
            this.dial_sensitivity = dial_sensitivity;
            return this;
        }
        
        public Builder serial_number(final Long serial_number) {
            this.serial_number = serial_number;
            return this;
        }
        
        public Builder use_gestures(final Boolean use_gestures) {
            this.use_gestures = use_gestures;
            return this;
        }
    }
    
    public enum DialSensitivity implements ProtoEnum
    {
        DIAL_SENSITIVITY_LOW(1), 
        DIAL_SENSITIVITY_STANDARD(0);
        
        private final int value;
        
        private DialSensitivity(final int value) {
            this.value = value;
        }
        
        @Override
        public int getValue() {
            return this.value;
        }
    }
}
