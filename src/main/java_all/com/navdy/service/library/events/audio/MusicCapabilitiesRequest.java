package com.navdy.service.library.events.audio;

import com.squareup.wire.Message;

public final class MusicCapabilitiesRequest extends Message
{
    private static final long serialVersionUID = 0L;
    
    public MusicCapabilitiesRequest() {
    }
    
    private MusicCapabilitiesRequest(final Builder builder) {
        this.setBuilder((Message.Builder)builder);
    }
    
    @Override
    public boolean equals(final Object o) {
        return o instanceof MusicCapabilitiesRequest;
    }
    
    @Override
    public int hashCode() {
        return 0;
    }
    
    public static final class Builder extends Message.Builder<MusicCapabilitiesRequest>
    {
        public Builder() {
        }
        
        public Builder(final MusicCapabilitiesRequest musicCapabilitiesRequest) {
            super(musicCapabilitiesRequest);
        }
        
        public MusicCapabilitiesRequest build() {
            return new MusicCapabilitiesRequest(this, null);
        }
    }
}
