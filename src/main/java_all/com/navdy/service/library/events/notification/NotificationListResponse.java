package com.navdy.service.library.events.notification;

import java.util.Collections;
import com.squareup.wire.ProtoField;
import com.navdy.service.library.events.RequestStatus;
import java.util.List;
import com.squareup.wire.Message;

public final class NotificationListResponse extends Message
{
    public static final List<String> DEFAULT_IDS;
    public static final RequestStatus DEFAULT_STATUS;
    public static final String DEFAULT_STATUS_DETAIL = "";
    private static final long serialVersionUID = 0L;
    @ProtoField(label = Label.REPEATED, tag = 3, type = Datatype.STRING)
    public final List<String> ids;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.ENUM)
    public final RequestStatus status;
    @ProtoField(tag = 2, type = Datatype.STRING)
    public final String status_detail;
    
    static {
        DEFAULT_STATUS = RequestStatus.REQUEST_SUCCESS;
        DEFAULT_IDS = Collections.<String>emptyList();
    }
    
    public NotificationListResponse(final RequestStatus status, final String status_detail, final List<String> list) {
        this.status = status;
        this.status_detail = status_detail;
        this.ids = Message.<String>immutableCopyOf(list);
    }
    
    private NotificationListResponse(final Builder builder) {
        this(builder.status, builder.status_detail, builder.ids);
        this.setBuilder((Message.Builder)builder);
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o != this) {
            if (!(o instanceof NotificationListResponse)) {
                b = false;
            }
            else {
                final NotificationListResponse notificationListResponse = (NotificationListResponse)o;
                if (!this.equals(this.status, notificationListResponse.status) || !this.equals(this.status_detail, notificationListResponse.status_detail) || !this.equals(this.ids, notificationListResponse.ids)) {
                    b = false;
                }
            }
        }
        return b;
    }
    
    @Override
    public int hashCode() {
        int hashCode = 0;
        int hashCode2;
        if ((hashCode2 = this.hashCode) == 0) {
            int hashCode3;
            if (this.status != null) {
                hashCode3 = this.status.hashCode();
            }
            else {
                hashCode3 = 0;
            }
            if (this.status_detail != null) {
                hashCode = this.status_detail.hashCode();
            }
            int hashCode4;
            if (this.ids != null) {
                hashCode4 = this.ids.hashCode();
            }
            else {
                hashCode4 = 1;
            }
            hashCode2 = (hashCode3 * 37 + hashCode) * 37 + hashCode4;
            this.hashCode = hashCode2;
        }
        return hashCode2;
    }
    
    public static final class Builder extends Message.Builder<NotificationListResponse>
    {
        public List<String> ids;
        public RequestStatus status;
        public String status_detail;
        
        public Builder() {
        }
        
        public Builder(final NotificationListResponse notificationListResponse) {
            super(notificationListResponse);
            if (notificationListResponse != null) {
                this.status = notificationListResponse.status;
                this.status_detail = notificationListResponse.status_detail;
                this.ids = (List<String>)Message.<Object>copyOf((List<Object>)notificationListResponse.ids);
            }
        }
        
        public NotificationListResponse build() {
            ((Message.Builder)this).checkRequiredFields();
            return new NotificationListResponse(this, null);
        }
        
        public Builder ids(final List<String> list) {
            this.ids = Message.Builder.<String>checkForNulls(list);
            return this;
        }
        
        public Builder status(final RequestStatus status) {
            this.status = status;
            return this;
        }
        
        public Builder status_detail(final String status_detail) {
            this.status_detail = status_detail;
            return this;
        }
    }
}
