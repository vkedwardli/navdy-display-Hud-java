package com.navdy.service.library.events.messaging;

import com.squareup.wire.ProtoField;
import com.squareup.wire.Message;

public final class SmsMessageRequest extends Message
{
    public static final String DEFAULT_ID = "";
    public static final String DEFAULT_MESSAGE = "";
    public static final String DEFAULT_NAME = "";
    public static final String DEFAULT_NUMBER = "";
    private static final long serialVersionUID = 0L;
    @ProtoField(tag = 4, type = Datatype.STRING)
    public final String id;
    @ProtoField(tag = 2, type = Datatype.STRING)
    public final String message;
    @ProtoField(tag = 3, type = Datatype.STRING)
    public final String name;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.STRING)
    public final String number;
    
    private SmsMessageRequest(final Builder builder) {
        this(builder.number, builder.message, builder.name, builder.id);
        this.setBuilder((Message.Builder)builder);
    }
    
    public SmsMessageRequest(final String number, final String message, final String name, final String id) {
        this.number = number;
        this.message = message;
        this.name = name;
        this.id = id;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o != this) {
            if (!(o instanceof SmsMessageRequest)) {
                b = false;
            }
            else {
                final SmsMessageRequest smsMessageRequest = (SmsMessageRequest)o;
                if (!this.equals(this.number, smsMessageRequest.number) || !this.equals(this.message, smsMessageRequest.message) || !this.equals(this.name, smsMessageRequest.name) || !this.equals(this.id, smsMessageRequest.id)) {
                    b = false;
                }
            }
        }
        return b;
    }
    
    @Override
    public int hashCode() {
        int hashCode = 0;
        int hashCode2;
        if ((hashCode2 = this.hashCode) == 0) {
            int hashCode3;
            if (this.number != null) {
                hashCode3 = this.number.hashCode();
            }
            else {
                hashCode3 = 0;
            }
            int hashCode4;
            if (this.message != null) {
                hashCode4 = this.message.hashCode();
            }
            else {
                hashCode4 = 0;
            }
            int hashCode5;
            if (this.name != null) {
                hashCode5 = this.name.hashCode();
            }
            else {
                hashCode5 = 0;
            }
            if (this.id != null) {
                hashCode = this.id.hashCode();
            }
            hashCode2 = ((hashCode3 * 37 + hashCode4) * 37 + hashCode5) * 37 + hashCode;
            this.hashCode = hashCode2;
        }
        return hashCode2;
    }
    
    public static final class Builder extends Message.Builder<SmsMessageRequest>
    {
        public String id;
        public String message;
        public String name;
        public String number;
        
        public Builder() {
        }
        
        public Builder(final SmsMessageRequest smsMessageRequest) {
            super(smsMessageRequest);
            if (smsMessageRequest != null) {
                this.number = smsMessageRequest.number;
                this.message = smsMessageRequest.message;
                this.name = smsMessageRequest.name;
                this.id = smsMessageRequest.id;
            }
        }
        
        public SmsMessageRequest build() {
            ((Message.Builder)this).checkRequiredFields();
            return new SmsMessageRequest(this, null);
        }
        
        public Builder id(final String id) {
            this.id = id;
            return this;
        }
        
        public Builder message(final String message) {
            this.message = message;
            return this;
        }
        
        public Builder name(final String name) {
            this.name = name;
            return this;
        }
        
        public Builder number(final String number) {
            this.number = number;
            return this;
        }
    }
}
