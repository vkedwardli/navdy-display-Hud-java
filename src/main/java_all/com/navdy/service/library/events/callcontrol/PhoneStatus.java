package com.navdy.service.library.events.callcontrol;

import com.squareup.wire.ProtoEnum;

public enum PhoneStatus implements ProtoEnum
{
    PHONE_CONNECTING(6), 
    PHONE_DIALING(4), 
    PHONE_DISCONNECTING(7), 
    PHONE_HELD(5), 
    PHONE_IDLE(1), 
    PHONE_OFFHOOK(3), 
    PHONE_RINGING(2);
    
    private final int value;
    
    private PhoneStatus(final int value) {
        this.value = value;
    }
    
    @Override
    public int getValue() {
        return this.value;
    }
}
