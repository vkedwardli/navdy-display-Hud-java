package com.navdy.service.library.events.audio;

import com.squareup.wire.ProtoField;
import com.squareup.wire.Message;

public final class MusicCollectionInfo extends Message
{
    public static final Boolean DEFAULT_CANSHUFFLE;
    public static final String DEFAULT_COLLECTIONID = "";
    public static final MusicCollectionSource DEFAULT_COLLECTIONSOURCE;
    public static final MusicCollectionType DEFAULT_COLLECTIONTYPE;
    public static final Integer DEFAULT_INDEX;
    public static final String DEFAULT_NAME = "";
    public static final Integer DEFAULT_SIZE;
    public static final String DEFAULT_SUBTITLE = "";
    private static final long serialVersionUID = 0L;
    @ProtoField(tag = 8, type = Datatype.BOOL)
    public final Boolean canShuffle;
    @ProtoField(tag = 3, type = Datatype.STRING)
    public final String collectionId;
    @ProtoField(tag = 1, type = Datatype.ENUM)
    public final MusicCollectionSource collectionSource;
    @ProtoField(tag = 2, type = Datatype.ENUM)
    public final MusicCollectionType collectionType;
    @ProtoField(tag = 6, type = Datatype.INT32)
    public final Integer index;
    @ProtoField(tag = 4, type = Datatype.STRING)
    public final String name;
    @ProtoField(tag = 5, type = Datatype.INT32)
    public final Integer size;
    @ProtoField(tag = 7, type = Datatype.STRING)
    public final String subtitle;
    
    static {
        DEFAULT_COLLECTIONSOURCE = MusicCollectionSource.COLLECTION_SOURCE_UNKNOWN;
        DEFAULT_COLLECTIONTYPE = MusicCollectionType.COLLECTION_TYPE_UNKNOWN;
        DEFAULT_SIZE = 0;
        DEFAULT_INDEX = 0;
        DEFAULT_CANSHUFFLE = false;
    }
    
    private MusicCollectionInfo(final Builder builder) {
        this(builder.collectionSource, builder.collectionType, builder.collectionId, builder.name, builder.size, builder.index, builder.subtitle, builder.canShuffle);
        this.setBuilder((Message.Builder)builder);
    }
    
    public MusicCollectionInfo(final MusicCollectionSource collectionSource, final MusicCollectionType collectionType, final String collectionId, final String name, final Integer size, final Integer index, final String subtitle, final Boolean canShuffle) {
        this.collectionSource = collectionSource;
        this.collectionType = collectionType;
        this.collectionId = collectionId;
        this.name = name;
        this.size = size;
        this.index = index;
        this.subtitle = subtitle;
        this.canShuffle = canShuffle;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o != this) {
            if (!(o instanceof MusicCollectionInfo)) {
                b = false;
            }
            else {
                final MusicCollectionInfo musicCollectionInfo = (MusicCollectionInfo)o;
                if (!this.equals(this.collectionSource, musicCollectionInfo.collectionSource) || !this.equals(this.collectionType, musicCollectionInfo.collectionType) || !this.equals(this.collectionId, musicCollectionInfo.collectionId) || !this.equals(this.name, musicCollectionInfo.name) || !this.equals(this.size, musicCollectionInfo.size) || !this.equals(this.index, musicCollectionInfo.index) || !this.equals(this.subtitle, musicCollectionInfo.subtitle) || !this.equals(this.canShuffle, musicCollectionInfo.canShuffle)) {
                    b = false;
                }
            }
        }
        return b;
    }
    
    @Override
    public int hashCode() {
        int hashCode = 0;
        int hashCode2;
        if ((hashCode2 = this.hashCode) == 0) {
            int hashCode3;
            if (this.collectionSource != null) {
                hashCode3 = this.collectionSource.hashCode();
            }
            else {
                hashCode3 = 0;
            }
            int hashCode4;
            if (this.collectionType != null) {
                hashCode4 = this.collectionType.hashCode();
            }
            else {
                hashCode4 = 0;
            }
            int hashCode5;
            if (this.collectionId != null) {
                hashCode5 = this.collectionId.hashCode();
            }
            else {
                hashCode5 = 0;
            }
            int hashCode6;
            if (this.name != null) {
                hashCode6 = this.name.hashCode();
            }
            else {
                hashCode6 = 0;
            }
            int hashCode7;
            if (this.size != null) {
                hashCode7 = this.size.hashCode();
            }
            else {
                hashCode7 = 0;
            }
            int hashCode8;
            if (this.index != null) {
                hashCode8 = this.index.hashCode();
            }
            else {
                hashCode8 = 0;
            }
            int hashCode9;
            if (this.subtitle != null) {
                hashCode9 = this.subtitle.hashCode();
            }
            else {
                hashCode9 = 0;
            }
            if (this.canShuffle != null) {
                hashCode = this.canShuffle.hashCode();
            }
            hashCode2 = ((((((hashCode3 * 37 + hashCode4) * 37 + hashCode5) * 37 + hashCode6) * 37 + hashCode7) * 37 + hashCode8) * 37 + hashCode9) * 37 + hashCode;
            this.hashCode = hashCode2;
        }
        return hashCode2;
    }
    
    public static final class Builder extends Message.Builder<MusicCollectionInfo>
    {
        public Boolean canShuffle;
        public String collectionId;
        public MusicCollectionSource collectionSource;
        public MusicCollectionType collectionType;
        public Integer index;
        public String name;
        public Integer size;
        public String subtitle;
        
        public Builder() {
        }
        
        public Builder(final MusicCollectionInfo musicCollectionInfo) {
            super(musicCollectionInfo);
            if (musicCollectionInfo != null) {
                this.collectionSource = musicCollectionInfo.collectionSource;
                this.collectionType = musicCollectionInfo.collectionType;
                this.collectionId = musicCollectionInfo.collectionId;
                this.name = musicCollectionInfo.name;
                this.size = musicCollectionInfo.size;
                this.index = musicCollectionInfo.index;
                this.subtitle = musicCollectionInfo.subtitle;
                this.canShuffle = musicCollectionInfo.canShuffle;
            }
        }
        
        public MusicCollectionInfo build() {
            return new MusicCollectionInfo(this, null);
        }
        
        public Builder canShuffle(final Boolean canShuffle) {
            this.canShuffle = canShuffle;
            return this;
        }
        
        public Builder collectionId(final String collectionId) {
            this.collectionId = collectionId;
            return this;
        }
        
        public Builder collectionSource(final MusicCollectionSource collectionSource) {
            this.collectionSource = collectionSource;
            return this;
        }
        
        public Builder collectionType(final MusicCollectionType collectionType) {
            this.collectionType = collectionType;
            return this;
        }
        
        public Builder index(final Integer index) {
            this.index = index;
            return this;
        }
        
        public Builder name(final String name) {
            this.name = name;
            return this;
        }
        
        public Builder size(final Integer size) {
            this.size = size;
            return this;
        }
        
        public Builder subtitle(final String subtitle) {
            this.subtitle = subtitle;
            return this;
        }
    }
}
