package com.navdy.service.library.events.hudcontrol;

import com.squareup.wire.ProtoEnum;
import com.squareup.wire.ProtoField;
import com.squareup.wire.Message;

public final class AccelerateShutdown extends Message
{
    public static final AccelerateReason DEFAULT_REASON;
    private static final long serialVersionUID = 0L;
    @ProtoField(tag = 1, type = Datatype.ENUM)
    public final AccelerateReason reason;
    
    static {
        DEFAULT_REASON = AccelerateReason.ACCELERATE_REASON_UNKNOWN;
    }
    
    public AccelerateShutdown(final AccelerateReason reason) {
        this.reason = reason;
    }
    
    private AccelerateShutdown(final Builder builder) {
        this(builder.reason);
        this.setBuilder((Message.Builder)builder);
    }
    
    @Override
    public boolean equals(final Object o) {
        return o == this || (o instanceof AccelerateShutdown && this.equals(this.reason, ((AccelerateShutdown)o).reason));
    }
    
    @Override
    public int hashCode() {
        int hashCode = this.hashCode;
        if (hashCode == 0) {
            if (this.reason != null) {
                hashCode = this.reason.hashCode();
            }
            else {
                hashCode = 0;
            }
            this.hashCode = hashCode;
        }
        return hashCode;
    }
    
    public enum AccelerateReason implements ProtoEnum
    {
        ACCELERATE_REASON_HFP_DISCONNECT(2), 
        ACCELERATE_REASON_UNKNOWN(1), 
        ACCELERATE_REASON_WALKING(3);
        
        private final int value;
        
        private AccelerateReason(final int value) {
            this.value = value;
        }
        
        @Override
        public int getValue() {
            return this.value;
        }
    }
    
    public static final class Builder extends Message.Builder<AccelerateShutdown>
    {
        public AccelerateReason reason;
        
        public Builder() {
        }
        
        public Builder(final AccelerateShutdown accelerateShutdown) {
            super(accelerateShutdown);
            if (accelerateShutdown != null) {
                this.reason = accelerateShutdown.reason;
            }
        }
        
        public AccelerateShutdown build() {
            return new AccelerateShutdown(this, null);
        }
        
        public Builder reason(final AccelerateReason reason) {
            this.reason = reason;
            return this;
        }
    }
}
