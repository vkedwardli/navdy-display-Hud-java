package com.navdy.service.library.events.connection;

import com.squareup.wire.ProtoEnum;
import com.squareup.wire.ProtoField;
import com.squareup.wire.Message;

public final class ConnectionStateChange extends Message
{
    public static final String DEFAULT_REMOTEDEVICEID = "";
    public static final ConnectionState DEFAULT_STATE;
    private static final long serialVersionUID = 0L;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.STRING)
    public final String remoteDeviceId;
    @ProtoField(label = Label.REQUIRED, tag = 2, type = Datatype.ENUM)
    public final ConnectionState state;
    
    static {
        DEFAULT_STATE = ConnectionState.CONNECTION_CONNECTED;
    }
    
    private ConnectionStateChange(final Builder builder) {
        this(builder.remoteDeviceId, builder.state);
        this.setBuilder((Message.Builder)builder);
    }
    
    public ConnectionStateChange(final String remoteDeviceId, final ConnectionState state) {
        this.remoteDeviceId = remoteDeviceId;
        this.state = state;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o != this) {
            if (!(o instanceof ConnectionStateChange)) {
                b = false;
            }
            else {
                final ConnectionStateChange connectionStateChange = (ConnectionStateChange)o;
                if (!this.equals(this.remoteDeviceId, connectionStateChange.remoteDeviceId) || !this.equals(this.state, connectionStateChange.state)) {
                    b = false;
                }
            }
        }
        return b;
    }
    
    @Override
    public int hashCode() {
        int hashCode = 0;
        int hashCode2;
        if ((hashCode2 = this.hashCode) == 0) {
            int hashCode3;
            if (this.remoteDeviceId != null) {
                hashCode3 = this.remoteDeviceId.hashCode();
            }
            else {
                hashCode3 = 0;
            }
            if (this.state != null) {
                hashCode = this.state.hashCode();
            }
            hashCode2 = hashCode3 * 37 + hashCode;
            this.hashCode = hashCode2;
        }
        return hashCode2;
    }
    
    public static final class Builder extends Message.Builder<ConnectionStateChange>
    {
        public String remoteDeviceId;
        public ConnectionState state;
        
        public Builder() {
        }
        
        public Builder(final ConnectionStateChange connectionStateChange) {
            super(connectionStateChange);
            if (connectionStateChange != null) {
                this.remoteDeviceId = connectionStateChange.remoteDeviceId;
                this.state = connectionStateChange.state;
            }
        }
        
        public ConnectionStateChange build() {
            ((Message.Builder)this).checkRequiredFields();
            return new ConnectionStateChange(this, null);
        }
        
        public Builder remoteDeviceId(final String remoteDeviceId) {
            this.remoteDeviceId = remoteDeviceId;
            return this;
        }
        
        public Builder state(final ConnectionState state) {
            this.state = state;
            return this;
        }
    }
    
    public enum ConnectionState implements ProtoEnum
    {
        CONNECTION_CONNECTED(1), 
        CONNECTION_DISCONNECTED(2), 
        CONNECTION_LINK_ESTABLISHED(3), 
        CONNECTION_LINK_LOST(4), 
        CONNECTION_VERIFIED(5);
        
        private final int value;
        
        private ConnectionState(final int value) {
            this.value = value;
        }
        
        @Override
        public int getValue() {
            return this.value;
        }
    }
}
