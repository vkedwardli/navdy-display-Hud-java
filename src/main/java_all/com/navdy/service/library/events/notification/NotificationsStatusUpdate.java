package com.navdy.service.library.events.notification;

import com.squareup.wire.ProtoField;
import com.squareup.wire.Message;

public final class NotificationsStatusUpdate extends Message
{
    public static final NotificationsError DEFAULT_ERRORDETAILS;
    public static final ServiceType DEFAULT_SERVICE;
    public static final NotificationsState DEFAULT_STATE;
    private static final long serialVersionUID = 0L;
    @ProtoField(tag = 3, type = Datatype.ENUM)
    public final NotificationsError errorDetails;
    @ProtoField(tag = 2, type = Datatype.ENUM)
    public final ServiceType service;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.ENUM)
    public final NotificationsState state;
    
    static {
        DEFAULT_STATE = NotificationsState.NOTIFICATIONS_ENABLED;
        DEFAULT_SERVICE = ServiceType.SERVICE_ANCS;
        DEFAULT_ERRORDETAILS = NotificationsError.NOTIFICATIONS_ERROR_UNKNOWN;
    }
    
    public NotificationsStatusUpdate(final NotificationsState state, final ServiceType service, final NotificationsError errorDetails) {
        this.state = state;
        this.service = service;
        this.errorDetails = errorDetails;
    }
    
    private NotificationsStatusUpdate(final Builder builder) {
        this(builder.state, builder.service, builder.errorDetails);
        this.setBuilder((Message.Builder)builder);
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o != this) {
            if (!(o instanceof NotificationsStatusUpdate)) {
                b = false;
            }
            else {
                final NotificationsStatusUpdate notificationsStatusUpdate = (NotificationsStatusUpdate)o;
                if (!this.equals(this.state, notificationsStatusUpdate.state) || !this.equals(this.service, notificationsStatusUpdate.service) || !this.equals(this.errorDetails, notificationsStatusUpdate.errorDetails)) {
                    b = false;
                }
            }
        }
        return b;
    }
    
    @Override
    public int hashCode() {
        int hashCode = 0;
        int hashCode2;
        if ((hashCode2 = this.hashCode) == 0) {
            int hashCode3;
            if (this.state != null) {
                hashCode3 = this.state.hashCode();
            }
            else {
                hashCode3 = 0;
            }
            int hashCode4;
            if (this.service != null) {
                hashCode4 = this.service.hashCode();
            }
            else {
                hashCode4 = 0;
            }
            if (this.errorDetails != null) {
                hashCode = this.errorDetails.hashCode();
            }
            hashCode2 = (hashCode3 * 37 + hashCode4) * 37 + hashCode;
            this.hashCode = hashCode2;
        }
        return hashCode2;
    }
    
    public static final class Builder extends Message.Builder<NotificationsStatusUpdate>
    {
        public NotificationsError errorDetails;
        public ServiceType service;
        public NotificationsState state;
        
        public Builder() {
        }
        
        public Builder(final NotificationsStatusUpdate notificationsStatusUpdate) {
            super(notificationsStatusUpdate);
            if (notificationsStatusUpdate != null) {
                this.state = notificationsStatusUpdate.state;
                this.service = notificationsStatusUpdate.service;
                this.errorDetails = notificationsStatusUpdate.errorDetails;
            }
        }
        
        public NotificationsStatusUpdate build() {
            ((Message.Builder)this).checkRequiredFields();
            return new NotificationsStatusUpdate(this, null);
        }
        
        public Builder errorDetails(final NotificationsError errorDetails) {
            this.errorDetails = errorDetails;
            return this;
        }
        
        public Builder service(final ServiceType service) {
            this.service = service;
            return this;
        }
        
        public Builder state(final NotificationsState state) {
            this.state = state;
            return this;
        }
    }
}
