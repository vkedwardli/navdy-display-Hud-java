package com.navdy.service.library.events.places;

import com.squareup.wire.ProtoField;
import com.navdy.service.library.events.destination.Destination;
import com.navdy.service.library.events.RequestStatus;
import com.squareup.wire.Message;

public final class DestinationSelectedResponse extends Message
{
    public static final String DEFAULT_REQUEST_ID = "";
    public static final RequestStatus DEFAULT_REQUEST_STATUS;
    private static final long serialVersionUID = 0L;
    @ProtoField(tag = 3)
    public final Destination destination;
    @ProtoField(tag = 1, type = Datatype.STRING)
    public final String request_id;
    @ProtoField(tag = 2, type = Datatype.ENUM)
    public final RequestStatus request_status;
    
    static {
        DEFAULT_REQUEST_STATUS = RequestStatus.REQUEST_SUCCESS;
    }
    
    private DestinationSelectedResponse(final Builder builder) {
        this(builder.request_id, builder.request_status, builder.destination);
        this.setBuilder((Message.Builder)builder);
    }
    
    public DestinationSelectedResponse(final String request_id, final RequestStatus request_status, final Destination destination) {
        this.request_id = request_id;
        this.request_status = request_status;
        this.destination = destination;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o != this) {
            if (!(o instanceof DestinationSelectedResponse)) {
                b = false;
            }
            else {
                final DestinationSelectedResponse destinationSelectedResponse = (DestinationSelectedResponse)o;
                if (!this.equals(this.request_id, destinationSelectedResponse.request_id) || !this.equals(this.request_status, destinationSelectedResponse.request_status) || !this.equals(this.destination, destinationSelectedResponse.destination)) {
                    b = false;
                }
            }
        }
        return b;
    }
    
    @Override
    public int hashCode() {
        int hashCode = 0;
        int hashCode2;
        if ((hashCode2 = this.hashCode) == 0) {
            int hashCode3;
            if (this.request_id != null) {
                hashCode3 = this.request_id.hashCode();
            }
            else {
                hashCode3 = 0;
            }
            int hashCode4;
            if (this.request_status != null) {
                hashCode4 = this.request_status.hashCode();
            }
            else {
                hashCode4 = 0;
            }
            if (this.destination != null) {
                hashCode = this.destination.hashCode();
            }
            hashCode2 = (hashCode3 * 37 + hashCode4) * 37 + hashCode;
            this.hashCode = hashCode2;
        }
        return hashCode2;
    }
    
    public static final class Builder extends Message.Builder<DestinationSelectedResponse>
    {
        public Destination destination;
        public String request_id;
        public RequestStatus request_status;
        
        public Builder() {
        }
        
        public Builder(final DestinationSelectedResponse destinationSelectedResponse) {
            super(destinationSelectedResponse);
            if (destinationSelectedResponse != null) {
                this.request_id = destinationSelectedResponse.request_id;
                this.request_status = destinationSelectedResponse.request_status;
                this.destination = destinationSelectedResponse.destination;
            }
        }
        
        public DestinationSelectedResponse build() {
            return new DestinationSelectedResponse(this, null);
        }
        
        public Builder destination(final Destination destination) {
            this.destination = destination;
            return this;
        }
        
        public Builder request_id(final String request_id) {
            this.request_id = request_id;
            return this;
        }
        
        public Builder request_status(final RequestStatus request_status) {
            this.request_status = request_status;
            return this;
        }
    }
}
