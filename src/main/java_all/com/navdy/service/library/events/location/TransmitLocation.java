package com.navdy.service.library.events.location;

import com.squareup.wire.ProtoField;
import com.squareup.wire.Message;

public final class TransmitLocation extends Message
{
    public static final Boolean DEFAULT_SENDLOCATION;
    private static final long serialVersionUID = 0L;
    @ProtoField(tag = 1, type = Datatype.BOOL)
    public final Boolean sendLocation;
    
    static {
        DEFAULT_SENDLOCATION = false;
    }
    
    private TransmitLocation(final Builder builder) {
        this(builder.sendLocation);
        this.setBuilder((Message.Builder)builder);
    }
    
    public TransmitLocation(final Boolean sendLocation) {
        this.sendLocation = sendLocation;
    }
    
    @Override
    public boolean equals(final Object o) {
        return o == this || (o instanceof TransmitLocation && this.equals(this.sendLocation, ((TransmitLocation)o).sendLocation));
    }
    
    @Override
    public int hashCode() {
        int hashCode = this.hashCode;
        if (hashCode == 0) {
            if (this.sendLocation != null) {
                hashCode = this.sendLocation.hashCode();
            }
            else {
                hashCode = 0;
            }
            this.hashCode = hashCode;
        }
        return hashCode;
    }
    
    public static final class Builder extends Message.Builder<TransmitLocation>
    {
        public Boolean sendLocation;
        
        public Builder() {
        }
        
        public Builder(final TransmitLocation transmitLocation) {
            super(transmitLocation);
            if (transmitLocation != null) {
                this.sendLocation = transmitLocation.sendLocation;
            }
        }
        
        public TransmitLocation build() {
            return new TransmitLocation(this, null);
        }
        
        public Builder sendLocation(final Boolean sendLocation) {
            this.sendLocation = sendLocation;
            return this;
        }
    }
}
