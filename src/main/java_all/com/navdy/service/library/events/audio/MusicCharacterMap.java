package com.navdy.service.library.events.audio;

import com.squareup.wire.ProtoField;
import com.squareup.wire.Message;

public final class MusicCharacterMap extends Message
{
    public static final String DEFAULT_CHARACTER = "";
    public static final Integer DEFAULT_OFFSET;
    private static final long serialVersionUID = 0L;
    @ProtoField(tag = 1, type = Datatype.STRING)
    public final String character;
    @ProtoField(tag = 2, type = Datatype.UINT32)
    public final Integer offset;
    
    static {
        DEFAULT_OFFSET = 0;
    }
    
    private MusicCharacterMap(final Builder builder) {
        this(builder.character, builder.offset);
        this.setBuilder((Message.Builder)builder);
    }
    
    public MusicCharacterMap(final String character, final Integer offset) {
        this.character = character;
        this.offset = offset;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o != this) {
            if (!(o instanceof MusicCharacterMap)) {
                b = false;
            }
            else {
                final MusicCharacterMap musicCharacterMap = (MusicCharacterMap)o;
                if (!this.equals(this.character, musicCharacterMap.character) || !this.equals(this.offset, musicCharacterMap.offset)) {
                    b = false;
                }
            }
        }
        return b;
    }
    
    @Override
    public int hashCode() {
        int hashCode = 0;
        int hashCode2;
        if ((hashCode2 = this.hashCode) == 0) {
            int hashCode3;
            if (this.character != null) {
                hashCode3 = this.character.hashCode();
            }
            else {
                hashCode3 = 0;
            }
            if (this.offset != null) {
                hashCode = this.offset.hashCode();
            }
            hashCode2 = hashCode3 * 37 + hashCode;
            this.hashCode = hashCode2;
        }
        return hashCode2;
    }
    
    public static final class Builder extends Message.Builder<MusicCharacterMap>
    {
        public String character;
        public Integer offset;
        
        public Builder() {
        }
        
        public Builder(final MusicCharacterMap musicCharacterMap) {
            super(musicCharacterMap);
            if (musicCharacterMap != null) {
                this.character = musicCharacterMap.character;
                this.offset = musicCharacterMap.offset;
            }
        }
        
        public MusicCharacterMap build() {
            return new MusicCharacterMap(this, null);
        }
        
        public Builder character(final String character) {
            this.character = character;
            return this;
        }
        
        public Builder offset(final Integer offset) {
            this.offset = offset;
            return this;
        }
    }
}
