package com.navdy.service.library.events.debug;

import com.squareup.wire.Message;

public final class StopDrivePlaybackEvent extends Message
{
    private static final long serialVersionUID = 0L;
    
    public StopDrivePlaybackEvent() {
    }
    
    private StopDrivePlaybackEvent(final Builder builder) {
        this.setBuilder((Message.Builder)builder);
    }
    
    @Override
    public boolean equals(final Object o) {
        return o instanceof StopDrivePlaybackEvent;
    }
    
    @Override
    public int hashCode() {
        return 0;
    }
    
    public static final class Builder extends Message.Builder<StopDrivePlaybackEvent>
    {
        public Builder() {
        }
        
        public Builder(final StopDrivePlaybackEvent stopDrivePlaybackEvent) {
            super(stopDrivePlaybackEvent);
        }
        
        public StopDrivePlaybackEvent build() {
            return new StopDrivePlaybackEvent(this, null);
        }
    }
}
