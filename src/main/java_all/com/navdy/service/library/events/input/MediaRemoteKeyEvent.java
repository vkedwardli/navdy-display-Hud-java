package com.navdy.service.library.events.input;

import com.squareup.wire.ProtoField;
import com.squareup.wire.Message;

public final class MediaRemoteKeyEvent extends Message
{
    public static final KeyEvent DEFAULT_ACTION;
    public static final MediaRemoteKey DEFAULT_KEY;
    private static final long serialVersionUID = 0L;
    @ProtoField(label = Label.REQUIRED, tag = 2, type = Datatype.ENUM)
    public final KeyEvent action;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.ENUM)
    public final MediaRemoteKey key;
    
    static {
        DEFAULT_KEY = MediaRemoteKey.MEDIA_REMOTE_KEY_SIRI;
        DEFAULT_ACTION = KeyEvent.KEY_DOWN;
    }
    
    public MediaRemoteKeyEvent(final MediaRemoteKey key, final KeyEvent action) {
        this.key = key;
        this.action = action;
    }
    
    private MediaRemoteKeyEvent(final Builder builder) {
        this(builder.key, builder.action);
        this.setBuilder((Message.Builder)builder);
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o != this) {
            if (!(o instanceof MediaRemoteKeyEvent)) {
                b = false;
            }
            else {
                final MediaRemoteKeyEvent mediaRemoteKeyEvent = (MediaRemoteKeyEvent)o;
                if (!this.equals(this.key, mediaRemoteKeyEvent.key) || !this.equals(this.action, mediaRemoteKeyEvent.action)) {
                    b = false;
                }
            }
        }
        return b;
    }
    
    @Override
    public int hashCode() {
        int hashCode = 0;
        int hashCode2;
        if ((hashCode2 = this.hashCode) == 0) {
            int hashCode3;
            if (this.key != null) {
                hashCode3 = this.key.hashCode();
            }
            else {
                hashCode3 = 0;
            }
            if (this.action != null) {
                hashCode = this.action.hashCode();
            }
            hashCode2 = hashCode3 * 37 + hashCode;
            this.hashCode = hashCode2;
        }
        return hashCode2;
    }
    
    public static final class Builder extends Message.Builder<MediaRemoteKeyEvent>
    {
        public KeyEvent action;
        public MediaRemoteKey key;
        
        public Builder() {
        }
        
        public Builder(final MediaRemoteKeyEvent mediaRemoteKeyEvent) {
            super(mediaRemoteKeyEvent);
            if (mediaRemoteKeyEvent != null) {
                this.key = mediaRemoteKeyEvent.key;
                this.action = mediaRemoteKeyEvent.action;
            }
        }
        
        public Builder action(final KeyEvent action) {
            this.action = action;
            return this;
        }
        
        public MediaRemoteKeyEvent build() {
            ((Message.Builder)this).checkRequiredFields();
            return new MediaRemoteKeyEvent(this, null);
        }
        
        public Builder key(final MediaRemoteKey key) {
            this.key = key;
            return this;
        }
    }
}
