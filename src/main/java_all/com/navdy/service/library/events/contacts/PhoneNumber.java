package com.navdy.service.library.events.contacts;

import com.squareup.wire.ProtoField;
import com.squareup.wire.Message;

public final class PhoneNumber extends Message
{
    public static final String DEFAULT_CUSTOMTYPE = "";
    public static final String DEFAULT_NUMBER = "";
    public static final PhoneNumberType DEFAULT_NUMBERTYPE;
    private static final long serialVersionUID = 0L;
    @ProtoField(tag = 3, type = Datatype.STRING)
    public final String customType;
    @ProtoField(tag = 1, type = Datatype.STRING)
    public final String number;
    @ProtoField(tag = 2, type = Datatype.ENUM)
    public final PhoneNumberType numberType;
    
    static {
        DEFAULT_NUMBERTYPE = PhoneNumberType.PHONE_NUMBER_HOME;
    }
    
    private PhoneNumber(final Builder builder) {
        this(builder.number, builder.numberType, builder.customType);
        this.setBuilder((Message.Builder)builder);
    }
    
    public PhoneNumber(final String number, final PhoneNumberType numberType, final String customType) {
        this.number = number;
        this.numberType = numberType;
        this.customType = customType;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o != this) {
            if (!(o instanceof PhoneNumber)) {
                b = false;
            }
            else {
                final PhoneNumber phoneNumber = (PhoneNumber)o;
                if (!this.equals(this.number, phoneNumber.number) || !this.equals(this.numberType, phoneNumber.numberType) || !this.equals(this.customType, phoneNumber.customType)) {
                    b = false;
                }
            }
        }
        return b;
    }
    
    @Override
    public int hashCode() {
        int hashCode = 0;
        int hashCode2;
        if ((hashCode2 = this.hashCode) == 0) {
            int hashCode3;
            if (this.number != null) {
                hashCode3 = this.number.hashCode();
            }
            else {
                hashCode3 = 0;
            }
            int hashCode4;
            if (this.numberType != null) {
                hashCode4 = this.numberType.hashCode();
            }
            else {
                hashCode4 = 0;
            }
            if (this.customType != null) {
                hashCode = this.customType.hashCode();
            }
            hashCode2 = (hashCode3 * 37 + hashCode4) * 37 + hashCode;
            this.hashCode = hashCode2;
        }
        return hashCode2;
    }
    
    public static final class Builder extends Message.Builder<PhoneNumber>
    {
        public String customType;
        public String number;
        public PhoneNumberType numberType;
        
        public Builder() {
        }
        
        public Builder(final PhoneNumber phoneNumber) {
            super(phoneNumber);
            if (phoneNumber != null) {
                this.number = phoneNumber.number;
                this.numberType = phoneNumber.numberType;
                this.customType = phoneNumber.customType;
            }
        }
        
        public PhoneNumber build() {
            return new PhoneNumber(this, null);
        }
        
        public Builder customType(final String customType) {
            this.customType = customType;
            return this;
        }
        
        public Builder number(final String number) {
            this.number = number;
            return this;
        }
        
        public Builder numberType(final PhoneNumberType numberType) {
            this.numberType = numberType;
            return this;
        }
    }
}
