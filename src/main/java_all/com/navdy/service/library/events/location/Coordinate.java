package com.navdy.service.library.events.location;

import com.squareup.wire.ProtoField;
import com.squareup.wire.Message;

public final class Coordinate extends Message
{
    public static final Float DEFAULT_ACCURACY;
    public static final Double DEFAULT_ALTITUDE;
    public static final Float DEFAULT_BEARING;
    public static final Double DEFAULT_LATITUDE;
    public static final Double DEFAULT_LONGITUDE;
    public static final String DEFAULT_PROVIDER = "";
    public static final Float DEFAULT_SPEED;
    public static final Long DEFAULT_TIMESTAMP;
    private static final long serialVersionUID = 0L;
    @ProtoField(tag = 3, type = Datatype.FLOAT)
    public final Float accuracy;
    @ProtoField(tag = 4, type = Datatype.DOUBLE)
    public final Double altitude;
    @ProtoField(tag = 5, type = Datatype.FLOAT)
    public final Float bearing;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.DOUBLE)
    public final Double latitude;
    @ProtoField(label = Label.REQUIRED, tag = 2, type = Datatype.DOUBLE)
    public final Double longitude;
    @ProtoField(tag = 8, type = Datatype.STRING)
    public final String provider;
    @ProtoField(tag = 6, type = Datatype.FLOAT)
    public final Float speed;
    @ProtoField(tag = 7, type = Datatype.INT64)
    public final Long timestamp;
    
    static {
        DEFAULT_LATITUDE = 0.0;
        DEFAULT_LONGITUDE = 0.0;
        DEFAULT_ACCURACY = 0.0f;
        DEFAULT_ALTITUDE = 0.0;
        DEFAULT_BEARING = 0.0f;
        DEFAULT_SPEED = 0.0f;
        DEFAULT_TIMESTAMP = 0L;
    }
    
    private Coordinate(final Builder builder) {
        this(builder.latitude, builder.longitude, builder.accuracy, builder.altitude, builder.bearing, builder.speed, builder.timestamp, builder.provider);
        this.setBuilder((Message.Builder)builder);
    }
    
    public Coordinate(final Double latitude, final Double longitude, final Float accuracy, final Double altitude, final Float bearing, final Float speed, final Long timestamp, final String provider) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.accuracy = accuracy;
        this.altitude = altitude;
        this.bearing = bearing;
        this.speed = speed;
        this.timestamp = timestamp;
        this.provider = provider;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o != this) {
            if (!(o instanceof Coordinate)) {
                b = false;
            }
            else {
                final Coordinate coordinate = (Coordinate)o;
                if (!this.equals(this.latitude, coordinate.latitude) || !this.equals(this.longitude, coordinate.longitude) || !this.equals(this.accuracy, coordinate.accuracy) || !this.equals(this.altitude, coordinate.altitude) || !this.equals(this.bearing, coordinate.bearing) || !this.equals(this.speed, coordinate.speed) || !this.equals(this.timestamp, coordinate.timestamp) || !this.equals(this.provider, coordinate.provider)) {
                    b = false;
                }
            }
        }
        return b;
    }
    
    @Override
    public int hashCode() {
        int hashCode = 0;
        int hashCode2;
        if ((hashCode2 = this.hashCode) == 0) {
            int hashCode3;
            if (this.latitude != null) {
                hashCode3 = this.latitude.hashCode();
            }
            else {
                hashCode3 = 0;
            }
            int hashCode4;
            if (this.longitude != null) {
                hashCode4 = this.longitude.hashCode();
            }
            else {
                hashCode4 = 0;
            }
            int hashCode5;
            if (this.accuracy != null) {
                hashCode5 = this.accuracy.hashCode();
            }
            else {
                hashCode5 = 0;
            }
            int hashCode6;
            if (this.altitude != null) {
                hashCode6 = this.altitude.hashCode();
            }
            else {
                hashCode6 = 0;
            }
            int hashCode7;
            if (this.bearing != null) {
                hashCode7 = this.bearing.hashCode();
            }
            else {
                hashCode7 = 0;
            }
            int hashCode8;
            if (this.speed != null) {
                hashCode8 = this.speed.hashCode();
            }
            else {
                hashCode8 = 0;
            }
            int hashCode9;
            if (this.timestamp != null) {
                hashCode9 = this.timestamp.hashCode();
            }
            else {
                hashCode9 = 0;
            }
            if (this.provider != null) {
                hashCode = this.provider.hashCode();
            }
            hashCode2 = ((((((hashCode3 * 37 + hashCode4) * 37 + hashCode5) * 37 + hashCode6) * 37 + hashCode7) * 37 + hashCode8) * 37 + hashCode9) * 37 + hashCode;
            this.hashCode = hashCode2;
        }
        return hashCode2;
    }
    
    public static final class Builder extends Message.Builder<Coordinate>
    {
        public Float accuracy;
        public Double altitude;
        public Float bearing;
        public Double latitude;
        public Double longitude;
        public String provider;
        public Float speed;
        public Long timestamp;
        
        public Builder() {
        }
        
        public Builder(final Coordinate coordinate) {
            super(coordinate);
            if (coordinate != null) {
                this.latitude = coordinate.latitude;
                this.longitude = coordinate.longitude;
                this.accuracy = coordinate.accuracy;
                this.altitude = coordinate.altitude;
                this.bearing = coordinate.bearing;
                this.speed = coordinate.speed;
                this.timestamp = coordinate.timestamp;
                this.provider = coordinate.provider;
            }
        }
        
        public Builder accuracy(final Float accuracy) {
            this.accuracy = accuracy;
            return this;
        }
        
        public Builder altitude(final Double altitude) {
            this.altitude = altitude;
            return this;
        }
        
        public Builder bearing(final Float bearing) {
            this.bearing = bearing;
            return this;
        }
        
        public Coordinate build() {
            ((Message.Builder)this).checkRequiredFields();
            return new Coordinate(this, null);
        }
        
        public Builder latitude(final Double latitude) {
            this.latitude = latitude;
            return this;
        }
        
        public Builder longitude(final Double longitude) {
            this.longitude = longitude;
            return this;
        }
        
        public Builder provider(final String provider) {
            this.provider = provider;
            return this;
        }
        
        public Builder speed(final Float speed) {
            this.speed = speed;
            return this;
        }
        
        public Builder timestamp(final Long timestamp) {
            this.timestamp = timestamp;
            return this;
        }
    }
}
