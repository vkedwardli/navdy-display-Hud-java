package com.navdy.service.library.events.file;

import com.squareup.wire.ProtoField;
import com.squareup.wire.Message;

public final class FileListRequest extends Message
{
    public static final FileType DEFAULT_FILE_TYPE;
    private static final long serialVersionUID = 0L;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.ENUM)
    public final FileType file_type;
    
    static {
        DEFAULT_FILE_TYPE = FileType.FILE_TYPE_OTA;
    }
    
    private FileListRequest(final Builder builder) {
        this(builder.file_type);
        this.setBuilder((Message.Builder)builder);
    }
    
    public FileListRequest(final FileType file_type) {
        this.file_type = file_type;
    }
    
    @Override
    public boolean equals(final Object o) {
        return o == this || (o instanceof FileListRequest && this.equals(this.file_type, ((FileListRequest)o).file_type));
    }
    
    @Override
    public int hashCode() {
        int hashCode = this.hashCode;
        if (hashCode == 0) {
            if (this.file_type != null) {
                hashCode = this.file_type.hashCode();
            }
            else {
                hashCode = 0;
            }
            this.hashCode = hashCode;
        }
        return hashCode;
    }
    
    public static final class Builder extends Message.Builder<FileListRequest>
    {
        public FileType file_type;
        
        public Builder() {
        }
        
        public Builder(final FileListRequest fileListRequest) {
            super(fileListRequest);
            if (fileListRequest != null) {
                this.file_type = fileListRequest.file_type;
            }
        }
        
        public FileListRequest build() {
            ((Message.Builder)this).checkRequiredFields();
            return new FileListRequest(this, null);
        }
        
        public Builder file_type(final FileType file_type) {
            this.file_type = file_type;
            return this;
        }
    }
}
