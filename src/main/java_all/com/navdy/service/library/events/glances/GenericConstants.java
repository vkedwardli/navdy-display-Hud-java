package com.navdy.service.library.events.glances;

import com.squareup.wire.ProtoEnum;

public enum GenericConstants implements ProtoEnum
{
    GENERIC_MAIN_ICON(2), 
    GENERIC_MESSAGE(1), 
    GENERIC_SIDE_ICON(3), 
    GENERIC_TITLE(0);
    
    private final int value;
    
    private GenericConstants(final int value) {
        this.value = value;
    }
    
    @Override
    public int getValue() {
        return this.value;
    }
}
