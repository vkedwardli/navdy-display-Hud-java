package com.navdy.service.library.events.glances;

import com.squareup.wire.ProtoField;
import com.squareup.wire.Message;

public final class KeyValue extends Message
{
    public static final String DEFAULT_KEY = "";
    public static final String DEFAULT_VALUE = "";
    private static final long serialVersionUID = 0L;
    @ProtoField(tag = 1, type = Datatype.STRING)
    public final String key;
    @ProtoField(tag = 2, type = Datatype.STRING)
    public final String value;
    
    private KeyValue(final Builder builder) {
        this(builder.key, builder.value);
        this.setBuilder((Message.Builder)builder);
    }
    
    public KeyValue(final String key, final String value) {
        this.key = key;
        this.value = value;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o != this) {
            if (!(o instanceof KeyValue)) {
                b = false;
            }
            else {
                final KeyValue keyValue = (KeyValue)o;
                if (!this.equals(this.key, keyValue.key) || !this.equals(this.value, keyValue.value)) {
                    b = false;
                }
            }
        }
        return b;
    }
    
    @Override
    public int hashCode() {
        int hashCode = 0;
        int hashCode2;
        if ((hashCode2 = this.hashCode) == 0) {
            int hashCode3;
            if (this.key != null) {
                hashCode3 = this.key.hashCode();
            }
            else {
                hashCode3 = 0;
            }
            if (this.value != null) {
                hashCode = this.value.hashCode();
            }
            hashCode2 = hashCode3 * 37 + hashCode;
            this.hashCode = hashCode2;
        }
        return hashCode2;
    }
    
    public static final class Builder extends Message.Builder<KeyValue>
    {
        public String key;
        public String value;
        
        public Builder() {
        }
        
        public Builder(final KeyValue keyValue) {
            super(keyValue);
            if (keyValue != null) {
                this.key = keyValue.key;
                this.value = keyValue.value;
            }
        }
        
        public KeyValue build() {
            return new KeyValue(this, null);
        }
        
        public Builder key(final String key) {
            this.key = key;
            return this;
        }
        
        public Builder value(final String value) {
            this.value = value;
            return this;
        }
    }
}
