package com.navdy.service.library.events.glances;

import com.squareup.wire.Message;

public final class ClearGlances extends Message
{
    private static final long serialVersionUID = 0L;
    
    public ClearGlances() {
    }
    
    private ClearGlances(final Builder builder) {
        this.setBuilder((Message.Builder)builder);
    }
    
    @Override
    public boolean equals(final Object o) {
        return o instanceof ClearGlances;
    }
    
    @Override
    public int hashCode() {
        return 0;
    }
    
    public static final class Builder extends Message.Builder<ClearGlances>
    {
        public Builder() {
        }
        
        public Builder(final ClearGlances clearGlances) {
            super(clearGlances);
        }
        
        public ClearGlances build() {
            return new ClearGlances(this, null);
        }
    }
}
