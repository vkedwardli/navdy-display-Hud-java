package com.navdy.service.library.events.audio;

import com.squareup.wire.ProtoEnum;
import com.squareup.wire.ProtoField;
import com.squareup.wire.Message;

public final class SpeechRequest extends Message
{
    public static final Category DEFAULT_CATEGORY;
    public static final String DEFAULT_ID = "";
    public static final String DEFAULT_LANGUAGE = "";
    public static final Boolean DEFAULT_SENDSTATUS;
    public static final String DEFAULT_WORDS = "";
    private static final long serialVersionUID = 0L;
    @ProtoField(tag = 2, type = Datatype.ENUM)
    public final Category category;
    @ProtoField(tag = 3, type = Datatype.STRING)
    public final String id;
    @ProtoField(tag = 5, type = Datatype.STRING)
    public final String language;
    @ProtoField(tag = 4, type = Datatype.BOOL)
    public final Boolean sendStatus;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.STRING)
    public final String words;
    
    static {
        DEFAULT_CATEGORY = Category.SPEECH_TURN_BY_TURN;
        DEFAULT_SENDSTATUS = false;
    }
    
    private SpeechRequest(final Builder builder) {
        this(builder.words, builder.category, builder.id, builder.sendStatus, builder.language);
        this.setBuilder((Message.Builder)builder);
    }
    
    public SpeechRequest(final String words, final Category category, final String id, final Boolean sendStatus, final String language) {
        this.words = words;
        this.category = category;
        this.id = id;
        this.sendStatus = sendStatus;
        this.language = language;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o != this) {
            if (!(o instanceof SpeechRequest)) {
                b = false;
            }
            else {
                final SpeechRequest speechRequest = (SpeechRequest)o;
                if (!this.equals(this.words, speechRequest.words) || !this.equals(this.category, speechRequest.category) || !this.equals(this.id, speechRequest.id) || !this.equals(this.sendStatus, speechRequest.sendStatus) || !this.equals(this.language, speechRequest.language)) {
                    b = false;
                }
            }
        }
        return b;
    }
    
    @Override
    public int hashCode() {
        int hashCode = 0;
        int hashCode2;
        if ((hashCode2 = this.hashCode) == 0) {
            int hashCode3;
            if (this.words != null) {
                hashCode3 = this.words.hashCode();
            }
            else {
                hashCode3 = 0;
            }
            int hashCode4;
            if (this.category != null) {
                hashCode4 = this.category.hashCode();
            }
            else {
                hashCode4 = 0;
            }
            int hashCode5;
            if (this.id != null) {
                hashCode5 = this.id.hashCode();
            }
            else {
                hashCode5 = 0;
            }
            int hashCode6;
            if (this.sendStatus != null) {
                hashCode6 = this.sendStatus.hashCode();
            }
            else {
                hashCode6 = 0;
            }
            if (this.language != null) {
                hashCode = this.language.hashCode();
            }
            hashCode2 = (((hashCode3 * 37 + hashCode4) * 37 + hashCode5) * 37 + hashCode6) * 37 + hashCode;
            this.hashCode = hashCode2;
        }
        return hashCode2;
    }
    
    public static final class Builder extends Message.Builder<SpeechRequest>
    {
        public Category category;
        public String id;
        public String language;
        public Boolean sendStatus;
        public String words;
        
        public Builder() {
        }
        
        public Builder(final SpeechRequest speechRequest) {
            super(speechRequest);
            if (speechRequest != null) {
                this.words = speechRequest.words;
                this.category = speechRequest.category;
                this.id = speechRequest.id;
                this.sendStatus = speechRequest.sendStatus;
                this.language = speechRequest.language;
            }
        }
        
        public SpeechRequest build() {
            ((Message.Builder)this).checkRequiredFields();
            return new SpeechRequest(this, null);
        }
        
        public Builder category(final Category category) {
            this.category = category;
            return this;
        }
        
        public Builder id(final String id) {
            this.id = id;
            return this;
        }
        
        public Builder language(final String language) {
            this.language = language;
            return this;
        }
        
        public Builder sendStatus(final Boolean sendStatus) {
            this.sendStatus = sendStatus;
            return this;
        }
        
        public Builder words(final String words) {
            this.words = words;
            return this;
        }
    }
    
    public enum Category implements ProtoEnum
    {
        SPEECH_CAMERA_WARNING(8), 
        SPEECH_GPS_CONNECTIVITY(4), 
        SPEECH_MESSAGE_READ_OUT(5), 
        SPEECH_NOTIFICATION(7), 
        SPEECH_REROUTE(2), 
        SPEECH_SPEED_WARNING(3), 
        SPEECH_TURN_BY_TURN(1), 
        SPEECH_WELCOME_MESSAGE(6);
        
        private final int value;
        
        private Category(final int value) {
            this.value = value;
        }
        
        @Override
        public int getValue() {
            return this.value;
        }
    }
}
