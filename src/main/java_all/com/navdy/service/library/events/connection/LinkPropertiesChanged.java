package com.navdy.service.library.events.connection;

import com.squareup.wire.ProtoField;
import com.squareup.wire.Message;

public final class LinkPropertiesChanged extends Message
{
    public static final Integer DEFAULT_BANDWIDTHLEVEL;
    private static final long serialVersionUID = 0L;
    @ProtoField(tag = 1, type = Datatype.INT32)
    public final Integer bandwidthLevel;
    
    static {
        DEFAULT_BANDWIDTHLEVEL = 0;
    }
    
    private LinkPropertiesChanged(final Builder builder) {
        this(builder.bandwidthLevel);
        this.setBuilder((Message.Builder)builder);
    }
    
    public LinkPropertiesChanged(final Integer bandwidthLevel) {
        this.bandwidthLevel = bandwidthLevel;
    }
    
    @Override
    public boolean equals(final Object o) {
        return o == this || (o instanceof LinkPropertiesChanged && this.equals(this.bandwidthLevel, ((LinkPropertiesChanged)o).bandwidthLevel));
    }
    
    @Override
    public int hashCode() {
        int hashCode = this.hashCode;
        if (hashCode == 0) {
            if (this.bandwidthLevel != null) {
                hashCode = this.bandwidthLevel.hashCode();
            }
            else {
                hashCode = 0;
            }
            this.hashCode = hashCode;
        }
        return hashCode;
    }
    
    public static final class Builder extends Message.Builder<LinkPropertiesChanged>
    {
        public Integer bandwidthLevel;
        
        public Builder() {
        }
        
        public Builder(final LinkPropertiesChanged linkPropertiesChanged) {
            super(linkPropertiesChanged);
            if (linkPropertiesChanged != null) {
                this.bandwidthLevel = linkPropertiesChanged.bandwidthLevel;
            }
        }
        
        public Builder bandwidthLevel(final Integer bandwidthLevel) {
            this.bandwidthLevel = bandwidthLevel;
            return this;
        }
        
        public LinkPropertiesChanged build() {
            return new LinkPropertiesChanged(this, null);
        }
    }
}
