package com.navdy.service.library.events.photo;

import com.squareup.wire.ProtoField;
import com.squareup.wire.Message;

public final class PhotoUpdateQuery extends Message
{
    public static final String DEFAULT_ALBUM = "";
    public static final String DEFAULT_AUTHOR = "";
    public static final String DEFAULT_CHECKSUM = "";
    public static final String DEFAULT_IDENTIFIER = "";
    public static final PhotoType DEFAULT_PHOTOTYPE;
    public static final Long DEFAULT_SIZE;
    public static final String DEFAULT_TRACK = "";
    private static final long serialVersionUID = 0L;
    @ProtoField(tag = 5, type = Datatype.STRING)
    public final String album;
    @ProtoField(tag = 7, type = Datatype.STRING)
    public final String author;
    @ProtoField(tag = 4, type = Datatype.STRING)
    public final String checksum;
    @ProtoField(tag = 1, type = Datatype.STRING)
    public final String identifier;
    @ProtoField(tag = 2, type = Datatype.ENUM)
    public final PhotoType photoType;
    @ProtoField(tag = 3, type = Datatype.INT64)
    public final Long size;
    @ProtoField(tag = 6, type = Datatype.STRING)
    public final String track;
    
    static {
        DEFAULT_PHOTOTYPE = PhotoType.PHOTO_CONTACT;
        DEFAULT_SIZE = 0L;
    }
    
    private PhotoUpdateQuery(final Builder builder) {
        this(builder.identifier, builder.photoType, builder.size, builder.checksum, builder.album, builder.track, builder.author);
        this.setBuilder((Message.Builder)builder);
    }
    
    public PhotoUpdateQuery(final String identifier, final PhotoType photoType, final Long size, final String checksum, final String album, final String track, final String author) {
        this.identifier = identifier;
        this.photoType = photoType;
        this.size = size;
        this.checksum = checksum;
        this.album = album;
        this.track = track;
        this.author = author;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o != this) {
            if (!(o instanceof PhotoUpdateQuery)) {
                b = false;
            }
            else {
                final PhotoUpdateQuery photoUpdateQuery = (PhotoUpdateQuery)o;
                if (!this.equals(this.identifier, photoUpdateQuery.identifier) || !this.equals(this.photoType, photoUpdateQuery.photoType) || !this.equals(this.size, photoUpdateQuery.size) || !this.equals(this.checksum, photoUpdateQuery.checksum) || !this.equals(this.album, photoUpdateQuery.album) || !this.equals(this.track, photoUpdateQuery.track) || !this.equals(this.author, photoUpdateQuery.author)) {
                    b = false;
                }
            }
        }
        return b;
    }
    
    @Override
    public int hashCode() {
        int hashCode = 0;
        int hashCode2;
        if ((hashCode2 = this.hashCode) == 0) {
            int hashCode3;
            if (this.identifier != null) {
                hashCode3 = this.identifier.hashCode();
            }
            else {
                hashCode3 = 0;
            }
            int hashCode4;
            if (this.photoType != null) {
                hashCode4 = this.photoType.hashCode();
            }
            else {
                hashCode4 = 0;
            }
            int hashCode5;
            if (this.size != null) {
                hashCode5 = this.size.hashCode();
            }
            else {
                hashCode5 = 0;
            }
            int hashCode6;
            if (this.checksum != null) {
                hashCode6 = this.checksum.hashCode();
            }
            else {
                hashCode6 = 0;
            }
            int hashCode7;
            if (this.album != null) {
                hashCode7 = this.album.hashCode();
            }
            else {
                hashCode7 = 0;
            }
            int hashCode8;
            if (this.track != null) {
                hashCode8 = this.track.hashCode();
            }
            else {
                hashCode8 = 0;
            }
            if (this.author != null) {
                hashCode = this.author.hashCode();
            }
            hashCode2 = (((((hashCode3 * 37 + hashCode4) * 37 + hashCode5) * 37 + hashCode6) * 37 + hashCode7) * 37 + hashCode8) * 37 + hashCode;
            this.hashCode = hashCode2;
        }
        return hashCode2;
    }
    
    public static final class Builder extends Message.Builder<PhotoUpdateQuery>
    {
        public String album;
        public String author;
        public String checksum;
        public String identifier;
        public PhotoType photoType;
        public Long size;
        public String track;
        
        public Builder() {
        }
        
        public Builder(final PhotoUpdateQuery photoUpdateQuery) {
            super(photoUpdateQuery);
            if (photoUpdateQuery != null) {
                this.identifier = photoUpdateQuery.identifier;
                this.photoType = photoUpdateQuery.photoType;
                this.size = photoUpdateQuery.size;
                this.checksum = photoUpdateQuery.checksum;
                this.album = photoUpdateQuery.album;
                this.track = photoUpdateQuery.track;
                this.author = photoUpdateQuery.author;
            }
        }
        
        public Builder album(final String album) {
            this.album = album;
            return this;
        }
        
        public Builder author(final String author) {
            this.author = author;
            return this;
        }
        
        public PhotoUpdateQuery build() {
            return new PhotoUpdateQuery(this, null);
        }
        
        public Builder checksum(final String checksum) {
            this.checksum = checksum;
            return this;
        }
        
        public Builder identifier(final String identifier) {
            this.identifier = identifier;
            return this;
        }
        
        public Builder photoType(final PhotoType photoType) {
            this.photoType = photoType;
            return this;
        }
        
        public Builder size(final Long size) {
            this.size = size;
            return this;
        }
        
        public Builder track(final String track) {
            this.track = track;
            return this;
        }
    }
}
