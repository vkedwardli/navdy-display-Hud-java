package com.navdy.service.library.events.destination;

import com.squareup.wire.ProtoEnum;
import java.util.Collections;
import com.navdy.service.library.events.location.LatLong;
import com.squareup.wire.ProtoField;
import com.navdy.service.library.events.places.PlaceType;
import com.navdy.service.library.events.contacts.PhoneNumber;
import com.navdy.service.library.events.contacts.Contact;
import java.util.List;
import com.squareup.wire.Message;

public final class Destination extends Message
{
    public static final List<Contact> DEFAULT_CONTACTS;
    public static final String DEFAULT_DESTINATIONDISTANCE = "";
    public static final Integer DEFAULT_DESTINATIONICON;
    public static final Integer DEFAULT_DESTINATIONICONBKCOLOR;
    public static final String DEFAULT_DESTINATION_SUBTITLE = "";
    public static final String DEFAULT_DESTINATION_TITLE = "";
    public static final FavoriteType DEFAULT_FAVORITE_TYPE;
    public static final String DEFAULT_FULL_ADDRESS = "";
    public static final String DEFAULT_IDENTIFIER = "";
    public static final Boolean DEFAULT_IS_RECOMMENDATION;
    public static final Long DEFAULT_LAST_NAVIGATED_TO;
    public static final List<PhoneNumber> DEFAULT_PHONENUMBERS;
    public static final String DEFAULT_PLACE_ID = "";
    public static final PlaceType DEFAULT_PLACE_TYPE;
    public static final SuggestionType DEFAULT_SUGGESTION_TYPE;
    private static final long serialVersionUID = 0L;
    @ProtoField(label = Label.REPEATED, messageType = Contact.class, tag = 18)
    public final List<Contact> contacts;
    @ProtoField(tag = 16, type = Datatype.STRING)
    public final String destinationDistance;
    @ProtoField(tag = 13, type = Datatype.INT32)
    public final Integer destinationIcon;
    @ProtoField(tag = 14, type = Datatype.INT32)
    public final Integer destinationIconBkColor;
    @ProtoField(label = Label.REQUIRED, tag = 5, type = Datatype.STRING)
    public final String destination_subtitle;
    @ProtoField(label = Label.REQUIRED, tag = 4, type = Datatype.STRING)
    public final String destination_title;
    @ProtoField(tag = 2)
    public final LatLong display_position;
    @ProtoField(label = Label.REQUIRED, tag = 7, type = Datatype.ENUM)
    public final FavoriteType favorite_type;
    @ProtoField(label = Label.REQUIRED, tag = 3, type = Datatype.STRING)
    public final String full_address;
    @ProtoField(label = Label.REQUIRED, tag = 8, type = Datatype.STRING)
    public final String identifier;
    @ProtoField(tag = 10, type = Datatype.BOOL)
    public final Boolean is_recommendation;
    @ProtoField(tag = 11, type = Datatype.INT64)
    public final Long last_navigated_to;
    @ProtoField(label = Label.REQUIRED, tag = 1)
    public final LatLong navigation_position;
    @ProtoField(label = Label.REPEATED, messageType = PhoneNumber.class, tag = 17)
    public final List<PhoneNumber> phoneNumbers;
    @ProtoField(tag = 15, type = Datatype.STRING)
    public final String place_id;
    @ProtoField(tag = 12, type = Datatype.ENUM)
    public final PlaceType place_type;
    @ProtoField(tag = 9, type = Datatype.ENUM)
    public final SuggestionType suggestion_type;
    
    static {
        DEFAULT_FAVORITE_TYPE = FavoriteType.FAVORITE_NONE;
        DEFAULT_SUGGESTION_TYPE = SuggestionType.SUGGESTION_NONE;
        DEFAULT_IS_RECOMMENDATION = false;
        DEFAULT_LAST_NAVIGATED_TO = 0L;
        DEFAULT_PLACE_TYPE = PlaceType.PLACE_TYPE_UNKNOWN;
        DEFAULT_DESTINATIONICON = 0;
        DEFAULT_DESTINATIONICONBKCOLOR = 0;
        DEFAULT_PHONENUMBERS = Collections.<PhoneNumber>emptyList();
        DEFAULT_CONTACTS = Collections.<Contact>emptyList();
    }
    
    private Destination(final Builder builder) {
        this(builder.navigation_position, builder.display_position, builder.full_address, builder.destination_title, builder.destination_subtitle, builder.favorite_type, builder.identifier, builder.suggestion_type, builder.is_recommendation, builder.last_navigated_to, builder.place_type, builder.destinationIcon, builder.destinationIconBkColor, builder.place_id, builder.destinationDistance, builder.phoneNumbers, builder.contacts);
        this.setBuilder((Message.Builder)builder);
    }
    
    public Destination(final LatLong navigation_position, final LatLong display_position, final String full_address, final String destination_title, final String destination_subtitle, final FavoriteType favorite_type, final String identifier, final SuggestionType suggestion_type, final Boolean is_recommendation, final Long last_navigated_to, final PlaceType place_type, final Integer destinationIcon, final Integer destinationIconBkColor, final String place_id, final String destinationDistance, final List<PhoneNumber> list, final List<Contact> list2) {
        this.navigation_position = navigation_position;
        this.display_position = display_position;
        this.full_address = full_address;
        this.destination_title = destination_title;
        this.destination_subtitle = destination_subtitle;
        this.favorite_type = favorite_type;
        this.identifier = identifier;
        this.suggestion_type = suggestion_type;
        this.is_recommendation = is_recommendation;
        this.last_navigated_to = last_navigated_to;
        this.place_type = place_type;
        this.destinationIcon = destinationIcon;
        this.destinationIconBkColor = destinationIconBkColor;
        this.place_id = place_id;
        this.destinationDistance = destinationDistance;
        this.phoneNumbers = Message.<PhoneNumber>immutableCopyOf(list);
        this.contacts = Message.<Contact>immutableCopyOf(list2);
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o != this) {
            if (!(o instanceof Destination)) {
                b = false;
            }
            else {
                final Destination destination = (Destination)o;
                if (!this.equals(this.navigation_position, destination.navigation_position) || !this.equals(this.display_position, destination.display_position) || !this.equals(this.full_address, destination.full_address) || !this.equals(this.destination_title, destination.destination_title) || !this.equals(this.destination_subtitle, destination.destination_subtitle) || !this.equals(this.favorite_type, destination.favorite_type) || !this.equals(this.identifier, destination.identifier) || !this.equals(this.suggestion_type, destination.suggestion_type) || !this.equals(this.is_recommendation, destination.is_recommendation) || !this.equals(this.last_navigated_to, destination.last_navigated_to) || !this.equals(this.place_type, destination.place_type) || !this.equals(this.destinationIcon, destination.destinationIcon) || !this.equals(this.destinationIconBkColor, destination.destinationIconBkColor) || !this.equals(this.place_id, destination.place_id) || !this.equals(this.destinationDistance, destination.destinationDistance) || !this.equals(this.phoneNumbers, destination.phoneNumbers) || !this.equals(this.contacts, destination.contacts)) {
                    b = false;
                }
            }
        }
        return b;
    }
    
    @Override
    public int hashCode() {
        int hashCode = 1;
        int hashCode2 = 0;
        int hashCode3;
        if ((hashCode3 = this.hashCode) == 0) {
            int hashCode4;
            if (this.navigation_position != null) {
                hashCode4 = this.navigation_position.hashCode();
            }
            else {
                hashCode4 = 0;
            }
            int hashCode5;
            if (this.display_position != null) {
                hashCode5 = this.display_position.hashCode();
            }
            else {
                hashCode5 = 0;
            }
            int hashCode6;
            if (this.full_address != null) {
                hashCode6 = this.full_address.hashCode();
            }
            else {
                hashCode6 = 0;
            }
            int hashCode7;
            if (this.destination_title != null) {
                hashCode7 = this.destination_title.hashCode();
            }
            else {
                hashCode7 = 0;
            }
            int hashCode8;
            if (this.destination_subtitle != null) {
                hashCode8 = this.destination_subtitle.hashCode();
            }
            else {
                hashCode8 = 0;
            }
            int hashCode9;
            if (this.favorite_type != null) {
                hashCode9 = this.favorite_type.hashCode();
            }
            else {
                hashCode9 = 0;
            }
            int hashCode10;
            if (this.identifier != null) {
                hashCode10 = this.identifier.hashCode();
            }
            else {
                hashCode10 = 0;
            }
            int hashCode11;
            if (this.suggestion_type != null) {
                hashCode11 = this.suggestion_type.hashCode();
            }
            else {
                hashCode11 = 0;
            }
            int hashCode12;
            if (this.is_recommendation != null) {
                hashCode12 = this.is_recommendation.hashCode();
            }
            else {
                hashCode12 = 0;
            }
            int hashCode13;
            if (this.last_navigated_to != null) {
                hashCode13 = this.last_navigated_to.hashCode();
            }
            else {
                hashCode13 = 0;
            }
            int hashCode14;
            if (this.place_type != null) {
                hashCode14 = this.place_type.hashCode();
            }
            else {
                hashCode14 = 0;
            }
            int hashCode15;
            if (this.destinationIcon != null) {
                hashCode15 = this.destinationIcon.hashCode();
            }
            else {
                hashCode15 = 0;
            }
            int hashCode16;
            if (this.destinationIconBkColor != null) {
                hashCode16 = this.destinationIconBkColor.hashCode();
            }
            else {
                hashCode16 = 0;
            }
            int hashCode17;
            if (this.place_id != null) {
                hashCode17 = this.place_id.hashCode();
            }
            else {
                hashCode17 = 0;
            }
            if (this.destinationDistance != null) {
                hashCode2 = this.destinationDistance.hashCode();
            }
            int hashCode18;
            if (this.phoneNumbers != null) {
                hashCode18 = this.phoneNumbers.hashCode();
            }
            else {
                hashCode18 = 1;
            }
            if (this.contacts != null) {
                hashCode = this.contacts.hashCode();
            }
            hashCode3 = (((((((((((((((hashCode4 * 37 + hashCode5) * 37 + hashCode6) * 37 + hashCode7) * 37 + hashCode8) * 37 + hashCode9) * 37 + hashCode10) * 37 + hashCode11) * 37 + hashCode12) * 37 + hashCode13) * 37 + hashCode14) * 37 + hashCode15) * 37 + hashCode16) * 37 + hashCode17) * 37 + hashCode2) * 37 + hashCode18) * 37 + hashCode;
            this.hashCode = hashCode3;
        }
        return hashCode3;
    }
    
    public static final class Builder extends Message.Builder<Destination>
    {
        public List<Contact> contacts;
        public String destinationDistance;
        public Integer destinationIcon;
        public Integer destinationIconBkColor;
        public String destination_subtitle;
        public String destination_title;
        public LatLong display_position;
        public FavoriteType favorite_type;
        public String full_address;
        public String identifier;
        public Boolean is_recommendation;
        public Long last_navigated_to;
        public LatLong navigation_position;
        public List<PhoneNumber> phoneNumbers;
        public String place_id;
        public PlaceType place_type;
        public SuggestionType suggestion_type;
        
        public Builder() {
        }
        
        public Builder(final Destination destination) {
            super(destination);
            if (destination != null) {
                this.navigation_position = destination.navigation_position;
                this.display_position = destination.display_position;
                this.full_address = destination.full_address;
                this.destination_title = destination.destination_title;
                this.destination_subtitle = destination.destination_subtitle;
                this.favorite_type = destination.favorite_type;
                this.identifier = destination.identifier;
                this.suggestion_type = destination.suggestion_type;
                this.is_recommendation = destination.is_recommendation;
                this.last_navigated_to = destination.last_navigated_to;
                this.place_type = destination.place_type;
                this.destinationIcon = destination.destinationIcon;
                this.destinationIconBkColor = destination.destinationIconBkColor;
                this.place_id = destination.place_id;
                this.destinationDistance = destination.destinationDistance;
                this.phoneNumbers = (List<PhoneNumber>)Message.<Object>copyOf((List<Object>)destination.phoneNumbers);
                this.contacts = (List<Contact>)Message.<Object>copyOf((List<Object>)destination.contacts);
            }
        }
        
        public Destination build() {
            ((Message.Builder)this).checkRequiredFields();
            return new Destination(this, null);
        }
        
        public Builder contacts(final List<Contact> list) {
            this.contacts = Message.Builder.<Contact>checkForNulls(list);
            return this;
        }
        
        public Builder destinationDistance(final String destinationDistance) {
            this.destinationDistance = destinationDistance;
            return this;
        }
        
        public Builder destinationIcon(final Integer destinationIcon) {
            this.destinationIcon = destinationIcon;
            return this;
        }
        
        public Builder destinationIconBkColor(final Integer destinationIconBkColor) {
            this.destinationIconBkColor = destinationIconBkColor;
            return this;
        }
        
        public Builder destination_subtitle(final String destination_subtitle) {
            this.destination_subtitle = destination_subtitle;
            return this;
        }
        
        public Builder destination_title(final String destination_title) {
            this.destination_title = destination_title;
            return this;
        }
        
        public Builder display_position(final LatLong display_position) {
            this.display_position = display_position;
            return this;
        }
        
        public Builder favorite_type(final FavoriteType favorite_type) {
            this.favorite_type = favorite_type;
            return this;
        }
        
        public Builder full_address(final String full_address) {
            this.full_address = full_address;
            return this;
        }
        
        public Builder identifier(final String identifier) {
            this.identifier = identifier;
            return this;
        }
        
        public Builder is_recommendation(final Boolean is_recommendation) {
            this.is_recommendation = is_recommendation;
            return this;
        }
        
        public Builder last_navigated_to(final Long last_navigated_to) {
            this.last_navigated_to = last_navigated_to;
            return this;
        }
        
        public Builder navigation_position(final LatLong navigation_position) {
            this.navigation_position = navigation_position;
            return this;
        }
        
        public Builder phoneNumbers(final List<PhoneNumber> list) {
            this.phoneNumbers = Message.Builder.<PhoneNumber>checkForNulls(list);
            return this;
        }
        
        public Builder place_id(final String place_id) {
            this.place_id = place_id;
            return this;
        }
        
        public Builder place_type(final PlaceType place_type) {
            this.place_type = place_type;
            return this;
        }
        
        public Builder suggestion_type(final SuggestionType suggestion_type) {
            this.suggestion_type = suggestion_type;
            return this;
        }
    }
    
    public enum FavoriteType implements ProtoEnum
    {
        FAVORITE_CONTACT(3), 
        FAVORITE_CUSTOM(10), 
        FAVORITE_HOME(1), 
        FAVORITE_NONE(0), 
        FAVORITE_WORK(2);
        
        private final int value;
        
        private FavoriteType(final int value) {
            this.value = value;
        }
        
        @Override
        public int getValue() {
            return this.value;
        }
    }
    
    public enum SuggestionType implements ProtoEnum
    {
        SUGGESTION_CALENDAR(1), 
        SUGGESTION_NONE(0), 
        SUGGESTION_RECENT(2);
        
        private final int value;
        
        private SuggestionType(final int value) {
            this.value = value;
        }
        
        @Override
        public int getValue() {
            return this.value;
        }
    }
}
