package com.navdy.service.library.events.navigation;

import com.squareup.wire.Message;

public final class GetNavigationSessionState extends Message
{
    private static final long serialVersionUID = 0L;
    
    public GetNavigationSessionState() {
    }
    
    private GetNavigationSessionState(final Builder builder) {
        this.setBuilder((Message.Builder)builder);
    }
    
    @Override
    public boolean equals(final Object o) {
        return o instanceof GetNavigationSessionState;
    }
    
    @Override
    public int hashCode() {
        return 0;
    }
    
    public static final class Builder extends Message.Builder<GetNavigationSessionState>
    {
        public Builder() {
        }
        
        public Builder(final GetNavigationSessionState getNavigationSessionState) {
            super(getNavigationSessionState);
        }
        
        public GetNavigationSessionState build() {
            return new GetNavigationSessionState(this, null);
        }
    }
}
