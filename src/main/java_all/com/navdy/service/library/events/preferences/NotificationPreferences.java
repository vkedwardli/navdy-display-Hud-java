package com.navdy.service.library.events.preferences;

import java.util.Collections;
import com.squareup.wire.ProtoField;
import com.navdy.service.library.events.notification.NotificationSetting;
import java.util.List;
import com.squareup.wire.Message;

public final class NotificationPreferences extends Message
{
    public static final Boolean DEFAULT_ENABLED;
    public static final Boolean DEFAULT_READALOUD;
    public static final Long DEFAULT_SERIAL_NUMBER;
    public static final List<NotificationSetting> DEFAULT_SETTINGS;
    public static final Boolean DEFAULT_SHOWCONTENT;
    private static final long serialVersionUID = 0L;
    @ProtoField(tag = 3, type = Datatype.BOOL)
    public final Boolean enabled;
    @ProtoField(tag = 4, type = Datatype.BOOL)
    public final Boolean readAloud;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.INT64)
    public final Long serial_number;
    @ProtoField(label = Label.REPEATED, messageType = NotificationSetting.class, tag = 2)
    public final List<NotificationSetting> settings;
    @ProtoField(tag = 5, type = Datatype.BOOL)
    public final Boolean showContent;
    
    static {
        DEFAULT_SERIAL_NUMBER = 0L;
        DEFAULT_SETTINGS = Collections.<NotificationSetting>emptyList();
        DEFAULT_ENABLED = false;
        DEFAULT_READALOUD = true;
        DEFAULT_SHOWCONTENT = true;
    }
    
    private NotificationPreferences(final Builder builder) {
        this(builder.serial_number, builder.settings, builder.enabled, builder.readAloud, builder.showContent);
        this.setBuilder((Message.Builder)builder);
    }
    
    public NotificationPreferences(final Long serial_number, final List<NotificationSetting> list, final Boolean enabled, final Boolean readAloud, final Boolean showContent) {
        this.serial_number = serial_number;
        this.settings = Message.<NotificationSetting>immutableCopyOf(list);
        this.enabled = enabled;
        this.readAloud = readAloud;
        this.showContent = showContent;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o != this) {
            if (!(o instanceof NotificationPreferences)) {
                b = false;
            }
            else {
                final NotificationPreferences notificationPreferences = (NotificationPreferences)o;
                if (!this.equals(this.serial_number, notificationPreferences.serial_number) || !this.equals(this.settings, notificationPreferences.settings) || !this.equals(this.enabled, notificationPreferences.enabled) || !this.equals(this.readAloud, notificationPreferences.readAloud) || !this.equals(this.showContent, notificationPreferences.showContent)) {
                    b = false;
                }
            }
        }
        return b;
    }
    
    @Override
    public int hashCode() {
        int hashCode = 0;
        int hashCode2;
        if ((hashCode2 = this.hashCode) == 0) {
            int hashCode3;
            if (this.serial_number != null) {
                hashCode3 = this.serial_number.hashCode();
            }
            else {
                hashCode3 = 0;
            }
            int hashCode4;
            if (this.settings != null) {
                hashCode4 = this.settings.hashCode();
            }
            else {
                hashCode4 = 1;
            }
            int hashCode5;
            if (this.enabled != null) {
                hashCode5 = this.enabled.hashCode();
            }
            else {
                hashCode5 = 0;
            }
            int hashCode6;
            if (this.readAloud != null) {
                hashCode6 = this.readAloud.hashCode();
            }
            else {
                hashCode6 = 0;
            }
            if (this.showContent != null) {
                hashCode = this.showContent.hashCode();
            }
            hashCode2 = (((hashCode3 * 37 + hashCode4) * 37 + hashCode5) * 37 + hashCode6) * 37 + hashCode;
            this.hashCode = hashCode2;
        }
        return hashCode2;
    }
    
    public static final class Builder extends Message.Builder<NotificationPreferences>
    {
        public Boolean enabled;
        public Boolean readAloud;
        public Long serial_number;
        public List<NotificationSetting> settings;
        public Boolean showContent;
        
        public Builder() {
        }
        
        public Builder(final NotificationPreferences notificationPreferences) {
            super(notificationPreferences);
            if (notificationPreferences != null) {
                this.serial_number = notificationPreferences.serial_number;
                this.settings = (List<NotificationSetting>)Message.<Object>copyOf((List<Object>)notificationPreferences.settings);
                this.enabled = notificationPreferences.enabled;
                this.readAloud = notificationPreferences.readAloud;
                this.showContent = notificationPreferences.showContent;
            }
        }
        
        public NotificationPreferences build() {
            ((Message.Builder)this).checkRequiredFields();
            return new NotificationPreferences(this, null);
        }
        
        public Builder enabled(final Boolean enabled) {
            this.enabled = enabled;
            return this;
        }
        
        public Builder readAloud(final Boolean readAloud) {
            this.readAloud = readAloud;
            return this;
        }
        
        public Builder serial_number(final Long serial_number) {
            this.serial_number = serial_number;
            return this;
        }
        
        public Builder settings(final List<NotificationSetting> list) {
            this.settings = Message.Builder.<NotificationSetting>checkForNulls(list);
            return this;
        }
        
        public Builder showContent(final Boolean showContent) {
            this.showContent = showContent;
            return this;
        }
    }
}
