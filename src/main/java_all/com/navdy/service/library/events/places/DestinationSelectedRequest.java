package com.navdy.service.library.events.places;

import com.squareup.wire.ProtoField;
import com.navdy.service.library.events.destination.Destination;
import com.squareup.wire.Message;

public final class DestinationSelectedRequest extends Message
{
    public static final String DEFAULT_REQUEST_ID = "";
    private static final long serialVersionUID = 0L;
    @ProtoField(tag = 2)
    public final Destination destination;
    @ProtoField(tag = 1, type = Datatype.STRING)
    public final String request_id;
    
    private DestinationSelectedRequest(final Builder builder) {
        this(builder.request_id, builder.destination);
        this.setBuilder((Message.Builder)builder);
    }
    
    public DestinationSelectedRequest(final String request_id, final Destination destination) {
        this.request_id = request_id;
        this.destination = destination;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o != this) {
            if (!(o instanceof DestinationSelectedRequest)) {
                b = false;
            }
            else {
                final DestinationSelectedRequest destinationSelectedRequest = (DestinationSelectedRequest)o;
                if (!this.equals(this.request_id, destinationSelectedRequest.request_id) || !this.equals(this.destination, destinationSelectedRequest.destination)) {
                    b = false;
                }
            }
        }
        return b;
    }
    
    @Override
    public int hashCode() {
        int hashCode = 0;
        int hashCode2;
        if ((hashCode2 = this.hashCode) == 0) {
            int hashCode3;
            if (this.request_id != null) {
                hashCode3 = this.request_id.hashCode();
            }
            else {
                hashCode3 = 0;
            }
            if (this.destination != null) {
                hashCode = this.destination.hashCode();
            }
            hashCode2 = hashCode3 * 37 + hashCode;
            this.hashCode = hashCode2;
        }
        return hashCode2;
    }
    
    public static final class Builder extends Message.Builder<DestinationSelectedRequest>
    {
        public Destination destination;
        public String request_id;
        
        public Builder() {
        }
        
        public Builder(final DestinationSelectedRequest destinationSelectedRequest) {
            super(destinationSelectedRequest);
            if (destinationSelectedRequest != null) {
                this.request_id = destinationSelectedRequest.request_id;
                this.destination = destinationSelectedRequest.destination;
            }
        }
        
        public DestinationSelectedRequest build() {
            return new DestinationSelectedRequest(this, null);
        }
        
        public Builder destination(final Destination destination) {
            this.destination = destination;
            return this;
        }
        
        public Builder request_id(final String request_id) {
            this.request_id = request_id;
            return this;
        }
    }
}
