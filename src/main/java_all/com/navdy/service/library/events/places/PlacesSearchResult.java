package com.navdy.service.library.events.places;

import com.navdy.service.library.events.location.Coordinate;
import com.squareup.wire.ProtoField;
import com.squareup.wire.Message;

public final class PlacesSearchResult extends Message
{
    public static final String DEFAULT_ADDRESS = "";
    public static final String DEFAULT_CATEGORY = "";
    public static final Double DEFAULT_DISTANCE;
    public static final String DEFAULT_LABEL = "";
    private static final long serialVersionUID = 0L;
    @ProtoField(tag = 4, type = Datatype.STRING)
    public final String address;
    @ProtoField(tag = 5, type = Datatype.STRING)
    public final String category;
    @ProtoField(tag = 3)
    public final Coordinate destinationLocation;
    @ProtoField(tag = 6, type = Datatype.DOUBLE)
    public final Double distance;
    @ProtoField(tag = 1, type = Datatype.STRING)
    public final String label;
    @ProtoField(tag = 2)
    public final Coordinate navigationPosition;
    
    static {
        DEFAULT_DISTANCE = 0.0;
    }
    
    private PlacesSearchResult(final Builder builder) {
        this(builder.label, builder.navigationPosition, builder.destinationLocation, builder.address, builder.category, builder.distance);
        this.setBuilder((Message.Builder)builder);
    }
    
    public PlacesSearchResult(final String label, final Coordinate navigationPosition, final Coordinate destinationLocation, final String address, final String category, final Double distance) {
        this.label = label;
        this.navigationPosition = navigationPosition;
        this.destinationLocation = destinationLocation;
        this.address = address;
        this.category = category;
        this.distance = distance;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o != this) {
            if (!(o instanceof PlacesSearchResult)) {
                b = false;
            }
            else {
                final PlacesSearchResult placesSearchResult = (PlacesSearchResult)o;
                if (!this.equals(this.label, placesSearchResult.label) || !this.equals(this.navigationPosition, placesSearchResult.navigationPosition) || !this.equals(this.destinationLocation, placesSearchResult.destinationLocation) || !this.equals(this.address, placesSearchResult.address) || !this.equals(this.category, placesSearchResult.category) || !this.equals(this.distance, placesSearchResult.distance)) {
                    b = false;
                }
            }
        }
        return b;
    }
    
    @Override
    public int hashCode() {
        int hashCode = 0;
        int hashCode2;
        if ((hashCode2 = this.hashCode) == 0) {
            int hashCode3;
            if (this.label != null) {
                hashCode3 = this.label.hashCode();
            }
            else {
                hashCode3 = 0;
            }
            int hashCode4;
            if (this.navigationPosition != null) {
                hashCode4 = this.navigationPosition.hashCode();
            }
            else {
                hashCode4 = 0;
            }
            int hashCode5;
            if (this.destinationLocation != null) {
                hashCode5 = this.destinationLocation.hashCode();
            }
            else {
                hashCode5 = 0;
            }
            int hashCode6;
            if (this.address != null) {
                hashCode6 = this.address.hashCode();
            }
            else {
                hashCode6 = 0;
            }
            int hashCode7;
            if (this.category != null) {
                hashCode7 = this.category.hashCode();
            }
            else {
                hashCode7 = 0;
            }
            if (this.distance != null) {
                hashCode = this.distance.hashCode();
            }
            hashCode2 = ((((hashCode3 * 37 + hashCode4) * 37 + hashCode5) * 37 + hashCode6) * 37 + hashCode7) * 37 + hashCode;
            this.hashCode = hashCode2;
        }
        return hashCode2;
    }
    
    public static final class Builder extends Message.Builder<PlacesSearchResult>
    {
        public String address;
        public String category;
        public Coordinate destinationLocation;
        public Double distance;
        public String label;
        public Coordinate navigationPosition;
        
        public Builder() {
        }
        
        public Builder(final PlacesSearchResult placesSearchResult) {
            super(placesSearchResult);
            if (placesSearchResult != null) {
                this.label = placesSearchResult.label;
                this.navigationPosition = placesSearchResult.navigationPosition;
                this.destinationLocation = placesSearchResult.destinationLocation;
                this.address = placesSearchResult.address;
                this.category = placesSearchResult.category;
                this.distance = placesSearchResult.distance;
            }
        }
        
        public Builder address(final String address) {
            this.address = address;
            return this;
        }
        
        public PlacesSearchResult build() {
            return new PlacesSearchResult(this, null);
        }
        
        public Builder category(final String category) {
            this.category = category;
            return this;
        }
        
        public Builder destinationLocation(final Coordinate destinationLocation) {
            this.destinationLocation = destinationLocation;
            return this;
        }
        
        public Builder distance(final Double distance) {
            this.distance = distance;
            return this;
        }
        
        public Builder label(final String label) {
            this.label = label;
            return this;
        }
        
        public Builder navigationPosition(final Coordinate navigationPosition) {
            this.navigationPosition = navigationPosition;
            return this;
        }
    }
}
