package com.navdy.service.library.events.audio;

import com.squareup.wire.ProtoEnum;
import com.squareup.wire.ProtoField;
import com.squareup.wire.Message;

public final class VoiceAssistResponse extends Message
{
    public static final VoiceAssistState DEFAULT_STATE;
    private static final long serialVersionUID = 0L;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.ENUM)
    public final VoiceAssistState state;
    
    static {
        DEFAULT_STATE = VoiceAssistState.VOICE_ASSIST_AVAILABLE;
    }
    
    private VoiceAssistResponse(final Builder builder) {
        this(builder.state);
        this.setBuilder((Message.Builder)builder);
    }
    
    public VoiceAssistResponse(final VoiceAssistState state) {
        this.state = state;
    }
    
    @Override
    public boolean equals(final Object o) {
        return o == this || (o instanceof VoiceAssistResponse && this.equals(this.state, ((VoiceAssistResponse)o).state));
    }
    
    @Override
    public int hashCode() {
        int hashCode = this.hashCode;
        if (hashCode == 0) {
            if (this.state != null) {
                hashCode = this.state.hashCode();
            }
            else {
                hashCode = 0;
            }
            this.hashCode = hashCode;
        }
        return hashCode;
    }
    
    public static final class Builder extends Message.Builder<VoiceAssistResponse>
    {
        public VoiceAssistState state;
        
        public Builder() {
        }
        
        public Builder(final VoiceAssistResponse voiceAssistResponse) {
            super(voiceAssistResponse);
            if (voiceAssistResponse != null) {
                this.state = voiceAssistResponse.state;
            }
        }
        
        public VoiceAssistResponse build() {
            ((Message.Builder)this).checkRequiredFields();
            return new VoiceAssistResponse(this, null);
        }
        
        public Builder state(final VoiceAssistState state) {
            this.state = state;
            return this;
        }
    }
    
    public enum VoiceAssistState implements ProtoEnum
    {
        VOICE_ASSIST_AVAILABLE(0), 
        VOICE_ASSIST_LISTENING(2), 
        VOICE_ASSIST_NOT_AVAILABLE(1), 
        VOICE_ASSIST_STOPPED(3);
        
        private final int value;
        
        private VoiceAssistState(final int value) {
            this.value = value;
        }
        
        @Override
        public int getValue() {
            return this.value;
        }
    }
}
