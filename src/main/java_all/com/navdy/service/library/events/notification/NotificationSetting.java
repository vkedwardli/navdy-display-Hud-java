package com.navdy.service.library.events.notification;

import com.squareup.wire.ProtoField;
import com.squareup.wire.Message;

public final class NotificationSetting extends Message
{
    public static final String DEFAULT_APP = "";
    public static final Boolean DEFAULT_ENABLED;
    private static final long serialVersionUID = 0L;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.STRING)
    public final String app;
    @ProtoField(label = Label.REQUIRED, tag = 2, type = Datatype.BOOL)
    public final Boolean enabled;
    
    static {
        DEFAULT_ENABLED = false;
    }
    
    private NotificationSetting(final Builder builder) {
        this(builder.app, builder.enabled);
        this.setBuilder((Message.Builder)builder);
    }
    
    public NotificationSetting(final String app, final Boolean enabled) {
        this.app = app;
        this.enabled = enabled;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o != this) {
            if (!(o instanceof NotificationSetting)) {
                b = false;
            }
            else {
                final NotificationSetting notificationSetting = (NotificationSetting)o;
                if (!this.equals(this.app, notificationSetting.app) || !this.equals(this.enabled, notificationSetting.enabled)) {
                    b = false;
                }
            }
        }
        return b;
    }
    
    @Override
    public int hashCode() {
        int hashCode = 0;
        int hashCode2;
        if ((hashCode2 = this.hashCode) == 0) {
            int hashCode3;
            if (this.app != null) {
                hashCode3 = this.app.hashCode();
            }
            else {
                hashCode3 = 0;
            }
            if (this.enabled != null) {
                hashCode = this.enabled.hashCode();
            }
            hashCode2 = hashCode3 * 37 + hashCode;
            this.hashCode = hashCode2;
        }
        return hashCode2;
    }
    
    public static final class Builder extends Message.Builder<NotificationSetting>
    {
        public String app;
        public Boolean enabled;
        
        public Builder() {
        }
        
        public Builder(final NotificationSetting notificationSetting) {
            super(notificationSetting);
            if (notificationSetting != null) {
                this.app = notificationSetting.app;
                this.enabled = notificationSetting.enabled;
            }
        }
        
        public Builder app(final String app) {
            this.app = app;
            return this;
        }
        
        public NotificationSetting build() {
            ((Message.Builder)this).checkRequiredFields();
            return new NotificationSetting(this, null);
        }
        
        public Builder enabled(final Boolean enabled) {
            this.enabled = enabled;
            return this;
        }
    }
}
