package com.navdy.service.library.events.contacts;

import java.util.Collections;
import com.squareup.wire.ProtoField;
import com.navdy.service.library.events.RequestStatus;
import java.util.List;
import com.squareup.wire.Message;

public final class ContactResponse extends Message
{
    public static final List<Contact> DEFAULT_CONTACTS;
    public static final String DEFAULT_IDENTIFIER = "";
    public static final RequestStatus DEFAULT_STATUS;
    public static final String DEFAULT_STATUSDETAIL = "";
    private static final long serialVersionUID = 0L;
    @ProtoField(label = Label.REPEATED, messageType = Contact.class, tag = 5)
    public final List<Contact> contacts;
    @ProtoField(tag = 3, type = Datatype.STRING)
    public final String identifier;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.ENUM)
    public final RequestStatus status;
    @ProtoField(tag = 2, type = Datatype.STRING)
    public final String statusDetail;
    
    static {
        DEFAULT_STATUS = RequestStatus.REQUEST_SUCCESS;
        DEFAULT_CONTACTS = Collections.<Contact>emptyList();
    }
    
    public ContactResponse(final RequestStatus status, final String statusDetail, final String identifier, final List<Contact> list) {
        this.status = status;
        this.statusDetail = statusDetail;
        this.identifier = identifier;
        this.contacts = Message.<Contact>immutableCopyOf(list);
    }
    
    private ContactResponse(final Builder builder) {
        this(builder.status, builder.statusDetail, builder.identifier, builder.contacts);
        this.setBuilder((Message.Builder)builder);
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o != this) {
            if (!(o instanceof ContactResponse)) {
                b = false;
            }
            else {
                final ContactResponse contactResponse = (ContactResponse)o;
                if (!this.equals(this.status, contactResponse.status) || !this.equals(this.statusDetail, contactResponse.statusDetail) || !this.equals(this.identifier, contactResponse.identifier) || !this.equals(this.contacts, contactResponse.contacts)) {
                    b = false;
                }
            }
        }
        return b;
    }
    
    @Override
    public int hashCode() {
        int hashCode = 0;
        int hashCode2;
        if ((hashCode2 = this.hashCode) == 0) {
            int hashCode3;
            if (this.status != null) {
                hashCode3 = this.status.hashCode();
            }
            else {
                hashCode3 = 0;
            }
            int hashCode4;
            if (this.statusDetail != null) {
                hashCode4 = this.statusDetail.hashCode();
            }
            else {
                hashCode4 = 0;
            }
            if (this.identifier != null) {
                hashCode = this.identifier.hashCode();
            }
            int hashCode5;
            if (this.contacts != null) {
                hashCode5 = this.contacts.hashCode();
            }
            else {
                hashCode5 = 1;
            }
            hashCode2 = ((hashCode3 * 37 + hashCode4) * 37 + hashCode) * 37 + hashCode5;
            this.hashCode = hashCode2;
        }
        return hashCode2;
    }
    
    public static final class Builder extends Message.Builder<ContactResponse>
    {
        public List<Contact> contacts;
        public String identifier;
        public RequestStatus status;
        public String statusDetail;
        
        public Builder() {
        }
        
        public Builder(final ContactResponse contactResponse) {
            super(contactResponse);
            if (contactResponse != null) {
                this.status = contactResponse.status;
                this.statusDetail = contactResponse.statusDetail;
                this.identifier = contactResponse.identifier;
                this.contacts = (List<Contact>)Message.<Object>copyOf((List<Object>)contactResponse.contacts);
            }
        }
        
        public ContactResponse build() {
            ((Message.Builder)this).checkRequiredFields();
            return new ContactResponse(this, null);
        }
        
        public Builder contacts(final List<Contact> list) {
            this.contacts = Message.Builder.<Contact>checkForNulls(list);
            return this;
        }
        
        public Builder identifier(final String identifier) {
            this.identifier = identifier;
            return this;
        }
        
        public Builder status(final RequestStatus status) {
            this.status = status;
            return this;
        }
        
        public Builder statusDetail(final String statusDetail) {
            this.statusDetail = statusDetail;
            return this;
        }
    }
}
