package com.navdy.service.library.events.connection;

import com.squareup.wire.ProtoEnum;
import com.squareup.wire.ProtoField;
import com.squareup.wire.Message;

public final class ConnectionStatus extends Message
{
    public static final String DEFAULT_REMOTEDEVICEID = "";
    public static final Status DEFAULT_STATUS;
    private static final long serialVersionUID = 0L;
    @ProtoField(tag = 2, type = Datatype.STRING)
    public final String remoteDeviceId;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.ENUM)
    public final Status status;
    
    static {
        DEFAULT_STATUS = Status.CONNECTION_PAIRING;
    }
    
    private ConnectionStatus(final Builder builder) {
        this(builder.status, builder.remoteDeviceId);
        this.setBuilder((Message.Builder)builder);
    }
    
    public ConnectionStatus(final Status status, final String remoteDeviceId) {
        this.status = status;
        this.remoteDeviceId = remoteDeviceId;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o != this) {
            if (!(o instanceof ConnectionStatus)) {
                b = false;
            }
            else {
                final ConnectionStatus connectionStatus = (ConnectionStatus)o;
                if (!this.equals(this.status, connectionStatus.status) || !this.equals(this.remoteDeviceId, connectionStatus.remoteDeviceId)) {
                    b = false;
                }
            }
        }
        return b;
    }
    
    @Override
    public int hashCode() {
        int hashCode = 0;
        int hashCode2;
        if ((hashCode2 = this.hashCode) == 0) {
            int hashCode3;
            if (this.status != null) {
                hashCode3 = this.status.hashCode();
            }
            else {
                hashCode3 = 0;
            }
            if (this.remoteDeviceId != null) {
                hashCode = this.remoteDeviceId.hashCode();
            }
            hashCode2 = hashCode3 * 37 + hashCode;
            this.hashCode = hashCode2;
        }
        return hashCode2;
    }
    
    public static final class Builder extends Message.Builder<ConnectionStatus>
    {
        public String remoteDeviceId;
        public Status status;
        
        public Builder() {
        }
        
        public Builder(final ConnectionStatus connectionStatus) {
            super(connectionStatus);
            if (connectionStatus != null) {
                this.status = connectionStatus.status;
                this.remoteDeviceId = connectionStatus.remoteDeviceId;
            }
        }
        
        public ConnectionStatus build() {
            ((Message.Builder)this).checkRequiredFields();
            return new ConnectionStatus(this, null);
        }
        
        public Builder remoteDeviceId(final String remoteDeviceId) {
            this.remoteDeviceId = remoteDeviceId;
            return this;
        }
        
        public Builder status(final Status status) {
            this.status = status;
            return this;
        }
    }
    
    public enum Status implements ProtoEnum
    {
        CONNECTION_FOUND(4), 
        CONNECTION_LOST(5), 
        CONNECTION_PAIRED_DEVICES_CHANGED(6), 
        CONNECTION_PAIRING(1), 
        CONNECTION_SEARCH_FINISHED(3), 
        CONNECTION_SEARCH_STARTED(2);
        
        private final int value;
        
        private Status(final int value) {
            this.value = value;
        }
        
        @Override
        public int getValue() {
            return this.value;
        }
    }
}
