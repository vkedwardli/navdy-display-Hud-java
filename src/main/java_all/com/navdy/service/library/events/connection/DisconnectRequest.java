package com.navdy.service.library.events.connection;

import com.squareup.wire.ProtoField;
import com.squareup.wire.Message;

public final class DisconnectRequest extends Message
{
    public static final Boolean DEFAULT_FORGET;
    private static final long serialVersionUID = 0L;
    @ProtoField(tag = 1, type = Datatype.BOOL)
    public final Boolean forget;
    
    static {
        DEFAULT_FORGET = false;
    }
    
    private DisconnectRequest(final Builder builder) {
        this(builder.forget);
        this.setBuilder((Message.Builder)builder);
    }
    
    public DisconnectRequest(final Boolean forget) {
        this.forget = forget;
    }
    
    @Override
    public boolean equals(final Object o) {
        return o == this || (o instanceof DisconnectRequest && this.equals(this.forget, ((DisconnectRequest)o).forget));
    }
    
    @Override
    public int hashCode() {
        int hashCode = this.hashCode;
        if (hashCode == 0) {
            if (this.forget != null) {
                hashCode = this.forget.hashCode();
            }
            else {
                hashCode = 0;
            }
            this.hashCode = hashCode;
        }
        return hashCode;
    }
    
    public static final class Builder extends Message.Builder<DisconnectRequest>
    {
        public Boolean forget;
        
        public Builder() {
        }
        
        public Builder(final DisconnectRequest disconnectRequest) {
            super(disconnectRequest);
            if (disconnectRequest != null) {
                this.forget = disconnectRequest.forget;
            }
        }
        
        public DisconnectRequest build() {
            return new DisconnectRequest(this, null);
        }
        
        public Builder forget(final Boolean forget) {
            this.forget = forget;
            return this;
        }
    }
}
