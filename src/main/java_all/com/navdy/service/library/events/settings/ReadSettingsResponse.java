package com.navdy.service.library.events.settings;

import java.util.Collections;
import com.squareup.wire.ProtoField;
import java.util.List;
import com.squareup.wire.Message;

public final class ReadSettingsResponse extends Message
{
    public static final List<Setting> DEFAULT_SETTINGS;
    private static final long serialVersionUID = 0L;
    @ProtoField(label = Label.REQUIRED, tag = 1)
    public final ScreenConfiguration screenConfiguration;
    @ProtoField(label = Label.REPEATED, messageType = Setting.class, tag = 2)
    public final List<Setting> settings;
    
    static {
        DEFAULT_SETTINGS = Collections.<Setting>emptyList();
    }
    
    private ReadSettingsResponse(final Builder builder) {
        this(builder.screenConfiguration, builder.settings);
        this.setBuilder((Message.Builder)builder);
    }
    
    public ReadSettingsResponse(final ScreenConfiguration screenConfiguration, final List<Setting> list) {
        this.screenConfiguration = screenConfiguration;
        this.settings = Message.<Setting>immutableCopyOf(list);
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o != this) {
            if (!(o instanceof ReadSettingsResponse)) {
                b = false;
            }
            else {
                final ReadSettingsResponse readSettingsResponse = (ReadSettingsResponse)o;
                if (!this.equals(this.screenConfiguration, readSettingsResponse.screenConfiguration) || !this.equals(this.settings, readSettingsResponse.settings)) {
                    b = false;
                }
            }
        }
        return b;
    }
    
    @Override
    public int hashCode() {
        int hashCode;
        if ((hashCode = this.hashCode) == 0) {
            int hashCode2;
            if (this.screenConfiguration != null) {
                hashCode2 = this.screenConfiguration.hashCode();
            }
            else {
                hashCode2 = 0;
            }
            int hashCode3;
            if (this.settings != null) {
                hashCode3 = this.settings.hashCode();
            }
            else {
                hashCode3 = 1;
            }
            hashCode = hashCode2 * 37 + hashCode3;
            this.hashCode = hashCode;
        }
        return hashCode;
    }
    
    public static final class Builder extends Message.Builder<ReadSettingsResponse>
    {
        public ScreenConfiguration screenConfiguration;
        public List<Setting> settings;
        
        public Builder() {
        }
        
        public Builder(final ReadSettingsResponse readSettingsResponse) {
            super(readSettingsResponse);
            if (readSettingsResponse != null) {
                this.screenConfiguration = readSettingsResponse.screenConfiguration;
                this.settings = (List<Setting>)Message.<Object>copyOf((List<Object>)readSettingsResponse.settings);
            }
        }
        
        public ReadSettingsResponse build() {
            ((Message.Builder)this).checkRequiredFields();
            return new ReadSettingsResponse(this, null);
        }
        
        public Builder screenConfiguration(final ScreenConfiguration screenConfiguration) {
            this.screenConfiguration = screenConfiguration;
            return this;
        }
        
        public Builder settings(final List<Setting> list) {
            this.settings = Message.Builder.<Setting>checkForNulls(list);
            return this;
        }
    }
}
