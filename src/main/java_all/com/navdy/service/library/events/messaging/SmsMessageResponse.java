package com.navdy.service.library.events.messaging;

import com.squareup.wire.ProtoField;
import com.navdy.service.library.events.RequestStatus;
import com.squareup.wire.Message;

public final class SmsMessageResponse extends Message
{
    public static final String DEFAULT_ID = "";
    public static final String DEFAULT_NAME = "";
    public static final String DEFAULT_NUMBER = "";
    public static final RequestStatus DEFAULT_STATUS;
    private static final long serialVersionUID = 0L;
    @ProtoField(tag = 4, type = Datatype.STRING)
    public final String id;
    @ProtoField(tag = 3, type = Datatype.STRING)
    public final String name;
    @ProtoField(tag = 2, type = Datatype.STRING)
    public final String number;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.ENUM)
    public final RequestStatus status;
    
    static {
        DEFAULT_STATUS = RequestStatus.REQUEST_SUCCESS;
    }
    
    public SmsMessageResponse(final RequestStatus status, final String number, final String name, final String id) {
        this.status = status;
        this.number = number;
        this.name = name;
        this.id = id;
    }
    
    private SmsMessageResponse(final Builder builder) {
        this(builder.status, builder.number, builder.name, builder.id);
        this.setBuilder((Message.Builder)builder);
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o != this) {
            if (!(o instanceof SmsMessageResponse)) {
                b = false;
            }
            else {
                final SmsMessageResponse smsMessageResponse = (SmsMessageResponse)o;
                if (!this.equals(this.status, smsMessageResponse.status) || !this.equals(this.number, smsMessageResponse.number) || !this.equals(this.name, smsMessageResponse.name) || !this.equals(this.id, smsMessageResponse.id)) {
                    b = false;
                }
            }
        }
        return b;
    }
    
    @Override
    public int hashCode() {
        int hashCode = 0;
        int hashCode2;
        if ((hashCode2 = this.hashCode) == 0) {
            int hashCode3;
            if (this.status != null) {
                hashCode3 = this.status.hashCode();
            }
            else {
                hashCode3 = 0;
            }
            int hashCode4;
            if (this.number != null) {
                hashCode4 = this.number.hashCode();
            }
            else {
                hashCode4 = 0;
            }
            int hashCode5;
            if (this.name != null) {
                hashCode5 = this.name.hashCode();
            }
            else {
                hashCode5 = 0;
            }
            if (this.id != null) {
                hashCode = this.id.hashCode();
            }
            hashCode2 = ((hashCode3 * 37 + hashCode4) * 37 + hashCode5) * 37 + hashCode;
            this.hashCode = hashCode2;
        }
        return hashCode2;
    }
    
    public static final class Builder extends Message.Builder<SmsMessageResponse>
    {
        public String id;
        public String name;
        public String number;
        public RequestStatus status;
        
        public Builder() {
        }
        
        public Builder(final SmsMessageResponse smsMessageResponse) {
            super(smsMessageResponse);
            if (smsMessageResponse != null) {
                this.status = smsMessageResponse.status;
                this.number = smsMessageResponse.number;
                this.name = smsMessageResponse.name;
                this.id = smsMessageResponse.id;
            }
        }
        
        public SmsMessageResponse build() {
            ((Message.Builder)this).checkRequiredFields();
            return new SmsMessageResponse(this, null);
        }
        
        public Builder id(final String id) {
            this.id = id;
            return this;
        }
        
        public Builder name(final String name) {
            this.name = name;
            return this;
        }
        
        public Builder number(final String number) {
            this.number = number;
            return this;
        }
        
        public Builder status(final RequestStatus status) {
            this.status = status;
            return this;
        }
    }
}
