package com.navdy.service.library.events.places;

import com.squareup.wire.ProtoField;
import com.squareup.wire.Message;

public final class PlacesSearchRequest extends Message
{
    public static final Integer DEFAULT_MAXRESULTS;
    public static final String DEFAULT_REQUESTID = "";
    public static final Integer DEFAULT_SEARCHAREA;
    public static final String DEFAULT_SEARCHQUERY = "";
    private static final long serialVersionUID = 0L;
    @ProtoField(tag = 3, type = Datatype.INT32)
    public final Integer maxResults;
    @ProtoField(tag = 4, type = Datatype.STRING)
    public final String requestId;
    @ProtoField(tag = 2, type = Datatype.INT32)
    public final Integer searchArea;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.STRING)
    public final String searchQuery;
    
    static {
        DEFAULT_SEARCHAREA = 0;
        DEFAULT_MAXRESULTS = 0;
    }
    
    private PlacesSearchRequest(final Builder builder) {
        this(builder.searchQuery, builder.searchArea, builder.maxResults, builder.requestId);
        this.setBuilder((Message.Builder)builder);
    }
    
    public PlacesSearchRequest(final String searchQuery, final Integer searchArea, final Integer maxResults, final String requestId) {
        this.searchQuery = searchQuery;
        this.searchArea = searchArea;
        this.maxResults = maxResults;
        this.requestId = requestId;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o != this) {
            if (!(o instanceof PlacesSearchRequest)) {
                b = false;
            }
            else {
                final PlacesSearchRequest placesSearchRequest = (PlacesSearchRequest)o;
                if (!this.equals(this.searchQuery, placesSearchRequest.searchQuery) || !this.equals(this.searchArea, placesSearchRequest.searchArea) || !this.equals(this.maxResults, placesSearchRequest.maxResults) || !this.equals(this.requestId, placesSearchRequest.requestId)) {
                    b = false;
                }
            }
        }
        return b;
    }
    
    @Override
    public int hashCode() {
        int hashCode = 0;
        int hashCode2;
        if ((hashCode2 = this.hashCode) == 0) {
            int hashCode3;
            if (this.searchQuery != null) {
                hashCode3 = this.searchQuery.hashCode();
            }
            else {
                hashCode3 = 0;
            }
            int hashCode4;
            if (this.searchArea != null) {
                hashCode4 = this.searchArea.hashCode();
            }
            else {
                hashCode4 = 0;
            }
            int hashCode5;
            if (this.maxResults != null) {
                hashCode5 = this.maxResults.hashCode();
            }
            else {
                hashCode5 = 0;
            }
            if (this.requestId != null) {
                hashCode = this.requestId.hashCode();
            }
            hashCode2 = ((hashCode3 * 37 + hashCode4) * 37 + hashCode5) * 37 + hashCode;
            this.hashCode = hashCode2;
        }
        return hashCode2;
    }
    
    public static final class Builder extends Message.Builder<PlacesSearchRequest>
    {
        public Integer maxResults;
        public String requestId;
        public Integer searchArea;
        public String searchQuery;
        
        public Builder() {
        }
        
        public Builder(final PlacesSearchRequest placesSearchRequest) {
            super(placesSearchRequest);
            if (placesSearchRequest != null) {
                this.searchQuery = placesSearchRequest.searchQuery;
                this.searchArea = placesSearchRequest.searchArea;
                this.maxResults = placesSearchRequest.maxResults;
                this.requestId = placesSearchRequest.requestId;
            }
        }
        
        public PlacesSearchRequest build() {
            ((Message.Builder)this).checkRequiredFields();
            return new PlacesSearchRequest(this, null);
        }
        
        public Builder maxResults(final Integer maxResults) {
            this.maxResults = maxResults;
            return this;
        }
        
        public Builder requestId(final String requestId) {
            this.requestId = requestId;
            return this;
        }
        
        public Builder searchArea(final Integer searchArea) {
            this.searchArea = searchArea;
            return this;
        }
        
        public Builder searchQuery(final String searchQuery) {
            this.searchQuery = searchQuery;
            return this;
        }
    }
}
