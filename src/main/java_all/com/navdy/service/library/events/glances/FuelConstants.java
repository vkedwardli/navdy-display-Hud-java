package com.navdy.service.library.events.glances;

import com.squareup.wire.ProtoEnum;

public enum FuelConstants implements ProtoEnum
{
    FUEL_LEVEL(0), 
    GAS_STATION_ADDRESS(3), 
    GAS_STATION_DISTANCE(4), 
    GAS_STATION_NAME(2), 
    NO_ROUTE(1);
    
    private final int value;
    
    private FuelConstants(final int value) {
        this.value = value;
    }
    
    @Override
    public int getValue() {
        return this.value;
    }
}
