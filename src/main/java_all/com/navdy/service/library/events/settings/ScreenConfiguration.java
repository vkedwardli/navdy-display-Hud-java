package com.navdy.service.library.events.settings;

import com.squareup.wire.ProtoField;
import com.squareup.wire.Message;

public final class ScreenConfiguration extends Message
{
    public static final Integer DEFAULT_MARGINBOTTOM;
    public static final Integer DEFAULT_MARGINLEFT;
    public static final Integer DEFAULT_MARGINRIGHT;
    public static final Integer DEFAULT_MARGINTOP;
    public static final Float DEFAULT_SCALEX;
    public static final Float DEFAULT_SCALEY;
    private static final long serialVersionUID = 0L;
    @ProtoField(label = Label.REQUIRED, tag = 4, type = Datatype.INT32)
    public final Integer marginBottom;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.INT32)
    public final Integer marginLeft;
    @ProtoField(label = Label.REQUIRED, tag = 3, type = Datatype.INT32)
    public final Integer marginRight;
    @ProtoField(label = Label.REQUIRED, tag = 2, type = Datatype.INT32)
    public final Integer marginTop;
    @ProtoField(label = Label.REQUIRED, tag = 5, type = Datatype.FLOAT)
    public final Float scaleX;
    @ProtoField(label = Label.REQUIRED, tag = 6, type = Datatype.FLOAT)
    public final Float scaleY;
    
    static {
        DEFAULT_MARGINLEFT = 0;
        DEFAULT_MARGINTOP = 0;
        DEFAULT_MARGINRIGHT = 0;
        DEFAULT_MARGINBOTTOM = 0;
        DEFAULT_SCALEX = 1.0f;
        DEFAULT_SCALEY = 1.0f;
    }
    
    private ScreenConfiguration(final Builder builder) {
        this(builder.marginLeft, builder.marginTop, builder.marginRight, builder.marginBottom, builder.scaleX, builder.scaleY);
        this.setBuilder((Message.Builder)builder);
    }
    
    public ScreenConfiguration(final Integer marginLeft, final Integer marginTop, final Integer marginRight, final Integer marginBottom, final Float scaleX, final Float scaleY) {
        this.marginLeft = marginLeft;
        this.marginTop = marginTop;
        this.marginRight = marginRight;
        this.marginBottom = marginBottom;
        this.scaleX = scaleX;
        this.scaleY = scaleY;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o != this) {
            if (!(o instanceof ScreenConfiguration)) {
                b = false;
            }
            else {
                final ScreenConfiguration screenConfiguration = (ScreenConfiguration)o;
                if (!this.equals(this.marginLeft, screenConfiguration.marginLeft) || !this.equals(this.marginTop, screenConfiguration.marginTop) || !this.equals(this.marginRight, screenConfiguration.marginRight) || !this.equals(this.marginBottom, screenConfiguration.marginBottom) || !this.equals(this.scaleX, screenConfiguration.scaleX) || !this.equals(this.scaleY, screenConfiguration.scaleY)) {
                    b = false;
                }
            }
        }
        return b;
    }
    
    @Override
    public int hashCode() {
        int hashCode = 0;
        int hashCode2;
        if ((hashCode2 = this.hashCode) == 0) {
            int hashCode3;
            if (this.marginLeft != null) {
                hashCode3 = this.marginLeft.hashCode();
            }
            else {
                hashCode3 = 0;
            }
            int hashCode4;
            if (this.marginTop != null) {
                hashCode4 = this.marginTop.hashCode();
            }
            else {
                hashCode4 = 0;
            }
            int hashCode5;
            if (this.marginRight != null) {
                hashCode5 = this.marginRight.hashCode();
            }
            else {
                hashCode5 = 0;
            }
            int hashCode6;
            if (this.marginBottom != null) {
                hashCode6 = this.marginBottom.hashCode();
            }
            else {
                hashCode6 = 0;
            }
            int hashCode7;
            if (this.scaleX != null) {
                hashCode7 = this.scaleX.hashCode();
            }
            else {
                hashCode7 = 0;
            }
            if (this.scaleY != null) {
                hashCode = this.scaleY.hashCode();
            }
            hashCode2 = ((((hashCode3 * 37 + hashCode4) * 37 + hashCode5) * 37 + hashCode6) * 37 + hashCode7) * 37 + hashCode;
            this.hashCode = hashCode2;
        }
        return hashCode2;
    }
    
    public static final class Builder extends Message.Builder<ScreenConfiguration>
    {
        public Integer marginBottom;
        public Integer marginLeft;
        public Integer marginRight;
        public Integer marginTop;
        public Float scaleX;
        public Float scaleY;
        
        public Builder() {
        }
        
        public Builder(final ScreenConfiguration screenConfiguration) {
            super(screenConfiguration);
            if (screenConfiguration != null) {
                this.marginLeft = screenConfiguration.marginLeft;
                this.marginTop = screenConfiguration.marginTop;
                this.marginRight = screenConfiguration.marginRight;
                this.marginBottom = screenConfiguration.marginBottom;
                this.scaleX = screenConfiguration.scaleX;
                this.scaleY = screenConfiguration.scaleY;
            }
        }
        
        public ScreenConfiguration build() {
            ((Message.Builder)this).checkRequiredFields();
            return new ScreenConfiguration(this, null);
        }
        
        public Builder marginBottom(final Integer marginBottom) {
            this.marginBottom = marginBottom;
            return this;
        }
        
        public Builder marginLeft(final Integer marginLeft) {
            this.marginLeft = marginLeft;
            return this;
        }
        
        public Builder marginRight(final Integer marginRight) {
            this.marginRight = marginRight;
            return this;
        }
        
        public Builder marginTop(final Integer marginTop) {
            this.marginTop = marginTop;
            return this;
        }
        
        public Builder scaleX(final Float scaleX) {
            this.scaleX = scaleX;
            return this;
        }
        
        public Builder scaleY(final Float scaleY) {
            this.scaleY = scaleY;
            return this;
        }
    }
}
