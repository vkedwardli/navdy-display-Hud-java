package com.navdy.service.library.task;

import java.util.concurrent.RunnableFuture;
import java.util.concurrent.ThreadPoolExecutor;
import java.io.Serializable;
import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;
import java.util.concurrent.Future;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.ExecutorService;
import android.util.SparseArray;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.Comparator;
import com.navdy.service.library.log.Logger;
import java.util.concurrent.atomic.AtomicLong;

public final class TaskManager
{
    private static final int INITIAL_CAPACITY = 10;
    private static final boolean VERBOSE = false;
    private static AtomicLong orderCounter;
    private static Logger sLogger;
    private static final Comparator<Runnable> sPriorityComparator;
    private static final TaskManager sSingleton;
    private static final AtomicInteger threadNameCounter;
    private final SparseArray<ExecutorService> executors;
    private boolean initialized;
    
    static {
        TaskManager.sLogger = new Logger(TaskManager.class);
        TaskManager.orderCounter = new AtomicLong(1L);
        threadNameCounter = new AtomicInteger(0);
        sPriorityComparator = new PriorityTaskComparator();
        sSingleton = new TaskManager();
    }
    
    private TaskManager() {
        this.executors = (SparseArray<ExecutorService>)new SparseArray();
    }
    
    private ExecutorService createExecutor(final int n) {
        return new TaskManagerExecutor(n, n, 60L, TimeUnit.SECONDS, new PriorityBlockingQueue<Runnable>(10, TaskManager.sPriorityComparator), new ThreadFactory() {
            @Override
            public Thread newThread(final Runnable runnable) {
                final Thread thread = new Thread(runnable);
                thread.setName("navdy_" + TaskManager.threadNameCounter.incrementAndGet());
                thread.setDaemon(true);
                return thread;
            }
        });
    }
    
    public static TaskManager getInstance() {
        return TaskManager.sSingleton;
    }
    
    public void addTaskQueue(final int n, final int n2) {
        if (this.initialized) {
            throw new IllegalStateException("already initialized");
        }
        if (this.executors.get(n) != null) {
            throw new IllegalArgumentException("already exists");
        }
        this.executors.put(n, this.createExecutor(n2));
    }
    
    public Future execute(final Runnable runnable, final int n) {
        return this.execute(runnable, n, TaskPriority.NORMAL);
    }
    
    public Future execute(final Runnable runnable, final int n, final TaskPriority taskPriority) {
        Future<?> submit;
        if (!this.initialized) {
            TaskManager.sLogger.w("Task manager not initialized", new Throwable());
            submit = null;
        }
        else {
            if (runnable == null) {
                throw new IllegalArgumentException();
            }
            submit = ((ExecutorService)this.executors.get(n)).submit(new PriorityRunnable(runnable, taskPriority, TaskManager.orderCounter.getAndIncrement()));
        }
        return submit;
    }
    
    public ExecutorService getExecutor(final int n) {
        return (ExecutorService)this.executors.get(n);
    }
    
    public void init() {
        if (this.initialized) {
            throw new IllegalStateException("already initialized");
        }
        this.initialized = true;
    }
    
    private static class PriorityRunnable implements Runnable
    {
        final long order;
        final int priority;
        final Runnable runnable;
        
        PriorityRunnable(final Runnable runnable, final TaskPriority taskPriority, final long order) {
            this.runnable = runnable;
            this.priority = taskPriority.getValue();
            this.order = order;
        }
        
        @Override
        public void run() {
            try {
                this.runnable.run();
            }
            catch (Throwable t) {
                TaskManager.sLogger.e("TaskManager:", t);
            }
        }
    }
    
    private static final class PriorityTask<T> extends FutureTask<T> implements Comparable<PriorityTask<T>>
    {
        private final PriorityRunnable priorityRunnable;
        
        public PriorityTask(final PriorityRunnable priorityRunnable, final T t) {
            super(priorityRunnable, t);
            this.priorityRunnable = priorityRunnable;
        }
        
        public PriorityTask(final PriorityRunnable priorityRunnable, final Callable<T> callable) {
            super(callable);
            this.priorityRunnable = priorityRunnable;
        }
        
        @Override
        public int compareTo(final PriorityTask<T> priorityTask) {
            int n;
            if ((n = this.priorityRunnable.priority - priorityTask.priorityRunnable.priority) == 0) {
                n = (int)(this.priorityRunnable.order - priorityTask.priorityRunnable.order);
            }
            return n;
        }
        
        @Override
        public boolean equals(final Object o) {
            boolean b2;
            final boolean b = b2 = false;
            if (o instanceof PriorityTask) {
                final PriorityTask priorityTask = (PriorityTask)o;
                b2 = b;
                if (this.priorityRunnable.priority == priorityTask.priorityRunnable.priority) {
                    b2 = b;
                    if (this.priorityRunnable.order == priorityTask.priorityRunnable.order) {
                        b2 = true;
                    }
                }
            }
            return b2;
        }
        
        @Override
        public int hashCode() {
            return (int)this.priorityRunnable.order;
        }
    }
    
    private static class PriorityTaskComparator implements Comparator<Runnable>, Serializable
    {
        @Override
        public int compare(final Runnable runnable, final Runnable runnable2) {
            return ((PriorityTask)runnable).compareTo((PriorityTask)runnable2);
        }
    }
    
    public static class TaskManagerExecutor extends ThreadPoolExecutor
    {
        TaskManagerExecutor(final int n, final int n2, final long n3, final TimeUnit timeUnit, final BlockingQueue<Runnable> blockingQueue, final ThreadFactory threadFactory) {
            super(n, n2, n3, timeUnit, blockingQueue, threadFactory);
        }
        
        @Override
        protected <T> RunnableFuture<T> newTaskFor(final Runnable runnable, final T t) {
            return new PriorityTask<T>((PriorityRunnable)runnable, t);
        }
    }
    
    public enum TaskPriority
    {
        HIGH(3), 
        LOW(1), 
        NORMAL(2);
        
        private final int val;
        
        private TaskPriority(final int val) {
            this.val = val;
        }
        
        public int getValue() {
            return this.val;
        }
    }
}
