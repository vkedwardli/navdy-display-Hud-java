package com.navdy.hud.mfi;

import android.content.Intent;
import java.io.IOException;
import android.util.Log;
import android.content.Context;

public class AuthCoprocessor
{
    private static final int ACCESSORY_CERTIFICATE_DATA_REGISTER = 49;
    private static final int ACCESSORY_CERTIFICATE_LENGTH_REGISTER = 48;
    private static final int ACCESSORY_SIGNATURE_GENERATION = 1;
    private static final int AUTH_ERROR_FLAG = 128;
    private static final int BUS = 0;
    private static final int CHALLENGE_DATA_REGISTER = 33;
    private static final int CHALLENGE_LENGTH_REGISTER = 32;
    private static final int DEVICE_ADDRESS = 17;
    private static final int DEVICE_CERTIFICATE_DATA_REGISTER = 81;
    private static final int DEVICE_CERTIFICATE_LENGTH_REGISTER = 80;
    private static final int DEVICE_CERTIFICATE_VALIDATION = 4;
    private static final int DEVICE_CHALLENGE_GENERATION = 2;
    private static final int DEVICE_SIGNATURE_VALIDATION = 3;
    private static final int ERROR_CODE_REGISTER = 5;
    private static final int PAGE_SIZE = 128;
    private static final int SIGNATURE_DATA_REGISTER = 18;
    private static final int SIGNATURE_LENGTH_REGISTER = 17;
    private static final int STATUS_CONTROL_REGISTER = 16;
    private static final String TAG = "AuthCoprocessor";
    private Context context;
    private boolean crashReportSent;
    private I2C i2c;
    private IAPListener iapListener;
    
    public AuthCoprocessor(final IAPListener iapListener, final Context context) {
        this.crashReportSent = false;
        this.i2c = new I2C(0);
        if (iapListener == null) {
            this.iapListener = new IAPListener() {
                @Override
                public void onCoprocessorStatusCheckFailed(final String s, final String s2, final String s3) {
                }
                
                @Override
                public void onDeviceAuthenticationSuccess(final int n) {
                }
            };
        }
        else {
            this.iapListener = iapListener;
        }
        this.context = context;
    }
    
    private void checkStatus(final String s) throws IOException {
        final int byte1 = this.i2c.readByte(17, 16);
        final int byte2 = this.i2c.readByte(17, 5);
        if ((byte1 & 0x80) != 0x0) {
            final String bytesToHex = Utils.bytesToHex(new byte[] { (byte)byte1 });
            final String bytesToHex2 = Utils.bytesToHex(new byte[] { (byte)byte2 });
            Log.e("AuthCoprocessor", "auth CP status: " + bytesToHex);
            Log.e("AuthCoprocessor", "auth CP error code: " + bytesToHex2);
            this.sendCrashReport();
            try {
                this.iapListener.onCoprocessorStatusCheckFailed(s, bytesToHex, bytesToHex2);
                throw new RuntimeException("failed to " + s);
            }
            catch (Throwable t) {
                Log.e("AuthCoprocessor", "logging to listener failed", t);
                throw new RuntimeException("failed to " + s);
            }
        }
    }
    
    private void sendCrashReport() {
        if (this.crashReportSent) {
            Log.e("AuthCoprocessor", "skipping duplicate crash report");
        }
        else if (this.context == null) {
            Log.e("AuthCoprocessor", "cannot sent crash report, null context");
        }
        else {
            try {
                final Intent intent = new Intent(this.context, (Class)Class.forName("com.navdy.hud.app.util.CrashReportService"));
                intent.setAction("DumpCrashReport");
                intent.putExtra("EXTRA_CRASH_TYPE", "MFI_ERROR");
                this.context.startService(intent);
                this.crashReportSent = true;
            }
            catch (Exception ex) {
                Log.e("AuthCoprocessor", "error in lookup of CrashReportService class", (Throwable)ex);
            }
        }
    }
    
    private void writePages(int i, int n, final byte[] array, final String s) throws IOException {
        this.i2c.writeShort(17, i, array.length);
        this.checkStatus("write " + s + " length");
        i = array.length;
        int n2 = 0;
        final byte[] array2 = new byte[128];
        while (i > 0) {
            final int min = Math.min(i, 128);
            System.arraycopy(array, n2, array2, 0, min);
            this.i2c.write(17, n, array2, 0, array2.length);
            this.checkStatus("write " + s);
            ++n;
            n2 += min;
            i -= min;
        }
    }
    
    public void close() {
        this.i2c.close();
    }
    
    public byte[] createAccessorySignature(byte[] array) {
        Label_0033: {
            try {
                if (array.length != 20) {
                    throw new RuntimeException("wrong challenge size");
                }
                break Label_0033;
            }
            catch (Exception ex) {
                Log.e("AuthCoprocessor", "", (Throwable)ex);
                array = null;
            }
            return array;
        }
        this.writePages(32, 33, array, "accessory signature");
        this.i2c.writeShort(17, 17, 128);
        this.i2c.writeByte(17, 16, 1);
        this.checkStatus("generate accessory signature");
        array = new byte[this.i2c.readShort(17, 17)];
        this.i2c.read(17, 18, array);
        this.checkStatus("read accessory signature");
        return array;
    }
    
    public void open() throws IOException {
        this.i2c.open();
    }
    
    public byte[] readAccessoryCertificate() throws IOException {
        final byte[] array = new byte[this.i2c.readShort(17, 48)];
        this.i2c.read(17, 49, array);
        this.checkStatus("read accessory certificate");
        return array;
    }
    
    public byte[] validateDeviceCertificateAndGenerateDeviceChallenge(byte[] array) {
        try {
            this.writePages(80, 81, array, "device certificate");
            this.i2c.writeByte(17, 16, 4);
            this.checkStatus("validate device certificate");
            this.i2c.writeByte(17, 16, 2);
            this.checkStatus("generate device challenge");
            array = new byte[this.i2c.readShort(17, 32)];
            this.i2c.read(17, 33, array);
            this.checkStatus("read device challenge");
            return array;
        }
        catch (Exception ex) {
            Log.e("AuthCoprocessor", "", (Throwable)ex);
            array = null;
            return array;
        }
    }
    
    public boolean validateDeviceSignature(final byte[] array) {
        try {
            this.writePages(17, 18, array, "device signature");
            this.i2c.writeByte(17, 16, 3);
            this.checkStatus("validate device signature");
            return true;
        }
        catch (Exception ex) {
            Log.e("AuthCoprocessor", "", (Throwable)ex);
            return false;
        }
    }
}
