package com.navdy.hud.mfi;

import java.io.IOException;
import java.io.OutputStream;
import java.io.DataOutputStream;
import java.io.ByteArrayOutputStream;

public class ByteArrayBuilder
{
    ByteArrayOutputStream bos;
    DataOutputStream dos;
    
    public ByteArrayBuilder() {
        this.bos = new ByteArrayOutputStream();
        this.dos = new DataOutputStream(this.bos);
    }
    
    public ByteArrayBuilder(final int n) {
        this.bos = new ByteArrayOutputStream(n);
        this.dos = new DataOutputStream(this.bos);
    }
    
    public ByteArrayBuilder addBlob(final byte[] array) throws IOException {
        this.dos.write(array);
        return this;
    }
    
    public ByteArrayBuilder addBlob(final byte[] array, final int n, final int n2) throws IOException {
        this.dos.write(array, n, n2);
        return this;
    }
    
    public ByteArrayBuilder addInt16(final int n) throws IOException {
        this.dos.writeShort(n);
        return this;
    }
    
    public ByteArrayBuilder addInt32(final int n) throws IOException {
        this.dos.writeInt(n);
        return this;
    }
    
    public ByteArrayBuilder addInt64(final long n) throws IOException {
        this.dos.writeLong(n);
        return this;
    }
    
    public ByteArrayBuilder addInt8(final int n) throws IOException {
        this.dos.writeByte(n);
        return this;
    }
    
    public ByteArrayBuilder addUTF8(final String s) throws IOException {
        this.dos.write(s.getBytes("UTF-8"));
        return this;
    }
    
    public byte[] build() {
        return this.bos.toByteArray();
    }
    
    public int size() {
        return this.bos.size();
    }
}
