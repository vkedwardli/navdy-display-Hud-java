package com.navdy.hud.mfi;

import java.io.OutputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import android.os.Build;
import android.os.Build;
import java.util.Arrays;
import java.io.IOException;
import android.util.Log;
import android.os.Message;
import android.os.Looper;
import android.content.Context;
import android.os.Handler;
import java.util.HashMap;
import android.util.SparseArray;

public class iAPProcessor implements SessionPacketReceiver, EASessionPacketReceiver
{
    public static final String CHARSET_NAME = "UTF-8";
    public static final int DEVICE_AUTHENTICATION_CERTIFICATE_WAIT_TIME = 3000;
    public static final int DEVICE_AUTHENTICATION_RETRY_INTERVAL = 5000;
    private static final int EASESSION_SESSION = 7;
    public static final int HEADER_BUFFER = 100;
    public static final String HID_COMPONENT_NAME = "navdy-media-remote";
    public static final int MAX_DEVICE_AUTHENTICATION_RETRIES = 5;
    private static final int MESSAGE_LINK_LOST = 2;
    private static final int MESSAGE_RETRY_DEVICE_AUTHENTICATION = 3;
    private static final int MESSAGE_SESSION_MESSAGE = 1;
    private static final int MESSAGE_SKIP_DEVICE_AUTHENTICATION = 4;
    public static final String NAVDY_CLIENT_BUNDLE_ID = "com.navdy.NavdyClient";
    public static final int PRODUCT_IDENTIFIER = 1;
    public static final String PROTOCOL_V1 = "com.navdy.hud.api.v1";
    public static final String PROXY_V1 = "com.navdy.hud.proxy.v1";
    public static final int STARTING_OFFSET = 6;
    private static final String TAG;
    public static final int VENDOR_IDENTIFIER = 39046;
    static final SparseArray<iAPMessage> iAPMsgById;
    static final boolean isCommunicationUpdateSupported = false;
    static final boolean isCommunicationsSupported = true;
    static final boolean isHIDKeyboardSupported = false;
    static final boolean isHIDMediaRemoteSupported = true;
    private static final boolean isHIDSupported = true;
    static final boolean isMusicSupported = true;
    public static final MediaRemoteComponent mediaRemoteComponent;
    private static final byte[] msgStart;
    private static String[] protocols;
    iAPMessage __someiAPMessageToForceEnumLoading__;
    String accessoryName;
    private AuthCoprocessor authCoprocessor;
    private HashMap<iAPMessage, IAPControlMessageProcessor> controlProcessors;
    private int controlSession;
    private int deviceAuthenticationRetries;
    private SparseArray<EASession> eaSessionById;
    int[] hidDescriptorKeyboard;
    int[] hidDescriptorMediaRemote;
    private IAPListener iapListener;
    private boolean isDeviceAuthenticationSupported;
    private boolean linkEstablished;
    private LinkLayer linkLayer;
    private IIAPFileTransferManager mFileTransferManager;
    private final Handler myHandler;
    private SparseArray<SessionMessage> reassambleMsgs;
    private StateListener stateListener;
    
    static {
        TAG = iAPProcessor.class.getSimpleName();
        iAPProcessor.protocols = new String[] { "com.navdy.hud.api.v1", "com.navdy.hud.proxy.v1" };
        mediaRemoteComponent = MediaRemoteComponent.HIDMediaRemote;
        iAPMsgById = new SparseArray();
        msgStart = new byte[] { 64, 64 };
    }
    
    public iAPProcessor(final IAPListener iapListener) {
        this(iapListener, null);
    }
    
    public iAPProcessor(final IAPListener iapListener, final Context context) {
        this.eaSessionById = (SparseArray<EASession>)new SparseArray();
        this.deviceAuthenticationRetries = 0;
        this.controlSession = -1;
        this.isDeviceAuthenticationSupported = true;
        this.hidDescriptorMediaRemote = new int[] { 5, 12, 9, 1, 161, 1, 21, 0, 37, 1, 9, 207, 9, 205, 9, 181, 9, 182, 117, 1, 149, 4, 129, 2, 117, 4, 149, 1, 129, 3, 192 };
        this.hidDescriptorKeyboard = new int[] { 5, 12, 9, 1, 161, 1, 5, 7, 21, 0, 37, 1, 117, 8, 149, 1, 129, 3, 5, 12, 21, 0, 37, 1, 9, 64, 9, 205, 9, 181, 9, 182, 117, 1, 149, 4, 129, 2, 117, 4, 149, 1, 129, 3, 192 };
        this.accessoryName = "Navdy HUD";
        this.myHandler = new Handler(Looper.getMainLooper()) {
            public void handleMessage(final Message message) {
                switch (message.what) {
                    case 1:
                        iAPProcessor.this.handleSessionPacket((SessionPacket)message.obj);
                        break;
                    case 2:
                        iAPProcessor.this.handleLinkLoss();
                        break;
                    case 3:
                        iAPProcessor.this.retryDeviceAuthentication();
                        break;
                    case 4:
                        if (iAPProcessor.this.stateListener != null) {
                            Log.d("MFi", "Skipping Device Authentication as we did not get the certificate");
                            iAPProcessor.this.stateListener.onReady();
                            break;
                        }
                        break;
                }
            }
        };
        this.__someiAPMessageToForceEnumLoading__ = iAPMessage.RequestAuthenticationRequest;
        this.reassambleMsgs = (SparseArray<SessionMessage>)new SparseArray();
        this.iapListener = iapListener;
        this.authCoprocessor = new AuthCoprocessor(iapListener, context);
        this.controlProcessors = new HashMap<iAPMessage, IAPControlMessageProcessor>();
    }
    
    static void copy(final byte[] array, final int n, final byte[] array2) {
        System.arraycopy(array2, 0, array, n, array2.length);
    }
    
    private void handleLinkLoss() {
        this.myHandler.removeMessages(3);
        this.myHandler.removeMessages(4);
        this.linkEstablished = false;
        this.deviceAuthenticationRetries = 0;
        if (this.mFileTransferManager != null) {
            this.mFileTransferManager.clear();
        }
        for (int i = 0; i < this.eaSessionById.size(); ++i) {
            this.stopEASession((EASession)this.eaSessionById.valueAt(i));
        }
        this.eaSessionById.clear();
    }
    
    private void handleSessionMessage(int controlSession, int uInt16, byte[] array) throws IOException {
        final iAPMessage iapMessage = (iAPMessage)iAPProcessor.iAPMsgById.get(uInt16);
        Label_0042: {
            if (iapMessage == null) {
                Log.e(iAPProcessor.TAG, String.format("received unknown message (%d bytes)", array.length));
            }
            else {
                Log.d("MFi", "received message " + iapMessage.toString() + " (" + array.length + " bytes) ");
                switch (iapMessage) {
                    case StopExternalAccessoryProtocolSession:
                        controlSession = parse(array).getUInt16(0);
                        this.stopEASession((EASession)this.eaSessionById.get(controlSession));
                        this.eaSessionById.remove(controlSession);
                        break;
                    case StartExternalAccessoryProtocolSession: {
                        final IAP2Params parse = parse(array);
                        controlSession = parse.getUInt8(0);
                        uInt16 = parse.getUInt16(1);
                        Log.d("MFi", "StartExternalAccessoryProtocolSession: " + uInt16 + " (protoId: " + controlSession + ")");
                        final EASession eaSession = new EASession(this, this.linkLayer.getRemoteAddress(), this.linkLayer.getRemoteName(), uInt16, iAPProcessor.protocols[controlSession]);
                        this.eaSessionById.append(uInt16, eaSession);
                        this.startEASession(eaSession);
                        break;
                    }
                    case RequestAuthenticationChallengeResponse:
                        array = parse(array).getBlob(0);
                        this.authCoprocessor.open();
                        try {
                            this.sendToDevice(controlSession, ((IAP2ParamsCreator)new IAP2SessionMessage(iAPMessage.AuthenticationResponse)).addBlob(0, this.authCoprocessor.createAccessorySignature(array)));
                            break;
                        }
                        finally {
                            this.authCoprocessor.close();
                        }
                    case StartIdentification:
                        this.isDeviceAuthenticationSupported = true;
                        this.sendIdentification(controlSession);
                    case AuthenticationSucceeded:
                        break;
                    default: {
                        final IAPControlMessageProcessor iapControlMessageProcessor = this.controlProcessors.get(iapMessage);
                        if (iapControlMessageProcessor != null) {
                            iapControlMessageProcessor.processControlMessage(iapMessage, controlSession, array);
                            break;
                        }
                        break;
                    }
                    case RequestAuthenticationRequest:
                        this.controlSession = controlSession;
                        this.deviceAuthenticationRetries = 0;
                        this.authCoprocessor.open();
                        try {
                            this.sendToDevice(controlSession, ((IAP2ParamsCreator)new IAP2SessionMessage(iAPMessage.AuthenticationCertificate)).addBlob(0, this.authCoprocessor.readAccessoryCertificate()));
                            break;
                        }
                        finally {
                            this.authCoprocessor.close();
                        }
                    case IdentificationAccepted:
                        this.deviceAuthenticationRetries = 0;
                        if (this.isDeviceAuthenticationSupported) {
                            Log.d("MFi", "DeviceAuthentication supported, attempting");
                            this.sendToDevice(controlSession, (IAP2ParamsCreator)new IAP2SessionMessage(iAPMessage.RequestDeviceAuthenticationCertificate));
                            this.myHandler.sendEmptyMessageDelayed(4, 3000L);
                            break;
                        }
                        if (this.stateListener != null) {
                            this.stateListener.onReady();
                            break;
                        }
                        break;
                    case IdentificationRejected:
                        Log.d(iAPProcessor.TAG, String.format("msg data: %s", Utils.bytesToHex(array, true)));
                        if (this.isDeviceAuthenticationSupported) {
                            Log.d("MFi", "Identification rejected, trying without DeviceAuthentication");
                            this.isDeviceAuthenticationSupported = false;
                            this.sendIdentification(controlSession);
                            break;
                        }
                        Log.d("MFi", "Identification rejected, even with DeviceAuthentication turned off");
                        if (this.stateListener != null) {
                            this.stateListener.onError();
                            break;
                        }
                        break;
                    case DeviceAuthenticationCertificate:
                        this.myHandler.removeMessages(4);
                        array = parse(array).getBlob(0);
                        this.authCoprocessor.open();
                        try {
                            this.sendToDevice(controlSession, ((IAP2ParamsCreator)new IAP2SessionMessage(iAPMessage.RequestDeviceAuthenticationChallengeResponse)).addBlob(0, this.authCoprocessor.validateDeviceCertificateAndGenerateDeviceChallenge(array)));
                            break;
                        }
                        finally {
                            this.authCoprocessor.close();
                        }
                    case DeviceAuthenticationResponse:
                        while (true) {
                            array = parse(array).getBlob(0);
                            this.authCoprocessor.open();
                            while (true) {
                                Label_0771: {
                                    try {
                                        int validateDeviceSignature;
                                        if (array.length > 128) {
                                            Log.w("MFi", "DeviceAuthenticationResponse response too big - working around iOS 10 bug");
                                            validateDeviceSignature = 1;
                                        }
                                        else {
                                            validateDeviceSignature = (this.authCoprocessor.validateDeviceSignature(array) ? 1 : 0);
                                        }
                                        if (validateDeviceSignature == 0) {
                                            Log.e("MFi", "DeviceAuthenticationResponse validation failed");
                                        }
                                        if (this.deviceAuthenticationRetries >= 5) {
                                            Log.d("MFi", "DeviceAuthenticationRetries reached the limit so sending success");
                                            validateDeviceSignature = 1;
                                        }
                                        iAPMessage iapMessage2;
                                        if (validateDeviceSignature != 0) {
                                            iapMessage2 = iAPMessage.DeviceAuthenticationSucceeded;
                                        }
                                        else {
                                            iapMessage2 = iAPMessage.DeviceAuthenticationFailed;
                                        }
                                        this.sendToDevice(controlSession, (IAP2ParamsCreator)new IAP2SessionMessage(iapMessage2));
                                        if (validateDeviceSignature == 0) {
                                            this.myHandler.sendEmptyMessageDelayed(3, 5000L);
                                        }
                                        else if (this.iapListener != null) {
                                            this.iapListener.onDeviceAuthenticationSuccess(this.deviceAuthenticationRetries);
                                        }
                                        if (this.stateListener != null) {
                                            if (validateDeviceSignature == 0) {
                                                break Label_0771;
                                            }
                                            this.stateListener.onReady();
                                        }
                                        break Label_0042;
                                    }
                                    finally {
                                        this.authCoprocessor.close();
                                    }
                                }
                                this.stateListener.onError();
                                continue;
                            }
                        }
                        break;
                }
            }
        }
    }
    
    private void handleSessionPacket(final SessionPacket sessionPacket) {
        while (true) {
            SessionMessage sessionMessage2 = null;
        Label_0323:
            while (true) {
                SessionMessage sessionMessage = null;
                Label_0220: {
                    try {
                        this.linkEstablished = true;
                        if (sessionPacket.session == 2) {
                            if (this.mFileTransferManager != null) {
                                this.mFileTransferManager.queue(sessionPacket.data);
                            }
                        }
                        else {
                            sessionMessage = (SessionMessage)this.reassambleMsgs.get(sessionPacket.session);
                            if ((sessionMessage2 = sessionMessage) == null) {
                                if (!this.startsWith(sessionPacket.data, iAPProcessor.msgStart)) {
                                    break Label_0220;
                                }
                                sessionMessage2 = new SessionMessage(Utils.unpackUInt16(sessionPacket.data, 2), Utils.unpackUInt16(sessionPacket.data, 4));
                                this.reassambleMsgs.put(sessionPacket.session, sessionMessage2);
                            }
                            if (sessionMessage2 != null) {
                                sessionMessage2.append(sessionPacket.data);
                                if (sessionMessage2.builder.size() >= sessionMessage2.msgLen) {
                                    if (sessionMessage2.builder.size() != sessionMessage2.msgLen) {
                                        this.reassambleMsgs.remove(sessionPacket.session);
                                        throw new RuntimeException("bad message size");
                                    }
                                    break Label_0323;
                                }
                            }
                        }
                        return;
                    }
                    catch (Exception ex) {
                        Log.e(iAPProcessor.TAG, "", (Throwable)ex);
                        this.reassambleMsgs.remove(sessionPacket.session);
                        if (this.stateListener != null) {
                            this.stateListener.onError();
                        }
                        return;
                    }
                }
                final int unpackUInt16 = Utils.unpackUInt16(sessionPacket.data, 0);
                Log.d("MFi", "Apple Device(E):" + unpackUInt16 + " Size:" + sessionPacket.data.length);
                sessionMessage2 = sessionMessage;
                if (this.eaSessionById.get(unpackUInt16) != null) {
                    ((EASession)this.eaSessionById.get(unpackUInt16)).queue(Arrays.copyOfRange(sessionPacket.data, 2, sessionPacket.data.length));
                    sessionMessage2 = sessionMessage;
                    continue;
                }
                continue;
            }
            this.handleSessionMessage(sessionPacket.session, sessionMessage2.msgId, sessionMessage2.builder.build());
            this.reassambleMsgs.remove(sessionPacket.session);
        }
    }
    
    public static IAP2Params parse(final byte[] array) {
        if (array == null) {
            throw new IllegalArgumentException();
        }
        return new IAP2Params(array, 6);
    }
    
    private void retryDeviceAuthentication() {
        if (this.linkEstablished) {
            ++this.deviceAuthenticationRetries;
            this.sendToDevice(this.controlSession, (IAP2ParamsCreator)new IAP2SessionMessage(iAPMessage.RequestDeviceAuthenticationCertificate));
        }
    }
    
    private void sendHIDReport(final int n) {
        final int ordinal = iAPProcessor.mediaRemoteComponent.ordinal();
        int[] array = null;
        switch (iAPProcessor.mediaRemoteComponent) {
            case HIDKeyboard:
                array = new int[] { 0, n };
                break;
            case HIDMediaRemote:
                array = new int[] { n };
                break;
        }
        this.sendToDevice(this.controlSession, ((IAP2ParamsCreator)new IAP2SessionMessage(iAPMessage.AccessoryHIDReport)).addUInt16(AccessoryHIDReport.HIDComponentIdentifier, ordinal).addBlob(AccessoryHIDReport.HIDReport, Utils.intsToBytes(array)));
    }
    
    private void sendIdentification(final int n) throws IOException {
        final IAP2SessionMessage iap2SessionMessage = new IAP2SessionMessage(iAPMessage.IdentificationInformation);
        ((IAP2ParamsCreator)iap2SessionMessage).addString(IdentificationInformation.Name, this.accessoryName).addString(IdentificationInformation.ModelIdentifier, Build.MODEL).addString(IdentificationInformation.Manufacturer, Build.MANUFACTURER).addString(IdentificationInformation.SerialNumber, Build.SERIAL).addString(IdentificationInformation.FirmwareVersion, Build$VERSION.RELEASE).addString(IdentificationInformation.HardwareVersion, Build.HARDWARE);
        final ByteArrayBuilder byteArrayBuilder = new ByteArrayBuilder();
        if (this.isDeviceAuthenticationSupported) {
            byteArrayBuilder.addInt16(iAPMessage.RequestDeviceAuthenticationCertificate.id).addInt16(iAPMessage.RequestDeviceAuthenticationChallengeResponse.id).addInt16(iAPMessage.DeviceAuthenticationSucceeded.id).addInt16(iAPMessage.DeviceAuthenticationFailed.id).build();
        }
        byteArrayBuilder.addInt16(iAPMessage.StartHID.id).addInt16(iAPMessage.StopHID.id).addInt16(iAPMessage.AccessoryHIDReport.id);
        byteArrayBuilder.addInt16(iAPMessage.StartCallStateUpdates.id).addInt16(iAPMessage.StopCallStateUpdates.id).addInt16(iAPMessage.InitiateCall.id).addInt16(iAPMessage.AcceptCall.id).addInt16(iAPMessage.EndCall.id).addInt16(iAPMessage.MuteStatusUpdate.id);
        byteArrayBuilder.addInt16(iAPMessage.StartNowPlayingUpdates.id).addInt16(iAPMessage.StopNowPlayingUpdates.id);
        byteArrayBuilder.addInt16(iAPMessage.RequestAppLaunch.id);
        final ByteArrayBuilder byteArrayBuilder2 = new ByteArrayBuilder();
        byteArrayBuilder2.addInt16(iAPMessage.StartExternalAccessoryProtocolSession.id).addInt16(iAPMessage.StopExternalAccessoryProtocolSession.id);
        byteArrayBuilder2.addInt16(iAPMessage.DeviceHIDReport.id);
        byteArrayBuilder2.addInt16(iAPMessage.CallStateUpdate.id);
        byteArrayBuilder2.addInt16(iAPMessage.NowPlayingUpdate.id);
        if (this.isDeviceAuthenticationSupported) {
            byteArrayBuilder2.addInt16(iAPMessage.DeviceAuthenticationCertificate.id).addInt16(iAPMessage.DeviceAuthenticationResponse.id);
        }
        ((IAP2ParamsCreator)iap2SessionMessage).addBlob(IdentificationInformation.MessagesSentByAccessory, byteArrayBuilder.build()).addBlob(IdentificationInformation.MessagesReceivedFromDevice, byteArrayBuilder2.build()).addEnum(IdentificationInformation.PowerProvidingCapability, 0).addUInt16(IdentificationInformation.MaximumCurrentDrawnFromDevice, 0).addBlob(IdentificationInformation.SupportedExternalAccessoryProtocol, new IAP2ParamsCreator().addUInt8(ExternalAccessoryProtocol.ExternalAccessoryProtocolIdentifier, 0).addString(ExternalAccessoryProtocol.ExternalAccessoryProtocolName, iAPProcessor.protocols[0]).addEnum(ExternalAccessoryProtocol.ExternalAccessoryProtocolMatchAction, 1).toBytes()).addBlob(IdentificationInformation.SupportedExternalAccessoryProtocol, new IAP2ParamsCreator().addUInt8(ExternalAccessoryProtocol.ExternalAccessoryProtocolIdentifier, 1).addString(ExternalAccessoryProtocol.ExternalAccessoryProtocolName, iAPProcessor.protocols[1]).addEnum(ExternalAccessoryProtocol.ExternalAccessoryProtocolMatchAction, 1).toBytes()).addString(IdentificationInformation.CurrentLanguage, "en").addString(IdentificationInformation.SupportedLanguage, "en").addBlob(IdentificationInformation.BluetoothTransportComponent, new IAP2ParamsCreator().addUInt16(BluetoothTransportComponent.TransportComponentIdentifier, 1234).addString(BluetoothTransportComponent.TransportComponentName, "navdy-transport").addNone(BluetoothTransportComponent.TransportSupportsiAP2Connection).addBlob(BluetoothTransportComponent.BluetoothTransportMediaAccessControlAddress, this.linkLayer.getLocalAddress()).toBytes());
        ((IAP2ParamsCreator)iap2SessionMessage).addBlob(IdentificationInformation.iAP2HIDComponent, new IAP2ParamsCreator().addUInt16(iAP2HIDComponent.HIDComponentIdentifier, MediaRemoteComponent.HIDMediaRemote.ordinal()).addString(iAP2HIDComponent.HIDComponentName, "navdy-media-remote").addEnum(iAP2HIDComponent.HIDComponentFunction, HIDComponentFunction.MediaPlaybackRemote).toBytes());
        this.sendToDevice(n, (IAP2ParamsCreator)iap2SessionMessage);
    }
    
    private void startEASession(final EASession eaSession) {
        Log.d(iAPProcessor.TAG, "startEASession: session: " + eaSession.getSessionIdentifier());
        if (this.stateListener != null) {
            this.stateListener.onSessionStart(eaSession);
        }
    }
    
    private boolean startsWith(final byte[] array, final byte[] array2) {
        final boolean b = false;
        int i = 0;
        while (i < array.length) {
            boolean b2;
            if (i >= array2.length) {
                b2 = true;
            }
            else {
                b2 = b;
                if (array[i] == array2[i]) {
                    ++i;
                    continue;
                }
            }
            return b2;
        }
        return b;
    }
    
    private void stopEASession(final EASession eaSession) {
        Log.d(iAPProcessor.TAG, "stopEASession: session: " + eaSession.getSessionIdentifier());
        if (this.stateListener != null) {
            this.stateListener.onSessionStop(eaSession);
        }
    }
    
    public void connect(final IIAPFileTransferManager mFileTransferManager) {
        this.mFileTransferManager = mFileTransferManager;
    }
    
    public void connect(final LinkLayer linkLayer) {
        this.linkLayer = linkLayer;
    }
    
    public void connect(final StateListener stateListener) {
        this.stateListener = stateListener;
    }
    
    public int getControlSessionId() {
        return this.controlSession;
    }
    
    public void launchApp(final String s, final boolean b) {
        final IAP2SessionMessage iap2SessionMessage = new IAP2SessionMessage(iAPMessage.RequestAppLaunch.id);
        ((IAP2ParamsCreator)iap2SessionMessage).addString(RequestAppLaunch.AppBundleID, s);
        final RequestAppLaunch launchAlert = RequestAppLaunch.LaunchAlert;
        AppLaunchMethod appLaunchMethod;
        if (b) {
            appLaunchMethod = AppLaunchMethod.LaunchWithUserAlert;
        }
        else {
            appLaunchMethod = AppLaunchMethod.LaunchWithoutAlert;
        }
        ((IAP2ParamsCreator)iap2SessionMessage).addEnum(launchAlert, appLaunchMethod);
        this.sendControlMessage((IAP2ParamsCreator)iap2SessionMessage);
    }
    
    public void launchApp(final boolean b) {
        this.launchApp("com.navdy.NavdyClient", b);
    }
    
    public void linkLost() {
        this.myHandler.sendEmptyMessage(2);
    }
    
    public void onKeyDown(final int n) {
        this.sendHIDReport(1 << n);
    }
    
    public void onKeyDown(final Enum enum1) {
        this.onKeyDown(enum1.ordinal());
    }
    
    public void onKeyUp(final int n) {
        this.sendHIDReport(0);
    }
    
    public void onKeyUp(final Enum enum1) {
        this.sendHIDReport(0);
    }
    
    @Override
    public void queue(final EASessionPacket eaSessionPacket) {
        final int sessionIdentifier = ((EASession)this.eaSessionById.get(eaSessionPacket.session)).getSessionIdentifier();
        final int n = this.linkLayer.native_get_maximum_payload_size() - 100;
        if (n > 0) {
            Utils.logTransfer(iAPProcessor.TAG, "EA->iAP[" + sessionIdentifier + "/" + 7 + "]", new Object[0]);
            int length = eaSessionPacket.data.length;
            int n2 = 0;
        Label_0196_Outer:
            while (true) {
                final int min = Math.min(length, n);
                final int n3 = length - min;
                while (true) {
                    try {
                        Log.d("MFi", "Accessory(E)   :" + sessionIdentifier + " Size:" + min);
                        this.sendToDevice(7, new ByteArrayBuilder(min + 2).addInt16(sessionIdentifier).addBlob(eaSessionPacket.data, n2, min).build());
                        n2 += min;
                        length = n3;
                        if (n3 <= 0) {
                            break;
                        }
                        continue Label_0196_Outer;
                    }
                    catch (IOException ex) {
                        Log.e(iAPProcessor.TAG, "Error while sending to the device" + ex);
                        continue;
                    }
                    break;
                }
            }
        }
    }
    
    @Override
    public void queue(final SessionPacket sessionPacket) {
        this.myHandler.sendMessage(this.myHandler.obtainMessage(1, sessionPacket));
    }
    
    public void registerControlMessageProcessor(final iAPMessage iapMessage, final IAPControlMessageProcessor iapControlMessageProcessor) {
        if (iapControlMessageProcessor != null) {
            this.controlProcessors.put(iapMessage, iapControlMessageProcessor);
        }
    }
    
    public void sendControlMessage(final IAP2ParamsCreator iap2ParamsCreator) {
        this.sendToDevice(this.controlSession, iap2ParamsCreator);
    }
    
    public void sendToDevice(final int n, final IAP2ParamsCreator iap2ParamsCreator) {
        final byte[] bytes = iap2ParamsCreator.toBytes();
        if (iap2ParamsCreator instanceof IAP2SessionMessage) {
            final iAPMessage iapMessage = (iAPMessage)iAPProcessor.iAPMsgById.get(((IAP2SessionMessage)iap2ParamsCreator).msgId);
            Log.d("MFi", "sending message " + iapMessage.toString());
            Utils.logTransfer(iAPProcessor.TAG, "sending message %s (%d bytes)", iapMessage.toString(), bytes.length);
        }
        this.sendToDevice(n, bytes);
    }
    
    public void sendToDevice(final int n, final byte[] array) {
        this.linkLayer.queue(new SessionPacket(n, array));
    }
    
    public void setAccessoryName(final String accessoryName) {
        this.accessoryName = accessoryName;
    }
    
    public void startHIDSession() {
        int[] array = null;
        final int ordinal = iAPProcessor.mediaRemoteComponent.ordinal();
        switch (iAPProcessor.mediaRemoteComponent) {
            case HIDKeyboard:
                array = this.hidDescriptorKeyboard;
                break;
            case HIDMediaRemote:
                array = this.hidDescriptorMediaRemote;
                break;
        }
        this.sendToDevice(this.controlSession, ((IAP2ParamsCreator)new IAP2SessionMessage(iAPMessage.StartHID)).addUInt16(StartHID.HIDComponentIdentifier, ordinal).addUInt16(StartHID.VendorIdentifier, 39046).addUInt16(StartHID.ProductIdentifier, 1).addBlob(StartHID.HIDReportDescriptor, Utils.intsToBytes(array)));
    }
    
    public void stopHIDSession() {
        this.sendToDevice(this.controlSession, ((IAP2ParamsCreator)new IAP2SessionMessage(iAPMessage.StopHID)).addUInt16(AccessoryHIDReport.HIDComponentIdentifier, iAPProcessor.mediaRemoteComponent.ordinal()));
    }
    
    enum AccessoryHIDReport
    {
        HIDComponentIdentifier, 
        HIDReport;
    }
    
    enum AppLaunchMethod
    {
        LaunchWithUserAlert, 
        LaunchWithoutAlert;
    }
    
    enum BluetoothTransportComponent
    {
        BluetoothTransportMediaAccessControlAddress, 
        TransportComponentIdentifier, 
        TransportComponentName, 
        TransportSupportsiAP2Connection;
    }
    
    enum ExternalAccessoryProtocol
    {
        ExternalAccessoryProtocolIdentifier, 
        ExternalAccessoryProtocolMatchAction, 
        ExternalAccessoryProtocolName, 
        NativeTransportComponentIdentifier;
    }
    
    enum HIDComponentFunction
    {
        AssistiveSwitchControl, 
        AssistiveTouchPointer, 
        ExtendedGamepadFormFitting, 
        ExtendedGamepadNonFormFitting, 
        Headphone, 
        Keyboard, 
        MediaPlaybackRemote, 
        StandardGamepadFormFitting;
    }
    
    static class IAP2Param
    {
        int id;
        int length;
        int offset;
    }
    
    static class IAP2Params
    {
        public static final int DATA_OFFSET = 4;
        public static final int ID_OFFSET = 2;
        byte[] data;
        SparseArray<IAP2Param> params;
        
        IAP2Params(final byte[] data, int i) {
            this.params = (SparseArray<IAP2Param>)new SparseArray();
            this.data = data;
            while (i < data.length) {
                final IAP2Param iap2Param = new IAP2Param();
                iap2Param.offset = i;
                iap2Param.length = Utils.unpackUInt16(data, i);
                iap2Param.id = Utils.unpackUInt16(data, i + 2);
                this.params.put(iap2Param.id, iap2Param);
                i += iap2Param.length;
            }
        }
        
        byte[] getBlob(final int n) {
            final IAP2Param iap2Param = (IAP2Param)this.params.get(n);
            return Arrays.copyOfRange(this.data, iap2Param.offset + 4, iap2Param.offset + iap2Param.length);
        }
        
        byte[] getBlob(final Enum enum1) {
            return this.getBlob(enum1.ordinal());
        }
        
        boolean getBoolean(final int n) {
            return this.getUInt8(n) != 0;
        }
        
        boolean getBoolean(final Enum enum1) {
            return this.getBoolean(enum1.ordinal());
        }
        
        int getEnum(final int n) {
            return this.getUInt8(n);
        }
        
        int getEnum(final Enum enum1) {
            return this.getEnum(enum1.ordinal());
        }
        
         <E> E getEnum(final Class<E> clazz, int enum1) {
            enum1 = this.getEnum(enum1);
            final E[] enumConstants = clazz.getEnumConstants();
            E e;
            if (enum1 >= 0 && enum1 < enumConstants.length) {
                e = enumConstants[enum1];
            }
            else {
                e = null;
            }
            return e;
        }
        
         <E> E getEnum(final Class<E> clazz, final Enum enum1) {
            return this.<E>getEnum(clazz, enum1.ordinal());
        }
        
        int getUInt16(final int n) {
            return Utils.unpackUInt16(this.data, ((IAP2Param)this.params.get(n)).offset + 4);
        }
        
        int getUInt16(final Enum enum1) {
            return this.getUInt16(enum1.ordinal());
        }
        
        long getUInt32(final int n) {
            return Utils.unpackUInt32(this.data, ((IAP2Param)this.params.get(n)).offset + 4);
        }
        
        long getUInt32(final Enum enum1) {
            return this.getUInt32(enum1.ordinal());
        }
        
        BigInteger getUInt64(final int n) {
            return Utils.unpackUInt64(this.data, ((IAP2Param)this.params.get(n)).offset + 4);
        }
        
        int getUInt8(final int n) {
            return Utils.unpackUInt8(this.data, ((IAP2Param)this.params.get(n)).offset + 4);
        }
        
        int getUInt8(final Enum enum1) {
            return this.getUInt8(enum1.ordinal());
        }
        
        String getUTF8(final int n) {
            final byte[] blob = this.getBlob(n);
            try {
                return new String(blob, 0, blob.length - 1, "UTF-8");
            }
            catch (UnsupportedEncodingException ex) {
                return null;
            }
        }
        
        String getUTF8(final Enum enum1) {
            return this.getUTF8(enum1.ordinal());
        }
        
        boolean hasParam(final int n) {
            return this.params.get(n) != null;
        }
        
        boolean hasParam(final Enum enum1) {
            return this.hasParam(enum1.ordinal());
        }
    }
    
    static class IAP2ParamsCreator
    {
        private static final int META_DATA_LENGTH = 4;
        public static final int SIZE_OF_LENGTH = 2;
        public static final int SIZE_OF_PARAM_ID = 2;
        DataOutputStream dos;
        ByteArrayOutputStream params;
        
        public IAP2ParamsCreator() {
            this.params = new ByteArrayOutputStream();
            this.dos = new DataOutputStream(this.params);
        }
        
        IAP2ParamsCreator addBlob(final int n, final byte[] array) {
            try {
                this.dos.writeShort(array.length + 4);
                this.dos.writeShort(n);
                this.dos.write(array);
                return this;
            }
            catch (IOException ex) {
                Log.e(iAPProcessor.TAG, "Exception while creating message ", (Throwable)ex);
                return this;
            }
        }
        
        IAP2ParamsCreator addBlob(final Enum enum1, final byte[] array) {
            return this.addBlob(enum1.ordinal(), array);
        }
        
        IAP2ParamsCreator addBoolean(final int n, final boolean b) {
            int n2;
            if (b) {
                n2 = 1;
            }
            else {
                n2 = 0;
            }
            return this.addUInt8(n, n2);
        }
        
        IAP2ParamsCreator addBoolean(final Enum enum1, final boolean b) {
            final int ordinal = enum1.ordinal();
            int n;
            if (b) {
                n = 1;
            }
            else {
                n = 0;
            }
            return this.addUInt8(ordinal, n);
        }
        
        IAP2ParamsCreator addEnum(final int n, final int n2) {
            return this.addUInt8(n, n2);
        }
        
        IAP2ParamsCreator addEnum(final Enum enum1, final int n) {
            return this.addEnum(enum1.ordinal(), n);
        }
        
        IAP2ParamsCreator addEnum(final Enum enum1, final Enum enum2) {
            return this.addEnum(enum1.ordinal(), enum2.ordinal());
        }
        
        IAP2ParamsCreator addNone(final int n) {
            try {
                this.dos.writeShort(4);
                this.dos.writeShort(n);
                return this;
            }
            catch (IOException ex) {
                Log.e(iAPProcessor.TAG, "Exception while creating message ", (Throwable)ex);
                return this;
            }
        }
        
        IAP2ParamsCreator addNone(final Enum enum1) {
            return this.addNone(enum1.ordinal());
        }
        
        IAP2ParamsCreator addString(final int n, final String s) {
            try {
                final byte[] bytes = s.getBytes("UTF-8");
                this.dos.writeShort(bytes.length + 4 + 1);
                this.dos.writeShort(n);
                this.dos.write(bytes);
                this.dos.write(new byte[] { 0 });
                return this;
            }
            catch (IOException ex) {
                Log.e(iAPProcessor.TAG, "Exception while creating message ", (Throwable)ex);
                return this;
            }
        }
        
        IAP2ParamsCreator addString(final Enum enum1, final String s) {
            return this.addString(enum1.ordinal(), s);
        }
        
        IAP2ParamsCreator addUInt16(final int n, final int n2) {
            try {
                this.dos.writeShort(6);
                this.dos.writeShort(n);
                this.dos.writeShort(n2);
                return this;
            }
            catch (IOException ex) {
                Log.e(iAPProcessor.TAG, "Exception while creating message ", (Throwable)ex);
                return this;
            }
        }
        
        IAP2ParamsCreator addUInt16(final Enum enum1, final int n) {
            return this.addUInt16(enum1.ordinal(), n);
        }
        
        IAP2ParamsCreator addUInt8(final int n, final int n2) {
            try {
                this.dos.writeShort(5);
                this.dos.writeShort(n);
                this.dos.writeByte(n2);
                return this;
            }
            catch (IOException ex) {
                Log.e(iAPProcessor.TAG, "Exception while creating message ", (Throwable)ex);
                return this;
            }
        }
        
        IAP2ParamsCreator addUInt8(final Enum enum1, final int n) {
            return this.addUInt8(enum1.ordinal(), n);
        }
        
        byte[] toBytes() {
            return this.params.toByteArray();
        }
    }
    
    static class IAP2SessionMessage extends IAP2ParamsCreator
    {
        int msgId;
        
        IAP2SessionMessage(final int msgId) {
            this.msgId = msgId;
            try {
                this.dos.write(iAPProcessor.msgStart);
                this.dos.writeShort(0);
                this.dos.writeShort(msgId);
            }
            catch (IOException ex) {
                Log.e(iAPProcessor.TAG, "Exception while creating message ", (Throwable)ex);
            }
        }
        
        IAP2SessionMessage(final iAPMessage iapMessage) {
            this(iapMessage.id);
        }
        
        @Override
        byte[] toBytes() {
            final byte[] bytes = super.toBytes();
            iAPProcessor.copy(bytes, 2, Utils.packUInt16(bytes.length));
            return bytes;
        }
    }
    
    enum IdentificationInformation
    {
        AppMatchTeamID, 
        BluetoothTransportComponent, 
        CurrentLanguage, 
        FirmwareVersion, 
        HardwareVersion, 
        LocationInformationComponent, 
        Manufacturer, 
        MaximumCurrentDrawnFromDevice, 
        MessagesReceivedFromDevice, 
        MessagesSentByAccessory, 
        ModelIdentifier, 
        Name, 
        PowerProvidingCapability, 
        SerialNumber, 
        SerialTransportComponent, 
        SupportedExternalAccessoryProtocol, 
        SupportedLanguage, 
        USBDeviceTransportComponent, 
        USBHostHIDComponent, 
        USBHostTransportComponent, 
        VehicleInformationComponent, 
        VehicleStatusComponent, 
        iAP2HIDComponent;
    }
    
    public enum MediaRemoteComponent
    {
        HIDKeyboard, 
        HIDMediaRemote;
    }
    
    enum RequestAppLaunch
    {
        AppBundleID, 
        LaunchAlert;
    }
    
    class SessionMessage
    {
        ByteArrayBuilder builder;
        int msgId;
        int msgLen;
        
        SessionMessage(final int msgLen, final int msgId) {
            this.msgLen = msgLen;
            this.msgId = msgId;
            this.builder = new ByteArrayBuilder(msgLen);
        }
        
        void append(final byte[] array) {
            try {
                this.builder.addBlob(array);
            }
            catch (IOException ex) {
                Log.e(iAPProcessor.TAG, "Error appending messages ");
            }
        }
    }
    
    enum StartHID
    {
        HIDComponentIdentifier, 
        HIDReportDescriptor, 
        LocalizedKeyboardCountryCode, 
        ProductIdentifier, 
        VendorIdentifier;
    }
    
    public interface StateListener
    {
        void onError();
        
        void onReady();
        
        void onSessionStart(final EASession p0);
        
        void onSessionStop(final EASession p0);
    }
    
    enum StopHID
    {
        HIDComponentIdentifier;
    }
    
    enum iAP2HIDComponent
    {
        HIDComponentFunction, 
        HIDComponentIdentifier, 
        HIDComponentName;
    }
    
    enum iAPMessage
    {
        AcceptCall(16731), 
        AccessoryHIDReport(26626), 
        AuthenticationCertificate(43521), 
        AuthenticationFailed(43524), 
        AuthenticationResponse(43523), 
        AuthenticationSucceeded(43525), 
        CallStateUpdate(16725), 
        CancelIdentification(7428), 
        CommunicationUpdate(16728), 
        DeviceAuthenticationCertificate(43537), 
        DeviceAuthenticationFailed(43540), 
        DeviceAuthenticationResponse(43539), 
        DeviceAuthenticationSucceeded(43541), 
        DeviceHIDReport(26625), 
        EndCall(16732), 
        HoldStatusUpdate(16735), 
        IdentificationAccepted(7426), 
        IdentificationInformation(7425), 
        IdentificationInformationUpdate(7429), 
        IdentificationRejected(7427), 
        InitiateCall(16730), 
        ListUpdate(16753), 
        MuteStatusUpdate(16736), 
        NowPlayingUpdate(20481), 
        RequestAppLaunch(59906), 
        RequestAuthenticationChallengeResponse(43522), 
        RequestAuthenticationRequest(43520), 
        RequestDeviceAuthenticationCertificate(43536), 
        RequestDeviceAuthenticationChallengeResponse(43538), 
        StartCallStateUpdates(16724), 
        StartCommunicationUpdates(16727), 
        StartExternalAccessoryProtocolSession(59904), 
        StartHID(26624), 
        StartIdentification(7424), 
        StartListUpdates(16752), 
        StartNowPlayingUpdates(20480), 
        StatusExternalAccessoryProtocolSession(59906), 
        StopCallStateUpdates(16726), 
        StopCommunicationUpdates(16729), 
        StopExternalAccessoryProtocolSession(59905), 
        StopHID(26627), 
        StopListUpdate(16754), 
        StopNowPlayingUpdates(20482);
        
        int id;
        
        private iAPMessage(final int id) {
            this.id = id;
            iAPProcessor.iAPMsgById.put(id, this);
        }
    }
}
