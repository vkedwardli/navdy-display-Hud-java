package com.navdy.hud.mfi;

import android.util.Log;
import android.text.TextUtils;

public class IAPCommunicationsManager extends IAPControlMessageProcessor
{
    private static String TAG;
    public static iAPProcessor.iAPMessage[] mMessages;
    private CallStateUpdate.Status callStatus;
    private String callUUid;
    private IAPCommunicationsUpdateListener mUpdatesListener;
    
    static {
        IAPCommunicationsManager.TAG = IAPCommunicationsManager.class.getSimpleName();
        IAPCommunicationsManager.mMessages = new iAPProcessor.iAPMessage[] { iAPProcessor.iAPMessage.CallStateUpdate, iAPProcessor.iAPMessage.CommunicationUpdate };
    }
    
    public IAPCommunicationsManager(final iAPProcessor iapProcessor) {
        super(IAPCommunicationsManager.mMessages, iapProcessor);
    }
    
    public static CallStateUpdate parseCallStateUpdate(final iAPProcessor.IAP2Params iap2Params) {
        final CallStateUpdate callStateUpdate = new CallStateUpdate();
        if (iap2Params.hasParam(StartCallStateUpdates.RemoteID)) {
            callStateUpdate.remoteID = iap2Params.getUTF8(StartCallStateUpdates.RemoteID);
        }
        if (iap2Params.hasParam(StartCallStateUpdates.DisplayName)) {
            callStateUpdate.displayName = iap2Params.getUTF8(StartCallStateUpdates.DisplayName);
        }
        if (iap2Params.hasParam(StartCallStateUpdates.Status)) {
            callStateUpdate.status = iap2Params.<CallStateUpdate.Status>getEnum(CallStateUpdate.Status.class, StartCallStateUpdates.Status);
        }
        if (iap2Params.hasParam(StartCallStateUpdates.Direction)) {
            callStateUpdate.direction = iap2Params.<CallStateUpdate.Direction>getEnum(CallStateUpdate.Direction.class, StartCallStateUpdates.Direction);
        }
        if (iap2Params.hasParam(StartCallStateUpdates.CallUUID)) {
            callStateUpdate.callUUID = iap2Params.getUTF8(StartCallStateUpdates.CallUUID);
        }
        if (iap2Params.hasParam(StartCallStateUpdates.AddressBookID)) {
            callStateUpdate.addressBookID = iap2Params.getUTF8(StartCallStateUpdates.AddressBookID);
        }
        if (iap2Params.hasParam(StartCallStateUpdates.Label)) {
            callStateUpdate.label = iap2Params.getUTF8(StartCallStateUpdates.Label);
        }
        if (iap2Params.hasParam(StartCallStateUpdates.Service)) {
            callStateUpdate.service = iap2Params.<Service>getEnum(Service.class, StartCallStateUpdates.Service);
        }
        if (iap2Params.hasParam(StartCallStateUpdates.IsConferenced)) {
            callStateUpdate.isConferenced = iap2Params.getBoolean(StartCallStateUpdates.IsConferenced);
        }
        if (iap2Params.hasParam(StartCallStateUpdates.ConferenceGroup)) {
            callStateUpdate.conferenceGroup = iap2Params.getUInt8(StartCallStateUpdates.ConferenceGroup);
        }
        if (iap2Params.hasParam(StartCallStateUpdates.DisconnectReason)) {
            callStateUpdate.disconnectReason = iap2Params.<CallStateUpdate.DisconnectReason>getEnum(CallStateUpdate.DisconnectReason.class, StartCallStateUpdates.DisconnectReason);
        }
        return callStateUpdate;
    }
    
    public static CommunicationUpdate parseCommunicationUpdate(final iAPProcessor.IAP2Params iap2Params) {
        final CommunicationUpdate communicationUpdate = new CommunicationUpdate();
        if (iap2Params.hasParam(StartCommunicationUpdates.MuteStatus.id)) {
            communicationUpdate.muteStatus = iap2Params.getBoolean(StartCommunicationUpdates.MuteStatus.id);
        }
        return communicationUpdate;
    }
    
    public void acceptCall() {
        if (!TextUtils.isEmpty((CharSequence)this.callUUid)) {
            final iAPProcessor.IAP2SessionMessage iap2SessionMessage = new iAPProcessor.IAP2SessionMessage(iAPProcessor.iAPMessage.AcceptCall);
            ((iAPProcessor.IAP2ParamsCreator)iap2SessionMessage).addEnum(AcceptCall.AcceptAction, AcceptAction.HoldAndAccept);
            ((iAPProcessor.IAP2ParamsCreator)iap2SessionMessage).addString(AcceptCall.CallUUID, this.callUUid);
            this.miAPProcessor.sendControlMessage((iAPProcessor.IAP2ParamsCreator)iap2SessionMessage);
        }
    }
    
    public void acceptCall(final String s) {
        if (!TextUtils.isEmpty((CharSequence)s)) {
            final iAPProcessor.IAP2SessionMessage iap2SessionMessage = new iAPProcessor.IAP2SessionMessage(iAPProcessor.iAPMessage.AcceptCall);
            ((iAPProcessor.IAP2ParamsCreator)iap2SessionMessage).addEnum(AcceptCall.AcceptAction, AcceptAction.HoldAndAccept);
            ((iAPProcessor.IAP2ParamsCreator)iap2SessionMessage).addString(AcceptCall.CallUUID, s);
            this.miAPProcessor.sendControlMessage((iAPProcessor.IAP2ParamsCreator)iap2SessionMessage);
        }
    }
    
    @Override
    public void bProcessControlMessage(final iAPProcessor.iAPMessage iapMessage, final int n, final byte[] array) {
        final iAPProcessor.IAP2Params parse = iAPProcessor.parse(array);
        Label_0081: {
            if (iapMessage.id != iAPProcessor.iAPMessage.CallStateUpdate.id) {
                break Label_0081;
            }
            final CallStateUpdate callStateUpdate = parseCallStateUpdate(parse);
            this.callStatus = callStateUpdate.status;
            this.callUUid = callStateUpdate.callUUID;
            try {
                this.mUpdatesListener.onCallStateUpdate(callStateUpdate);
                return;
            }
            catch (Throwable t) {
                Log.d(IAPCommunicationsManager.TAG, "Bad call state update listener " + t);
                return;
            }
        }
        if (iapMessage.id == iAPProcessor.iAPMessage.CommunicationUpdate.id) {
            final CommunicationUpdate communicationUpdate = parseCommunicationUpdate(parse);
            try {
                this.mUpdatesListener.onCommunicationUpdate(communicationUpdate);
            }
            catch (Throwable t2) {
                Log.d(IAPCommunicationsManager.TAG, "Bad call state update listener " + t2);
            }
        }
    }
    
    public void callVoiceMail() {
        final iAPProcessor.IAP2SessionMessage iap2SessionMessage = new iAPProcessor.IAP2SessionMessage(iAPProcessor.iAPMessage.InitiateCall);
        ((iAPProcessor.IAP2ParamsCreator)iap2SessionMessage).addEnum(InitiateCall.Type, InitiateCallType.VoiceMail);
        this.miAPProcessor.sendControlMessage((iAPProcessor.IAP2ParamsCreator)iap2SessionMessage);
    }
    
    public void endCall() {
        if (!TextUtils.isEmpty((CharSequence)this.callUUid)) {
            final iAPProcessor.IAP2SessionMessage iap2SessionMessage = new iAPProcessor.IAP2SessionMessage(iAPProcessor.iAPMessage.EndCall);
            ((iAPProcessor.IAP2ParamsCreator)iap2SessionMessage).addEnum(EndCall.EndAction, EndCallAction.EndDecline);
            ((iAPProcessor.IAP2ParamsCreator)iap2SessionMessage).addString(EndCall.CallUUID, this.callUUid);
            this.miAPProcessor.sendControlMessage((iAPProcessor.IAP2ParamsCreator)iap2SessionMessage);
        }
    }
    
    public void endCall(final String s) {
        if (!TextUtils.isEmpty((CharSequence)s)) {
            final iAPProcessor.IAP2SessionMessage iap2SessionMessage = new iAPProcessor.IAP2SessionMessage(iAPProcessor.iAPMessage.EndCall);
            ((iAPProcessor.IAP2ParamsCreator)iap2SessionMessage).addEnum(EndCall.EndAction, EndCallAction.EndDecline);
            ((iAPProcessor.IAP2ParamsCreator)iap2SessionMessage).addString(EndCall.CallUUID, s);
            this.miAPProcessor.sendControlMessage((iAPProcessor.IAP2ParamsCreator)iap2SessionMessage);
        }
    }
    
    public void initiateDestinationCall(final String s) {
        final iAPProcessor.IAP2SessionMessage iap2SessionMessage = new iAPProcessor.IAP2SessionMessage(iAPProcessor.iAPMessage.InitiateCall);
        ((iAPProcessor.IAP2ParamsCreator)iap2SessionMessage).addEnum(InitiateCall.Type, InitiateCallType.Destination);
        ((iAPProcessor.IAP2ParamsCreator)iap2SessionMessage).addString(InitiateCall.DestinationID, s);
        ((iAPProcessor.IAP2ParamsCreator)iap2SessionMessage).addEnum(InitiateCall.Service, Service.Telephony);
        this.miAPProcessor.sendControlMessage((iAPProcessor.IAP2ParamsCreator)iap2SessionMessage);
    }
    
    public void mute() {
        final iAPProcessor.IAP2SessionMessage iap2SessionMessage = new iAPProcessor.IAP2SessionMessage(iAPProcessor.iAPMessage.MuteStatusUpdate);
        ((iAPProcessor.IAP2ParamsCreator)iap2SessionMessage).addBoolean(MuteStatusUpdate.MuteStatus, true);
        this.miAPProcessor.sendControlMessage((iAPProcessor.IAP2ParamsCreator)iap2SessionMessage);
    }
    
    public void redial() {
        final iAPProcessor.IAP2SessionMessage iap2SessionMessage = new iAPProcessor.IAP2SessionMessage(iAPProcessor.iAPMessage.InitiateCall);
        ((iAPProcessor.IAP2ParamsCreator)iap2SessionMessage).addEnum(InitiateCall.Type, InitiateCallType.Redial);
        this.miAPProcessor.sendControlMessage((iAPProcessor.IAP2ParamsCreator)iap2SessionMessage);
    }
    
    public void setUpdatesListener(final IAPCommunicationsUpdateListener mUpdatesListener) {
        this.mUpdatesListener = mUpdatesListener;
    }
    
    public void startCommunicationUpdates() {
    }
    
    public void startUpdates() {
        final iAPProcessor.IAP2SessionMessage iap2SessionMessage = new iAPProcessor.IAP2SessionMessage(iAPProcessor.iAPMessage.StartCallStateUpdates);
        ((iAPProcessor.IAP2ParamsCreator)iap2SessionMessage).addNone(StartCallStateUpdates.RemoteID).addNone(StartCallStateUpdates.DisplayName).addNone(StartCallStateUpdates.Status).addNone(StartCallStateUpdates.Direction).addNone(StartCallStateUpdates.CallUUID).addNone(StartCallStateUpdates.AddressBookID).addNone(StartCallStateUpdates.Label).addNone(StartCallStateUpdates.Service).addNone(StartCallStateUpdates.IsConferenced).addNone(StartCallStateUpdates.ConferenceGroup).addNone(StartCallStateUpdates.DisconnectReason);
        this.miAPProcessor.sendControlMessage((iAPProcessor.IAP2ParamsCreator)iap2SessionMessage);
    }
    
    public void stopCommunicationUpdates() {
    }
    
    public void stopUpdates() {
        this.miAPProcessor.sendControlMessage((iAPProcessor.IAP2ParamsCreator)new iAPProcessor.IAP2SessionMessage(iAPProcessor.iAPMessage.StopCallStateUpdates));
    }
    
    public void unMute() {
        final iAPProcessor.IAP2SessionMessage iap2SessionMessage = new iAPProcessor.IAP2SessionMessage(iAPProcessor.iAPMessage.MuteStatusUpdate);
        ((iAPProcessor.IAP2ParamsCreator)iap2SessionMessage).addBoolean(MuteStatusUpdate.MuteStatus, false);
        this.miAPProcessor.sendControlMessage((iAPProcessor.IAP2ParamsCreator)iap2SessionMessage);
    }
    
    enum AcceptAction
    {
        EndAndAccept, 
        HoldAndAccept;
    }
    
    enum AcceptCall
    {
        AcceptAction, 
        CallUUID;
    }
    
    enum EndCall
    {
        CallUUID, 
        EndAction;
    }
    
    enum EndCallAction
    {
        EndAll, 
        EndDecline;
    }
    
    enum InitiateCall
    {
        AddressBookId, 
        DestinationID, 
        Service, 
        Type;
    }
    
    enum InitiateCallType
    {
        Destination, 
        Redial, 
        VoiceMail;
    }
    
    enum MuteStatusUpdate
    {
        MuteStatus;
    }
    
    public enum Service
    {
        FaceTimeAudio, 
        FaceTimeVideo, 
        Telephony, 
        Unknown;
    }
    
    enum StartCallStateUpdates
    {
        AddressBookID, 
        CallUUID, 
        ConferenceGroup, 
        Direction, 
        DisconnectReason, 
        DisplayName, 
        IsConferenced, 
        Label, 
        RemoteID, 
        Service, 
        Skip, 
        Status;
    }
    
    enum StartCommunicationUpdates
    {
        MuteStatus(9);
        
        int id;
        
        private StartCommunicationUpdates(final int id) {
            this.id = id;
        }
    }
}
