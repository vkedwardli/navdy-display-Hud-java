package com.navdy.hud.mfi;

import java.util.Arrays;
import java.util.ArrayList;
import android.os.Looper;
import android.os.Handler;
import java.util.List;
import java.io.IOException;
import java.io.OutputStream;
import java.io.InputStream;
import android.util.Log;
import okio.Buffer;

public class EASession
{
    private static final String TAG;
    private Buffer buffer;
    private boolean closed;
    private final Object dataAvailable;
    private iAPProcessor iAPProcessor;
    private String macAddress;
    private EASession mySelf;
    private String name;
    private String protocol;
    private int sessionIdentifier;
    
    static {
        TAG = EASession.class.getSimpleName();
    }
    
    public EASession(final iAPProcessor iapProcessor, final String macAddress, final String name, final int sessionIdentifier, final String protocol) {
        this.dataAvailable = new Object();
        this.mySelf = this;
        this.iAPProcessor = iapProcessor;
        this.macAddress = macAddress;
        this.name = name;
        this.sessionIdentifier = sessionIdentifier;
        this.protocol = protocol;
        if (Log.isLoggable(EASession.TAG, 3)) {
            Log.d(EASession.TAG, String.format("%s: created", this.mySelf));
        }
        this.buffer = new Buffer();
    }
    
    public void close() {
        Log.d(EASession.TAG, "Closing EASession streams");
        final Object dataAvailable = this.dataAvailable;
        synchronized (dataAvailable) {
            this.closed = true;
            this.buffer.close();
            this.dataAvailable.notify();
        }
    }
    
    public InputStream getInputStream() {
        return new BlockingInputStream(this.buffer.inputStream());
    }
    
    public String getMacAddress() {
        return this.macAddress;
    }
    
    public String getName() {
        return this.name;
    }
    
    public OutputStream getOutputStream() {
        return new OutStream();
    }
    
    public String getProtocol() {
        return this.protocol;
    }
    
    public int getSessionIdentifier() {
        return this.sessionIdentifier;
    }
    
    public void queue(final byte[] array) {
        if (Log.isLoggable(EASession.TAG, 3)) {
            Log.d(EASession.TAG, String.format("%s: buffer.write: %d bytes: %s, '%s'", this.mySelf, array.length, Utils.bytesToHex(array, true), Utils.toASCII(array)));
        }
        final Object dataAvailable = this.dataAvailable;
        synchronized (dataAvailable) {
            this.buffer.write(array);
            this.dataAvailable.notify();
        }
    }
    
    @Override
    public String toString() {
        return String.format("%s{%s/%d}", super.toString(), this.protocol, this.sessionIdentifier);
    }
    
    class BlockingInputStream extends InputStream
    {
        private InputStream source;
        
        public BlockingInputStream(final InputStream source) {
            this.source = source;
        }
        
        private void waitForAvailable() throws IOException {
            if (this.source.available() != 0) {
                return;
            }
            try {
                EASession.this.dataAvailable.wait();
            }
            catch (InterruptedException ex) {}
        }
        
        @Override
        public int read() throws IOException {
            final Object access$000 = EASession.this.dataAvailable;
            synchronized (access$000) {
                this.waitForAvailable();
                return this.source.read();
            }
        }
        
        @Override
        public int read(final byte[] array) throws IOException {
            final Object access$000 = EASession.this.dataAvailable;
            synchronized (access$000) {
                this.waitForAvailable();
                return this.source.read(array);
            }
        }
        
        @Override
        public int read(final byte[] array, int read, final int n) throws IOException {
            final Object access$000 = EASession.this.dataAvailable;
            synchronized (access$000) {
                this.waitForAvailable();
                read = this.source.read(array, read, n);
                return read;
            }
        }
    }
    
    class OutStream extends OutputStream
    {
        private int FLUSH_DELAY;
        private Runnable autoFlush;
        private List<byte[]> frames;
        private Handler handler;
        
        OutStream() {
            this.handler = new Handler(Looper.getMainLooper());
            this.FLUSH_DELAY = 250;
            this.frames = new ArrayList<byte[]>();
            this.autoFlush = new Runnable() {
                @Override
                public void run() {
                    try {
                        OutStream.this.flush();
                    }
                    catch (IOException ex) {}
                }
            };
        }
        
        private byte[] assembleFrames() {
            byte[] array = null;
            int n = 0;
            for (int i = 0; i < this.frames.size(); ++i) {
                n += this.frames.get(i).length;
            }
            if (n > 0) {
                if (this.frames.size() == 1) {
                    array = this.frames.get(0);
                }
                else {
                    final int n2 = 0;
                    final byte[] array2 = new byte[n];
                    int n3 = 0;
                    int n4 = n2;
                    while (true) {
                        array = array2;
                        if (n3 >= this.frames.size()) {
                            break;
                        }
                        final byte[] array3 = this.frames.get(n3);
                        System.arraycopy(array3, 0, array2, n4, array3.length);
                        n4 += array3.length;
                        ++n3;
                    }
                }
                this.frames.clear();
            }
            return array;
        }
        
        @Override
        public void close() throws IOException {
            this.flush();
        }
        
        @Override
        public void flush() throws IOException {
            synchronized (this) {
                this.handler.removeCallbacks(this.autoFlush);
                final byte[] assembleFrames = this.assembleFrames();
                if (!EASession.this.closed && assembleFrames != null) {
                    EASession.this.iAPProcessor.queue(new EASessionPacket(EASession.this.getSessionIdentifier(), assembleFrames));
                }
            }
        }
        
        @Override
        public void write(final int n) throws IOException {
            this.write(new byte[] { (byte)n });
        }
        
        @Override
        public void write(final byte[] array) throws IOException {
            synchronized (this) {
                if (Log.isLoggable(EASession.TAG, 3)) {
                    Log.d(EASession.TAG, String.format("%s: OutStream.write: %d bytes: %s, '%s'", EASession.this.mySelf, array.length, Utils.bytesToHex(array, true), Utils.toASCII(array)));
                }
                this.frames.add(array);
                this.handler.removeCallbacks(this.autoFlush);
                this.handler.postDelayed(this.autoFlush, (long)this.FLUSH_DELAY);
            }
        }
        
        @Override
        public void write(final byte[] array, final int n, final int n2) throws IOException {
            this.write(Arrays.copyOfRange(array, n, n2));
        }
    }
}
