package com.navdy.hud.mfi;

public interface PacketQueue<T>
{
    void queue(final T p0);
}
