package com.navdy.hud.mfi;

public class EASessionPacket extends Packet
{
    int session;
    
    public EASessionPacket(final int session, final byte[] array) {
        super(array);
        this.session = session;
    }
}
