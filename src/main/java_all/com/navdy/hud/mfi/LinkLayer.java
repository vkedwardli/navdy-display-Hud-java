package com.navdy.hud.mfi;

import android.os.Message;
import android.os.Looper;
import android.util.Log;
import android.os.Handler;
import java.util.concurrent.ConcurrentLinkedQueue;

public class LinkLayer implements SessionPacketReceiver, LinkPacketReceiver
{
    public static final int CONTROL_BYTE_ACK = 64;
    public static final int CONTROL_BYTE_EAK = 32;
    public static final int CONTROL_BYTE_RST = 16;
    public static final int CONTROL_BYTE_SLP = 8;
    public static final int CONTROL_BYTE_SYN = 128;
    public static final int DELAY_MILLIS = 20;
    public static final int MESSAGE_CONNECTION_ENDED = 4;
    public static final int MESSAGE_CONNECTION_STARTED = 3;
    public static final int MESSAGE_LINK_PACKET = 1;
    public static final int MESSAGE_PERIODIC = 0;
    public static final int MESSAGE_SESSION_PACKET = 2;
    public static final String TAG = "LinkLayer";
    private static final boolean sLogLinkPacket = true;
    private ConcurrentLinkedQueue<SessionPacket> controlMessages;
    private iAPProcessor iAPProcessor;
    private Handler myHandler;
    private ConcurrentLinkedQueue<SessionPacket> nonControlMessages;
    private PhysicalLayer physicalLayer;
    private String remoteAddress;
    private String remoteName;
    
    static {
        System.loadLibrary("iap2");
    }
    
    public LinkLayer() {
        this.start();
    }
    
    private void log(final String s, final Packet packet) {
        Utils.logTransfer("LinkLayer", "%s: %s", s, packet.data);
    }
    
    private void logOutGoingLinkPacket(final byte[] array) {
        if (array != null && array.length > 7) {
            final byte b = array[5];
            final byte b2 = array[6];
            final int n = array[4] & 0xFF;
            final StringBuilder sb = new StringBuilder();
            if ((n & 0x40) != 0x0) {
                sb.append(" ACK ");
            }
            if ((n & 0x80) != 0x0) {
                sb.append(" SYN ");
            }
            if ((n & 0x20) != 0x0) {
                sb.append(" EAK ");
            }
            if ((n & 0x10) != 0x0) {
                sb.append(" RST ");
            }
            if ((n & 0x8) != 0x0) {
                sb.append(" SLP ");
            }
            Log.d("MFi", "Accessory(L)   :" + (b & 0xFF) + " " + (b2 & 0xFF) + " Size:" + array.length + sb.toString());
        }
    }
    
    private native int native_get_message_session();
    
    private native byte[] native_pop_message_data(final int p0);
    
    private native int native_runloop(final int p0, final byte[] p1, final int p2);
    
    private void queuePacket(final int n, final Object o) {
        this.myHandler.sendMessage(this.myHandler.obtainMessage(n, o));
    }
    
    public void connect(final PhysicalLayer physicalLayer) {
        this.physicalLayer = physicalLayer;
    }
    
    public void connect(final iAPProcessor iapProcessor) {
        this.iAPProcessor = iapProcessor;
    }
    
    public void connectionEnded() {
        if (this.myHandler != null) {
            this.myHandler.removeMessages(0);
            this.myHandler.removeMessages(1);
            this.myHandler.removeMessages(2);
        }
        this.controlMessages.clear();
        this.nonControlMessages.clear();
        this.queuePacket(4, null);
    }
    
    public void connectionStarted(final String remoteAddress, final String remoteName) {
        this.remoteAddress = remoteAddress;
        this.remoteName = remoteName;
        this.queuePacket(3, null);
    }
    
    public byte[] getLocalAddress() {
        return this.physicalLayer.getLocalAddress();
    }
    
    public String getRemoteAddress() {
        return this.remoteAddress;
    }
    
    public String getRemoteName() {
        return this.remoteName;
    }
    
    public native int native_get_maximum_payload_size();
    
    @Override
    public void queue(final LinkPacket linkPacket) {
        this.queuePacket(1, linkPacket);
    }
    
    @Override
    public void queue(final SessionPacket sessionPacket) {
        this.log("iAP->LL[" + sessionPacket.session + "]", sessionPacket);
        if (sessionPacket.session == this.iAPProcessor.getControlSessionId()) {
            this.controlMessages.add(sessionPacket);
        }
        else {
            this.nonControlMessages.add(sessionPacket);
        }
        this.myHandler.sendEmptyMessage(2);
    }
    
    public void start() {
        this.controlMessages = new ConcurrentLinkedQueue<SessionPacket>();
        this.nonControlMessages = new ConcurrentLinkedQueue<SessionPacket>();
        (this.myHandler = new Handler(Looper.getMainLooper()) {
            private void doNativeRunLoop(final int n, final byte[] array, final int n2) {
                if (LinkLayer.this.native_runloop(n, array, n2) < 0) {
                    Log.i("LinkLayer", "runloop detected a disconnect");
                    LinkLayer.this.connectionEnded();
                }
                else {
                    this.transferPackets(1);
                    this.transferPackets(2);
                }
            }
            
            private void transferPackets(final int n) {
                while (true) {
                    final int access$400 = LinkLayer.this.native_get_message_session();
                    final byte[] access$401 = LinkLayer.this.native_pop_message_data(n);
                    if (access$401 == null) {
                        break;
                    }
                    switch (n) {
                        default:
                            Log.e("LinkLayer", "transferPackets: unsupported message type (" + n + ")");
                            continue;
                        case 1:
                            LinkLayer.this.logOutGoingLinkPacket(access$401);
                            LinkLayer.this.physicalLayer.queue(new LinkPacket(access$401));
                            continue;
                        case 2: {
                            final SessionPacket sessionPacket = new SessionPacket(access$400, access$401);
                            LinkLayer.this.log("LL->iAP[" + sessionPacket.session + "]", sessionPacket);
                            LinkLayer.this.iAPProcessor.queue(sessionPacket);
                            continue;
                        }
                    }
                }
            }
            
            public void handleMessage(final Message message) {
                final byte[] array = null;
                final int n = 0;
                final int what = message.what;
                byte[] array2 = array;
                int n2 = what;
                int session = n;
                while (true) {
                    switch (message.what) {
                        default:
                            session = n;
                            n2 = what;
                            array2 = array;
                            break Label_0070;
                        case 4:
                            LinkLayer.this.iAPProcessor.linkLost();
                            array2 = array;
                            n2 = what;
                            session = n;
                            break Label_0070;
                        case 1:
                            array2 = ((Packet)message.obj).data;
                            n2 = what;
                            session = n;
                        case 3:
                            if (message.what == 0) {
                                this.sendEmptyMessageDelayed(0, 20L);
                            }
                            this.doNativeRunLoop(n2, array2, session);
                        case 0:
                        case 2: {
                            SessionPacket sessionPacket = null;
                            if (LinkLayer.this.controlMessages.peek() != null) {
                                sessionPacket = LinkLayer.this.controlMessages.poll();
                            }
                            else if (LinkLayer.this.nonControlMessages.peek() != null) {
                                sessionPacket = LinkLayer.this.nonControlMessages.poll();
                            }
                            array2 = array;
                            n2 = what;
                            session = n;
                            if (sessionPacket != null) {
                                n2 = 2;
                                array2 = sessionPacket.data;
                                session = sessionPacket.session;
                            }
                            continue;
                        }
                    }
                    break;
                }
            }
        }).sendEmptyMessageDelayed(0, 20L);
    }
    
    public void stop() {
    }
    
    public interface PhysicalLayer extends LinkPacketReceiver
    {
        byte[] getLocalAddress();
    }
}
