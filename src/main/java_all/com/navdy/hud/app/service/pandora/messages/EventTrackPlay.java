package com.navdy.hud.app.service.pandora.messages;

import com.navdy.hud.app.service.pandora.exceptions.MessageWrongWayException;
import com.navdy.hud.app.service.pandora.exceptions.UnexpectedEndOfStringException;
import com.navdy.hud.app.service.pandora.exceptions.StringOverflowException;
import java.io.IOException;

public class EventTrackPlay extends BaseOutgoingEmptyMessage
{
    public static final EventTrackPlay INSTANCE;
    
    static {
        INSTANCE = new EventTrackPlay();
    }
    
    @Override
    protected byte getMessageType() {
        return 48;
    }
    
    @Override
    public String toString() {
        return "Play music action";
    }
}
