package com.navdy.hud.app.service.pandora.messages;

import com.navdy.hud.app.service.pandora.exceptions.MessageWrongWayException;
import com.navdy.hud.app.service.pandora.exceptions.UnexpectedEndOfStringException;
import com.navdy.hud.app.service.pandora.exceptions.StringOverflowException;
import java.io.IOException;

public class SessionTerminate extends BaseOutgoingEmptyMessage
{
    public static final SessionTerminate INSTANCE;
    
    static {
        INSTANCE = new SessionTerminate();
    }
    
    @Override
    protected byte getMessageType() {
        return 5;
    }
    
    @Override
    public String toString() {
        return "Session Terminate";
    }
}
