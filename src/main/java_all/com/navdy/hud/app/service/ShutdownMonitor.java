package com.navdy.hud.app.service;

import com.navdy.hud.app.util.GenericUtil;
import com.navdy.hud.app.event.Wakeup;
import com.navdy.hud.app.event.DrivingStateChange;
import com.navdy.service.library.events.connection.ConnectionStateChange;
import com.navdy.service.library.events.hudcontrol.AccelerateShutdown;
import com.squareup.otto.Subscribe;
import java.nio.charset.Charset;
import com.navdy.hud.app.debug.DriveRecorder;
import android.location.LocationManager;
import mortar.Mortar;
import com.navdy.hud.app.HudApplication;
import android.os.SystemClock;
import android.net.LocalSocketAddress;
import android.net.LocalSocketAddress;
import com.navdy.hud.app.event.Shutdown;
import com.navdy.hud.app.device.light.LED;
import java.io.Closeable;
import com.navdy.service.library.util.IOUtils;
import com.navdy.hud.app.util.os.SystemProperties;
import android.os.Looper;
import com.navdy.hud.app.util.DeviceUtil;
import android.os.Bundle;
import com.navdy.service.library.task.TaskManager;
import java.util.concurrent.TimeUnit;
import com.navdy.hud.app.device.PowerManager;
import java.io.OutputStream;
import java.io.InputStream;
import android.net.LocalSocket;
import com.navdy.hud.app.obd.ObdManager;
import android.location.Location;
import android.util.ArrayMap;
import android.location.LocationListener;
import com.navdy.hud.app.manager.InputManager;
import android.os.Handler;
import com.navdy.hud.app.device.dial.DialManager;
import javax.inject.Inject;
import com.squareup.otto.Bus;
import com.navdy.service.library.log.Logger;

public class ShutdownMonitor implements Runnable
{
    private static final long ACCELERATED_TIMEOUT;
    private static final long ACTIVE_USE_TIMEOUT;
    private static final long BOOT_MODE_TIMEOUT;
    private static final float MOVEMENT_THRESHOLD = 60.0f;
    private static final long OBD_ACTIVE_TIMEOUT;
    private static final long OBD_DISCONNECT_DELAY;
    private static final long OBD_DISCONNECT_TIMEOUT;
    private static final long PENDING_SHUTDOWN_TIMEOUT;
    private static final long POWER_DISCONECT_SMOOTHING_TIMEOUT;
    private static long REPORT_INTERVAL = 0L;
    private static final long SAMPLING_INTERVAL;
    private static final String SHUTDOWND_REQUEST_CLICK = "powerclick";
    private static final String SHUTDOWND_REQUEST_DOUBLE_CLICK = "powerdoubleclick";
    private static final String SHUTDOWND_REQUEST_LONG_PRESS = "powerlongpress";
    private static final String SHUTDOWND_REQUEST_POWER_STATE = "powerstate";
    private static String SHUTDOWN_OVERRIDE_SETTING;
    private static final String SOCKET_NAME = "shutdownd";
    private static String TEMPORARY_SHUTDOWN_OVERRIDE_SETTING;
    private static ShutdownMonitor sInstance;
    private static final Logger sLogger;
    @Inject
    Bus bus;
    private String lastWakeReason;
    private long mAccelerateTime;
    private long mDialLastActivityTimeMsecs;
    private DialManager mDialManager;
    private boolean mDriving;
    private Handler mHandler;
    private boolean mInactivityShutdownDisabled;
    @Inject
    InputManager mInputManager;
    private boolean mIsDialConnected;
    private long mLastInputEventTimeMsecs;
    private long mLastMovementReport;
    private long mLastMovementTimeMsecs;
    private LocationListener mLocationListener;
    private boolean mMainPowerOn;
    private MonitorState mMonitorState;
    private ArrayMap<String, Location> mMovementBases;
    private long mObdDisconnectTimeMsecs;
    private ObdManager mObdManager;
    private long mRemoteDeviceConnectTimeMsecs;
    private boolean mScreenDimmingDisabled;
    private LocalSocket mSocket;
    private InputStream mSocketInputStream;
    private OutputStream mSocketOutputStream;
    private long mStateChangeTime;
    private boolean mUsbPowerOn;
    private double maxVoltage;
    private final NotificationReceiver notifyReceiver;
    private Runnable powerLossRunnable;
    @Inject
    PowerManager powerManager;
    
    static {
        sLogger = new Logger(ShutdownMonitor.class);
        ShutdownMonitor.sInstance = null;
        SAMPLING_INTERVAL = TimeUnit.SECONDS.toMillis(15L);
        BOOT_MODE_TIMEOUT = TimeUnit.MINUTES.toMillis(7L);
        OBD_ACTIVE_TIMEOUT = TimeUnit.MINUTES.toMillis(20L);
        OBD_DISCONNECT_TIMEOUT = TimeUnit.SECONDS.toMillis(5L);
        ACCELERATED_TIMEOUT = TimeUnit.SECONDS.toMillis(30L);
        PENDING_SHUTDOWN_TIMEOUT = TimeUnit.MINUTES.toMillis(5L);
        ACTIVE_USE_TIMEOUT = TimeUnit.MINUTES.toMillis(3L);
        OBD_DISCONNECT_DELAY = TimeUnit.SECONDS.toMillis(30L);
        POWER_DISCONECT_SMOOTHING_TIMEOUT = TimeUnit.SECONDS.toMillis(3L);
        ShutdownMonitor.SHUTDOWN_OVERRIDE_SETTING = "persist.sys.noautoshutdown";
        ShutdownMonitor.TEMPORARY_SHUTDOWN_OVERRIDE_SETTING = "sys.powerctl.noautoshutdown";
        ShutdownMonitor.REPORT_INTERVAL = TimeUnit.MINUTES.toMillis(1L);
    }
    
    private ShutdownMonitor() {
        this.mRemoteDeviceConnectTimeMsecs = 0L;
        this.mIsDialConnected = false;
        this.mDialLastActivityTimeMsecs = 0L;
        this.mLastInputEventTimeMsecs = 0L;
        this.mObdDisconnectTimeMsecs = 0L;
        this.mDriving = false;
        this.maxVoltage = 0.0;
        this.powerLossRunnable = null;
        this.mScreenDimmingDisabled = false;
        this.mInactivityShutdownDisabled = false;
        this.mMonitorState = MonitorState.STATE_BOOT;
        this.mStateChangeTime = 0L;
        this.mAccelerateTime = 0L;
        this.mMovementBases = (ArrayMap<String, Location>)new ArrayMap();
        this.mLastMovementTimeMsecs = 0L;
        this.mLastMovementReport = 0L;
        this.mLocationListener = (LocationListener)new LocationListener() {
            public void onLocationChanged(Location location) {
                location = new Location(location);
                TaskManager.getInstance().execute(new Runnable() {
                    @Override
                    public void run() {
                        String provider;
                        if ((provider = location.getProvider()) == null) {
                            provider = "unknown";
                        }
                        final Location location = (Location)ShutdownMonitor.this.mMovementBases.get(provider);
                        if (location == null) {
                            ShutdownMonitor.this.mMovementBases.put(provider, location);
                            ShutdownMonitor.this.onMovement();
                        }
                        else if (location.distanceTo(location) > 60.0f) {
                            ShutdownMonitor.this.mMovementBases.put(provider, location);
                            ShutdownMonitor.this.onMovement();
                            final long access$1700 = ShutdownMonitor.this.getCurrentTimeMsecs();
                            if (ShutdownMonitor.this.mLastMovementReport == 0L || access$1700 - ShutdownMonitor.this.mLastMovementReport >= ShutdownMonitor.REPORT_INTERVAL) {
                                ShutdownMonitor.this.mLastMovementReport = access$1700;
                                ShutdownMonitor.sLogger.i("Moved significant distance - based on " + provider + " provider");
                            }
                        }
                    }
                }, 1);
            }
            
            public void onProviderDisabled(final String s) {
            }
            
            public void onProviderEnabled(final String s) {
            }
            
            public void onStatusChanged(final String s, final int n, final Bundle bundle) {
            }
        };
        this.notifyReceiver = new NotificationReceiver();
        if (!DeviceUtil.isNavdyDevice()) {
            ShutdownMonitor.sLogger.w("not a Navdy device, ShutdownMonitor will not run");
        }
        else {
            final long currentTimeMsecs = this.getCurrentTimeMsecs();
            this.mHandler = new Handler(Looper.getMainLooper());
            this.mDialManager = DialManager.getInstance();
            this.mObdManager = ObdManager.getInstance();
            this.mLastInputEventTimeMsecs = currentTimeMsecs;
            this.mDialLastActivityTimeMsecs = currentTimeMsecs;
            this.mObdDisconnectTimeMsecs = 0L;
            this.mLastMovementTimeMsecs = 0L;
            this.mRemoteDeviceConnectTimeMsecs = 0L;
            TaskManager.getInstance().execute(new Runnable() {
                @Override
                public void run() {
                    ShutdownMonitor.this.establishSocketConnection();
                    new MessageReceiver().start();
                    ShutdownMonitor.this.mHandler.postDelayed((Runnable)ShutdownMonitor.this, ShutdownMonitor.SAMPLING_INTERVAL);
                }
            }, 1);
        }
    }
    
    private boolean canDoShutdown() {
        boolean b = true;
        int n;
        if (SystemProperties.getBoolean(ShutdownMonitor.SHUTDOWN_OVERRIDE_SETTING, false) || SystemProperties.getBoolean(ShutdownMonitor.TEMPORARY_SHUTDOWN_OVERRIDE_SETTING, false) || this.mUsbPowerOn) {
            n = 1;
        }
        else {
            n = 0;
        }
        if (n != 0) {
            b = false;
        }
        return b;
    }
    
    private long checkObdConnection() {
        long n = 0L;
        if (this.mObdDisconnectTimeMsecs != 0L) {
            n = this.getCurrentTimeMsecs() - this.mObdDisconnectTimeMsecs;
        }
        return n;
    }
    
    private void closeSocketConnection() {
        synchronized (this) {
            IOUtils.closeStream(this.mSocketInputStream);
            IOUtils.closeStream(this.mSocketOutputStream);
            IOUtils.closeStream((Closeable)this.mSocket);
            this.mSocket = null;
            this.mSocketInputStream = null;
            this.mSocketOutputStream = null;
        }
    }
    
    private void enterActiveUseState() {
        this.setMonitorState(MonitorState.STATE_ACTIVE_USE);
    }
    
    private void enterBootState() {
        this.setMonitorState(MonitorState.STATE_BOOT);
    }
    
    private void enterObdActiveState() {
        this.setMonitorState(MonitorState.STATE_OBD_ACTIVE);
    }
    
    private void enterPendingShutdownState() {
        if (this.canDoShutdown()) {
            LED.writeToSysfs("0", "/sys/dlpc/led_enable");
            this.setMonitorState(MonitorState.STATE_PENDING_SHUTDOWN);
        }
        else {
            ShutdownMonitor.sLogger.v("pending shutdown state disabled by property setting");
        }
    }
    
    private void enterQuiteModeState() {
        this.setMonitorState(MonitorState.STATE_QUIET_MODE);
    }
    
    private void enterShutdownPromptState(final Shutdown.Reason reason) {
        this.bus.post(new Shutdown(reason));
        this.setMonitorState(MonitorState.STATE_SHUTDOWN_PROMPT);
    }
    
    private void enterShutdownPromptStateIfNotDisabled(final Shutdown.Reason reason) {
        final boolean canDoShutdown = this.canDoShutdown();
        if (!this.mInactivityShutdownDisabled && canDoShutdown) {
            this.enterShutdownPromptState(reason);
        }
        else {
            ShutdownMonitor.sLogger.v(String.format("shutdown prompt state disabled, shutdownAllowed = %b, mInactivityShutdownDisabled = %b, mUsbPowerOn = %b", canDoShutdown, this.mInactivityShutdownDisabled, this.mUsbPowerOn));
        }
    }
    
    private void establishSocketConnection() {
        synchronized (this) {
            this.mSocket = new LocalSocket();
            final LocalSocketAddress localSocketAddress = new LocalSocketAddress("shutdownd", LocalSocketAddress$Namespace.RESERVED);
            try {
                this.mSocket.connect(localSocketAddress);
                this.mSocketOutputStream = this.mSocket.getOutputStream();
                this.mSocketInputStream = this.mSocket.getInputStream();
            }
            catch (Exception ex) {
                ShutdownMonitor.sLogger.e("exception while attempting to connect to shutdownd socket", ex);
                this.closeSocketConnection();
            }
        }
    }
    
    private long evaluateState() {
        final long currentTimeMsecs = this.getCurrentTimeMsecs();
        final long sampling_INTERVAL = ShutdownMonitor.SAMPLING_INTERVAL;
        final double batteryVoltage = this.mObdManager.getBatteryVoltage();
        if (batteryVoltage > this.maxVoltage) {
            this.maxVoltage = batteryVoltage;
        }
        long min = sampling_INTERVAL;
        switch (this.mMonitorState) {
            default:
                min = sampling_INTERVAL;
                return min;
            case STATE_SHUTDOWN_PROMPT:
                return min;
            case STATE_BOOT:
                if (this.mObdManager.isConnected()) {
                    this.enterObdActiveState();
                    min = sampling_INTERVAL;
                    return min;
                }
                min = sampling_INTERVAL;
                if (currentTimeMsecs - this.mStateChangeTime > ShutdownMonitor.BOOT_MODE_TIMEOUT) {
                    this.enterActiveUseState();
                    min = sampling_INTERVAL;
                    return min;
                }
                return min;
            case STATE_OBD_ACTIVE: {
                final long checkObdConnection = this.checkObdConnection();
                if (checkObdConnection >= ShutdownMonitor.OBD_DISCONNECT_TIMEOUT) {
                    ShutdownMonitor.sLogger.i(String.format("OBD disconnected, driving = %b, battery voltage = %5.1f, max voltage = %5.1f", this.mDriving, batteryVoltage, this.maxVoltage));
                    if (!this.mDriving && batteryVoltage != -1.0 && batteryVoltage <= 12.899999618530273) {
                        this.enterShutdownPromptStateIfNotDisabled(Shutdown.Reason.ENGINE_OFF);
                        min = sampling_INTERVAL;
                        return min;
                    }
                    if (checkObdConnection < ShutdownMonitor.OBD_DISCONNECT_DELAY) {
                        min = Math.min(TimeUnit.SECONDS.toMillis(5L), ShutdownMonitor.OBD_DISCONNECT_DELAY - checkObdConnection);
                        return min;
                    }
                    if (this.mDriving) {
                        this.enterActiveUseState();
                        min = sampling_INTERVAL;
                        return min;
                    }
                    this.enterPendingShutdownState();
                    min = sampling_INTERVAL;
                    return min;
                }
                else {
                    min = sampling_INTERVAL;
                    if (currentTimeMsecs - this.lastWakeEventTime() < ShutdownMonitor.OBD_ACTIVE_TIMEOUT) {
                        return min;
                    }
                    min = sampling_INTERVAL;
                    if (!this.mDriving) {
                        this.enterShutdownPromptStateIfNotDisabled(Shutdown.Reason.ENGINE_OFF);
                        min = sampling_INTERVAL;
                        return min;
                    }
                    return min;
                }
                break;
            }
            case STATE_ACTIVE_USE:
                if (this.mObdManager.isConnected()) {
                    this.enterObdActiveState();
                    min = sampling_INTERVAL;
                    return min;
                }
                if (this.mAccelerateTime != 0L) {
                    final long lastWakeEventTime = this.lastWakeEventTime();
                    if (currentTimeMsecs - lastWakeEventTime >= ShutdownMonitor.ACCELERATED_TIMEOUT && !this.mDriving) {
                        this.enterShutdownPromptStateIfNotDisabled(Shutdown.Reason.ACCELERATE_SHUTDOWN);
                        min = sampling_INTERVAL;
                        return min;
                    }
                    min = ShutdownMonitor.ACCELERATED_TIMEOUT + lastWakeEventTime - currentTimeMsecs;
                    return min;
                }
                else {
                    min = sampling_INTERVAL;
                    if (this.mScreenDimmingDisabled) {
                        return min;
                    }
                    min = sampling_INTERVAL;
                    if (currentTimeMsecs - this.lastWakeEventTime() <= ShutdownMonitor.ACTIVE_USE_TIMEOUT) {
                        return min;
                    }
                    min = sampling_INTERVAL;
                    if (!this.mDriving) {
                        ShutdownMonitor.sLogger.i("Active use timeout - last wake event was " + this.lastWakeReason);
                        this.enterPendingShutdownState();
                        min = sampling_INTERVAL;
                        return min;
                    }
                    return min;
                }
                break;
            case STATE_PENDING_SHUTDOWN: {
                final long lastWakeEventTime2 = this.lastWakeEventTime();
                if (this.mObdManager.isConnected()) {
                    this.enterObdActiveState();
                    min = sampling_INTERVAL;
                    return min;
                }
                if (lastWakeEventTime2 > this.mStateChangeTime) {
                    ShutdownMonitor.sLogger.i("Wake reason:" + this.lastWakeReason);
                    this.enterActiveUseState();
                    min = sampling_INTERVAL;
                    return min;
                }
                if (this.mAccelerateTime == 0L) {
                    min = sampling_INTERVAL;
                    if (currentTimeMsecs - lastWakeEventTime2 < ShutdownMonitor.PENDING_SHUTDOWN_TIMEOUT) {
                        return min;
                    }
                }
                this.enterShutdownPromptStateIfNotDisabled(Shutdown.Reason.INACTIVITY);
                min = sampling_INTERVAL;
                return min;
            }
        }
    }
    
    private long getCurrentTimeMsecs() {
        return SystemClock.elapsedRealtime();
    }
    
    public static ShutdownMonitor getInstance() {
        synchronized (ShutdownMonitor.class) {
            if (ShutdownMonitor.sInstance == null) {
                (ShutdownMonitor.sInstance = new ShutdownMonitor()).init();
            }
            return ShutdownMonitor.sInstance;
        }
    }
    
    private InputStream getSocketInputStream() {
        synchronized (this) {
            if (this.mSocketInputStream == null) {
                this.establishSocketConnection();
            }
            return this.mSocketInputStream;
        }
    }
    
    private OutputStream getSocketOutputStream() {
        synchronized (this) {
            if (this.mSocketOutputStream == null) {
                this.establishSocketConnection();
            }
            return this.mSocketOutputStream;
        }
    }
    
    private void init() {
        while (true) {
            Mortar.inject(HudApplication.getAppContext(), this);
            final Looper mainLooper = Looper.getMainLooper();
            final LocationManager locationManager = (LocationManager)HudApplication.getAppContext().getSystemService("location");
            while (true) {
                try {
                    locationManager.requestLocationUpdates("gps", 0L, 0.0f, this.mLocationListener, mainLooper);
                    locationManager.requestLocationUpdates("network", 0L, 0.0f, this.mLocationListener, mainLooper);
                    this.bus.register(this.notifyReceiver);
                    if (this.powerManager.inQuietMode()) {
                        this.enterQuiteModeState();
                        return;
                    }
                }
                catch (IllegalArgumentException ex) {
                    ShutdownMonitor.sLogger.e("failed to register with GPS or Network provider");
                    continue;
                }
                break;
            }
            this.enterBootState();
        }
    }
    
    private long lastWakeEventTime() {
        this.updateStats();
        final long currentTimeMsecs = this.getCurrentTimeMsecs();
        final long mStateChangeTime = this.mStateChangeTime;
        this.lastWakeReason = "stateChange";
        long mDialLastActivityTimeMsecs = mStateChangeTime;
        if (this.mDialLastActivityTimeMsecs > mStateChangeTime) {
            mDialLastActivityTimeMsecs = this.mDialLastActivityTimeMsecs;
            this.lastWakeReason = "dial";
        }
        long mLastInputEventTimeMsecs = mDialLastActivityTimeMsecs;
        if (this.mLastInputEventTimeMsecs > mDialLastActivityTimeMsecs) {
            mLastInputEventTimeMsecs = this.mLastInputEventTimeMsecs;
            this.lastWakeReason = "input";
        }
        long mRemoteDeviceConnectTimeMsecs = mLastInputEventTimeMsecs;
        if (this.mRemoteDeviceConnectTimeMsecs > mLastInputEventTimeMsecs) {
            mRemoteDeviceConnectTimeMsecs = this.mRemoteDeviceConnectTimeMsecs;
            this.lastWakeReason = "phone";
        }
        long mLastMovementTimeMsecs = mRemoteDeviceConnectTimeMsecs;
        if (this.mLastMovementTimeMsecs > mRemoteDeviceConnectTimeMsecs) {
            mLastMovementTimeMsecs = this.mLastMovementTimeMsecs;
            this.lastWakeReason = "movement";
        }
        final DriveRecorder driveRecorder = this.mObdManager.getDriveRecorder();
        long n = mLastMovementTimeMsecs;
        if (driveRecorder != null) {
            n = mLastMovementTimeMsecs;
            if (driveRecorder.isDemoPlaying()) {
                n = currentTimeMsecs;
                this.lastWakeReason = "recording";
            }
        }
        final String value = SystemProperties.get("navdy.ota.downloading", "0");
        while (true) {
            try {
                final long n2 = Integer.parseInt(value) * 1000L;
                long n3 = n;
                if (n2 > n) {
                    this.lastWakeReason = "ota";
                    n3 = n2;
                }
                return n3;
            }
            catch (NumberFormatException ex) {
                ShutdownMonitor.sLogger.e(String.format("%s property has invalid value: %s", "navdy.ota.downloading", value));
                final long n2 = 0L;
                continue;
            }
            break;
        }
    }
    
    private void leavePendingShutdownState() {
        LED.writeToSysfs("1", "/sys/dlpc/led_enable");
    }
    
    private void leaveShutdownPromptState() {
        this.mAccelerateTime = 0L;
    }
    
    private void onMovement() {
        this.mLastMovementTimeMsecs = this.getCurrentTimeMsecs();
        this.mAccelerateTime = 0L;
        this.onWakeEvent();
    }
    
    private void onWakeEvent() {
        if (this.mMonitorState == MonitorState.STATE_SHUTDOWN_PROMPT || this.mMonitorState == MonitorState.STATE_PENDING_SHUTDOWN) {
            this.enterActiveUseState();
        }
    }
    
    private void setMonitorState(final MonitorState mMonitorState) {
        switch (this.mMonitorState) {
            case STATE_PENDING_SHUTDOWN:
                this.leavePendingShutdownState();
                break;
            case STATE_SHUTDOWN_PROMPT:
                this.leaveShutdownPromptState();
                break;
        }
        ShutdownMonitor.sLogger.v(String.format("setting monitor state to %s", mMonitorState));
        this.mMonitorState = mMonitorState;
        this.mStateChangeTime = this.getCurrentTimeMsecs();
    }
    
    private void updateStats() {
        final long currentTimeMsecs = this.getCurrentTimeMsecs();
        final boolean dialConnected = this.mDialManager.isDialConnected();
        if (dialConnected && !this.mIsDialConnected) {
            this.mDialLastActivityTimeMsecs = currentTimeMsecs;
        }
        this.mIsDialConnected = dialConnected;
    }
    
    public void disableInactivityShutdown(final boolean mInactivityShutdownDisabled) {
        this.mInactivityShutdownDisabled = mInactivityShutdownDisabled;
    }
    
    public void disableScreenDim(final boolean mScreenDimmingDisabled) {
        this.mScreenDimmingDisabled = mScreenDimmingDisabled;
        this.recordInputEvent();
        final Logger sLogger = ShutdownMonitor.sLogger;
        String s;
        if (mScreenDimmingDisabled) {
            s = "disabled";
        }
        else {
            s = "enabled";
        }
        sLogger.v(String.format("screen dimming %s", s));
    }
    
    public void recordInputEvent() {
        this.mLastInputEventTimeMsecs = this.getCurrentTimeMsecs();
        if (this.mMonitorState == MonitorState.STATE_PENDING_SHUTDOWN) {
            this.enterActiveUseState();
        }
    }
    
    @Override
    public void run() {
        TaskManager.getInstance().execute(new Runnable() {
            @Override
            public void run() {
                ShutdownMonitor.this.updateStats();
                ShutdownMonitor.this.mHandler.postDelayed((Runnable)ShutdownMonitor.this, ShutdownMonitor.this.evaluateState());
            }
        }, 10);
    }
    
    private class MessageReceiver extends Thread
    {
        public MessageReceiver() {
            super("ShutdownMonitor MessageReceiver thread");
        }
        
        @Override
        public void run() {
            final byte[] array = new byte[100];
            final Charset forName = Charset.forName("US-ASCII");
            int n = 0;
            String string = "";
            while (true) {
                int read;
                final int n2 = read = -1;
                int n3 = n;
                Label_0164: {
                    Label_0185: {
                        while (true) {
                            try {
                                final InputStream access$000 = ShutdownMonitor.this.getSocketInputStream();
                                read = n2;
                                if (access$000 != null) {
                                    final int n4 = 0;
                                    final int n5 = 0;
                                    read = n2;
                                    n3 = n4;
                                    final int n6 = read = access$000.read(array);
                                    n = n5;
                                    if (n6 < 0) {
                                        read = n6;
                                        n3 = n4;
                                        ShutdownMonitor.sLogger.e("end of file reading shutdownd socket");
                                        read = n6;
                                        n3 = n4;
                                        ShutdownMonitor.this.closeSocketConnection();
                                        n = n5;
                                        read = n6;
                                    }
                                }
                                if (read >= 0) {
                                    break Label_0185;
                                }
                                if (++n > 3) {
                                    ShutdownMonitor.sLogger.e("MessageReceiver thread terminating");
                                    return;
                                }
                            }
                            catch (Exception ex) {
                                ShutdownMonitor.sLogger.e("exception reading shutdownd socket", ex);
                                ShutdownMonitor.this.closeSocketConnection();
                                n = n3;
                                continue;
                            }
                            break;
                        }
                        break Label_0164;
                        while (true) {
                        Label_0270_Outer:
                            while (true) {
                                String s = null;
                            Label_0362:
                                while (true) {
                                    Label_0355: {
                                        try {
                                            string += new String(array, 0, read, forName);
                                            while (string.contains("\n")) {
                                                final String[] split = string.split("\n", 2);
                                                s = split[0];
                                                if (split.length <= 1) {
                                                    break Label_0355;
                                                }
                                                string = split[1];
                                                ShutdownMonitor.sLogger.v("got command[" + s + "]");
                                                if (!"powerclick".equals(s)) {
                                                    break Label_0362;
                                                }
                                                ShutdownMonitor.this.mInputManager.injectKey(InputManager.CustomKeyEvent.POWER_BUTTON_CLICK);
                                            }
                                            break;
                                        }
                                        catch (Exception ex2) {
                                            ShutdownMonitor.sLogger.e("exception while processing data from shutdownd", ex2);
                                            string = "";
                                            break;
                                        }
                                    }
                                    string = "";
                                    continue;
                                }
                                if ("powerdoubleclick".equals(s)) {
                                    ShutdownMonitor.this.mInputManager.injectKey(InputManager.CustomKeyEvent.POWER_BUTTON_DOUBLE_CLICK);
                                    continue Label_0270_Outer;
                                }
                                if ("powerlongpress".equals(s)) {
                                    ShutdownMonitor.this.mInputManager.injectKey(InputManager.CustomKeyEvent.POWER_BUTTON_LONG_PRESS);
                                    continue Label_0270_Outer;
                                }
                                if (s.startsWith("powerstate")) {
                                    final String trim = s.substring("powerstate".length() + 1).trim();
                                    ShutdownMonitor.sLogger.d("State : " + trim);
                                    final boolean b = trim.charAt(0) != '0';
                                    final boolean b2 = trim.charAt(1) != '0';
                                    ShutdownMonitor.sLogger.d("USB on : " + b + " , Main on : " + b2);
                                    if (!b && !b2) {
                                        if (ShutdownMonitor.this.mMainPowerOn || ShutdownMonitor.this.mUsbPowerOn) {
                                            ShutdownMonitor.this.powerLossRunnable = new Runnable() {
                                                @Override
                                                public void run() {
                                                    if (!ShutdownMonitor.this.mMainPowerOn && !ShutdownMonitor.this.mUsbPowerOn) {
                                                        ShutdownMonitor.this.bus.post(new Shutdown(Shutdown.Reason.POWER_LOSS));
                                                    }
                                                }
                                            };
                                            ShutdownMonitor.this.mHandler.postDelayed(ShutdownMonitor.this.powerLossRunnable, ShutdownMonitor.POWER_DISCONECT_SMOOTHING_TIMEOUT);
                                        }
                                    }
                                    else if (!ShutdownMonitor.this.mMainPowerOn) {
                                        if (!ShutdownMonitor.this.mUsbPowerOn && ShutdownMonitor.this.powerLossRunnable != null) {
                                            ShutdownMonitor.this.mHandler.removeCallbacks(ShutdownMonitor.this.powerLossRunnable);
                                            ShutdownMonitor.this.powerLossRunnable = null;
                                        }
                                        ShutdownMonitor.this.mObdManager.updateListener();
                                    }
                                    ShutdownMonitor.this.mMainPowerOn = b2;
                                    ShutdownMonitor.this.mUsbPowerOn = b;
                                    continue Label_0270_Outer;
                                }
                                ShutdownMonitor.sLogger.e("invalid command from shutdownd socket: " + s);
                                continue Label_0270_Outer;
                            }
                        }
                    }
                    continue;
                }
                final long n7 = n * 10000;
                try {
                    Thread.sleep(n7);
                }
                catch (InterruptedException ex3) {}
            }
        }
    }
    
    private enum MonitorState
    {
        STATE_ACTIVE_USE, 
        STATE_BOOT, 
        STATE_OBD_ACTIVE, 
        STATE_PENDING_SHUTDOWN, 
        STATE_QUIET_MODE, 
        STATE_SHUTDOWN_PROMPT;
    }
    
    private class NotificationReceiver
    {
        private void triggerShutdown(final Shutdown shutdown) {
            TaskManager.getInstance().execute(new ShutdownRunnable(shutdown), 1);
        }
        
        @Subscribe
        public void ObdStateChangeEvent(final ObdManager.ObdConnectionStatusEvent obdConnectionStatusEvent) {
            if (obdConnectionStatusEvent.connected) {
                ShutdownMonitor.this.mObdDisconnectTimeMsecs = 0L;
                ShutdownMonitor.this.mAccelerateTime = 0L;
            }
            else {
                ShutdownMonitor.this.mObdDisconnectTimeMsecs = ShutdownMonitor.this.getCurrentTimeMsecs();
                ShutdownMonitor.this.mHandler.removeCallbacks((Runnable)ShutdownMonitor.this);
                ShutdownMonitor.this.mHandler.postDelayed((Runnable)ShutdownMonitor.this, ShutdownMonitor.OBD_DISCONNECT_TIMEOUT);
            }
        }
        
        @Subscribe
        public void onAccelerateShutdown(final AccelerateShutdown accelerateShutdown) {
            ShutdownMonitor.sLogger.v(String.format("received AccelerateShutdown, reason = %s", accelerateShutdown.reason));
            ShutdownMonitor.this.mAccelerateTime = ShutdownMonitor.this.getCurrentTimeMsecs();
        }
        
        @Subscribe
        public void onConnectionStateChange(final ConnectionStateChange connectionStateChange) {
            if (connectionStateChange.state == ConnectionStateChange.ConnectionState.CONNECTION_VERIFIED) {
                ShutdownMonitor.this.mRemoteDeviceConnectTimeMsecs = ShutdownMonitor.this.getCurrentTimeMsecs();
                ShutdownMonitor.this.mAccelerateTime = 0L;
                ShutdownMonitor.this.onWakeEvent();
            }
            else if (connectionStateChange.state == ConnectionStateChange.ConnectionState.CONNECTION_DISCONNECTED) {
                ShutdownMonitor.this.mRemoteDeviceConnectTimeMsecs = 0L;
            }
        }
        
        @Subscribe
        public void onDrivingStateChange(final DrivingStateChange drivingStateChange) {
            ShutdownMonitor.this.mDriving = drivingStateChange.driving;
            if (ShutdownMonitor.this.mDriving) {
                ShutdownMonitor.this.onMovement();
            }
        }
        
        @Subscribe
        public void onShutdown(final Shutdown shutdown) {
            switch (shutdown.state) {
                case CANCELED:
                    ShutdownMonitor.this.enterActiveUseState();
                    break;
                case CONFIRMED:
                    ShutdownMonitor.this.mHandler.post((Runnable)new Runnable() {
                        @Override
                        public void run() {
                            NotificationReceiver.this.triggerShutdown(shutdown);
                        }
                    });
                    break;
            }
        }
        
        @Subscribe
        public void onWakeup(final Wakeup wakeup) {
            ShutdownMonitor.this.enterBootState();
        }
        
        private class ShutdownRunnable implements Runnable
        {
            private final Shutdown event;
            
            ShutdownRunnable(final Shutdown event) {
                this.event = event;
            }
            
            @Override
            public void run() {
                final Shutdown.Reason reason = this.event.reason;
                HudApplication.getApplication().setShutdownReason(reason);
                boolean b = !ShutdownMonitor.this.powerManager.quietModeEnabled();
                final double batteryVoltage = ShutdownMonitor.this.mObdManager.getBatteryVoltage();
                final boolean autoOnEnabled = ShutdownMonitor.this.mObdManager.getObdDeviceConfigurationManager().isAutoOnEnabled();
                final boolean b2 = batteryVoltage >= 13.100000381469727;
                switch (reason) {
                    case POWER_LOSS:
                    case CRITICAL_VOLTAGE:
                    case LOW_VOLTAGE:
                    case TIMEOUT:
                    case HIGH_TEMPERATURE:
                        b = true;
                        break;
                }
                ShutdownMonitor.sLogger.i("Shutting down, auto-on:" + autoOnEnabled + " voltage:" + batteryVoltage + " charging:" + b2);
                if (autoOnEnabled && !b2) {
                    if (ShutdownMonitor.this.mObdManager.isSleeping()) {
                        ShutdownMonitor.this.mObdManager.wakeup();
                        GenericUtil.sleep(1000);
                    }
                    ShutdownMonitor.this.mObdManager.sleep(b);
                }
                else {
                    b = true;
                }
                ShutdownMonitor.this.powerManager.androidShutdown(reason, b);
            }
        }
    }
}
