package com.navdy.hud.app.service.pandora.exceptions;

public class UnsupportedMessageReceivedException extends Exception
{
    public UnsupportedMessageReceivedException(final byte b) {
        super("Message received from Pandora is not supported: " + String.format("%02X", b));
    }
}
