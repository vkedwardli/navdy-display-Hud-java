package com.navdy.hud.app.service;

import com.navdy.hud.app.obd.ObdManager;
import mortar.Mortar;
import android.app.PendingIntent;
import android.os.SystemClock;
import android.app.AlarmManager;
import android.content.Intent;
import com.navdy.hud.app.HudApplication;
import java.util.Iterator;
import java.util.ArrayList;
import java.io.File;
import com.navdy.service.library.log.Logger;
import java.util.concurrent.atomic.AtomicBoolean;

public class ObdCANBusDataUploadService extends S3FileUploadService
{
    private static final int MAX_FILES_TO_UPLOAD = 5;
    public static final String NAVDY_GESTURE_VIDEOS_BUCKET = "navdy-obd-data";
    private static AtomicBoolean isUploading;
    private static boolean mIsInitialized;
    private static Request sCurrentRequest;
    private static Logger sLogger;
    private static String sObdDatFilesFolder;
    private static final UploadQueue sObdDataFilesUploadQueue;
    
    static {
        ObdCANBusDataUploadService.sLogger = new Logger(ObdCANBusDataUploadService.class);
        sObdDataFilesUploadQueue = new UploadQueue();
        ObdCANBusDataUploadService.isUploading = new AtomicBoolean(false);
        ObdCANBusDataUploadService.mIsInitialized = false;
    }
    
    public ObdCANBusDataUploadService() {
        super(ObdCANBusDataUploadService.class.getName());
    }
    
    public static void addObdDataFileToQueue(final File file) {
        initializeIfNecessary(file);
        final UploadQueue sObdDataFilesUploadQueue = ObdCANBusDataUploadService.sObdDataFilesUploadQueue;
        synchronized (sObdDataFilesUploadQueue) {
            ObdCANBusDataUploadService.sObdDataFilesUploadQueue.add(new Request(file, ""));
            ObdCANBusDataUploadService.sLogger.d("Queue size : " + ObdCANBusDataUploadService.sObdDataFilesUploadQueue.size());
            if (!ObdCANBusDataUploadService.isUploading.get()) {
                while (ObdCANBusDataUploadService.sObdDataFilesUploadQueue.size() > 5) {
                    ObdCANBusDataUploadService.sObdDataFilesUploadQueue.pop();
                }
            }
        }
    }
    // monitorexit(uploadQueue)
    
    private static void initializeIfNecessary() {
        initializeIfNecessary(null);
    }
    
    private static void initializeIfNecessary(final File file) {
        final UploadQueue sObdDataFilesUploadQueue = ObdCANBusDataUploadService.sObdDataFilesUploadQueue;
        Label_0167: {
            final ArrayList<File> list;
            synchronized (sObdDataFilesUploadQueue) {
                if (ObdCANBusDataUploadService.mIsInitialized) {
                    break Label_0167;
                }
                ObdCANBusDataUploadService.sLogger.d("Not initialized , initializing now");
                if (ObdCANBusDataUploadService.mIsInitialized) {
                    break Label_0167;
                }
                ObdCANBusDataUploadService.sObdDatFilesFolder = "/sdcard/.canBusLogs/upload";
                final File[] listFiles = new File(ObdCANBusDataUploadService.sObdDatFilesFolder).listFiles();
                list = new ArrayList<File>();
                for (final File file2 : listFiles) {
                    if (file2.isFile() && file2.getName().endsWith(".zip")) {
                        ObdCANBusDataUploadService.sLogger.d("ObdCanBus Data bundle :" + file2);
                        list.add(file2);
                    }
                }
            }
            populateFilesQueue(list, ObdCANBusDataUploadService.sObdDataFilesUploadQueue, 5);
            ObdCANBusDataUploadService.mIsInitialized = true;
        }
    }
    // monitorexit(uploadQueue)
    
    public static void populateFilesQueue(final ArrayList<File> list, final UploadQueue uploadQueue, final int n) {
        if (list != null) {
            for (final File file : list) {
                if (file.isFile()) {
                    uploadQueue.add(new Request(file, null));
                    if (uploadQueue.size() != n) {
                        continue;
                    }
                    uploadQueue.pop();
                }
            }
        }
    }
    
    public static void scheduleWithDelay(final long n) {
        final Intent intent = new Intent(HudApplication.getAppContext(), (Class)ObdCANBusDataUploadService.class);
        intent.setAction("SYNC");
        ((AlarmManager)HudApplication.getAppContext().getSystemService("alarm")).setExact(3, SystemClock.elapsedRealtime() + n, PendingIntent.getService(HudApplication.getAppContext(), 123, intent, 268435456));
    }
    
    public static void syncNow() {
        ObdCANBusDataUploadService.sLogger.d("synNow");
        final Intent intent = new Intent(HudApplication.getAppContext(), (Class)ObdCANBusDataUploadService.class);
        intent.setAction("SYNC");
        HudApplication.getAppContext().startService(intent);
    }
    
    @Override
    public boolean canCompleteRequest(final Request request) {
        return true;
    }
    
    @Override
    protected String getAWSBucket() {
        return "navdy-obd-data";
    }
    
    @Override
    protected Request getCurrentRequest() {
        return ObdCANBusDataUploadService.sCurrentRequest;
    }
    
    @Override
    protected AtomicBoolean getIsUploading() {
        return ObdCANBusDataUploadService.isUploading;
    }
    
    @Override
    protected String getKeyPrefix(final File file) {
        return "CANBus";
    }
    
    @Override
    protected Logger getLogger() {
        return ObdCANBusDataUploadService.sLogger;
    }
    
    @Override
    protected UploadQueue getUploadQueue() {
        return ObdCANBusDataUploadService.sObdDataFilesUploadQueue;
    }
    
    @Override
    protected void initialize() {
        initializeIfNecessary();
    }
    
    @Override
    public void onCreate() {
        Mortar.inject(HudApplication.getAppContext(), this);
        super.onCreate();
    }
    
    @Override
    public void reSchedule() {
        ObdCANBusDataUploadService.sLogger.d("reschedule");
        scheduleWithDelay(10000L);
    }
    
    @Override
    protected void setCurrentRequest(final Request sCurrentRequest) {
        ObdCANBusDataUploadService.sCurrentRequest = sCurrentRequest;
    }
    
    @Override
    public void sync() {
        syncNow();
    }
    
    @Override
    protected void uploadFinished(final boolean b, final String s, final String s2) {
        if (b) {
            ObdManager.getInstance().getObdCanBusRecordingPolicy().onFileUploaded(new File(s));
        }
        else {
            ObdCANBusDataUploadService.sLogger.e("File upload failed " + s);
        }
    }
}
