package com.navdy.hud.app.service.pandora.messages;

import java.io.ByteArrayOutputStream;
import com.navdy.hud.app.service.pandora.exceptions.MessageWrongWayException;
import com.navdy.hud.app.service.pandora.exceptions.UnexpectedEndOfStringException;
import com.navdy.hud.app.service.pandora.exceptions.StringOverflowException;
import java.io.IOException;

public class GetTrackAlbumArt extends BaseOutgoingConstantMessage
{
    public static final GetTrackAlbumArt INSTANCE;
    
    static {
        INSTANCE = new GetTrackAlbumArt();
    }
    
    @Override
    protected ByteArrayOutputStream putThis(final ByteArrayOutputStream byteArrayOutputStream) throws IOException, StringOverflowException, UnexpectedEndOfStringException, MessageWrongWayException {
        BaseOutgoingMessage.putByte(byteArrayOutputStream, (byte)20);
        BaseOutgoingMessage.putInt(byteArrayOutputStream, 9025);
        return byteArrayOutputStream;
    }
    
    @Override
    public String toString() {
        return "Requesting album artwork";
    }
}
