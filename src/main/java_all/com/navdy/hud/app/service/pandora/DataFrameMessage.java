package com.navdy.hud.app.service.pandora;

import java.nio.ByteBuffer;

public class DataFrameMessage extends FrameMessage
{
    public DataFrameMessage(final boolean b, final byte[] array) throws IllegalArgumentException {
        super(b, array);
        if (array.length == 0) {
            throw new IllegalArgumentException("Empty payload in data frame is not allowed");
        }
    }
    
    protected static DataFrameMessage parseDataFrame(byte[] array) throws IllegalArgumentException {
        if (array[0] != 126 || array[array.length - 1] != 124 || array[1] != 0) {
            throw new IllegalArgumentException("Data frame start/stop/type bytes not in place");
        }
        final ByteBuffer unescapeBytes = FrameMessage.unescapeBytes(array);
        unescapeBytes.position(2);
        final byte value = unescapeBytes.get();
        boolean b;
        if (value == 0) {
            b = true;
        }
        else {
            if (value != 1) {
                throw new IllegalArgumentException("Unknown message frame sequence: " + String.format("%02X", value));
            }
            b = false;
        }
        final int int1 = unescapeBytes.getInt();
        if (int1 == 0) {
            throw new IllegalArgumentException("Data frame has 0 as payload's length");
        }
        final byte[] array2 = new byte[int1];
        unescapeBytes.get(array2);
        array = new byte[2];
        unescapeBytes.get(array);
        if (unescapeBytes.remaining() != 1) {
            throw new IllegalArgumentException("Corrupted message frame");
        }
        final byte[] array3 = new byte[int1 + 6];
        unescapeBytes.position(1);
        unescapeBytes.get(array3);
        if (!CRC16CCITT.checkCRC(array3, array)) {
            throw new IllegalArgumentException("CRC fail");
        }
        return new DataFrameMessage(b, array2);
    }
    
    @Override
    public byte[] buildFrame() {
        final byte b = 0;
        final int length = this.payload.length;
        final ByteBuffer put = ByteBuffer.allocate(length + 6).put((byte)0);
        byte b2;
        if (this.isSequence0) {
            b2 = b;
        }
        else {
            b2 = 1;
        }
        return FrameMessage.buildFrameFromCRCPart(put.put(b2).putInt(length).put(this.payload));
    }
}
