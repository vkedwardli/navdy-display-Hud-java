package com.navdy.hud.app.service.pandora.messages;

import com.navdy.hud.app.service.pandora.exceptions.CorruptedPayloadException;
import com.navdy.hud.app.service.pandora.exceptions.MessageWrongWayException;
import com.navdy.hud.app.service.pandora.exceptions.UnterminatedStringException;
import com.navdy.hud.app.service.pandora.exceptions.StringOverflowException;
import java.nio.ByteBuffer;

public class ReturnTrackInfoExtended extends BaseIncomingMessage
{
    public String album;
    public int albumArtLength;
    public String artist;
    public short duration;
    public short elapsed;
    public byte identityFlags;
    public byte permissionFlags;
    public byte rating;
    public String title;
    public int trackToken;
    
    protected static BaseIncomingMessage innerBuildFromPayload(final byte[] array) throws MessageWrongWayException, CorruptedPayloadException {
        final ByteBuffer wrap = ByteBuffer.wrap(array);
        wrap.get();
        final ReturnTrackInfoExtended returnTrackInfoExtended = new ReturnTrackInfoExtended();
        returnTrackInfoExtended.trackToken = wrap.getInt();
        returnTrackInfoExtended.albumArtLength = wrap.getInt();
        returnTrackInfoExtended.duration = wrap.getShort();
        returnTrackInfoExtended.elapsed = wrap.getShort();
        returnTrackInfoExtended.rating = wrap.get();
        returnTrackInfoExtended.permissionFlags = wrap.get();
        returnTrackInfoExtended.identityFlags = wrap.get();
        try {
            returnTrackInfoExtended.title = BaseIncomingMessage.getString(wrap);
            returnTrackInfoExtended.artist = BaseIncomingMessage.getString(wrap);
            returnTrackInfoExtended.album = BaseIncomingMessage.getString(wrap);
            return returnTrackInfoExtended;
        }
        catch (StringOverflowException ex) {}
        catch (UnterminatedStringException ex2) {
            goto Label_0101;
        }
    }
    
    @Override
    public String toString() {
        return "Got extended track's info for track with token: " + this.trackToken;
    }
}
