package com.navdy.hud.app.service;

import com.navdy.service.library.events.NavdyEventUtil;
import com.navdy.hud.app.event.ShowScreenWithArgs;
import com.navdy.hud.app.screen.WelcomeScreen;
import android.os.Bundle;
import com.navdy.service.library.events.ui.Screen;
import com.navdy.service.library.events.ui.ShowScreen;
import com.navdy.hud.app.device.dial.DialManager;
import com.navdy.hud.app.event.Wakeup;
import com.navdy.hud.app.event.Shutdown;
import com.navdy.service.library.events.connection.NetworkLinkReady;
import com.navdy.hud.app.framework.phonecall.CallUtils;
import com.navdy.hud.app.event.LocalSpeechRequest;
import com.navdy.service.library.events.connection.LinkPropertiesChanged;
import com.navdy.hud.app.util.OTAUpdateService;
import com.navdy.hud.app.event.DriverProfileChanged;
import com.navdy.hud.app.event.Disconnect;
import com.navdy.hud.app.event.DeviceInfoAvailable;
import com.navdy.service.library.events.preferences.LocalPreferences;
import com.navdy.hud.app.framework.DriverProfileHelper;
import java.util.TimeZone;
import android.app.AlarmManager;
import com.navdy.hud.app.util.DeviceUtil;
import com.navdy.service.library.events.settings.DateTimeConfiguration;
import android.os.SystemClock;
import android.text.TextUtils;
import com.navdy.service.library.events.connection.ConnectionStateChange;
import com.squareup.otto.Subscribe;
import com.navdy.service.library.device.RemoteDevice;
import com.navdy.service.library.events.debug.StartDriveRecordingEvent;
import com.navdy.service.library.events.connection.ConnectionRequest;
import com.navdy.service.library.events.audio.NowPlayingUpdateRequest;
import com.navdy.hud.app.event.RemoteEvent;
import com.navdy.service.library.events.callcontrol.CallStateUpdateRequest;
import com.navdy.service.library.events.preferences.NotificationPreferences;
import com.navdy.hud.app.framework.notifications.NotificationManager;
import com.navdy.hud.app.util.picasso.PicassoUtil;
import com.navdy.hud.app.analytics.AnalyticsSupport;
import com.navdy.hud.app.profile.DriverProfile;
import com.navdy.hud.app.framework.network.NetworkStateManager;
import com.squareup.wire.Message;
import android.os.Build;
import com.navdy.service.library.Version;
import com.navdy.service.library.device.NavdyDeviceId;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import android.os.Build;
import android.os.Looper;
import com.navdy.hud.app.ui.component.UISettings;
import java.util.ArrayList;
import com.navdy.hud.app.ui.framework.UIStateManager;
import com.navdy.hud.app.common.TimeHelper;
import com.navdy.hud.app.device.PowerManager;
import android.os.Handler;
import com.navdy.hud.app.profile.DriverProfileManager;
import com.navdy.service.library.events.DeviceInfo;
import android.content.Context;
import com.squareup.otto.Bus;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.events.LegacyCapability;
import java.util.List;
import com.navdy.service.library.events.Capabilities;

public class ConnectionHandler
{
    static Capabilities CAPABILITIES;
    private static final DeviceSyncEvent DEVICE_FULL_SYNC_EVENT;
    private static final DeviceSyncEvent DEVICE_MINIMAL_SYNC_EVENT;
    public static final int IOS_APP_LAUNCH_TIMEOUT = 5000;
    private static List<LegacyCapability> LEGACY_CAPABILITIES;
    private static final int LINK_LOST_TIME_OUT_AFTER_DISCONNECTED = 1000;
    private static final int REDOWNLOAD_THRESHOLD = 30000;
    private static final Logger sLogger;
    private Bus bus;
    protected Context context;
    protected boolean disconnecting;
    private volatile boolean forceFullUpdate;
    private volatile boolean isNetworkLinkReady;
    private String lastConnectedDeviceId;
    protected DeviceInfo lastConnectedDeviceInfo;
    private long lastDisconnectTime;
    private boolean mAppClosed;
    private boolean mAppLaunchAttempted;
    private Runnable mAppLaunchAttemptedRunnable;
    protected CustomNotificationServiceHandler mCustomNotificationServiceHandler;
    protected Runnable mDisconnectRunnable;
    protected DriverProfileManager mDriverProfileManager;
    protected Handler mHandler;
    private PowerManager mPowerManager;
    protected Runnable mReconnectTimedOutRunnable;
    protected RemoteDeviceProxy mRemoteDevice;
    protected ConnectionServiceProxy proxy;
    private TimeHelper timeHelper;
    private volatile boolean triggerDownload;
    private UIStateManager uiStateManager;
    
    static {
        sLogger = new Logger(ConnectionHandler.class);
        (ConnectionHandler.LEGACY_CAPABILITIES = new ArrayList<LegacyCapability>()).add(LegacyCapability.CAPABILITY_VOICE_SEARCH);
        ConnectionHandler.LEGACY_CAPABILITIES.add(LegacyCapability.CAPABILITY_COMPACT_UI);
        ConnectionHandler.LEGACY_CAPABILITIES.add(LegacyCapability.CAPABILITY_LOCAL_MUSIC_BROWSER);
        ConnectionHandler.CAPABILITIES = new Capabilities.Builder().compactUi(true).localMusicBrowser(true).voiceSearch(true).voiceSearchNewIOSPauseBehaviors(true).musicArtworkCache(true).searchResultList(true).cannedResponseToSms(UISettings.supportsIosSms()).customDialLongPress(true).build();
        DEVICE_FULL_SYNC_EVENT = new DeviceSyncEvent(1);
        DEVICE_MINIMAL_SYNC_EVENT = new DeviceSyncEvent(0);
    }
    
    public ConnectionHandler(final Context context, final ConnectionServiceProxy proxy, final PowerManager mPowerManager, final DriverProfileManager mDriverProfileManager, final UIStateManager uiStateManager, final TimeHelper timeHelper) {
        this.forceFullUpdate = false;
        this.isNetworkLinkReady = false;
        this.triggerDownload = true;
        this.mAppClosed = true;
        this.mAppLaunchAttempted = false;
        this.context = context;
        this.proxy = proxy;
        this.mPowerManager = mPowerManager;
        (this.bus = this.proxy.getBus()).register(this);
        (this.mCustomNotificationServiceHandler = new CustomNotificationServiceHandler(this.bus)).start();
        this.mDriverProfileManager = mDriverProfileManager;
        this.uiStateManager = uiStateManager;
        this.timeHelper = timeHelper;
        ConnectionHandler.sLogger.i("Creating connectionHandler:" + this);
        this.mHandler = new Handler(Looper.getMainLooper());
        this.mDisconnectRunnable = new Runnable() {
            @Override
            public void run() {
                ConnectionHandler.this.handleDisconnectWithoutLinkLoss();
            }
        };
        this.mReconnectTimedOutRunnable = new Runnable() {
            @Override
            public void run() {
                ConnectionHandler.this.sendConnectionNotification();
            }
        };
        this.mAppLaunchAttemptedRunnable = new Runnable() {
            @Override
            public void run() {
                ConnectionHandler.sLogger.d("No response after the timeout after App launch attempt");
                ConnectionHandler.this.sendConnectionNotification();
            }
        };
    }
    
    private String getSystemVersion() {
        String incremental;
        if (!String.valueOf(3049).equals(Build$VERSION.INCREMENTAL)) {
            incremental = "1.3.3051-corona";
        }
        else {
            incremental = Build$VERSION.INCREMENTAL;
        }
        return incremental;
    }
    
    private void handleDisconnectWithoutLinkLoss() {
        final DeviceInfo.Platform remoteDevicePlatform = RemoteDeviceManager.getInstance().getRemoteDevicePlatform();
        if (remoteDevicePlatform != null && remoteDevicePlatform.equals(DeviceInfo.Platform.PLATFORM_iOS)) {
            this.sendConnectionNotification();
        }
    }
    
    private void onConnect(NavdyDeviceId thisDevice) {
        if (this.mRemoteDevice == null) {
            this.mRemoteDevice = new RemoteDeviceProxy(this.proxy, this.context, thisDevice);
        }
        this.mDriverProfileManager.loadProfileForId(thisDevice);
        thisDevice = NavdyDeviceId.getThisDevice(this.context);
        final DeviceInfo build = new DeviceInfo.Builder().deviceId(thisDevice.toString()).clientVersion("1.3.3051-corona").protocolVersion(Version.PROTOCOL_VERSION.toString()).deviceName(thisDevice.getDeviceName()).systemVersion(this.getSystemVersion()).model(Build.MODEL).deviceUuid(Build.SERIAL).systemApiLevel(Build$VERSION.SDK_INT).kernelVersion(System.getProperty("os.version")).platform(DeviceInfo.Platform.PLATFORM_Android).buildType(Build.TYPE).deviceMake("Navdy").forceFullUpdate(this.forceFullUpdate).legacyCapabilities(ConnectionHandler.LEGACY_CAPABILITIES).capabilities(ConnectionHandler.CAPABILITIES).build();
        ConnectionHandler.sLogger.d("onConnect - sending device info: " + build);
        this.mRemoteDevice.postEvent(build);
    }
    
    private void onDisconnect(final NavdyDeviceId navdyDeviceId) {
        synchronized (this) {
            ConnectionHandler.sLogger.v("onDisconnect:" + navdyDeviceId);
            this.isNetworkLinkReady = false;
            NetworkStateManager.getInstance().networkNotAvailable();
            this.mDriverProfileManager.setCurrentProfile(null);
            this.mHandler.postDelayed(this.mDisconnectRunnable, 1000L);
        }
    }
    
    private void onLinkEstablished(final NavdyDeviceId navdyDeviceId) {
        this.mPowerManager.wakeUp(AnalyticsSupport.WakeupReason.PHONE);
        if (this.mRemoteDevice != null) {}
        this.mRemoteDevice = new RemoteDeviceProxy(this.proxy, this.context, navdyDeviceId);
        this.mHandler.removeCallbacks(this.mReconnectTimedOutRunnable);
    }
    
    private void onLinkLost(final NavdyDeviceId navdyDeviceId) {
        int n = 1;
        synchronized (this) {
            ConnectionHandler.sLogger.v("onLinkLost:" + navdyDeviceId);
            this.isNetworkLinkReady = false;
            this.mHandler.removeCallbacks(this.mDisconnectRunnable);
            this.mHandler.removeCallbacks(this.mReconnectTimedOutRunnable);
            if (this.mRemoteDevice == null) {
                n = 0;
            }
            this.mRemoteDevice = null;
            PicassoUtil.clearCache();
            if (n != 0) {
                NotificationManager.getInstance().handleDisconnect(true);
                if (!this.disconnecting) {
                    this.mHandler.postDelayed(this.mReconnectTimedOutRunnable, 15000L);
                }
            }
            this.disconnecting = false;
        }
    }
    
    private void requestIAPUpdateBasedOnPreference(final NotificationPreferences notificationPreferences) {
        if (notificationPreferences != null) {
            if (Boolean.TRUE.equals(notificationPreferences.enabled)) {
                this.bus.post(new RemoteEvent(new CallStateUpdateRequest(Boolean.valueOf(true))));
                this.bus.post(new RemoteEvent(new NowPlayingUpdateRequest(Boolean.valueOf(true))));
            }
            else {
                this.bus.post(new RemoteEvent(new CallStateUpdateRequest(Boolean.valueOf(false))));
                this.bus.post(new RemoteEvent(new NowPlayingUpdateRequest(Boolean.valueOf(false))));
            }
        }
    }
    
    public void connect() {
        this.proxy.connect();
    }
    
    public void connectToDevice(final NavdyDeviceId navdyDeviceId) {
        final ConnectionRequest.Action connection_SELECT = ConnectionRequest.Action.CONNECTION_SELECT;
        String string;
        if (navdyDeviceId != null) {
            string = navdyDeviceId.toString();
        }
        else {
            string = null;
        }
        this.sendLocalMessage(new ConnectionRequest(connection_SELECT, string));
    }
    
    public void disconnect() {
        if (this.mRemoteDevice != null) {
            this.lastConnectedDeviceInfo = null;
            this.disconnecting = true;
            this.connectToDevice(null);
        }
    }
    
    public NavdyDeviceId getConnectedDevice() {
        NavdyDeviceId deviceId;
        if (this.mRemoteDevice != null) {
            deviceId = this.mRemoteDevice.getDeviceId();
        }
        else {
            deviceId = null;
        }
        return deviceId;
    }
    
    public StartDriveRecordingEvent getDriverRecordingEvent() {
        StartDriveRecordingEvent driverRecordingEvent;
        if (this.proxy != null) {
            driverRecordingEvent = this.proxy.getDriverRecordingEvent();
        }
        else {
            driverRecordingEvent = null;
        }
        return driverRecordingEvent;
    }
    
    public DeviceInfo getLastConnectedDeviceInfo() {
        return this.lastConnectedDeviceInfo;
    }
    
    public RemoteDevice getRemoteDevice() {
        return this.mRemoteDevice;
    }
    
    public boolean isAppClosed() {
        return this.mAppClosed;
    }
    
    public boolean isAppLaunchAttempted() {
        return this.mAppLaunchAttempted;
    }
    
    public boolean isNetworkLinkReady() {
        return this.isNetworkLinkReady;
    }
    
    @Subscribe
    public void onApplicationLaunchAttempted(final ApplicationLaunchAttempted applicationLaunchAttempted) {
        ConnectionHandler.sLogger.d("Attempting to launch the App, Will wait and see");
        if (this.mAppClosed) {
            this.mAppLaunchAttempted = true;
            this.mHandler.removeCallbacks(this.mAppLaunchAttemptedRunnable);
            this.mHandler.postDelayed(this.mAppLaunchAttemptedRunnable, 5000L);
        }
    }
    
    @Subscribe
    public void onConnectionStateChange(final ConnectionStateChange connectionStateChange) {
        ConnectionHandler.sLogger.d("ConnectionStateChange - " + connectionStateChange.state);
        final String remoteDeviceId = connectionStateChange.remoteDeviceId;
        NavdyDeviceId unknown_ID;
        if (TextUtils.isEmpty((CharSequence)remoteDeviceId)) {
            unknown_ID = NavdyDeviceId.UNKNOWN_ID;
        }
        else {
            unknown_ID = new NavdyDeviceId(remoteDeviceId);
        }
        this.mAppLaunchAttempted = false;
        this.mHandler.removeCallbacks(this.mAppLaunchAttemptedRunnable);
        switch (connectionStateChange.state) {
            case CONNECTION_LINK_ESTABLISHED:
                this.mAppClosed = true;
                this.onLinkEstablished(unknown_ID);
                break;
            case CONNECTION_CONNECTED:
                this.onConnect(unknown_ID);
                this.mAppClosed = false;
                break;
            case CONNECTION_DISCONNECTED:
                this.lastDisconnectTime = SystemClock.elapsedRealtime();
                this.mAppClosed = true;
                this.onDisconnect(unknown_ID);
                break;
            case CONNECTION_LINK_LOST:
                this.lastDisconnectTime = SystemClock.elapsedRealtime();
                this.mAppClosed = true;
                this.onLinkLost(unknown_ID);
                break;
        }
    }
    
    @Subscribe
    public void onDateTimeConfiguration(final DateTimeConfiguration clockConfiguration) {
        while (true) {
            ConnectionHandler.sLogger.i("Received timestamp from the client: " + clockConfiguration.timestamp + " " + clockConfiguration.timezone);
            while (true) {
                Label_0461: {
                    AlarmManager alarmManager = null;
                    TimeZone timeZone = null;
                    Label_0414: {
                        try {
                            if (DeviceUtil.isNavdyDevice()) {
                                alarmManager = (AlarmManager)this.context.getSystemService("alarm");
                                ConnectionHandler.sLogger.v("setting date timezone[" + clockConfiguration.timezone + "] time[" + clockConfiguration.timestamp + "]");
                                if (clockConfiguration.timezone == null) {
                                    break Label_0461;
                                }
                                if (TimeZone.getDefault().getID().equalsIgnoreCase(clockConfiguration.timezone)) {
                                    ConnectionHandler.sLogger.v("timezone already set to " + clockConfiguration.timezone);
                                }
                                else {
                                    timeZone = TimeZone.getTimeZone(clockConfiguration.timezone);
                                    if (timeZone.getID().equalsIgnoreCase(clockConfiguration.timezone)) {
                                        break Label_0414;
                                    }
                                    ConnectionHandler.sLogger.e("timezone not found on HUD:" + clockConfiguration.timezone);
                                }
                                ConnectionHandler.sLogger.v("setting time [" + clockConfiguration.timestamp + "]");
                                alarmManager.setTime((long)clockConfiguration.timestamp);
                                ConnectionHandler.sLogger.v("set time [" + clockConfiguration.timestamp + "]");
                                this.bus.post(TimeHelper.DATE_TIME_AVAILABLE_EVENT);
                                ConnectionHandler.sLogger.v("post date complete");
                            }
                            final DriverProfileManager driverProfileManager = DriverProfileHelper.getInstance().getDriverProfileManager();
                            driverProfileManager.getSessionPreferences().setClockConfiguration(clockConfiguration);
                            driverProfileManager.updateLocalPreferences(new LocalPreferences.Builder(driverProfileManager.getCurrentProfile().getLocalPreferences()).clockFormat(clockConfiguration.format).build());
                            return;
                        }
                        catch (Throwable t) {
                            ConnectionHandler.sLogger.e("Cannot update time on device", t);
                            return;
                        }
                    }
                    ConnectionHandler.sLogger.v("timezone found on HUD:" + timeZone.getID());
                    alarmManager.setTimeZone(clockConfiguration.timezone);
                    continue;
                }
                ConnectionHandler.sLogger.w("timezone not provided");
                continue;
            }
        }
    }
    
    @Subscribe
    public void onDeviceInfo(final DeviceInfo deviceInfo) {
        boolean b = false;
        ConnectionHandler.sLogger.i("Received deviceInfo:" + deviceInfo);
        if (this.mRemoteDevice != null) {
            this.triggerDownload = true;
            if (this.lastConnectedDeviceId != null) {
                final long n = SystemClock.elapsedRealtime() - this.lastDisconnectTime;
                if (n < 30000L) {
                    ConnectionHandler.sLogger.v("threshold met=" + n);
                    if (TextUtils.equals((CharSequence)this.lastConnectedDeviceId, (CharSequence)deviceInfo.deviceId)) {
                        this.triggerDownload = false;
                        ConnectionHandler.sLogger.v("same device connected:" + deviceInfo.deviceId);
                    }
                    else {
                        ConnectionHandler.sLogger.v("different device connected last[" + this.lastConnectedDeviceId + "] current[" + deviceInfo.deviceId + "]");
                        this.lastConnectedDeviceId = deviceInfo.deviceId;
                    }
                }
                else {
                    ConnectionHandler.sLogger.v("threshold not met=" + n);
                    this.lastConnectedDeviceId = deviceInfo.deviceId;
                }
            }
            else {
                ConnectionHandler.sLogger.v("no last device");
                this.lastConnectedDeviceId = deviceInfo.deviceId;
            }
            final String string = this.mRemoteDevice.getDeviceId().toString();
            final DeviceInfo build = new DeviceInfo.Builder(deviceInfo).deviceId(string).build();
            String deviceId;
            if (this.lastConnectedDeviceInfo != null) {
                deviceId = this.lastConnectedDeviceInfo.deviceId;
            }
            else {
                deviceId = null;
            }
            if (!string.equals(deviceId)) {
                b = true;
            }
            this.lastConnectedDeviceInfo = build;
            this.mRemoteDevice.setDeviceInfo(build);
            this.bus.post(new DeviceInfoAvailable(build));
            if (this.triggerDownload) {
                ConnectionHandler.sLogger.v("trigger device sync");
                this.bus.post(ConnectionHandler.DEVICE_FULL_SYNC_EVENT);
            }
            else if (build.platform == DeviceInfo.Platform.PLATFORM_iOS) {
                ConnectionHandler.sLogger.v("triggering minimal sync");
                this.bus.post(ConnectionHandler.DEVICE_MINIMAL_SYNC_EVENT);
            }
            else {
                ConnectionHandler.sLogger.v("don't trigger device sync");
            }
            if (b) {
                this.sendConnectionNotification();
            }
            else {
                NotificationManager.getInstance().handleConnect();
                NotificationManager.getInstance().hideConnectionToast(true);
                final long n2 = SystemClock.elapsedRealtime() - this.lastDisconnectTime;
                ConnectionHandler.sLogger.i("Tracking a reconnect that took " + n2 + "ms");
                AnalyticsSupport.recordPhoneDisconnect(true, String.valueOf(n2));
            }
        }
    }
    
    @Subscribe
    public void onDisconnect(final Disconnect disconnect) {
        this.disconnect();
    }
    
    @Subscribe
    public void onDriverProfileUpdated(final DriverProfileChanged driverProfileChanged) {
        final DriverProfile currentProfile = this.mDriverProfileManager.getCurrentProfile();
        NotificationPreferences notificationPreferences;
        if (currentProfile != null) {
            notificationPreferences = currentProfile.getNotificationPreferences();
        }
        else {
            notificationPreferences = null;
        }
        this.requestIAPUpdateBasedOnPreference(notificationPreferences);
    }
    
    @Subscribe
    public void onIncrementalOTAFailureDetected(final OTAUpdateService.IncrementalOTAFailureDetected incrementalOTAFailureDetected) {
        ConnectionHandler.sLogger.d("Incremental OTA failure detected, indicate the remote device to force full update");
        this.forceFullUpdate = true;
    }
    
    @Subscribe
    public void onLinkPropertiesChanged(final LinkPropertiesChanged linkPropertiesChanged) {
        if (linkPropertiesChanged != null && linkPropertiesChanged.bandwidthLevel != null && this.mRemoteDevice != null) {
            this.mRemoteDevice.setLinkBandwidthLevel(linkPropertiesChanged.bandwidthLevel);
        }
    }
    
    @Subscribe
    public void onLocalSpeechRequest(final LocalSpeechRequest localSpeechRequest) {
        if (!CallUtils.isPhoneCallInProgress()) {
            if (ConnectionHandler.sLogger.isLoggable(2)) {
                ConnectionHandler.sLogger.v("[tts-outbound] [" + localSpeechRequest.speechRequest.category.name() + "] [" + localSpeechRequest.speechRequest.words + "]");
            }
            this.onRemoteEvent(new RemoteEvent(localSpeechRequest.speechRequest));
        }
    }
    
    @Subscribe
    public void onNetworkLinkReady(final NetworkLinkReady networkLinkReady) {
        ConnectionHandler.sLogger.v("DummyNetNetworkFactory:onConnect");
        this.isNetworkLinkReady = true;
        NetworkStateManager.getInstance().networkAvailable();
        GestureVideosSyncService.scheduleWithDelay(30000L);
        ObdCANBusDataUploadService.scheduleWithDelay(45000L);
    }
    
    @Subscribe
    public void onNotificationPreference(final NotificationPreferences notificationPreferences) {
        this.requestIAPUpdateBasedOnPreference(notificationPreferences);
    }
    
    @Subscribe
    public void onRemoteEvent(final RemoteEvent remoteEvent) {
        if (this.mRemoteDevice != null) {
            this.mRemoteDevice.postEvent(remoteEvent.getMessage());
        }
    }
    
    @Subscribe
    public void onShutdown(final Shutdown shutdown) {
        if (shutdown.state == Shutdown.State.SHUTTING_DOWN) {
            this.shutdown();
        }
    }
    
    @Subscribe
    public void onWakeup(final Wakeup wakeup) {
        if (DialManager.getInstance().getBondedDialCount() == 0) {
            ConnectionHandler.sLogger.v("go to dial pairing screen");
            this.bus.post(new ShowScreen.Builder().screen(Screen.SCREEN_DIAL_PAIRING).build());
        }
        else if (this.mRemoteDevice == null) {
            ConnectionHandler.sLogger.v("go to welcome screen");
            final Bundle bundle = new Bundle();
            bundle.putString(WelcomeScreen.ARG_ACTION, WelcomeScreen.ACTION_RECONNECT);
            this.bus.post(new ShowScreenWithArgs(Screen.SCREEN_WELCOME, bundle, false));
        }
        else {
            this.bus.post(new ShowScreen.Builder().screen(this.uiStateManager.getDefaultMainActiveScreen()).build());
        }
    }
    
    public void searchForDevices() {
        this.sendLocalMessage(new ConnectionRequest(ConnectionRequest.Action.CONNECTION_START_SEARCH, null));
    }
    
    public void sendConnectionNotification() {
        final NotificationManager instance = NotificationManager.getInstance();
        if (RemoteDeviceManager.getInstance().isRemoteDeviceConnected()) {
            if (this.isAppClosed()) {
                instance.handleDisconnect(false);
                instance.showHideToast(false);
            }
            else {
                instance.handleConnect();
                instance.showHideToast(true);
            }
        }
        else {
            instance.handleDisconnect(true);
            instance.showHideToast(false);
        }
    }
    
    public void sendLocalMessage(final Message message) {
        try {
            this.proxy.postRemoteEvent(NavdyDeviceId.getThisDevice(this.context), NavdyEventUtil.eventFromMessage(message));
        }
        catch (Exception ex) {
            ConnectionHandler.sLogger.e("Failed to send local event", ex);
        }
    }
    
    public boolean serviceConnected() {
        return this.proxy.connected();
    }
    
    public void shutdown() {
        this.disconnect();
        this.proxy.disconnect();
    }
    
    public void stopSearch() {
        this.sendLocalMessage(new ConnectionRequest(ConnectionRequest.Action.CONNECTION_STOP_SEARCH, null));
    }
    
    public static class ApplicationLaunchAttempted
    {
    }
    
    public static class DeviceSyncEvent
    {
        public static final int FULL = 1;
        public static final int MINIMAL = 0;
        public final int amount;
        
        public DeviceSyncEvent(final int amount) {
            this.amount = amount;
        }
    }
}
