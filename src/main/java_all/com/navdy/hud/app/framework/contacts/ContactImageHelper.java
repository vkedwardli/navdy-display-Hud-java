package com.navdy.hud.app.framework.contacts;

import com.navdy.hud.app.util.PhoneUtil;
import com.navdy.hud.app.util.GenericUtil;
import android.content.res.Resources;
import java.util.HashMap;
import com.navdy.hud.app.HudApplication;
import java.util.Map;

public class ContactImageHelper
{
    public static final int DEFAULT_IMAGE_INDEX = -1;
    public static int NO_CONTACT_COLOR = 0;
    public static final int NO_CONTACT_IMAGE = R.drawable.icon_user_numberonly;
    private static final Map<Integer, Integer> sContactColorMap;
    private static final Map<Integer, Integer> sContactImageMap;
    private static final ContactImageHelper sInstance;
    
    static {
        final Resources resources = HudApplication.getAppContext().getResources();
        sContactImageMap = new HashMap<Integer, Integer>();
        sContactColorMap = new HashMap<Integer, Integer>();
        ContactImageHelper.NO_CONTACT_COLOR = resources.getColor(R.color.grey_4a);
        ContactImageHelper.sContactColorMap.put(0, resources.getColor(R.color.icon_user_bg_0));
        final Map<Integer, Integer> sContactImageMap2 = ContactImageHelper.sContactImageMap;
        final int n = 0 + 1;
        sContactImageMap2.put(0, R.drawable.icon_user_bg_0);
        ContactImageHelper.sContactColorMap.put(n, resources.getColor(R.color.icon_user_bg_1));
        final Map<Integer, Integer> sContactImageMap3 = ContactImageHelper.sContactImageMap;
        final int n2 = n + 1;
        sContactImageMap3.put(n, R.drawable.icon_user_bg_1);
        ContactImageHelper.sContactColorMap.put(n2, resources.getColor(R.color.icon_user_bg_2));
        final Map<Integer, Integer> sContactImageMap4 = ContactImageHelper.sContactImageMap;
        final int n3 = n2 + 1;
        sContactImageMap4.put(n2, R.drawable.icon_user_bg_2);
        ContactImageHelper.sContactColorMap.put(n3, resources.getColor(R.color.icon_user_bg_3));
        final Map<Integer, Integer> sContactImageMap5 = ContactImageHelper.sContactImageMap;
        final int n4 = n3 + 1;
        sContactImageMap5.put(n3, R.drawable.icon_user_bg_3);
        ContactImageHelper.sContactColorMap.put(n4, resources.getColor(R.color.icon_user_bg_4));
        final Map<Integer, Integer> sContactImageMap6 = ContactImageHelper.sContactImageMap;
        final int n5 = n4 + 1;
        sContactImageMap6.put(n4, R.drawable.icon_user_bg_4);
        ContactImageHelper.sContactColorMap.put(n5, resources.getColor(R.color.icon_user_bg_5));
        final Map<Integer, Integer> sContactImageMap7 = ContactImageHelper.sContactImageMap;
        final int n6 = n5 + 1;
        sContactImageMap7.put(n5, R.drawable.icon_user_bg_5);
        ContactImageHelper.sContactColorMap.put(n6, resources.getColor(R.color.icon_user_bg_6));
        final Map<Integer, Integer> sContactImageMap8 = ContactImageHelper.sContactImageMap;
        final int n7 = n6 + 1;
        sContactImageMap8.put(n6, R.drawable.icon_user_bg_6);
        ContactImageHelper.sContactColorMap.put(n7, resources.getColor(R.color.icon_user_bg_7));
        final Map<Integer, Integer> sContactImageMap9 = ContactImageHelper.sContactImageMap;
        final int n8 = n7 + 1;
        sContactImageMap9.put(n7, R.drawable.icon_user_bg_7);
        ContactImageHelper.sContactColorMap.put(n8, resources.getColor(R.color.icon_user_bg_8));
        ContactImageHelper.sContactImageMap.put(n8, R.drawable.icon_user_bg_8);
        sInstance = new ContactImageHelper();
    }
    
    public static ContactImageHelper getInstance() {
        return ContactImageHelper.sInstance;
    }
    
    public int getContactImageIndex(String s) {
        int abs;
        if (s == null) {
            abs = -1;
        }
        else {
            final String s2 = s = GenericUtil.normalizeToFilename(s);
            if (ContactUtil.isValidNumber(s2)) {
                s = PhoneUtil.convertToE164Format(s2);
            }
            abs = Math.abs(s.hashCode() % ContactImageHelper.sContactImageMap.size());
        }
        return abs;
    }
    
    public int getDriverImageResId(final String s) {
        return this.getResourceId(Math.abs(GenericUtil.normalizeToFilename(s).hashCode() % ContactImageHelper.sContactImageMap.size()));
    }
    
    public int getResourceColor(int n) {
        if (n < 0 || n >= ContactImageHelper.sContactColorMap.size()) {
            n = ContactImageHelper.NO_CONTACT_COLOR;
        }
        else {
            n = ContactImageHelper.sContactColorMap.get(n);
        }
        return n;
    }
    
    public int getResourceId(int intValue) {
        if (intValue < 0 || intValue >= ContactImageHelper.sContactImageMap.size()) {
            intValue = R.drawable.icon_user_numberonly;
        }
        else {
            intValue = ContactImageHelper.sContactImageMap.get(intValue);
        }
        return intValue;
    }
}
