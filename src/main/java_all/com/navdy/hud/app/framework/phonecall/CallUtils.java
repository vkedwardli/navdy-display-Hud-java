package com.navdy.hud.app.framework.phonecall;

import com.navdy.hud.app.manager.RemoteDeviceManager;

public class CallUtils
{
    private static CallManager callManager;
    
    public static boolean isPhoneCallInProgress() {
        synchronized (CallUtils.class) {
            if (CallUtils.callManager == null) {
                CallUtils.callManager = RemoteDeviceManager.getInstance().getCallManager();
            }
            return CallUtils.callManager.isCallInProgress();
        }
    }
}
