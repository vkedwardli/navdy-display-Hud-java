package com.navdy.hud.app.framework.contacts;

import java.util.Locale;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.navdy.hud.app.framework.DriverProfileHelper;
import com.navdy.hud.app.util.PhoneUtil;
import android.text.TextUtils;
import com.navdy.service.library.events.contacts.PhoneNumber;
import android.os.Parcel;
import com.navdy.service.library.log.Logger;
import android.os.Parcelable.Creator;
import android.os.Parcelable;

public class Contact implements Parcelable
{
    public static final Parcelable.Creator<Contact> CREATOR;
    private static final Logger sLogger;
    public int defaultImageIndex;
    public String firstName;
    public String formattedNumber;
    public String initials;
    public String name;
    public String number;
    public NumberType numberType;
    public String numberTypeStr;
    public long numericNumber;
    
    static {
        sLogger = new Logger(Contact.class);
        CREATOR = (Parcelable.Creator)new Parcelable.Creator<Contact>() {
            public Contact createFromParcel(final Parcel parcel) {
                return new Contact(parcel);
            }
            
            public Contact[] newArray(final int n) {
                return new Contact[n];
            }
        };
    }
    
    public Contact(final Parcel parcel) {
        this.name = parcel.readString();
        this.number = parcel.readString();
        final int int1 = parcel.readInt();
        if (int1 != -1) {
            this.numberType = NumberType.values()[int1];
        }
        this.defaultImageIndex = parcel.readInt();
        this.numericNumber = parcel.readLong();
        this.firstName = parcel.readString();
        this.initials = parcel.readString();
        this.numberTypeStr = parcel.readString();
        this.formattedNumber = parcel.readString();
    }
    
    public Contact(final com.navdy.service.library.events.contacts.Contact contact) {
        this.name = contact.name;
        this.number = contact.number;
        this.numberType = ContactUtil.getNumberType(contact.numberType);
        this.defaultImageIndex = ContactImageHelper.getInstance().getContactImageIndex(this.number);
        this.numberTypeStr = contact.label;
        this.init(0L);
    }
    
    public Contact(final PhoneNumber phoneNumber) {
        this.number = phoneNumber.number;
        this.numberType = ContactUtil.getNumberType(phoneNumber.numberType);
        this.defaultImageIndex = ContactImageHelper.getInstance().getContactImageIndex(this.number);
        this.numberTypeStr = phoneNumber.customType;
        this.init(0L);
    }
    
    public Contact(final String name, final String number, final NumberType numberType, final int defaultImageIndex, final long numericNumber) {
        this.name = name;
        this.number = number;
        this.numberType = numberType;
        this.defaultImageIndex = defaultImageIndex;
        this.init(this.numericNumber = numericNumber);
    }
    
    private void init(final long numericNumber) {
        if (!TextUtils.isEmpty((CharSequence)this.name)) {
            this.firstName = ContactUtil.getFirstName(this.name);
            this.initials = ContactUtil.getInitials(this.name);
        }
        if (this.numberTypeStr == null) {
            this.numberTypeStr = ContactUtil.getPhoneType(this.numberType);
        }
        this.formattedNumber = PhoneUtil.formatPhoneNumber(this.number);
        final Locale currentLocale = DriverProfileHelper.getInstance().getCurrentLocale();
        if (!TextUtils.isEmpty((CharSequence)this.number)) {
            if (numericNumber > 0L) {
                this.numericNumber = numericNumber;
            }
            else {
                try {
                    this.numericNumber = PhoneNumberUtil.getInstance().parse(this.number, currentLocale.getCountry()).getNationalNumber();
                }
                catch (Throwable t) {
                    Contact.sLogger.e(t);
                }
            }
        }
    }
    
    public int describeContents() {
        return 0;
    }
    
    public void setName(final String name) {
        this.name = name;
    }
    
    public void writeToParcel(final Parcel parcel, int ordinal) {
        parcel.writeString(this.name);
        parcel.writeString(this.number);
        if (this.numberType != null) {
            ordinal = this.numberType.ordinal();
        }
        else {
            ordinal = -1;
        }
        parcel.writeInt(ordinal);
        parcel.writeInt(this.defaultImageIndex);
        parcel.writeLong(this.numericNumber);
        parcel.writeString(this.firstName);
        parcel.writeString(this.initials);
        parcel.writeString(this.numberTypeStr);
        parcel.writeString(this.formattedNumber);
    }
}
