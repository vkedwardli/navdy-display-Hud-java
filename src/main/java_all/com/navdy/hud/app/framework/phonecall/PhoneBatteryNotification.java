package com.navdy.hud.app.framework.phonecall;

import java.io.Serializable;
import com.navdy.hud.app.framework.toast.IToastCallback;
import android.text.TextUtils;
import com.navdy.hud.app.framework.voice.TTSUtils;
import android.os.Bundle;
import com.navdy.service.library.events.callcontrol.PhoneBatteryStatus;
import com.navdy.hud.app.framework.toast.ToastManager;
import com.navdy.hud.app.HudApplication;
import com.navdy.service.library.log.Logger;
import android.content.res.Resources;

public class PhoneBatteryNotification
{
    private static final String EMPTY = "";
    private static final String ID_BATTERY_EXTREMELY_LOW = "phone-bat-exlow";
    private static final String ID_BATTERY_LOW = "phone-bat-low";
    private static final String ID_NO_NETWORK = "phone-no-network";
    private static final int PHONE_BATTERY_TIMEOUT = 2000;
    private static final int PHONE_BATTERY_TIMEOUT_EXTREMELY_LOW = 20000;
    private static final int PHONE_NO_NETWORK_TIMEOUT = 2000;
    private static final String noNetwork;
    private static final Resources resources;
    private static final Logger sLogger;
    private static final String unknown;
    
    static {
        sLogger = new Logger(PhoneBatteryNotification.class);
        resources = HudApplication.getAppContext().getResources();
        unknown = PhoneBatteryNotification.resources.getString(R.string.question_mark);
        noNetwork = PhoneBatteryNotification.resources.getString(R.string.phone_no_network);
    }
    
    private static void dismissAllBatteryToasts() {
        final ToastManager instance = ToastManager.getInstance();
        instance.dismissCurrentToast("phone-bat-low", "phone-bat-exlow");
        instance.clearPendingToast("phone-bat-low", "phone-bat-exlow");
    }
    
    public static void dismissAllToasts() {
        dismissAllBatteryToasts();
        final ToastManager instance = ToastManager.getInstance();
        instance.dismissCurrentToast("phone-no-network");
        instance.clearPendingToast("phone-no-network");
    }
    
    public static void showBatteryToast(final PhoneBatteryStatus phoneBatteryStatus) {
        PhoneBatteryNotification.sLogger.v("[PhoneBatteryStatus] status[" + phoneBatteryStatus.status + "] charging[" + phoneBatteryStatus.charging + "] level[" + phoneBatteryStatus.level + "]");
        if (phoneBatteryStatus.status == PhoneBatteryStatus.BatteryStatus.BATTERY_OK || Boolean.TRUE.equals(phoneBatteryStatus.charging)) {
            PhoneBatteryNotification.sLogger.v("phone battery is ok/charging");
            dismissAllBatteryToasts();
        }
        else {
            final Bundle bundle = new Bundle();
            Serializable s;
            if (phoneBatteryStatus.level != null) {
                s = phoneBatteryStatus.level;
            }
            else {
                s = PhoneBatteryNotification.unknown;
            }
            final String value = String.valueOf(s);
            String s2 = "";
            int n = 0;
            final String s3 = null;
            String s4 = null;
            final ToastManager instance = ToastManager.getInstance();
            String s5 = null;
            switch (phoneBatteryStatus.status) {
                default:
                    s5 = s3;
                    break;
                case BATTERY_EXTREMELY_LOW:
                    s2 = PhoneBatteryNotification.resources.getString(R.string.phone_ex_low_battery, new Object[] { value });
                    n = R.drawable.icon_toast_phone_battery_very_low;
                    bundle.putInt("13", 20000);
                    s5 = "phone-bat-exlow";
                    bundle.putBoolean("12", true);
                    s4 = TTSUtils.TTS_PHONE_BATTERY_VERY_LOW;
                    instance.dismissCurrentToast("phone-bat-low");
                    instance.clearPendingToast("phone-bat-low", "phone-bat-exlow");
                    break;
                case BATTERY_LOW:
                    s2 = PhoneBatteryNotification.resources.getString(R.string.phone_low_battery, new Object[] { value });
                    n = R.drawable.icon_toast_phone_battery_low;
                    bundle.putInt("13", 2000);
                    s5 = "phone-bat-low";
                    s4 = TTSUtils.TTS_PHONE_BATTERY_LOW;
                    instance.dismissCurrentToast("phone-bat-exlow");
                    instance.clearPendingToast("phone-bat-low", "phone-bat-exlow");
                    break;
            }
            bundle.putInt("8", n);
            bundle.putString("4", s2);
            bundle.putInt("5", R.style.Glances_1);
            bundle.putString("17", s4);
            if (!TextUtils.equals((CharSequence)s5, (CharSequence)instance.getCurrentToastId())) {
                ToastManager.getInstance().addToast(new ToastManager.ToastParams(s5, bundle, null, false, false));
            }
        }
    }
}
