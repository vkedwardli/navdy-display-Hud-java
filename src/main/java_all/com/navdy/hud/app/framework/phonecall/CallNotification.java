package com.navdy.hud.app.framework.phonecall;

import com.navdy.service.library.events.DeviceInfo;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.view.ToastView;
import com.navdy.hud.app.framework.toast.IToastCallback;
import android.os.Bundle;
import com.squareup.otto.Subscribe;
import com.navdy.service.library.events.callcontrol.PhoneStatus;
import com.navdy.service.library.events.photo.PhotoType;
import com.navdy.hud.app.framework.contacts.PhoneImageDownloader;
import com.navdy.service.library.events.input.GestureEvent;
import com.navdy.hud.app.ui.framework.UIStateManager;
import com.navdy.hud.app.manager.InputManager;
import android.animation.ObjectAnimator;
import android.animation.Animator;
import android.animation.AnimatorSet;
import android.view.LayoutInflater;
import com.navdy.hud.app.framework.notifications.NotificationType;
import com.navdy.hud.app.framework.glance.GlanceViewCache;
import android.view.View;
import android.content.Context;
import android.text.TextUtils;
import com.navdy.hud.app.framework.glance.GlanceConstants;
import com.navdy.hud.app.util.PhoneUtil;
import android.os.SystemClock;
import com.navdy.hud.app.framework.notifications.NotificationManager;
import com.navdy.hud.app.framework.toast.ToastManager;
import com.navdy.hud.app.HudApplication;
import java.util.List;
import com.navdy.service.library.events.callcontrol.CallAction;
import java.util.concurrent.TimeUnit;
import android.os.Looper;
import com.makeramen.RoundedTransformationBuilder;
import com.squareup.picasso.Transformation;
import android.content.res.Resources;
import com.navdy.hud.app.ui.component.ChoiceLayout;
import android.os.Handler;
import com.navdy.hud.app.framework.notifications.INotificationController;
import android.view.ViewGroup;
import android.widget.TextView;
import com.navdy.hud.app.ui.component.image.InitialsImageView;
import android.widget.ImageView;
import com.squareup.otto.Bus;
import com.navdy.hud.app.ui.component.ChoiceLayout2;
import java.util.ArrayList;
import com.navdy.service.library.log.Logger;
import com.navdy.hud.app.framework.contacts.ContactUtil;
import com.navdy.hud.app.framework.notifications.INotification;

public class CallNotification implements INotification, IContactCallback
{
    public static final String CALL_NOTIFICATION_TOAST_ID = "incomingcall#toast";
    private static final String EMPTY = "";
    private static final int END_NOTIFICATION_TIMEOUT = 3000;
    private static final int FAILED_NOTIFICATION_TIMEOUT = 10000;
    private static final float IMAGE_SCALE = 0.5f;
    private static final int IN_CALL_NOTIFICATION_TIMEOUT = 30000;
    private static final int MISSED_NOTIFICATION_TIMEOUT = 10000;
    private static final int TAG_ACCEPT = 101;
    private static final int TAG_CANCEL = 105;
    private static final int TAG_DISMISS = 107;
    private static final int TAG_END = 106;
    private static final int TAG_IGNORE = 102;
    private static final int TAG_MUTE = 103;
    private static final int TAG_UNMUTE = 104;
    private static final Logger sLogger;
    private final String activeCall;
    private ArrayList<ChoiceLayout2.Choice> activeCallChoices;
    private ArrayList<ChoiceLayout2.Choice> activeCallMutedChoices;
    private ArrayList<ChoiceLayout2.Choice> activeCallNoMuteChoices;
    private Bus bus;
    private final String callAccept;
    private final String callCancel;
    private final String callDialing;
    private final String callDismiss;
    private final String callEnd;
    private final String callEnded;
    private final String callFailed;
    private final String callIgnore;
    private CallManager callManager;
    private final String callMissed;
    private final String callMute;
    private final String callMuted;
    private ImageView callStatusImage;
    private final String callUnmute;
    private InitialsImageView callerImage;
    private TextView callerName;
    private TextView callerStatus;
    private final String callerUnknown;
    private ChoiceLayout2 choiceLayout;
    private ChoiceLayout2.IListener choiceListener;
    private ViewGroup container;
    private int contentWidth;
    private INotificationController controller;
    private ArrayList<ChoiceLayout2.Choice> dialingCallChoices;
    private final int dismisColor;
    private Runnable durationRunnable;
    private final int endColor;
    private ArrayList<ChoiceLayout2.Choice> endedCallChoices;
    private ViewGroup extendedContainer;
    private TextView extendedTitle;
    private ArrayList<ChoiceLayout2.Choice> failedCallChoices;
    private Handler handler;
    private final String incomingCall;
    private ArrayList<ChoiceLayout.Choice> incomingCallChoices;
    private ArrayList<ChoiceLayout2.Choice> missedCallChoices;
    private final int muteColor;
    private Resources resources;
    private Transformation roundTransformation;
    private final int unmuteColor;
    private final int unselectedColor;
    
    static {
        sLogger = new Logger(CallNotification.class);
    }
    
    CallNotification(final CallManager callManager, final Bus bus) {
        this.incomingCallChoices = new ArrayList<ChoiceLayout.Choice>(2);
        this.activeCallChoices = new ArrayList<ChoiceLayout2.Choice>(2);
        this.activeCallNoMuteChoices = new ArrayList<ChoiceLayout2.Choice>(1);
        this.activeCallMutedChoices = new ArrayList<ChoiceLayout2.Choice>(2);
        this.dialingCallChoices = new ArrayList<ChoiceLayout2.Choice>(1);
        this.failedCallChoices = new ArrayList<ChoiceLayout2.Choice>(1);
        this.endedCallChoices = new ArrayList<ChoiceLayout2.Choice>(1);
        this.missedCallChoices = new ArrayList<ChoiceLayout2.Choice>(1);
        this.roundTransformation = new RoundedTransformationBuilder().oval(true).build();
        this.handler = new Handler(Looper.getMainLooper());
        this.durationRunnable = new Runnable() {
            @Override
            public void run() {
                try {
                    CallNotification.this.setCallDuration(CallNotification.this.extendedTitle);
                }
                finally {
                    CallNotification.this.handler.postDelayed(CallNotification.this.durationRunnable, TimeUnit.MINUTES.toMillis(1L));
                }
            }
        };
        this.choiceListener = new ChoiceLayout2.IListener() {
            @Override
            public void executeItem(final Selection selection) {
                switch (selection.id) {
                    case 103:
                        if (CallNotification.this.callManager.state != CallManager.CallNotificationState.IN_PROGRESS) {
                            CallNotification.this.dismissNotification();
                            break;
                        }
                        CallNotification.this.callManager.sendCallAction(CallAction.CALL_MUTE, null);
                        CallNotification.this.callerStatus.setText((CharSequence)CallNotification.this.callMuted);
                        CallNotification.this.callStatusImage.setImageResource(R.drawable.icon_call_muted);
                        CallNotification.this.choiceLayout.setChoices(CallNotification.this.activeCallMutedChoices, 0, CallNotification.this.choiceListener, 0.5f);
                        CallNotification.this.choiceLayout.setTag(CallNotification.this.activeCallMutedChoices);
                        break;
                    case 106:
                        if (CallNotification.this.callManager.state != CallManager.CallNotificationState.IN_PROGRESS) {
                            CallNotification.this.dismissNotification();
                            break;
                        }
                        CallNotification.this.callManager.sendCallAction(CallAction.CALL_END, null);
                        break;
                    case 104:
                        if (CallNotification.this.callManager.state != CallManager.CallNotificationState.IN_PROGRESS) {
                            CallNotification.this.dismissNotification();
                            break;
                        }
                        CallNotification.this.callManager.sendCallAction(CallAction.CALL_UNMUTE, null);
                        CallNotification.this.callerStatus.setText((CharSequence)"");
                        CallNotification.this.callStatusImage.setImageResource(R.drawable.icon_call_green);
                        CallNotification.this.choiceLayout.setChoices(CallNotification.this.activeCallChoices, 0, CallNotification.this.choiceListener, 0.5f);
                        CallNotification.this.choiceLayout.setTag(CallNotification.this.activeCallChoices);
                        break;
                    case 105:
                        if (CallNotification.this.callManager.state == CallManager.CallNotificationState.DIALING) {
                            CallNotification.this.callManager.sendCallAction(CallAction.CALL_END, null);
                            break;
                        }
                        if (CallNotification.this.callManager.state != CallManager.CallNotificationState.IN_PROGRESS) {
                            CallNotification.this.dismissNotification();
                            break;
                        }
                        break;
                    case 107:
                        CallNotification.this.dismissNotification();
                        break;
                    case 5:
                        if (CallNotification.this.controller != null && CallNotification.this.controller.isExpandedWithStack()) {
                            CallNotification.this.controller.collapseNotification(false, false);
                            break;
                        }
                        break;
                }
            }
            
            @Override
            public void itemSelected(final Selection selection) {
            }
        };
        this.callManager = callManager;
        this.bus = bus;
        this.resources = HudApplication.getAppContext().getResources();
        this.callEnded = this.resources.getString(R.string.call_ended);
        this.callDialing = this.resources.getString(R.string.call_dialing);
        this.callMuted = this.resources.getString(R.string.call_muted);
        this.callerUnknown = this.resources.getString(R.string.caller_unknown);
        this.callAccept = this.resources.getString(R.string.call_accept);
        this.callIgnore = this.resources.getString(R.string.call_ignore);
        this.callMute = this.resources.getString(R.string.call_mute);
        this.callEnd = this.resources.getString(R.string.call_end);
        this.callUnmute = this.resources.getString(R.string.call_unmute);
        this.callCancel = this.resources.getString(R.string.call_cancel);
        this.callDismiss = this.resources.getString(R.string.call_dismiss);
        this.callFailed = this.resources.getString(R.string.call_failed);
        this.callMissed = this.resources.getString(R.string.call_missed);
        this.incomingCall = this.resources.getString(R.string.incoming_call);
        this.activeCall = this.resources.getString(R.string.phone_call_duration);
        this.muteColor = this.resources.getColor(R.color.call_mute);
        this.unmuteColor = this.resources.getColor(R.color.call_unmute);
        this.endColor = this.resources.getColor(R.color.call_end);
        this.dismisColor = this.resources.getColor(R.color.glance_dismiss);
        this.unselectedColor = -16777216;
        this.contentWidth = (int)this.resources.getDimension(R.dimen.toast_app_disconnected_width);
        this.incomingCallChoices.add(new ChoiceLayout.Choice(this.callAccept, 101));
        this.incomingCallChoices.add(new ChoiceLayout.Choice(this.callIgnore, 102));
        this.activeCallChoices.add(new ChoiceLayout2.Choice(103, R.drawable.icon_glances_mute, this.muteColor, R.drawable.icon_glances_mute, this.unselectedColor, this.callMute, this.muteColor));
        this.activeCallChoices.add(new ChoiceLayout2.Choice(106, R.drawable.icon_glances_endcall, this.endColor, R.drawable.icon_glances_endcall, this.unselectedColor, this.callEnd, this.endColor));
        this.activeCallNoMuteChoices.add(new ChoiceLayout2.Choice(106, R.drawable.icon_glances_endcall, this.endColor, R.drawable.icon_glances_endcall, this.unselectedColor, this.callEnd, this.endColor));
        this.activeCallMutedChoices.add(new ChoiceLayout2.Choice(104, R.drawable.icon_glances_unmute, this.unmuteColor, R.drawable.icon_glances_unmute, this.unselectedColor, this.callUnmute, this.unmuteColor));
        this.activeCallMutedChoices.add(new ChoiceLayout2.Choice(106, R.drawable.icon_glances_endcall, this.endColor, R.drawable.icon_glances_endcall, this.unselectedColor, this.callEnd, this.endColor));
        this.dialingCallChoices.add(new ChoiceLayout2.Choice(105, R.drawable.icon_glances_endcall, this.endColor, R.drawable.icon_glances_endcall, this.unselectedColor, this.callEnd, this.endColor));
        this.failedCallChoices.add(new ChoiceLayout2.Choice(107, R.drawable.icon_glances_dismiss, this.dismisColor, R.drawable.icon_glances_dismiss, this.unselectedColor, this.callDismiss, this.dismisColor));
        this.endedCallChoices.add(new ChoiceLayout2.Choice(107, R.drawable.icon_glances_dismiss, this.dismisColor, R.drawable.icon_glances_dismiss, this.unselectedColor, this.callDismiss, this.dismisColor));
        this.missedCallChoices.add(new ChoiceLayout2.Choice(107, R.drawable.icon_glances_dismiss, this.dismisColor, R.drawable.icon_glances_dismiss, this.unselectedColor, this.callDismiss, this.dismisColor));
    }
    
    private boolean checkIfCollapseRequired() {
        boolean b2;
        final boolean b = b2 = false;
        if (!this.isAlive()) {
            b2 = b;
            if (this.controller != null) {
                b2 = b;
                if (this.controller.isExpandedWithStack()) {
                    CallNotification.sLogger.v("collapse required");
                    this.controller.collapseNotification(false, false);
                    b2 = true;
                }
            }
        }
        return b2;
    }
    
    public static void clearToast() {
        final ToastManager instance = ToastManager.getInstance();
        instance.dismissCurrentToast("incomingcall#toast");
        instance.clearAllPendingToast();
        instance.disableToasts(false);
    }
    
    private void dismissNotification() {
        NotificationManager.getInstance().removeNotification("navdy#phone#call#notif");
        this.callManager.state = CallManager.CallNotificationState.IDLE;
    }
    
    private String getCallerNumber() {
        return this.callManager.number;
    }
    
    private int getTimeout(final CallManager.CallNotificationState callNotificationState) {
        int n = 0;
        if (callNotificationState != null) {
            switch (callNotificationState) {
                case MISSED:
                    n = 10000;
                    break;
                case ENDED:
                    n = 3000;
                    break;
                case FAILED:
                    n = 10000;
                    break;
                case IN_PROGRESS:
                case DIALING:
                    n = 30000;
                    break;
            }
        }
        return n;
    }
    
    private boolean isTimeoutRequired(final CallManager.CallNotificationState callNotificationState) {
        boolean b = false;
        if (callNotificationState != null) {
            switch (callNotificationState) {
                case MISSED:
                case ENDED:
                case FAILED:
                case DIALING:
                    b = true;
                    break;
                case IN_PROGRESS:
                    if (this.controller == null || !this.controller.isExpandedWithStack()) {
                        b = true;
                        break;
                    }
                    break;
            }
        }
        return b;
    }
    
    private void setCallDuration(final TextView textView) {
        final long callStartMs = this.callManager.callStartMs;
        if (callStartMs <= 0L) {
            textView.setText((CharSequence)this.activeCall);
        }
        else {
            final long minutes = TimeUnit.MILLISECONDS.toMinutes(SystemClock.elapsedRealtime() - callStartMs);
            if (minutes < 1L) {
                textView.setText((CharSequence)this.activeCall);
            }
            else {
                textView.setText((CharSequence)this.resources.getString(R.string.phone_call_duration_min, new Object[] { minutes }));
            }
        }
    }
    
    private boolean setCaller() {
        final String caller = this.getCaller();
        boolean b;
        if (ContactUtil.isValidNumber(caller)) {
            this.callerName.setText((CharSequence)PhoneUtil.formatPhoneNumber(caller));
            b = true;
        }
        else {
            this.callerName.setText((CharSequence)caller);
            b = false;
        }
        return b;
    }
    
    private void switchToExpandedMode() {
        if (this.controller != null) {
            this.controller.stopTimeout(true);
            this.choiceLayout.setChoices(GlanceConstants.backChoice2, 0, this.choiceListener, 0.5f);
            this.choiceLayout.setTag(GlanceConstants.backChoice2);
            if (!this.controller.isExpandedWithStack()) {
                this.controller.expandNotification(true);
            }
        }
    }
    
    @Override
    public boolean canAddToStackIfCurrentExists() {
        return true;
    }
    
    @Override
    public boolean expandNotification() {
        boolean b = false;
        if (this.controller != null) {
            final Object tag = this.choiceLayout.getTag();
            if (tag == this.activeCallChoices || tag == this.activeCallMutedChoices || tag == this.activeCallNoMuteChoices) {
                this.switchToExpandedMode();
                b = true;
            }
        }
        return b;
    }
    
    public String getCaller() {
        String s;
        if (!TextUtils.isEmpty((CharSequence)this.callManager.contact)) {
            s = this.callManager.contact;
        }
        else if (!TextUtils.isEmpty((CharSequence)this.callManager.number)) {
            s = this.callManager.number;
        }
        else {
            s = this.callerUnknown;
        }
        return s;
    }
    
    @Override
    public int getColor() {
        return GlanceConstants.colorPhoneCall;
    }
    
    @Override
    public View getExpandedView(final Context context, final Object o) {
        this.extendedContainer = (ViewGroup)GlanceViewCache.getView(GlanceViewCache.ViewType.BIG_MULTI_TEXT, context);
        (this.extendedTitle = (TextView)this.extendedContainer.findViewById(R.id.title1)).setTextAppearance(context, R.style.glance_title_1);
        ((TextView)this.extendedContainer.findViewById(R.id.title2)).setVisibility(GONE);
        this.setCallDuration(this.extendedTitle);
        this.extendedTitle.setVisibility(View.VISIBLE);
        this.handler.removeCallbacks(this.durationRunnable);
        this.handler.postDelayed(this.durationRunnable, TimeUnit.MINUTES.toMillis(1L));
        return (View)this.extendedContainer;
    }
    
    @Override
    public int getExpandedViewIndicatorColor() {
        return 0;
    }
    
    @Override
    public String getId() {
        return "navdy#phone#call#notif";
    }
    
    @Override
    public int getTimeout() {
        return 0;
    }
    
    @Override
    public NotificationType getType() {
        return NotificationType.PHONE_CALL;
    }
    
    @Override
    public View getView(final Context context) {
        if (this.container == null) {
            this.container = (ViewGroup)LayoutInflater.from(context).inflate(R.layout.notification_phonecall, (ViewGroup)null);
            this.callerName = (TextView)this.container.findViewById(R.id.callerName);
            this.callerStatus = (TextView)this.container.findViewById(R.id.callerStatus);
            this.callerImage = (InitialsImageView)this.container.findViewById(R.id.callerImage);
            this.callStatusImage = (ImageView)this.container.findViewById(R.id.callStatusImage);
            this.choiceLayout = (ChoiceLayout2)this.container.findViewById(R.id.choiceLayout);
        }
        return (View)this.container;
    }
    
    @Override
    public AnimatorSet getViewSwitchAnimation(final boolean b) {
        final AnimatorSet set = new AnimatorSet();
        if (!b) {
            set.playTogether(new Animator[] { ObjectAnimator.ofFloat(this.callerName, View.ALPHA, new float[] { 1.0f, 0.0f }), ObjectAnimator.ofFloat(this.callerStatus, View.ALPHA, new float[] { 1.0f, 0.0f }), ObjectAnimator.ofFloat(this.choiceLayout, View.ALPHA, new float[] { 1.0f, 0.0f }) });
        }
        else {
            set.playTogether(new Animator[] { ObjectAnimator.ofFloat(this.callerName, View.ALPHA, new float[] { 0.0f, 1.0f }), ObjectAnimator.ofFloat(this.callerStatus, View.ALPHA, new float[] { 0.0f, 1.0f }), ObjectAnimator.ofFloat(this.choiceLayout, View.ALPHA, new float[] { 0.0f, 1.0f }) });
        }
        return set;
    }
    
    @Override
    public boolean isAlive() {
        boolean b = false;
        switch (this.callManager.state) {
            default:
                b = b;
                return b;
            case IN_PROGRESS:
            case RINGING:
            case DIALING:
                b = true;
            case IDLE:
            case MISSED:
            case ENDED:
            case FAILED:
            case REJECTED:
            case CANCELLED:
                return b;
        }
    }
    
    @Override
    public boolean isContextValid() {
        return this.controller != null;
    }
    
    @Override
    public boolean isPurgeable() {
        return !this.isAlive();
    }
    
    @Override
    public IInputHandler nextHandler() {
        return null;
    }
    
    @Override
    public void onClick() {
    }
    
    @Override
    public void onExpandedNotificationEvent(final UIStateManager.Mode mode) {
        if (mode == UIStateManager.Mode.COLLAPSE && this.controller != null) {
            this.updateState();
        }
    }
    
    @Override
    public void onExpandedNotificationSwitched() {
    }
    
    @Override
    public boolean onGesture(final GestureEvent gestureEvent) {
        return false;
    }
    
    @Override
    public boolean onKey(final CustomKeyEvent customKeyEvent) {
        boolean b = false;
        boolean b2 = false;
        switch (customKeyEvent) {
            case LEFT:
                b = this.choiceLayout.moveSelectionLeft();
                b2 = true;
                break;
            case RIGHT:
                b = this.choiceLayout.moveSelectionRight();
                b2 = true;
                break;
            case SELECT:
                this.choiceLayout.executeSelectedItem();
                b2 = true;
                break;
        }
        if (this.controller != null && b && this.isTimeoutRequired(this.callManager.state) && this.getTimeout(this.callManager.state) > 0) {
            this.controller.resetTimeout();
        }
        return b2;
    }
    
    @Override
    public void onNotificationEvent(final UIStateManager.Mode mode) {
        if (mode == UIStateManager.Mode.EXPAND) {
            final CallManager.CallNotificationState state = this.callManager.state;
            if (this.isTimeoutRequired(state)) {
                if (this.controller != null) {
                    CallNotification.sLogger.v("set timeout:" + state);
                    this.controller.stopTimeout(true);
                    this.controller.startTimeout(this.getTimeout(state));
                }
            }
            else if (this.controller != null) {
                CallNotification.sLogger.v("reset timeout:" + state);
                this.controller.stopTimeout(true);
            }
        }
    }
    
    @Subscribe
    public void onPhotoDownload(final PhoneImageDownloader.PhotoDownloadStatus photoDownloadStatus) {
        if (this.controller != null && !photoDownloadStatus.alreadyDownloaded && photoDownloadStatus.photoType == PhotoType.PHOTO_CONTACT && this.callManager.phoneStatus != PhoneStatus.PHONE_IDLE && TextUtils.equals((CharSequence)this.callManager.number, (CharSequence)photoDownloadStatus.sourceIdentifier)) {
            ContactUtil.setContactPhoto(this.callManager.contact, this.callManager.number, false, this.callerImage, this.roundTransformation, (ContactUtil.IContactCallback)this);
        }
    }
    
    @Override
    public void onStart(final INotificationController controller) {
        this.controller = controller;
        this.bus.register(this);
        this.updateState();
        if (!controller.isExpandedWithStack()) {
            this.container.setTranslationX(0.0f);
            this.container.setTranslationY(0.0f);
            this.container.setAlpha(1.0f);
            this.callerName.setAlpha(1.0f);
            this.callerStatus.setAlpha(1.0f);
            this.choiceLayout.setAlpha(1.0f);
        }
    }
    
    @Override
    public void onStop() {
        this.handler.removeCallbacks(this.durationRunnable);
        this.bus.unregister(this);
        this.controller = null;
        if (this.choiceLayout != null) {
            this.choiceLayout.clear();
        }
        if (this.extendedContainer != null) {
            final ViewGroup viewGroup = (ViewGroup)this.extendedContainer.getParent();
            if (viewGroup != null) {
                viewGroup.removeView((View)this.extendedContainer);
            }
            GlanceViewCache.putView(GlanceViewCache.ViewType.BIG_MULTI_TEXT, (View)this.extendedContainer);
            this.extendedContainer = null;
        }
    }
    
    @Override
    public void onTrackHand(final float n) {
    }
    
    @Override
    public void onUpdate() {
        this.updateState();
    }
    
    public void showIncomingCallToast() {
        final String caller = this.getCaller();
        String callerNumber;
        if (TextUtils.isEmpty((CharSequence)(callerNumber = this.getCallerNumber()))) {
            callerNumber = null;
        }
        final Bundle bundle = new Bundle();
        bundle.putString("1", this.incomingCall);
        bundle.putInt("1_1", R.style.ToastMainTitle);
        bundle.putInt("11", R.drawable.icon_call_green);
        bundle.putParcelableArrayList("20", (ArrayList)this.incomingCallChoices);
        bundle.putBoolean("19", true);
        bundle.putInt("16", this.contentWidth);
        if (callerNumber != null && !TextUtils.equals((CharSequence)caller, (CharSequence)callerNumber)) {
            bundle.putString("4", caller);
            bundle.putInt("5", R.style.Glances_1);
            bundle.putString("6", PhoneUtil.formatPhoneNumber(callerNumber));
            bundle.putInt("7", R.style.Toast_1);
        }
        else {
            if (ContactUtil.isValidNumber(caller)) {
                bundle.putString("4", PhoneUtil.formatPhoneNumber(callerNumber));
            }
            else {
                bundle.putString("4", caller);
            }
            bundle.putInt("5", R.style.Glances_1);
        }
        final IToastCallback toastCallback = new IToastCallback() {
            IContactCallback cb = new IContactCallback() {
                @Override
                public boolean isContextValid() {
                    return IToastCallback.this.toastView != null;
                }
            };
            InitialsImageView toastImage;
            ToastView toastView;
            
            @Override
            public void executeChoiceItem(final int n, final int n2) {
                switch (n2) {
                    case 101:
                        if (CallNotification.this.callManager.state == CallManager.CallNotificationState.RINGING) {
                            CallNotification.this.callManager.sendCallAction(CallAction.CALL_ACCEPT, null);
                        }
                        ToastManager.getInstance().dismissCurrentToast("incomingcall#toast");
                        break;
                    case 102:
                        if (CallNotification.this.callManager.state == CallManager.CallNotificationState.RINGING) {
                            CallNotification.this.callManager.sendCallAction(CallAction.CALL_REJECT, null);
                        }
                        ToastManager.getInstance().dismissCurrentToast("incomingcall#toast");
                        break;
                }
            }
            
            @Override
            public boolean onKey(final CustomKeyEvent customKeyEvent) {
                return false;
            }
            
            @Subscribe
            public void onPhotoDownload(final PhoneImageDownloader.PhotoDownloadStatus photoDownloadStatus) {
                if (this.toastView != null && this.toastImage != null && !photoDownloadStatus.alreadyDownloaded && photoDownloadStatus.photoType == PhotoType.PHOTO_CONTACT && CallNotification.this.callManager.phoneStatus != PhoneStatus.PHONE_IDLE && TextUtils.equals((CharSequence)callerNumber, (CharSequence)photoDownloadStatus.sourceIdentifier)) {
                    ContactUtil.setContactPhoto(caller, callerNumber, false, this.toastImage, CallNotification.this.roundTransformation, this.cb);
                }
            }
            
            @Override
            public void onStart(final ToastView toastView) {
                this.toastView = toastView;
                (this.toastImage = toastView.getMainLayout().screenImage).setVisibility(View.VISIBLE);
                CallNotification.this.bus.register(this);
                ContactUtil.setContactPhoto(caller, callerNumber, true, this.toastImage, CallNotification.this.roundTransformation, this.cb);
            }
            
            @Override
            public void onStop() {
                CallNotification.this.bus.unregister(this);
                this.toastView = null;
                this.toastImage = null;
            }
        };
        final ToastManager instance = ToastManager.getInstance();
        clearToast();
        instance.addToast(new ToastManager.ToastParams("incomingcall#toast", bundle, toastCallback, false, true, true));
    }
    
    @Override
    public boolean supportScroll() {
        return false;
    }
    
    void updateState() {
        final CallManager.CallNotificationState state = this.callManager.state;
        CallNotification.sLogger.v("call state:" + state);
        if (!this.checkIfCollapseRequired()) {
            switch (state) {
                case IDLE:
                    if (NotificationManager.getInstance().isCurrentNotificationId("navdy#phone#call#notif")) {
                        this.dismissNotification();
                        break;
                    }
                    break;
                case MISSED:
                    this.setCaller();
                    this.callerStatus.setText((CharSequence)this.callMissed);
                    this.choiceLayout.setVisibility(View.VISIBLE);
                    this.choiceLayout.setChoices(this.missedCallChoices, 0, this.choiceListener, 0.5f);
                    this.choiceLayout.setTag(this.missedCallChoices);
                    this.callStatusImage.setImageResource(R.drawable.icon_badge_callend);
                    this.callStatusImage.setVisibility(View.VISIBLE);
                    ContactUtil.setContactPhoto(this.callManager.contact, this.callManager.number, false, this.callerImage, this.roundTransformation, (ContactUtil.IContactCallback)this);
                    break;
                case REJECTED:
                    this.dismissNotification();
                    break;
                case CANCELLED:
                    this.dismissNotification();
                    break;
                case ENDED:
                    this.setCaller();
                    this.callerStatus.setText((CharSequence)this.callEnded);
                    this.choiceLayout.setVisibility(View.VISIBLE);
                    this.choiceLayout.setChoices(this.endedCallChoices, 0, this.choiceListener, 0.5f);
                    this.choiceLayout.setTag(this.endedCallChoices);
                    this.callStatusImage.setImageResource(R.drawable.icon_badge_callend);
                    this.callStatusImage.setVisibility(View.VISIBLE);
                    ContactUtil.setContactPhoto(this.callManager.contact, this.callManager.number, false, this.callerImage, this.roundTransformation, (ContactUtil.IContactCallback)this);
                    break;
                case DIALING:
                    this.setCaller();
                    this.callerStatus.setText((CharSequence)this.callDialing);
                    this.callStatusImage.setVisibility(INVISIBLE);
                    ContactUtil.setContactPhoto(this.callManager.contact, this.callManager.number, true, this.callerImage, this.roundTransformation, (ContactUtil.IContactCallback)this);
                    this.choiceLayout.setChoices(this.dialingCallChoices, 0, this.choiceListener, 0.5f);
                    this.choiceLayout.setTag(this.dialingCallChoices);
                    this.choiceLayout.setVisibility(View.VISIBLE);
                    break;
                case IN_PROGRESS:
                    if (this.setCaller()) {
                        this.callerStatus.setText((CharSequence)"");
                    }
                    else {
                        final String callerNumber = this.getCallerNumber();
                        if (ContactUtil.isValidNumber(callerNumber)) {
                            this.callerStatus.setText((CharSequence)PhoneUtil.formatPhoneNumber(callerNumber));
                        }
                    }
                    this.callStatusImage.setImageResource(R.drawable.icon_badge_call);
                    this.callStatusImage.setVisibility(View.VISIBLE);
                    ContactUtil.setContactPhoto(this.callManager.contact, this.callManager.number, false, this.callerImage, this.roundTransformation, (ContactUtil.IContactCallback)this);
                    if (this.controller.isExpandedWithStack()) {
                        this.choiceLayout.setChoices(GlanceConstants.backChoice2, 0, this.choiceListener, 0.5f);
                        this.choiceLayout.setTag(GlanceConstants.backChoice2);
                    }
                    else {
                        final DeviceInfo remoteDeviceInfo = RemoteDeviceManager.getInstance().getRemoteDeviceInfo();
                        if (remoteDeviceInfo == null || remoteDeviceInfo.platform == DeviceInfo.Platform.PLATFORM_Android) {
                            this.choiceLayout.setChoices(this.activeCallNoMuteChoices, 0, this.choiceListener, 0.5f);
                            this.choiceLayout.setTag(this.activeCallNoMuteChoices);
                        }
                        else if (!this.callManager.callMuted) {
                            this.choiceLayout.setChoices(this.activeCallChoices, 0, this.choiceListener, 0.5f);
                            this.choiceLayout.setTag(this.activeCallChoices);
                        }
                        else {
                            this.choiceLayout.setChoices(this.activeCallMutedChoices, 0, this.choiceListener, 0.5f);
                            this.choiceLayout.setTag(this.activeCallMutedChoices);
                        }
                    }
                    this.choiceLayout.setVisibility(View.VISIBLE);
                    break;
                case FAILED:
                    this.setCaller();
                    this.callerStatus.setText((CharSequence)this.callFailed);
                    this.choiceLayout.setVisibility(View.VISIBLE);
                    this.choiceLayout.setChoices(this.failedCallChoices, 0, this.choiceListener, 0.5f);
                    this.choiceLayout.setTag(this.failedCallChoices);
                    this.callStatusImage.setImageResource(R.drawable.icon_call_red);
                    this.callStatusImage.setVisibility(View.VISIBLE);
                    ContactUtil.setContactPhoto(this.callManager.contact, this.callManager.number, false, this.callerImage, this.roundTransformation, (ContactUtil.IContactCallback)this);
                    break;
            }
            if (this.isTimeoutRequired(state)) {
                if (this.controller != null) {
                    CallNotification.sLogger.v("set timeout:" + state);
                    this.controller.stopTimeout(true);
                    this.controller.startTimeout(this.getTimeout(state));
                }
            }
            else if (this.controller != null) {
                CallNotification.sLogger.v("reset timeout:" + state);
                this.controller.stopTimeout(true);
            }
        }
    }
}
