package com.navdy.hud.app.framework.contacts;

import android.text.TextUtils;
import com.navdy.service.library.events.RequestStatus;
import com.navdy.hud.app.event.DriverProfileChanged;
import com.squareup.otto.Subscribe;
import com.navdy.service.library.events.connection.ConnectionStateChange;
import com.navdy.service.library.events.photo.PhotoResponse;
import com.navdy.service.library.util.IOUtils;
import com.navdy.hud.app.util.picasso.PicassoUtil;
import com.navdy.service.library.task.TaskManager;
import com.squareup.wire.Message;
import com.navdy.hud.app.event.RemoteEvent;
import com.navdy.service.library.events.photo.PhotoRequest;
import com.navdy.hud.app.framework.DriverProfileHelper;
import com.navdy.hud.app.util.GenericUtil;
import java.io.File;
import com.navdy.hud.app.profile.DriverProfile;
import com.navdy.service.library.events.photo.PhotoType;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.HudApplication;
import java.util.Comparator;
import android.content.Context;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.HashSet;
import com.squareup.otto.Bus;
import com.navdy.service.library.log.Logger;

public class PhoneImageDownloader
{
    private static final String MD5_EXTENSION = ".md5";
    private static final boolean VERBOSE = false;
    private static final PhoneImageDownloader sInstance;
    private static final Logger sLogger;
    private Bus bus;
    private HashSet<String> contactPhotoChecked;
    private PriorityBlockingQueue<Info> contactQueue;
    private Context context;
    private volatile String currentDriverProfileId;
    private boolean downloading;
    private Object lockObj;
    private Comparator<Info> priorityComparator;
    
    static {
        sLogger = new Logger(PhoneImageDownloader.class);
        sInstance = new PhoneImageDownloader();
    }
    
    private PhoneImageDownloader() {
        this.priorityComparator = new Comparator<Info>() {
            @Override
            public int compare(final Info info, final Info info2) {
                int n;
                if (info instanceof Info && info2 instanceof Info) {
                    n = info.priority - info2.priority;
                }
                else {
                    n = 0;
                }
                return n;
            }
        };
        this.context = HudApplication.getAppContext();
        this.contactQueue = new PriorityBlockingQueue<Info>(10, this.priorityComparator);
        this.lockObj = new Object();
        this.contactPhotoChecked = new HashSet<String>();
        (this.bus = RemoteDeviceManager.getInstance().getBus()).register(this);
    }
    
    private void download() {
        while (true) {
            while (true) {
                Object bus = null;
                Label_0225: {
                    try {
                        final Object lockObj = this.lockObj;
                        synchronized (lockObj) {
                            if (this.contactQueue.size() == 0) {
                                PhoneImageDownloader.sLogger.v("no request pending");
                                this.downloading = false;
                            }
                            else {
                                bus = this.contactQueue.remove();
                                if (((Info)bus).priority == Priority.NORMAL.getValue()) {
                                    GenericUtil.sleep(500);
                                }
                                this.currentDriverProfileId = DriverProfileHelper.getInstance().getCurrentProfile().getProfileName();
                                final String hash = this.hashFor(((Info)bus).sourceIdentifier, ((Info)bus).photoType);
                                if (hash != null) {
                                    break Label_0225;
                                }
                                PhoneImageDownloader.sLogger.v("sending fresh download request file [" + ((Info)bus).fileName + "] id[" + ((Info)bus).sourceIdentifier + "]");
                                final PhotoRequest photoRequest = new PhotoRequest(((Info)bus).sourceIdentifier, hash, ((Info)bus).photoType, ((Info)bus).displayName);
                                bus = this.bus;
                                ((Bus)bus).post(new RemoteEvent(photoRequest));
                            }
                            // monitorexit(lockObj)
                            return;
                        }
                    }
                    catch (Throwable t) {
                        PhoneImageDownloader.sLogger.e(t);
                        this.invokeDownload();
                        return;
                    }
                }
                PhoneImageDownloader.sLogger.v("checking for update file [" + ((Info)bus).fileName + "] id[" + ((Info)bus).sourceIdentifier + "]");
                continue;
            }
        }
    }
    
    public static final PhoneImageDownloader getInstance() {
        return PhoneImageDownloader.sInstance;
    }
    
    private File getPhotoPath(final DriverProfile driverProfile, final PhotoType photoType) {
        File file = null;
        switch (photoType) {
            default:
                file = null;
                break;
            case PHOTO_ALBUM_ART:
                file = driverProfile.getMusicImageDir();
                break;
            case PHOTO_CONTACT:
                file = driverProfile.getContactsImageDir();
                break;
            case PHOTO_DRIVER_PROFILE:
                file = driverProfile.getPreferencesDirectory();
                break;
        }
        return file;
    }
    
    private void invokeDownload() {
        this.currentDriverProfileId = null;
        TaskManager.getInstance().execute(new Runnable() {
            @Override
            public void run() {
                PhoneImageDownloader.this.download();
            }
        }, 7);
    }
    
    private String normalizedFilename(String normalizeToFilename, final PhotoType photoType) {
        if (photoType == PhotoType.PHOTO_DRIVER_PROFILE) {
            normalizeToFilename = "DriverImage";
        }
        else {
            normalizeToFilename = GenericUtil.normalizeToFilename(normalizeToFilename);
        }
        return normalizeToFilename;
    }
    
    private void purgeQueue() {
        PhoneImageDownloader.sLogger.i("purging queue");
        this.currentDriverProfileId = null;
        final Object lockObj = this.lockObj;
        synchronized (lockObj) {
            this.contactQueue.clear();
            this.downloading = false;
        }
    }
    
    private void removeImage(final String s, final PhotoType photoType) {
        if (s == null || photoType == null) {
            PhoneImageDownloader.sLogger.i("null param");
        }
        else {
            final File imagePath = this.getImagePath(s, photoType);
            PicassoUtil.getInstance().invalidate(imagePath);
            TaskManager.getInstance().execute(new Runnable() {
                @Override
                public void run() {
                    if (IOUtils.deleteFile(PhoneImageDownloader.this.context, imagePath.getAbsolutePath())) {
                        PhoneImageDownloader.sLogger.v("removed [" + s + "]");
                    }
                    PhoneImageDownloader.this.bus.post(new PhotoDownloadStatus(s, photoType, false, false));
                }
            }, 7);
        }
    }
    
    private void storePhoto(final PhotoResponse photoResponse) {
        if (photoResponse.photo == null) {
            PhoneImageDownloader.sLogger.v("photo is upto-date type[" + photoResponse.photoType + "] id[" + photoResponse.identifier + "]");
            this.bus.post(new PhotoDownloadStatus(photoResponse.identifier, photoResponse.photoType, true, true));
            this.invokeDownload();
        }
        else {
            TaskManager.getInstance().execute(new Runnable() {
                @Override
                public void run() {
                    // 
                    This method could not be decompiled.
                    // 
                    // Original Bytecode:
                    // 
                    //     3: ldc             "storing photo"
                    //     5: invokevirtual   com/navdy/service/library/log/Logger.v:(Ljava/lang/String;)V
                    //     8: aconst_null    
                    //     9: astore_1       
                    //    10: aconst_null    
                    //    11: astore_2       
                    //    12: aconst_null    
                    //    13: astore_3       
                    //    14: aconst_null    
                    //    15: astore          4
                    //    17: aconst_null    
                    //    18: astore          5
                    //    20: aconst_null    
                    //    21: astore          6
                    //    23: aconst_null    
                    //    24: astore          7
                    //    26: aload_2        
                    //    27: astore          8
                    //    29: aload           5
                    //    31: astore          9
                    //    33: aload_0        
                    //    34: getfield        com/navdy/hud/app/framework/contacts/PhoneImageDownloader$3.val$event:Lcom/navdy/service/library/events/photo/PhotoResponse;
                    //    37: getfield        com/navdy/service/library/events/photo/PhotoResponse.photo:Lokio/ByteString;
                    //    40: invokevirtual   okio/ByteString.toByteArray:()[B
                    //    43: astore          10
                    //    45: aload_2        
                    //    46: astore          8
                    //    48: aload           5
                    //    50: astore          9
                    //    52: aload           10
                    //    54: iconst_0       
                    //    55: aload           10
                    //    57: arraylength    
                    //    58: invokestatic    android/graphics/BitmapFactory.decodeByteArray:([BII)Landroid/graphics/Bitmap;
                    //    61: astore          11
                    //    63: aload_2        
                    //    64: astore          8
                    //    66: aload           5
                    //    68: astore          9
                    //    70: aload_0        
                    //    71: getfield        com/navdy/hud/app/framework/contacts/PhoneImageDownloader$3.this$0:Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;
                    //    74: aload_0        
                    //    75: getfield        com/navdy/hud/app/framework/contacts/PhoneImageDownloader$3.val$event:Lcom/navdy/service/library/events/photo/PhotoResponse;
                    //    78: getfield        com/navdy/service/library/events/photo/PhotoResponse.identifier:Ljava/lang/String;
                    //    81: aload_0        
                    //    82: getfield        com/navdy/hud/app/framework/contacts/PhoneImageDownloader$3.val$event:Lcom/navdy/service/library/events/photo/PhotoResponse;
                    //    85: getfield        com/navdy/service/library/events/photo/PhotoResponse.photoType:Lcom/navdy/service/library/events/photo/PhotoType;
                    //    88: invokestatic    com/navdy/hud/app/framework/contacts/PhoneImageDownloader.access$200:(Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;Ljava/lang/String;Lcom/navdy/service/library/events/photo/PhotoType;)Ljava/lang/String;
                    //    91: astore          12
                    //    93: aload           11
                    //    95: ifnull          662
                    //    98: aload_2        
                    //    99: astore          8
                    //   101: aload           5
                    //   103: astore          9
                    //   105: invokestatic    com/navdy/hud/app/framework/DriverProfileHelper.getInstance:()Lcom/navdy/hud/app/framework/DriverProfileHelper;
                    //   108: invokevirtual   com/navdy/hud/app/framework/DriverProfileHelper.getCurrentProfile:()Lcom/navdy/hud/app/profile/DriverProfile;
                    //   111: astore          11
                    //   113: aload_2        
                    //   114: astore          8
                    //   116: aload           5
                    //   118: astore          9
                    //   120: aload           11
                    //   122: invokevirtual   com/navdy/hud/app/profile/DriverProfile.getProfileName:()Ljava/lang/String;
                    //   125: aload_0        
                    //   126: getfield        com/navdy/hud/app/framework/contacts/PhoneImageDownloader$3.this$0:Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;
                    //   129: invokestatic    com/navdy/hud/app/framework/contacts/PhoneImageDownloader.access$300:(Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;)Ljava/lang/String;
                    //   132: invokestatic    android/text/TextUtils.equals:(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z
                    //   135: ifeq            424
                    //   138: aload_2        
                    //   139: astore          8
                    //   141: aload           5
                    //   143: astore          9
                    //   145: new             Ljava/io/File;
                    //   148: astore          7
                    //   150: aload_2        
                    //   151: astore          8
                    //   153: aload           5
                    //   155: astore          9
                    //   157: aload           7
                    //   159: aload_0        
                    //   160: getfield        com/navdy/hud/app/framework/contacts/PhoneImageDownloader$3.this$0:Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;
                    //   163: aload           11
                    //   165: aload_0        
                    //   166: getfield        com/navdy/hud/app/framework/contacts/PhoneImageDownloader$3.val$event:Lcom/navdy/service/library/events/photo/PhotoResponse;
                    //   169: getfield        com/navdy/service/library/events/photo/PhotoResponse.photoType:Lcom/navdy/service/library/events/photo/PhotoType;
                    //   172: invokestatic    com/navdy/hud/app/framework/contacts/PhoneImageDownloader.access$400:(Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;Lcom/navdy/hud/app/profile/DriverProfile;Lcom/navdy/service/library/events/photo/PhotoType;)Ljava/io/File;
                    //   175: aload           12
                    //   177: invokespecial   java/io/File.<init>:(Ljava/io/File;Ljava/lang/String;)V
                    //   180: aload_2        
                    //   181: astore          8
                    //   183: aload           5
                    //   185: astore          9
                    //   187: new             Ljava/io/FileOutputStream;
                    //   190: astore_3       
                    //   191: aload_2        
                    //   192: astore          8
                    //   194: aload           5
                    //   196: astore          9
                    //   198: aload_3        
                    //   199: aload           7
                    //   201: invokespecial   java/io/FileOutputStream.<init>:(Ljava/io/File;)V
                    //   204: aload_3        
                    //   205: aload           10
                    //   207: invokevirtual   java/io/FileOutputStream.write:([B)V
                    //   210: aload           10
                    //   212: invokestatic    com/navdy/service/library/util/IOUtils.hashForBytes:([B)Ljava/lang/String;
                    //   215: astore          9
                    //   217: new             Ljava/io/File;
                    //   220: astore_1       
                    //   221: aload_0        
                    //   222: getfield        com/navdy/hud/app/framework/contacts/PhoneImageDownloader$3.this$0:Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;
                    //   225: aload           11
                    //   227: aload_0        
                    //   228: getfield        com/navdy/hud/app/framework/contacts/PhoneImageDownloader$3.val$event:Lcom/navdy/service/library/events/photo/PhotoResponse;
                    //   231: getfield        com/navdy/service/library/events/photo/PhotoResponse.photoType:Lcom/navdy/service/library/events/photo/PhotoType;
                    //   234: invokestatic    com/navdy/hud/app/framework/contacts/PhoneImageDownloader.access$400:(Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;Lcom/navdy/hud/app/profile/DriverProfile;Lcom/navdy/service/library/events/photo/PhotoType;)Ljava/io/File;
                    //   237: astore          8
                    //   239: new             Ljava/lang/StringBuilder;
                    //   242: astore_2       
                    //   243: aload_2        
                    //   244: invokespecial   java/lang/StringBuilder.<init>:()V
                    //   247: aload_1        
                    //   248: aload           8
                    //   250: aload_2        
                    //   251: aload           12
                    //   253: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
                    //   256: ldc             ".md5"
                    //   258: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
                    //   261: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
                    //   264: invokespecial   java/io/File.<init>:(Ljava/io/File;Ljava/lang/String;)V
                    //   267: new             Ljava/io/FileOutputStream;
                    //   270: astore          8
                    //   272: aload           8
                    //   274: aload_1        
                    //   275: invokespecial   java/io/FileOutputStream.<init>:(Ljava/io/File;)V
                    //   278: aload           8
                    //   280: aload           9
                    //   282: invokevirtual   java/lang/String.getBytes:()[B
                    //   285: invokevirtual   java/io/FileOutputStream.write:([B)V
                    //   288: invokestatic    com/navdy/hud/app/framework/contacts/PhoneImageDownloader.access$100:()Lcom/navdy/service/library/log/Logger;
                    //   291: astore          9
                    //   293: new             Ljava/lang/StringBuilder;
                    //   296: astore          4
                    //   298: aload           4
                    //   300: invokespecial   java/lang/StringBuilder.<init>:()V
                    //   303: aload           9
                    //   305: aload           4
                    //   307: ldc             "photo saved["
                    //   309: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
                    //   312: aload_0        
                    //   313: getfield        com/navdy/hud/app/framework/contacts/PhoneImageDownloader$3.val$event:Lcom/navdy/service/library/events/photo/PhotoResponse;
                    //   316: getfield        com/navdy/service/library/events/photo/PhotoResponse.photoType:Lcom/navdy/service/library/events/photo/PhotoType;
                    //   319: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/Object;)Ljava/lang/StringBuilder;
                    //   322: ldc             "] id["
                    //   324: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
                    //   327: aload_0        
                    //   328: getfield        com/navdy/hud/app/framework/contacts/PhoneImageDownloader$3.val$event:Lcom/navdy/service/library/events/photo/PhotoResponse;
                    //   331: getfield        com/navdy/service/library/events/photo/PhotoResponse.identifier:Ljava/lang/String;
                    //   334: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
                    //   337: ldc             "]"
                    //   339: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
                    //   342: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
                    //   345: invokevirtual   com/navdy/service/library/log/Logger.v:(Ljava/lang/String;)V
                    //   348: invokestatic    com/navdy/hud/app/util/picasso/PicassoUtil.getInstance:()Lcom/squareup/picasso/Picasso;
                    //   351: aload           7
                    //   353: invokevirtual   com/squareup/picasso/Picasso.invalidate:(Ljava/io/File;)V
                    //   356: aload_0        
                    //   357: getfield        com/navdy/hud/app/framework/contacts/PhoneImageDownloader$3.this$0:Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;
                    //   360: invokestatic    com/navdy/hud/app/framework/contacts/PhoneImageDownloader.access$500:(Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;)Lcom/squareup/otto/Bus;
                    //   363: astore          4
                    //   365: new             Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$PhotoDownloadStatus;
                    //   368: astore          9
                    //   370: aload           9
                    //   372: aload_0        
                    //   373: getfield        com/navdy/hud/app/framework/contacts/PhoneImageDownloader$3.val$event:Lcom/navdy/service/library/events/photo/PhotoResponse;
                    //   376: getfield        com/navdy/service/library/events/photo/PhotoResponse.identifier:Ljava/lang/String;
                    //   379: aload_0        
                    //   380: getfield        com/navdy/hud/app/framework/contacts/PhoneImageDownloader$3.val$event:Lcom/navdy/service/library/events/photo/PhotoResponse;
                    //   383: getfield        com/navdy/service/library/events/photo/PhotoResponse.photoType:Lcom/navdy/service/library/events/photo/PhotoType;
                    //   386: iconst_1       
                    //   387: iconst_0       
                    //   388: invokespecial   com/navdy/hud/app/framework/contacts/PhoneImageDownloader$PhotoDownloadStatus.<init>:(Ljava/lang/String;Lcom/navdy/service/library/events/photo/PhotoType;ZZ)V
                    //   391: aload           4
                    //   393: aload           9
                    //   395: invokevirtual   com/squareup/otto/Bus.post:(Ljava/lang/Object;)V
                    //   398: aload_3        
                    //   399: invokestatic    com/navdy/service/library/util/IOUtils.fileSync:(Ljava/io/FileOutputStream;)V
                    //   402: aload_3        
                    //   403: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
                    //   406: aload           8
                    //   408: invokestatic    com/navdy/service/library/util/IOUtils.fileSync:(Ljava/io/FileOutputStream;)V
                    //   411: aload           8
                    //   413: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
                    //   416: aload_0        
                    //   417: getfield        com/navdy/hud/app/framework/contacts/PhoneImageDownloader$3.this$0:Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;
                    //   420: invokestatic    com/navdy/hud/app/framework/contacts/PhoneImageDownloader.access$600:(Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;)V
                    //   423: return         
                    //   424: aload_2        
                    //   425: astore          8
                    //   427: aload           5
                    //   429: astore          9
                    //   431: invokestatic    com/navdy/hud/app/framework/contacts/PhoneImageDownloader.access$100:()Lcom/navdy/service/library/log/Logger;
                    //   434: astore          12
                    //   436: aload_2        
                    //   437: astore          8
                    //   439: aload           5
                    //   441: astore          9
                    //   443: new             Ljava/lang/StringBuilder;
                    //   446: astore          6
                    //   448: aload_2        
                    //   449: astore          8
                    //   451: aload           5
                    //   453: astore          9
                    //   455: aload           6
                    //   457: invokespecial   java/lang/StringBuilder.<init>:()V
                    //   460: aload_2        
                    //   461: astore          8
                    //   463: aload           5
                    //   465: astore          9
                    //   467: aload           12
                    //   469: aload           6
                    //   471: ldc             "photo cannot save ["
                    //   473: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
                    //   476: aload_0        
                    //   477: getfield        com/navdy/hud/app/framework/contacts/PhoneImageDownloader$3.val$event:Lcom/navdy/service/library/events/photo/PhotoResponse;
                    //   480: getfield        com/navdy/service/library/events/photo/PhotoResponse.photoType:Lcom/navdy/service/library/events/photo/PhotoType;
                    //   483: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/Object;)Ljava/lang/StringBuilder;
                    //   486: ldc             "] id["
                    //   488: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
                    //   491: aload_0        
                    //   492: getfield        com/navdy/hud/app/framework/contacts/PhoneImageDownloader$3.val$event:Lcom/navdy/service/library/events/photo/PhotoResponse;
                    //   495: getfield        com/navdy/service/library/events/photo/PhotoResponse.identifier:Ljava/lang/String;
                    //   498: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
                    //   501: ldc             "] profile changed from ["
                    //   503: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
                    //   506: aload_0        
                    //   507: getfield        com/navdy/hud/app/framework/contacts/PhoneImageDownloader$3.this$0:Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;
                    //   510: invokestatic    com/navdy/hud/app/framework/contacts/PhoneImageDownloader.access$300:(Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;)Ljava/lang/String;
                    //   513: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
                    //   516: ldc             "] to ["
                    //   518: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
                    //   521: aload           11
                    //   523: invokevirtual   com/navdy/hud/app/profile/DriverProfile.getProfileName:()Ljava/lang/String;
                    //   526: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
                    //   529: ldc             "]"
                    //   531: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
                    //   534: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
                    //   537: invokevirtual   com/navdy/service/library/log/Logger.w:(Ljava/lang/String;)V
                    //   540: aload           7
                    //   542: astore          8
                    //   544: goto            398
                    //   547: astore          6
                    //   549: aload_1        
                    //   550: astore_3       
                    //   551: aload_3        
                    //   552: astore          8
                    //   554: aload           4
                    //   556: astore          9
                    //   558: invokestatic    com/navdy/hud/app/framework/contacts/PhoneImageDownloader.access$100:()Lcom/navdy/service/library/log/Logger;
                    //   561: aload           6
                    //   563: invokevirtual   com/navdy/service/library/log/Logger.e:(Ljava/lang/Throwable;)V
                    //   566: aload_3        
                    //   567: astore          8
                    //   569: aload           4
                    //   571: astore          9
                    //   573: aload_0        
                    //   574: getfield        com/navdy/hud/app/framework/contacts/PhoneImageDownloader$3.this$0:Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;
                    //   577: invokestatic    com/navdy/hud/app/framework/contacts/PhoneImageDownloader.access$500:(Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;)Lcom/squareup/otto/Bus;
                    //   580: astore_1       
                    //   581: aload_3        
                    //   582: astore          8
                    //   584: aload           4
                    //   586: astore          9
                    //   588: new             Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$PhotoDownloadStatus;
                    //   591: astore          6
                    //   593: aload_3        
                    //   594: astore          8
                    //   596: aload           4
                    //   598: astore          9
                    //   600: aload           6
                    //   602: aload_0        
                    //   603: getfield        com/navdy/hud/app/framework/contacts/PhoneImageDownloader$3.val$event:Lcom/navdy/service/library/events/photo/PhotoResponse;
                    //   606: getfield        com/navdy/service/library/events/photo/PhotoResponse.identifier:Ljava/lang/String;
                    //   609: aload_0        
                    //   610: getfield        com/navdy/hud/app/framework/contacts/PhoneImageDownloader$3.val$event:Lcom/navdy/service/library/events/photo/PhotoResponse;
                    //   613: getfield        com/navdy/service/library/events/photo/PhotoResponse.photoType:Lcom/navdy/service/library/events/photo/PhotoType;
                    //   616: iconst_0       
                    //   617: iconst_0       
                    //   618: invokespecial   com/navdy/hud/app/framework/contacts/PhoneImageDownloader$PhotoDownloadStatus.<init>:(Ljava/lang/String;Lcom/navdy/service/library/events/photo/PhotoType;ZZ)V
                    //   621: aload_3        
                    //   622: astore          8
                    //   624: aload           4
                    //   626: astore          9
                    //   628: aload_1        
                    //   629: aload           6
                    //   631: invokevirtual   com/squareup/otto/Bus.post:(Ljava/lang/Object;)V
                    //   634: aload_3        
                    //   635: invokestatic    com/navdy/service/library/util/IOUtils.fileSync:(Ljava/io/FileOutputStream;)V
                    //   638: aload_3        
                    //   639: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
                    //   642: aload           4
                    //   644: invokestatic    com/navdy/service/library/util/IOUtils.fileSync:(Ljava/io/FileOutputStream;)V
                    //   647: aload           4
                    //   649: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
                    //   652: aload_0        
                    //   653: getfield        com/navdy/hud/app/framework/contacts/PhoneImageDownloader$3.this$0:Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;
                    //   656: invokestatic    com/navdy/hud/app/framework/contacts/PhoneImageDownloader.access$600:(Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;)V
                    //   659: goto            423
                    //   662: aload_2        
                    //   663: astore          8
                    //   665: aload           5
                    //   667: astore          9
                    //   669: aload_0        
                    //   670: getfield        com/navdy/hud/app/framework/contacts/PhoneImageDownloader$3.this$0:Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;
                    //   673: invokestatic    com/navdy/hud/app/framework/contacts/PhoneImageDownloader.access$500:(Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;)Lcom/squareup/otto/Bus;
                    //   676: astore          6
                    //   678: aload_2        
                    //   679: astore          8
                    //   681: aload           5
                    //   683: astore          9
                    //   685: new             Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader$PhotoDownloadStatus;
                    //   688: astore          12
                    //   690: aload_2        
                    //   691: astore          8
                    //   693: aload           5
                    //   695: astore          9
                    //   697: aload           12
                    //   699: aload_0        
                    //   700: getfield        com/navdy/hud/app/framework/contacts/PhoneImageDownloader$3.val$event:Lcom/navdy/service/library/events/photo/PhotoResponse;
                    //   703: getfield        com/navdy/service/library/events/photo/PhotoResponse.identifier:Ljava/lang/String;
                    //   706: aload_0        
                    //   707: getfield        com/navdy/hud/app/framework/contacts/PhoneImageDownloader$3.val$event:Lcom/navdy/service/library/events/photo/PhotoResponse;
                    //   710: getfield        com/navdy/service/library/events/photo/PhotoResponse.photoType:Lcom/navdy/service/library/events/photo/PhotoType;
                    //   713: iconst_0       
                    //   714: iconst_0       
                    //   715: invokespecial   com/navdy/hud/app/framework/contacts/PhoneImageDownloader$PhotoDownloadStatus.<init>:(Ljava/lang/String;Lcom/navdy/service/library/events/photo/PhotoType;ZZ)V
                    //   718: aload_2        
                    //   719: astore          8
                    //   721: aload           5
                    //   723: astore          9
                    //   725: aload           6
                    //   727: aload           12
                    //   729: invokevirtual   com/squareup/otto/Bus.post:(Ljava/lang/Object;)V
                    //   732: aload           7
                    //   734: astore          8
                    //   736: goto            398
                    //   739: astore_3       
                    //   740: aload           8
                    //   742: invokestatic    com/navdy/service/library/util/IOUtils.fileSync:(Ljava/io/FileOutputStream;)V
                    //   745: aload           8
                    //   747: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
                    //   750: aload           9
                    //   752: invokestatic    com/navdy/service/library/util/IOUtils.fileSync:(Ljava/io/FileOutputStream;)V
                    //   755: aload           9
                    //   757: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
                    //   760: aload_0        
                    //   761: getfield        com/navdy/hud/app/framework/contacts/PhoneImageDownloader$3.this$0:Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;
                    //   764: invokestatic    com/navdy/hud/app/framework/contacts/PhoneImageDownloader.access$600:(Lcom/navdy/hud/app/framework/contacts/PhoneImageDownloader;)V
                    //   767: aload_3        
                    //   768: athrow         
                    //   769: astore          4
                    //   771: aload_3        
                    //   772: astore          8
                    //   774: aload           6
                    //   776: astore          9
                    //   778: aload           4
                    //   780: astore_3       
                    //   781: goto            740
                    //   784: astore          4
                    //   786: aload           8
                    //   788: astore          9
                    //   790: aload_3        
                    //   791: astore          8
                    //   793: aload           4
                    //   795: astore_3       
                    //   796: goto            740
                    //   799: astore          6
                    //   801: goto            551
                    //   804: astore          6
                    //   806: aload           8
                    //   808: astore          4
                    //   810: goto            551
                    //    Exceptions:
                    //  Try           Handler
                    //  Start  End    Start  End    Type                 
                    //  -----  -----  -----  -----  ---------------------
                    //  33     45     547    551    Ljava/lang/Throwable;
                    //  33     45     739    740    Any
                    //  52     63     547    551    Ljava/lang/Throwable;
                    //  52     63     739    740    Any
                    //  70     93     547    551    Ljava/lang/Throwable;
                    //  70     93     739    740    Any
                    //  105    113    547    551    Ljava/lang/Throwable;
                    //  105    113    739    740    Any
                    //  120    138    547    551    Ljava/lang/Throwable;
                    //  120    138    739    740    Any
                    //  145    150    547    551    Ljava/lang/Throwable;
                    //  145    150    739    740    Any
                    //  157    180    547    551    Ljava/lang/Throwable;
                    //  157    180    739    740    Any
                    //  187    191    547    551    Ljava/lang/Throwable;
                    //  187    191    739    740    Any
                    //  198    204    547    551    Ljava/lang/Throwable;
                    //  198    204    739    740    Any
                    //  204    278    799    804    Ljava/lang/Throwable;
                    //  204    278    769    784    Any
                    //  278    398    804    813    Ljava/lang/Throwable;
                    //  278    398    784    799    Any
                    //  431    436    547    551    Ljava/lang/Throwable;
                    //  431    436    739    740    Any
                    //  443    448    547    551    Ljava/lang/Throwable;
                    //  443    448    739    740    Any
                    //  455    460    547    551    Ljava/lang/Throwable;
                    //  455    460    739    740    Any
                    //  467    540    547    551    Ljava/lang/Throwable;
                    //  467    540    739    740    Any
                    //  558    566    739    740    Any
                    //  573    581    739    740    Any
                    //  588    593    739    740    Any
                    //  600    621    739    740    Any
                    //  628    634    739    740    Any
                    //  669    678    547    551    Ljava/lang/Throwable;
                    //  669    678    739    740    Any
                    //  685    690    547    551    Ljava/lang/Throwable;
                    //  685    690    739    740    Any
                    //  697    718    547    551    Ljava/lang/Throwable;
                    //  697    718    739    740    Any
                    //  725    732    547    551    Ljava/lang/Throwable;
                    //  725    732    739    740    Any
                    // 
                    // The error that occurred was:
                    // 
                    // java.lang.IndexOutOfBoundsException: Index: 384, Size: 384
                    //     at java.util.ArrayList.rangeCheck(Unknown Source)
                    //     at java.util.ArrayList.get(Unknown Source)
                    //     at com.strobel.decompiler.ast.AstBuilder.convertToAst(AstBuilder.java:3321)
                    //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:113)
                    //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:210)
                    //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
                    //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
                    //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
                    //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
                    //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
                    //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
                    //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
                    //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformCall(AstMethodBodyBuilder.java:1163)
                    //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformByteCode(AstMethodBodyBuilder.java:1010)
                    //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformExpression(AstMethodBodyBuilder.java:540)
                    //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformByteCode(AstMethodBodyBuilder.java:554)
                    //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformExpression(AstMethodBodyBuilder.java:540)
                    //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformNode(AstMethodBodyBuilder.java:392)
                    //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformBlock(AstMethodBodyBuilder.java:333)
                    //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformNode(AstMethodBodyBuilder.java:437)
                    //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformBlock(AstMethodBodyBuilder.java:333)
                    //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:294)
                    //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
                    //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
                    //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
                    //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
                    //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
                    //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
                    //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
                    //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
                    //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
                    //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
                    //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
                    //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
                    //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
                    //     at java.lang.Thread.run(Unknown Source)
                    // 
                    throw new IllegalStateException("An error occurred while decompiling this method.");
                }
            }, 7);
        }
    }
    
    public void clearAllPhotoCheckEntries() {
        final Object lockObj = this.lockObj;
        synchronized (lockObj) {
            this.contactPhotoChecked.clear();
            PhoneImageDownloader.sLogger.v("cleared all photo check entries");
        }
    }
    
    public void clearPhotoCheckEntry(final String s, final PhotoType photoType) {
        final String normalizedFilename = this.normalizedFilename(s, photoType);
        final Object lockObj = this.lockObj;
        synchronized (lockObj) {
            if (this.contactPhotoChecked.remove(normalizedFilename)) {
                PhoneImageDownloader.sLogger.v("photo check entry removed [" + normalizedFilename + "]");
            }
        }
    }
    
    public File getImagePath(final String s, final PhotoType photoType) {
        return this.getImagePath(s, "", photoType);
    }
    
    public File getImagePath(String normalizedFilename, final String s, final PhotoType photoType) {
        normalizedFilename = this.normalizedFilename(normalizedFilename, photoType);
        final File photoPath = this.getPhotoPath(DriverProfileHelper.getInstance().getCurrentProfile(), photoType);
        File file;
        if (photoPath == null) {
            file = null;
        }
        else {
            file = new File(photoPath, normalizedFilename + s);
        }
        return file;
    }
    
    public String hashFor(String convertFileToString, PhotoType photoType) {
        final String s = null;
        final String normalizedFilename = this.normalizedFilename(convertFileToString, photoType);
        final File photoPath = this.getPhotoPath(DriverProfileHelper.getInstance().getCurrentProfile(), photoType);
        photoType = (PhotoType)new File(photoPath, normalizedFilename);
        convertFileToString = s;
        if (((File)photoType).exists()) {
            convertFileToString = s;
            if (!((File)photoType).isDirectory()) {
                final File file = new File(photoPath, normalizedFilename + ".md5");
                if (!file.exists()) {
                    PhoneImageDownloader.sLogger.w("md5 does not exist, delete file, " + normalizedFilename);
                    IOUtils.deleteFile(this.context, ((File)photoType).getAbsolutePath());
                    convertFileToString = s;
                }
                else {
                    try {
                        convertFileToString = IOUtils.convertFileToString(file.getAbsolutePath());
                    }
                    catch (Throwable t) {
                        PhoneImageDownloader.sLogger.e("error reading md5[" + normalizedFilename + "]", t);
                        IOUtils.deleteFile(this.context, ((File)photoType).getAbsolutePath());
                        IOUtils.deleteFile(this.context, file.getAbsolutePath());
                        convertFileToString = s;
                    }
                }
            }
        }
        return convertFileToString;
    }
    
    @Subscribe
    public void onConnectionStatusChange(final ConnectionStateChange connectionStateChange) {
        switch (connectionStateChange.state) {
            case CONNECTION_DISCONNECTED:
                this.purgeQueue();
                break;
        }
    }
    
    @Subscribe
    public void onDriverProfileChanged(final DriverProfileChanged driverProfileChanged) {
        this.purgeQueue();
    }
    
    @Subscribe
    public void onPhotoResponse(final PhotoResponse photoResponse) {
        PhoneImageDownloader.sLogger.v("got photoResponse:" + photoResponse.status + " type[" + photoResponse.photoType + "] id[" + photoResponse.identifier + "]");
        if (this.currentDriverProfileId == null) {
            PhoneImageDownloader.sLogger.w("currentDriverProfile is null");
            this.invokeDownload();
        }
        else {
            final String normalizedFilename = this.normalizedFilename(photoResponse.identifier, photoResponse.photoType);
            if (photoResponse.status == RequestStatus.REQUEST_SUCCESS) {
                this.contactPhotoChecked.add(normalizedFilename);
                this.storePhoto(photoResponse);
            }
            else {
                if (photoResponse.status == RequestStatus.REQUEST_NOT_AVAILABLE) {
                    this.contactPhotoChecked.add(normalizedFilename);
                    this.removeImage(photoResponse.identifier, photoResponse.photoType);
                    PhoneImageDownloader.sLogger.v("photo not available type[" + photoResponse.photoType + "] id[" + photoResponse.identifier + "]");
                }
                else {
                    PhoneImageDownloader.sLogger.v("photo not available type[" + photoResponse.photoType + "] id[" + photoResponse.identifier + "] error[" + photoResponse.status + "]");
                }
                this.invokeDownload();
            }
        }
    }
    
    public void submitDownload(final String s, Priority sLogger, final PhotoType photoType, final String s2) {
        if (TextUtils.isEmpty((CharSequence)s)) {
            PhoneImageDownloader.sLogger.w("invalid id passed");
        }
        else {
            final String normalizedFilename = this.normalizedFilename(s, photoType);
            final Object lockObj = this.lockObj;
            synchronized (lockObj) {
                if (photoType == PhotoType.PHOTO_CONTACT && this.contactPhotoChecked.contains(normalizedFilename)) {
                    sLogger = (Priority)PhoneImageDownloader.sLogger;
                    ((Logger)sLogger).v("contact photo request has already been processed name[" + normalizedFilename + "]");
                    return;
                }
            }
            PhoneImageDownloader.sLogger.v("submitting photo request name[" + normalizedFilename + "] type [" + photoType + "] displayName[" + s2 + "]");
            final String s3;
            this.contactQueue.add(new Info(normalizedFilename, sLogger, photoType, s3, s2));
            if (!this.downloading) {
                this.downloading = true;
                this.invokeDownload();
            }
        }
        // monitorexit(o)
    }
    
    private static class Info
    {
        String displayName;
        String fileName;
        PhotoType photoType;
        int priority;
        String sourceIdentifier;
        
        Info(final String fileName, final Priority priority, final PhotoType photoType, final String sourceIdentifier, final String displayName) {
            this.fileName = fileName;
            this.priority = priority.getValue();
            this.photoType = photoType;
            this.sourceIdentifier = sourceIdentifier;
            this.displayName = displayName;
        }
    }
    
    public static class PhotoDownloadStatus
    {
        public boolean alreadyDownloaded;
        public PhotoType photoType;
        public String sourceIdentifier;
        public boolean success;
        
        PhotoDownloadStatus(final String sourceIdentifier, final PhotoType photoType, final boolean success, final boolean alreadyDownloaded) {
            this.sourceIdentifier = sourceIdentifier;
            this.photoType = photoType;
            this.success = success;
            this.alreadyDownloaded = alreadyDownloaded;
        }
    }
    
    public enum Priority
    {
        HIGH(2), 
        NORMAL(1);
        
        private final int val;
        
        private Priority(final int val) {
            this.val = val;
        }
        
        public int getValue() {
            return this.val;
        }
    }
}
