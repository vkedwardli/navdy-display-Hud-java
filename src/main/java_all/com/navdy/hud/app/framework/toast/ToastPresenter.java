package com.navdy.hud.app.framework.toast;

import android.graphics.Bitmap;
import android.view.ViewGroup;
import com.navdy.hud.app.ui.component.ConfirmationLayout;
import android.content.Context;
import com.navdy.hud.app.device.light.LightManager;
import com.navdy.hud.app.util.picasso.PicassoUtil;
import java.io.File;
import com.navdy.hud.app.ui.component.image.InitialsImageView;
import android.view.View;
import android.view.ViewGroup.MarginLayoutParams;
import com.navdy.hud.app.view.MaxWidthLinearLayout;
import android.widget.RelativeLayout;
import com.navdy.hud.app.device.light.HUDLightUtils;
import com.navdy.hud.app.util.GenericUtil;
import com.navdy.hud.app.util.ViewUtil;
import android.os.Bundle;
import android.widget.TextView;
import android.content.res.Resources;
import android.os.Looper;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.HudApplication;
import java.util.ArrayList;
import com.navdy.hud.app.view.ToastView;
import com.navdy.service.library.log.Logger;
import android.os.Handler;
import com.navdy.hud.app.gesture.GestureServiceConnector;
import com.navdy.hud.app.device.light.LED;
import com.navdy.hud.app.ui.component.ChoiceLayout;
import com.squareup.otto.Bus;
import java.util.List;

public final class ToastPresenter
{
    public static final int ANIMATION_TRANSLATION;
    private static final List<String> DISMISS_CHOICE;
    public static final String EXTRA_CHOICE_LAYOUT_PADDING = "15";
    public static final String EXTRA_CHOICE_LIST = "20";
    public static final String EXTRA_DEFAULT_CHOICE = "12";
    public static final String EXTRA_INFO_CONTAINER_DEFAULT_MAX_WIDTH = "16_1";
    public static final String EXTRA_INFO_CONTAINER_LEFT_PADDING = "16_2";
    public static final String EXTRA_INFO_CONTAINER_MAX_WIDTH = "16";
    public static final String EXTRA_MAIN_IMAGE = "8";
    public static final String EXTRA_MAIN_IMAGE_CACHE_KEY = "9";
    public static final String EXTRA_MAIN_IMAGE_INITIALS = "10";
    public static final String EXTRA_MAIN_TITLE = "1";
    public static final String EXTRA_MAIN_TITLE_1 = "2";
    public static final String EXTRA_MAIN_TITLE_1_STYLE = "3";
    public static final String EXTRA_MAIN_TITLE_2 = "4";
    public static final String EXTRA_MAIN_TITLE_2_STYLE = "5";
    public static final String EXTRA_MAIN_TITLE_3 = "6";
    public static final String EXTRA_MAIN_TITLE_3_STYLE = "7";
    public static final String EXTRA_MAIN_TITLE_STYLE = "1_1";
    public static final String EXTRA_NO_START_DELAY = "21";
    public static final String EXTRA_SHOW_SCREEN_ID = "18";
    public static final String EXTRA_SIDE_IMAGE = "11";
    public static final String EXTRA_SUPPORTS_GESTURE = "19";
    public static final String EXTRA_TIMEOUT = "13";
    public static final String EXTRA_TOAST_ID = "id";
    public static final String EXTRA_TTS = "17";
    public static final int TOAST_ANIM_DURATION_IN = 100;
    public static final int TOAST_ANIM_DURATION_OUT = 100;
    public static final int TOAST_ANIM_DURATION_OUT_QUICK = 50;
    public static final int TOAST_START_DELAY = 250;
    private static final Bus bus;
    private static ChoiceLayout.IListener choiceListener;
    private static ToastManager.ToastInfo currentInfo;
    private static ChoiceLayout.IListener defaultChoiceListener;
    public static final int defaultMaxInfoWidth;
    private static LED.Settings gestureLedSettings;
    private static final GestureServiceConnector gestureServiceConnector;
    private static Handler handler;
    private static String id;
    private static int mainSectionBottomPadding;
    private static final Logger sLogger;
    private static volatile String screenName;
    private static boolean supportsGesture;
    private static int timeout;
    private static Runnable timeoutRunnable;
    private static IToastCallback toastCallback;
    private static ToastView toastView;
    
    static {
        sLogger = new Logger(ToastPresenter.class);
        DISMISS_CHOICE = new ArrayList<String>();
        final Resources resources = HudApplication.getAppContext().getResources();
        ToastPresenter.DISMISS_CHOICE.add(resources.getString(R.string.dismiss));
        ANIMATION_TRANSLATION = (int)resources.getDimension(R.dimen.toast_anim_translation);
        defaultMaxInfoWidth = (int)resources.getDimension(R.dimen.carousel_main_right_section_width);
        final RemoteDeviceManager instance = RemoteDeviceManager.getInstance();
        bus = instance.getBus();
        gestureServiceConnector = instance.getGestureServiceConnector();
        ToastPresenter.handler = new Handler(Looper.getMainLooper());
        ToastPresenter.timeoutRunnable = new Runnable() {
            @Override
            public void run() {
                if (ToastPresenter.toastView != null) {
                    ToastPresenter.sLogger.v("timeoutRunnable");
                    ToastPresenter.toastView.animateOut(false);
                }
            }
        };
        ToastPresenter.mainSectionBottomPadding = -1;
        ToastPresenter.defaultChoiceListener = new ChoiceLayout.IListener() {
            @Override
            public void executeItem(final int n, final int n2) {
                ToastPresenter.dismiss(false);
            }
            
            @Override
            public void itemSelected(final int n, final int n2) {
            }
        };
        ToastPresenter.choiceListener = new ChoiceLayout.IListener() {
            @Override
            public void executeItem(final int n, final int n2) {
                if (ToastPresenter.toastCallback != null) {
                    ToastPresenter.toastCallback.executeChoiceItem(n, n2);
                }
            }
            
            @Override
            public void itemSelected(final int n, final int n2) {
                ToastPresenter.resetTimeout();
            }
        };
    }
    
    private static int applyTextAndStyle(final TextView textView, final Bundle bundle, final String s, final String s2, final int n) {
        return ViewUtil.applyTextAndStyle(textView, bundle.getString(s), n, bundle.getInt(s2, -1));
    }
    
    public static void clear() {
        if (GenericUtil.isMainThread()) {
            clearInternal();
        }
        else {
            ToastPresenter.handler.post((Runnable)new Runnable() {
                @Override
                public void run() {
                    clearInternal();
                }
            });
        }
    }
    
    private static void clearInternal() {
        ToastPresenter.sLogger.v("clearInternal");
        ToastPresenter.handler.removeCallbacks(ToastPresenter.timeoutRunnable);
        if (ToastPresenter.toastCallback != null) {
            ToastPresenter.sLogger.v("called onStop():" + ToastPresenter.id);
            ToastPresenter.toastCallback.onStop();
            if (ToastPresenter.supportsGesture) {
                HUDLightUtils.removeSettings(ToastPresenter.gestureLedSettings);
            }
        }
        ToastPresenter.bus.post(new ToastManager.DismissedToast(ToastPresenter.id));
        ToastPresenter.currentInfo = null;
        ToastPresenter.supportsGesture = false;
        ToastPresenter.toastCallback = null;
        ToastPresenter.sLogger.v("toast-cb null");
        ToastPresenter.toastView = null;
        ToastPresenter.id = null;
        ToastPresenter.timeout = 0;
    }
    
    public static void clearScreenName() {
        ToastPresenter.screenName = null;
    }
    
    public static void dismiss() {
        dismiss(false);
    }
    
    public static void dismiss(final boolean b) {
        if (GenericUtil.isMainThread()) {
            dismissInternal(b);
        }
        else {
            ToastPresenter.handler.post((Runnable)new Runnable() {
                @Override
                public void run() {
                    dismissInternal(b);
                }
            });
        }
    }
    
    private static void dismissInternal(final boolean b) {
        ToastPresenter.sLogger.v("dismissInternal");
        ToastPresenter.handler.removeCallbacks(ToastPresenter.timeoutRunnable);
        if (ToastPresenter.toastView != null) {
            ToastPresenter.toastView.animateOut(b);
        }
    }
    
    public static IToastCallback getCurrentCallback() {
        return ToastPresenter.toastCallback;
    }
    
    public static String getCurrentId() {
        return ToastPresenter.id;
    }
    
    public static String getScreenName() {
        return ToastPresenter.screenName;
    }
    
    public static boolean hasTimeout() {
        return ToastPresenter.timeout > 0;
    }
    
    static void present(final ToastView toastView, final ToastManager.ToastInfo currentInfo) {
        if (toastView == null || currentInfo == null) {
            ToastPresenter.sLogger.e("invalid toast info");
        }
        else {
            ToastPresenter.currentInfo = currentInfo;
            ToastPresenter.toastView = toastView;
            if (ToastPresenter.mainSectionBottomPadding == -1) {
                ToastPresenter.mainSectionBottomPadding = ToastPresenter.toastView.getMainLayout().mainSection.getPaddingBottom();
            }
            updateView(toastView, currentInfo);
        }
    }
    
    public static void resetTimeout() {
        if (ToastPresenter.timeout > 0) {
            ToastPresenter.handler.removeCallbacks(ToastPresenter.timeoutRunnable);
            ToastPresenter.handler.postDelayed(ToastPresenter.timeoutRunnable, (long)ToastPresenter.timeout);
            ToastPresenter.sLogger.v("reset timeout = " + ToastPresenter.timeout);
        }
    }
    
    private static void updateView(final ToastView toastView, final ToastManager.ToastInfo toastInfo) {
        final Context appContext = HudApplication.getAppContext();
        final Bundle data = toastInfo.toastParams.data;
        final ConfirmationLayout confirmation = toastView.getConfirmation();
        confirmation.screenTitle.setTextAppearance(appContext, R.style.mainTitle);
        confirmation.title1.setTextAppearance(appContext, R.style.title1);
        confirmation.title2.setTextAppearance(appContext, R.style.title2);
        confirmation.title3.setTextAppearance(appContext, R.style.title3);
        final int int1 = data.getInt("15", 0);
        if (int1 > 0) {
            final RelativeLayout$LayoutParams relativeLayout$LayoutParams = (RelativeLayout$LayoutParams)confirmation.choiceLayout.getLayoutParams();
            relativeLayout$LayoutParams.topMargin = 0;
            relativeLayout$LayoutParams.leftMargin = 0;
            relativeLayout$LayoutParams.rightMargin = 0;
            relativeLayout$LayoutParams.bottomMargin = int1;
        }
        ToastPresenter.id = data.getString("id");
        ToastPresenter.toastCallback = ToastManager.getInstance().getCallback(ToastPresenter.id);
        final Logger sLogger = ToastPresenter.sLogger;
        String s;
        if ("toast-cb-update :" + ToastPresenter.toastCallback == null) {
            s = "null";
        }
        else {
            s = "not null";
        }
        sLogger.v(s);
        final int int2 = data.getInt("8", 0);
        final String string = data.getString("9");
        final String string2 = data.getString("10");
        ToastPresenter.timeout = data.getInt("13", 0);
        final boolean boolean1 = data.getBoolean("12", false);
        final int int3 = data.getInt("11", 0);
        ToastPresenter.screenName = data.getString("18", (String)null);
        ToastPresenter.supportsGesture = data.getBoolean("19");
        if (ToastPresenter.supportsGesture && !ToastPresenter.gestureServiceConnector.isRunning()) {
            ToastPresenter.sLogger.v("gesture not available, turn off");
            ToastPresenter.supportsGesture = false;
        }
        final ArrayList parcelableArrayList = data.getParcelableArrayList("20");
        final MaxWidthLinearLayout maxWidthLinearLayout = (MaxWidthLinearLayout)confirmation.findViewById(R.id.infoContainer);
        int maxWidth = 0;
        if (data.containsKey("16")) {
            maxWidth = data.getInt("16");
            if (maxWidth <= 0) {
                maxWidth = 0;
            }
        }
        else if (data.containsKey("16_1")) {
            maxWidth = ToastPresenter.defaultMaxInfoWidth;
        }
        maxWidthLinearLayout.setMaxWidth(maxWidth);
        ((ViewGroup.MarginLayoutParams)maxWidthLinearLayout.getLayoutParams()).width = -2;
        if (data.containsKey("16_2")) {
            maxWidthLinearLayout.setPadding(data.getInt("16_2"), maxWidthLinearLayout.getPaddingTop(), maxWidthLinearLayout.getPaddingRight(), maxWidthLinearLayout.getPaddingBottom());
        }
        confirmation.fluctuatorView.setVisibility(GONE);
        final int applyTextAndStyle = applyTextAndStyle(confirmation.screenTitle, data, "1", "1_1", 1);
        final int applyTextAndStyle2 = applyTextAndStyle(confirmation.title1, data, "2", "3", 1);
        final int applyTextAndStyle3 = applyTextAndStyle(confirmation.title2, data, "4", "5", 1);
        final int applyTextAndStyle4 = applyTextAndStyle(confirmation.title3, data, "6", "7", 3);
        final ViewGroup mainSection = confirmation.mainSection;
        if (0 + applyTextAndStyle + applyTextAndStyle2 + applyTextAndStyle3 + applyTextAndStyle4 > 4) {
            ViewUtil.setBottomPadding((View)confirmation.mainSection, ToastPresenter.mainSectionBottomPadding + appContext.getResources().getDimensionPixelOffset(R.dimen.main_section_vertical_padding));
        }
        else {
            ViewUtil.setBottomPadding((View)confirmation.mainSection, ToastPresenter.mainSectionBottomPadding);
        }
        if (int2 != 0) {
            if (string2 != null) {
                confirmation.screenImage.setImage(int2, string2, InitialsImageView.Style.LARGE);
                confirmation.screenImage.setVisibility(View.VISIBLE);
            }
            else {
                confirmation.screenImage.setImage(int2, null, InitialsImageView.Style.DEFAULT);
                confirmation.screenImage.setVisibility(View.VISIBLE);
            }
        }
        else if (string != null) {
            final Bitmap bitmapfromCache = PicassoUtil.getBitmapfromCache(new File(string));
            if (bitmapfromCache != null) {
                confirmation.screenImage.setInitials(null, InitialsImageView.Style.DEFAULT);
                confirmation.screenImage.setImageBitmap(bitmapfromCache);
                confirmation.screenImage.setVisibility(View.VISIBLE);
            }
        }
        else {
            confirmation.screenImage.setImageResource(0);
            confirmation.screenImage.setVisibility(GONE);
        }
        if (int3 != 0) {
            confirmation.sideImage.setImageResource(int3);
            confirmation.sideImage.setVisibility(View.VISIBLE);
        }
        else {
            confirmation.sideImage.setVisibility(GONE);
        }
        if (boolean1) {
            confirmation.setChoices(ToastPresenter.DISMISS_CHOICE, 0, ToastPresenter.defaultChoiceListener);
            confirmation.choiceLayout.setVisibility(View.VISIBLE);
            ToastPresenter.supportsGesture = false;
        }
        else if (parcelableArrayList != null) {
            if (ToastPresenter.supportsGesture && parcelableArrayList.size() > 2) {
                throw new RuntimeException("max 2 choice allowed when gesture are on");
            }
            confirmation.setChoicesList(parcelableArrayList, 0, ToastPresenter.choiceListener);
            confirmation.choiceLayout.setVisibility(View.VISIBLE);
        }
        else {
            ToastPresenter.supportsGesture = false;
            confirmation.choiceLayout.setVisibility(GONE);
        }
        if (ToastPresenter.supportsGesture) {
            confirmation.leftSwipe.setVisibility(View.VISIBLE);
            confirmation.rightSwipe.setVisibility(View.VISIBLE);
            ToastPresenter.toastView.gestureOn = true;
        }
        else {
            confirmation.leftSwipe.setVisibility(GONE);
            confirmation.rightSwipe.setVisibility(GONE);
            ToastPresenter.toastView.gestureOn = boolean1;
        }
        if (ToastPresenter.timeout > 0) {
            ToastPresenter.handler.postDelayed(ToastPresenter.timeoutRunnable, (long)(ToastPresenter.timeout + 100));
            ToastPresenter.sLogger.v("timeout = " + (ToastPresenter.timeout + 100));
        }
        if (ToastPresenter.toastCallback != null) {
            ToastPresenter.sLogger.v("called onStart():" + ToastPresenter.id);
            ToastPresenter.toastCallback.onStart(toastView);
            if (ToastPresenter.supportsGesture) {
                ToastPresenter.gestureLedSettings = HUDLightUtils.showGestureDetectionEnabled(HudApplication.getAppContext(), LightManager.getInstance(), "Toast" + data.getString("1"));
            }
        }
        if (false && !toastInfo.ttsDone) {
            toastInfo.ttsDone = true;
        }
        toastView.animateIn(null, ToastPresenter.screenName, ToastPresenter.id, data.getBoolean("21", false));
    }
}
