package com.navdy.hud.app.framework.contacts;

import java.io.ObjectInputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import android.widget.ImageView;
import com.squareup.picasso.Callback;
import com.navdy.service.library.task.TaskManager;
import com.navdy.hud.app.util.picasso.PicassoUtil;
import com.navdy.service.library.events.photo.PhotoType;
import com.navdy.hud.app.framework.DriverProfileHelper;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.navdy.hud.app.util.GenericUtil;
import android.text.TextUtils;
import com.navdy.service.library.events.contacts.PhoneNumber;
import java.util.Iterator;
import java.util.ArrayList;
import com.navdy.service.library.events.contacts.Contact;
import java.util.List;
import com.navdy.service.library.events.contacts.PhoneNumberType;
import android.graphics.Bitmap;
import java.io.File;
import com.squareup.picasso.Transformation;
import com.navdy.hud.app.ui.component.image.InitialsImageView;
import android.content.res.Resources;
import com.navdy.hud.app.HudApplication;
import android.os.Looper;
import android.os.Handler;
import java.util.HashSet;

public class ContactUtil
{
    private static final String EMPTY = "";
    private static String HOME_STR;
    private static HashSet<Character> MARKER_CHARS;
    private static String MOBILE_STR;
    public static final String SPACE = " ";
    private static final char SPACE_CHAR = ' ';
    private static String WORK_STR;
    private static StringBuilder builder;
    private static Handler handler;
    
    static {
        ContactUtil.builder = new StringBuilder();
        ContactUtil.handler = new Handler(Looper.getMainLooper());
        final Resources resources = HudApplication.getAppContext().getResources();
        ContactUtil.WORK_STR = resources.getString(R.string.work);
        ContactUtil.MOBILE_STR = resources.getString(R.string.mobile);
        ContactUtil.HOME_STR = resources.getString(R.string.home);
        (ContactUtil.MARKER_CHARS = new HashSet<Character>()).add('\u200e');
        ContactUtil.MARKER_CHARS.add('\u202a');
        ContactUtil.MARKER_CHARS.add('\u202c');
        ContactUtil.MARKER_CHARS.add('\u200e');
    }
    
    public static PhoneNumberType convertNumberType(final NumberType numberType) {
        PhoneNumberType phoneNumberType = null;
        if (numberType == null) {
            phoneNumberType = null;
        }
        else {
            switch (numberType) {
                default:
                    phoneNumberType = PhoneNumberType.PHONE_NUMBER_OTHER;
                    break;
                case HOME:
                    phoneNumberType = PhoneNumberType.PHONE_NUMBER_HOME;
                    break;
                case WORK_MOBILE:
                case WORK:
                    phoneNumberType = PhoneNumberType.PHONE_NUMBER_WORK;
                    break;
                case MOBILE:
                    phoneNumberType = PhoneNumberType.PHONE_NUMBER_MOBILE;
                    break;
            }
        }
        return phoneNumberType;
    }
    
    public static List<com.navdy.hud.app.framework.contacts.Contact> fromContacts(final List<Contact> list) {
        List<com.navdy.hud.app.framework.contacts.Contact> list2;
        if (list == null) {
            list2 = null;
        }
        else {
            final ArrayList<com.navdy.hud.app.framework.contacts.Contact> list3 = new ArrayList<com.navdy.hud.app.framework.contacts.Contact>(list.size());
            final Iterator<Contact> iterator = list.iterator();
            while (true) {
                list2 = list3;
                if (!iterator.hasNext()) {
                    break;
                }
                list3.add(new com.navdy.hud.app.framework.contacts.Contact(iterator.next()));
            }
        }
        return list2;
    }
    
    public static List<com.navdy.hud.app.framework.contacts.Contact> fromPhoneNumbers(final List<PhoneNumber> list) {
        List<com.navdy.hud.app.framework.contacts.Contact> list2;
        if (list == null) {
            list2 = null;
        }
        else {
            final ArrayList<com.navdy.hud.app.framework.contacts.Contact> list3 = new ArrayList<com.navdy.hud.app.framework.contacts.Contact>(list.size());
            final Iterator<PhoneNumber> iterator = list.iterator();
            while (true) {
                list2 = list3;
                if (!iterator.hasNext()) {
                    break;
                }
                list3.add(new com.navdy.hud.app.framework.contacts.Contact(iterator.next()));
            }
        }
        return list2;
    }
    
    public static String getFirstName(String substring) {
        if (TextUtils.isEmpty((CharSequence)substring)) {
            substring = "";
        }
        else {
            final String trim = substring.trim();
            final int index = trim.indexOf(" ");
            substring = trim;
            if (index > 0) {
                substring = trim.substring(0, index);
            }
        }
        return substring;
    }
    
    public static String getInitials(String s) {
        synchronized (ContactUtil.class) {
            if (TextUtils.isEmpty((CharSequence)s)) {
                s = "";
            }
            else {
                final String trim = GenericUtil.removePunctuation(s).trim();
                final int index = trim.indexOf(" ");
                if (index > 0) {
                    s = trim.substring(0, index).trim().replaceAll("[\\u202A]", "");
                    final String replaceAll = trim.substring(index + 1).trim().replaceAll("[\\u202A]", "");
                    ContactUtil.builder.setLength(0);
                    if (!TextUtils.isEmpty((CharSequence)s)) {
                        ContactUtil.builder.append(s.charAt(0));
                    }
                    if (!TextUtils.isEmpty((CharSequence)replaceAll)) {
                        ContactUtil.builder.append(replaceAll.charAt(0));
                    }
                    s = ContactUtil.builder.toString().toUpperCase();
                }
                else if (trim.length() > 0) {
                    s = String.valueOf(trim.charAt(0)).toUpperCase();
                }
                else {
                    s = "";
                }
            }
            return s;
        }
    }
    
    public static NumberType getNumberType(final PhoneNumberType phoneNumberType) {
        NumberType numberType = null;
        if (phoneNumberType == null) {
            numberType = null;
        }
        else {
            switch (phoneNumberType) {
                default:
                    numberType = NumberType.OTHER;
                    break;
                case PHONE_NUMBER_HOME:
                    numberType = NumberType.HOME;
                    break;
                case PHONE_NUMBER_MOBILE:
                    numberType = NumberType.MOBILE;
                    break;
                case PHONE_NUMBER_WORK:
                    numberType = NumberType.WORK;
                    break;
            }
        }
        return numberType;
    }
    
    public static long getPhoneNumber(final String s) {
        try {
            return PhoneNumberUtil.getInstance().parse(s, DriverProfileHelper.getInstance().getCurrentLocale().getCountry()).getNationalNumber();
        }
        catch (Throwable t) {
            return -1L;
        }
    }
    
    public static String getPhoneType(final NumberType numberType) {
        final String s = null;
        String s2 = null;
        if (numberType == null) {
            s2 = s;
        }
        else {
            switch (numberType) {
                default:
                    s2 = s;
                    break;
                case WORK_MOBILE:
                case WORK:
                    s2 = ContactUtil.WORK_STR;
                    break;
                case MOBILE:
                    s2 = ContactUtil.MOBILE_STR;
                    break;
                case HOME:
                    s2 = ContactUtil.HOME_STR;
                    break;
            }
        }
        return s2;
    }
    
    public static boolean isDisplayNameValid(final String s, final String s2, final long n) {
        final boolean b = false;
        boolean b2;
        if (TextUtils.isEmpty((CharSequence)s) && TextUtils.isEmpty((CharSequence)s2)) {
            b2 = b;
        }
        else if (!TextUtils.isEmpty((CharSequence)s) && TextUtils.isEmpty((CharSequence)s2)) {
            b2 = true;
        }
        else {
            b2 = b;
            if (!s2.equalsIgnoreCase(s)) {
                while (true) {
                    try {
                        final long nationalNumber = PhoneNumberUtil.getInstance().parse(s, DriverProfileHelper.getInstance().getCurrentLocale().getCountry()).getNationalNumber();
                        b2 = b;
                        if (nationalNumber != n) {
                            b2 = true;
                        }
                    }
                    catch (Throwable t) {
                        continue;
                    }
                    break;
                }
            }
        }
        return b2;
    }
    
    public static boolean isImageAvailable(final String s, final String s2) {
        boolean b = false;
        if ((!TextUtils.isEmpty((CharSequence)s) || !TextUtils.isEmpty((CharSequence)s2)) && !TextUtils.isEmpty((CharSequence)s2)) {
            b = true;
        }
        return b;
    }
    
    public static boolean isValidNumber(final String s) {
        boolean b = false;
        try {
            if (!TextUtils.isEmpty((CharSequence)s)) {
                PhoneNumberUtil.getInstance().parse(s, DriverProfileHelper.getInstance().getCurrentLocale().getCountry());
                b = true;
            }
            return b;
        }
        catch (Throwable t) {
            return b;
        }
        return b;
    }
    
    public static String sanitizeString(String s) {
        if (s != null) {
            boolean b = false;
            final int length = s.length();
            final char[] charArray = s.toCharArray();
            for (int i = 0; i < charArray.length; ++i) {
                if (ContactUtil.MARKER_CHARS.contains(charArray[i])) {
                    b = true;
                    charArray[i] = 32;
                }
            }
            if (!b) {
                s = s.trim();
            }
            else {
                s = new String(charArray, 0, length).trim();
            }
        }
        return s;
    }
    
    public static void setContactPhoto(final String s, final String s2, final boolean b, final InitialsImageView initialsImageView, final Transformation transformation, final IContactCallback contactCallback) {
        initialsImageView.setTag(null);
        if (TextUtils.isEmpty((CharSequence)s) && TextUtils.isEmpty((CharSequence)s2)) {
            initialsImageView.setImage(R.drawable.icon_user_numberonly, null, InitialsImageView.Style.DEFAULT);
        }
        else if (TextUtils.isEmpty((CharSequence)s2)) {
            initialsImageView.setImage(R.drawable.icon_user_numberonly, null, InitialsImageView.Style.DEFAULT);
        }
        else {
            String s3 = s;
            if (!isDisplayNameValid(s, s2, getPhoneNumber(s2))) {
                s3 = null;
            }
            final PhoneImageDownloader instance = PhoneImageDownloader.getInstance();
            final File imagePath = instance.getImagePath(s2, PhotoType.PHOTO_CONTACT);
            final Bitmap bitmapfromCache = PicassoUtil.getBitmapfromCache(imagePath);
            if (bitmapfromCache != null) {
                initialsImageView.setInitials(null, InitialsImageView.Style.DEFAULT);
                initialsImageView.setImageBitmap(bitmapfromCache);
                setContactPhoto(s3, s2, b, initialsImageView, transformation, contactCallback, imagePath, false, instance, bitmapfromCache);
            }
            else {
                TaskManager.getInstance().execute(new Runnable() {
                    @Override
                    public void run() {
                        if (contactCallback.isContextValid()) {
                            ContactUtil.handler.post((Runnable)new Runnable() {
                                final /* synthetic */ boolean val$imageExists = imagePath.exists();
                                
                                @Override
                                public void run() {
                                    setContactPhoto(s3, s2, b, initialsImageView, transformation, contactCallback, imagePath, this.val$imageExists, instance, null);
                                }
                            });
                        }
                    }
                }, 1);
            }
        }
    }
    
    private static void setContactPhoto(final String s, final String s2, final boolean b, final InitialsImageView initialsImageView, final Transformation transformation, final IContactCallback contactCallback, final File file, final boolean b2, final PhoneImageDownloader phoneImageDownloader, final Bitmap bitmap) {
        Label_0052: {
            if (bitmap != null) {
                break Label_0052;
            }
            if (contactCallback.isContextValid()) {
                if (b2) {
                    updateInitials(InitialsImageView.Style.DEFAULT, null, initialsImageView);
                    setImage(file, initialsImageView, transformation);
                    break Label_0052;
                }
                if (s != null) {
                    final ContactImageHelper instance = ContactImageHelper.getInstance();
                    initialsImageView.setImage(instance.getResourceId(instance.getContactImageIndex(s2)), getInitials(s), InitialsImageView.Style.LARGE);
                    break Label_0052;
                }
                initialsImageView.setImage(R.drawable.icon_user_numberonly, null, InitialsImageView.Style.DEFAULT);
                break Label_0052;
            }
            return;
        }
        if (b) {
            phoneImageDownloader.clearPhotoCheckEntry(s2, PhotoType.PHOTO_CONTACT);
            phoneImageDownloader.submitDownload(s2, PhoneImageDownloader.Priority.HIGH, PhotoType.PHOTO_CONTACT, s);
        }
    }
    
    private static void setImage(final File file, final InitialsImageView initialsImageView, final Transformation transformation) {
        PicassoUtil.getInstance().load(file).fit().transform(transformation).into(initialsImageView, new Callback() {
            @Override
            public void onError() {
            }
            
            @Override
            public void onSuccess() {
                initialsImageView.setTag(file.getAbsolutePath());
            }
        });
    }
    
    public static List<Contact> toContacts(final List<com.navdy.hud.app.framework.contacts.Contact> list) {
        final List<Contact> list2 = null;
        List<Contact> list3;
        if (list == null) {
            list3 = list2;
        }
        else {
            final ArrayList<Contact> list4 = new ArrayList<Contact>(list.size());
            for (final com.navdy.hud.app.framework.contacts.Contact contact : list) {
                final Contact.Builder numberType = new Contact.Builder().name(contact.name).number(contact.number).numberType(convertNumberType(contact.numberType));
                String numberTypeStr;
                if (contact.numberType == NumberType.OTHER) {
                    numberTypeStr = contact.numberTypeStr;
                }
                else {
                    numberTypeStr = null;
                }
                list4.add(numberType.label(numberTypeStr).build());
            }
            list3 = list4;
        }
        return list3;
    }
    
    public static List<PhoneNumber> toPhoneNumbers(final List<com.navdy.hud.app.framework.contacts.Contact> list) {
        final List<PhoneNumber> list2 = null;
        List<PhoneNumber> list3;
        if (list == null) {
            list3 = list2;
        }
        else {
            final ArrayList<PhoneNumber> list4 = new ArrayList<PhoneNumber>(list.size());
            for (final com.navdy.hud.app.framework.contacts.Contact contact : list) {
                final PhoneNumber.Builder numberType = new PhoneNumber.Builder().number(contact.number).numberType(convertNumberType(contact.numberType));
                String numberTypeStr;
                if (contact.numberType == NumberType.OTHER) {
                    numberTypeStr = contact.numberTypeStr;
                }
                else {
                    numberTypeStr = null;
                }
                list4.add(numberType.customType(numberTypeStr).build());
            }
            list3 = list4;
        }
        return list3;
    }
    
    private static void updateInitials(final InitialsImageView.Style style, final String s, final InitialsImageView initialsImageView) {
        initialsImageView.setInitials(s, style);
    }
    
    public interface IContactCallback
    {
        boolean isContextValid();
    }
}
