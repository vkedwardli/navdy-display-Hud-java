package com.navdy.hud.app.framework.notifications;

import android.animation.AnimatorSet;
import com.navdy.hud.app.framework.connection.ConnectionNotification;
import com.navdy.service.library.events.glances.ClearGlances;
import com.squareup.otto.Subscribe;
import com.navdy.hud.app.framework.phonecall.CallManager;
import com.navdy.hud.app.event.Shutdown;
import com.navdy.hud.app.framework.toast.ToastManager;
import com.navdy.hud.app.analytics.AnalyticsSupport;
import com.navdy.hud.app.framework.glance.GlanceHandler;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.framework.glance.GlanceConstants;
import java.util.Iterator;
import com.navdy.hud.app.framework.phonecall.CallNotification;
import com.navdy.hud.app.manager.InputManager;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import com.navdy.hud.app.framework.glance.GlanceHelper;
import com.navdy.hud.app.ui.component.image.InitialsImageView;
import android.widget.TextView;
import android.widget.ImageView;
import com.navdy.hud.app.framework.glance.GlanceViewCache;
import com.navdy.hud.app.screen.BaseScreen;
import java.io.Serializable;
import android.text.TextUtils;
import android.graphics.RectF;
import java.util.EnumSet;
import mortar.Mortar;
import com.navdy.hud.app.ui.component.homescreen.HomeScreenView;
import com.navdy.hud.app.device.light.LightManager;
import com.navdy.hud.app.event.ShowScreenWithArgs;
import com.navdy.hud.app.device.light.HUDLightUtils;
import android.content.Context;
import com.navdy.service.library.events.preferences.NotificationPreferences;
import android.view.ViewGroup;
import com.navdy.hud.app.framework.DriverProfileHelper;
import com.navdy.hud.app.HudApplication;
import java.util.LinkedList;
import android.os.Looper;
import com.navdy.hud.app.ui.framework.UIStateManager;
import android.content.res.Resources;
import android.os.Bundle;
import com.navdy.service.library.events.ui.Screen;
import java.util.Queue;
import com.navdy.service.library.device.NavdyDeviceId;
import java.util.ArrayList;
import java.util.TreeMap;
import java.util.HashMap;
import com.navdy.hud.app.view.NotificationView;
import com.navdy.hud.app.ui.component.carousel.ProgressIndicator;
import com.navdy.hud.app.ui.component.carousel.CarouselIndicator;
import com.navdy.hud.app.ui.framework.INotificationAnimationListener;
import com.navdy.hud.app.ui.activity.Main;
import android.os.Handler;
import com.navdy.hud.app.device.light.LED;
import android.widget.FrameLayout;
import android.view.View;
import java.util.Comparator;
import javax.inject.Inject;
import com.squareup.otto.Bus;
import java.util.HashSet;
import com.navdy.service.library.log.Logger;

public final class NotificationManager
{
    private static final int EXPAND_VIEW_ANIMATION_DURATION = 250;
    public static final int EXPAND_VIEW_ANIMATION_START_DELAY_DURATION = 250;
    private static final int EXPAND_VIEW_ELEMENT_ANIMATION_DURATION = 100;
    private static final int EXPAND_VIEW_QUICK_DURATION = 0;
    public static final String EXTRA_VOICE_SEARCH_NOTIFICATION = "extra_voice_search_notification";
    private static final NotificationChange NOTIFICATION_CHANGE;
    private static final NotificationManager sInstance;
    static final Logger sLogger;
    private HashSet<String> NOTIFS_NOT_ALLOWED_ON_DISCONNECT;
    private HashSet<String> NOTIFS_REMOVED_ON_DISCONNECT;
    int animationTranslation;
    @Inject
    Bus bus;
    private Comparator<Info> comparator;
    private Info currentNotification;
    private boolean deleteAllGlances;
    private boolean disconnected;
    private boolean enable;
    private boolean enablePhoneCalls;
    private View expandedNotifCoverView;
    private FrameLayout expandedNotifView;
    private boolean expandedWithStack;
    private LED.Settings gestureEnabledSettings;
    private Handler handler;
    private Runnable ignoreScrollRunnable;
    private volatile boolean isAnimating;
    private volatile boolean isCollapsed;
    private volatile boolean isExpanded;
    private Info lastStackCurrentNotification;
    private Object lockObj;
    private Main mainScreen;
    private INotificationAnimationListener notifAnimationListener;
    private CarouselIndicator notifIndicator;
    private ProgressIndicator notifScrollIndicator;
    private NotificationView notifView;
    private INotificationController notificationController;
    private long notificationCounter;
    private HashMap<String, Info> notificationIdMap;
    private TreeMap<Info, Object> notificationPriorityMap;
    private ArrayList<Info> notificationSavedList;
    private NavdyDeviceId notificationSavedListDeviceId;
    private Queue<NotificationAnimator.OperationInfo> operationQueue;
    private boolean operationRunning;
    INotification pendingNotification;
    private Screen pendingScreen;
    private Bundle pendingScreenArgs;
    private Object pendingScreenArgs2;
    private Bundle pendingShutdownArgs;
    private Resources resources;
    private IProgressUpdate scrollProgress;
    private ScrollState scrollState;
    private boolean showOn;
    private Info stackCurrentNotification;
    private Info stagedNotification;
    private boolean ttsOn;
    @Inject
    UIStateManager uiStateManager;
    
    static {
        sLogger = new Logger(NotificationManager.class);
        NOTIFICATION_CHANGE = new NotificationChange();
        sInstance = new NotificationManager();
    }
    
    private NotificationManager() {
        this.handler = new Handler(Looper.getMainLooper());
        this.operationQueue = new LinkedList<NotificationAnimator.OperationInfo>();
        this.enable = true;
        this.enablePhoneCalls = true;
        this.scrollState = ScrollState.NONE;
        this.ignoreScrollRunnable = new Runnable() {
            @Override
            public void run() {
                NotificationManager.sLogger.v("limit reached:true");
                NotificationManager.this.scrollState = ScrollState.LIMIT_REACHED;
            }
        };
        this.resources = HudApplication.getAppContext().getResources();
        this.notificationController = new INotificationController() {
            @Override
            public void collapseNotification(final boolean b, final boolean b2) {
                NotificationManager.this.collapseNotificationInternal(b, b2, false);
            }
            
            @Override
            public void expandNotification(final boolean b) {
                NotificationManager.sLogger.v("expandNotification  running=" + NotificationManager.this.operationRunning + " qsize:" + NotificationManager.this.operationQueue.size());
                final NotificationView access$100 = NotificationManager.this.getNotificationView();
                if (access$100 != null && NotificationManager.this.currentNotification != null) {
                    if (NotificationManager.this.isAnimating) {
                        NotificationManager.sLogger.i("animation in progress, ignore expand");
                    }
                    else if (this.isExpanded()) {
                        NotificationManager.sLogger.v("already expanded");
                    }
                    else {
                        final FrameLayout access$101 = NotificationManager.this.getExpandedNotificationView();
                        final View access$102 = NotificationManager.this.getExpandedNotificationCoverView();
                        final CarouselIndicator access$103 = NotificationManager.this.getNotificationIndicator();
                        NotificationManager.this.scrollState = ScrollState.NONE;
                        if (access$101.getChildCount() > 0) {
                            access$101.removeAllViews();
                        }
                        if (b) {
                            final NotificationPreferences notificationPreferences = DriverProfileHelper.getInstance().getCurrentProfile().getNotificationPreferences();
                            NotificationManager.this.ttsOn = notificationPreferences.readAloud;
                            NotificationManager.this.showOn = notificationPreferences.showContent;
                            NotificationManager.sLogger.v("expanded with[" + NotificationManager.this.currentNotification.notification.getId() + "] tts=" + NotificationManager.this.ttsOn + " show=" + NotificationManager.this.showOn);
                        }
                        final View view = null;
                        View expandedView = null;
                        Label_0363: {
                            if (b) {
                                expandedView = view;
                                if (!b) {
                                    break Label_0363;
                                }
                                expandedView = view;
                                if (!NotificationManager.this.showOn) {
                                    break Label_0363;
                                }
                            }
                            if ((expandedView = NotificationManager.this.currentNotification.notification.getExpandedView(access$100.getContext(), null)) == null) {
                                NotificationManager.sLogger.w("cannot expand, view is null");
                                NotificationManager.this.ttsOn = false;
                                NotificationManager.this.showOn = false;
                                return;
                            }
                        }
                        NotificationManager.this.expandedWithStack = b;
                        if (b) {
                            NotificationManager.this.stackCurrentNotification = NotificationManager.this.currentNotification;
                            NotificationManager.this.lastStackCurrentNotification = null;
                            if (NotificationManager.sLogger.isLoggable(2)) {
                                NotificationManager.this.printPriorityMap();
                            }
                            access$101.setTag(true);
                        }
                        else {
                            access$101.setTag(null);
                            NotificationManager.this.stackCurrentNotification = null;
                            NotificationManager.this.lastStackCurrentNotification = null;
                        }
                        access$100.border.stopTimeout(true, null);
                        access$100.hideNextNotificationColor();
                        final INotification notification = NotificationManager.this.currentNotification.notification;
                        if (b) {
                            int notificationCount = NotificationManager.this.getNotificationCount();
                            if (NotificationManager.this.isPhoneNotifPresentAndNotAlive()) {
                                --notificationCount;
                            }
                            int access$104 = NotificationManager.this.getNotificationIndex(NotificationManager.this.currentNotification);
                            NotificationManager.sLogger.v("expandNotification index:" + access$104);
                            if (access$104 == -1) {
                                access$104 = 0;
                            }
                            else {
                                ++access$104;
                            }
                            final int n = notificationCount + 2;
                            int n2 = access$104;
                            if (access$104 >= n) {
                                n2 = n - 1;
                            }
                            NotificationManager.this.updateExpandedIndicator(n, n2, NotificationManager.this.currentNotification.notification.getColor());
                        }
                        final Runnable runnable = new Runnable() {
                            @Override
                            public void run() {
                                NotificationManager.sLogger.v("calling onExpandedNotificationEvent-expand");
                                notification.onExpandedNotificationEvent(UIStateManager.Mode.EXPAND);
                                access$103.setVisibility(View.VISIBLE);
                                NotificationManager.this.displayScrollingIndicator(access$103.getCurrentItem());
                            }
                        };
                        if (!NotificationManager.this.expandedWithStack || (NotificationManager.this.expandedWithStack && NotificationManager.this.showOn)) {
                            access$100.animate().x(0.0f).withEndAction((Runnable)new Runnable() {
                                @Override
                                public void run() {
                                    NotificationAnimator.animateExpandedViews(expandedView, null, (ViewGroup)access$101, NotificationManager.this.animationTranslation, 250, 0, runnable);
                                }
                            }).withStartAction((Runnable)new Runnable() {
                                @Override
                                public void run() {
                                    access$102.setVisibility(View.VISIBLE);
                                    access$101.setVisibility(View.VISIBLE);
                                }
                            }).start();
                        }
                        else {
                            runnable.run();
                        }
                    }
                }
                else {
                    NotificationManager.sLogger.i("cannot display expand notif:no current notif");
                }
            }
            
            @Override
            public Context getUIContext() {
                return NotificationManager.this.getUIContext();
            }
            
            @Override
            public boolean isExpanded() {
                return NotificationManager.this.isExpandedNotificationVisible();
            }
            
            @Override
            public boolean isExpandedWithStack() {
                return NotificationManager.this.isExpanded();
            }
            
            @Override
            public boolean isShowOn() {
                return NotificationManager.this.showOn;
            }
            
            @Override
            public boolean isTtsOn() {
                return NotificationManager.this.ttsOn;
            }
            
            @Override
            public void moveNext(final boolean b) {
                NotificationManager.this.moveNext(b);
            }
            
            @Override
            public void movePrevious(final boolean b) {
                NotificationManager.this.movePrevious(b);
            }
            
            @Override
            public void resetTimeout() {
                final NotificationView access$100 = NotificationManager.this.getNotificationView();
                if (access$100 != null) {
                    access$100.border.resetTimeout();
                }
            }
            
            @Override
            public void startTimeout(final int n) {
                final NotificationView access$100 = NotificationManager.this.getNotificationView();
                if (access$100 != null) {
                    access$100.border.startTimeout(n);
                }
            }
            
            @Override
            public void stopTimeout(final boolean b) {
                final NotificationView access$100 = NotificationManager.this.getNotificationView();
                if (access$100 != null) {
                    access$100.border.stopTimeout(b, null);
                }
            }
        };
        this.NOTIFS_REMOVED_ON_DISCONNECT = new HashSet<String>();
        this.NOTIFS_NOT_ALLOWED_ON_DISCONNECT = new HashSet<String>();
        this.comparator = new Comparator<Info>() {
            @Override
            public int compare(final Info info, final Info info2) {
                int n = info.priority - info2.priority;
                if (n == 0) {
                    n = (int)(info2.uniqueCounter - info.uniqueCounter);
                }
                return n;
            }
        };
        this.lockObj = new Object();
        this.notificationPriorityMap = new TreeMap<Info, Object>(this.comparator);
        this.notificationIdMap = new HashMap<String, Info>();
        this.notificationSavedList = new ArrayList<Info>();
        this.disconnected = true;
        this.pendingShutdownArgs = null;
        this.notifAnimationListener = new INotificationAnimationListener() {
            @Override
            public void onStart(final String s, final NotificationType notificationType, final UIStateManager.Mode mode) {
                NotificationManager.sLogger.v("notif-anim-start [" + s + "] type[" + notificationType + "] mode [" + mode + "]");
                NotificationManager.this.isAnimating = true;
                if (mode == UIStateManager.Mode.EXPAND) {
                    NotificationManager.this.isCollapsed = false;
                    final NotificationView access$100 = NotificationManager.this.getNotificationView();
                    access$100.border.stopTimeout(true, null);
                    NotificationManager.sLogger.v("notif-anim-start showing notif [" + NotificationManager.this.currentNotification.notification.getId() + "]");
                    final View view = NotificationManager.this.currentNotification.notification.getView(access$100.getContext());
                    NotificationManager.this.currentNotification.pushBackDuetoHighPriority = false;
                    NotificationManager.this.currentNotification.startCalled = true;
                    NotificationManager.this.currentNotification.notification.onStart(NotificationManager.this.notificationController);
                    NotificationManager.sLogger.v("current notification [" + NotificationManager.this.currentNotification.notification.getId() + "] called start[" + System.identityHashCode(NotificationManager.this.currentNotification.notification) + "]");
                    access$100.addCustomView(NotificationManager.this.currentNotification.notification, view);
                    NotificationManager.this.setNotificationColor();
                    NotificationManager.this.currentNotification.notification.onNotificationEvent(UIStateManager.Mode.EXPAND);
                }
                else {
                    NotificationManager.this.isExpanded = false;
                    HUDLightUtils.removeSettings(NotificationManager.this.gestureEnabledSettings);
                }
            }
            
            @Override
            public void onStop(final String s, NotificationType pendingNotification, final UIStateManager.Mode mode) {
            Label_1715_Outer:
                while (true) {
                    NotificationManager.sLogger.v("notif-anim-stop [" + s + "] type[" + pendingNotification + "] mode [" + mode + "]");
                    // monitorenter(o2 = NotificationManager.access$2300(this.this$0))
                    while (true) {
                        Label_1955: {
                            while (true) {
                                String id = null;
                            Label_0814:
                                while (true) {
                                    int n = 0;
                                    Label_0300: {
                                        try {
                                            pendingNotification = (NotificationType)NotificationManager.this.pendingNotification;
                                            NotificationManager.this.pendingNotification = null;
                                            if (mode != UIStateManager.Mode.COLLAPSE) {
                                                break Label_1955;
                                            }
                                            final boolean b = false;
                                            final boolean b2 = false;
                                            int n2;
                                            n = (n2 = 0);
                                            try {
                                                NotificationManager.this.isCollapsed = true;
                                                n2 = n;
                                                final NotificationView access$100 = NotificationManager.this.getNotificationView();
                                                n2 = n;
                                                NotificationManager.this.hideNotificationCoverView();
                                                n2 = n;
                                                if (!NotificationManager.this.deleteAllGlances) {
                                                    break Label_0300;
                                                }
                                                n2 = n;
                                                NotificationManager.this.deleteAllGlances = false;
                                                n2 = n;
                                                NotificationManager.sLogger.v("*** user selected delete all glances");
                                                n2 = n;
                                                access$100.removeCustomView();
                                                n2 = n;
                                                final Object access$101 = NotificationManager.this.lockObj;
                                                n2 = n;
                                                // monitorenter(o = access$101)
                                                try {
                                                    NotificationManager.this.removeAllNotification();
                                                    // monitorexit(o)
                                                    if (!false) {
                                                        final HomeScreenView homescreenView = NotificationManager.this.uiStateManager.getHomescreenView();
                                                        if (homescreenView.hasModeView()) {
                                                            NotificationManager.sLogger.v("mode-view: notif onStop show");
                                                            homescreenView.animateInModeView();
                                                        }
                                                    }
                                                    return;
                                                }
                                                finally {
                                                    // monitorexit(o)
                                                    n2 = n;
                                                }
                                            }
                                            finally {
                                                if (n2 == 0) {
                                                    final HomeScreenView homescreenView2 = NotificationManager.this.uiStateManager.getHomescreenView();
                                                    if (homescreenView2.hasModeView()) {
                                                        NotificationManager.sLogger.v("mode-view: notif onStop show");
                                                        homescreenView2.animateInModeView();
                                                    }
                                                }
                                            }
                                        }
                                        finally {
                                            NotificationManager.this.isAnimating = false;
                                        }
                                        break Label_0300;
                                        while (true) {
                                            NotificationManager.sLogger.v("notif-anim-stop no staged-notif, check if there is pending one");
                                            // monitorenter(o2 = NotificationManager.access$2300(this.this$0))
                                            final boolean b;
                                            n = (b ? 1 : 0);
                                            final boolean b2;
                                            int n2 = b2 ? 1 : 0;
                                            while (true) {
                                                try {
                                                    if (NotificationManager.this.notificationPriorityMap.size() > 0) {
                                                        n2 = (b2 ? 1 : 0);
                                                        final Info info = NotificationManager.this.notificationPriorityMap.lastKey();
                                                        n = (b ? 1 : 0);
                                                        n2 = (b2 ? 1 : 0);
                                                        if (info.pushBackDuetoHighPriority) {
                                                            n2 = (b2 ? 1 : 0);
                                                            final Logger sLogger = NotificationManager.sLogger;
                                                            n2 = (b2 ? 1 : 0);
                                                            n2 = (b2 ? 1 : 0);
                                                            final StringBuilder sb = new StringBuilder();
                                                            n2 = (b2 ? 1 : 0);
                                                            sLogger.v(sb.append("notif-anim-stop found pending [").append(info.notification.getId()).append("]").toString());
                                                            n2 = (b2 ? 1 : 0);
                                                            NotificationManager.this.currentNotification = info;
                                                            n2 = (b2 ? 1 : 0);
                                                            NotificationManager.this.isAnimating = false;
                                                            n2 = 1;
                                                            n = 1;
                                                            NotificationManager.this.showNotification();
                                                        }
                                                    }
                                                    n2 = n;
                                                    // monitorexit(o2)
                                                    n2 = n;
                                                    if (NotificationManager.this.pendingShutdownArgs != null) {
                                                        n2 = n;
                                                        final Bus bus = NotificationManager.this.bus;
                                                        n2 = n;
                                                        n2 = n;
                                                        final ShowScreenWithArgs showScreenWithArgs = new ShowScreenWithArgs(Screen.SCREEN_SHUTDOWN_CONFIRMATION, NotificationManager.this.pendingShutdownArgs, false);
                                                        n2 = n;
                                                        bus.post(showScreenWithArgs);
                                                        n2 = n;
                                                        NotificationManager.this.pendingShutdownArgs = null;
                                                        n2 = n;
                                                        NotificationManager.this.pendingScreen = null;
                                                        n2 = n;
                                                        NotificationManager.this.pendingScreenArgs = null;
                                                        n2 = n;
                                                        NotificationManager.this.pendingScreenArgs2 = null;
                                                        if (n == 0) {
                                                            final HomeScreenView homescreenView3 = NotificationManager.this.uiStateManager.getHomescreenView();
                                                            if (homescreenView3.hasModeView()) {
                                                                NotificationManager.sLogger.v("mode-view: notif onStop show");
                                                                homescreenView3.animateInModeView();
                                                            }
                                                        }
                                                        NotificationManager.this.isAnimating = false;
                                                        return;
                                                    }
                                                }
                                                finally {
                                                }
                                                // monitorexit(o2)
                                                final INotification notification;
                                                if (notification != null) {
                                                    NotificationManager.sLogger.v("adding pending notification:" + notification + " current = " + NotificationManager.this.currentNotification);
                                                    NotificationManager.this.isAnimating = false;
                                                    n2 = n;
                                                    NotificationManager.this.addNotification(notification);
                                                    continue Label_1715_Outer;
                                                }
                                                n2 = n;
                                                if (NotificationManager.this.pendingScreen != null) {
                                                    NotificationManager.this.bus.post(new ShowScreenWithArgs(NotificationManager.this.pendingScreen, NotificationManager.this.pendingScreenArgs, NotificationManager.this.pendingScreenArgs2, false));
                                                    final Logger sLogger2 = NotificationManager.sLogger;
                                                    final StringBuilder sb2 = new StringBuilder();
                                                    n2 = n;
                                                    sLogger2.v(sb2.append("launched pending screen:").append(NotificationManager.this.pendingScreen).toString());
                                                    continue Label_1715_Outer;
                                                }
                                                continue Label_1715_Outer;
                                            }
                                        }
                                    }
                                    final NotificationView notificationView;
                                    NotificationManager.this.cleanupViews(notificationView);
                                    int n2 = n;
                                    if (NotificationManager.this.currentNotification == null) {
                                        if (!false) {
                                            final HomeScreenView homescreenView4 = NotificationManager.this.uiStateManager.getHomescreenView();
                                            if (homescreenView4.hasModeView()) {
                                                NotificationManager.sLogger.v("mode-view: notif onStop show");
                                                homescreenView4.animateInModeView();
                                            }
                                        }
                                        NotificationManager.this.isAnimating = false;
                                        return;
                                    }
                                    NotificationManager.this.currentNotification.notification.onNotificationEvent(UIStateManager.Mode.COLLAPSE);
                                    id = NotificationManager.this.currentNotification.notification.getId();
                                    NotificationManager.sLogger.v("notif-anim-start [" + id + "]");
                                    NotificationManager.this.notificationController.stopTimeout(false);
                                    if (!NotificationManager.this.currentNotification.removed && !NotificationManager.this.currentNotification.resurrected && !NotificationManager.this.currentNotification.notification.isAlive()) {
                                        NotificationManager.sLogger.v("notif-anim-stop not alive, marked removed");
                                        NotificationManager.this.currentNotification.removed = true;
                                    }
                                    if (!NotificationManager.this.currentNotification.removed) {
                                        break Label_0814;
                                    }
                                    NotificationManager.sLogger.v("notif-anim-stop removed [" + id + "]");
                                    Label_0923: {
                                        try {
                                            NotificationManager.this.removeNotificationfromStack(NotificationManager.this.currentNotification, false);
                                            // monitorexit(o2)
                                            notificationView.removeCustomView();
                                            if (NotificationManager.this.currentNotification.resurrected) {
                                                NotificationManager.sLogger.v("notif-anim-stop notif resurrected");
                                                NotificationManager.this.currentNotification.resurrected = false;
                                                NotificationManager.this.isAnimating = false;
                                                NotificationManager.this.showNotification(true);
                                                if (!true) {
                                                    final HomeScreenView homescreenView5 = NotificationManager.this.uiStateManager.getHomescreenView();
                                                    if (homescreenView5.hasModeView()) {
                                                        NotificationManager.sLogger.v("mode-view: notif onStop show");
                                                        homescreenView5.animateInModeView();
                                                    }
                                                }
                                                NotificationManager.this.isAnimating = false;
                                                return;
                                            }
                                            break Label_0923;
                                        }
                                        finally {
                                            // monitorexit(o2)
                                            n2 = n;
                                        }
                                        break Label_0814;
                                    }
                                    NotificationManager.this.currentNotification = null;
                                    if (NotificationManager.this.stagedNotification == null && NotificationManager.this.pendingScreen != null && NotificationManager.this.isScreenHighPriority(NotificationManager.this.pendingScreen)) {
                                        NotificationManager.this.bus.post(new ShowScreenWithArgs(NotificationManager.this.pendingScreen, NotificationManager.this.pendingScreenArgs, NotificationManager.this.pendingScreenArgs2, false));
                                        NotificationManager.sLogger.v("launched pending screen hp:" + NotificationManager.this.pendingScreen);
                                        NotificationManager.this.pendingScreen = null;
                                        NotificationManager.this.pendingScreenArgs = null;
                                        n2 = n;
                                        NotificationManager.this.pendingScreenArgs2 = null;
                                        if (!false) {
                                            final HomeScreenView homescreenView6 = NotificationManager.this.uiStateManager.getHomescreenView();
                                            if (homescreenView6.hasModeView()) {
                                                NotificationManager.sLogger.v("mode-view: notif onStop show");
                                                homescreenView6.animateInModeView();
                                            }
                                        }
                                        NotificationManager.this.isAnimating = false;
                                        return;
                                    }
                                    if (NotificationManager.this.stagedNotification != null) {
                                        NotificationManager.sLogger.v("notif-anim-stop staged-notif [" + NotificationManager.this.stagedNotification.notification.getId() + "]");
                                        NotificationManager.this.currentNotification = NotificationManager.this.stagedNotification;
                                        NotificationManager.this.stagedNotification = null;
                                        NotificationManager.this.isAnimating = false;
                                        n2 = 1;
                                        NotificationManager.this.showNotification();
                                        if (!true) {
                                            final HomeScreenView homescreenView7 = NotificationManager.this.uiStateManager.getHomescreenView();
                                            if (homescreenView7.hasModeView()) {
                                                NotificationManager.sLogger.v("mode-view: notif onStop show");
                                                homescreenView7.animateInModeView();
                                            }
                                        }
                                        NotificationManager.this.isAnimating = false;
                                        return;
                                    }
                                    continue Label_1715_Outer;
                                }
                                NotificationManager.sLogger.v("notif-anim-stop pushed back [" + id + "]");
                                if (NotificationManager.this.currentNotification.startCalled) {
                                    NotificationManager.this.currentNotification.startCalled = false;
                                    NotificationManager.this.currentNotification.notification.onStop();
                                    continue;
                                }
                                continue;
                            }
                        }
                        NotificationManager.this.isExpanded = true;
                        if (NotificationManager.this.currentNotification == null || NotificationManager.this.currentNotification.removed || NotificationManager.this.stagedNotification != null) {
                            NotificationManager.sLogger.v("notif-anim-start got removed/changed while it was animating, hide it");
                            NotificationManager.this.isAnimating = false;
                            NotificationManager.this.hideNotification();
                        }
                        else {
                            NotificationManager.this.setNotificationColor();
                            NotificationManager.this.notifView.showNextNotificationColor();
                            NotificationManager.this.notificationController.startTimeout(NotificationManager.this.currentNotification.notification.getTimeout());
                        }
                        NotificationManager.this.gestureEnabledSettings = HUDLightUtils.showGestureDetectionEnabled(HudApplication.getAppContext(), LightManager.getInstance(), "Notification");
                        continue;
                    }
                }
            }
        };
        this.scrollProgress = new IProgressUpdate() {
            @Override
            public void onPosChange(final int n) {
                if (NotificationManager.this.stackCurrentNotification != null && NotificationManager.this.stackCurrentNotification.notification.supportScroll()) {
                    int currentItem;
                    if (n <= 0) {
                        currentItem = 1;
                    }
                    else if ((currentItem = n) > 100) {
                        currentItem = 100;
                    }
                    if (NotificationManager.this.notifScrollIndicator.getCurrentItem() != currentItem) {
                        if (currentItem == 1 || currentItem == 100) {
                            NotificationManager.this.startScrollThresholdTimer();
                        }
                        NotificationManager.this.notifScrollIndicator.setCurrentItem(currentItem);
                    }
                }
            }
        };
        Mortar.inject(HudApplication.getAppContext(), this);
        this.uiStateManager.addNotificationAnimationListener(this.notifAnimationListener);
        this.NOTIFS_REMOVED_ON_DISCONNECT.add("navdy#phone#call#notif");
        this.NOTIFS_REMOVED_ON_DISCONNECT.add("navdy#music#notif");
        this.NOTIFS_NOT_ALLOWED_ON_DISCONNECT.add("navdy#phone#call#notif");
        this.NOTIFS_NOT_ALLOWED_ON_DISCONNECT.add("navdy#music#notif");
        this.animationTranslation = (int)HudApplication.getAppContext().getResources().getDimension(R.dimen.glance_anim_translation);
        this.bus.register(this);
    }
    
    private void addNotificationInternal(final INotification notification, final EnumSet<Screen> set) {
        Label_0020: {
            if (this.mainScreen != null) {
                break Label_0020;
            }
            this.getNotificationView();
            if (this.mainScreen != null) {
                break Label_0020;
            }
            return;
        }
        final String id = notification.getId();
        if (TextUtils.isEmpty((CharSequence)id)) {
            NotificationManager.sLogger.w("notification id is null");
            return;
        }
        final NotificationType type = notification.getType();
        if (type == null) {
            NotificationManager.sLogger.w("notification type is null");
            return;
        }
        while (true) {
            NotificationManager.sLogger.v("addNotif type[" + notification.getType() + "] id[" + id + "]");
            final Object lockObj = this.lockObj;
            Label_0316: {
            Label_0304:
                while (true) {
                    Label_0296: {
                        synchronized (lockObj) {
                            if (this.currentNotification != null && !this.isNotificationViewShowing()) {
                                final Logger sLogger = NotificationManager.sLogger;
                                final StringBuilder append = new StringBuilder().append("addNotif current-notification marked null:");
                                if (this.currentNotification.notification == null) {
                                    break Label_0296;
                                }
                                final Serializable type2 = this.currentNotification.notification.getType();
                                sLogger.v(append.append(type2).toString());
                                this.currentNotification = null;
                            }
                            if (this.currentNotification == null || notification.canAddToStackIfCurrentExists()) {
                                break;
                            }
                            if (!TextUtils.equals((CharSequence)this.currentNotification.notification.getId(), (CharSequence)notification.getId())) {
                                break Label_0316;
                            }
                            if (this.currentNotification.startCalled) {
                                NotificationManager.sLogger.v("addNotif updated");
                                this.currentNotification.notification.onUpdate();
                                return;
                            }
                            break Label_0304;
                        }
                    }
                    final Serializable type2 = "unk";
                    continue;
                }
                NotificationManager.sLogger.v("addNotif cannot be added current rule-1");
                return;
            }
            NotificationManager.sLogger.v("addNotif cannot be added current rule-2");
            return;
        }
        if (set != null) {
            if (this.currentNotification != null) {
                NotificationManager.sLogger.v("addNotif screen constraint failed, current notification active");
                // monitorexit(o)
                return;
            }
            final BaseScreen currentScreen = this.uiStateManager.getCurrentScreen();
            if (currentScreen == null || !set.contains(currentScreen.getScreen())) {
                NotificationManager.sLogger.v("addNotif screen constraint failed, current screen:" + currentScreen);
                // monitorexit(o)
                return;
            }
            if (this.mainScreen.isNotificationExpanding() || this.mainScreen.isNotificationCollapsing() || this.mainScreen.isNotificationViewShowing() || this.mainScreen.isScreenAnimating()) {
                NotificationManager.sLogger.v("addNotif screen constraint failed, mainscreen animating");
                // monitorexit(o)
                return;
            }
        }
        if (this.currentNotification != null && TextUtils.equals((CharSequence)this.currentNotification.notification.getId(), (CharSequence)notification.getId())) {
            if (this.currentNotification.removed) {
                NotificationManager.sLogger.v("addNotif notif was marked to remove,resurrecting it back");
                this.currentNotification.removed = false;
                this.currentNotification.resurrected = true;
            }
            else if (this.currentNotification.startCalled) {
                NotificationManager.sLogger.v("addNotif already on screen,update");
                notification.onUpdate();
            }
            else {
                NotificationManager.sLogger.v("addNotif not on screen yet");
                if (!this.mainScreen.isNotificationViewShowing()) {
                    this.showNotification();
                }
            }
            // monitorexit(o)
            return;
        }
        final Info currentNotification = this.notificationIdMap.get(id);
        if (currentNotification != null) {
            if (this.currentNotification == null) {
                NotificationManager.sLogger.v("addNotif already exist on stack, making it current");
                this.currentNotification = currentNotification;
                this.showNotification();
            }
            else {
                NotificationManager.sLogger.v("addNotif already exist on stack, no-op");
            }
            // monitorexit(o)
            return;
        }
        final Info info = new Info();
        info.notification = notification;
        info.priority = type.getPriority();
        final long n = this.notificationCounter + 1L;
        this.notificationCounter = n;
        info.uniqueCounter = n;
        this.notificationIdMap.put(id, info);
        this.notificationPriorityMap.put(info, info);
        NotificationManager.sLogger.v("addNotif added to stack");
        if (this.currentNotification == null) {
            NotificationManager.sLogger.v("addNotif make current [" + id + "]");
            this.currentNotification = info;
            if (this.isExpanded()) {
                this.currentNotification.pushBackDuetoHighPriority = true;
                this.hideNotification();
            }
            else if (!this.showNotification()) {
                NotificationManager.sLogger.v("addNotif not making current, disabled");
                this.currentNotification.pushBackDuetoHighPriority = true;
                this.currentNotification = null;
            }
        }
        else {
            final boolean b = info.priority > this.currentNotification.priority;
            final boolean purgeable = this.currentNotification.notification.isPurgeable();
            if (b || purgeable) {
                NotificationManager.sLogger.v("addNotif high priority[" + b + "] purgeable[" + purgeable + "]");
                if (this.stagedNotification != null) {
                    if (TextUtils.equals((CharSequence)this.stagedNotification.notification.getId(), (CharSequence)info.notification.getId())) {
                        NotificationManager.sLogger.v("addNotif stage notif already added");
                        // monitorexit(o)
                        return;
                    }
                    if (info.priority <= this.stagedNotification.priority) {
                        NotificationManager.sLogger.v("addNotif not replacing staged notif staged [" + this.stagedNotification.notification.getId() + "]");
                        // monitorexit(o)
                        return;
                    }
                    NotificationManager.sLogger.v("addNotif replacing staged notif since it high priority than staged [" + this.stagedNotification.notification.getId() + "]");
                    this.stagedNotification.pushBackDuetoHighPriority = true;
                    this.stagedNotification = info;
                    if (!this.isAnimating) {
                        if (this.isExpanded) {
                            NotificationManager.sLogger.v("addNotif-1 hideNotif");
                            this.hideNotification();
                            // monitorexit(o)
                            return;
                        }
                        NotificationManager.sLogger.v("addNotif-1 showNotif");
                        this.showNotification();
                    }
                    else {
                        NotificationManager.sLogger.v("addNotif anim in progress");
                    }
                }
                else {
                    if (!this.currentNotification.notification.isAlive()) {
                        this.currentNotification.removed = true;
                        NotificationManager.sLogger.v("addNotif current notification not alive anymore [" + this.currentNotification.notification.getType() + "]");
                    }
                    else {
                        this.currentNotification.pushBackDuetoHighPriority = true;
                    }
                    this.stagedNotification = info;
                    if (!this.isAnimating) {
                        if (!this.isExpanded) {
                            if (!this.currentNotification.startCalled) {
                                NotificationManager.sLogger.v("addNotif swapping notif");
                                this.stagedNotification = null;
                                this.currentNotification = info;
                            }
                            else {
                                NotificationManager.sLogger.v("addNotif showNotif");
                            }
                            this.showNotification();
                        }
                        else {
                            NotificationManager.sLogger.v("addNotif hideNotif");
                            this.hideNotification();
                        }
                    }
                    else {
                        NotificationManager.sLogger.v("addNotif anim in progress");
                    }
                }
            }
            else {
                NotificationManager.sLogger.v("addNotif lower priority than current[" + this.currentNotification.notification.getType() + "] update color");
                info.pushBackDuetoHighPriority = true;
                this.setNotificationColor();
                if (!this.isAnimating && !this.isExpanded && !this.isNotificationViewShowing()) {
                    NotificationManager.sLogger.v("add notif lower priority, but view hidden");
                    this.showNotification();
                }
            }
        }
        this.postNotificationChange();
        this.updateExpandedIndicator();
    }
    // monitorexit(o)
    
    private void animateOutExpandedView(final boolean b, final Info info, final boolean b2) {
        if (this.isExpanded() || this.isExpandedNotificationVisible()) {
            this.runOperation(NotificationAnimator.Operation.COLLAPSE_VIEW, false, b, b2, false, null, false);
        }
    }
    
    private void animateOutExpandedView(final boolean b, final boolean b2) {
        this.animateOutExpandedView(b, this.currentNotification, b2);
    }
    
    private void cleanupView(final View view) {
        final GlanceViewCache.ViewType viewType = (GlanceViewCache.ViewType)view.getTag(R.id.glance_view_type);
        view.setTag(R.id.glance_view_type, null);
        view.setTag(R.id.glance_can_delete, null);
        if (viewType != null) {
            switch (viewType) {
                case SMALL_IMAGE:
                    ((ImageView)view.findViewById(R.id.imageView)).setImageResource(0);
                    GlanceViewCache.putView(GlanceViewCache.ViewType.SMALL_IMAGE, view);
                    break;
                case SMALL_GLANCE_MESSAGE: {
                    final TextView textView = (TextView)view.findViewById(R.id.mainTitle);
                    textView.setAlpha(1.0f);
                    textView.setVisibility(View.VISIBLE);
                    final TextView textView2 = (TextView)view.findViewById(R.id.subTitle);
                    textView2.setAlpha(1.0f);
                    textView2.setVisibility(View.VISIBLE);
                    final View viewById = view.findViewById(R.id.choiceLayout);
                    if (viewById != null) {
                        viewById.setAlpha(1.0f);
                        viewById.setVisibility(View.VISIBLE);
                        viewById.setTag(null);
                        viewById.setTag(R.id.message_prev_choice, null);
                        viewById.setTag(R.id.message_secondary_screen, null);
                    }
                    final InitialsImageView initialsImageView = (InitialsImageView)view.findViewById(R.id.mainImage);
                    initialsImageView.setImage(0, null, InitialsImageView.Style.DEFAULT);
                    initialsImageView.setTag(null);
                    ((ImageView)view.findViewById(R.id.sideImage)).setImageResource(0);
                    view.setAlpha(1.0f);
                    GlanceViewCache.putView(GlanceViewCache.ViewType.SMALL_GLANCE_MESSAGE, view);
                    break;
                }
            }
        }
    }
    
    private void cleanupViews(final NotificationView notificationView) {
        NotificationManager.sLogger.v("*** cleaning up expanded view");
        this.expandedWithStack = false;
        final FrameLayout expandedNotificationView = this.getExpandedNotificationView();
        final View expandedViewChild = this.getExpandedViewChild();
        if (expandedViewChild != null) {
            ((ViewGroup)expandedNotificationView).removeView(expandedViewChild);
            if (GlanceHelper.isDeleteAllView(expandedViewChild)) {
                expandedViewChild.setTag(null);
                expandedViewChild.setAlpha(1.0f);
                GlanceViewCache.putView(GlanceViewCache.ViewType.BIG_TEXT, expandedViewChild);
            }
            NotificationManager.sLogger.v("*** removed expanded view");
        }
        final ViewGroup notificationContainer = this.notifView.getNotificationContainer();
        if (notificationContainer.getChildCount() > 0) {
            final View child = notificationContainer.getChildAt(0);
            notificationContainer.removeView(child);
            if (GlanceHelper.isDeleteAllView(child)) {
                child.setTag(null);
                this.cleanupView(child);
            }
            NotificationManager.sLogger.v("*** removed notif view");
        }
        if (this.stackCurrentNotification != null && this.stackCurrentNotification.startCalled) {
            this.stackCurrentNotification.startCalled = false;
            this.stackCurrentNotification.notification.onStop();
        }
        this.stackCurrentNotification = null;
        this.lastStackCurrentNotification = null;
        ((ViewGroup)expandedNotificationView).setVisibility(GONE);
        final CarouselIndicator notificationIndicator = this.getNotificationIndicator();
        if (notificationIndicator != null) {
            notificationIndicator.setVisibility(GONE);
            this.hideScrollingIndicator();
        }
        this.setNotificationColor();
        notificationView.showNextNotificationColor();
    }
    
    private void clearAllGlances() {
        NotificationManager.sLogger.v("glances turned off");
        if (this.isExpanded() || this.isExpandedNotificationVisible()) {
            NotificationManager.sLogger.v("glances turned off: expanded mode");
            this.deleteAllGlances = true;
            this.clearOperationQueue();
            this.collapseExpandedNotification(true, true);
        }
        else if (this.isNotificationViewShowing()) {
            NotificationManager.sLogger.v("glances turned off: notif showing");
            this.deleteAllGlances = true;
            this.collapseNotification();
        }
        else {
            NotificationManager.sLogger.v("glances turned off: notif not showing");
            final Object lockObj = this.lockObj;
            synchronized (lockObj) {
                this.removeAllNotification();
            }
        }
    }
    
    private void collapseExpandedViewInternal(final boolean b, final boolean b2) {
        NotificationManager.sLogger.v("collapseExpandedViewInternal");
        this.isAnimating = true;
        this.clearOperationQueue();
        final Runnable runnable = new Runnable() {
            @Override
            public void run() {
                NotificationManager.this.getExpandedNotificationCoverView().setVisibility(GONE);
                NotificationManager.this.expandedWithStack = false;
                final boolean val$hideNotification = b;
                int n;
                if (NotificationManager.this.stackCurrentNotification == null || NotificationManager.this.stackCurrentNotification == NotificationManager.this.currentNotification) {
                    n = (val$hideNotification ? 1 : 0);
                    if (NotificationManager.this.currentNotification != null) {
                        if (NotificationManager.this.currentNotification.removed) {
                            n = 1;
                            NotificationManager.sLogger.v("stackchange:hide-remove");
                        }
                        else {
                            n = (val$hideNotification ? 1 : 0);
                            if (!NotificationManager.this.currentNotification.startCalled) {
                                n = (val$hideNotification ? 1 : 0);
                                if (!NotificationManager.this.currentNotification.removed) {
                                    NotificationManager.this.currentNotification.notification.getView(NotificationManager.this.notifView.getContext());
                                    NotificationManager.this.currentNotification.startCalled = true;
                                    NotificationManager.this.currentNotification.notification.onStart(NotificationManager.this.notificationController);
                                    n = (val$hideNotification ? 1 : 0);
                                }
                            }
                        }
                    }
                }
                else {
                    NotificationManager.sLogger.v("stackchange:current and stack are diff");
                    if (NotificationManager.this.currentNotification != null && NotificationManager.this.currentNotification.startCalled) {
                        NotificationManager.sLogger.v("stackchange:stopping current:" + NotificationManager.this.currentNotification.notification.getId());
                        NotificationManager.this.currentNotification.startCalled = false;
                        NotificationManager.this.currentNotification.notification.onStop();
                    }
                    NotificationManager.this.currentNotification = NotificationManager.this.stackCurrentNotification;
                    n = (val$hideNotification ? 1 : 0);
                    if (NotificationManager.this.currentNotification != null) {
                        NotificationManager.this.notifView.switchNotfication(NotificationManager.this.currentNotification.notification);
                        boolean b;
                        if (NotificationManager.this.currentNotification.removed) {
                            b = true;
                            NotificationManager.sLogger.v("stackchange:hide-diff-remove");
                        }
                        else {
                            b = val$hideNotification;
                            if (!NotificationManager.this.currentNotification.startCalled) {
                                NotificationManager.sLogger.v("stackchange:starting stacked:" + NotificationManager.this.currentNotification.notification.getId());
                                NotificationManager.this.currentNotification.notification.getView(NotificationManager.this.notifView.getContext());
                                NotificationManager.this.currentNotification.startCalled = true;
                                NotificationManager.this.currentNotification.notification.onStart(NotificationManager.this.notificationController);
                                b = val$hideNotification;
                            }
                        }
                        n = (b ? 1 : 0);
                        if (!b) {
                            NotificationManager.sLogger.v("calling onExpandedNotificationEvent-collapse-");
                            NotificationManager.this.currentNotification.notification.onExpandedNotificationEvent(UIStateManager.Mode.COLLAPSE);
                            n = (b ? 1 : 0);
                        }
                    }
                    NotificationManager.this.setNotificationColor();
                    NotificationManager.this.notifView.showNextNotificationColor();
                }
                NotificationManager.this.stackCurrentNotification = null;
                NotificationManager.this.lastStackCurrentNotification = null;
                NotificationManager.this.clearOperationQueue();
                if (n != 0) {
                    NotificationManager.this.hideNotificationInternal();
                }
                else {
                    NotificationManager.this.isAnimating = false;
                }
            }
        };
        final FrameLayout expandedNotificationView = this.getExpandedNotificationView();
        final View expandedViewChild = this.getExpandedViewChild();
        final Runnable runnable2 = new Runnable() {
            @Override
            public void run() {
                NotificationManager.this.hideScrollingIndicator();
                if (expandedViewChild != null) {
                    ((ViewGroup)expandedNotificationView).removeView(expandedViewChild);
                }
                if (GlanceHelper.isDeleteAllView(expandedViewChild)) {
                    expandedViewChild.setTag(null);
                    expandedViewChild.setAlpha(1.0f);
                    GlanceViewCache.putView(GlanceViewCache.ViewType.BIG_TEXT, expandedViewChild);
                }
                NotificationAnimator.animateExpandedViewOut(NotificationManager.this.notifView, NotificationManager.this.uiStateManager.getMainPanelWidth() - NotificationManager.this.uiStateManager.getSidePanelWidth(), (View)NotificationManager.this.expandedNotifView, (View)NotificationManager.this.notifIndicator, runnable, NotificationManager.this.currentNotification, NotificationManager.this.showOn, NotificationManager.this.isExpandedNotificationVisible());
            }
        };
        if ((this.showOn || this.isExpandedNotificationVisible()) && expandedViewChild != null) {
            int n;
            if (b2) {
                n = 0;
            }
            else {
                n = 250;
            }
            NotificationAnimator.animateChildViewOut(expandedViewChild, n, runnable2);
        }
        else {
            runnable2.run();
        }
    }
    
    private void collapseNotificationInternal(final boolean b, final boolean b2, final boolean b3) {
        NotificationManager.sLogger.v("collapseNotificationInternal");
        if (this.getNotificationView() != null && (this.isExpanded() || this.isExpandedNotificationVisible())) {
            if (!b3) {
                this.animateOutExpandedView(b, this.currentNotification, b2);
            }
            else {
                this.collapseExpandedViewInternal(b, b2);
            }
        }
    }
    
    private void deleteAllGlances() {
        final Object lockObj = this.lockObj;
        synchronized (lockObj) {
            if (this.deleteAllGlances) {
                this.animateOutExpandedView(true, true);
            }
        }
    }
    
    private void displayScrollingIndicator(final int n) {
        if (this.isExpanded() && this.showOn) {
            if (this.stackCurrentNotification != null && this.stackCurrentNotification.notification.supportScroll()) {
                final IScrollEvent scrollEvent = (IScrollEvent)this.stackCurrentNotification.notification;
                this.notifScrollIndicator.setBackgroundColor(this.stackCurrentNotification.notification.getColor());
                this.notifIndicator.getViewTreeObserver().addOnGlobalLayoutListener((ViewTreeObserver.OnGlobalLayoutListener)new ViewTreeObserver.OnGlobalLayoutListener() {
                    public void onGlobalLayout() {
                        final RectF itemPos = NotificationManager.this.notifIndicator.getItemPos(n);
                        Label_0050: {
                            if (itemPos == null) {
                                break Label_0050;
                            }
                            if (itemPos.left != 0.0f || itemPos.top != 0.0f) {
                                NotificationManager.this.setNotifScrollIndicatorXY(itemPos, scrollEvent);
                                break Label_0050;
                            }
                            return;
                        }
                        NotificationManager.this.notifIndicator.getViewTreeObserver().removeOnGlobalLayoutListener((ViewTreeObserver.OnGlobalLayoutListener)this);
                    }
                });
            }
            else {
                this.hideScrollingIndicator();
            }
        }
    }
    
    private void executeItemInternal() {
        final boolean loggable = NotificationManager.sLogger.isLoggable(2);
        if (this.isExpanded()) {
            if (this.stackCurrentNotification != null) {
                if (loggable) {
                    NotificationManager.sLogger.v("executeItemInternal: expanded :" + this.stackCurrentNotification);
                }
                this.clearOperationQueue();
                ((InputManager.IInputHandler)this.stackCurrentNotification.notification).onKey(InputManager.CustomKeyEvent.SELECT);
            }
            else {
                if (loggable) {
                    NotificationManager.sLogger.v("executeItemInternal: expanded check for delete:");
                }
                View view;
                if (this.showOn) {
                    view = this.getExpandedViewChild();
                }
                else {
                    view = this.notifView.getCurrentNotificationViewChild();
                }
                if (GlanceHelper.isDeleteAllView(view)) {
                    if (view.getTag(R.id.glance_can_delete) != null) {
                        NotificationManager.sLogger.v("executeItemInternal: no glance to delete");
                        this.runQueuedOperation();
                    }
                    else {
                        if (loggable) {
                            NotificationManager.sLogger.v("executeItemInternal: delete all");
                        }
                        this.deleteAllGlances = true;
                        this.clearOperationQueue();
                        GlanceHelper.showGlancesDeletedToast();
                        NotificationManager.sLogger.v("executeItemInternal: show delete all toast");
                    }
                }
            }
        }
        else if (this.isExpandedNotificationVisible()) {
            if (loggable) {
                NotificationManager.sLogger.v("executeItemInternal: expandedNoStack :" + this.currentNotification);
            }
            if (this.currentNotification != null) {
                this.clearOperationQueue();
                ((InputManager.IInputHandler)this.currentNotification.notification).onKey(InputManager.CustomKeyEvent.SELECT);
            }
            else {
                this.runQueuedOperation();
            }
        }
    }
    
    private View getExpandedNotificationCoverView() {
        View view;
        if (this.expandedNotifCoverView != null) {
            view = this.expandedNotifCoverView;
        }
        else {
            if (this.mainScreen == null) {
                this.getMainScreen();
            }
            if (this.mainScreen != null) {
                this.expandedNotifCoverView = this.mainScreen.getExpandedNotificationCoverView();
            }
            view = this.expandedNotifCoverView;
        }
        return view;
    }
    
    private FrameLayout getExpandedNotificationView() {
        FrameLayout frameLayout;
        if (this.expandedNotifView != null) {
            frameLayout = this.expandedNotifView;
        }
        else {
            if (this.mainScreen == null) {
                this.getMainScreen();
            }
            if (this.mainScreen != null) {
                this.expandedNotifView = this.mainScreen.getExpandedNotificationView();
            }
            frameLayout = this.expandedNotifView;
        }
        return frameLayout;
    }
    
    private Info getHigherNotification(Info info) {
        Info info2;
        do {
            info = this.notificationPriorityMap.higherKey(info);
            if (info == null) {
                info2 = null;
                break;
            }
            info2 = info;
            if (!(info.notification instanceof CallNotification)) {
                break;
            }
            info2 = info;
        } while (!info.notification.isAlive());
        return info2;
    }
    
    private Info getHighestNotification() {
        final Object lockObj = this.lockObj;
        synchronized (lockObj) {
            return this.notificationPriorityMap.lastKey();
        }
    }
    
    public static NotificationManager getInstance() {
        return NotificationManager.sInstance;
    }
    
    private Info getLowerNotification(Info info) {
        Info info2;
        do {
            info = this.notificationPriorityMap.lowerKey(info);
            if (info == null) {
                info2 = null;
                break;
            }
            info2 = info;
            if (!(info.notification instanceof CallNotification)) {
                break;
            }
            info2 = info;
        } while (!info.notification.isAlive());
        return info2;
    }
    
    private Info getLowestNotification() {
        final Object lockObj = this.lockObj;
        synchronized (lockObj) {
            return this.notificationPriorityMap.firstKey();
        }
    }
    
    private Main getMainScreen() {
        if (this.mainScreen == null) {
            this.mainScreen = this.uiStateManager.getRootScreen();
        }
        return this.mainScreen;
    }
    
    private int getNonRemovableNotifCount() {
        int n = 0;
        if (this.getNotification("navdy#phone#call#notif") != null) {
            n = 0 + 1;
        }
        int n2 = n;
        if (this.getNotification("navdy#traffic#reroute#notif") != null) {
            n2 = n + 1;
        }
        return n2;
    }
    
    private Info getNotificationByIndex(final int n) {
        // monitorenter(lockObj = this.lockObj)
        Label_0024: {
            if (n < 0) {
                break Label_0024;
            }
            while (true) {
            Label_0095:
                while (true) {
                    int n2 = 0;
                    Label_0089: {
                        try {
                            Info info;
                            if (n >= this.notificationPriorityMap.size()) {
                                // monitorexit(lockObj)
                                info = null;
                            }
                            else {
                                final Iterator<Info> iterator = this.notificationPriorityMap.descendingKeySet().iterator();
                                n2 = 0;
                                if (!iterator.hasNext()) {
                                    break Label_0095;
                                }
                                info = iterator.next();
                                if (n2 != n) {
                                    break Label_0089;
                                }
                            }
                            // monitorexit(lockObj)
                            return info;
                        }
                        finally {
                        }
                        // monitorexit(lockObj)
                    }
                    ++n2;
                    continue;
                }
                // monitorexit(lockObj)
                return null;
            }
        }
    }
    
    private Info getNotificationFromId(final String s) {
        final Object lockObj = this.lockObj;
        synchronized (lockObj) {
            return this.notificationIdMap.get(s);
        }
    }
    
    private int getNotificationIndex(final Info info) {
        final Object lockObj = this.lockObj;
        synchronized (lockObj) {
            final Iterator<Info> iterator = this.notificationPriorityMap.descendingKeySet().iterator();
            int n = -1;
            while (iterator.hasNext()) {
                final int n2 = ++n;
                if (iterator.next() == info) {
                    return n2;
                }
            }
            // monitorexit(lockObj)
            return -1;
        }
    }
    
    private CarouselIndicator getNotificationIndicator() {
        CarouselIndicator carouselIndicator;
        if (this.notifIndicator != null) {
            carouselIndicator = this.notifIndicator;
        }
        else {
            if (this.mainScreen == null) {
                this.getMainScreen();
            }
            if (this.mainScreen != null) {
                this.notifIndicator = this.mainScreen.getNotificationIndicator();
                this.notifScrollIndicator = this.mainScreen.getNotificationScrollIndicator();
            }
            carouselIndicator = this.notifIndicator;
        }
        return carouselIndicator;
    }
    
    private NotificationView getNotificationView() {
        NotificationView notificationView;
        if (this.notifView != null) {
            notificationView = this.notifView;
        }
        else {
            if (this.mainScreen == null) {
                this.getMainScreen();
            }
            if (this.mainScreen != null) {
                this.notifView = this.mainScreen.getNotificationView();
            }
            notificationView = this.notifView;
        }
        return notificationView;
    }
    
    private void handleExpandedMove(final InputManager.CustomKeyEvent p0, final boolean p1) {
        // 
        This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     3: new             Ljava/lang/StringBuilder;
        //     6: dup            
        //     7: invokespecial   java/lang/StringBuilder.<init>:()V
        //    10: ldc_w           "handleExpandedMove key:"
        //    13: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    16: aload_1        
        //    17: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //    20: ldc_w           " gesture:"
        //    23: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    26: iload_2        
        //    27: invokevirtual   java/lang/StringBuilder.append:(Z)Ljava/lang/StringBuilder;
        //    30: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //    33: invokevirtual   com/navdy/service/library/log/Logger.v:(Ljava/lang/String;)V
        //    36: aconst_null    
        //    37: astore_3       
        //    38: aconst_null    
        //    39: astore          4
        //    41: iconst_0       
        //    42: istore          5
        //    44: iconst_0       
        //    45: istore          6
        //    47: iconst_0       
        //    48: istore          7
        //    50: iconst_0       
        //    51: istore          8
        //    53: iconst_0       
        //    54: istore          9
        //    56: iconst_0       
        //    57: istore          10
        //    59: aload_0        
        //    60: getfield        com/navdy/hud/app/framework/notifications/NotificationManager.notifIndicator:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;
        //    63: invokevirtual   com/navdy/hud/app/ui/component/carousel/CarouselIndicator.getCurrentItem:()I
        //    66: istore          11
        //    68: aload_0        
        //    69: getfield        com/navdy/hud/app/framework/notifications/NotificationManager.notifIndicator:Lcom/navdy/hud/app/ui/component/carousel/CarouselIndicator;
        //    72: invokevirtual   com/navdy/hud/app/ui/component/carousel/CarouselIndicator.getItemCount:()I
        //    75: istore          12
        //    77: iload           8
        //    79: istore          13
        //    81: getstatic       com/navdy/hud/app/framework/notifications/NotificationManager$16.$SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent:[I
        //    84: aload_1        
        //    85: invokevirtual   com/navdy/hud/app/manager/InputManager$CustomKeyEvent.ordinal:()I
        //    88: iaload         
        //    89: tableswitch {
        //                2: 367
        //                3: 635
        //                4: 973
        //          default: 116
        //        }
        //   116: iload           5
        //   118: istore_2       
        //   119: aload           4
        //   121: astore_1       
        //   122: iload           8
        //   124: istore          13
        //   126: aload_0        
        //   127: getfield        com/navdy/hud/app/framework/notifications/NotificationManager.notifView:Lcom/navdy/hud/app/view/NotificationView;
        //   130: invokevirtual   com/navdy/hud/app/view/NotificationView.getContext:()Landroid/content/Context;
        //   133: astore          14
        //   135: aload_1        
        //   136: ifnonnull       144
        //   139: iload           7
        //   141: ifeq            1505
        //   144: iload           8
        //   146: istore          13
        //   148: aload_0        
        //   149: getfield        com/navdy/hud/app/framework/notifications/NotificationManager.lockObj:Ljava/lang/Object;
        //   152: astore          15
        //   154: iload           8
        //   156: istore          13
        //   158: aload           15
        //   160: dup            
        //   161: astore          16
        //   163: monitorenter   
        //   164: aload_1        
        //   165: ifnull          1141
        //   168: getstatic       com/navdy/hud/app/framework/notifications/NotificationManager.sLogger:Lcom/navdy/service/library/log/Logger;
        //   171: iconst_2       
        //   172: invokevirtual   com/navdy/service/library/log/Logger.isLoggable:(I)Z
        //   175: ifeq            218
        //   178: getstatic       com/navdy/hud/app/framework/notifications/NotificationManager.sLogger:Lcom/navdy/service/library/log/Logger;
        //   181: astore          4
        //   183: new             Ljava/lang/StringBuilder;
        //   186: astore_3       
        //   187: aload_3        
        //   188: invokespecial   java/lang/StringBuilder.<init>:()V
        //   191: aload           4
        //   193: aload_3        
        //   194: ldc_w           "handleExpandedMove: going to "
        //   197: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   200: aload_1        
        //   201: getfield        com/navdy/hud/app/framework/notifications/NotificationManager$Info.notification:Lcom/navdy/hud/app/framework/notifications/INotification;
        //   204: invokeinterface com/navdy/hud/app/framework/notifications/INotification.getId:()Ljava/lang/String;
        //   209: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   212: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   215: invokevirtual   com/navdy/service/library/log/Logger.v:(Ljava/lang/String;)V
        //   218: aload_1        
        //   219: getfield        com/navdy/hud/app/framework/notifications/NotificationManager$Info.notification:Lcom/navdy/hud/app/framework/notifications/INotification;
        //   222: aload           14
        //   224: invokeinterface com/navdy/hud/app/framework/notifications/INotification.getView:(Landroid/content/Context;)Landroid/view/View;
        //   229: astore          17
        //   231: aload_0        
        //   232: getfield        com/navdy/hud/app/framework/notifications/NotificationManager.showOn:Z
        //   235: ifeq            1135
        //   238: aload_1        
        //   239: getfield        com/navdy/hud/app/framework/notifications/NotificationManager$Info.notification:Lcom/navdy/hud/app/framework/notifications/INotification;
        //   242: aload           14
        //   244: aconst_null    
        //   245: invokeinterface com/navdy/hud/app/framework/notifications/INotification.getExpandedView:(Landroid/content/Context;Ljava/lang/Object;)Landroid/view/View;
        //   250: astore          4
        //   252: aload_1        
        //   253: iconst_1       
        //   254: putfield        com/navdy/hud/app/framework/notifications/NotificationManager$Info.startCalled:Z
        //   257: aload_1        
        //   258: getfield        com/navdy/hud/app/framework/notifications/NotificationManager$Info.notification:Lcom/navdy/hud/app/framework/notifications/INotification;
        //   261: aload_0        
        //   262: getfield        com/navdy/hud/app/framework/notifications/NotificationManager.notificationController:Lcom/navdy/hud/app/framework/notifications/INotificationController;
        //   265: invokeinterface com/navdy/hud/app/framework/notifications/INotification.onStart:(Lcom/navdy/hud/app/framework/notifications/INotificationController;)V
        //   270: aload_1        
        //   271: getfield        com/navdy/hud/app/framework/notifications/NotificationManager$Info.notification:Lcom/navdy/hud/app/framework/notifications/INotification;
        //   274: invokeinterface com/navdy/hud/app/framework/notifications/INotification.getColor:()I
        //   279: istore          7
        //   281: aload           4
        //   283: astore_3       
        //   284: aload_0        
        //   285: getfield        com/navdy/hud/app/framework/notifications/NotificationManager.stackCurrentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
        //   288: ifnull          1491
        //   291: aload_0        
        //   292: getfield        com/navdy/hud/app/framework/notifications/NotificationManager.stackCurrentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
        //   295: getfield        com/navdy/hud/app/framework/notifications/NotificationManager$Info.notification:Lcom/navdy/hud/app/framework/notifications/INotification;
        //   298: iconst_0       
        //   299: invokeinterface com/navdy/hud/app/framework/notifications/INotification.getViewSwitchAnimation:(Z)Landroid/animation/AnimatorSet;
        //   304: astore          4
        //   306: aload           4
        //   308: ifnull          1477
        //   311: new             Lcom/navdy/hud/app/framework/notifications/NotificationManager$14;
        //   314: astore          18
        //   316: aload           18
        //   318: aload_0        
        //   319: aload           17
        //   321: aload_3        
        //   322: aload_1        
        //   323: iload_2        
        //   324: iload           7
        //   326: invokespecial   com/navdy/hud/app/framework/notifications/NotificationManager$14.<init>:(Lcom/navdy/hud/app/framework/notifications/NotificationManager;Landroid/view/View;Landroid/view/View;Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;ZI)V
        //   329: aload           4
        //   331: aload           18
        //   333: invokevirtual   android/animation/AnimatorSet.addListener:(Landroid/animation/Animator.AnimatorListener;)V
        //   336: aload           4
        //   338: ldc2_w          100
        //   341: invokevirtual   android/animation/AnimatorSet.setDuration:(J)Landroid/animation/AnimatorSet;
        //   344: pop            
        //   345: aload           4
        //   347: invokevirtual   android/animation/AnimatorSet.start:()V
        //   350: aload           16
        //   352: monitorexit    
        //   353: iload           10
        //   355: istore          7
        //   357: iload           7
        //   359: ifeq            366
        //   362: aload_0        
        //   363: invokespecial   com/navdy/hud/app/framework/notifications/NotificationManager.runQueuedOperation:()V
        //   366: return         
        //   367: iload           11
        //   369: ifne            413
        //   372: iconst_1       
        //   373: istore          7
        //   375: iload           7
        //   377: istore          13
        //   379: getstatic       com/navdy/hud/app/framework/notifications/NotificationManager.sLogger:Lcom/navdy/service/library/log/Logger;
        //   382: iconst_2       
        //   383: invokevirtual   com/navdy/service/library/log/Logger.isLoggable:(I)Z
        //   386: ifeq            402
        //   389: iload           7
        //   391: istore          13
        //   393: getstatic       com/navdy/hud/app/framework/notifications/NotificationManager.sLogger:Lcom/navdy/service/library/log/Logger;
        //   396: ldc_w           "handleExpandedMove: cannot go up"
        //   399: invokevirtual   com/navdy/service/library/log/Logger.v:(Ljava/lang/String;)V
        //   402: iconst_1       
        //   403: ifeq            366
        //   406: aload_0        
        //   407: invokespecial   com/navdy/hud/app/framework/notifications/NotificationManager.runQueuedOperation:()V
        //   410: goto            366
        //   413: iload           11
        //   415: iconst_1       
        //   416: if_icmpne       429
        //   419: iconst_1       
        //   420: istore          7
        //   422: aload_3        
        //   423: astore_1       
        //   424: iconst_1       
        //   425: istore_2       
        //   426: goto            122
        //   429: iload           8
        //   431: istore          13
        //   433: aload_0        
        //   434: getfield        com/navdy/hud/app/framework/notifications/NotificationManager.lockObj:Ljava/lang/Object;
        //   437: astore_3       
        //   438: iload           8
        //   440: istore          13
        //   442: aload_3        
        //   443: dup            
        //   444: astore          19
        //   446: monitorenter   
        //   447: aload_0        
        //   448: getfield        com/navdy/hud/app/framework/notifications/NotificationManager.stackCurrentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
        //   451: ifnull          540
        //   454: aload_0        
        //   455: aload_0        
        //   456: getfield        com/navdy/hud/app/framework/notifications/NotificationManager.stackCurrentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
        //   459: invokespecial   com/navdy/hud/app/framework/notifications/NotificationManager.getHigherNotification:(Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;)Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
        //   462: astore          4
        //   464: iload           6
        //   466: istore          7
        //   468: aload           4
        //   470: astore_1       
        //   471: aload           4
        //   473: ifnonnull       512
        //   476: iconst_1       
        //   477: istore          13
        //   479: iload           13
        //   481: istore          7
        //   483: aload           4
        //   485: astore_1       
        //   486: getstatic       com/navdy/hud/app/framework/notifications/NotificationManager.sLogger:Lcom/navdy/service/library/log/Logger;
        //   489: iconst_2       
        //   490: invokevirtual   com/navdy/service/library/log/Logger.isLoggable:(I)Z
        //   493: ifeq            512
        //   496: getstatic       com/navdy/hud/app/framework/notifications/NotificationManager.sLogger:Lcom/navdy/service/library/log/Logger;
        //   499: ldc_w           "handleExpandedMove: cannot go up, no notification left"
        //   502: invokevirtual   com/navdy/service/library/log/Logger.v:(Ljava/lang/String;)V
        //   505: aload           4
        //   507: astore_1       
        //   508: iload           13
        //   510: istore          7
        //   512: aload           19
        //   514: monitorexit    
        //   515: goto            424
        //   518: astore_1       
        //   519: aload           19
        //   521: monitorexit    
        //   522: iload           8
        //   524: istore          13
        //   526: aload_1        
        //   527: athrow         
        //   528: astore_1       
        //   529: iload           13
        //   531: ifeq            538
        //   534: aload_0        
        //   535: invokespecial   com/navdy/hud/app/framework/notifications/NotificationManager.runQueuedOperation:()V
        //   538: aload_1        
        //   539: athrow         
        //   540: aload_0        
        //   541: getfield        com/navdy/hud/app/framework/notifications/NotificationManager.lastStackCurrentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
        //   544: astore          4
        //   546: aload_0        
        //   547: aconst_null    
        //   548: putfield        com/navdy/hud/app/framework/notifications/NotificationManager.lastStackCurrentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
        //   551: iload           6
        //   553: istore          7
        //   555: aload           4
        //   557: astore_1       
        //   558: aload           4
        //   560: ifnonnull       512
        //   563: getstatic       com/navdy/hud/app/framework/notifications/NotificationManager.sLogger:Lcom/navdy/service/library/log/Logger;
        //   566: ldc_w           "handleExpandedMove: no prev notification"
        //   569: invokevirtual   com/navdy/service/library/log/Logger.v:(Ljava/lang/String;)V
        //   572: iload           6
        //   574: istore          7
        //   576: aload           4
        //   578: astore_1       
        //   579: iload           11
        //   581: iload           12
        //   583: iconst_1       
        //   584: isub           
        //   585: if_icmpne       512
        //   588: aload_0        
        //   589: invokespecial   com/navdy/hud/app/framework/notifications/NotificationManager.getLowestNotification:()Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
        //   592: astore_1       
        //   593: getstatic       com/navdy/hud/app/framework/notifications/NotificationManager.sLogger:Lcom/navdy/service/library/log/Logger;
        //   596: astore          17
        //   598: new             Ljava/lang/StringBuilder;
        //   601: astore          4
        //   603: aload           4
        //   605: invokespecial   java/lang/StringBuilder.<init>:()V
        //   608: aload           17
        //   610: aload           4
        //   612: ldc_w           "handleExpandedMove: got lowest:"
        //   615: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   618: aload_1        
        //   619: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //   622: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   625: invokevirtual   com/navdy/service/library/log/Logger.v:(Ljava/lang/String;)V
        //   628: iload           6
        //   630: istore          7
        //   632: goto            512
        //   635: iload           11
        //   637: iload           12
        //   639: iconst_1       
        //   640: isub           
        //   641: if_icmpne       820
        //   644: iload_2        
        //   645: ifeq            787
        //   648: iload           8
        //   650: istore          13
        //   652: aload_0        
        //   653: getfield        com/navdy/hud/app/framework/notifications/NotificationManager.showOn:Z
        //   656: ifeq            710
        //   659: iload           8
        //   661: istore          13
        //   663: aload_0        
        //   664: invokevirtual   com/navdy/hud/app/framework/notifications/NotificationManager.getExpandedViewChild:()Landroid/view/View;
        //   667: astore_1       
        //   668: iload           8
        //   670: istore          13
        //   672: aload_1        
        //   673: ldc_w           R.id.glance_can_delete
        //   676: invokevirtual   android/view/View.getTag:(I)Ljava/lang/Object;
        //   679: ifnull          725
        //   682: iload           8
        //   684: istore          13
        //   686: getstatic       com/navdy/hud/app/framework/notifications/NotificationManager.sLogger:Lcom/navdy/service/library/log/Logger;
        //   689: ldc_w           "no glance to delete:right"
        //   692: invokevirtual   com/navdy/service/library/log/Logger.v:(Ljava/lang/String;)V
        //   695: iconst_1       
        //   696: istore          7
        //   698: iload           7
        //   700: ifeq            366
        //   703: aload_0        
        //   704: invokespecial   com/navdy/hud/app/framework/notifications/NotificationManager.runQueuedOperation:()V
        //   707: goto            366
        //   710: iload           8
        //   712: istore          13
        //   714: aload_0        
        //   715: getfield        com/navdy/hud/app/framework/notifications/NotificationManager.notifView:Lcom/navdy/hud/app/view/NotificationView;
        //   718: invokevirtual   com/navdy/hud/app/view/NotificationView.getCurrentNotificationViewChild:()Landroid/view/View;
        //   721: astore_1       
        //   722: goto            668
        //   725: iload           8
        //   727: istore          13
        //   729: aload_0        
        //   730: iconst_1       
        //   731: putfield        com/navdy/hud/app/framework/notifications/NotificationManager.deleteAllGlances:Z
        //   734: iload           8
        //   736: istore          13
        //   738: aload_0        
        //   739: invokevirtual   com/navdy/hud/app/framework/notifications/NotificationManager.clearOperationQueue:()V
        //   742: iload           8
        //   744: istore          13
        //   746: invokestatic    com/navdy/hud/app/framework/glance/GlanceHelper.showGlancesDeletedToast:()V
        //   749: iload           8
        //   751: istore          13
        //   753: iload           9
        //   755: istore          7
        //   757: getstatic       com/navdy/hud/app/framework/notifications/NotificationManager.sLogger:Lcom/navdy/service/library/log/Logger;
        //   760: iconst_2       
        //   761: invokevirtual   com/navdy/service/library/log/Logger.isLoggable:(I)Z
        //   764: ifeq            698
        //   767: iload           8
        //   769: istore          13
        //   771: getstatic       com/navdy/hud/app/framework/notifications/NotificationManager.sLogger:Lcom/navdy/service/library/log/Logger;
        //   774: ldc_w           "handleExpandedMove: delete all glances"
        //   777: invokevirtual   com/navdy/service/library/log/Logger.v:(Ljava/lang/String;)V
        //   780: iload           9
        //   782: istore          7
        //   784: goto            698
        //   787: iload           8
        //   789: istore          13
        //   791: getstatic       com/navdy/hud/app/framework/notifications/NotificationManager.sLogger:Lcom/navdy/service/library/log/Logger;
        //   794: iconst_2       
        //   795: invokevirtual   com/navdy/service/library/log/Logger.isLoggable:(I)Z
        //   798: ifeq            814
        //   801: iload           8
        //   803: istore          13
        //   805: getstatic       com/navdy/hud/app/framework/notifications/NotificationManager.sLogger:Lcom/navdy/service/library/log/Logger;
        //   808: ldc_w           "handleExpandedMove: cannot go down"
        //   811: invokevirtual   com/navdy/service/library/log/Logger.v:(Ljava/lang/String;)V
        //   814: iconst_1       
        //   815: istore          7
        //   817: goto            698
        //   820: iload           11
        //   822: iload           12
        //   824: iconst_2       
        //   825: isub           
        //   826: if_icmpne       841
        //   829: iconst_1       
        //   830: istore          7
        //   832: aload           4
        //   834: astore_1       
        //   835: iload           5
        //   837: istore_2       
        //   838: goto            122
        //   841: iload           8
        //   843: istore          13
        //   845: aload_0        
        //   846: getfield        com/navdy/hud/app/framework/notifications/NotificationManager.lockObj:Ljava/lang/Object;
        //   849: astore_3       
        //   850: iload           8
        //   852: istore          13
        //   854: aload_3        
        //   855: dup            
        //   856: astore          19
        //   858: monitorenter   
        //   859: aload_0        
        //   860: getfield        com/navdy/hud/app/framework/notifications/NotificationManager.stackCurrentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
        //   863: ifnull          894
        //   866: aload_0        
        //   867: aload_0        
        //   868: getfield        com/navdy/hud/app/framework/notifications/NotificationManager.stackCurrentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
        //   871: invokespecial   com/navdy/hud/app/framework/notifications/NotificationManager.getLowerNotification:(Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;)Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
        //   874: astore_1       
        //   875: aload           19
        //   877: monitorexit    
        //   878: iload           5
        //   880: istore_2       
        //   881: goto            122
        //   884: astore_1       
        //   885: aload           19
        //   887: monitorexit    
        //   888: iload           8
        //   890: istore          13
        //   892: aload_1        
        //   893: athrow         
        //   894: aload_0        
        //   895: getfield        com/navdy/hud/app/framework/notifications/NotificationManager.lastStackCurrentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
        //   898: astore          4
        //   900: aload_0        
        //   901: aconst_null    
        //   902: putfield        com/navdy/hud/app/framework/notifications/NotificationManager.lastStackCurrentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
        //   905: aload           4
        //   907: astore_1       
        //   908: aload           4
        //   910: ifnonnull       875
        //   913: getstatic       com/navdy/hud/app/framework/notifications/NotificationManager.sLogger:Lcom/navdy/service/library/log/Logger;
        //   916: ldc_w           "handleExpandedMove: no next notification"
        //   919: invokevirtual   com/navdy/service/library/log/Logger.v:(Ljava/lang/String;)V
        //   922: aload           4
        //   924: astore_1       
        //   925: iload           11
        //   927: ifne            875
        //   930: aload_0        
        //   931: invokespecial   com/navdy/hud/app/framework/notifications/NotificationManager.getHighestNotification:()Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
        //   934: astore_1       
        //   935: getstatic       com/navdy/hud/app/framework/notifications/NotificationManager.sLogger:Lcom/navdy/service/library/log/Logger;
        //   938: astore          4
        //   940: new             Ljava/lang/StringBuilder;
        //   943: astore          17
        //   945: aload           17
        //   947: invokespecial   java/lang/StringBuilder.<init>:()V
        //   950: aload           4
        //   952: aload           17
        //   954: ldc_w           "handleExpandedMove: got highest:"
        //   957: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   960: aload_1        
        //   961: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //   964: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   967: invokevirtual   com/navdy/service/library/log/Logger.v:(Ljava/lang/String;)V
        //   970: goto            875
        //   973: iload           8
        //   975: istore          13
        //   977: aload_0        
        //   978: getfield        com/navdy/hud/app/framework/notifications/NotificationManager.stackCurrentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
        //   981: ifnull          1013
        //   984: iload           8
        //   986: istore          13
        //   988: aload_0        
        //   989: getfield        com/navdy/hud/app/framework/notifications/NotificationManager.stackCurrentNotification:Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;
        //   992: getfield        com/navdy/hud/app/framework/notifications/NotificationManager$Info.notification:Lcom/navdy/hud/app/framework/notifications/INotification;
        //   995: aload_1        
        //   996: invokeinterface com/navdy/hud/app/framework/notifications/INotification.onKey:(Lcom/navdy/hud/app/manager/InputManager$CustomKeyEvent;)Z
        //  1001: pop            
        //  1002: iconst_0       
        //  1003: ifeq            366
        //  1006: aload_0        
        //  1007: invokespecial   com/navdy/hud/app/framework/notifications/NotificationManager.runQueuedOperation:()V
        //  1010: goto            366
        //  1013: iload           8
        //  1015: istore          13
        //  1017: aload_0        
        //  1018: getfield        com/navdy/hud/app/framework/notifications/NotificationManager.showOn:Z
        //  1021: ifeq            1082
        //  1024: iload           8
        //  1026: istore          13
        //  1028: aload_0        
        //  1029: invokevirtual   com/navdy/hud/app/framework/notifications/NotificationManager.getExpandedViewChild:()Landroid/view/View;
        //  1032: astore_1       
        //  1033: iload           8
        //  1035: istore          13
        //  1037: aload_1        
        //  1038: invokestatic    com/navdy/hud/app/framework/glance/GlanceHelper.isDeleteAllView:(Landroid/view/View;)Z
        //  1041: ifeq            1124
        //  1044: iload           8
        //  1046: istore          13
        //  1048: aload_1        
        //  1049: ldc_w           R.id.glance_can_delete
        //  1052: invokevirtual   android/view/View.getTag:(I)Ljava/lang/Object;
        //  1055: ifnull          1097
        //  1058: iload           8
        //  1060: istore          13
        //  1062: getstatic       com/navdy/hud/app/framework/notifications/NotificationManager.sLogger:Lcom/navdy/service/library/log/Logger;
        //  1065: ldc_w           "no glance to delete:select"
        //  1068: invokevirtual   com/navdy/service/library/log/Logger.v:(Ljava/lang/String;)V
        //  1071: iconst_1       
        //  1072: ifeq            366
        //  1075: aload_0        
        //  1076: invokespecial   com/navdy/hud/app/framework/notifications/NotificationManager.runQueuedOperation:()V
        //  1079: goto            366
        //  1082: iload           8
        //  1084: istore          13
        //  1086: aload_0        
        //  1087: getfield        com/navdy/hud/app/framework/notifications/NotificationManager.notifView:Lcom/navdy/hud/app/view/NotificationView;
        //  1090: invokevirtual   com/navdy/hud/app/view/NotificationView.getCurrentNotificationViewChild:()Landroid/view/View;
        //  1093: astore_1       
        //  1094: goto            1033
        //  1097: iload           8
        //  1099: istore          13
        //  1101: aload_0        
        //  1102: iconst_1       
        //  1103: putfield        com/navdy/hud/app/framework/notifications/NotificationManager.deleteAllGlances:Z
        //  1106: iload           8
        //  1108: istore          13
        //  1110: invokestatic    com/navdy/hud/app/framework/glance/GlanceHelper.showGlancesDeletedToast:()V
        //  1113: iconst_0       
        //  1114: ifeq            366
        //  1117: aload_0        
        //  1118: invokespecial   com/navdy/hud/app/framework/notifications/NotificationManager.runQueuedOperation:()V
        //  1121: goto            366
        //  1124: iconst_0       
        //  1125: ifeq            366
        //  1128: aload_0        
        //  1129: invokespecial   com/navdy/hud/app/framework/notifications/NotificationManager.runQueuedOperation:()V
        //  1132: goto            366
        //  1135: aconst_null    
        //  1136: astore          4
        //  1138: goto            252
        //  1141: getstatic       com/navdy/hud/app/framework/notifications/NotificationManager.sLogger:Lcom/navdy/service/library/log/Logger;
        //  1144: iconst_2       
        //  1145: invokevirtual   com/navdy/service/library/log/Logger.isLoggable:(I)Z
        //  1148: ifeq            1160
        //  1151: getstatic       com/navdy/hud/app/framework/notifications/NotificationManager.sLogger:Lcom/navdy/service/library/log/Logger;
        //  1154: ldc_w           "handleExpandedMove: going to delete all"
        //  1157: invokevirtual   com/navdy/service/library/log/Logger.v:(Ljava/lang/String;)V
        //  1160: aload_0        
        //  1161: getfield        com/navdy/hud/app/framework/notifications/NotificationManager.showOn:Z
        //  1164: ifeq            1265
        //  1167: getstatic       com/navdy/hud/app/framework/glance/GlanceViewCache$ViewType.SMALL_IMAGE:Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;
        //  1170: aload           14
        //  1172: invokestatic    com/navdy/hud/app/framework/glance/GlanceViewCache.getView:(Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;Landroid/content/Context;)Landroid/view/View;
        //  1175: astore          4
        //  1177: aload           4
        //  1179: ldc_w           R.id.glance_view_type
        //  1182: getstatic       com/navdy/hud/app/framework/glance/GlanceViewCache$ViewType.SMALL_IMAGE:Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;
        //  1185: invokevirtual   android/view/View.setTag:(ILjava/lang/Object;)V
        //  1188: aload           4
        //  1190: ldc_w           R.id.imageView
        //  1193: invokevirtual   android/view/View.findViewById:(I)Landroid/view/View;
        //  1196: checkcast       Landroid/widget/ImageView;
        //  1199: astore_3       
        //  1200: aload           4
        //  1202: getstatic       com/navdy/hud/app/framework/glance/GlanceConstants.DELETE_ALL_VIEW_TAG:Ljava/lang/Object;
        //  1205: invokevirtual   android/view/View.setTag:(Ljava/lang/Object;)V
        //  1208: aload_3        
        //  1209: ldc_w           R.drawable.icon_dismiss_all_glances
        //  1212: invokevirtual   android/widget/ImageView.setImageResource:(I)V
        //  1215: aload_0        
        //  1216: getfield        com/navdy/hud/app/framework/notifications/NotificationManager.showOn:Z
        //  1219: ifeq            1472
        //  1222: getstatic       com/navdy/hud/app/framework/glance/GlanceViewCache$ViewType.BIG_TEXT:Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;
        //  1225: aload           14
        //  1227: invokestatic    com/navdy/hud/app/framework/glance/GlanceViewCache.getView:(Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;Landroid/content/Context;)Landroid/view/View;
        //  1230: astore_3       
        //  1231: aload_3        
        //  1232: getstatic       com/navdy/hud/app/framework/glance/GlanceConstants.DELETE_ALL_VIEW_TAG:Ljava/lang/Object;
        //  1235: invokevirtual   android/view/View.setTag:(Ljava/lang/Object;)V
        //  1238: aload_0        
        //  1239: aload_3        
        //  1240: ldc_w           R.id.textView
        //  1243: invokevirtual   android/view/View.findViewById:(I)Landroid/view/View;
        //  1246: checkcast       Landroid/widget/TextView;
        //  1249: aload_3        
        //  1250: invokespecial   com/navdy/hud/app/framework/notifications/NotificationManager.updateDeleteAllGlanceCount:(Landroid/widget/TextView;Landroid/view/View;)V
        //  1253: getstatic       com/navdy/hud/app/framework/glance/GlanceConstants.colorDeleteAll:I
        //  1256: istore          7
        //  1258: aload           4
        //  1260: astore          17
        //  1262: goto            284
        //  1265: getstatic       com/navdy/hud/app/framework/glance/GlanceViewCache$ViewType.SMALL_GLANCE_MESSAGE:Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;
        //  1268: aload           14
        //  1270: invokestatic    com/navdy/hud/app/framework/glance/GlanceViewCache.getView:(Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;Landroid/content/Context;)Landroid/view/View;
        //  1273: astore          18
        //  1275: aload           18
        //  1277: ldc_w           R.id.glance_view_type
        //  1280: getstatic       com/navdy/hud/app/framework/glance/GlanceViewCache$ViewType.SMALL_GLANCE_MESSAGE:Lcom/navdy/hud/app/framework/glance/GlanceViewCache$ViewType;
        //  1283: invokevirtual   android/view/View.setTag:(ILjava/lang/Object;)V
        //  1286: aload           18
        //  1288: ldc_w           R.id.mainImage
        //  1291: invokevirtual   android/view/View.findViewById:(I)Landroid/view/View;
        //  1294: checkcast       Landroid/widget/ImageView;
        //  1297: astore          17
        //  1299: aload           18
        //  1301: ldc_w           R.id.mainTitle
        //  1304: invokevirtual   android/view/View.findViewById:(I)Landroid/view/View;
        //  1307: checkcast       Landroid/widget/TextView;
        //  1310: astore          4
        //  1312: aload           4
        //  1314: getstatic       com/navdy/hud/app/framework/glance/GlanceConstants.glancesDismissAll:Ljava/lang/String;
        //  1317: invokevirtual   android/widget/TextView.setText:(Ljava/lang/CharSequence;)V
        //  1320: aload           4
        //  1322: fconst_1       
        //  1323: invokevirtual   android/widget/TextView.setAlpha:(F)V
        //  1326: aload           4
        //  1328: iconst_0       
        //  1329: invokevirtual   android/widget/TextView.setVisibility:(I)V
        //  1332: aload           18
        //  1334: ldc_w           R.id.subTitle
        //  1337: invokevirtual   android/view/View.findViewById:(I)Landroid/view/View;
        //  1340: checkcast       Landroid/widget/TextView;
        //  1343: astore          4
        //  1345: aload_0        
        //  1346: invokevirtual   com/navdy/hud/app/framework/notifications/NotificationManager.getNotificationCount:()I
        //  1349: aload_0        
        //  1350: invokespecial   com/navdy/hud/app/framework/notifications/NotificationManager.getNonRemovableNotifCount:()I
        //  1353: isub           
        //  1354: istore          7
        //  1356: iload           7
        //  1358: ifne            1460
        //  1361: aload           18
        //  1363: ldc_w           R.id.glance_can_delete
        //  1366: iconst_1       
        //  1367: invokestatic    java/lang/Integer.valueOf:(I)Ljava/lang/Integer;
        //  1370: invokevirtual   android/view/View.setTag:(ILjava/lang/Object;)V
        //  1373: aload           4
        //  1375: aload_0        
        //  1376: getfield        com/navdy/hud/app/framework/notifications/NotificationManager.resources:Landroid/content/res/Resources;
        //  1379: ldc_w           R.string.count
        //  1382: iconst_1       
        //  1383: anewarray       Ljava/lang/Object;
        //  1386: dup            
        //  1387: iconst_0       
        //  1388: iload           7
        //  1390: invokestatic    java/lang/Integer.valueOf:(I)Ljava/lang/Integer;
        //  1393: aastore        
        //  1394: invokevirtual   android/content/res/Resources.getString:(I[Ljava/lang/Object;)Ljava/lang/String;
        //  1397: invokevirtual   android/widget/TextView.setText:(Ljava/lang/CharSequence;)V
        //  1400: aload           4
        //  1402: fconst_1       
        //  1403: invokevirtual   android/widget/TextView.setAlpha:(F)V
        //  1406: aload           4
        //  1408: iconst_0       
        //  1409: invokevirtual   android/widget/TextView.setVisibility:(I)V
        //  1412: aload           18
        //  1414: ldc_w           R.id.choiceLayout
        //  1417: invokevirtual   android/view/View.findViewById:(I)Landroid/view/View;
        //  1420: astore          20
        //  1422: aload           17
        //  1424: astore_3       
        //  1425: aload           18
        //  1427: astore          4
        //  1429: aload           20
        //  1431: ifnull          1200
        //  1434: aload           20
        //  1436: iconst_4       
        //  1437: invokevirtual   android/view/View.setVisibility:(I)V
        //  1440: aload           17
        //  1442: astore_3       
        //  1443: aload           18
        //  1445: astore          4
        //  1447: goto            1200
        //  1450: astore_1       
        //  1451: aload           16
        //  1453: monitorexit    
        //  1454: iload           8
        //  1456: istore          13
        //  1458: aload_1        
        //  1459: athrow         
        //  1460: aload           18
        //  1462: ldc_w           R.id.glance_can_delete
        //  1465: aconst_null    
        //  1466: invokevirtual   android/view/View.setTag:(ILjava/lang/Object;)V
        //  1469: goto            1373
        //  1472: aconst_null    
        //  1473: astore_3       
        //  1474: goto            1253
        //  1477: aload_0        
        //  1478: aload           17
        //  1480: aload_3        
        //  1481: aload_1        
        //  1482: iload_2        
        //  1483: iload           7
        //  1485: invokevirtual   com/navdy/hud/app/framework/notifications/NotificationManager.viewSwitchAnimation:(Landroid/view/View;Landroid/view/View;Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;ZI)V
        //  1488: goto            350
        //  1491: aload_0        
        //  1492: aload           17
        //  1494: aload_3        
        //  1495: aload_1        
        //  1496: iload_2        
        //  1497: iload           7
        //  1499: invokevirtual   com/navdy/hud/app/framework/notifications/NotificationManager.viewSwitchAnimation:(Landroid/view/View;Landroid/view/View;Lcom/navdy/hud/app/framework/notifications/NotificationManager$Info;ZI)V
        //  1502: goto            350
        //  1505: iload           8
        //  1507: istore          13
        //  1509: getstatic       com/navdy/hud/app/framework/notifications/NotificationManager.sLogger:Lcom/navdy/service/library/log/Logger;
        //  1512: iconst_2       
        //  1513: invokevirtual   com/navdy/service/library/log/Logger.isLoggable:(I)Z
        //  1516: ifeq            1532
        //  1519: iload           8
        //  1521: istore          13
        //  1523: getstatic       com/navdy/hud/app/framework/notifications/NotificationManager.sLogger:Lcom/navdy/service/library/log/Logger;
        //  1526: ldc_w           "handleExpandedMove: no-op"
        //  1529: invokevirtual   com/navdy/service/library/log/Logger.v:(Ljava/lang/String;)V
        //  1532: iconst_1       
        //  1533: istore          7
        //  1535: goto            357
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type
        //  -----  -----  -----  -----  ----
        //  81     116    528    540    Any
        //  126    135    528    540    Any
        //  148    154    528    540    Any
        //  158    164    528    540    Any
        //  168    218    1450   1460   Any
        //  218    252    1450   1460   Any
        //  252    281    1450   1460   Any
        //  284    306    1450   1460   Any
        //  311    350    1450   1460   Any
        //  350    353    1450   1460   Any
        //  379    389    528    540    Any
        //  393    402    528    540    Any
        //  433    438    528    540    Any
        //  442    447    528    540    Any
        //  447    464    518    528    Any
        //  486    505    518    528    Any
        //  512    515    518    528    Any
        //  519    522    518    528    Any
        //  526    528    528    540    Any
        //  540    551    518    528    Any
        //  563    572    518    528    Any
        //  588    628    518    528    Any
        //  652    659    528    540    Any
        //  663    668    528    540    Any
        //  672    682    528    540    Any
        //  686    695    528    540    Any
        //  714    722    528    540    Any
        //  729    734    528    540    Any
        //  738    742    528    540    Any
        //  746    749    528    540    Any
        //  757    767    528    540    Any
        //  771    780    528    540    Any
        //  791    801    528    540    Any
        //  805    814    528    540    Any
        //  845    850    528    540    Any
        //  854    859    528    540    Any
        //  859    875    884    894    Any
        //  875    878    884    894    Any
        //  885    888    884    894    Any
        //  892    894    528    540    Any
        //  894    905    884    894    Any
        //  913    922    884    894    Any
        //  930    970    884    894    Any
        //  977    984    528    540    Any
        //  988    1002   528    540    Any
        //  1017   1024   528    540    Any
        //  1028   1033   528    540    Any
        //  1037   1044   528    540    Any
        //  1048   1058   528    540    Any
        //  1062   1071   528    540    Any
        //  1086   1094   528    540    Any
        //  1101   1106   528    540    Any
        //  1110   1113   528    540    Any
        //  1141   1160   1450   1460   Any
        //  1160   1200   1450   1460   Any
        //  1200   1253   1450   1460   Any
        //  1253   1258   1450   1460   Any
        //  1265   1356   1450   1460   Any
        //  1361   1373   1450   1460   Any
        //  1373   1422   1450   1460   Any
        //  1434   1440   1450   1460   Any
        //  1451   1454   1450   1460   Any
        //  1458   1460   528    540    Any
        //  1460   1469   1450   1460   Any
        //  1477   1488   1450   1460   Any
        //  1491   1502   1450   1460   Any
        //  1509   1519   528    540    Any
        //  1523   1532   528    540    Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0218:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    private void handleRemoveNotificationInExpandedMode(final String s, final boolean b) {
        if (this.isExpanded() && !this.deleteAllGlances) {
            this.runOperation(NotificationAnimator.Operation.REMOVE_NOTIFICATION, false, false, false, false, s, b);
        }
    }
    
    private void handleRemoveNotificationInExpandedModeInternal(String id, final boolean b) {
        if (this.isNotifCurrent(id)) {
            NotificationManager.sLogger.v("notif-end: is current:" + id);
            if (b) {
                this.collapseNotificationInternal(false, false, true);
            }
        }
        else if (this.getNotificationCount() == 1) {
            NotificationManager.sLogger.v("remove-end: no more glance");
            this.collapseExpandedViewInternal(true, true);
        }
        else {
            NotificationManager.sLogger.v("remove-end: not current, remove:" + id);
            this.removeNotificationfromStack(this.getNotificationFromId(id), true);
            if (this.lastStackCurrentNotification != null && TextUtils.equals((CharSequence)this.lastStackCurrentNotification.notification.getId(), (CharSequence)id)) {
                if (this.stackCurrentNotification == null) {
                    this.lastStackCurrentNotification = this.getLowerNotification(this.lastStackCurrentNotification);
                    final Logger sLogger = NotificationManager.sLogger;
                    final StringBuilder append = new StringBuilder().append("after remove notif is:");
                    String id2;
                    if (this.lastStackCurrentNotification == null) {
                        id2 = "null";
                    }
                    else {
                        id2 = this.lastStackCurrentNotification.notification.getId();
                    }
                    sLogger.v(append.append(id2).toString());
                }
                else {
                    this.lastStackCurrentNotification = null;
                }
            }
            if (this.currentNotification != null && TextUtils.equals((CharSequence)this.currentNotification.notification.getId(), (CharSequence)id)) {
                this.currentNotification = this.stackCurrentNotification;
                final Logger sLogger2 = NotificationManager.sLogger;
                final StringBuilder append2 = new StringBuilder().append("after remove current notif set:");
                if (this.currentNotification == null) {
                    id = "null";
                }
                else {
                    id = this.currentNotification.notification.getId();
                }
                sLogger2.v(append2.append(id).toString());
            }
            this.postNotificationChange();
            this.updateExpandedIndicatorInternal();
        }
    }
    
    private void hideNotificationInternal() {
        final NotificationView notificationView = this.getNotificationView();
        if (notificationView.border.isVisible()) {
            NotificationManager.sLogger.v("hideNotification:border visible");
            notificationView.border.stopTimeout(true, new Runnable() {
                @Override
                public void run() {
                    NotificationManager.sLogger.v("hideNotification-anim:screen_back");
                    NotificationManager.this.bus.post(new ShowScreenWithArgs(Screen.SCREEN_BACK, null, null, true));
                }
            });
        }
        else {
            NotificationManager.sLogger.v("hideNotification:screen_back");
            this.bus.post(new ShowScreenWithArgs(Screen.SCREEN_BACK, null, null, true));
        }
    }
    
    private void hideScrollingIndicator() {
        if (this.notifScrollIndicator != null) {
            ((View)this.notifScrollIndicator.getParent()).setVisibility(GONE);
        }
    }
    
    private boolean isDeleteAllIndex(final int n, final int n2) {
        return n == 0 || n == n2 - 1;
    }
    
    private boolean isNotifCurrent(final String s) {
        boolean b = true;
        if (this.notificationController.isExpandedWithStack() && this.expandedWithStack) {
            if (this.stackCurrentNotification == null || !TextUtils.equals((CharSequence)s, (CharSequence)this.stackCurrentNotification.notification.getId()) || !this.stackCurrentNotification.startCalled) {
                b = false;
            }
        }
        else if (this.currentNotification == null || !TextUtils.equals((CharSequence)s, (CharSequence)this.currentNotification.notification.getId()) || !this.currentNotification.startCalled) {
            b = false;
        }
        return b;
    }
    
    private boolean isPhoneNotifPresentAndNotAlive() {
        final CallNotification callNotification = (CallNotification)this.getNotification("navdy#phone#call#notif");
        return callNotification != null && !callNotification.isAlive();
    }
    
    private boolean isScreenHighPriority(final Screen screen) {
        boolean b = false;
        switch (screen) {
            default:
                b = false;
                break;
            case SCREEN_DESTINATION_PICKER:
                b = true;
                break;
        }
        return b;
    }
    
    private void moveNextInternal(final boolean b) {
        final boolean loggable = NotificationManager.sLogger.isLoggable(2);
        if (loggable) {
            NotificationManager.sLogger.v("moveNextInternal:" + b);
        }
        if (this.isExpanded()) {
            this.handleExpandedMove(InputManager.CustomKeyEvent.RIGHT, b);
        }
        else if (this.isExpandedNotificationVisible()) {
            int currentItem = this.notifIndicator.getCurrentItem();
            if (currentItem == this.notifIndicator.getItemCount() - 1) {
                this.runQueuedOperation();
                if (loggable) {
                    NotificationManager.sLogger.v("moveNextInternal:cannot go next");
                }
            }
            else {
                ++currentItem;
                if (loggable) {
                    NotificationManager.sLogger.v("moveNextInternal:going to " + currentItem);
                }
                this.viewSwitchAnimation(null, this.currentNotification.notification.getExpandedView(this.notifView.getContext(), currentItem), null, false, this.currentNotification.notification.getExpandedViewIndicatorColor());
            }
        }
    }
    
    private void movePrevInternal(final boolean b) {
        final boolean loggable = NotificationManager.sLogger.isLoggable(2);
        if (loggable) {
            NotificationManager.sLogger.v("movePrevInternal:" + b);
        }
        if (this.isExpanded()) {
            this.handleExpandedMove(InputManager.CustomKeyEvent.LEFT, b);
        }
        else if (this.isExpandedNotificationVisible()) {
            int currentItem = this.notifIndicator.getCurrentItem();
            this.notifIndicator.getItemCount();
            if (currentItem == 0) {
                this.runQueuedOperation();
                if (loggable) {
                    NotificationManager.sLogger.v("movePrevInternal: cannot go prev");
                }
            }
            else {
                --currentItem;
                if (loggable) {
                    NotificationManager.sLogger.v("movePrevInternal: going to " + currentItem);
                }
                this.viewSwitchAnimation(null, this.currentNotification.notification.getExpandedView(this.notifView.getContext(), currentItem), null, true, GlanceConstants.colorWhite);
            }
        }
    }
    
    private void removeAllNotification() {
        Object sLogger2 = null;
    Label_0330:
        while (true) {
            this.expandedWithStack = false;
            final Object lockObj = this.lockObj;
            while (true) {
                String s;
                synchronized (lockObj) {
                    if (this.currentNotification != null && this.currentNotification.startCalled && NotificationHelper.isNotificationRemovable(this.currentNotification.notification.getId())) {
                        final Logger sLogger = NotificationManager.sLogger;
                        sLogger2 = new StringBuilder();
                        sLogger.v(((StringBuilder)sLogger2).append("removeAllNotification calling stop: ").append(this.currentNotification.notification.getId()).toString());
                        this.currentNotification.startCalled = false;
                        this.currentNotification.notification.onStop();
                    }
                    final int size = this.notificationIdMap.size();
                    final Logger sLogger3 = NotificationManager.sLogger;
                    sLogger2 = new StringBuilder();
                    sLogger3.v(((StringBuilder)sLogger2).append("removeAllNotification:").append(size).toString());
                    sLogger2 = null;
                    final Object o = null;
                    if (size <= 0) {
                        break;
                    }
                    final Iterator<String> iterator = this.notificationIdMap.keySet().iterator();
                    while (true) {
                        sLogger2 = o;
                        if (!iterator.hasNext()) {
                            break Label_0330;
                        }
                        s = iterator.next();
                        sLogger2 = this.notificationIdMap.get(s);
                        if (!NotificationHelper.isNotificationRemovable(s)) {
                            break;
                        }
                        this.notificationPriorityMap.remove(sLogger2);
                        iterator.remove();
                        sLogger2 = NotificationManager.sLogger;
                        ((Logger)sLogger2).v("removeAllNotification removed:" + s);
                    }
                }
                final Object o = sLogger2;
                final Logger sLogger4 = NotificationManager.sLogger;
                sLogger2 = new StringBuilder();
                sLogger4.v(((StringBuilder)sLogger2).append("removeAllNotification left:").append(s).toString());
                continue;
            }
        }
        this.stackCurrentNotification = null;
        this.lastStackCurrentNotification = null;
        this.currentNotification = (Info)sLogger2;
        this.postNotificationChange();
    }
    // monitorexit(o2)
    
    private void removeNotificationfromStack(final Info info, final boolean b) {
        if (info != null) {
            if (this.notificationIdMap.remove(info.notification.getId()) == null) {
                NotificationManager.sLogger.v("removeNotification, not found in id map");
            }
            if (this.notificationPriorityMap.remove(info) == null) {
                NotificationManager.sLogger.v("removeNotification, not found in priority map");
            }
            if (info.startCalled) {
                info.startCalled = false;
                info.notification.onStop();
            }
            String id = null;
            if (this.stagedNotification != null) {
                id = this.stagedNotification.notification.getId();
                if (TextUtils.equals((CharSequence)this.stagedNotification.notification.getId(), (CharSequence)info.notification.getId())) {
                    this.stagedNotification = null;
                    NotificationManager.sLogger.v("cancelling staged notification, getting removed");
                    id = id;
                }
            }
            this.postNotificationChange();
            NotificationManager.sLogger.v("removed notification from stack [" + info.notification.getId() + "] staged[" + id + "]");
            if (!b) {
                this.updateExpandedIndicator();
            }
        }
    }
    
    private void restoreStack() {
        while (true) {
            final Object lockObj = this.lockObj;
            Label_0292: {
                Label_0180: {
                    synchronized (lockObj) {
                        final int size = this.notificationSavedList.size();
                        NotificationManager.sLogger.v("restoreStack:" + size);
                        if (size > 0) {
                            final NavdyDeviceId deviceId = RemoteDeviceManager.getInstance().getDeviceId();
                            if (deviceId == null) {
                                NotificationManager.sLogger.w("restoreStack no device id");
                                this.notificationSavedList.clear();
                            }
                            else {
                                if (deviceId.equals(this.notificationSavedListDeviceId)) {
                                    break Label_0180;
                                }
                                NotificationManager.sLogger.w("restoreStack device id does not match current[" + deviceId + "] saved[" + this.notificationSavedListDeviceId + "]");
                                this.notificationSavedList.clear();
                                GlanceHandler.getInstance().clearState();
                            }
                            // monitorexit(lockObj)
                            return;
                        }
                        break Label_0292;
                    }
                }
                for (final Info info : this.notificationSavedList) {
                    final String id = info.notification.getId();
                    NotificationManager.sLogger.w("restoreStack restored:" + id);
                    this.notificationIdMap.put(id, info);
                    this.notificationPriorityMap.put(info, info);
                }
                GlanceHandler.getInstance().restoreState();
            }
            // monitorexit(o)
            this.uiStateManager.enableNotificationColor(true);
        }
    }
    
    private void runOperation(final NotificationAnimator.Operation operation, final boolean b, final boolean b2, final boolean b3, final boolean b4, final String s, final boolean b5) {
        if (!b && this.operationRunning) {
            if (this.operationQueue.size() >= 3) {
                switch (operation) {
                    case MOVE_NEXT:
                    case MOVE_PREV:
                        return;
                }
            }
            this.operationQueue.add(new NotificationAnimator.OperationInfo(operation, b2, b3, b4, s, b5));
        }
        else {
            NotificationManager.sLogger.v("run operation=" + operation);
            this.operationRunning = true;
            switch (operation) {
                case MOVE_NEXT: {
                    this.moveNextInternal(b4);
                    if (this.deleteAllGlances) {
                        String s2;
                        if (b4) {
                            s2 = "swipe";
                        }
                        else {
                            s2 = "dial";
                        }
                        AnalyticsSupport.recordGlanceAction("Glance_Delete_All", null, s2);
                        break;
                    }
                    String glanceNavigationSource;
                    if (b4) {
                        glanceNavigationSource = "swipe";
                    }
                    else {
                        glanceNavigationSource = "dial";
                    }
                    AnalyticsSupport.setGlanceNavigationSource(glanceNavigationSource);
                    break;
                }
                case MOVE_PREV: {
                    this.movePrevInternal(b4);
                    String glanceNavigationSource2;
                    if (b4) {
                        glanceNavigationSource2 = "swipe";
                    }
                    else {
                        glanceNavigationSource2 = "dial";
                    }
                    AnalyticsSupport.setGlanceNavigationSource(glanceNavigationSource2);
                    break;
                }
                case SELECT:
                    this.executeItemInternal();
                    if (this.deleteAllGlances) {
                        String s3;
                        if (b4) {
                            s3 = "swipe";
                        }
                        else {
                            s3 = "dial";
                        }
                        AnalyticsSupport.recordGlanceAction("Glance_Delete_All", null, s3);
                        break;
                    }
                    break;
                case COLLAPSE_VIEW:
                    this.collapseExpandedViewInternal(b2, b3);
                    break;
                case UPDATE_INDICATOR:
                    this.updateExpandedIndicatorInternal();
                    break;
                case REMOVE_NOTIFICATION:
                    this.handleRemoveNotificationInExpandedModeInternal(s, b5);
                    break;
            }
        }
    }
    
    private void runQueuedOperation() {
        if (this.operationQueue.size() > 0) {
            this.handler.post((Runnable)new Runnable() {
                final /* synthetic */ NotificationAnimator.OperationInfo val$operationInfo = NotificationManager.this.operationQueue.remove();
                
                @Override
                public void run() {
                    NotificationManager.this.runOperation(this.val$operationInfo.operation, true, this.val$operationInfo.hideNotification, this.val$operationInfo.quick, this.val$operationInfo.gesture, this.val$operationInfo.id, this.val$operationInfo.collapse);
                }
            });
        }
        else {
            NotificationManager.sLogger.v("operationRunning=false");
            this.operationRunning = false;
        }
    }
    
    private void saveStack() {
        while (true) {
            final Object lockObj = this.lockObj;
            while (true) {
                Serializable s = null;
                final Iterator<String> iterator;
                Object sLogger2 = null;
                Label_0210: {
                    synchronized (lockObj) {
                        final ToastManager instance = ToastManager.getInstance();
                        instance.dismissCurrentToast("incomingcall#toast");
                        instance.clearPendingToast("incomingcall#toast");
                        RemoteDeviceManager.getInstance().getCallManager().clearCallStack();
                        this.notificationSavedList.clear();
                        final int size = this.notificationIdMap.size();
                        final Logger sLogger = NotificationManager.sLogger;
                        s = new StringBuilder();
                        sLogger.v(((StringBuilder)s).append("saveStack:").append(size).toString());
                        if (size > 0) {
                            iterator = this.notificationIdMap.keySet().iterator();
                            while (iterator.hasNext()) {
                                s = iterator.next();
                                sLogger2 = this.notificationIdMap.get(s);
                                if (!this.NOTIFS_REMOVED_ON_DISCONNECT.contains(s)) {
                                    break Label_0210;
                                }
                                this.notificationPriorityMap.remove(sLogger2);
                                iterator.remove();
                                final Logger sLogger3 = NotificationManager.sLogger;
                                sLogger2 = new StringBuilder();
                                sLogger3.v(((StringBuilder)sLogger2).append("saveStack removed:").append((String)s).toString());
                            }
                            break;
                        }
                        break;
                    }
                }
                if (NotificationType.GLANCE == ((Info)sLogger2).notification.getType()) {
                    NotificationManager.sLogger.v("saveStack saved:" + (String)s);
                    this.notificationSavedList.add((Info)sLogger2);
                    iterator.remove();
                    this.notificationPriorityMap.remove(sLogger2);
                    continue;
                }
                sLogger2 = NotificationManager.sLogger;
                ((Logger)sLogger2).v("saveStack left:" + (String)s);
                continue;
            }
        }
        this.currentNotification = null;
        this.stagedNotification = null;
        GlanceHandler.getInstance().saveState();
        // monitorexit(o)
        this.uiStateManager.enableNotificationColor(true);
    }
    
    private void setNotifScrollIndicatorXY(final RectF rectF, final IScrollEvent scrollEvent) {
        scrollEvent.setListener(this.scrollProgress);
        this.notifScrollIndicator.setCurrentItem(1);
        final View view = (View)this.notifScrollIndicator.getParent();
        view.setX(this.notifIndicator.getX() + GlanceConstants.scrollingIndicatorLeftPadding);
        view.setY(this.notifIndicator.getY() + rectF.top + GlanceConstants.scrollingIndicatorCircleSize / 2 - GlanceConstants.scrollingIndicatorHeight / 2);
        view.setVisibility(View.VISIBLE);
    }
    
    private void showNotification(final boolean b) {
        NotificationManager.sLogger.v("show notification isExpanded:" + this.isExpanded + " anim:" + this.isAnimating + " ignore:" + b);
        if (!this.isExpanded && !this.isAnimating) {
            this.bus.post(new ShowScreenWithArgs(Screen.SCREEN_NOTIFICATION, null, null, b));
        }
    }
    
    private void startScrollThresholdTimer() {
        this.scrollState = ScrollState.NOT_HANDLED;
        this.handler.removeCallbacks(this.ignoreScrollRunnable);
        this.handler.postDelayed(this.ignoreScrollRunnable, 750L);
    }
    
    private void updateDeleteAllGlanceCount(final TextView textView, final View view) {
        final int n = this.getNotificationCount() - this.getNonRemovableNotifCount();
        if (n == 0) {
            textView.setText((CharSequence)GlanceConstants.glancesCannotDelete);
            view.setTag(R.id.glance_can_delete, 1);
        }
        else {
            if (this.showOn) {
                textView.setText((CharSequence)this.resources.getString(R.string.glances_dismiss_all, new Object[] { n }));
            }
            else {
                textView.setText((CharSequence)this.resources.getString(R.string.count, new Object[] { n }));
            }
            view.setTag(R.id.glance_can_delete, null);
        }
    }
    
    private void updateExpandedIndicator() {
        if (this.isExpanded() && !this.deleteAllGlances) {
            if (this.isAnimating()) {
                NotificationManager.sLogger.v("updateExpandedIndicator animating no-op");
            }
            else {
                this.runOperation(NotificationAnimator.Operation.UPDATE_INDICATOR, false, false, false, false, null, false);
            }
        }
    }
    
    private void updateExpandedIndicatorInternal() {
        final FrameLayout expandedNotificationView = this.getExpandedNotificationView();
        if (this.isExpanded() && ((View)expandedNotificationView).getTag() != null) {
            final int itemCount = this.notifIndicator.getItemCount();
            final int currentItem = this.notifIndicator.getCurrentItem();
            final boolean deleteAllIndex = this.isDeleteAllIndex(currentItem, itemCount);
            int notificationCount;
            final int n = notificationCount = this.getNotificationCount();
            if (this.isPhoneNotifPresentAndNotAlive()) {
                notificationCount = n - 1;
            }
            final int n2 = notificationCount + 2;
            final int colorDeleteAll = GlanceConstants.colorDeleteAll;
            int n3;
            if ((n3 = currentItem) >= n2) {
                n3 = n2 - 1;
            }
            int n4 = colorDeleteAll;
            if (this.stackCurrentNotification != null) {
                final int notificationIndex = this.getNotificationIndex(this.stackCurrentNotification);
                n4 = colorDeleteAll;
                if ((n3 = notificationIndex) != -1) {
                    n4 = this.stackCurrentNotification.notification.getColor();
                    n3 = notificationIndex + 1;
                }
            }
            if (this.isDeleteAllIndex(n3, n2)) {
                n4 = GlanceConstants.colorDeleteAll;
            }
            else if (deleteAllIndex) {
                final Info notificationByIndex = this.getNotificationByIndex(notificationCount - 1);
                if (notificationByIndex != null) {
                    this.lastStackCurrentNotification = notificationByIndex;
                    this.updateExpandedIndicator(n2, n2 - 1, notificationByIndex.notification.getColor());
                    this.movePrevInternal(false);
                    return;
                }
                n4 = GlanceConstants.colorDeleteAll;
                n3 = n2 - 1;
            }
            this.updateExpandedIndicator(n2, n3, n4);
            View view;
            if (this.showOn) {
                view = this.getExpandedViewChild();
            }
            else {
                if (this.notifView == null) {
                    this.getNotificationView();
                }
                view = this.notifView.getCurrentNotificationViewChild();
            }
            if (GlanceHelper.isDeleteAllView(view)) {
                TextView textView;
                if (this.showOn) {
                    textView = (TextView)view.findViewById(R.id.textView);
                }
                else {
                    textView = (TextView)view.findViewById(R.id.subTitle);
                }
                this.updateDeleteAllGlanceCount(textView, view);
            }
            this.runQueuedOperation();
        }
    }
    
    public void addNotification(final INotification notification) {
        this.addNotification(notification, null);
    }
    
    public void addNotification(final INotification notification, final EnumSet<Screen> set) {
        if (notification != null) {
            if (this.uiStateManager.getRootScreen() == null) {
                NotificationManager.sLogger.v("notification not accepted, Main screen not on [" + notification.getId() + "]");
            }
            else if (this.disconnected && this.NOTIFS_NOT_ALLOWED_ON_DISCONNECT.contains(notification.getId())) {
                NotificationManager.sLogger.i("notification not allowed during disconnect[" + notification.getId() + "]");
            }
            else if (Looper.getMainLooper() != Looper.myLooper()) {
                this.handler.post((Runnable)new Runnable() {
                    @Override
                    public void run() {
                        NotificationManager.this.addNotificationInternal(notification, set);
                    }
                });
            }
            else {
                this.addNotificationInternal(notification, set);
            }
        }
    }
    
    public void addPendingNotification(final INotification pendingNotification) {
        this.pendingNotification = pendingNotification;
        NotificationManager.sLogger.v("addPendingNotification:" + this.pendingNotification);
    }
    
    public void clearOperationQueue() {
        NotificationManager.sLogger.v("clearOperationQueue:" + this.operationQueue.size());
        this.operationQueue.clear();
        this.operationRunning = false;
    }
    
    public void collapseExpandedNotification(final boolean b, final boolean b2) {
        this.notificationController.collapseNotification(b, b2);
    }
    
    public boolean collapseNotification() {
        final boolean b = false;
        Label_0025: {
            if (this.mainScreen != null) {
                break Label_0025;
            }
            this.getNotificationView();
            if (this.mainScreen != null) {
                break Label_0025;
            }
            return b;
        }
        boolean b2 = b;
        if (!this.mainScreen.isNotificationViewShowing()) {
            return b2;
        }
        b2 = b;
        if (this.mainScreen.isNotificationCollapsing()) {
            return b2;
        }
        b2 = b;
        if (!this.mainScreen.isNotificationExpanding()) {
            this.hideNotification();
            b2 = true;
            return b2;
        }
        return b2;
    }
    
    public void currentNotificationTimeout() {
        if (this.currentNotification != null) {
            NotificationManager.sLogger.v("currentNotificationTimeout");
            AnalyticsSupport.recordGlanceAction("Glance_Dismiss", this.currentNotification.notification, "timeout");
            this.removeNotification(this.currentNotification.notification.getId(), false);
        }
    }
    
    public void enableNotifications(final boolean enable) {
        NotificationManager.sLogger.v("notification enabled[" + enable + "]");
        this.enablePhoneNotifications(this.enable = enable);
    }
    
    public void enablePhoneNotifications(final boolean enablePhoneCalls) {
        NotificationManager.sLogger.v("notification enabled phone[" + enablePhoneCalls + "]");
        this.enablePhoneCalls = enablePhoneCalls;
    }
    
    public void executeItem() {
        if (this.isExpanded() || this.isExpandedNotificationVisible()) {
            if (!this.deleteAllGlances) {
                this.runOperation(NotificationAnimator.Operation.SELECT, false, false, false, false, null, false);
            }
            else {
                NotificationManager.sLogger.v("executeItem: no-op deleteGlances set");
            }
        }
    }
    
    public boolean expandNotification() {
        final boolean b = false;
        Label_0025: {
            if (this.mainScreen != null) {
                break Label_0025;
            }
            this.getNotificationView();
            if (this.mainScreen != null) {
                break Label_0025;
            }
            return b;
        }
        boolean b2 = b;
        if (this.mainScreen.isNotificationViewShowing()) {
            return b2;
        }
        b2 = b;
        if (this.mainScreen.isNotificationExpanding()) {
            return b2;
        }
        b2 = b;
        if (this.makeNotificationCurrent(true)) {
            this.showNotification();
            b2 = true;
            return b2;
        }
        return b2;
    }
    
    public INotification getCurrentNotification() {
        INotification notification;
        if (this.currentNotification == null) {
            notification = null;
        }
        else {
            notification = this.currentNotification.notification;
        }
        return notification;
    }
    
    public int getExpandedIndicatorCount() {
        return this.notifIndicator.getItemCount();
    }
    
    public int getExpandedIndicatorCurrentItem() {
        return this.notifIndicator.getCurrentItem();
    }
    
    public View getExpandedViewChild() {
        View child = null;
        final FrameLayout expandedNotificationView = this.getExpandedNotificationView();
        if (expandedNotificationView != null && expandedNotificationView.getChildCount() != 0) {
            child = expandedNotificationView.getChildAt(0);
        }
        return child;
    }
    
    public INotification getNotification(final String s) {
        final INotification notification = null;
        INotification notification2 = null;
        if (TextUtils.isEmpty((CharSequence)s)) {
            notification2 = notification;
        }
        else {
            final Object lockObj = this.lockObj;
            synchronized (lockObj) {
                final Info info = this.notificationIdMap.get(s);
                if (info != null) {
                    final INotification notification3 = info.notification;
                    return notification2;
                }
            }
            // monitorexit(o)
            notification2 = notification;
        }
        return notification2;
    }
    
    public int getNotificationColor() {
        while (true) {
            int color = -1;
            final Object lockObj = this.lockObj;
            synchronized (lockObj) {
                if (this.notificationPriorityMap.size() > 0) {
                    final Info info = this.notificationPriorityMap.lastKey();
                    if (!info.removed) {
                        color = info.notification.getColor();
                    }
                    // monitorexit(lockObj)
                    return color;
                }
            }
            // monitorexit(o)
            return color;
        }
    }
    
    public int getNotificationCount() {
        final Object lockObj = this.lockObj;
        synchronized (lockObj) {
            return this.notificationIdMap.size();
        }
    }
    
    public String getTopNotificationId() {
        while (true) {
            final Object lockObj = this.lockObj;
            Label_0072: {
                synchronized (lockObj) {
                    String s;
                    if (this.currentNotification != null) {
                        s = this.currentNotification.notification.getId();
                    }
                    else {
                        if (this.notificationPriorityMap.size() <= 0) {
                            break Label_0072;
                        }
                        s = this.notificationPriorityMap.lastKey().notification.getId();
                    }
                    // monitorexit(lockObj)
                    return s;
                }
            }
            // monitorexit(o)
            return null;
        }
    }
    
    public Context getUIContext() {
        return this.getNotificationView().getContext();
    }
    
    public void handleConnect() {
        NotificationManager.sLogger.v("client connected: restore stack");
        this.disconnected = false;
        this.restoreStack();
        this.notificationSavedListDeviceId = RemoteDeviceManager.getInstance().getDeviceId();
    }
    
    public void handleDisconnect(final boolean b) {
        if (!this.disconnected) {
            NotificationManager.sLogger.v("client disconnected: save stack :" + b);
            this.disconnected = true;
            this.saveStack();
            if (this.isExpanded() || this.isExpandedNotificationVisible()) {
                this.collapseExpandedNotification(true, true);
            }
            else {
                this.collapseNotification();
            }
            RemoteDeviceManager.getInstance().getCallManager().disconnect();
        }
    }
    
    public boolean handleKey(final InputManager.CustomKeyEvent customKeyEvent) {
        this.getNotificationView();
        NotificationManager.sLogger.v("handleKey: running=" + this.operationRunning + " qsize:" + this.operationQueue.size() + " animating:" + this.isAnimating);
        boolean b;
        if (this.isAnimating) {
            NotificationManager.sLogger.v("handleKey: animating no-op");
            b = false;
        }
        else {
            if (!this.operationRunning && this.isExpanded() && this.stackCurrentNotification != null && this.showOn && this.stackCurrentNotification.notification.supportScroll() && this.stackCurrentNotification.startCalled) {
                int n;
                if (customKeyEvent == InputManager.CustomKeyEvent.LEFT || customKeyEvent == InputManager.CustomKeyEvent.RIGHT) {
                    n = 1;
                }
                else {
                    n = 0;
                }
                if (n != 0) {
                    if (((InputManager.IInputHandler)this.stackCurrentNotification.notification).onKey(customKeyEvent)) {
                        this.scrollState = ScrollState.HANDLED;
                        this.handler.removeCallbacks(this.ignoreScrollRunnable);
                        b = true;
                        return b;
                    }
                    switch (this.scrollState) {
                        case NOT_HANDLED:
                            b = true;
                            return b;
                        case HANDLED:
                            this.startScrollThresholdTimer();
                            b = true;
                            return b;
                    }
                }
            }
            this.scrollState = ScrollState.NONE;
            b = false;
            switch (customKeyEvent) {
                case LEFT:
                    this.movePrevious(false);
                    b = true;
                    break;
                case RIGHT:
                    this.moveNext(false);
                    b = true;
                    break;
                case SELECT:
                    this.executeItem();
                    b = true;
                    break;
                case POWER_BUTTON_LONG_PRESS:
                    NotificationManager.sLogger.v("power button click: show shutdown");
                    this.pendingShutdownArgs = Shutdown.Reason.POWER_BUTTON.asBundle();
                    this.collapseExpandedNotification(true, true);
                    b = true;
                    break;
            }
        }
        return b;
    }
    
    public void hideConnectionToast(final boolean b) {
        final ToastManager instance = ToastManager.getInstance();
        if (b) {
            instance.dismissCurrentToast("disconnection#toast");
        }
        else {
            instance.dismissCurrentToast("connection#toast");
        }
    }
    
    public void hideNotification() {
        if (this.isExpanded && !this.isAnimating) {
            if (this.isExpanded()) {
                NotificationManager.sLogger.v("hideNotification:expand hide");
                this.animateOutExpandedView(true, this.currentNotification, false);
            }
            else {
                this.hideNotificationInternal();
            }
        }
    }
    
    public void hideNotificationCoverView() {
        final View expandedNotificationCoverView = this.getExpandedNotificationCoverView();
        if (expandedNotificationCoverView != null && expandedNotificationCoverView.getVisibility() == 0) {
            NotificationManager.sLogger.v("hideNotificationCoverView");
            expandedNotificationCoverView.setVisibility(GONE);
        }
    }
    
    public boolean isAnimating() {
        return this.isAnimating;
    }
    
    public boolean isCurrentItemDeleteAll() {
        return this.notifIndicator != null && this.isDeleteAllIndex(this.notifIndicator.getCurrentItem(), this.notifIndicator.getItemCount());
    }
    
    public boolean isCurrentNotificationId(final String s) {
        boolean b = false;
        if (this.currentNotification != null && TextUtils.equals((CharSequence)this.currentNotification.notification.getId(), (CharSequence)s)) {
            b = true;
        }
        return b;
    }
    
    public boolean isExpanded() {
        return this.expandedWithStack;
    }
    
    public boolean isExpandedNotificationVisible() {
        final boolean b = false;
        final FrameLayout expandedNotificationView = this.getExpandedNotificationView();
        boolean b2 = b;
        if (expandedNotificationView != null) {
            b2 = b;
            if (((View)expandedNotificationView).getVisibility() == 0) {
                b2 = true;
            }
        }
        return b2;
    }
    
    public boolean isNotificationMarkedForRemoval(final String s) {
        final Object lockObj = this.lockObj;
        synchronized (lockObj) {
            final Info notificationFromId = this.getNotificationFromId(s);
            boolean removed;
            if (notificationFromId != null) {
                removed = notificationFromId.removed;
            }
            else {
                removed = false;
            }
            // monitorexit(lockObj)
            return removed;
        }
    }
    
    public boolean isNotificationPresent(final String s) {
        final Object lockObj = this.lockObj;
        synchronized (lockObj) {
            return this.notificationIdMap.containsKey(s);
        }
    }
    
    public boolean isNotificationViewShowing() {
        boolean b = false;
        Label_0023: {
            if (this.mainScreen != null) {
                break Label_0023;
            }
            this.getNotificationView();
            if (this.mainScreen != null) {
                break Label_0023;
            }
            return b;
        }
        if (this.mainScreen.isNotificationViewShowing() || this.mainScreen.isNotificationExpanding()) {
            b = true;
            return b;
        }
        return b;
    }
    
    public boolean isNotificationsEnabled() {
        return this.enable;
    }
    
    public boolean isPhoneNotificationsEnabled() {
        return this.enablePhoneCalls;
    }
    
    public boolean makeNotificationCurrent(final boolean b) {
        while (true) {
            final boolean b2 = false;
            final Object lockObj = this.lockObj;
            // monitorexit(o)
            Label_0307: {
                synchronized (lockObj) {
                    if (this.currentNotification != null) {
                        boolean b3;
                        if (this.notificationPriorityMap.size() == 0) {
                            if (this.currentNotification.startCalled) {
                                this.currentNotification.notification.onStop();
                            }
                            this.currentNotification = null;
                            // monitorexit(lockObj)
                            b3 = b2;
                        }
                        else {
                            final Info currentNotification = this.notificationPriorityMap.lastKey();
                            if (currentNotification != null && currentNotification != this.currentNotification) {
                                NotificationManager.sLogger.v("mknc:current[" + this.currentNotification.notification.getId() + "] is not highest[" + currentNotification.notification.getId() + "]");
                                if (this.currentNotification.startCalled) {
                                    this.currentNotification.startCalled = false;
                                    this.currentNotification.notification.onStop();
                                    NotificationManager.sLogger.v("mknc: stop called");
                                }
                                this.currentNotification = currentNotification;
                            }
                            // monitorexit(lockObj)
                            b3 = true;
                        }
                        return b3;
                    }
                    if (this.notificationPriorityMap.size() <= 0) {
                        break Label_0307;
                    }
                    if (b) {
                        NotificationManager.sLogger.v("make all visible");
                        final Iterator<Info> iterator = this.notificationPriorityMap.keySet().iterator();
                        while (iterator.hasNext()) {
                            iterator.next().pushBackDuetoHighPriority = true;
                        }
                    }
                }
                this.currentNotification = this.notificationPriorityMap.lastKey();
                return true;
            }
            // monitorexit(o)
            return b2;
        }
    }
    
    public void moveNext(final boolean b) {
        if (this.isExpanded() || this.isExpandedNotificationVisible()) {
            if (!this.deleteAllGlances) {
                this.runOperation(NotificationAnimator.Operation.MOVE_NEXT, false, false, false, b, null, false);
            }
            else {
                NotificationManager.sLogger.v("moveNext: no-op deleteGlances set");
            }
        }
    }
    
    public void movePrevious(final boolean b) {
        if (this.isExpanded() || this.isExpandedNotificationVisible()) {
            if (!this.deleteAllGlances) {
                this.runOperation(NotificationAnimator.Operation.MOVE_PREV, false, false, false, b, null, false);
            }
            else {
                NotificationManager.sLogger.v("movePrevious: no-op deleteGlances set");
            }
        }
    }
    
    @Subscribe
    public void onCallAccepted(final CallManager.CallAccepted callAccepted) {
        NotificationManager.sLogger.v("phone-start");
        this.updateExpandedIndicator();
    }
    
    @Subscribe
    public void onCallEnded(final CallManager.CallEnded callEnded) {
        if (this.notificationController.isExpandedWithStack()) {
            this.handleRemoveNotificationInExpandedMode("navdy#phone#call#notif", false);
        }
        else {
            NotificationManager.sLogger.v("phone-end");
        }
    }
    
    @Subscribe
    public void onClearGlances(final ClearGlances clearGlances) {
        this.clearAllGlances();
    }
    
    @Subscribe
    public void onNotificationPreference(final NotificationPreferences notificationPreferences) {
        if (!notificationPreferences.enabled) {
            this.clearAllGlances();
        }
    }
    
    @Subscribe
    public void onShowToast(final ToastManager.ShowToast showToast) {
        if (this.deleteAllGlances && TextUtils.equals((CharSequence)showToast.name, (CharSequence)"glance-deleted")) {
            NotificationManager.sLogger.v("showToast: called deleteAllGlances");
            this.deleteAllGlances();
        }
    }
    
    public void postNotificationChange() {
        this.bus.post(NotificationManager.NOTIFICATION_CHANGE);
    }
    
    public void printPriorityMap() {
    Label_0041_Outer:
        while (true) {
            final Object lockObj = this.lockObj;
            while (true) {
            Label_0217:
                while (true) {
                    synchronized (lockObj) {
                        if (this.stackCurrentNotification == null) {
                            NotificationManager.sLogger.v("notif-debug-map stack current = null");
                            if (this.currentNotification == null) {
                                NotificationManager.sLogger.v("notif-debug-map current = null");
                                final Iterator<Info> iterator = this.notificationPriorityMap.keySet().iterator();
                                NotificationManager.sLogger.v("notif-debug-map-start");
                                while (iterator.hasNext()) {
                                    final Info info = iterator.next();
                                    NotificationManager.sLogger.v("notif-debug [" + info.notification.getId() + "] priority[" + info.priority + "] counter [" + info.uniqueCounter + "]");
                                }
                                break;
                            }
                            break Label_0217;
                        }
                    }
                    NotificationManager.sLogger.v("notif-debug-map stack current = " + this.stackCurrentNotification.notification.getId());
                    continue Label_0041_Outer;
                }
                NotificationManager.sLogger.v("notif-debug-map current = " + this.currentNotification.notification.getId());
                continue;
            }
        }
        NotificationManager.sLogger.v("notif-debug-map-stop");
    }
    // monitorexit(o)
    
    public void removeNotification(final String s) {
        if (Looper.getMainLooper() != Looper.myLooper()) {
            this.handler.post((Runnable)new Runnable() {
                @Override
                public void run() {
                    NotificationManager.this.removeNotification(s, true);
                }
            });
        }
        else {
            this.removeNotification(s, true);
        }
    }
    
    public void removeNotification(final String s, final boolean b) {
        this.removeNotification(s, b, null, null, null);
    }
    
    public void removeNotification(final String s, final boolean removed, Screen pendingScreen, final Bundle pendingScreenArgs, final Object pendingScreenArgs2) {
        if (!TextUtils.isEmpty((CharSequence)s)) {
            final Object lockObj = this.lockObj;
            synchronized (lockObj) {
                this.pendingScreen = pendingScreen;
                this.pendingScreenArgs = pendingScreenArgs;
                this.pendingScreenArgs2 = pendingScreenArgs2;
                pendingScreen = (Screen)this.notificationIdMap.get(s);
                if (pendingScreen == null) {
                    return;
                }
            }
            final String s2;
            if (this.notificationController.isExpandedWithStack() && !NotificationHelper.isNotificationRemovable(s2)) {
                NotificationManager.sLogger.v("removeNotification, handleRemoveNotificationInExpandedMode");
                this.handleRemoveNotificationInExpandedMode(s2, ((Info)pendingScreen).removed = true);
            }
            // monitorexit(o)
            else {
                ((Info)pendingScreen).removed = removed;
                if (((Info)pendingScreen).removed && ((Info)pendingScreen).resurrected) {
                    NotificationManager.sLogger.v("removeNotification [" + s2 + "] resurrected:false");
                    ((Info)pendingScreen).resurrected = false;
                }
                if (!((Info)pendingScreen).removed && !((Info)pendingScreen).notification.isAlive()) {
                    NotificationManager.sLogger.v("removeNotification [" + s2 + "] is not alive");
                    ((Info)pendingScreen).removed = true;
                }
                if (!((Info)pendingScreen).removed) {
                    NotificationManager.sLogger.v("removeNotification [" + s2 + "] is still alive");
                }
                if (this.currentNotification != null && TextUtils.equals((CharSequence)this.currentNotification.notification.getId(), (CharSequence)((Info)pendingScreen).notification.getId())) {
                    if (this.isExpanded && !this.isAnimating) {
                        this.hideNotification();
                    }
                }
                else {
                    NotificationManager.sLogger.v("bkgnd notification being removed");
                    this.removeNotificationfromStack((Info)pendingScreen, false);
                    this.setNotificationColor();
                }
            }
            // monitorexit(o)
        }
    }
    
    public void setExpandedStack(final boolean expandedWithStack) {
        this.expandedWithStack = expandedWithStack;
    }
    
    public void setNotificationColor() {
        final NotificationView notificationView = this.getNotificationView();
        if (notificationView != null) {
            if (this.currentNotification != null) {
                while (true) {
                    final Object lockObj = this.lockObj;
                    while (true) {
                        final Info lowerNotification;
                        synchronized (lockObj) {
                            lowerNotification = this.getLowerNotification(this.currentNotification);
                            if (lowerNotification == null) {
                                notificationView.resetNextNotificationColor();
                                break;
                            }
                        }
                        final NotificationView notificationView2;
                        notificationView2.setNextNotificationColor(lowerNotification.notification.getColor());
                        continue;
                    }
                }
            }
            else {
                notificationView.resetNextNotificationColor();
            }
        }
    }
    
    public void showHideToast(final boolean b) {
        final ToastManager instance = ToastManager.getInstance();
        if (!this.uiStateManager.isWelcomeScreenOn()) {
            if (!b) {
                if (!instance.isCurrentToast("disconnection#toast")) {
                    ConnectionNotification.showDisconnectedToast(false);
                }
            }
            else if (!instance.isCurrentToast("connection#toast")) {
                ConnectionNotification.showConnectedToast();
            }
        }
        else {
            NotificationManager.sLogger.v("welcome screen on, no disconnect notification");
            this.hideConnectionToast(b);
        }
    }
    
    public boolean showNotification() {
        boolean b = false;
        if (!this.enable) {
            if (!this.enablePhoneCalls || !this.isCurrentNotificationId("navdy#phone#call#notif")) {
                NotificationManager.sLogger.v("notification not enabled don't show notification");
                return b;
            }
            NotificationManager.sLogger.v("notification only phone-notif allowed");
        }
        if (this.uiStateManager.isWelcomeScreenOn()) {
            NotificationManager.sLogger.v("welcome screen on, don't show notification");
        }
        else {
            this.showNotification(false);
            b = true;
        }
        return b;
    }
    
    public void updateExpandedIndicator(final int itemCount, final int n, final int n2) {
        this.notifIndicator.setItemCount(itemCount);
        this.notifIndicator.setCurrentItem(n, n2);
        this.displayScrollingIndicator(n);
        NotificationManager.sLogger.v("updateExpandedIndicator count =" + itemCount + " currentitem=" + n);
    }
    
    public void viewSwitchAnimation(final View view, final View view2, final Info info, final boolean b, final int n) {
        final View expandedViewChild = this.getExpandedViewChild();
        int currentItem = this.notifIndicator.getCurrentItem();
        if (b) {
            --currentItem;
        }
        else {
            ++currentItem;
        }
        final Runnable runnable = new Runnable() {
            @Override
            public void run() {
                while (true) {
                    final Object access$2300 = NotificationManager.this.lockObj;
                    while (true) {
                        synchronized (access$2300) {
                            NotificationManager.this.notifIndicator.setCurrentItem(currentItem, n);
                            if (NotificationManager.this.expandedWithStack) {
                                if (NotificationManager.this.stackCurrentNotification != null && NotificationManager.this.stackCurrentNotification.startCalled) {
                                    NotificationManager.this.stackCurrentNotification.startCalled = false;
                                    NotificationManager.this.stackCurrentNotification.notification.onStop();
                                }
                                if (info == null) {
                                    NotificationManager.this.lastStackCurrentNotification = NotificationManager.this.stackCurrentNotification;
                                }
                                NotificationManager.this.stackCurrentNotification = info;
                                if (expandedViewChild != null && GlanceHelper.isDeleteAllView(expandedViewChild)) {
                                    NotificationManager.this.expandedNotifView.removeView(expandedViewChild);
                                    NotificationManager.sLogger.v("delete all big view removed");
                                    expandedViewChild.setTag(null);
                                    GlanceViewCache.putView(GlanceViewCache.ViewType.BIG_TEXT, expandedViewChild);
                                }
                                if (NotificationManager.sLogger.isLoggable(2)) {
                                    NotificationManager.this.printPriorityMap();
                                }
                                if (NotificationManager.this.stackCurrentNotification != null) {
                                    NotificationManager.this.stackCurrentNotification.notification.onExpandedNotificationSwitched();
                                }
                                NotificationManager.this.displayScrollingIndicator(currentItem);
                            }
                            NotificationManager.this.runQueuedOperation();
                            // monitorexit(access$2300)
                            if (NotificationManager.this.stackCurrentNotification != null) {
                                final INotification notification = NotificationManager.this.stackCurrentNotification.notification;
                                AnalyticsSupport.recordGlanceAction("Glance_Advance", notification, null);
                                return;
                            }
                        }
                        final INotification notification = null;
                        continue;
                    }
                }
            }
        };
        if (view != null) {
            final View currentNotificationViewChild = this.notifView.getCurrentNotificationViewChild();
            final ViewGroup notificationContainer = this.notifView.getNotificationContainer();
            int animationTranslation;
            if (b) {
                animationTranslation = -this.animationTranslation;
            }
            else {
                animationTranslation = this.animationTranslation;
            }
            NotificationAnimator.animateNotifViews(view, currentNotificationViewChild, notificationContainer, animationTranslation, 250, 250, new Runnable() {
                @Override
                public void run() {
                    if (info != null) {
                        final AnimatorSet viewSwitchAnimation = info.notification.getViewSwitchAnimation(true);
                        if (viewSwitchAnimation != null) {
                            viewSwitchAnimation.setDuration(100L);
                            viewSwitchAnimation.start();
                        }
                    }
                    if (currentNotificationViewChild != null && GlanceHelper.isDeleteAllView(currentNotificationViewChild)) {
                        notificationContainer.removeView(currentNotificationViewChild);
                        NotificationManager.sLogger.v("delete all small view removed");
                        currentNotificationViewChild.setTag(null);
                        NotificationManager.this.cleanupView(currentNotificationViewChild);
                    }
                    if (view2 == null) {
                        runnable.run();
                    }
                }
            });
        }
        if (view2 != null) {
            final FrameLayout expandedNotifView = this.expandedNotifView;
            int animationTranslation2;
            if (b) {
                animationTranslation2 = -this.animationTranslation;
            }
            else {
                animationTranslation2 = this.animationTranslation;
            }
            NotificationAnimator.animateExpandedViews(view2, expandedViewChild, (ViewGroup)expandedNotifView, animationTranslation2, 250, 250, runnable);
        }
        if (this.isExpanded()) {
            this.hideScrollingIndicator();
            if (this.stackCurrentNotification != null && this.stackCurrentNotification.notification.supportScroll()) {
                ((IScrollEvent)this.stackCurrentNotification.notification).setListener(null);
            }
        }
        final AnimatorSet itemMoveAnimator = this.notifIndicator.getItemMoveAnimator(currentItem, n);
        if (itemMoveAnimator != null) {
            itemMoveAnimator.start();
        }
    }
    
    static class Info
    {
        INotification notification;
        int priority;
        boolean pushBackDuetoHighPriority;
        boolean removed;
        boolean resurrected;
        boolean startCalled;
        long uniqueCounter;
    }
    
    public static class NotificationChange
    {
    }
    
    private enum ScrollState
    {
        HANDLED, 
        LIMIT_REACHED, 
        NONE, 
        NOT_HANDLED;
    }
}
