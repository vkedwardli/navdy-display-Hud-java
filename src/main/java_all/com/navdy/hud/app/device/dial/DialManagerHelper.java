package com.navdy.hud.app.device.dial;

import java.util.Map;
import com.localytics.android.Localytics;
import java.util.HashMap;
import android.os.Handler;
import com.navdy.hud.app.bluetooth.utils.BluetoothUtils;
import com.navdy.service.library.task.TaskManager;
import android.bluetooth.BluetoothProfile;
import com.navdy.hud.app.HudApplication;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothProfile;
import com.navdy.service.library.log.Logger;

public class DialManagerHelper
{
    private static final Logger sLogger;
    
    static {
        sLogger = DialManager.sLogger;
    }
    
    public static void connectToDial(final BluetoothAdapter bluetoothAdapter, final BluetoothDevice bluetoothDevice, final IDialConnection dialConnection) {
        if (bluetoothAdapter != null && bluetoothDevice != null) {
            if (dialConnection != null) {
                final boolean profileProxy = bluetoothAdapter.getProfileProxy(HudApplication.getAppContext(), (BluetoothProfile$ServiceListener)new BluetoothProfile$ServiceListener() {
                    boolean eventSent = false;
                    
                    public void onServiceConnected(final int n, final BluetoothProfile bluetoothProfile) {
                        TaskManager.getInstance().execute(new Runnable() {
                            @Override
                            public void run() {
                                while (true) {
                                    Label_0242: {
                                        try {
                                            final int connectionState = bluetoothProfile.getConnectionState(bluetoothDevice);
                                            DialManagerHelper.sLogger.v("[Dial]connectToDial Device " + bluetoothDevice.getName() + " state is " + BluetoothUtils.getConnectedState(connectionState));
                                            if (connectionState == 2) {
                                                DialManagerHelper.sLogger.v("[Dial]connectToDial already connected:" + bluetoothDevice);
                                                if (!BluetoothProfile$ServiceListener.this.eventSent) {
                                                    BluetoothProfile$ServiceListener.this.eventSent = true;
                                                    dialConnection.onAttemptConnection(true);
                                                }
                                            }
                                            else {
                                                if (forceConnect(bluetoothProfile, bluetoothDevice)) {
                                                    break Label_0242;
                                                }
                                                DialManagerHelper.sLogger.v("[Dial]connectToDial proxy-connect to hid device failed");
                                                if (!BluetoothProfile$ServiceListener.this.eventSent) {
                                                    BluetoothProfile$ServiceListener.this.eventSent = true;
                                                    dialConnection.onAttemptConnection(false);
                                                }
                                            }
                                            return;
                                        }
                                        catch (Throwable t) {
                                            DialManagerHelper.sLogger.e("[Dial] connectToDial", t);
                                            if (!BluetoothProfile$ServiceListener.this.eventSent) {
                                                BluetoothProfile$ServiceListener.this.eventSent = true;
                                                dialConnection.onAttemptConnection(false);
                                            }
                                            return;
                                        }
                                    }
                                    DialManagerHelper.sLogger.v("[Dial]connectToDial proxy-connect to hid device successful");
                                    if (!BluetoothProfile$ServiceListener.this.eventSent) {
                                        BluetoothProfile$ServiceListener.this.eventSent = true;
                                        dialConnection.onAttemptConnection(true);
                                    }
                                }
                            }
                        }, 1);
                    }
                    
                    public void onServiceDisconnected(final int n) {
                        TaskManager.getInstance().execute(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    DialManagerHelper.sLogger.v("[Dial]connectToDial onServiceDisconnected: could not connect proxy");
                                    if (!BluetoothProfile$ServiceListener.this.eventSent) {
                                        BluetoothProfile$ServiceListener.this.eventSent = true;
                                        dialConnection.onAttemptConnection(false);
                                    }
                                }
                                catch (Throwable t) {
                                    DialManagerHelper.sLogger.e("[Dial]", t);
                                }
                            }
                        }, 1);
                    }
                }, 4);
                DialManagerHelper.sLogger.v("[Dial]connectToDial getProfileProxy returned:" + profileProxy);
                if (!profileProxy) {
                    TaskManager.getInstance().execute(new Runnable() {
                        @Override
                        public void run() {
                            dialConnection.onAttemptConnection(false);
                        }
                    }, 1);
                }
                return;
            }
        }
        try {
            throw new IllegalArgumentException();
        }
        catch (Throwable t) {
            DialManagerHelper.sLogger.e("connectToDial", t);
            TaskManager.getInstance().execute(new Runnable() {
                @Override
                public void run() {
                    dialConnection.onAttemptConnection(false);
                }
            }, 1);
        }
    }
    
    public static void disconnectFromDial(final BluetoothAdapter bluetoothAdapter, final BluetoothDevice bluetoothDevice, final IDialConnection dialConnection) {
        if (bluetoothAdapter != null && bluetoothDevice != null) {
            if (dialConnection != null) {
                final boolean profileProxy = bluetoothAdapter.getProfileProxy(HudApplication.getAppContext(), (BluetoothProfile$ServiceListener)new BluetoothProfile$ServiceListener() {
                    boolean eventSent = false;
                    
                    public void onServiceConnected(final int n, final BluetoothProfile bluetoothProfile) {
                        TaskManager.getInstance().execute(new Runnable() {
                            @Override
                            public void run() {
                                while (true) {
                                    try {
                                        final int connectionState = bluetoothProfile.getConnectionState(bluetoothDevice);
                                        DialManagerHelper.sLogger.v("[Dial]disconnectFromDial Device " + bluetoothDevice.getName() + " state is " + BluetoothUtils.getConnectedState(connectionState));
                                        if (connectionState == 2) {
                                            if (!forceDisconnect(bluetoothProfile, bluetoothDevice)) {
                                                DialManagerHelper.sLogger.v("[Dial]disconnectFromDial proxy-disconnect from hid device failed");
                                                if (!BluetoothProfile$ServiceListener.this.eventSent) {
                                                    BluetoothProfile$ServiceListener.this.eventSent = true;
                                                    dialConnection.onAttemptDisconnection(false);
                                                }
                                            }
                                            else {
                                                DialManagerHelper.sLogger.v("[Dial]disconnectFromDial proxy-disconnect from hid device successful");
                                                if (!BluetoothProfile$ServiceListener.this.eventSent) {
                                                    BluetoothProfile$ServiceListener.this.eventSent = true;
                                                    dialConnection.onAttemptDisconnection(true);
                                                }
                                            }
                                            return;
                                        }
                                    }
                                    catch (Throwable t) {
                                        DialManagerHelper.sLogger.e("[Dial] disconnectFromDial", t);
                                        if (!BluetoothProfile$ServiceListener.this.eventSent) {
                                            BluetoothProfile$ServiceListener.this.eventSent = true;
                                            dialConnection.onAttemptDisconnection(false);
                                        }
                                        return;
                                    }
                                    DialManagerHelper.sLogger.v("[Dial]disconnectFromDial already disconnected:" + bluetoothDevice);
                                    if (!BluetoothProfile$ServiceListener.this.eventSent) {
                                        BluetoothProfile$ServiceListener.this.eventSent = true;
                                        dialConnection.onAttemptDisconnection(true);
                                    }
                                }
                            }
                        }, 1);
                    }
                    
                    public void onServiceDisconnected(final int n) {
                        TaskManager.getInstance().execute(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    DialManagerHelper.sLogger.v("[Dial]disconnectFromDial onServiceDisconnected: could not connect proxy");
                                    if (!BluetoothProfile$ServiceListener.this.eventSent) {
                                        BluetoothProfile$ServiceListener.this.eventSent = true;
                                        dialConnection.onAttemptDisconnection(false);
                                    }
                                }
                                catch (Throwable t) {
                                    DialManagerHelper.sLogger.e("[Dial]", t);
                                }
                            }
                        }, 1);
                    }
                }, 4);
                DialManagerHelper.sLogger.v("[Dial]disconnectFromDial getProfileProxy returned:" + profileProxy);
                if (!profileProxy) {
                    TaskManager.getInstance().execute(new Runnable() {
                        @Override
                        public void run() {
                            dialConnection.onAttemptDisconnection(false);
                        }
                    }, 1);
                }
                return;
            }
        }
        try {
            throw new IllegalArgumentException();
        }
        catch (Throwable t) {
            DialManagerHelper.sLogger.e("disconnectFromDial", t);
            TaskManager.getInstance().execute(new Runnable() {
                @Override
                public void run() {
                    dialConnection.onAttemptDisconnection(false);
                }
            }, 1);
        }
    }
    
    private static boolean forceConnect(final BluetoothProfile bluetoothProfile, final BluetoothDevice bluetoothDevice) {
        return invokeMethod(bluetoothProfile, bluetoothDevice, "connect");
    }
    
    private static boolean forceDisconnect(final BluetoothProfile bluetoothProfile, final BluetoothDevice bluetoothDevice) {
        return invokeMethod(bluetoothProfile, bluetoothDevice, "disconnect");
    }
    
    private static boolean invokeMethod(final BluetoothProfile p0, final BluetoothDevice p1, final String p2) {
        // 
        This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: invokestatic    java/lang/Boolean.valueOf:(Z)Ljava/lang/Boolean;
        //     4: astore_3       
        //     5: ldc             "android.bluetooth.BluetoothInputDevice"
        //     7: invokestatic    java/lang/Class.forName:(Ljava/lang/String;)Ljava/lang/Class;
        //    10: aload_2        
        //    11: iconst_1       
        //    12: anewarray       Ljava/lang/Class;
        //    15: dup            
        //    16: iconst_0       
        //    17: ldc             Landroid/bluetooth/BluetoothDevice;.class
        //    19: aastore        
        //    20: invokevirtual   java/lang/Class.getMethod:(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
        //    23: aload_0        
        //    24: iconst_1       
        //    25: anewarray       Ljava/lang/Object;
        //    28: dup            
        //    29: iconst_0       
        //    30: aload_1        
        //    31: aastore        
        //    32: invokevirtual   java/lang/reflect/Method.invoke:(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
        //    35: checkcast       Ljava/lang/Boolean;
        //    38: astore_0       
        //    39: aload_0        
        //    40: invokevirtual   java/lang/Boolean.booleanValue:()Z
        //    43: istore          4
        //    45: iload           4
        //    47: ireturn        
        //    48: astore_0       
        //    49: getstatic       com/navdy/hud/app/device/dial/DialManagerHelper.sLogger:Lcom/navdy/service/library/log/Logger;
        //    52: ldc             "[Dial]"
        //    54: aload_0        
        //    55: invokevirtual   com/navdy/service/library/log/Logger.e:(Ljava/lang/String;Ljava/lang/Throwable;)V
        //    58: aload_3        
        //    59: astore_0       
        //    60: goto            39
        //    63: astore_0       
        //    64: getstatic       com/navdy/hud/app/device/dial/DialManagerHelper.sLogger:Lcom/navdy/service/library/log/Logger;
        //    67: ldc             "[Dial]"
        //    69: aload_0        
        //    70: invokevirtual   com/navdy/service/library/log/Logger.e:(Ljava/lang/String;Ljava/lang/Throwable;)V
        //    73: iconst_0       
        //    74: istore          4
        //    76: goto            45
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  5      39     48     63     Ljava/lang/Throwable;
        //  39     45     63     79     Ljava/lang/Throwable;
        //  49     58     63     79     Ljava/lang/Throwable;
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0039:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public static void isDialConnected(final BluetoothAdapter bluetoothAdapter, final BluetoothDevice bluetoothDevice, final IDialConnectionStatus dialConnectionStatus) {
        if (bluetoothAdapter != null && bluetoothDevice != null) {
            if (dialConnectionStatus != null) {
                final boolean profileProxy = bluetoothAdapter.getProfileProxy(HudApplication.getAppContext(), (BluetoothProfile$ServiceListener)new BluetoothProfile$ServiceListener() {
                    public void onServiceConnected(final int n, final BluetoothProfile bluetoothProfile) {
                        TaskManager.getInstance().execute(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    final int connectionState = bluetoothProfile.getConnectionState(bluetoothDevice);
                                    DialManagerHelper.sLogger.v("[Dial]Device " + bluetoothDevice.getName() + " state is " + BluetoothUtils.getConnectedState(connectionState));
                                    if (connectionState == 2) {
                                        DialManagerHelper.sLogger.v("[Dial]already connected");
                                        dialConnectionStatus.onStatus(true);
                                    }
                                    else {
                                        dialConnectionStatus.onStatus(false);
                                    }
                                }
                                catch (Throwable t) {
                                    DialManagerHelper.sLogger.e("[Dial]", t);
                                    dialConnectionStatus.onStatus(false);
                                }
                            }
                        }, 1);
                    }
                    
                    public void onServiceDisconnected(final int n) {
                        TaskManager.getInstance().execute(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    DialManagerHelper.sLogger.v("[Dial]onServiceDisconnected: could not connect proxy");
                                    dialConnectionStatus.onStatus(false);
                                }
                                catch (Throwable t) {
                                    DialManagerHelper.sLogger.e("[Dial]", t);
                                }
                            }
                        }, 1);
                    }
                }, 4);
                DialManagerHelper.sLogger.v("[Dial]getProfileProxy returned:" + profileProxy);
                if (!profileProxy) {
                    TaskManager.getInstance().execute(new Runnable() {
                        @Override
                        public void run() {
                            dialConnectionStatus.onStatus(false);
                        }
                    }, 1);
                }
                return;
            }
        }
        try {
            throw new IllegalArgumentException();
        }
        catch (Throwable t) {
            DialManagerHelper.sLogger.e("isDialConnected", t);
            TaskManager.getInstance().execute(new Runnable() {
                @Override
                public void run() {
                    dialConnectionStatus.onStatus(false);
                }
            }, 1);
        }
    }
    
    public static void sendLocalyticsEvent(final Handler handler, final boolean b, final boolean b2, final int n, final boolean b3) {
        sendLocalyticsEvent(handler, b, b2, n, b3, null);
    }
    
    public static void sendLocalyticsEvent(final Handler handler, final boolean b, final boolean b2, final int n, final boolean b3, final String s) {
        final Runnable runnable = new Runnable() {
            final /* synthetic */ BluetoothDevice val$device = DialManager.getInstance().getDialDevice();
            
            @Override
            public void run() {
                try {
                    final HashMap<String, String> hashMap = new HashMap<String, String>();
                    String s;
                    if (b) {
                        s = "true";
                    }
                    else {
                        s = "false";
                    }
                    hashMap.put("First_Time", s);
                    String s2;
                    if (b2) {
                        s2 = "true";
                    }
                    else {
                        s2 = "false";
                    }
                    hashMap.put("Success", s2);
                    String s3;
                    if (b3) {
                        s3 = "Pair_Attempt";
                    }
                    else {
                        s3 = "Connect";
                    }
                    hashMap.put("Type", s3);
                    hashMap.put("Result", s);
                    String address = null;
                    if (this.val$device != null) {
                        address = this.val$device.getAddress();
                    }
                    hashMap.put("Dial_Address", address);
                    if (b2) {
                        final DialManager instance = DialManager.getInstance();
                        final String value = String.valueOf(instance.getLastKnownBatteryLevel());
                        final String value2 = String.valueOf(instance.getLastKnownRawBatteryLevel());
                        final String value3 = String.valueOf(instance.getLastKnownSystemTemperature());
                        final String firmWareVersion = instance.getFirmWareVersion();
                        final String hardwareVersion = instance.getHardwareVersion();
                        final int incrementalVersion = instance.getDialFirmwareUpdater().getVersions().dial.incrementalVersion;
                        hashMap.put("Battery_Level", value);
                        hashMap.put("Raw_Battery_Level", value2);
                        hashMap.put("System_Temperature", value3);
                        hashMap.put("FW_Version", firmWareVersion);
                        hashMap.put("HW_Version", hardwareVersion);
                        hashMap.put("Incremental_Version", Integer.toString(incrementalVersion));
                        DialManagerHelper.sLogger.v("sending firstTime [" + b + "] foundDial[" + b2 + "] battery[" + value + "] rawBattery[" + value2 + "] temperature[" + value3 + "]" + " fw[" + firmWareVersion + "] hw[" + hardwareVersion + "] incremental[" + incrementalVersion + "] type[" + s3 + "] result [" + s + "] address [" + address + "]");
                    }
                    else {
                        DialManagerHelper.sLogger.v("sending firstTime [" + b + "] foundDial[" + b2 + "] type[" + s3 + "] result [" + s + "] address [" + address + "]");
                    }
                    Localytics.tagEvent("Dial_Pairing", hashMap);
                }
                catch (Throwable t) {
                    DialManagerHelper.sLogger.e(t);
                }
            }
        };
        long n2;
        if (b2) {
            n2 = n;
        }
        else {
            n2 = 0L;
        }
        handler.postDelayed((Runnable)runnable, n2);
    }
    
    public static void sendRebootLocalyticsEvent(final Handler handler, final boolean b, final BluetoothDevice bluetoothDevice, final String s) {
        handler.post((Runnable)new Runnable() {
            @Override
            public void run() {
                try {
                    final HashMap<String, String> hashMap = new HashMap<String, String>();
                    hashMap.put("Type", "Reboot");
                    hashMap.put("Result", s);
                    String s;
                    if (b) {
                        s = "true";
                    }
                    else {
                        s = "false";
                    }
                    hashMap.put("Success", s);
                    String address = null;
                    if (bluetoothDevice != null) {
                        address = bluetoothDevice.getAddress();
                    }
                    hashMap.put("Dial_Address", address);
                    final DialManager instance = DialManager.getInstance();
                    final String value = String.valueOf(instance.getLastKnownBatteryLevel());
                    final String value2 = String.valueOf(instance.getLastKnownRawBatteryLevel());
                    final String value3 = String.valueOf(instance.getLastKnownSystemTemperature());
                    final String firmWareVersion = instance.getFirmWareVersion();
                    final String hardwareVersion = instance.getHardwareVersion();
                    final int incrementalVersion = instance.getDialFirmwareUpdater().getVersions().dial.incrementalVersion;
                    hashMap.put("Battery_Level", value);
                    hashMap.put("Raw_Battery_Level", value2);
                    hashMap.put("System_Temperature", value3);
                    hashMap.put("FW_Version", firmWareVersion);
                    hashMap.put("HW_Version", hardwareVersion);
                    hashMap.put("Incremental_Version", Integer.toString(incrementalVersion));
                    DialManagerHelper.sLogger.v("sending foundDial[" + b + "] battery[" + value + "] rawBattery[" + value2 + "] temperature[" + value3 + "]" + " fw[" + firmWareVersion + "] hw[" + hardwareVersion + "] incremental[" + incrementalVersion + "] type[" + "Reboot" + "] result [" + s + "] address [" + address + "]");
                    Localytics.tagEvent("Dial_Pairing", hashMap);
                }
                catch (Throwable t) {
                    DialManagerHelper.sLogger.e(t);
                }
            }
        });
    }
    
    public interface IDialConnection
    {
        void onAttemptConnection(final boolean p0);
        
        void onAttemptDisconnection(final boolean p0);
    }
    
    public interface IDialConnectionStatus
    {
        void onStatus(final boolean p0);
    }
    
    public interface IDialForgotten
    {
        void onForgotten();
    }
}
