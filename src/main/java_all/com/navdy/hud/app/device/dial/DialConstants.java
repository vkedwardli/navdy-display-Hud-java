package com.navdy.hud.app.device.dial;

import java.util.concurrent.TimeUnit;
import java.util.UUID;

public class DialConstants
{
    public static final String ACTION_CONNECTION_STATE_CHANGED = "android.bluetooth.input.profile.action.CONNECTION_STATE_CHANGED";
    static final BatteryChangeEvent BATTERY_CHANGE_EVENT;
    static final int BATTERY_CHECK_INTERVAL = 300000;
    static final float BATTERY_DISPLAY_SCALE = 0.1f;
    public static final int BATTERY_LEVEL_UNKNOWN = -1;
    static final UUID BATTERY_LEVEL_UUID;
    static final UUID BATTERY_SERVICE_UUID;
    static final String CONNECT = "CONNECT";
    static final int CONNECTION_NOT_DONE_TIMEOUT = 5000;
    static final UUID DEVICE_INFO_FIRMWARE_VERSION_UUID;
    static final UUID DEVICE_INFO_HARDWARE_VERSION_UUID;
    static final UUID DEVICE_INFO_SERVICE_UUID;
    public static final String DIAL_ADDRESS = "Dial_Address";
    public static final String DIAL_BATTERY_LEVEL = "Battery_Level";
    static final int DIAL_EVENTS_PORT = 23654;
    public static final String DIAL_EVENT_CONNECT = "Connect";
    public static final String DIAL_EVENT_INPUT_REPORT_DESCRIPTOR = "INPUTDESCRIPTOR_WRITTEN";
    public static final String DIAL_EVENT_PAIR = "Pair_Attempt";
    public static final String DIAL_EVENT_REBOOT = "Reboot";
    public static final String DIAL_EVENT_TYPE = "Type";
    static final String DIAL_EXTREMELY_LOW_BATTERY_PROPERTY = "last_extremely_low_battery_notification";
    public static final String DIAL_FIRST_TIME = "First_Time";
    static final byte[] DIAL_FORGET_KEYS_MAGIC;
    static final UUID DIAL_FORGET_KEYS_UUID;
    public static final String DIAL_FW_VERSION = "FW_Version";
    public static final String DIAL_HW_VERSION = "HW_Version";
    public static final String DIAL_INCREMENTAL_VERSION = "Incremental_Version";
    public static final String DIAL_PAIRING_EVENT = "Dial_Pairing";
    public static final String DIAL_PAIRING_SUCCESS = "Success";
    public static final String DIAL_RAW_BATTERY_LEVEL = "Raw_Battery_Level";
    static final byte[] DIAL_REBOOT_MAGIC;
    public static final String DIAL_REBOOT_PROPERTY = "last_dial_reboot";
    static final UUID DIAL_REBOOT_UUID;
    public static final String DIAL_RESULT = "Result";
    public static final String DIAL_TEMPERATURE = "System_Temperature";
    static final String DIAL_VERY_LOW_BATTERY_PROPERTY = "last_very_low_battery_notification";
    static final String DISCONNECT = "DISCONNECT";
    static final int DISCONNECTED_THRESHOLD = 5000;
    static final String ENCRYPTION_FAILED = "ENCRYPTION_FAILED";
    static final int EXTREMELY_LOW_BATTERY = 10;
    static final UUID FIRMWARE_REVISION_CHARACTERISTIC_UUID;
    public static final int GATT_READ_DELAY = 5000;
    static final UUID HID_HOST_READY_UUID;
    static final int HID_PROFILE = 4;
    static final UUID HID_SERVICE_UUID;
    static final DialManagerInitEvent INIT_EVENT;
    static final int LOW_BATTERY = 80;
    static final int MAXIMUM_BOND_TIME = 30000;
    static final int MAX_BOND_RETRIES = 3;
    static final int MAX_ENCRYPTION_RETRY = 2;
    static final long MIN_TIME_BETWEEN_EXTREMELY_LOW_WARNINGS;
    static final long MIN_TIME_BETWEEN_REBOOTS;
    static final long MIN_TIME_BETWEEN_VERY_LOW_WARNINGS;
    static final String[] NAVDY_DIAL_FILTER;
    static final UUID OTA_CONTROL_CHARACTERISTIC_UUID;
    static final UUID OTA_DATA_CHARACTERISTIC_UUID;
    public static final String OTA_DIAL_NAME_KEY = "OtaDialNameKey";
    static final UUID OTA_INCREMENTAL_VERSION_CHARACTERISTIC_UUID;
    static final UUID OTA_SERVICE_UUID;
    static final int PAIRING_RETRY_DELAY = 4000;
    static final PairingRule PAIRING_RULE;
    static final UUID RAW_BATTERY_LEVEL_UUID;
    static final int SCAN_HANG_TIME = 30000;
    static final UUID SYSTEM_TEMPERATURE_UUID;
    public static final String VERSION_PREFIX = "1.0.";
    static final int VERY_LOW_BATTERY = 50;
    public static final String VIDEO_SHOWN_PREF = "first_run_dial_video_shown";
    
    static {
        INIT_EVENT = new DialManagerInitEvent();
        BATTERY_CHANGE_EVENT = new BatteryChangeEvent();
        HID_SERVICE_UUID = UUID.fromString("00001812-0000-1000-8000-00805f9b34fb");
        BATTERY_SERVICE_UUID = UUID.fromString("0000180F-0000-1000-8000-00805f9b34fb");
        DEVICE_INFO_SERVICE_UUID = UUID.fromString("0000180a-0000-1000-8000-00805f9b34fb");
        OTA_SERVICE_UUID = UUID.fromString("1d14d6ee-fd63-4fa1-bfa4-8f47b42119f0");
        BATTERY_LEVEL_UUID = UUID.fromString("00002a19-0000-1000-8000-00805f9b34fb");
        SYSTEM_TEMPERATURE_UUID = UUID.fromString("00002a6e-0000-1000-8000-00805f9b34fb");
        RAW_BATTERY_LEVEL_UUID = UUID.fromString("40cc0c40-3caa-11e6-b55c-0002a5d5c51c");
        DEVICE_INFO_HARDWARE_VERSION_UUID = UUID.fromString("00002a27-0000-1000-8000-00805f9b34fb");
        DEVICE_INFO_FIRMWARE_VERSION_UUID = UUID.fromString("00002a26-0000-1000-8000-00805f9b34fb");
        HID_HOST_READY_UUID = UUID.fromString("40cc0c40-3caa-11e6-b55c-0002a5d5c51b");
        OTA_CONTROL_CHARACTERISTIC_UUID = UUID.fromString("f7bf3564-fb6d-4e53-88a4-5e37e0326063");
        OTA_DATA_CHARACTERISTIC_UUID = UUID.fromString("984227f3-34fc-4045-a5d0-2c581f81a153");
        OTA_INCREMENTAL_VERSION_CHARACTERISTIC_UUID = UUID.fromString("2a9e7517-f977-4775-a993-b827ae0fff34");
        FIRMWARE_REVISION_CHARACTERISTIC_UUID = UUID.fromString("00002a26-0000-1000-8000-00805f9b34fb");
        DIAL_FORGET_KEYS_UUID = UUID.fromString("87c72b94-46fc-44ae-bd8d-d307bceb3b9f");
        DIAL_REBOOT_UUID = UUID.fromString("87c72b00-46fc-44ae-bd8d-d307bceb3b9f");
        DIAL_FORGET_KEYS_MAGIC = "DIE".getBytes();
        DIAL_REBOOT_MAGIC = "REBOOT".getBytes();
        NAVDY_DIAL_FILTER = new String[] { "Navdy Dial" };
        MIN_TIME_BETWEEN_VERY_LOW_WARNINGS = TimeUnit.DAYS.toMillis(7L);
        MIN_TIME_BETWEEN_EXTREMELY_LOW_WARNINGS = TimeUnit.DAYS.toMillis(30L);
        PAIRING_RULE = PairingRule.FIRST;
        MIN_TIME_BETWEEN_REBOOTS = TimeUnit.DAYS.toMillis(30L);
    }
    
    public static class BatteryChangeEvent
    {
    }
    
    public static class DialConnectionStatus
    {
        public String dialName;
        public Status status;
        
        DialConnectionStatus(final Status status) {
            this.status = status;
        }
        
        public enum Status
        {
            CONNECTED, 
            CONNECTING, 
            CONNECTION_FAILED, 
            DISCONNECTED, 
            NO_DIAL_FOUND;
        }
    }
    
    public static class DialManagerInitEvent
    {
    }
    
    public enum NotificationReason
    {
        CONNECTED, 
        DISCONNECTED, 
        EXTREMELY_LOW_BATTERY, 
        LOW_BATTERY, 
        OK_BATTERY, 
        VERY_LOW_BATTERY;
    }
    
    enum PairingRule
    {
        FIRST;
    }
}
