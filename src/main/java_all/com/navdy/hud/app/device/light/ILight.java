package com.navdy.hud.app.device.light;

public interface ILight
{
    void popSetting();
    
    void pushSetting(final LightSettings p0);
    
    void removeSetting(final LightSettings p0);
    
    void reset();
    
    void turnOff();
    
    void turnOn();
}
