package com.navdy.hud.app.presenter;

import android.os.Bundle;
import javax.inject.Inject;
import com.squareup.otto.Bus;
import com.navdy.service.library.log.Logger;
import javax.inject.Singleton;
import com.navdy.hud.app.view.NotificationView;
import com.navdy.hud.app.ui.framework.BasePresenter;

@Singleton
public class NotificationPresenter extends BasePresenter<NotificationView>
{
    private static final Logger sLogger;
    @Inject
    Bus bus;
    
    static {
        sLogger = new Logger(NotificationPresenter.class);
    }
    
    @Override
    public void onLoad(final Bundle bundle) {
        super.onLoad(bundle);
        this.bus.register(this);
    }
}
