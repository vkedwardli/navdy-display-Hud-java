package com.navdy.hud.app.screen;

import com.navdy.hud.app.view.DashboardWidgetView;
import com.navdy.hud.app.view.DashboardWidgetPresenter;
import java.util.List;

public interface IDashboardGaugesManager
{
    int getOptionIconResourceForGauge(final int p0);
    
    String getOptionLabelForGauge(final int p0);
    
    List<Integer> getSmartDashGauges();
    
    DashboardWidgetPresenter getWidgetPresenterForGauge(final int p0, final boolean p1);
    
    boolean isGaugeWorking(final int p0);
    
    void showGauge(final int p0, final DashboardWidgetView p1);
    
    void updateGauge(final int p0, final Object p1);
    
    void updateUserPreference(final GaugePositioning p0, final int p1);
    
    public enum GaugePositioning
    {
        LEFT, 
        RIGHT;
    }
}
