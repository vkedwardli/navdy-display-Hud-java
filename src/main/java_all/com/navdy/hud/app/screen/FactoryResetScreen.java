package com.navdy.hud.app.screen;

import android.os.Bundle;
import com.navdy.hud.app.manager.InputManager;
import android.os.RecoverySystem;
import com.navdy.service.library.task.TaskManager;
import com.navdy.hud.app.device.dial.DialManager;
import com.navdy.hud.app.analytics.AnalyticsSupport;
import com.navdy.hud.app.event.Shutdown;
import com.navdy.service.library.events.ui.ShowScreen;
import com.navdy.hud.app.HudApplication;
import java.util.ArrayList;
import android.content.res.Resources;
import com.navdy.hud.app.ui.component.ConfirmationLayout;
import com.navdy.hud.app.ui.component.ChoiceLayout;
import javax.inject.Inject;
import com.squareup.otto.Bus;
import java.util.List;
import javax.inject.Singleton;
import com.navdy.hud.app.ui.framework.BasePresenter;
import com.navdy.hud.app.view.FactoryResetView;
import com.navdy.hud.app.ui.activity.Main;
import dagger.Module;
import com.navdy.service.library.events.ui.Screen;
import com.navdy.service.library.log.Logger;
import flow.Layout;

@Layout(R.layout.screen_factory_reset)
public class FactoryResetScreen extends BaseScreen
{
    private static final Logger sLogger;
    
    static {
        sLogger = new Logger(FactoryResetScreen.class);
    }
    
    @Override
    public Object getDaggerModule() {
        return new Module();
    }
    
    @Override
    public String getMortarScopeName() {
        return this.getClass().getName();
    }
    
    @Override
    public Screen getScreen() {
        return Screen.SCREEN_FACTORY_RESET;
    }
    
    @dagger.Module(addsTo = Main.Module.class, injects = { FactoryResetView.class })
    public class Module
    {
    }
    
    @Singleton
    public static class Presenter extends BasePresenter<FactoryResetView>
    {
        private static final int CANCEL_POSITION = 1;
        private static final List<String> CONFIRMATION_CHOICES;
        private static final int FACTORY_RESET_POSITION = 0;
        @Inject
        Bus bus;
        private ChoiceLayout.IListener confirmationListener;
        private ConfirmationLayout factoryResetConfirmation;
        private Resources resources;
        
        static {
            (CONFIRMATION_CHOICES = new ArrayList<String>()).add(HudApplication.getAppContext().getResources().getString(R.string.factory_reset_go));
            Presenter.CONFIRMATION_CHOICES.add(HudApplication.getAppContext().getResources().getString(R.string.factory_reset_cancel));
        }
        
        public Presenter() {
            this.confirmationListener = new ChoiceLayout.IListener() {
                @Override
                public void executeItem(final int n, final int n2) {
                    switch (n) {
                        case 1:
                            Presenter.this.bus.post(new ShowScreen.Builder().screen(Screen.SCREEN_BACK).build());
                            break;
                        case 0:
                            AnalyticsSupport.recordShutdown(Shutdown.Reason.FACTORY_RESET, false);
                            DialManager.getInstance().requestDialForgetKeys();
                            TaskManager.getInstance().execute(new Runnable() {
                                @Override
                                public void run() {
                                    try {
                                        RecoverySystem.rebootWipeUserData(HudApplication.getAppContext());
                                    }
                                    catch (Exception ex) {
                                        FactoryResetScreen.sLogger.e("Failed to do factory reset", ex);
                                        Presenter.this.bus.post(new ShowScreen.Builder().screen(Screen.SCREEN_BACK).build());
                                    }
                                }
                            }, 1);
                            break;
                    }
                }
                
                @Override
                public void itemSelected(final int n, final int n2) {
                }
            };
        }
        
        public boolean handleKey(final InputManager.CustomKeyEvent customKeyEvent) {
            return this.factoryResetConfirmation.handleKey(customKeyEvent);
        }
        
        @Override
        public void onLoad(final Bundle bundle) {
            this.resources = HudApplication.getAppContext().getResources();
            this.updateView();
            super.onLoad(bundle);
        }
        
        protected void updateView() {
            final FactoryResetView factoryResetView = this.getView();
            if (factoryResetView != null) {
                this.factoryResetConfirmation = factoryResetView.getConfirmation();
                this.factoryResetConfirmation.screenTitle.setText((CharSequence)this.resources.getString(R.string.factory_reset_screen_title));
                this.factoryResetConfirmation.title1.setVisibility(GONE);
                this.factoryResetConfirmation.title2.setVisibility(GONE);
                this.factoryResetConfirmation.title3.setSingleLine(false);
                this.factoryResetConfirmation.title3.setText((CharSequence)this.resources.getString(R.string.factory_reset_description));
                this.factoryResetConfirmation.screenImage.setImageDrawable(this.resources.getDrawable(R.drawable.icon_settings_factory_reset));
                this.factoryResetConfirmation.setChoices(Presenter.CONFIRMATION_CHOICES, 1, this.confirmationListener);
            }
        }
    }
}
