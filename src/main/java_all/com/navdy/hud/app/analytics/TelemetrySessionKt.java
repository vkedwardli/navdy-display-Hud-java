package com.navdy.hud.app.analytics;

import kotlin.Metadata;

@Metadata(bv = { 1, 0, 1 }, d1 = { "\u0000\u0012\n\u0000\n\u0002\u0010\u0007\n\u0002\b\u0004\n\u0002\u0010\t\n\u0002\b\u0003\u001a\n\u0010\u0000\u001a\u00020\u0001*\u00020\u0001\u001a\n\u0010\u0002\u001a\u00020\u0001*\u00020\u0001\u001a\n\u0010\u0003\u001a\u00020\u0001*\u00020\u0001\u001a\n\u0010\u0004\u001a\u00020\u0001*\u00020\u0001\u001a\n\u0010\u0005\u001a\u00020\u0006*\u00020\u0006\u001a\u0012\u0010\u0007\u001a\u00020\u0006*\u00020\u00062\u0006\u0010\b\u001a\u00020\u0006¨\u0006\t" }, d2 = { "KMPHToMetersPerSecond", "", "MPHToMetersPerSecond", "MetersPerSecondToKMPH", "MetersPerSecondToMPH", "milliSecondsToSeconds", "", "timeSince", "time", "app_hudRelease" }, k = 2, mv = { 1, 1, 6 })
public final class TelemetrySessionKt
{
    public static final float KMPHToMetersPerSecond(final float n) {
        return 0.277778f * n;
    }
    
    public static final float MPHToMetersPerSecond(final float n) {
        return 0.44704f * n;
    }
    
    public static final float MetersPerSecondToKMPH(final float n) {
        return 3.6f * n;
    }
    
    public static final float MetersPerSecondToMPH(final float n) {
        return 2.23694f * n;
    }
    
    public static final long milliSecondsToSeconds(final long n) {
        return n / 1000;
    }
    
    public static final long timeSince(final long n, final long n2) {
        return n - n2;
    }
}
