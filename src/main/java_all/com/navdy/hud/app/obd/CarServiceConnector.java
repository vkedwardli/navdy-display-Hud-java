package com.navdy.hud.app.obd;

import android.os.IBinder;
import android.content.ComponentName;
import android.content.Intent;
import android.content.Context;
import com.navdy.obd.ICarService;
import com.navdy.hud.app.common.ServiceReconnector;

public class CarServiceConnector extends ServiceReconnector
{
    private ICarService carApi;
    
    public CarServiceConnector(final Context context) {
        super(context, new Intent("com.navdy.obd.action.START_AUTO_CONNECT"), ICarService.class.getName());
    }
    
    public ICarService getCarApi() {
        return this.carApi;
    }
    
    @Override
    protected void onConnected(final ComponentName componentName, final IBinder binder) {
        this.carApi = ICarService.Stub.asInterface(binder);
        ObdManager.getInstance().serviceConnected();
    }
    
    @Override
    protected void onDisconnected(final ComponentName componentName) {
        this.carApi = null;
        ObdManager.getInstance().serviceDisconnected();
    }
}
