package com.navdy.hud.app.obd;

import java.io.InputStream;
import com.navdy.service.library.util.IOUtils;
import com.navdy.hud.app.HudApplication;
import java.io.IOException;
import java.util.regex.Pattern;
import com.navdy.service.library.task.TaskManager;
import com.navdy.obd.ICarService;
import android.os.RemoteException;
import android.text.TextUtils;
import com.navdy.service.library.log.Logger;

public class ObdDeviceConfigurationManager
{
    public static final String ASSETS_FOLDER_PREFIX = "stn_obd";
    public static final String CAR_CONFIGURATION_MAPPING_FILE = "configuration_mapping.csv";
    private static Configuration[] CAR_DETAILS_CONFIGURATION_MAPPING;
    public static final String CONFIGURATION_FILE_EXTENSION = "conf";
    public static final String CONFIGURATION_NAME_PREFIX = "Navdy OBD-II ";
    public static final String DEBUG_CONFIGURATION = "debug";
    public static final String DEFAULT_OFF_CONFIGURATION = "no_trigger";
    public static final String DEFAULT_ON_CONFIGURATION = "default_on";
    public static final String STSATI = "STSATI";
    private static Configuration[] VIN_CONFIGURATION_MAPPING;
    public static final String VIN_CONFIGURATION_MAPPING_FILE = "vin_configuration_mapping.csv";
    private static final Logger sLogger;
    private CarServiceConnector mCarServiceConnector;
    private volatile boolean mConnected;
    private String mExpectedConfigurationFileName;
    private volatile boolean mIsAutoOnEnabled;
    private volatile boolean mIsVinRecognized;
    private volatile String mLastKnowVinNumber;
    
    static {
        sLogger = new Logger(ObdDeviceConfigurationManager.class);
    }
    
    public ObdDeviceConfigurationManager(final CarServiceConnector mCarServiceConnector) {
        this.mIsAutoOnEnabled = true;
        this.mExpectedConfigurationFileName = null;
        this.mCarServiceConnector = mCarServiceConnector;
    }
    
    private void applyConfiguration(final OBDConfiguration obdConfiguration) {
        if (obdConfiguration == null || !this.mConnected) {
            return;
        }
        final ICarService carApi = this.mCarServiceConnector.getCarApi();
        if (TextUtils.isEmpty((CharSequence)obdConfiguration.configurationData) || TextUtils.isEmpty((CharSequence)obdConfiguration.configurationIdentifier)) {
            return;
        }
        ObdDeviceConfigurationManager.sLogger.d("Writing new configuration :" + obdConfiguration);
        try {
            ObdDeviceConfigurationManager.sLogger.d("New Configuration applied : " + carApi.applyConfiguration(obdConfiguration.configurationData) + ", configuration : " + carApi.getCurrentConfigurationName());
        }
        catch (RemoteException ex) {
            ObdDeviceConfigurationManager.sLogger.e("Failed to apply the configuration :", ex.getCause());
        }
    }
    
    private void bSetConfigurationFile(final String mExpectedConfigurationFileName) {
        if (!this.validateConfigurationName(mExpectedConfigurationFileName)) {
            ObdDeviceConfigurationManager.sLogger.d("Received an invalid configuration name : " + mExpectedConfigurationFileName);
            throw new IllegalArgumentException("Configuration does not exists");
        }
        this.mExpectedConfigurationFileName = mExpectedConfigurationFileName;
        this.syncConfiguration();
    }
    
    private void decodeVin() {
        TaskManager.getInstance().execute(new Runnable() {
            @Override
            public void run() {
                if (ObdDeviceConfigurationManager.isValidVin(ObdDeviceConfigurationManager.this.mLastKnowVinNumber) && ObdDeviceConfigurationManager.this.mIsAutoOnEnabled) {
                    if (ObdDeviceConfigurationManager.VIN_CONFIGURATION_MAPPING == null) {
                        ObdDeviceConfigurationManager.VIN_CONFIGURATION_MAPPING = ObdDeviceConfigurationManager.this.loadConfigurationMappingList("vin_configuration_mapping.csv");
                    }
                    final Configuration pickConfiguration = ObdDeviceConfigurationManager.pickConfiguration(ObdDeviceConfigurationManager.VIN_CONFIGURATION_MAPPING, ObdDeviceConfigurationManager.this.mLastKnowVinNumber);
                    if (pickConfiguration != null) {
                        ObdDeviceConfigurationManager.sLogger.d("decodeVin : got the matching configuration :" + pickConfiguration.configurationName + ", Vin :" + ObdDeviceConfigurationManager.this.mLastKnowVinNumber);
                        ObdDeviceConfigurationManager.this.mIsVinRecognized = true;
                        ObdDeviceConfigurationManager.this.bSetConfigurationFile(pickConfiguration.configurationName);
                    }
                    else {
                        ObdDeviceConfigurationManager.this.mIsVinRecognized = false;
                    }
                }
            }
        }, 13);
    }
    
    static OBDConfiguration extractOBDConfiguration(String s) {
        final StringBuilder sb = new StringBuilder();
        final String[] split = s.split("\n");
        s = null;
        String trim;
        for (int length = split.length, i = 0; i < length; ++i, s = trim) {
            final String s2 = split[i];
            if (!s2.startsWith("ST")) {
                trim = s;
                if (!s2.startsWith("AT")) {
                    continue;
                }
            }
            sb.append(s2).append("\n");
            trim = s;
            if (s2.startsWith("STSATI")) {
                trim = s2.substring("STSATI".length()).trim();
            }
        }
        return new OBDConfiguration(sb.toString().trim(), s);
    }
    
    public static boolean isValidVin(final String s) {
        return !TextUtils.isEmpty((CharSequence)s) && s.length() == 17;
    }
    
    private Configuration[] loadConfigurationMappingList(final String s) {
        int n = 0;
        Configuration[] array = null;
        try {
            final String[] split = this.readObdAssetFile(s).split("\n");
            array = array;
            final Configuration[] array2 = array = new Configuration[split.length];
            final int length = split.length;
            int n2 = 0;
            while (true) {
                array = array2;
                if (n >= length) {
                    break;
                }
                array = array2;
                final String[] split2 = split[n].split(",");
                array = array2;
                array = array2;
                final Configuration configuration = new Configuration();
                array = array2;
                configuration.pattern = Pattern.compile(split2[0], 2);
                array = array2;
                configuration.configurationName = split2[1];
                array2[n2] = configuration;
                ++n;
                ++n2;
            }
        }
        catch (IOException ex) {
            ObdDeviceConfigurationManager.sLogger.d("Failed to load the configuration mapping file");
        }
        return array;
    }
    
    public static Configuration pickConfiguration(final Configuration[] array, final String s) {
        final Configuration configuration = null;
        final int length = array.length;
        int n = 0;
        Configuration configuration2;
        while (true) {
            configuration2 = configuration;
            if (n >= length) {
                break;
            }
            configuration2 = array[n];
            if (configuration2.pattern.matcher(s).matches()) {
                break;
            }
            ++n;
        }
        return configuration2;
    }
    
    private OBDConfiguration readConfiguration(String obdConfiguration) {
        try {
            obdConfiguration = extractOBDConfiguration(this.readObdAssetFile((String)obdConfiguration + "." + "conf"));
            return (OBDConfiguration)obdConfiguration;
        }
        catch (IOException ex) {
            ObdDeviceConfigurationManager.sLogger.e("Error reading the configuration file :" + (String)obdConfiguration);
            obdConfiguration = null;
            return (OBDConfiguration)obdConfiguration;
        }
    }
    
    private String readObdAssetFile(String convertInputStreamToString) throws IOException {
        final InputStream open = HudApplication.getAppContext().getAssets().open("stn_obd/" + convertInputStreamToString);
        convertInputStreamToString = IOUtils.convertInputStreamToString(open, "UTF-8");
        try {
            open.close();
            return convertInputStreamToString;
        }
        catch (IOException ex) {
            ObdDeviceConfigurationManager.sLogger.d("Error closing the stream after reading the asset file");
            return convertInputStreamToString;
        }
    }
    
    private void syncConfiguration() {
        if (this.mConnected && !TextUtils.isEmpty((CharSequence)this.mExpectedConfigurationFileName)) {
            final ICarService carApi = this.mCarServiceConnector.getCarApi();
            final OBDConfiguration configuration = this.readConfiguration(this.mExpectedConfigurationFileName);
            try {
                final String currentConfigurationName = carApi.getCurrentConfigurationName();
                final String configurationIdentifier = configuration.configurationIdentifier;
                if (configurationIdentifier.equals(currentConfigurationName)) {
                    ObdDeviceConfigurationManager.sLogger.d("The configuration on the obd chip is as expected");
                }
                else {
                    ObdDeviceConfigurationManager.sLogger.d("The configuration on the obd chip is different");
                    ObdDeviceConfigurationManager.sLogger.d("Expected : " + configurationIdentifier);
                    ObdDeviceConfigurationManager.sLogger.d("Actual : " + currentConfigurationName);
                    ObdDeviceConfigurationManager.sLogger.d("Syncing configuration...");
                    this.applyConfiguration(configuration);
                }
            }
            catch (RemoteException ex) {
                ObdDeviceConfigurationManager.sLogger.e("Error while applying configuration", (Throwable)ex);
            }
        }
    }
    
    public static void turnOffOBDSleep() {
        ObdManager.getInstance().getObdDeviceConfigurationManager().setConfigurationFile("debug");
    }
    
    private boolean validateConfigurationName(final String p0) {
        // 
        This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: istore_2       
        //     2: aload_1        
        //     3: invokestatic    android/text/TextUtils.isEmpty:(Ljava/lang/CharSequence;)Z
        //     6: ifeq            11
        //     9: iload_2        
        //    10: ireturn        
        //    11: invokestatic    com/navdy/hud/app/HudApplication.getAppContext:()Landroid/content/Context;
        //    14: invokevirtual   android/content/Context.getAssets:()Landroid/content/res/AssetManager;
        //    17: astore_3       
        //    18: new             Ljava/lang/StringBuilder;
        //    21: astore          4
        //    23: aload           4
        //    25: invokespecial   java/lang/StringBuilder.<init>:()V
        //    28: aload_3        
        //    29: aload           4
        //    31: ldc_w           "stn_obd/"
        //    34: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    37: aload_1        
        //    38: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    41: ldc_w           "."
        //    44: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    47: ldc             "conf"
        //    49: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    52: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //    55: invokevirtual   android/content/res/AssetManager.open:(Ljava/lang/String;)Ljava/io/InputStream;
        //    58: astore_1       
        //    59: aload_1        
        //    60: ifnull          9
        //    63: aload_1        
        //    64: invokevirtual   java/io/InputStream.close:()V
        //    67: iconst_1       
        //    68: istore_2       
        //    69: goto            9
        //    72: astore_1       
        //    73: goto            9
        //    76: astore_1       
        //    77: goto            67
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  11     59     72     76     Ljava/io/IOException;
        //  63     67     76     80     Ljava/io/IOException;
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0067:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public boolean isAutoOnEnabled() {
        return this.mIsAutoOnEnabled;
    }
    
    public void setAutoOnEnabled(final boolean mIsAutoOnEnabled) {
        if (!(this.mIsAutoOnEnabled = mIsAutoOnEnabled)) {
            this.setConfigurationFile("no_trigger");
        }
    }
    
    public void setCarDetails(final String s, final String s2, final String s3) {
        TaskManager.getInstance().execute(new Runnable() {
            @Override
            public void run() {
                if (!ObdDeviceConfigurationManager.this.mIsAutoOnEnabled) {
                    ObdDeviceConfigurationManager.sLogger.d("setCarDetails: Auto on is de selected by the user , so skipping the overwriting of configuration");
                }
                else if (ObdDeviceConfigurationManager.this.mIsVinRecognized) {
                    ObdDeviceConfigurationManager.sLogger.d("setCarDetails: Vin is recognized and it is used as the preferred source");
                }
                else {
                    final StringBuilder sb = new StringBuilder();
                    String val$make;
                    if (TextUtils.isEmpty((CharSequence)s)) {
                        val$make = "*";
                    }
                    else {
                        val$make = s;
                    }
                    final StringBuilder append = sb.append(val$make).append("_");
                    String val$model;
                    if (TextUtils.isEmpty((CharSequence)s2)) {
                        val$model = "*";
                    }
                    else {
                        val$model = s2;
                    }
                    final StringBuilder append2 = append.append(val$model).append("_");
                    String val$year;
                    if (TextUtils.isEmpty((CharSequence)s3)) {
                        val$year = "*";
                    }
                    else {
                        val$year = s3;
                    }
                    final String string = append2.append(val$year).toString();
                    ObdDeviceConfigurationManager.sLogger.d("Vehicle name :" + string);
                    if (ObdDeviceConfigurationManager.CAR_DETAILS_CONFIGURATION_MAPPING == null) {
                        ObdDeviceConfigurationManager.CAR_DETAILS_CONFIGURATION_MAPPING = ObdDeviceConfigurationManager.this.loadConfigurationMappingList("configuration_mapping.csv");
                    }
                    final Configuration pickConfiguration = ObdDeviceConfigurationManager.pickConfiguration(ObdDeviceConfigurationManager.CAR_DETAILS_CONFIGURATION_MAPPING, string);
                    if (pickConfiguration != null) {
                        ObdDeviceConfigurationManager.this.bSetConfigurationFile(pickConfiguration.configurationName);
                    }
                }
            }
        }, 13);
    }
    
    public void setConfigurationFile(final String s) {
        TaskManager.getInstance().execute(new Runnable() {
            @Override
            public void run() {
                ObdDeviceConfigurationManager.this.bSetConfigurationFile(s);
            }
        }, 13);
    }
    
    public void setConnectionState(final boolean mConnected) {
        Label_0098: {
            if (mConnected == this.mConnected) {
                break Label_0098;
            }
            this.mConnected = mConnected;
            ObdDeviceConfigurationManager.sLogger.d("OBD Connection state changed");
            if (!this.mConnected) {
                return;
            }
            final ICarService carApi = this.mCarServiceConnector.getCarApi();
            try {
                this.mLastKnowVinNumber = carApi.getVIN();
                ObdDeviceConfigurationManager.sLogger.d("Vin number read :" + this.mLastKnowVinNumber);
                this.decodeVin();
                return;
            }
            catch (RemoteException ex) {
                ObdDeviceConfigurationManager.sLogger.e("Remote exception while getting the VIN number ");
                return;
            }
        }
        this.mLastKnowVinNumber = "";
        this.mIsAutoOnEnabled = true;
        this.mIsVinRecognized = false;
    }
    
    public void setDecodedCarDetails(final CarDetails carDetails) {
    }
    
    static class Configuration
    {
        String configurationName;
        Pattern pattern;
    }
    
    static class OBDConfiguration
    {
        String configurationData;
        String configurationIdentifier;
        
        public OBDConfiguration(final String configurationData, final String configurationIdentifier) {
            this.configurationData = configurationData;
            this.configurationIdentifier = configurationIdentifier;
        }
    }
}
