package com.navdy.hud.app.view;

import com.navdy.service.library.events.input.Gesture;
import butterknife.ButterKnife;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.FrameLayout.LayoutParams;
import android.view.View;
import com.navdy.hud.app.analytics.AnalyticsSupport;
import mortar.Mortar;
import com.navdy.service.library.events.input.GestureEvent;
import android.util.AttributeSet;
import android.content.Context;
import javax.inject.Inject;
import com.navdy.hud.app.presenter.NotificationPresenter;
import com.navdy.hud.app.framework.notifications.NotificationManager;
import com.navdy.hud.app.framework.notifications.INotification;
import com.navdy.hud.app.ui.component.image.ColorImageView;
import android.widget.FrameLayout;
import com.navdy.hud.app.gesture.MultipleClickGestureDetector;
import butterknife.InjectView;
import com.navdy.hud.app.ui.component.ShrinkingBorderView;
import com.navdy.service.library.log.Logger;
import com.navdy.hud.app.gesture.GestureDetector;
import com.navdy.hud.app.manager.InputManager;
import android.widget.RelativeLayout;

public class NotificationView extends RelativeLayout implements IInputHandler, GestureListener
{
    private static final int CLICK_COUNT = 2;
    private static final Logger sLogger;
    @InjectView(R.id.border)
    public ShrinkingBorderView border;
    private ShrinkingBorderView.IListener borderListener;
    private MultipleClickGestureDetector clickGestureDetector;
    private MultipleClickGestureDetector.IMultipleClickKeyGesture clickGestureListener;
    @InjectView(R.id.customNotificationContainer)
    FrameLayout customNotificationContainer;
    private int defaultColor;
    private boolean initialized;
    @InjectView(R.id.nextNotificationColorView)
    public ColorImageView nextNotificationColorView;
    private INotification notification;
    private NotificationManager notificationManager;
    @Inject
    NotificationPresenter presenter;
    
    static {
        sLogger = new Logger(NotificationView.class);
    }
    
    public NotificationView(final Context context) {
        this(context, null);
    }
    
    public NotificationView(final Context context, final AttributeSet set) {
        super(context, set);
        this.clickGestureListener = new MultipleClickGestureDetector.IMultipleClickKeyGesture() {
            @Override
            public IInputHandler nextHandler() {
                return null;
            }
            
            @Override
            public boolean onGesture(final GestureEvent gestureEvent) {
                return false;
            }
            
            @Override
            public boolean onKey(final CustomKeyEvent customKeyEvent) {
                boolean b = false;
                if (NotificationView.this.notification != null) {
                    if (NotificationView.this.notificationManager.isExpanded() || NotificationView.this.notificationManager.isExpandedNotificationVisible()) {
                        b = NotificationView.this.notificationManager.handleKey(customKeyEvent);
                    }
                    else {
                        b = ((InputManager.IInputHandler)NotificationView.this.notification).onKey(customKeyEvent);
                    }
                }
                return b;
            }
            
            @Override
            public void onMultipleClick(final int n) {
                NotificationView.this.takeNotificationAction(false);
            }
        };
        this.borderListener = new ShrinkingBorderView.IListener() {
            @Override
            public void timeout() {
                if (NotificationView.this.initialized) {
                    NotificationManager.getInstance().currentNotificationTimeout();
                }
            }
        };
        if (!this.isInEditMode()) {
            Mortar.inject(context, this);
            this.notificationManager = NotificationManager.getInstance();
            this.clickGestureDetector = new MultipleClickGestureDetector(2, this.clickGestureListener);
        }
    }
    
    private void takeNotificationAction(boolean b) {
        if (this.notification != null) {
            if (this.notificationManager.isExpanded() || this.notificationManager.isExpandedNotificationVisible()) {
                boolean b2 = false;
                if (this.notificationManager.isCurrentItemDeleteAll()) {
                    String s;
                    if (b) {
                        s = "swipe";
                    }
                    else {
                        s = "doubleclick";
                    }
                    AnalyticsSupport.recordGlanceAction("Glance_Dismiss", null, s);
                    b2 = true;
                    b = true;
                }
                else {
                    final INotification notification = this.notification;
                    String s2;
                    if (b) {
                        s2 = "swipe";
                    }
                    else {
                        s2 = "doubleclick";
                    }
                    AnalyticsSupport.recordGlanceAction("Glance_Open_Mini", notification, s2);
                }
                this.notificationManager.collapseExpandedNotification(b2, b);
            }
            else {
                final INotification notification2 = this.notification;
                String s3;
                if (b) {
                    s3 = "swipe";
                }
                else {
                    s3 = "doubleclick";
                }
                AnalyticsSupport.recordGlanceAction("Glance_Dismiss", notification2, s3);
                this.notificationManager.collapseNotification();
            }
        }
    }
    
    public void addCustomView(final INotification notification, final View view) {
        this.removeCustomView();
        this.customNotificationContainer.addView(view, (ViewGroup.LayoutParams)new FrameLayout.LayoutParams(-1, -1));
        this.notification = notification;
        this.initialized = true;
    }
    
    public void clearNotification() {
        this.customNotificationContainer.removeAllViews();
    }
    
    public View getCurrentNotificationViewChild() {
        View child;
        if (this.customNotificationContainer.getChildCount() > 0) {
            child = this.customNotificationContainer.getChildAt(0);
        }
        else {
            child = null;
        }
        return child;
    }
    
    public ViewGroup getNotificationContainer() {
        return (ViewGroup)this.customNotificationContainer;
    }
    
    public void hideNextNotificationColor() {
        this.nextNotificationColorView.setVisibility(INVISIBLE);
    }
    
    public IInputHandler nextHandler() {
        return null;
    }
    
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.defaultColor = this.getContext().getResources().getColor(R.color.black);
        if (this.presenter != null) {
            this.presenter.takeView(this);
        }
    }
    
    public void onClick() {
    }
    
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (this.presenter != null) {
            this.presenter.dropView(this);
        }
    }
    
    protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.inject((View)this);
        this.border.setListener(this.borderListener);
        this.resetNextNotificationColor();
    }
    
    public boolean onGesture(final GestureEvent gestureEvent) {
        final boolean b = false;
        boolean b2;
        if (!this.initialized) {
            b2 = b;
        }
        else {
            b2 = b;
            if (this.notification != null) {
                b2 = b;
                if (gestureEvent != null) {
                    b2 = b;
                    if (gestureEvent.gesture != null) {
                        if (this.notificationManager.isAnimating()) {
                            NotificationView.sLogger.v("ignore gesture, animation in progress");
                            b2 = true;
                        }
                        else {
                            switch (gestureEvent.gesture) {
                                case GESTURE_SWIPE_LEFT:
                                    if (!this.notificationManager.isExpanded() && !this.notificationManager.isExpandedNotificationVisible()) {
                                        if (((InputManager.IInputHandler)this.notification).onGesture(gestureEvent)) {
                                            NotificationView.sLogger.v("gesture handled by notif:" + this.notification.getType());
                                            break;
                                        }
                                        AnalyticsSupport.recordGlanceAction("Glance_Open_Full", this.notification, "swipe");
                                        NotificationView.sLogger.v("gesture expanded:" + this.notification.expandNotification());
                                        break;
                                    }
                                    else {
                                        if (this.notificationManager.isExpanded() || this.notificationManager.isExpandedNotificationVisible()) {
                                            this.notificationManager.moveNext(true);
                                            break;
                                        }
                                        break;
                                    }
                                    break;
                                case GESTURE_SWIPE_RIGHT: {
                                    boolean onGesture;
                                    final boolean b3 = onGesture = false;
                                    if (this.notification != null) {
                                        onGesture = b3;
                                        if (!this.notificationManager.isExpanded()) {
                                            onGesture = b3;
                                            if (!this.notificationManager.isExpandedNotificationVisible()) {
                                                onGesture = ((InputManager.IInputHandler)this.notification).onGesture(gestureEvent);
                                            }
                                        }
                                    }
                                    if (!onGesture) {
                                        this.takeNotificationAction(true);
                                        break;
                                    }
                                    NotificationView.sLogger.v("gesture handled by notif:" + this.notification.getType());
                                    break;
                                }
                            }
                            b2 = true;
                        }
                    }
                }
            }
        }
        return b2;
    }
    
    public boolean onKey(final CustomKeyEvent customKeyEvent) {
        return this.initialized && this.clickGestureDetector.onKey(customKeyEvent);
    }
    
    public void onTrackHand(final float n) {
    }
    
    public void removeCustomView() {
        this.customNotificationContainer.removeAllViews();
        this.border.stopTimeout(false, null);
        this.initialized = false;
    }
    
    public void resetNextNotificationColor() {
        this.nextNotificationColorView.setColor(this.defaultColor);
    }
    
    public void setNextNotificationColor(final int color) {
        this.nextNotificationColorView.setColor(color);
    }
    
    public void showNextNotificationColor() {
        this.nextNotificationColorView.setVisibility(View.VISIBLE);
    }
    
    public void switchNotfication(final INotification notification) {
        this.notification = notification;
    }
}
