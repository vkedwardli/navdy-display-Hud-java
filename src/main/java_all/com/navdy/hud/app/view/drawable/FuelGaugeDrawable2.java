package com.navdy.hud.app.view.drawable;

import android.graphics.Rect;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Canvas;
import android.content.Context;

public class FuelGaugeDrawable2 extends GaugeDrawable
{
    private static final int TICKS_COUNT = 4;
    private static final int TICK_ANGLE_WIDTH = 2;
    private int mBackgroundColor;
    private int mFuelGaugeWidth;
    private boolean mLeftOriented;
    
    public FuelGaugeDrawable2(final Context context, final int n) {
        super(context, 0, n);
        this.mLeftOriented = true;
        this.mFuelGaugeWidth = context.getResources().getDimensionPixelSize(R.dimen.fuel_gauge_width);
        this.mBackgroundColor = this.mColorTable[3];
    }
    
    @Override
    public void draw(final Canvas canvas) {
        super.draw(canvas);
        final Rect bounds = this.getBounds();
        RectF rectF;
        if (this.mLeftOriented) {
            rectF = new RectF((float)bounds.left, (float)bounds.top, (float)(bounds.right + bounds.width()), (float)bounds.bottom);
        }
        else {
            rectF = new RectF((float)(bounds.left - bounds.width()), (float)bounds.top, (float)bounds.right, (float)bounds.bottom);
        }
        rectF.inset((float)(this.mFuelGaugeWidth / 2 + 1), (float)(this.mFuelGaugeWidth / 2 + 1));
        this.mPaint.setStyle(Paint$Style.STROKE);
        this.mPaint.setStrokeWidth((float)this.mFuelGaugeWidth);
        this.mPaint.setColor(this.mBackgroundColor);
        int n;
        if (this.mLeftOriented) {
            n = 90;
        }
        else {
            n = 270;
        }
        canvas.drawArc(rectF, (float)n, (float)180, false, this.mPaint);
        this.mPaint.setColor(this.mDefaultColor);
        final int n2 = (int)(this.mValue / this.mMaxValue * 180);
        int n3;
        if (this.mLeftOriented) {
            n3 = 90;
        }
        else {
            n3 = 450 - n2;
        }
        canvas.drawArc(rectF, (float)n3, (float)n2, false, this.mPaint);
        this.mPaint.setColor(-16777216);
        for (int i = 1; i < 4; ++i) {
            canvas.drawArc(rectF, (float)((i * 45 + n) % 360), 2.0f, false, this.mPaint);
        }
    }
    
    public void setLeftOriented(final boolean mLeftOriented) {
        this.mLeftOriented = mLeftOriented;
    }
}
