package com.navdy.hud.app.view;

import com.navdy.service.library.events.input.GestureEvent;
import android.view.View;
import butterknife.ButterKnife;
import com.navdy.hud.app.framework.toast.ToastManager;
import mortar.Mortar;
import android.util.AttributeSet;
import android.content.Context;
import javax.inject.Inject;
import com.navdy.hud.app.screen.FirstLaunchScreen;
import butterknife.InjectView;
import android.widget.ImageView;
import com.navdy.service.library.log.Logger;
import com.navdy.hud.app.manager.InputManager;
import android.widget.RelativeLayout;

public class FirstLaunchView extends RelativeLayout implements IInputHandler
{
    private static final Logger sLogger;
    @InjectView(R.id.firstLaunchLogo)
    public ImageView firstLaunchLogo;
    @Inject
    FirstLaunchScreen.Presenter presenter;
    
    static {
        sLogger = new Logger(FirstLaunchView.class);
    }
    
    public FirstLaunchView(final Context context) {
        this(context, null);
    }
    
    public FirstLaunchView(final Context context, final AttributeSet set) {
        this(context, set, 0);
    }
    
    public FirstLaunchView(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
        if (!this.isInEditMode()) {
            Mortar.inject(context, this);
        }
    }
    
    public IInputHandler nextHandler() {
        return null;
    }
    
    protected void onAttachedToWindow() {
        FirstLaunchView.sLogger.v("onAttachToWindow");
        super.onAttachedToWindow();
        ToastManager.getInstance().disableToasts(true);
        if (this.presenter != null) {
            this.presenter.takeView(this);
        }
    }
    
    protected void onDetachedFromWindow() {
        FirstLaunchView.sLogger.v("onDetachToWindow");
        ToastManager.getInstance().disableToasts(false);
        super.onDetachedFromWindow();
        if (this.presenter != null) {
            this.presenter.dropView(this);
        }
    }
    
    protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.inject((View)this);
    }
    
    public boolean onGesture(final GestureEvent gestureEvent) {
        return true;
    }
    
    public boolean onKey(final CustomKeyEvent customKeyEvent) {
        return true;
    }
}
