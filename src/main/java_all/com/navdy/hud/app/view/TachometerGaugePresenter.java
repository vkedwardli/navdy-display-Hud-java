package com.navdy.hud.app.view;

import android.util.TypedValue;
import android.os.Build;
import android.graphics.drawable.Drawable;
import android.content.res.Resources;
import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.content.Context;
import android.animation.ValueAnimator;
import com.navdy.hud.app.manager.SpeedManager;
import com.navdy.hud.app.view.drawable.TachometerGaugeDrawable;
import android.os.Handler;

public class TachometerGaugePresenter extends GaugeViewPresenter implements SerialValueAnimatorAdapter
{
    public static final String ID = "TACHOMETER_GAUGE_IDENTIFIER";
    private static final int MAX_MARKER_ANIMATION_DELAY = 500;
    private static final int MAX_MARKER_ANIMATION_DURATION = 1000;
    private static final int MAX_RPM_VALUE = 8000;
    private static final int RPM_DIFFERENCE = 100;
    private static final int RPM_WARNING_THRESHOLD = 7000;
    public static final int VALUE_ANIMATION_DURATION = 200;
    int animatedRpm;
    private Runnable animationRunnable;
    private boolean fullMode;
    private Handler handler;
    private final String kmhString;
    private int lastRPM;
    TachometerGaugeDrawable mTachometerGaugeDrawable;
    private int maxMarkerAlpha;
    private int maxRpmValue;
    private final String mphString;
    int rpm;
    private SerialValueAnimator serialValueAnimator;
    private boolean showingMaxMarkerAnimation;
    int speed;
    private int speedFastWarningColor;
    int speedLimit;
    private SpeedManager speedManager;
    private int speedSafeColor;
    private int speedVeryFastWarningColor;
    private ValueAnimator valueAnimator;
    private String widgetName;
    
    public TachometerGaugePresenter(final Context context) {
        this.speedManager = SpeedManager.getInstance();
        this.lastRPM = Integer.MIN_VALUE;
        this.handler = new Handler();
        this.serialValueAnimator = new SerialValueAnimator((SerialValueAnimator.SerialValueAnimatorAdapter)this, 200);
        (this.mTachometerGaugeDrawable = new TachometerGaugeDrawable(context, 0)).setMaxGaugeValue(8000.0f);
        this.mTachometerGaugeDrawable.setRPMWarningLevel(7000);
        this.speedSafeColor = context.getResources().getColor(R.color.hud_white);
        this.speedFastWarningColor = context.getResources().getColor(R.color.cyan);
        this.speedVeryFastWarningColor = context.getResources().getColor(R.color.speed_very_fast_warning_color);
        this.widgetName = context.getResources().getString(R.string.gauge_tacho_meter);
        final Resources resources = context.getResources();
        this.mphString = resources.getString(R.string.mph);
        this.kmhString = resources.getString(R.string.kilometers_per_hour);
        this.animationRunnable = new Runnable() {
            @Override
            public void run() {
                TachometerGaugePresenter.this.valueAnimator = new ValueAnimator();
                TachometerGaugePresenter.this.valueAnimator.setIntValues(new int[] { 255, 0 });
                TachometerGaugePresenter.this.valueAnimator.setDuration(1000L);
                TachometerGaugePresenter.this.valueAnimator.addUpdateListener((ValueAnimator.AnimatorUpdateListener)new ValueAnimator.AnimatorUpdateListener() {
                    public void onAnimationUpdate(final ValueAnimator valueAnimator) {
                        TachometerGaugePresenter.this.maxMarkerAlpha = (int)valueAnimator.getAnimatedValue();
                        TachometerGaugePresenter.this.reDraw();
                    }
                });
                TachometerGaugePresenter.this.valueAnimator.addListener((Animator.AnimatorListener)new Animator.AnimatorListener() {
                    public void onAnimationCancel(final Animator animator) {
                    }
                    
                    public void onAnimationEnd(final Animator animator) {
                        TachometerGaugePresenter.this.maxMarkerAlpha = 0;
                        TachometerGaugePresenter.this.showingMaxMarkerAnimation = false;
                        TachometerGaugePresenter.this.maxRpmValue = 0;
                        TachometerGaugePresenter.this.reDraw();
                    }
                    
                    public void onAnimationRepeat(final Animator animator) {
                    }
                    
                    public void onAnimationStart(final Animator animator) {
                    }
                });
                TachometerGaugePresenter.this.valueAnimator.start();
            }
        };
    }
    
    @Override
    public void animationComplete(final float n) {
    }
    
    @Override
    public Drawable getDrawable() {
        return this.mTachometerGaugeDrawable;
    }
    
    @Override
    public float getValue() {
        return this.rpm;
    }
    
    @Override
    public String getWidgetIdentifier() {
        return "TACHOMETER_GAUGE_IDENTIFIER";
    }
    
    @Override
    public String getWidgetName() {
        return null;
    }
    
    public void setRPM(final int lastRPM) {
        if (this.isDashActive && this.lastRPM != lastRPM && Math.abs(this.lastRPM - lastRPM) >= 100) {
            this.lastRPM = lastRPM;
            this.serialValueAnimator.setValue(lastRPM);
        }
    }
    
    public void setSpeed(final int speed) {
        if (speed != -1 && this.speed != speed) {
            this.speed = speed;
            if (this.isDashActive) {
                this.reDraw();
            }
        }
    }
    
    public void setSpeedLimit(final int speedLimit) {
        if (this.speedLimit != speedLimit) {
            this.speedLimit = speedLimit;
            if (this.isDashActive) {
                this.reDraw();
            }
        }
    }
    
    @Override
    public void setValue(final float n) {
        this.rpm = (int)n;
        if (this.rpm > this.maxRpmValue) {
            this.maxRpmValue = Math.min(8000, this.rpm);
            this.handler.removeCallbacks(this.animationRunnable);
            this.showingMaxMarkerAnimation = false;
            this.maxMarkerAlpha = 0;
            if (this.valueAnimator != null && this.valueAnimator.isRunning()) {
                this.valueAnimator.cancel();
            }
        }
        if (this.rpm < this.maxRpmValue && !this.showingMaxMarkerAnimation) {
            this.maxMarkerAlpha = 255;
            this.showingMaxMarkerAnimation = true;
            this.handler.removeCallbacks(this.animationRunnable);
            this.handler.postDelayed(this.animationRunnable, 500L);
        }
        this.reDraw();
    }
    
    @Override
    public void setView(final DashboardWidgetView view) {
        if (view != null) {
            view.setContentView(R.layout.tachometer_gauge);
        }
        super.setView(view);
        this.mGaugeView = (GaugeView)view;
        if (this.mGaugeView != null && Build$VERSION.SDK_INT >= 21) {
            final TypedValue typedValue = new TypedValue();
            this.mGaugeView.getResources().getValue(R.dimen.speedometer_text_letter_spacing, typedValue, true);
            this.mGaugeView.getValueTextView().setLetterSpacing(typedValue.getFloat());
        }
    }
    
    @Override
    protected void updateGauge() {
        if (this.mGaugeView != null) {
            this.mTachometerGaugeDrawable.setGaugeValue(this.rpm);
            this.mTachometerGaugeDrawable.setMaxMarkerValue(this.maxRpmValue);
            this.mTachometerGaugeDrawable.setMaxMarkerAlpha(this.maxMarkerAlpha);
            this.mGaugeView.setValueText(Integer.toString(this.speed));
            String s = null;
            int n = 0;
            switch (this.speedManager.getSpeedUnit()) {
                default:
                    s = this.mphString;
                    n = 8;
                    break;
                case KILOMETERS_PER_HOUR:
                    s = this.kmhString;
                    n = 13;
                    break;
            }
            if (this.speedLimit > 0) {
                if (this.speed >= this.speedLimit + n) {
                    this.mGaugeView.setUnitText(this.mGaugeView.getResources().getString(R.string.speed_limit_with_unit, new Object[] { this.speedLimit, s }));
                    this.mGaugeView.getUnitTextView().setTextColor(this.speedVeryFastWarningColor);
                }
                else if (this.speed >= this.speedLimit) {
                    this.mGaugeView.setUnitText(this.mGaugeView.getResources().getString(R.string.speed_limit_with_unit, new Object[] { this.speedLimit, s }));
                    this.mGaugeView.getUnitTextView().setTextColor(this.speedFastWarningColor);
                }
                else {
                    this.mGaugeView.getUnitTextView().setTextColor(this.speedSafeColor);
                    this.mGaugeView.setUnitText(s);
                }
            }
            else {
                this.mGaugeView.setUnitText(s);
                this.mGaugeView.setValueText(Integer.toString(this.speed));
                this.mGaugeView.getUnitTextView().setTextColor(this.speedSafeColor);
            }
        }
    }
}
