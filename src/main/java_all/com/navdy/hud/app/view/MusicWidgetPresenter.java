package com.navdy.hud.app.view;

import android.os.Bundle;
import java.util.Set;
import com.navdy.service.library.util.ScalingUtilities;
import com.navdy.service.library.task.TaskManager;
import android.text.TextUtils;
import android.support.annotation.Nullable;
import okio.ByteString;
import android.graphics.drawable.Drawable;
import mortar.Mortar;
import com.navdy.hud.app.HudApplication;
import android.content.Context;
import com.navdy.service.library.events.audio.MusicTrackInfo;
import javax.inject.Inject;
import com.navdy.hud.app.framework.music.AlbumArtImageView;
import android.os.Handler;
import com.navdy.hud.app.framework.music.OutlineTextView;
import android.graphics.Bitmap;
import com.navdy.service.library.log.Logger;
import com.navdy.hud.app.manager.MusicManager;

public class MusicWidgetPresenter extends DashboardWidgetPresenter implements MusicUpdateListener
{
    private static final float ARTWORK_ALPHA_NOT_PLAYING = 0.5f;
    private static final float ARTWORK_ALPHA_PLAYING = 1.0f;
    private static Logger logger;
    private Bitmap albumArt;
    private boolean animateNextArtworkTransition;
    private OutlineTextView artistText;
    private Handler handler;
    private AlbumArtImageView image;
    private CharSequence lastAlbumArtHash;
    private String musicGaugeName;
    @Inject
    MusicManager musicManager;
    private OutlineTextView stateText;
    private OutlineTextView titleText;
    private MusicTrackInfo trackInfo;
    
    static {
        MusicWidgetPresenter.logger = new Logger(MusicWidgetPresenter.class);
    }
    
    public MusicWidgetPresenter(final Context context) {
        this.trackInfo = null;
        this.handler = new Handler();
        this.musicGaugeName = context.getResources().getString(R.string.widget_music);
        Mortar.inject(HudApplication.getAppContext(), this);
    }
    
    private void redrawOnMainThread() {
        this.handler.post((Runnable)new Runnable() {
            @Override
            public void run() {
                MusicWidgetPresenter.this.reDraw();
            }
        });
    }
    
    private void resetArtwork() {
        this.albumArt = null;
        this.lastAlbumArtHash = null;
    }
    
    private void updateMetadata() {
        final CharSequence text = this.titleText.getText();
        if (this.trackInfo.name == null || !this.trackInfo.name.equals(text)) {
            this.titleText.setText((CharSequence)this.trackInfo.name);
        }
        final CharSequence text2 = this.artistText.getText();
        if (this.trackInfo.author == null || !this.trackInfo.author.equals(text2)) {
            this.artistText.setText((CharSequence)this.trackInfo.author);
        }
    }
    
    @Override
    public Drawable getDrawable() {
        return null;
    }
    
    @Override
    public String getWidgetIdentifier() {
        MusicWidgetPresenter.logger.d("getWidgetIdentifier");
        return "MUSIC_WIDGET";
    }
    
    @Override
    public String getWidgetName() {
        return this.musicGaugeName;
    }
    
    @Override
    public void onAlbumArtUpdate(@Nullable final ByteString byteString, final boolean animateNextArtworkTransition) {
        MusicWidgetPresenter.logger.d("onAlbumArtUpdate " + byteString + ", " + animateNextArtworkTransition);
        this.animateNextArtworkTransition = animateNextArtworkTransition;
        if (byteString == null) {
            MusicWidgetPresenter.logger.v("ByteString image to set in notification is null");
            this.resetArtwork();
            this.reDraw();
        }
        else {
            final String hex = byteString.md5().hex();
            if (TextUtils.equals(this.lastAlbumArtHash, (CharSequence)hex) && this.albumArt != null) {
                MusicWidgetPresenter.logger.d("Already have this artwork, ignoring");
            }
            else {
                this.lastAlbumArtHash = hex;
                TaskManager.getInstance().execute(new Runnable() {
                    @Override
                    public void run() {
                        MusicWidgetPresenter.logger.d("PhotoUpdate runnable " + byteString);
                        final byte[] byteArray = byteString.toByteArray();
                        if (byteArray == null || byteArray.length <= 0) {
                            MusicWidgetPresenter.logger.i("Received photo has null or empty byte array");
                            MusicWidgetPresenter.this.resetArtwork();
                            MusicWidgetPresenter.this.redrawOnMainThread();
                        }
                        else {
                            final int dimensionPixelSize = HudApplication.getAppContext().getResources().getDimensionPixelSize(R.dimen.music_gauge_album_art_size);
                            Bitmap bitmap = null;
                            Bitmap decodeByteArray = null;
                            try {
                                final Bitmap bitmap2 = bitmap = (decodeByteArray = ScalingUtilities.decodeByteArray(byteArray, dimensionPixelSize, dimensionPixelSize, ScalingUtilities.ScalingLogic.FIT));
                                MusicWidgetPresenter.this.albumArt = ScalingUtilities.createScaledBitmap(bitmap2, dimensionPixelSize, dimensionPixelSize, ScalingUtilities.ScalingLogic.FIT);
                                decodeByteArray = bitmap2;
                                bitmap = bitmap2;
                                MusicWidgetPresenter.this.redrawOnMainThread();
                            }
                            catch (Exception ex) {
                                bitmap = decodeByteArray;
                                MusicWidgetPresenter.logger.e("Error updating the art work received ", ex);
                                bitmap = decodeByteArray;
                                MusicWidgetPresenter.this.resetArtwork();
                                bitmap = decodeByteArray;
                                MusicWidgetPresenter.this.redrawOnMainThread();
                            }
                            finally {
                                if (bitmap != null && bitmap != MusicWidgetPresenter.this.albumArt) {
                                    bitmap.recycle();
                                }
                            }
                        }
                    }
                }, 1);
            }
        }
    }
    
    @Override
    public void onTrackUpdated(final MusicTrackInfo trackInfo, final Set<MediaControl> set, final boolean b) {
        this.trackInfo = trackInfo;
        if (b) {
            MusicWidgetPresenter.logger.d("onTrackUpdated, delayed!");
            this.handler.postDelayed((Runnable)new Runnable() {
                @Override
                public void run() {
                    MusicWidgetPresenter.this.reDraw();
                }
            }, 250L);
        }
        else {
            MusicWidgetPresenter.logger.d("onTrackUpdated (immediate)");
            this.reDraw();
        }
    }
    
    @Override
    public void setView(final DashboardWidgetView view, final Bundle bundle) {
        MusicWidgetPresenter.logger.d("setView " + view);
        if (view != null) {
            view.setContentView(R.layout.music_widget);
            this.titleText = (OutlineTextView)view.findViewById(R.id.txt_name);
            this.artistText = (OutlineTextView)view.findViewById(R.id.txt_author);
            this.stateText = (OutlineTextView)view.findViewById(R.id.txt_player_state);
            this.image = (AlbumArtImageView)view.findViewById(R.id.img_album_art);
        }
        super.setView(view);
    }
    
    @Override
    public void setWidgetVisibleToUser(final boolean widgetVisibleToUser) {
        MusicWidgetPresenter.logger.d("setWidgetVisibleToUser " + widgetVisibleToUser);
        if (widgetVisibleToUser) {
            MusicWidgetPresenter.logger.v("setWidgetVisibleToUser: add music update listener");
            this.musicManager.addMusicUpdateListener((MusicManager.MusicUpdateListener)this);
            this.trackInfo = this.musicManager.getCurrentTrack();
        }
        else {
            MusicWidgetPresenter.logger.v("setWidgetVisibleToUser: remove music update listener");
            this.musicManager.removeMusicUpdateListener((MusicManager.MusicUpdateListener)this);
        }
        super.setWidgetVisibleToUser(widgetVisibleToUser);
    }
    
    @Override
    protected void updateGauge() {
        this.image.setArtworkBitmap(this.albumArt, this.animateNextArtworkTransition);
        if (this.trackInfo != null && this.trackInfo != MusicManager.EMPTY_TRACK) {
            if (MusicManager.tryingToPlay(this.trackInfo.playbackState)) {
                this.image.setAlpha(1.0f);
                this.stateText.setVisibility(INVISIBLE);
            }
            else {
                this.image.setAlpha(0.5f);
                this.stateText.setVisibility(View.VISIBLE);
            }
            this.updateMetadata();
        }
        else {
            this.titleText.setText((CharSequence)"");
            this.artistText.setText((CharSequence)"");
            this.stateText.setVisibility(INVISIBLE);
        }
    }
}
