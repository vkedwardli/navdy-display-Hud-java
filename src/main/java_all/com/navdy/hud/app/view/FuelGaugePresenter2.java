package com.navdy.hud.app.view;

import android.view.View;
import butterknife.ButterKnife;
import android.os.Bundle;
import android.graphics.drawable.Drawable;
import android.widget.TextView;
import android.content.Context;
import butterknife.InjectView;
import android.widget.ImageView;
import com.navdy.hud.app.view.drawable.FuelGaugeDrawable2;

public class FuelGaugePresenter2 extends DashboardWidgetPresenter
{
    public static final int LOW_FUEL_WARNING_LIMIT = 25;
    public static final int VERY_LOW_FUEL_WARNING_LIMIT = 13;
    FuelGaugeDrawable2 fuelGaugeDrawable2;
    private String fuelGaugeName;
    @InjectView(R.id.fuel_tank_side_indicator_left)
    ImageView fuelTankIndicatorLeft;
    @InjectView(R.id.fuel_tank_side_indicator_right)
    ImageView fuelTankIndicatorRight;
    @InjectView(R.id.fuel_type_icon)
    ImageView fuelTypeIndicator;
    @InjectView(R.id.low_fuel_indicator_left)
    ImageView lowFuelIndicatorLeft;
    @InjectView(R.id.low_fuel_indicator_right)
    ImageView lowFuelIndicatorRight;
    private Context mContext;
    private int mFuelLevel;
    private int mFuelRange;
    private String mFuelRangeUnit;
    private boolean mLeftOriented;
    private FuelTankSide mTanksSide;
    @InjectView(R.id.txt_value)
    TextView rangeText;
    @InjectView(R.id.txt_unit)
    TextView rangeUnitText;
    
    public FuelGaugePresenter2(final Context mContext) {
        this.mTanksSide = FuelTankSide.UNKNOWN;
        this.mFuelRangeUnit = "";
        this.mLeftOriented = true;
        this.mContext = mContext;
        (this.fuelGaugeDrawable2 = new FuelGaugeDrawable2(mContext, R.array.smart_dash_fuel_gauge2_state_colors)).setMinValue(0.0f);
        this.fuelGaugeDrawable2.setMaxGaugeValue(100.0f);
        this.fuelGaugeName = mContext.getResources().getString(R.string.widget_fuel);
    }
    
    @Override
    public Drawable getDrawable() {
        return this.fuelGaugeDrawable2;
    }
    
    @Override
    public String getWidgetIdentifier() {
        return "FUEL_GAUGE_ID";
    }
    
    @Override
    public String getWidgetName() {
        return this.fuelGaugeName;
    }
    
    public void setFuelLevel(final int mFuelLevel) {
        this.mFuelLevel = mFuelLevel;
        this.reDraw();
    }
    
    public void setFuelRange(final int mFuelRange) {
        this.mFuelRange = mFuelRange;
    }
    
    public void setFuelRangeUnit(final String mFuelRangeUnit) {
        this.mFuelRangeUnit = mFuelRangeUnit;
    }
    
    public void setFuelTankSide(final FuelTankSide mTanksSide) {
        this.mTanksSide = mTanksSide;
    }
    
    @Override
    public void setView(final DashboardWidgetView dashboardWidgetView, final Bundle bundle) {
        int contentView;
        final int n = contentView = R.layout.fuel_gauge_left;
        if (bundle != null) {
            contentView = n;
            switch (bundle.getInt("EXTRA_GRAVITY")) {
                default:
                    contentView = n;
                    break;
                case 2:
                    contentView = R.layout.fuel_gauge_right;
                    this.mLeftOriented = false;
                    break;
                case 0:
                    contentView = R.layout.fuel_gauge_left;
                    this.mLeftOriented = true;
                case 1:
                    break;
            }
        }
        if (dashboardWidgetView != null) {
            dashboardWidgetView.setContentView(contentView);
            ButterKnife.inject(this, (View)dashboardWidgetView);
        }
        super.setView(dashboardWidgetView, bundle);
        this.reDraw();
    }
    
    @Override
    protected void updateGauge() {
        final boolean b = true;
        if (this.mWidgetView != null) {
            int n;
            if (this.mFuelLevel <= 25) {
                n = 1;
            }
            else {
                n = 0;
            }
            boolean b2;
            if (this.mFuelLevel <= 13) {
                b2 = true;
            }
            else {
                b2 = false;
            }
            this.fuelGaugeDrawable2.setGaugeValue(this.mFuelLevel);
            this.fuelGaugeDrawable2.setLeftOriented(this.mLeftOriented);
            final FuelGaugeDrawable2 fuelGaugeDrawable2 = this.fuelGaugeDrawable2;
            int state;
            if (n != 0) {
                state = (b ? 1 : 0);
                if (b2) {
                    state = 2;
                }
            }
            else {
                state = 0;
            }
            fuelGaugeDrawable2.setState(state);
            this.lowFuelIndicatorLeft.setVisibility(GONE);
            this.lowFuelIndicatorRight.setVisibility(GONE);
            switch (this.mTanksSide) {
                case UNKNOWN:
                    this.fuelTankIndicatorLeft.setVisibility(GONE);
                    this.fuelTankIndicatorRight.setVisibility(GONE);
                    if (b2) {
                        this.lowFuelIndicatorLeft.setVisibility(View.VISIBLE);
                        break;
                    }
                    break;
                case LEFT:
                    this.fuelTankIndicatorLeft.setVisibility(View.VISIBLE);
                    this.fuelTankIndicatorRight.setVisibility(GONE);
                    if (b2) {
                        this.lowFuelIndicatorRight.setVisibility(View.VISIBLE);
                        break;
                    }
                    break;
                case RIGHT:
                    this.fuelTankIndicatorLeft.setVisibility(GONE);
                    this.fuelTankIndicatorRight.setVisibility(View.VISIBLE);
                    if (b2) {
                        this.lowFuelIndicatorLeft.setVisibility(View.VISIBLE);
                        break;
                    }
                    break;
            }
            if (this.mFuelRange > 0) {
                this.rangeText.setText((CharSequence)Integer.toString(this.mFuelRange));
            }
            else {
                this.rangeText.setText((CharSequence)"");
            }
            this.rangeUnitText.setText((CharSequence)this.mFuelRangeUnit);
        }
    }
    
    public enum FuelTankSide
    {
        LEFT, 
        RIGHT, 
        UNKNOWN;
    }
}
