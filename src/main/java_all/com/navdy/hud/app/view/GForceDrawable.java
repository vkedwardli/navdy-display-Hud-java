package com.navdy.hud.app.view;

import android.graphics.Rect;
import java.util.Locale;
import android.graphics.RectF;
import android.graphics.Canvas;
import android.content.res.Resources;
import android.graphics.Paint;
import android.content.Context;
import android.graphics.Paint;
import com.navdy.service.library.log.Logger;
import android.graphics.Typeface;
import com.navdy.hud.app.view.drawable.CustomDrawable;

public class GForceDrawable extends CustomDrawable
{
    private static final int BACKGROUND = 0;
    private static final int DANGER = 4;
    private static final int EXTREME = 5;
    private static final int HIGHLIGHT = 2;
    public static final float HIGH_G = 1.05f;
    private static final int LOW = 1;
    public static final float LOW_G = 0.45f;
    public static final float MAX_ACCEL = 2.0f;
    public static final float MEDIUM_G = 0.75f;
    public static final float NO_G = 0.15f;
    private static final int WARNING = 3;
    private static Typeface typeface;
    private int[] colors;
    protected Logger logger;
    private Paint mTextPaint;
    private int textColor;
    private float xAccel;
    private float yAccel;
    private float zAccel;
    
    public GForceDrawable(final Context context) {
        this.logger = new Logger(this.getClass());
        this.mPaint.setColor(-1);
        this.mPaint.setStrokeWidth(1.0f);
        if (GForceDrawable.typeface == null) {
            GForceDrawable.typeface = Typeface.create("sans-serif-medium", 0);
        }
        (this.mTextPaint = new Paint()).setTextAlign(Paint$Align.CENTER);
        final Resources resources = context.getResources();
        this.colors = resources.getIntArray(R.array.gforce_colors);
        this.textColor = resources.getColor(R.color.gforce_text);
    }
    
    private float clamp(final float n, float n2, final float n3) {
        if (n >= n2) {
            n2 = n;
            if (n > n3) {
                n2 = n3;
            }
        }
        return n2;
    }
    
    @Override
    public void draw(final Canvas canvas) {
        super.draw(canvas);
        final Rect bounds = this.getBounds();
        final int n = bounds.width() - 10;
        final float n2 = (bounds.height() - 10) / 2;
        final float n3 = n / 2;
        final float clamp = this.clamp(this.xAccel, -2.0f, 2.0f);
        final float clamp2 = this.clamp(this.yAccel, -2.0f, 2.0f);
        this.clamp(this.zAccel, -2.0f, 2.0f);
        if (false) {
            final int min = Math.min(10, Math.max(1, n / 4 / 4));
            for (int n4 = 2 * min, i = -n4; i <= n4; ++i) {
                final float n5 = 5 + (1.0f + i / n4) * n3;
                int n6;
                if (i % min == 0) {
                    n6 = 1;
                }
                else {
                    n6 = 0;
                }
                int n7;
                if (n6 != 0) {
                    n7 = 10;
                }
                else {
                    n7 = 5;
                }
                canvas.drawLine(n5, 5 + n2 - n7 / 2, n5, 5 + n2 + n7 / 2, this.mPaint);
                final float n8 = 5 + (1.0f + i / n4) * n2;
                canvas.drawLine(5 + n3 - n7 / 2, n8, 5 + n3 + n7 / 2, n8, this.mPaint);
            }
            canvas.drawCircle(5 + n3 + clamp * n3 / 2.0f, 5 + n2 - clamp2 * n2 / 2.0f, 4.0f + Math.abs(this.zAccel) * 3.0f, this.mPaint);
        }
        else {
            final float max = Math.max(Math.abs(clamp), Math.abs(clamp2));
            this.mPaint.setColor(this.colors[0]);
            canvas.drawCircle(5 + n3, 5 + n2, 50.0f, this.mPaint);
            float n9 = (float)(Math.atan(-clamp2 / clamp) * 180.0 / 3.141592653589793);
            if (clamp < 0.0f) {
                n9 += 180.0f;
            }
            int n10 = this.colors[1];
            if (max > 0.15f) {
                final float n11 = 20.0f + 70.0f * (this.clamp(max, 0.15f, 0.75f) - 0.15f) / 0.6f;
                final float n12 = n11 / 2.0f;
                if (max < 0.45f) {
                    n10 = this.colors[2];
                }
                else if (max < 0.75f) {
                    n10 = this.colors[3];
                }
                else if (max < 1.05f) {
                    n10 = this.colors[4];
                }
                else {
                    n10 = this.colors[5];
                }
                this.mPaint.setColor(n10);
                canvas.drawArc(new RectF(50.0f - 50.0f, 50.0f - 50.0f, 50.0f + 50.0f, 50.0f + 50.0f), n9 - n12, n11, true, this.mPaint);
            }
            this.mPaint.setColor(-16777216);
            canvas.drawCircle(5 + n3, 5 + n2, 36.0f, this.mPaint);
            this.mTextPaint.setColor(this.textColor);
            this.mTextPaint.setTextSize(16.0f);
            this.mTextPaint.setTypeface(Typeface.SANS_SERIF);
            canvas.drawText("G", 5 + n3, 5 + n2 - 18.0f, this.mTextPaint);
            canvas.drawText("-", 5 + n3, 5 + n2 + 28.0f, this.mTextPaint);
            this.mTextPaint.setColor(n10);
            this.mTextPaint.setTextSize(28.0f);
            this.mTextPaint.setTypeface(GForceDrawable.typeface);
            int n13;
            if (this.xAccel == 0.0f && this.yAccel == 0.0f && this.zAccel == 0.0f) {
                n13 = 1;
            }
            else {
                n13 = 0;
            }
            String format;
            if (n13 != 0) {
                format = "-";
            }
            else {
                format = String.format(Locale.US, "%01.2f", max);
            }
            canvas.drawText(format, 5 + n3, 5 + n2 + 10.0f, this.mTextPaint);
        }
    }
    
    protected void onBoundsChange(final Rect rect) {
        super.onBoundsChange(rect);
    }
    
    public void setAcceleration(final float xAccel, final float yAccel, final float zAccel) {
        this.xAccel = xAccel;
        this.yAccel = yAccel;
        this.zAccel = zAccel;
    }
}
