package com.navdy.hud.app.view;

import com.navdy.service.library.events.input.Gesture;
import com.navdy.service.library.events.input.GestureEvent;
import android.content.res.Resources;
import com.navdy.hud.app.device.dial.DialFirmwareUpdater;
import java.util.List;
import java.util.ArrayList;
import com.navdy.hud.app.util.ViewUtil;
import com.navdy.hud.app.device.dial.DialManager;
import android.view.View;
import butterknife.ButterKnife;
import java.io.Serializable;
import com.navdy.hud.app.screen.BaseScreen;
import com.navdy.hud.app.framework.notifications.NotificationManager;
import com.navdy.hud.app.framework.toast.ToastManager;
import com.navdy.service.library.events.ui.Screen;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.analytics.AnalyticsSupport;
import mortar.Mortar;
import android.util.AttributeSet;
import android.content.Context;
import javax.inject.Inject;
import com.navdy.hud.app.screen.DialUpdateConfirmationScreen;
import android.widget.TextView;
import android.widget.ImageView;
import butterknife.InjectView;
import com.navdy.service.library.log.Logger;
import com.navdy.hud.app.manager.InputManager;
import com.navdy.hud.app.ui.component.ChoiceLayout;
import android.widget.RelativeLayout;

public class DialUpdateConfirmationView extends RelativeLayout implements IListener, IInputHandler
{
    private static final int UPDATE_MESSAGE_MAX_WIDTH = 380;
    private static final Logger sLogger;
    private boolean isReminder;
    @InjectView(R.id.choiceLayout)
    ChoiceLayout mChoiceLayout;
    @InjectView(R.id.image)
    ImageView mIcon;
    @InjectView(R.id.title3)
    TextView mInfoText;
    @InjectView(R.id.leftSwipe)
    ImageView mLefttSwipe;
    @InjectView(R.id.title2)
    TextView mMainTitleText;
    @Inject
    DialUpdateConfirmationScreen.Presenter mPresenter;
    @InjectView(R.id.rightSwipe)
    ImageView mRightSwipe;
    @InjectView(R.id.mainTitle)
    TextView mScreenTitleText;
    @InjectView(R.id.title4)
    TextView mSummaryText;
    @InjectView(R.id.mainSection)
    RelativeLayout mainSection;
    private String updateTargetVersion;
    
    static {
        sLogger = new Logger(DialUpdateConfirmationView.class);
    }
    
    public DialUpdateConfirmationView(final Context context) {
        this(context, null, 0);
    }
    
    public DialUpdateConfirmationView(final Context context, final AttributeSet set) {
        this(context, set, 0);
    }
    
    public DialUpdateConfirmationView(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
        this.updateTargetVersion = "1.0.60";
        this.isReminder = false;
        if (!this.isInEditMode()) {
            Mortar.inject(context, this);
        }
    }
    
    public void executeItem(final int n, final int n2) {
        switch (n) {
            case 0:
                this.mPresenter.install();
                if (this.isReminder) {
                    AnalyticsSupport.recordUpdatePrompt(true, true, this.updateTargetVersion);
                    break;
                }
                break;
            case 1:
                this.mPresenter.finish();
                if (this.isReminder) {
                    AnalyticsSupport.recordUpdatePrompt(false, true, this.updateTargetVersion);
                    break;
                }
                break;
        }
    }
    
    public void itemSelected(final int n, final int n2) {
    }
    
    public IInputHandler nextHandler() {
        return null;
    }
    
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
    }
    
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        final BaseScreen currentScreen = RemoteDeviceManager.getInstance().getUiStateManager().getCurrentScreen();
        boolean b2;
        final boolean b = b2 = true;
        if (currentScreen != null) {
            b2 = b;
            if (currentScreen.getScreen() == Screen.SCREEN_DIAL_UPDATE_PROGRESS) {
                b2 = false;
            }
        }
        final Logger sLogger = DialUpdateConfirmationView.sLogger;
        final StringBuilder append = new StringBuilder().append("enableNotif:").append(b2).append(" screen[");
        Serializable screen;
        if (currentScreen == null) {
            screen = "none";
        }
        else {
            screen = currentScreen.getScreen();
        }
        sLogger.v(append.append(screen).append("]").toString());
        if (b2) {
            ToastManager.getInstance().disableToasts(false);
            NotificationManager.getInstance().enableNotifications(true);
        }
        if (this.mPresenter != null) {
            this.mPresenter.dropView(this);
        }
    }
    
    protected void onFinishInflate() {
        super.onFinishInflate();
        String string = "1.0.1";
        ButterKnife.inject((View)this);
        if (this.mPresenter != null) {
            this.mPresenter.takeView(this);
            this.isReminder = this.mPresenter.isReminder();
            ToastManager.getInstance().disableToasts(true);
            NotificationManager.getInstance().enableNotifications(false);
            final DialFirmwareUpdater.Versions versions = DialManager.getInstance().getDialFirmwareUpdater().getVersions();
            this.updateTargetVersion = "1.0." + versions.local.incrementalVersion;
            string = "1.0." + versions.dial.incrementalVersion;
        }
        this.mScreenTitleText.setVisibility(GONE);
        this.findViewById(R.id.title1).setVisibility(GONE);
        this.mMainTitleText.setText(R.string.dial_update_ready_to_install);
        ViewUtil.adjustPadding((View)this.mainSection, 0, 0, 0, 10);
        ViewUtil.autosize(this.mMainTitleText, 2, 380, R.array.title_sizes);
        this.mMainTitleText.setVisibility(View.VISIBLE);
        this.mIcon.setImageResource(R.drawable.icon_dial_update);
        this.mInfoText.setText(R.string.dial_update_installation_will_take);
        this.mInfoText.setVisibility(View.VISIBLE);
        ((MaxWidthLinearLayout)this.findViewById(R.id.infoContainer)).setMaxWidth(380);
        final Resources resources = this.getContext().getResources();
        this.mSummaryText.setText((CharSequence)String.format(resources.getString(R.string.dial_update_will_upgrade_to_from), this.updateTargetVersion, string));
        this.mSummaryText.setVisibility(View.VISIBLE);
        final ArrayList<Choice> list = new ArrayList<Choice>();
        if (this.isReminder) {
            list.add(new ChoiceLayout.Choice(resources.getString(R.string.install_now), 0));
            list.add(new ChoiceLayout.Choice(resources.getString(R.string.install_later), 1));
        }
        else {
            list.add(new ChoiceLayout.Choice(resources.getString(R.string.install), 0));
            list.add(new ChoiceLayout.Choice(resources.getString(R.string.cancel), 1));
        }
        this.mChoiceLayout.setChoices(Mode.LABEL, list, 0, (ChoiceLayout.IListener)this);
        this.mRightSwipe.setVisibility(View.VISIBLE);
        this.mLefttSwipe.setVisibility(View.VISIBLE);
    }
    
    public boolean onGesture(final GestureEvent gestureEvent) {
        boolean b = true;
        if (gestureEvent.gesture != null) {
            switch (gestureEvent.gesture) {
                case GESTURE_SWIPE_LEFT:
                    this.executeItem(0, 0);
                    return b;
                case GESTURE_SWIPE_RIGHT:
                    this.executeItem(1, 0);
                    return b;
            }
        }
        b = false;
        return b;
    }
    
    public boolean onKey(final CustomKeyEvent customKeyEvent) {
        boolean b = true;
        if (this.mChoiceLayout != null && this.mChoiceLayout.getVisibility() == 0) {
            switch (customKeyEvent) {
                case LEFT:
                    this.mChoiceLayout.moveSelectionLeft();
                    break;
                case RIGHT:
                    this.mChoiceLayout.moveSelectionRight();
                    break;
                case SELECT:
                    this.mChoiceLayout.executeSelectedItem(true);
                    break;
            }
        }
        else {
            b = false;
        }
        return b;
    }
}
