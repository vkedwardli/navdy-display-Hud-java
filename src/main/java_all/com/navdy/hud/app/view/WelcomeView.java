package com.navdy.hud.app.view;

import com.navdy.service.library.events.input.GestureEvent;
import butterknife.ButterKnife;
import android.content.res.TypedArray;
import android.content.res.Resources;
import android.text.style.StyleSpan;
import android.text.style.ForegroundColorSpan;
import android.text.SpannableStringBuilder;
import com.navdy.hud.app.ui.component.carousel.ShrinkAnimator;
import com.navdy.hud.app.ui.component.image.CrossFadeImageView;
import android.graphics.Bitmap;
import java.util.HashMap;
import com.navdy.service.library.device.NavdyDeviceId;
import com.navdy.service.library.task.TaskManager;
import com.navdy.hud.app.util.picasso.PicassoUtil;
import com.navdy.hud.app.framework.contacts.ContactUtil;
import com.navdy.hud.app.ui.component.image.InitialsImageView;
import com.navdy.hud.app.R;
import com.squareup.picasso.Callback;
import java.io.File;
import mortar.Mortar;
import android.os.Looper;
import com.makeramen.RoundedTransformationBuilder;
import java.util.ArrayList;
import android.util.AttributeSet;
import android.content.Context;
import java.util.concurrent.TimeUnit;
import com.squareup.picasso.Transformation;
import android.view.View;
import javax.inject.Inject;
import com.navdy.hud.app.screen.WelcomeScreen;
import com.navdy.hud.app.ui.component.carousel.Carousel;
import java.util.List;
import android.os.Handler;
import android.widget.TextView;
import android.widget.RelativeLayout;
import com.navdy.hud.app.ui.component.ChoiceLayout;
import com.navdy.hud.app.ui.component.carousel.CarouselLayout;
import android.widget.ImageView;
import butterknife.InjectView;
import com.navdy.hud.app.ui.component.FluctuatorAnimatorView;
import com.navdy.service.library.log.Logger;
import com.navdy.hud.app.manager.InputManager;
import android.widget.FrameLayout;

public class WelcomeView extends FrameLayout implements IInputHandler
{
    private static final int DOWNLOAD_APP_STAY_INTERVAL;
    private static final int DOWNLOAD_APP_TRANSITION_INTERVAL;
    public static final int STATE_DOWNLOAD_APP = 1;
    public static final int STATE_PICKER = 2;
    public static final int STATE_UNKNOWN = -1;
    public static final int STATE_WELCOME = 3;
    private static int[] appStoreIcons;
    private static int downloadColor1;
    private static int downloadColor2;
    private static String[] downloadText1;
    private static String[] downloadText2;
    private static int[] playStoreIcons;
    private static final Logger sLogger;
    private Runnable animationRunnable;
    @InjectView(R.id.animatorView)
    public FluctuatorAnimatorView animatorView;
    @InjectView(R.id.app_store_img_1)
    public ImageView appStoreImage1;
    @InjectView(R.id.app_store_img_2)
    public ImageView appStoreImage2;
    @InjectView(R.id.carousel)
    public CarouselLayout carousel;
    @InjectView(R.id.choiceLayout)
    public ChoiceLayout choiceLayout;
    private int currentDownloadIndex;
    private int currentDownloadView;
    @InjectView(R.id.download_app)
    public RelativeLayout downloadAppContainer;
    @InjectView(R.id.download_text_1)
    public TextView downloadAppTextView1;
    @InjectView(R.id.download_text_2)
    public TextView downloadAppTextView2;
    private Handler handler;
    private int initialState;
    @InjectView(R.id.leftDot)
    public ImageView leftDot;
    public List<Carousel.Model> list;
    @InjectView(R.id.message)
    public TextView messageView;
    @InjectView(R.id.play_store_img_1)
    public ImageView playStoreImage1;
    @InjectView(R.id.play_store_img_2)
    public ImageView playStoreImage2;
    @Inject
    public WelcomeScreen.Presenter presenter;
    @InjectView(R.id.rightDot)
    public ImageView rightDot;
    @InjectView(R.id.rootContainer)
    public View rootContainer;
    Transformation roundTransformation;
    private boolean searching;
    private int state;
    @InjectView(R.id.subTitle)
    public TextView subTitleView;
    @InjectView(R.id.title)
    public TextView titleView;
    
    static {
        sLogger = new Logger(WelcomeView.class);
        DOWNLOAD_APP_TRANSITION_INTERVAL = (int)TimeUnit.MILLISECONDS.toMillis(200L);
        DOWNLOAD_APP_STAY_INTERVAL = (int)TimeUnit.SECONDS.toMillis(5L) - WelcomeView.DOWNLOAD_APP_TRANSITION_INTERVAL;
    }
    
    public WelcomeView(final Context context) {
        this(context, null);
    }
    
    public WelcomeView(final Context context, final AttributeSet set) {
        this(context, set, 0);
    }
    
    public WelcomeView(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
        this.state = -1;
        this.currentDownloadIndex = -1;
        this.currentDownloadView = -1;
        this.list = new ArrayList<Carousel.Model>();
        this.roundTransformation = new RoundedTransformationBuilder().oval(true).build();
        this.handler = new Handler(Looper.getMainLooper());
        this.animationRunnable = new Runnable() {
            @Override
            public void run() {
                if (WelcomeView.this.currentDownloadIndex != -1) {
                    TextView textView = null;
                    TextView textView2 = null;
                    ImageView imageView = null;
                    ImageView imageView2 = null;
                    ImageView imageView3 = null;
                    ImageView imageView4 = null;
                    switch (WelcomeView.this.currentDownloadView) {
                        case 0:
                            textView = WelcomeView.this.downloadAppTextView1;
                            textView2 = WelcomeView.this.downloadAppTextView2;
                            imageView = WelcomeView.this.appStoreImage1;
                            imageView2 = WelcomeView.this.appStoreImage2;
                            imageView3 = WelcomeView.this.playStoreImage1;
                            imageView4 = WelcomeView.this.playStoreImage2;
                            WelcomeView.this.currentDownloadView = 1;
                            break;
                        case 1:
                            textView = WelcomeView.this.downloadAppTextView2;
                            textView2 = WelcomeView.this.downloadAppTextView1;
                            imageView = WelcomeView.this.appStoreImage2;
                            imageView2 = WelcomeView.this.appStoreImage1;
                            imageView3 = WelcomeView.this.playStoreImage2;
                            imageView4 = WelcomeView.this.playStoreImage1;
                            WelcomeView.this.currentDownloadView = 0;
                            break;
                    }
                    textView.animate().alpha(0.0f).setDuration((long)WelcomeView.DOWNLOAD_APP_TRANSITION_INTERVAL);
                    textView2.animate().alpha(1.0f).setDuration((long)WelcomeView.DOWNLOAD_APP_TRANSITION_INTERVAL).withEndAction((Runnable)new Runnable() {
                        @Override
                        public void run() {
                            WelcomeView.this.currentDownloadIndex++;
                            if (WelcomeView.this.currentDownloadIndex == WelcomeView.appStoreIcons.length) {
                                WelcomeView.this.currentDownloadIndex = 0;
                            }
                            WelcomeView.this.handler.removeCallbacks(WelcomeView.this.animationRunnable);
                            WelcomeView.this.handler.postDelayed(WelcomeView.this.animationRunnable, (long)WelcomeView.DOWNLOAD_APP_STAY_INTERVAL);
                            WelcomeView.this.setDownloadText(textView, WelcomeView.this.currentDownloadIndex);
                            WelcomeView.this.setDownloadImage(imageView, WelcomeView.appStoreIcons[WelcomeView.this.currentDownloadIndex]);
                            WelcomeView.this.setDownloadImage(imageView3, WelcomeView.playStoreIcons[WelcomeView.this.currentDownloadIndex]);
                        }
                    });
                    imageView.animate().alpha(0.0f).setDuration((long)WelcomeView.DOWNLOAD_APP_TRANSITION_INTERVAL);
                    imageView2.animate().alpha(1.0f).setDuration((long)WelcomeView.DOWNLOAD_APP_TRANSITION_INTERVAL);
                    imageView3.animate().alpha(0.0f).setDuration((long)WelcomeView.DOWNLOAD_APP_TRANSITION_INTERVAL);
                    imageView4.animate().alpha(1.0f).setDuration((long)WelcomeView.DOWNLOAD_APP_TRANSITION_INTERVAL);
                }
            }
        };
        if (!this.isInEditMode()) {
            Mortar.inject(context, this);
        }
        this.initFromAttributes(context, set);
    }
    
    private void initFromAttributes(Context obtainStyledAttributes, final AttributeSet set) {
        obtainStyledAttributes = (Context)obtainStyledAttributes.getTheme().obtainStyledAttributes(set, R.styleable.WelcomeView, 0, 0);
        try {
            this.initialState = ((TypedArray)obtainStyledAttributes).getInt(0, 1);
        }
        finally {
            ((TypedArray)obtainStyledAttributes).recycle();
        }
    }
    
    private void initializeCarousel() {
        final Carousel.InitParams initParams = new Carousel.InitParams();
        initParams.model = this.list;
        initParams.rootContainer = this.rootContainer;
        initParams.viewProcessor = new Carousel.ViewProcessor() {
            private void updateInitials(final boolean b, final Model model, final ImageView imageView) {
                final InitialsImageView initialsImageView = (InitialsImageView)imageView;
                if (model.id == R.id.welcome_menu_add_driver || model.id == R.id.welcome_menu_searching) {
                    initialsImageView.setInitials(null, InitialsImageView.Style.LARGE);
                }
                else {
                    final WelcomeScreen.DeviceMetadata deviceMetadata = (WelcomeScreen.DeviceMetadata)model.extras;
                    boolean b2;
                    if (model.id >= 100) {
                        b2 = true;
                    }
                    else {
                        b2 = false;
                    }
                    float alpha;
                    if (b2 && !b) {
                        alpha = 0.5f;
                    }
                    else {
                        alpha = 1.0f;
                    }
                    imageView.setAlpha(alpha);
                    final NavdyDeviceId deviceId = deviceMetadata.deviceId;
                    imageView.setTag(deviceId);
                    final HashMap<Integer, String> infoMap = model.infoMap;
                    String s;
                    if (infoMap != null) {
                        s = infoMap.get(R.id.subTitle);
                    }
                    else {
                        s = "";
                    }
                    final String initials = ContactUtil.getInitials(s);
                    initialsImageView.setInitials(null, InitialsImageView.Style.DEFAULT);
                    final File driverImageFile = deviceMetadata.driverProfile.getDriverImageFile();
                    final Bitmap bitmapfromCache = PicassoUtil.getBitmapfromCache(driverImageFile);
                    if (bitmapfromCache != null) {
                        initialsImageView.setImageBitmap(bitmapfromCache);
                    }
                    else {
                        TaskManager.getInstance().execute(new Runnable() {
                            @Override
                            public void run() {
                                if (imageView.getTag() == deviceId) {
                                    WelcomeView.this.handler.post((Runnable)new Runnable() {
                                        final /* synthetic */ boolean val$imageExists = driverImageFile.exists();
                                        
                                        @Override
                                        public void run() {
                                            if (imageView.getTag() == deviceId) {
                                                if (!this.val$imageExists) {
                                                    initialsImageView.setInitials(initials, InitialsImageView.Style.LARGE);
                                                }
                                                else {
                                                    WelcomeView.this.setImage(driverImageFile, initialsImageView, null);
                                                }
                                            }
                                        }
                                    });
                                }
                            }
                        }, 1);
                    }
                }
            }
            
            @Override
            public void processInfoView(final Model model, final View view, final int n) {
            }
            
            @Override
            public void processLargeImageView(final Model model, final View view, final int n, final int n2, final int n3) {
                final CrossFadeImageView crossFadeImageView = (CrossFadeImageView)view;
                this.updateInitials(false, model, (ImageView)crossFadeImageView.getSmall());
                this.updateInitials(true, model, (ImageView)crossFadeImageView.getBig());
            }
            
            @Override
            public void processSmallImageView(final Model model, final View view, final int n, final int n2, final int n3) {
                final CrossFadeImageView crossFadeImageView = (CrossFadeImageView)view;
                this.updateInitials(false, model, (ImageView)crossFadeImageView.getSmall());
                this.updateInitials(true, model, (ImageView)crossFadeImageView.getBig());
            }
        };
        initParams.infoLayoutResourceId = R.layout.carousel_view_menu;
        initParams.imageLytResourceId = R.layout.crossfade_image_lyt;
        initParams.carouselIndicator = null;
        initParams.animator = new ShrinkAnimator();
        this.carousel.init(initParams);
        this.carousel.setListener(new Carousel.Listener() {
            @Override
            public void onCurrentItemChanged(final int n, int n2) {
                final int n3 = 0;
                final ImageView leftDot = WelcomeView.this.leftDot;
                if (n > 1) {
                    n2 = 0;
                }
                else {
                    n2 = 8;
                }
                leftDot.setVisibility(n2);
                final ImageView rightDot = WelcomeView.this.rightDot;
                if (n + 2 < WelcomeView.this.carousel.getCount()) {
                    n2 = n3;
                }
                else {
                    n2 = 8;
                }
                rightDot.setVisibility(n2);
                final Model model = WelcomeView.this.carousel.getModel(n);
                WelcomeView.this.updateInfo(model);
                if (WelcomeView.this.presenter != null) {
                    WelcomeView.this.presenter.onCurrentItemChanged(n, model.id);
                }
            }
            
            @Override
            public void onCurrentItemChanging(int n, final int n2, int n3) {
                final int n4 = 0;
                n3 = 0;
                if (n2 > n) {
                    final ImageView rightDot = WelcomeView.this.rightDot;
                    if (n2 + 2 < WelcomeView.this.carousel.getCount()) {
                        n = n3;
                    }
                    else {
                        n = 8;
                    }
                    rightDot.setVisibility(n);
                }
                else {
                    final ImageView leftDot = WelcomeView.this.leftDot;
                    if (n2 > 1) {
                        n = n4;
                    }
                    else {
                        n = 8;
                    }
                    leftDot.setVisibility(n);
                }
                WelcomeView.this.updateInfo(WelcomeView.this.carousel.getModel(n2));
            }
            
            @Override
            public void onExecuteItem(final int n, final int n2) {
                if (WelcomeView.this.presenter != null) {
                    WelcomeView.this.presenter.executeItem(n, n2);
                }
            }
            
            @Override
            public void onExit() {
            }
        });
    }
    
    private void setDownloadImage(final ImageView imageView, final int imageResource) {
        imageView.setImageResource(imageResource);
    }
    
    private void setDownloadText(final TextView textView, final int n) {
        final SpannableStringBuilder text = new SpannableStringBuilder();
        text.append((CharSequence)WelcomeView.downloadText1[n]);
        text.setSpan(new ForegroundColorSpan(WelcomeView.downloadColor1), 0, text.length(), 33);
        text.append((CharSequence)" ");
        final int length = text.length();
        text.append((CharSequence)WelcomeView.downloadText2[n]);
        text.setSpan(new ForegroundColorSpan(WelcomeView.downloadColor2), length, text.length(), 33);
        text.setSpan(new StyleSpan(1), length, text.length(), 33);
        textView.setText((CharSequence)text);
    }
    
    private void setImage(final File file, final ImageView imageView, final Callback callback) {
        PicassoUtil.getInstance().load(file).noPlaceholder().fit().transform(this.roundTransformation).noFade().into(imageView, callback);
    }
    
    private void startDownloadAppAnimation() {
        WelcomeView.sLogger.v("startDownloadAppAnimation");
        final Resources resources = this.getContext().getResources();
        if (WelcomeView.downloadText1 == null) {
            WelcomeView.downloadText1 = resources.getStringArray(R.array.download_app_1);
            WelcomeView.downloadText2 = resources.getStringArray(R.array.download_app_2);
            WelcomeView.downloadColor1 = resources.getColor(R.color.download_app_1);
            WelcomeView.downloadColor2 = resources.getColor(R.color.download_app_2);
            final TypedArray obtainTypedArray = this.getResources().obtainTypedArray(R.array.download_app_icon_app_store);
            WelcomeView.appStoreIcons = new int[WelcomeView.downloadText1.length];
            for (int i = 0; i < WelcomeView.appStoreIcons.length; ++i) {
                WelcomeView.appStoreIcons[i] = obtainTypedArray.getResourceId(i, 0);
            }
            obtainTypedArray.recycle();
            final TypedArray obtainTypedArray2 = this.getResources().obtainTypedArray(R.array.download_app_icon_play_store);
            WelcomeView.playStoreIcons = new int[WelcomeView.downloadText1.length];
            for (int j = 0; j < WelcomeView.playStoreIcons.length; ++j) {
                WelcomeView.playStoreIcons[j] = obtainTypedArray2.getResourceId(j, 0);
            }
            obtainTypedArray2.recycle();
        }
        this.currentDownloadIndex = 0;
        this.currentDownloadView = 0;
        final int currentDownloadIndex = this.currentDownloadIndex;
        final int n = this.currentDownloadIndex + 1;
        this.setDownloadText(this.downloadAppTextView1, currentDownloadIndex);
        this.setDownloadText(this.downloadAppTextView2, n);
        this.setDownloadImage(this.appStoreImage1, WelcomeView.appStoreIcons[currentDownloadIndex]);
        this.setDownloadImage(this.appStoreImage2, WelcomeView.appStoreIcons[n]);
        this.setDownloadImage(this.playStoreImage1, WelcomeView.playStoreIcons[currentDownloadIndex]);
        this.setDownloadImage(this.playStoreImage2, WelcomeView.playStoreIcons[n]);
        this.downloadAppTextView1.setAlpha(1.0f);
        this.downloadAppTextView2.setAlpha(0.0f);
        this.appStoreImage1.setAlpha(1.0f);
        this.appStoreImage2.setAlpha(0.0f);
        this.playStoreImage1.setAlpha(1.0f);
        this.playStoreImage2.setAlpha(0.0f);
        this.downloadAppContainer.setVisibility(View.VISIBLE);
        ++this.currentDownloadIndex;
        this.handler.removeCallbacks(this.animationRunnable);
        this.handler.postDelayed(this.animationRunnable, (long)WelcomeView.DOWNLOAD_APP_STAY_INTERVAL);
    }
    
    private void stopDownloadAppAnimation() {
        WelcomeView.sLogger.v("stopDownloadAppAnimation");
        this.handler.removeCallbacks(this.animationRunnable);
        this.downloadAppTextView1.animate().cancel();
        this.downloadAppTextView2.animate().cancel();
        this.appStoreImage1.animate().cancel();
        this.appStoreImage2.animate().cancel();
        this.playStoreImage1.animate().cancel();
        this.playStoreImage2.animate().cancel();
        this.downloadAppContainer.setVisibility(GONE);
        this.currentDownloadIndex = -1;
    }
    
    private void updateInfo(final Carousel.Model model) {
        if (model != null && model.infoMap != null) {
            final String s = model.infoMap.get(R.id.title);
            final String s2 = model.infoMap.get(R.id.subTitle);
            final String s3 = model.infoMap.get(R.id.message);
            final TextView titleView = this.titleView;
            String text;
            if (s != null) {
                text = s;
            }
            else {
                text = "";
            }
            titleView.setText((CharSequence)text);
            final TextView subTitleView = this.subTitleView;
            String text2;
            if (s2 != null) {
                text2 = s2;
            }
            else {
                text2 = "";
            }
            subTitleView.setText((CharSequence)text2);
            final TextView messageView = this.messageView;
            String text3;
            if (s3 != null) {
                text3 = s3;
            }
            else {
                text3 = "";
            }
            messageView.setText((CharSequence)text3);
        }
    }
    
    public IInputHandler nextHandler() {
        return InputManager.nextContainingHandler((View)this);
    }
    
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (this.presenter != null) {
            this.presenter.takeView(this);
        }
        if (this.searching) {
            this.animatorView.start();
        }
    }
    
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.searching = false;
        this.animatorView.stop();
        this.stopDownloadAppAnimation();
        if (this.presenter != null) {
            this.presenter.dropView(this);
        }
    }
    
    protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.inject((View)this);
        this.setState(this.initialState);
        this.initializeCarousel();
        final ArrayList<ChoiceLayout.Choice> list = new ArrayList<ChoiceLayout.Choice>(1);
        list.add(new ChoiceLayout.Choice(this.getResources().getString(R.string.welcome_cancel), 0));
        this.choiceLayout.setChoices(ChoiceLayout.Mode.LABEL, list, 0, (ChoiceLayout.IListener)new ChoiceLayout.IListener() {
            @Override
            public void executeItem(final int n, final int n2) {
                WelcomeView.this.presenter.cancel();
            }
            
            @Override
            public void itemSelected(final int n, final int n2) {
            }
        });
    }
    
    public boolean onGesture(final GestureEvent gestureEvent) {
        return false;
    }
    
    public boolean onKey(final CustomKeyEvent customKeyEvent) {
        boolean onKey = true;
        if (this.state == 1) {
            switch (customKeyEvent) {
                default:
                    onKey = false;
                    break;
                case LEFT:
                    this.choiceLayout.moveSelectionLeft();
                    break;
                case RIGHT:
                    this.choiceLayout.moveSelectionRight();
                    break;
                case SELECT:
                    this.choiceLayout.executeSelectedItem(true);
                    break;
            }
        }
        else {
            onKey = this.carousel.onKey(customKeyEvent);
        }
        return onKey;
    }
    
    public void setSearching(final boolean searching) {
        if (searching != this.searching) {
            if (!(this.searching = searching)) {
                this.animatorView.stop();
                this.animatorView.setVisibility(GONE);
            }
            else {
                this.animatorView.setVisibility(View.VISIBLE);
                this.animatorView.start();
            }
        }
    }
    
    public void setState(final int state) {
        if (this.state != state) {
            this.state = state;
            this.rootContainer.setVisibility(GONE);
            this.stopDownloadAppAnimation();
            this.leftDot.setVisibility(GONE);
            this.rightDot.setVisibility(GONE);
            switch (state) {
                case 1:
                    this.startDownloadAppAnimation();
                    break;
                case 2:
                case 3:
                    this.rootContainer.setVisibility(View.VISIBLE);
                    break;
            }
        }
    }
}
