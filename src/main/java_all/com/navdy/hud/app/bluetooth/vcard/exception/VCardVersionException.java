package com.navdy.hud.app.bluetooth.vcard.exception;

public class VCardVersionException extends VCardException
{
    public VCardVersionException() {
    }
    
    public VCardVersionException(final String s) {
        super(s);
    }
}
