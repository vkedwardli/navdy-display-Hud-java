package com.navdy.hud.app.bluetooth.vcard;

import android.util.Log;
import java.util.Arrays;
import java.util.HashSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Collection;
import java.util.Map;
import java.util.List;

public class VCardProperty
{
    private static final String LOG_TAG = "vCard";
    private byte[] mByteValue;
    private List<String> mGroupList;
    private String mName;
    private Map<String, Collection<String>> mParameterMap;
    private String mRawValue;
    private List<String> mValueList;
    
    public VCardProperty() {
        this.mParameterMap = new HashMap<String, Collection<String>>();
    }
    
    public void addGroup(final String s) {
        if (this.mGroupList == null) {
            this.mGroupList = new ArrayList<String>();
        }
        this.mGroupList.add(s);
    }
    
    public void addParameter(final String s, final String s2) {
        Object o;
        if (!this.mParameterMap.containsKey(s)) {
            if (s.equals("TYPE")) {
                o = new HashSet<String>();
            }
            else {
                o = new ArrayList<String>();
            }
            this.mParameterMap.put(s, (Collection<String>)o);
        }
        else {
            o = this.mParameterMap.get(s);
        }
        ((Collection<String>)o).add(s2);
    }
    
    public void addValues(final List<String> list) {
        if (this.mValueList == null) {
            this.mValueList = new ArrayList<String>(list);
        }
        else {
            this.mValueList.addAll(list);
        }
    }
    
    public void addValues(final String... array) {
        if (this.mValueList == null) {
            this.mValueList = Arrays.<String>asList(array);
        }
        else {
            this.mValueList.addAll(Arrays.<String>asList(array));
        }
    }
    
    public byte[] getByteValue() {
        return this.mByteValue;
    }
    
    public List<String> getGroupList() {
        return this.mGroupList;
    }
    
    public String getName() {
        return this.mName;
    }
    
    public Map<String, Collection<String>> getParameterMap() {
        return this.mParameterMap;
    }
    
    public Collection<String> getParameters(final String s) {
        return this.mParameterMap.get(s);
    }
    
    public String getRawValue() {
        return this.mRawValue;
    }
    
    public List<String> getValueList() {
        return this.mValueList;
    }
    
    public void setByteValue(final byte[] mByteValue) {
        this.mByteValue = mByteValue;
    }
    
    public void setName(final String mName) {
        if (this.mName != null) {
            Log.w("vCard", String.format("Property name is re-defined (existing: %s, requested: %s", this.mName, mName));
        }
        this.mName = mName;
    }
    
    public void setParameter(final String s, final String s2) {
        this.mParameterMap.clear();
        this.addParameter(s, s2);
    }
    
    public void setRawValue(final String mRawValue) {
        this.mRawValue = mRawValue;
    }
    
    public void setValues(final List<String> mValueList) {
        this.mValueList = mValueList;
    }
    
    public void setValues(final String... array) {
        this.mValueList = Arrays.<String>asList(array);
    }
}
