package com.navdy.hud.app.bluetooth.pbap;

import com.navdy.hud.app.bluetooth.obex.HeaderSet;
import java.io.IOException;
import android.util.Log;
import java.io.InputStream;
import com.navdy.hud.app.bluetooth.vcard.VCardEntry;
import java.util.ArrayList;
import com.navdy.hud.app.bluetooth.pbap.utils.ObexAppParameters;

final class BluetoothPbapRequestPullPhoneBook extends BluetoothPbapRequest
{
    private static final String TAG = "BTPbapReqPullPBook";
    private static final String TYPE = "x-bt/phonebook";
    private final byte mFormat;
    private int mNewMissedCalls;
    private BluetoothPbapVcardList mResponse;
    
    public BluetoothPbapRequestPullPhoneBook(final String s, final long n, final byte b, final int n2, final int n3) {
        this.mNewMissedCalls = -1;
        if (n2 < 0 || n2 > 65535) {
            throw new IllegalArgumentException("maxListCount should be [0..65535]");
        }
        if (n3 < 0 || n3 > 65535) {
            throw new IllegalArgumentException("listStartOffset should be [0..65535]");
        }
        this.mHeaderSet.setHeader(1, s);
        this.mHeaderSet.setHeader(66, "x-bt/phonebook");
        final ObexAppParameters obexAppParameters = new ObexAppParameters();
        byte mFormat = b;
        if (b != 0 && (mFormat = b) != 1) {
            mFormat = 0;
        }
        if (n != 0L) {
            obexAppParameters.add((byte)6, n);
        }
        obexAppParameters.add((byte)7, mFormat);
        if (n2 > 0) {
            obexAppParameters.add((byte)4, (short)n2);
        }
        else {
            obexAppParameters.add((byte)4, (short)(-1));
        }
        if (n3 > 0) {
            obexAppParameters.add((byte)5, (short)n3);
        }
        obexAppParameters.addToHeaderSet(this.mHeaderSet);
        this.mFormat = mFormat;
    }
    
    public ArrayList<VCardEntry> getList() {
        return this.mResponse.getList();
    }
    
    public int getNewMissedCalls() {
        return this.mNewMissedCalls;
    }
    
    @Override
    protected void readResponse(final InputStream inputStream) throws IOException {
        Log.v("BTPbapReqPullPBook", "readResponse");
        this.mResponse = new BluetoothPbapVcardList(inputStream, this.mFormat);
    }
    
    @Override
    protected void readResponseHeaders(final HeaderSet set) {
        Log.v("BTPbapReqPullPBook", "readResponse");
        final ObexAppParameters fromHeaderSet = ObexAppParameters.fromHeaderSet(set);
        if (fromHeaderSet.exists((byte)9)) {
            this.mNewMissedCalls = fromHeaderSet.getByte((byte)9);
        }
    }
}
