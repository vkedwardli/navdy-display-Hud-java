package com.navdy.hud.app.bluetooth.vcard;

import java.io.ByteArrayOutputStream;
import android.text.Editable;
import android.telephony.PhoneNumberUtils;
import android.text.SpannableStringBuilder;
import com.navdy.hud.app.bluetooth.vcard.exception.VCardException;
import java.nio.ByteBuffer;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.util.Iterator;
import android.util.Log;
import java.util.ArrayList;
import java.util.List;
import android.text.TextUtils;
import java.util.Collection;
import java.util.Arrays;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Set;
import java.util.Map;

public class VCardUtils
{
    private static final String LOG_TAG = "vCard";
    private static final int[] sEscapeIndicatorsV30;
    private static final int[] sEscapeIndicatorsV40;
    private static final Map<Integer, String> sKnownImPropNameMap_ItoS;
    private static final Map<String, Integer> sKnownPhoneTypeMap_StoI;
    private static final Map<Integer, String> sKnownPhoneTypesMap_ItoS;
    private static final Set<String> sMobilePhoneLabelSet;
    private static final Set<String> sPhoneTypesUnknownToContactsSet;
    private static final Set<Character> sUnAcceptableAsciiInV21WordSet;
    
    static {
        sKnownPhoneTypesMap_ItoS = new HashMap<Integer, String>();
        sKnownPhoneTypeMap_StoI = new HashMap<String, Integer>();
        VCardUtils.sKnownPhoneTypesMap_ItoS.put(9, "CAR");
        VCardUtils.sKnownPhoneTypeMap_StoI.put("CAR", 9);
        VCardUtils.sKnownPhoneTypesMap_ItoS.put(6, "PAGER");
        VCardUtils.sKnownPhoneTypeMap_StoI.put("PAGER", 6);
        VCardUtils.sKnownPhoneTypesMap_ItoS.put(11, "ISDN");
        VCardUtils.sKnownPhoneTypeMap_StoI.put("ISDN", 11);
        VCardUtils.sKnownPhoneTypeMap_StoI.put("HOME", 1);
        VCardUtils.sKnownPhoneTypeMap_StoI.put("WORK", 3);
        VCardUtils.sKnownPhoneTypeMap_StoI.put("CELL", 2);
        VCardUtils.sKnownPhoneTypeMap_StoI.put("OTHER", 7);
        VCardUtils.sKnownPhoneTypeMap_StoI.put("CALLBACK", 8);
        VCardUtils.sKnownPhoneTypeMap_StoI.put("COMPANY-MAIN", 10);
        VCardUtils.sKnownPhoneTypeMap_StoI.put("RADIO", 14);
        VCardUtils.sKnownPhoneTypeMap_StoI.put("TTY-TDD", 16);
        VCardUtils.sKnownPhoneTypeMap_StoI.put("ASSISTANT", 19);
        VCardUtils.sKnownPhoneTypeMap_StoI.put("VOICE", 7);
        (sPhoneTypesUnknownToContactsSet = new HashSet<String>()).add("MODEM");
        VCardUtils.sPhoneTypesUnknownToContactsSet.add("MSG");
        VCardUtils.sPhoneTypesUnknownToContactsSet.add("BBS");
        VCardUtils.sPhoneTypesUnknownToContactsSet.add("VIDEO");
        (sKnownImPropNameMap_ItoS = new HashMap<Integer, String>()).put(0, "X-AIM");
        VCardUtils.sKnownImPropNameMap_ItoS.put(1, "X-MSN");
        VCardUtils.sKnownImPropNameMap_ItoS.put(2, "X-YAHOO");
        VCardUtils.sKnownImPropNameMap_ItoS.put(3, "X-SKYPE-USERNAME");
        VCardUtils.sKnownImPropNameMap_ItoS.put(5, "X-GOOGLE-TALK");
        VCardUtils.sKnownImPropNameMap_ItoS.put(6, "X-ICQ");
        VCardUtils.sKnownImPropNameMap_ItoS.put(7, "X-JABBER");
        VCardUtils.sKnownImPropNameMap_ItoS.put(4, "X-QQ");
        VCardUtils.sKnownImPropNameMap_ItoS.put(8, "X-NETMEETING");
        sMobilePhoneLabelSet = new HashSet<String>(Arrays.<String>asList("MOBILE", "携帯電話", "携帯", "ケイタイ", "ｹｲﾀｲ"));
        sUnAcceptableAsciiInV21WordSet = new HashSet<Character>(Arrays.<Character>asList('[', ']', '=', ':', '.', ',', ' '));
        sEscapeIndicatorsV30 = new int[] { 58, 59, 44, 32 };
        sEscapeIndicatorsV40 = new int[] { 59, 58 };
    }
    
    public static boolean appearsLikeAndroidVCardQuotedPrintable(final String s) {
        final boolean b = false;
        final int n = s.length() % 3;
        boolean b2 = b;
        if (s.length() >= 2) {
            if (n != 1 && n != 0) {
                b2 = b;
            }
            else {
                for (int i = 0; i < s.length(); i += 3) {
                    b2 = b;
                    if (s.charAt(i) != '=') {
                        return b2;
                    }
                }
                b2 = true;
            }
        }
        return b2;
    }
    
    public static boolean areAllEmpty(final String... array) {
        final boolean b = true;
        boolean b2;
        if (array == null) {
            b2 = b;
        }
        else {
            final int length = array.length;
            int n = 0;
            while (true) {
                b2 = b;
                if (n >= length) {
                    return b2;
                }
                if (!TextUtils.isEmpty((CharSequence)array[n])) {
                    break;
                }
                ++n;
            }
            b2 = false;
        }
        return b2;
    }
    
    public static List<String> constructListFromValue(final String s, final int n) {
        final ArrayList<String> list = new ArrayList<String>();
        StringBuilder sb = new StringBuilder();
        for (int length = s.length(), i = 0; i < length; ++i) {
            final char char1 = s.charAt(i);
            if (char1 == '\\' && i < length - 1) {
                final char char2 = s.charAt(i + 1);
                String s2;
                if (VCardConfig.isVersion40(n)) {
                    s2 = VCardParserImpl_V40.unescapeCharacter(char2);
                }
                else if (VCardConfig.isVersion30(n)) {
                    s2 = VCardParserImpl_V30.unescapeCharacter(char2);
                }
                else {
                    if (!VCardConfig.isVersion21(n)) {
                        Log.w("vCard", "Unknown vCard type");
                    }
                    s2 = VCardParserImpl_V21.unescapeCharacter(char2);
                }
                if (s2 != null) {
                    sb.append(s2);
                    ++i;
                }
                else {
                    sb.append(char1);
                }
            }
            else if (char1 == ';') {
                list.add(sb.toString());
                sb = new StringBuilder();
            }
            else {
                sb.append(char1);
            }
        }
        list.add(sb.toString());
        return list;
    }
    
    public static String constructNameFromElements(final int n, final String s, final String s2, final String s3) {
        return constructNameFromElements(n, s, s2, s3, null, null);
    }
    
    public static String constructNameFromElements(int n, final String s, String s2, final String s3, final String s4, final String s5) {
        final StringBuilder sb = new StringBuilder();
        final String[] sortNameElements = sortNameElements(n, s, s2, s3);
        n = 1;
        if (!TextUtils.isEmpty((CharSequence)s4)) {
            n = 0;
            sb.append(s4);
        }
        int n2;
        for (int length = sortNameElements.length, i = 0; i < length; ++i, n = n2) {
            s2 = sortNameElements[i];
            n2 = n;
            if (!TextUtils.isEmpty((CharSequence)s2)) {
                if (n != 0) {
                    n = 0;
                }
                else {
                    sb.append(' ');
                }
                sb.append(s2);
                n2 = n;
            }
        }
        if (!TextUtils.isEmpty((CharSequence)s5)) {
            if (n == 0) {
                sb.append(' ');
            }
            sb.append(s5);
        }
        return sb.toString();
    }
    
    public static boolean containsOnlyAlphaDigitHyphen(final Collection<String> collection) {
        final boolean b = true;
        boolean b2;
        if (collection == null) {
            b2 = b;
        }
        else {
            final Iterator<String> iterator = collection.iterator();
        Block_8:
            while (true) {
                b2 = b;
                if (!iterator.hasNext()) {
                    return b2;
                }
                final String s = iterator.next();
                if (TextUtils.isEmpty((CharSequence)s)) {
                    continue;
                }
                for (int length = s.length(), i = 0; i < length; i = s.offsetByCodePoints(i, 1)) {
                    final int codePoint = s.codePointAt(i);
                    if ((97 > codePoint || codePoint >= 123) && (65 > codePoint || codePoint >= 91) && (48 > codePoint || codePoint >= 58) && codePoint != 45) {
                        break Block_8;
                    }
                }
            }
            b2 = false;
        }
        return b2;
    }
    
    public static boolean containsOnlyAlphaDigitHyphen(final String... array) {
        return array == null || containsOnlyAlphaDigitHyphen(Arrays.<String>asList(array));
    }
    
    public static boolean containsOnlyNonCrLfPrintableAscii(final Collection<String> collection) {
        final boolean b = true;
        boolean b2;
        if (collection == null) {
            b2 = b;
        }
        else {
            final Iterator<String> iterator = collection.iterator();
        Label_0083:
            while (true) {
                b2 = b;
                if (!iterator.hasNext()) {
                    return b2;
                }
                final String s = iterator.next();
                if (TextUtils.isEmpty((CharSequence)s)) {
                    continue;
                }
                for (int length = s.length(), i = 0; i < length; i = s.offsetByCodePoints(i, 1)) {
                    final int codePoint = s.codePointAt(i);
                    if (32 > codePoint || codePoint > 126) {
                        break Label_0083;
                    }
                }
            }
            b2 = false;
        }
        return b2;
    }
    
    public static boolean containsOnlyNonCrLfPrintableAscii(final String... array) {
        return array == null || containsOnlyNonCrLfPrintableAscii(Arrays.<String>asList(array));
    }
    
    public static boolean containsOnlyPrintableAscii(final Collection<String> collection) {
        final boolean b = true;
        boolean b2;
        if (collection == null) {
            b2 = b;
        }
        else {
            final Iterator<String> iterator = collection.iterator();
            String s;
            do {
                b2 = b;
                if (!iterator.hasNext()) {
                    return b2;
                }
                s = iterator.next();
            } while (TextUtils.isEmpty((CharSequence)s) || TextUtilsPort.isPrintableAsciiOnly(s));
            b2 = false;
        }
        return b2;
    }
    
    public static boolean containsOnlyPrintableAscii(final String... array) {
        return array == null || containsOnlyPrintableAscii(Arrays.<String>asList(array));
    }
    
    public static boolean containsOnlyWhiteSpaces(final Collection<String> collection) {
        final boolean b = true;
        boolean b2;
        if (collection == null) {
            b2 = b;
        }
        else {
            final Iterator<String> iterator = collection.iterator();
        Block_5:
            while (true) {
                b2 = b;
                if (!iterator.hasNext()) {
                    return b2;
                }
                final String s = iterator.next();
                if (TextUtils.isEmpty((CharSequence)s)) {
                    continue;
                }
                for (int length = s.length(), i = 0; i < length; i = s.offsetByCodePoints(i, 1)) {
                    if (!Character.isWhitespace(s.codePointAt(i))) {
                        break Block_5;
                    }
                }
            }
            b2 = false;
        }
        return b2;
    }
    
    public static boolean containsOnlyWhiteSpaces(final String... array) {
        return array == null || containsOnlyWhiteSpaces(Arrays.<String>asList(array));
    }
    
    public static final String convertStringCharset(String s, final String s2, final String s3) {
        if (!s2.equalsIgnoreCase(s3)) {
            final ByteBuffer encode = Charset.forName(s2).encode(s);
            final byte[] array = new byte[encode.remaining()];
            encode.get(array);
            try {
                s = new String(array, s3);
            }
            catch (UnsupportedEncodingException ex) {
                Log.e("vCard", "Failed to encode: charset=" + s3);
                s = null;
            }
        }
        return s;
    }
    
    public static final VCardParser getAppropriateParser(final int n) throws VCardException {
        VCardParser vCardParser;
        if (VCardConfig.isVersion21(n)) {
            vCardParser = new VCardParser_V21();
        }
        else if (VCardConfig.isVersion30(n)) {
            vCardParser = new VCardParser_V30();
        }
        else {
            if (!VCardConfig.isVersion40(n)) {
                throw new VCardException("Version is not specified");
            }
            vCardParser = new VCardParser_V40();
        }
        return vCardParser;
    }
    
    public static int getPhoneNumberFormat(int n) {
        if (VCardConfig.isJapaneseDevice(n)) {
            n = 2;
        }
        else {
            n = 1;
        }
        return n;
    }
    
    public static Object getPhoneTypeFromStrings(final Collection<String> collection, String s) {
        String s2 = s;
        if (s == null) {
            s2 = "";
        }
        int intValue = -1;
        s = null;
        final String s3 = null;
        int n = 0;
        int n2 = 0;
        int n3 = 0;
        int n4 = 0;
        int n5 = intValue;
        if (collection != null) {
            final Iterator<String> iterator = collection.iterator();
            String s4 = s3;
            while (true) {
                n3 = n4;
                n = n2;
                s = s4;
                n5 = intValue;
                if (!iterator.hasNext()) {
                    break;
                }
                s = iterator.next();
                if (s == null) {
                    continue;
                }
                final String upperCase = s.toUpperCase();
                if (upperCase.equals("PREF")) {
                    n4 = 1;
                }
                else if (upperCase.equals("FAX")) {
                    n2 = 1;
                }
                else {
                    if (upperCase.startsWith("X-") && intValue < 0) {
                        s = s.substring(2);
                    }
                    if (s.length() == 0) {
                        continue;
                    }
                    final Integer n6 = VCardUtils.sKnownPhoneTypeMap_StoI.get(s.toUpperCase());
                    if (n6 != null) {
                        final int intValue2 = n6;
                        final int index = s2.indexOf("@");
                        if ((intValue2 != 6 || index <= 0 || index >= s2.length() - 1) && intValue >= 0 && intValue != 0 && intValue != 7) {
                            continue;
                        }
                        intValue = n6;
                    }
                    else {
                        if (intValue >= 0) {
                            continue;
                        }
                        intValue = 0;
                        s4 = s;
                    }
                }
            }
        }
        int n7;
        if ((n7 = n5) < 0) {
            if (n3 != 0) {
                n7 = 12;
            }
            else {
                n7 = 1;
            }
        }
        int n8 = n7;
        if (n != 0) {
            if (n7 == 1) {
                n8 = 5;
            }
            else if (n7 == 3) {
                n8 = 4;
            }
            else if ((n8 = n7) == 7) {
                n8 = 13;
            }
        }
        if (n8 != 0) {
            s = (String)n8;
        }
        return s;
    }
    
    public static String getPhoneTypeString(final Integer n) {
        return VCardUtils.sKnownPhoneTypesMap_ItoS.get(n);
    }
    
    public static String getPropertyNameForIm(final int n) {
        return VCardUtils.sKnownImPropNameMap_ItoS.get(n);
    }
    
    public static String guessImageType(final byte[] array) {
        String s;
        if (array == null) {
            s = null;
        }
        else if (array.length >= 3 && array[0] == 71 && array[1] == 73 && array[2] == 70) {
            s = "GIF";
        }
        else if (array.length >= 4 && array[0] == -119 && array[1] == 80 && array[2] == 78 && array[3] == 71) {
            s = "PNG";
        }
        else if (array.length >= 2 && array[0] == -1 && array[1] == -40) {
            s = "JPEG";
        }
        else {
            s = null;
        }
        return s;
    }
    
    public static boolean isMobilePhoneLabel(final String s) {
        return "_AUTO_CELL".equals(s) || VCardUtils.sMobilePhoneLabelSet.contains(s);
    }
    
    public static boolean isV21Word(final String s) {
        final boolean b = true;
        boolean b2;
        if (TextUtils.isEmpty((CharSequence)s)) {
            b2 = b;
        }
        else {
            final int length = s.length();
            int offsetByCodePoints = 0;
            while (true) {
                b2 = b;
                if (offsetByCodePoints >= length) {
                    return b2;
                }
                final int codePoint = s.codePointAt(offsetByCodePoints);
                if (32 > codePoint || codePoint > 126 || VCardUtils.sUnAcceptableAsciiInV21WordSet.contains((char)codePoint)) {
                    break;
                }
                offsetByCodePoints = s.offsetByCodePoints(offsetByCodePoints, 1);
            }
            b2 = false;
        }
        return b2;
    }
    
    public static boolean isValidInV21ButUnknownToContactsPhoteType(final String s) {
        return VCardUtils.sPhoneTypesUnknownToContactsSet.contains(s);
    }
    
    public static String parseQuotedPrintable(final String p0, final boolean p1, final String p2, final String p3) {
        // 
        This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     3: dup            
        //     4: invokespecial   java/lang/StringBuilder.<init>:()V
        //     7: astore          4
        //     9: aload_0        
        //    10: invokevirtual   java/lang/String.length:()I
        //    13: istore          5
        //    15: iconst_0       
        //    16: istore          6
        //    18: iload           6
        //    20: iload           5
        //    22: if_icmpge       101
        //    25: aload_0        
        //    26: iload           6
        //    28: invokevirtual   java/lang/String.charAt:(I)C
        //    31: istore          7
        //    33: iload           7
        //    35: bipush          61
        //    37: if_icmpne       90
        //    40: iload           6
        //    42: iload           5
        //    44: iconst_1       
        //    45: isub           
        //    46: if_icmpge       90
        //    49: aload_0        
        //    50: iload           6
        //    52: iconst_1       
        //    53: iadd           
        //    54: invokevirtual   java/lang/String.charAt:(I)C
        //    57: istore          8
        //    59: iload           8
        //    61: bipush          32
        //    63: if_icmpeq       73
        //    66: iload           8
        //    68: bipush          9
        //    70: if_icmpne       90
        //    73: aload           4
        //    75: iload           8
        //    77: invokevirtual   java/lang/StringBuilder.append:(C)Ljava/lang/StringBuilder;
        //    80: pop            
        //    81: iinc            6, 1
        //    84: iinc            6, 1
        //    87: goto            18
        //    90: aload           4
        //    92: iload           7
        //    94: invokevirtual   java/lang/StringBuilder.append:(C)Ljava/lang/StringBuilder;
        //    97: pop            
        //    98: goto            84
        //   101: aload           4
        //   103: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   106: astore          9
        //   108: iload_1        
        //   109: ifeq            194
        //   112: aload           9
        //   114: ldc_w           "\r\n"
        //   117: invokevirtual   java/lang/String.split:(Ljava/lang/String;)[Ljava/lang/String;
        //   120: astore_0       
        //   121: new             Ljava/lang/StringBuilder;
        //   124: dup            
        //   125: invokespecial   java/lang/StringBuilder.<init>:()V
        //   128: astore          9
        //   130: aload_0        
        //   131: arraylength    
        //   132: istore          5
        //   134: iconst_0       
        //   135: istore          6
        //   137: iload           6
        //   139: iload           5
        //   141: if_icmpge       399
        //   144: aload_0        
        //   145: iload           6
        //   147: aaload         
        //   148: astore          10
        //   150: aload           10
        //   152: astore          4
        //   154: aload           10
        //   156: ldc_w           "="
        //   159: invokevirtual   java/lang/String.endsWith:(Ljava/lang/String;)Z
        //   162: ifeq            180
        //   165: aload           10
        //   167: iconst_0       
        //   168: aload           10
        //   170: invokevirtual   java/lang/String.length:()I
        //   173: iconst_1       
        //   174: isub           
        //   175: invokevirtual   java/lang/String.substring:(II)Ljava/lang/String;
        //   178: astore          4
        //   180: aload           9
        //   182: aload           4
        //   184: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   187: pop            
        //   188: iinc            6, 1
        //   191: goto            137
        //   194: new             Ljava/lang/StringBuilder;
        //   197: dup            
        //   198: invokespecial   java/lang/StringBuilder.<init>:()V
        //   201: astore_0       
        //   202: aload           9
        //   204: invokevirtual   java/lang/String.length:()I
        //   207: istore          11
        //   209: new             Ljava/util/ArrayList;
        //   212: dup            
        //   213: invokespecial   java/util/ArrayList.<init>:()V
        //   216: astore          10
        //   218: iconst_0       
        //   219: istore          6
        //   221: iload           6
        //   223: iload           11
        //   225: if_icmpge       364
        //   228: aload           9
        //   230: iload           6
        //   232: invokevirtual   java/lang/String.charAt:(I)C
        //   235: istore          8
        //   237: iload           8
        //   239: bipush          10
        //   241: if_icmpne       275
        //   244: aload           10
        //   246: aload_0        
        //   247: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   250: invokevirtual   java/util/ArrayList.add:(Ljava/lang/Object;)Z
        //   253: pop            
        //   254: new             Ljava/lang/StringBuilder;
        //   257: dup            
        //   258: invokespecial   java/lang/StringBuilder.<init>:()V
        //   261: astore_0       
        //   262: iload           6
        //   264: istore          5
        //   266: iload           5
        //   268: iconst_1       
        //   269: iadd           
        //   270: istore          6
        //   272: goto            221
        //   275: iload           8
        //   277: bipush          13
        //   279: if_icmpne       350
        //   282: aload           10
        //   284: aload_0        
        //   285: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   288: invokevirtual   java/util/ArrayList.add:(Ljava/lang/Object;)Z
        //   291: pop            
        //   292: new             Ljava/lang/StringBuilder;
        //   295: dup            
        //   296: invokespecial   java/lang/StringBuilder.<init>:()V
        //   299: astore          4
        //   301: aload           4
        //   303: astore_0       
        //   304: iload           6
        //   306: istore          5
        //   308: iload           6
        //   310: iload           11
        //   312: iconst_1       
        //   313: isub           
        //   314: if_icmpge       266
        //   317: aload           4
        //   319: astore_0       
        //   320: iload           6
        //   322: istore          5
        //   324: aload           9
        //   326: iload           6
        //   328: iconst_1       
        //   329: iadd           
        //   330: invokevirtual   java/lang/String.charAt:(I)C
        //   333: bipush          10
        //   335: if_icmpne       266
        //   338: iload           6
        //   340: iconst_1       
        //   341: iadd           
        //   342: istore          5
        //   344: aload           4
        //   346: astore_0       
        //   347: goto            266
        //   350: aload_0        
        //   351: iload           8
        //   353: invokevirtual   java/lang/StringBuilder.append:(C)Ljava/lang/StringBuilder;
        //   356: pop            
        //   357: iload           6
        //   359: istore          5
        //   361: goto            266
        //   364: aload_0        
        //   365: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   368: astore_0       
        //   369: aload_0        
        //   370: invokevirtual   java/lang/String.length:()I
        //   373: ifle            383
        //   376: aload           10
        //   378: aload_0        
        //   379: invokevirtual   java/util/ArrayList.add:(Ljava/lang/Object;)Z
        //   382: pop            
        //   383: aload           10
        //   385: iconst_0       
        //   386: anewarray       Ljava/lang/String;
        //   389: invokevirtual   java/util/ArrayList.toArray:([Ljava/lang/Object;)[Ljava/lang/Object;
        //   392: checkcast       [Ljava/lang/String;
        //   395: astore_0       
        //   396: goto            121
        //   399: aload           9
        //   401: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   404: astore          4
        //   406: aload           4
        //   408: invokestatic    android/text/TextUtils.isEmpty:(Ljava/lang/CharSequence;)Z
        //   411: ifeq            423
        //   414: ldc             "vCard"
        //   416: ldc_w           "Given raw string is empty."
        //   419: invokestatic    android/util/Log.w:(Ljava/lang/String;Ljava/lang/String;)I
        //   422: pop            
        //   423: aload           4
        //   425: aload_2        
        //   426: invokevirtual   java/lang/String.getBytes:(Ljava/lang/String;)[B
        //   429: astore_0       
        //   430: aload_0        
        //   431: invokestatic    com/navdy/hud/app/bluetooth/vcard/VCardUtils$QuotedPrintableCodecPort.decodeQuotedPrintable:([B)[B
        //   434: astore_2       
        //   435: aload_2        
        //   436: astore_0       
        //   437: new             Ljava/lang/String;
        //   440: astore_2       
        //   441: aload_2        
        //   442: aload_0        
        //   443: aload_3        
        //   444: invokespecial   java/lang/String.<init>:([BLjava/lang/String;)V
        //   447: aload_2        
        //   448: astore_0       
        //   449: aload_0        
        //   450: areturn        
        //   451: astore_0       
        //   452: ldc             "vCard"
        //   454: new             Ljava/lang/StringBuilder;
        //   457: dup            
        //   458: invokespecial   java/lang/StringBuilder.<init>:()V
        //   461: ldc_w           "Failed to decode: "
        //   464: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   467: aload_2        
        //   468: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   471: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   474: invokestatic    android/util/Log.w:(Ljava/lang/String;Ljava/lang/String;)I
        //   477: pop            
        //   478: aload           4
        //   480: invokevirtual   java/lang/String.getBytes:()[B
        //   483: astore_0       
        //   484: goto            430
        //   487: astore_2       
        //   488: ldc             "vCard"
        //   490: ldc_w           "DecoderException is thrown."
        //   493: invokestatic    android/util/Log.e:(Ljava/lang/String;Ljava/lang/String;)I
        //   496: pop            
        //   497: goto            437
        //   500: astore_2       
        //   501: ldc             "vCard"
        //   503: new             Ljava/lang/StringBuilder;
        //   506: dup            
        //   507: invokespecial   java/lang/StringBuilder.<init>:()V
        //   510: ldc_w           "Failed to encode: charset="
        //   513: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   516: aload_3        
        //   517: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   520: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   523: invokestatic    android/util/Log.e:(Ljava/lang/String;Ljava/lang/String;)I
        //   526: pop            
        //   527: new             Ljava/lang/String;
        //   530: dup            
        //   531: aload_0        
        //   532: invokespecial   java/lang/String.<init>:([B)V
        //   535: astore_0       
        //   536: goto            449
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                                                           
        //  -----  -----  -----  -----  ---------------------------------------------------------------
        //  423    430    451    487    Ljava/io/UnsupportedEncodingException;
        //  430    435    487    500    Lcom/navdy/hud/app/bluetooth/vcard/VCardUtils$DecoderException;
        //  437    447    500    539    Ljava/io/UnsupportedEncodingException;
        // 
        // The error that occurred was:
        // 
        // java.lang.IndexOutOfBoundsException: Index: 264, Size: 264
        //     at java.util.ArrayList.rangeCheck(Unknown Source)
        //     at java.util.ArrayList.get(Unknown Source)
        //     at com.strobel.decompiler.ast.AstBuilder.convertToAst(AstBuilder.java:3321)
        //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:113)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:210)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public static String[] sortNameElements(final int n, final String s, final String s2, final String s3) {
        final String[] array = new String[3];
        switch (VCardConfig.getNameOrderType(n)) {
            default:
                array[0] = s3;
                array[1] = s2;
                array[2] = s;
                break;
            case 8:
                if (containsOnlyPrintableAscii(s) && containsOnlyPrintableAscii(s3)) {
                    array[0] = s3;
                    array[1] = s2;
                    array[2] = s;
                    break;
                }
                array[0] = s;
                array[1] = s2;
                array[2] = s3;
                break;
            case 4:
                array[0] = s2;
                array[1] = s3;
                array[2] = s;
                break;
        }
        return array;
    }
    
    public static String toHalfWidthString(String string) {
        if (TextUtils.isEmpty((CharSequence)string)) {
            string = null;
        }
        else {
            final StringBuilder sb = new StringBuilder();
            for (int length = string.length(), i = 0; i < length; i = string.offsetByCodePoints(i, 1)) {
                final char char1 = string.charAt(i);
                final String tryGetHalfWidthText = JapaneseUtils.tryGetHalfWidthText(char1);
                if (tryGetHalfWidthText != null) {
                    sb.append(tryGetHalfWidthText);
                }
                else {
                    sb.append(char1);
                }
            }
            string = sb.toString();
        }
        return string;
    }
    
    private static String toStringAsParamValue(String string, final int[] array) {
        String s = string;
        if (TextUtils.isEmpty((CharSequence)string)) {
            s = "";
        }
        final StringBuilder sb = new StringBuilder();
        final int length = s.length();
        int n = 0;
        int n2;
    Label_0068:
        for (int i = 0; i < length; i = s.offsetByCodePoints(i, 1), n = n2) {
            final int codePoint = s.codePointAt(i);
            n2 = n;
            if (codePoint >= 32) {
                if (codePoint == 34) {
                    n2 = n;
                }
                else {
                    sb.appendCodePoint(codePoint);
                    final int length2 = array.length;
                    int n3 = 0;
                    while (true) {
                        n2 = n;
                        if (n3 >= length2) {
                            continue Label_0068;
                        }
                        if (codePoint == array[n3]) {
                            break;
                        }
                        ++n3;
                    }
                    n2 = 1;
                }
            }
        }
        final String string2 = sb.toString();
        if (string2.isEmpty() || containsOnlyWhiteSpaces(string2)) {
            string = "";
        }
        else {
            string = string2;
            if (n != 0) {
                string = '\"' + string2 + '\"';
            }
        }
        return string;
    }
    
    public static String toStringAsV30ParamValue(final String s) {
        return toStringAsParamValue(s, VCardUtils.sEscapeIndicatorsV30);
    }
    
    public static String toStringAsV40ParamValue(final String s) {
        return toStringAsParamValue(s, VCardUtils.sEscapeIndicatorsV40);
    }
    
    private static class DecoderException extends Exception
    {
        public DecoderException(final String s) {
            super(s);
        }
    }
    
    public static class PhoneNumberUtilsPort
    {
        public static String formatNumber(final String s, final int n) {
            final SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder((CharSequence)s);
            PhoneNumberUtils.formatNumber((Editable)spannableStringBuilder, n);
            return spannableStringBuilder.toString();
        }
    }
    
    private static class QuotedPrintableCodecPort
    {
        private static byte ESCAPE_CHAR;
        
        static {
            QuotedPrintableCodecPort.ESCAPE_CHAR = 61;
        }
        
        public static final byte[] decodeQuotedPrintable(byte[] byteArray) throws DecoderException {
            if (byteArray == null) {
                byteArray = null;
            }
            else {
                final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                for (int i = 0; i < byteArray.length; ++i) {
                    final byte b = byteArray[i];
                    if (b == QuotedPrintableCodecPort.ESCAPE_CHAR) {
                        ++i;
                        int digit = 0;
                        int digit2 = 0;
                        try {
                            digit = Character.digit((char)byteArray[i], 16);
                            ++i;
                            try {
                                digit2 = Character.digit((char)byteArray[i], 16);
                                if (digit == -1 || digit2 == -1) {
                                    throw new DecoderException("Invalid quoted-printable encoding");
                                }
                            }
                            catch (ArrayIndexOutOfBoundsException ex) {
                                throw new DecoderException("Invalid quoted-printable encoding");
                            }
                        }
                        catch (ArrayIndexOutOfBoundsException ex2) {}
                        byteArrayOutputStream.write((char)((digit << 4) + digit2));
                    }
                    else {
                        byteArrayOutputStream.write(b);
                    }
                }
                byteArray = byteArrayOutputStream.toByteArray();
            }
            return byteArray;
        }
    }
    
    public static class TextUtilsPort
    {
        public static boolean isPrintableAscii(final char c) {
            return (' ' <= c && c <= '~') || c == '\r' || c == '\n';
        }
        
        public static boolean isPrintableAsciiOnly(final CharSequence charSequence) {
            for (int length = charSequence.length(), i = 0; i < length; ++i) {
                if (!isPrintableAscii(charSequence.charAt(i))) {
                    return false;
                }
            }
            return true;
        }
    }
}
