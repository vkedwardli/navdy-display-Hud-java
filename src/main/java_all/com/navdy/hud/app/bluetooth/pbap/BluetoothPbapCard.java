package com.navdy.hud.app.bluetooth.pbap;

import java.util.Iterator;
import java.util.List;
import org.json.JSONException;
import org.json.JSONArray;
import org.json.JSONObject;
import com.navdy.hud.app.bluetooth.vcard.VCardEntry;

public class BluetoothPbapCard
{
    public final String N;
    public final String firstName;
    public final String handle;
    public final String lastName;
    public final String middleName;
    public final String prefix;
    public final String suffix;
    
    public BluetoothPbapCard(String s, final String n) {
        final String s2 = null;
        this.handle = s;
        this.N = n;
        final String[] split = n.split(";", 5);
        if (split.length < 1) {
            s = null;
        }
        else {
            s = split[0];
        }
        this.lastName = s;
        if (split.length < 2) {
            s = null;
        }
        else {
            s = split[1];
        }
        this.firstName = s;
        if (split.length < 3) {
            s = null;
        }
        else {
            s = split[2];
        }
        this.middleName = s;
        if (split.length < 4) {
            s = null;
        }
        else {
            s = split[3];
        }
        this.prefix = s;
        if (split.length < 5) {
            s = s2;
        }
        else {
            s = split[4];
        }
        this.suffix = s;
    }
    
    public static String jsonifyVcardEntry(VCardEntry vCardEntry) {
        final JSONObject jsonObject = new JSONObject();
        while (true) {
            try {
                final VCardEntry.NameData nameData = vCardEntry.getNameData();
                jsonObject.put("formatted", nameData.getFormatted());
                jsonObject.put("family", nameData.getFamily());
                jsonObject.put("given", nameData.getGiven());
                jsonObject.put("middle", nameData.getMiddle());
                jsonObject.put("prefix", nameData.getPrefix());
                jsonObject.put("suffix", nameData.getSuffix());
                while (true) {
                    JSONArray jsonArray = null;
                    Label_0311: {
                        try {
                            jsonArray = new JSONArray();
                            final List<VCardEntry.PhoneData> phoneList = vCardEntry.getPhoneList();
                            if (phoneList != null) {
                                for (final VCardEntry.PhoneData phoneData : phoneList) {
                                    final JSONObject jsonObject2 = new JSONObject();
                                    jsonObject2.put("type", phoneData.getType());
                                    jsonObject2.put("number", phoneData.getNumber());
                                    jsonObject2.put("label", phoneData.getLabel());
                                    jsonObject2.put("is_primary", phoneData.isPrimary());
                                    jsonArray.put(jsonObject2);
                                }
                                break Label_0311;
                            }
                        }
                        catch (JSONException ex) {}
                        JSONArray jsonArray2 = null;
                        Label_0322: {
                            try {
                                jsonArray2 = new JSONArray();
                                vCardEntry = (VCardEntry)vCardEntry.getEmailList();
                                if (vCardEntry != null) {
                                    vCardEntry = (VCardEntry)((List<Object>)vCardEntry).iterator();
                                    while (((Iterator)vCardEntry).hasNext()) {
                                        final VCardEntry.EmailData emailData = ((Iterator<VCardEntry.EmailData>)vCardEntry).next();
                                        final JSONObject jsonObject3 = new JSONObject();
                                        jsonObject3.put("type", emailData.getType());
                                        jsonObject3.put("address", emailData.getAddress());
                                        jsonObject3.put("label", emailData.getLabel());
                                        jsonObject3.put("is_primary", emailData.isPrimary());
                                        jsonArray2.put(jsonObject3);
                                    }
                                    break Label_0322;
                                }
                            }
                            catch (JSONException ex2) {}
                            return jsonObject.toString();
                        }
                        jsonObject.put("emails", jsonArray2);
                        return jsonObject.toString();
                    }
                    jsonObject.put("phones", jsonArray);
                    continue;
                }
            }
            catch (JSONException ex3) {
                continue;
            }
            break;
        }
    }
    
    @Override
    public String toString() {
        final JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("handle", this.handle);
            jsonObject.put("N", this.N);
            jsonObject.put("lastName", this.lastName);
            jsonObject.put("firstName", this.firstName);
            jsonObject.put("middleName", this.middleName);
            jsonObject.put("prefix", this.prefix);
            jsonObject.put("suffix", this.suffix);
            return jsonObject.toString();
        }
        catch (JSONException ex) {
            return jsonObject.toString();
        }
    }
}
