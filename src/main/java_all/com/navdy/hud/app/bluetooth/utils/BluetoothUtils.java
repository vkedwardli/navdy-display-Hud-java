package com.navdy.hud.app.bluetooth.utils;

public final class BluetoothUtils
{
    public static String getBondState(final int n) {
        String string = null;
        switch (n) {
            default:
                string = "Unknown:" + n;
                break;
            case 12:
                string = "Bonded";
                break;
            case 11:
                string = "Bonding";
                break;
            case 10:
                string = "Not Bonded";
                break;
        }
        return string;
    }
    
    public static String getConnectedState(final int n) {
        String string = null;
        switch (n) {
            default:
                string = "Unknown:" + n;
                break;
            case 2:
                string = "Connected";
                break;
            case 1:
                string = "Connecting";
                break;
            case 0:
                string = "Disconnected";
                break;
            case 3:
                string = "Disconnecting";
                break;
        }
        return string;
    }
    
    public static String getDeviceType(final int n) {
        String string = null;
        switch (n) {
            default:
                string = "Unknown:" + n;
                break;
            case 1:
                string = "Classic";
                break;
            case 3:
                string = "Dual";
                break;
            case 2:
                string = "LE";
                break;
        }
        return string;
    }
}
