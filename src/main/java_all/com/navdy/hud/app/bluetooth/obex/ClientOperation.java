package com.navdy.hud.app.bluetooth.obex;

import java.io.OutputStream;
import java.io.InputStream;
import java.io.DataOutputStream;
import java.io.DataInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

public final class ClientOperation implements Operation, BaseStream
{
    private static final String TAG = "ClientOperation";
    private static final boolean V = false;
    private boolean mEndOfBodySent;
    private String mExceptionMessage;
    private boolean mGetFinalFlag;
    private boolean mGetOperation;
    private boolean mInputOpen;
    private int mMaxPacketSize;
    private boolean mOperationDone;
    private ClientSession mParent;
    private PrivateInputStream mPrivateInput;
    private boolean mPrivateInputOpen;
    private PrivateOutputStream mPrivateOutput;
    private boolean mPrivateOutputOpen;
    private HeaderSet mReplyHeader;
    private HeaderSet mRequestHeader;
    private boolean mSendBodyHeader;
    private boolean mSrmActive;
    private boolean mSrmEnabled;
    private boolean mSrmWaitingForRemote;
    
    public ClientOperation(int i, final ClientSession mParent, final HeaderSet set, final boolean mGetOperation) throws IOException {
        this.mSendBodyHeader = true;
        this.mSrmActive = false;
        this.mSrmEnabled = false;
        this.mSrmWaitingForRemote = true;
        this.mParent = mParent;
        this.mEndOfBodySent = false;
        this.mInputOpen = true;
        this.mOperationDone = false;
        this.mMaxPacketSize = i;
        this.mGetOperation = mGetOperation;
        this.mGetFinalFlag = false;
        this.mPrivateInputOpen = false;
        this.mPrivateOutputOpen = false;
        this.mPrivateInput = null;
        this.mPrivateOutput = null;
        this.mReplyHeader = new HeaderSet();
        this.mRequestHeader = new HeaderSet();
        final int[] headerList = set.getHeaderList();
        if (headerList != null) {
            for (i = 0; i < headerList.length; ++i) {
                this.mRequestHeader.setHeader(headerList[i], set.getHeader(headerList[i]));
            }
        }
        if (set.mAuthChall != null) {
            this.mRequestHeader.mAuthChall = new byte[set.mAuthChall.length];
            System.arraycopy(set.mAuthChall, 0, this.mRequestHeader.mAuthChall, 0, set.mAuthChall.length);
        }
        if (set.mAuthResp != null) {
            this.mRequestHeader.mAuthResp = new byte[set.mAuthResp.length];
            System.arraycopy(set.mAuthResp, 0, this.mRequestHeader.mAuthResp, 0, set.mAuthResp.length);
        }
        if (set.mConnectionID != null) {
            this.mRequestHeader.mConnectionID = new byte[4];
            System.arraycopy(set.mConnectionID, 0, this.mRequestHeader.mConnectionID, 0, 4);
        }
    }
    
    private void checkForSrm() throws IOException {
        final Byte b = (Byte)this.mReplyHeader.getHeader(151);
        if (this.mParent.isSrmSupported() && b != null && b == 1) {
            this.mSrmEnabled = true;
        }
        if (this.mSrmEnabled) {
            this.mSrmWaitingForRemote = false;
            final Byte b2 = (Byte)this.mReplyHeader.getHeader(152);
            if (b2 != null && b2 == 1) {
                this.mSrmWaitingForRemote = true;
                this.mReplyHeader.setHeader(152, null);
            }
        }
        if (!this.mSrmWaitingForRemote && this.mSrmEnabled) {
            this.mSrmActive = true;
        }
    }
    
    private boolean sendRequest(int n) throws IOException {
        boolean b = false;
        boolean b2 = false;
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        int size = -1;
        final byte[] header = ObexHelper.createHeader(this.mRequestHeader, true);
        if (this.mPrivateOutput != null) {
            size = this.mPrivateOutput.size();
        }
        if (header.length + 3 + 3 > this.mMaxPacketSize) {
            int i = 0;
            int n2 = 0;
            while (i != header.length) {
                i = ObexHelper.findHeaderEnd(header, n2, this.mMaxPacketSize - 3);
                if (i == -1) {
                    this.mOperationDone = true;
                    this.abort();
                    this.mExceptionMessage = "Header larger then can be sent in a packet";
                    this.mInputOpen = false;
                    if (this.mPrivateInput != null) {
                        this.mPrivateInput.close();
                    }
                    if (this.mPrivateOutput != null) {
                        this.mPrivateOutput.close();
                    }
                    throw new IOException("OBEX Packet exceeds max packet size");
                }
                final byte[] array = new byte[i - n2];
                System.arraycopy(header, n2, array, 0, array.length);
                if (!this.mParent.sendRequest(n, array, this.mReplyHeader, this.mPrivateInput, false)) {
                    b = false;
                    return b;
                }
                if (this.mReplyHeader.responseCode != 144) {
                    b = false;
                    return b;
                }
                n2 = i;
            }
            this.checkForSrm();
            b = (size > 0);
        }
        else {
            int n3 = n;
            if (!this.mSendBodyHeader) {
                n3 = (n | 0x80);
            }
            byteArrayOutputStream.write(header);
            if ((n = size) > 0) {
                if ((n = size) > this.mMaxPacketSize - header.length - 6) {
                    b2 = true;
                    n = this.mMaxPacketSize - header.length - 6;
                }
                final byte[] bytes = this.mPrivateOutput.readBytes(n);
                if (this.mPrivateOutput.isClosed() && !b2 && !this.mEndOfBodySent && (n3 & 0x80) != 0x0) {
                    byteArrayOutputStream.write(73);
                    this.mEndOfBodySent = true;
                }
                else {
                    byteArrayOutputStream.write(72);
                }
                final int n4 = n + 3;
                byteArrayOutputStream.write((byte)(n4 >> 8));
                byteArrayOutputStream.write((byte)n4);
                n = n4;
                b = b2;
                if (bytes != null) {
                    byteArrayOutputStream.write(bytes);
                    b = b2;
                    n = n4;
                }
            }
            if (this.mPrivateOutputOpen && n <= 0 && !this.mEndOfBodySent) {
                if ((n3 & 0x80) == 0x0) {
                    byteArrayOutputStream.write(72);
                }
                else {
                    byteArrayOutputStream.write(73);
                    this.mEndOfBodySent = true;
                }
                byteArrayOutputStream.write((byte)0);
                byteArrayOutputStream.write((byte)3);
            }
            if (byteArrayOutputStream.size() == 0) {
                if (!this.mParent.sendRequest(n3, null, this.mReplyHeader, this.mPrivateInput, this.mSrmActive)) {
                    b = false;
                }
                else {
                    this.checkForSrm();
                }
            }
            else if (byteArrayOutputStream.size() > 0 && !this.mParent.sendRequest(n3, byteArrayOutputStream.toByteArray(), this.mReplyHeader, this.mPrivateInput, this.mSrmActive)) {
                b = false;
            }
            else {
                this.checkForSrm();
                boolean b3 = b;
                if (this.mPrivateOutput != null) {
                    b3 = b;
                    if (this.mPrivateOutput.size() > 0) {
                        b3 = true;
                    }
                }
                b = b3;
            }
        }
        return b;
    }
    
    private void startProcessing() throws IOException {
        while (true) {
            final boolean b;
            synchronized (this) {
                if (this.mPrivateInput == null) {
                    this.mPrivateInput = new PrivateInputStream(this);
                }
                b = true;
                boolean sendRequest = true;
                if (this.mGetOperation) {
                    if (!this.mOperationDone) {
                        this.mReplyHeader.responseCode = 144;
                        while (sendRequest && this.mReplyHeader.responseCode == 144) {
                            sendRequest = this.sendRequest(3);
                        }
                        if (this.mReplyHeader.responseCode == 144) {
                            this.mParent.sendRequest(131, null, this.mReplyHeader, this.mPrivateInput, this.mSrmActive);
                        }
                        if (this.mReplyHeader.responseCode != 144) {
                            this.mOperationDone = true;
                        }
                        else {
                            this.checkForSrm();
                        }
                    }
                    return;
                }
            }
            if (!this.mOperationDone) {
                this.mReplyHeader.responseCode = 144;
                for (boolean sendRequest2 = b; sendRequest2 && this.mReplyHeader.responseCode == 144; sendRequest2 = this.sendRequest(2)) {}
            }
            if (this.mReplyHeader.responseCode == 144) {
                this.mParent.sendRequest(130, null, this.mReplyHeader, this.mPrivateInput, this.mSrmActive);
            }
            if (this.mReplyHeader.responseCode != 144) {
                this.mOperationDone = true;
            }
        }
    }
    
    private void validateConnection() throws IOException {
        this.ensureOpen();
        if (this.mPrivateInput == null) {
            this.startProcessing();
        }
    }
    
    @Override
    public void abort() throws IOException {
        synchronized (this) {
            this.ensureOpen();
            if (this.mOperationDone && this.mReplyHeader.responseCode != 144) {
                throw new IOException("Operation has already ended");
            }
        }
        this.mExceptionMessage = "Operation aborted";
        if (!this.mOperationDone && this.mReplyHeader.responseCode == 144) {
            this.mOperationDone = true;
            this.mParent.sendRequest(255, null, this.mReplyHeader, null, false);
            if (this.mReplyHeader.responseCode != 160) {
                throw new IOException("Invalid response code from server");
            }
            this.mExceptionMessage = null;
        }
        this.close();
    }
    // monitorexit(this)
    
    @Override
    public void close() throws IOException {
        this.mInputOpen = false;
        this.mPrivateInputOpen = false;
        this.mPrivateOutputOpen = false;
        this.mParent.setRequestInactive();
    }
    
    @Override
    public boolean continueOperation(final boolean b, boolean mOperationDone) throws IOException {
        while (true) {
            final boolean b2 = false;
            Label_0149: {
                synchronized (this) {
                    if (!this.mGetOperation) {
                        break Label_0149;
                    }
                    if (mOperationDone && !this.mOperationDone) {
                        this.mParent.sendRequest(131, null, this.mReplyHeader, this.mPrivateInput, this.mSrmActive);
                        if (this.mReplyHeader.responseCode != 144) {
                            this.mOperationDone = true;
                        }
                        else {
                            this.checkForSrm();
                        }
                        return true;
                    }
                }
                if (!mOperationDone && !this.mOperationDone) {
                    if (this.mPrivateInput == null) {
                        this.mPrivateInput = new PrivateInputStream(this);
                    }
                    this.sendRequest(3);
                    return true;
                }
                boolean b3 = b2;
                if (this.mOperationDone) {
                    b3 = b2;
                    return b3;
                }
                return b3;
            }
            if (!mOperationDone && !this.mOperationDone) {
                if (this.mReplyHeader.responseCode == -1) {
                    this.mReplyHeader.responseCode = 144;
                }
                this.sendRequest(2);
                return true;
            }
            if (mOperationDone) {
                final boolean b3 = b2;
                if (!this.mOperationDone) {
                    return b3;
                }
            }
            mOperationDone = this.mOperationDone;
            boolean b3 = b2;
            if (mOperationDone) {
                b3 = b2;
                return b3;
            }
            return b3;
        }
    }
    
    @Override
    public void ensureNotDone() throws IOException {
        if (this.mOperationDone) {
            throw new IOException("Operation has completed");
        }
    }
    
    @Override
    public void ensureOpen() throws IOException {
        this.mParent.ensureOpen();
        if (this.mExceptionMessage != null) {
            throw new IOException(this.mExceptionMessage);
        }
        if (!this.mInputOpen) {
            throw new IOException("Operation has already ended");
        }
    }
    
    @Override
    public String getEncoding() {
        return null;
    }
    
    @Override
    public int getHeaderLength() {
        return ObexHelper.createHeader(this.mRequestHeader, false).length;
    }
    
    @Override
    public long getLength() {
        long longValue = -1L;
        try {
            final Long n = (Long)this.mReplyHeader.getHeader(195);
            if (n != null) {
                longValue = n;
            }
            return longValue;
        }
        catch (IOException ex) {
            return longValue;
        }
        return longValue;
    }
    
    @Override
    public int getMaxPacketSize() {
        return this.mMaxPacketSize - 6 - this.getHeaderLength();
    }
    
    @Override
    public HeaderSet getReceivedHeader() throws IOException {
        this.ensureOpen();
        return this.mReplyHeader;
    }
    
    @Override
    public int getResponseCode() throws IOException {
        synchronized (this) {
            if (this.mReplyHeader.responseCode == -1 || this.mReplyHeader.responseCode == 144) {
                this.validateConnection();
            }
            return this.mReplyHeader.responseCode;
        }
    }
    
    @Override
    public String getType() {
        try {
            return (String)this.mReplyHeader.getHeader(66);
        }
        catch (IOException ex) {
            return null;
        }
    }
    
    @Override
    public void noBodyHeader() {
        this.mSendBodyHeader = false;
    }
    
    @Override
    public DataInputStream openDataInputStream() throws IOException {
        return new DataInputStream(this.openInputStream());
    }
    
    @Override
    public DataOutputStream openDataOutputStream() throws IOException {
        return new DataOutputStream(this.openOutputStream());
    }
    
    @Override
    public InputStream openInputStream() throws IOException {
        this.ensureOpen();
        if (this.mPrivateInputOpen) {
            throw new IOException("no more input streams available");
        }
        if (this.mGetOperation) {
            this.validateConnection();
        }
        else if (this.mPrivateInput == null) {
            this.mPrivateInput = new PrivateInputStream(this);
        }
        this.mPrivateInputOpen = true;
        return this.mPrivateInput;
    }
    
    @Override
    public OutputStream openOutputStream() throws IOException {
        this.ensureOpen();
        this.ensureNotDone();
        if (this.mPrivateOutputOpen) {
            throw new IOException("no more output streams available");
        }
        if (this.mPrivateOutput == null) {
            this.mPrivateOutput = new PrivateOutputStream(this, this.getMaxPacketSize());
        }
        this.mPrivateOutputOpen = true;
        return this.mPrivateOutput;
    }
    
    @Override
    public void sendHeaders(final HeaderSet set) throws IOException {
        this.ensureOpen();
        if (this.mOperationDone) {
            throw new IOException("Operation has already exchanged all data");
        }
        if (set == null) {
            throw new IOException("Headers may not be null");
        }
        final int[] headerList = set.getHeaderList();
        if (headerList != null) {
            for (int i = 0; i < headerList.length; ++i) {
                this.mRequestHeader.setHeader(headerList[i], set.getHeader(headerList[i]));
            }
        }
    }
    
    public void setGetFinalFlag(final boolean mGetFinalFlag) {
        this.mGetFinalFlag = mGetFinalFlag;
    }
    
    @Override
    public void streamClosed(final boolean b) throws IOException {
        if (!this.mGetOperation) {
            if (!b && !this.mOperationDone) {
                boolean b3;
                final boolean b2 = b3 = true;
                if (this.mPrivateOutput != null) {
                    b3 = b2;
                    if (this.mPrivateOutput.size() <= 0) {
                        b3 = b2;
                        if (ObexHelper.createHeader(this.mRequestHeader, false).length <= 0) {
                            b3 = false;
                        }
                    }
                }
                boolean sendRequest = b3;
                if (this.mReplyHeader.responseCode == -1) {
                    this.mReplyHeader.responseCode = 144;
                    sendRequest = b3;
                }
                while (sendRequest && this.mReplyHeader.responseCode == 144) {
                    sendRequest = this.sendRequest(2);
                }
                while (this.mReplyHeader.responseCode == 144) {
                    this.sendRequest(130);
                }
                this.mOperationDone = true;
            }
            else if (b && this.mOperationDone) {
                this.mOperationDone = true;
            }
        }
        else if (b && !this.mOperationDone) {
            if (this.mReplyHeader.responseCode == -1) {
                this.mReplyHeader.responseCode = 144;
            }
            while (this.mReplyHeader.responseCode == 144 && !this.mOperationDone && this.sendRequest(131)) {}
            while (this.mReplyHeader.responseCode == 144 && !this.mOperationDone) {
                this.mParent.sendRequest(131, null, this.mReplyHeader, this.mPrivateInput, false);
            }
            this.mOperationDone = true;
        }
        else if (!b && !this.mOperationDone) {
            boolean b5;
            final boolean b4 = b5 = true;
            if (this.mPrivateOutput != null) {
                b5 = b4;
                if (this.mPrivateOutput.size() <= 0) {
                    b5 = b4;
                    if (ObexHelper.createHeader(this.mRequestHeader, false).length <= 0) {
                        b5 = false;
                    }
                }
            }
            if (this.mPrivateInput == null) {
                this.mPrivateInput = new PrivateInputStream(this);
            }
            boolean sendRequest2 = b5;
            if (this.mPrivateOutput != null) {
                sendRequest2 = b5;
                if (this.mPrivateOutput.size() <= 0) {
                    sendRequest2 = false;
                }
            }
            this.mReplyHeader.responseCode = 144;
            while (sendRequest2 && this.mReplyHeader.responseCode == 144) {
                sendRequest2 = this.sendRequest(3);
            }
            this.sendRequest(131);
            if (this.mReplyHeader.responseCode != 144) {
                this.mOperationDone = true;
            }
        }
    }
}
