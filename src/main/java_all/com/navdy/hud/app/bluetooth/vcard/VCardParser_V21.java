package com.navdy.hud.app.bluetooth.vcard;

import com.navdy.hud.app.bluetooth.vcard.exception.VCardException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;
import java.util.Collection;
import java.util.HashSet;
import java.util.Arrays;
import java.util.Set;

public final class VCardParser_V21 extends VCardParser
{
    static final Set<String> sAvailableEncoding;
    static final Set<String> sKnownPropertyNameSet;
    static final Set<String> sKnownTypeSet;
    static final Set<String> sKnownValueSet;
    private final VCardParserImpl_V21 mVCardParserImpl;
    
    static {
        sKnownPropertyNameSet = Collections.<String>unmodifiableSet((Set<? extends String>)new HashSet<String>(Arrays.<String>asList("BEGIN", "END", "LOGO", "PHOTO", "LABEL", "FN", "TITLE", "SOUND", "VERSION", "TEL", "EMAIL", "TZ", "GEO", "NOTE", "URL", "BDAY", "ROLE", "REV", "UID", "KEY", "MAILER")));
        sKnownTypeSet = Collections.<String>unmodifiableSet((Set<? extends String>)new HashSet<String>(Arrays.<String>asList("DOM", "INTL", "POSTAL", "PARCEL", "HOME", "WORK", "PREF", "VOICE", "FAX", "MSG", "CELL", "PAGER", "BBS", "MODEM", "CAR", "ISDN", "VIDEO", "AOL", "APPLELINK", "ATTMAIL", "CIS", "EWORLD", "INTERNET", "IBMMAIL", "MCIMAIL", "POWERSHARE", "PRODIGY", "TLX", "X400", "GIF", "CGM", "WMF", "BMP", "MET", "PMB", "DIB", "PICT", "TIFF", "PDF", "PS", "JPEG", "QTIME", "MPEG", "MPEG2", "AVI", "WAVE", "AIFF", "PCM", "X509", "PGP")));
        sKnownValueSet = Collections.<String>unmodifiableSet((Set<? extends String>)new HashSet<String>(Arrays.<String>asList("INLINE", "URL", "CONTENT-ID", "CID")));
        sAvailableEncoding = Collections.<String>unmodifiableSet((Set<? extends String>)new HashSet<String>(Arrays.<String>asList("7BIT", "8BIT", "QUOTED-PRINTABLE", "BASE64", "B")));
    }
    
    public VCardParser_V21() {
        this.mVCardParserImpl = new VCardParserImpl_V21();
    }
    
    public VCardParser_V21(final int n) {
        this.mVCardParserImpl = new VCardParserImpl_V21(n);
    }
    
    @Override
    public void addInterpreter(final VCardInterpreter vCardInterpreter) {
        this.mVCardParserImpl.addInterpreter(vCardInterpreter);
    }
    
    @Override
    public void cancel() {
        this.mVCardParserImpl.cancel();
    }
    
    @Override
    public void parse(final InputStream inputStream) throws IOException, VCardException {
        this.mVCardParserImpl.parse(inputStream);
    }
    
    @Override
    public void parseOne(final InputStream inputStream) throws IOException, VCardException {
        this.mVCardParserImpl.parseOne(inputStream);
    }
}
