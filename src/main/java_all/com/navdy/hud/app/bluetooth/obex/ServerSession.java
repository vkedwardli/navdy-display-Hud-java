package com.navdy.hud.app.bluetooth.obex;

import android.util.Log;
import java.io.IOException;
import java.io.OutputStream;
import java.io.InputStream;

public final class ServerSession extends ObexSession implements Runnable
{
    private static final String TAG = "Obex ServerSession";
    private static final boolean V = false;
    private boolean mClosed;
    private InputStream mInput;
    private ServerRequestHandler mListener;
    private int mMaxPacketLength;
    private OutputStream mOutput;
    private Thread mProcessThread;
    private ObexTransport mTransport;
    
    public ServerSession(final ObexTransport mTransport, final ServerRequestHandler mListener, final Authenticator mAuthenticator) throws IOException {
        this.mAuthenticator = mAuthenticator;
        this.mTransport = mTransport;
        this.mInput = this.mTransport.openInputStream();
        this.mOutput = this.mTransport.openOutputStream();
        this.mListener = mListener;
        this.mMaxPacketLength = 256;
        this.mClosed = false;
        (this.mProcessThread = new Thread(this)).start();
    }
    
    private void handleAbortRequest() throws IOException {
        final HeaderSet set = new HeaderSet();
        final HeaderSet set2 = new HeaderSet();
        final int n = (this.mInput.read() << 8) + this.mInput.read();
        int validateResponseCode;
        if (n > ObexHelper.getMaxRxPacketSize(this.mTransport)) {
            validateResponseCode = 206;
        }
        else {
            for (int i = 3; i < n; ++i) {
                this.mInput.read();
            }
            final int onAbort = this.mListener.onAbort(set, set2);
            Log.v("Obex ServerSession", "onAbort request handler return value- " + onAbort);
            validateResponseCode = this.validateResponseCode(onAbort);
        }
        this.sendResponse(validateResponseCode, null);
    }
    
    private void handleConnectRequest() throws IOException {
        final int n = 7;
        Object header = null;
        final int n2 = -1;
        final HeaderSet set = new HeaderSet();
        final HeaderSet set2 = new HeaderSet();
        final int n3 = (this.mInput.read() << 8) + this.mInput.read();
        this.mInput.read();
        this.mInput.read();
        this.mMaxPacketLength = this.mInput.read();
        this.mMaxPacketLength = (this.mMaxPacketLength << 8) + this.mInput.read();
        if (this.mMaxPacketLength > 65534) {
            this.mMaxPacketLength = 65534;
        }
        if (this.mMaxPacketLength > ObexHelper.getMaxTxPacketSize(this.mTransport)) {
            Log.w("Obex ServerSession", "Requested MaxObexPacketSize " + this.mMaxPacketLength + " is larger than the max size supported by the transport: " + ObexHelper.getMaxTxPacketSize(this.mTransport) + " Reducing to this size.");
            this.mMaxPacketLength = ObexHelper.getMaxTxPacketSize(this.mTransport);
        }
        int validateResponseCode = 0;
        int n4 = 0;
        Label_0199: {
            if (n3 > ObexHelper.getMaxRxPacketSize(this.mTransport)) {
                validateResponseCode = 206;
                n4 = 7;
            }
            else {
                if (n3 > 7) {
                    final byte[] array = new byte[n3 - 7];
                    for (int i = this.mInput.read(array); i != array.length; i += this.mInput.read(array, i, array.length - i)) {}
                    ObexHelper.updateHeaderSet(set, array);
                }
                while (true) {
                    Label_0421: {
                        if (this.mListener.getConnectionId() != -1L && set.mConnectionID != null) {
                            this.mListener.setConnectionId(ObexHelper.convertToLong(set.mConnectionID));
                            break Label_0421;
                        }
                        Label_0663: {
                            break Label_0663;
                            while (true) {
                                while (true) {
                                    long connectionId = 0L;
                                    Label_0696: {
                                        try {
                                            validateResponseCode = this.validateResponseCode(this.mListener.onConnect(set, set2));
                                            if (set2.nonce != null) {
                                                this.mChallengeDigest = new byte[16];
                                                System.arraycopy(set2.nonce, 0, this.mChallengeDigest, 0, 16);
                                            }
                                            else {
                                                this.mChallengeDigest = null;
                                            }
                                            connectionId = this.mListener.getConnectionId();
                                            if (connectionId != -1L) {
                                                break Label_0696;
                                            }
                                            set2.mConnectionID = null;
                                            header = ObexHelper.createHeader(set2, false);
                                            final int n5 = 7 + header.length;
                                            final int mMaxPacketLength = this.mMaxPacketLength;
                                            n4 = n5;
                                            if (n5 > mMaxPacketLength) {
                                                n4 = 7;
                                                header = null;
                                                validateResponseCode = 208;
                                            }
                                            break Label_0199;
                                            this.mListener.setConnectionId(1L);
                                            break;
                                        }
                                        catch (Exception ex) {
                                            n4 = 7;
                                            header = null;
                                            validateResponseCode = 208;
                                            break Label_0199;
                                        }
                                    }
                                    set2.mConnectionID = ObexHelper.convertToByteArray(connectionId);
                                    continue;
                                }
                            }
                        }
                    }
                    int n6 = n2;
                    if (set.mAuthResp != null) {
                        n6 = n2;
                        if (!this.handleAuthResp(set.mAuthResp)) {
                            n6 = 193;
                            this.mListener.onAuthenticationFailure(ObexHelper.getTagValue((byte)1, set.mAuthResp));
                        }
                        set.mAuthResp = null;
                    }
                    validateResponseCode = n6;
                    n4 = n;
                    if (n6 != 193) {
                        if (set.mAuthChall != null) {
                            this.handleAuthChall(set);
                            set2.mAuthResp = new byte[set.mAuthResp.length];
                            System.arraycopy(set.mAuthResp, 0, set2.mAuthResp, 0, set2.mAuthResp.length);
                            set.mAuthChall = null;
                            set.mAuthResp = null;
                        }
                        continue;
                    }
                    break;
                }
            }
        }
        final byte[] convertToByteArray = ObexHelper.convertToByteArray(n4);
        final byte[] array2 = new byte[n4];
        final int maxRxPacketSize = ObexHelper.getMaxRxPacketSize(this.mTransport);
        array2[0] = (byte)validateResponseCode;
        array2[1] = convertToByteArray[2];
        array2[2] = convertToByteArray[3];
        array2[3] = 16;
        array2[4] = 0;
        array2[5] = (byte)(maxRxPacketSize >> 8);
        array2[6] = (byte)(maxRxPacketSize & 0xFF);
        if (header != null) {
            System.arraycopy(header, 0, array2, 7, header.length);
        }
        this.mOutput.write(array2);
        this.mOutput.flush();
    }
    
    private void handleDisconnectRequest() throws IOException {
        final int n = 160;
        final int n2 = 3;
        Object header = null;
        final HeaderSet set = new HeaderSet();
        final HeaderSet set2 = new HeaderSet();
        final int n3 = (this.mInput.read() << 8) + this.mInput.read();
        int n4 = 0;
        int n5 = 0;
        Label_0065: {
            if (n3 > ObexHelper.getMaxRxPacketSize(this.mTransport)) {
                n4 = 206;
                n5 = 3;
            }
            else {
                if (n3 > 3) {
                    final byte[] array = new byte[n3 - 3];
                    for (int i = this.mInput.read(array); i != array.length; i += this.mInput.read(array, i, array.length - i)) {}
                    ObexHelper.updateHeaderSet(set, array);
                }
                while (true) {
                    Label_0236: {
                        if (this.mListener.getConnectionId() != -1L && set.mConnectionID != null) {
                            this.mListener.setConnectionId(ObexHelper.convertToLong(set.mConnectionID));
                            break Label_0236;
                        }
                        Label_0397: {
                            break Label_0397;
                            while (true) {
                                while (true) {
                                    long connectionId = 0L;
                                    Label_0420: {
                                        try {
                                            this.mListener.onDisconnect(set, set2);
                                            connectionId = this.mListener.getConnectionId();
                                            if (connectionId != -1L) {
                                                break Label_0420;
                                            }
                                            set2.mConnectionID = null;
                                            header = ObexHelper.createHeader(set2, false);
                                            final int n6 = 3 + header.length;
                                            final int n7;
                                            n4 = n7;
                                            n5 = n6;
                                            if (n6 > this.mMaxPacketLength) {
                                                n5 = 3;
                                                header = null;
                                                n4 = 208;
                                            }
                                            break Label_0065;
                                            this.mListener.setConnectionId(1L);
                                            break;
                                        }
                                        catch (Exception ex) {
                                            this.sendResponse(208, null);
                                            return;
                                        }
                                    }
                                    set2.mConnectionID = ObexHelper.convertToByteArray(connectionId);
                                    continue;
                                }
                            }
                        }
                    }
                    int n7 = n;
                    if (set.mAuthResp != null) {
                        n7 = n;
                        if (!this.handleAuthResp(set.mAuthResp)) {
                            n7 = 193;
                            this.mListener.onAuthenticationFailure(ObexHelper.getTagValue((byte)1, set.mAuthResp));
                        }
                        set.mAuthResp = null;
                    }
                    n4 = n7;
                    n5 = n2;
                    if (n7 != 193) {
                        if (set.mAuthChall != null) {
                            this.handleAuthChall(set);
                            set.mAuthChall = null;
                        }
                        continue;
                    }
                    break;
                }
            }
        }
        byte[] array2;
        if (header != null) {
            array2 = new byte[header.length + 3];
        }
        else {
            array2 = new byte[3];
        }
        array2[0] = (byte)n4;
        array2[1] = (byte)(n5 >> 8);
        array2[2] = (byte)n5;
        if (header != null) {
            System.arraycopy(header, 0, array2, 3, header.length);
        }
        this.mOutput.write(array2);
        this.mOutput.flush();
    }
    
    private void handleGetRequest(int validateResponseCode) throws IOException {
        final ServerOperation serverOperation = new ServerOperation(this, this.mInput, validateResponseCode, this.mMaxPacketLength, this.mListener);
        try {
            validateResponseCode = this.validateResponseCode(this.mListener.onGet(serverOperation));
            if (!serverOperation.isAborted) {
                serverOperation.sendReply(validateResponseCode);
            }
        }
        catch (Exception ex) {
            this.sendResponse(208, null);
        }
    }
    
    private void handlePutRequest(int n) throws IOException {
        while (true) {
            final ServerOperation serverOperation = new ServerOperation(this, this.mInput, n, this.mMaxPacketLength, this.mListener);
            Label_0137: {
                try {
                    if (serverOperation.finalBitSet && !serverOperation.isValidBody()) {
                        n = this.validateResponseCode(this.mListener.onDelete(serverOperation.requestHeader, serverOperation.replyHeader));
                    }
                    else {
                        n = this.validateResponseCode(this.mListener.onPut(serverOperation));
                    }
                    if (n != 160 && !serverOperation.isAborted) {
                        serverOperation.sendReply(n);
                    }
                    else if (!serverOperation.isAborted) {
                        while (!serverOperation.finalBitSet) {
                            serverOperation.sendReply(144);
                        }
                        break Label_0137;
                    }
                    return;
                }
                catch (Exception ex) {
                    if (!serverOperation.isAborted) {
                        this.sendResponse(208, null);
                    }
                    return;
                }
            }
            serverOperation.sendReply(n);
        }
    }
    
    private void handleSetPathRequest() throws IOException {
        final int n = 3;
        Object header = null;
        final int n2 = -1;
        final HeaderSet set = new HeaderSet();
        final HeaderSet set2 = new HeaderSet();
        final int n3 = (this.mInput.read() << 8) + this.mInput.read();
        final int read = this.mInput.read();
        this.mInput.read();
        int validateResponseCode = 0;
        int n4 = 0;
        Label_0080: {
            if (n3 > ObexHelper.getMaxRxPacketSize(this.mTransport)) {
                validateResponseCode = 206;
                n4 = 3;
            }
            else {
                int n5 = n2;
                while (true) {
                    Label_0302: {
                        if (n3 <= 5) {
                            break Label_0302;
                        }
                        final byte[] array = new byte[n3 - 5];
                        for (int i = this.mInput.read(array); i != array.length; i += this.mInput.read(array, i, array.length - i)) {}
                        ObexHelper.updateHeaderSet(set, array);
                        Label_0249: {
                            if (this.mListener.getConnectionId() != -1L && set.mConnectionID != null) {
                                this.mListener.setConnectionId(ObexHelper.convertToLong(set.mConnectionID));
                                break Label_0249;
                            }
                            Label_0519: {
                                break Label_0519;
                            Label_0481_Outer:
                                while (true) {
                                    while (true) {
                                        long connectionId = 0L;
                                    Label_0550:
                                        while (true) {
                                            Label_0542: {
                                                try {
                                                    final boolean b;
                                                    final boolean b2;
                                                    validateResponseCode = this.validateResponseCode(this.mListener.onSetPath(set, set2, b, b2));
                                                    if (set2.nonce == null) {
                                                        break Label_0542;
                                                    }
                                                    this.mChallengeDigest = new byte[16];
                                                    System.arraycopy(set2.nonce, 0, this.mChallengeDigest, 0, 16);
                                                    connectionId = this.mListener.getConnectionId();
                                                    if (connectionId != -1L) {
                                                        break Label_0550;
                                                    }
                                                    set2.mConnectionID = null;
                                                    header = ObexHelper.createHeader(set2, false);
                                                    if ((n4 = 3 + header.length) > this.mMaxPacketLength) {
                                                        n4 = 3;
                                                        header = null;
                                                        validateResponseCode = 208;
                                                    }
                                                    break Label_0080;
                                                    this.mListener.setConnectionId(1L);
                                                    break;
                                                }
                                                catch (Exception ex) {
                                                    this.sendResponse(208, null);
                                                    return;
                                                }
                                            }
                                            this.mChallengeDigest = null;
                                            continue Label_0481_Outer;
                                        }
                                        set2.mConnectionID = ObexHelper.convertToByteArray(connectionId);
                                        continue;
                                    }
                                }
                            }
                        }
                        n5 = n2;
                        if (set.mAuthResp != null) {
                            n5 = n2;
                            if (!this.handleAuthResp(set.mAuthResp)) {
                                n5 = 193;
                                this.mListener.onAuthenticationFailure(ObexHelper.getTagValue((byte)1, set.mAuthResp));
                            }
                            set.mAuthResp = null;
                        }
                    }
                    validateResponseCode = n5;
                    n4 = n;
                    if (n5 != 193) {
                        if (set.mAuthChall != null) {
                            this.handleAuthChall(set);
                            set2.mAuthResp = new byte[set.mAuthResp.length];
                            System.arraycopy(set.mAuthResp, 0, set2.mAuthResp, 0, set2.mAuthResp.length);
                            set.mAuthChall = null;
                            set.mAuthResp = null;
                        }
                        boolean b = false;
                        boolean b2 = true;
                        if ((read & 0x1) != 0x0) {
                            b = true;
                        }
                        if ((read & 0x2) != 0x0) {
                            b2 = false;
                        }
                        continue;
                    }
                    break;
                }
            }
        }
        final byte[] array2 = new byte[n4];
        array2[0] = (byte)validateResponseCode;
        array2[1] = (byte)(n4 >> 8);
        array2[2] = (byte)n4;
        if (header != null) {
            System.arraycopy(header, 0, array2, 3, header.length);
        }
        this.mOutput.write(array2);
        this.mOutput.flush();
    }
    
    private int validateResponseCode(final int n) {
        int n2;
        if (n >= 160 && n <= 166) {
            n2 = n;
        }
        else if ((n < 176 || (n2 = n) > 181) && (n < 192 || (n2 = n) > 207) && (n < 208 || (n2 = n) > 213) && (n < 224 || (n2 = n) > 225)) {
            n2 = 208;
        }
        return n2;
    }
    
    public void close() {
        // 
         This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: dup            
        //     2: astore_1       
        //     3: monitorenter   
        //     4: aload_0        
        //     5: getfield        com/navdy/hud/app/bluetooth/obex/ServerSession.mListener:Lcom/navdy/hud/app/bluetooth/obex/ServerRequestHandler;
        //     8: ifnull          18
        //    11: aload_0        
        //    12: getfield        com/navdy/hud/app/bluetooth/obex/ServerSession.mListener:Lcom/navdy/hud/app/bluetooth/obex/ServerRequestHandler;
        //    15: invokevirtual   com/navdy/hud/app/bluetooth/obex/ServerRequestHandler.onClose:()V
        //    18: aload_0        
        //    19: iconst_1       
        //    20: putfield        com/navdy/hud/app/bluetooth/obex/ServerSession.mClosed:Z
        //    23: aload_0        
        //    24: getfield        com/navdy/hud/app/bluetooth/obex/ServerSession.mInput:Ljava/io/InputStream;
        //    27: ifnull          37
        //    30: aload_0        
        //    31: getfield        com/navdy/hud/app/bluetooth/obex/ServerSession.mInput:Ljava/io/InputStream;
        //    34: invokevirtual   java/io/InputStream.close:()V
        //    37: aload_0        
        //    38: getfield        com/navdy/hud/app/bluetooth/obex/ServerSession.mOutput:Ljava/io/OutputStream;
        //    41: ifnull          51
        //    44: aload_0        
        //    45: getfield        com/navdy/hud/app/bluetooth/obex/ServerSession.mOutput:Ljava/io/OutputStream;
        //    48: invokevirtual   java/io/OutputStream.close:()V
        //    51: aload_0        
        //    52: getfield        com/navdy/hud/app/bluetooth/obex/ServerSession.mTransport:Lcom/navdy/hud/app/bluetooth/obex/ObexTransport;
        //    55: ifnull          67
        //    58: aload_0        
        //    59: getfield        com/navdy/hud/app/bluetooth/obex/ServerSession.mTransport:Lcom/navdy/hud/app/bluetooth/obex/ObexTransport;
        //    62: invokeinterface com/navdy/hud/app/bluetooth/obex/ObexTransport.close:()V
        //    67: aload_0        
        //    68: aconst_null    
        //    69: putfield        com/navdy/hud/app/bluetooth/obex/ServerSession.mTransport:Lcom/navdy/hud/app/bluetooth/obex/ObexTransport;
        //    72: aload_0        
        //    73: aconst_null    
        //    74: putfield        com/navdy/hud/app/bluetooth/obex/ServerSession.mInput:Ljava/io/InputStream;
        //    77: aload_0        
        //    78: aconst_null    
        //    79: putfield        com/navdy/hud/app/bluetooth/obex/ServerSession.mOutput:Ljava/io/OutputStream;
        //    82: aload_0        
        //    83: aconst_null    
        //    84: putfield        com/navdy/hud/app/bluetooth/obex/ServerSession.mListener:Lcom/navdy/hud/app/bluetooth/obex/ServerRequestHandler;
        //    87: aload_1        
        //    88: monitorexit    
        //    89: return         
        //    90: astore_2       
        //    91: aload_1        
        //    92: monitorexit    
        //    93: aload_2        
        //    94: athrow         
        //    95: astore_2       
        //    96: goto            67
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  4      18     90     95     Any
        //  18     37     95     99     Ljava/lang/Exception;
        //  18     37     90     95     Any
        //  37     51     95     99     Ljava/lang/Exception;
        //  37     51     90     95     Any
        //  51     67     95     99     Ljava/lang/Exception;
        //  51     67     90     95     Any
        //  67     87     90     95     Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0018:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public ObexTransport getTransport() {
        return this.mTransport;
    }
    
    @Override
    public void run() {
        int i = 0;
        while (i == 0) {
            Label_0233: {
                Label_0228: {
                    Label_0221: {
                        Label_0214: {
                            int read = 0;
                            try {
                                if (this.mClosed) {
                                    goto Label_0168;
                                }
                                read = this.mInput.read();
                                switch (read) {
                                    default:
                                        for (int read2 = this.mInput.read(), read3 = this.mInput.read(), j = 3; j < (read2 << 8) + read3; ++j) {
                                            this.mInput.read();
                                        }
                                        break Label_0233;
                                    case 128:
                                        this.handleConnectRequest();
                                        continue;
                                    case 129:
                                        goto Label_0173;
                                        goto Label_0173;
                                    case 3:
                                    case 131:
                                        goto Label_0182;
                                        goto Label_0182;
                                    case 2:
                                    case 130:
                                        break;
                                    case 133:
                                        break Label_0214;
                                    case 255:
                                        break Label_0221;
                                    case -1:
                                        break Label_0228;
                                }
                            }
                            catch (NullPointerException ex) {
                                Log.d("Obex ServerSession", "Exception occured - ignoring", (Throwable)ex);
                            }
                            catch (Exception ex2) {
                                Log.d("Obex ServerSession", "Exception occured - ignoring", (Throwable)ex2);
                                goto Label_0168;
                            }
                            this.handlePutRequest(read);
                            continue;
                        }
                        this.handleSetPathRequest();
                        continue;
                    }
                    this.handleAbortRequest();
                    continue;
                }
                i = 1;
                continue;
            }
            this.sendResponse(209, null);
        }
        goto Label_0168;
    }
    
    public void sendResponse(final int n, byte[] array) throws IOException {
        final OutputStream mOutput = this.mOutput;
        if (mOutput != null) {
            if (array != null) {
                final int n2 = 3 + array.length;
                final byte[] array2 = new byte[n2];
                array2[0] = (byte)n;
                array2[1] = (byte)(n2 >> 8);
                array2[2] = (byte)n2;
                System.arraycopy(array, 0, array2, 3, array.length);
                array = array2;
            }
            else {
                array = new byte[] { (byte)n, (byte)0, (byte)3 };
            }
            mOutput.write(array);
            mOutput.flush();
        }
    }
}
