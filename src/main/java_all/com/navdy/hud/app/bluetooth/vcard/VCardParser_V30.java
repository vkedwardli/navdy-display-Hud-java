package com.navdy.hud.app.bluetooth.vcard;

import com.navdy.hud.app.bluetooth.vcard.exception.VCardException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;
import java.util.Collection;
import java.util.HashSet;
import java.util.Arrays;
import java.util.Set;

public class VCardParser_V30 extends VCardParser
{
    static final Set<String> sAcceptableEncoding;
    static final Set<String> sKnownPropertyNameSet;
    private final VCardParserImpl_V30 mVCardParserImpl;
    
    static {
        sKnownPropertyNameSet = Collections.<String>unmodifiableSet((Set<? extends String>)new HashSet<String>(Arrays.<String>asList("BEGIN", "END", "LOGO", "PHOTO", "LABEL", "FN", "TITLE", "SOUND", "VERSION", "TEL", "EMAIL", "TZ", "GEO", "NOTE", "URL", "BDAY", "ROLE", "REV", "UID", "KEY", "MAILER", "NAME", "PROFILE", "SOURCE", "NICKNAME", "CLASS", "SORT-STRING", "CATEGORIES", "PRODID", "IMPP")));
        sAcceptableEncoding = Collections.<String>unmodifiableSet((Set<? extends String>)new HashSet<String>(Arrays.<String>asList("7BIT", "8BIT", "BASE64", "B")));
    }
    
    public VCardParser_V30() {
        this.mVCardParserImpl = new VCardParserImpl_V30();
    }
    
    public VCardParser_V30(final int n) {
        this.mVCardParserImpl = new VCardParserImpl_V30(n);
    }
    
    @Override
    public void addInterpreter(final VCardInterpreter vCardInterpreter) {
        this.mVCardParserImpl.addInterpreter(vCardInterpreter);
    }
    
    @Override
    public void cancel() {
        this.mVCardParserImpl.cancel();
    }
    
    @Override
    public void parse(final InputStream inputStream) throws IOException, VCardException {
        this.mVCardParserImpl.parse(inputStream);
    }
    
    @Override
    public void parseOne(final InputStream inputStream) throws IOException, VCardException {
        this.mVCardParserImpl.parseOne(inputStream);
    }
}
