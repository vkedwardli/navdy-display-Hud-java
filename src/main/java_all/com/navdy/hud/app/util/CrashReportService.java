package com.navdy.hud.app.util;

import android.text.TextUtils;
import com.navdy.hud.app.storage.PathManager;
import com.navdy.service.library.events.DeviceInfo;
import android.content.Intent;
import com.navdy.service.library.util.IOUtils;
import com.navdy.hud.app.HudApplication;
import java.util.Locale;
import java.util.Comparator;
import com.navdy.service.library.log.Logger;
import java.io.File;
import java.util.concurrent.PriorityBlockingQueue;
import java.text.SimpleDateFormat;
import android.app.IntentService;

public class CrashReportService extends IntentService
{
    private static final String ACTION_ADD_REPORT = "AddReport";
    private static final String ACTION_DUMP_CRASH_REPORT = "DumpCrashReport";
    private static String CRASH_INFO_TEXT_FILE_NAME;
    private static String CRASH_LOG_TEXT_FILE_NAME;
    private static final SimpleDateFormat DATE_FORMAT;
    public static final String EXTRA_CRASH_TYPE = "EXTRA_CRASH_TYPE";
    public static final String EXTRA_FILE_PATH = "EXTRA_FILE_PATH";
    private static int LOG_FILE_SIZE = 0;
    private static final int MAX_NON_FATAL_CRASH_REPORTS_OUT_STANDING = 10;
    private static final SimpleDateFormat TIME_FORMAT_FOR_REPORT;
    private static boolean mIsInitialized;
    private static String sCrashReportsFolder;
    private static PriorityBlockingQueue<File> sCrashReportsToSend;
    private static Logger sLogger;
    private static int sOutStandingCrashReportsToBeCleared;
    
    static {
        CrashReportService.sCrashReportsToSend = new PriorityBlockingQueue<File>(10, new FilesModifiedTimeComparator());
        CrashReportService.sOutStandingCrashReportsToBeCleared = 0;
        CrashReportService.sLogger = new Logger(CrashReportService.class);
        CrashReportService.CRASH_INFO_TEXT_FILE_NAME = "info.txt";
        CrashReportService.CRASH_LOG_TEXT_FILE_NAME = "log.txt";
        CrashReportService.LOG_FILE_SIZE = 51200;
        CrashReportService.mIsInitialized = false;
        DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd'_'HH:mm:ss.SSS", Locale.US);
        TIME_FORMAT_FOR_REPORT = new SimpleDateFormat("dd MM yyyy' 'HH:mm:ss.SSS", Locale.US);
    }
    
    public CrashReportService() {
        super("CrashReportService");
    }
    
    private void addReport(File file) {
        final PriorityBlockingQueue<File> sCrashReportsToSend = CrashReportService.sCrashReportsToSend;
        synchronized (sCrashReportsToSend) {
            CrashReportService.sCrashReportsToSend.add(file);
            if (CrashReportService.sCrashReportsToSend.size() > 10) {
                file = CrashReportService.sCrashReportsToSend.poll();
                if (file != null) {
                    IOUtils.deleteFile(HudApplication.getAppContext(), file.getAbsolutePath());
                }
                if (CrashReportService.sOutStandingCrashReportsToBeCleared > 0) {
                    --CrashReportService.sOutStandingCrashReportsToBeCleared;
                }
            }
        }
    }
    
    public static void addSnapshotAsync(final String s) {
        final Intent intent = new Intent(HudApplication.getAppContext(), (Class)CrashReportService.class);
        intent.setAction("AddReport");
        intent.putExtra("EXTRA_FILE_PATH", s);
        HudApplication.getAppContext().startService(intent);
    }
    
    public static void clearCrashReports() {
        final PriorityBlockingQueue<File> sCrashReportsToSend = CrashReportService.sCrashReportsToSend;
        synchronized (sCrashReportsToSend) {
            while (CrashReportService.sOutStandingCrashReportsToBeCleared > 0) {
                final File file = CrashReportService.sCrashReportsToSend.poll();
                if (file != null) {
                    IOUtils.deleteFile(HudApplication.getAppContext(), file.getAbsolutePath());
                    --CrashReportService.sOutStandingCrashReportsToBeCleared;
                }
            }
        }
    }
    // monitorexit(priorityBlockingQueue)
    
    public static void compressCrashReportsToZip(final File[] p0, final String p1) {
        // 
        This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     3: dup            
        //     4: aload_1        
        //     5: invokespecial   java/io/File.<init>:(Ljava/lang/String;)V
        //     8: astore_2       
        //     9: aload_2        
        //    10: invokevirtual   java/io/File.exists:()Z
        //    13: ifeq            24
        //    16: invokestatic    com/navdy/hud/app/HudApplication.getAppContext:()Landroid/content/Context;
        //    19: aload_1        
        //    20: invokestatic    com/navdy/service/library/util/IOUtils.deleteFile:(Landroid/content/Context;Ljava/lang/String;)Z
        //    23: pop            
        //    24: aload_2        
        //    25: invokevirtual   java/io/File.createNewFile:()Z
        //    28: pop            
        //    29: iconst_0       
        //    30: istore_3       
        //    31: iload_3        
        //    32: istore          4
        //    34: aload_0        
        //    35: ifnull          50
        //    38: iload_3        
        //    39: istore          4
        //    41: aload_0        
        //    42: arraylength    
        //    43: ifle            50
        //    46: aload_0        
        //    47: arraylength    
        //    48: istore          4
        //    50: getstatic       com/navdy/hud/app/util/CrashReportService.sCrashReportsToSend:Ljava/util/concurrent/PriorityBlockingQueue;
        //    53: astore_2       
        //    54: aload_2        
        //    55: dup            
        //    56: astore          5
        //    58: monitorenter   
        //    59: iload           4
        //    61: getstatic       com/navdy/hud/app/util/CrashReportService.sCrashReportsToSend:Ljava/util/concurrent/PriorityBlockingQueue;
        //    64: invokevirtual   java/util/concurrent/PriorityBlockingQueue.size:()I
        //    67: iadd           
        //    68: anewarray       Ljava/io/File;
        //    71: astore          6
        //    73: iconst_0       
        //    74: istore          4
        //    76: aload_0        
        //    77: ifnull          142
        //    80: aload_0        
        //    81: arraylength    
        //    82: istore          7
        //    84: iconst_0       
        //    85: istore_3       
        //    86: iconst_0       
        //    87: istore          4
        //    89: iload_3        
        //    90: iload           7
        //    92: if_icmpge       142
        //    95: aload           6
        //    97: iload           4
        //    99: aload_0        
        //   100: iload_3        
        //   101: aaload         
        //   102: aastore        
        //   103: iinc            3, 1
        //   106: iinc            4, 1
        //   109: goto            89
        //   112: astore_2       
        //   113: getstatic       com/navdy/hud/app/util/CrashReportService.sLogger:Lcom/navdy/service/library/log/Logger;
        //   116: new             Ljava/lang/StringBuilder;
        //   119: dup            
        //   120: invokespecial   java/lang/StringBuilder.<init>:()V
        //   123: ldc             "Failed to create new file at the specified output file path "
        //   125: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   128: aload_1        
        //   129: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   132: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   135: aload_2        
        //   136: invokevirtual   com/navdy/service/library/log/Logger.e:(Ljava/lang/String;Ljava/lang/Throwable;)V
        //   139: goto            29
        //   142: getstatic       com/navdy/hud/app/util/CrashReportService.sCrashReportsToSend:Ljava/util/concurrent/PriorityBlockingQueue;
        //   145: invokevirtual   java/util/concurrent/PriorityBlockingQueue.size:()I
        //   148: putstatic       com/navdy/hud/app/util/CrashReportService.sOutStandingCrashReportsToBeCleared:I
        //   151: getstatic       com/navdy/hud/app/util/CrashReportService.sCrashReportsToSend:Ljava/util/concurrent/PriorityBlockingQueue;
        //   154: invokevirtual   java/util/concurrent/PriorityBlockingQueue.iterator:()Ljava/util/Iterator;
        //   157: astore_0       
        //   158: aload_0        
        //   159: invokeinterface java/util/Iterator.hasNext:()Z
        //   164: ifeq            187
        //   167: aload           6
        //   169: iload           4
        //   171: aload_0        
        //   172: invokeinterface java/util/Iterator.next:()Ljava/lang/Object;
        //   177: checkcast       Ljava/io/File;
        //   180: aastore        
        //   181: iinc            4, 1
        //   184: goto            158
        //   187: aload           5
        //   189: monitorexit    
        //   190: invokestatic    com/navdy/hud/app/HudApplication.getAppContext:()Landroid/content/Context;
        //   193: aload           6
        //   195: aload_1        
        //   196: invokestatic    com/navdy/service/library/util/IOUtils.compressFilesToZip:(Landroid/content/Context;[Ljava/io/File;Ljava/lang/String;)V
        //   199: return         
        //   200: astore_0       
        //   201: aload           5
        //   203: monitorexit    
        //   204: aload_0        
        //   205: athrow         
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  24     29     112    142    Ljava/io/IOException;
        //  59     73     200    206    Any
        //  80     84     200    206    Any
        //  142    158    200    206    Any
        //  158    181    200    206    Any
        //  187    190    200    206    Any
        //  201    204    200    206    Any
        // 
        // The error that occurred was:
        // 
        // java.lang.NullPointerException
        //     at com.strobel.assembler.ir.StackMappingVisitor.push(StackMappingVisitor.java:290)
        //     at com.strobel.assembler.ir.StackMappingVisitor$InstructionAnalyzer.execute(StackMappingVisitor.java:833)
        //     at com.strobel.assembler.ir.StackMappingVisitor$InstructionAnalyzer.visit(StackMappingVisitor.java:398)
        //     at com.strobel.decompiler.ast.AstBuilder.performStackAnalysis(AstBuilder.java:2030)
        //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:108)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:210)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    private void dumpCrashReport(final String p0) {
        // 
        This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     3: dup            
        //     4: invokespecial   java/lang/StringBuilder.<init>:()V
        //     7: astore_2       
        //     8: new             Ljava/util/Date;
        //    11: dup            
        //    12: invokestatic    java/lang/System.currentTimeMillis:()J
        //    15: invokespecial   java/util/Date.<init>:(J)V
        //    18: astore_3       
        //    19: getstatic       com/navdy/hud/app/util/CrashReportService.TIME_FORMAT_FOR_REPORT:Ljava/text/SimpleDateFormat;
        //    22: aload_3        
        //    23: invokevirtual   java/text/SimpleDateFormat.format:(Ljava/util/Date;)Ljava/lang/String;
        //    26: astore          4
        //    28: new             Ljava/io/File;
        //    31: dup            
        //    32: new             Ljava/lang/StringBuilder;
        //    35: dup            
        //    36: invokespecial   java/lang/StringBuilder.<init>:()V
        //    39: getstatic       com/navdy/hud/app/util/CrashReportService.sCrashReportsFolder:Ljava/lang/String;
        //    42: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    45: getstatic       java/io/File.separator:Ljava/lang/String;
        //    48: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    51: getstatic       com/navdy/hud/app/util/CrashReportService.CRASH_INFO_TEXT_FILE_NAME:Ljava/lang/String;
        //    54: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    57: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //    60: invokespecial   java/io/File.<init>:(Ljava/lang/String;)V
        //    63: astore          5
        //    65: aload           5
        //    67: invokevirtual   java/io/File.exists:()Z
        //    70: ifeq            83
        //    73: aload_0        
        //    74: aload           5
        //    76: invokevirtual   java/io/File.getAbsolutePath:()Ljava/lang/String;
        //    79: invokestatic    com/navdy/service/library/util/IOUtils.deleteFile:(Landroid/content/Context;Ljava/lang/String;)Z
        //    82: pop            
        //    83: aload           5
        //    85: invokevirtual   java/io/File.getParentFile:()Ljava/io/File;
        //    88: astore          6
        //    90: aload           6
        //    92: invokevirtual   java/io/File.exists:()Z
        //    95: ifne            104
        //    98: aload           6
        //   100: invokevirtual   java/io/File.mkdirs:()Z
        //   103: pop            
        //   104: aconst_null    
        //   105: astore          7
        //   107: aconst_null    
        //   108: astore          8
        //   110: aload           7
        //   112: astore          6
        //   114: aload           5
        //   116: invokevirtual   java/io/File.createNewFile:()Z
        //   119: pop            
        //   120: aload           7
        //   122: astore          6
        //   124: new             Ljava/io/FileWriter;
        //   127: astore          9
        //   129: aload           7
        //   131: astore          6
        //   133: aload           9
        //   135: aload           5
        //   137: invokespecial   java/io/FileWriter.<init>:(Ljava/io/File;)V
        //   140: new             Ljava/lang/StringBuilder;
        //   143: astore          6
        //   145: aload           6
        //   147: invokespecial   java/lang/StringBuilder.<init>:()V
        //   150: aload           9
        //   152: aload           6
        //   154: ldc             "Crash Type : "
        //   156: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   159: aload_1        
        //   160: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   163: ldc             "\n"
        //   165: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   168: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   171: invokevirtual   java/io/FileWriter.write:(Ljava/lang/String;)V
        //   174: new             Ljava/lang/StringBuilder;
        //   177: astore          6
        //   179: aload           6
        //   181: invokespecial   java/lang/StringBuilder.<init>:()V
        //   184: aload           9
        //   186: aload           6
        //   188: ldc             "Report Time : "
        //   190: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   193: aload           4
        //   195: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   198: ldc             " , "
        //   200: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   203: aload_2        
        //   204: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   207: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   210: ldc             "\n"
        //   212: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   215: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   218: invokevirtual   java/io/FileWriter.write:(Ljava/lang/String;)V
        //   221: aload           9
        //   223: invokestatic    com/navdy/hud/app/util/os/PropsFileUpdater.readProps:()Ljava/lang/String;
        //   226: invokevirtual   java/io/FileWriter.write:(Ljava/lang/String;)V
        //   229: getstatic       com/navdy/service/library/device/RemoteDevice.sLastSeenDeviceInfo:Lcom/navdy/service/library/device/RemoteDevice$LastSeenDeviceInfo;
        //   232: invokevirtual   com/navdy/service/library/device/RemoteDevice$LastSeenDeviceInfo.getDeviceInfo:()Lcom/navdy/service/library/events/DeviceInfo;
        //   235: astore          6
        //   237: aload           6
        //   239: ifnull          252
        //   242: aload           9
        //   244: aload           6
        //   246: invokestatic    com/navdy/hud/app/util/CrashReportService.printDeviceInfo:(Lcom/navdy/service/library/events/DeviceInfo;)Ljava/lang/String;
        //   249: invokevirtual   java/io/FileWriter.write:(Ljava/lang/String;)V
        //   252: aload           9
        //   254: invokevirtual   java/io/FileWriter.flush:()V
        //   257: aload           9
        //   259: invokevirtual   java/io/FileWriter.close:()V
        //   262: new             Ljava/io/File;
        //   265: astore          6
        //   267: new             Ljava/lang/StringBuilder;
        //   270: astore          8
        //   272: aload           8
        //   274: invokespecial   java/lang/StringBuilder.<init>:()V
        //   277: aload           6
        //   279: aload           8
        //   281: getstatic       com/navdy/hud/app/util/CrashReportService.sCrashReportsFolder:Ljava/lang/String;
        //   284: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   287: getstatic       java/io/File.separator:Ljava/lang/String;
        //   290: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   293: ldc_w           "temp"
        //   296: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   299: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   302: invokespecial   java/io/File.<init>:(Ljava/lang/String;)V
        //   305: aload           6
        //   307: invokevirtual   java/io/File.exists:()Z
        //   310: ifeq            319
        //   313: aload_0        
        //   314: aload           6
        //   316: invokestatic    com/navdy/service/library/util/IOUtils.deleteDirectory:(Landroid/content/Context;Ljava/io/File;)V
        //   319: aload           6
        //   321: invokevirtual   java/io/File.mkdirs:()Z
        //   324: pop            
        //   325: aload           6
        //   327: invokevirtual   java/io/File.getAbsolutePath:()Ljava/lang/String;
        //   330: invokestatic    com/navdy/service/library/util/LogUtils.copySnapshotSystemLogs:(Ljava/lang/String;)V
        //   333: aload           6
        //   335: invokevirtual   java/io/File.listFiles:()[Ljava/io/File;
        //   338: astore          7
        //   340: aload           7
        //   342: ifnull          407
        //   345: aload           7
        //   347: arraylength    
        //   348: iconst_1       
        //   349: iadd           
        //   350: istore          10
        //   352: iload           10
        //   354: anewarray       Ljava/io/File;
        //   357: astore          8
        //   359: aload           8
        //   361: iconst_0       
        //   362: aload           5
        //   364: aastore        
        //   365: aload           7
        //   367: ifnull          413
        //   370: aload           7
        //   372: arraylength    
        //   373: istore          11
        //   375: iconst_0       
        //   376: istore          10
        //   378: iconst_1       
        //   379: istore          12
        //   381: iload           10
        //   383: iload           11
        //   385: if_icmpge       413
        //   388: aload           8
        //   390: iload           12
        //   392: aload           7
        //   394: iload           10
        //   396: aaload         
        //   397: aastore        
        //   398: iinc            10, 1
        //   401: iinc            12, 1
        //   404: goto            381
        //   407: iconst_1       
        //   408: istore          10
        //   410: goto            352
        //   413: new             Ljava/lang/StringBuilder;
        //   416: astore          7
        //   418: aload           7
        //   420: invokespecial   java/lang/StringBuilder.<init>:()V
        //   423: aload           7
        //   425: getstatic       com/navdy/hud/app/util/CrashReportService.DATE_FORMAT:Ljava/text/SimpleDateFormat;
        //   428: aload_3        
        //   429: invokevirtual   java/text/SimpleDateFormat.format:(Ljava/util/Date;)Ljava/lang/String;
        //   432: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   435: ldc_w           "_"
        //   438: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   441: aload_1        
        //   442: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   445: ldc_w           ".zip"
        //   448: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   451: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   454: astore_1       
        //   455: new             Ljava/io/File;
        //   458: astore_3       
        //   459: new             Ljava/lang/StringBuilder;
        //   462: astore          7
        //   464: aload           7
        //   466: invokespecial   java/lang/StringBuilder.<init>:()V
        //   469: aload_3        
        //   470: aload           7
        //   472: getstatic       com/navdy/hud/app/util/CrashReportService.sCrashReportsFolder:Ljava/lang/String;
        //   475: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   478: getstatic       java/io/File.separator:Ljava/lang/String;
        //   481: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   484: aload_1        
        //   485: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   488: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   491: invokespecial   java/io/File.<init>:(Ljava/lang/String;)V
        //   494: invokestatic    com/navdy/hud/app/HudApplication.getAppContext:()Landroid/content/Context;
        //   497: aload           8
        //   499: aload_3        
        //   500: invokevirtual   java/io/File.getAbsolutePath:()Ljava/lang/String;
        //   503: invokestatic    com/navdy/service/library/util/IOUtils.compressFilesToZip:(Landroid/content/Context;[Ljava/io/File;Ljava/lang/String;)V
        //   506: aload_0        
        //   507: aload           5
        //   509: invokevirtual   java/io/File.getAbsolutePath:()Ljava/lang/String;
        //   512: invokestatic    com/navdy/service/library/util/IOUtils.deleteFile:(Landroid/content/Context;Ljava/lang/String;)Z
        //   515: pop            
        //   516: aload_0        
        //   517: aload           6
        //   519: invokestatic    com/navdy/service/library/util/IOUtils.deleteDirectory:(Landroid/content/Context;Ljava/io/File;)V
        //   522: aload_0        
        //   523: aload_3        
        //   524: invokespecial   com/navdy/hud/app/util/CrashReportService.addReport:(Ljava/io/File;)V
        //   527: aload           9
        //   529: invokevirtual   java/io/FileWriter.close:()V
        //   532: return         
        //   533: astore_1       
        //   534: getstatic       com/navdy/hud/app/util/CrashReportService.sLogger:Lcom/navdy/service/library/log/Logger;
        //   537: ldc_w           "Error closing the File Writer"
        //   540: invokevirtual   com/navdy/service/library/log/Logger.d:(Ljava/lang/String;)V
        //   543: goto            532
        //   546: astore          9
        //   548: aload           8
        //   550: astore_1       
        //   551: aload_1        
        //   552: astore          6
        //   554: getstatic       com/navdy/hud/app/util/CrashReportService.sLogger:Lcom/navdy/service/library/log/Logger;
        //   557: ldc_w           "Exception while creating the crash report "
        //   560: aload           9
        //   562: invokevirtual   com/navdy/service/library/log/Logger.e:(Ljava/lang/String;Ljava/lang/Throwable;)V
        //   565: aload_1        
        //   566: invokevirtual   java/io/FileWriter.close:()V
        //   569: goto            532
        //   572: astore_1       
        //   573: getstatic       com/navdy/hud/app/util/CrashReportService.sLogger:Lcom/navdy/service/library/log/Logger;
        //   576: ldc_w           "Error closing the File Writer"
        //   579: invokevirtual   com/navdy/service/library/log/Logger.d:(Ljava/lang/String;)V
        //   582: goto            532
        //   585: astore_1       
        //   586: aload           6
        //   588: invokevirtual   java/io/FileWriter.close:()V
        //   591: aload_1        
        //   592: athrow         
        //   593: astore          6
        //   595: getstatic       com/navdy/hud/app/util/CrashReportService.sLogger:Lcom/navdy/service/library/log/Logger;
        //   598: ldc_w           "Error closing the File Writer"
        //   601: invokevirtual   com/navdy/service/library/log/Logger.d:(Ljava/lang/String;)V
        //   604: goto            591
        //   607: astore_1       
        //   608: aload           9
        //   610: astore          6
        //   612: goto            586
        //   615: astore_1       
        //   616: aload           9
        //   618: astore          6
        //   620: aload_1        
        //   621: astore          9
        //   623: aload           6
        //   625: astore_1       
        //   626: goto            551
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  114    120    546    551    Ljava/lang/Throwable;
        //  114    120    585    586    Any
        //  124    129    546    551    Ljava/lang/Throwable;
        //  124    129    585    586    Any
        //  133    140    546    551    Ljava/lang/Throwable;
        //  133    140    585    586    Any
        //  140    237    615    629    Ljava/lang/Throwable;
        //  140    237    607    615    Any
        //  242    252    615    629    Ljava/lang/Throwable;
        //  242    252    607    615    Any
        //  252    319    615    629    Ljava/lang/Throwable;
        //  252    319    607    615    Any
        //  319    340    615    629    Ljava/lang/Throwable;
        //  319    340    607    615    Any
        //  345    352    615    629    Ljava/lang/Throwable;
        //  345    352    607    615    Any
        //  352    359    615    629    Ljava/lang/Throwable;
        //  352    359    607    615    Any
        //  370    375    615    629    Ljava/lang/Throwable;
        //  370    375    607    615    Any
        //  413    527    615    629    Ljava/lang/Throwable;
        //  413    527    607    615    Any
        //  527    532    533    546    Ljava/io/IOException;
        //  554    565    585    586    Any
        //  565    569    572    585    Ljava/io/IOException;
        //  586    591    593    607    Ljava/io/IOException;
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0252:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public static void dumpCrashReportAsync(final CrashType crashType) {
        final Intent intent = new Intent(HudApplication.getAppContext(), (Class)CrashReportService.class);
        intent.setAction("DumpCrashReport");
        intent.putExtra("EXTRA_CRASH_TYPE", crashType.name());
        HudApplication.getAppContext().startService(intent);
    }
    
    public static CrashType getCrashTypeForId(final int n) {
        final CrashType[] values = CrashType.values();
        CrashType crashType;
        if (n >= 0 && n < values.length) {
            crashType = values[n];
        }
        else {
            crashType = null;
        }
        return crashType;
    }
    
    public static void populateFilesQueue(final File[] array, final PriorityBlockingQueue<File> priorityBlockingQueue, final int n) {
        if (array != null) {
            for (final File file : array) {
                if (file.isFile()) {
                    CrashReportService.sLogger.d("File " + file.getName());
                    priorityBlockingQueue.add(file);
                    if (priorityBlockingQueue.size() == n) {
                        final File file2 = priorityBlockingQueue.poll();
                        CrashReportService.sLogger.d("Deleting the old file " + file2.getName());
                        IOUtils.deleteFile(HudApplication.getAppContext(), file2.getAbsolutePath());
                    }
                }
            }
        }
    }
    
    public static String printDeviceInfo(final DeviceInfo deviceInfo) {
        final StringBuilder sb = new StringBuilder();
        sb.append("\nDevice Information\n");
        sb.append("-----------------------\n\n");
        sb.append("Platform :" + deviceInfo.platform.name() + "\n");
        sb.append("Name :" + deviceInfo.deviceName + "\n");
        sb.append("System Version :" + deviceInfo.systemVersion + "\n");
        sb.append("Model :" + deviceInfo.model + "\n");
        sb.append("Device ID :" + deviceInfo.deviceId + "\n");
        sb.append("Make :" + deviceInfo.deviceMake + "\n");
        sb.append("Build Type :" + deviceInfo.buildType + "\n");
        sb.append("API level :" + deviceInfo.systemApiLevel + "\n");
        sb.append("Protocol version :" + deviceInfo.protocolVersion + "\n");
        sb.append("Force Full Update Flag :" + deviceInfo.forceFullUpdate + "\n");
        return sb.toString();
    }
    
    public void onCreate() {
        super.onCreate();
        if (!CrashReportService.mIsInitialized) {
            CrashReportService.sCrashReportsFolder = PathManager.getInstance().getNonFatalCrashReportDir();
            final File[] listFiles = new File(CrashReportService.sCrashReportsFolder).listFiles();
            final Logger sLogger = CrashReportService.sLogger;
            final StringBuilder append = new StringBuilder().append("Number of Fatal crash reports :");
            int length;
            if (listFiles != null) {
                length = listFiles.length;
            }
            else {
                length = 0;
            }
            sLogger.d(append.append(length).toString());
            if (listFiles != null) {
                populateFilesQueue(listFiles, CrashReportService.sCrashReportsToSend, 10);
            }
            CrashReportService.mIsInitialized = true;
        }
    }
    
    protected void onHandleIntent(final Intent intent) {
        while (true) {
            try {
                CrashReportService.sLogger.d("onHandleIntent " + intent.toString());
                if ("DumpCrashReport".equals(intent.getAction())) {
                    final String stringExtra = intent.getStringExtra("EXTRA_CRASH_TYPE");
                    if (stringExtra != null) {
                        CrashReportService.sLogger.d("Dumping a crash " + stringExtra);
                        this.dumpCrashReport(stringExtra);
                    }
                    else {
                        CrashReportService.sLogger.e("Missing crash type");
                    }
                    return;
                }
            }
            catch (Throwable t) {
                CrashReportService.sLogger.e("Exception while handling intent ", t);
                return;
            }
            if (!"AddReport".equals(intent.getAction())) {
                return;
            }
            final String stringExtra2 = intent.getStringExtra("EXTRA_FILE_PATH");
            if (TextUtils.isEmpty((CharSequence)stringExtra2)) {
                return;
            }
            final File file = new File(stringExtra2);
            if (file.exists() && file.isFile()) {
                this.addReport(file);
            }
        }
    }
    
    public enum CrashType
    {
        BLUETOOTH_DISCONNECTED, 
        GENERIC_NON_FATAL_CRASH, 
        OBD_RESET;
    }
    
    public static class FilesModifiedTimeComparator implements Comparator<File>
    {
        @Override
        public int compare(final File file, final File file2) {
            return (int)(file.lastModified() - file2.lastModified());
        }
    }
}
