package com.navdy.hud.app.util;

import com.navdy.service.library.events.LegacyCapability;
import com.navdy.service.library.events.DeviceInfo;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.service.library.events.Capabilities;

public class RemoteCapabilitiesUtil
{
    private static Capabilities getRemoteCapabilities() {
        final DeviceInfo remoteDeviceInfo = RemoteDeviceManager.getInstance().getRemoteDeviceInfo();
        Capabilities capabilities;
        if (remoteDeviceInfo != null) {
            capabilities = remoteDeviceInfo.capabilities;
        }
        else {
            capabilities = null;
        }
        return capabilities;
    }
    
    public static boolean supportsNavigationCoordinateLookup() {
        final Capabilities remoteCapabilities = getRemoteCapabilities();
        return remoteCapabilities != null && Boolean.TRUE.equals(remoteCapabilities.navCoordsLookup);
    }
    
    public static boolean supportsPlaceSearch() {
        final Capabilities remoteCapabilities = getRemoteCapabilities();
        boolean b;
        if (remoteCapabilities != null) {
            b = Boolean.TRUE.equals(remoteCapabilities.placeTypeSearch);
        }
        else {
            b = RemoteDeviceManager.getInstance().doesRemoteDeviceHasCapability(LegacyCapability.CAPABILITY_PLACE_TYPE_SEARCH);
        }
        return b;
    }
    
    public static boolean supportsVoiceSearch() {
        final Capabilities remoteCapabilities = getRemoteCapabilities();
        return remoteCapabilities != null && Boolean.TRUE.equals(remoteCapabilities.voiceSearch);
    }
    
    public static boolean supportsVoiceSearchNewIOSPauseBehaviors() {
        final Capabilities remoteCapabilities = getRemoteCapabilities();
        return remoteCapabilities != null && Boolean.TRUE.equals(remoteCapabilities.voiceSearchNewIOSPauseBehaviors);
    }
}
