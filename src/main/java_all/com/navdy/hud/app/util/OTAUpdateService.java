package com.navdy.hud.app.util;

import android.content.BroadcastReceiver;
import mortar.Mortar;
import android.os.IBinder;
import java.security.GeneralSecurityException;
import android.os.RecoverySystem;
import com.navdy.hud.app.device.dial.DialManager;
import com.navdy.hud.app.obd.ObdDeviceConfigurationManager;
import android.content.Intent;
import java.lang.reflect.InvocationTargetException;
import android.os.RecoverySystem;
import com.navdy.hud.app.event.Shutdown;
import java.util.regex.Matcher;
import com.navdy.hud.app.HudApplication;
import com.navdy.service.library.events.file.FileType;
import com.navdy.hud.app.storage.PathManager;
import com.navdy.service.library.task.TaskManager;
import android.os.Build;
import com.navdy.hud.app.analytics.AnalyticsSupport;
import android.text.TextUtils;
import com.navdy.service.library.util.IOUtils;
import java.io.IOException;
import java.io.File;
import android.content.Context;
import android.content.SharedPreferences;
import javax.inject.Inject;
import com.squareup.otto.Bus;
import com.navdy.service.library.log.Logger;
import java.util.regex.Pattern;
import android.app.Service;

public class OTAUpdateService extends Service
{
    public static final int CAPTURE_GROUP_FROM_VERSION = 2;
    public static final int CAPTURE_GROUP_TARGET_VERSION = 3;
    public static final String COMMAND_CHECK_UPDATE_ON_REBOOT = "CHECK_UPDATE";
    public static final String COMMAND_DO_NOT_PROMPT = "DO_NOT_PROMPT";
    public static final String COMMAND_INSTALL_UPDATE = "INSTALL_UPDATE";
    public static final String COMMAND_INSTALL_UPDATE_REBOOT_QUIET = "INSTALL_UPDATE_REBOOT_QUIET";
    public static final String COMMAND_INSTALL_UPDATE_SHUTDOWN = "INSTALL_UPDATE_SHUTDOWN";
    public static final String COMMAND_VERIFY_UPDATE = "VERIFY_UPDATE";
    public static final String DO_NOT_PROMPT = "do_not_prompt";
    public static final String EXTRA_COMMAND = "COMMAND";
    private static final String FAILED_OTA_FILE = "failed_ota_file";
    private static final long MAX_FILE_SIZE = 2147483648L;
    public static final int MAX_RETRY_COUNT = 3;
    private static final int MAX_ZIP_ENTRIES = 1000;
    public static final String RECOVERY_LAST_INSTALL_FILE_PATH = "/cache/recovery/last_install";
    private static final String RECOVERY_LAST_LOG_FILE_PATH = "/cache/recovery/last_log";
    public static final String RETRY_COUNT = "retry_count";
    public static final String UPDATE_FILE = "last_update_file_received";
    private static final String UPDATE_FROM_VERSION = "update_from_version";
    public static final String VERIFIED = "verified";
    public static final String VERSION_PREFIX = "1.0.";
    private static final Pattern sFileNamePattern;
    private static final Logger sLogger;
    private static String swVersion;
    @Inject
    Bus bus;
    private boolean installing;
    @Inject
    SharedPreferences sharedPreferences;
    
    static {
        sLogger = new Logger(OTAUpdateService.class);
        sFileNamePattern = Pattern.compile("^navdy_ota_(([0-9]+)_to_)?([0-9]+)\\.zip");
    }
    
    private boolean checkIfIncrementalUpdatesFailed(final File ex) {
        final File file = new File("/cache/recovery/last_install");
        boolean b2;
        final boolean b = b2 = true;
        if (!file.exists()) {
            return b2;
        }
        try {
            final String convertFileToString = IOUtils.convertFileToString(file.getAbsolutePath());
            b2 = b;
            if (TextUtils.isEmpty((CharSequence)convertFileToString)) {
                return b2;
            }
            final String[] split = convertFileToString.split("\n");
            b2 = b;
            if (split.length < 2) {
                return b2;
            }
            final String s = split[0];
            OTAUpdateService.sLogger.d("Last installed update was " + s);
            final String name = new File(s).getName();
            Label_0418: {
                if (Integer.parseInt(split[1]) == 0) {
                    break Label_0418;
                }
                boolean b3 = true;
            Label_0209_Outer:
                while (true) {
                    if (!b3) {
                        this.reportOTAFailure(name);
                    }
                    final Logger sLogger = OTAUpdateService.sLogger;
                    final StringBuilder append = new StringBuilder().append("Last install succeeded: ");
                    Label_0424: {
                        if (!b3) {
                            break Label_0424;
                        }
                        String s2 = "1";
                        sLogger.d(append.append(s2).toString());
                        final int fromVersion = extractFromVersion(name);
                        Label_0432: {
                            if (fromVersion == -1) {
                                break Label_0432;
                            }
                            boolean b4 = true;
                            while (true) {
                                final String string = this.sharedPreferences.getString("update_from_version", "");
                                if (!TextUtils.isEmpty((CharSequence)string)) {
                                    AnalyticsSupport.recordOTAInstallResult(b4, string, b3);
                                    this.sharedPreferences.edit().remove("update_from_version").commit();
                                }
                                b2 = b;
                                if (!b4) {
                                    return b2;
                                }
                                OTAUpdateService.sLogger.d("Last install was an incremental update");
                                int int1 = 0;
                                try {
                                    int1 = Integer.parseInt(Build$VERSION.INCREMENTAL);
                                    b2 = b;
                                    if (int1 == fromVersion) {
                                        OTAUpdateService.sLogger.d("The last installed incremental from version is same as current version :" + int1);
                                        b2 = b;
                                        if (!b3) {
                                            OTAUpdateService.sLogger.e("Last incremental update has failed");
                                            this.bus.post(new IncrementalOTAFailureDetected());
                                            b2 = b;
                                            if (ex != null) {
                                                b2 = b;
                                                if (((File)ex).getAbsolutePath().equals(s)) {
                                                    OTAUpdateService.sLogger.d("Current update is the same as the one failed during last install");
                                                    b2 = false;
                                                }
                                            }
                                        }
                                    }
                                    return b2;
                                    b3 = false;
                                    continue Label_0209_Outer;
                                    b4 = false;
                                    continue;
                                    s2 = "0";
                                }
                                catch (NumberFormatException ex2) {
                                    OTAUpdateService.sLogger.e("Cannot parse the incremental version of the build :" + Build$VERSION.INCREMENTAL);
                                }
                                break;
                            }
                        }
                    }
                    break;
                }
            }
        }
        catch (IOException ex) {
            OTAUpdateService.sLogger.e("Error reading the file :" + file.getAbsolutePath(), ex);
            b2 = b;
            return b2;
        }
    }
    
    private static File checkUpdateFile(final Context context, final SharedPreferences sharedPreferences) {
        OTAUpdateService.sLogger.d("Checking for update file");
        OTAUpdateService.swVersion = null;
        final String string = sharedPreferences.getString("last_update_file_received", (String)null);
        File file;
        if (string == null) {
            OTAUpdateService.sLogger.e("No update available");
            file = null;
        }
        else {
            final File file2 = new File(string);
            if (!file2.isFile() || !file2.exists() || !file2.canRead()) {
                OTAUpdateService.sLogger.e("Invalid update file");
                file = null;
            }
            else {
                int int1 = 0;
                int incrementalVersion;
                while (true) {
                    try {
                        int1 = Integer.parseInt(Build$VERSION.INCREMENTAL);
                        OTAUpdateService.sLogger.d("Incremental version of the build on the device:" + int1);
                        incrementalVersion = extractIncrementalVersion(file2.getName());
                        final int fromVersion = extractFromVersion(file2.getName());
                        OTAUpdateService.sLogger.d("Update from version " + fromVersion);
                        if (fromVersion != -1 && int1 != 0 && fromVersion != int1) {
                            OTAUpdateService.sLogger.e("Update from version mismatch " + fromVersion + " " + int1);
                            clearUpdate(file2, context, sharedPreferences);
                            file = null;
                            return file;
                        }
                    }
                    catch (NumberFormatException ex) {
                        OTAUpdateService.sLogger.e("Cannot parse the incremental version of the build :" + Build$VERSION.INCREMENTAL);
                        continue;
                    }
                    break;
                }
                if (incrementalVersion <= int1) {
                    OTAUpdateService.sLogger.e("Already up to date :" + int1);
                    clearUpdate(file2, context, sharedPreferences);
                    file = null;
                }
                else {
                    file = file2;
                    if (sharedPreferences.getInt("retry_count", 0) >= 3) {
                        OTAUpdateService.sLogger.d("Maximum retries. Deleting the update file.");
                        clearUpdate(file2, context, sharedPreferences);
                        file = null;
                    }
                }
            }
        }
        return file;
    }
    
    private void cleanupOldFiles() {
        TaskManager.getInstance().execute(new Runnable() {
            @Override
            public void run() {
                int access$700 = -1;
                while (true) {
                    try {
                        final int int1 = Integer.parseInt(Build$VERSION.INCREMENTAL);
                        access$700 = int1;
                        if (int1 == -1) {
                            final String string = OTAUpdateService.this.sharedPreferences.getString("last_update_file_received", (String)null);
                            access$700 = int1;
                            if (!TextUtils.isEmpty((CharSequence)string)) {
                                access$700 = extractIncrementalVersion(string);
                            }
                        }
                        if (access$700 > 0) {
                            final File file = new File(PathManager.getInstance().getDirectoryForFileType(FileType.FILE_TYPE_OTA));
                            if (file.exists()) {
                                final File[] listFiles = file.listFiles();
                                if (listFiles != null) {
                                    for (final File file2 : listFiles) {
                                        if (file2.isFile() && OTAUpdateService.isOtaFile(file2.getName()) && extractIncrementalVersion(file2.getName()) <= access$700) {
                                            OTAUpdateService.sLogger.e("Clearing the old update file :" + file2.getName());
                                            IOUtils.deleteFile(HudApplication.getAppContext(), file2.getAbsolutePath());
                                        }
                                    }
                                }
                            }
                        }
                    }
                    catch (NumberFormatException ex) {
                        OTAUpdateService.sLogger.e("Cannot parse the incremental version of the build :" + Build$VERSION.INCREMENTAL);
                        final int int1 = access$700;
                        continue;
                    }
                    break;
                }
            }
        }, 1);
    }
    
    public static void clearUpdate(final File file, final Context context, final SharedPreferences sharedPreferences) {
        if (file != null) {
            OTAUpdateService.sLogger.d("Clearing the update");
            IOUtils.deleteFile(context, file.getAbsolutePath());
        }
        sharedPreferences.edit().remove("last_update_file_received").remove("retry_count").remove("do_not_prompt").remove("verified").commit();
    }
    
    private static int extractFromVersion(String group) {
        final int n = -1;
        final Matcher matcher = OTAUpdateService.sFileNamePattern.matcher(group);
        int int1 = n;
        if (matcher.find()) {
            int1 = n;
            if (matcher.groupCount() > 0) {
                group = matcher.group(2);
                if (group == null) {
                    int1 = n;
                }
                else {
                    try {
                        int1 = Integer.parseInt(group);
                    }
                    catch (NumberFormatException ex) {
                        OTAUpdateService.sLogger.e("Cannot parse the file to retrieve the from version" + ex);
                        ex.printStackTrace();
                        int1 = n;
                    }
                }
            }
        }
        return int1;
    }
    
    private static int extractIncrementalVersion(String group) {
        final Matcher matcher = OTAUpdateService.sFileNamePattern.matcher(group);
        if (!matcher.find() || matcher.groupCount() <= 0) {
            return -1;
        }
        group = matcher.group(3);
        try {
            return Integer.parseInt(group);
        }
        catch (NumberFormatException ex) {
            OTAUpdateService.sLogger.e("Cannot parse the file to retrieve the target version" + ex);
            ex.printStackTrace();
        }
        return -1;
    }
    
    public static String getCurrentVersion() {
        return shortVersion("1.3.3051-corona");
    }
    
    public static String getIncrementalUpdateVersion(final SharedPreferences sharedPreferences) {
        final String s = null;
        final String string = sharedPreferences.getString("last_update_file_received", (String)null);
        String value;
        if (string == null) {
            value = s;
        }
        else {
            final int incrementalVersion = extractIncrementalVersion(new File(string).getName());
            value = s;
            if (incrementalVersion > 0) {
                value = String.valueOf(incrementalVersion);
            }
        }
        return value;
    }
    
    public static String getSWUpdateVersion() {
        return OTAUpdateService.swVersion;
    }
    
    private static String getUpdateVersion(final SharedPreferences p0) {
        // 
        This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: astore_1       
        //     2: aload_0        
        //     3: ldc             "last_update_file_received"
        //     5: aconst_null    
        //     6: invokeinterface android/content/SharedPreferences.getString:(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
        //    11: astore_2       
        //    12: aload_2        
        //    13: ifnonnull       18
        //    16: aload_1        
        //    17: areturn        
        //    18: new             Ljava/io/File;
        //    21: dup            
        //    22: aload_2        
        //    23: invokespecial   java/io/File.<init>:(Ljava/lang/String;)V
        //    26: astore_3       
        //    27: aconst_null    
        //    28: astore          4
        //    30: aconst_null    
        //    31: astore          5
        //    33: aconst_null    
        //    34: astore          6
        //    36: aconst_null    
        //    37: astore          7
        //    39: aconst_null    
        //    40: astore          8
        //    42: aconst_null    
        //    43: astore          9
        //    45: aconst_null    
        //    46: astore          10
        //    48: aconst_null    
        //    49: astore          11
        //    51: aload           6
        //    53: astore_1       
        //    54: aload           4
        //    56: astore_2       
        //    57: new             Ljava/util/zip/ZipFile;
        //    60: astore          12
        //    62: aload           6
        //    64: astore_1       
        //    65: aload           4
        //    67: astore_2       
        //    68: aload           12
        //    70: aload_3        
        //    71: invokespecial   java/util/zip/ZipFile.<init>:(Ljava/io/File;)V
        //    74: aload           7
        //    76: astore          5
        //    78: aload           8
        //    80: astore_2       
        //    81: aload           10
        //    83: astore_1       
        //    84: aload           12
        //    86: ldc_w           "META-INF/com/android/metadata"
        //    89: invokevirtual   java/util/zip/ZipFile.getEntry:(Ljava/lang/String;)Ljava/util/zip/ZipEntry;
        //    92: astore          11
        //    94: aload           7
        //    96: astore          5
        //    98: aload           8
        //   100: astore_2       
        //   101: aload           10
        //   103: astore_1       
        //   104: new             Ljava/util/Properties;
        //   107: astore          9
        //   109: aload           7
        //   111: astore          5
        //   113: aload           8
        //   115: astore_2       
        //   116: aload           10
        //   118: astore_1       
        //   119: aload           9
        //   121: invokespecial   java/util/Properties.<init>:()V
        //   124: aload           7
        //   126: astore          5
        //   128: aload           8
        //   130: astore_2       
        //   131: aload           10
        //   133: astore_1       
        //   134: aload           12
        //   136: aload           11
        //   138: invokevirtual   java/util/zip/ZipFile.getInputStream:(Ljava/util/zip/ZipEntry;)Ljava/io/InputStream;
        //   141: astore          7
        //   143: aload           7
        //   145: astore          5
        //   147: aload           7
        //   149: astore_2       
        //   150: aload           10
        //   152: astore_1       
        //   153: aload           9
        //   155: aload           7
        //   157: invokevirtual   java/util/Properties.load:(Ljava/io/InputStream;)V
        //   160: aload           7
        //   162: astore          5
        //   164: aload           7
        //   166: astore_2       
        //   167: aload           10
        //   169: astore_1       
        //   170: aload           9
        //   172: ldc_w           "version-name"
        //   175: invokevirtual   java/util/Properties.getProperty:(Ljava/lang/String;)Ljava/lang/String;
        //   178: astore          10
        //   180: aload           7
        //   182: astore          5
        //   184: aload           7
        //   186: astore_2       
        //   187: aload           10
        //   189: astore_1       
        //   190: aload           10
        //   192: invokestatic    com/navdy/hud/app/util/OTAUpdateService.shortVersion:(Ljava/lang/String;)Ljava/lang/String;
        //   195: astore          10
        //   197: aload           10
        //   199: astore_1       
        //   200: aload           7
        //   202: invokestatic    com/navdy/service/library/util/IOUtils.closeObject:(Ljava/io/Closeable;)V
        //   205: aload           12
        //   207: invokestatic    com/navdy/service/library/util/IOUtils.closeObject:(Ljava/io/Closeable;)V
        //   210: aload_1        
        //   211: astore          7
        //   213: aload           7
        //   215: astore_1       
        //   216: aload           7
        //   218: invokestatic    android/text/TextUtils.isEmpty:(Ljava/lang/CharSequence;)Z
        //   221: ifeq            16
        //   224: new             Ljava/lang/StringBuilder;
        //   227: dup            
        //   228: invokespecial   java/lang/StringBuilder.<init>:()V
        //   231: ldc             "1.0."
        //   233: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   236: aload_0        
        //   237: invokestatic    com/navdy/hud/app/util/OTAUpdateService.getIncrementalUpdateVersion:(Landroid/content/SharedPreferences;)Ljava/lang/String;
        //   240: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   243: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   246: astore_1       
        //   247: goto            16
        //   250: astore          10
        //   252: aload           11
        //   254: astore          7
        //   256: aload           9
        //   258: astore          12
        //   260: aload           12
        //   262: astore_1       
        //   263: aload           5
        //   265: astore_2       
        //   266: getstatic       com/navdy/hud/app/util/OTAUpdateService.sLogger:Lcom/navdy/service/library/log/Logger;
        //   269: ldc_w           "Error extracting version-name from ota metadata "
        //   272: aload           10
        //   274: invokevirtual   com/navdy/service/library/log/Logger.w:(Ljava/lang/String;Ljava/lang/Throwable;)V
        //   277: aload           12
        //   279: invokestatic    com/navdy/service/library/util/IOUtils.closeObject:(Ljava/io/Closeable;)V
        //   282: aload           5
        //   284: invokestatic    com/navdy/service/library/util/IOUtils.closeObject:(Ljava/io/Closeable;)V
        //   287: goto            213
        //   290: astore_0       
        //   291: aload_1        
        //   292: invokestatic    com/navdy/service/library/util/IOUtils.closeObject:(Ljava/io/Closeable;)V
        //   295: aload_2        
        //   296: invokestatic    com/navdy/service/library/util/IOUtils.closeObject:(Ljava/io/Closeable;)V
        //   299: aload_0        
        //   300: athrow         
        //   301: astore_0       
        //   302: aload           12
        //   304: astore_2       
        //   305: aload           5
        //   307: astore_1       
        //   308: goto            291
        //   311: astore          10
        //   313: aload           12
        //   315: astore          5
        //   317: aload_2        
        //   318: astore          12
        //   320: aload_1        
        //   321: astore          7
        //   323: goto            260
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  57     62     250    260    Ljava/io/IOException;
        //  57     62     290    291    Any
        //  68     74     250    260    Ljava/io/IOException;
        //  68     74     290    291    Any
        //  84     94     311    326    Ljava/io/IOException;
        //  84     94     301    311    Any
        //  104    109    311    326    Ljava/io/IOException;
        //  104    109    301    311    Any
        //  119    124    311    326    Ljava/io/IOException;
        //  119    124    301    311    Any
        //  134    143    311    326    Ljava/io/IOException;
        //  134    143    301    311    Any
        //  153    160    311    326    Ljava/io/IOException;
        //  153    160    301    311    Any
        //  170    180    311    326    Ljava/io/IOException;
        //  170    180    301    311    Any
        //  190    197    311    326    Ljava/io/IOException;
        //  190    197    301    311    Any
        //  266    277    290    291    Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0213:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    private void installPackage(Context cause, final File file, final String s) throws IOException {
        AnalyticsSupport.recordShutdown(Shutdown.Reason.OTA, false);
        try {
            RecoverySystem.class.getMethod("installPackage", Context.class, File.class, String.class).invoke(null, cause, file, s);
        }
        catch (InvocationTargetException ex) {
            cause = (Context)ex.getCause();
            if (cause instanceof IOException) {
                throw (IOException)cause;
            }
            OTAUpdateService.sLogger.e("unexpected exception from RecoverySystem.installPackage()" + cause);
        }
        catch (Exception ex2) {
            OTAUpdateService.sLogger.e("error invoking Recovery.installPackage(): " + ex2);
            RecoverySystem.installPackage(cause, file);
        }
    }
    
    public static boolean isOTAUpdateVerified(final SharedPreferences sharedPreferences) {
        return sharedPreferences.getBoolean("verified", false);
    }
    
    public static boolean isOtaFile(final String s) {
        final Matcher matcher = OTAUpdateService.sFileNamePattern.matcher(s);
        return matcher != null && matcher.find();
    }
    
    public static boolean isUpdateAvailable() {
        return !TextUtils.isEmpty((CharSequence)OTAUpdateService.swVersion);
    }
    
    private void reportOTAFailure(final String p0) {
        // 
        This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: aload_0        
        //     2: getfield        com/navdy/hud/app/util/OTAUpdateService.sharedPreferences:Landroid/content/SharedPreferences;
        //     5: ldc             "failed_ota_file"
        //     7: ldc             ""
        //     9: invokeinterface android/content/SharedPreferences.getString:(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
        //    14: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //    17: ifeq            47
        //    20: getstatic       com/navdy/hud/app/util/OTAUpdateService.sLogger:Lcom/navdy/service/library/log/Logger;
        //    23: new             Ljava/lang/StringBuilder;
        //    26: dup            
        //    27: invokespecial   java/lang/StringBuilder.<init>:()V
        //    30: ldc_w           "update failure already reported for "
        //    33: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    36: aload_1        
        //    37: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    40: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //    43: invokevirtual   com/navdy/service/library/log/Logger.v:(Ljava/lang/String;)V
        //    46: return         
        //    47: new             Ljava/lang/StringBuilder;
        //    50: astore_2       
        //    51: aload_2        
        //    52: invokespecial   java/lang/StringBuilder.<init>:()V
        //    55: aload_2        
        //    56: ldc_w           "\n========== /cache/recovery/last_install\n"
        //    59: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    62: ldc             "/cache/recovery/last_install"
        //    64: invokestatic    com/navdy/service/library/util/IOUtils.convertFileToString:(Ljava/lang/String;)Ljava/lang/String;
        //    67: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    70: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //    73: astore_2       
        //    74: new             Ljava/lang/StringBuilder;
        //    77: astore_3       
        //    78: aload_3        
        //    79: invokespecial   java/lang/StringBuilder.<init>:()V
        //    82: aload_3        
        //    83: ldc_w           "\n========== /cache/recovery/last_log\n"
        //    86: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    89: ldc             "/cache/recovery/last_log"
        //    91: invokestatic    com/navdy/service/library/util/IOUtils.convertFileToString:(Ljava/lang/String;)Ljava/lang/String;
        //    94: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    97: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   100: astore_3       
        //   101: new             Lcom/navdy/hud/app/util/OTAUpdateService$OTAFailedException;
        //   104: dup            
        //   105: aload_2        
        //   106: aload_3        
        //   107: invokespecial   com/navdy/hud/app/util/OTAUpdateService$OTAFailedException.<init>:(Ljava/lang/String;Ljava/lang/String;)V
        //   110: astore_2       
        //   111: invokestatic    com/navdy/service/library/task/TaskManager.getInstance:()Lcom/navdy/service/library/task/TaskManager;
        //   114: new             Lcom/navdy/hud/app/util/OTAUpdateService$3;
        //   117: dup            
        //   118: aload_0        
        //   119: aload_2        
        //   120: invokespecial   com/navdy/hud/app/util/OTAUpdateService$3.<init>:(Lcom/navdy/hud/app/util/OTAUpdateService;Lcom/navdy/hud/app/util/OTAUpdateService$OTAFailedException;)V
        //   123: iconst_1       
        //   124: invokevirtual   com/navdy/service/library/task/TaskManager.execute:(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;
        //   127: pop            
        //   128: aload_0        
        //   129: getfield        com/navdy/hud/app/util/OTAUpdateService.sharedPreferences:Landroid/content/SharedPreferences;
        //   132: invokeinterface android/content/SharedPreferences.edit:()Landroid/content/SharedPreferences$Editor;
        //   137: ldc             "failed_ota_file"
        //   139: aload_1        
        //   140: invokeinterface android/content/SharedPreferences$Editor.putString:(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;
        //   145: invokeinterface android/content/SharedPreferences$Editor.commit:()Z
        //   150: pop            
        //   151: goto            46
        //   154: astore_3       
        //   155: ldc_w           "\n*** failed to read /cache/recovery/last_install"
        //   158: astore_2       
        //   159: getstatic       com/navdy/hud/app/util/OTAUpdateService.sLogger:Lcom/navdy/service/library/log/Logger;
        //   162: ldc_w           "exception reading /cache/recovery/last_install"
        //   165: aload_3        
        //   166: invokevirtual   com/navdy/service/library/log/Logger.e:(Ljava/lang/String;Ljava/lang/Throwable;)V
        //   169: goto            74
        //   172: astore          4
        //   174: ldc_w           "\n*** failed to read /cache/recovery/last_log"
        //   177: astore_3       
        //   178: getstatic       com/navdy/hud/app/util/OTAUpdateService.sLogger:Lcom/navdy/service/library/log/Logger;
        //   181: ldc_w           "exception reading /cache/recovery/last_log"
        //   184: aload           4
        //   186: invokevirtual   com/navdy/service/library/log/Logger.e:(Ljava/lang/String;Ljava/lang/Throwable;)V
        //   189: goto            101
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  47     74     154    172    Ljava/io/IOException;
        //  74     101    172    192    Ljava/io/IOException;
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0074:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public static String shortVersion(String s) {
        if (s != null) {
            s = s.split("-")[0];
        }
        else {
            s = null;
        }
        return s;
    }
    
    public static void startServiceToVerifyUpdate() {
        final Context appContext = HudApplication.getAppContext();
        final Intent intent = new Intent(appContext, (Class)OTAUpdateService.class);
        intent.putExtra("COMMAND", "VERIFY_UPDATE");
        appContext.startService(intent);
    }
    
    private void update(final String s) {
        if (!this.installing) {
            this.installing = true;
            this.bus.post(new InstallingUpdate());
            TaskManager.getInstance().execute(new Runnable() {
                @Override
                public void run() {
                    try {
                        OTAUpdateService.sLogger.d("Applying the update");
                        final File access$100 = checkUpdateFile((Context)OTAUpdateService.this, OTAUpdateService.this.sharedPreferences);
                        Label_0224: {
                            if (access$100 == null) {
                                break Label_0224;
                            }
                            try {
                                final int int1 = OTAUpdateService.this.sharedPreferences.getInt("retry_count", 0);
                                OTAUpdateService.this.sharedPreferences.edit().putInt("retry_count", int1 + 1).putString("update_from_version", Build$VERSION.INCREMENTAL).commit();
                                OTAUpdateService.sLogger.d("Trying to the install the update, retry count :" + (int1 + 1));
                                try {
                                    AnalyticsSupport.recordInstallOTAUpdate(OTAUpdateService.this.sharedPreferences);
                                    CrashReporter.getInstance().stopCrashReporting(true);
                                    OTAUpdateService.sLogger.i("NAVDY_NOT_A_CRASH");
                                    ObdDeviceConfigurationManager.turnOffOBDSleep();
                                    DialManager.getInstance().requestDialReboot(false);
                                    Thread.sleep(2000L);
                                    OTAUpdateService.this.installPackage((Context)OTAUpdateService.this, access$100, s);
                                    return;
                                }
                                catch (Throwable t) {
                                    OTAUpdateService.sLogger.e("Exception while reporting the update", t);
                                }
                            }
                            catch (Throwable access$100) {
                                OTAUpdateService.sLogger.e("Exception while running update OTA package", (Throwable)access$100);
                                CrashReporter.getInstance().reportNonFatalException((Throwable)access$100);
                                return;
                            }
                        }
                        OTAUpdateService.clearUpdate(access$100, (Context)OTAUpdateService.this, OTAUpdateService.this.sharedPreferences);
                    }
                    catch (Exception access$100) {
                        OTAUpdateService.sLogger.e("Failed to install OTA package", (Throwable)access$100);
                        OTAUpdateService.this.installing = false;
                    }
                    finally {
                        OTAUpdateService.this.installing = false;
                    }
                }
            }, 1);
        }
    }
    
    public static boolean verifyOTAZipFile(final File p0, final int p1, final long p2) {
        // 
        This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: astore          4
        //     3: aconst_null    
        //     4: astore          5
        //     6: aload           4
        //     8: astore          6
        //    10: aload_0        
        //    11: invokevirtual   java/io/File.getParentFile:()Ljava/io/File;
        //    14: astore          7
        //    16: aload           4
        //    18: astore          6
        //    20: aload           7
        //    22: invokevirtual   java/io/File.getCanonicalPath:()Ljava/lang/String;
        //    25: astore          8
        //    27: aload           4
        //    29: astore          6
        //    31: new             Ljava/util/zip/ZipFile;
        //    34: astore          9
        //    36: aload           4
        //    38: astore          6
        //    40: aload           9
        //    42: aload_0        
        //    43: invokespecial   java/util/zip/ZipFile.<init>:(Ljava/io/File;)V
        //    46: aload           9
        //    48: invokevirtual   java/util/zip/ZipFile.entries:()Ljava/util/Enumeration;
        //    51: astore          5
        //    53: iconst_0       
        //    54: istore          10
        //    56: lconst_0       
        //    57: lstore          11
        //    59: aload           5
        //    61: invokeinterface java/util/Enumeration.hasMoreElements:()Z
        //    66: ifeq            323
        //    69: aload           5
        //    71: invokeinterface java/util/Enumeration.nextElement:()Ljava/lang/Object;
        //    76: checkcast       Ljava/util/zip/ZipEntry;
        //    79: astore_0       
        //    80: iinc            10, 1
        //    83: lload           11
        //    85: lstore          13
        //    87: aload_0        
        //    88: invokevirtual   java/util/zip/ZipEntry.isDirectory:()Z
        //    91: ifne            118
        //    94: aload_0        
        //    95: invokevirtual   java/util/zip/ZipEntry.getSize:()J
        //    98: lstore          15
        //   100: lload           11
        //   102: lstore          13
        //   104: lload           15
        //   106: lconst_0       
        //   107: lcmp           
        //   108: ifle            118
        //   111: lload           11
        //   113: lload           15
        //   115: ladd           
        //   116: lstore          13
        //   118: iload           10
        //   120: iload_1        
        //   121: if_icmple       168
        //   124: getstatic       com/navdy/hud/app/util/OTAUpdateService.sLogger:Lcom/navdy/service/library/log/Logger;
        //   127: astore          6
        //   129: new             Ljava/lang/StringBuilder;
        //   132: astore_0       
        //   133: aload_0        
        //   134: invokespecial   java/lang/StringBuilder.<init>:()V
        //   137: aload           6
        //   139: aload_0        
        //   140: ldc_w           "OTA zip file failed verification : Too many entries "
        //   143: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   146: iload           10
        //   148: invokevirtual   java/lang/StringBuilder.append:(I)Ljava/lang/StringBuilder;
        //   151: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   154: invokevirtual   com/navdy/service/library/log/Logger.d:(Ljava/lang/String;)V
        //   157: iconst_0       
        //   158: istore          17
        //   160: aload           9
        //   162: invokestatic    com/navdy/service/library/util/IOUtils.closeObject:(Ljava/io/Closeable;)V
        //   165: iload           17
        //   167: ireturn        
        //   168: lload           13
        //   170: lload_2        
        //   171: lcmp           
        //   172: ifle            218
        //   175: getstatic       com/navdy/hud/app/util/OTAUpdateService.sLogger:Lcom/navdy/service/library/log/Logger;
        //   178: astore          6
        //   180: new             Ljava/lang/StringBuilder;
        //   183: astore_0       
        //   184: aload_0        
        //   185: invokespecial   java/lang/StringBuilder.<init>:()V
        //   188: aload           6
        //   190: aload_0        
        //   191: ldc_w           "OTA zip file failed verification : Exceeded max uncompressed size "
        //   194: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   197: lload_2        
        //   198: invokevirtual   java/lang/StringBuilder.append:(J)Ljava/lang/StringBuilder;
        //   201: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   204: invokevirtual   com/navdy/service/library/log/Logger.d:(Ljava/lang/String;)V
        //   207: iconst_0       
        //   208: istore          17
        //   210: aload           9
        //   212: invokestatic    com/navdy/service/library/util/IOUtils.closeObject:(Ljava/io/Closeable;)V
        //   215: goto            165
        //   218: new             Ljava/io/File;
        //   221: astore          6
        //   223: aload           6
        //   225: aload           7
        //   227: aload_0        
        //   228: invokevirtual   java/util/zip/ZipEntry.getName:()Ljava/lang/String;
        //   231: invokespecial   java/io/File.<init>:(Ljava/io/File;Ljava/lang/String;)V
        //   234: aload           6
        //   236: invokevirtual   java/io/File.getCanonicalPath:()Ljava/lang/String;
        //   239: astore          6
        //   241: aload           6
        //   243: invokestatic    android/text/TextUtils.isEmpty:(Ljava/lang/CharSequence;)Z
        //   246: ifne            263
        //   249: lload           13
        //   251: lstore          11
        //   253: aload           6
        //   255: aload           8
        //   257: invokevirtual   java/lang/String.startsWith:(Ljava/lang/String;)Z
        //   260: ifne            59
        //   263: getstatic       com/navdy/hud/app/util/OTAUpdateService.sLogger:Lcom/navdy/service/library/log/Logger;
        //   266: astore          5
        //   268: new             Ljava/lang/StringBuilder;
        //   271: astore          4
        //   273: aload           4
        //   275: invokespecial   java/lang/StringBuilder.<init>:()V
        //   278: aload           5
        //   280: aload           4
        //   282: ldc_w           "OTA zip failed verification : Zip entry tried to write outside destination "
        //   285: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   288: aload_0        
        //   289: invokevirtual   java/util/zip/ZipEntry.getName:()Ljava/lang/String;
        //   292: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   295: ldc_w           " , Canonical path "
        //   298: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   301: aload           6
        //   303: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   306: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   309: invokevirtual   com/navdy/service/library/log/Logger.d:(Ljava/lang/String;)V
        //   312: iconst_0       
        //   313: istore          17
        //   315: aload           9
        //   317: invokestatic    com/navdy/service/library/util/IOUtils.closeObject:(Ljava/io/Closeable;)V
        //   320: goto            165
        //   323: iconst_1       
        //   324: istore          17
        //   326: aload           9
        //   328: invokestatic    com/navdy/service/library/util/IOUtils.closeObject:(Ljava/io/Closeable;)V
        //   331: goto            165
        //   334: astore          9
        //   336: aload           5
        //   338: astore_0       
        //   339: aload_0        
        //   340: astore          6
        //   342: getstatic       com/navdy/hud/app/util/OTAUpdateService.sLogger:Lcom/navdy/service/library/log/Logger;
        //   345: astore          5
        //   347: aload_0        
        //   348: astore          6
        //   350: new             Ljava/lang/StringBuilder;
        //   353: astore          4
        //   355: aload_0        
        //   356: astore          6
        //   358: aload           4
        //   360: invokespecial   java/lang/StringBuilder.<init>:()V
        //   363: aload_0        
        //   364: astore          6
        //   366: aload           5
        //   368: aload           4
        //   370: ldc_w           "Exception while verifying the OTA package "
        //   373: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   376: aload           9
        //   378: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //   381: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   384: invokevirtual   com/navdy/service/library/log/Logger.e:(Ljava/lang/String;)V
        //   387: aload_0        
        //   388: astore          6
        //   390: aload           9
        //   392: invokevirtual   java/io/IOException.printStackTrace:()V
        //   395: aload_0        
        //   396: invokestatic    com/navdy/service/library/util/IOUtils.closeObject:(Ljava/io/Closeable;)V
        //   399: iconst_0       
        //   400: istore          17
        //   402: goto            165
        //   405: astore_0       
        //   406: aload           6
        //   408: invokestatic    com/navdy/service/library/util/IOUtils.closeObject:(Ljava/io/Closeable;)V
        //   411: aload_0        
        //   412: athrow         
        //   413: astore_0       
        //   414: aload           9
        //   416: astore          6
        //   418: goto            406
        //   421: astore          6
        //   423: aload           9
        //   425: astore_0       
        //   426: aload           6
        //   428: astore          9
        //   430: goto            339
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  10     16     334    339    Ljava/io/IOException;
        //  10     16     405    406    Any
        //  20     27     334    339    Ljava/io/IOException;
        //  20     27     405    406    Any
        //  31     36     334    339    Ljava/io/IOException;
        //  31     36     405    406    Any
        //  40     46     334    339    Ljava/io/IOException;
        //  40     46     405    406    Any
        //  46     53     421    433    Ljava/io/IOException;
        //  46     53     413    421    Any
        //  59     80     421    433    Ljava/io/IOException;
        //  59     80     413    421    Any
        //  87     100    421    433    Ljava/io/IOException;
        //  87     100    413    421    Any
        //  124    157    421    433    Ljava/io/IOException;
        //  124    157    413    421    Any
        //  175    207    421    433    Ljava/io/IOException;
        //  175    207    413    421    Any
        //  218    249    421    433    Ljava/io/IOException;
        //  218    249    413    421    Any
        //  253    263    421    433    Ljava/io/IOException;
        //  253    263    413    421    Any
        //  263    312    421    433    Ljava/io/IOException;
        //  263    312    413    421    Any
        //  342    347    405    406    Any
        //  350    355    405    406    Any
        //  358    363    405    406    Any
        //  366    387    405    406    Any
        //  390    395    405    406    Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0059:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public boolean bVerifyUpdate(final File ex, final SharedPreferences sharedPreferences) {
        try {
            RecoverySystem.verifyPackage((File)ex, (RecoverySystem$ProgressListener)new RecoverySystem$ProgressListener() {
                public void onProgress(final int n) {
                    OTAUpdateService.sLogger.d(String.format("Verify progress: %d%% completed", n));
                }
            }, (File)null);
            verifyOTAZipFile((File)ex, 1000, 2147483648L);
            return true;
        }
        catch (IOException ex2) {}
        catch (GeneralSecurityException ex) {
            goto Label_0031;
        }
    }
    
    public void checkForUpdate() {
        TaskManager.getInstance().execute(new Runnable() {
            @Override
            public void run() {
                OTAUpdateService.sLogger.d("Checking the update");
                final File access$100 = checkUpdateFile((Context)OTAUpdateService.this, OTAUpdateService.this.sharedPreferences);
                if (access$100 == null) {
                    OTAUpdateService.sLogger.e("No update file found");
                    OTAUpdateService.this.checkIfIncrementalUpdatesFailed(null);
                    OTAUpdateService.clearUpdate(access$100, (Context)OTAUpdateService.this, OTAUpdateService.this.sharedPreferences);
                }
                else {
                    final boolean access$101 = OTAUpdateService.this.checkIfIncrementalUpdatesFailed(access$100);
                    OTAUpdateService.sLogger.d("Current update valid ? :" + access$101);
                    if (access$101) {
                        if (!OTAUpdateService.isOTAUpdateVerified(OTAUpdateService.this.sharedPreferences)) {
                            if (!OTAUpdateService.this.bVerifyUpdate(access$100, OTAUpdateService.this.sharedPreferences)) {
                                OTAUpdateService.clearUpdate(access$100, (Context)OTAUpdateService.this, OTAUpdateService.this.sharedPreferences);
                                return;
                            }
                            OTAUpdateService.this.sharedPreferences.edit().putBoolean("verified", true).commit();
                            OTAUpdateService.this.bus.post(new UpdateVerified());
                        }
                        OTAUpdateService.swVersion = getUpdateVersion(OTAUpdateService.this.sharedPreferences);
                        OTAUpdateService.sLogger.v("verified swVersion:" + OTAUpdateService.swVersion);
                    }
                    else {
                        OTAUpdateService.clearUpdate(access$100, (Context)OTAUpdateService.this, OTAUpdateService.this.sharedPreferences);
                    }
                }
            }
        }, 1);
    }
    
    public IBinder onBind(final Intent intent) {
        return null;
    }
    
    public void onCreate() {
        super.onCreate();
        Mortar.inject(HudApplication.getAppContext(), this);
    }
    
    public int onStartCommand(final Intent intent, int n, final int n2) {
        super.onStartCommand(intent, n, n2);
        final String stringExtra = intent.getStringExtra("COMMAND");
        OTAUpdateService.sLogger.d("OTA Update service started command[" + stringExtra + "]");
        if (intent.hasExtra("COMMAND")) {
            n = -1;
            switch (stringExtra.hashCode()) {
                case -1672139796:
                    if (stringExtra.equals("INSTALL_UPDATE_REBOOT_QUIET")) {
                        n = 0;
                        break;
                    }
                    break;
                case 227671624:
                    if (stringExtra.equals("INSTALL_UPDATE_SHUTDOWN")) {
                        n = 1;
                        break;
                    }
                    break;
                case -2036275187:
                    if (stringExtra.equals("INSTALL_UPDATE")) {
                        n = 2;
                        break;
                    }
                    break;
                case 1870386340:
                    if (stringExtra.equals("DO_NOT_PROMPT")) {
                        n = 3;
                        break;
                    }
                    break;
                case 1813668687:
                    if (stringExtra.equals("VERIFY_UPDATE")) {
                        n = 4;
                        break;
                    }
                    break;
            }
            switch (n) {
                default:
                    OTAUpdateService.sLogger.e("onStartCommand() invalid command: " + stringExtra);
                    break;
                case 0:
                    this.update("--reboot_quiet_after");
                    break;
                case 1:
                    this.update("--shutdown_after");
                    break;
                case 2:
                    this.update(null);
                    break;
                case 3:
                    this.sharedPreferences.edit().putBoolean("do_not_prompt", true).commit();
                    break;
                case 4:
                    this.checkForUpdate();
                    this.cleanupOldFiles();
                    break;
            }
        }
        return 2;
    }
    
    public static class IncrementalOTAFailureDetected
    {
    }
    
    public static class InstallingUpdate
    {
    }
    
    public static class OTADownloadIntentsReceiver extends BroadcastReceiver
    {
        private SharedPreferences sharedPreferences;
        
        public OTADownloadIntentsReceiver(final SharedPreferences sharedPreferences) {
            this.sharedPreferences = sharedPreferences;
        }
        
        public void onReceive(final Context context, final Intent intent) {
            TaskManager.getInstance().execute(new Runnable() {
                final /* synthetic */ String val$path = intent.getStringExtra("path");
                
                @Override
                public void run() {
                    final File file = new File(this.val$path);
                    if (OTAUpdateService.isOtaFile(file.getName())) {
                        final String string = OTADownloadIntentsReceiver.this.sharedPreferences.getString("last_update_file_received", (String)null);
                        final String absolutePath = file.getAbsolutePath();
                        OTADownloadIntentsReceiver.this.sharedPreferences.edit().putString("last_update_file_received", absolutePath).remove("do_not_prompt").remove("retry_count").remove("verified").commit();
                        if (!absolutePath.equals(string)) {
                            if (string != null) {
                                IOUtils.deleteFile(context, string);
                                OTAUpdateService.sLogger.d("Delete the old update file " + string + ", new update :" + absolutePath);
                            }
                            AnalyticsSupport.recordDownloadOTAUpdate(OTADownloadIntentsReceiver.this.sharedPreferences);
                        }
                        OTAUpdateService.startServiceToVerifyUpdate();
                    }
                }
            }, 1);
        }
    }
    
    public static class OTAFailedException extends Exception
    {
        public String last_install;
        public String last_log;
        
        OTAFailedException(final String last_install, final String last_log) {
            super("OTA installation failed");
            this.last_install = last_install;
            this.last_log = last_log;
        }
    }
    
    public static class UpdateVerified
    {
    }
}
