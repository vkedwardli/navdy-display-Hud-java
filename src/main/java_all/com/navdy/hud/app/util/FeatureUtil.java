package com.navdy.hud.app.util;

import android.content.pm.PackageManager;
import android.content.IntentFilter;
import com.navdy.hud.app.HudApplication;
import android.text.TextUtils;
import com.navdy.hud.app.util.os.SystemProperties;
import android.os.Bundle;
import com.navdy.service.library.task.TaskManager;
import android.content.Intent;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.BroadcastReceiver;
import java.util.HashSet;
import com.navdy.service.library.log.Logger;

public final class FeatureUtil
{
    public static final String FEATURE_ACTION = "com.navdy.hud.app.feature";
    public static final String FEATURE_FUEL_ROUTING = "com.navdy.hud.app.feature.fuelRouting";
    public static final String FEATURE_GESTURE_COLLECTOR = "com.navdy.hud.app.feature.gestureCollector";
    public static final String FEATURE_GESTURE_ENGINE = "com.navdy.hud.app.feature.gestureEngine";
    public static final String FEATURE_VOICE_SEARCH_ADDITIONAL_RESULTS_PROPERTY = "persist.voice.search.additional.results";
    public static final String FEATURE_VOICE_SEARCH_LIST_PROPERTY = "persist.sys.voicesearch.list";
    private static final String GESTURE_CALIBRATED_PROPERTY = "persist.sys.swiped.calib";
    private static final Logger sLogger;
    private HashSet<Feature> featuresEnabledMap;
    private BroadcastReceiver receiver;
    private SharedPreferences sharedPreferences;
    
    static {
        sLogger = new Logger(FeatureUtil.class);
    }
    
    public FeatureUtil(final SharedPreferences sharedPreferences) {
        this.featuresEnabledMap = new HashSet<Feature>();
        this.receiver = new BroadcastReceiver() {
            public void onReceive(final Context context, final Intent intent) {
                try {
                    if ("com.navdy.hud.app.feature".equalsIgnoreCase(intent.getAction())) {
                        final Bundle extras = intent.getExtras();
                        if (extras != null && extras.size() == 1) {
                            TaskManager.getInstance().execute(new Runnable() {
                                @Override
                                public void run() {
                                    while (true) {
                                        boolean boolean1 = false;
                                        Feature access$000 = null;
                                        Label_0240: {
                                            Label_0172: {
                                                try {
                                                    final String s = extras.keySet().iterator().next();
                                                    boolean1 = extras.getBoolean(s);
                                                    access$000 = FeatureUtil.this.getFeatureFromName(s);
                                                    if (access$000 == null) {
                                                        FeatureUtil.sLogger.i("invalid feature:" + s);
                                                    }
                                                    else {
                                                        if (access$000 != Feature.GESTURE_ENGINE) {
                                                            break Label_0172;
                                                        }
                                                        FeatureUtil.this.sharedPreferences.edit().putBoolean("gesture.engine", boolean1).apply();
                                                        FeatureUtil.sLogger.v("gesture engine setting set to " + boolean1);
                                                        if (DeviceUtil.supportsCamera()) {
                                                            break Label_0240;
                                                        }
                                                        FeatureUtil.sLogger.v("gesture engine cannot be enabled/disabled, no camera");
                                                    }
                                                    return;
                                                }
                                                catch (Throwable t) {
                                                    FeatureUtil.sLogger.e(t);
                                                    return;
                                                }
                                            }
                                            if (access$000 == Feature.GESTURE_COLLECTOR) {
                                                FeatureUtil.sLogger.v("gesture collector setting set to " + boolean1);
                                                if (!DeviceUtil.supportsCamera()) {
                                                    FeatureUtil.sLogger.v("gesture collector cannot be enabled/disabled, no camera");
                                                    return;
                                                }
                                                if (!FeatureUtil.this.isGestureCollectorInstalled()) {
                                                    return;
                                                }
                                            }
                                        }
                                        FeatureUtil.this.setFeature(access$000, boolean1);
                                    }
                                }
                            }, 1);
                        }
                    }
                }
                catch (Throwable t) {
                    FeatureUtil.sLogger.e(t);
                }
            }
        };
        this.sharedPreferences = sharedPreferences;
        boolean b;
        if (!TextUtils.isEmpty((CharSequence)SystemProperties.get("persist.sys.swiped.calib", ""))) {
            b = true;
        }
        else {
            b = false;
        }
        int n;
        if (sharedPreferences.getBoolean("gesture.engine", true) || b) {
            n = 1;
        }
        else {
            n = 0;
        }
        SystemProperties.getBoolean("persist.sys.voicesearch.list", false);
        SystemProperties.getBoolean("persist.voice.search.additional.results", true);
        if (n != 0) {
            this.featuresEnabledMap.add(Feature.GESTURE_ENGINE);
            FeatureUtil.sLogger.v("enabled gesture engine");
            if (this.isGestureCollectorInstalled()) {
                this.featuresEnabledMap.add(Feature.GESTURE_COLLECTOR);
                FeatureUtil.sLogger.v("enabled gesture collector");
            }
        }
        else {
            FeatureUtil.sLogger.v("not enabled gesture engine");
        }
        this.featuresEnabledMap.add(Feature.FUEL_ROUTING);
        HudApplication.getAppContext().registerReceiver(this.receiver, new IntentFilter("com.navdy.hud.app.feature"));
    }
    
    private Feature getFeatureFromName(final String s) {
        final Feature feature = null;
        Feature feature2 = null;
        if (s == null) {
            feature2 = feature;
        }
        else {
            switch (s) {
                default:
                    feature2 = feature;
                    break;
                case "com.navdy.hud.app.feature.gestureCollector":
                    feature2 = Feature.GESTURE_COLLECTOR;
                    break;
                case "com.navdy.hud.app.feature.gestureEngine":
                    feature2 = Feature.GESTURE_ENGINE;
                    break;
                case "com.navdy.hud.app.feature.fuelRouting":
                    feature2 = Feature.FUEL_ROUTING;
                    break;
            }
        }
        return feature2;
    }
    
    private boolean isGestureCollectorInstalled() {
        boolean b = true;
        try {
            HudApplication.getAppContext().getPackageManager().getPackageInfo("com.navdy.collector", 1);
            return b;
        }
        catch (Throwable t) {
            if (!(t instanceof PackageManager$NameNotFoundException)) {
                FeatureUtil.sLogger.v("gesture collector not enabled", t);
            }
            else {
                FeatureUtil.sLogger.v("gesture collector not found");
            }
            b = false;
            return b;
        }
    }
    
    private void setFeature(final Feature feature, final boolean b) {
        // monitorenter(this)
        Label_0054: {
            if (!b) {
                break Label_0054;
            }
            try {
                this.featuresEnabledMap.add(feature);
                FeatureUtil.sLogger.v("enabled featured:" + feature);
                return;
                this.featuresEnabledMap.remove(feature);
                FeatureUtil.sLogger.v("disabled featured:" + feature);
            }
            finally {
            }
            // monitorexit(this)
        }
    }
    
    public boolean isFeatureEnabled(final Feature feature) {
        synchronized (this) {
            return this.featuresEnabledMap.contains(feature);
        }
    }
    
    public enum Feature
    {
        FUEL_ROUTING, 
        GESTURE_COLLECTOR, 
        GESTURE_ENGINE, 
        GESTURE_PROGRESS;
    }
}
