package com.navdy.hud.app.util;

import android.content.res.Resources;
import android.support.annotation.ArrayRes;
import android.text.TextPaint;
import android.text.StaticLayout;
import android.text.Layout;
import android.content.res.TypedArray;
import android.content.Context;
import android.text.TextUtils;
import android.support.annotation.StyleRes;
import android.widget.TextView;
import android.view.View;
import android.util.SparseArray;

public class ViewUtil
{
    private static final int LINE_SPACING_INDEX = 2;
    private static final int MAX_LINE_INDEX = 0;
    private static final int[] MULTI_LINE_ATTRS;
    private static final int SINGLE_LINE_INDEX = 1;
    static SparseArray<float[]> TEXT_SIZES;
    
    static {
        MULTI_LINE_ATTRS = new int[] { 16843091, 16843101, 16843288 };
        ViewUtil.TEXT_SIZES = (SparseArray<float[]>)new SparseArray();
    }
    
    public static void adjustPadding(final View view, final int n, final int n2, final int n3, final int n4) {
        view.setPadding(view.getPaddingLeft() + n, view.getPaddingTop() + n2, view.getPaddingRight() + n3, view.getPaddingBottom() + n4);
    }
    
    public static int applyTextAndStyle(final TextView textView, final CharSequence text, int n, @StyleRes final int n2) {
        final int n3 = 0;
        final Context context = textView.getContext();
        if (TextUtils.isEmpty(text)) {
            textView.setVisibility(GONE);
            n = n3;
        }
        else {
            textView.setVisibility(View.VISIBLE);
            boolean boolean1 = n == 1;
            float float1 = 1.0f;
            int int1 = n;
            if (n2 != -1) {
                final TypedArray obtainStyledAttributes = context.obtainStyledAttributes(n2, ViewUtil.MULTI_LINE_ATTRS);
                int1 = obtainStyledAttributes.getInt(0, n);
                if (obtainStyledAttributes.hasValue(1)) {
                    boolean1 = obtainStyledAttributes.getBoolean(1, true);
                }
                else {
                    boolean1 = (int1 == 1);
                }
                float1 = obtainStyledAttributes.getFloat(2, 1.0f);
                obtainStyledAttributes.recycle();
                textView.setTextAppearance(context, n2);
            }
            textView.setText(text);
            textView.setLineSpacing(0.0f, float1);
            textView.setSingleLine(boolean1);
            textView.setMaxLines(int1);
            n = int1;
        }
        return n;
    }
    
    public static float autosize(final TextView textView, final int n, final int n2, final float[] array) {
        final int[] array2 = new int[array.length];
        for (int i = 0; i < array.length; ++i) {
            array2[i] = n;
        }
        return autosize(textView, array2, n2, array, null);
    }
    
    public static float autosize(final TextView textView, final int[] array, final int n, final float[] array2, final int[] array3) {
        if (array.length != array2.length) {
            throw new IllegalArgumentException("maxLinesArray length must match textSizes length");
        }
        final CharSequence text = textView.getText();
        final TextPaint paint = textView.getPaint();
        int n2 = -1;
        int n3 = array2.length - 1;
        float n4 = array2[n3];
        int n5 = 0;
        int n6;
        float textSize;
        int n7;
        while (true) {
            n6 = n3;
            textSize = n4;
            n7 = n2;
            if (n5 >= array2.length) {
                break;
            }
            final int n8 = array[n5];
            textView.setSingleLine(n8 == 1);
            final float textSize2 = array2[n5];
            paint.setTextSize(textSize2);
            final int lineCount = new StaticLayout(text, paint, n, Layout$Alignment.ALIGN_NORMAL, 1.0f, 0.0f, false).getLineCount();
            n6 = n3;
            textSize = n4;
            n7 = n2;
            Label_0179: {
                if (lineCount <= n8) {
                    if (n2 != -1) {
                        n6 = n3;
                        textSize = n4;
                        if (lineCount >= (n7 = n2)) {
                            break Label_0179;
                        }
                    }
                    n7 = lineCount;
                    textSize = textSize2;
                    n6 = n5;
                }
            }
            if (lineCount == 1) {
                break;
            }
            ++n5;
            n3 = n6;
            n4 = textSize;
            n2 = n7;
        }
        if (array3 != null) {
            array3[0] = n6;
            if (n7 == -1) {
                n7 = array[n6];
            }
            array3[1] = n7;
        }
        textView.setTextSize(textSize);
        return textSize;
    }
    
    public static void autosize(final TextView textView, final int n, final int n2, @ArrayRes final int n3) {
        autosize(textView, n, n2, lookupSizes(textView.getResources(), n3));
    }
    
    private static float[] lookupSizes(final Resources resources, @ArrayRes final int n) {
        float[] array;
        if ((array = (float[])ViewUtil.TEXT_SIZES.get(n)) == null) {
            final int[] intArray = resources.getIntArray(n);
            array = new float[intArray.length];
            for (int i = 0; i < intArray.length; ++i) {
                array[i] = intArray[i];
            }
            ViewUtil.TEXT_SIZES.put(n, array);
        }
        return array;
    }
    
    public static void setBottomPadding(final View view, final int n) {
        view.setPadding(view.getPaddingLeft(), view.getPaddingTop(), view.getPaddingRight(), n);
    }
}
