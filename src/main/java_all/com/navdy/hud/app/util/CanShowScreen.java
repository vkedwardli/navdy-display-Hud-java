package com.navdy.hud.app.util;

import flow.Flow;
import mortar.Blueprint;

public interface CanShowScreen<S extends Blueprint>
{
    void showScreen(final S p0, final Flow.Direction p1, final int p2, final int p3);
}
