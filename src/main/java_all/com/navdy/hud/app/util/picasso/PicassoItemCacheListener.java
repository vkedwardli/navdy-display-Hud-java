package com.navdy.hud.app.util.picasso;

public interface PicassoItemCacheListener<K>
{
    void itemAdded(final K p0);
    
    void itemRemoved(final K p0);
}
