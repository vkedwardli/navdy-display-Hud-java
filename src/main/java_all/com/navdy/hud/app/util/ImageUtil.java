package com.navdy.hud.app.util;

import android.graphics.Xfermode;
import android.graphics.PorterDuffXfermode;
import android.graphics.PorterDuff;
import android.graphics.ColorFilter;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.ColorMatrix;
import android.graphics.Paint;
import android.graphics.Canvas;
import android.graphics.Bitmap;

public class ImageUtil
{
    public static Bitmap applySaturation(final Bitmap bitmap, final float saturation) {
        GenericUtil.checkNotOnMainThread();
        final Bitmap bitmap2 = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), bitmap.getConfig());
        final Canvas canvas = new Canvas(bitmap2);
        final Paint paint = new Paint();
        final ColorMatrix colorMatrix = new ColorMatrix();
        colorMatrix.setSaturation(saturation);
        paint.setColorFilter((ColorFilter)new ColorMatrixColorFilter(colorMatrix));
        canvas.drawBitmap(bitmap, 0.0f, 0.0f, paint);
        return bitmap2;
    }
    
    public static Bitmap blend(Bitmap copy, final Bitmap bitmap) {
        GenericUtil.checkNotOnMainThread();
        copy = copy.copy(copy.getConfig(), true);
        final Paint paint = new Paint();
        paint.setXfermode((Xfermode)new PorterDuffXfermode(PorterDuff$Mode.MULTIPLY));
        new Canvas(copy).drawBitmap(bitmap, 0.0f, 0.0f, paint);
        return copy;
    }
    
    public static Bitmap hueShift(Bitmap bitmap, float n) {
        final Bitmap bitmap2 = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), bitmap.getConfig());
        final Canvas canvas = new Canvas(bitmap2);
        final Paint paint = new Paint();
        final ColorMatrix colorMatrix = new ColorMatrix();
        final float n2 = Math.min(n, Math.max(-n, 180.0f)) / 180.0f * 3.1415927f;
        if (n2 == 0.0f) {
            bitmap = null;
        }
        else {
            n = (float)Math.cos(n2);
            final float n3 = (float)Math.sin(n2);
            colorMatrix.postConcat(new ColorMatrix(new float[] { (1.0f - 0.213f) * n + 0.213f + -0.213f * n3, -0.715f * n + 0.715f + -0.715f * n3, -0.072f * n + 0.072f + (1.0f - 0.072f) * n3, 0.0f, 0.0f, -0.213f * n + 0.213f + 0.143f * n3, (1.0f - 0.715f) * n + 0.715f + 0.14f * n3, -0.072f * n + 0.072f + -0.283f * n3, 0.0f, 0.0f, -0.213f * n + 0.213f + -(1.0f - 0.213f) * n3, -0.715f * n + 0.715f + n3 * 0.715f, (1.0f - 0.072f) * n + 0.072f + n3 * 0.072f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f }));
            paint.setColorFilter((ColorFilter)new ColorMatrixColorFilter(colorMatrix));
            canvas.drawBitmap(bitmap, 0.0f, 0.0f, paint);
            bitmap = bitmap2;
        }
        return bitmap;
    }
}
