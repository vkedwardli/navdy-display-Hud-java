package com.navdy.hud.app.util;

import com.navdy.hud.app.manager.SpeedManager;
import android.os.SystemClock;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 1 }, d1 = { "\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u0006\n\u0002\b\t\n\u0002\u0010\t\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u0006\u0010\u0017\u001a\u00020\u0018R\u0014\u0010\u0003\u001a\u00020\u0004X\u0086D¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006R$\u0010\t\u001a\u00020\b2\u0006\u0010\u0007\u001a\u00020\b@FX\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\n\u0010\u000b\"\u0004\b\f\u0010\rR\u001a\u0010\u000e\u001a\u00020\bX\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u000f\u0010\u000b\"\u0004\b\u0010\u0010\rR\u001a\u0010\u0011\u001a\u00020\u0012X\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0013\u0010\u0014\"\u0004\b\u0015\u0010\u0016¨\u0006\u0019" }, d2 = { "Lcom/navdy/hud/app/util/HeadingDataUtil;", "", "()V", "HEADING_EXPIRE_INTERVAL", "", "getHEADING_EXPIRE_INTERVAL", "()I", "value", "", "heading", "getHeading", "()D", "setHeading", "(D)V", "lastHeading", "getLastHeading", "setLastHeading", "lastHeadingSampleTime", "", "getLastHeadingSampleTime", "()J", "setLastHeadingSampleTime", "(J)V", "reset", "", "app_hudRelease" }, k = 1, mv = { 1, 1, 6 })
public final class HeadingDataUtil
{
    private final int HEADING_EXPIRE_INTERVAL;
    private double heading;
    private double lastHeading;
    private long lastHeadingSampleTime;
    
    public HeadingDataUtil() {
        this.HEADING_EXPIRE_INTERVAL = 3000;
    }
    
    public final int getHEADING_EXPIRE_INTERVAL() {
        return this.HEADING_EXPIRE_INTERVAL;
    }
    
    public final double getHeading() {
        return this.heading;
    }
    
    public final double getLastHeading() {
        return this.lastHeading;
    }
    
    public final long getLastHeadingSampleTime() {
        return this.lastHeadingSampleTime;
    }
    
    public final void reset() {
        this.lastHeading = 0.0;
        this.lastHeadingSampleTime = SystemClock.elapsedRealtime();
    }
    
    public final void setHeading(final double n) {
        final SpeedManager instance = SpeedManager.getInstance();
        final long elapsedRealtime = SystemClock.elapsedRealtime();
        if (instance.getCurrentSpeed() >= 1 && (this.lastHeading == 0.0 || Math.abs(this.heading - this.lastHeading) < 120 || elapsedRealtime - this.lastHeadingSampleTime > this.HEADING_EXPIRE_INTERVAL)) {
            this.lastHeading = n;
            this.lastHeadingSampleTime = SystemClock.elapsedRealtime();
            this.heading = n;
        }
    }
    
    public final void setLastHeading(final double lastHeading) {
        this.lastHeading = lastHeading;
    }
    
    public final void setLastHeadingSampleTime(final long lastHeadingSampleTime) {
        this.lastHeadingSampleTime = lastHeadingSampleTime;
    }
}
