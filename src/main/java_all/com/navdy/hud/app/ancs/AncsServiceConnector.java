package com.navdy.hud.app.ancs;

import com.navdy.hud.app.event.Shutdown;
import com.navdy.service.library.events.notification.NotificationsStatusRequest;
import com.navdy.service.library.events.preferences.NotificationPreferences;
import com.navdy.service.library.events.ui.DismissScreen;
import com.navdy.service.library.events.ui.Screen;
import com.navdy.hud.app.event.InitEvents;
import com.navdy.hud.app.event.DriverProfileChanged;
import com.navdy.hud.app.event.DeviceInfoAvailable;
import com.squareup.otto.Subscribe;
import com.navdy.service.library.events.connection.ConnectionStateChange;
import android.os.IBinder;
import java.util.Arrays;
import com.navdy.service.library.events.notification.ServiceType;
import com.navdy.service.library.events.notification.NotificationsStatusUpdate;
import android.content.ComponentName;
import android.content.Intent;
import java.util.ArrayList;
import java.util.Date;
import com.navdy.hud.app.framework.glance.GlanceApp;
import com.navdy.hud.app.framework.glance.GlanceHelper;
import okio.ByteString;
import com.navdy.service.library.task.TaskManager;
import com.navdy.service.library.events.notification.NotificationAction;
import java.util.List;
import com.navdy.hud.app.framework.DriverProfileHelper;
import com.navdy.service.library.device.NavdyDeviceId;
import android.os.ParcelUuid;
import java.util.UUID;
import com.navdy.service.library.events.notification.NotificationEvent;
import com.navdy.service.library.events.glances.GlanceEvent;
import com.squareup.wire.Message;
import com.navdy.hud.app.event.RemoteEvent;
import android.os.SystemClock;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.ancs.AppleNotification;
import android.os.RemoteException;
import com.navdy.service.library.events.notification.NotificationsError;
import com.navdy.service.library.events.notification.NotificationsState;
import com.navdy.ancs.IAncsService;
import com.navdy.hud.app.profile.NotificationSettings;
import com.navdy.service.library.events.DeviceInfo;
import android.content.Context;
import com.squareup.otto.Bus;
import com.navdy.hud.app.util.NotificationActionCache;
import com.navdy.ancs.IAncsServiceListener;
import com.navdy.service.library.events.notification.NotificationCategory;
import com.navdy.service.library.log.Logger;
import java.util.HashMap;
import com.navdy.hud.app.common.ServiceReconnector;

public class AncsServiceConnector extends ServiceReconnector
{
    private static final int NOTIFICATION_EVENT_SEND_THRESHOLD = 15000;
    private static final int ONE_HOUR_IN_MS = 3600000;
    private static HashMap<String, Boolean> blackList;
    private static HashMap<String, Long> enableNotificationTimeMap;
    static final Logger sLogger;
    private NotificationCategory[] categoryMap;
    private Runnable connectRunnable;
    private Runnable disconnectRunnable;
    private IAncsServiceListener listener;
    private boolean localeUpToDate;
    protected NotificationActionCache mActionCache;
    protected Bus mBus;
    protected Context mContext;
    private DeviceInfo mDeviceInfo;
    protected NotificationSettings mNotificationSettings;
    protected IAncsService mService;
    private NotificationsState notificationsState;
    private boolean reconnect;
    
    static {
        sLogger = new Logger(AncsServiceConnector.class);
        AncsServiceConnector.blackList = new HashMap<String, Boolean>();
        AncsServiceConnector.enableNotificationTimeMap = new HashMap<String, Long>();
        AncsServiceConnector.blackList.put("com.apple.mobilephone", true);
    }
    
    public AncsServiceConnector(final Context mContext, final Bus mBus) {
        super(mContext, getServiceIntent(), null);
        this.notificationsState = NotificationsState.NOTIFICATIONS_STOPPED;
        this.listener = new IAncsServiceListener.Stub() {
            public void onConnectionStateChange(final int n) throws RemoteException {
                AncsServiceConnector.sLogger.d("ANCS connection state change - " + n);
                final NotificationsState notificationsState = null;
                NotificationsError notificationsError2;
                final NotificationsError notificationsError = notificationsError2 = null;
                NotificationsState notificationsState2 = notificationsState;
                Label_0082: {
                    switch (n) {
                        default:
                            notificationsState2 = notificationsState;
                            notificationsError2 = notificationsError;
                            break Label_0082;
                        case 5:
                            notificationsError2 = NotificationsError.NOTIFICATIONS_ERROR_BOND_REMOVED;
                            notificationsState2 = NotificationsState.NOTIFICATIONS_PAIRING_FAILED;
                            break Label_0082;
                        case 4:
                            notificationsError2 = NotificationsError.NOTIFICATIONS_ERROR_AUTH_FAILED;
                            notificationsState2 = NotificationsState.NOTIFICATIONS_PAIRING_FAILED;
                            break Label_0082;
                        case 1:
                            notificationsState2 = NotificationsState.NOTIFICATIONS_CONNECTING;
                            notificationsError2 = notificationsError;
                            break Label_0082;
                        case 2:
                            notificationsState2 = NotificationsState.NOTIFICATIONS_ENABLED;
                            notificationsError2 = notificationsError;
                            break Label_0082;
                        case 0:
                            notificationsState2 = NotificationsState.NOTIFICATIONS_STOPPED;
                            notificationsError2 = notificationsError;
                        case 3:
                            if (notificationsState2 != null) {
                                AncsServiceConnector.this.setNotificationsState(notificationsState2, notificationsError2);
                            }
                    }
                }
            }
            
            public void onNotification(final AppleNotification appleNotification) throws RemoteException {
                while (true) {
                    final String appId = appleNotification.getAppId();
                    AncsServiceConnector.sLogger.d("Hud listener notified of apple notification id[" + appId);
                    Boolean enabled = null;
                    Label_0166: {
                        GlanceEvent access$200 = null;
                    Label_0144:
                        while (true) {
                            Label_0139: {
                                try {
                                    if (!RemoteDeviceManager.getInstance().isRemoteDeviceConnected()) {
                                        AncsServiceConnector.sLogger.d("not connected");
                                    }
                                    else {
                                        if (AncsServiceConnector.blackList.get(appId) == null) {
                                            break Label_0139;
                                        }
                                        final int n = 1;
                                        if (n == 0 && appleNotification.getEventId() == 0) {
                                            enabled = AncsServiceConnector.this.mNotificationSettings.enabled(appleNotification);
                                            if (!Boolean.TRUE.equals(enabled) || appleNotification.isPreExisting()) {
                                                break Label_0166;
                                            }
                                            access$200 = AncsServiceConnector.this.convertToGlanceEvent(appleNotification);
                                            if (access$200 != null) {
                                                break Label_0144;
                                            }
                                            AncsServiceConnector.sLogger.e("glance not converted");
                                        }
                                    }
                                    return;
                                }
                                catch (Throwable t) {
                                    AncsServiceConnector.sLogger.e("ancs-notif", t);
                                    return;
                                }
                                return;
                            }
                            final int n = 0;
                            continue;
                        }
                        AncsServiceConnector.sLogger.d("posting notification");
                        AncsServiceConnector.this.mBus.post(access$200);
                        return;
                    }
                    if (enabled != null) {
                        return;
                    }
                    final Long n2 = AncsServiceConnector.enableNotificationTimeMap.get(appId);
                    final long elapsedRealtime = SystemClock.elapsedRealtime();
                    if (n2 != null && elapsedRealtime - n2 < 15000L) {
                        AncsServiceConnector.sLogger.d("not sending unknown notification to client, within threshold");
                        return;
                    }
                    final NotificationEvent access$201 = AncsServiceConnector.this.convertNotification(appleNotification);
                    AncsServiceConnector.sLogger.d("sending unknown notification to client - " + access$201);
                    AncsServiceConnector.this.mBus.post(new RemoteEvent(access$201));
                    AncsServiceConnector.enableNotificationTimeMap.put(appId, elapsedRealtime);
                }
            }
        };
        this.categoryMap = new NotificationCategory[] { NotificationCategory.CATEGORY_OTHER, NotificationCategory.CATEGORY_INCOMING_CALL, NotificationCategory.CATEGORY_MISSED_CALL, NotificationCategory.CATEGORY_VOICE_MAIL, NotificationCategory.CATEGORY_SOCIAL, NotificationCategory.CATEGORY_SCHEDULE, NotificationCategory.CATEGORY_EMAIL, NotificationCategory.CATEGORY_NEWS, NotificationCategory.CATEGORY_HEALTH_AND_FITNESS, NotificationCategory.CATEGORY_BUSINESS_AND_FINANCE, NotificationCategory.CATEGORY_LOCATION, NotificationCategory.CATEGORY_ENTERTAINMENT };
        this.connectRunnable = new Runnable() {
            @Override
            public void run() {
                AncsServiceConnector.sLogger.d("Establishing ANCS connection");
                if (AncsServiceConnector.this.mService == null || AncsServiceConnector.this.mDeviceInfo == null || !AncsServiceConnector.this.localeUpToDate) {
                    return;
                }
                try {
                    AncsServiceConnector.this.mService.connectToDevice(new ParcelUuid(UUID.fromString(AncsServiceConnector.this.mDeviceInfo.deviceUuid)), new NavdyDeviceId(AncsServiceConnector.this.mDeviceInfo.deviceId).getBluetoothAddress());
                }
                catch (RemoteException ex) {
                    AncsServiceConnector.sLogger.e("Failed to connect to navdy BTLE service", (Throwable)ex);
                }
            }
        };
        this.disconnectRunnable = new Runnable() {
            @Override
            public void run() {
                AncsServiceConnector.this.stop();
                AncsServiceConnector.this.restart();
            }
        };
        this.mContext = mContext;
        this.mBus = mBus;
        this.mNotificationSettings = DriverProfileHelper.getInstance().getCurrentProfile().getNotificationSettings();
        this.mActionCache = new NotificationActionCache(this.getClass());
        mBus.register(this);
    }
    
    private List<NotificationAction> buildActions(final AppleNotification appleNotification) {
        final List<NotificationAction> list = null;
        List<NotificationAction> defaultActions;
        if (appleNotification.getAppId().equals("com.apple.MobileSMS")) {
            defaultActions = list;
        }
        else {
            defaultActions = this.getDefaultActions(appleNotification);
        }
        return defaultActions;
    }
    
    private void connect() {
        this.reconnect = true;
        if (this.isIOS(this.mDeviceInfo)) {
            TaskManager.getInstance().execute(this.connectRunnable, 1);
        }
    }
    
    private NotificationEvent convertNotification(final AppleNotification appleNotification) {
        final String title = appleNotification.getTitle();
        return new NotificationEvent(appleNotification.getNotificationUid(), this.mapCategory(appleNotification.getCategoryId()), title, appleNotification.getSubTitle(), appleNotification.getMessage(), appleNotification.getAppId(), this.buildActions(appleNotification), null, null, null, title, true, appleNotification.getAppName());
    }
    
    private GlanceEvent convertToGlanceEvent(final AppleNotification appleNotification) {
        final String appId = appleNotification.getAppId();
        final String value = String.valueOf(appleNotification.getNotificationUid());
        final String title = appleNotification.getTitle();
        final String subTitle = appleNotification.getSubTitle();
        final String message = appleNotification.getMessage();
        final int eventId = appleNotification.getEventId();
        final Date date = appleNotification.getDate();
        GlanceEvent glanceEvent = null;
        if (title == null && message == null && subTitle == null) {
            glanceEvent = null;
        }
        else {
            GlanceApp glanceApp = GlanceHelper.getGlancesApp(appId);
            boolean b;
            if (glanceApp == null) {
                glanceApp = GlanceApp.GENERIC;
                b = false;
            }
            else {
                b = true;
            }
            AncsServiceConnector.sLogger.v("[ancs-notif] appId[" + appId + "]" + " id[" + value + "]" + " title[" + title + "]" + " subTitle[" + subTitle + "]" + " message[" + message + "]" + " eventId[" + String.valueOf(eventId) + "]" + " time[" + date + "]" + " whitelisted[" + b + "]" + " app[" + glanceApp + "]");
            final GlanceEvent glanceEvent2 = null;
            switch (glanceApp) {
                default:
                    AncsServiceConnector.sLogger.w("[ancs-notif] app of type [" + glanceApp + "] not handled");
                    glanceEvent = glanceEvent2;
                    break;
                case GOOGLE_CALENDAR:
                    glanceEvent = AncsGlanceHelper.buildGoogleCalendarEvent(appId, value, title, subTitle, message, date);
                    break;
                case GOOGLE_MAIL:
                    glanceEvent = AncsGlanceHelper.buildGoogleMailEvent(appId, value, title, subTitle, message, date);
                    break;
                case GENERIC_MAIL:
                    glanceEvent = AncsGlanceHelper.buildGenericMailEvent(appId, value, title, subTitle, message, date);
                    break;
                case GOOGLE_HANGOUT:
                    glanceEvent = AncsGlanceHelper.buildGoogleHangoutEvent(appId, value, title, subTitle, message, date);
                    break;
                case SLACK:
                    glanceEvent = AncsGlanceHelper.buildSlackEvent(appId, value, title, subTitle, message, date);
                    break;
                case WHATS_APP:
                    glanceEvent = AncsGlanceHelper.buildWhatsappEvent(appId, value, title, subTitle, message, date);
                    break;
                case FACEBOOK_MESSENGER:
                    glanceEvent = AncsGlanceHelper.buildFacebookMessengerEvent(appId, value, title, subTitle, message, date);
                    break;
                case FACEBOOK:
                    glanceEvent = AncsGlanceHelper.buildFacebookEvent(appId, value, title, subTitle, message, date);
                    break;
                case TWITTER:
                    glanceEvent = AncsGlanceHelper.buildTwitterEvent(appId, value, title, subTitle, message, date);
                    break;
                case IMESSAGE:
                    glanceEvent = AncsGlanceHelper.buildiMessageEvent(appId, value, title, subTitle, message, date);
                    break;
                case APPLE_MAIL:
                    glanceEvent = AncsGlanceHelper.buildAppleMailEvent(appId, value, title, subTitle, message, date);
                    break;
                case APPLE_CALENDAR:
                    glanceEvent = AncsGlanceHelper.buildAppleCalendarEvent(appId, value, title, subTitle, message, date);
                    break;
                case GENERIC:
                    glanceEvent = AncsGlanceHelper.buildGenericEvent(appId, value, title, subTitle, message, date);
                    break;
            }
        }
        return glanceEvent;
    }
    
    private void disconnect() {
        this.reconnect = false;
        TaskManager.getInstance().execute(this.disconnectRunnable, 1);
    }
    
    private List<NotificationAction> getDefaultActions(final AppleNotification appleNotification) {
        final ArrayList<NotificationAction> list = new ArrayList<NotificationAction>();
        final int notificationUid = appleNotification.getNotificationUid();
        if (appleNotification.hasPositiveAction()) {
            list.add(this.mActionCache.buildAction(notificationUid, 0, 0, appleNotification.getPositiveActionLabel()));
        }
        if (appleNotification.hasNegativeAction()) {
            list.add(this.mActionCache.buildAction(notificationUid, 1, 0, appleNotification.getNegativeActionLabel()));
        }
        return list;
    }
    
    private static Intent getServiceIntent() {
        final Intent intent = new Intent();
        intent.setComponent(new ComponentName("com.navdy.ancs.app", "com.navdy.ancs.ANCSService"));
        return intent;
    }
    
    private boolean isIOS(final DeviceInfo deviceInfo) {
        return deviceInfo != null && ((deviceInfo.platform != null && deviceInfo.platform == DeviceInfo.Platform.PLATFORM_iOS) || deviceInfo.model.contains("iPhone"));
    }
    
    private NotificationCategory mapCategory(final int n) {
        NotificationCategory category_OTHER;
        if (n < 0 || n >= this.categoryMap.length) {
            category_OTHER = NotificationCategory.CATEGORY_OTHER;
        }
        else {
            category_OTHER = this.categoryMap[n];
        }
        return category_OTHER;
    }
    
    private void setNotificationsState(final NotificationsState notificationsState, final NotificationsError notificationsError) {
        if (this.notificationsState != notificationsState) {
            this.notificationsState = notificationsState;
            this.mBus.post(new RemoteEvent(new NotificationsStatusUpdate.Builder().state(this.notificationsState).service(ServiceType.SERVICE_ANCS).errorDetails(notificationsError).build()));
        }
    }
    
    private void stop() {
        AncsServiceConnector.sLogger.d("Disconnecting ANCS connection");
        if (this.mService == null) {
            return;
        }
        try {
            this.mService.disconnect();
        }
        catch (RemoteException ex) {
            AncsServiceConnector.sLogger.e("Failed to disconnect ANCS", (Throwable)ex);
        }
    }
    
    private void updateNotificationFilters() {
        this.mNotificationSettings = DriverProfileHelper.getInstance().getCurrentProfile().getNotificationSettings();
        if (this.mService == null) {
            return;
        }
        try {
            final long currentTimeMillis = System.currentTimeMillis();
            List<String> enabledApps;
            if (this.mNotificationSettings != null) {
                enabledApps = this.mNotificationSettings.enabledApps();
            }
            else {
                enabledApps = null;
            }
            final Logger sLogger = AncsServiceConnector.sLogger;
            final StringBuilder append = new StringBuilder().append("Setting ANCS to filter apps: ");
            String string;
            if (enabledApps != null) {
                string = Arrays.toString(enabledApps.toArray());
            }
            else {
                string = "null";
            }
            sLogger.d(append.append(string).toString());
            this.mService.setNotificationFilter(enabledApps, currentTimeMillis - 3600000L);
        }
        catch (RemoteException ex) {
            AncsServiceConnector.sLogger.w("Failed to update notification filter");
        }
    }
    
    @Override
    protected void onConnected(final ComponentName componentName, final IBinder binder) {
        this.mService = IAncsService.Stub.asInterface(binder);
        while (true) {
            try {
                this.updateNotificationFilters();
                this.mService.addListener(this.listener);
                if (this.reconnect) {
                    this.connect();
                }
            }
            catch (RemoteException ex) {
                AncsServiceConnector.sLogger.e("Failed to add notification listener", (Throwable)ex);
                continue;
            }
            break;
        }
    }
    
    @Subscribe
    public void onConnectionChange(final ConnectionStateChange connectionStateChange) {
        switch (connectionStateChange.state) {
            case CONNECTION_DISCONNECTED:
                this.mDeviceInfo = null;
                this.localeUpToDate = false;
                this.disconnect();
                break;
        }
    }
    
    @Subscribe
    public void onDeviceInfoAvailable(final DeviceInfoAvailable deviceInfoAvailable) {
        AncsServiceConnector.sLogger.i("device info updated");
        this.mDeviceInfo = deviceInfoAvailable.deviceInfo;
    }
    
    @Override
    protected void onDisconnected(final ComponentName componentName) {
        this.mService = null;
    }
    
    @Subscribe
    public void onDriverProfileChanged(final DriverProfileChanged driverProfileChanged) {
        this.updateNotificationFilters();
    }
    
    @Subscribe
    public void onInitPhase(final InitEvents.InitPhase initPhase) {
        switch (initPhase.phase) {
            case LOCALE_UP_TO_DATE:
                this.localeUpToDate = true;
                if (this.reconnect) {
                    this.connect();
                    break;
                }
                break;
            case SWITCHING_LOCALE:
                this.shutdown();
                break;
        }
    }
    
    @Subscribe
    public void onNotificationAction(final NotificationAction notificationAction) {
        if (this.mActionCache.validAction(notificationAction)) {
            this.performAction(notificationAction.notificationId, notificationAction.actionId);
            this.mBus.post(new DismissScreen(Screen.SCREEN_NOTIFICATION));
            this.mActionCache.markComplete(notificationAction);
        }
    }
    
    @Subscribe
    public void onNotificationPreferences(final NotificationPreferences notificationPreferences) {
        this.updateNotificationFilters();
    }
    
    @Subscribe
    public void onNotificationsRequest(final NotificationsStatusRequest notificationsStatusRequest) {
        if (notificationsStatusRequest.service == null || ServiceType.SERVICE_ANCS.equals(notificationsStatusRequest.service)) {
            AncsServiceConnector.sLogger.i("Received NotificationsStatusRequest:" + notificationsStatusRequest);
            if (notificationsStatusRequest.newState != null) {
                switch (notificationsStatusRequest.newState) {
                    case NOTIFICATIONS_ENABLED:
                        this.connect();
                        break;
                    case NOTIFICATIONS_STOPPED:
                        this.disconnect();
                        break;
                }
            }
            this.mBus.post(new RemoteEvent(new NotificationsStatusUpdate.Builder().state(this.notificationsState).service(ServiceType.SERVICE_ANCS).build()));
        }
    }
    
    @Subscribe
    public void onShutdown(final Shutdown shutdown) {
        if (shutdown.state == Shutdown.State.CONFIRMED) {
            this.shutdown();
        }
    }
    
    public void performAction(final int n, final int n2) {
        if (this.mService == null) {
            return;
        }
        try {
            AncsServiceConnector.sLogger.d("Performing action (" + n2 + ") for notification - " + n);
            this.mService.performNotificationAction(n, n2);
        }
        catch (RemoteException ex) {
            AncsServiceConnector.sLogger.e("Exception when performing notification action", (Throwable)ex);
        }
    }
    
    @Override
    public void shutdown() {
        this.reconnect = false;
        this.stop();
        super.shutdown();
    }
}
