package com.navdy.hud.app.ui.component.homescreen;

import android.view.ViewGroup;
import com.navdy.hud.app.maps.NavigationMode;
import android.animation.Animator;
import android.view.View;
import android.animation.AnimatorSet;
import com.navdy.hud.app.view.MainView;
import android.util.AttributeSet;
import android.content.Context;
import com.navdy.service.library.log.Logger;
import android.widget.FrameLayout;

public class HomeScreenRightSectionView extends FrameLayout
{
    private static final Logger sLogger;
    
    static {
        sLogger = new Logger(HomeScreenRightSectionView.class);
    }
    
    public HomeScreenRightSectionView(final Context context) {
        super(context);
    }
    
    public HomeScreenRightSectionView(final Context context, final AttributeSet set) {
        super(context, set);
    }
    
    public HomeScreenRightSectionView(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
    }
    
    public void ejectRightSection() {
        HomeScreenRightSectionView.sLogger.v("ejectRightSection");
        this.removeAllViews();
    }
    
    public void getCustomAnimator(final MainView.CustomAnimationMode customAnimationMode, final AnimatorSet.Builder animatorSet$Builder) {
        switch (customAnimationMode) {
            case SHRINK_MODE:
                animatorSet$Builder.with((Animator)HomeScreenUtils.getXPositionAnimator((View)this, HomeScreenResourceValues.mainScreenRightSectionShrinkX));
                break;
            case EXPAND:
                animatorSet$Builder.with((Animator)HomeScreenUtils.getXPositionAnimator((View)this, HomeScreenResourceValues.mainScreenRightSectionX));
                break;
        }
    }
    
    public boolean hasView() {
        return this.getChildCount() > 0;
    }
    
    public void injectRightSection(final View view) {
        HomeScreenRightSectionView.sLogger.v("injectRightSection");
        this.removeAllViews();
        this.addView(view);
    }
    
    public boolean isViewVisible() {
        return this.getX() < HomeScreenResourceValues.mainScreenRightSectionX;
    }
    
    public void setView(final MainView.CustomAnimationMode customAnimationMode) {
        HomeScreenRightSectionView.sLogger.v("setview: " + customAnimationMode);
        this.setX((float)HomeScreenResourceValues.mainScreenRightSectionX);
    }
    
    public void updateLayoutForMode(final NavigationMode navigationMode, final HomeScreenView homeScreenView) {
        int height;
        if (homeScreenView.isNavigationActive()) {
            height = HomeScreenResourceValues.activeMapHeight;
        }
        else {
            height = HomeScreenResourceValues.openMapHeight;
        }
        ((ViewGroup.MarginLayoutParams)this.getLayoutParams()).height = height;
        this.requestLayout();
    }
}
