package com.navdy.hud.app.ui.component.carousel;

import android.util.Log;
import java.util.ArrayList;
import java.util.HashMap;
import android.view.View;
import java.util.List;

public final class Carousel
{
    public static class InitParams
    {
        public AnimationStrategy animator;
        public CarouselIndicator carouselIndicator;
        public boolean exitOnDoubleClick;
        public boolean fastScrollAnimation;
        public int imageLytResourceId;
        public int infoLayoutResourceId;
        public List<Model> model;
        public View rootContainer;
        public ViewProcessor viewProcessor;
    }
    
    public interface Listener
    {
        void onCurrentItemChanged(final int p0, final int p1);
        
        void onCurrentItemChanging(final int p0, final int p1, final int p2);
        
        void onExecuteItem(final int p0, final int p1);
        
        void onExit();
    }
    
    public static class Model
    {
        public Object extras;
        public int id;
        public HashMap<Integer, String> infoMap;
        public int largeImageRes;
        public int smallImageColor;
        public int smallImageRes;
    }
    
    public static class ViewCacheManager
    {
        private int maxCacheCount;
        private ArrayList<View> middleLeftViewCache;
        private ArrayList<View> middleRightViewCache;
        private ArrayList<View> sideViewCache;
        
        public ViewCacheManager(final int maxCacheCount) {
            this.sideViewCache = new ArrayList<View>();
            this.middleLeftViewCache = new ArrayList<View>();
            this.middleRightViewCache = new ArrayList<View>();
            this.maxCacheCount = maxCacheCount;
        }
        
        public void clearCache() {
            this.sideViewCache.clear();
            this.middleLeftViewCache.clear();
            this.middleRightViewCache.clear();
        }
        
        public View getView(final ViewType viewType) {
            final View view = null;
            View view2 = null;
            switch (viewType) {
                default:
                    view2 = view;
                    break;
                case SIDE:
                    view2 = view;
                    if (this.sideViewCache.size() > 0) {
                        view2 = this.sideViewCache.remove(0);
                        break;
                    }
                    break;
                case MIDDLE_LEFT:
                    view2 = view;
                    if (this.middleLeftViewCache.size() > 0) {
                        view2 = this.middleLeftViewCache.remove(0);
                        break;
                    }
                    break;
                case MIDDLE_RIGHT:
                    view2 = view;
                    if (this.middleRightViewCache.size() > 0) {
                        view2 = this.middleRightViewCache.remove(0);
                        break;
                    }
                    break;
            }
            if (view2 != null && view2.getParent() != null) {
                Log.e("CAROUSEL", "uhoh");
            }
            return view2;
        }
        
        public void putView(final ViewType viewType, final View view) {
            if (view == null || view.getParent() != null) {
                Log.e("CAROUSEL", "uhoh");
            }
            switch (viewType) {
                case SIDE:
                    if (this.sideViewCache.size() < this.maxCacheCount) {
                        this.sideViewCache.add(view);
                        break;
                    }
                    break;
                case MIDDLE_LEFT:
                    if (this.middleLeftViewCache.size() < this.maxCacheCount) {
                        this.middleLeftViewCache.add(view);
                        break;
                    }
                    break;
                case MIDDLE_RIGHT:
                    if (this.middleRightViewCache.size() < this.maxCacheCount) {
                        this.middleRightViewCache.add(view);
                        break;
                    }
                    break;
            }
        }
    }
    
    public interface ViewProcessor
    {
        void processInfoView(final Model p0, final View p1, final int p2);
        
        void processLargeImageView(final Model p0, final View p1, final int p2, final int p3, final int p4);
        
        void processSmallImageView(final Model p0, final View p1, final int p2, final int p3, final int p4);
    }
    
    public enum ViewType
    {
        MIDDLE_LEFT, 
        MIDDLE_RIGHT, 
        SIDE;
    }
}
