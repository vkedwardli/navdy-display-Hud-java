package com.navdy.hud.app.ui.component.homescreen;

import com.squareup.otto.Subscribe;
import com.navdy.hud.app.maps.MapEvents;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import butterknife.ButterKnife;
import android.animation.Animator;
import android.view.View;
import android.animation.AnimatorSet;
import com.navdy.hud.app.util.os.SystemProperties;
import android.util.AttributeSet;
import android.content.Context;
import com.navdy.hud.app.ui.framework.UIStateManager;
import com.navdy.service.library.log.Logger;
import butterknife.InjectView;
import android.widget.ImageView;
import com.navdy.hud.app.view.MainView;
import android.widget.LinearLayout;

public class TbtEarlyView extends LinearLayout implements IHomeScreenLifecycle
{
    private static final String TBT_EARLY_SHRUNK_MODE_VISIBLE = "persist.sys.tbtearlyshrunk";
    private MainView.CustomAnimationMode currentMode;
    @InjectView(R.id.earlyManeuverImage)
    ImageView earlyManeuverImage;
    private HomeScreenView homeScreenView;
    private final boolean isShrunkVisible;
    private int lastNextTurnIconId;
    private Logger logger;
    private boolean paused;
    private UIStateManager uiStateManager;
    
    public TbtEarlyView(final Context context) {
        this(context, null);
    }
    
    public TbtEarlyView(final Context context, final AttributeSet set) {
        this(context, set, 0);
    }
    
    public TbtEarlyView(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
        this.lastNextTurnIconId = -1;
        this.isShrunkVisible = SystemProperties.getBoolean("persist.sys.tbtearlyshrunk", true);
    }
    
    private void setEarlyManeuver(final int lastNextTurnIconId) {
        if (this.lastNextTurnIconId != lastNextTurnIconId) {
            this.lastNextTurnIconId = lastNextTurnIconId;
            if (!this.paused) {
                if (this.lastNextTurnIconId != -1) {
                    this.earlyManeuverImage.setImageResource(this.lastNextTurnIconId);
                }
                this.showHideEarlyManeuver();
            }
        }
    }
    
    public void clearState() {
        this.lastNextTurnIconId = -1;
        this.showHideEarlyManeuver();
    }
    
    public void getCustomAnimator(final MainView.CustomAnimationMode currentMode, final AnimatorSet.Builder animatorSet$Builder) {
        this.currentMode = currentMode;
        switch (currentMode) {
            case SHRINK_LEFT:
                if (!this.isShrunkVisible) {
                    animatorSet$Builder.with((Animator)HomeScreenUtils.getAlphaAnimator((View)this, 0));
                }
                animatorSet$Builder.with((Animator)HomeScreenUtils.getWidthAnimator((View)this, HomeScreenResourceValues.activeRoadEarlyManeuverWidthShrunk));
                animatorSet$Builder.with((Animator)HomeScreenUtils.getXPositionAnimator((View)this, HomeScreenResourceValues.activeRoadEarlyManeuverShrinkLeftX));
                animatorSet$Builder.with((Animator)HomeScreenUtils.getYPositionAnimator((View)this, HomeScreenResourceValues.activeRoadEarlyManeuverYShrunk));
                break;
            case EXPAND:
                if (!this.isShrunkVisible) {
                    animatorSet$Builder.with((Animator)HomeScreenUtils.getAlphaAnimator((View)this, 1));
                }
                animatorSet$Builder.with((Animator)HomeScreenUtils.getWidthAnimator((View)this, HomeScreenResourceValues.activeRoadEarlyManeuverWidth));
                animatorSet$Builder.with((Animator)HomeScreenUtils.getXPositionAnimator((View)this, HomeScreenResourceValues.activeRoadEarlyManeuverX));
                animatorSet$Builder.with((Animator)HomeScreenUtils.getYPositionAnimator((View)this, HomeScreenResourceValues.activeRoadEarlyManeuverY));
                break;
        }
    }
    
    protected void onFinishInflate() {
        this.logger = HomeScreenView.sLogger;
        super.onFinishInflate();
        ButterKnife.inject((View)this);
        final RemoteDeviceManager instance = RemoteDeviceManager.getInstance();
        this.uiStateManager = instance.getUiStateManager();
        instance.getBus().register(this);
    }
    
    @Subscribe
    public void onManeuverDisplay(final MapEvents.ManeuverDisplay maneuverDisplay) {
        this.setEarlyManeuver(maneuverDisplay.nextTurnIconId);
    }
    
    public void onPause() {
        if (!this.paused) {
            this.paused = true;
            this.logger.v("::onPause:tbtearly");
        }
    }
    
    public void onResume() {
        if (this.paused) {
            this.paused = false;
            this.logger.v("::onResume:tbtearly");
            final int lastNextTurnIconId = this.lastNextTurnIconId;
            if (lastNextTurnIconId != -1) {
                this.logger.v("::onResume:tbtearly set last turn icon");
                this.lastNextTurnIconId = -1;
                this.setEarlyManeuver(lastNextTurnIconId);
            }
        }
    }
    
    public void setView(final MainView.CustomAnimationMode currentMode) {
        this.currentMode = currentMode;
        switch (currentMode) {
            case EXPAND:
                this.setX((float)HomeScreenResourceValues.activeRoadEarlyManeuverX);
                this.setY((float)HomeScreenResourceValues.activeRoadEarlyManeuverY);
                break;
            case SHRINK_LEFT:
                this.setX((float)HomeScreenResourceValues.activeRoadEarlyManeuverShrinkLeftX);
                this.setY((float)HomeScreenResourceValues.activeRoadEarlyManeuverYShrunk);
                break;
        }
    }
    
    public void setViewXY() {
        this.setView(this.uiStateManager.getCustomAnimateMode());
    }
    
    public void showHideEarlyManeuver() {
        if (this.lastNextTurnIconId != -1) {
            if (this.homeScreenView == null) {
                this.homeScreenView = this.uiStateManager.getHomescreenView();
            }
            int n;
            if (this.currentMode != MainView.CustomAnimationMode.EXPAND && !this.isShrunkVisible) {
                n = 1;
            }
            else {
                n = 0;
            }
            if (n != 0) {
                this.setVisibility(INVISIBLE);
            }
            else {
                this.setVisibility(View.VISIBLE);
            }
        }
        else {
            this.setVisibility(INVISIBLE);
            this.earlyManeuverImage.setImageResource(0);
        }
    }
}
