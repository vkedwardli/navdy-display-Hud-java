package com.navdy.hud.app.ui.component.main;

import android.view.View;
import com.navdy.hud.app.maps.NavigationMode;
import com.navdy.hud.app.maps.here.HereNavController;
import com.navdy.hud.app.maps.here.HereNavigationManager;
import com.navdy.hud.app.maps.here.HereMapsManager;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import android.util.AttributeSet;
import android.content.Context;
import com.navdy.hud.app.ui.component.homescreen.NavigationView;
import com.navdy.hud.app.ui.component.homescreen.HomeScreenView;
import com.navdy.service.library.log.Logger;
import android.widget.FrameLayout;

public class MainLowerView extends FrameLayout
{
    private static final Logger sLogger;
    private HomeScreenView homeScreenView;
    private NavigationView navigationView;
    
    static {
        sLogger = new Logger(MainLowerView.class);
    }
    
    public MainLowerView(final Context context) {
        super(context);
    }
    
    public MainLowerView(final Context context, final AttributeSet set) {
        super(context, set);
    }
    
    public MainLowerView(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
    }
    
    private boolean initView() {
        if (this.navigationView == null) {
            this.navigationView = RemoteDeviceManager.getInstance().getUiStateManager().getNavigationView();
        }
        if (this.homeScreenView == null) {
            this.homeScreenView = RemoteDeviceManager.getInstance().getUiStateManager().getHomescreenView();
        }
        return this.navigationView != null && this.homeScreenView != null;
    }
    
    public void ejectView() {
        MainLowerView.sLogger.v("ejectView");
        if (this.initView() && HereMapsManager.getInstance().isInitialized() && this.getVisibility() == 0) {
            this.animate().cancel();
            this.setVisibility(GONE);
            this.setAlpha(0.0f);
            this.removeAllViews();
            if (HereNavigationManager.getInstance().getNavController().getState() != HereNavController.State.NAVIGATING) {
                MainLowerView.sLogger.v("ejectView: reset to open mode");
                this.homeScreenView.setMode(NavigationMode.MAP);
            }
            else {
                MainLowerView.sLogger.v("ejectView: nav active already");
            }
        }
    }
    
    public void injectView(final View view) {
        MainLowerView.sLogger.v("injectView");
        if (this.initView() && HereMapsManager.getInstance().isInitialized()) {
            this.removeAllViews();
            this.addView(view);
            this.setAlpha(0.0f);
            this.setVisibility(View.VISIBLE);
            if (!this.homeScreenView.isNavigationActive()) {
                MainLowerView.sLogger.v("injectView: set to nav mode");
                this.homeScreenView.setMode(NavigationMode.MAP_ON_ROUTE);
            }
            else {
                MainLowerView.sLogger.v("injectView: nav active already");
            }
            this.animate().alpha(1.0f).start();
        }
    }
}
