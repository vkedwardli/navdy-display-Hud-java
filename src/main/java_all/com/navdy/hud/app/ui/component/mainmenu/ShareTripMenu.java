package com.navdy.hud.app.ui.component.mainmenu;

import com.navdy.hud.app.ui.component.destination.DestinationPickerScreen;
import com.navdy.service.library.events.navigation.NavigationRouteRequest;
import com.here.android.mpa.mapping.MapMarker;
import com.navdy.hud.app.analytics.AnalyticsSupport;
import com.here.android.mpa.common.GeoCoordinate;
import com.navdy.hud.app.maps.here.HereNavigationManager;
import com.navdy.hud.app.framework.notifications.INotification;
import com.navdy.hud.app.framework.glympse.GlympseNotification;
import com.navdy.hud.app.framework.notifications.NotificationManager;
import com.navdy.hud.app.framework.glympse.GlympseManager;
import com.navdy.hud.app.framework.contacts.Contact;
import com.navdy.service.library.log.Logger;
import com.navdy.hud.app.ui.component.destination.IDestinationPicker;

class ShareTripMenu implements IDestinationPicker
{
    private static final int SEND_WITHOUT_MESSAGE_POS = 0;
    private static final Logger logger;
    private final Contact contact;
    private String failedDestinationLabel;
    private double failedLatitude;
    private double failedLongitude;
    private String failedMessage;
    private final GlympseManager glympseManager;
    private boolean hasFailed;
    private boolean itemSelected;
    private final String notificationId;
    
    static {
        logger = new Logger(ShareTripMenu.class);
    }
    
    ShareTripMenu(final Contact contact, final String notificationId) {
        this.itemSelected = false;
        this.glympseManager = GlympseManager.getInstance();
        this.contact = contact;
        this.notificationId = notificationId;
        this.hasFailed = false;
    }
    
    private void addGlympseOfflineGlance(final String s, final Contact contact, final String s2, final double n, final double n2) {
        NotificationManager.getInstance().addNotification(new GlympseNotification(contact, GlympseNotification.Type.OFFLINE, s, s2, n, n2));
    }
    
    private void sendShare(final Contact contact) {
        this.sendShare(contact, null);
    }
    
    private void sendShare(final Contact contact, final String failedMessage) {
        final HereNavigationManager instance = HereNavigationManager.getInstance();
        String failedDestinationLabel = null;
        double latitude = 0.0;
        double longitude = 0.0;
        if (this.notificationId != null) {
            NotificationManager.getInstance().removeNotification(this.notificationId);
        }
        final GeoCoordinate geoCoordinate = null;
        GeoCoordinate coordinate;
        String s2;
        if (instance.isNavigationModeOn()) {
            final String s = "Trip";
            final String destinationLabel = instance.getDestinationLabel();
            final MapMarker destinationMarker = instance.getDestinationMarker();
            if (destinationMarker != null) {
                coordinate = destinationMarker.getCoordinate();
                s2 = s;
                failedDestinationLabel = destinationLabel;
            }
            else {
                final NavigationRouteRequest currentNavigationRouteRequest = instance.getCurrentNavigationRouteRequest();
                failedDestinationLabel = destinationLabel;
                coordinate = geoCoordinate;
                s2 = s;
                if (currentNavigationRouteRequest != null) {
                    failedDestinationLabel = destinationLabel;
                    coordinate = geoCoordinate;
                    s2 = s;
                    if (currentNavigationRouteRequest.destination != null) {
                        coordinate = new GeoCoordinate(currentNavigationRouteRequest.destination.latitude, currentNavigationRouteRequest.destination.longitude);
                        failedDestinationLabel = destinationLabel;
                        s2 = s;
                    }
                }
            }
        }
        else {
            s2 = "Location";
            coordinate = geoCoordinate;
        }
        if (coordinate != null) {
            latitude = coordinate.getLatitude();
            longitude = coordinate.getLongitude();
        }
        final StringBuilder sb = new StringBuilder();
        this.hasFailed = (this.glympseManager.addMessage(contact, failedMessage, failedDestinationLabel, latitude, longitude, sb) != GlympseManager.Error.NONE);
        if (this.hasFailed) {
            this.failedMessage = failedMessage;
            this.failedDestinationLabel = failedDestinationLabel;
            this.failedLatitude = latitude;
            this.failedLongitude = longitude;
        }
        AnalyticsSupport.recordGlympseSent(!this.hasFailed, s2, failedMessage, sb.toString());
        ShareTripMenu.logger.v("Glympse: success= " + !this.hasFailed + "share=" + s2);
    }
    
    @Override
    public void onDestinationPickerClosed() {
        if (!this.itemSelected) {
            AnalyticsSupport.recordMenuSelection("back");
        }
        if (this.hasFailed) {
            this.addGlympseOfflineGlance(this.failedMessage, this.contact, this.failedDestinationLabel, this.failedLatitude, this.failedLongitude);
        }
    }
    
    @Override
    public boolean onItemClicked(final int n, final int n2, final DestinationPickerScreen.DestinationPickerState destinationPickerState) {
        final String s = GlympseManager.getInstance().getMessages()[n2];
        ShareTripMenu.logger.v("selected option: " + s);
        AnalyticsSupport.recordMenuSelection("glympse_menu_" + s);
        this.itemSelected = true;
        if (n2 == 0) {
            this.sendShare(this.contact);
        }
        else {
            this.sendShare(this.contact, s);
        }
        return true;
    }
    
    @Override
    public boolean onItemSelected(final int n, final int n2, final DestinationPickerScreen.DestinationPickerState destinationPickerState) {
        return true;
    }
}
