package com.navdy.hud.app.ui.component.tbt;

import java.io.ObjectInputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import android.animation.AnimatorSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup;
import kotlin.TypeCastException;
import android.text.TextUtils;
import com.navdy.hud.app.maps.MapEvents;
import android.text.style.StyleSpan;
import android.text.SpannableStringBuilder;
import android.text.StaticLayout;
import android.text.Layout;
import android.util.AttributeSet;
import org.jetbrains.annotations.NotNull;
import android.content.Context;
import kotlin.jvm.internal.Intrinsics;
import com.navdy.hud.app.HudApplication;
//import kotlin.jvm.internal.DefaultConstructorMarker;
import com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder;
import com.navdy.hud.app.view.MainView;
import java.util.HashMap;
import android.content.res.Resources;
import com.navdy.service.library.log.Logger;
import kotlin.Metadata;
import android.widget.TextView;

@Metadata(bv = { 1, 0, 1 }, d1 = { "\u0000z\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u0007\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u000e\u0018\u0000 82\u00020\u0001:\u000289B\u000f\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004B\u0017\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006¢\u0006\u0002\u0010\u0007B\u001f\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\b\u001a\u00020\t¢\u0006\u0002\u0010\nJ(\u0010\u001c\u001a\u00020\u001d2\u0006\u0010\u001e\u001a\u00020\u00102\u0006\u0010\u001f\u001a\u00020 2\u0006\u0010\u001a\u001a\u00020\u001b2\u0006\u0010!\u001a\u00020\tH\u0002J\u0006\u0010\"\u001a\u00020\u001dJ\u0010\u0010#\u001a\u00020$2\u0006\u0010\u001e\u001a\u00020\u0010H\u0002J\u001a\u0010%\u001a\u00020\u001d2\u0006\u0010&\u001a\u00020\f2\n\u0010'\u001a\u00060(R\u00020)J \u0010*\u001a\u00020\u001d2\u0006\u0010+\u001a\u00020,2\u0006\u0010-\u001a\u00020\u000e2\u0006\u0010.\u001a\u00020\u000eH\u0002J\u000e\u0010/\u001a\u00020\u001d2\u0006\u0010&\u001a\u00020\fJ\u0010\u00100\u001a\u00020\u001d2\u0006\u00101\u001a\u00020\tH\u0002J\u0018\u00102\u001a\u00020\u001d2\u0006\u0010\u001e\u001a\u00020\u00102\u0006\u0010.\u001a\u00020\u000eH\u0002J8\u00103\u001a\u00020\u000e2\u0006\u00104\u001a\u00020 2\u0006\u0010!\u001a\u00020\t2\u0006\u0010\u001e\u001a\u00020\u00102\u0006\u0010\u001a\u001a\u00020\u001b2\u0006\u00105\u001a\u00020\u000e2\u0006\u00106\u001a\u00020\u000eH\u0002J\"\u00107\u001a\u00020\u001d2\u0006\u0010+\u001a\u00020,2\b\b\u0002\u0010-\u001a\u00020\u000e2\b\b\u0002\u0010.\u001a\u00020\u000eR\u0010\u0010\u000b\u001a\u0004\u0018\u00010\fX\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\u000f\u001a\u0004\u0018\u00010\u0010X\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\tX\u0082\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\u0012\u001a\u0004\u0018\u00010\u0013X\u0082\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\u0014\u001a\u0004\u0018\u00010\u0010X\u0082\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\u0015\u001a\u0004\u0018\u00010\u0010X\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\u0001X\u0082\u0004¢\u0006\u0002\n\u0000R\u0012\u0010\u0017\u001a\u00060\u0018j\u0002`\u0019X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u001a\u001a\u00020\u001bX\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006:" }, d2 = { "Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;", "Landroid/widget/TextView;", "context", "Landroid/content/Context;", "(Landroid/content/Context;)V", "attrs", "Landroid/util/AttributeSet;", "(Landroid/content/Context;Landroid/util/AttributeSet;)V", "defStyleAttr", "", "(Landroid/content/Context;Landroid/util/AttributeSet;I)V", "currentMode", "Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;", "instructionWidthIncludesNext", "", "lastInstruction", "", "lastInstructionWidth", "lastManeuverState", "Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;", "lastRoadText", "lastTurnText", "sizeCalculationTextView", "stringBuilder", "Ljava/lang/StringBuilder;", "Lkotlin/text/StringBuilder;", "widthInfo", "Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$WidthInfo;", "calculateTextWidth", "", "instruction", "textSize", "", "maxWidth", "clear", "getBoldText", "Landroid/text/SpannableStringBuilder;", "getCustomAnimator", "mode", "mainBuilder", "Landroid/animation/AnimatorSet.Builder;", "Landroid/animation/AnimatorSet;", "setInstruction", "event", "Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;", "adjustWidthForNextManeuver", "nextManeuverVisible", "setMode", "setViewWidth", "width", "setWidth", "tryTextSize", "fontSize", "check2Lines", "applyBold", "updateDisplay", "Companion", "WidthInfo", "app_hudRelease" }, k = 1, mv = { 1, 1, 6 })
public final class TbtTextInstructionView extends TextView
{
    public static final Companion Companion;
    private static final int fullWidth;
    private static final Logger logger;
    private static final int mediumWidth;
    private static final Resources resources;
    private static final int singleLineMinWidth;
    private static final float size18;
    private static final float size20;
    private static final float size22;
    private static final float size24;
    private static final float size26;
    private static final int smallWidth;
    private HashMap _$_findViewCache;
    private MainView.CustomAnimationMode currentMode;
    private boolean instructionWidthIncludesNext;
    private String lastInstruction;
    private int lastInstructionWidth;
    private HereManeuverDisplayBuilder.ManeuverState lastManeuverState;
    private String lastRoadText;
    private String lastTurnText;
    private final TextView sizeCalculationTextView;
    private final StringBuilder stringBuilder;
    private final WidthInfo widthInfo;
    
    static {
        Companion = new Companion();
        logger = new Logger("TbtTextInstructionView");
        final Resources resources2 = HudApplication.getAppContext().getResources();
        Intrinsics.checkExpressionValueIsNotNull(resources2, "HudApplication.getAppContext().resources");
        resources = resources2;
        fullWidth = TbtTextInstructionView.Companion.getResources().getDimensionPixelSize(R.dimen.tbt_instruction_full_w);
        mediumWidth = TbtTextInstructionView.Companion.getResources().getDimensionPixelSize(R.dimen.tbt_instruction_medium_w);
        smallWidth = TbtTextInstructionView.Companion.getResources().getDimensionPixelSize(R.dimen.tbt_instruction_small_w);
        singleLineMinWidth = TbtTextInstructionView.Companion.getResources().getDimensionPixelSize(R.dimen.tbt_instruction_min_w);
        size26 = TbtTextInstructionView.Companion.getResources().getDimension(R.dimen.tbt_instruction_26);
        size24 = TbtTextInstructionView.Companion.getResources().getDimension(R.dimen.tbt_instruction_24);
        size22 = TbtTextInstructionView.Companion.getResources().getDimension(R.dimen.tbt_instruction_22);
        size20 = TbtTextInstructionView.Companion.getResources().getDimension(R.dimen.tbt_instruction_20);
        size18 = TbtTextInstructionView.Companion.getResources().getDimension(R.dimen.tbt_instruction_18);
    }
    
    public TbtTextInstructionView(@NotNull final Context context) {
        super(context);
        Intrinsics.checkParameterIsNotNull(context, "context");
        this.lastInstructionWidth = -1;
        this.stringBuilder = new StringBuilder();
        this.widthInfo = new WidthInfo();
        this.sizeCalculationTextView = new TextView(HudApplication.getAppContext());
    }
    
    public TbtTextInstructionView(@NotNull final Context context, @NotNull final AttributeSet set) {
        Intrinsics.checkParameterIsNotNull(context, "context");
        Intrinsics.checkParameterIsNotNull(set, "attrs");
        super(context, set);
        this.lastInstructionWidth = -1;
        this.stringBuilder = new StringBuilder();
        this.widthInfo = new WidthInfo();
        this.sizeCalculationTextView = new TextView(HudApplication.getAppContext());
    }
    
    public TbtTextInstructionView(@NotNull final Context context, @NotNull final AttributeSet set, final int n) {
        Intrinsics.checkParameterIsNotNull(context, "context");
        Intrinsics.checkParameterIsNotNull(set, "attrs");
        super(context, set, n);
        this.lastInstructionWidth = -1;
        this.stringBuilder = new StringBuilder();
        this.widthInfo = new WidthInfo();
        this.sizeCalculationTextView = new TextView(HudApplication.getAppContext());
    }
    
    public static final /* synthetic */ int access$getFullWidth$cp() {
        return TbtTextInstructionView.fullWidth;
    }
    
    @NotNull
    public static final /* synthetic */ Logger access$getLogger$cp() {
        return TbtTextInstructionView.logger;
    }
    
    public static final /* synthetic */ int access$getMediumWidth$cp() {
        return TbtTextInstructionView.mediumWidth;
    }
    
    @NotNull
    public static final /* synthetic */ Resources access$getResources$cp() {
        return TbtTextInstructionView.resources;
    }
    
    public static final /* synthetic */ int access$getSingleLineMinWidth$cp() {
        return TbtTextInstructionView.singleLineMinWidth;
    }
    
    public static final /* synthetic */ float access$getSize18$cp() {
        return TbtTextInstructionView.size18;
    }
    
    public static final /* synthetic */ float access$getSize20$cp() {
        return TbtTextInstructionView.size20;
    }
    
    public static final /* synthetic */ float access$getSize22$cp() {
        return TbtTextInstructionView.size22;
    }
    
    public static final /* synthetic */ float access$getSize24$cp() {
        return TbtTextInstructionView.size24;
    }
    
    public static final /* synthetic */ float access$getSize26$cp() {
        return TbtTextInstructionView.size26;
    }
    
    public static final /* synthetic */ int access$getSmallWidth$cp() {
        return TbtTextInstructionView.smallWidth;
    }
    
    private final void calculateTextWidth(final String s, final float textSize, final WidthInfo widthInfo, final int n) {
        widthInfo.clear();
        this.sizeCalculationTextView.setTextSize(textSize);
        final StaticLayout staticLayout = new StaticLayout((CharSequence)s, this.sizeCalculationTextView.getPaint(), n, Layout.Alignment.ALIGN_NORMAL, 1.0f, 0.0f, false);
        widthInfo.setNumLines$app_hudRelease(staticLayout.getLineCount());
        widthInfo.setLineWidth1$app_hudRelease((int)staticLayout.getLineMax(0));
        if (widthInfo.getNumLines$app_hudRelease() > 1) {
            widthInfo.setLineWidth2$app_hudRelease((int)staticLayout.getLineMax(1));
        }
    }
    
    private final SpannableStringBuilder getBoldText(final String s) {
        final SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder();
        spannableStringBuilder.append((CharSequence)s);
        spannableStringBuilder.setSpan(new StyleSpan(1), 0, spannableStringBuilder.length(), 33);
        return spannableStringBuilder;
    }
    
    private final void setInstruction(final MapEvents.ManeuverDisplay maneuverDisplay, final boolean b, final boolean b2) {
        if (!TextUtils.equals((CharSequence)maneuverDisplay.pendingTurn, (CharSequence)this.lastTurnText) || !TextUtils.equals((CharSequence)maneuverDisplay.pendingRoad, (CharSequence)this.lastRoadText) || b) {
            this.lastTurnText = maneuverDisplay.pendingTurn;
            this.lastRoadText = maneuverDisplay.pendingRoad;
            this.stringBuilder.setLength(0);
            if (this.lastTurnText != null && HereManeuverDisplayBuilder.canShowTurnText(this.lastTurnText)) {
                this.stringBuilder.append(this.lastTurnText);
                this.stringBuilder.append(" ");
            }
            if (this.lastRoadText != null) {
                this.stringBuilder.append(this.lastRoadText);
            }
            final String string = this.stringBuilder.toString();
            Intrinsics.checkExpressionValueIsNotNull(this.lastInstruction = string, "str");
            this.setWidth(string, b2);
        }
    }
    
    private final void setViewWidth(final int width) {
        final ViewGroup.LayoutParams layoutParams = this.getLayoutParams();
        if (layoutParams == null) {
            throw new TypeCastException("null cannot be cast to non-null type android.view.ViewGroup.MarginLayoutParams");
        }
        ((ViewGroup.MarginLayoutParams)layoutParams).width = width;
        this.requestLayout();
    }
    
    private final void setWidth(final String s, final boolean b) {
        this.lastInstructionWidth = -1;
        int n = 0;
        final MainView.CustomAnimationMode currentMode = this.currentMode;
        if (currentMode != null) {
            switch (TbtTextInstructionView$WhenMappings.$EnumSwitchMapping$1[currentMode.ordinal()]) {
                case 1:
                    if (b) {
                        n = TbtTextInstructionView.Companion.getMediumWidth();
                        break;
                    }
                    n = TbtTextInstructionView.Companion.getFullWidth();
                    break;
                case 2:
                    n = TbtTextInstructionView.Companion.getSmallWidth();
                    break;
            }
        }
        if (!this.tryTextSize(TbtTextInstructionView.Companion.getSize26(), n, s, this.widthInfo, false, false) && !this.tryTextSize(TbtTextInstructionView.Companion.getSize24(), n, s, this.widthInfo, true, false) && !this.tryTextSize(TbtTextInstructionView.Companion.getSize22(), n, s, this.widthInfo, true, false) && !this.tryTextSize(TbtTextInstructionView.Companion.getSize20(), n, s, this.widthInfo, true, true)) {
            this.setTextSize(TbtTextInstructionView.Companion.getSize18());
            this.setText((CharSequence)this.getBoldText(s));
            this.setViewWidth(n);
            this.lastInstructionWidth = n;
        }
    }
    
    private final boolean tryTextSize(final float n, int n2, final String s, final WidthInfo widthInfo, final boolean b, final boolean b2) {
        final boolean b3 = false;
        this.calculateTextWidth(s, n, widthInfo, n2);
        boolean b4;
        if (widthInfo.getNumLines$app_hudRelease() == 1) {
            this.setTextSize(n);
            CharSequence text;
            if (!b2) {
                text = s;
            }
            else {
                text = (CharSequence)this.getBoldText(s);
            }
            this.setText(text);
            if ((n2 = widthInfo.getLineWidth1$app_hudRelease()) < TbtTextInstructionView.Companion.getSingleLineMinWidth()) {
                n2 = TbtTextInstructionView.Companion.getSingleLineMinWidth();
            }
            this.setViewWidth(n2);
            this.lastInstructionWidth = n2;
            b4 = true;
        }
        else {
            b4 = b3;
            if (b) {
                b4 = b3;
                if (widthInfo.getNumLines$app_hudRelease() == 2) {
                    this.setTextSize(n);
                    CharSequence text2;
                    if (!b2) {
                        text2 = s;
                    }
                    else {
                        text2 = (CharSequence)this.getBoldText(s);
                    }
                    this.setText(text2);
                    this.setViewWidth(n2);
                    this.lastInstructionWidth = n2;
                    b4 = true;
                }
            }
        }
        return b4;
    }
    
    public void _$_clearFindViewByIdCache() {
        if (this._$_findViewCache != null) {
            this._$_findViewCache.clear();
        }
    }
    
    public View _$_findCachedViewById(final int n) {
        if (this._$_findViewCache == null) {
            this._$_findViewCache = new HashMap();
        }
        View viewById;
        if ((viewById = this._$_findViewCache.get(n)) == null) {
            viewById = this.findViewById(n);
            this._$_findViewCache.put(n, viewById);
        }
        return viewById;
    }
    
    public final void clear() {
        this.setText((CharSequence)"");
        this.lastTurnText = null;
        this.lastRoadText = null;
        this.lastInstruction = null;
        this.lastInstructionWidth = -1;
        this.instructionWidthIncludesNext = false;
    }
    
    public final void getCustomAnimator(@NotNull final MainView.CustomAnimationMode customAnimationMode, @NotNull final AnimatorSet.Builder animatorSet$Builder) {
        Intrinsics.checkParameterIsNotNull(customAnimationMode, "mode");
        Intrinsics.checkParameterIsNotNull(animatorSet$Builder, "mainBuilder");
    }
    
    public final void setMode(@NotNull final MainView.CustomAnimationMode currentMode) {
        Intrinsics.checkParameterIsNotNull(currentMode, "mode");
        if (!Intrinsics.areEqual(this.currentMode, currentMode)) {
            if (this.lastInstructionWidth == -1) {
                if (Intrinsics.areEqual(currentMode, MainView.CustomAnimationMode.EXPAND)) {
                    this.setViewWidth(TbtTextInstructionView.Companion.getFullWidth());
                }
                else {
                    this.setViewWidth(TbtTextInstructionView.Companion.getMediumWidth());
                }
            }
            else {
                final boolean b = false;
                final ViewGroup.LayoutParams layoutParams = this.getLayoutParams();
                if (layoutParams == null) {
                    throw new TypeCastException("null cannot be cast to non-null type android.view.ViewGroup.MarginLayoutParams");
                }
                final ViewGroup.MarginLayoutParams viewGroup$MarginLayoutParams = (ViewGroup.MarginLayoutParams)layoutParams;
                int n = 0;
                switch (TbtTextInstructionView$WhenMappings.$EnumSwitchMapping$0[currentMode.ordinal()]) {
                    default:
                        n = (b ? 1 : 0);
                        break;
                    case 1:
                        n = (b ? 1 : 0);
                        if (!Intrinsics.areEqual(this.currentMode, MainView.CustomAnimationMode.SHRINK_LEFT)) {
                            break;
                        }
                        n = (b ? 1 : 0);
                        if (viewGroup$MarginLayoutParams.width == TbtTextInstructionView.Companion.getMediumWidth()) {
                            n = 1;
                            break;
                        }
                        break;
                    case 2:
                        n = (b ? 1 : 0);
                        if (!Intrinsics.areEqual(this.currentMode, MainView.CustomAnimationMode.EXPAND)) {
                            break;
                        }
                        n = (b ? 1 : 0);
                        if (this.lastInstructionWidth > TbtTextInstructionView.Companion.getMediumWidth()) {
                            n = 1;
                            break;
                        }
                        break;
                }
                if (n != 0) {
                    final String lastInstruction = this.lastInstruction;
                    if (lastInstruction != null) {
                        this.setWidth(lastInstruction, true);
                    }
                }
            }
            this.currentMode = currentMode;
        }
    }
    
    public final void updateDisplay(@NotNull final MapEvents.ManeuverDisplay maneuverDisplay, final boolean b, final boolean b2) {
        Intrinsics.checkParameterIsNotNull(maneuverDisplay, "event");
        this.setInstruction(maneuverDisplay, b, b2);
        this.lastManeuverState = maneuverDisplay.maneuverState;
    }
    
    @Metadata(bv = { 1, 0, 1 }, d1 = { "\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0007\n\u0002\b\r\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0014\u0010\u0003\u001a\u00020\u0004X\u0082\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006R\u0014\u0010\u0007\u001a\u00020\bX\u0082\u0004¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0014\u0010\u000b\u001a\u00020\u0004X\u0082\u0004¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\u0006R\u0014\u0010\r\u001a\u00020\u000eX\u0082\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010R\u0014\u0010\u0011\u001a\u00020\u0004X\u0082\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u0006R\u0014\u0010\u0013\u001a\u00020\u0014X\u0082\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0015\u0010\u0016R\u0014\u0010\u0017\u001a\u00020\u0014X\u0082\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0018\u0010\u0016R\u0014\u0010\u0019\u001a\u00020\u0014X\u0082\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u001a\u0010\u0016R\u0014\u0010\u001b\u001a\u00020\u0014X\u0082\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u001c\u0010\u0016R\u0014\u0010\u001d\u001a\u00020\u0014X\u0082\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u001e\u0010\u0016R\u0014\u0010\u001f\u001a\u00020\u0004X\u0082\u0004¢\u0006\b\n\u0000\u001a\u0004\b \u0010\u0006¨\u0006!" }, d2 = { "Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$Companion;", "", "()V", "fullWidth", "", "getFullWidth", "()I", "logger", "Lcom/navdy/service/library/log/Logger;", "getLogger", "()Lcom/navdy/service/library/log/Logger;", "mediumWidth", "getMediumWidth", "resources", "Landroid/content/res/Resources;", "getResources", "()Landroid/content/res/Resources;", "singleLineMinWidth", "getSingleLineMinWidth", "size18", "", "getSize18", "()F", "size20", "getSize20", "size22", "getSize22", "size24", "getSize24", "size26", "getSize26", "smallWidth", "getSmallWidth", "app_hudRelease" }, k = 1, mv = { 1, 1, 6 })
    public static final class Companion
    {
        private final int getFullWidth() {
            return TbtTextInstructionView.access$getFullWidth$cp();
        }
        
        private final Logger getLogger() {
            return TbtTextInstructionView.access$getLogger$cp();
        }
        
        private final int getMediumWidth() {
            return TbtTextInstructionView.access$getMediumWidth$cp();
        }
        
        private final Resources getResources() {
            return TbtTextInstructionView.access$getResources$cp();
        }
        
        private final int getSingleLineMinWidth() {
            return TbtTextInstructionView.access$getSingleLineMinWidth$cp();
        }
        
        private final float getSize18() {
            return TbtTextInstructionView.access$getSize18$cp();
        }
        
        private final float getSize20() {
            return TbtTextInstructionView.access$getSize20$cp();
        }
        
        private final float getSize22() {
            return TbtTextInstructionView.access$getSize22$cp();
        }
        
        private final float getSize24() {
            return TbtTextInstructionView.access$getSize24$cp();
        }
        
        private final float getSize26() {
            return TbtTextInstructionView.access$getSize26$cp();
        }
        
        private final int getSmallWidth() {
            return TbtTextInstructionView.access$getSmallWidth$cp();
        }
    }
    
    @Metadata(bv = { 1, 0, 1 }, d1 = { "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u000b\n\u0002\u0010\u0002\n\u0000\b\u0002\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u0006\u0010\u000f\u001a\u00020\u0010R\u001a\u0010\u0003\u001a\u00020\u0004X\u0080\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\bR\u001a\u0010\t\u001a\u00020\u0004X\u0080\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\n\u0010\u0006\"\u0004\b\u000b\u0010\bR\u001a\u0010\f\u001a\u00020\u0004X\u0080\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\r\u0010\u0006\"\u0004\b\u000e\u0010\b¨\u0006\u0011" }, d2 = { "Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView$WidthInfo;", "", "()V", "lineWidth1", "", "getLineWidth1$app_hudRelease", "()I", "setLineWidth1$app_hudRelease", "(I)V", "lineWidth2", "getLineWidth2$app_hudRelease", "setLineWidth2$app_hudRelease", "numLines", "getNumLines$app_hudRelease", "setNumLines$app_hudRelease", "clear", "", "app_hudRelease" }, k = 1, mv = { 1, 1, 6 })
    private static final class WidthInfo
    {
        private int lineWidth1;
        private int lineWidth2;
        private int numLines;
        
        public final void clear() {
            this.numLines = 0;
            this.lineWidth1 = 0;
            this.lineWidth2 = 0;
        }
        
        public final int getLineWidth1$app_hudRelease() {
            return this.lineWidth1;
        }
        
        public final int getLineWidth2$app_hudRelease() {
            return this.lineWidth2;
        }
        
        public final int getNumLines$app_hudRelease() {
            return this.numLines;
        }
        
        public final void setLineWidth1$app_hudRelease(final int lineWidth1) {
            this.lineWidth1 = lineWidth1;
        }
        
        public final void setLineWidth2$app_hudRelease(final int lineWidth2) {
            this.lineWidth2 = lineWidth2;
        }
        
        public final void setNumLines$app_hudRelease(final int numLines) {
            this.numLines = numLines;
        }
    }
}
