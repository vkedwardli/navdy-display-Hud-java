package com.navdy.hud.app.manager;

import com.squareup.otto.Subscribe;
import android.os.Looper;
import com.navdy.hud.app.event.InitEvents;
import android.bluetooth.BluetoothAdapter;
import android.os.Handler;
import com.navdy.hud.app.service.ConnectionHandler;
import com.squareup.otto.Bus;
import com.navdy.service.library.log.Logger;

public class InitManager
{
    private static final Logger sLogger;
    private Bus bus;
    private Runnable checkConnectionServiceReady;
    private ConnectionHandler connectionHandler;
    private Handler handler;
    
    static {
        sLogger = new Logger(InitManager.class);
    }
    
    public InitManager(final Bus bus, final ConnectionHandler connectionHandler) {
        this.checkConnectionServiceReady = new Runnable() {
            public boolean notified;
            
            @Override
            public void run() {
                if (!this.notified && InitManager.this.connectionHandler.serviceConnected() && BluetoothAdapter.getDefaultAdapter().isEnabled()) {
                    InitManager.sLogger.i("bt and connection service ready");
                    this.notified = true;
                    InitManager.this.bus.post(new InitEvents.InitPhase(InitEvents.Phase.CONNECTION_SERVICE_STARTED));
                    InitManager.this.handler.post((Runnable)new PhaseEmitter(InitEvents.Phase.POST_START));
                    InitManager.this.handler.post((Runnable)new PhaseEmitter(InitEvents.Phase.LATE));
                }
            }
        };
        this.bus = bus;
        this.handler = new Handler(Looper.getMainLooper());
        this.bus.register(this);
        this.connectionHandler = connectionHandler;
    }
    
    @Subscribe
    public void onBluetoothChanged(final InitEvents.BluetoothStateChanged bluetoothStateChanged) {
        this.handler.post(this.checkConnectionServiceReady);
    }
    
    @Subscribe
    public void onConnectionService(final InitEvents.ConnectionServiceStarted connectionServiceStarted) {
        this.handler.post(this.checkConnectionServiceReady);
    }
    
    public void start() {
        this.handler.post((Runnable)new PhaseEmitter(InitEvents.Phase.PRE_USER_INTERACTION));
        this.handler.post(this.checkConnectionServiceReady);
    }
    
    private class PhaseEmitter implements Runnable
    {
        private InitEvents.Phase phase;
        
        public PhaseEmitter(final InitEvents.Phase phase) {
            this.phase = phase;
        }
        
        @Override
        public void run() {
            InitManager.sLogger.i("Triggering init phase:" + this.phase);
            InitManager.this.bus.post(new InitEvents.InitPhase(this.phase));
        }
    }
}
