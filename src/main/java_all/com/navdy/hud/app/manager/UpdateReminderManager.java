package com.navdy.hud.app.manager;

import com.navdy.hud.app.event.ShowScreenWithArgs;
import com.navdy.service.library.events.ui.Screen;
import com.navdy.hud.app.screen.DialUpdateProgressScreen;
import android.os.Bundle;
import com.navdy.hud.app.common.TimeHelper;
import com.squareup.otto.Subscribe;
import com.navdy.hud.app.device.dial.DialManager;
import com.navdy.hud.app.util.OTAUpdateService;
import mortar.Mortar;
import android.os.Looper;
import android.content.Context;
import java.util.concurrent.TimeUnit;
import android.content.SharedPreferences;
import android.os.Handler;
import javax.inject.Inject;
import com.squareup.otto.Bus;
import com.navdy.service.library.log.Logger;

public class UpdateReminderManager
{
    private static long DIAL_CONNECT_CHECK_INTERVAL = 0L;
    public static final String DIAL_REMINDER_TIME = "dial_reminder_time";
    public static final String EXTRA_UPDATE_REMINDER = "UPDATE_REMINDER";
    private static long INITIAL_REMINDER_INTERVAL = 0L;
    public static final String OTA_REMINDER_TIME = "ota_reminder_time";
    private static long REMINDER_INTERVAL;
    private static final Logger sLogger;
    @Inject
    Bus bus;
    private boolean dialUpdateAvailable;
    private Handler handler;
    private ReminderRunnable reminderRunnable;
    @Inject
    SharedPreferences sharedPreferences;
    private boolean timeAvailable;
    
    static {
        sLogger = new Logger(UpdateReminderManager.class);
        UpdateReminderManager.INITIAL_REMINDER_INTERVAL = TimeUnit.DAYS.toMillis(10L);
        UpdateReminderManager.REMINDER_INTERVAL = TimeUnit.DAYS.toMillis(1L);
        UpdateReminderManager.DIAL_CONNECT_CHECK_INTERVAL = TimeUnit.MINUTES.toMillis(15L);
    }
    
    public UpdateReminderManager(final Context context) {
        this.handler = new Handler(Looper.getMainLooper());
        this.reminderRunnable = null;
        this.timeAvailable = false;
        this.dialUpdateAvailable = false;
        Mortar.inject(context, this);
        this.bus.register(this);
    }
    
    private void clearReminder() {
        if (this.reminderRunnable != null) {
            this.handler.removeCallbacks((Runnable)this.reminderRunnable);
            this.reminderRunnable = null;
        }
    }
    
    private boolean isOTAUpdateRequired() {
        return OTAUpdateService.isUpdateAvailable();
    }
    
    private void scheduleReminder() {
        if (this.timeAvailable) {
            this.clearReminder();
            long long1 = this.sharedPreferences.getLong("ota_reminder_time", 0L);
            long long2 = 0L;
            if (this.dialUpdateAvailable) {
                long2 = this.sharedPreferences.getLong("dial_reminder_time", 0L);
            }
            final long n = 0L;
            boolean b = false;
            if (long1 == 0L || (long2 != 0L && long1 > long2)) {
                long1 = n;
                if (long2 != 0L) {
                    b = true;
                    long1 = long2;
                }
            }
            final long currentTimeMillis = System.currentTimeMillis();
            if (long1 != 0L) {
                final long n2 = long1 - currentTimeMillis;
                long millis;
                if (b && n2 < TimeUnit.SECONDS.toMillis(1L)) {
                    millis = TimeUnit.SECONDS.toMillis(1L);
                }
                else {
                    millis = n2;
                    if (n2 < 0L) {
                        millis = 0L;
                    }
                }
                final Logger sLogger = UpdateReminderManager.sLogger;
                String s;
                if (b) {
                    s = "dial";
                }
                else {
                    s = "OTA";
                }
                sLogger.v(String.format("new scheduling %s reminder for +%d seconds.", s, TimeUnit.MILLISECONDS.toSeconds(millis)));
                this.reminderRunnable = new ReminderRunnable(b);
                this.handler.postDelayed((Runnable)this.reminderRunnable, millis);
            }
        }
    }
    
    @Subscribe
    public void handleDialUpdate(final DialManager.DialUpdateStatus dialUpdateStatus) {
        final long long1 = this.sharedPreferences.getLong("dial_reminder_time", 0L);
        if (dialUpdateStatus.available) {
            final long currentTimeMillis = System.currentTimeMillis();
            if (long1 == 0L) {
                this.sharedPreferences.edit().putLong("dial_reminder_time", currentTimeMillis + UpdateReminderManager.INITIAL_REMINDER_INTERVAL).apply();
                UpdateReminderManager.sLogger.v(String.format("handleDialUpdate(): setting dial update reminder time to +%d seconds", TimeUnit.MILLISECONDS.toSeconds(UpdateReminderManager.INITIAL_REMINDER_INTERVAL)));
            }
            else {
                UpdateReminderManager.sLogger.v(String.format("handleDialUpdate(): dial update reminder time already set to +%d seconds", TimeUnit.MILLISECONDS.toSeconds(long1 - currentTimeMillis)));
            }
            this.dialUpdateAvailable = true;
            this.scheduleReminder();
        }
        else if (long1 != 0L) {
            this.sharedPreferences.edit().remove("dial_reminder_time").apply();
            UpdateReminderManager.sLogger.v("handleDialUpdate(): clearing dial reminder");
        }
    }
    
    @Subscribe
    public void handleOTAUpdate(final OTAUpdateService.UpdateVerified updateVerified) {
        final long long1 = this.sharedPreferences.getLong("ota_reminder_time", 0L);
        final long currentTimeMillis = System.currentTimeMillis();
        if (long1 == 0L) {
            this.sharedPreferences.edit().putLong("ota_reminder_time", UpdateReminderManager.INITIAL_REMINDER_INTERVAL + currentTimeMillis).apply();
            UpdateReminderManager.sLogger.v(String.format("handleOTAUpdate() setting OTA reminder for +%d seconds ", TimeUnit.MILLISECONDS.toSeconds(UpdateReminderManager.INITIAL_REMINDER_INTERVAL)));
        }
        else {
            UpdateReminderManager.sLogger.v(String.format("handleOTAUpdate() OTA reminder already set for +%d seconds", TimeUnit.MILLISECONDS.toSeconds(long1 - currentTimeMillis)));
        }
        this.scheduleReminder();
    }
    
    @Subscribe
    public void onDateTimeAvailable(final TimeHelper.DateTimeAvailableEvent dateTimeAvailableEvent) {
        this.timeAvailable = true;
        final long long1 = this.sharedPreferences.getLong("ota_reminder_time", 0L);
        if (this.isOTAUpdateRequired()) {
            final long currentTimeMillis = System.currentTimeMillis();
            if (long1 == 0L) {
                this.sharedPreferences.edit().putLong("ota_reminder_time", UpdateReminderManager.INITIAL_REMINDER_INTERVAL + currentTimeMillis).apply();
                UpdateReminderManager.sLogger.v(String.format("onDateTimeAvailable(): setting OTA reminder time to +%d seconds ", TimeUnit.MILLISECONDS.toSeconds(UpdateReminderManager.INITIAL_REMINDER_INTERVAL)));
            }
            else {
                UpdateReminderManager.sLogger.v(String.format("onDateTimeAvailable() OTA reminder time already set to +%d seconds", TimeUnit.MILLISECONDS.toSeconds(long1 - currentTimeMillis)));
            }
        }
        else if (long1 != 0L) {
            this.sharedPreferences.edit().remove("ota_reminder_time").apply();
            UpdateReminderManager.sLogger.v("onDateTimeAvailable(): clearing ota reminder");
        }
        this.scheduleReminder();
    }
    
    private class ReminderRunnable implements Runnable
    {
        boolean isDialUpdate;
        
        ReminderRunnable(final boolean isDialUpdate) {
            this.isDialUpdate = isDialUpdate;
        }
        
        @Override
        public void run() {
            final Bundle bundle = new Bundle();
            if (this.isDialUpdate) {
                final DialManager instance = DialManager.getInstance();
                if (instance.isDialConnected()) {
                    if (instance.getDialFirmwareUpdater().getVersions().isUpdateAvailable()) {
                        bundle.putBoolean("UPDATE_REMINDER", true);
                        if (!DialUpdateProgressScreen.updateStarted) {
                            UpdateReminderManager.this.bus.post(new ShowScreenWithArgs(Screen.SCREEN_DIAL_UPDATE_CONFIRMATION, bundle, false));
                        }
                        UpdateReminderManager.this.sharedPreferences.edit().putLong("dial_reminder_time", System.currentTimeMillis() + UpdateReminderManager.REMINDER_INTERVAL).apply();
                    }
                    else {
                        UpdateReminderManager.this.sharedPreferences.edit().remove("dial_reminder_time").apply();
                    }
                }
                else {
                    UpdateReminderManager.this.sharedPreferences.edit().putLong("dial_reminder_time", System.currentTimeMillis() + UpdateReminderManager.DIAL_CONNECT_CHECK_INTERVAL).apply();
                }
            }
            else if (UpdateReminderManager.this.isOTAUpdateRequired()) {
                bundle.putBoolean("UPDATE_REMINDER", true);
                UpdateReminderManager.this.bus.post(new ShowScreenWithArgs(Screen.SCREEN_OTA_CONFIRMATION, bundle, false));
                UpdateReminderManager.this.sharedPreferences.edit().putLong("ota_reminder_time", System.currentTimeMillis() + UpdateReminderManager.REMINDER_INTERVAL).apply();
            }
            else {
                UpdateReminderManager.this.sharedPreferences.edit().remove("ota_reminder_time").apply();
            }
            UpdateReminderManager.this.scheduleReminder();
        }
    }
}
