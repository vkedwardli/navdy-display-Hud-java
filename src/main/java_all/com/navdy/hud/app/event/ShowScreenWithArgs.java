package com.navdy.hud.app.event;

import com.navdy.service.library.events.ui.Screen;
import android.os.Bundle;

public class ShowScreenWithArgs
{
    public Bundle args;
    public Object args2;
    public boolean ignoreAnimation;
    public Screen screen;
    
    public ShowScreenWithArgs(final Screen screen, final Bundle args, final Object args2, final boolean ignoreAnimation) {
        this.screen = screen;
        this.args = args;
        this.args2 = args2;
        this.ignoreAnimation = ignoreAnimation;
    }
    
    public ShowScreenWithArgs(final Screen screen, final Bundle bundle, final boolean b) {
        this(screen, bundle, null, b);
    }
}
