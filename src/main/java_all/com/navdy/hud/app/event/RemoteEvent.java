package com.navdy.hud.app.event;

import com.squareup.wire.Message;

public class RemoteEvent
{
    protected Message message;
    
    public RemoteEvent() {
    }
    
    public RemoteEvent(final Message message) {
        this.message = message;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (this != o) {
            if (o == null || this.getClass() != o.getClass()) {
                b = false;
            }
            else {
                final RemoteEvent remoteEvent = (RemoteEvent)o;
                if (this.message != null) {
                    if (this.message.equals(remoteEvent.message)) {
                        return b;
                    }
                }
                else if (remoteEvent.message == null) {
                    return b;
                }
                b = false;
            }
        }
        return b;
    }
    
    public Message getMessage() {
        return this.message;
    }
    
    @Override
    public int hashCode() {
        int hashCode;
        if (this.message != null) {
            hashCode = this.message.hashCode();
        }
        else {
            hashCode = 0;
        }
        return hashCode;
    }
}
