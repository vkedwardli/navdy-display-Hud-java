package com.navdy.hud.app.event;

public class InitEvents
{
    public static class BluetoothStateChanged
    {
        public final boolean enabled;
        
        public BluetoothStateChanged(final boolean enabled) {
            this.enabled = enabled;
        }
    }
    
    public static class ConnectionServiceStarted
    {
    }
    
    public static class InitPhase
    {
        public final Phase phase;
        
        public InitPhase(final Phase phase) {
            this.phase = phase;
        }
    }
    
    public enum Phase
    {
        CONNECTION_SERVICE_STARTED, 
        EARLY, 
        LATE, 
        LOCALE_UP_TO_DATE, 
        POST_START, 
        PRE_USER_INTERACTION, 
        SWITCHING_LOCALE;
    }
}
