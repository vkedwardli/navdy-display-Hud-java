package com.navdy.hud.app.profile;

import com.navdy.service.library.events.preferences.NotificationPreferencesUpdate;
import com.navdy.service.library.events.preferences.NavigationPreferences;
import com.navdy.service.library.events.preferences.NavigationPreferencesUpdate;
import com.navdy.service.library.events.preferences.InputPreferencesUpdate;
import com.squareup.wire.Wire;
import com.navdy.hud.app.event.DriverProfileUpdated;
import com.navdy.service.library.events.preferences.DriverProfilePreferencesUpdate;
import com.navdy.hud.app.analytics.AnalyticsSupport;
import com.navdy.service.library.events.preferences.DisplaySpeakerPreferencesUpdate;
import com.navdy.hud.app.service.ConnectionHandler;
import com.squareup.otto.Subscribe;
import com.navdy.service.library.events.glances.CannedMessagesUpdate;
import java.util.Collection;
import android.content.SharedPreferences;
import android.content.Context;
import com.navdy.service.library.events.preferences.LocalPreferences;
import java.io.IOException;
import com.navdy.hud.app.event.RemoteEvent;
import com.navdy.service.library.events.glances.CannedMessagesRequest;
import com.navdy.service.library.events.preferences.InputPreferencesRequest;
import com.navdy.service.library.events.preferences.NotificationPreferencesRequest;
import com.navdy.service.library.events.preferences.NavigationPreferencesRequest;
import com.navdy.service.library.events.preferences.DriverProfilePreferencesRequest;
import java.util.Objects;
import com.navdy.service.library.events.photo.PhotoType;
import com.navdy.hud.app.framework.contacts.PhoneImageDownloader;
import com.navdy.hud.app.util.GenericUtil;
import com.navdy.service.library.events.RequestStatus;
import com.squareup.wire.Message;
import java.util.Iterator;
import android.text.TextUtils;
import com.navdy.service.library.task.TaskManager;
import java.util.Locale;
import com.navdy.hud.app.event.InitEvents;
import com.navdy.hud.app.HudApplication;
import com.navdy.service.library.events.preferences.DriverProfilePreferences;
import java.io.FileFilter;
import java.io.File;
import java.util.HashSet;
import com.navdy.hud.app.common.TimeHelper;
import com.navdy.hud.app.storage.PathManager;
import java.util.Set;
import com.squareup.otto.Bus;
import java.util.HashMap;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.device.NavdyDeviceId;
import com.navdy.hud.app.event.DriverProfileChanged;

public class DriverProfileManager
{
    private static final String DEFAULT_PROFILE_NAME = "DefaultProfile";
    private static final DriverProfileChanged DRIVER_PROFILE_CHANGED;
    private static final String LOCAL_PREFERENCES_SUFFIX = "_preferences";
    private static final NavdyDeviceId MOCK_PROFILE_DEVICE_ID;
    private static final Logger sLogger;
    private String lastUserEmail;
    private String lastUserName;
    private HashMap<String, DriverProfile> mAllProfiles;
    private Bus mBus;
    private DriverProfile mCurrentProfile;
    private String mProfileDirectoryName;
    private Set<DriverProfile> mPublicProfiles;
    private DriverSessionPreferences mSessionPrefs;
    private DriverProfile theDefaultProfile;
    
    static {
        sLogger = new Logger(DriverProfileManager.class);
        MOCK_PROFILE_DEVICE_ID = NavdyDeviceId.UNKNOWN_ID;
        DRIVER_PROFILE_CHANGED = new DriverProfileChanged();
    }
    
    public DriverProfileManager(Bus name, final PathManager pathManager, final TimeHelper timeHelper) {
        this.mAllProfiles = new HashMap<String, DriverProfile>();
        this.mPublicProfiles = new HashSet<DriverProfile>();
        this.mProfileDirectoryName = pathManager.getDriverProfilesDir();
        (this.mBus = name).register(this);
        DriverProfileManager.sLogger.i("initializing in " + this.mProfileDirectoryName);
        final File[] listFiles = new File(this.mProfileDirectoryName).listFiles(new FileFilter() {
            @Override
            public boolean accept(final File file) {
                return file.isDirectory();
            }
        });
        final int length = listFiles.length;
        int i = 0;
        while (i < length) {
            name = (Bus)listFiles[i];
            try {
                final DriverProfile theDefaultProfile = new DriverProfile((File)name);
                name = (Bus)((File)name).getName();
                this.mAllProfiles.put((String)name, theDefaultProfile);
                if (theDefaultProfile.isProfilePublic()) {
                    this.mPublicProfiles.add(theDefaultProfile);
                }
                if (((String)name).equals(this.profileNameForId(DriverProfileManager.MOCK_PROFILE_DEVICE_ID))) {
                    this.theDefaultProfile = theDefaultProfile;
                }
                ++i;
                continue;
            }
            catch (Exception ex) {
                DriverProfileManager.sLogger.e("could not load profile from " + name, ex);
            }
            break;
        }
        if (this.theDefaultProfile == null) {
            this.theDefaultProfile = this.createProfileForId(DriverProfileManager.MOCK_PROFILE_DEVICE_ID);
        }
        this.theDefaultProfile.getNavigationPreferences();
        this.mSessionPrefs = new DriverSessionPreferences(this.mBus, this.theDefaultProfile, timeHelper);
        this.setCurrentProfile(this.theDefaultProfile);
    }
    
    private void changeLocale(final boolean b) {
        while (true) {
            Label_0151: {
                try {
                    final Locale currentLocale = this.getCurrentLocale();
                    DriverProfileManager.sLogger.v("[HUD-locale] changeLocale current=" + currentLocale);
                    if (HudLocale.switchLocale(HudApplication.getAppContext(), currentLocale)) {
                        DriverProfileManager.sLogger.v("[HUD-locale] change, restarting...");
                        this.mBus.post(new InitEvents.InitPhase(InitEvents.Phase.SWITCHING_LOCALE));
                        HudLocale.showLocaleChangeToast();
                    }
                    else {
                        DriverProfileManager.sLogger.v("[HUD-locale] not changed");
                        this.mBus.post(new InitEvents.InitPhase(InitEvents.Phase.LOCALE_UP_TO_DATE));
                        if (HudLocale.isLocaleSupported(currentLocale)) {
                            break Label_0151;
                        }
                        if (b) {
                            HudLocale.showLocaleNotSupportedToast(currentLocale.getDisplayLanguage());
                        }
                    }
                    return;
                }
                catch (Throwable t) {
                    DriverProfileManager.sLogger.e("[HUD-locale] changeLocale", t);
                    return;
                }
            }
            HudLocale.dismissToast();
        }
    }
    
    private void copyCurrentProfileToDefault() {
        if (this.mCurrentProfile != this.theDefaultProfile) {
            TaskManager.getInstance().execute(new Runnable() {
                @Override
                public void run() {
                    DriverProfileManager.sLogger.v("copyCurrentProfileToDefault");
                    DriverProfileManager.this.theDefaultProfile.copy(DriverProfileManager.this.mCurrentProfile);
                }
            }, 10);
        }
    }
    
    private DriverProfilePreferences findSupportedLocale(final DriverProfilePreferences driverProfilePreferences) {
        final Locale localeForID = HudLocale.getLocaleForID(driverProfilePreferences.locale);
        DriverProfilePreferences build = driverProfilePreferences;
        if (!HudLocale.isLocaleSupported(localeForID)) {
            build = driverProfilePreferences;
            if (driverProfilePreferences.additionalLocales != null) {
                final Locale locale = null;
                final Iterator<String> iterator = driverProfilePreferences.additionalLocales.iterator();
                Locale localeForID2;
                while (true) {
                    localeForID2 = locale;
                    if (!iterator.hasNext()) {
                        break;
                    }
                    final String s = iterator.next();
                    if (TextUtils.isEmpty((CharSequence)s)) {
                        continue;
                    }
                    localeForID2 = HudLocale.getLocaleForID(s);
                    if (HudLocale.isLocaleSupported(localeForID2)) {
                        DriverProfileManager.sLogger.v("HUD-locale additional locale supported:" + localeForID2);
                        break;
                    }
                    DriverProfileManager.sLogger.v("HUD-locale additional locale not supported:" + localeForID2);
                }
                build = driverProfilePreferences;
                if (localeForID2 != null) {
                    build = new DriverProfilePreferences.Builder(driverProfilePreferences).locale(localeForID2.toString()).build();
                }
            }
        }
        return build;
    }
    
    private void handleUpdateInBackground(final Message message, final RequestStatus requestStatus, final Message message2, final Runnable runnable) {
        if (this.mCurrentProfile != this.theDefaultProfile) {
            final String simpleName = message.getClass().getSimpleName();
            switch (requestStatus) {
                default:
                    DriverProfileManager.sLogger.i("profile status for " + simpleName + " was " + requestStatus + ", which I didn't expect; ignoring.");
                    break;
                case REQUEST_VERSION_IS_CURRENT:
                    DriverProfileManager.sLogger.i(simpleName + " returned up to date!");
                    break;
                case REQUEST_SUCCESS:
                    DriverProfileManager.sLogger.i("Received " + simpleName + ", applying");
                    if (this.mCurrentProfile == null) {
                        break;
                    }
                    if (message2 != null) {
                        TaskManager.getInstance().execute(runnable, 10);
                        break;
                    }
                    DriverProfileManager.sLogger.i(simpleName + " with no preferences!");
                    break;
            }
        }
    }
    
    private String profileNameForId(final NavdyDeviceId navdyDeviceId) {
        String bluetoothAddress;
        if ((bluetoothAddress = navdyDeviceId.getBluetoothAddress()) == null) {
            bluetoothAddress = "DefaultProfile";
        }
        return GenericUtil.normalizeToFilename(bluetoothAddress);
    }
    
    private void refreshProfileImageIfNeeded(final String s) {
        final PhoneImageDownloader instance = PhoneImageDownloader.getInstance();
        final String hash = instance.hashFor("DriverImage", PhotoType.PHOTO_DRIVER_PROFILE);
        if (!Objects.equals(s, hash)) {
            DriverProfileManager.sLogger.d("asking for new photo - local:" + hash + " remote:" + s);
            instance.submitDownload("DriverImage", PhoneImageDownloader.Priority.NORMAL, PhotoType.PHOTO_DRIVER_PROFILE, null);
        }
    }
    
    private void requestPreferenceUpdates(final DriverProfile driverProfile) {
        DriverProfileManager.sLogger.v("requestPreferenceUpdates:" + driverProfile.getProfileName());
        this.sendEventToClient(new DriverProfilePreferencesRequest.Builder().serial_number(driverProfile.mDriverProfilePreferences.serial_number).build());
        DriverProfileManager.sLogger.v("requestPreferenceUpdates: nav:" + driverProfile.mNavigationPreferences.serial_number);
        this.sendEventToClient(new NavigationPreferencesRequest.Builder().serial_number(driverProfile.mNavigationPreferences.serial_number).build());
        DriverProfileManager.sLogger.v("requestPreferenceUpdates: notif:" + driverProfile.mNotificationPreferences.serial_number);
        this.sendEventToClient(new NotificationPreferencesRequest.Builder().serial_number(driverProfile.mNotificationPreferences.serial_number).build());
        DriverProfileManager.sLogger.v("requestPreferenceUpdates: input:" + driverProfile.mInputPreferences.serial_number);
        this.sendEventToClient(new InputPreferencesRequest.Builder().serial_number(driverProfile.mInputPreferences.serial_number).build());
    }
    
    private void requestUpdates(final DriverProfile driverProfile) {
        DriverProfileManager.sLogger.v("requestUpdates(canned-msgs) [" + driverProfile.mCannedMessages.serial_number + "]");
        this.sendEventToClient(new CannedMessagesRequest.Builder().serial_number(driverProfile.mCannedMessages.serial_number).build());
    }
    
    private void sendEventToClient(final Message message) {
        this.mBus.post(new RemoteEvent(message));
    }
    
    public DriverProfile createProfileForId(final NavdyDeviceId navdyDeviceId) {
        final String profileNameForId = this.profileNameForId(navdyDeviceId);
        try {
            final DriverProfile profileForId = DriverProfile.createProfileForId(profileNameForId, this.mProfileDirectoryName);
            this.mAllProfiles.put(profileNameForId, profileForId);
            return profileForId;
        }
        catch (IOException ex) {
            DriverProfileManager.sLogger.e("could not create new profile: " + ex);
            return null;
        }
    }
    
    public void disableTraffic() {
        this.mCurrentProfile.setTrafficEnabled(false);
        this.theDefaultProfile.setTrafficEnabled(false);
    }
    
    public void enableTraffic() {
        this.mCurrentProfile.setTrafficEnabled(true);
        this.theDefaultProfile.setTrafficEnabled(true);
    }
    
    public Locale getCurrentLocale() {
        return this.mCurrentProfile.getLocale();
    }
    
    public DriverProfile getCurrentProfile() {
        return this.mCurrentProfile;
    }
    
    public String getLastUserEmail() {
        return this.lastUserEmail;
    }
    
    public String getLastUserName() {
        return this.lastUserName;
    }
    
    public LocalPreferences getLocalPreferences() {
        return this.mCurrentProfile.getLocalPreferences();
    }
    
    public SharedPreferences getLocalPreferencesForCurrentDriverProfile(final Context context) {
        return this.getLocalPreferencesForDriverProfile(context, this.getCurrentProfile());
    }
    
    public SharedPreferences getLocalPreferencesForDriverProfile(final Context context, final DriverProfile driverProfile) {
        return context.getSharedPreferences(driverProfile.getProfileName() + "_preferences", 0);
    }
    
    public DriverProfile getProfileForId(final NavdyDeviceId navdyDeviceId) {
        return this.mAllProfiles.get(this.profileNameForId(navdyDeviceId));
    }
    
    public Collection<DriverProfile> getPublicProfiles() {
        return this.mPublicProfiles;
    }
    
    public DriverSessionPreferences getSessionPreferences() {
        return this.mSessionPrefs;
    }
    
    boolean isDefaultProfile(final DriverProfile driverProfile) {
        return driverProfile == this.theDefaultProfile;
    }
    
    public boolean isManualZoom() {
        final LocalPreferences localPreferences = this.getLocalPreferences();
        return localPreferences != null && Boolean.TRUE.equals(localPreferences.manualZoom);
    }
    
    public boolean isTrafficEnabled() {
        return this.mCurrentProfile.isTrafficEnabled();
    }
    
    public void loadProfileForId(final NavdyDeviceId navdyDeviceId) {
        TaskManager.getInstance().execute(new Runnable() {
            @Override
            public void run() {
                DriverProfileManager.sLogger.v("loading profile:" + navdyDeviceId);
                DriverProfile currentProfile;
                if ((currentProfile = DriverProfileManager.this.getProfileForId(navdyDeviceId)) == null) {
                    DriverProfileManager.sLogger.w("profile not loaded:" + navdyDeviceId);
                    currentProfile = DriverProfileManager.this.createProfileForId(navdyDeviceId);
                }
                DriverProfileManager.this.setCurrentProfile(currentProfile);
            }
        }, 10);
    }
    
    @Subscribe
    public void onCannedMessagesUpdate(final CannedMessagesUpdate cannedMessagesUpdate) {
        this.handleUpdateInBackground(cannedMessagesUpdate, cannedMessagesUpdate.status, cannedMessagesUpdate, new Runnable() {
            @Override
            public void run() {
                DriverProfileManager.this.mCurrentProfile.setCannedMessages(cannedMessagesUpdate);
            }
        });
    }
    
    @Subscribe
    public void onDeviceSyncRequired(final ConnectionHandler.DeviceSyncEvent deviceSyncEvent) {
        DriverProfileManager.sLogger.v("calling requestPreferenceUpdates");
        this.requestPreferenceUpdates(this.mCurrentProfile);
        DriverProfileManager.sLogger.v("calling requestUpdates");
        this.requestUpdates(this.mCurrentProfile);
    }
    
    @Subscribe
    public void onDisplaySpeakerPreferencesUpdate(final DisplaySpeakerPreferencesUpdate displaySpeakerPreferencesUpdate) {
        this.handleUpdateInBackground(displaySpeakerPreferencesUpdate, displaySpeakerPreferencesUpdate.status, displaySpeakerPreferencesUpdate.preferences, new Runnable() {
            @Override
            public void run() {
                DriverProfileManager.this.mCurrentProfile.setSpeakerPreferences(displaySpeakerPreferencesUpdate.preferences);
                DriverProfileManager.this.theDefaultProfile.setSpeakerPreferences(displaySpeakerPreferencesUpdate.preferences);
                AnalyticsSupport.recordPreferenceChange(DriverProfileManager.this.mCurrentProfile);
                DriverProfileManager.this.mBus.post(DriverProfileManager.this.mCurrentProfile.getSpeakerPreferences());
            }
        });
    }
    
    @Subscribe
    public void onDriverProfileChange(final DriverProfileChanged driverProfileChanged) {
        this.mBus.post(this.mCurrentProfile.getNavigationPreferences());
    }
    
    @Subscribe
    public void onDriverProfilePreferencesUpdate(final DriverProfilePreferencesUpdate driverProfilePreferencesUpdate) {
        if (this.mCurrentProfile != this.theDefaultProfile) {
            switch (driverProfilePreferencesUpdate.status) {
                default:
                    DriverProfileManager.sLogger.i("profile status was " + driverProfilePreferencesUpdate.status + ", which I didn't expect; ignoring.");
                    break;
                case REQUEST_VERSION_IS_CURRENT:
                    DriverProfileManager.sLogger.i("prefs are up to date!");
                    TaskManager.getInstance().execute(new Runnable() {
                        @Override
                        public void run() {
                            DriverProfileManager.this.changeLocale(false);
                            DriverProfileManager.this.mBus.post(new DriverProfileUpdated(DriverProfileUpdated.State.UP_TO_DATE));
                        }
                    }, 10);
                    break;
                case REQUEST_SUCCESS: {
                    DriverProfileManager.sLogger.i("Received profile update, applying");
                    if (this.mCurrentProfile == null) {
                        break;
                    }
                    final DriverProfilePreferences preferences = driverProfilePreferencesUpdate.preferences;
                    if (preferences != null) {
                        TaskManager.getInstance().execute(new Runnable() {
                            @Override
                            public void run() {
                                DriverProfileManager.this.refreshProfileImageIfNeeded(preferences.photo_checksum);
                            }
                        }, 10);
                        if (Wire.<Boolean>get(preferences.profile_is_public, DriverProfilePreferences.DEFAULT_PROFILE_IS_PUBLIC)) {
                            this.mPublicProfiles.add(this.mCurrentProfile);
                        }
                        else {
                            this.mPublicProfiles.remove(this.mCurrentProfile);
                        }
                        TaskManager.getInstance().execute(new Runnable() {
                            @Override
                            public void run() {
                                final DriverProfilePreferences access$400 = DriverProfileManager.this.findSupportedLocale(preferences);
                                final Locale locale = DriverProfileManager.this.mCurrentProfile.getLocale();
                                DriverProfileManager.this.mCurrentProfile.setDriverProfilePreferences(access$400);
                                DriverProfileManager.this.theDefaultProfile.setDriverProfilePreferences(access$400);
                                final Locale locale2 = DriverProfileManager.this.mCurrentProfile.getLocale();
                                final boolean b = !locale.equals(locale2);
                                DriverProfileManager.sLogger.i("[HUD-locale] changed[" + b + "] current[" + locale + "] new[" + locale2 + "]");
                                DriverProfileManager.this.changeLocale(b);
                                DriverProfileManager.this.mBus.post(new DriverProfileUpdated(DriverProfileUpdated.State.UPDATED));
                            }
                        }, 10);
                        break;
                    }
                    DriverProfileManager.sLogger.i("preferences update with no preferences!");
                    break;
                }
            }
        }
    }
    
    @Subscribe
    public void onInputPreferencesUpdate(final InputPreferencesUpdate inputPreferencesUpdate) {
        this.handleUpdateInBackground(inputPreferencesUpdate, inputPreferencesUpdate.status, inputPreferencesUpdate.preferences, new Runnable() {
            @Override
            public void run() {
                DriverProfileManager.this.mCurrentProfile.setInputPreferences(inputPreferencesUpdate.preferences);
                DriverProfileManager.this.theDefaultProfile.setInputPreferences(inputPreferencesUpdate.preferences);
                AnalyticsSupport.recordPreferenceChange(DriverProfileManager.this.mCurrentProfile);
                DriverProfileManager.this.mBus.post(DriverProfileManager.this.mCurrentProfile.getInputPreferences());
            }
        });
    }
    
    @Subscribe
    public void onNavigationPreferencesUpdate(final NavigationPreferencesUpdate navigationPreferencesUpdate) {
        this.handleUpdateInBackground(navigationPreferencesUpdate, navigationPreferencesUpdate.status, navigationPreferencesUpdate.preferences, new Runnable() {
            @Override
            public void run() {
                DriverProfileManager.this.mCurrentProfile.setNavigationPreferences(navigationPreferencesUpdate.preferences);
                DriverProfileManager.this.theDefaultProfile.setNavigationPreferences(navigationPreferencesUpdate.preferences);
                final NavigationPreferences navigationPreferences = DriverProfileManager.this.mCurrentProfile.getNavigationPreferences();
                AnalyticsSupport.recordNavigationPreferenceChange(DriverProfileManager.this.mCurrentProfile);
                DriverProfileManager.this.mSessionPrefs.getNavigationSessionPreference().setDefault(navigationPreferences);
                DriverProfileManager.this.mBus.post(navigationPreferences);
            }
        });
    }
    
    @Subscribe
    public void onNotificationPreferencesUpdate(final NotificationPreferencesUpdate notificationPreferencesUpdate) {
        this.handleUpdateInBackground(notificationPreferencesUpdate, notificationPreferencesUpdate.status, notificationPreferencesUpdate.preferences, new Runnable() {
            @Override
            public void run() {
                DriverProfileManager.this.mCurrentProfile.setNotificationPreferences(notificationPreferencesUpdate.preferences);
                DriverProfileManager.this.theDefaultProfile.setNotificationPreferences(notificationPreferencesUpdate.preferences);
                AnalyticsSupport.recordPreferenceChange(DriverProfileManager.this.mCurrentProfile);
                DriverProfileManager.this.mBus.post(DriverProfileManager.this.mCurrentProfile.getNotificationPreferences());
            }
        });
    }
    
    @Subscribe
    public void onPhotoDownload(final PhoneImageDownloader.PhotoDownloadStatus photoDownloadStatus) {
        if (photoDownloadStatus.photoType == PhotoType.PHOTO_DRIVER_PROFILE && !photoDownloadStatus.alreadyDownloaded && this.mCurrentProfile != this.theDefaultProfile) {
            if (photoDownloadStatus.success) {
                DriverProfileManager.sLogger.d("applying new photo");
            }
            else {
                DriverProfileManager.sLogger.i("driver photo not downloaded");
            }
        }
    }
    
    public void setCurrentProfile(final DriverProfile driverProfile) {
        DriverProfileManager.sLogger.i("setting current profile to " + driverProfile);
        DriverProfile theDefaultProfile = driverProfile;
        if (driverProfile == null) {
            theDefaultProfile = this.theDefaultProfile;
        }
        if (this.mCurrentProfile != theDefaultProfile) {
            this.mCurrentProfile = theDefaultProfile;
            this.mSessionPrefs.setDefault(this.mCurrentProfile, this.isDefaultProfile(this.mCurrentProfile));
            this.theDefaultProfile.setTrafficEnabled(this.mCurrentProfile.isTrafficEnabled());
            this.copyCurrentProfileToDefault();
            this.mBus.post(DriverProfileManager.DRIVER_PROFILE_CHANGED);
        }
        this.lastUserName = this.mCurrentProfile.getDriverName();
        this.lastUserEmail = this.mCurrentProfile.getDriverEmail();
        DriverProfileManager.sLogger.v("lastUserEmail:" + this.lastUserEmail);
    }
    
    public void updateLocalPreferences(final LocalPreferences localPreferences) {
        if (this.mCurrentProfile != null) {
            TaskManager.getInstance().execute(new Runnable() {
                @Override
                public void run() {
                    DriverProfileManager.this.mCurrentProfile.setLocalPreferences(localPreferences);
                    DriverProfileManager.this.theDefaultProfile.setLocalPreferences(localPreferences);
                    AnalyticsSupport.recordPreferenceChange(DriverProfileManager.this.mCurrentProfile);
                    DriverProfileManager.this.mBus.post(localPreferences);
                }
            }, 10);
        }
    }
}
