package com.navdy.hud.app.maps.here;

import java.io.ObjectInputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import com.here.android.mpa.guidance.TrafficNotificationInfo;
import com.here.android.mpa.guidance.TrafficNotification;
import com.here.android.mpa.mapping.TrafficEvent;
import com.navdy.hud.app.maps.MapEvents;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import com.navdy.service.library.task.TaskManager;
import com.navdy.service.library.events.navigation.NavigationSessionState;
import java.util.EnumSet;
import com.here.android.mpa.common.GeoCoordinate;
import com.here.android.mpa.routing.Signpost;
import java.util.UUID;
import java.io.InputStream;
import java.io.FileInputStream;
import java.io.Closeable;
import com.navdy.service.library.events.navigation.NavigationRouteRequest;
import com.here.android.mpa.routing.RouteTta;
import com.navdy.service.library.device.NavdyDeviceId;
import com.here.android.mpa.common.PositioningManager;
import com.navdy.hud.app.ui.component.homescreen.HomeScreenView;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import java.util.Collection;
import java.util.ArrayList;
import com.here.android.mpa.routing.RouteElement;
import com.here.android.mpa.common.RoadElement;
import android.graphics.Bitmap;
import com.here.android.mpa.common.Image;
import java.util.Iterator;
import java.io.OutputStream;
import android.graphics.Bitmap;
import java.io.ByteArrayOutputStream;
import com.navdy.service.library.util.IOUtils;
import java.io.File;
import com.navdy.hud.app.storage.PathManager;
import com.navdy.hud.app.util.GenericUtil;
import com.here.android.mpa.routing.Route;
import android.text.SpannableStringBuilder;
import java.util.Date;
import java.util.List;
import android.text.TextUtils;
import com.here.android.mpa.routing.Maneuver;
import android.content.res.Resources;
import android.content.Context;
import com.navdy.hud.app.HudApplication;
import com.navdy.service.library.events.Ext_NavdyEvent;
import java.util.Locale;
import java.util.concurrent.TimeUnit;
import com.squareup.wire.Wire;
import android.text.style.TextAppearanceSpan;
import java.util.HashSet;
import com.navdy.service.library.log.Logger;

public class HereMapUtil
{
    private static final String ACTIVE_GAS_ROUTE_INFO_REQUEST = "gasrouterequest.bin";
    private static final String ACTIVE_ROUTE_INFO_DEVICE_ID = "routerequest.device";
    private static final String ACTIVE_ROUTE_INFO_REQUEST = "routerequest.bin";
    private static final int EQUALITY_DISTANCE_VARIATION = 500;
    private static final long INVALID_ETA_TIME_DIFF = 360000L;
    public static final double MILES_TO_METERS = 1609.34;
    public static final int SHORT_DISTANCE_IN_METERS = 20;
    private static final long STALE_ROUTE_TIME;
    public static final String TBT_ISO3_LANG_CODE;
    private static final boolean VERBOSE = false;
    private static final String day;
    private static final String hr;
    private static String lastKnownDeviceId;
    private static final String min;
    private static final String min_short;
    private static final Logger sLogger;
    private static final StringBuilder signPostBuilder;
    private static final StringBuilder signPostBuilder2;
    private static final HashSet<String> signPostSet;
    private static final TextAppearanceSpan sp_20_b_1;
    private static final TextAppearanceSpan sp_20_b_2;
    private static final TextAppearanceSpan sp_24_1;
    private static final TextAppearanceSpan sp_24_2;
    private static final TextAppearanceSpan sp_26_1;
    private static final TextAppearanceSpan sp_26_2;
    private static final StringBuilder stringBuilder;
    private static final StringBuilder trafficEventBuilder;
    private static final Wire wire;
    
    static {
        sLogger = new Logger(HereMapUtil.class);
        STALE_ROUTE_TIME = TimeUnit.HOURS.toMillis(3L);
        stringBuilder = new StringBuilder();
        TBT_ISO3_LANG_CODE = new Locale("en", "US").getISO3Language().toUpperCase();
        signPostBuilder = new StringBuilder();
        signPostBuilder2 = new StringBuilder();
        signPostSet = new HashSet<String>();
        trafficEventBuilder = new StringBuilder();
        wire = new Wire((Class<?>[])new Class[] { Ext_NavdyEvent.class });
        final Context appContext = HudApplication.getAppContext();
        final Resources resources = appContext.getResources();
        sp_20_b_1 = new TextAppearanceSpan(appContext, R.style.route_duration_20_b);
        sp_20_b_2 = new TextAppearanceSpan(appContext, R.style.route_duration_20_b);
        sp_24_1 = new TextAppearanceSpan(appContext, R.style.route_duration_24);
        sp_24_2 = new TextAppearanceSpan(appContext, R.style.route_duration_24);
        sp_26_1 = new TextAppearanceSpan(appContext, R.style.route_duration_26);
        sp_26_2 = new TextAppearanceSpan(appContext, R.style.route_duration_26);
        min = resources.getString(R.string.route_time_min);
        min_short = resources.getString(R.string.route_time_min_short);
        hr = resources.getString(R.string.route_time_hr);
        day = resources.getString(R.string.route_time_day);
    }
    
    public static boolean areManeuverEqual(final Maneuver maneuver, final Maneuver maneuver2) {
        boolean equals;
        if (maneuver == null || maneuver2 == null) {
            equals = false;
        }
        else {
            final boolean b = equals = maneuver.getRoadElements().equals(maneuver2.getRoadElements());
            if (!b) {
                equals = b;
                boolean b2 = b;
                try {
                    if (maneuver.getTurn() == maneuver2.getTurn()) {
                        equals = b;
                        b2 = b;
                        if (TextUtils.equals((CharSequence)maneuver.getNextRoadNumber(), (CharSequence)maneuver2.getNextRoadNumber())) {
                            equals = b;
                            b2 = b;
                            if (TextUtils.equals((CharSequence)maneuver.getNextRoadName(), (CharSequence)maneuver2.getNextRoadName())) {
                                equals = b;
                                b2 = b;
                                if (TextUtils.equals((CharSequence)maneuver.getRoadNumber(), (CharSequence)maneuver2.getRoadNumber())) {
                                    equals = b;
                                    b2 = b;
                                    if (TextUtils.equals((CharSequence)maneuver.getRoadName(), (CharSequence)maneuver2.getRoadName())) {
                                        equals = b;
                                        b2 = b;
                                        if (maneuver.getAction() == maneuver2.getAction()) {
                                            equals = b;
                                            b2 = b;
                                            if (Math.abs(maneuver.getDistanceToNextManeuver() - maneuver2.getDistanceToNextManeuver()) <= 500) {
                                                final boolean b3 = true;
                                                equals = true;
                                                b2 = b3;
                                                if (HereMapUtil.sLogger.isLoggable(2)) {
                                                    b2 = b3;
                                                    final int abs = Math.abs(maneuver.getDistanceToNextManeuver() - maneuver2.getDistanceToNextManeuver());
                                                    b2 = b3;
                                                    final Logger sLogger = HereMapUtil.sLogger;
                                                    b2 = b3;
                                                    b2 = b3;
                                                    final StringBuilder sb = new StringBuilder();
                                                    b2 = b3;
                                                    sLogger.v(sb.append("areManeuverEqual: distance diff=").append(abs).append(" allowance=").append(500).toString());
                                                    equals = equals;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Throwable t) {
                    HereMapUtil.sLogger.e(t);
                    equals = b2;
                }
            }
        }
        return equals;
    }
    
    static boolean areManeuversEqual(final List<Maneuver> list, int n, final List<Maneuver> list2, int n2, final List<Maneuver> list3) {
        final boolean b = false;
        int n3 = list.size() - n;
        final int n4 = list2.size() - n2;
        int n5;
        Maneuver maneuver;
        Maneuver maneuver2;
        boolean b2;
        for (n5 = n, n = n2, n2 = n4; n3 > 0 && n2 > 0; --n3, --n2, ++n, ++n5) {
            maneuver = list.get(n5);
            maneuver2 = list2.get(n);
            if (!areManeuverEqual(maneuver, maneuver2)) {
                HereMapUtil.sLogger.v("maneuver are NOT equal");
                printManeuverDetails(maneuver, "[maneuver-1]");
                printManeuverDetails(maneuver2, "[maneuver-2]");
                b2 = b;
                if (list3 != null) {
                    list3.add(maneuver);
                    b2 = b;
                }
                return b2;
            }
            if (HereMapUtil.sLogger.isLoggable(2)) {
                HereMapUtil.sLogger.v("maneuver are equal");
                printManeuverDetails(maneuver, "[maneuver-1]");
                printManeuverDetails(maneuver2, "[maneuver-2]");
            }
        }
        if (n3 > 0 || n2 > 0) {
            if (n3 > 0 && list3 != null) {
                list3.add(list.get(n5));
            }
            b2 = b;
            return b2;
        }
        b2 = true;
        return b2;
    }
    
    public static String concatText(String string, final String s, final boolean b, boolean b2) {
        final StringBuilder stringBuilder = HereMapUtil.stringBuilder;
        synchronized (stringBuilder) {
            HereMapUtil.stringBuilder.setLength(0);
            boolean b3 = false;
            if (!TextUtils.isEmpty((CharSequence)string)) {
                HereMapUtil.stringBuilder.append(string);
                b3 = true;
            }
            else {
                b2 = false;
            }
            if (!TextUtils.isEmpty((CharSequence)s) && !b2) {
                int n;
                if (b3) {
                    HereMapUtil.stringBuilder.append(" ");
                    n = (b ? 1 : 0);
                    if (b) {
                        HereMapUtil.stringBuilder.append("(");
                        n = (b ? 1 : 0);
                    }
                }
                else {
                    n = 0;
                }
                HereMapUtil.stringBuilder.append(s);
                if (n != 0) {
                    HereMapUtil.stringBuilder.append(")");
                }
            }
            string = HereMapUtil.stringBuilder.toString();
            return string;
        }
    }
    
    public static String convertDateToEta(final Date date) {
        String s;
        if (!isValidEtaDate(date)) {
            s = null;
        }
        else {
            final String s2 = null;
            final long n = date.getTime() - System.currentTimeMillis();
            s = s2;
            if (n > 0L) {
                final long minutes = TimeUnit.MILLISECONDS.toMinutes(n);
                if (minutes < 60L) {
                    s = String.valueOf(minutes) + " " + HereMapUtil.min;
                }
                else {
                    final long hours = TimeUnit.MILLISECONDS.toHours(n);
                    final long minutes2 = TimeUnit.MILLISECONDS.toMinutes(n - TimeUnit.HOURS.toMillis(hours));
                    if (hours < 24L) {
                        s = String.valueOf(hours) + " " + HereMapUtil.hr + " " + String.valueOf(minutes2) + " " + HereMapUtil.min_short;
                    }
                    else {
                        final long days = TimeUnit.MILLISECONDS.toDays(n);
                        s = String.valueOf(days) + " " + HereMapUtil.day + " " + String.valueOf(TimeUnit.MILLISECONDS.toHours(n - TimeUnit.DAYS.toMillis(days))) + " " + HereMapUtil.hr;
                    }
                }
            }
        }
        return s;
    }
    
    public static int findManeuverIndex(final List<Maneuver> list, final Maneuver maneuver) {
        int n;
        if (list == null || maneuver == null) {
            n = -1;
        }
        else {
            for (int size = list.size(), i = 0; i < size; ++i) {
                n = i;
                if (areManeuverEqual(list.get(i), maneuver)) {
                    return n;
                }
            }
            n = -1;
        }
        return n;
    }
    
    private static void formatString(final SpannableStringBuilder spannableStringBuilder, final String s, final TextAppearanceSpan textAppearanceSpan, final String s2, final TextAppearanceSpan textAppearanceSpan2, final String s3, final TextAppearanceSpan textAppearanceSpan3, final String s4, final TextAppearanceSpan textAppearanceSpan4, final boolean b) {
        if (s != null) {
            spannableStringBuilder.append((CharSequence)s);
            spannableStringBuilder.setSpan(textAppearanceSpan, 0, spannableStringBuilder.length(), 33);
        }
        if (s2 != null) {
            if (b) {
                spannableStringBuilder.append((CharSequence)" ");
            }
            final int length = spannableStringBuilder.length();
            spannableStringBuilder.append((CharSequence)s2);
            spannableStringBuilder.setSpan(textAppearanceSpan2, length, spannableStringBuilder.length(), 33);
        }
        if (s3 != null) {
            final int length2 = spannableStringBuilder.length();
            spannableStringBuilder.append((CharSequence)" ");
            spannableStringBuilder.append((CharSequence)s3);
            spannableStringBuilder.setSpan(textAppearanceSpan3, length2, spannableStringBuilder.length(), 33);
        }
        if (s4 != null) {
            if (b) {
                spannableStringBuilder.append((CharSequence)" ");
            }
            final int length3 = spannableStringBuilder.length();
            spannableStringBuilder.append((CharSequence)s4);
            spannableStringBuilder.setSpan(textAppearanceSpan4, length3, spannableStringBuilder.length(), 33);
        }
    }
    
    public static void generateRouteIcons(String string, final String s, final Route route) {
        GenericUtil.checkNotOnMainThread();
        int n = 0;
        Label_0388: {
            try {
                final Context appContext = HudApplication.getAppContext();
                string = PathManager.getInstance().getMapsPartitionPath() + File.separator + "routeicons" + File.separator + string + "_" + GenericUtil.normalizeToFilename(s);
                HereMapUtil.sLogger.v("generateRouteIcons: " + string);
                final File file = new File(string);
                if (file.exists()) {
                    IOUtils.deleteDirectory(appContext, file);
                }
                file.mkdirs();
                final List<Maneuver> maneuvers = route.getManeuvers();
                n = 0;
                for (final Maneuver maneuver : maneuvers) {
                    final Image nextRoadImage = maneuver.getNextRoadImage();
                    if (nextRoadImage != null) {
                        final int n2 = (int)nextRoadImage.getWidth();
                        final int n3 = (int)nextRoadImage.getHeight();
                        HereMapUtil.sLogger.v("generateRouteIcons: w=" + n2 + " h=" + n3);
                        final Bitmap bitmap = nextRoadImage.getBitmap(n2 * 3, n3 * 3);
                        if (bitmap == null) {
                            continue;
                        }
                        final String string2 = "man_" + GenericUtil.normalizeToFilename(getNextRoadName(maneuver)) + "_" + n2 + "_" + n3 + ".png";
                        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                        bitmap.compress(Bitmap$CompressFormat.PNG, 100, (OutputStream)byteArrayOutputStream);
                        IOUtils.copyFile(string + File.separator + string2, byteArrayOutputStream.toByteArray());
                        ++n;
                        try {
                            HereMapUtil.sLogger.v("icon generated");
                        }
                        catch (Throwable t) {
                            HereMapUtil.sLogger.e(t);
                        }
                    }
                }
                break Label_0388;
            }
            catch (Throwable t2) {}
            return;
        }
        HereMapUtil.sLogger.v("total icons generated:" + n);
    }
    
    public static List<RouteElement> getAheadRouteElements(final Route route, final RoadElement roadElement, final Maneuver maneuver, final Maneuver maneuver2) {
        Object o;
        if (route == null || roadElement == null || maneuver2 == null) {
            HereMapUtil.sLogger.i("getAheadRouteElements:invalid arg");
            o = null;
        }
        else {
            final List<Maneuver> maneuvers = route.getManeuvers();
            final int maneuverIndex = findManeuverIndex(maneuvers, maneuver2);
            if (maneuverIndex == -1) {
                HereMapUtil.sLogger.i("getAheadRouteElements:maneuver index not found");
                o = null;
            }
            else {
                HereMapUtil.sLogger.i("getAheadRouteElements:found maneuver index:" + maneuverIndex + " total:" + maneuvers.size());
                final ArrayList<Object> list = new ArrayList<Object>();
                if (maneuver != null) {
                    final List<RouteElement> routeElements = maneuver.getRouteElements();
                    if (routeElements != null && routeElements.size() > 0) {
                        final int size = routeElements.size();
                        int n = 0;
                        int n2;
                        for (int i = 0; i < size; ++i, n = n2) {
                            final RouteElement routeElement = routeElements.get(i);
                            if (n == 0) {
                                final RoadElement roadElement2 = routeElement.getRoadElement();
                                n2 = n;
                                if (roadElement2 != null) {
                                    n2 = n;
                                    if (roadElement2.equals(roadElement)) {
                                        n2 = 1;
                                    }
                                }
                            }
                            else {
                                list.add(routeElement);
                                n2 = n;
                            }
                        }
                    }
                }
                final int size2 = maneuvers.size();
                int n3 = maneuverIndex;
                while (true) {
                    o = list;
                    if (n3 >= size2) {
                        break;
                    }
                    list.addAll(maneuvers.get(n3).getRouteElements());
                    ++n3;
                }
            }
        }
        return (List<RouteElement>)o;
    }
    
    public static String getAllRoadNames(final List<Maneuver> list, int i) {
        String string;
        if (list == null || i < 0) {
            string = "";
        }
        else {
            final int size = list.size();
            if (i >= size) {
                string = "";
            }
            else {
                final StringBuilder sb = new StringBuilder();
                while (i < size) {
                    sb.append("[");
                    sb.append(getRoadName(list.get(i)));
                    sb.append("]");
                    sb.append(" ");
                    ++i;
                }
                string = sb.toString();
            }
        }
        return string;
    }
    
    public static String getCurrentEtaString() {
        final String s = null;
        String lastEtaDate;
        try {
            if (!HereMapsManager.getInstance().isInitialized()) {
                lastEtaDate = s;
            }
            else {
                lastEtaDate = s;
                if (HereNavigationManager.getInstance().isNavigationModeOn()) {
                    final HomeScreenView homescreenView = RemoteDeviceManager.getInstance().getUiStateManager().getHomescreenView();
                    lastEtaDate = s;
                    if (homescreenView != null) {
                        lastEtaDate = homescreenView.getEtaView().getLastEtaDate();
                    }
                }
            }
            return lastEtaDate;
        }
        catch (Throwable t) {
            HereMapUtil.sLogger.e(t);
            lastEtaDate = s;
            return lastEtaDate;
        }
        return lastEtaDate;
    }
    
    public static String getCurrentEtaStringWithDestination() {
        try {
            String s;
            if (!HereMapsManager.getInstance().isInitialized()) {
                s = null;
            }
            else {
                final HereNavigationManager instance = HereNavigationManager.getInstance();
                if (!instance.isNavigationModeOn()) {
                    s = null;
                }
                else {
                    String s2;
                    if (TextUtils.isEmpty((CharSequence)(s2 = instance.getDestinationLabel())) && TextUtils.isEmpty((CharSequence)(s2 = instance.getDestinationStreetAddress()))) {
                        s2 = null;
                    }
                    final Resources resources = HudApplication.getAppContext().getResources();
                    if (HereNavigationManager.getInstance().hasArrived()) {
                        if (s2 != null) {
                            s = resources.getString(R.string.mm_active_trip_arrived, new Object[] { s2 });
                        }
                        else {
                            s = resources.getString(R.string.mm_active_trip_arrived_no_label);
                        }
                    }
                    else {
                        final String currentEtaString = getCurrentEtaString();
                        if (currentEtaString == null) {
                            s = null;
                        }
                        else {
                            s = currentEtaString;
                            if (s2 != null) {
                                s = HudApplication.getAppContext().getString(R.string.mm_active_trip_eta, new Object[] { currentEtaString, s2 });
                            }
                        }
                    }
                }
            }
            return s;
        }
        catch (Throwable t) {
            HereMapUtil.sLogger.e(t);
            return null;
        }
    }
    
    public static RoadElement getCurrentRoadElement() {
        final PositioningManager positioningManager = HereMapsManager.getInstance().getPositioningManager();
        RoadElement roadElement;
        if (positioningManager == null) {
            roadElement = null;
        }
        else {
            roadElement = positioningManager.getRoadElement();
        }
        return roadElement;
    }
    
    public static String getCurrentRoadName() {
        String roadName = null;
        final PositioningManager positioningManager = HereMapsManager.getInstance().getPositioningManager();
        if (positioningManager != null) {
            final RoadElement roadElement = positioningManager.getRoadElement();
            if (roadElement != null) {
                roadName = getRoadName(roadElement);
            }
        }
        return roadName;
    }
    
    public static float getCurrentSpeedLimit() {
        final RoadElement currentRoadElement = getCurrentRoadElement();
        float speedLimit;
        if (currentRoadElement != null) {
            speedLimit = currentRoadElement.getSpeedLimit();
        }
        else {
            speedLimit = 0.0f;
        }
        return speedLimit;
    }
    
    static List<Maneuver> getDifferentManeuver(final Route route, final Route route2, int i, final List<Maneuver> list) {
        final ArrayList<Maneuver> list2 = new ArrayList<Maneuver>();
        final List<Maneuver> maneuvers = route.getManeuvers();
        final List<Maneuver> maneuvers2 = route2.getManeuvers();
        final int size = maneuvers.size();
        int size2 = maneuvers2.size();
        if (size > size2) {
            --size2;
        }
        else {
            size2 = size - 1;
        }
        final int n = 0;
        int n2 = i;
        i = n;
        while (i <= size2) {
            final Maneuver maneuver = maneuvers.get(i);
            final Maneuver maneuver2 = maneuvers2.get(i);
            if (HereMapUtil.sLogger.isLoggable(2)) {
                printManeuverDetails(maneuver, "getDifferentManeuver-1");
                printManeuverDetails(maneuver2, "getDifferentManeuver-2");
            }
            int n3 = n2;
            if (!areManeuverEqual(maneuver, maneuver2)) {
                final String nextRoadName = getNextRoadName(maneuver);
                if (TextUtils.isEmpty((CharSequence)nextRoadName)) {
                    ++i;
                    continue;
                }
                if (HereMapUtil.sLogger.isLoggable(2)) {
                    HereMapUtil.sLogger.v("getDifferentManeuver:diff:" + nextRoadName);
                }
                list2.add(maneuver);
                list.add(maneuver2);
                n3 = --n2;
                if (n2 == 0) {
                    break;
                }
            }
            ++i;
            n2 = n3;
        }
        return list2;
    }
    
    public static long getDistanceToRoadElement(final RoadElement roadElement, final RoadElement roadElement2, final List<Maneuver> list) {
        long n;
        if (roadElement == null || roadElement2 == null || list == null) {
            n = -1L;
        }
        else {
            long n2 = -1L;
            int n3 = 0;
            for (int size = list.size(), i = 0; i < size; ++i) {
                final List<RoadElement> roadElements = list.get(i).getRoadElements();
                for (int size2 = roadElements.size(), j = 0; j < size2; ++j) {
                    final RoadElement roadElement3 = roadElements.get(j);
                    if (n3 == 0) {
                        if (roadElement3.equals(roadElement)) {
                            n3 = 1;
                            n2 = (long)roadElement3.getGeometryLength();
                        }
                    }
                    else {
                        n = n2;
                        if (roadElement3.equals(roadElement2)) {
                            return n;
                        }
                        n2 += (long)roadElement3.getGeometryLength();
                    }
                }
            }
            n = -1L;
        }
        return n;
    }
    
    public static SpannableStringBuilder getEtaString(final Route route, final String s, final boolean b) {
        final SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder();
        if (route != null) {
            final Date routeTtaDate = getRouteTtaDate(route);
            long n = 0L;
            if (routeTtaDate != null) {
                n = routeTtaDate.getTime() - System.currentTimeMillis();
            }
            else {
                HereMapUtil.sLogger.i("tta date is null for route:" + s);
            }
            if (n > 0L) {
                final long minutes = TimeUnit.MILLISECONDS.toMinutes(n);
                if (minutes < 60L) {
                    formatString(spannableStringBuilder, String.valueOf(minutes), HereMapUtil.sp_26_1, HereMapUtil.min, HereMapUtil.sp_20_b_1, null, null, null, null, true);
                }
                else {
                    final long hours = TimeUnit.MILLISECONDS.toHours(n);
                    final long minutes2 = TimeUnit.MILLISECONDS.toMinutes(n - TimeUnit.HOURS.toMillis(hours));
                    if (hours < 10L) {
                        formatString(spannableStringBuilder, String.valueOf(hours), HereMapUtil.sp_26_1, HereMapUtil.hr, HereMapUtil.sp_20_b_1, String.valueOf(minutes2), HereMapUtil.sp_26_2, HereMapUtil.min_short, HereMapUtil.sp_20_b_2, b);
                    }
                    else if (hours < 24L) {
                        formatString(spannableStringBuilder, String.valueOf(hours), HereMapUtil.sp_24_1, HereMapUtil.hr, HereMapUtil.sp_20_b_1, String.valueOf(minutes2), HereMapUtil.sp_24_2, HereMapUtil.min_short, HereMapUtil.sp_20_b_2, b);
                    }
                    else {
                        final long days = TimeUnit.MILLISECONDS.toDays(n);
                        final long hours2 = TimeUnit.MILLISECONDS.toHours(n - TimeUnit.DAYS.toMillis(days));
                        if (days == 1L) {
                            formatString(spannableStringBuilder, String.valueOf(days), HereMapUtil.sp_26_1, HereMapUtil.day, HereMapUtil.sp_20_b_1, String.valueOf(hours2), HereMapUtil.sp_26_2, HereMapUtil.hr, HereMapUtil.sp_20_b_2, b);
                        }
                        else {
                            formatString(spannableStringBuilder, String.valueOf(days), HereMapUtil.sp_24_1, HereMapUtil.day, HereMapUtil.sp_20_b_1, String.valueOf(hours2), HereMapUtil.sp_24_2, HereMapUtil.hr, HereMapUtil.sp_20_b_2, b);
                        }
                    }
                }
            }
        }
        return spannableStringBuilder;
    }
    
    public static Maneuver getLastEqualManeuver(final Route route, final Route route2) {
        final List<Maneuver> maneuvers = route.getManeuvers();
        final List<Maneuver> maneuvers2 = route2.getManeuvers();
        int n;
        if (maneuvers.size() < maneuvers2.size()) {
            n = maneuvers.size() - 1;
        }
        else {
            n = maneuvers2.size() - 1;
        }
        final Maneuver maneuver = null;
        int n2 = 0;
        Maneuver maneuver2;
        while (true) {
            maneuver2 = maneuver;
            if (n2 > n) {
                break;
            }
            if (areManeuverEqual(maneuvers.get(n2), maneuvers2.get(n2)) && n2 > 0) {
                maneuver2 = maneuvers.get(n2 - 1);
                break;
            }
            ++n2;
        }
        return maneuver2;
    }
    
    public static NavdyDeviceId getLastKnownDeviceId() {
        if (HereMapUtil.lastKnownDeviceId == null) {
            return null;
        }
        try {
            final NavdyDeviceId navdyDeviceId = new NavdyDeviceId(HereMapUtil.lastKnownDeviceId);
            HereMapUtil.lastKnownDeviceId = null;
            return navdyDeviceId;
        }
        catch (Throwable t) {
            HereMapUtil.lastKnownDeviceId = null;
            HereMapUtil.sLogger.e(t);
            return null;
        }
        return null;
    }
    
    public static Maneuver getLastManeuver(final List<Maneuver> list) {
        Maneuver maneuver;
        if (list == null || list.size() <= 1) {
            maneuver = null;
        }
        else {
            maneuver = list.get(list.size() - 2);
        }
        return maneuver;
    }
    
    public static long getManeuverDriveTime(final Maneuver maneuver, final LongContainer longContainer) {
        long n;
        if (maneuver == null) {
            n = 0L;
        }
        else {
            final List<RoadElement> roadElements = maneuver.getRoadElements();
            if (roadElements == null || roadElements.size() == 0) {
                n = 0L;
            }
            else {
                long n2 = 0L;
                if (longContainer != null) {
                    longContainer.val = 0L;
                }
                final Iterator<RoadElement> iterator = roadElements.iterator();
                while (true) {
                    n = n2;
                    if (!iterator.hasNext()) {
                        break;
                    }
                    final RoadElement roadElement = iterator.next();
                    if (roadElement == null || roadElement.getDefaultSpeed() <= 0.0f) {
                        continue;
                    }
                    final double geometryLength = roadElement.getGeometryLength();
                    final long n3 = n2 += Math.round(geometryLength / roadElement.getDefaultSpeed());
                    if (longContainer == null) {
                        continue;
                    }
                    longContainer.val += (long)geometryLength;
                    n2 = n3;
                }
            }
        }
        return n;
    }
    
    public static String getNextRoadName(final Maneuver maneuver) {
        return concatText(maneuver.getNextRoadNumber(), maneuver.getNextRoadName(), true, isCurrentRoadHighway(maneuver));
    }
    
    public static String getRoadName(final RoadElement roadElement) {
        String concatText;
        if (roadElement == null) {
            concatText = null;
        }
        else {
            concatText = concatText(roadElement.getRouteName(), roadElement.getRoadName(), true, isCurrentRoadHighway(roadElement));
        }
        return concatText;
    }
    
    public static String getRoadName(final Maneuver maneuver) {
        return concatText(maneuver.getRoadNumber(), maneuver.getRoadName(), true, isCurrentRoadHighway(maneuver));
    }
    
    public static String getRoadName(final List<RoadElement> list) {
        String s;
        if (list == null || list.size() == 0) {
            s = "";
        }
        else {
            for (final RoadElement roadElement : list) {
                if (!TextUtils.isEmpty((CharSequence)(s = roadElement.getRoadName()))) {
                    return s;
                }
                s = roadElement.getRouteName();
                if (!TextUtils.isEmpty((CharSequence)s)) {
                    return s;
                }
            }
            s = "";
        }
        return s;
    }
    
    public static Date getRouteTtaDate(final Route route) {
        final Date date = null;
        Date date2;
        if (route == null) {
            date2 = date;
        }
        else {
            try {
                final RouteTta tta = route.getTta(Route.TrafficPenaltyMode.OPTIMAL, 268435455);
                RouteTta routeTta;
                if ((routeTta = tta) != null) {
                    routeTta = tta;
                    if (tta.getDuration() == 0) {
                        routeTta = null;
                    }
                }
                RouteTta tta2;
                if ((tta2 = routeTta) == null) {
                    tta2 = route.getTta(Route.TrafficPenaltyMode.DISABLED, 268435455);
                }
                date2 = date;
                if (tta2 != null) {
                    date2 = new Date(System.currentTimeMillis() + TimeUnit.SECONDS.toMillis(tta2.getDuration()));
                }
            }
            catch (Throwable t) {
                HereMapUtil.sLogger.e(t);
                date2 = date;
            }
        }
        return date2;
    }
    
    public static SavedRouteData getSavedRouteData(final Logger logger) {
        synchronized (HereMapUtil.class) {
            GenericUtil.checkNotOnMainThread();
            final NavigationRouteRequest savedRouteRequest = getSavedRouteRequest(logger);
            return new SavedRouteData(savedRouteRequest, savedRouteRequest != null && savedRouteRequest.routeAttributes.contains(NavigationRouteRequest.RouteAttribute.ROUTE_ATTRIBUTE_GAS));
        }
    }
    
    private static NavigationRouteRequest getSavedRouteRequest(Logger o) {
        while (true) {
            GenericUtil.checkNotOnMainThread();
            ((Logger)o).v("getSavedRouteRequest");
            while (true) {
                String activeRouteInfoDir = null;
                File file = null;
                Label_0410: {
                    Label_0289: {
                        Object convertFileToString;
                        Throwable t;
                        try {
                            HereMapUtil.lastKnownDeviceId = null;
                            activeRouteInfoDir = PathManager.getInstance().getActiveRouteInfoDir();
                            file = new File(activeRouteInfoDir + File.separator + "gasrouterequest.bin");
                            int n2;
                            final int n = n2 = 0;
                            if (file.exists()) {
                                if (isRouteFileStale(file)) {
                                    ((Logger)o).v("getSavedRouteRequest: gas route is stale file-time =" + file.lastModified() + " now=" + System.currentTimeMillis() + " deleted=" + IOUtils.deleteFile(HudApplication.getAppContext(), file.getAbsolutePath()));
                                    n2 = n;
                                }
                                else {
                                    n2 = 1;
                                    ((Logger)o).v("getSavedRouteRequest: gas route is good, file-time =" + file.lastModified() + " now=" + System.currentTimeMillis());
                                }
                            }
                            if (n2 != 0) {
                                break Label_0410;
                            }
                            ((Logger)o).v("getSavedRouteRequest: no saved gas route");
                            file = new File(activeRouteInfoDir + File.separator + "routerequest.bin");
                            if (!file.exists()) {
                                ((Logger)o).v("getSavedRouteRequest: no saved route");
                                o = null;
                                return (NavigationRouteRequest)o;
                            }
                            break Label_0289;
                        }
                        catch (Throwable t2) {
                            convertFileToString = null;
                            t = t2;
                        }
                        HereMapUtil.sLogger.e(t);
                        IOUtils.closeStream((Closeable)convertFileToString);
                        removeRouteInfo((Logger)o, true);
                        HereMapUtil.lastKnownDeviceId = null;
                        o = null;
                        return (NavigationRouteRequest)o;
                    }
                    if (isRouteFileStale(file)) {
                        ((Logger)o).v("getSavedRouteRequest: route is stale file-time =" + file.lastModified() + " now=" + System.currentTimeMillis() + " deleted=" + IOUtils.deleteFile(HudApplication.getAppContext(), file.getAbsolutePath()));
                        o = null;
                        return (NavigationRouteRequest)o;
                    }
                    ((Logger)o).v("getSavedRouteRequest: route is good file-time =" + file.lastModified() + " now=" + System.currentTimeMillis());
                }
                Object convertFileToString = new FileInputStream(file);
                try {
                    final NavigationRouteRequest build = new NavigationRouteRequest.Builder(HereMapUtil.wire.<NavigationRouteRequest>parseFrom((InputStream)convertFileToString, NavigationRouteRequest.class)).requestId(UUID.randomUUID().toString()).build();
                    IOUtils.closeStream((Closeable)convertFileToString);
                    convertFileToString = new StringBuilder();
                    final File file2 = new File(((StringBuilder)convertFileToString).append(activeRouteInfoDir).append(File.separator).append("routerequest.device").toString());
                    if (!file2.exists()) {
                        ((Logger)o).v("getSavedRouteRequest: no saved device id");
                        o = build;
                        return (NavigationRouteRequest)o;
                    }
                    convertFileToString = IOUtils.convertFileToString(file2.getAbsolutePath());
                    final NavdyDeviceId deviceId = RemoteDeviceManager.getInstance().getDeviceId();
                    if (deviceId != null && !deviceId.equals(new NavdyDeviceId((String)convertFileToString))) {
                        ((Logger)o).v("getSavedRouteRequest: device id is different now[" + deviceId + "] stored[" + (String)convertFileToString + "]");
                        removeRouteInfo((Logger)o, true);
                        o = null;
                        return (NavigationRouteRequest)o;
                    }
                    HereMapUtil.lastKnownDeviceId = (String)convertFileToString;
                    ((Logger)o).v("getSavedRouteRequest: returning route");
                    o = build;
                    return (NavigationRouteRequest)o;
                }
                catch (Throwable t) {
                    continue;
                }
                break;
            }
        }
    }
    
    public static String getSignpostText(final Signpost signpost, final Maneuver maneuver, final SignPostInfo signPostInfo, final ArrayList<String> list) {
        String string;
        if (signpost != null) {
        Label_0100_Outer:
            while (true) {
                final StringBuilder signPostBuilder = HereMapUtil.signPostBuilder;
                while (true) {
                    Label_0522: {
                        synchronized (signPostBuilder) {
                            HereMapUtil.signPostBuilder.setLength(0);
                            HereMapUtil.signPostBuilder2.setLength(0);
                            HereMapUtil.signPostSet.clear();
                            String s = signpost.getExitNumber();
                            Object o = signpost.getExitText();
                            int n = 0;
                            int n2 = 0;
                            int n3 = 0;
                            if (!TextUtils.isEmpty((CharSequence)s)) {
                                if (maneuver.getAction() == Maneuver.Action.LEAVE_HIGHWAY) {
                                    break Label_0522;
                                }
                                HereMapUtil.signPostBuilder.append(HereManeuverDisplayBuilder.EXIT_NUMBER);
                                HereMapUtil.signPostBuilder.append(" ");
                                final int n4 = 0;
                                n3 = 1;
                                HereMapUtil.signPostBuilder.append(s);
                                n = n4;
                                n2 = n3;
                                if (list != null) {
                                    list.add(s);
                                    n2 = n3;
                                    n = n4;
                                }
                            }
                            if (!TextUtils.isEmpty((CharSequence)o)) {
                                if (needSeparator(HereMapUtil.signPostBuilder)) {
                                    HereMapUtil.signPostBuilder.append(" ");
                                }
                                HereMapUtil.signPostBuilder.append((String)o);
                                if (list != null) {
                                    list.add((String)o);
                                }
                            }
                            if (n != 0) {
                                HereMapUtil.signPostBuilder.append(" ");
                            }
                            final Iterator<Signpost.LocalizedLabel> iterator = signpost.getExitDirections().iterator();
                            n3 = n2;
                            while (iterator.hasNext()) {
                                o = iterator.next();
                                if (((Signpost.LocalizedLabel)o).getLanguage().equalsIgnoreCase(HereMapUtil.TBT_ISO3_LANG_CODE)) {
                                    final String routeName = ((Signpost.LocalizedLabel)o).getRouteName();
                                    s = ((Signpost.LocalizedLabel)o).getRouteDirection();
                                    o = ((Signpost.LocalizedLabel)o).getText();
                                    int n5 = n3;
                                    if (!TextUtils.isEmpty((CharSequence)routeName)) {
                                        if (signPostInfo != null) {
                                            signPostInfo.hasNumberInSignPost = true;
                                        }
                                        if ((n5 = n3) != 0) {
                                            n3 = (n5 = 0);
                                            if (needSeparator(HereMapUtil.signPostBuilder)) {
                                                HereMapUtil.signPostBuilder.append("/");
                                                n5 = n3;
                                            }
                                        }
                                        if (needSeparator(HereMapUtil.signPostBuilder)) {
                                            HereMapUtil.signPostBuilder.append("/");
                                        }
                                        HereMapUtil.signPostBuilder.append(routeName);
                                    }
                                    if (!TextUtils.isEmpty((CharSequence)s)) {
                                        if (signPostInfo != null && signPostInfo.hasNumberInSignPost) {
                                            HereMapUtil.signPostBuilder.append(" ");
                                        }
                                        if (list != null && !TextUtils.isEmpty((CharSequence)routeName)) {
                                            list.add(routeName + " " + s);
                                        }
                                        HereMapUtil.signPostBuilder.append(s);
                                    }
                                    n3 = n5;
                                    if (TextUtils.isEmpty((CharSequence)o)) {
                                        continue Label_0100_Outer;
                                    }
                                    n3 = n5;
                                    if (HereMapUtil.signPostSet.contains(o)) {
                                        continue Label_0100_Outer;
                                    }
                                    if (signPostInfo != null) {
                                        signPostInfo.hasNameInSignPost = true;
                                    }
                                    if (needSeparator(HereMapUtil.signPostBuilder2)) {
                                        HereMapUtil.signPostBuilder2.append("/");
                                    }
                                    HereMapUtil.signPostBuilder2.append((String)o);
                                    HereMapUtil.signPostSet.add((String)o);
                                    n3 = n5;
                                }
                            }
                            break;
                        }
                    }
                    final int n4 = 1;
                    continue;
                }
            }
            if (HereMapUtil.signPostBuilder2.length() > 0) {
                if (needSeparator(HereMapUtil.signPostBuilder)) {
                    HereMapUtil.signPostBuilder.append("/");
                }
                HereMapUtil.signPostBuilder.append(HereMapUtil.signPostBuilder2.toString());
            }
            string = HereMapUtil.signPostBuilder.toString();
        }
        // monitorexit(sb)
        else {
            string = null;
        }
        return string;
    }
    
    public static boolean hasSameSignpostAndToRoad(final Maneuver maneuver, final Maneuver maneuver2) {
        boolean b = true;
        if (maneuver == null || maneuver2 == null) {
            b = false;
        }
        else {
            final String concatText = concatText(maneuver.getNextRoadNumber(), maneuver.getNextRoadName(), true, false);
            final String concatText2 = concatText(maneuver2.getNextRoadNumber(), maneuver2.getNextRoadName(), true, false);
            if (TextUtils.isEmpty((CharSequence)concatText) || TextUtils.isEmpty((CharSequence)concatText2) || !TextUtils.equals((CharSequence)concatText, (CharSequence)concatText2)) {
                b = false;
            }
            else {
                final String signpostText = getSignpostText(maneuver.getSignpost(), maneuver, null, null);
                final String signpostText2 = getSignpostText(maneuver2.getSignpost(), maneuver2, null, null);
                if (TextUtils.isEmpty((CharSequence)signpostText) || TextUtils.isEmpty((CharSequence)signpostText2) || !TextUtils.equals((CharSequence)signpostText, (CharSequence)signpostText2)) {
                    b = false;
                }
            }
        }
        return b;
    }
    
    public static boolean isCoordinateEqual(final GeoCoordinate geoCoordinate, final GeoCoordinate geoCoordinate2) {
        final double latitude = geoCoordinate.getLatitude();
        final double longitude = geoCoordinate.getLongitude();
        final double latitude2 = geoCoordinate2.getLatitude();
        final double longitude2 = geoCoordinate2.getLongitude();
        return latitude == latitude2 && longitude == longitude2;
    }
    
    public static boolean isCurrentRoadHighway(final RoadElement roadElement) {
        boolean b = false;
        if (roadElement != null && roadElement.getAttributes().contains(RoadElement.Attribute.HIGHWAY)) {
            b = true;
        }
        return b;
    }
    
    public static boolean isCurrentRoadHighway(final Maneuver maneuver) {
        final boolean b = false;
        boolean currentRoadHighway;
        if (maneuver == null) {
            currentRoadHighway = b;
        }
        else if (isManeuverActionHighway(maneuver.getAction())) {
            currentRoadHighway = true;
        }
        else {
            final List<RoadElement> roadElements = maneuver.getRoadElements();
            currentRoadHighway = b;
            if (roadElements != null) {
                currentRoadHighway = b;
                if (roadElements.size() != 0) {
                    currentRoadHighway = isCurrentRoadHighway(roadElements.get(0));
                }
            }
        }
        return currentRoadHighway;
    }
    
    public static boolean isGeoCoordinateEquals(final GeoCoordinate geoCoordinate, final GeoCoordinate geoCoordinate2) {
        return geoCoordinate.getLatitude() == geoCoordinate2.getLatitude() && geoCoordinate.getLongitude() == geoCoordinate2.getLongitude();
    }
    
    public static boolean isHighwayAction(final Maneuver maneuver) {
        final boolean b = false;
        boolean b2;
        if (maneuver == null) {
            b2 = b;
        }
        else {
            final Maneuver.Action action = maneuver.getAction();
            b2 = b;
            if (action != null) {
                switch (action) {
                    default:
                        b2 = b;
                        break;
                    case ENTER_HIGHWAY:
                    case ENTER_HIGHWAY_FROM_LEFT:
                    case ENTER_HIGHWAY_FROM_RIGHT:
                        b2 = true;
                        break;
                }
            }
        }
        return b2;
    }
    
    public static boolean isInTunnel(final RoadElement roadElement) {
        if (roadElement == null) {
            return false;
        }
        final EnumSet<RoadElement.Attribute> attributes = roadElement.getAttributes();
        if (attributes == null) {
            return false;
        }
        return attributes.contains(RoadElement.Attribute.TUNNEL);
        contains = false;
        return contains;
    }
    
    public static boolean isManeuverActionHighway(final Maneuver.Action action) {
        boolean b = false;
        if (action != null) {
            switch (action) {
                case CONTINUE_HIGHWAY:
                case CHANGE_HIGHWAY:
                case ENTER_HIGHWAY:
                case ENTER_HIGHWAY_FROM_LEFT:
                case ENTER_HIGHWAY_FROM_RIGHT:
                    b = true;
                    break;
            }
        }
        return b;
    }
    
    public static boolean isManeuverShort(final Maneuver maneuver) {
        return maneuver.getDistanceToNextManeuver() <= 20;
    }
    
    static boolean isRouteFileStale(final File file) {
        final long n = System.currentTimeMillis() - file.lastModified();
        return n >= HereMapUtil.STALE_ROUTE_TIME || n < 0L;
    }
    
    public static boolean isSavedRoute(final NavigationRouteRequest navigationRouteRequest) {
        return navigationRouteRequest != null && navigationRouteRequest.routeAttributes != null && navigationRouteRequest.routeAttributes.contains(NavigationRouteRequest.RouteAttribute.ROUTE_ATTRIBUTE_SAVED_ROUTE);
    }
    
    public static boolean isStartManeuver(final Maneuver maneuver) {
        boolean b = false;
        if (maneuver != null && maneuver.getIcon() == Maneuver.Icon.START) {
            b = true;
        }
        return b;
    }
    
    public static boolean isValidEtaDate(final Date date) {
        boolean b = false;
        if (date != null) {
            final int year = date.getYear();
            if (date == null || year <= 71) {
                HereMapUtil.sLogger.e("invalid eta date[" + date + "]");
            }
            else {
                final long n = System.currentTimeMillis() - date.getTime();
                if (n >= 360000L) {
                    HereMapUtil.sLogger.e("invalid eta timediff[" + n + "]");
                }
                else {
                    if (n > 0L) {
                        HereMapUtil.sLogger.i("eta is behind timediff[" + n + "]");
                    }
                    b = true;
                }
            }
        }
        return b;
    }
    
    static boolean isValidNavigationState(final NavigationSessionState navigationSessionState) {
        return navigationSessionState != null && (navigationSessionState == NavigationSessionState.NAV_SESSION_STARTED || navigationSessionState == NavigationSessionState.NAV_SESSION_PAUSED || navigationSessionState == NavigationSessionState.NAV_SESSION_STOPPED);
    }
    
    public static void loadImage(final Image image, final int n, final int n2, final IImageLoadCallback imageLoadCallback) {
        if (image == null || imageLoadCallback == null || n <= 0 || n2 <= 0) {
            throw new IllegalArgumentException();
        }
        TaskManager.getInstance().execute(new Runnable() {
            @Override
            public void run() {
                try {
                    imageLoadCallback.result(image, image.getBitmap(n, n2));
                }
                catch (Throwable t) {
                    HereMapUtil.sLogger.e(t);
                    imageLoadCallback.result(image, null);
                }
            }
        }, 1);
    }
    
    public static boolean needSeparator(final StringBuilder sb) {
        final boolean b = false;
        boolean b2;
        if (sb == null) {
            b2 = b;
        }
        else {
            final int length = sb.length();
            b2 = b;
            if (length != 0) {
                final char char1 = sb.charAt(length - 1);
                b2 = b;
                if (char1 != '/') {
                    b2 = b;
                    if (char1 != ' ') {
                        b2 = true;
                    }
                }
            }
        }
        return b2;
    }
    
    public static String parseStreetAddress(String substring) {
        if (substring == null) {
            substring = null;
        }
        else {
            final int index = substring.indexOf(",");
            if (index >= 0) {
                substring = substring.substring(0, index);
            }
        }
        return substring;
    }
    
    public static void printManeuverDetails(final Maneuver maneuver, final String s) {
        HereMapUtil.sLogger.v(s + " turn[" + maneuver.getTurn().name() + "] to[" + maneuver.getNextRoadNumber() + " " + maneuver.getNextRoadName() + "] distance[" + maneuver.getDistanceToNextManeuver() + " meters] action[" + maneuver.getAction() + "] current[" + maneuver.getRoadNumber() + " " + maneuver.getRoadName() + "] distanceFromStart[" + maneuver.getDistanceFromStart() + " meters]");
    }
    
    public static void printRouteDetails(final Route route, final String s, final NavigationRouteRequest navigationRouteRequest, final StringBuilder sb) {
        printRouteDetails(route, s, navigationRouteRequest, sb, null);
    }
    
    public static void printRouteDetails(final Route route, final String s, final NavigationRouteRequest navigationRouteRequest, final StringBuilder sb, @Nullable Maneuver maneuver) {
        if (route != null) {
            final long elapsedRealtime = SystemClock.elapsedRealtime();
            final String string = "[Route] start[" + route.getStart() + "] end[" + route.getDestination() + "] length[" + route.getLength() + " meters] tta[" + route.getTta(Route.TrafficPenaltyMode.OPTIMAL, 268435455).getDuration() + " seconds]";
            if (sb == null) {
                HereMapUtil.sLogger.v(string);
            }
            else {
                sb.append(string + "\n");
            }
            List<Maneuver> list2;
            final List<Maneuver> list = list2 = route.getManeuvers();
            if (maneuver != null) {
                final int maneuverIndex = findManeuverIndex(list, maneuver);
                if (maneuverIndex < 0) {
                    return;
                }
                list2 = list.subList(maneuverIndex, list.size());
            }
            final int size = list2.size();
            maneuver = null;
            Maneuver maneuver2 = null;
            Maneuver maneuver3;
            for (int i = 0; i < size; ++i, maneuver2 = maneuver3) {
                if (maneuver2 != null) {
                    maneuver = maneuver2;
                }
                maneuver3 = list2.get(i);
                Maneuver maneuver4 = null;
                if (i < size - 1) {
                    maneuver4 = list2.get(i + 1);
                }
                String name = "";
                final Maneuver.Icon icon = maneuver3.getIcon();
                if (icon != null) {
                    name = icon.name();
                }
                String name2 = "";
                final Maneuver.TrafficDirection trafficDirection = maneuver3.getTrafficDirection();
                if (trafficDirection != null) {
                    name2 = trafficDirection.name();
                }
                final String string2 = "    [Maneuver-Raw] turn[" + maneuver3.getTurn().name() + "] to[" + maneuver3.getNextRoadNumber() + " " + maneuver3.getNextRoadName() + "] distance[" + maneuver3.getDistanceToNextManeuver() + " meters] action[" + maneuver3.getAction() + "] current[" + maneuver3.getRoadNumber() + " " + maneuver3.getRoadName() + "] traffic-directon[" + name2 + "] angle[" + maneuver3.getAngle() + "] orientation[" + maneuver3.getMapOrientation() + "] distanceFromStart[" + maneuver3.getDistanceFromStart() + " meters] Icon[" + name + "]";
                if (sb != null) {
                    sb.append(string2 + "\n");
                }
                else {
                    HereMapUtil.sLogger.v(string2);
                }
                final Signpost signpost = maneuver3.getSignpost();
                if (signpost != null) {
                    final String string3 = "    [Maneuver-Raw-SignPost] exitNumber[" + signpost.getExitNumber() + "] exitText[" + signpost.getExitText() + "]";
                    if (sb != null) {
                        sb.append(string3);
                    }
                    else {
                        HereMapUtil.sLogger.v(string3);
                    }
                    for (final Signpost.LocalizedLabel localizedLabel : signpost.getExitDirections()) {
                        final String string4 = "     [Maneuver-Raw-SignPost-Label]  text[" + localizedLabel.getText() + "] routeDirection[" + localizedLabel.getRouteDirection() + "] routeName[" + localizedLabel.getRouteName() + "]";
                        if (sb != null) {
                            sb.append(string4 + "\n");
                        }
                        else {
                            HereMapUtil.sLogger.v(string4);
                        }
                    }
                }
                MapEvents.ManeuverDisplay maneuverDisplay;
                if (i == 0) {
                    maneuverDisplay = HereManeuverDisplayBuilder.getStartManeuverDisplay(maneuver3, maneuver4);
                }
                else {
                    maneuverDisplay = HereManeuverDisplayBuilder.getManeuverDisplay(maneuver3, maneuver4 == null, maneuver3.getDistanceToNextManeuver(), s, maneuver, navigationRouteRequest, maneuver4, true, false, null);
                }
                HereMapUtil.sLogger.v("[Maneuver-Display] " + maneuverDisplay.toString());
                final String string5 = "[Maneuver-Display-short] " + maneuverDisplay.pendingTurn + " " + maneuverDisplay.pendingRoad + " ( " + maneuverDisplay.distanceToPendingRoadText + " )";
                if (sb != null) {
                    sb.append(string5 + "\n");
                }
                else {
                    HereMapUtil.sLogger.v(string5);
                }
            }
            HereMapUtil.sLogger.v("Time to print route:" + (SystemClock.elapsedRealtime() - elapsedRealtime));
        }
    }
    
    static void printTrafficEvent(final TrafficEvent trafficEvent, String value, Route route) {
        while (true) {
            final StringBuilder trafficEventBuilder = HereMapUtil.trafficEventBuilder;
            while (true) {
                synchronized (trafficEventBuilder) {
                    HereMapUtil.trafficEventBuilder.setLength(0);
                    HereMapUtil.trafficEventBuilder.append((String)value);
                    HereMapUtil.trafficEventBuilder.append(":: ");
                    StringBuilder sb = HereMapUtil.trafficEventBuilder;
                    sb.append("severity:" + trafficEvent.getSeverity().name());
                    sb = HereMapUtil.trafficEventBuilder;
                    sb.append(", short-text:" + trafficEvent.getShortText());
                    sb = HereMapUtil.trafficEventBuilder;
                    sb.append(", text:" + trafficEvent.getEventText());
                    final StringBuilder trafficEventBuilder2 = HereMapUtil.trafficEventBuilder;
                    sb = new StringBuilder();
                    trafficEventBuilder2.append(sb.append(", isActive:").append(trafficEvent.isActive()).toString());
                    sb = HereMapUtil.trafficEventBuilder;
                    sb.append(", isFlow:" + trafficEvent.isFlow());
                    final StringBuilder trafficEventBuilder3 = HereMapUtil.trafficEventBuilder;
                    sb = new StringBuilder();
                    trafficEventBuilder3.append(sb.append(", isIncident:").append(trafficEvent.isIncident()).toString());
                    sb = HereMapUtil.trafficEventBuilder;
                    sb.append(", isVisible:" + trafficEvent.isVisible());
                    sb = HereMapUtil.trafficEventBuilder;
                    final StringBuilder append = new StringBuilder().append(", isOnRoute:");
                    if (route == null) {
                        value = "n/a";
                        sb.append(append.append(value).toString());
                        route = (Route)HereMapUtil.trafficEventBuilder;
                        ((StringBuilder)route).append(", isReroutable:" + trafficEvent.isReroutable());
                        final StringBuilder trafficEventBuilder4 = HereMapUtil.trafficEventBuilder;
                        route = (Route)new StringBuilder();
                        trafficEventBuilder4.append(((StringBuilder)route).append(", affected len:").append(trafficEvent.getAffectedLength()).toString());
                        final StringBuilder trafficEventBuilder5 = HereMapUtil.trafficEventBuilder;
                        route = (Route)new StringBuilder();
                        trafficEventBuilder5.append(((StringBuilder)route).append(", offRoute icon:").append(trafficEvent.getIconOffRoute()).toString());
                        final StringBuilder trafficEventBuilder6 = HereMapUtil.trafficEventBuilder;
                        route = (Route)new StringBuilder();
                        trafficEventBuilder6.append(((StringBuilder)route).append(", onRoute icon:").append(trafficEvent.getIconOnRoute()).toString());
                        route = (Route)HereMapUtil.trafficEventBuilder;
                        ((StringBuilder)route).append(", firstAffectedStreet:" + trafficEvent.getFirstAffectedStreet());
                        route = (Route)HereMapUtil.trafficEventBuilder;
                        ((StringBuilder)route).append(", activateDate:" + trafficEvent.getActivationDate());
                        final StringBuilder trafficEventBuilder7 = HereMapUtil.trafficEventBuilder;
                        route = (Route)new StringBuilder();
                        trafficEventBuilder7.append(((StringBuilder)route).append(", updateDate:").append(trafficEvent.getUpdateDate()).toString());
                        final StringBuilder trafficEventBuilder8 = HereMapUtil.trafficEventBuilder;
                        route = (Route)new StringBuilder();
                        sb = ((StringBuilder)route).append(", now:");
                        route = (Route)new Date();
                        trafficEventBuilder8.append(sb.append(route).toString());
                        final List<String> affectedStreets = trafficEvent.getAffectedStreets();
                        HereMapUtil.trafficEventBuilder.append(", affectedStreet: ");
                        if (affectedStreets != null) {
                            final Iterator<String> iterator = affectedStreets.iterator();
                            while (iterator.hasNext()) {
                                value = iterator.next();
                                HereMapUtil.trafficEventBuilder.append((String)value);
                                HereMapUtil.trafficEventBuilder.append(", ");
                            }
                            break;
                        }
                        break;
                    }
                }
                value = trafficEvent.isOnRoute(route);
                continue;
            }
        }
        HereMapUtil.sLogger.v(HereMapUtil.trafficEventBuilder.toString());
        HereMapUtil.trafficEventBuilder.setLength(0);
    }
    // monitorexit(sb2)
    
    static void printTrafficNotification(final TrafficNotification trafficNotification, final String s) {
        final List<TrafficNotificationInfo> infoList = trafficNotification.getInfoList();
        if (infoList != null) {
            for (final TrafficNotificationInfo trafficNotificationInfo : infoList) {
                HereMapUtil.sLogger.v(s + " type:" + trafficNotificationInfo.getType().name());
                HereMapUtil.sLogger.v(s + " distance:" + trafficNotificationInfo.getDistanceInMeters());
                printTrafficEvent(trafficNotificationInfo.getEvent(), s, null);
            }
        }
    }
    
    public static void removeRouteInfo(final Logger logger, final boolean b) {
        synchronized (HereMapUtil.class) {
            removeRouteInfoFiles(logger, b);
        }
    }
    
    private static void removeRouteInfoFiles(final Logger logger, final boolean b) {
        try {
            final Context appContext = HudApplication.getAppContext();
            final String activeRouteInfoDir = PathManager.getInstance().getActiveRouteInfoDir();
            final boolean deleteFile = IOUtils.deleteFile(appContext, activeRouteInfoDir + File.separator + "gasrouterequest.bin");
            logger.v("removeRouteInfo delete gasRoute[" + deleteFile + "]");
            if (!deleteFile || b) {
                logger.v("removeRouteInfo delete route[" + IOUtils.deleteFile(appContext, activeRouteInfoDir + File.separator + "routerequest.bin") + "] delete id[" + IOUtils.deleteFile(appContext, activeRouteInfoDir + File.separator + "routerequest.device") + "]");
            }
        }
        catch (Throwable t) {
            HereMapUtil.sLogger.e(t);
        }
    }
    
    static float roundToIntegerStep(final int n, final float n2) {
        return n * Math.round(n2 / n);
    }
    
    public static double roundToN(final double n, final int n2) {
        return (long)(n2 * n) / n2;
    }
    
    public static void saveGasRouteInfo(final NavigationRouteRequest navigationRouteRequest, final NavdyDeviceId navdyDeviceId, final Logger logger) {
        saveRouteInfoInternal(navigationRouteRequest, navdyDeviceId, logger, "gasrouterequest.bin");
    }
    
    public static void saveRouteInfo(final NavigationRouteRequest navigationRouteRequest, final NavdyDeviceId navdyDeviceId, final Logger logger) {
        saveRouteInfoInternal(navigationRouteRequest, navdyDeviceId, logger, "routerequest.bin");
    }
    
    private static void saveRouteInfoInternal(final NavigationRouteRequest p0, final NavdyDeviceId p1, final Logger p2, final String p3) {
        // 
        This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     2: dup            
        //     3: astore          4
        //     5: monitorenter   
        //     6: aconst_null    
        //     7: putstatic       com/navdy/hud/app/maps/here/HereMapUtil.lastKnownDeviceId:Ljava/lang/String;
        //    10: aload_1        
        //    11: astore          5
        //    13: aload_1        
        //    14: ifnonnull       82
        //    17: getstatic       com/navdy/hud/app/maps/here/HereMapUtil.sLogger:Lcom/navdy/service/library/log/Logger;
        //    20: ldc_w           "saveRouteInfo: null deviceid"
        //    23: invokevirtual   com/navdy/service/library/log/Logger.v:(Ljava/lang/String;)V
        //    26: new             Lcom/navdy/service/library/device/NavdyDeviceId;
        //    29: astore          5
        //    31: aload           5
        //    33: invokestatic    com/navdy/hud/app/manager/RemoteDeviceManager.getInstance:()Lcom/navdy/hud/app/manager/RemoteDeviceManager;
        //    36: invokevirtual   com/navdy/hud/app/manager/RemoteDeviceManager.getConnectionHandler:()Lcom/navdy/hud/app/service/ConnectionHandler;
        //    39: invokevirtual   com/navdy/hud/app/service/ConnectionHandler.getLastConnectedDeviceInfo:()Lcom/navdy/service/library/events/DeviceInfo;
        //    42: getfield        com/navdy/service/library/events/DeviceInfo.deviceId:Ljava/lang/String;
        //    45: invokespecial   com/navdy/service/library/device/NavdyDeviceId.<init>:(Ljava/lang/String;)V
        //    48: getstatic       com/navdy/hud/app/maps/here/HereMapUtil.sLogger:Lcom/navdy/service/library/log/Logger;
        //    51: astore_1       
        //    52: new             Ljava/lang/StringBuilder;
        //    55: astore          6
        //    57: aload           6
        //    59: invokespecial   java/lang/StringBuilder.<init>:()V
        //    62: aload_1        
        //    63: aload           6
        //    65: ldc_w           "saveRouteInfo: last deviceid found:"
        //    68: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    71: aload           5
        //    73: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //    76: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //    79: invokevirtual   com/navdy/service/library/log/Logger.v:(Ljava/lang/String;)V
        //    82: new             Ljava/lang/StringBuilder;
        //    85: astore_1       
        //    86: aload_1        
        //    87: invokespecial   java/lang/StringBuilder.<init>:()V
        //    90: aload_2        
        //    91: aload_1        
        //    92: ldc_w           "saveRouteInfo request["
        //    95: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    98: aload_0        
        //    99: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //   102: ldc_w           "] deviceId["
        //   105: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   108: aload           5
        //   110: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //   113: ldc_w           "]"
        //   116: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   119: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   122: invokevirtual   com/navdy/service/library/log/Logger.v:(Ljava/lang/String;)V
        //   125: aload_0        
        //   126: getfield        com/navdy/service/library/events/navigation/NavigationRouteRequest.destination:Lcom/navdy/service/library/events/location/Coordinate;
        //   129: ifnonnull       199
        //   132: aload_2        
        //   133: ldc_w           "route does not have destination"
        //   136: invokevirtual   com/navdy/service/library/log/Logger.w:(Ljava/lang/String;)V
        //   139: getstatic       com/navdy/hud/app/maps/here/HereMapUtil.sLogger:Lcom/navdy/service/library/log/Logger;
        //   142: iconst_1       
        //   143: invokestatic    com/navdy/hud/app/maps/here/HereMapUtil.removeRouteInfoFiles:(Lcom/navdy/service/library/log/Logger;Z)V
        //   146: aload           4
        //   148: monitorexit    
        //   149: return         
        //   150: astore          5
        //   152: getstatic       com/navdy/hud/app/maps/here/HereMapUtil.sLogger:Lcom/navdy/service/library/log/Logger;
        //   155: ldc_w           "saveRouteInfo"
        //   158: aload           5
        //   160: invokevirtual   com/navdy/service/library/log/Logger.e:(Ljava/lang/String;Ljava/lang/Throwable;)V
        //   163: aload_1        
        //   164: astore          5
        //   166: goto            82
        //   169: aconst_null    
        //   170: astore_0       
        //   171: astore_1       
        //   172: getstatic       com/navdy/hud/app/maps/here/HereMapUtil.sLogger:Lcom/navdy/service/library/log/Logger;
        //   175: aload_1        
        //   176: invokevirtual   com/navdy/service/library/log/Logger.e:(Ljava/lang/Throwable;)V
        //   179: aload_0        
        //   180: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
        //   183: getstatic       com/navdy/hud/app/maps/here/HereMapUtil.sLogger:Lcom/navdy/service/library/log/Logger;
        //   186: iconst_1       
        //   187: invokestatic    com/navdy/hud/app/maps/here/HereMapUtil.removeRouteInfoFiles:(Lcom/navdy/service/library/log/Logger;Z)V
        //   190: goto            146
        //   193: astore_0       
        //   194: aload           4
        //   196: monitorexit    
        //   197: aload_0        
        //   198: athrow         
        //   199: new             Ljava/util/ArrayList;
        //   202: astore          6
        //   204: aload           6
        //   206: aload_0        
        //   207: getfield        com/navdy/service/library/events/navigation/NavigationRouteRequest.routeAttributes:Ljava/util/List;
        //   210: invokespecial   java/util/ArrayList.<init>:(Ljava/util/Collection;)V
        //   213: aload           6
        //   215: getstatic       com/navdy/service/library/events/navigation/NavigationRouteRequest$RouteAttribute.ROUTE_ATTRIBUTE_SAVED_ROUTE:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$RouteAttribute;
        //   218: invokeinterface java/util/List.contains:(Ljava/lang/Object;)Z
        //   223: ifne            237
        //   226: aload           6
        //   228: getstatic       com/navdy/service/library/events/navigation/NavigationRouteRequest$RouteAttribute.ROUTE_ATTRIBUTE_SAVED_ROUTE:Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$RouteAttribute;
        //   231: invokeinterface java/util/List.add:(Ljava/lang/Object;)Z
        //   236: pop            
        //   237: new             Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;
        //   240: astore_1       
        //   241: aload_1        
        //   242: aload_0        
        //   243: invokespecial   com/navdy/service/library/events/navigation/NavigationRouteRequest$Builder.<init>:(Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;)V
        //   246: aload_1        
        //   247: aload           6
        //   249: invokevirtual   com/navdy/service/library/events/navigation/NavigationRouteRequest$Builder.routeAttributes:(Ljava/util/List;)Lcom/navdy/service/library/events/navigation/NavigationRouteRequest$Builder;
        //   252: invokevirtual   com/navdy/service/library/events/navigation/NavigationRouteRequest$Builder.build:()Lcom/navdy/service/library/events/navigation/NavigationRouteRequest;
        //   255: astore          7
        //   257: getstatic       com/navdy/hud/app/maps/here/HereMapUtil.sLogger:Lcom/navdy/service/library/log/Logger;
        //   260: ldc_w           "saveRouteInfo added saved route attribute"
        //   263: invokevirtual   com/navdy/service/library/log/Logger.v:(Ljava/lang/String;)V
        //   266: invokestatic    com/navdy/hud/app/storage/PathManager.getInstance:()Lcom/navdy/hud/app/storage/PathManager;
        //   269: invokevirtual   com/navdy/hud/app/storage/PathManager.getActiveRouteInfoDir:()Ljava/lang/String;
        //   272: astore          6
        //   274: new             Ljava/io/FileOutputStream;
        //   277: astore_1       
        //   278: new             Ljava/lang/StringBuilder;
        //   281: astore_0       
        //   282: aload_0        
        //   283: invokespecial   java/lang/StringBuilder.<init>:()V
        //   286: aload_1        
        //   287: aload_0        
        //   288: aload           6
        //   290: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   293: getstatic       java/io/File.separator:Ljava/lang/String;
        //   296: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   299: aload_3        
        //   300: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   303: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   306: invokespecial   java/io/FileOutputStream.<init>:(Ljava/lang/String;)V
        //   309: aload_1        
        //   310: astore_0       
        //   311: aload_1        
        //   312: astore_3       
        //   313: aload           7
        //   315: invokevirtual   com/navdy/service/library/events/navigation/NavigationRouteRequest.toByteArray:()[B
        //   318: astore          7
        //   320: aload_1        
        //   321: astore_0       
        //   322: aload_1        
        //   323: astore_3       
        //   324: aload_1        
        //   325: aload           7
        //   327: invokevirtual   java/io/FileOutputStream.write:([B)V
        //   330: aload_1        
        //   331: astore_0       
        //   332: aload_1        
        //   333: astore_3       
        //   334: aload_1        
        //   335: invokestatic    com/navdy/service/library/util/IOUtils.fileSync:(Ljava/io/FileOutputStream;)V
        //   338: aload_1        
        //   339: astore_0       
        //   340: aload_1        
        //   341: astore_3       
        //   342: aload_1        
        //   343: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
        //   346: new             Ljava/lang/StringBuilder;
        //   349: astore_0       
        //   350: aload_0        
        //   351: invokespecial   java/lang/StringBuilder.<init>:()V
        //   354: aload_2        
        //   355: aload_0        
        //   356: ldc_w           "saveRouteInfo: route info size["
        //   359: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   362: aload           7
        //   364: arraylength    
        //   365: invokevirtual   java/lang/StringBuilder.append:(I)Ljava/lang/StringBuilder;
        //   368: ldc_w           "]"
        //   371: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   374: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   377: invokevirtual   com/navdy/service/library/log/Logger.v:(Ljava/lang/String;)V
        //   380: aload           5
        //   382: ifnull          506
        //   385: new             Ljava/io/FileOutputStream;
        //   388: astore_1       
        //   389: new             Ljava/lang/StringBuilder;
        //   392: astore_0       
        //   393: aload_0        
        //   394: invokespecial   java/lang/StringBuilder.<init>:()V
        //   397: aload_1        
        //   398: aload_0        
        //   399: aload           6
        //   401: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   404: getstatic       java/io/File.separator:Ljava/lang/String;
        //   407: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   410: ldc             "routerequest.device"
        //   412: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   415: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   418: invokespecial   java/io/FileOutputStream.<init>:(Ljava/lang/String;)V
        //   421: aload_1        
        //   422: astore_0       
        //   423: aload_1        
        //   424: astore_3       
        //   425: aload           5
        //   427: invokevirtual   com/navdy/service/library/device/NavdyDeviceId.toString:()Ljava/lang/String;
        //   430: invokevirtual   java/lang/String.getBytes:()[B
        //   433: astore          6
        //   435: aload_1        
        //   436: astore_0       
        //   437: aload_1        
        //   438: astore_3       
        //   439: aload_1        
        //   440: aload           6
        //   442: invokevirtual   java/io/FileOutputStream.write:([B)V
        //   445: aload_1        
        //   446: astore_0       
        //   447: aload_1        
        //   448: astore_3       
        //   449: aload_1        
        //   450: invokestatic    com/navdy/service/library/util/IOUtils.fileSync:(Ljava/io/FileOutputStream;)V
        //   453: aload_1        
        //   454: astore_0       
        //   455: aload_1        
        //   456: astore_3       
        //   457: aload_1        
        //   458: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
        //   461: aload           5
        //   463: invokevirtual   com/navdy/service/library/device/NavdyDeviceId.toString:()Ljava/lang/String;
        //   466: putstatic       com/navdy/hud/app/maps/here/HereMapUtil.lastKnownDeviceId:Ljava/lang/String;
        //   469: new             Ljava/lang/StringBuilder;
        //   472: astore_0       
        //   473: aload_0        
        //   474: invokespecial   java/lang/StringBuilder.<init>:()V
        //   477: aload_2        
        //   478: aload_0        
        //   479: ldc_w           "saveRouteInfo: device id size["
        //   482: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   485: aload           6
        //   487: arraylength    
        //   488: invokevirtual   java/lang/StringBuilder.append:(I)Ljava/lang/StringBuilder;
        //   491: ldc_w           "]"
        //   494: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   497: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   500: invokevirtual   com/navdy/service/library/log/Logger.v:(Ljava/lang/String;)V
        //   503: goto            146
        //   506: aload_2        
        //   507: ldc_w           "saveRouteInfo: device id not written:null"
        //   510: invokevirtual   com/navdy/service/library/log/Logger.v:(Ljava/lang/String;)V
        //   513: goto            146
        //   516: astore_1       
        //   517: goto            172
        //   520: astore          6
        //   522: aload           5
        //   524: astore_1       
        //   525: aload           6
        //   527: astore          5
        //   529: goto            152
        //   532: astore_0       
        //   533: goto            194
        //   536: astore_0       
        //   537: goto            194
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  6      10     169    172    Ljava/lang/Throwable;
        //  6      10     193    194    Any
        //  17     26     169    172    Ljava/lang/Throwable;
        //  17     26     193    194    Any
        //  26     48     150    152    Ljava/lang/Throwable;
        //  26     48     193    194    Any
        //  48     82     520    532    Ljava/lang/Throwable;
        //  48     82     532    536    Any
        //  82     146    169    172    Ljava/lang/Throwable;
        //  82     146    193    194    Any
        //  152    163    169    172    Ljava/lang/Throwable;
        //  152    163    193    194    Any
        //  172    190    193    194    Any
        //  199    237    169    172    Ljava/lang/Throwable;
        //  199    237    193    194    Any
        //  237    309    169    172    Ljava/lang/Throwable;
        //  237    309    193    194    Any
        //  313    320    516    520    Ljava/lang/Throwable;
        //  313    320    536    540    Any
        //  324    330    516    520    Ljava/lang/Throwable;
        //  324    330    536    540    Any
        //  334    338    516    520    Ljava/lang/Throwable;
        //  334    338    536    540    Any
        //  342    346    516    520    Ljava/lang/Throwable;
        //  342    346    536    540    Any
        //  346    380    169    172    Ljava/lang/Throwable;
        //  346    380    193    194    Any
        //  385    421    169    172    Ljava/lang/Throwable;
        //  385    421    193    194    Any
        //  425    435    516    520    Ljava/lang/Throwable;
        //  425    435    536    540    Any
        //  439    445    516    520    Ljava/lang/Throwable;
        //  439    445    536    540    Any
        //  449    453    516    520    Ljava/lang/Throwable;
        //  449    453    536    540    Any
        //  457    461    516    520    Ljava/lang/Throwable;
        //  457    461    536    540    Any
        //  461    503    169    172    Ljava/lang/Throwable;
        //  461    503    193    194    Any
        //  506    513    169    172    Ljava/lang/Throwable;
        //  506    513    193    194    Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0082:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public interface IImageLoadCallback
    {
        void result(final Image p0, final Bitmap p1);
    }
    
    public static class LongContainer
    {
        public long val;
    }
    
    public static class SavedRouteData
    {
        public final boolean isGasRoute;
        public final NavigationRouteRequest navigationRouteRequest;
        
        public SavedRouteData(final NavigationRouteRequest navigationRouteRequest, final boolean isGasRoute) {
            this.navigationRouteRequest = navigationRouteRequest;
            this.isGasRoute = isGasRoute;
        }
    }
    
    public static class SignPostInfo
    {
        public boolean hasNameInSignPost;
        public boolean hasNumberInSignPost;
    }
}
