package com.navdy.hud.app.maps.here;

import com.navdy.hud.app.util.CrashReporter;
import com.squareup.otto.Subscribe;
import java.util.HashMap;
import com.navdy.hud.app.util.GenericUtil;
import com.navdy.hud.app.maps.util.MapUtils;
import android.text.TextUtils;
import java.util.Iterator;
import com.here.android.mpa.routing.RouteElements;
import com.here.android.mpa.routing.RouteElement;
import com.here.android.mpa.common.RoadElement;
import com.navdy.service.library.task.TaskManager;
import java.util.List;
import com.navdy.hud.app.maps.MapEvents;
import com.here.android.mpa.guidance.NavigationManager;
import com.navdy.service.library.log.Logger;
import android.os.Handler;
import com.here.android.mpa.routing.Route;
import com.here.android.mpa.guidance.TrafficUpdater;
import com.here.android.mpa.mapping.TrafficEvent;
import com.squareup.otto.Bus;

public class HereTrafficUpdater
{
    private static final int MAX_DISTANCE_NOTIFICATION_METERS = 5000;
    private static final int MIN_DISTANCE_NOTIFICATION_METERS = 200;
    private static final int MIN_JAM_DURATION_SECS = 180;
    private static final int ONE_MINUTE_SECS = 60;
    private static final long REFRESH_INTERVAL_MILLIS = 30000L;
    public static final boolean TRAFFIC_DEBUG_ENABLED = false;
    private final Bus bus;
    private TrafficEvent currentEvent;
    private int currentRemainingTimeInJam;
    private TrafficUpdater.RequestInfo currentRequestInfo;
    private Route currentRoute;
    private State currentState;
    private Runnable dismissEtaUpdate;
    private final Handler handler;
    private final Logger logger;
    private final NavigationManager navigationManager;
    private TrafficUpdater.GetEventsListener onGetEventsListener;
    private TrafficUpdater.Listener onRequestListener;
    private Runnable refreshRunnable;
    private final String tag;
    private final TrafficUpdater trafficUpdater;
    private final boolean verbose;
    
    public HereTrafficUpdater(final Logger logger, final String tag, final boolean verbose, final NavigationManager navigationManager, final Bus bus) {
        this.refreshRunnable = new Runnable() {
            @Override
            public void run() {
                HereTrafficUpdater.this.startRequest();
            }
        };
        this.dismissEtaUpdate = new Runnable() {
            @Override
            public void run() {
                HereTrafficUpdater.this.bus.post(new MapEvents.TrafficDelayDismissEvent());
            }
        };
        this.onRequestListener = new TrafficUpdater.Listener() {
            @Override
            public void onStatusChanged(final RequestState requestState) {
                if (HereTrafficUpdater.this.currentRoute == null) {
                    HereTrafficUpdater.this.logger.w(HereTrafficUpdater.this.tag + "onRequestListener triggered and currentRoute is null");
                }
                else if (requestState == RequestState.DONE) {
                    HereTrafficUpdater.this.trafficUpdater.getEvents(HereTrafficUpdater.this.currentRoute, HereTrafficUpdater.this.onGetEventsListener);
                }
                else {
                    HereTrafficUpdater.this.logger.i(HereTrafficUpdater.this.tag + " error on completion of TrafficUpdater.request");
                    HereTrafficUpdater.this.refresh();
                }
            }
        };
        this.onGetEventsListener = new TrafficUpdater.GetEventsListener() {
            @Override
            public void onComplete(final List<TrafficEvent> list, final Error error) {
                if (HereTrafficUpdater.this.currentRoute == null) {
                    HereTrafficUpdater.this.logger.w(HereTrafficUpdater.this.tag + "onGetEventsListener triggered and currentRoute is null");
                }
                else {
                    if (error == Error.NONE) {
                        if (HereTrafficUpdater.this.currentState == State.ACTIVE || HereTrafficUpdater.this.currentState == State.TRACK_DISTANCE) {
                            TaskManager.getInstance().execute(new Runnable() {
                                @Override
                                public void run() {
                                    HereTrafficUpdater.this.processEvents(HereTrafficUpdater.this.currentRoute, list);
                                }
                            }, 2);
                        }
                    }
                    else {
                        HereTrafficUpdater.this.logger.i(HereTrafficUpdater.this.tag + " error " + error.name() + " while getting TrafficEvents.");
                    }
                    HereTrafficUpdater.this.refresh();
                }
            }
        };
        this.logger = logger;
        this.tag = tag;
        this.verbose = verbose;
        this.navigationManager = navigationManager;
        this.bus = bus;
        (this.trafficUpdater = TrafficUpdater.getInstance()).enableUpdate(true);
        this.handler = new Handler();
        this.currentState = State.STOPPED;
        this.bus.register(this);
    }
    
    private void cancelRequest() {
        if (this.currentRequestInfo == null) {
            this.logger.w(this.tag + "cancelRequest: currentRequestInfo must not be null.");
        }
        else {
            if (this.currentRequestInfo.getError() == TrafficUpdater.Error.NONE) {
                this.trafficUpdater.cancelRequest(this.currentRequestInfo.getRequestId());
            }
            this.currentRequestInfo = null;
        }
    }
    
    private void changeState(final State currentState) {
        if (currentState != this.currentState) {
            if (currentState == State.STOPPED) {
                if (this.currentState == State.TRACK_DISTANCE) {
                    this.stopTrackingDistance();
                }
                else if (this.currentState == State.TRACK_JAM) {
                    this.stopTrackingJamTime();
                }
                this.stopUpdates();
            }
            else if (currentState == State.ACTIVE) {
                if (this.currentState == State.STOPPED) {
                    this.startUpdates();
                }
                else if (this.currentState == State.TRACK_DISTANCE) {
                    this.stopTrackingDistance();
                }
                else if (this.currentState == State.TRACK_JAM) {
                    this.stopTrackingJamTime();
                }
            }
            else if (currentState == State.TRACK_DISTANCE) {
                if (this.currentState != State.ACTIVE) {
                    this.logger.w(this.tag + " invalid state change from " + this.currentState.name() + " to " + currentState.name());
                    return;
                }
                this.startTrackingDistance();
            }
            else if (currentState == State.TRACK_JAM) {
                if (this.currentState == State.ACTIVE) {
                    this.startTrackingJamTime();
                }
                else {
                    if (this.currentState != State.TRACK_DISTANCE) {
                        this.logger.w(this.tag + " invalid state change from " + this.currentState.name() + " to " + currentState.name());
                        return;
                    }
                    this.stopTrackingDistance();
                    this.startTrackingJamTime();
                }
            }
            this.currentState = currentState;
        }
    }
    
    private int getRemainingTimeInJam(final Route route, final TrafficEvent trafficEvent) {
        double n = 0.0;
        final List<RoadElement> affectedRoadElements = trafficEvent.getAffectedRoadElements();
        final RoadElement roadElement = affectedRoadElements.get(affectedRoadElements.size() - 1);
        final RouteElements routeElementsFromLength = route.getRouteElementsFromLength((int)this.navigationManager.getElapsedDistance(), route.getLength());
        final String string = roadElement.getIdentifier().toString();
        final boolean b = false;
        final Iterator<RouteElement> iterator = routeElementsFromLength.getElements().iterator();
        boolean b2;
        while (true) {
            b2 = b;
            if (!iterator.hasNext()) {
                break;
            }
            final RoadElement roadElement2 = iterator.next().getRoadElement();
            if (roadElement2.getIdentifier().toString().equals(string)) {
                b2 = true;
                break;
            }
            n += roadElement2.getGeometryLength() / roadElement2.getDefaultSpeed();
        }
        int n2;
        if (b2) {
            n2 = (int)n;
        }
        else {
            n2 = 0;
        }
        return n2;
    }
    
    private boolean isCurrentRoute(final Route route) {
        return route.equals(this.currentRoute);
    }
    
    private void onDistanceTrackingUpdate(final HerePositionUpdateListener.MapMatchEvent mapMatchEvent) {
        if (this.currentEvent == null) {
            this.logger.w(this.tag + "onDistanceTrackingUpdate triggered while currentEvent is null.");
        }
        else {
            int n;
            if (this.currentEvent.getSeverity().getValue() >= TrafficEvent.Severity.VERY_HIGH.getValue()) {
                n = 1;
            }
            else {
                n = 0;
            }
            final String s = null;
            final RoadElement roadElement = mapMatchEvent.mapMatch.getRoadElement();
            String string = s;
            if (roadElement != null) {
                string = roadElement.getIdentifier().toString();
            }
            final String string2 = this.currentEvent.getAffectedRoadElements().get(0).getIdentifier().toString();
            if (this.verbose) {
                this.logger.v(this.tag + "Position update: currentRoadId=" + string + "; eventFirstRoadId=" + string2);
            }
            if (TextUtils.equals((CharSequence)string2, (CharSequence)string)) {
                if (n != 0) {
                    final int remainingTimeInJam = this.getRemainingTimeInJam(this.currentRoute, this.currentEvent);
                    this.logger.v(this.tag + "triggering jam, remaining time=" + remainingTimeInJam + " sec");
                    if (remainingTimeInJam >= 180) {
                        this.bus.post(new JamTrackEvent(this.currentRoute, this.currentEvent, remainingTimeInJam));
                        return;
                    }
                }
                this.currentEvent = null;
                this.changeState(State.ACTIVE);
            }
        }
    }
    
    private void onJamTrackingUpdate() {
        if (this.currentRoute == null) {
            this.logger.w(this.tag + "onJamTrackingUpdate triggered while currentRoute is null.");
        }
        else if (this.currentEvent == null) {
            this.logger.w(this.tag + "onJamTrackingUpdate triggered while currentEvent is null.");
        }
        else if (this.currentRemainingTimeInJam == 0) {
            this.logger.w(this.tag + "onJamTrackingUpdate triggered while currentRemainingTimeInJam is zero.");
        }
        else {
            final int remainingTimeInJam = this.getRemainingTimeInJam(this.currentRoute, this.currentEvent);
            if (this.verbose) {
                this.logger.v(this.tag + "Jam time update: remainingTimeInJam=" + remainingTimeInJam);
            }
            if (remainingTimeInJam > 0) {
                final int minuteCeiling = MapUtils.getMinuteCeiling(this.currentRemainingTimeInJam);
                final int minuteCeiling2 = MapUtils.getMinuteCeiling(remainingTimeInJam);
                if (Math.abs(minuteCeiling - minuteCeiling2) >= 60) {
                    this.currentRemainingTimeInJam = remainingTimeInJam;
                    this.bus.post(new MapEvents.TrafficJamProgressEvent(minuteCeiling2));
                }
            }
            else {
                this.currentRemainingTimeInJam = 0;
                this.changeState(State.ACTIVE);
            }
        }
    }
    
    private void processEvents(final Route route, final List<TrafficEvent> list) {
        GenericUtil.checkNotOnMainThread();
        final TrafficEvent trafficEvent = null;
        final TrafficEvent trafficEvent2 = null;
        final HashMap<Object, TrafficEvent> hashMap = new HashMap<Object, TrafficEvent>();
        for (final TrafficEvent trafficEvent3 : list) {
            final List<RoadElement> affectedRoadElements = trafficEvent3.getAffectedRoadElements();
            if (affectedRoadElements.size() > 0) {
                hashMap.put(affectedRoadElements.get(0).getIdentifier().toString(), trafficEvent3);
            }
        }
        int n = 0;
        final Iterator<RouteElement> iterator2 = route.getRouteElementsFromLength((int)this.navigationManager.getElapsedDistance(), route.getLength()).getElements().iterator();
        TrafficEvent trafficEvent4 = trafficEvent2;
    Label_0452:
        while (true) {
            int n2;
            TrafficEvent trafficEvent7;
            do {
                TrafficEvent trafficEvent5 = trafficEvent4;
                TrafficEvent trafficEvent6 = trafficEvent;
                if (iterator2.hasNext()) {
                    final RoadElement roadElement = iterator2.next().getRoadElement();
                    final String string = roadElement.getIdentifier().toString();
                    this.logger.v(this.tag + "route element id=" + string);
                    trafficEvent7 = trafficEvent4;
                    if (hashMap.containsKey(string)) {
                        final TrafficEvent trafficEvent8 = hashMap.get(string);
                        final boolean b = trafficEvent8.getSeverity().getValue() > TrafficEvent.Severity.HIGH.getValue();
                        final boolean b2 = trafficEvent8.getSeverity().getValue() > TrafficEvent.Severity.NORMAL.getValue();
                        this.logger.v(this.tag + "event inside route: isVerySevereEvent=" + b + "; isSevereEvent=" + b2 + "; distanceToLocation=" + n);
                        if (b && n < 200) {
                            final int remainingTimeInJam = this.getRemainingTimeInJam(route, trafficEvent8);
                            this.logger.v(this.tag + "triggering jam, remaining time=" + remainingTimeInJam + " sec");
                            trafficEvent7 = trafficEvent4;
                            if (remainingTimeInJam >= 180) {
                                this.bus.post(new JamTrackEvent(route, trafficEvent8, remainingTimeInJam));
                                return;
                            }
                        }
                        else {
                            if (b && n > 200) {
                                trafficEvent6 = trafficEvent8;
                                trafficEvent5 = trafficEvent4;
                                break Label_0452;
                            }
                            trafficEvent7 = trafficEvent4;
                            if (b2 && (trafficEvent7 = trafficEvent4) == null) {
                                trafficEvent7 = trafficEvent4;
                                if (n > 200) {
                                    trafficEvent7 = trafficEvent8;
                                }
                            }
                        }
                    }
                    n2 = (int)(n + roadElement.getGeometryLength());
                    trafficEvent4 = trafficEvent7;
                    continue;
                }
                break Label_0452;
                if (trafficEvent6 != null) {
                    this.bus.post(new DistanceTrackEvent(route, trafficEvent6));
                    return;
                }
                if (trafficEvent5 != null) {
                    this.bus.post(new DistanceTrackEvent(route, trafficEvent5));
                    return;
                }
                this.bus.post(new TrackDismissEvent(route));
                return;
            } while ((n = n2) <= 5000);
            TrafficEvent trafficEvent5 = trafficEvent7;
            TrafficEvent trafficEvent6 = trafficEvent;
            continue Label_0452;
        }
    }
    
    private void refresh() {
        this.logger.v(this.tag + "posting refresh in 30 sec...");
        this.cancelRequest();
        this.handler.removeCallbacks(this.refreshRunnable);
        this.handler.postDelayed(this.refreshRunnable, 30000L);
    }
    
    private void startRequest() {
        if (this.currentRoute == null) {
            this.logger.w(this.tag + "startRequest: currentRoute must not be null.");
        }
        else {
            this.currentRequestInfo = this.trafficUpdater.request(this.currentRoute, this.onRequestListener);
            switch (this.currentRequestInfo.getError()) {
                case UNKNOWN:
                case OUT_OF_MEMORY:
                case REQUEST_FAILED:
                    this.logger.i(this.tag + " recovering from error before TrafficUpdater.request");
                    this.refresh();
                case NONE:
                    break;
                default:
                    this.logger.i(this.tag + " got error before TrafficUpdater.request. Invalid/malformed request.");
                    this.changeState(State.STOPPED);
                    break;
            }
        }
    }
    
    private void startTrackingDistance() {
        this.bus.post(this.transformToLiveTrafficEvent(this.currentEvent));
    }
    
    private void startTrackingJamTime() {
        this.bus.post(new MapEvents.TrafficJamProgressEvent(MapUtils.getMinuteCeiling(this.currentRemainingTimeInJam)));
    }
    
    private void startUpdates() {
        if (this.currentRoute == null) {
            this.logger.e(this.tag + " route must not be null.");
        }
    }
    
    private void stopTrackingDistance() {
        this.bus.post(new MapEvents.LiveTrafficDismissEvent());
    }
    
    private void stopTrackingJamTime() {
        this.bus.post(new MapEvents.TrafficJamDismissEvent());
    }
    
    private void stopUpdates() {
        this.cancelRequest();
        this.currentEvent = null;
        this.handler.removeCallbacks(this.refreshRunnable);
        this.handler.removeCallbacks(this.dismissEtaUpdate);
        this.currentRoute = null;
        this.bus.post(new MapEvents.TrafficJamDismissEvent());
        this.bus.post(new MapEvents.TrafficDelayDismissEvent());
    }
    
    private void trackEvent(final TrafficEvent currentEvent) {
        this.currentEvent = currentEvent;
        this.changeState(State.TRACK_DISTANCE);
    }
    
    private void trackJam(final int currentRemainingTimeInJam) {
        this.currentRemainingTimeInJam = currentRemainingTimeInJam;
        this.changeState(State.TRACK_JAM);
    }
    
    private MapEvents.LiveTrafficEvent transformToLiveTrafficEvent(final TrafficEvent trafficEvent) {
        MapEvents.LiveTrafficEvent.Type type;
        if (trafficEvent.isFlow()) {
            type = MapEvents.LiveTrafficEvent.Type.CONGESTION;
        }
        else {
            type = MapEvents.LiveTrafficEvent.Type.INCIDENT;
        }
        MapEvents.LiveTrafficEvent.Severity severity;
        if (trafficEvent.getSeverity().getValue() > TrafficEvent.Severity.HIGH.getValue()) {
            severity = MapEvents.LiveTrafficEvent.Severity.VERY_HIGH;
        }
        else {
            severity = MapEvents.LiveTrafficEvent.Severity.HIGH;
        }
        return new MapEvents.LiveTrafficEvent(type, severity);
    }
    
    public void activate(final Route currentRoute) {
        this.currentRoute = currentRoute;
        this.changeState(State.ACTIVE);
    }
    
    @Subscribe
    public void onDistanceTrackEvent(final DistanceTrackEvent distanceTrackEvent) {
        if (this.isCurrentRoute(distanceTrackEvent.route)) {
            this.trackEvent(distanceTrackEvent.trafficEvent);
        }
    }
    
    @Subscribe
    public void onJamTrackEvent(final JamTrackEvent jamTrackEvent) {
        if (this.isCurrentRoute(jamTrackEvent.route)) {
            this.currentEvent = jamTrackEvent.trafficEvent;
            this.trackJam(jamTrackEvent.remainingTimeInJam);
        }
    }
    
    @Subscribe
    public void onTrackDismissEvent(final TrackDismissEvent trackDismissEvent) {
        if (this.isCurrentRoute(trackDismissEvent.route)) {
            this.changeState(State.ACTIVE);
        }
    }
    
    @Subscribe
    public void onTrackingUpdate(final HerePositionUpdateListener.MapMatchEvent mapMatchEvent) {
        try {
            if (this.currentState == State.TRACK_DISTANCE) {
                this.onDistanceTrackingUpdate(mapMatchEvent);
            }
            else if (this.currentState == State.TRACK_JAM) {
                this.onJamTrackingUpdate();
            }
        }
        catch (Throwable t) {
            this.logger.e(t);
            CrashReporter.getInstance().reportNonFatalException(t);
        }
    }
    
    public void stop() {
        this.changeState(State.STOPPED);
    }
    
    private class DistanceTrackEvent
    {
        public final Route route;
        public final TrafficEvent trafficEvent;
        
        public DistanceTrackEvent(final Route route, final TrafficEvent trafficEvent) {
            this.route = route;
            this.trafficEvent = trafficEvent;
        }
    }
    
    private class JamTrackEvent
    {
        public final int remainingTimeInJam;
        public final Route route;
        public final TrafficEvent trafficEvent;
        
        public JamTrackEvent(final Route route, final TrafficEvent trafficEvent, final int remainingTimeInJam) {
            this.route = route;
            this.trafficEvent = trafficEvent;
            this.remainingTimeInJam = remainingTimeInJam;
        }
    }
    
    public enum State
    {
        ACTIVE, 
        STOPPED, 
        TRACK_DISTANCE, 
        TRACK_JAM;
    }
    
    private class TrackDismissEvent
    {
        public final Route route;
        
        public TrackDismissEvent(final Route route) {
            this.route = route;
        }
    }
}
