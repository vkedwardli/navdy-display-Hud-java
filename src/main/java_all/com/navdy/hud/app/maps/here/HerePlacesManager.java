package com.navdy.hud.app.maps.here;

import com.navdy.hud.app.framework.network.NetworkStateManager;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.here.android.mpa.search.SearchRequest;
import com.navdy.service.library.events.location.Coordinate;
import com.navdy.service.library.events.places.PlacesSearchResult;
import com.navdy.service.library.events.places.PlacesSearchResponse;
import com.squareup.wire.Message;
import com.navdy.service.library.events.places.AutoCompleteResponse;
import com.navdy.hud.app.maps.MapsEventHandler;
import com.navdy.service.library.events.places.PlacesSearchRequest;
import com.here.android.mpa.search.Category;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.Iterator;
import com.here.android.mpa.search.DiscoveryResult;
import com.here.android.mpa.search.DiscoveryResultPage;
import com.here.android.mpa.search.ExploreRequest;
import com.here.android.mpa.search.CategoryFilter;
import android.text.TextUtils;
import android.os.SystemClock;
import com.navdy.service.library.task.TaskManager;
import com.navdy.service.library.events.places.AutoCompleteRequest;
import com.here.android.mpa.search.NavigationPosition;
import com.here.android.mpa.search.Location;
import com.here.android.mpa.search.GeocodeRequest;
import com.here.android.mpa.search.ErrorCode;
import com.here.android.mpa.search.ResultListener;
import com.here.android.mpa.search.TextSuggestionRequest;
import com.here.android.mpa.common.GeoCoordinate;
import com.navdy.service.library.events.RequestStatus;
import com.here.android.mpa.search.Place;
import java.util.List;
import com.navdy.hud.app.HudApplication;
import com.navdy.service.library.log.Logger;
import android.content.Context;
import com.here.android.mpa.search.PlaceLink;
import java.util.ArrayList;

public class HerePlacesManager
{
    private static final ArrayList<String> EMPTY_AUTO_COMPLETE_RESULT;
    private static final ArrayList<PlaceLink> EMPTY_RESULT;
    public static final String HERE_PLACE_TYPE_ATM = "atm-bank-exchange";
    public static final String HERE_PLACE_TYPE_COFFEE = "coffee-tea";
    public static final String HERE_PLACE_TYPE_GAS = "petrol-station";
    public static final String HERE_PLACE_TYPE_HOSPITAL = "hospital-health-care-facility";
    public static final String HERE_PLACE_TYPE_PARKING = "parking-facility";
    public static final String[] HERE_PLACE_TYPE_RESTAURANT;
    public static final String HERE_PLACE_TYPE_STORE = "shopping";
    private static final int MAX_RESULTS_AUTOCOMPLETE = 10;
    private static final int MAX_RESULTS_SEARCH = 15;
    private static final Context context;
    private static final HereMapsManager hereMapsManager;
    private static final Logger sLogger;
    private static volatile boolean willRestablishOnline;
    
    static {
        sLogger = new Logger(HerePlacesManager.class);
        EMPTY_RESULT = new ArrayList<PlaceLink>(0);
        EMPTY_AUTO_COMPLETE_RESULT = new ArrayList<String>(0);
        hereMapsManager = HereMapsManager.getInstance();
        context = HudApplication.getAppContext();
        HERE_PLACE_TYPE_RESTAURANT = new String[] { "restaurant", "snacks-fast-food" };
    }
    
    public static void autoComplete(final GeoCoordinate searchCenter, final String s, final int n, final int collectionSize, final AutoCompleteCallback autoCompleteCallback) {
        final TextSuggestionRequest textSuggestionRequest = new TextSuggestionRequest(s);
        textSuggestionRequest.setCollectionSize(collectionSize);
        textSuggestionRequest.setSearchCenter(searchCenter);
        final ErrorCode execute = textSuggestionRequest.execute(new ResultListener<List<String>>() {
            @Override
            public void onCompleted(final List<String> list, final ErrorCode errorCode) {
                try {
                    autoCompleteCallback.result(errorCode, list);
                }
                catch (Throwable t) {
                    HerePlacesManager.sLogger.e(t);
                }
            }
        });
        if (execute == ErrorCode.NONE) {
            return;
        }
        try {
            autoCompleteCallback.result(execute, null);
        }
        catch (Throwable t) {
            HerePlacesManager.sLogger.e(t);
        }
    }
    
    private static void establishOffline() {
        HerePlacesManager.willRestablishOnline = HereMapsManager.getInstance().isEngineOnline();
        if (HerePlacesManager.willRestablishOnline) {
            HerePlacesManager.sLogger.v("turn engine offline");
            HereMapsManager.getInstance().turnOffline();
        }
    }
    
    public static GeocodeRequest geoCodeStreetAddress(final GeoCoordinate geoCoordinate, final String s, final int n, final GeoCodeCallback geoCodeCallback) {
        final GeocodeRequest geocodeRequest = new GeocodeRequest(s);
        if (geoCoordinate != null) {
            geocodeRequest.setSearchArea(geoCoordinate, n);
        }
        final ErrorCode execute = geocodeRequest.execute(new ResultListener<List<Location>>() {
            @Override
            public void onCompleted(final List<Location> list, final ErrorCode errorCode) {
                try {
                    geoCodeCallback.result(errorCode, list);
                }
                catch (Throwable t) {
                    HerePlacesManager.sLogger.e(t);
                }
            }
        });
        GeocodeRequest geocodeRequest2 = geocodeRequest;
        if (execute == ErrorCode.NONE) {
            return geocodeRequest2;
        }
        try {
            geoCodeCallback.result(execute, null);
            geocodeRequest2 = null;
            return geocodeRequest2;
        }
        catch (Throwable t) {
            HerePlacesManager.sLogger.e(t);
            return null;
        }
    }
    
    public static GeoCoordinate getPlaceEntry(final Place place) {
        GeoCoordinate geoCoordinate;
        if (place.getLocation().getAccessPoints().size() > 0) {
            geoCoordinate = place.getLocation().getAccessPoints().get(0).getCoordinate();
        }
        else {
            geoCoordinate = place.getLocation().getCoordinate();
        }
        return geoCoordinate;
    }
    
    public static void handleAutoCompleteRequest(final AutoCompleteRequest autoCompleteRequest) {
        TaskManager.getInstance().execute(new Runnable() {
            @Override
            public void run() {
                final long elapsedRealtime = SystemClock.elapsedRealtime();
                final String partialSearch = autoCompleteRequest.partialSearch;
                Label_0159: {
                    if (autoCompleteRequest.searchArea == null) {
                        break Label_0159;
                    }
                    int intValue = autoCompleteRequest.searchArea;
                Label_0056_Outer:
                    while (true) {
                        Label_0165: {
                            if (autoCompleteRequest.maxResults == null) {
                                break Label_0165;
                            }
                            int intValue2 = autoCompleteRequest.maxResults;
                            while (true) {
                                int n;
                                if (intValue2 <= 0 || (n = intValue2) > 10) {
                                    n = 10;
                                }
                                try {
                                    HerePlacesManager.printAutoCompleteRequest(autoCompleteRequest);
                                    if (!HerePlacesManager.hereMapsManager.isInitialized()) {
                                        HerePlacesManager.sLogger.i("engine not ready");
                                        returnAutoCompleteErrorResponse(partialSearch, RequestStatus.REQUEST_NOT_READY, HerePlacesManager.context.getString(R.string.map_engine_not_ready));
                                    }
                                    else if (TextUtils.isEmpty((CharSequence)partialSearch)) {
                                        HerePlacesManager.sLogger.i("invalid autocomplete query");
                                        returnAutoCompleteErrorResponse(partialSearch, RequestStatus.REQUEST_INVALID_REQUEST, HerePlacesManager.context.getString(R.string.empty_search_query));
                                        if (HerePlacesManager.sLogger.isLoggable(2)) {
                                            HerePlacesManager.sLogger.v("handleAutoCompleteRequest- time-1 =" + (SystemClock.elapsedRealtime() - elapsedRealtime));
                                        }
                                    }
                                    else {
                                        final HereLocationFixManager locationFixManager = HerePlacesManager.hereMapsManager.getLocationFixManager();
                                        if (locationFixManager == null) {
                                            HerePlacesManager.sLogger.i("engine not ready, location fix manager n/a");
                                            returnAutoCompleteErrorResponse(partialSearch, RequestStatus.REQUEST_NOT_READY, HerePlacesManager.context.getString(R.string.map_engine_not_ready));
                                            if (HerePlacesManager.sLogger.isLoggable(2)) {
                                                HerePlacesManager.sLogger.v("handleAutoCompleteRequest- time-1 =" + (SystemClock.elapsedRealtime() - elapsedRealtime));
                                            }
                                        }
                                        else {
                                            final GeoCoordinate lastGeoCoordinate = locationFixManager.getLastGeoCoordinate();
                                            if (lastGeoCoordinate == null) {
                                                HerePlacesManager.sLogger.i("start location n/a");
                                                returnAutoCompleteErrorResponse(partialSearch, RequestStatus.REQUEST_NO_LOCATION_SERVICE, HerePlacesManager.context.getString(R.string.no_current_position));
                                                if (HerePlacesManager.sLogger.isLoggable(2)) {
                                                    HerePlacesManager.sLogger.v("handleAutoCompleteRequest- time-1 =" + (SystemClock.elapsedRealtime() - elapsedRealtime));
                                                }
                                            }
                                            else {
                                                HerePlacesManager.autoComplete(lastGeoCoordinate, partialSearch, intValue, n, (AutoCompleteCallback)new AutoCompleteCallback() {
                                                    @Override
                                                    public void result(final ErrorCode errorCode, final List<String> list) {
                                                        TaskManager.getInstance().execute(new Runnable() {
                                                            @Override
                                                            public void run() {
                                                                try {
                                                                    if (errorCode != ErrorCode.NONE) {
                                                                        returnAutoCompleteErrorResponse(partialSearch, RequestStatus.REQUEST_SERVICE_ERROR, HerePlacesManager.context.getString(R.string.search_error, new Object[] { errorCode.toString() }));
                                                                    }
                                                                    else {
                                                                        if (list == null || list.size() == 0) {
                                                                            HerePlacesManager.sLogger.d("no results");
                                                                            returnAutoCompleteResults(partialSearch, n, HerePlacesManager.EMPTY_AUTO_COMPLETE_RESULT);
                                                                        }
                                                                        else {
                                                                            HerePlacesManager.sLogger.d("returned results:" + list.size());
                                                                            returnAutoCompleteResults(partialSearch, n, list);
                                                                        }
                                                                        if (HerePlacesManager.sLogger.isLoggable(2)) {
                                                                            HerePlacesManager.sLogger.v("handleAutoCompleteRequest- time-2 =" + (SystemClock.elapsedRealtime() - elapsedRealtime));
                                                                        }
                                                                    }
                                                                }
                                                                catch (Throwable t) {
                                                                    HerePlacesManager.sLogger.e(t);
                                                                    returnAutoCompleteErrorResponse(partialSearch, RequestStatus.REQUEST_UNKNOWN_ERROR, HerePlacesManager.context.getString(R.string.search_error));
                                                                    if (HerePlacesManager.sLogger.isLoggable(2)) {
                                                                        HerePlacesManager.sLogger.v("handleAutoCompleteRequest- time-2 =" + (SystemClock.elapsedRealtime() - elapsedRealtime));
                                                                    }
                                                                }
                                                                finally {
                                                                    if (HerePlacesManager.sLogger.isLoggable(2)) {
                                                                        HerePlacesManager.sLogger.v("handleAutoCompleteRequest- time-2 =" + (SystemClock.elapsedRealtime() - elapsedRealtime));
                                                                    }
                                                                }
                                                            }
                                                        }, 19);
                                                    }
                                                });
                                                if (HerePlacesManager.sLogger.isLoggable(2)) {
                                                    HerePlacesManager.sLogger.v("handleAutoCompleteRequest- time-1 =" + (SystemClock.elapsedRealtime() - elapsedRealtime));
                                                }
                                            }
                                        }
                                    }
                                    return;
                                    intValue2 = 10;
                                    continue;
                                    intValue = -1;
                                    continue Label_0056_Outer;
                                }
                                catch (Throwable t) {
                                    HerePlacesManager.sLogger.e(t);
                                    returnAutoCompleteErrorResponse(partialSearch, RequestStatus.REQUEST_UNKNOWN_ERROR, HerePlacesManager.context.getString(R.string.search_error));
                                    if (HerePlacesManager.sLogger.isLoggable(2)) {
                                        HerePlacesManager.sLogger.v("handleAutoCompleteRequest- time-1 =" + (SystemClock.elapsedRealtime() - elapsedRealtime));
                                    }
                                }
                                finally {
                                    if (HerePlacesManager.sLogger.isLoggable(2)) {
                                        HerePlacesManager.sLogger.v("handleAutoCompleteRequest- time-1 =" + (SystemClock.elapsedRealtime() - elapsedRealtime));
                                    }
                                }
                                break;
                            }
                        }
                        break;
                    }
                }
            }
        }, 19);
    }
    
    public static void handleCategoriesRequest(final CategoryFilter categoryFilter, final int n, final OnCategoriesSearchListener onCategoriesSearchListener) {
        handleCategoriesRequest(categoryFilter, n, (OnCategoriesSearchListener)new OnCategoriesSearchListener() {
            @Override
            public void onCompleted(final List<Place> list) {
                onCategoriesSearchListener.onCompleted(list);
            }
            
            @Override
            public void onError(final Error error) {
                HerePlacesManager.handleCategoriesRequest(categoryFilter, n, (OnCategoriesSearchListener)new OnCategoriesSearchListener() {
                    @Override
                    public void onCompleted(final List<Place> list) {
                        onCategoriesSearchListener.onCompleted(list);
                    }
                    
                    @Override
                    public void onError(final Error error) {
                        onCategoriesSearchListener.onError(error);
                    }
                }, true);
            }
        }, false);
    }
    
    public static void handleCategoriesRequest(final CategoryFilter categoryFilter, final int n, final OnCategoriesSearchListener onCategoriesSearchListener, final boolean b) {
        TaskManager.getInstance().execute(new Runnable() {
            @Override
            public void run() {
                final long elapsedRealtime = SystemClock.elapsedRealtime();
                if (categoryFilter == null || n <= 0 || onCategoriesSearchListener == null) {
                    throw new IllegalArgumentException();
                }
                Label_0339: {
                    try {
                        if (!HerePlacesManager.hereMapsManager.isInitialized()) {
                            HerePlacesManager.sLogger.i("engine not ready");
                            onCategoriesSearchListener.onError(Error.NO_MAP_ENGINE);
                        }
                        else {
                            final HereLocationFixManager locationFixManager = HerePlacesManager.hereMapsManager.getLocationFixManager();
                            if (locationFixManager == null) {
                                HerePlacesManager.sLogger.i("engine not ready, location fix manager n/a");
                                onCategoriesSearchListener.onError(Error.NO_USER_LOCATION);
                                HerePlacesManager.sLogger.v("handleCategoriesRequest time-1=" + (SystemClock.elapsedRealtime() - elapsedRealtime));
                            }
                            else {
                                GeoCoordinate geoCoordinate = HerePlacesManager.hereMapsManager.getRouteStartPoint();
                                if (geoCoordinate == null) {
                                    geoCoordinate = locationFixManager.getLastGeoCoordinate();
                                }
                                else {
                                    HerePlacesManager.sLogger.v("using debug start point:" + geoCoordinate);
                                }
                                if (geoCoordinate != null) {
                                    break Label_0339;
                                }
                                HerePlacesManager.sLogger.i("start location n/a");
                                onCategoriesSearchListener.onError(Error.NO_USER_LOCATION);
                                HerePlacesManager.sLogger.v("handleCategoriesRequest time-1=" + (SystemClock.elapsedRealtime() - elapsedRealtime));
                            }
                        }
                    }
                    catch (Throwable t) {
                        HerePlacesManager.sLogger.e("HERE internal DiscoveryRequest.execute exception: ", t);
                        tryRestablishOnline();
                        onCategoriesSearchListener.onError(Error.UNKNOWN_ERROR);
                        HerePlacesManager.sLogger.v("handleCategoriesRequest time-1=" + (SystemClock.elapsedRealtime() - elapsedRealtime));
                        return;
                        // iftrue(Label_0498:, !this.val$offline)
                        // iftrue(Label_0464:, execute == ErrorCode.NONE)
                        // iftrue(Label_0349:, HereMapsManager.getInstance().isEngineOnline())
                        while (true) {
                            Block_9: {
                            Label_0464:
                                while (true) {
                                    onCategoriesSearchListener.onError(Error.BAD_REQUEST);
                                    HerePlacesManager.sLogger.v("handleCategoriesRequest time-1=" + (SystemClock.elapsedRealtime() - elapsedRealtime));
                                    return;
                                    break Block_9;
                                    final GeoCoordinate searchCenter;
                                    final ErrorCode execute = new ExploreRequest().setCategoryFilter(categoryFilter).setSearchCenter(searchCenter).setCollectionSize(n).execute(new ResultListener<DiscoveryResultPage>() {
                                        @Override
                                        public void onCompleted(final DiscoveryResultPage discoveryResultPage, final ErrorCode errorCode) {
                                            TaskManager.getInstance().execute(new Runnable() {
                                                @Override
                                                public void run() {
                                                    Label_0328: {
                                                        try {
                                                            if (errorCode != ErrorCode.NONE) {
                                                                HerePlacesManager.sLogger.e("Error in nearby categories response: " + errorCode.name());
                                                                tryRestablishOnline();
                                                                onCategoriesSearchListener.onError(Error.RESPONSE_ERROR);
                                                                return;
                                                            }
                                                            final List<DiscoveryResult> items = discoveryResultPage.getItems();
                                                            final ArrayList<PlaceLink> list = new ArrayList<PlaceLink>();
                                                            for (final DiscoveryResult discoveryResult : items) {
                                                                if (HerePlacesManager.sLogger.isLoggable(2)) {
                                                                    HerePlacesManager.sLogger.v("Found a nearby category item: " + discoveryResult.getTitle());
                                                                }
                                                                if (discoveryResult.getResultType() == DiscoveryResult.ResultType.PLACE) {
                                                                    HerePlacesManager.sLogger.v("Discovered a place: " + discoveryResult.getTitle());
                                                                    list.add((PlaceLink)discoveryResult);
                                                                }
                                                            }
                                                            break Label_0328;
                                                        }
                                                        catch (Throwable t) {
                                                            HerePlacesManager.sLogger.e("HERE internal DiscoveryRequest.execute callback exception: ", t);
                                                            tryRestablishOnline();
                                                            onCategoriesSearchListener.onError(Error.UNKNOWN_ERROR);
                                                            HerePlacesManager.sLogger.v("handleCategoriesRequest- time-2 =" + (SystemClock.elapsedRealtime() - elapsedRealtime));
                                                            return;
                                                            Label_0406: {
                                                                final List list2;
                                                                handlePlaceLinksRequest(list2, onCategoriesSearchListener);
                                                            }
                                                            HerePlacesManager.sLogger.v("handleCategoriesRequest- time-2 =" + (SystemClock.elapsedRealtime() - elapsedRealtime));
                                                            return;
                                                            while (true) {
                                                                HerePlacesManager.sLogger.e("no places links found");
                                                                tryRestablishOnline();
                                                                onCategoriesSearchListener.onError(Error.RESPONSE_ERROR);
                                                                HerePlacesManager.sLogger.v("handleCategoriesRequest- time-2 =" + (SystemClock.elapsedRealtime() - elapsedRealtime));
                                                                return;
                                                                continue;
                                                            }
                                                        }
                                                        // iftrue(Label_0406:, list2.size() != 0)
                                                        finally {
                                                            HerePlacesManager.sLogger.v("handleCategoriesRequest- time-2 =" + (SystemClock.elapsedRealtime() - elapsedRealtime));
                                                        }
                                                    }
                                                }
                                            }, 2);
                                        }
                                    });
                                    HerePlacesManager.sLogger.e("Error while requesting nearby gas stations: " + execute.name());
                                    tryRestablishOnline();
                                    onCategoriesSearchListener.onError(Error.BAD_REQUEST);
                                    break Label_0464;
                                    Label_0498: {
                                        continue;
                                    }
                                }
                                HerePlacesManager.sLogger.v("handleCategoriesRequest time-1=" + (SystemClock.elapsedRealtime() - elapsedRealtime));
                                return;
                            }
                            establishOffline();
                            continue;
                        }
                    }
                    finally {
                        HerePlacesManager.sLogger.v("handleCategoriesRequest time-1=" + (SystemClock.elapsedRealtime() - elapsedRealtime));
                    }
                }
            }
        }, 2);
    }
    
    private static void handlePlaceLinksRequest(final List<PlaceLink> list, final OnCategoriesSearchListener onCategoriesSearchListener) {
        final ArrayList list2 = new ArrayList();
        final AtomicInteger atomicInteger = new AtomicInteger(list.size());
        final Iterator<PlaceLink> iterator = list.iterator();
        while (iterator.hasNext()) {
            final ErrorCode execute = iterator.next().getDetailsRequest().execute(new ResultListener<Place>() {
                @Override
                public void onCompleted(final Place place, final ErrorCode errorCode) {
                    TaskManager.getInstance().execute(new Runnable() {
                        @Override
                        public void run() {
                            while (true) {
                                Label_0217: {
                                    int decrementAndGet = 0;
                                    Label_0176: {
                                        try {
                                            decrementAndGet = atomicInteger.decrementAndGet();
                                            if (errorCode == ErrorCode.NONE && place != null) {
                                                HerePlacesManager.sLogger.v("Found a Place: " + place.getName());
                                                final Iterator<Category> iterator = place.getCategories().iterator();
                                                while (iterator.hasNext()) {
                                                    HerePlacesManager.sLogger.v("place category: " + iterator.next().getName());
                                                }
                                                break Label_0176;
                                            }
                                            break Label_0217;
                                        }
                                        catch (Throwable t) {
                                            HerePlacesManager.sLogger.e("HERE internal PlaceLink.getDetailsRequest callback exception: ", t);
                                            if (atomicInteger.get() == 0) {
                                                invokeCategorySearchListener(list2, onCategoriesSearchListener);
                                            }
                                        }
                                        return;
                                    }
                                    list2.add(place);
                                    if (decrementAndGet == 0) {
                                        invokeCategorySearchListener(list2, onCategoriesSearchListener);
                                        return;
                                    }
                                    return;
                                }
                                HerePlacesManager.sLogger.e("Error in place detail response: " + errorCode.name());
                                continue;
                            }
                        }
                    }, 2);
                }
            });
            if (execute != ErrorCode.NONE) {
                HerePlacesManager.sLogger.e("Error in place detail request: " + execute.name());
                if (atomicInteger.decrementAndGet() != 0 || list2.size() != 0) {
                    continue;
                }
                tryRestablishOnline();
                onCategoriesSearchListener.onError(Error.BAD_REQUEST);
            }
        }
    }
    
    public static void handlePlacesSearchRequest(final PlacesSearchRequest placesSearchRequest) {
        TaskManager.getInstance().execute(new Runnable() {
            @Override
            public void run() {
                final long elapsedRealtime = SystemClock.elapsedRealtime();
                final String searchQuery = placesSearchRequest.searchQuery;
                Label_0159: {
                    if (placesSearchRequest.searchArea == null) {
                        break Label_0159;
                    }
                    int intValue = placesSearchRequest.searchArea;
                Label_0056_Outer:
                    while (true) {
                        Label_0165: {
                            if (placesSearchRequest.maxResults == null) {
                                break Label_0165;
                            }
                            int intValue2 = placesSearchRequest.maxResults;
                            while (true) {
                                int n;
                                if (intValue2 <= 0 || (n = intValue2) > 15) {
                                    n = 15;
                                }
                                try {
                                    HerePlacesManager.printSearchRequest(placesSearchRequest);
                                    if (!HerePlacesManager.hereMapsManager.isInitialized()) {
                                        HerePlacesManager.sLogger.i("[search] engine not ready");
                                        returnSearchErrorResponse(searchQuery, RequestStatus.REQUEST_NOT_READY, HerePlacesManager.context.getString(R.string.map_engine_not_ready));
                                    }
                                    else if (TextUtils.isEmpty((CharSequence)searchQuery)) {
                                        HerePlacesManager.sLogger.i("[search] invalid search query");
                                        returnSearchErrorResponse(searchQuery, RequestStatus.REQUEST_INVALID_REQUEST, HerePlacesManager.context.getString(R.string.empty_search_query));
                                        if (HerePlacesManager.sLogger.isLoggable(2)) {
                                            HerePlacesManager.sLogger.v("[search] handleSearchRequest- time-1 =" + (SystemClock.elapsedRealtime() - elapsedRealtime));
                                        }
                                    }
                                    else {
                                        final HereLocationFixManager locationFixManager = HerePlacesManager.hereMapsManager.getLocationFixManager();
                                        if (locationFixManager == null) {
                                            HerePlacesManager.sLogger.i("[search] engine not ready, location fix manager n/a");
                                            returnSearchErrorResponse(searchQuery, RequestStatus.REQUEST_NOT_READY, HerePlacesManager.context.getString(R.string.map_engine_not_ready));
                                            if (HerePlacesManager.sLogger.isLoggable(2)) {
                                                HerePlacesManager.sLogger.v("[search] handleSearchRequest- time-1 =" + (SystemClock.elapsedRealtime() - elapsedRealtime));
                                            }
                                        }
                                        else {
                                            final GeoCoordinate lastGeoCoordinate = locationFixManager.getLastGeoCoordinate();
                                            if (lastGeoCoordinate == null) {
                                                HerePlacesManager.sLogger.i("start location n/a");
                                                returnSearchErrorResponse(searchQuery, RequestStatus.REQUEST_NO_LOCATION_SERVICE, HerePlacesManager.context.getString(R.string.no_current_position));
                                                if (HerePlacesManager.sLogger.isLoggable(2)) {
                                                    HerePlacesManager.sLogger.v("[search] handleSearchRequest- time-1 =" + (SystemClock.elapsedRealtime() - elapsedRealtime));
                                                }
                                            }
                                            else {
                                                HerePlacesManager.searchPlaces(lastGeoCoordinate, searchQuery, intValue, n, (PlacesSearchCallback)new PlacesSearchCallback() {
                                                    @Override
                                                    public void result(final ErrorCode errorCode, final DiscoveryResultPage discoveryResultPage) {
                                                        TaskManager.getInstance().execute(new Runnable() {
                                                            @Override
                                                            public void run() {
                                                                Label_0368: {
                                                                    while (true) {
                                                                        try {
                                                                            if (errorCode != ErrorCode.NONE) {
                                                                                returnSearchErrorResponse(searchQuery, RequestStatus.REQUEST_SERVICE_ERROR, HerePlacesManager.context.getString(R.string.search_error, new Object[] { errorCode.toString() }));
                                                                            }
                                                                            else {
                                                                                if (discoveryResultPage == null) {
                                                                                    break Label_0368;
                                                                                }
                                                                                final List<PlaceLink> placeLinks = discoveryResultPage.getPlaceLinks();
                                                                                if (placeLinks == null || placeLinks.size() == 0) {
                                                                                    HerePlacesManager.sLogger.d("[search] no results");
                                                                                    returnSearchResults(lastGeoCoordinate, searchQuery, HerePlacesManager.EMPTY_RESULT, n, intValue);
                                                                                }
                                                                                else {
                                                                                    HerePlacesManager.sLogger.d("[search] returned results:" + placeLinks.size());
                                                                                    returnSearchResults(lastGeoCoordinate, searchQuery, placeLinks, n, intValue);
                                                                                }
                                                                                if (HerePlacesManager.sLogger.isLoggable(2)) {
                                                                                    HerePlacesManager.sLogger.v("[search] handleSearchRequest- time-2 =" + (SystemClock.elapsedRealtime() - elapsedRealtime));
                                                                                }
                                                                            }
                                                                        }
                                                                        catch (Throwable t) {
                                                                            HerePlacesManager.sLogger.e(t);
                                                                            returnSearchErrorResponse(searchQuery, RequestStatus.REQUEST_UNKNOWN_ERROR, HerePlacesManager.context.getString(R.string.search_error));
                                                                            if (HerePlacesManager.sLogger.isLoggable(2)) {
                                                                                HerePlacesManager.sLogger.v("[search] handleSearchRequest- time-2 =" + (SystemClock.elapsedRealtime() - elapsedRealtime));
                                                                                return;
                                                                            }
                                                                            return;
                                                                            HerePlacesManager.sLogger.d("[search] no results");
                                                                            returnSearchResults(lastGeoCoordinate, searchQuery, HerePlacesManager.EMPTY_RESULT, n, intValue);
                                                                            continue Label_0056_Outer;
                                                                        }
                                                                        finally {
                                                                            if (HerePlacesManager.sLogger.isLoggable(2)) {
                                                                                HerePlacesManager.sLogger.v("[search] handleSearchRequest- time-2 =" + (SystemClock.elapsedRealtime() - elapsedRealtime));
                                                                            }
                                                                        }
                                                                        break;
                                                                    }
                                                                }
                                                            }
                                                        }, 19);
                                                    }
                                                });
                                                if (HerePlacesManager.sLogger.isLoggable(2)) {
                                                    HerePlacesManager.sLogger.v("[search] handleSearchRequest- time-1 =" + (SystemClock.elapsedRealtime() - elapsedRealtime));
                                                }
                                            }
                                        }
                                    }
                                    return;
                                    intValue = -1;
                                    continue Label_0056_Outer;
                                    intValue2 = 15;
                                    continue;
                                }
                                catch (Throwable t) {
                                    HerePlacesManager.sLogger.e(t);
                                    returnSearchErrorResponse(searchQuery, RequestStatus.REQUEST_UNKNOWN_ERROR, HerePlacesManager.context.getString(R.string.search_error));
                                    if (HerePlacesManager.sLogger.isLoggable(2)) {
                                        HerePlacesManager.sLogger.v("[search] handleSearchRequest- time-1 =" + (SystemClock.elapsedRealtime() - elapsedRealtime));
                                    }
                                }
                                finally {
                                    if (HerePlacesManager.sLogger.isLoggable(2)) {
                                        HerePlacesManager.sLogger.v("[search] handleSearchRequest- time-1 =" + (SystemClock.elapsedRealtime() - elapsedRealtime));
                                    }
                                }
                                break;
                            }
                        }
                        break;
                    }
                }
            }
        }, 19);
    }
    
    private static void invokeCategorySearchListener(final List<Place> list, final OnCategoriesSearchListener onCategoriesSearchListener) {
        final int size = list.size();
        if (size == 0) {
            HerePlacesManager.sLogger.v("places query complete, no result");
            tryRestablishOnline();
            onCategoriesSearchListener.onError(Error.RESPONSE_ERROR);
        }
        else {
            HerePlacesManager.sLogger.v("places query complete:" + size);
            tryRestablishOnline();
            onCategoriesSearchListener.onCompleted(list);
        }
    }
    
    public static void printAutoCompleteRequest(final AutoCompleteRequest autoCompleteRequest) {
        try {
            HerePlacesManager.sLogger.v("AutoCompleteRequest query[" + autoCompleteRequest.partialSearch + "] maxResults[" + autoCompleteRequest.maxResults + "]");
        }
        catch (Throwable t) {
            HerePlacesManager.sLogger.e(t);
        }
    }
    
    public static void printSearchRequest(final PlacesSearchRequest placesSearchRequest) {
        try {
            HerePlacesManager.sLogger.v("PlacesSearchRequest query[" + placesSearchRequest.searchQuery + "] area[" + placesSearchRequest.searchArea + "] maxResults[" + placesSearchRequest.maxResults + "]");
        }
        catch (Throwable t) {
            HerePlacesManager.sLogger.e(t);
        }
    }
    
    private static void returnAutoCompleteErrorResponse(final String s, final RequestStatus requestStatus, final String s2) {
        try {
            HerePlacesManager.sLogger.e("[autocomplete]" + requestStatus + ": " + s2);
            MapsEventHandler.getInstance().sendEventToClient(new AutoCompleteResponse(s, requestStatus, s2, null));
        }
        catch (Throwable t) {
            HerePlacesManager.sLogger.e(t);
        }
    }
    
    private static void returnAutoCompleteResults(final String s, final int n, final List<String> list) {
        TaskManager.getInstance().execute(new Runnable() {
            @Override
            public void run() {
                try {
                    List<String> list = list;
                    if (list.size() > n) {
                        list = list.subList(0, n);
                    }
                    MapsEventHandler.getInstance().sendEventToClient(new AutoCompleteResponse(s, RequestStatus.REQUEST_SUCCESS, null, list));
                    HerePlacesManager.sLogger.v("[autocomplete] returned:" + list.size());
                }
                catch (Throwable t) {
                    HerePlacesManager.sLogger.e(t);
                    returnSearchErrorResponse(s, RequestStatus.REQUEST_UNKNOWN_ERROR, HerePlacesManager.context.getString(R.string.search_error));
                }
            }
        }, 2);
    }
    
    private static void returnSearchErrorResponse(final String s, final RequestStatus requestStatus, final String s2) {
        try {
            HerePlacesManager.sLogger.e("[search]" + requestStatus + ": " + s2);
            MapsEventHandler.getInstance().sendEventToClient(new PlacesSearchResponse.Builder().searchQuery(s).status(requestStatus).statusDetail(s2).results(null).build());
        }
        catch (Throwable t) {
            HerePlacesManager.sLogger.e(t);
        }
    }
    
    private static void returnSearchResults(final GeoCoordinate geoCoordinate, final String s, final List<PlaceLink> list, final int n, final int n2) {
        TaskManager.getInstance().execute(new Runnable() {
            @Override
            public void run() {
                int n = 0;
                try {
                    final ArrayList<PlacesSearchResult> list = new ArrayList<PlacesSearchResult>(n);
                    for (final PlaceLink placeLink : list) {
                        final GeoCoordinate position = placeLink.getPosition();
                        if (position != null && (geoCoordinate == null || n2 <= 0 || geoCoordinate.distanceTo(position) <= n2)) {
                            final Coordinate build = new Coordinate.Builder().latitude(position.getLatitude()).longitude(position.getLongitude()).build();
                            final String vicinity = placeLink.getVicinity();
                            final String title = placeLink.getTitle();
                            String name = null;
                            final double distance = placeLink.getDistance();
                            String replace;
                            if ((replace = vicinity) != null) {
                                replace = vicinity.replace("<br/>", ", ");
                            }
                            final Category category = placeLink.getCategory();
                            if (category != null) {
                                name = category.getName();
                            }
                            list.add(new PlacesSearchResult(title, null, build, replace, name, distance));
                            if (++n == n) {
                                HerePlacesManager.sLogger.v("[search] max results reached:" + n);
                                break;
                            }
                            continue;
                        }
                    }
                    MapsEventHandler.getInstance().sendEventToClient(new PlacesSearchResponse.Builder().searchQuery(s).status(RequestStatus.REQUEST_SUCCESS).statusDetail(null).results(list).build());
                    HerePlacesManager.sLogger.v("[search] returned:" + list.size());
                }
                catch (Throwable t) {
                    HerePlacesManager.sLogger.e(t);
                    returnSearchErrorResponse(s, RequestStatus.REQUEST_UNKNOWN_ERROR, HerePlacesManager.context.getString(R.string.search_error));
                }
            }
        }, 2);
    }
    
    public static void searchPlaces(final GeoCoordinate searchCenter, final String s, final int n, final int collectionSize, final PlacesSearchCallback placesSearchCallback) {
        final SearchRequest searchRequest = new SearchRequest(s);
        searchRequest.setSearchCenter(searchCenter);
        searchRequest.setCollectionSize(collectionSize);
        final ErrorCode execute = searchRequest.execute(new ResultListener<DiscoveryResultPage>() {
            @Override
            public void onCompleted(final DiscoveryResultPage discoveryResultPage, final ErrorCode errorCode) {
                try {
                    placesSearchCallback.result(errorCode, discoveryResultPage);
                }
                catch (Throwable t) {
                    HerePlacesManager.sLogger.e(t);
                }
            }
        });
        if (execute == ErrorCode.NONE) {
            return;
        }
        try {
            placesSearchCallback.result(execute, null);
        }
        catch (Throwable t) {
            HerePlacesManager.sLogger.e(t);
        }
    }
    
    private static void tryRestablishOnline() {
        if (HerePlacesManager.willRestablishOnline && !HereMapsManager.getInstance().isEngineOnline() && RemoteDeviceManager.getInstance().isRemoteDeviceConnected() && NetworkStateManager.isConnectedToNetwork(HudApplication.getAppContext())) {
            HerePlacesManager.sLogger.v("turn engine online");
            HereMapsManager.getInstance().turnOnline();
        }
        HerePlacesManager.willRestablishOnline = false;
    }
    
    public interface AutoCompleteCallback
    {
        void result(final ErrorCode p0, final List<String> p1);
    }
    
    public enum Error
    {
        BAD_REQUEST, 
        NO_MAP_ENGINE, 
        NO_ROUTES, 
        NO_USER_LOCATION, 
        RESPONSE_ERROR, 
        UNKNOWN_ERROR;
    }
    
    public interface GeoCodeCallback
    {
        void result(final ErrorCode p0, final List<Location> p1);
    }
    
    public interface OnCategoriesSearchListener
    {
        void onCompleted(final List<Place> p0);
        
        void onError(final Error p0);
    }
    
    public interface PlacesSearchCallback
    {
        void result(final ErrorCode p0, final DiscoveryResultPage p1);
    }
}
