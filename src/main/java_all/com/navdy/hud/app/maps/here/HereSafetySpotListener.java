package com.navdy.hud.app.maps.here;

import com.here.android.mpa.guidance.SafetySpotNotification;
import com.navdy.hud.app.framework.voice.TTSUtils;
import com.navdy.service.library.events.audio.SpeechRequest;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.manager.SpeedManager;
import com.navdy.hud.app.maps.util.DistanceConverter;
import com.navdy.hud.app.framework.DriverProfileHelper;
import java.util.Iterator;
import com.here.android.mpa.mapping.SafetySpotInfo;
import java.util.Collection;
import com.here.android.mpa.mapping.MapObject;
import com.here.android.mpa.common.Image;
import com.here.android.mpa.mapping.MapMarker;
import com.here.android.mpa.guidance.SafetySpotNotificationInfo;
import com.here.android.mpa.common.GeoCoordinate;
import android.os.Looper;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import java.util.List;
import android.os.Handler;
import com.squareup.otto.Bus;
import com.navdy.service.library.log.Logger;
import com.here.android.mpa.guidance.NavigationManager;

public class HereSafetySpotListener extends SafetySpotListener
{
    private static final int CHECK_INTERVAL;
    private static final int SAFETY_SPOT_HIDE_DISTANCE = 1000;
    private static final Logger sLogger;
    private final Bus bus;
    private boolean canDrawOnTheMap;
    private final Runnable checkExpiredSpots;
    private final Handler handler;
    private final HereMapController mapController;
    private List<SafetySpotContainer> safetySpotNotifications;
    
    static {
        sLogger = new Logger("SafetySpotListener");
        CHECK_INTERVAL = (int)TimeUnit.SECONDS.toMillis(30L);
    }
    
    HereSafetySpotListener(final Bus bus, final HereMapController mapController) {
        this.canDrawOnTheMap = true;
        this.safetySpotNotifications = new ArrayList<SafetySpotContainer>();
        this.handler = new Handler(Looper.getMainLooper());
        this.checkExpiredSpots = new Runnable() {
            @Override
            public void run() {
                try {
                    final List access$000 = HereSafetySpotListener.this.removeExpiredSpots();
                    if (HereSafetySpotListener.this.canDrawOnTheMap) {
                        HereSafetySpotListener.this.removeMapMarkers(access$000);
                    }
                }
                catch (Throwable t) {
                    HereSafetySpotListener.sLogger.e("check", t);
                    HereSafetySpotListener.this.handler.postDelayed((Runnable)this, (long)HereSafetySpotListener.CHECK_INTERVAL);
                }
                finally {
                    HereSafetySpotListener.this.handler.postDelayed((Runnable)this, (long)HereSafetySpotListener.CHECK_INTERVAL);
                }
            }
        };
        HereSafetySpotListener.sLogger.v("ctor");
        this.bus = bus;
        this.mapController = mapController;
    }
    
    private void addSafetySpotMarkers(List<SafetySpotNotificationInfo> list, final GeoCoordinate geoCoordinate) {
        final SafetySpotInfo.Type type = null;
        int n = 0;
        final ArrayList<SafetySpotContainer> list2 = new ArrayList<SafetySpotContainer>();
        final ArrayList<MapMarker> list3 = new ArrayList<MapMarker>();
        final Iterator<SafetySpotNotificationInfo> iterator = list.iterator();
        list = (List<SafetySpotNotificationInfo>)type;
        while (iterator.hasNext()) {
            final SafetySpotNotificationInfo safetySpotNotificationInfo = iterator.next();
            try {
                final SafetySpotContainer safetySpotContainer = new SafetySpotContainer();
                safetySpotContainer.safetySpotNotificationInfo = safetySpotNotificationInfo;
                final SafetySpotInfo safetySpot = safetySpotNotificationInfo.getSafetySpot();
                final SafetySpotInfo.Type type2 = safetySpot.getType();
                final GeoCoordinate coordinate = safetySpot.getCoordinate();
                final Image image = new Image();
                switch (type2) {
                    default:
                        image.setImageResource(R.drawable.icon_red_light_camera);
                        break;
                    case SPEED_CAMERA:
                        image.setImageResource(R.drawable.icon_speed_camera);
                        break;
                }
                safetySpotContainer.mapMarker = new MapMarker(coordinate, image);
                if (this.canDrawOnTheMap) {
                    list3.add(safetySpotContainer.mapMarker);
                }
                list2.add(safetySpotContainer);
                final int n2 = (int)geoCoordinate.distanceTo(coordinate);
                final int n3 = (int)safetySpotNotificationInfo.getDistance();
                HereSafetySpotListener.sLogger.i("add safety spot id[" + System.identityHashCode(safetySpotNotificationInfo) + "] type[" + type2 + "] distance calc[" + n2 + "] distance[" + n3 + "]");
                if (list == null || n > n3) {
                    list = (List<SafetySpotNotificationInfo>)type2;
                    n = n3;
                    continue;
                }
                continue;
            }
            catch (Throwable t) {
                HereSafetySpotListener.sLogger.e("map marker error", t);
                continue;
            }
            break;
        }
        if (this.canDrawOnTheMap) {
            this.mapController.addMapObjects((List<MapObject>)list3);
        }
        this.safetySpotNotifications.addAll(list2);
        if (list != null && n > 0) {
            this.sendTts((SafetySpotInfo.Type)list, n);
        }
    }
    
    private boolean isSafetySpotRelevant(final SafetySpotNotificationInfo safetySpotNotificationInfo, final GeoCoordinate geoCoordinate) {
        final SafetySpotInfo safetySpot = safetySpotNotificationInfo.getSafetySpot();
        final int n = (int)geoCoordinate.distanceTo(safetySpotNotificationInfo.getSafetySpot().getCoordinate());
        HereSafetySpotListener.sLogger.i("check safety spot id[" + System.identityHashCode(safetySpotNotificationInfo) + "] type[" + safetySpot.getType() + "] distance calc[" + n + "] distance[" + safetySpotNotificationInfo.getDistance() + "]");
        return n >= 1000;
    }
    
    private List<SafetySpotContainer> removeExpiredSpots() {
        List<SafetySpotContainer> list = null;
        final GeoCoordinate lastGeoCoordinate = HereMapsManager.getInstance().getLocationFixManager().getLastGeoCoordinate();
        if (lastGeoCoordinate == null) {
            HereSafetySpotListener.sLogger.e("check:no current location");
        }
        else if (this.safetySpotNotifications.size() != 0) {
            final ArrayList<SafetySpotContainer> list2 = new ArrayList<SafetySpotContainer>();
            list = new ArrayList<SafetySpotContainer>();
            for (final SafetySpotContainer safetySpotContainer : this.safetySpotNotifications) {
                final int identityHashCode = System.identityHashCode(safetySpotContainer.safetySpotNotificationInfo);
                if (this.isSafetySpotRelevant(safetySpotContainer.safetySpotNotificationInfo, lastGeoCoordinate)) {
                    list.add(safetySpotContainer);
                    HereSafetySpotListener.sLogger.i("check remove:" + identityHashCode);
                }
                else {
                    list2.add(safetySpotContainer);
                    HereSafetySpotListener.sLogger.i("check valid:" + identityHashCode);
                }
            }
            this.safetySpotNotifications.clear();
            this.safetySpotNotifications.addAll(list2);
        }
        return list;
    }
    
    private void removeMapMarkers(final List<SafetySpotContainer> list) {
        if (list != null) {
            final ArrayList<MapMarker> list2 = new ArrayList<MapMarker>();
            final Iterator<SafetySpotContainer> iterator = list.iterator();
            while (iterator.hasNext()) {
                list2.add(iterator.next().mapMarker);
            }
            this.mapController.removeMapObjects((List<MapObject>)list2);
        }
    }
    
    private void sendTts(final SafetySpotInfo.Type type, int n) {
        if (Boolean.TRUE.equals(DriverProfileHelper.getInstance().getCurrentProfile().getNavigationPreferences().spokenCameraWarnings)) {
            final DistanceConverter.Distance distance = new DistanceConverter.Distance();
            DistanceConverter.convertToDistance(SpeedManager.getInstance().getSpeedUnit(), n, distance);
            final String formattedDistance = HereManeuverDisplayBuilder.getFormattedDistance(distance.value, distance.unit, true);
            switch (type) {
                default:
                    n = R.string.camera_warning_red_light;
                    break;
                case SPEED_CAMERA:
                    n = R.string.camera_warning_speed;
                    break;
                case SPEED_REDLIGHT_CAMERA:
                    n = R.string.camera_warning_both;
                    break;
            }
            TTSUtils.sendSpeechRequest(HudApplication.getAppContext().getResources().getString(n, new Object[] { formattedDistance }), SpeechRequest.Category.SPEECH_CAMERA_WARNING, null);
        }
    }
    
    @Override
    public void onSafetySpot(final SafetySpotNotification safetySpotNotification) {
        this.handler.post((Runnable)new Runnable() {
            @Override
            public void run() {
                final List<SafetySpotNotificationInfo> safetySpotNotificationInfos = safetySpotNotification.getSafetySpotNotificationInfos();
                if (safetySpotNotificationInfos == null || safetySpotNotificationInfos.size() == 0) {
                    HereSafetySpotListener.sLogger.i("no safety spots");
                }
                else {
                    final GeoCoordinate lastGeoCoordinate = HereMapsManager.getInstance().getLocationFixManager().getLastGeoCoordinate();
                    if (lastGeoCoordinate == null) {
                        HereSafetySpotListener.sLogger.e("no current location");
                    }
                    else {
                        final ArrayList<SafetySpotNotificationInfo> list = new ArrayList<SafetySpotNotificationInfo>();
                        for (final SafetySpotNotificationInfo safetySpotNotificationInfo : safetySpotNotificationInfos) {
                            final SafetySpotInfo safetySpot = safetySpotNotificationInfo.getSafetySpot();
                            if (safetySpot != null && safetySpot.getType() != SafetySpotInfo.Type.UNDEFINED) {
                                list.add(safetySpotNotificationInfo);
                            }
                        }
                        if (list.size() == 0) {
                            HereSafetySpotListener.sLogger.i("no valid safety spots");
                        }
                        else {
                            HereSafetySpotListener.this.handler.removeCallbacks(HereSafetySpotListener.this.checkExpiredSpots);
                            HereSafetySpotListener.this.addSafetySpotMarkers(list, lastGeoCoordinate);
                            HereSafetySpotListener.this.handler.postDelayed(HereSafetySpotListener.this.checkExpiredSpots, (long)HereSafetySpotListener.CHECK_INTERVAL);
                        }
                    }
                }
            }
        });
    }
    
    public void setSafetySpotsEnabledOnMap(final boolean b) {
        this.handler.post((Runnable)new Runnable() {
            @Override
            public void run() {
                if (HereSafetySpotListener.this.canDrawOnTheMap != b) {
                    HereSafetySpotListener.this.canDrawOnTheMap = b;
                    if (HereSafetySpotListener.this.canDrawOnTheMap) {
                        HereSafetySpotListener.sLogger.d("Enabling the safety spots on the Map, Total spots Valid : " + HereSafetySpotListener.this.safetySpotNotifications.size() + ", Invalid : " + HereSafetySpotListener.this.removeExpiredSpots());
                        final ArrayList<MapMarker> list = new ArrayList<MapMarker>();
                        final Iterator<SafetySpotContainer> iterator = HereSafetySpotListener.this.safetySpotNotifications.iterator();
                        while (iterator.hasNext()) {
                            list.add(iterator.next().mapMarker);
                        }
                        HereSafetySpotListener.this.mapController.addMapObjects((List<MapObject>)list);
                    }
                    else {
                        HereSafetySpotListener.sLogger.d("Remove all the safety spots from the map");
                        HereSafetySpotListener.this.removeMapMarkers(HereSafetySpotListener.this.safetySpotNotifications);
                    }
                }
            }
        });
    }
    
    private static class SafetySpotContainer
    {
        private MapMarker mapMarker;
        private SafetySpotNotificationInfo safetySpotNotificationInfo;
    }
}
