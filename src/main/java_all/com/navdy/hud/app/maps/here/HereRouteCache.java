package com.navdy.hud.app.maps.here;

import com.here.android.mpa.common.GeoCoordinate;
import com.navdy.service.library.events.navigation.NavigationRouteResult;
import com.navdy.service.library.events.navigation.NavigationRouteRequest;
import com.here.android.mpa.routing.Route;
import com.navdy.service.library.log.Logger;
import android.util.LruCache;

public class HereRouteCache extends LruCache<String, RouteInfo>
{
    public static final int MAX_CACHE_ROUTE_COUNT = 20;
    private static final Logger sLogger;
    private static final HereRouteCache sSingleton;
    
    static {
        sLogger = new Logger(HereRouteCache.class);
        sSingleton = new HereRouteCache();
    }
    
    private HereRouteCache() {
        super(20);
    }
    
    public static HereRouteCache getInstance() {
        return HereRouteCache.sSingleton;
    }
    
    public void addRoute(final String s, final RouteInfo routeInfo) {
        super.put(s, routeInfo);
    }
    
    protected void entryRemoved(final boolean b, final String s, final Route route, final Route route2) {
        if (b && route != null) {
            HereRouteCache.sLogger.v("route removed from cache:" + s);
        }
    }
    
    public RouteInfo getRoute(final String s) {
        RouteInfo routeInfo;
        if (s != null) {
            routeInfo = (RouteInfo)super.get(s);
        }
        else {
            routeInfo = null;
        }
        return routeInfo;
    }
    
    public RouteInfo removeRoute(final String s) {
        return (RouteInfo)super.remove(s);
    }
    
    protected int sizeOf(final String s, final Route route) {
        return 1;
    }
    
    public static class RouteInfo
    {
        public Route route;
        public NavigationRouteRequest routeRequest;
        public NavigationRouteResult routeResult;
        public GeoCoordinate routeStartPoint;
        public boolean traffic;
        
        public RouteInfo(final Route route, final NavigationRouteRequest routeRequest, final NavigationRouteResult routeResult, final boolean traffic, final GeoCoordinate routeStartPoint) {
            this.route = route;
            this.routeRequest = routeRequest;
            this.routeResult = routeResult;
            this.traffic = traffic;
            this.routeStartPoint = routeStartPoint;
        }
    }
}
