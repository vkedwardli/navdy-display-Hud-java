package com.navdy.hud.app.maps.here;

import com.here.android.mpa.routing.Route;
import com.navdy.service.library.task.TaskManager;
import com.navdy.service.library.log.Logger;
import com.here.android.mpa.guidance.NavigationManager;

class HereNavigationEventListener extends NavigationManagerEventListener
{
    private final HereNavigationManager hereNavigationManager;
    private final Logger logger;
    private final String tag;
    
    HereNavigationEventListener(final Logger logger, final String tag, final HereNavigationManager hereNavigationManager) {
        this.logger = logger;
        this.tag = tag;
        this.hereNavigationManager = hereNavigationManager;
    }
    
    @Override
    public void onEnded(final NavigationMode navigationMode) {
        this.logger.i(this.tag + " onEnded:" + navigationMode);
        if (this.hereNavigationManager.isLastManeuver()) {
            this.logger.i(this.tag + " onEnded: last maneuver on, marking arrived");
            this.hereNavigationManager.arrived();
        }
    }
    
    @Override
    public void onNavigationModeChanged() {
        TaskManager.getInstance().execute(new Runnable() {
            @Override
            public void run() {
                HereNavigationEventListener.this.logger.i(HereNavigationEventListener.this.tag + " onNavigationModeChanged HERE mode=" + HereNavigationEventListener.this.hereNavigationManager.getHereNavigationState() + " Navdy=" + HereNavigationEventListener.this.hereNavigationManager.getNavigationState());
            }
        }, 2);
    }
    
    @Override
    public void onRouteUpdated(final Route route) {
        this.logger.i(this.tag + " onRouteUpdated:" + route.toString());
    }
}
