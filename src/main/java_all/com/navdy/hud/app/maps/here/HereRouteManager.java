package com.navdy.hud.app.maps.here;

import com.navdy.hud.app.framework.destinations.Destination;
import com.here.android.mpa.routing.RoutingError;
import com.navdy.hud.app.analytics.NavigationQualityTracker;
import com.navdy.hud.app.event.RemoteEvent;
import com.navdy.service.library.events.navigation.NavigationSessionState;
import com.navdy.service.library.events.navigation.NavigationSessionRequest;
import com.navdy.hud.app.framework.voice.TTSUtils;
import com.navdy.service.library.events.navigation.NavigationRouteStatus;
import com.navdy.hud.app.framework.notifications.NotificationManager;
import java.util.Iterator;
import com.navdy.service.library.events.navigation.NavigationRouteResponse;
import com.here.android.mpa.search.Location;
import com.here.android.mpa.search.ErrorCode;
import com.navdy.hud.app.analytics.AnalyticsSupport;
import com.navdy.service.library.events.location.Coordinate;
import com.navdy.hud.app.framework.network.NetworkStateManager;
import com.squareup.wire.Message;
import com.navdy.service.library.events.navigation.RouteManeuverResponse;
import com.here.android.mpa.routing.Maneuver;
import com.navdy.service.library.events.navigation.RouteManeuver;
import android.os.SystemClock;
import com.navdy.service.library.task.TaskManager;
import com.navdy.service.library.events.navigation.RouteManeuverRequest;
import com.navdy.service.library.events.navigation.NavigationRouteCancelRequest;
import android.text.TextUtils;
import com.here.android.mpa.routing.RouteOptions;
import java.util.Date;
import android.content.res.Resources;
import com.navdy.hud.app.maps.notification.RouteCalculationNotification;
import com.navdy.service.library.events.RequestStatus;
import com.navdy.service.library.events.navigation.NavigationRouteResult;
import java.util.ArrayList;
import java.util.List;
import com.here.android.mpa.common.GeoCoordinate;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import java.util.LinkedList;
import java.util.concurrent.TimeUnit;
import com.navdy.hud.app.util.os.SystemProperties;
import android.os.Looper;
import com.navdy.hud.app.HudApplication;
import com.navdy.service.library.log.Logger;
import com.navdy.hud.app.maps.MapEvents;
import java.util.Queue;
import java.util.HashSet;
import com.navdy.hud.app.maps.MapsEventHandler;
import android.os.Handler;
import android.content.Context;
import com.squareup.otto.Bus;
import com.here.android.mpa.search.GeocodeRequest;
import com.navdy.service.library.events.navigation.NavigationRouteRequest;

public class HereRouteManager
{
    private static final String EMPTY_STR = "";
    public static final long MAX_ALLOWED_NAV_DISPLAY_POSITION_DIFF = 160934L;
    private static final double MAX_DISTANCE = 1.5E7;
    private static final int MAX_ONLINE_ROUTE_CALCULATION_TIME;
    private static final int MAX_ROUTES;
    private static final String MAX_ROUTES_PROP = "persist.sys.routes.max";
    private static final boolean VERBOSE = false;
    private static volatile String activeGeoCalcId;
    private static NavigationRouteRequest activeGeoRouteRequest;
    private static volatile String activeRouteCalcId;
    private static HereRouteCalculator activeRouteCalculator;
    private static boolean activeRouteCancelledByUser;
    private static GeocodeRequest activeRouteGeocodeRequest;
    private static int activeRouteProgress;
    private static NavigationRouteRequest activeRouteRequest;
    private static final Bus bus;
    private static final Context context;
    private static Handler handler;
    private static final HereMapsManager hereMapsManager;
    private static final Object lockObj;
    private static final MapsEventHandler mapsEventHandler;
    private static final HashSet<String> pendingRouteRequestIds;
    private static final Queue<NavigationRouteRequest> pendingRouteRequestQueue;
    private static MapEvents.RouteCalculationEvent routeCalculationEvent;
    private static final Logger sLogger;
    
    static {
        sLogger = new Logger(HereRouteManager.class);
        hereMapsManager = HereMapsManager.getInstance();
        context = HudApplication.getAppContext();
        HereRouteManager.handler = new Handler(Looper.getMainLooper());
        MAX_ROUTES = SystemProperties.getInt("persist.sys.routes.max", 3);
        MAX_ONLINE_ROUTE_CALCULATION_TIME = (int)TimeUnit.SECONDS.toMillis(20L);
        mapsEventHandler = MapsEventHandler.getInstance();
        lockObj = new Object();
        pendingRouteRequestQueue = new LinkedList<NavigationRouteRequest>();
        pendingRouteRequestIds = new HashSet<String>();
        bus = RemoteDeviceManager.getInstance().getBus();
    }
    
    public static String buildChangeRouteTTS(final String s) {
        return buildRouteTTS(R.string.route_change_msg, s);
    }
    
    public static String buildRouteTTS(final int n, String s) {
        if (!HereMapsManager.getInstance().isInitialized()) {
            s = "";
        }
        else {
            final HereNavigationManager instance = HereNavigationManager.getInstance();
            final Resources resources = HudApplication.getAppContext().getResources();
            final String s2 = "";
            final String s3 = "";
            String s4 = "";
            final HereRouteCache instance2 = HereRouteCache.getInstance();
            String currentRouteId;
            if ((currentRouteId = s) == null) {
                currentRouteId = instance.getCurrentRouteId();
            }
            final HereRouteCache.RouteInfo route = instance2.getRoute(currentRouteId);
            String string = s3;
            if (route != null) {
                final String s5 = s = route.routeResult.via;
                if (Boolean.TRUE.equals(instance.getGeneratePhoneticTTS())) {
                    s = "\u001b\\tn=address\\" + s5 + "\u001b\\tn=normal\\";
                }
                final Date routeTtaDate = HereMapUtil.getRouteTtaDate(route.route);
                final StringBuilder sb = new StringBuilder();
                string = RemoteDeviceManager.getInstance().getTimeHelper().formatTime12Hour(routeTtaDate, sb, false) + " " + sb.toString();
                s4 = s;
            }
            switch (n) {
                default:
                    s = "";
                    break;
                case R.string.route_start_msg: {
                    final RouteOptions routeOptions = instance.getRouteOptions();
                    s = s2;
                    if (routeOptions != null) {
                        switch (routeOptions.getRouteType()) {
                            default:
                                s = s2;
                                break;
                            case SHORTEST:
                                s = RouteCalculationNotification.shortestRoute;
                                break;
                            case FASTEST:
                                s = RouteCalculationNotification.fastestRoute;
                                break;
                        }
                    }
                    s = resources.getString(R.string.route_start_msg, new Object[] { s, s4, string });
                    break;
                }
                case R.string.route_change_msg:
                    s = resources.getString(R.string.route_change_msg, new Object[] { s4, string });
                    break;
            }
        }
        return s;
    }
    
    public static String buildStartRouteTTS() {
        return buildRouteTTS(R.string.route_start_msg, null);
    }
    
    public static boolean cancelActiveRouteRequest() {
        boolean handleRouteCancelRequest;
        if (TextUtils.isEmpty((CharSequence)HereRouteManager.activeRouteCalcId)) {
            HereRouteManager.sLogger.v("no active route id");
            handleRouteCancelRequest = false;
        }
        else {
            handleRouteCancelRequest = handleRouteCancelRequest(new NavigationRouteCancelRequest(HereRouteManager.activeRouteCalcId), true);
        }
        return handleRouteCancelRequest;
    }
    
    private static void cancelCurrentGeocodeRequest() {
        final Object lockObj = HereRouteManager.lockObj;
        synchronized (lockObj) {
            if (HereRouteManager.activeRouteGeocodeRequest == null) {
                return;
            }
            try {
                HereRouteManager.activeRouteGeocodeRequest.cancel();
                HereRouteManager.sLogger.v("geo: geocode request cancelled:" + HereRouteManager.activeGeoCalcId);
                if (HereRouteManager.activeGeoRouteRequest != null) {
                    returnErrorResponse(RequestStatus.REQUEST_CANCELLED, HereRouteManager.activeGeoRouteRequest, null);
                }
                HereRouteManager.activeRouteGeocodeRequest = null;
                HereRouteManager.activeGeoCalcId = null;
                HereRouteManager.activeGeoRouteRequest = null;
            }
            catch (Throwable t) {
                HereRouteManager.sLogger.e(t);
            }
        }
    }
    
    public static void clearActiveRouteCalc() {
        final Object lockObj = HereRouteManager.lockObj;
        synchronized (lockObj) {
            HereRouteManager.sLogger.v("clearActiveRouteCalc");
            HereRouteManager.activeRouteCalculator = null;
            HereRouteManager.activeRouteCalcId = null;
            HereRouteManager.activeRouteRequest = null;
            HereRouteManager.activeRouteProgress = 0;
            HereRouteManager.activeRouteCancelledByUser = false;
            HereRouteManager.routeCalculationEvent = null;
            final int size = HereRouteManager.pendingRouteRequestQueue.size();
            if (size > 0) {
                HereRouteManager.sLogger.v("pending route queue size:" + size);
                final NavigationRouteRequest navigationRouteRequest = HereRouteManager.pendingRouteRequestQueue.remove();
                HereRouteManager.pendingRouteRequestIds.remove(navigationRouteRequest.requestId);
                HereRouteManager.handler.post((Runnable)new Runnable() {
                    @Override
                    public void run() {
                        HereRouteManager.sLogger.v("launched pending request");
                        HereRouteManager.handleRouteRequest(navigationRouteRequest);
                    }
                });
            }
        }
    }
    
    public static void clearActiveRouteCalc(final String s) {
        final Object lockObj = HereRouteManager.lockObj;
        synchronized (lockObj) {
            if (TextUtils.equals((CharSequence)s, (CharSequence)HereRouteManager.activeRouteCalcId)) {
                clearActiveRouteCalc();
            }
        }
    }
    
    public static String getActiveRouteCalcId() {
        return HereRouteManager.activeRouteCalcId;
    }
    
    public static boolean handleRouteCancelRequest(final NavigationRouteCancelRequest p0, final boolean p1) {
        // 
        This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: istore_2       
        //     2: getstatic       com/navdy/hud/app/maps/here/HereRouteManager.sLogger:Lcom/navdy/service/library/log/Logger;
        //     5: astore_3       
        //     6: new             Ljava/lang/StringBuilder;
        //     9: astore          4
        //    11: aload           4
        //    13: invokespecial   java/lang/StringBuilder.<init>:()V
        //    16: aload_3        
        //    17: aload           4
        //    19: ldc_w           "routeCancelRequest:"
        //    22: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    25: aload_0        
        //    26: getfield        com/navdy/service/library/events/navigation/NavigationRouteCancelRequest.handle:Ljava/lang/String;
        //    29: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    32: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //    35: invokevirtual   com/navdy/service/library/log/Logger.v:(Ljava/lang/String;)V
        //    38: aload_0        
        //    39: getfield        com/navdy/service/library/events/navigation/NavigationRouteCancelRequest.handle:Ljava/lang/String;
        //    42: ifnonnull       58
        //    45: getstatic       com/navdy/hud/app/maps/here/HereRouteManager.sLogger:Lcom/navdy/service/library/log/Logger;
        //    48: ldc_w           "routeCancelRequest:null handle"
        //    51: invokevirtual   com/navdy/service/library/log/Logger.v:(Ljava/lang/String;)V
        //    54: iload_2        
        //    55: istore_1       
        //    56: iload_1        
        //    57: ireturn        
        //    58: getstatic       com/navdy/hud/app/maps/here/HereRouteManager.lockObj:Ljava/lang/Object;
        //    61: astore_3       
        //    62: aload_3        
        //    63: dup            
        //    64: astore          5
        //    66: monitorenter   
        //    67: iconst_0       
        //    68: istore          6
        //    70: iload           6
        //    72: istore          7
        //    74: getstatic       com/navdy/hud/app/maps/here/HereRouteManager.activeRouteGeocodeRequest:Lcom/here/android/mpa/search/GeocodeRequest;
        //    77: ifnull          385
        //    80: iload           6
        //    82: istore          7
        //    84: aload_0        
        //    85: getfield        com/navdy/service/library/events/navigation/NavigationRouteCancelRequest.handle:Ljava/lang/String;
        //    88: getstatic       com/navdy/hud/app/maps/here/HereRouteManager.activeGeoCalcId:Ljava/lang/String;
        //    91: invokestatic    android/text/TextUtils.equals:(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z
        //    94: ifeq            385
        //    97: iload           6
        //    99: istore          7
        //   101: invokestatic    com/navdy/hud/app/maps/here/HereRouteManager.cancelCurrentGeocodeRequest:()V
        //   104: iload           6
        //   106: istore          7
        //   108: getstatic       com/navdy/hud/app/maps/here/HereRouteManager.sLogger:Lcom/navdy/service/library/log/Logger;
        //   111: astore          8
        //   113: iload           6
        //   115: istore          7
        //   117: new             Ljava/lang/StringBuilder;
        //   120: astore          4
        //   122: iload           6
        //   124: istore          7
        //   126: aload           4
        //   128: invokespecial   java/lang/StringBuilder.<init>:()V
        //   131: iload           6
        //   133: istore          7
        //   135: aload           8
        //   137: aload           4
        //   139: ldc_w           "routeCancelRequest: sent navrouteresponse-geocode cancel ["
        //   142: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   145: aload_0        
        //   146: getfield        com/navdy/service/library/events/navigation/NavigationRouteCancelRequest.handle:Ljava/lang/String;
        //   149: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   152: ldc_w           "]"
        //   155: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   158: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   161: invokevirtual   com/navdy/service/library/log/Logger.v:(Ljava/lang/String;)V
        //   164: iload           6
        //   166: istore          7
        //   168: new             Lcom/navdy/service/library/events/navigation/NavigationRouteResponse$Builder;
        //   171: astore          4
        //   173: iload           6
        //   175: istore          7
        //   177: aload           4
        //   179: invokespecial   com/navdy/service/library/events/navigation/NavigationRouteResponse$Builder.<init>:()V
        //   182: iload           6
        //   184: istore          7
        //   186: aload           4
        //   188: aload_0        
        //   189: getfield        com/navdy/service/library/events/navigation/NavigationRouteCancelRequest.handle:Ljava/lang/String;
        //   192: invokevirtual   com/navdy/service/library/events/navigation/NavigationRouteResponse$Builder.requestId:(Ljava/lang/String;)Lcom/navdy/service/library/events/navigation/NavigationRouteResponse$Builder;
        //   195: getstatic       com/navdy/service/library/events/RequestStatus.REQUEST_CANCELLED:Lcom/navdy/service/library/events/RequestStatus;
        //   198: invokevirtual   com/navdy/service/library/events/navigation/NavigationRouteResponse$Builder.status:(Lcom/navdy/service/library/events/RequestStatus;)Lcom/navdy/service/library/events/navigation/NavigationRouteResponse$Builder;
        //   201: astore          4
        //   203: iload           6
        //   205: istore          7
        //   207: new             Lcom/navdy/service/library/events/location/Coordinate$Builder;
        //   210: astore          8
        //   212: iload           6
        //   214: istore          7
        //   216: aload           8
        //   218: invokespecial   com/navdy/service/library/events/location/Coordinate$Builder.<init>:()V
        //   221: iload           6
        //   223: istore          7
        //   225: aload           4
        //   227: aload           8
        //   229: dconst_0       
        //   230: invokestatic    java/lang/Double.valueOf:(D)Ljava/lang/Double;
        //   233: invokevirtual   com/navdy/service/library/events/location/Coordinate$Builder.latitude:(Ljava/lang/Double;)Lcom/navdy/service/library/events/location/Coordinate$Builder;
        //   236: dconst_0       
        //   237: invokestatic    java/lang/Double.valueOf:(D)Ljava/lang/Double;
        //   240: invokevirtual   com/navdy/service/library/events/location/Coordinate$Builder.longitude:(Ljava/lang/Double;)Lcom/navdy/service/library/events/location/Coordinate$Builder;
        //   243: invokevirtual   com/navdy/service/library/events/location/Coordinate$Builder.build:()Lcom/navdy/service/library/events/location/Coordinate;
        //   246: invokevirtual   com/navdy/service/library/events/navigation/NavigationRouteResponse$Builder.destination:(Lcom/navdy/service/library/events/location/Coordinate;)Lcom/navdy/service/library/events/navigation/NavigationRouteResponse$Builder;
        //   249: invokevirtual   com/navdy/service/library/events/navigation/NavigationRouteResponse$Builder.build:()Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;
        //   252: astore          8
        //   254: iload           6
        //   256: istore          7
        //   258: getstatic       com/navdy/hud/app/maps/here/HereRouteManager.bus:Lcom/squareup/otto/Bus;
        //   261: astore          4
        //   263: iload           6
        //   265: istore          7
        //   267: new             Lcom/navdy/hud/app/event/RemoteEvent;
        //   270: astore          9
        //   272: iload           6
        //   274: istore          7
        //   276: aload           9
        //   278: aload           8
        //   280: invokespecial   com/navdy/hud/app/event/RemoteEvent.<init>:(Lcom/squareup/wire/Message;)V
        //   283: iload           6
        //   285: istore          7
        //   287: aload           4
        //   289: aload           9
        //   291: invokevirtual   com/squareup/otto/Bus.post:(Ljava/lang/Object;)V
        //   294: iconst_0       
        //   295: ifne            377
        //   298: getstatic       com/navdy/hud/app/maps/here/HereRouteManager.sLogger:Lcom/navdy/service/library/log/Logger;
        //   301: astore          4
        //   303: new             Ljava/lang/StringBuilder;
        //   306: astore          8
        //   308: aload           8
        //   310: invokespecial   java/lang/StringBuilder.<init>:()V
        //   313: aload           4
        //   315: aload           8
        //   317: ldc_w           "may be pending route exists: "
        //   320: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   323: aload_0        
        //   324: getfield        com/navdy/service/library/events/navigation/NavigationRouteCancelRequest.handle:Ljava/lang/String;
        //   327: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   330: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   333: invokevirtual   com/navdy/service/library/log/Logger.v:(Ljava/lang/String;)V
        //   336: new             Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;
        //   339: astore          4
        //   341: aload           4
        //   343: invokespecial   com/navdy/hud/app/maps/MapEvents$RouteCalculationEvent.<init>:()V
        //   346: aload           4
        //   348: aload_0        
        //   349: getfield        com/navdy/service/library/events/navigation/NavigationRouteCancelRequest.handle:Ljava/lang/String;
        //   352: putfield        com/navdy/hud/app/maps/MapEvents$RouteCalculationEvent.pendingNavigationRequestId:Ljava/lang/String;
        //   355: aload           4
        //   357: iload_1        
        //   358: putfield        com/navdy/hud/app/maps/MapEvents$RouteCalculationEvent.abortOriginDisplay:Z
        //   361: aload           4
        //   363: getstatic       com/navdy/hud/app/maps/MapEvents$RouteCalculationState.ABORT_NAVIGATION:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationState;
        //   366: putfield        com/navdy/hud/app/maps/MapEvents$RouteCalculationEvent.state:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationState;
        //   369: getstatic       com/navdy/hud/app/maps/here/HereRouteManager.bus:Lcom/squareup/otto/Bus;
        //   372: aload           4
        //   374: invokevirtual   com/squareup/otto/Bus.post:(Ljava/lang/Object;)V
        //   377: aload           5
        //   379: monitorexit    
        //   380: iconst_1       
        //   381: istore_1       
        //   382: goto            56
        //   385: iload           6
        //   387: istore          7
        //   389: getstatic       com/navdy/hud/app/maps/here/HereRouteManager.activeRouteCalculator:Lcom/navdy/hud/app/maps/here/HereRouteCalculator;
        //   392: ifnonnull       559
        //   395: iload           6
        //   397: istore          7
        //   399: getstatic       com/navdy/hud/app/maps/here/HereRouteManager.sLogger:Lcom/navdy/service/library/log/Logger;
        //   402: astore          8
        //   404: iload           6
        //   406: istore          7
        //   408: new             Ljava/lang/StringBuilder;
        //   411: astore          4
        //   413: iload           6
        //   415: istore          7
        //   417: aload           4
        //   419: invokespecial   java/lang/StringBuilder.<init>:()V
        //   422: iload           6
        //   424: istore          7
        //   426: aload           8
        //   428: aload           4
        //   430: ldc_w           "no active route calculation:"
        //   433: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   436: aload_0        
        //   437: getfield        com/navdy/service/library/events/navigation/NavigationRouteCancelRequest.handle:Ljava/lang/String;
        //   440: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   443: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   446: invokevirtual   com/navdy/service/library/log/Logger.v:(Ljava/lang/String;)V
        //   449: iconst_0       
        //   450: ifne            532
        //   453: getstatic       com/navdy/hud/app/maps/here/HereRouteManager.sLogger:Lcom/navdy/service/library/log/Logger;
        //   456: astore          8
        //   458: new             Ljava/lang/StringBuilder;
        //   461: astore          4
        //   463: aload           4
        //   465: invokespecial   java/lang/StringBuilder.<init>:()V
        //   468: aload           8
        //   470: aload           4
        //   472: ldc_w           "may be pending route exists: "
        //   475: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   478: aload_0        
        //   479: getfield        com/navdy/service/library/events/navigation/NavigationRouteCancelRequest.handle:Ljava/lang/String;
        //   482: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   485: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   488: invokevirtual   com/navdy/service/library/log/Logger.v:(Ljava/lang/String;)V
        //   491: new             Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;
        //   494: astore          4
        //   496: aload           4
        //   498: invokespecial   com/navdy/hud/app/maps/MapEvents$RouteCalculationEvent.<init>:()V
        //   501: aload           4
        //   503: aload_0        
        //   504: getfield        com/navdy/service/library/events/navigation/NavigationRouteCancelRequest.handle:Ljava/lang/String;
        //   507: putfield        com/navdy/hud/app/maps/MapEvents$RouteCalculationEvent.pendingNavigationRequestId:Ljava/lang/String;
        //   510: aload           4
        //   512: iload_1        
        //   513: putfield        com/navdy/hud/app/maps/MapEvents$RouteCalculationEvent.abortOriginDisplay:Z
        //   516: aload           4
        //   518: getstatic       com/navdy/hud/app/maps/MapEvents$RouteCalculationState.ABORT_NAVIGATION:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationState;
        //   521: putfield        com/navdy/hud/app/maps/MapEvents$RouteCalculationEvent.state:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationState;
        //   524: getstatic       com/navdy/hud/app/maps/here/HereRouteManager.bus:Lcom/squareup/otto/Bus;
        //   527: aload           4
        //   529: invokevirtual   com/squareup/otto/Bus.post:(Ljava/lang/Object;)V
        //   532: aload           5
        //   534: monitorexit    
        //   535: iload_2        
        //   536: istore_1       
        //   537: goto            56
        //   540: astore_0       
        //   541: aload           5
        //   543: monitorexit    
        //   544: aload_0        
        //   545: athrow         
        //   546: astore_0       
        //   547: getstatic       com/navdy/hud/app/maps/here/HereRouteManager.sLogger:Lcom/navdy/service/library/log/Logger;
        //   550: aload_0        
        //   551: invokevirtual   com/navdy/service/library/log/Logger.e:(Ljava/lang/Throwable;)V
        //   554: iload_2        
        //   555: istore_1       
        //   556: goto            56
        //   559: iload           6
        //   561: istore          7
        //   563: aload_0        
        //   564: getfield        com/navdy/service/library/events/navigation/NavigationRouteCancelRequest.handle:Ljava/lang/String;
        //   567: getstatic       com/navdy/hud/app/maps/here/HereRouteManager.activeRouteCalcId:Ljava/lang/String;
        //   570: invokestatic    android/text/TextUtils.equals:(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z
        //   573: ifne            721
        //   576: iload           6
        //   578: istore          7
        //   580: getstatic       com/navdy/hud/app/maps/here/HereRouteManager.sLogger:Lcom/navdy/service/library/log/Logger;
        //   583: astore          8
        //   585: iload           6
        //   587: istore          7
        //   589: new             Ljava/lang/StringBuilder;
        //   592: astore          4
        //   594: iload           6
        //   596: istore          7
        //   598: aload           4
        //   600: invokespecial   java/lang/StringBuilder.<init>:()V
        //   603: iload           6
        //   605: istore          7
        //   607: aload           8
        //   609: aload           4
        //   611: ldc_w           "invalid active route id:"
        //   614: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   617: aload_0        
        //   618: getfield        com/navdy/service/library/events/navigation/NavigationRouteCancelRequest.handle:Ljava/lang/String;
        //   621: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   624: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   627: invokevirtual   com/navdy/service/library/log/Logger.v:(Ljava/lang/String;)V
        //   630: iconst_0       
        //   631: ifne            713
        //   634: getstatic       com/navdy/hud/app/maps/here/HereRouteManager.sLogger:Lcom/navdy/service/library/log/Logger;
        //   637: astore          4
        //   639: new             Ljava/lang/StringBuilder;
        //   642: astore          8
        //   644: aload           8
        //   646: invokespecial   java/lang/StringBuilder.<init>:()V
        //   649: aload           4
        //   651: aload           8
        //   653: ldc_w           "may be pending route exists: "
        //   656: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   659: aload_0        
        //   660: getfield        com/navdy/service/library/events/navigation/NavigationRouteCancelRequest.handle:Ljava/lang/String;
        //   663: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   666: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   669: invokevirtual   com/navdy/service/library/log/Logger.v:(Ljava/lang/String;)V
        //   672: new             Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;
        //   675: astore          4
        //   677: aload           4
        //   679: invokespecial   com/navdy/hud/app/maps/MapEvents$RouteCalculationEvent.<init>:()V
        //   682: aload           4
        //   684: aload_0        
        //   685: getfield        com/navdy/service/library/events/navigation/NavigationRouteCancelRequest.handle:Ljava/lang/String;
        //   688: putfield        com/navdy/hud/app/maps/MapEvents$RouteCalculationEvent.pendingNavigationRequestId:Ljava/lang/String;
        //   691: aload           4
        //   693: iload_1        
        //   694: putfield        com/navdy/hud/app/maps/MapEvents$RouteCalculationEvent.abortOriginDisplay:Z
        //   697: aload           4
        //   699: getstatic       com/navdy/hud/app/maps/MapEvents$RouteCalculationState.ABORT_NAVIGATION:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationState;
        //   702: putfield        com/navdy/hud/app/maps/MapEvents$RouteCalculationEvent.state:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationState;
        //   705: getstatic       com/navdy/hud/app/maps/here/HereRouteManager.bus:Lcom/squareup/otto/Bus;
        //   708: aload           4
        //   710: invokevirtual   com/squareup/otto/Bus.post:(Ljava/lang/Object;)V
        //   713: aload           5
        //   715: monitorexit    
        //   716: iload_2        
        //   717: istore_1       
        //   718: goto            56
        //   721: iload           6
        //   723: istore          7
        //   725: getstatic       com/navdy/hud/app/maps/here/HereRouteManager.activeRouteCancelledByUser:Z
        //   728: ifeq            876
        //   731: iload           6
        //   733: istore          7
        //   735: getstatic       com/navdy/hud/app/maps/here/HereRouteManager.sLogger:Lcom/navdy/service/library/log/Logger;
        //   738: astore          8
        //   740: iload           6
        //   742: istore          7
        //   744: new             Ljava/lang/StringBuilder;
        //   747: astore          4
        //   749: iload           6
        //   751: istore          7
        //   753: aload           4
        //   755: invokespecial   java/lang/StringBuilder.<init>:()V
        //   758: iload           6
        //   760: istore          7
        //   762: aload           8
        //   764: aload           4
        //   766: ldc_w           "request already cancelled:"
        //   769: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   772: aload_0        
        //   773: getfield        com/navdy/service/library/events/navigation/NavigationRouteCancelRequest.handle:Ljava/lang/String;
        //   776: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   779: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   782: invokevirtual   com/navdy/service/library/log/Logger.v:(Ljava/lang/String;)V
        //   785: iconst_0       
        //   786: ifne            868
        //   789: getstatic       com/navdy/hud/app/maps/here/HereRouteManager.sLogger:Lcom/navdy/service/library/log/Logger;
        //   792: astore          4
        //   794: new             Ljava/lang/StringBuilder;
        //   797: astore          8
        //   799: aload           8
        //   801: invokespecial   java/lang/StringBuilder.<init>:()V
        //   804: aload           4
        //   806: aload           8
        //   808: ldc_w           "may be pending route exists: "
        //   811: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   814: aload_0        
        //   815: getfield        com/navdy/service/library/events/navigation/NavigationRouteCancelRequest.handle:Ljava/lang/String;
        //   818: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   821: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   824: invokevirtual   com/navdy/service/library/log/Logger.v:(Ljava/lang/String;)V
        //   827: new             Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;
        //   830: astore          4
        //   832: aload           4
        //   834: invokespecial   com/navdy/hud/app/maps/MapEvents$RouteCalculationEvent.<init>:()V
        //   837: aload           4
        //   839: aload_0        
        //   840: getfield        com/navdy/service/library/events/navigation/NavigationRouteCancelRequest.handle:Ljava/lang/String;
        //   843: putfield        com/navdy/hud/app/maps/MapEvents$RouteCalculationEvent.pendingNavigationRequestId:Ljava/lang/String;
        //   846: aload           4
        //   848: iload_1        
        //   849: putfield        com/navdy/hud/app/maps/MapEvents$RouteCalculationEvent.abortOriginDisplay:Z
        //   852: aload           4
        //   854: getstatic       com/navdy/hud/app/maps/MapEvents$RouteCalculationState.ABORT_NAVIGATION:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationState;
        //   857: putfield        com/navdy/hud/app/maps/MapEvents$RouteCalculationEvent.state:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationState;
        //   860: getstatic       com/navdy/hud/app/maps/here/HereRouteManager.bus:Lcom/squareup/otto/Bus;
        //   863: aload           4
        //   865: invokevirtual   com/squareup/otto/Bus.post:(Ljava/lang/Object;)V
        //   868: aload           5
        //   870: monitorexit    
        //   871: iload_2        
        //   872: istore_1       
        //   873: goto            56
        //   876: iload_1        
        //   877: ifeq            932
        //   880: iload           6
        //   882: istore          7
        //   884: getstatic       com/navdy/hud/app/maps/here/HereRouteManager.sLogger:Lcom/navdy/service/library/log/Logger;
        //   887: ldc_w           "send cancel to client"
        //   890: invokevirtual   com/navdy/service/library/log/Logger.v:(Ljava/lang/String;)V
        //   893: iload           6
        //   895: istore          7
        //   897: getstatic       com/navdy/hud/app/maps/here/HereRouteManager.bus:Lcom/squareup/otto/Bus;
        //   900: astore          8
        //   902: iload           6
        //   904: istore          7
        //   906: new             Lcom/navdy/hud/app/event/RemoteEvent;
        //   909: astore          4
        //   911: iload           6
        //   913: istore          7
        //   915: aload           4
        //   917: aload_0        
        //   918: invokespecial   com/navdy/hud/app/event/RemoteEvent.<init>:(Lcom/squareup/wire/Message;)V
        //   921: iload           6
        //   923: istore          7
        //   925: aload           8
        //   927: aload           4
        //   929: invokevirtual   com/squareup/otto/Bus.post:(Ljava/lang/Object;)V
        //   932: iconst_1       
        //   933: istore          6
        //   935: iload           6
        //   937: istore          7
        //   939: iconst_1       
        //   940: putstatic       com/navdy/hud/app/maps/here/HereRouteManager.activeRouteCancelledByUser:Z
        //   943: iload           6
        //   945: istore          7
        //   947: getstatic       com/navdy/hud/app/maps/here/HereRouteManager.activeRouteCalculator:Lcom/navdy/hud/app/maps/here/HereRouteCalculator;
        //   950: invokevirtual   com/navdy/hud/app/maps/here/HereRouteCalculator.cancel:()V
        //   953: iload           6
        //   955: istore          7
        //   957: getstatic       com/navdy/hud/app/maps/here/HereRouteManager.sLogger:Lcom/navdy/service/library/log/Logger;
        //   960: ldc_w           "called cancel"
        //   963: invokevirtual   com/navdy/service/library/log/Logger.v:(Ljava/lang/String;)V
        //   966: iload           6
        //   968: istore          7
        //   970: getstatic       com/navdy/hud/app/maps/here/HereRouteManager.sLogger:Lcom/navdy/service/library/log/Logger;
        //   973: astore          8
        //   975: iload           6
        //   977: istore          7
        //   979: new             Ljava/lang/StringBuilder;
        //   982: astore          4
        //   984: iload           6
        //   986: istore          7
        //   988: aload           4
        //   990: invokespecial   java/lang/StringBuilder.<init>:()V
        //   993: iload           6
        //   995: istore          7
        //   997: aload           8
        //   999: aload           4
        //  1001: ldc_w           "routeCancelRequest: sent navrouteresponse cancel ["
        //  1004: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //  1007: aload_0        
        //  1008: getfield        com/navdy/service/library/events/navigation/NavigationRouteCancelRequest.handle:Ljava/lang/String;
        //  1011: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //  1014: ldc_w           "]"
        //  1017: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //  1020: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //  1023: invokevirtual   com/navdy/service/library/log/Logger.v:(Ljava/lang/String;)V
        //  1026: iload           6
        //  1028: istore          7
        //  1030: new             Lcom/navdy/service/library/events/navigation/NavigationRouteResponse$Builder;
        //  1033: astore          4
        //  1035: iload           6
        //  1037: istore          7
        //  1039: aload           4
        //  1041: invokespecial   com/navdy/service/library/events/navigation/NavigationRouteResponse$Builder.<init>:()V
        //  1044: iload           6
        //  1046: istore          7
        //  1048: aload           4
        //  1050: aload_0        
        //  1051: getfield        com/navdy/service/library/events/navigation/NavigationRouteCancelRequest.handle:Ljava/lang/String;
        //  1054: invokevirtual   com/navdy/service/library/events/navigation/NavigationRouteResponse$Builder.requestId:(Ljava/lang/String;)Lcom/navdy/service/library/events/navigation/NavigationRouteResponse$Builder;
        //  1057: getstatic       com/navdy/service/library/events/RequestStatus.REQUEST_CANCELLED:Lcom/navdy/service/library/events/RequestStatus;
        //  1060: invokevirtual   com/navdy/service/library/events/navigation/NavigationRouteResponse$Builder.status:(Lcom/navdy/service/library/events/RequestStatus;)Lcom/navdy/service/library/events/navigation/NavigationRouteResponse$Builder;
        //  1063: astore          8
        //  1065: iload           6
        //  1067: istore          7
        //  1069: new             Lcom/navdy/service/library/events/location/Coordinate$Builder;
        //  1072: astore          4
        //  1074: iload           6
        //  1076: istore          7
        //  1078: aload           4
        //  1080: invokespecial   com/navdy/service/library/events/location/Coordinate$Builder.<init>:()V
        //  1083: iload           6
        //  1085: istore          7
        //  1087: aload           8
        //  1089: aload           4
        //  1091: dconst_0       
        //  1092: invokestatic    java/lang/Double.valueOf:(D)Ljava/lang/Double;
        //  1095: invokevirtual   com/navdy/service/library/events/location/Coordinate$Builder.latitude:(Ljava/lang/Double;)Lcom/navdy/service/library/events/location/Coordinate$Builder;
        //  1098: dconst_0       
        //  1099: invokestatic    java/lang/Double.valueOf:(D)Ljava/lang/Double;
        //  1102: invokevirtual   com/navdy/service/library/events/location/Coordinate$Builder.longitude:(Ljava/lang/Double;)Lcom/navdy/service/library/events/location/Coordinate$Builder;
        //  1105: invokevirtual   com/navdy/service/library/events/location/Coordinate$Builder.build:()Lcom/navdy/service/library/events/location/Coordinate;
        //  1108: invokevirtual   com/navdy/service/library/events/navigation/NavigationRouteResponse$Builder.destination:(Lcom/navdy/service/library/events/location/Coordinate;)Lcom/navdy/service/library/events/navigation/NavigationRouteResponse$Builder;
        //  1111: invokevirtual   com/navdy/service/library/events/navigation/NavigationRouteResponse$Builder.build:()Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;
        //  1114: astore          8
        //  1116: iload           6
        //  1118: istore          7
        //  1120: getstatic       com/navdy/hud/app/maps/here/HereRouteManager.bus:Lcom/squareup/otto/Bus;
        //  1123: astore          4
        //  1125: iload           6
        //  1127: istore          7
        //  1129: new             Lcom/navdy/hud/app/event/RemoteEvent;
        //  1132: astore          9
        //  1134: iload           6
        //  1136: istore          7
        //  1138: aload           9
        //  1140: aload           8
        //  1142: invokespecial   com/navdy/hud/app/event/RemoteEvent.<init>:(Lcom/squareup/wire/Message;)V
        //  1145: iload           6
        //  1147: istore          7
        //  1149: aload           4
        //  1151: aload           9
        //  1153: invokevirtual   com/squareup/otto/Bus.post:(Ljava/lang/Object;)V
        //  1156: iconst_1       
        //  1157: ifne            1239
        //  1160: getstatic       com/navdy/hud/app/maps/here/HereRouteManager.sLogger:Lcom/navdy/service/library/log/Logger;
        //  1163: astore          8
        //  1165: new             Ljava/lang/StringBuilder;
        //  1168: astore          4
        //  1170: aload           4
        //  1172: invokespecial   java/lang/StringBuilder.<init>:()V
        //  1175: aload           8
        //  1177: aload           4
        //  1179: ldc_w           "may be pending route exists: "
        //  1182: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //  1185: aload_0        
        //  1186: getfield        com/navdy/service/library/events/navigation/NavigationRouteCancelRequest.handle:Ljava/lang/String;
        //  1189: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //  1192: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //  1195: invokevirtual   com/navdy/service/library/log/Logger.v:(Ljava/lang/String;)V
        //  1198: new             Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;
        //  1201: astore          4
        //  1203: aload           4
        //  1205: invokespecial   com/navdy/hud/app/maps/MapEvents$RouteCalculationEvent.<init>:()V
        //  1208: aload           4
        //  1210: aload_0        
        //  1211: getfield        com/navdy/service/library/events/navigation/NavigationRouteCancelRequest.handle:Ljava/lang/String;
        //  1214: putfield        com/navdy/hud/app/maps/MapEvents$RouteCalculationEvent.pendingNavigationRequestId:Ljava/lang/String;
        //  1217: aload           4
        //  1219: iload_1        
        //  1220: putfield        com/navdy/hud/app/maps/MapEvents$RouteCalculationEvent.abortOriginDisplay:Z
        //  1223: aload           4
        //  1225: getstatic       com/navdy/hud/app/maps/MapEvents$RouteCalculationState.ABORT_NAVIGATION:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationState;
        //  1228: putfield        com/navdy/hud/app/maps/MapEvents$RouteCalculationEvent.state:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationState;
        //  1231: getstatic       com/navdy/hud/app/maps/here/HereRouteManager.bus:Lcom/squareup/otto/Bus;
        //  1234: aload           4
        //  1236: invokevirtual   com/squareup/otto/Bus.post:(Ljava/lang/Object;)V
        //  1239: aload           5
        //  1241: monitorexit    
        //  1242: iconst_1       
        //  1243: istore_1       
        //  1244: goto            56
        //  1247: astore          4
        //  1249: iload           7
        //  1251: ifne            1333
        //  1254: getstatic       com/navdy/hud/app/maps/here/HereRouteManager.sLogger:Lcom/navdy/service/library/log/Logger;
        //  1257: astore          8
        //  1259: new             Ljava/lang/StringBuilder;
        //  1262: astore          9
        //  1264: aload           9
        //  1266: invokespecial   java/lang/StringBuilder.<init>:()V
        //  1269: aload           8
        //  1271: aload           9
        //  1273: ldc_w           "may be pending route exists: "
        //  1276: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //  1279: aload_0        
        //  1280: getfield        com/navdy/service/library/events/navigation/NavigationRouteCancelRequest.handle:Ljava/lang/String;
        //  1283: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //  1286: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //  1289: invokevirtual   com/navdy/service/library/log/Logger.v:(Ljava/lang/String;)V
        //  1292: new             Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationEvent;
        //  1295: astore          8
        //  1297: aload           8
        //  1299: invokespecial   com/navdy/hud/app/maps/MapEvents$RouteCalculationEvent.<init>:()V
        //  1302: aload           8
        //  1304: aload_0        
        //  1305: getfield        com/navdy/service/library/events/navigation/NavigationRouteCancelRequest.handle:Ljava/lang/String;
        //  1308: putfield        com/navdy/hud/app/maps/MapEvents$RouteCalculationEvent.pendingNavigationRequestId:Ljava/lang/String;
        //  1311: aload           8
        //  1313: iload_1        
        //  1314: putfield        com/navdy/hud/app/maps/MapEvents$RouteCalculationEvent.abortOriginDisplay:Z
        //  1317: aload           8
        //  1319: getstatic       com/navdy/hud/app/maps/MapEvents$RouteCalculationState.ABORT_NAVIGATION:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationState;
        //  1322: putfield        com/navdy/hud/app/maps/MapEvents$RouteCalculationEvent.state:Lcom/navdy/hud/app/maps/MapEvents$RouteCalculationState;
        //  1325: getstatic       com/navdy/hud/app/maps/here/HereRouteManager.bus:Lcom/squareup/otto/Bus;
        //  1328: aload           8
        //  1330: invokevirtual   com/squareup/otto/Bus.post:(Ljava/lang/Object;)V
        //  1333: aload           4
        //  1335: athrow         
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  2      54     546    559    Ljava/lang/Throwable;
        //  58     67     546    559    Ljava/lang/Throwable;
        //  74     80     1247   1336   Any
        //  84     97     1247   1336   Any
        //  101    104    1247   1336   Any
        //  108    113    1247   1336   Any
        //  117    122    1247   1336   Any
        //  126    131    1247   1336   Any
        //  135    164    1247   1336   Any
        //  168    173    1247   1336   Any
        //  177    182    1247   1336   Any
        //  186    203    1247   1336   Any
        //  207    212    1247   1336   Any
        //  216    221    1247   1336   Any
        //  225    254    1247   1336   Any
        //  258    263    1247   1336   Any
        //  267    272    1247   1336   Any
        //  276    283    1247   1336   Any
        //  287    294    1247   1336   Any
        //  298    377    540    546    Any
        //  377    380    540    546    Any
        //  389    395    1247   1336   Any
        //  399    404    1247   1336   Any
        //  408    413    1247   1336   Any
        //  417    422    1247   1336   Any
        //  426    449    1247   1336   Any
        //  453    532    540    546    Any
        //  532    535    540    546    Any
        //  541    544    540    546    Any
        //  544    546    546    559    Ljava/lang/Throwable;
        //  563    576    1247   1336   Any
        //  580    585    1247   1336   Any
        //  589    594    1247   1336   Any
        //  598    603    1247   1336   Any
        //  607    630    1247   1336   Any
        //  634    713    540    546    Any
        //  713    716    540    546    Any
        //  725    731    1247   1336   Any
        //  735    740    1247   1336   Any
        //  744    749    1247   1336   Any
        //  753    758    1247   1336   Any
        //  762    785    1247   1336   Any
        //  789    868    540    546    Any
        //  868    871    540    546    Any
        //  884    893    1247   1336   Any
        //  897    902    1247   1336   Any
        //  906    911    1247   1336   Any
        //  915    921    1247   1336   Any
        //  925    932    1247   1336   Any
        //  939    943    1247   1336   Any
        //  947    953    1247   1336   Any
        //  957    966    1247   1336   Any
        //  970    975    1247   1336   Any
        //  979    984    1247   1336   Any
        //  988    993    1247   1336   Any
        //  997    1026   1247   1336   Any
        //  1030   1035   1247   1336   Any
        //  1039   1044   1247   1336   Any
        //  1048   1065   1247   1336   Any
        //  1069   1074   1247   1336   Any
        //  1078   1083   1247   1336   Any
        //  1087   1116   1247   1336   Any
        //  1120   1125   1247   1336   Any
        //  1129   1134   1247   1336   Any
        //  1138   1145   1247   1336   Any
        //  1149   1156   1247   1336   Any
        //  1160   1239   540    546    Any
        //  1239   1242   540    546    Any
        //  1254   1333   540    546    Any
        //  1333   1336   540    546    Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0377:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public static void handleRouteManeuverRequest(final RouteManeuverRequest routeManeuverRequest) {
        TaskManager.getInstance().execute(new Runnable() {
            @Override
            public void run() {
                try {
                    Label_0385: {
                        if (routeManeuverRequest.routeId == null) {
                            break Label_0385;
                        }
                        final long elapsedRealtime = SystemClock.elapsedRealtime();
                        final HereRouteCache.RouteInfo route = HereRouteCache.getInstance().getRoute(routeManeuverRequest.routeId);
                        if (route == null) {
                            break Label_0385;
                        }
                        String streetAddress = null;
                        if (route.routeRequest != null) {
                            streetAddress = route.routeRequest.streetAddress;
                        }
                        final List<Maneuver> maneuvers = route.route.getManeuvers();
                        final ArrayList<RouteManeuver> list = new ArrayList<RouteManeuver>(20);
                        final int size = maneuvers.size();
                        Maneuver maneuver = null;
                        Maneuver maneuver2 = null;
                        Maneuver maneuver3;
                        for (int i = 0; i < size; ++i, maneuver2 = maneuver3) {
                            if (maneuver2 != null) {
                                maneuver = maneuver2;
                            }
                            maneuver3 = maneuvers.get(i);
                            Maneuver maneuver4 = null;
                            if (i < size - 1) {
                                maneuver4 = maneuvers.get(i + 1);
                            }
                            int n = 0;
                            MapEvents.ManeuverDisplay maneuverDisplay;
                            if (i == 0) {
                                maneuverDisplay = HereManeuverDisplayBuilder.getStartManeuverDisplay(maneuver3, maneuver4);
                            }
                            else {
                                final MapEvents.ManeuverDisplay maneuverDisplay2 = maneuverDisplay = HereManeuverDisplayBuilder.getManeuverDisplay(maneuver3, maneuver4 == null, maneuver3.getDistanceToNextManeuver(), streetAddress, maneuver, route.routeRequest, maneuver4, 1 != 0, 0 != 0, null);
                                if (maneuver4 != null) {
                                    n = (int)((maneuver4.getStartTime().getTime() - maneuver3.getStartTime().getTime()) / 1000L);
                                    maneuverDisplay = maneuverDisplay2;
                                }
                            }
                            list.add(new RouteManeuver(maneuverDisplay.currentRoad, maneuverDisplay.navigationTurn, maneuverDisplay.pendingRoad, maneuverDisplay.distance, maneuverDisplay.distanceUnit, n));
                        }
                        HereRouteManager.sLogger.v("Time to build maneuver list:" + (SystemClock.elapsedRealtime() - elapsedRealtime));
                        HereRouteManager.mapsEventHandler.sendEventToClient(new RouteManeuverResponse(RequestStatus.REQUEST_SUCCESS, null, list));
                        return;
                    }
                    HereRouteManager.mapsEventHandler.sendEventToClient(new RouteManeuverResponse(RequestStatus.REQUEST_INVALID_REQUEST, null, null));
                }
                catch (Throwable t) {
                    HereRouteManager.mapsEventHandler.sendEventToClient(new RouteManeuverResponse(RequestStatus.REQUEST_SERVICE_ERROR, t.toString(), null));
                    HereRouteManager.sLogger.e(t);
                }
            }
        }, 2);
    }
    
    public static void handleRouteRequest(final NavigationRouteRequest navigationRouteRequest) {
        TaskManager.getInstance().execute(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    Object o = null;
                    Label_0114: {
                        try {
                            HereRouteManager.printRouteRequest(navigationRouteRequest);
                            if (!HereRouteManager.hereMapsManager.isInitialized()) {
                                HereRouteManager.sLogger.i("engine not ready");
                                returnErrorResponse(RequestStatus.REQUEST_NOT_READY, navigationRouteRequest, HereRouteManager.context.getString(R.string.map_engine_not_ready));
                            }
                            else {
                                o = HereRouteManager.hereMapsManager.getLocationFixManager();
                                if (o != null) {
                                    break Label_0114;
                                }
                                HereRouteManager.sLogger.i("engine not ready, location fix manager n/a");
                                returnErrorResponse(RequestStatus.REQUEST_NOT_READY, navigationRouteRequest, HereRouteManager.context.getString(R.string.map_engine_not_ready));
                            }
                            return;
                        }
                        catch (Throwable t) {
                            HereRouteManager.sLogger.e("", t);
                            returnErrorResponse(RequestStatus.REQUEST_UNKNOWN_ERROR, navigationRouteRequest, HereRouteManager.context.getString(R.string.search_error));
                            return;
                        }
                    }
                    if (!NetworkStateManager.getInstance().isNetworkStateInitialized()) {
                        HereRouteManager.sLogger.i("network state is not initialized yet");
                        returnErrorResponse(RequestStatus.REQUEST_NOT_READY, navigationRouteRequest, HereRouteManager.context.getString(R.string.network_not_initialized));
                        return;
                    }
                    GeoCoordinate geoCoordinate = HereRouteManager.hereMapsManager.getRouteStartPoint();
                    if (geoCoordinate == null) {
                        geoCoordinate = ((HereLocationFixManager)o).getLastGeoCoordinate();
                    }
                    else {
                        final Logger access$100 = HereRouteManager.sLogger;
                        o = new StringBuilder();
                        access$100.v(((StringBuilder)o).append("using debug start point:").append(geoCoordinate).toString());
                    }
                    if (geoCoordinate == null) {
                        HereRouteManager.sLogger.i("start location n/a");
                        returnErrorResponse(RequestStatus.REQUEST_NO_LOCATION_SERVICE, navigationRouteRequest, HereRouteManager.context.getString(R.string.no_current_position));
                        return;
                    }
                    if (navigationRouteRequest.requestId == null) {
                        HereRouteManager.sLogger.i("no request id specified");
                        returnErrorResponse(RequestStatus.REQUEST_INVALID_REQUEST, navigationRouteRequest, "no route id");
                        return;
                    }
                    HereRouteManager.sLogger.i("reset traffic reroute listener");
                    HereNavigationManager.getInstance().resetTrafficRerouteListener();
                    // monitorenter(access$101 = HereRouteManager.access$400())
                    final StringBuilder sb = null;
                    Label_0363: {
                        try {
                            if (HereRouteManager.activeRouteRequest == null) {
                                o = sb;
                                if (!HereRouteManager.isUIShowingRouteCalculation()) {
                                    break Label_0363;
                                }
                            }
                            if (HereMapUtil.isSavedRoute(navigationRouteRequest)) {
                                HereRouteManager.sLogger.i("saved route cannot override an existing route calculation");
                                returnErrorResponse(RequestStatus.REQUEST_ALREADY_IN_PROGRESS, navigationRouteRequest, null);
                                return;
                            }
                        }
                        finally {
                        }
                        // monitorexit(access$101)
                        o = sb;
                        if (HereRouteManager.activeRouteRequest != null) {
                            o = HereRouteManager.activeRouteRequest.requestId;
                        }
                    }
                    if (HereRouteManager.pendingRouteRequestIds.contains(navigationRouteRequest.requestId) || TextUtils.equals((CharSequence)navigationRouteRequest.requestId, (CharSequence)o)) {
                        HereRouteManager.sLogger.v("route request[" + navigationRouteRequest.requestId + "] already received");
                        // monitorexit(access$101)
                        return;
                    }
                    // monitorexit(access$101)
                    final HereNavigationManager instance = HereNavigationManager.getInstance();
                    if (instance.hasArrived()) {
                        HereRouteManager.sLogger.v("stop arrival mode");
                        instance.setIgnoreArrived(true);
                        instance.stopNavigation();
                    }
                    final GeoCoordinate geoCoordinate2;
                    handleRouteRequest(navigationRouteRequest, geoCoordinate2);
                }
            }
        }, 19);
    }
    
    private static void handleRouteRequest(final NavigationRouteRequest activeGeoRouteRequest, final GeoCoordinate geoCoordinate) {
        final long elapsedRealtime = SystemClock.elapsedRealtime();
        final ArrayList<GeoCoordinate> list = new ArrayList<GeoCoordinate>();
        if (activeGeoRouteRequest.waypoints != null) {
            for (final Coordinate coordinate : activeGeoRouteRequest.waypoints) {
                list.add(new GeoCoordinate(coordinate.latitude, coordinate.longitude));
            }
        }
        Object lockObj;
        if (activeGeoRouteRequest.destinationDisplay != null && activeGeoRouteRequest.destinationDisplay.latitude != 0.0 && activeGeoRouteRequest.destinationDisplay.longitude != 0.0 && activeGeoRouteRequest.destination.latitude != activeGeoRouteRequest.destinationDisplay.latitude && activeGeoRouteRequest.destination.longitude != activeGeoRouteRequest.destinationDisplay.longitude) {
            final long n = (long)new GeoCoordinate(activeGeoRouteRequest.destinationDisplay.latitude, activeGeoRouteRequest.destinationDisplay.longitude).distanceTo(new GeoCoordinate(activeGeoRouteRequest.destination.latitude, activeGeoRouteRequest.destination.longitude));
            if (n >= 160934L) {
                lockObj = new GeoCoordinate(activeGeoRouteRequest.destinationDisplay.latitude, activeGeoRouteRequest.destinationDisplay.longitude);
                HereRouteManager.sLogger.e("display/nav distance check failed:" + n);
                AnalyticsSupport.recordBadRoutePosition(activeGeoRouteRequest.destinationDisplay.latitude + "," + activeGeoRouteRequest.destinationDisplay.longitude, activeGeoRouteRequest.destination.latitude + "," + activeGeoRouteRequest.destination.longitude, activeGeoRouteRequest.streetAddress);
            }
            else {
                HereRouteManager.sLogger.v("display/nav distance check pass:" + n);
                lockObj = new GeoCoordinate(activeGeoRouteRequest.destination.latitude, activeGeoRouteRequest.destination.longitude);
            }
        }
        else {
            HereRouteManager.sLogger.v("display/nav distance check pass: no display pos");
            lockObj = new GeoCoordinate(activeGeoRouteRequest.destination.latitude, activeGeoRouteRequest.destination.longitude);
        }
        HereRouteManager.sLogger.v("handleRouteRequest start=" + geoCoordinate + ", end=" + lockObj);
        boolean b = false;
        Label_0620: {
            if (!Boolean.TRUE.equals(activeGeoRouteRequest.geoCodeStreetAddress)) {
                final double distanceTo = geoCoordinate.distanceTo((GeoCoordinate)lockObj);
                if (distanceTo <= 1.5E7) {
                    break Label_0620;
                }
                HereRouteManager.sLogger.e("distance between start and endpoint:" + distanceTo + " max:" + 1.5E7);
                returnErrorResponse(RequestStatus.REQUEST_INVALID_REQUEST, activeGeoRouteRequest, HereRouteManager.context.getString(R.string.long_drive_error));
            }
            else {
                if (!TextUtils.isEmpty((CharSequence)activeGeoRouteRequest.streetAddress)) {
                    b = true;
                    break Label_0620;
                }
                returnErrorResponse(RequestStatus.REQUEST_INVALID_REQUEST, activeGeoRouteRequest, HereRouteManager.context.getString(R.string.unknown_address));
            }
            return;
        }
        // monitorexit(o)
        while (true) {
            boolean b2 = false;
            while (true) {
                Label_0632: {
                    if (HereRouteManager.hereMapsManager.isEngineOnline()) {
                        b2 = true;
                        break Label_0632;
                    }
                    Label_0928: {
                        break Label_0928;
                        while (true) {
                            HereRouteManager.sLogger.v("Street Address:" + activeGeoRouteRequest.streetAddress);
                            lockObj = HereRouteManager.lockObj;
                            while (true) {
                                Label_0972: {
                                    synchronized (lockObj) {
                                        if (isRouteCalcInProgress()) {
                                            if (activeGeoRouteRequest.cancelCurrent == null || !activeGeoRouteRequest.cancelCurrent) {
                                                returnErrorResponse(RequestStatus.REQUEST_ALREADY_IN_PROGRESS, activeGeoRouteRequest, HereRouteManager.activeRouteCalcId);
                                                return;
                                            }
                                            if (HereRouteManager.activeRouteCalculator != null) {
                                                HereRouteManager.sLogger.v("geo:route cancelling current request :" + HereRouteManager.activeRouteCalcId);
                                                HereRouteManager.activeRouteCalculator.cancel();
                                                HereRouteManager.activeRouteCalculator = null;
                                                returnErrorResponse(RequestStatus.REQUEST_CANCELLED, HereRouteManager.activeRouteRequest, HereRouteManager.activeRouteCalcId);
                                            }
                                        }
                                        if (isRouteCalcGeocodeRequestInProgress()) {
                                            if (activeGeoRouteRequest.cancelCurrent == null || !activeGeoRouteRequest.cancelCurrent) {
                                                break;
                                            }
                                            cancelCurrentGeocodeRequest();
                                        }
                                        HereRouteManager.activeGeoCalcId = activeGeoRouteRequest.requestId;
                                        HereRouteManager.activeGeoRouteRequest = activeGeoRouteRequest;
                                        HereRouteManager.activeRouteGeocodeRequest = HerePlacesManager.geoCodeStreetAddress(geoCoordinate, activeGeoRouteRequest.streetAddress, 100000, (HerePlacesManager.GeoCodeCallback)new HerePlacesManager.GeoCodeCallback() {
                                            @Override
                                            public void result(final ErrorCode errorCode, List<Location> o) {
                                                while (true) {
                                                    Label_0205: {
                                                        final Object access$101;
                                                        try {
                                                            if (errorCode == ErrorCode.CANCELLED) {
                                                                final Logger access$100 = HereRouteManager.sLogger;
                                                                o = new StringBuilder();
                                                                access$100.v(((StringBuilder)o).append("geocode request has been cancelled [").append(activeGeoRouteRequest.requestId).append("]").toString());
                                                            }
                                                            else {
                                                                // monitorenter(access$101 = HereRouteManager.access$400())
                                                                final GeoCodeCallback geoCodeCallback = this;
                                                                final NavigationRouteRequest navigationRouteRequest = activeGeoRouteRequest;
                                                                final String s = navigationRouteRequest.requestId;
                                                                final String s2 = HereRouteManager.activeGeoCalcId;
                                                                final boolean b = TextUtils.equals((CharSequence)s, (CharSequence)s2);
                                                                final Object o2 = access$101;
                                                                // monitorexit(o2)
                                                                final boolean b2 = b;
                                                                if (b2) {
                                                                    break Label_0205;
                                                                }
                                                                o = HereRouteManager.sLogger;
                                                                final StringBuilder sb = new(java.lang.StringBuilder.class);
                                                                final StringBuilder sb3;
                                                                final StringBuilder sb2 = sb3 = sb;
                                                                new StringBuilder();
                                                                final StringBuilder sb4 = (StringBuilder)o;
                                                                final StringBuilder sb5 = sb2;
                                                                final String s3 = "geocode request is not valid active:";
                                                                final StringBuilder sb6 = sb5.append(s3);
                                                                final String s4 = HereRouteManager.activeGeoCalcId;
                                                                final StringBuilder sb7 = sb6.append(s4);
                                                                final String s5 = " request:";
                                                                final StringBuilder sb8 = sb7.append(s5);
                                                                final GeoCodeCallback geoCodeCallback2 = this;
                                                                final NavigationRouteRequest navigationRouteRequest2 = activeGeoRouteRequest;
                                                                final String s6 = navigationRouteRequest2.requestId;
                                                                final StringBuilder sb9 = sb8.append(s6);
                                                                final String s7 = sb9.toString();
                                                                ((Logger)sb4).v(s7);
                                                                final NavigationRouteResponse navigationRouteResponse = new(com.navdy.service.library.events.navigation.NavigationRouteResponse.class);
                                                                final NavigationRouteResponse navigationRouteResponse3;
                                                                final NavigationRouteResponse navigationRouteResponse2 = navigationRouteResponse3 = navigationRouteResponse;
                                                                final RequestStatus requestStatus = RequestStatus.REQUEST_CANCELLED;
                                                                final String s8 = null;
                                                                final GeoCodeCallback geoCodeCallback3 = this;
                                                                final NavigationRouteRequest navigationRouteRequest3 = activeGeoRouteRequest;
                                                                final Coordinate coordinate = navigationRouteRequest3.destination;
                                                                final String s9 = null;
                                                                final List<NavigationRouteResult> list = null;
                                                                final boolean b3 = false;
                                                                final Boolean b4 = b3;
                                                                final GeoCodeCallback geoCodeCallback4 = this;
                                                                final NavigationRouteRequest navigationRouteRequest4 = activeGeoRouteRequest;
                                                                final String s10 = navigationRouteRequest4.requestId;
                                                                new NavigationRouteResponse(requestStatus, s8, coordinate, s9, list, b4, s10);
                                                                final MapsEventHandler mapsEventHandler = MapsEventHandler.getInstance();
                                                                final NavigationRouteResponse navigationRouteResponse4 = navigationRouteResponse2;
                                                                mapsEventHandler.sendEventToClient(navigationRouteResponse4);
                                                            }
                                                            return;
                                                        }
                                                        catch (Throwable t) {
                                                            HereRouteManager.sLogger.v("Street Address placesSearch exception", t);
                                                            returnErrorResponse(RequestStatus.REQUEST_UNKNOWN_ERROR, activeGeoRouteRequest, HereRouteManager.context.getString(R.string.geocoding_failed));
                                                            return;
                                                        }
                                                        try {
                                                            final GeoCodeCallback geoCodeCallback = this;
                                                            final NavigationRouteRequest navigationRouteRequest = activeGeoRouteRequest;
                                                            final String s = navigationRouteRequest.requestId;
                                                            final String s2 = HereRouteManager.activeGeoCalcId;
                                                            final boolean b = TextUtils.equals((CharSequence)s, (CharSequence)s2);
                                                            final Object o2 = access$101;
                                                            // monitorexit(o2)
                                                            final boolean b2 = b;
                                                            if (!b2) {
                                                                o = HereRouteManager.sLogger;
                                                                final StringBuilder sb = new(java.lang.StringBuilder.class);
                                                                final StringBuilder sb3;
                                                                final StringBuilder sb2 = sb3 = sb;
                                                                new StringBuilder();
                                                                final StringBuilder sb4 = (StringBuilder)o;
                                                                final StringBuilder sb5 = sb2;
                                                                final String s3 = "geocode request is not valid active:";
                                                                final StringBuilder sb6 = sb5.append(s3);
                                                                final String s4 = HereRouteManager.activeGeoCalcId;
                                                                final StringBuilder sb7 = sb6.append(s4);
                                                                final String s5 = " request:";
                                                                final StringBuilder sb8 = sb7.append(s5);
                                                                final GeoCodeCallback geoCodeCallback2 = this;
                                                                final NavigationRouteRequest navigationRouteRequest2 = activeGeoRouteRequest;
                                                                final String s6 = navigationRouteRequest2.requestId;
                                                                final StringBuilder sb9 = sb8.append(s6);
                                                                final String s7 = sb9.toString();
                                                                ((Logger)sb4).v(s7);
                                                                final NavigationRouteResponse navigationRouteResponse = new(com.navdy.service.library.events.navigation.NavigationRouteResponse.class);
                                                                final NavigationRouteResponse navigationRouteResponse3;
                                                                final NavigationRouteResponse navigationRouteResponse2 = navigationRouteResponse3 = navigationRouteResponse;
                                                                final RequestStatus requestStatus = RequestStatus.REQUEST_CANCELLED;
                                                                final String s8 = null;
                                                                final GeoCodeCallback geoCodeCallback3 = this;
                                                                final NavigationRouteRequest navigationRouteRequest3 = activeGeoRouteRequest;
                                                                final Coordinate coordinate = navigationRouteRequest3.destination;
                                                                final String s9 = null;
                                                                final List<NavigationRouteResult> list = null;
                                                                final boolean b3 = false;
                                                                final Boolean b4 = b3;
                                                                final GeoCodeCallback geoCodeCallback4 = this;
                                                                final NavigationRouteRequest navigationRouteRequest4 = activeGeoRouteRequest;
                                                                final String s10 = navigationRouteRequest4.requestId;
                                                                new NavigationRouteResponse(requestStatus, s8, coordinate, s9, list, b4, s10);
                                                                final MapsEventHandler mapsEventHandler = MapsEventHandler.getInstance();
                                                                final NavigationRouteResponse navigationRouteResponse4 = navigationRouteResponse2;
                                                                mapsEventHandler.sendEventToClient(navigationRouteResponse4);
                                                                return;
                                                            }
                                                        }
                                                        finally {
                                                        }
                                                        // monitorexit(access$101)
                                                    }
                                                    final Object access$102 = HereRouteManager.lockObj;
                                                    synchronized (access$102) {
                                                        HereRouteManager.activeRouteGeocodeRequest = null;
                                                        HereRouteManager.activeGeoCalcId = null;
                                                        HereRouteManager.activeGeoRouteRequest = null;
                                                        // monitorexit(access$102)
                                                        final ErrorCode errorCode2;
                                                        if (errorCode2 == ErrorCode.NONE && o != null && ((List)o).size() > 0) {
                                                            HereRouteManager.sLogger.v("Street Address results size=" + ((List)o).size());
                                                            o = ((List<Location>)o).get(0).getCoordinate();
                                                            HereRouteManager.sLogger.v("Street Address placesSearch success: new-end-geo:" + o);
                                                            routeSearch(activeGeoRouteRequest, geoCoordinate, list, (GeoCoordinate)o, b2);
                                                            return;
                                                        }
                                                    }
                                                    final Logger access$103 = HereRouteManager.sLogger;
                                                    o = new StringBuilder();
                                                    final Enum enum1;
                                                    access$103.v(((StringBuilder)o).append("could not geocode, Street Address placesSearch error:").append(enum1.name()).toString());
                                                    returnErrorResponse(RequestStatus.REQUEST_SERVICE_ERROR, activeGeoRouteRequest, HereRouteManager.context.getString(R.string.geocoding_failed));
                                                }
                                            }
                                        });
                                        if (HereRouteManager.activeRouteGeocodeRequest == null) {
                                            HereRouteManager.activeRouteGeocodeRequest = null;
                                            HereRouteManager.activeGeoCalcId = null;
                                            HereRouteManager.sLogger.v("geocode request failed");
                                            // monitorexit(lockObj)
                                            HereRouteManager.sLogger.v("handleRouteRequest- time-1 =" + (SystemClock.elapsedRealtime() - elapsedRealtime));
                                            return;
                                        }
                                        break Label_0972;
                                        b2 = false;
                                        break Label_0632;
                                    }
                                    break;
                                }
                                HereRouteManager.sLogger.v("geocode request triggered");
                                continue;
                            }
                        }
                    }
                    returnErrorResponse(RequestStatus.REQUEST_ALREADY_IN_PROGRESS, activeGeoRouteRequest, HereRouteManager.activeGeoCalcId);
                    return;
                }
                HereRouteManager.sLogger.v("maps engine online:" + b2);
                if (b) {
                    continue;
                }
                break;
            }
            HereRouteManager.sLogger.v("geocoding not set");
            routeSearch(activeGeoRouteRequest, geoCoordinate, list, (GeoCoordinate)lockObj, b2);
            continue;
        }
    }
    
    private static boolean isRouteCalcGeocodeRequestInProgress() {
        final Object lockObj = HereRouteManager.lockObj;
        synchronized (lockObj) {
            boolean b;
            if (HereRouteManager.activeRouteGeocodeRequest != null) {
                HereRouteManager.sLogger.i("route calculation geocode already in progress:" + HereRouteManager.activeRouteCalcId);
                b = true;
            }
            else {
                b = false;
            }
            // monitorexit(lockObj)
            return b;
        }
    }
    
    private static boolean isRouteCalcInProgress() {
        final Object lockObj = HereRouteManager.lockObj;
        synchronized (lockObj) {
            boolean b;
            if (HereRouteManager.activeRouteCalculator != null) {
                HereRouteManager.sLogger.i("route calculation already in progress:" + HereRouteManager.activeRouteCalcId);
                b = true;
            }
            else {
                b = false;
            }
            // monitorexit(lockObj)
            return b;
        }
    }
    
    public static boolean isUIShowingRouteCalculation() {
        return NotificationManager.getInstance().getNotification("navdy#route#calc#notif") != null;
    }
    
    public static void printRouteRequest(final NavigationRouteRequest navigationRouteRequest) {
        final String s = null;
        String string = null;
        Label_0090: {
            StringBuilder sb = null;
            Label_0085: {
                try {
                    final List<NavigationRouteRequest.RouteAttribute> routeAttributes = navigationRouteRequest.routeAttributes;
                    string = s;
                    if (routeAttributes == null) {
                        break Label_0090;
                    }
                    string = s;
                    if (routeAttributes.size() > 0) {
                        sb = new StringBuilder();
                        final Iterator<NavigationRouteRequest.RouteAttribute> iterator = routeAttributes.iterator();
                        while (iterator.hasNext()) {
                            sb.append(iterator.next().name());
                            sb.append(",");
                        }
                        break Label_0085;
                    }
                    break Label_0090;
                }
                catch (Throwable t) {
                    HereRouteManager.sLogger.e(t);
                }
                return;
            }
            string = sb.toString();
        }
        HereRouteManager.sLogger.v("NavigationRouteRequest label[" + navigationRouteRequest.label + "] local[" + navigationRouteRequest.originDisplay + "] streetAddress[" + navigationRouteRequest.streetAddress + "] destination_id[" + navigationRouteRequest.destination_identifier + "] autonav[" + navigationRouteRequest.autoNavigate + "] geoCode[" + navigationRouteRequest.geoCodeStreetAddress + "] dest_coordinate[" + navigationRouteRequest.destination + "] favType[" + navigationRouteRequest.destinationType + "] requestId[" + navigationRouteRequest.requestId + "]" + "] display_coordinate[" + navigationRouteRequest.destinationDisplay + "] route attrs[" + string + "]");
    }
    
    private static void returnErrorResponse(final RequestStatus requestStatus, final NavigationRouteRequest navigationRouteRequest, final String s) {
        final boolean b = true;
        final boolean b2 = true;
        final boolean b3 = true;
        int n = b ? 1 : 0;
        int n2 = b2 ? 1 : 0;
        try {
            final Logger sLogger = HereRouteManager.sLogger;
            n = (b ? 1 : 0);
            n2 = (b2 ? 1 : 0);
            n = (b ? 1 : 0);
            n2 = (b2 ? 1 : 0);
            final StringBuilder sb = new StringBuilder();
            n = (b ? 1 : 0);
            n2 = (b2 ? 1 : 0);
            sLogger.e(sb.append(requestStatus).append(": ").append(s).toString());
            n = (b ? 1 : 0);
            n2 = (b2 ? 1 : 0);
            n = (b ? 1 : 0);
            n2 = (b2 ? 1 : 0);
            final NavigationRouteResponse response = new NavigationRouteResponse(requestStatus, s, navigationRouteRequest.destination, null, null, false, navigationRouteRequest.requestId);
            n = (b ? 1 : 0);
            n2 = (b2 ? 1 : 0);
            MapsEventHandler.getInstance().sendEventToClient(response);
            n = (b ? 1 : 0);
            n2 = (b2 ? 1 : 0);
            final Object lockObj = HereRouteManager.lockObj;
            n = (b ? 1 : 0);
            n2 = (b2 ? 1 : 0);
            // monitorenter(o = lockObj)
            n2 = (b3 ? 1 : 0);
            try {
                if (requestStatus == RequestStatus.REQUEST_ALREADY_IN_PROGRESS) {
                    n2 = 0;
                    // monitorexit(o)
                    if (false) {
                        clearActiveRouteCalc();
                    }
                }
                else {
                    n2 = (b3 ? 1 : 0);
                    if (HereRouteManager.routeCalculationEvent != null) {
                        n2 = (b3 ? 1 : 0);
                        HereRouteManager.routeCalculationEvent.response = response;
                        n2 = (b3 ? 1 : 0);
                        HereRouteManager.routeCalculationEvent.state = MapEvents.RouteCalculationState.STOPPED;
                        n2 = (b3 ? 1 : 0);
                        HereRouteManager.bus.post(HereRouteManager.routeCalculationEvent);
                    }
                    n2 = (b3 ? 1 : 0);
                    // monitorexit(o)
                    if (true) {
                        clearActiveRouteCalc();
                    }
                }
            }
            finally {
                // monitorexit(o)
                n = n2;
            }
        }
        catch (Throwable t) {
            n2 = n;
            HereRouteManager.sLogger.e(t);
        }
        finally {
            if (n2 != 0) {
                clearActiveRouteCalc();
            }
        }
    }
    
    private static void returnStatusResponse(final String s, final int n, final String s2) {
        try {
            HereRouteManager.sLogger.v("handle[" + s + "] progress [" + n + "]");
            MapsEventHandler.getInstance().sendEventToClient(new NavigationRouteStatus(s, n, s2));
        }
        catch (Throwable t) {
            HereRouteManager.sLogger.e(t);
        }
    }
    
    private static void returnSuccessResponse(final NavigationRouteRequest navigationRouteRequest, ArrayList<NavigationRouteResult> o, final boolean b) {
        Label_0280: {
            try {
                final NavigationRouteResponse response = new NavigationRouteResponse(RequestStatus.REQUEST_SUCCESS, "", navigationRouteRequest.destination, navigationRouteRequest.label, (List<NavigationRouteResult>)o, b, navigationRouteRequest.requestId);
                if (!b) {
                    TTSUtils.debugShowNoTrafficToast();
                }
                MapsEventHandler.getInstance().sendEventToClient(response);
                final Object lockObj = HereRouteManager.lockObj;
                synchronized (lockObj) {
                    if (HereRouteManager.routeCalculationEvent != null) {
                        HereRouteManager.routeCalculationEvent.response = response;
                        HereRouteManager.routeCalculationEvent.state = MapEvents.RouteCalculationState.STOPPED;
                        HereRouteManager.bus.post(HereRouteManager.routeCalculationEvent);
                    }
                    // monitorexit(lockObj)
                    HereRouteManager.sLogger.v("sent route response");
                    if (navigationRouteRequest.autoNavigate != null && navigationRouteRequest.autoNavigate) {
                        HereRouteManager.sLogger.i("start auto-navigation to route " + navigationRouteRequest.label);
                        o = new NavigationSessionRequest.Builder().newState(NavigationSessionState.NAV_SESSION_STARTED).label(navigationRouteRequest.label).routeId(((ArrayList<NavigationRouteResult>)o).get(0).routeId).simulationSpeed(0);
                        if (navigationRouteRequest.originDisplay != null) {
                            break Label_0280;
                        }
                        final boolean booleanValue = false;
                        o = ((NavigationSessionRequest.Builder)o).originDisplay(booleanValue).build();
                        HereRouteManager.bus.post(o);
                        HereRouteManager.bus.post(new RemoteEvent((Message)o));
                    }
                    NavigationQualityTracker.getInstance().trackCalculationWithTraffic(b);
                }
            }
            catch (Throwable t) {
                try {
                    HereRouteManager.sLogger.e(t);
                    return;
                    final NavigationRouteRequest navigationRouteRequest2;
                    final boolean booleanValue = navigationRouteRequest2.originDisplay;
                }
                finally {
                    clearActiveRouteCalc();
                }
            }
        }
    }
    
    private static void routeSearch(final NavigationRouteRequest navigationRouteRequest, GeoCoordinate geoCoordinate, final List<GeoCoordinate> list, final GeoCoordinate geoCoordinate2, final boolean b) {
        while (true) {
            final Object lockObj = HereRouteManager.lockObj;
            // monitorexit(o)
            final NavigationRouteRequest navigationRouteRequest2;
            Label_0192: {
                while (true) {
                    Label_0179: {
                        Label_0131: {
                            synchronized (lockObj) {
                                if (isRouteCalcGeocodeRequestInProgress()) {
                                    if (navigationRouteRequest.cancelCurrent == null || !navigationRouteRequest.cancelCurrent) {
                                        returnErrorResponse(RequestStatus.REQUEST_ALREADY_IN_PROGRESS, navigationRouteRequest, HereRouteManager.activeGeoCalcId);
                                        // monitorexit(lockObj)
                                        return;
                                    }
                                    cancelCurrentGeocodeRequest();
                                }
                                if (!isRouteCalcInProgress()) {
                                    break Label_0192;
                                }
                                if (navigationRouteRequest.cancelCurrent == null || !navigationRouteRequest.cancelCurrent) {
                                    break Label_0179;
                                }
                                HereRouteManager.pendingRouteRequestQueue.add(navigationRouteRequest);
                                HereRouteManager.pendingRouteRequestIds.add(navigationRouteRequest.requestId);
                                HereRouteManager.sLogger.v("add to pending Queue");
                                if (!HereRouteManager.activeRouteCancelledByUser) {
                                    break Label_0131;
                                }
                                HereRouteManager.sLogger.v("cancellation in progress");
                                return;
                            }
                        }
                        final Logger sLogger = HereRouteManager.sLogger;
                        geoCoordinate = (GeoCoordinate)new StringBuilder();
                        sLogger.v(((StringBuilder)geoCoordinate).append("cancelling current request :").append(HereRouteManager.activeRouteCalcId).toString());
                        HereRouteManager.activeRouteCancelledByUser = true;
                        HereRouteManager.activeRouteCalculator.cancel();
                        return;
                    }
                    returnErrorResponse(RequestStatus.REQUEST_ALREADY_IN_PROGRESS, navigationRouteRequest2, HereRouteManager.activeRouteCalcId);
                    continue;
                }
            }
            // monitorexit(o)
            if (b) {
                routeSearchTraffic(navigationRouteRequest2, geoCoordinate, list, geoCoordinate2, true);
                return;
            }
            routeSearchNoTraffic(navigationRouteRequest2, geoCoordinate, list, geoCoordinate2, true);
        }
    }
    
    private static void routeSearchNoTraffic(final NavigationRouteRequest navigationRouteRequest, final GeoCoordinate start, final List<GeoCoordinate> waypoints, final GeoCoordinate end, final boolean b) {
        final HereRouteCalculator hereRouteCalculator = new HereRouteCalculator(HereRouteManager.sLogger, false);
        final RouteOptions routeOptions = HereRouteManager.hereMapsManager.getRouteOptions();
        while (true) {
            Label_0184: {
                if (!b) {
                    break Label_0184;
                }
                HereRouteManager.activeRouteCalcId = navigationRouteRequest.requestId;
                HereRouteManager.activeRouteRequest = navigationRouteRequest;
                HereRouteManager.activeRouteProgress = 0;
                HereRouteManager.activeRouteCalculator = hereRouteCalculator;
                returnStatusResponse(HereRouteManager.activeRouteCalcId, HereRouteManager.activeRouteProgress, HereRouteManager.activeRouteRequest.requestId);
                final Object lockObj = HereRouteManager.lockObj;
                synchronized (lockObj) {
                    HereRouteManager.routeCalculationEvent = new MapEvents.RouteCalculationEvent();
                    HereRouteManager.routeCalculationEvent.state = MapEvents.RouteCalculationState.STARTED;
                    HereRouteManager.routeCalculationEvent.start = start;
                    HereRouteManager.routeCalculationEvent.waypoints = waypoints;
                    HereRouteManager.routeCalculationEvent.end = end;
                    HereRouteManager.routeCalculationEvent.request = navigationRouteRequest;
                    HereRouteManager.routeCalculationEvent.routeOptions = routeOptions;
                    HereRouteManager.bus.post(HereRouteManager.routeCalculationEvent);
                    // monitorexit(lockObj)
                    HereRouteManager.sLogger.v("no-traffic route calc started");
                    hereRouteCalculator.calculateRoute(navigationRouteRequest, start, waypoints, end, false, (HereRouteCalculator.RouteCalculatorListener)new HereRouteCalculator.RouteCalculatorListener() {
                        @Override
                        public void error(final RoutingError routingError, final Throwable t) {
                            Label_0045: {
                                if (routingError == null) {
                                    break Label_0045;
                                }
                                final String name = routingError.name();
                                while (true) {
                                    final Object access$400 = HereRouteManager.lockObj;
                                    synchronized (access$400) {
                                        if (HereRouteManager.activeRouteCancelledByUser) {
                                            HereRouteManager.sLogger.v("user cancelled routing");
                                            returnErrorResponse(RequestStatus.REQUEST_CANCELLED, navigationRouteRequest, null);
                                        }
                                        else {
                                            returnErrorResponse(RequestStatus.REQUEST_SERVICE_ERROR, navigationRouteRequest, name);
                                        }
                                        return;
                                        HereRouteManager.context.getString(R.string.search_error);
                                    }
                                }
                            }
                        }
                        
                        @Override
                        public void postSuccess(final ArrayList<NavigationRouteResult> list) {
                            returnSuccessResponse(navigationRouteRequest, list, false);
                        }
                        
                        @Override
                        public void preSuccess() {
                            final Object access$400 = HereRouteManager.lockObj;
                            synchronized (access$400) {
                                HereRouteManager.activeRouteCalculator = null;
                            }
                        }
                        
                        @Override
                        public void progress(final int n) {
                        }
                    }, HereRouteManager.MAX_ROUTES, routeOptions, true);
                    return;
                }
            }
            final Object lockObj2 = HereRouteManager.lockObj;
            synchronized (lockObj2) {
                Label_0215: {
                    if (HereRouteManager.activeRouteCalculator != null) {
                        HereRouteManager.sLogger.v("replaced active route calc with no traffic");
                        HereRouteManager.activeRouteCalculator = hereRouteCalculator;
                        break Label_0215;
                    }
                    break Label_0215;
                }
                continue;
            }
            break;
        }
    }
    
    private static void routeSearchTraffic(final NavigationRouteRequest activeRouteRequest, final GeoCoordinate geoCoordinate, final List<GeoCoordinate> list, final GeoCoordinate geoCoordinate2, final boolean b) {
        final HereRouteCalculator activeRouteCalculator = new HereRouteCalculator(HereRouteManager.sLogger, false);
        final Object o;
        final Runnable runnable;
        final RouteOptions routeOptions;
        Label_0067: {
            if (!b) {
                break Label_0067;
            }
            synchronized (o = HereRouteManager.lockObj) {
                HereRouteManager.activeRouteCalcId = activeRouteRequest.requestId;
                HereRouteManager.activeRouteRequest = activeRouteRequest;
                HereRouteManager.activeRouteProgress = 0;
                HereRouteManager.activeRouteCalculator = activeRouteCalculator;
                returnStatusResponse(HereRouteManager.activeRouteCalcId, HereRouteManager.activeRouteProgress, HereRouteManager.activeRouteRequest.requestId);
                // monitorexit(o)
                runnable = new Runnable() {
                    @Override
                    public void run() {
                        HereRouteManager.sLogger.w("cancel traffic based route,max time reached:" + HereRouteManager.MAX_ONLINE_ROUTE_CALCULATION_TIME);
                        activeRouteCalculator.cancel();
                    }
                };
                HereRouteManager.sLogger.v("traffic route calc started");
                HereRouteManager.handler.postDelayed((Runnable)runnable, (long)HereRouteManager.MAX_ONLINE_ROUTE_CALCULATION_TIME);
                routeOptions = HereRouteManager.hereMapsManager.getRouteOptions();
                // monitorenter(o = HereRouteManager.lockObj)
                final MapEvents.RouteCalculationEvent routeCalculationEvent = new(com.navdy.hud.app.maps.MapEvents.RouteCalculationEvent.class);
                final MapEvents.RouteCalculationEvent routeCalculationEvent3;
                final MapEvents.RouteCalculationEvent routeCalculationEvent2 = routeCalculationEvent3 = routeCalculationEvent;
                new MapEvents.RouteCalculationEvent();
                HereRouteManager.routeCalculationEvent = routeCalculationEvent2;
                final MapEvents.RouteCalculationEvent routeCalculationEvent4 = HereRouteManager.routeCalculationEvent;
                final MapEvents.RouteCalculationState routeCalculationState = MapEvents.RouteCalculationState.STARTED;
                routeCalculationEvent4.state = routeCalculationState;
                final MapEvents.RouteCalculationEvent routeCalculationEvent5 = HereRouteManager.routeCalculationEvent;
                final GeoCoordinate geoCoordinate3 = geoCoordinate;
                routeCalculationEvent5.start = geoCoordinate3;
                final MapEvents.RouteCalculationEvent routeCalculationEvent6 = HereRouteManager.routeCalculationEvent;
                final List<GeoCoordinate> list2 = list;
                routeCalculationEvent6.waypoints = list2;
                final MapEvents.RouteCalculationEvent routeCalculationEvent7 = HereRouteManager.routeCalculationEvent;
                final GeoCoordinate geoCoordinate4 = geoCoordinate2;
                routeCalculationEvent7.end = geoCoordinate4;
                final MapEvents.RouteCalculationEvent routeCalculationEvent8 = HereRouteManager.routeCalculationEvent;
                final NavigationRouteRequest navigationRouteRequest = activeRouteRequest;
                routeCalculationEvent8.request = navigationRouteRequest;
                final MapEvents.RouteCalculationEvent routeCalculationEvent9 = HereRouteManager.routeCalculationEvent;
                final RouteOptions routeOptions2 = routeOptions;
                routeCalculationEvent9.routeOptions = routeOptions2;
                final Bus bus = HereRouteManager.bus;
                final MapEvents.RouteCalculationEvent routeCalculationEvent10 = HereRouteManager.routeCalculationEvent;
                bus.post(routeCalculationEvent10);
                final Object o2 = o;
                // monitorexit(o2)
                final HereRouteCalculator hereRouteCalculator = activeRouteCalculator;
                final NavigationRouteRequest navigationRouteRequest2 = activeRouteRequest;
                final GeoCoordinate geoCoordinate5 = geoCoordinate;
                final List<GeoCoordinate> list3 = list;
                final GeoCoordinate geoCoordinate6 = geoCoordinate2;
                final boolean b2 = true;
                final NavigationRouteRequest navigationRouteRequest3 = activeRouteRequest;
                final Runnable runnable2 = runnable;
                final GeoCoordinate geoCoordinate7 = geoCoordinate;
                final List<GeoCoordinate> list4 = list;
                final GeoCoordinate geoCoordinate8 = geoCoordinate2;
                final HereRouteCalculator.RouteCalculatorListener routeCalculatorListener = new HereRouteCalculator.RouteCalculatorListener() {
                    final /* synthetic */ GeoCoordinate val$endPoint;
                    final /* synthetic */ NavigationRouteRequest val$request;
                    final /* synthetic */ Runnable val$runnable;
                    final /* synthetic */ GeoCoordinate val$startPoint;
                    final /* synthetic */ List val$waypoints;
                    
                    @Override
                    public void error(final RoutingError routingError, final Throwable t) {
                        HereRouteManager.handler.removeCallbacks(this.val$runnable);
                        Label_0083: {
                            if (routingError == null) {
                                break Label_0083;
                            }
                            HereRouteManager.sLogger.v("got error for traffic [" + routingError.name() + "]");
                            while (true) {
                                final Object access$400 = HereRouteManager.lockObj;
                                synchronized (access$400) {
                                    if (HereRouteManager.activeRouteCancelledByUser) {
                                        HereRouteManager.sLogger.v("user cancelled routing");
                                        returnErrorResponse(RequestStatus.REQUEST_CANCELLED, this.val$request, null);
                                    }
                                    else {
                                        // monitorexit(access$400)
                                        HereRouteManager.sLogger.v("launching no traffic");
                                        routeSearchNoTraffic(this.val$request, this.val$startPoint, this.val$waypoints, this.val$endPoint, false);
                                    }
                                    return;
                                    HereRouteManager.sLogger.v("got error for traffic", t);
                                }
                            }
                        }
                    }
                    
                    @Override
                    public void postSuccess(final ArrayList<NavigationRouteResult> list) {
                        HereRouteManager.sLogger.v("got result for traffic");
                        returnSuccessResponse(this.val$request, list, true);
                    }
                    
                    @Override
                    public void preSuccess() {
                        final Object access$400 = HereRouteManager.lockObj;
                        synchronized (access$400) {
                            HereRouteManager.activeRouteCalculator = null;
                            // monitorexit(access$400)
                            HereRouteManager.handler.removeCallbacks(this.val$runnable);
                            HereRouteManager.sLogger.v("route successfully calculated, post calc in progress");
                        }
                    }
                    
                    @Override
                    public void progress(final int n) {
                    }
                };
                final int n = HereRouteManager.MAX_ROUTES;
                final RouteOptions routeOptions3 = routeOptions;
                final boolean b3 = true;
                hereRouteCalculator.calculateRoute(navigationRouteRequest2, geoCoordinate5, list3, geoCoordinate6, b2, (HereRouteCalculator.RouteCalculatorListener)routeCalculatorListener, n, routeOptions3, b3);
                return;
            }
        }
        try {
            final MapEvents.RouteCalculationEvent routeCalculationEvent = new(com.navdy.hud.app.maps.MapEvents.RouteCalculationEvent.class);
            final MapEvents.RouteCalculationEvent routeCalculationEvent3;
            final MapEvents.RouteCalculationEvent routeCalculationEvent2 = routeCalculationEvent3 = routeCalculationEvent;
            new MapEvents.RouteCalculationEvent();
            HereRouteManager.routeCalculationEvent = routeCalculationEvent2;
            final MapEvents.RouteCalculationEvent routeCalculationEvent4 = HereRouteManager.routeCalculationEvent;
            final MapEvents.RouteCalculationState routeCalculationState = MapEvents.RouteCalculationState.STARTED;
            routeCalculationEvent4.state = routeCalculationState;
            final MapEvents.RouteCalculationEvent routeCalculationEvent5 = HereRouteManager.routeCalculationEvent;
            final GeoCoordinate geoCoordinate3 = geoCoordinate;
            routeCalculationEvent5.start = geoCoordinate3;
            final MapEvents.RouteCalculationEvent routeCalculationEvent6 = HereRouteManager.routeCalculationEvent;
            final List<GeoCoordinate> list2 = list;
            routeCalculationEvent6.waypoints = list2;
            final MapEvents.RouteCalculationEvent routeCalculationEvent7 = HereRouteManager.routeCalculationEvent;
            final GeoCoordinate geoCoordinate4 = geoCoordinate2;
            routeCalculationEvent7.end = geoCoordinate4;
            final MapEvents.RouteCalculationEvent routeCalculationEvent8 = HereRouteManager.routeCalculationEvent;
            final NavigationRouteRequest navigationRouteRequest = activeRouteRequest;
            routeCalculationEvent8.request = navigationRouteRequest;
            final MapEvents.RouteCalculationEvent routeCalculationEvent9 = HereRouteManager.routeCalculationEvent;
            final RouteOptions routeOptions2 = routeOptions;
            routeCalculationEvent9.routeOptions = routeOptions2;
            final Bus bus = HereRouteManager.bus;
            final MapEvents.RouteCalculationEvent routeCalculationEvent10 = HereRouteManager.routeCalculationEvent;
            bus.post(routeCalculationEvent10);
            final Object o2 = o;
            // monitorexit(o2)
            final HereRouteCalculator hereRouteCalculator = activeRouteCalculator;
            final NavigationRouteRequest navigationRouteRequest2 = activeRouteRequest;
            final GeoCoordinate geoCoordinate5 = geoCoordinate;
            final List<GeoCoordinate> list3 = list;
            final GeoCoordinate geoCoordinate6 = geoCoordinate2;
            final boolean b2 = true;
            final NavigationRouteRequest navigationRouteRequest3 = activeRouteRequest;
            final Runnable runnable2 = runnable;
            final GeoCoordinate geoCoordinate7 = geoCoordinate;
            final List<GeoCoordinate> list4 = list;
            final GeoCoordinate geoCoordinate8 = geoCoordinate2;
            final HereRouteCalculator.RouteCalculatorListener routeCalculatorListener = new HereRouteCalculator.RouteCalculatorListener(list4) {
                final /* synthetic */ GeoCoordinate val$endPoint;
                final /* synthetic */ NavigationRouteRequest val$request;
                final /* synthetic */ Runnable val$runnable;
                final /* synthetic */ GeoCoordinate val$startPoint;
                final /* synthetic */ List val$waypoints;
                
                @Override
                public void error(final RoutingError routingError, final Throwable t) {
                    HereRouteManager.handler.removeCallbacks(runnable2);
                    Label_0083: {
                        if (routingError == null) {
                            break Label_0083;
                        }
                        HereRouteManager.sLogger.v("got error for traffic [" + routingError.name() + "]");
                        while (true) {
                            final Object access$400 = HereRouteManager.lockObj;
                            synchronized (access$400) {
                                if (HereRouteManager.activeRouteCancelledByUser) {
                                    HereRouteManager.sLogger.v("user cancelled routing");
                                    returnErrorResponse(RequestStatus.REQUEST_CANCELLED, navigationRouteRequest3, null);
                                }
                                else {
                                    // monitorexit(access$400)
                                    HereRouteManager.sLogger.v("launching no traffic");
                                    routeSearchNoTraffic(navigationRouteRequest3, geoCoordinate7, list4, geoCoordinate8, false);
                                }
                                return;
                                HereRouteManager.sLogger.v("got error for traffic", t);
                            }
                        }
                    }
                }
                
                @Override
                public void postSuccess(final ArrayList<NavigationRouteResult> list) {
                    HereRouteManager.sLogger.v("got result for traffic");
                    returnSuccessResponse(navigationRouteRequest3, list, true);
                }
                
                @Override
                public void preSuccess() {
                    final Object access$400 = HereRouteManager.lockObj;
                    synchronized (access$400) {
                        HereRouteManager.activeRouteCalculator = null;
                        // monitorexit(access$400)
                        HereRouteManager.handler.removeCallbacks(runnable2);
                        HereRouteManager.sLogger.v("route successfully calculated, post calc in progress");
                    }
                }
                
                @Override
                public void progress(final int n) {
                }
            };
            final int n = HereRouteManager.MAX_ROUTES;
            final RouteOptions routeOptions3 = routeOptions;
            final boolean b3 = true;
            hereRouteCalculator.calculateRoute(navigationRouteRequest2, geoCoordinate5, list3, geoCoordinate6, b2, (HereRouteCalculator.RouteCalculatorListener)routeCalculatorListener, n, routeOptions3, b3);
        }
        finally {
        }
        // monitorexit(o)
    }
    
    public static void startNavigationLookup(final Destination lookupDestination) {
        if (lookupDestination == null) {
            HereRouteManager.sLogger.e("startNavigationLookup null destination");
        }
        else {
            final Object lockObj = HereRouteManager.lockObj;
            synchronized (lockObj) {
                HereRouteManager.routeCalculationEvent = new MapEvents.RouteCalculationEvent();
                HereRouteManager.routeCalculationEvent.state = MapEvents.RouteCalculationState.FINDING_NAV_COORDINATES;
                HereRouteManager.routeCalculationEvent.lookupDestination = lookupDestination;
                HereRouteManager.routeCalculationEvent.routeOptions = HereMapsManager.getInstance().getRouteOptions();
                HereRouteManager.sLogger.v("startNavigationLookup sent nav_coordinate_lookup ");
                HereRouteManager.bus.post(HereRouteManager.routeCalculationEvent);
            }
        }
    }
}
