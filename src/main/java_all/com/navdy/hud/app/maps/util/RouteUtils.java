package com.navdy.hud.app.maps.util;

import java.util.ArrayList;
import java.util.Iterator;
import com.here.android.mpa.common.GeoCoordinate;
import java.util.List;
import android.content.res.Resources;
import com.navdy.service.library.log.Logger;

public class RouteUtils
{
    private static final double SCALE_FACTOR = 1048576.0;
    private static final Logger sLogger;
    
    static {
        sLogger = new Logger(RouteUtils.class);
    }
    
    public static String formatEtaMinutes(final Resources resources, int n) {
        String s;
        if (n < 60) {
            s = resources.getString(R.string.eta_time_min, new Object[] { n });
        }
        else if (n < 1440) {
            final int n2 = n / 60;
            n -= n2 * 60;
            if (n > 0) {
                s = resources.getString(R.string.eta_time_hour, new Object[] { n2, n });
            }
            else {
                s = resources.getString(R.string.eta_time_hour_no_min, new Object[] { n2 });
            }
        }
        else {
            final int n3 = n / 1440;
            n = (n - n3 * 1440) / 60;
            if (n > 0) {
                s = resources.getString(R.string.eta_time_day, new Object[] { n3, n });
            }
            else {
                s = resources.getString(R.string.eta_time_day_no_hour, new Object[] { n3 });
            }
        }
        return s;
    }
    
    static Bounds getLatLngBounds(final List<GeoCoordinate> list) {
        double n;
        double min = n = list.get(0).getLatitude();
        double n2;
        double min2 = n2 = list.get(0).getLongitude();
        for (final GeoCoordinate geoCoordinate : list) {
            final double latitude = geoCoordinate.getLatitude();
            final double longitude = geoCoordinate.getLongitude();
            min = Math.min(latitude, min);
            n = Math.max(latitude, n);
            min2 = Math.min(longitude, min2);
            n2 = Math.max(longitude, n2);
        }
        return new Bounds(n2 - min2, n - min);
    }
    
    public static List<Float> simplify(final List<GeoCoordinate> list) {
        final Bounds latLngBounds = getLatLngBounds(list);
        return simplify(list, 2000.0 * Math.max(latLngBounds.width, latLngBounds.height));
    }
    
    public static List<Float> simplify(final List<GeoCoordinate> list, final double n) {
        return simplifyRadialDistance(list, n * n);
    }
    
    static List<Float> simplifyRadialDistance(final List<GeoCoordinate> list, final double n) {
        List<Float> list2;
        if (list == null || list.size() == 0) {
            list2 = null;
        }
        else {
            float n2 = (float)list.get(0).getLatitude();
            float n3 = (float)list.get(0).getLongitude();
            final ArrayList<Float> list3 = new ArrayList<Float>();
            list3.add(n2);
            list3.add(n3);
            final int size = list.size();
            final GeoCoordinate geoCoordinate = list.get(0);
            final float n4 = (float)geoCoordinate.getLatitude();
            final float n5 = (float)geoCoordinate.getLongitude();
            list3.add(n4);
            list3.add(n5);
            for (int i = 1; i < size - 1; ++i) {
                final GeoCoordinate geoCoordinate2 = list.get(i);
                final float n6 = (float)geoCoordinate2.getLatitude();
                final float n7 = (float)geoCoordinate2.getLongitude();
                final double n8 = (n6 - n2) * 1048576.0;
                final double n9 = (n7 - n3) * 1048576.0;
                if (n8 * n8 + n9 * n9 > n) {
                    list3.add(n6);
                    list3.add(n7);
                    n2 = n6;
                    n3 = n7;
                }
            }
            final GeoCoordinate geoCoordinate3 = list.get(size - 1);
            final float n10 = (float)geoCoordinate3.getLatitude();
            final float n11 = (float)geoCoordinate3.getLongitude();
            list3.add(n10);
            list3.add(n11);
            list2 = list3;
        }
        return list2;
    }
    
    public static final class Bounds
    {
        public double height;
        public double width;
        
        Bounds(final double width, final double height) {
            this.width = width;
            this.height = height;
        }
    }
}
