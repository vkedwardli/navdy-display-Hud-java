package com.navdy.hud.app.storage.db.table;

import com.navdy.hud.app.storage.db.DatabaseUtil;
import android.database.sqlite.SQLiteDatabase;
import com.navdy.service.library.log.Logger;

public class VinInformationTable
{
    public static final String TABLE_NAME = "vininfo";
    public static final String UNKNOWN_VIN = "UNKNOWN_VIN";
    public static final String VIN_INFO = "info";
    public static final String VIN_NUMBER = "vin";
    private static final Logger sLogger;
    
    static {
        sLogger = new Logger(VinInformationTable.class);
    }
    
    public static void createTable(final SQLiteDatabase sqLiteDatabase) {
        createTable_1(sqLiteDatabase);
    }
    
    public static void createTable_1(final SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS " + "vininfo" + " (" + "vin" + " TEXT NOT NULL," + "info" + " TEXT" + ");");
        VinInformationTable.sLogger.v("createdTable:" + "vininfo");
        DatabaseUtil.createIndex(sqLiteDatabase, "vininfo", "vin", VinInformationTable.sLogger);
    }
}
