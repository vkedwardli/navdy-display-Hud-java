package com.navdy.hud.app.storage.db;

import android.database.sqlite.SQLiteDatabaseCorruptException;
import com.navdy.hud.app.storage.db.table.FavoriteContactsTable;
import com.navdy.hud.app.storage.db.table.RecentCallsTable;
import com.navdy.hud.app.storage.db.table.MusicArtworkCacheTable;
import com.navdy.hud.app.storage.db.table.VinInformationTable;
import android.os.Build;
import com.navdy.hud.app.util.os.SystemProperties;
import com.navdy.hud.app.util.SerialNumber;
import android.annotation.SuppressLint;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteStatement;
import android.text.TextUtils;
import com.navdy.service.library.util.IOUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase;
import com.navdy.hud.app.HudApplication;
import java.io.File;
import com.navdy.hud.app.storage.PathManager;
import com.navdy.service.library.log.Logger;
import android.database.DatabaseErrorHandler;
import android.content.Context;
import android.database.sqlite.SQLiteOpenHelper;

public class HudDatabase extends SQLiteOpenHelper
{
    private static final String DATABASE_FILE;
    private static final String DATABASE_NAME = "hud.db";
    private static final int DATABASE_VERSION = 17;
    private static final String UPGRADE_METHOD_PREFIX = "upgradeDatabase_";
    private static final int VERSION_1 = 1;
    private static final int VERSION_10 = 10;
    private static final int VERSION_11 = 11;
    private static final int VERSION_12 = 12;
    private static final int VERSION_13 = 13;
    private static final int VERSION_14 = 14;
    private static final int VERSION_15 = 15;
    private static final int VERSION_16 = 16;
    private static final int VERSION_17 = 17;
    private static final int VERSION_2 = 2;
    private static final int VERSION_3 = 3;
    private static final int VERSION_4 = 4;
    private static final int VERSION_5 = 5;
    private static final int VERSION_6 = 6;
    private static final int VERSION_7 = 7;
    private static final int VERSION_8 = 8;
    private static final int VERSION_9 = 9;
    private static final Context context;
    private static final Object lockObj;
    private static volatile boolean sDBError;
    private static volatile Throwable sDBErrorStr;
    private static DatabaseErrorHandler sDatabaseErrorHandler;
    private static Logger sLogger;
    private static volatile HudDatabase sSingleton;
    
    static {
        HudDatabase.sLogger = new Logger(HudDatabase.class);
        DATABASE_FILE = PathManager.getInstance().getDatabaseDir() + File.separator + "hud.db";
        lockObj = new Object();
        context = HudApplication.getAppContext();
        HudDatabase.sDatabaseErrorHandler = (DatabaseErrorHandler)new DatabaseErrorHandler() {
            public void onCorruption(final SQLiteDatabase p0) {
                // 
                This method could not be decompiled.
                // 
                // Original Bytecode:
                // 
                //     3: ldc             "**** DB Corrupt ****"
                //     5: invokevirtual   com/navdy/service/library/log/Logger.e:(Ljava/lang/String;)V
                //     8: iconst_1       
                //     9: invokestatic    com/navdy/hud/app/storage/db/HudDatabase.access$102:(Z)Z
                //    12: pop            
                //    13: aconst_null    
                //    14: invokestatic    com/navdy/hud/app/storage/db/HudDatabase.access$202:(Ljava/lang/Throwable;)Ljava/lang/Throwable;
                //    17: pop            
                //    18: aload_1        
                //    19: invokevirtual   android/database/sqlite/SQLiteDatabase.isOpen:()Z
                //    22: ifne            29
                //    25: invokestatic    com/navdy/hud/app/storage/db/HudDatabase.deleteDatabaseFile:()V
                //    28: return         
                //    29: aconst_null    
                //    30: astore_2       
                //    31: aconst_null    
                //    32: astore_3       
                //    33: aload_2        
                //    34: astore          4
                //    36: aload_1        
                //    37: invokevirtual   android/database/sqlite/SQLiteDatabase.getAttachedDbs:()Ljava/util/List;
                //    40: astore          5
                //    42: aload           5
                //    44: astore_3       
                //    45: aload_3        
                //    46: astore          4
                //    48: aload_1        
                //    49: invokevirtual   android/database/sqlite/SQLiteDatabase.close:()V
                //    52: aload_3        
                //    53: ifnull          192
                //    56: aload_3        
                //    57: invokeinterface java/util/List.iterator:()Ljava/util/Iterator;
                //    62: astore_1       
                //    63: aload_1        
                //    64: invokeinterface java/util/Iterator.hasNext:()Z
                //    69: ifeq            192
                //    72: aload_1        
                //    73: invokeinterface java/util/Iterator.next:()Ljava/lang/Object;
                //    78: checkcast       Landroid/util/Pair;
                //    81: astore_3       
                //    82: invokestatic    com/navdy/hud/app/storage/db/HudDatabase.access$300:()Landroid/content/Context;
                //    85: aload_3        
                //    86: getfield        android/util/Pair.second:Ljava/lang/Object;
                //    89: checkcast       Ljava/lang/String;
                //    92: invokestatic    com/navdy/service/library/util/IOUtils.deleteFile:(Landroid/content/Context;Ljava/lang/String;)Z
                //    95: pop            
                //    96: goto            63
                //    99: astore_1       
                //   100: invokestatic    com/navdy/hud/app/storage/db/HudDatabase.access$000:()Lcom/navdy/service/library/log/Logger;
                //   103: aload_1        
                //   104: invokevirtual   com/navdy/service/library/log/Logger.e:(Ljava/lang/Throwable;)V
                //   107: goto            28
                //   110: astore          5
                //   112: aload_2        
                //   113: astore          4
                //   115: invokestatic    com/navdy/hud/app/storage/db/HudDatabase.access$000:()Lcom/navdy/service/library/log/Logger;
                //   118: aload           5
                //   120: invokevirtual   com/navdy/service/library/log/Logger.i:(Ljava/lang/Throwable;)V
                //   123: goto            45
                //   126: astore_1       
                //   127: aload           4
                //   129: ifnull          198
                //   132: aload           4
                //   134: invokeinterface java/util/List.iterator:()Ljava/util/Iterator;
                //   139: astore_3       
                //   140: aload_3        
                //   141: invokeinterface java/util/Iterator.hasNext:()Z
                //   146: ifeq            198
                //   149: aload_3        
                //   150: invokeinterface java/util/Iterator.next:()Ljava/lang/Object;
                //   155: checkcast       Landroid/util/Pair;
                //   158: astore          4
                //   160: invokestatic    com/navdy/hud/app/storage/db/HudDatabase.access$300:()Landroid/content/Context;
                //   163: aload           4
                //   165: getfield        android/util/Pair.second:Ljava/lang/Object;
                //   168: checkcast       Ljava/lang/String;
                //   171: invokestatic    com/navdy/service/library/util/IOUtils.deleteFile:(Landroid/content/Context;Ljava/lang/String;)Z
                //   174: pop            
                //   175: goto            140
                //   178: astore_1       
                //   179: aload_3        
                //   180: astore          4
                //   182: invokestatic    com/navdy/hud/app/storage/db/HudDatabase.access$000:()Lcom/navdy/service/library/log/Logger;
                //   185: aload_1        
                //   186: invokevirtual   com/navdy/service/library/log/Logger.i:(Ljava/lang/Throwable;)V
                //   189: goto            52
                //   192: invokestatic    com/navdy/hud/app/storage/db/HudDatabase.deleteDatabaseFile:()V
                //   195: goto            28
                //   198: invokestatic    com/navdy/hud/app/storage/db/HudDatabase.deleteDatabaseFile:()V
                //   201: aload_1        
                //   202: athrow         
                //    Exceptions:
                //  Try           Handler
                //  Start  End    Start  End    Type                                     
                //  -----  -----  -----  -----  -----------------------------------------
                //  18     28     99     110    Ljava/lang/Throwable;
                //  36     42     110    126    Landroid/database/sqlite/SQLiteException;
                //  36     42     126    178    Any
                //  48     52     178    192    Landroid/database/sqlite/SQLiteException;
                //  48     52     126    178    Any
                //  56     63     99     110    Ljava/lang/Throwable;
                //  63     96     99     110    Ljava/lang/Throwable;
                //  115    123    126    178    Any
                //  132    140    99     110    Ljava/lang/Throwable;
                //  140    175    99     110    Ljava/lang/Throwable;
                //  182    189    126    178    Any
                //  192    195    99     110    Ljava/lang/Throwable;
                //  198    203    99     110    Ljava/lang/Throwable;
                // 
                // The error that occurred was:
                // 
                // java.lang.IndexOutOfBoundsException: Index: 92, Size: 92
                //     at java.util.ArrayList.rangeCheck(Unknown Source)
                //     at java.util.ArrayList.get(Unknown Source)
                //     at com.strobel.decompiler.ast.AstBuilder.convertToAst(AstBuilder.java:3321)
                //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:113)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:210)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformCall(AstMethodBodyBuilder.java:1163)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformByteCode(AstMethodBodyBuilder.java:1010)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformExpression(AstMethodBodyBuilder.java:540)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformByteCode(AstMethodBodyBuilder.java:554)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformExpression(AstMethodBodyBuilder.java:540)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformNode(AstMethodBodyBuilder.java:392)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformBlock(AstMethodBodyBuilder.java:333)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:294)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
                //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
                //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
                //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
                //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
                //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
                //     at java.lang.Thread.run(Unknown Source)
                // 
                throw new IllegalStateException("An error occurred while decompiling this method.");
            }
        };
    }
    
    private HudDatabase() {
        super(HudDatabase.context, HudDatabase.DATABASE_FILE, (SQLiteDatabase$CursorFactory)null, 17, HudDatabase.sDatabaseErrorHandler);
    }
    
    public static void deleteDatabaseFile() {
        HudDatabase.sLogger.v("deleteDatabaseFile");
        try {
            final File file = new File(HudDatabase.DATABASE_FILE);
            if (file.exists()) {
                final String absolutePath = file.getAbsolutePath();
                HudDatabase.sLogger.e("delete:" + IOUtils.deleteFile(HudDatabase.context, absolutePath) + " - " + absolutePath);
            }
            final File file2 = new File(HudDatabase.DATABASE_FILE + "-journal");
            if (file2.exists()) {
                final String absolutePath2 = file2.getAbsolutePath();
                HudDatabase.sLogger.e("delete:" + IOUtils.deleteFile(HudDatabase.context, absolutePath2) + " - " + absolutePath2);
            }
            final File file3 = new File(HudDatabase.DATABASE_FILE + "-shm");
            if (file3.exists()) {
                final String absolutePath3 = file3.getAbsolutePath();
                HudDatabase.sLogger.e("delete:" + IOUtils.deleteFile(HudDatabase.context, absolutePath3) + " - " + absolutePath3);
            }
            final File file4 = new File(HudDatabase.DATABASE_FILE + "-wal");
            if (file4.exists()) {
                final String absolutePath4 = file4.getAbsolutePath();
                HudDatabase.sLogger.e("delete:" + IOUtils.deleteFile(HudDatabase.context, absolutePath4) + " - " + absolutePath4);
            }
            SQLiteDatabase.deleteDatabase(file);
        }
        catch (Throwable t) {
            HudDatabase.sLogger.e("Error while trying to delete dbase");
        }
    }
    
    public static Throwable getErrorStr() {
        return HudDatabase.sDBErrorStr;
    }
    
    public static HudDatabase getInstance() {
        Label_0078: {
            if (HudDatabase.sSingleton != null) {
                break Label_0078;
            }
            final Object lockObj = HudDatabase.lockObj;
            synchronized (lockObj) {
                if (HudDatabase.sSingleton != null) {
                    break Label_0078;
                }
                try {
                    if (!isDatabaseStable(HudDatabase.DATABASE_FILE)) {
                        HudDatabase.sLogger.i("db is not stable, deleting it");
                        deleteDatabaseFile();
                        HudDatabase.sLogger.i("db delete complete");
                    }
                    HudDatabase.sLogger.v("creating db-instance");
                    HudDatabase.sSingleton = new HudDatabase();
                    HudDatabase.sLogger.v("created db-instance");
                    return HudDatabase.sSingleton;
                }
                catch (Throwable sdbErrorStr) {
                    HudDatabase.sDBError = true;
                    HudDatabase.sDBErrorStr = sdbErrorStr;
                    HudDatabase.sLogger.e(sdbErrorStr);
                }
            }
        }
    }
    
    private static boolean isDatabaseIntegrityFine(final SQLiteDatabase sqLiteDatabase) {
        SQLiteStatement compileStatement;
        final SQLiteStatement sqLiteStatement = compileStatement = null;
        try {
            final long currentTimeMillis = System.currentTimeMillis();
            compileStatement = sqLiteStatement;
            final SQLiteStatement sqLiteStatement2 = compileStatement = sqLiteDatabase.compileStatement("PRAGMA integrity_check");
            final String simpleQueryForString = sqLiteStatement2.simpleQueryForString();
            compileStatement = sqLiteStatement2;
            final long currentTimeMillis2 = System.currentTimeMillis();
            compileStatement = sqLiteStatement2;
            final Logger sLogger = HudDatabase.sLogger;
            compileStatement = sqLiteStatement2;
            compileStatement = sqLiteStatement2;
            final StringBuilder sb = new StringBuilder();
            compileStatement = sqLiteStatement2;
            sLogger.v(sb.append("db integrity check took:").append(currentTimeMillis2 - currentTimeMillis).toString());
            compileStatement = sqLiteStatement2;
            Label_0123: {
                if (TextUtils.isEmpty((CharSequence)simpleQueryForString)) {
                    break Label_0123;
                }
                compileStatement = sqLiteStatement2;
                if (!"ok".equalsIgnoreCase(simpleQueryForString)) {
                    break Label_0123;
                }
                boolean b = true;
                if (sqLiteStatement2 != null) {
                    sqLiteStatement2.close();
                    b = b;
                }
                return b;
            }
            boolean b;
            final boolean b2 = b = false;
            if (sqLiteStatement2 != null) {
                sqLiteStatement2.close();
                b = b2;
                return b;
            }
            return b;
        }
        finally {
            if (compileStatement != null) {
                compileStatement.close();
            }
        }
    }
    
    private static boolean isDatabaseStable(String openDatabase) {
        boolean b = true;
        final File file = new File(openDatabase);
        if (file.exists()) {
            Label_0062: {
                try {
                    openDatabase = (String)SQLiteDatabase.openDatabase(openDatabase, (SQLiteDatabase$CursorFactory)null, 16);
                    if (openDatabase != null) {
                        break Label_0062;
                    }
                    HudDatabase.sLogger.e("dbase could not be opened");
                    b = false;
                }
                catch (SQLiteException ex) {
                    HudDatabase.sLogger.e("dbase could not be opened", (Throwable)ex);
                    b = false;
                }
                return b;
                while (true) {
                    try {
                        if (!isDatabaseIntegrityFine((SQLiteDatabase)openDatabase)) {
                            HudDatabase.sLogger.e("data integrity is not valid");
                            ((SQLiteDatabase)openDatabase).close();
                            b = false;
                        }
                        else {
                            if (((SQLiteDatabase)openDatabase).inTransaction()) {
                                HudDatabase.sLogger.v("dbase in transaction");
                                ((SQLiteDatabase)openDatabase).endTransaction();
                            }
                            if (((SQLiteDatabase)openDatabase).isDbLockedByCurrentThread()) {
                                HudDatabase.sLogger.v("dbase is currently locked");
                            }
                            final long currentTimeMillis = System.currentTimeMillis();
                            ((SQLiteDatabase)openDatabase).execSQL("VACUUM");
                            HudDatabase.sLogger.i("dbase VACCUM complete in " + (System.currentTimeMillis() - currentTimeMillis));
                            ((SQLiteDatabase)openDatabase).close();
                            if (!file.exists() || !file.canRead()) {
                                HudDatabase.sLogger.e("dbase not stable");
                                b = false;
                            }
                        }
                    }
                    catch (Exception ex2) {
                        HudDatabase.sLogger.e(ex2);
                        ((SQLiteDatabase)openDatabase).close();
                        continue;
                    }
                    finally {
                        ((SQLiteDatabase)openDatabase).close();
                    }
                    break;
                }
            }
        }
        return b;
    }
    
    public static boolean isInErrorState() {
        return HudDatabase.sDBError;
    }
    
    @SuppressLint({ "CommitPrefEdits" })
    public static void upgradeDatabase_10(final SQLiteDatabase sqLiteDatabase) {
        RemoteDeviceManager.getInstance().getSharedPreferences().edit().putBoolean("gesture.engine", true).commit();
        HudDatabase.sLogger.v("enabling gesture engine");
    }
    
    public static void upgradeDatabase_11(final SQLiteDatabase sqLiteDatabase) {
        try {
            HudDatabase.sLogger.v("trying to set [" + "persist.sys.hud_gps" + "]");
            final char char1 = SerialNumber.instance.revisionCode.charAt(0);
            if (char1 >= '4' && char1 <= '6') {
                SystemProperties.set("persist.sys.hud_gps", "-1");
                HudDatabase.sLogger.v("setting property for [" + Build.SERIAL + "] to [-1] digit[" + char1 + "]");
            }
            else {
                HudDatabase.sLogger.v("not setting property [" + Build.SERIAL + "]");
            }
        }
        catch (Throwable t) {
            HudDatabase.sLogger.e("dbupgrade --> 11 failed", t);
        }
    }
    
    public static void upgradeDatabase_12(final SQLiteDatabase sqLiteDatabase) {
        try {
            final Context appContext = HudApplication.getAppContext();
            IOUtils.deleteDirectory(appContext, new File(appContext.getFilesDir(), ".Fabric"));
            HudDatabase.sLogger.v("dbupgrade --> Fabric dir removed");
        }
        catch (Throwable t) {
            HudDatabase.sLogger.e("dbupgrade --> 12 failed", t);
        }
    }
    
    public static void upgradeDatabase_13(final SQLiteDatabase sqLiteDatabase) {
        VinInformationTable.createTable(sqLiteDatabase);
    }
    
    public static void upgradeDatabase_14(final SQLiteDatabase p0) {
        // 
        This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     3: ldc_w           "looking for native crash files"
        //     6: invokevirtual   com/navdy/service/library/log/Logger.v:(Ljava/lang/String;)V
        //     9: new             Ljava/io/File;
        //    12: astore_1       
        //    13: aload_1        
        //    14: ldc_w           "/data/anr"
        //    17: invokespecial   java/io/File.<init>:(Ljava/lang/String;)V
        //    20: aload_1        
        //    21: invokevirtual   java/io/File.exists:()Z
        //    24: ifeq            231
        //    27: aload_1        
        //    28: invokevirtual   java/io/File.isDirectory:()Z
        //    31: ifeq            231
        //    34: new             Lcom/navdy/hud/app/storage/db/HudDatabase$2;
        //    37: astore_0       
        //    38: aload_0        
        //    39: invokespecial   com/navdy/hud/app/storage/db/HudDatabase$2.<init>:()V
        //    42: aload_1        
        //    43: aload_0        
        //    44: invokevirtual   java/io/File.list:(Ljava/io/FilenameFilter;)[Ljava/lang/String;
        //    47: astore_0       
        //    48: aload_0        
        //    49: ifnull          57
        //    52: aload_0        
        //    53: arraylength    
        //    54: ifne            124
        //    57: getstatic       com/navdy/hud/app/storage/db/HudDatabase.sLogger:Lcom/navdy/service/library/log/Logger;
        //    60: ldc_w           "no native crash files"
        //    63: invokevirtual   com/navdy/service/library/log/Logger.v:(Ljava/lang/String;)V
        //    66: getstatic       com/navdy/hud/app/storage/db/HudDatabase.sLogger:Lcom/navdy/service/library/log/Logger;
        //    69: ldc_w           "looking for tombstone"
        //    72: invokevirtual   com/navdy/service/library/log/Logger.v:(Ljava/lang/String;)V
        //    75: new             Ljava/io/File;
        //    78: astore_0       
        //    79: aload_0        
        //    80: ldc_w           "/data/tombstones"
        //    83: invokespecial   java/io/File.<init>:(Ljava/lang/String;)V
        //    86: aload_0        
        //    87: invokevirtual   java/io/File.exists:()Z
        //    90: ifeq            372
        //    93: aload_0        
        //    94: invokevirtual   java/io/File.isDirectory:()Z
        //    97: ifeq            372
        //   100: aload_0        
        //   101: invokevirtual   java/io/File.list:()[Ljava/lang/String;
        //   104: astore_0       
        //   105: aload_0        
        //   106: ifnull          114
        //   109: aload_0        
        //   110: arraylength    
        //   111: ifne            265
        //   114: getstatic       com/navdy/hud/app/storage/db/HudDatabase.sLogger:Lcom/navdy/service/library/log/Logger;
        //   117: ldc_w           "no tombstones"
        //   120: invokevirtual   com/navdy/service/library/log/Logger.v:(Ljava/lang/String;)V
        //   123: return         
        //   124: iconst_0       
        //   125: istore_2       
        //   126: iload_2        
        //   127: aload_0        
        //   128: arraylength    
        //   129: if_icmpge       66
        //   132: new             Ljava/io/File;
        //   135: astore_3       
        //   136: new             Ljava/lang/StringBuilder;
        //   139: astore_1       
        //   140: aload_1        
        //   141: invokespecial   java/lang/StringBuilder.<init>:()V
        //   144: aload_3        
        //   145: aload_1        
        //   146: ldc_w           "/data/anr"
        //   149: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   152: getstatic       java/io/File.separator:Ljava/lang/String;
        //   155: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   158: aload_0        
        //   159: iload_2        
        //   160: aaload         
        //   161: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   164: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   167: invokespecial   java/io/File.<init>:(Ljava/lang/String;)V
        //   170: aload_3        
        //   171: invokevirtual   java/io/File.getAbsolutePath:()Ljava/lang/String;
        //   174: astore_1       
        //   175: aload_3        
        //   176: invokevirtual   java/io/File.delete:()Z
        //   179: istore          4
        //   181: getstatic       com/navdy/hud/app/storage/db/HudDatabase.sLogger:Lcom/navdy/service/library/log/Logger;
        //   184: astore_3       
        //   185: new             Ljava/lang/StringBuilder;
        //   188: astore          5
        //   190: aload           5
        //   192: invokespecial   java/lang/StringBuilder.<init>:()V
        //   195: aload_3        
        //   196: aload           5
        //   198: ldc_w           "removing native crash file["
        //   201: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   204: aload_1        
        //   205: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   208: ldc_w           "] :"
        //   211: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   214: iload           4
        //   216: invokevirtual   java/lang/StringBuilder.append:(Z)Ljava/lang/StringBuilder;
        //   219: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   222: invokevirtual   com/navdy/service/library/log/Logger.v:(Ljava/lang/String;)V
        //   225: iinc            2, 1
        //   228: goto            126
        //   231: getstatic       com/navdy/hud/app/storage/db/HudDatabase.sLogger:Lcom/navdy/service/library/log/Logger;
        //   234: ldc_w           "no native crashes to delete"
        //   237: invokevirtual   com/navdy/service/library/log/Logger.v:(Ljava/lang/String;)V
        //   240: goto            66
        //   243: astore_0       
        //   244: getstatic       com/navdy/hud/app/storage/db/HudDatabase.sLogger:Lcom/navdy/service/library/log/Logger;
        //   247: aload_0        
        //   248: invokevirtual   com/navdy/service/library/log/Logger.e:(Ljava/lang/Throwable;)V
        //   251: goto            66
        //   254: astore_0       
        //   255: getstatic       com/navdy/hud/app/storage/db/HudDatabase.sLogger:Lcom/navdy/service/library/log/Logger;
        //   258: aload_0        
        //   259: invokevirtual   com/navdy/service/library/log/Logger.e:(Ljava/lang/Throwable;)V
        //   262: goto            123
        //   265: iconst_0       
        //   266: istore_2       
        //   267: iload_2        
        //   268: aload_0        
        //   269: arraylength    
        //   270: if_icmpge       123
        //   273: new             Ljava/io/File;
        //   276: astore_3       
        //   277: new             Ljava/lang/StringBuilder;
        //   280: astore_1       
        //   281: aload_1        
        //   282: invokespecial   java/lang/StringBuilder.<init>:()V
        //   285: aload_3        
        //   286: aload_1        
        //   287: ldc_w           "/data/tombstones"
        //   290: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   293: getstatic       java/io/File.separator:Ljava/lang/String;
        //   296: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   299: aload_0        
        //   300: iload_2        
        //   301: aaload         
        //   302: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   305: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   308: invokespecial   java/io/File.<init>:(Ljava/lang/String;)V
        //   311: aload_3        
        //   312: invokevirtual   java/io/File.getAbsolutePath:()Ljava/lang/String;
        //   315: astore_1       
        //   316: aload_3        
        //   317: invokevirtual   java/io/File.delete:()Z
        //   320: istore          4
        //   322: getstatic       com/navdy/hud/app/storage/db/HudDatabase.sLogger:Lcom/navdy/service/library/log/Logger;
        //   325: astore_3       
        //   326: new             Ljava/lang/StringBuilder;
        //   329: astore          5
        //   331: aload           5
        //   333: invokespecial   java/lang/StringBuilder.<init>:()V
        //   336: aload_3        
        //   337: aload           5
        //   339: ldc_w           "removing tombstone file["
        //   342: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   345: aload_1        
        //   346: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   349: ldc_w           "] :"
        //   352: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   355: iload           4
        //   357: invokevirtual   java/lang/StringBuilder.append:(Z)Ljava/lang/StringBuilder;
        //   360: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   363: invokevirtual   com/navdy/service/library/log/Logger.v:(Ljava/lang/String;)V
        //   366: iinc            2, 1
        //   369: goto            267
        //   372: getstatic       com/navdy/hud/app/storage/db/HudDatabase.sLogger:Lcom/navdy/service/library/log/Logger;
        //   375: ldc_w           "no tombstones to delete"
        //   378: invokevirtual   com/navdy/service/library/log/Logger.v:(Ljava/lang/String;)V
        //   381: goto            123
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  0      48     243    254    Ljava/lang/Throwable;
        //  52     57     243    254    Ljava/lang/Throwable;
        //  57     66     243    254    Ljava/lang/Throwable;
        //  66     105    254    265    Ljava/lang/Throwable;
        //  109    114    254    265    Ljava/lang/Throwable;
        //  114    123    254    265    Ljava/lang/Throwable;
        //  126    225    243    254    Ljava/lang/Throwable;
        //  231    240    243    254    Ljava/lang/Throwable;
        //  244    251    254    265    Ljava/lang/Throwable;
        //  267    366    254    265    Ljava/lang/Throwable;
        //  372    381    254    265    Ljava/lang/Throwable;
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0066:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    @SuppressLint({ "CommitPrefEdits" })
    public static void upgradeDatabase_15(final SQLiteDatabase sqLiteDatabase) {
        RemoteDeviceManager.getInstance().getSharedPreferences().edit().remove("last_low_voltage_event").commit();
    }
    
    public static void upgradeDatabase_16(final SQLiteDatabase sqLiteDatabase) {
        MusicArtworkCacheTable.createTable(sqLiteDatabase);
    }
    
    @SuppressLint({ "CommitPrefEdits" })
    public static void upgradeDatabase_17(final SQLiteDatabase sqLiteDatabase) {
        RemoteDeviceManager.getInstance().getSharedPreferences().edit().remove("dial_reminder_time").commit();
    }
    
    public static void upgradeDatabase_2(final SQLiteDatabase sqLiteDatabase) {
        RecentCallsTable.upgradeDatabase_2(sqLiteDatabase);
        FavoriteContactsTable.createTable(sqLiteDatabase);
    }
    
    public static void upgradeDatabase_3(final SQLiteDatabase sqLiteDatabase) {
    }
    
    public static void upgradeDatabase_4(final SQLiteDatabase sqLiteDatabase) {
    }
    
    public static void upgradeDatabase_5(final SQLiteDatabase sqLiteDatabase) {
    }
    
    public static void upgradeDatabase_6(final SQLiteDatabase sqLiteDatabase) {
        RemoteDeviceManager.getInstance().getSharedPreferences().edit().putString("map.scheme", "carnav.day").apply();
        HudDatabase.sLogger.v("map scheme changed to carnav.day");
    }
    
    public static void upgradeDatabase_7(final SQLiteDatabase sqLiteDatabase) {
        final File file = new File(PathManager.getInstance().getHereVoiceSkinsPath(), "en-US_TTS");
        IOUtils.deleteDirectory(HudApplication.getAppContext(), file);
        HudDatabase.sLogger.v("deleted old here voice skins [" + file + "]");
    }
    
    public static void upgradeDatabase_8(final SQLiteDatabase sqLiteDatabase) {
        RemoteDeviceManager.getInstance().getSharedPreferences().edit().putString("screen.led_brightness", "255").apply();
        HudDatabase.sLogger.v("default max LED brightness changed to 255");
    }
    
    public static void upgradeDatabase_9(final SQLiteDatabase sqLiteDatabase) {
        RemoteDeviceManager.getInstance().getSharedPreferences().edit().putBoolean("gesture.engine", false).apply();
        HudDatabase.sLogger.v("disabling gesture engine");
    }
    
    public SQLiteDatabase getWritableDatabase() {
        // monitorenter(this)
        SQLiteDatabase writableDatabase = null;
        try {
            writableDatabase = super.getWritableDatabase();
            return writableDatabase;
        }
        catch (Throwable sdbErrorStr) {
            HudDatabase.sLogger.e(sdbErrorStr);
            if (sdbErrorStr instanceof SQLiteDatabaseCorruptException) {
                HudDatabase.sLogger.e(":: db corrupted");
                return writableDatabase;
            }
            HudDatabase.sDBError = true;
            HudDatabase.sDBErrorStr = sdbErrorStr;
            return writableDatabase;
        }
    }
    
    public void onCreate(final SQLiteDatabase sqLiteDatabase) {
        HudDatabase.sDBError = false;
        HudDatabase.sDBErrorStr = null;
        HudDatabase.sLogger.v("onCreate::start::");
        sqLiteDatabase.beginTransaction();
        try {
            RecentCallsTable.createTable(sqLiteDatabase);
            FavoriteContactsTable.createTable(sqLiteDatabase);
            VinInformationTable.createTable(sqLiteDatabase);
            MusicArtworkCacheTable.createTable(sqLiteDatabase);
            sqLiteDatabase.setTransactionSuccessful();
            sqLiteDatabase.endTransaction();
            HudDatabase.sLogger.v("onCreate::end::");
        }
        finally {
            sqLiteDatabase.endTransaction();
        }
    }
    
    public void onOpen(final SQLiteDatabase sqLiteDatabase) {
        HudDatabase.sLogger.v("onOpen::");
        super.onOpen(sqLiteDatabase);
    }
    
    public void onUpgrade(final SQLiteDatabase sqLiteDatabase, final int n, final int n2) {
        HudDatabase.sLogger.v("onUpgrade::start:: " + n + " ==>" + n2);
        int i = n + 1;
        while (i <= n2) {
            try {
                HudDatabase.sLogger.v("onUpgrade:: " + i);
                HudDatabase.class.getMethod("upgradeDatabase_" + i, SQLiteDatabase.class).invoke(null, sqLiteDatabase);
                ++i;
                continue;
            }
            catch (Throwable t) {
                HudDatabase.sLogger.e(t);
                throw new RuntimeException(t);
            }
            break;
        }
        HudDatabase.sLogger.v("onUpgrade::end:: " + n + " ==>" + n2);
    }
}
