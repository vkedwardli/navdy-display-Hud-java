package com.navdy.hud.app.storage.db.table;

import com.navdy.hud.app.storage.db.DatabaseUtil;
import android.database.sqlite.SQLiteDatabase;
import com.navdy.service.library.log.Logger;

public class FavoriteContactsTable
{
    public static final String DEFAULT_IMAGE_INDEX = "def_image";
    public static final String DRIVER_ID = "device_id";
    public static final String NAME = "name";
    public static final String NUMBER = "number";
    public static final String NUMBER_NUMERIC = "number_numeric";
    public static final String NUMBER_TYPE = "number_type";
    public static final String TABLE_NAME = "fav_contacts";
    private static final Logger sLogger;
    
    static {
        sLogger = new Logger(FavoriteContactsTable.class);
    }
    
    public static void createTable(final SQLiteDatabase sqLiteDatabase) {
        createTable_1(sqLiteDatabase);
    }
    
    public static void createTable_1(final SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS " + "fav_contacts" + " (" + "device_id" + " TEXT NOT NULL," + "name" + " TEXT," + "number" + " TEXT NOT NULL," + "number_type" + " INTEGER NOT NULL," + "def_image" + " INTEGER," + "number_numeric" + " INTEGER" + ");");
        FavoriteContactsTable.sLogger.v("createdTable:" + "fav_contacts");
        final String string = "fav_contacts" + "_" + "device_id";
        sqLiteDatabase.execSQL("CREATE INDEX IF NOT EXISTS " + string + " ON " + "fav_contacts" + "(" + "device_id" + ");");
        FavoriteContactsTable.sLogger.v("createdIndex:" + string);
        final String string2 = "fav_contacts" + "_" + "number";
        sqLiteDatabase.execSQL("CREATE INDEX IF NOT EXISTS " + string2 + " ON " + "fav_contacts" + "(" + "number" + ");");
        FavoriteContactsTable.sLogger.v("createdIndex:" + string2);
        final String string3 = "fav_contacts" + "_" + "number_type";
        sqLiteDatabase.execSQL("CREATE INDEX IF NOT EXISTS " + string3 + " ON " + "fav_contacts" + "(" + "number_type" + ");");
        FavoriteContactsTable.sLogger.v("createdIndex:" + string3);
        final String string4 = "fav_contacts" + "_" + "def_image";
        sqLiteDatabase.execSQL("CREATE INDEX IF NOT EXISTS " + string4 + " ON " + "fav_contacts" + "(" + "def_image" + ");");
        FavoriteContactsTable.sLogger.v("createdIndex:" + string4);
        final String string5 = "fav_contacts" + "_" + "number_numeric";
        sqLiteDatabase.execSQL("CREATE INDEX IF NOT EXISTS " + string5 + " ON " + "fav_contacts" + "(" + "number_numeric" + ");");
        FavoriteContactsTable.sLogger.v("createdIndex:" + string5);
    }
    
    public static void upgradeDatabase_1(final SQLiteDatabase sqLiteDatabase) {
        DatabaseUtil.dropTable(sqLiteDatabase, "fav_contacts", FavoriteContactsTable.sLogger);
        createTable_1(sqLiteDatabase);
    }
}
