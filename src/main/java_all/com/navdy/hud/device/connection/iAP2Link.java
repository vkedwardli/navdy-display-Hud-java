package com.navdy.hud.device.connection;

import android.support.annotation.NonNull;
import java.util.Arrays;
import com.navdy.service.library.device.connection.Connection;
import com.navdy.service.library.device.link.IOThread;
import com.navdy.service.library.device.link.EventRequest;
import java.util.concurrent.LinkedBlockingDeque;
import com.navdy.service.library.events.NavdyEventUtil;
import com.navdy.hud.mfi.LinkPacket;
import com.navdy.hud.mfi.CommunicationUpdate;
import com.navdy.hud.mfi.CallStateUpdate;
import com.navdy.hud.mfi.Utils;
import java.io.OutputStream;
import java.io.InputStream;
import java.io.Closeable;
import com.navdy.service.library.util.IOUtils;
import com.navdy.service.library.device.connection.ConnectionType;
import android.text.TextUtils;
import com.navdy.service.library.events.input.LaunchAppEvent;
import com.navdy.service.library.events.callcontrol.PhoneStatusResponse;
import com.squareup.wire.Message;
import com.navdy.service.library.events.callcontrol.TelephonyResponse;
import com.navdy.service.library.events.RequestStatus;
import com.navdy.service.library.events.callcontrol.TelephonyRequest;
import android.bluetooth.BluetoothDevice;
import com.navdy.service.library.network.SocketAdapter;
import com.navdy.service.library.events.Ext_NavdyEvent;
import java.io.IOException;
import com.navdy.service.library.events.WireUtil;
import java.util.HashMap;
import android.content.Context;
import com.navdy.hud.mfi.IAPListener;
import com.squareup.wire.Wire;
import com.navdy.service.library.device.link.ReaderThread;
import android.bluetooth.BluetoothAdapter;
import com.navdy.service.library.device.link.LinkListener;
import com.navdy.service.library.events.callcontrol.CallAction;
import com.navdy.hud.mfi.IAPCommunicationsManager;
import com.navdy.service.library.events.NavdyEvent;
import java.util.Map;
import com.navdy.service.library.device.link.WriterThread;
import com.navdy.service.library.events.callcontrol.PhoneEvent;
import com.navdy.service.library.log.Logger;
import com.navdy.hud.mfi.EASession;
import com.navdy.hud.mfi.IAPCommunicationsUpdateListener;
import com.navdy.hud.mfi.iAPProcessor;
import com.navdy.hud.mfi.LinkLayer;
import com.navdy.service.library.device.link.Link;

public class iAP2Link implements Link, PhysicalLayer, StateListener, IAPCommunicationsUpdateListener
{
    public static EASession proxyEASession;
    private static final Logger sLogger;
    private int bandwidthLevel;
    private ConnectedThread connectedThread;
    private PhoneEvent currentPhoneEvent;
    private WriterThread.EventProcessor eventProcessor;
    private Map<NavdyEvent.MessageType, NavdyEventProcessor> eventProcessors;
    private IAPCommunicationsManager iAPCommunicationsManager;
    private iAPProcessor iAPProcessor;
    private CallAction lastCallActionRequested;
    private LinkLayer linkLayer;
    private LinkListener linkListener;
    private final BluetoothAdapter mAdapter;
    private iAP2MusicHelper musicHelper;
    private ProxyStream proxyStream;
    private ReaderThread readerThread;
    private Wire wire;
    private WriterThread writerThread;
    
    static {
        sLogger = new Logger(iAP2Link.class);
    }
    
    public iAP2Link(final IAPListener iapListener, final Context context) {
        this.bandwidthLevel = 1;
        this.eventProcessors = new HashMap<NavdyEvent.MessageType, NavdyEventProcessor>();
        this.eventProcessor = new WriterThread.EventProcessor() {
            @Override
            public byte[] processEvent(final byte[] array) {
                final NavdyEvent.MessageType eventType = WireUtil.getEventType(array);
                byte[] array2 = array;
                if (eventType != null) {
                    if (!iAP2Link.this.eventProcessors.containsKey(eventType)) {
                        array2 = array;
                    }
                    else {
                        final boolean b = false;
                        final NavdyEventProcessor navdyEventProcessor = iAP2Link.this.eventProcessors.get(eventType);
                        while (true) {
                            try {
                                final boolean processNavdyEvent = navdyEventProcessor.processNavdyEvent(iAP2Link.this.wire.<NavdyEvent>parseFrom(array, NavdyEvent.class));
                                array2 = array;
                                if (processNavdyEvent) {
                                    array2 = null;
                                }
                            }
                            catch (IOException ex) {
                                iAP2Link.sLogger.e("Error while parsing request", ex);
                                final boolean processNavdyEvent = b;
                                continue;
                            }
                            break;
                        }
                    }
                }
                return array2;
            }
        };
        this.proxyStream = new ProxyStream();
        this.mAdapter = BluetoothAdapter.getDefaultAdapter();
        (this.iAPProcessor = new iAPProcessor(iapListener, context)).setAccessoryName(this.getName());
        this.iAPCommunicationsManager = new IAPCommunicationsManager(this.iAPProcessor);
        this.musicHelper = new iAP2MusicHelper(this, this.iAPProcessor);
        this.linkLayer = new LinkLayer();
        this.iAPProcessor.connect(this.linkLayer);
        this.iAPProcessor.connect((iAPProcessor.StateListener)this);
        this.linkLayer.connect(this.iAPProcessor);
        this.linkLayer.connect((LinkLayer.PhysicalLayer)this);
        this.wire = new Wire((Class<?>[])new Class[] { Ext_NavdyEvent.class });
        this.addEventProcessors();
    }
    
    private void addEventProcessors() {
        this.eventProcessors.put(NavdyEvent.MessageType.TelephonyRequest, (NavdyEventProcessor)new NavdyEventProcessor() {
            @Override
            public boolean processNavdyEvent(final NavdyEvent navdyEvent) {
                final TelephonyRequest telephonyRequest = navdyEvent.<TelephonyRequest>getExtension(Ext_NavdyEvent.telephonyRequest);
                iAP2Link.this.lastCallActionRequested = telephonyRequest.action;
                final boolean performActionForTelephonyRequest = IAPMessageUtility.performActionForTelephonyRequest(telephonyRequest, iAP2Link.this.iAPCommunicationsManager);
                final iAP2Link this$0 = iAP2Link.this;
                final CallAction action = telephonyRequest.action;
                RequestStatus requestStatus;
                if (performActionForTelephonyRequest) {
                    requestStatus = RequestStatus.REQUEST_SUCCESS;
                }
                else {
                    requestStatus = RequestStatus.REQUEST_NOT_AVAILABLE;
                }
                this$0.sendMessageAsNavdyEvent(new TelephonyResponse(action, requestStatus, null));
                return true;
            }
        });
        this.eventProcessors.put(NavdyEvent.MessageType.PhoneStatusRequest, (NavdyEventProcessor)new NavdyEventProcessor() {
            @Override
            public boolean processNavdyEvent(final NavdyEvent navdyEvent) {
                iAP2Link.this.sendMessageAsNavdyEvent(new PhoneStatusResponse(RequestStatus.REQUEST_SUCCESS, iAP2Link.this.currentPhoneEvent));
                return true;
            }
        });
        this.eventProcessors.put(NavdyEvent.MessageType.LaunchAppEvent, (NavdyEventProcessor)new NavdyEventProcessor() {
            @Override
            public boolean processNavdyEvent(final NavdyEvent navdyEvent) {
                final LaunchAppEvent launchAppEvent = navdyEvent.<LaunchAppEvent>getExtension(Ext_NavdyEvent.launchAppEvent);
                if (!TextUtils.isEmpty((CharSequence)launchAppEvent.appBundleID)) {
                    iAP2Link.this.iAPProcessor.launchApp(launchAppEvent.appBundleID, false);
                }
                else {
                    iAP2Link.this.iAPProcessor.launchApp(false);
                }
                return true;
            }
        });
        this.eventProcessors.put(NavdyEvent.MessageType.CallStateUpdateRequest, (NavdyEventProcessor)new NavdyEventProcessor() {
            @Override
            public boolean processNavdyEvent(final NavdyEvent navdyEvent) {
                return true;
            }
        });
    }
    
    private BluetoothDevice getRemoteDevice(final SocketAdapter socketAdapter) {
        return this.mAdapter.getRemoteDevice(socketAdapter.getRemoteDevice().getBluetoothAddress());
    }
    
    private boolean startProtobufLink(final SocketAdapter socketAdapter) {
        boolean b = false;
        Closeable inputStream = null;
        try {
            final InputStream inputStream2 = (InputStream)(inputStream = socketAdapter.getInputStream());
            final OutputStream outputStream = socketAdapter.getOutputStream();
            inputStream = inputStream2;
            inputStream = inputStream2;
            final ReaderThread readerThread = new ReaderThread(ConnectionType.EA_PROTOBUF, inputStream2, this.linkListener, false);
            inputStream = inputStream2;
            this.readerThread = readerThread;
            inputStream = inputStream2;
            this.proxyStream.setOutputStream(outputStream);
            this.readerThread.start();
            b = true;
            return b;
        }
        catch (IOException ex) {
            IOUtils.closeStream(inputStream);
            this.readerThread = null;
            return b;
        }
    }
    
    private void stopProtobufLink() {
        if (this.readerThread != null) {
            this.readerThread.cancel();
            this.readerThread = null;
        }
        this.proxyStream.setOutputStream(null);
    }
    
    void addEventProcessor(final NavdyEvent.MessageType messageType, final NavdyEventProcessor navdyEventProcessor) {
        if (this.eventProcessors.containsKey(messageType)) {
            iAP2Link.sLogger.w("Already have a processor for that type!");
        }
        this.eventProcessors.put(messageType, navdyEventProcessor);
    }
    
    @Override
    public void close() {
        if (this.musicHelper != null) {
            this.musicHelper.close();
        }
        if (this.connectedThread != null) {
            this.connectedThread.cancel();
            this.connectedThread = null;
        }
        if (this.writerThread != null) {
            this.writerThread.cancel();
            this.writerThread = null;
        }
        if (this.readerThread != null) {
            this.readerThread.cancel();
            this.readerThread = null;
        }
    }
    
    @Override
    public int getBandWidthLevel() {
        return this.bandwidthLevel;
    }
    
    @Override
    public byte[] getLocalAddress() {
        return Utils.parseMACAddress(this.mAdapter.getAddress());
    }
    
    public String getName() {
        return this.mAdapter.getName();
    }
    
    @Override
    public void onCallStateUpdate(final CallStateUpdate callStateUpdate) {
        final PhoneEvent phoneEventForCallStateUpdate = IAPMessageUtility.getPhoneEventForCallStateUpdate(callStateUpdate, this.lastCallActionRequested);
        if (phoneEventForCallStateUpdate != null) {
            this.currentPhoneEvent = phoneEventForCallStateUpdate;
            this.lastCallActionRequested = null;
            this.sendMessageAsNavdyEvent(this.currentPhoneEvent);
        }
    }
    
    @Override
    public void onCommunicationUpdate(final CommunicationUpdate communicationUpdate) {
        iAP2Link.sLogger.e("CommunicationUpdate : " + communicationUpdate);
    }
    
    @Override
    public void onError() {
        iAP2Link.sLogger.e("Error in IAP session");
    }
    
    @Override
    public void onReady() {
        try {
            this.iAPCommunicationsManager.setUpdatesListener(this);
            this.iAPCommunicationsManager.startUpdates();
            this.iAPCommunicationsManager.startCommunicationUpdates();
            this.musicHelper.onReady();
            this.iAPProcessor.startHIDSession();
        }
        catch (Throwable t) {
            iAP2Link.sLogger.e("Error starting the call state updates ", t);
        }
    }
    
    @Override
    public void onSessionStart(final EASession proxyEASession) {
        final String protocol = proxyEASession.getProtocol();
        if (protocol.equals("com.navdy.hud.api.v1")) {
            this.startProtobufLink(new EASessionSocketAdapter(proxyEASession));
        }
        else if (protocol.equals("com.navdy.hud.proxy.v1")) {
            iAP2Link.proxyEASession = proxyEASession;
            if (this.linkListener != null) {
                this.linkListener.onNetworkLinkReady();
            }
        }
    }
    
    @Override
    public void onSessionStop(final EASession eaSession) {
        final String protocol = eaSession.getProtocol();
        if (protocol.equals("com.navdy.hud.api.v1")) {
            this.stopProtobufLink();
        }
        else if (protocol.equals("com.navdy.hud.proxy.v1")) {
            iAP2Link.proxyEASession = null;
        }
        eaSession.close();
    }
    
    @Override
    public void queue(final LinkPacket linkPacket) {
        if (this.connectedThread != null) {
            this.connectedThread.write(linkPacket.data);
        }
    }
    
    void sendMessageAsNavdyEvent(final Message message) {
        if (message != null && this.linkListener != null) {
            this.linkListener.onNavdyEventReceived(NavdyEventUtil.eventFromMessage(message).toByteArray());
        }
    }
    
    @Override
    public void setBandwidthLevel(final int n) {
        this.bandwidthLevel = n;
        this.musicHelper.setBandwidthLevel(n);
    }
    
    @Override
    public boolean start(final SocketAdapter socketAdapter, final LinkedBlockingDeque<EventRequest> linkedBlockingDeque, final LinkListener linkListener) {
        if ((this.writerThread != null && !this.writerThread.isClosing()) || (this.readerThread != null && !this.readerThread.isClosing())) {
            throw new IllegalStateException("Must stop threads before calling startLink");
        }
        this.linkListener = linkListener;
        this.connectedThread = new ConnectedThread(socketAdapter);
        this.writerThread = new WriterThread(linkedBlockingDeque, this.proxyStream, this.eventProcessor);
        this.connectedThread.start();
        this.writerThread.start();
        return true;
    }
    
    private class ConnectedThread extends IOThread
    {
        private final InputStream mmInStream;
        private final OutputStream mmOutStream;
        private final SocketAdapter mmSocket;
        
        public ConnectedThread(final SocketAdapter mmSocket) {
            iAP2Link.sLogger.d("create ConnectedThread");
            this.mmSocket = mmSocket;
            iAP2Link.this = null;
            final OutputStream outputStream = null;
            while (true) {
                try {
                    final iAP2Link iap2Link = iAP2Link.this = (iAP2Link)mmSocket.getInputStream();
                    final OutputStream outputStream2 = mmSocket.getOutputStream();
                    iAP2Link.this = iap2Link;
                    this.mmInStream = (InputStream)iAP2Link.this;
                    this.mmOutStream = outputStream2;
                }
                catch (IOException ex) {
                    iAP2Link.sLogger.e("temp sockets not created", ex);
                    final OutputStream outputStream2 = outputStream;
                    continue;
                }
                break;
            }
        }
        
        @Override
        public void cancel() {
            super.cancel();
            IOUtils.closeStream(this.mmOutStream);
            IOUtils.closeStream(this.mmSocket);
        }
        
        @Override
        public void run() {
            final BluetoothDevice access$800 = iAP2Link.this.getRemoteDevice(this.mmSocket);
            iAP2Link.sLogger.i("BEGIN mConnectedThread - " + access$800.getAddress() + " name:" + access$800.getName());
            final Connection.DisconnectCause normal = Connection.DisconnectCause.NORMAL;
            iAP2Link.this.linkListener.linkEstablished(ConnectionType.BT_IAP2_LINK);
            iAP2Link.this.linkLayer.connectionStarted(access$800.getAddress(), access$800.getName());
            final byte[] array = new byte[1024];
            Enum<Connection.DisconnectCause> aborted;
            while (true) {
                aborted = normal;
                if (!this.closing) {
                    try {
                        iAP2Link.this.linkLayer.queue(new LinkPacket(Arrays.copyOf(array, this.mmInStream.read(array))));
                        continue;
                    }
                    catch (IOException ex) {
                        aborted = normal;
                        if (!this.closing) {
                            iAP2Link.sLogger.e("disconnected: " + ex.getMessage());
                            aborted = Connection.DisconnectCause.ABORTED;
                        }
                    }
                    break;
                }
                break;
            }
            IOUtils.closeStream(this.mmInStream);
            iAP2Link.this.linkLayer.connectionEnded();
            iAP2Link.this.linkListener.linkLost(ConnectionType.BT_IAP2_LINK, (Connection.DisconnectCause)aborted);
        }
        
        public void write(final byte[] array) {
            try {
                this.mmOutStream.write(array);
            }
            catch (IOException ex) {
                iAP2Link.sLogger.e("Exception during write", ex);
            }
        }
    }
    
    interface NavdyEventProcessor
    {
        boolean processNavdyEvent(final NavdyEvent p0);
    }
    
    private class ProxyStream extends OutputStream
    {
        private OutputStream outputStream;
        
        public void setOutputStream(final OutputStream outputStream) {
            this.outputStream = outputStream;
        }
        
        @Override
        public void write(final int n) throws IOException {
            throw new IOException("Shouldn't be calling this method!");
        }
        
        @Override
        public void write(@NonNull final byte[] array) throws IOException {
            if (this.outputStream != null) {
                this.outputStream.write(array);
            }
        }
    }
}
