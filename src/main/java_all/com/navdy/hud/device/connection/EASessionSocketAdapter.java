package com.navdy.hud.device.connection;

import com.navdy.service.library.device.NavdyDeviceId;
import java.io.OutputStream;
import java.io.InputStream;
import java.io.IOException;
import com.navdy.service.library.log.Logger;
import com.navdy.hud.mfi.EASession;
import com.navdy.service.library.network.SocketAdapter;

public class EASessionSocketAdapter implements SocketAdapter
{
    EASession eaSession;
    private Logger sLogger;
    
    public EASessionSocketAdapter(final EASession eaSession) {
        this.sLogger = new Logger(EASessionSocketAdapter.class);
        this.eaSession = eaSession;
    }
    
    @Override
    public void close() throws IOException {
        this.sLogger.d("closing connection");
        if (this.eaSession != null) {
            this.eaSession.close();
            this.eaSession = null;
        }
    }
    
    @Override
    public void connect() throws IOException {
        if (!this.isConnected()) {
            throw new IOException("Can't create outbound EASession connections");
        }
        this.sLogger.d("Connecting to existing eaSession");
    }
    
    @Override
    public InputStream getInputStream() throws IOException {
        InputStream inputStream;
        if (this.eaSession != null) {
            inputStream = this.eaSession.getInputStream();
        }
        else {
            inputStream = null;
        }
        return inputStream;
    }
    
    @Override
    public OutputStream getOutputStream() throws IOException {
        OutputStream outputStream;
        if (this.eaSession != null) {
            outputStream = this.eaSession.getOutputStream();
        }
        else {
            outputStream = null;
        }
        return outputStream;
    }
    
    @Override
    public NavdyDeviceId getRemoteDevice() {
        NavdyDeviceId navdyDeviceId;
        if (this.isConnected()) {
            navdyDeviceId = new NavdyDeviceId(NavdyDeviceId.Type.EA, this.eaSession.getMacAddress(), this.eaSession.getName());
        }
        else {
            navdyDeviceId = null;
        }
        return navdyDeviceId;
    }
    
    @Override
    public boolean isConnected() {
        return this.eaSession != null;
    }
}
