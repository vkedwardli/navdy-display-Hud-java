package com.navdy.obd;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.os.Parcelable;

public class VoltageSettings implements Parcelable
{
    public static final Parcelable.Creator<VoltageSettings> CREATOR;
    public static final float DEFAULT_CHARGING_VOLTAGE = 13.1f;
    public static final float DEFAULT_ENGINE_OFF_VOLTAGE = 12.9f;
    public static final float DEFAULT_LOW_BATTERY_VOLTAGE = 12.2f;
    public final float chargingVoltage;
    public final float engineOffVoltage;
    public final float lowBatteryVoltage;
    
    static {
        CREATOR = (Parcelable.Creator)new Parcelable.Creator<VoltageSettings>() {
            public VoltageSettings createFromParcel(final Parcel parcel) {
                return new VoltageSettings(parcel);
            }
            
            public VoltageSettings[] newArray(final int n) {
                return new VoltageSettings[n];
            }
        };
    }
    
    public VoltageSettings() {
        this.lowBatteryVoltage = 12.2f;
        this.engineOffVoltage = 12.9f;
        this.chargingVoltage = 13.1f;
    }
    
    public VoltageSettings(final float lowBatteryVoltage, final float engineOffVoltage, final float chargingVoltage) {
        this.lowBatteryVoltage = lowBatteryVoltage;
        this.engineOffVoltage = engineOffVoltage;
        this.chargingVoltage = chargingVoltage;
    }
    
    public VoltageSettings(final Parcel parcel) {
        this.lowBatteryVoltage = parcel.readFloat();
        this.engineOffVoltage = parcel.readFloat();
        this.chargingVoltage = parcel.readFloat();
    }
    
    public int describeContents() {
        return 0;
    }
    
    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("VoltageSettings{");
        sb.append("lowBatteryVoltage=").append(this.lowBatteryVoltage);
        sb.append(", engineOffVoltage=").append(this.engineOffVoltage);
        sb.append(", chargingVoltage=").append(this.chargingVoltage);
        sb.append('}');
        return sb.toString();
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        parcel.writeFloat(this.lowBatteryVoltage);
        parcel.writeFloat(this.engineOffVoltage);
        parcel.writeFloat(this.chargingVoltage);
    }
}
