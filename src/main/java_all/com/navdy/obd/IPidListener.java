package com.navdy.obd;

import android.os.Parcelable.Creator;
import android.os.Parcel;
import android.os.IBinder;
import android.os.Binder;
import java.util.List;
import android.os.RemoteException;
import android.os.IInterface;

public interface IPidListener extends IInterface
{
    void onConnectionStateChange(final int p0) throws RemoteException;
    
    void pidsChanged(final List<Pid> p0) throws RemoteException;
    
    void pidsRead(final List<Pid> p0, final List<Pid> p1) throws RemoteException;
    
    public abstract static class Stub extends Binder implements IPidListener
    {
        private static final String DESCRIPTOR = "com.navdy.obd.IPidListener";
        static final int TRANSACTION_onConnectionStateChange = 2;
        static final int TRANSACTION_pidsChanged = 1;
        static final int TRANSACTION_pidsRead = 3;
        
        public Stub() {
            this.attachInterface((IInterface)this, "com.navdy.obd.IPidListener");
        }
        
        public static IPidListener asInterface(final IBinder binder) {
            IPidListener pidListener;
            if (binder == null) {
                pidListener = null;
            }
            else {
                final IInterface queryLocalInterface = binder.queryLocalInterface("com.navdy.obd.IPidListener");
                if (queryLocalInterface != null && queryLocalInterface instanceof IPidListener) {
                    pidListener = (IPidListener)queryLocalInterface;
                }
                else {
                    pidListener = new Proxy(binder);
                }
            }
            return pidListener;
        }
        
        public IBinder asBinder() {
            return (IBinder)this;
        }
        
        public boolean onTransact(final int n, final Parcel parcel, final Parcel parcel2, final int n2) throws RemoteException {
            boolean onTransact = true;
            switch (n) {
                default:
                    onTransact = super.onTransact(n, parcel, parcel2, n2);
                    break;
                case 1598968902:
                    parcel2.writeString("com.navdy.obd.IPidListener");
                    break;
                case 1:
                    parcel.enforceInterface("com.navdy.obd.IPidListener");
                    this.pidsChanged(parcel.createTypedArrayList((Parcelable.Creator)Pid.CREATOR));
                    parcel2.writeNoException();
                    break;
                case 2:
                    parcel.enforceInterface("com.navdy.obd.IPidListener");
                    this.onConnectionStateChange(parcel.readInt());
                    parcel2.writeNoException();
                    break;
                case 3:
                    parcel.enforceInterface("com.navdy.obd.IPidListener");
                    this.pidsRead(parcel.createTypedArrayList((Parcelable.Creator)Pid.CREATOR), parcel.createTypedArrayList((Parcelable.Creator)Pid.CREATOR));
                    parcel2.writeNoException();
                    break;
            }
            return onTransact;
        }
        
        private static class Proxy implements IPidListener
        {
            private IBinder mRemote;
            
            Proxy(final IBinder mRemote) {
                this.mRemote = mRemote;
            }
            
            public IBinder asBinder() {
                return this.mRemote;
            }
            
            public String getInterfaceDescriptor() {
                return "com.navdy.obd.IPidListener";
            }
            
            @Override
            public void onConnectionStateChange(final int n) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.navdy.obd.IPidListener");
                    obtain.writeInt(n);
                    this.mRemote.transact(2, obtain, obtain2, 0);
                    obtain2.readException();
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
            
            @Override
            public void pidsChanged(final List<Pid> list) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.navdy.obd.IPidListener");
                    obtain.writeTypedList((List)list);
                    this.mRemote.transact(1, obtain, obtain2, 0);
                    obtain2.readException();
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
            
            @Override
            public void pidsRead(final List<Pid> list, final List<Pid> list2) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.navdy.obd.IPidListener");
                    obtain.writeTypedList((List)list);
                    obtain.writeTypedList((List)list2);
                    this.mRemote.transact(3, obtain, obtain2, 0);
                    obtain2.readException();
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
        }
    }
}
